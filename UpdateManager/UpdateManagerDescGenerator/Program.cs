﻿using System;
using System.IO;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UMDescGenerator
{
    class Program
    {
        static void Main(string[] args)
        {            
            //var result = new List<string>();

            string resultFileName = "updatemanagerdesc.xml";
            string[] extensions = { ".exe", ".dll" };
            string[] mustNotContain = { ".vshost.exe" };
            string defaultReleaseDirectory = @"C:\3. DEV\ProfitGrabberPro\WindowsApplication.GUI.ProfitGrabber\bin\Release";

            string directory = defaultReleaseDirectory;

            Console.WriteLine($"UPDATE MANAGER DESC GENERATOR");
            Console.WriteLine(string.Empty);
            Console.WriteLine($"Default Directory is set to: {defaultReleaseDirectory}");
            Console.WriteLine(string.Empty);
            Console.WriteLine(string.Empty);

            Console.Write($"Enter NEW Release Binaries Directory or just hit ENTER to use DEFAULT:");
            string userDirectory = Console.ReadLine();

            if (string.Empty != userDirectory)
                directory = userDirectory;

            if (Directory.Exists(directory))
            {
                var dir = new DirectoryInfo(directory);

                var kvps = dir.GetFiles("*.*", SearchOption.TopDirectoryOnly)
                    .Where(file => extensions.Any(ext => ext == file.Extension) && !mustNotContain.Any(mnc => file.Name.Contains(mnc)) )
                    .Select(f => new KeyValuePair<string, string>(f.Name, FileVersionInfo.GetVersionInfo(f.FullName).FileVersion))
                    .ToList();
                                                
                if (Dump(resultFileName, kvps))
                {
                    Console.WriteLine($"Check {resultFileName} for results!!");
                }
            }
            else
            {
                Console.WriteLine($"Not Existing Directory: {directory}");
            }
                                                    
            Console.WriteLine($"Hit ENTER to Exit");
            Console.ReadLine();
        }

        static bool Dump(string resultFileName, List<KeyValuePair<string, string>> fileInfos)
        {
            try
            {
                string tab = "   ";
                using (StreamWriter sw = new StreamWriter(resultFileName))
                {
                    sw.WriteLine("<?xml version=\"1.0\" encoding=\"UTF-8\" ?>");
                    sw.WriteLine("<UpdateFiles>");
                    fileInfos.ForEach(kvp =>
                        {
                            sw.WriteLine($"{tab}<FileInfo FileName=\"{kvp.Key}\" Version=\"{kvp.Value}\" DestinationPath=\"[INSTALLDIR]\"/>");
                            Console.WriteLine($"{tab}<FileInfo FileName=\"{kvp.Key}\" Version=\"{kvp.Value}\" DestinationPath=\"[INSTALLDIR]\"/>");
                        }
                    );
                    sw.WriteLine("</UpdateFiles>");
                    sw.Flush();
                    sw.Close();
                    return true;
                }
            }
            catch(Exception e)
            {
                Console.WriteLine($"Dump failed with: {e}");
                return false;
            }
        }
    }
}
