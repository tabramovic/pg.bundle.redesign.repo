using System;

namespace UpdateManager
{
	/// <summary>
	/// Summary description for ComponentDescriptor.
	/// </summary>
	public class ComponentDescriptor
	{
		private string fromLocation = string.Empty;
		private string toLocation = string.Empty;
		private string componentName = string.Empty;

		/// <summary>
		/// Component Descriptor
		/// </summary>
		/// <param name="fromLocation">From [Full path]</param>
		/// <param name="toLocation">To</param>
		/// <param name="componentName">Component name</param>
		public ComponentDescriptor(string fromLocation, string toLocation, string componentName)
		{
			this.fromLocation = fromLocation;
			this.toLocation = toLocation;
			this.componentName = componentName;
		}

		public string FromLocation
		{
			get {return this.fromLocation;}
		}

		public string ToLocation
		{
			get {return this.toLocation;}
		}

		public string ComponentName
		{
			get {return this.componentName;}
		}
	}
}
