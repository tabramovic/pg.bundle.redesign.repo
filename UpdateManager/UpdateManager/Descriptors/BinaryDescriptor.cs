using System;

namespace UpdateManager
{
	/// <summary>
	/// Summary description for BinaryDescriptor.
	/// </summary>
	public class BinaryDescriptor : IComparable
	{
		private string fileName = string.Empty;
		private string version = string.Empty;
		private string destinationPath = string.Empty;

		public BinaryDescriptor(string fileName, string version, string destinationPath)
		{
			this.fileName = fileName;
			this.version = version;
			this.destinationPath = destinationPath;
		}

		public string FileName
		{
			get {return this.fileName;}
		}

		public string Version
		{
			get {return this.version;}
		}

		public string DestinationPath
		{
			get {return this.destinationPath;}
		}
	
		public ComponentDescriptor BinDesc2CompDesc()
		{
			string localDestination = this.destinationPath;
			localDestination = localDestination.Replace(Globals.InstallDir, Globals.PG_DIR);
			ComponentDescriptor cd = new ComponentDescriptor(Globals.UpdatesDir + this.fileName, localDestination + this.fileName, this.fileName);
			return cd;
		}

		#region IComparable Members

		public int CompareTo(object obj)
		{							
			BinaryDescriptor bd = obj as BinaryDescriptor;
			if (null == bd)
				return -1;

			System.Version leftVer = new System.Version(this.Version);
			System.Version rightVer = new System.Version(bd.Version);			
			
			return leftVer.CompareTo(rightVer);						
		}

		#endregion
	}
}
