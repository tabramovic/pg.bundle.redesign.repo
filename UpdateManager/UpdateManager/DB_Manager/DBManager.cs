using System;
using System.Collections;
using System.Text;
using System.ServiceProcess;
using System.IO;

namespace UpdateManager
{
    public class DBManager
    {
		public delegate void DBManagerMsgEventHandler(object sender, DBManagerEventArgs e);		
		public event DBManagerMsgEventHandler OnDBManagerMsgChanged;
		

        #region DATA
		string _origin, _destination;
        string _serviceName = "MSSQL$PG_DB2";
		ServiceController _service; 
        #endregion

        #region CTR
        public DBManager(string origin, string destination)
        {
            _origin = origin;
            _destination = destination;
			_service = GetService(_serviceName);
        } 
        #endregion

        #region PROPERTIES
        public string ServiceName
        {
            get { return this._serviceName; }
            set 
			{ 
				_serviceName = value; 
				_service = GetService(_serviceName);
			}
        }

        public string Origin
        { get { return this._origin; } }

        public string Destiantion
        { get { return this._destination; } } 
        #endregion

		private void FireMsg(string msg)
		{
			if (null != OnDBManagerMsgChanged)
				OnDBManagerMsgChanged(this, new DBManagerEventArgs(msg));
		}

        ServiceController[] GetServices()
        {
            return ServiceController.GetServices();
        }

        ServiceController GetService(string serviceName)
        {            
            foreach (ServiceController sc in GetServices())
            {
                if (sc.ServiceName == serviceName)                
                    return sc;                
            }
            return null;
        }
        

        bool StartService()
        {			
			if (null == _service)							
				return false;						
			else
			{	

				if (_service.Status == ServiceControllerStatus.Stopped)
				{
					// Start the service if the current status is stopped.

					FireMsg("Starting " + _service.ServiceName +" service...");
					try 
					{
						// Start the service, and wait until its status is "Running".
						_service.Start();
						_service.WaitForStatus(ServiceControllerStatus.Running);
   
						// Display the current service status.
						FireMsg(_service.ServiceName +  " service status is now set to " + _service.Status.ToString());
						return true;
					}
					catch (InvalidOperationException)
					{
						FireMsg("Can not start " + _service.ServiceName + " service");
					}
				}				
				return false;
			}
		}

        bool StopService(out string errMsg)
        {
			errMsg = string.Empty;

			if (null == _service)
			{						
				errMsg = "Can not stop database service!";
				return false;			
			}
									
			if (_service.CanStop)
			{
				if (_service.Status == ServiceControllerStatus.Running)
				{
					// Start the service if the current status is stopped.

					FireMsg("Stopping the " + _service.ServiceName + " service...");
					try 
					{
						// Stop the service, and wait until its status is "Stopped".
						_service.Stop();
						_service.WaitForStatus(ServiceControllerStatus.Stopped);
   
						// Display the current service status.
						FireMsg(_service.ServiceName +  " service status is now set to " + _service.Status.ToString());
						return true;
					}
					catch (InvalidOperationException)
					{
						errMsg = "Can not stop database service!";
						FireMsg("Can not stop " + _service.ServiceName + " service");
					}
				}				

			}					
			return false;								
        }

		bool CopyDBFiles(string source, string destination, bool appendDateTime, out string errMsg)
		{
			errMsg = string.Empty;
			if (!Directory.Exists(source))
			{
				errMsg = "Invalid Source Folder";
				return false;
			}
			if (!Directory.Exists(destination))
			{	
				try
				{
					Directory.CreateDirectory(destination);
				}
				catch
				{
					errMsg = "Invalid Destination Folder";
					return false;
				}
			}

			string currDest = destination;

			if (appendDateTime)
				currDest = destination;
						
			try
			{
				if (!Directory.Exists(currDest))
					Directory.CreateDirectory(currDest);

				if (TestForDBFiles(source))
				{

					File.Copy(source + @"\" + "dbDealMaker_Data.MDF", currDest + @"\" + "dbDealMaker_Data.MDF", true);			
					File.Copy(source + @"\" + "dbDealMaker_Log.LDF", currDest + @"\" + "dbDealMaker_Log.LDF", true);	
					FireMsg("Copying files database...");
				}
				else
				{
					errMsg = "Selected folder does not contain ProfitGrabber Pro database files!";
				}
			}
			catch (Exception exc)
			{
				errMsg = exc.Message;
			}
			
			return true;
		}

		bool TestForDBFiles(string source)
		{
			if (
				!File.Exists(source + @"\" + "dbDealMaker_Data.MDF") ||
				!File.Exists(source + @"\" + "dbDealMaker_Log.LDF") 
				)
			{				
				FireMsg("Selected folder does not contain ProfitGrabber Pro database files!");
				return false;
			}
			return true;
		}

		public bool CopyDatabase(bool appendDateTime,  out string errMsg)
		{
			bool res = true;
			errMsg = string.Empty;

			if (false == TestForDBFiles(_origin))
			{
				FireMsg("Selected folder does not contain ProfitGrabber Pro database files!");
				errMsg = "Selected folder does not contain ProfitGrabber Pro database files!";
				return false;
			}			

			if (false == StopService(out errMsg))
				return false;
			
			if (false == CopyDBFiles(_origin, _destination, appendDateTime, out errMsg))
				res = false;

			StartService();

			return res;
		}		
    }
}
