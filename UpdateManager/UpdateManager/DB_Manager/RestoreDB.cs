using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace UpdateManager
{
	/// <summary>
	/// Summary description for RestoreDB.
	/// </summary>
	public class RestoreDB : System.Windows.Forms.Form
	{
		bool validClose = false;
		private string _path = string.Empty;
		private System.Windows.Forms.Button _Close;
		private System.Windows.Forms.Label _Progress;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Button _startRestore;
		private System.Windows.Forms.Button _Browse;
		private System.Windows.Forms.TextBox _backupFolder;
		private System.Windows.Forms.Label label1;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public RestoreDB()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			_Browse.Click	+= new EventHandler(_Browse_Click);
			_Close.Click += new EventHandler(_Close_Click);
			_startRestore.Click += new EventHandler(_startRestore_Click);
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(RestoreDB));
			this._Close = new System.Windows.Forms.Button();
			this._Progress = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this._startRestore = new System.Windows.Forms.Button();
			this._Browse = new System.Windows.Forms.Button();
			this._backupFolder = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// _Close
			// 
			this._Close.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this._Close.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this._Close.Location = new System.Drawing.Point(480, 176);
			this._Close.Name = "_Close";
			this._Close.Size = new System.Drawing.Size(72, 23);
			this._Close.TabIndex = 17;
			this._Close.Text = "Close";
			// 
			// _Progress
			// 
			this._Progress.Location = new System.Drawing.Point(24, 144);
			this._Progress.Name = "_Progress";
			this._Progress.Size = new System.Drawing.Size(528, 23);
			this._Progress.TabIndex = 16;
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(16, 80);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(152, 23);
			this.label2.TabIndex = 15;
			this.label2.Text = "2. Start Restore Procedure";
			// 
			// _startRestore
			// 
			this._startRestore.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this._startRestore.Location = new System.Drawing.Point(40, 104);
			this._startRestore.Name = "_startRestore";
			this._startRestore.Size = new System.Drawing.Size(64, 23);
			this._startRestore.TabIndex = 14;
			this._startRestore.Text = "Start";
			// 
			// _Browse
			// 
			this._Browse.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this._Browse.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(210)), ((System.Byte)(216)), ((System.Byte)(240)));
			this._Browse.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this._Browse.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this._Browse.Location = new System.Drawing.Point(512, 38);
			this._Browse.Name = "_Browse";
			this._Browse.Size = new System.Drawing.Size(40, 23);
			this._Browse.TabIndex = 13;
			this._Browse.Text = "...";
			// 
			// _backupFolder
			// 
			this._backupFolder.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this._backupFolder.Location = new System.Drawing.Point(40, 39);
			this._backupFolder.Name = "_backupFolder";
			this._backupFolder.ReadOnly = true;
			this._backupFolder.Size = new System.Drawing.Size(464, 20);
			this._backupFolder.TabIndex = 12;
			this._backupFolder.TabStop = false;
			this._backupFolder.Text = "";
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(16, 16);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(128, 23);
			this.label1.TabIndex = 11;
			this.label1.Text = "1. Set DB Backup Folder";
			// 
			// RestoreDB
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(210)), ((System.Byte)(216)), ((System.Byte)(240)));
			this.ClientSize = new System.Drawing.Size(568, 214);
			this.Controls.Add(this._Close);
			this.Controls.Add(this._Progress);
			this.Controls.Add(this.label2);
			this.Controls.Add(this._startRestore);
			this.Controls.Add(this._Browse);
			this.Controls.Add(this._backupFolder);
			this.Controls.Add(this.label1);
			this.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "RestoreDB";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Restore Database Wizard";
			this.ResumeLayout(false);

		}
		#endregion

		private void _Browse_Click(object sender, EventArgs e)
		{
			BrowseForFolderClass myFolderBrowser = new BrowseForFolderClass();
			_path = myFolderBrowser.BrowseForFolder("Please, select a folder");
			this._backupFolder.Text = _path;
		}

		void EnableClosing(bool value)
		{
			_Close.Enabled = value;
		}
				
		protected override void OnClosing(CancelEventArgs e)
		{
			if (!validClose)
				e.Cancel = true;

			base.OnClosing (e);
		}

		private void _Close_Click(object sender, EventArgs e)
		{
			validClose = true;
			this.Close();
		}

		private void _startRestore_Click(object sender, EventArgs e)
		{
			if (string.Empty == _path)
			{
				MessageBox.Show(this, "Please select backup folder!", "Missing info!", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
			
			string errMsg = string.Empty;
			ShowDBManagerMsgAsync(string.Empty);
			EnableClosing(false);

			DBManager dbm = new DBManager(_path, Globals.PG_DIR);
			dbm.OnDBManagerMsgChanged += new UpdateManager.DBManager.DBManagerMsgEventHandler(OnDBManagerMsgChanged);
			if (false == dbm.CopyDatabase(false, out errMsg))
			{
				MessageBox.Show(this, errMsg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
			else
			{
				MessageBox.Show(this, "Restore Database successfull!", "Sucess", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
			dbm.OnDBManagerMsgChanged -= new UpdateManager.DBManager.DBManagerMsgEventHandler(OnDBManagerMsgChanged);
			EnableClosing(true);
		}		

		private void OnDBManagerMsgChanged(object sender, DBManagerEventArgs e)
		{
			ShowDBManagerMsgAsync(e.Message);
		}

		delegate void ShowDBManagerMsgAsyncDelegate (string _msg);
		void ShowDBManagerMsgAsync(string _msg)
		{
			if (this.InvokeRequired)
			{
				this.BeginInvoke(new ShowDBManagerMsgAsyncDelegate(ShowDBManagerMsgAsync), new object[] {_msg});				
			}
			else
			{								
				_Progress.Text = _msg;
				_Progress.Update();
			}
		}
	}
}
