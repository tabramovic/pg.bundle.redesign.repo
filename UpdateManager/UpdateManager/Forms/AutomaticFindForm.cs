using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

//Added
using System.IO;
using System.Threading;

namespace UpdateManager
{

	// A delegate type for hooking up change notifications. 
	public delegate void ChangedDirectoryEventHandler(object sender, DirChangedEventArgs e);

	/// <summary>
	/// Summary description for AutomaticFindForm.
	/// </summary>
	public class AutomaticFindForm : System.Windows.Forms.Form
	{
		public event ChangedDirectoryEventHandler DirChanged;
		private bool bFound = false;
		private string pgDir = string.Empty;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox tbDir;
		private System.Windows.Forms.Button bClose;
		private Thread workerThread = null;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public bool Found
		{
			get {return this.bFound;}			
		}


		public AutomaticFindForm()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			this.DirChanged += new ChangedDirectoryEventHandler(DirChanged_EH);
			this.bClose.Click += new EventHandler(AbortAndClose_EH);
			workerThread = new Thread(new ThreadStart(FindAppDir));
			workerThread.Start();
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.tbDir = new System.Windows.Forms.TextBox();
			this.bClose = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label1.Location = new System.Drawing.Point(8, 16);
			this.label1.Name = "label1";
			this.label1.TabIndex = 0;
			this.label1.Text = "Current Directory:";
			// 
			// tbDir
			// 
			this.tbDir.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.tbDir.Location = new System.Drawing.Point(24, 32);
			this.tbDir.Name = "tbDir";
			this.tbDir.ReadOnly = true;
			this.tbDir.Size = new System.Drawing.Size(616, 20);
			this.tbDir.TabIndex = 1;
			this.tbDir.TabStop = false;
			this.tbDir.Text = "";
			// 
			// bClose
			// 
			this.bClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.bClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.bClose.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.bClose.Location = new System.Drawing.Point(568, 64);
			this.bClose.Name = "bClose";
			this.bClose.TabIndex = 1;
			this.bClose.Text = "Cancel";
			// 
			// AutomaticFindForm
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(210)), ((System.Byte)(216)), ((System.Byte)(240)));
			this.CancelButton = this.bClose;
			this.ClientSize = new System.Drawing.Size(658, 96);
			this.Controls.Add(this.bClose);
			this.Controls.Add(this.tbDir);
			this.Controls.Add(this.label1);
			this.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
			this.Name = "AutomaticFindForm";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Scanning Directories...";
			this.ResumeLayout(false);

		}
		#endregion

		public string PG_DIR
		{
			get {return this.pgDir;}
		}

		private void UpdateUponFound(string dir)
		{
			this.bFound = true;
			this.bClose.Text = "Done";

			if (false == dir.EndsWith(@"\"))
				dir = dir + @"\";

			this.pgDir = dir;
			
			Application.CommonAppDataRegistry.SetValue("PG_DIR", dir);
			//fire event
		}


		// Invoke the Changed event; called whenever list changes 
		protected virtual void OnDirChanged(DirChangedEventArgs e) 
		{ 
			if (DirChanged != null) 
				DirChanged(this, e); 
		} 

	
		// Print out all logical drives on the system.
		private string[] GetLogicalDrives() 
		{
			string[] drives = null;
			try 
			{
				drives = System.IO.Directory.GetLogicalDrives();
			}
			catch (System.IO.IOException) 
			{
				MessageBox.Show("An I/O error occured!");
				return null;
			}
			catch (System.Security.SecurityException) 
			{
				MessageBox.Show("You do not have the required permission!");
				return null;
			}
			return drives;
		}

		private bool QuickFind()
		{
			bool bResult = false;
			string defaultDir = Globals.PredefinedInstalldir;
			try
			{
				string[] instances = Directory.GetFiles(defaultDir, "*DealMaker*");
				OnDirChanged(new DirChangedEventArgs(defaultDir));
				foreach (string instance in instances)
				{
					//remove Dir Name
					string appName = instance.Replace(defaultDir, string.Empty);
					if (@"\" == appName.Substring(0, 1))
					{
						appName = appName.Substring(1);
					}

						
					if (true == appName.Equals("DealMaker.exe"))
					{
						this.UpdateUponFound(Globals.PredefinedInstalldir);
								
						bResult = true;
						this.pgDir = defaultDir;
						return bResult;
					}
				}
			}
			catch (Exception)
			{
				bResult = false;
			}
			return bResult;
		}

		private void FindAppDir()
		{
			
			if (true == this.QuickFind())
			{
				return;
			}

			string[] drives = this.GetLogicalDrives();
			foreach (string drive in drives)
			{				
				try
				{
					if (drive == @"A:\")
						continue;

					if (false == this.bFound)
						this.ScanDir(drive);
				}
				catch (System.IO.IOException)
				{
					//we ignore if CD ROM drive isn't ready (there is noCD inside)
				}
				finally
				{
					//this.bClose.Text = "Close";
					OnDirChanged(new DirChangedEventArgs(this.pgDir));
				}
			}
		}

		private void ScanDir(string dir)
		{			
			if (true == bFound)
				return;

			OnDirChanged(new DirChangedEventArgs(dir)); 
			
			
			try
			{
				string[] dirs = Directory.GetDirectories(dir);
				if (-1 != dir.IndexOf("ProfitGrabber"))
				{									
					string[] instances = Directory.GetFiles(dir, "*DealMaker*");				
					foreach (string instance in instances)
					{
						//remove Dir Name
						string appName = instance.Replace(dir, string.Empty);
						if (@"\" == appName.Substring(0, 1))
						{
							appName = appName.Substring(1);
						}

						
						if (true == appName.Equals("DealMaker.exe"))
						{
							this.UpdateUponFound(dir);
								
							return;
						}
					}
				}
				if (false == this.bFound)
				{
					for (int i = 0; i < dirs.Length && (false == this.bFound); i++)
					{
						ScanDir((string)(dirs[i]));
					}
				}
			}
			catch (Exception)
			{
				return;
			}
		}

		private void DirChanged_EH(object sender, DirChangedEventArgs e)
		{
			this.tbDir.Text = e.Dir;
		}

		private void AbortAndClose_EH(object sender, EventArgs e)
		{			
			if (true == this.workerThread.IsAlive)
			{
				MessageBox.Show(this, "User Interrupted Search!", "Search Canceled",  MessageBoxButtons.OK,  MessageBoxIcon.Exclamation);
				//if (DialogResult.Yes == MessageBox.Show(this, "Are you sure that you want to cancel Search?", "Cancel Search",  MessageBoxButtons.YesNo,  MessageBoxIcon.Question, MessageBoxDefaultButton.Button2))
				{
					this.workerThread.Abort();
					this.Close();
				}								
			}
			else
			{
				this.Close();
			}
			
		}
	}
}
