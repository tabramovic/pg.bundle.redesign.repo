using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

//Added 
using System.Threading;

namespace UpdateManager
{
	

	/// <summary>
	/// Summary description for DownloadTrackForm.
	/// </summary>
	public class DownloadTrackForm : System.Windows.Forms.Form
	{
		bool _validExit = false;		
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label lDownloadFrom;
		private System.Windows.Forms.Label lDownloadTo;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label lStatus;
		private System.Windows.Forms.Button bCancel;
		private System.Windows.Forms.ProgressBar pbStatus;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
		private ComponentDescriptor[] cdArray;		
		private string currentlyDownloadedFile = string.Empty;
		private int currFileIndex = 0;
		private int totalFilesNumber = 0;

		public DownloadTrackForm()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			this.cdArray = cdArray;

			this.bCancel.Click += new EventHandler(CancelDownload_EH);
			DownloadManagerClass.DownloadPercentageChanged += new UpdateManager.DownloadManagerClass.ChangedDownloadPercentageEventHandler(DownloadPercentageChanged);
			DownloadManagerClass.FileDownloading += new UpdateManager.DownloadManagerClass.FileDownloadingEventHandler(FileDownloading);			
			DownloadManagerClass.FileNotFound += new UpdateManager.DownloadManagerClass.FileNotFoundEventHandler(FileNotFound);
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		/*
		protected override void OnPaint(PaintEventArgs e)
		{	
			Brush bgBrush = new SolidBrush(Color.FromArgb(210, 216, 240));
			Pen fgPen	= new Pen(new SolidBrush(Color.FromArgb(47, 65, 131)));
			GfxDrawer.DrawCurveFromOffsets(3, 115, 3, 3, 5, 3, "DOWNLOADING...", 2, 5, this, e.Graphics, bgBrush, fgPen);
		}
		*/

		protected override void OnClosing(CancelEventArgs e)
		{
			if (!_validExit)
				e.Cancel = true;

			base.OnClosing (e);
		}


		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(DownloadTrackForm));
			this.label1 = new System.Windows.Forms.Label();
			this.lDownloadFrom = new System.Windows.Forms.Label();
			this.lDownloadTo = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.pbStatus = new System.Windows.Forms.ProgressBar();
			this.lStatus = new System.Windows.Forms.Label();
			this.bCancel = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label1.Location = new System.Drawing.Point(16, 16);
			this.label1.Name = "label1";
			this.label1.TabIndex = 0;
			this.label1.Text = "Download From: ";
			// 
			// lDownloadFrom
			// 
			this.lDownloadFrom.Location = new System.Drawing.Point(120, 40);
			this.lDownloadFrom.Name = "lDownloadFrom";
			this.lDownloadFrom.Size = new System.Drawing.Size(464, 23);
			this.lDownloadFrom.TabIndex = 1;
			// 
			// lDownloadTo
			// 
			this.lDownloadTo.Location = new System.Drawing.Point(120, 16);
			this.lDownloadTo.Name = "lDownloadTo";
			this.lDownloadTo.Size = new System.Drawing.Size(464, 23);
			this.lDownloadTo.TabIndex = 3;
			// 
			// label3
			// 
			this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label3.Location = new System.Drawing.Point(16, 40);
			this.label3.Name = "label3";
			this.label3.TabIndex = 2;
			this.label3.Text = "Download To: ";
			// 
			// pbStatus
			// 
			this.pbStatus.Location = new System.Drawing.Point(16, 80);
			this.pbStatus.Name = "pbStatus";
			this.pbStatus.Size = new System.Drawing.Size(568, 23);
			this.pbStatus.TabIndex = 4;
			// 
			// lStatus
			// 
			this.lStatus.Location = new System.Drawing.Point(24, 112);
			this.lStatus.Name = "lStatus";
			this.lStatus.Size = new System.Drawing.Size(560, 23);
			this.lStatus.TabIndex = 5;
			this.lStatus.Text = "Status in Progress";
			this.lStatus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// bCancel
			// 
			this.bCancel.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.bCancel.Location = new System.Drawing.Point(512, 136);
			this.bCancel.Name = "bCancel";
			this.bCancel.TabIndex = 6;
			this.bCancel.Text = "Cancel";
			this.bCancel.Visible = false;
			// 
			// DownloadTrackForm
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(210)), ((System.Byte)(216)), ((System.Byte)(240)));
			this.ClientSize = new System.Drawing.Size(600, 168);
			this.Controls.Add(this.bCancel);
			this.Controls.Add(this.lStatus);
			this.Controls.Add(this.pbStatus);
			this.Controls.Add(this.lDownloadTo);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.lDownloadFrom);
			this.Controls.Add(this.label1);
			this.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MaximizeBox = false;
			this.Name = "DownloadTrackForm";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Download In Progress";
			this.TopMost = true;
			this.ResumeLayout(false);

		}
		#endregion
		
		public void DownloadFiles()
		{
			int i = 0;
			foreach (ComponentDescriptor componentDesc in this.cdArray)
			{
				i++;
				this.FillMaskWithInitialData(componentDesc);
				//DownloadManagerClass.Download(Globals.UpdateDescriptor, Globals.PG_DIR + Globals.UpdatesDir + Globals.LocalUpdateDescriptor, Globals.LocalUpdateDescriptor);
				this.currentlyDownloadedFile = componentDesc.ComponentName;
				this.lStatus.Text = this.currentlyDownloadedFile + ": " + Globals.Downloading + " - (" + i.ToString() + "/" + cdArray.Length.ToString() + ")";
				DownloadManagerClass.Download(componentDesc.FromLocation, componentDesc.ToLocation, componentDesc.ComponentName, 1, 1);				
			}
		}

		private void FillMaskWithInitialData(ComponentDescriptor cDesc)
		{
			this.pbStatus.Maximum = 0;
			this.pbStatus.Value = 0;
			this.lDownloadFrom.Text = cDesc.FromLocation;
			this.lDownloadTo.Text = cDesc.ToLocation;
			this.lStatus.Text = string.Empty;
		}

		private void CancelDownload_EH(object sender, EventArgs e)
		{									
			Globals.CancelDownload = true;
		}		

		private void DownloadPercentageChanged(object sender, PercentageChangedEventArgs e)
		{
			this.pbStatus.Maximum = e.totalLength;			
			this.pbStatus.Value = e.currentPos;

			if (this.pbStatus.Value == this.pbStatus.Maximum &&
				this.currFileIndex == this.totalFilesNumber)
			{					
				this.CloseForm();
			}
		}

		private void FileDownloading(object sender, FileDownloadingEventArgs e)
		{		
			this.lDownloadFrom.Text = e.sourcePath;
			this.lDownloadTo.Text	= e.destinationPath;
			this.totalFilesNumber = e.totalFiles;
			this.currFileIndex = e.currFile;
			if (e.eStatus == eDownloadFileStatus.eStarted)
			{
				this.lStatus.Text = e.fileName + ": " + Globals.Downloading + " (" + e.currFile.ToString() + " / " + e.totalFiles.ToString() + ")";
			}
			else
			{
				this.lStatus.Text = e.fileName + ": " + Globals.DownloadFinished + " (" + e.currFile.ToString() + " / " + e.totalFiles.ToString() + ")";
			}			
		}

        
		void FileNotFound(object sender, FileNotFoundEventArgs e)
		{
            FileNotFoundHandlerAsync(e);
		}

        delegate void FileNotFoundHandlerDelegate(FileNotFoundEventArgs e);
        void FileNotFoundHandlerAsync(FileNotFoundEventArgs e)
        {
            if (InvokeRequired)
            {
                BeginInvoke(new FileNotFoundHandlerDelegate(FileNotFoundHandlerAsync), new object[] { e });
            }
            else
            {
                this.TopMost = false;
                MessageBox.Show("Please Check your internet connection!" + System.Environment.NewLine +
                                "Program can not find file " + e.FileName + " at " + e.Source + System.Environment.NewLine +
                                System.Environment.NewLine +
                                System.Environment.NewLine +
                                "Detailed Description: " + System.Environment.NewLine +
                                e.ExcMessage);

                this.CloseForm();
            }
        }

		private void CloseForm()		
		{
			//if (true == Globals.Notification)
			//{
				//MessageBox.Show(this, "Download finished!", "You can now exit Update Manager.", MessageBoxButtons.OK, MessageBoxIcon.Information);
			//}

			DownloadManagerClass.DownloadPercentageChanged -= new UpdateManager.DownloadManagerClass.ChangedDownloadPercentageEventHandler(DownloadPercentageChanged);			
			DownloadManagerClass.FileDownloading -= new UpdateManager.DownloadManagerClass.FileDownloadingEventHandler(FileDownloading);			
			DownloadManagerClass.FileNotFound -= new UpdateManager.DownloadManagerClass.FileNotFoundEventHandler(FileNotFound);			
			
			_validExit = true;			
			this.Close();
		}
	}
}
