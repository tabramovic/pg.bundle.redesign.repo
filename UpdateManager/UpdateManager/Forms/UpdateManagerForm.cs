#define NETWORK

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;

//Added
using System.IO;
using System.Diagnostics;
using System.Xml;
using System.Threading;

using UpdateManager.Utils;
using UpdateManager.Profile_Manager;
using UpdateManager.BackupRestoreManager;

namespace UpdateManager
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class UpdateManagerForm : System.Windows.Forms.Form
	{
		private System.Windows.Forms.MainMenu appMenu;
		private System.Windows.Forms.MenuItem menuItem1;
		private System.Windows.Forms.MenuItem miExit;
		private System.Windows.Forms.MenuItem menuItem2;
		private System.Windows.Forms.MenuItem miSetPGDir;
		private System.Windows.Forms.MenuItem miScanPGDirectory;
		private System.Windows.Forms.MenuItem miMailPGContent;
		private System.Windows.Forms.MenuItem miFindPG;
		private System.Windows.Forms.MenuItem miManuallySetPGDir;
		private System.Windows.Forms.MenuItem menuItem4;
		private System.Windows.Forms.MenuItem miGetLatest;
		private System.Windows.Forms.StatusBar sBar;
		private System.Windows.Forms.StatusBarPanel sBarPan;
		private System.Windows.Forms.Panel leftPanel;
		private System.Windows.Forms.Splitter splitter1;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.ListView lvInstalled;
		private System.Windows.Forms.ColumnHeader columnHeader1;
		private System.Windows.Forms.ColumnHeader columnHeader2;
		private System.Windows.Forms.Panel rightPanel;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.ListView lvNewSw;
		private System.Windows.Forms.ColumnHeader columnHeader3;
		private System.Windows.Forms.ColumnHeader columnHeader4;
		private System.Windows.Forms.ImageList ilDownloadStatus;
		private System.Windows.Forms.Label labelWelcome;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Button bGetLatestVersion;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Button bDetails;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.Button bFindAuto;
		private System.Windows.Forms.Button bFindMan;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.TextBox tbPGDIR;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.LinkLabel llDetails;
		private System.Windows.Forms.MenuItem menuItem3;
		private System.Windows.Forms.MenuItem mi_BackupDB;
		private System.Windows.Forms.MenuItem mi_RestoreDB;
		private System.Windows.Forms.MenuItem menuItem5;
		private System.Windows.Forms.MenuItem _backupProfiles;
		private System.Windows.Forms.MenuItem _RestoreProfiles;
		private System.Windows.Forms.MenuItem menuItem6;
		private System.Windows.Forms.MenuItem _Backup;
		private System.Windows.Forms.MenuItem _Restore;
		private System.ComponentModel.IContainer components;

		public UpdateManagerForm()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();			
			
			this.Load += new EventHandler(UpdateManagerForm_Load);

			this.miExit.Click += new EventHandler(Exit_EH);
			this.miFindPG.Click += new EventHandler(FindPG_EH);
			this.bFindAuto.Click += new EventHandler(FindPG_EH);
			
			this.miManuallySetPGDir.Click += new EventHandler(SetPG_EH);
			this.bFindMan.Click += new EventHandler(SetPG_EH);
			
			this.miScanPGDirectory.Click += new EventHandler(ScanPGDir_EH);
			this.miMailPGContent.Click += new EventHandler(MailPGContent);
			this.miGetLatest.Click += new EventHandler(GetLatestFiles_EH);	
			this.bGetLatestVersion.Click += new EventHandler(GetLatestFiles_EH);
			this.bDetails.Click += new EventHandler(ViewDetails_EH);
			this.llDetails.Click += new EventHandler(ViewDetails_EH);

			this.mi_BackupDB.Click += new EventHandler(OnBackupDB);
			this.mi_RestoreDB.Click += new EventHandler(OnRestoreDB);

			_backupProfiles.Click += new EventHandler(OnbackupProfiles);
			_RestoreProfiles.Click += new EventHandler(OnRestoreProfiles);

			_Backup.Click += new EventHandler(OnBackupAll);
			_Restore.Click += new EventHandler(OnRestoreAll);
		
			DownloadManagerClass.FileDownloading += new DownloadManagerClass.FileDownloadingEventHandler(FileDownloading);
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(UpdateManagerForm));
			this.appMenu = new System.Windows.Forms.MainMenu();
			this.menuItem1 = new System.Windows.Forms.MenuItem();
			this.miExit = new System.Windows.Forms.MenuItem();
			this.menuItem2 = new System.Windows.Forms.MenuItem();
			this.miSetPGDir = new System.Windows.Forms.MenuItem();
			this.miFindPG = new System.Windows.Forms.MenuItem();
			this.miManuallySetPGDir = new System.Windows.Forms.MenuItem();
			this.miScanPGDirectory = new System.Windows.Forms.MenuItem();
			this.miMailPGContent = new System.Windows.Forms.MenuItem();
			this.menuItem4 = new System.Windows.Forms.MenuItem();
			this.miGetLatest = new System.Windows.Forms.MenuItem();
			this.menuItem3 = new System.Windows.Forms.MenuItem();
			this.mi_BackupDB = new System.Windows.Forms.MenuItem();
			this.mi_RestoreDB = new System.Windows.Forms.MenuItem();
			this.menuItem5 = new System.Windows.Forms.MenuItem();
			this._backupProfiles = new System.Windows.Forms.MenuItem();
			this._RestoreProfiles = new System.Windows.Forms.MenuItem();
			this.sBar = new System.Windows.Forms.StatusBar();
			this.sBarPan = new System.Windows.Forms.StatusBarPanel();
			this.leftPanel = new System.Windows.Forms.Panel();
			this.llDetails = new System.Windows.Forms.LinkLabel();
			this.label12 = new System.Windows.Forms.Label();
			this.label11 = new System.Windows.Forms.Label();
			this.tbPGDIR = new System.Windows.Forms.TextBox();
			this.label10 = new System.Windows.Forms.Label();
			this.bFindMan = new System.Windows.Forms.Button();
			this.bFindAuto = new System.Windows.Forms.Button();
			this.label9 = new System.Windows.Forms.Label();
			this.label8 = new System.Windows.Forms.Label();
			this.label7 = new System.Windows.Forms.Label();
			this.bDetails = new System.Windows.Forms.Button();
			this.label6 = new System.Windows.Forms.Label();
			this.bGetLatestVersion = new System.Windows.Forms.Button();
			this.label4 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.labelWelcome = new System.Windows.Forms.Label();
			this.lvInstalled = new System.Windows.Forms.ListView();
			this.columnHeader1 = new System.Windows.Forms.ColumnHeader();
			this.columnHeader2 = new System.Windows.Forms.ColumnHeader();
			this.label1 = new System.Windows.Forms.Label();
			this.splitter1 = new System.Windows.Forms.Splitter();
			this.rightPanel = new System.Windows.Forms.Panel();
			this.lvNewSw = new System.Windows.Forms.ListView();
			this.columnHeader3 = new System.Windows.Forms.ColumnHeader();
			this.columnHeader4 = new System.Windows.Forms.ColumnHeader();
			this.ilDownloadStatus = new System.Windows.Forms.ImageList(this.components);
			this.label2 = new System.Windows.Forms.Label();
			this.menuItem6 = new System.Windows.Forms.MenuItem();
			this._Backup = new System.Windows.Forms.MenuItem();
			this._Restore = new System.Windows.Forms.MenuItem();
			((System.ComponentModel.ISupportInitialize)(this.sBarPan)).BeginInit();
			this.leftPanel.SuspendLayout();
			this.rightPanel.SuspendLayout();
			this.SuspendLayout();
			// 
			// appMenu
			// 
			this.appMenu.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					this.menuItem1,
																					this.menuItem2,
																					this.menuItem4,
																					this.menuItem3,
																					this.menuItem5,
																					this.menuItem6});
			// 
			// menuItem1
			// 
			this.menuItem1.Index = 0;
			this.menuItem1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					  this.miExit});
			this.menuItem1.Text = "File";
			// 
			// miExit
			// 
			this.miExit.Index = 0;
			this.miExit.Text = "Exit";
			// 
			// menuItem2
			// 
			this.menuItem2.Index = 1;
			this.menuItem2.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					  this.miSetPGDir,
																					  this.miScanPGDirectory,
																					  this.miMailPGContent});
			this.menuItem2.Text = "PG Pro Info";
			// 
			// miSetPGDir
			// 
			this.miSetPGDir.Index = 0;
			this.miSetPGDir.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					   this.miFindPG,
																					   this.miManuallySetPGDir});
			this.miSetPGDir.Text = "Set PG Directory";
			// 
			// miFindPG
			// 
			this.miFindPG.Index = 0;
			this.miFindPG.Text = "Find PG";
			// 
			// miManuallySetPGDir
			// 
			this.miManuallySetPGDir.Index = 1;
			this.miManuallySetPGDir.Text = "Manually Set PG Directory";
			// 
			// miScanPGDirectory
			// 
			this.miScanPGDirectory.Index = 1;
			this.miScanPGDirectory.Text = "Scan PG Directory";
			this.miScanPGDirectory.Visible = false;
			// 
			// miMailPGContent
			// 
			this.miMailPGContent.Index = 2;
			this.miMailPGContent.Text = "Mail PG Directory Content to PG Support";
			// 
			// menuItem4
			// 
			this.menuItem4.Index = 2;
			this.menuItem4.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					  this.miGetLatest});
			this.menuItem4.Text = "Download Area";
			// 
			// miGetLatest
			// 
			this.miGetLatest.Index = 0;
			this.miGetLatest.Text = "Get Latest Version";
			// 
			// menuItem3
			// 
			this.menuItem3.Index = 3;
			this.menuItem3.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					  this.mi_BackupDB,
																					  this.mi_RestoreDB});
			this.menuItem3.Text = "Database Manager";
			this.menuItem3.Visible = false;
			// 
			// mi_BackupDB
			// 
			this.mi_BackupDB.Index = 0;
			this.mi_BackupDB.Text = "Backup Database";
			// 
			// mi_RestoreDB
			// 
			this.mi_RestoreDB.Index = 1;
			this.mi_RestoreDB.Text = "Restore Database";
			// 
			// menuItem5
			// 
			this.menuItem5.Index = 4;
			this.menuItem5.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					  this._backupProfiles,
																					  this._RestoreProfiles});
			this.menuItem5.Text = "Profile Manager";
			this.menuItem5.Visible = false;
			// 
			// _backupProfiles
			// 
			this._backupProfiles.Index = 0;
			this._backupProfiles.Text = "Backup Profiles";
			// 
			// _RestoreProfiles
			// 
			this._RestoreProfiles.Index = 1;
			this._RestoreProfiles.Text = "Restore Profiles";
			// 
			// sBar
			// 
			this.sBar.Location = new System.Drawing.Point(0, 531);
			this.sBar.Name = "sBar";
			this.sBar.Panels.AddRange(new System.Windows.Forms.StatusBarPanel[] {
																					this.sBarPan});
			this.sBar.Size = new System.Drawing.Size(792, 22);
			this.sBar.TabIndex = 0;
			// 
			// sBarPan
			// 
			this.sBarPan.Text = "statusBarPanel1";
			this.sBarPan.Width = 500;
			// 
			// leftPanel
			// 
			this.leftPanel.Controls.Add(this.llDetails);
			this.leftPanel.Controls.Add(this.label12);
			this.leftPanel.Controls.Add(this.label11);
			this.leftPanel.Controls.Add(this.tbPGDIR);
			this.leftPanel.Controls.Add(this.label10);
			this.leftPanel.Controls.Add(this.bFindMan);
			this.leftPanel.Controls.Add(this.bFindAuto);
			this.leftPanel.Controls.Add(this.label9);
			this.leftPanel.Controls.Add(this.label8);
			this.leftPanel.Controls.Add(this.label7);
			this.leftPanel.Controls.Add(this.bDetails);
			this.leftPanel.Controls.Add(this.label6);
			this.leftPanel.Controls.Add(this.bGetLatestVersion);
			this.leftPanel.Controls.Add(this.label4);
			this.leftPanel.Controls.Add(this.label3);
			this.leftPanel.Controls.Add(this.labelWelcome);
			this.leftPanel.Controls.Add(this.lvInstalled);
			this.leftPanel.Controls.Add(this.label1);
			this.leftPanel.Dock = System.Windows.Forms.DockStyle.Left;
			this.leftPanel.Location = new System.Drawing.Point(0, 0);
			this.leftPanel.Name = "leftPanel";
			this.leftPanel.Size = new System.Drawing.Size(456, 531);
			this.leftPanel.TabIndex = 1;
			// 
			// llDetails
			// 
			this.llDetails.Location = new System.Drawing.Point(62, 453);
			this.llDetails.Name = "llDetails";
			this.llDetails.Size = new System.Drawing.Size(40, 23);
			this.llDetails.TabIndex = 20;
			this.llDetails.TabStop = true;
			this.llDetails.Text = "Details";
			// 
			// label12
			// 
			this.label12.Location = new System.Drawing.Point(16, 328);
			this.label12.Name = "label12";
			this.label12.TabIndex = 19;
			this.label12.Text = "Get the Update:";
			// 
			// label11
			// 
			this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label11.Location = new System.Drawing.Point(8, 304);
			this.label11.Name = "label11";
			this.label11.TabIndex = 18;
			this.label11.Text = "Step #2:";
			// 
			// tbPGDIR
			// 
			this.tbPGDIR.Location = new System.Drawing.Point(16, 264);
			this.tbPGDIR.Name = "tbPGDIR";
			this.tbPGDIR.ReadOnly = true;
			this.tbPGDIR.Size = new System.Drawing.Size(424, 20);
			this.tbPGDIR.TabIndex = 17;
			this.tbPGDIR.Text = "";
			// 
			// label10
			// 
			this.label10.Location = new System.Drawing.Point(16, 240);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(120, 23);
			this.label10.TabIndex = 16;
			this.label10.Text = "The directory is set to:";
			// 
			// bFindMan
			// 
			this.bFindMan.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.bFindMan.Location = new System.Drawing.Point(176, 192);
			this.bFindMan.Name = "bFindMan";
			this.bFindMan.Size = new System.Drawing.Size(128, 40);
			this.bFindMan.TabIndex = 15;
			this.bFindMan.Text = "Manually Set PG Directory";
			// 
			// bFindAuto
			// 
			this.bFindAuto.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.bFindAuto.Location = new System.Drawing.Point(32, 192);
			this.bFindAuto.Name = "bFindAuto";
			this.bFindAuto.Size = new System.Drawing.Size(128, 40);
			this.bFindAuto.TabIndex = 14;
			this.bFindAuto.Text = "Auto-Find PG Directory (preferred)";
			// 
			// label9
			// 
			this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label9.Location = new System.Drawing.Point(8, 144);
			this.label9.Name = "label9";
			this.label9.TabIndex = 13;
			this.label9.Text = "Step #1:";
			// 
			// label8
			// 
			this.label8.Location = new System.Drawing.Point(16, 168);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(360, 24);
			this.label8.TabIndex = 12;
			this.label8.Text = "Set the ProfitGrabber directory where you installed your ProfitGrabber:";
			// 
			// label7
			// 
			this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label7.Location = new System.Drawing.Point(8, 112);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(216, 23);
			this.label7.TabIndex = 11;
			this.label7.Text = "The Update will be done in two steps:";
			this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// bDetails
			// 
			this.bDetails.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.bDetails.Location = new System.Drawing.Point(32, 480);
			this.bDetails.Name = "bDetails";
			this.bDetails.Size = new System.Drawing.Size(128, 40);
			this.bDetails.TabIndex = 10;
			this.bDetails.Text = "Details";
			this.bDetails.Visible = false;
			// 
			// label6
			// 
			this.label6.Location = new System.Drawing.Point(8, 440);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(384, 32);
			this.label6.TabIndex = 9;
			this.label6.Text = "To view currently installed ProfitGrabber Pro components click here:";
			// 
			// bGetLatestVersion
			// 
			this.bGetLatestVersion.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.bGetLatestVersion.Location = new System.Drawing.Point(32, 352);
			this.bGetLatestVersion.Name = "bGetLatestVersion";
			this.bGetLatestVersion.Size = new System.Drawing.Size(128, 40);
			this.bGetLatestVersion.TabIndex = 8;
			this.bGetLatestVersion.Text = "Get Latest Version";
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(8, 80);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(216, 23);
			this.label4.TabIndex = 6;
			this.label4.Text = "Make sure you have Internet connection!";
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(8, 48);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(432, 24);
			this.label3.TabIndex = 5;
			this.label3.Text = "The ProfitGrabber Pro UpdateManager will use Internet to search for updates to yo" +
				"ur installed ProfitGrabber Pro application and its components.";
			// 
			// labelWelcome
			// 
			this.labelWelcome.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.labelWelcome.Location = new System.Drawing.Point(8, 16);
			this.labelWelcome.Name = "labelWelcome";
			this.labelWelcome.Size = new System.Drawing.Size(144, 23);
			this.labelWelcome.TabIndex = 4;
			this.labelWelcome.Text = "Welcome to Live Update!";
			// 
			// lvInstalled
			// 
			this.lvInstalled.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.lvInstalled.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
																						  this.columnHeader1,
																						  this.columnHeader2});
			this.lvInstalled.FullRowSelect = true;
			this.lvInstalled.GridLines = true;
			this.lvInstalled.HoverSelection = true;
			this.lvInstalled.Location = new System.Drawing.Point(360, 488);
			this.lvInstalled.MultiSelect = false;
			this.lvInstalled.Name = "lvInstalled";
			this.lvInstalled.Size = new System.Drawing.Size(88, 32);
			this.lvInstalled.TabIndex = 2;
			this.lvInstalled.View = System.Windows.Forms.View.Details;
			this.lvInstalled.Visible = false;
			// 
			// columnHeader1
			// 
			this.columnHeader1.Text = "File name";
			this.columnHeader1.Width = 238;
			// 
			// columnHeader2
			// 
			this.columnHeader2.Text = "File Version";
			this.columnHeader2.Width = 78;
			// 
			// label1
			// 
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label1.Location = new System.Drawing.Point(392, 464);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(48, 23);
			this.label1.TabIndex = 1;
			this.label1.Text = "Installed Software";
			this.label1.Visible = false;
			// 
			// splitter1
			// 
			this.splitter1.BackColor = System.Drawing.Color.Gray;
			this.splitter1.Location = new System.Drawing.Point(456, 0);
			this.splitter1.Name = "splitter1";
			this.splitter1.Size = new System.Drawing.Size(3, 531);
			this.splitter1.TabIndex = 2;
			this.splitter1.TabStop = false;
			// 
			// rightPanel
			// 
			this.rightPanel.Controls.Add(this.lvNewSw);
			this.rightPanel.Controls.Add(this.label2);
			this.rightPanel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.rightPanel.Location = new System.Drawing.Point(459, 0);
			this.rightPanel.Name = "rightPanel";
			this.rightPanel.Size = new System.Drawing.Size(333, 531);
			this.rightPanel.TabIndex = 3;
			// 
			// lvNewSw
			// 
			this.lvNewSw.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.lvNewSw.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
																					  this.columnHeader3,
																					  this.columnHeader4});
			this.lvNewSw.FullRowSelect = true;
			this.lvNewSw.GridLines = true;
			this.lvNewSw.HoverSelection = true;
			this.lvNewSw.Location = new System.Drawing.Point(8, 24);
			this.lvNewSw.MultiSelect = false;
			this.lvNewSw.Name = "lvNewSw";
			this.lvNewSw.Size = new System.Drawing.Size(320, 496);
			this.lvNewSw.SmallImageList = this.ilDownloadStatus;
			this.lvNewSw.TabIndex = 3;
			this.lvNewSw.View = System.Windows.Forms.View.Details;
			// 
			// columnHeader3
			// 
			this.columnHeader3.Text = "File name";
			this.columnHeader3.Width = 238;
			// 
			// columnHeader4
			// 
			this.columnHeader4.Text = "File Version";
			this.columnHeader4.Width = 78;
			// 
			// ilDownloadStatus
			// 
			this.ilDownloadStatus.ImageSize = new System.Drawing.Size(16, 16);
			this.ilDownloadStatus.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ilDownloadStatus.ImageStream")));
			this.ilDownloadStatus.TransparentColor = System.Drawing.Color.Transparent;
			// 
			// label2
			// 
			this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label2.Location = new System.Drawing.Point(8, 8);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(136, 23);
			this.label2.TabIndex = 0;
			this.label2.Text = "Files To Be Downloaded";
			// 
			// menuItem6
			// 
			this.menuItem6.Index = 5;
			this.menuItem6.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					  this._Backup,
																					  this._Restore});
			this.menuItem6.Text = "Database and Profile Manager";
			// 
			// _Backup
			// 
			this._Backup.Index = 0;
			this._Backup.Text = "Backup Database and Profiles";
			// 
			// _Restore
			// 
			this._Restore.Index = 1;
			this._Restore.Text = "Restore Database and Profiles";
			// 
			// UpdateManagerForm
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(210)), ((System.Byte)(216)), ((System.Byte)(240)));
			this.ClientSize = new System.Drawing.Size(792, 553);
			this.Controls.Add(this.rightPanel);
			this.Controls.Add(this.splitter1);
			this.Controls.Add(this.leftPanel);
			this.Controls.Add(this.sBar);
			this.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Menu = this.appMenu;
			this.Name = "UpdateManagerForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
#if NETWORK
			this.Text = "ProfitGrabber Pro Network Version Update Manager Ver. " + Application.ProductVersion.ToString();
#else
            this.Text = "ProfitGrabber Pro Update Manager Ver. " + Application.ProductVersion.ToString();
#endif
			((System.ComponentModel.ISupportInitialize)(this.sBarPan)).EndInit();
			this.leftPanel.ResumeLayout(false);
			this.rightPanel.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.EnableVisualStyles();
			Application.DoEvents();
			Application.Run(new UpdateManagerForm());
		}

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad (e);
			
			//TA, 26.Nov.2005.
			//Now update process is launched from ProfitGrabber Pro application
			/*
			string proc = Globals.ProcName;
			Process[] processes = Process.GetProcessesByName(proc);
			if (processes.Length > 0)
			{
				MessageBox.Show(this, "Please close ProfitGrabber Pro before starting an update!", "Close ProfitGrabber Pro", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
				Application.Exit();
			}
			*/
		}


		private void Exit_EH(object sender, EventArgs e)
		{
			if (DialogResult.Yes == MessageBox.Show(this, "Are you sure that you want to exit ProfitGrabber Pro Update Manager?", "Exit Application?", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
			{
				Application.Exit();
			}
		}
				
		private void FindPG_EH(object sender, EventArgs e)
		{			
			AutomaticFindForm aff = new AutomaticFindForm();
			aff.ShowDialog();
			if (aff.Found)
			{
				this.sBar.Text = aff.PG_DIR;
				this.tbPGDIR.Text = aff.PG_DIR;
				Globals.PG_DIR = aff.PG_DIR;
				this.bGetLatestVersion.Enabled = true;
			}
		}

		private void SetPG_EH(object sender, EventArgs e)
		{
			string myPath;

			BrowseForFolderClass myFolderBrowser = new BrowseForFolderClass();
			myPath = myFolderBrowser.BrowseForFolder("Please, select a folder");
			if (myPath.Length > 0) 
			{
				Application.CommonAppDataRegistry.SetValue("PG_DIR", myPath);
				Globals.PG_DIR = (string)myPath;
				this.sBar.Text = Globals.PG_DIR;
				this.tbPGDIR.Text = Globals.PG_DIR;
				this.bGetLatestVersion.Enabled = true;
			}
		}

		private void ScanPGDir_EH(object sender, EventArgs e)
		{
			if (string.Empty == Globals.PG_DIR)
			{
				MessageBox.Show(this, "Please manually set or find ProfitGrabber Pro directory", "Find PG Dir", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
			Globals.htLocalFiles.Clear();
			try
			{
				string[] files = Directory.GetFiles(Globals.PG_DIR); 			
				foreach (string file in files)
				{
					FileVersionInfo fvi = FileVersionInfo.GetVersionInfo(file);
					if (false == Globals.htLocalFiles.Contains(file))
						Globals.htLocalFiles.Add(file, new BinaryDescriptor(file, fvi.FileVersion, Globals.InstallDir));
				}
				this.UpdateInstalledSoftware();	
			}
			catch (Exception)
			{
				MessageBox.Show(this, 
					"The location of your ProfitGrabber has changed." + System.Environment.NewLine + 
					"Please reset the ProfitGrabber directory as instructed by step # 1 in the Update Manager" +  System.Environment.NewLine + 
					"\t{use Auto-Find or Manually Set buttons}", "Files Not Found!", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
		}

		private void UpdateInstalledSoftware()
		{
			this.lvInstalled.Items.Clear();
			IDictionaryEnumerator ide = Globals.htLocalFiles.GetEnumerator();
			while (ide.MoveNext())
			{
				string fileName = (string)ide.Key;
				fileName = fileName.Replace(Globals.PG_DIR, string.Empty);
				ListViewItem lvi = new ListViewItem(fileName);
				lvi.SubItems.Add(((BinaryDescriptor)ide.Value).Version);
				this.lvInstalled.Items.Add(lvi);
			}
		}

		private void MailPGContent(object sender, EventArgs e)
		{
			string documentName = string.Empty;
			if (0 == Globals.htLocalFiles.Count)
			{
				MessageBox.Show(this, "Please set and scan ProfitGrabbper Pro Directory", "Scan Dir", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
			else
			{
				string MailLogsDir = Globals.PG_DIR + @"\Mail_Logs";
				if (false == Directory.Exists(MailLogsDir))
				{
					Directory.CreateDirectory(MailLogsDir);
				}
				documentName = MailLogsDir + @"\MailLog_" +  
											DateTime.Now.Year.ToString() + "_" + 
											DateTime.Now.Month.ToString() + "_" + 
											DateTime.Now.Day.ToString() + "_" +   
											DateTime.Now.Hour.ToString() + "_" +   
											DateTime.Now.Minute.ToString() + "_" +   
											DateTime.Now.Second.ToString() + "_" +   
											".txt";
				StreamWriter sw = new StreamWriter(documentName);
				IDictionaryEnumerator ide = Globals.htLocalFiles.GetEnumerator();
                sw.WriteLine("<?xml version=\"1.0\" encoding=\"UTF-8\" ?>");
                sw.WriteLine("<UpdateFiles>");
				while (ide.MoveNext())
				{
                    string fn = (string)ide.Key.ToString().Substring(Globals.PG_DIR.Length, ((string)ide.Key.ToString()).Length - Globals.PG_DIR.Length);
                    sw.WriteLine("<FileInfo FileName=\"" + fn + "\" Version=\"" + ((UpdateManager.BinaryDescriptor)ide.Value).Version + "\" DestinationPath=\"" + ((UpdateManager.BinaryDescriptor)ide.Value).DestinationPath + "\"/>");
				}
                sw.WriteLine("</UpdateFiles>");
				sw.Flush();
				sw.Close();												
			}

			string[] subjects = new string[1];
			string[] documents = new string[1];

			subjects[0] = "ProfitGrabber Pro Direcoty Content";
			documents[0] = documentName; 
			SimpleMapi.SendDocuments(subjects, documents);
		}

		private bool TestForPGRunning()
		{
			string proc = Globals.ProcName;
			Process[] processes = Process.GetProcessesByName(proc);
			if (processes.Length > 0)
			{
				return true;
			}
			return false;
		}

		private void GetLatestFiles_EH(object sender, EventArgs e)
		{			
			while (true == this.TestForPGRunning())
			{
				MessageBox.Show(this, "Update Process can not start while ProfitGrabber is running.\nPlease save your work and close ProfitGrabber Pro before click on OK!", "Please close ProfitGrabber Pro", MessageBoxButtons.OK, MessageBoxIcon.Information);
				System.Threading.Thread.Sleep(3500);
			}

			this.ScanPGDir_EH(sender, e);
			if (string.Empty == Globals.PG_DIR)
			{
				MessageBox.Show(this, "Please manually set or find ProfitGrabber Pro directory", "Find PG Dir", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
			AppHelper.GetUpdateDescriptor();			
			
			if (true == Globals.SuccessFullDownload)
			{
				if (true == AppHelper.ParseUpdateDescriptor())
				{
					Hashtable htDownloadFiles = AppHelper.CompareFiles();
					if (htDownloadFiles.Count > 0)
					{
						this.UpdateNewSwList(htDownloadFiles);
						ComponentDescriptor[] cdArray = AppHelper.PrepareBinariesForDownload(htDownloadFiles);
						if (null != cdArray && cdArray.Length > 0)
						{
							Globals.Notification = true;
							DownloadTrackForm dtf = new DownloadTrackForm();
							AppHelper.DownloadFiles(cdArray);
							dtf.ShowDialog();
							Globals.Notification = false;	

							MessageBox.Show("Update finished", "Profit Grabber Pro Update Manager", MessageBoxButtons.OK, MessageBoxIcon.Information);
						}
					}
					else
					{
						MessageBox.Show(this, "All of the ProfitGrabber Pro products and components installed on your computer are up-to-date.\nRemember to check for new updates frequently.", "Components are up-to-date.", MessageBoxButtons.OK, MessageBoxIcon.Information);
					}
							
				}
			}
		}

		private void UpdateNewSwList(Hashtable htDownloadFiles)
		{
			this.lvNewSw.Items.Clear();
			IDictionaryEnumerator ide = htDownloadFiles.GetEnumerator();
			while (ide.MoveNext())
			{
				string updateFileName = (string)ide.Key;
				BinaryDescriptor newFileBD = (BinaryDescriptor)ide.Value;

				ListViewItem lvi = new ListViewItem(updateFileName, 0);
				lvi.SubItems.Add(newFileBD.Version);
				lvi.Tag = updateFileName;

				this.lvNewSw.Items.Add(lvi);
			}
		}
						
		private void UpdateManagerForm_Load(object sender, EventArgs e)
		{						
			Globals.PG_DIR = (string)Application.CommonAppDataRegistry.GetValue("PG_DIR", string.Empty);
			if (Globals.PG_DIR != string.Empty)
			{
				this.sBar.Text = Globals.PG_DIR;
				this.tbPGDIR.Text = Globals.PG_DIR;
				this.bGetLatestVersion.Enabled = true;
				this.ScanPGDir_EH(this, null);
				Globals.DirectoryScanned = true;
			}
			else
			{
				this.sBar.Text = "Search for ProfitGrabber Pro Directory!!!";
				this.bGetLatestVersion.Enabled = false;
			}


		}

		private void FileDownloading(object sender, FileDownloadingEventArgs e)
		{
			if (e.eStatus == eDownloadFileStatus.eFinished)
			{
				IEnumerator ie = this.lvNewSw.Items.GetEnumerator();
				while (ie.MoveNext())
				{
					ListViewItem lvi = (ListViewItem)ie.Current;
					if ((string)lvi.Tag == e.fileName)
						lvi.ImageIndex = 1;
				}
			}
		}

		private void ViewDetails_EH(object sender, EventArgs e)
		{
			if (string.Empty == Globals.PG_DIR)
			{
				MessageBox.Show(this, "Please manually set or find ProfitGrabber Pro directory", "Find PG Dir", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}			
			//if (0 == Globals.htLocalFiles.Keys.Count)
			//{
				this.ScanPGDir_EH(sender, e);
			//}
			ViewInstalledSWForm visf = new ViewInstalledSWForm();
			visf.ShowDialog();
		}

		private bool TestForPGProcess()
		{
			ProcessTester pt = new ProcessTester(Globals.ProcName);
			bool processRunning = true;
			string error = string.Empty;

			while (true == pt.TestIfActive(out processRunning, out error) && processRunning)
			{
				MessageBox.Show(this, "Backup Process can not start while ProfitGrabber is running.\nPlease save your work and close ProfitGrabber Pro before click on OK!", "Please close ProfitGrabber Pro", MessageBoxButtons.OK, MessageBoxIcon.Information);
				System.Threading.Thread.Sleep(3500);
			}

			if (false == pt.TestIfActive(out processRunning, out error))
			{
				MessageBox.Show("Update Manager", "Error: Update Manager can not enumerate processes. For Vista users, check for Administrator rights" + Environment.NewLine + error, MessageBoxButtons.OK, MessageBoxIcon.Information );
				System.Environment.Exit(0);
			}

			if (string.Empty == Globals.PG_DIR)
			{
				MessageBox.Show(this, "Please manually set or find ProfitGrabber Pro directory", "Find PG Dir", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return false;
			}
			return true;
		}

		private void OnBackupDB(object sender, EventArgs e)
		{
			if (false == TestForPGProcess())
				return;
			
			BackupDB backupForm = new BackupDB();
			backupForm.ShowDialog();
		}

		private void OnRestoreDB(object sender, EventArgs e)
		{
			if (false == TestForPGProcess())
				return;

			RestoreDB restoreForm = new RestoreDB();
			restoreForm.ShowDialog();
		}

		private void OnbackupProfiles(object sender, EventArgs e)
		{
			if (false == TestForPGProcess())
				return;

			BackupProfile bp = new BackupProfile();
			bp.ShowDialog();
		}

		private void OnRestoreProfiles(object sender, EventArgs e)
		{
			if (false == TestForPGProcess())
				return;

			RestoreProfile rp = new RestoreProfile();
			rp.ShowDialog();
		}

		private void OnBackupAll(object sender, EventArgs e)
		{
			if (false == TestForPGProcess())
				return;

			BackupManager bm = new BackupManager();
			bm.ShowDialog();
		}

		private void OnRestoreAll(object sender, EventArgs e)
		{
			if (false == TestForPGProcess())
				return;

			RestoreManager rm = new RestoreManager();
			rm.ShowDialog();
		}
	}
}
