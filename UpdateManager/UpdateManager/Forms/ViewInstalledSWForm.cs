using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace UpdateManager
{
	/// <summary>
	/// Summary description for ViewInstalledSWForm.
	/// </summary>
	public class ViewInstalledSWForm : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.ListView lvInstalled;
		private System.Windows.Forms.ColumnHeader columnHeader1;
		private System.Windows.Forms.ColumnHeader columnHeader2;
		private System.Windows.Forms.Button bClose;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public ViewInstalledSWForm()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			this.bClose.Click += new EventHandler(Close_EH);
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.lvInstalled = new System.Windows.Forms.ListView();
			this.columnHeader1 = new System.Windows.Forms.ColumnHeader();
			this.columnHeader2 = new System.Windows.Forms.ColumnHeader();
			this.bClose = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(8, 16);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(120, 23);
			this.label1.TabIndex = 0;
			this.label1.Text = "Installed Components";
			// 
			// lvInstalled
			// 
			this.lvInstalled.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.lvInstalled.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
																						  this.columnHeader1,
																						  this.columnHeader2});
			this.lvInstalled.FullRowSelect = true;
			this.lvInstalled.GridLines = true;
			this.lvInstalled.HoverSelection = true;
			this.lvInstalled.Location = new System.Drawing.Point(8, 40);
			this.lvInstalled.MultiSelect = false;
			this.lvInstalled.Name = "lvInstalled";
			this.lvInstalled.Size = new System.Drawing.Size(320, 440);
			this.lvInstalled.TabIndex = 3;
			this.lvInstalled.View = System.Windows.Forms.View.Details;
			// 
			// columnHeader1
			// 
			this.columnHeader1.Text = "File name";
			this.columnHeader1.Width = 238;
			// 
			// columnHeader2
			// 
			this.columnHeader2.Text = "File Version";
			this.columnHeader2.Width = 78;
			// 
			// bClose
			// 
			this.bClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.bClose.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.bClose.Location = new System.Drawing.Point(336, 40);
			this.bClose.Name = "bClose";
			this.bClose.TabIndex = 4;
			this.bClose.Text = "Close";
			// 
			// ViewInstalledSWForm
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(210)), ((System.Byte)(216)), ((System.Byte)(240)));
			this.ClientSize = new System.Drawing.Size(424, 494);
			this.Controls.Add(this.bClose);
			this.Controls.Add(this.lvInstalled);
			this.Controls.Add(this.label1);
			this.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
			this.Name = "ViewInstalledSWForm";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Installed ProfitGrabber Pro Components";
			this.ResumeLayout(false);

		}
		#endregion

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad (e);
			IDictionaryEnumerator ide = Globals.htLocalFiles.GetEnumerator();
			while (ide.MoveNext())
			{
				string fileName = (string)ide.Key;
				fileName = fileName.Replace(Globals.PG_DIR, string.Empty);
				ListViewItem lvi = new ListViewItem(fileName);
				lvi.SubItems.Add(((BinaryDescriptor)ide.Value).Version);
				this.lvInstalled.Items.Add(lvi);
			}
		}


		private void Close_EH(object sender, EventArgs e)
		{
			this.Close();
		}
	}
}
