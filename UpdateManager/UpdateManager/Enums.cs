using System;

namespace UpdateManager
{
	/// <summary>
	/// Summary description for Enums.
	/// </summary>
	public enum eDownloadFileStatus
	{
		eStarted = 0, 
		eFinished,
	}
}
