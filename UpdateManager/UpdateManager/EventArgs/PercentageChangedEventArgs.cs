using System;

namespace UpdateManager
{
	/// <summary>
	/// Summary description for PercentageChangedEventArgs.
	/// </summary>
	public class PercentageChangedEventArgs : EventArgs		
	{
		public int totalLength	= 0;
		public int currentPos	= 0;
		
		public PercentageChangedEventArgs()
		{

		}

		public PercentageChangedEventArgs(int totalLength, int currentPos)
		{
			this.totalLength	= totalLength;
			this.currentPos		= currentPos;
		}
	}
}
