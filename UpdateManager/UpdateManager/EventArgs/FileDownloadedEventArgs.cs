using System;

namespace UpdateManager
{
	/// <summary>
	/// Summary description for FileDownloadingEventArgs.
	/// </summary>
	public class FileDownloadingEventArgs : EventArgs
	{
		public eDownloadFileStatus eStatus = eDownloadFileStatus.eStarted;
		public bool successfulDownload = false;
		public string destinationPath = string.Empty;
		public string sourcePath = string.Empty;
		public string fileName = string.Empty;
		public int currFile = 0;
		public int totalFiles = 0;

		public FileDownloadingEventArgs()
		{
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="eStatus">Download file status</param>
		/// <param name="successfulDownload">Result</param>
		/// <param name="destinationPath">Destination Path</param>
		/// <param name="sourcePath">Source Path</param>
		/// <param name="fileName">File Name</param>
		/// <param name="currFile">Current file index [Curr / Total]</param>
		/// <param name="totalFiles">Total number of files</param>
		public FileDownloadingEventArgs(eDownloadFileStatus eStatus, bool successfulDownload, string destinationPath, string sourcePath, string fileName, int currFile, int totalFiles)
		{
			this.eStatus			= eStatus;
			this.successfulDownload = successfulDownload;
			this.destinationPath	= destinationPath;
			this.sourcePath			= sourcePath;
			this.fileName			= fileName;
			this.currFile			= currFile;
			this.totalFiles			= totalFiles;
		}
	}
}
