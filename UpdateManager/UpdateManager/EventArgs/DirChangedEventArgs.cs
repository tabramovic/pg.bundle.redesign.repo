using System;

namespace UpdateManager
{
	/// <summary>
	/// Summary description for DirChangedEventArgs.
	/// </summary>
	public class DirChangedEventArgs : EventArgs
	{
		public string Dir;

		public DirChangedEventArgs(string dir)
		{
			this.Dir = dir;
		}
	}
}
