using System;

namespace UpdateManager
{
	/// <summary>
	/// Summary description for FileNotFoundEventArgs.
	/// </summary>
	public class FileNotFoundEventArgs : EventArgs
	{
		private string fileName			= string.Empty;
		private string source			= string.Empty;
		private string excMessage		= string.Empty;


		public FileNotFoundEventArgs(string fileName, string source, string excMessage)
		{
			this.fileName		= fileName;
			this.source			= source;
			this.excMessage		= excMessage;
		}

		public string FileName
		{
			get {return this.fileName;}
		}

		public string Source
		{
			get {return this.source;}
		}

		public string ExcMessage
		{
			get {return this.excMessage;}
		}
	}
}
