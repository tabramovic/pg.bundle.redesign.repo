using System;

namespace UpdateManager
{
	/// <summary>
	/// Summary description for DBManagerEventArgs.
	/// </summary>
	public class DBManagerEventArgs : EventArgs
	{
		string _msg;
		
		public DBManagerEventArgs(string msg)
		{
			_msg = msg;
		}

		public string Message
		{
			get { return _msg; }
		}
	}
}
