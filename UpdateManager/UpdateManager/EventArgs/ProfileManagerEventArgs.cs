using System;

namespace UpdateManager
{
	/// <summary>
	/// Summary description for ProfileManagerEventArgs.
	/// </summary>
	public class ProfileManagerEventArgs : EventArgs
	{
		string _msg;

		public ProfileManagerEventArgs(string msg)
		{
			_msg = msg;
		}

		public string Message
		{
			get { return _msg; }
		}
	}
}
