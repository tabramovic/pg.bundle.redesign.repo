using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace UpdateManager.Profile_Manager
{
	/// <summary>
	/// Summary description for RestoreProfile.
	/// </summary>
	public class RestoreProfile : System.Windows.Forms.Form
	{
		const string ProfilePath = @"\Profiles\";
		string _path = string.Empty;
		bool validClose = false;
		private System.Windows.Forms.TextBox _backupFolder;
		private System.Windows.Forms.Button _Close;
		private System.Windows.Forms.Label _Progress;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Button _startBackup;
		private System.Windows.Forms.Button _Browse;
		private System.Windows.Forms.Label label1;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public RestoreProfile()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			_Browse.Click += new EventHandler(OnBrowse);
			_startBackup.Click += new EventHandler(_startBackup_Click);
			_Close.Click += new EventHandler(_Close_Click);
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(RestoreProfile));
			this._backupFolder = new System.Windows.Forms.TextBox();
			this._Close = new System.Windows.Forms.Button();
			this._Progress = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this._startBackup = new System.Windows.Forms.Button();
			this._Browse = new System.Windows.Forms.Button();
			this.label1 = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// _backupFolder
			// 
			this._backupFolder.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this._backupFolder.Location = new System.Drawing.Point(39, 38);
			this._backupFolder.Name = "_backupFolder";
			this._backupFolder.ReadOnly = true;
			this._backupFolder.Size = new System.Drawing.Size(464, 20);
			this._backupFolder.TabIndex = 19;
			this._backupFolder.TabStop = false;
			this._backupFolder.Text = "";
			// 
			// _Close
			// 
			this._Close.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this._Close.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this._Close.Location = new System.Drawing.Point(479, 175);
			this._Close.Name = "_Close";
			this._Close.Size = new System.Drawing.Size(72, 23);
			this._Close.TabIndex = 24;
			this._Close.Text = "Close";
			// 
			// _Progress
			// 
			this._Progress.Location = new System.Drawing.Point(23, 143);
			this._Progress.Name = "_Progress";
			this._Progress.Size = new System.Drawing.Size(528, 33);
			this._Progress.TabIndex = 23;
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(15, 79);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(152, 23);
			this.label2.TabIndex = 22;
			this.label2.Text = "2. Start Backup Procedure";
			// 
			// _startBackup
			// 
			this._startBackup.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this._startBackup.Location = new System.Drawing.Point(39, 103);
			this._startBackup.Name = "_startBackup";
			this._startBackup.Size = new System.Drawing.Size(64, 23);
			this._startBackup.TabIndex = 21;
			this._startBackup.Text = "Start";
			// 
			// _Browse
			// 
			this._Browse.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this._Browse.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(210)), ((System.Byte)(216)), ((System.Byte)(240)));
			this._Browse.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this._Browse.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this._Browse.Location = new System.Drawing.Point(511, 37);
			this._Browse.Name = "_Browse";
			this._Browse.Size = new System.Drawing.Size(40, 23);
			this._Browse.TabIndex = 20;
			this._Browse.Text = "...";
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(15, 15);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(473, 23);
			this.label1.TabIndex = 18;
			this.label1.Text = "1. Set Profile Backup Folder  < i.e.: E:\\TEMP\\2006_8_30___17_34_51\\Profiles\\>";
			// 
			// RestoreProfile
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(210)), ((System.Byte)(216)), ((System.Byte)(240)));
			this.ClientSize = new System.Drawing.Size(566, 212);
			this.Controls.Add(this._Close);
			this.Controls.Add(this._backupFolder);
			this.Controls.Add(this._Progress);
			this.Controls.Add(this.label2);
			this.Controls.Add(this._startBackup);
			this.Controls.Add(this._Browse);
			this.Controls.Add(this.label1);
			this.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "RestoreProfile";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Restore Profile Wizard";
			this.ResumeLayout(false);

		}
		#endregion

		private void OnBrowse(object sender, EventArgs e)
		{
			BrowseForFolderClass myFolderBrowser = new BrowseForFolderClass();
			_path = myFolderBrowser.BrowseForFolder("Please, select a folder");
			this._backupFolder.Text = _path;
		}

		private void OnDBManagerMsgChanged(object sender, DBManagerEventArgs e)
		{
			ShowDBManagerMsgAsync(e.Message);
		}

		delegate void ShowDBManagerMsgAsyncDelegate (string _msg);
		void ShowDBManagerMsgAsync(string _msg)
		{
			if (this.InvokeRequired)
			{
				this.BeginInvoke(new ShowDBManagerMsgAsyncDelegate(ShowDBManagerMsgAsync), new object[] {_msg});				
			}
			else
			{								
				_Progress.Text = _msg;
				_Progress.Update();
			}
		}

		void EnableClosing(bool value)
		{
			_Close.Enabled = value;
		}
				
		protected override void OnClosing(CancelEventArgs e)
		{
			if (!validClose)
				e.Cancel = true;

			base.OnClosing (e);
		}

		private void _Close_Click(object sender, EventArgs e)
		{
			validClose = true;
			this.Close();
		}

		private void _startBackup_Click(object sender, EventArgs e)
		{
			if (string.Empty == _path)
			{
				MessageBox.Show(this, "Please select backup folder!", "Missing info!", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}

			_path = _path.ToUpper();
			string temp = _path.Substring(_path.Length - ProfilePath.Length, _path.Length - (_path.Length - ProfilePath.Length));
			if (temp != ProfilePath.ToUpper())
			{
				MessageBox.Show(this, "Please select profile backup folder!\nFolder name must end with 'Profiles'!", "Missing info!", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}

			_path = _path.Substring(0, _path.Length - ProfilePath.Length + 1);

			string errMsg = string.Empty;
			ShowDBManagerMsgAsync(string.Empty);
			EnableClosing(false);

			RestoreProfileManager rpm = new RestoreProfileManager(Globals.PG_DIR, _path);
			rpm.OnProfileManagerMsgChanged += new UpdateManager.Profile_Manager.ProfileManager.ProfileManagerMsgEventHandler(OnProfileManagerMsgChanged);			
			if (false == rpm.DoWork())
			{
				ShowDBManagerMsgAsync("Profile Backup Failed!");				
			}
			else
			{
				MessageBox.Show(this, "Backup Profile successfull!", "Sucess!", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
			rpm.OnProfileManagerMsgChanged += new UpdateManager.Profile_Manager.ProfileManager.ProfileManagerMsgEventHandler(OnProfileManagerMsgChanged);			
			EnableClosing(true);
		}

		private void OnProfileManagerMsgChanged(object sender, ProfileManagerEventArgs e)
		{
			ShowDBManagerMsgAsync(e.Message);
		}
	}
}
