using System;
using System.IO;

namespace UpdateManager.Profile_Manager
{
	/// <summary>
	/// RestoreProfileManager
	///		Restore Profiles
	/// </summary>
	public class RestoreProfileManager : ProfileManager
	{
		public RestoreProfileManager(string installDir, string backupDir) : base (installDir, backupDir)
		{			
		}

		/// <summary>
		/// Restore Procedure
		/// </summary>
		public override bool DoWork()
		{
			bool bRes = true;
			if (!Directory.Exists(base.InstallDir))		
			{
				base.FireMessage("Invalid Installation path");
				return false;
			}

			if (!Directory.Exists(base.BackupDir))
			{
				base.FireMessage("Invalid Backup Directory!");
				return false;
			}

			try
			{
				base.CopyDirectory(new DirectoryInfo(BackupDir), new DirectoryInfo(InstallDir), true);
			}
			catch (Exception exc)
			{
				base.FireMessage("Error during directory copy! " + exc.Message);
				bRes = false;
			}

			return bRes;
		}
	}
}
