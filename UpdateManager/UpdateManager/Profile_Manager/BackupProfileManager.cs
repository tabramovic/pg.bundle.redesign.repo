using System;
using System.IO;

namespace UpdateManager.Profile_Manager
{
	/// <summary>
	/// BackupProfileManager
	///		Backup Profiles
	/// </summary>
	public class BackupProfileManager : ProfileManager
	{
		public BackupProfileManager(string installDir, string backupDir) : base (installDir, backupDir)
		{
		}

		/// <summary>
		/// Backup Procedure
		/// </summary>
		public override bool DoWork()
		{
			bool bRes = true;
			if (!Directory.Exists(base.InstallDir))		
			{
				base.FireMessage("Invalid Installation path");
				return false;
			}

			if (!Directory.Exists(base.BackupDir))
			{
				try
				{
					Directory.CreateDirectory(base.BackupDir);
				}
				catch
				{
					base.FireMessage("Invalid Backup Directory!");
					return false;
				}
			}

			try
			{				
				base.CopyDirectory(new DirectoryInfo(InstallDir), new DirectoryInfo(BackupDir), true);
			}
			catch (Exception exc)
			{
				base.FireMessage("Error during directory copy! " + exc.Message);
				bRes = false;
			}

			return bRes;
		}
	}
}
