using System;
using System.IO;

namespace UpdateManager.Profile_Manager
{
	/// <summary>
	/// Profile Manager
	/// Reallocates profiles (backup & restore)
	/// </summary>
	public abstract class ProfileManager
	{
		public delegate void ProfileManagerMsgEventHandler(object sender, ProfileManagerEventArgs e);		
		public event ProfileManagerMsgEventHandler OnProfileManagerMsgChanged;
		
		string _installDir = string.Empty;
		string _backupDir = string.Empty;

		public ProfileManager(string installDir, string backupDir)
		{
			_installDir = installDir;
			_backupDir = backupDir;
		}

		public string InstallDir
		{
			get { return _installDir; }
		}

		public string BackupDir
		{
			get { return _backupDir; }
		}		
		
		public abstract bool DoWork();

		protected void FireMessage(string msg)
		{
			if (null != OnProfileManagerMsgChanged)
				OnProfileManagerMsgChanged(this, new UpdateManager.ProfileManagerEventArgs(msg));
		}

		protected void CopyDirectory(DirectoryInfo srcPath, DirectoryInfo destPath, bool recursive)
		{
			if(!destPath.Exists)
				destPath.Create();

			// copy files
			foreach(FileInfo fi in srcPath.GetFiles())
			{
				fi.CopyTo(Path.Combine(destPath.FullName, fi.Name), true);
				FireMessage(destPath.FullName + @"\" + fi.Name);
			}

			// copy directories
			if(recursive)
			{
				foreach(DirectoryInfo di in srcPath.GetDirectories())
				{
					FireMessage(di.FullName);
					CopyDirectory(di, new DirectoryInfo( Path.Combine( destPath.FullName, di.Name ) ), recursive);
					System.Threading.Thread.Sleep(500);	//wait 0.5 secs
				}
			}
		}
	}
}
