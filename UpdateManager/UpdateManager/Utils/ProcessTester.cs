using System;
using System.Diagnostics;

namespace UpdateManager.Utils
{
	/// <summary>
	/// Summary description for ProcessTester.
	/// </summary>
	public class ProcessTester
	{
		string _procName;
		public ProcessTester(string processName)
		{
			_procName = processName;
		}

		public bool TestIfActive(out bool processRunning, out string error)
		{
			processRunning = false;
			error = string.Empty;

			try
			{
				Process[] processes = Process.GetProcessesByName(_procName);
				if (processes.Length > 0)
				{
					processRunning = true;
				}
				processRunning = false;
				return true;
			}
			catch (Exception exc)
			{
				error = exc.Message;				
				return false;
			}			
		}
	}
}
