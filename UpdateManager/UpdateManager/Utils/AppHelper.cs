using System;

//Added 
using System.Windows.Forms;
using System.Threading;
using System.Collections;
using System.Xml;
using System.IO;

namespace UpdateManager
{
	/// <summary>
	/// Summary description for AppHelper.
	/// </summary>
	public class AppHelper
	{
		public AppHelper()
		{
			
		}

		public static void WatchGUIStarter()
		{
			DownloadTrackForm dtf = new DownloadTrackForm();
			dtf.ShowDialog();
		}

		public static void DownloadWorker()
		{
			Globals.SuccessFullDownload = true;
			int iCnt = 1;
			foreach (ComponentDescriptor componentDesc in Globals.cdDownloadArray)
			{
				bool bResult = DownloadManagerClass.Download(componentDesc.FromLocation, componentDesc.ToLocation, componentDesc.ComponentName, iCnt, Globals.cdDownloadArray.Length);
				if (false == bResult)
				{
					Globals.SuccessFullDownload = bResult;
				}
				iCnt++;
			}					
		}

		public static void DownloadFiles(ComponentDescriptor[] cdArray)
		{						
			Globals.cdDownloadArray = cdArray;
			Globals.downloadWorker = new Thread(new ThreadStart(DownloadWorker));
			Globals.downloadWorker.Priority = ThreadPriority.Highest;
			Globals.downloadWorker.Start();			
		}

		public static ComponentDescriptor[] PrepareBinariesForDownload(Hashtable htDownloadFiles)
		{
			ComponentDescriptor[] cdArray = new ComponentDescriptor[htDownloadFiles.Count];
			IDictionaryEnumerator ide = htDownloadFiles.GetEnumerator();
			int i = 0;
			while (ide.MoveNext())
			{
				BinaryDescriptor componentBinDesc = (BinaryDescriptor)ide.Value;				
				cdArray[i] = componentBinDesc.BinDesc2CompDesc();
				i++;
			}
			return cdArray;
		}

		public static void GetUpdateDescriptor()
		{
			if (false == Directory.Exists(Globals.PG_DIR + Globals.LocalUpdates))
			{
				Directory.CreateDirectory(Globals.PG_DIR + Globals.LocalUpdates);
			}
			
			//TA++
			//OLD
			//Globals.trackerThread = new Thread(new ThreadStart(AppHelper.WatchGUIStarter));
			//Globals.trackerThread.Priority = ThreadPriority.AboveNormal;			
			//Globals.trackerThread.Start();			
			//TA--
			
			AppHelper.DownloadFiles	
				(new ComponentDescriptor[] { 
					new ComponentDescriptor (
											   Globals.UpdatesDir + Globals.UpdateDescriptor,
											   Globals.PG_DIR + Globals.LocalUpdates + Globals.LocalUpdateDescriptor, 
											   Globals.LocalUpdateDescriptor
											)
											}
				);
			
			//NEW++
			Globals.trackerThread = new Thread(new ThreadStart(AppHelper.WatchGUIStarter));
			Globals.trackerThread.Priority = ThreadPriority.AboveNormal;			
			Globals.trackerThread.Start();			
			//NEW--

			Globals.trackerThread.Join();
			Globals.downloadWorker.Join();		
		}

		public static Hashtable CompareFiles()
		{			
			Hashtable htDownloadFiles = new Hashtable();
			IDictionaryEnumerator ide = Globals.htUpdateFiles.GetEnumerator();
			while (ide.MoveNext())
			{
				string updateFileName = (string)ide.Key;
				BinaryDescriptor updateBD = (BinaryDescriptor)ide.Value;
				
				if (false == Globals.htLocalFiles.Contains(Globals.PG_DIR + updateFileName))
				{
					htDownloadFiles.Add(updateFileName, updateBD);
				}
				else
				{
					string location = updateBD.DestinationPath;
					location = location.Replace(Globals.InstallDir, Globals.PG_DIR);
					BinaryDescriptor localFileBD = (BinaryDescriptor)Globals.htLocalFiles[location + updateFileName];					
					Version updateFileVer = null;
					Version localFileVer = null;
					try
					{
						updateFileVer = new Version(updateBD.Version);					
						localFileVer = new Version(localFileBD.Version);
						if (updateFileVer > localFileVer)
						{
							htDownloadFiles.Add(updateFileName, updateBD);
						}
					}
					catch (Exception)
					{
						if (null == localFileBD)	//download only if new files are not existing on HDD
							htDownloadFiles.Add(updateFileName, updateBD);
					}
				}
			}
			return htDownloadFiles;
		}

		public static bool ParseUpdateDescriptor()
		{			
			bool bResult = true;
			XmlTextReader reader = null;
			Globals.htUpdateFiles.Clear();
				
			try
			{
				reader = new XmlTextReader(Globals.PG_DIR + Globals.LocalUpdates + Globals.LocalUpdateDescriptor);
				reader.MoveToContent();
				reader.WhitespaceHandling = WhitespaceHandling.None;
				
				while (reader.Read())
				{
					string fileInfo = reader.Name;
					if ("FileInfo" == fileInfo)
					{
						string fileName = reader.GetAttribute("FileName");
						string version = reader.GetAttribute("Version");
						string destinationPath = reader.GetAttribute("DestinationPath");

						BinaryDescriptor bd = new BinaryDescriptor(fileName, version, destinationPath);
						Globals.htUpdateFiles.Add(fileName, bd);
					}					
				}
			}
			catch (Exception exc)
			{
				MessageBox.Show(exc.ToString());
				bResult = false;
			}
			finally
			{
				reader.Close();
			}
			return bResult;
		}

		public static string GetTimedPath()
		{
			return DateTime.Now.Year.ToString() + "_" + DateTime.Now.Month.ToString() + "_" + DateTime.Now.Day.ToString() + "___" + 
				DateTime.Now.Hour.ToString() + "_" + DateTime.Now.Minute.ToString() + "_" + DateTime.Now.Second.ToString();
		}

	}
}
