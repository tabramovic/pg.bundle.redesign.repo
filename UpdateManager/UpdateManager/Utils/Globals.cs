#define NETWORK

using System;
using System.Collections;
using System.Threading;

namespace UpdateManager
{
	/// <summary>
	/// Summary description for Globals.
	/// </summary>
	public class Globals
	{
		public Globals()
		{
			
		}

		public static bool Notification = false;

		public static string ProcName = "DealMaker";

#if (NETWORK)
		//NETWORK UPDATE MANAGER
		public static string UpdatesDir = "http://www.profitgrabber.com/RY16/updates/";		
#else
		//PLAIN UPDATE MANAGER
        public static string UpdatesDir = "http://www.profitgrabber.com/RY09/updates/";
#endif
		

		public static string UpdateDescriptor = "updatemanagerdesc.xml";		
		public static string LocalUpdates = @"Updates\";

		public static bool	 DirectoryScanned = false;
		public static string PG_DIR = string.Empty;		
		public static string LocalUpdateDescriptor = "UpdatesDescriptor.xml";

		public static string Downloading = "Downloading...";
		public static string DownloadFinished = "Finished";
		public static string CloseDialog = "Close";
		public static string InstallDir = "[INSTALLDIR]";

		
		public static Hashtable htLocalFiles = new Hashtable();
		public static Hashtable htUpdateFiles  = new Hashtable();
		public static Hashtable htDownloadFiles = new Hashtable();

		public static bool CancelDownload = false;
		public static bool SuccessFullDownload = true;
		
		public static ComponentDescriptor[] cdDownloadArray = null;

		public static Thread trackerThread = null;
		public static Thread downloadWorker = null;

		public static string PredefinedInstalldir = @"C:\Program Files\ProfitGrabber Pro\";
	}
}
