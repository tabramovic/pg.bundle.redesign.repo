using System;
using System.IO;
using System.Net;
using System.Windows.Forms;

namespace UpdateManager
{
	/// <summary>
	/// DownloadManager Class.
	/// </summary>
	public class DownloadManagerClass
	{		 
		public delegate void ChangedDownloadPercentageEventHandler(object sender, PercentageChangedEventArgs e);
		public delegate void FileDownloadingEventHandler(object sender, FileDownloadingEventArgs e);
		public delegate void FileNotFoundEventHandler(object sender, FileNotFoundEventArgs e);

		public static event ChangedDownloadPercentageEventHandler DownloadPercentageChanged;
		public static event FileDownloadingEventHandler FileDownloading;
		public static event FileNotFoundEventHandler FileNotFound;

		public DownloadManagerClass()
		{
			
		}

		public static bool Download(string path_download, string path_local, string fileName, int currFile, int totalFiles)
		{
			Globals.CancelDownload = false;

			//fire event - download started
			FileDownloadingEventArgs fdea = new FileDownloadingEventArgs(eDownloadFileStatus.eStarted, false, path_download, path_local, fileName, currFile, totalFiles);
			DownloadManagerClass.FileDownloading(null, fdea);
			
			bool result = true;			
			HttpWebRequest lHttpWebRequest;			
			HttpWebResponse lHttpWebResponse;			
			Stream lHttpWebResponseStream;
			FileStream lFileStream = null;
			try
			{
				lFileStream = new FileStream(path_local, FileMode.Create);
			}
			catch (Exception 
#if DEBUG
				exc
#endif	
				)
			{
#if DEBUG
				MessageBox.Show(exc.Message + System.Environment.NewLine + exc.InnerException);
#endif				
				result = false;
				return result;
			}
			// Declare a variable of type Byte Array named byteBuffer.
			byte[] byteBuffer = new byte[999];
			// Declare a variable of type Integer named bytesRead.
			int bytesRead;
			try
			{				
				try
				{
					lHttpWebRequest = (HttpWebRequest)WebRequest.Create(path_download);// Instantiate the HttpWebRequest object.				
					lHttpWebResponse = (HttpWebResponse)lHttpWebRequest.GetResponse();// Instantiate the HttpWebRespose object.				
					lHttpWebResponseStream = lHttpWebRequest.GetResponse().GetResponseStream();	// Instantiate the ResponseStream object.

					lHttpWebRequest.Timeout = 30;					
				}
				catch (Exception exc)
				{
					//fire event - file not found
					FileNotFoundEventArgs fnfea = new FileNotFoundEventArgs(fileName, path_download, exc.Message);
					DownloadManagerClass.FileNotFound(null, fnfea);
					result = false;
					return result;
				}
								
				Int32 totalLength = Convert.ToInt32(lHttpWebResponse.ContentLength);
				Int32 totalBytesRead = 0;
								
				do
				{														
					bytesRead = lHttpWebResponseStream.Read(byteBuffer, 0, 999);// Read up to 1000 bytes into the bytesRead array.					
					lFileStream.Write(byteBuffer, 0, bytesRead);// Write the bytes read to the file stream.														
					totalBytesRead += bytesRead;
					try
					{
						if (null != DownloadManagerClass.DownloadPercentageChanged)
						{
							PercentageChangedEventArgs pcea = new PercentageChangedEventArgs(totalLength, totalBytesRead);
							DownloadManagerClass.DownloadPercentageChanged(null, pcea);					
						}
					}
					catch					
					{
						//this would be really weird...
					}
				}while(bytesRead > 0);				
				lHttpWebResponseStream.Close();// Close the file and web response streams.				
			}
			catch(Exception download_error)
			{
				// display the whole error
#if DEBUG
				MessageBox.Show(download_error.ToString());
#endif
				//fire event - file not found
				try
				{
					if (null != DownloadManagerClass.FileNotFound)
					{
						FileNotFoundEventArgs fnfea = new FileNotFoundEventArgs(fileName, path_download, download_error.Message);
						DownloadManagerClass.FileNotFound(null, fnfea);
					}
				}
				catch				
				{
					//this would be really weird
				}
				result = false;
				return result;
			}
			finally
			{
				// Close the file and web response streams.
				lFileStream.Flush();
				lFileStream.Close();				
				
				if (true == result)
				{
					try
					{
						if (null != DownloadManagerClass.FileDownloading)
						{
							//fire event - downloaded
							fdea = new FileDownloadingEventArgs(eDownloadFileStatus.eFinished, result, path_local, path_download, fileName, currFile, totalFiles);
							DownloadManagerClass.FileDownloading(null, fdea);
						}
					}
					catch
					{
						//this would be weird...
					}					
				}
			}
			return result;
		}				
	}
}
