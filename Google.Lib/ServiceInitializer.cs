﻿using System.Threading;
using System.Threading.Tasks;
using System.Diagnostics;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Services;
using Google.Apis.Gmail.v1;
using System;

namespace google.oauth.lib
{
    public class ServiceInitializer
    {
        string[] Scopes = { GmailService.Scope.GmailSend, "https://www.googleapis.com/auth/spreadsheets" };
        async internal Task<BaseClientService.Initializer> InitializeGmailAccountService(string applicationName, string clientId, string clientSecret, string userToAuthorize = null)
        {
            if (string.IsNullOrEmpty(userToAuthorize))
                userToAuthorize = "user";

            var secrets = new ClientSecrets()
            {
                ClientId = clientId,
                ClientSecret = clientSecret
            };

            try
            {
                CancellationTokenSource cts = new CancellationTokenSource();
                cts.CancelAfter(TimeSpan.FromSeconds(100));
                CancellationToken ct = cts.Token;
                
                var credentialTask = await GoogleWebAuthorizationBroker.AuthorizeAsync(secrets, Scopes, userToAuthorize, ct);

                if (ct.IsCancellationRequested)
                    throw new Exception("timeout");
                
                Debug.WriteLine($"access token: {credentialTask.Token.AccessToken}");
                Debug.WriteLine($"refresh token: {credentialTask.Token.RefreshToken}");

                return new BaseClientService.Initializer()
                {
                    HttpClientInitializer = credentialTask,
                    ApplicationName = applicationName,
                };
            }
            catch (Exception exc)
            {
                throw exc;
            }
        }
    }
}
