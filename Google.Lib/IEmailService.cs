﻿using System.Net.Mail;
using System.Threading.Tasks;

namespace google.oauth.lib
{
    public interface IEmailService
    {
        string SendEmail(MailMessage mailMessage);
        Task<bool> InitializeGmailAccountService(string applicationName, string clientId, string clientSecret, string userToAuthorize = null);
    }
}
