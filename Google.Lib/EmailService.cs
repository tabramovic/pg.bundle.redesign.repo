﻿using Google.Apis.Gmail.v1;
using Google.Apis.Gmail.v1.Data;
using Google.Apis.Services;
using System;
using System.IO;
using System.Net.Mail;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Text;

namespace google.oauth.lib
{
    public class EmailService : IEmailService, IDisposable
    {
        BaseClientService.Initializer _initalizer;

        public void Dispose()
        {
            _initalizer = null;
        }

        public async Task<bool> InitializeGmailAccountService(string applicationName, string clientId, string clientSecret, string userToAuthorize = null)
        {            
            try
            {
                _initalizer = await new ServiceInitializer().InitializeGmailAccountService(applicationName, clientId, clientSecret, userToAuthorize);
                return true;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Returns SentMessage internal date
        /// </summary>        
        /// <param name="mm">Mail message</param>
        /// <returns></returns>
        public string SendEmail(MailMessage mailMessage)
        {
            if (null == _initalizer)
                throw new Exception("unauthorized");

            if (null == mailMessage)
                throw new Exception("mailMessageParam is null");

            var msg = new AE.Net.Mail.MailMessage
            {
                Subject = mailMessage.Subject,
                Body = mailMessage.Body,
                From = mailMessage.From
            };
            foreach (var mailAddress in mailMessage.To)
                msg.To.Add(mailAddress);

            msg.ReplyTo.Add(mailMessage.From); // Bounces without this!!

            if (null != mailMessage.Attachments && mailMessage.Attachments.Count > 0)
            {
                var attach = mailMessage.Attachments[0];
                var fileAsBytes = ((MemoryStream)attach.ContentStream).ToArray();

                var attachment = new AE.Net.Mail.Attachment()
                {
                    Body = Convert.ToBase64String(fileAsBytes),
                    ContentTransferEncoding = "base64",
                    Encoding = Encoding.ASCII,
                };


                //attachment.Headers.Add("Content-Disposition", new AE.Net.Mail.HeaderValue(@"application/vnd.ms-excel; filename=""test.xlsx"""));
                string cd = $"{attach.ContentType.MediaType}; filename=\"{Path.GetFileName(attach.Name)}\"";
                attachment.Headers.Add("Content-Disposition", new AE.Net.Mail.HeaderValue(cd));
                msg.Attachments.Add(attachment);
            }

            var msgStr = new StringWriter();
            msg.Save(msgStr);

            var gmailService = new GmailService(_initalizer);

            try
            {
                var result = gmailService.Users.Messages.Send(
                    new Message
                    {
                        Raw = ProjectUtils.Base64UrlEncode(msgStr.ToString())
                    },
                    "me")
                    .Execute();

                Debug.WriteLine($"Message ID {result.Id} sent, internal date set to: {result.InternalDate}");

                return result?.Id;
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
                throw;
            }
        }
    }
}
