﻿using System;
using System.IO;
using System.Net.Mime;

namespace google.oauth.lib
{
    public class ProjectUtils
    {
        internal static string Base64UrlEncode(string input)
        {
            var inputBytes = System.Text.Encoding.UTF8.GetBytes(input);
            // Special "url-safe" base64 encode.
            return Convert.ToBase64String(inputBytes)
              .Replace('+', '-')
              .Replace('/', '_')
              .Replace("=", "");
        }

        public static string GetMediaType(string file)
        {
            var ext = Path.GetExtension(file);
            switch (ext)
            {
                case ".pdf":
                    return MediaTypeNames.Application.Pdf;

                case ".jpg":
                    return MediaTypeNames.Image.Jpeg;

                case ".png":
                    return "image/png";

                case ".xls":
                    return "application/vnd.ms-excel";

                case ".xlsx":
                    return "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

                case ".doc":
                    return "application/msword";

                case ".docx":
                    return "application/vnd.openxmlformats-officedocument.wordprocessingml.document";

                default:
                    return MediaTypeNames.Application.Pdf;
            }
        }

        internal static ContentType GetContentType(string file)
        {
            return new ContentType(GetMediaType(file));
        }
    }
}
