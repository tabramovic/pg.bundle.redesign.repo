﻿using System;
using System.Configuration;
using System.Web.Http;
using Microsoft.Extensions.Logging;
using DataAppendService.Helpers;
using DataAppendService.Providers.IDI;

namespace DataAppendService
{
    public class WebApiApplication : System.Web.HttpApplication
    {        
        protected void Application_Start()
        {            
            System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;
                        
            var successfulInit = true;

            GlobalConfiguration.Configure(WebApiConfig.Register);
            UnityConfig.RegisterComponents();

            var logger =
                GlobalConfiguration.Configuration.DependencyResolver.GetService(typeof(ILogger)) 
                    as ILogger;

            logger.LogInformation("Starting Data Append Service.");

            var tokenManager =
                GlobalConfiguration.Configuration.DependencyResolver.GetService(typeof(ITokenManager))
                    as ITokenManager;

            tokenManager.StartRefreshingToken();

            try { Globals.IsHashVerificationEnabled = bool.Parse(ConfigurationManager.AppSettings["EnableHashVerification"]); }
            catch (Exception e) { logger.LogError($"Application_Start load&convert AppSettings[\"EnableHashVerification\"] failed with: {e}"); successfulInit = false; }                        

            logger.LogInformation($"Service starting {(successfulInit ? "successfully" : "missing service token or url or EnableHashVerification")}");
        }
    }
}
