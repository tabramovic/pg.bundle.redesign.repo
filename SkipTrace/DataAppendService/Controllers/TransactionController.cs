﻿using System;
using System.Web.Http;
using Microsoft.Extensions.Logging;
using DataAppendService.DAL;
using System.Web.Http.Description;

namespace DataAppendService.Controllers
{
    public class TransactionController : ApiController
    {
        private readonly ILogger _logger;

        public TransactionController(ILogger logger)
        {
            _logger = logger;
        }

        [HttpGet]
        [Route("api/Transaction/{pgid}")]
        [ResponseType(typeof(IHttpActionResult))]
        public IHttpActionResult Get(Guid pgid)
        {
            var tranlogDAL = new TranlogDAL(_logger);
            var transactions = tranlogDAL.GetAllTransactionsFor(pgid);

            if (null != transactions)
                return Ok(transactions);
            else
                return BadRequest();
        }        
    }
}
