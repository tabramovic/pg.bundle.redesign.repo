﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using DataAppendService.Helpers;
using DataAppendService.Lib.Models.IDI;
using DataAppendService.DAL;
using Microsoft.Extensions.Logging;

using DataAppendService.Providers.IDI;

namespace DataAppendService.Controllers
{
    public class AppendController : ApiController
    {        
        private readonly HttpClient _httpClient;
        private readonly ITokenManager _tokenManager;
        private readonly ILogger _logger;

        public AppendController(HttpClient httpClient, ITokenManager tokenManager, ILogger logger)
        {
            _httpClient = httpClient;
            _tokenManager = tokenManager;
            _logger = logger;
        }
        
        // GET: api/Append/TestIfAlive
        [HttpGet]
        [Route("api/Append/TestIfAlive")]
        public IEnumerable<string> TestIfAlive()
        {            
            return new string[] { "Alive at", $"{DateTime.Now.ToLongDateString()} {DateTime.Now.ToLongTimeString()} "};
        }

        // GET: api/Append/TestDBConnection
        [HttpGet]
        [Route("api/Append/TestDBConnection")]
        public IEnumerable<string> TestDBConnection()
        {
            string testDBConnError = string.Empty;
            var dbConnValid = true;
            try
            {
                var bd = new BaseDAL(_logger);
                var testConnectionRes = bd.TestConnection();
            }
            catch(Exception e)
            {
                dbConnValid = false;
                testDBConnError = $"Test DB Connection Failed: {e}";
            }
            return new string[] { "Alive at", $"{DateTime.Now.ToLongDateString()} {DateTime.Now.ToLongTimeString()},", $"{ (dbConnValid ? "DB Connection OK" : testDBConnError)}" };
        }


        // POST: api/Append
        public async Task<IHttpActionResult> Post([FromBody]ClientRequest clientRequest)
        {
            //do not check for null or empty FirstName or City for backward compatibility reasons (as all PG will not update at the same time)
            if (null == clientRequest 
                || clientRequest.PGID == Guid.Empty
                || string.IsNullOrEmpty(clientRequest.Address) 
                || string.IsNullOrEmpty(clientRequest.ZIP) 
                || string.IsNullOrEmpty(clientRequest.Hash)
                || ( Globals.IsHashVerificationEnabled && !clientRequest.IsValid()) 
                )
            {
                return BadRequest();
            }

            clientRequest.RequestType = "ALL";
                                    
            if (null == clientRequest.LastName)
                clientRequest.LastName = string.Empty;

            if (null == clientRequest.FirstName)
                clientRequest.FirstName = string.Empty;

            if (null == clientRequest.City)
                clientRequest.City = string.Empty;

            _logger.LogInformation($"Acquiring ReferenceId (transactionid) for search Request: {clientRequest}");

            var appendRequestDAL = new AppendRequestDAL(_logger);
            var insertDataResponse = appendRequestDAL.InsertDataAppendRequest(clientRequest.PGID, $"{clientRequest.FirstName} {clientRequest.LastName}", $"{clientRequest.Address} {clientRequest.City}", clientRequest.ZIP, clientRequest.RequestType);
            if (null == insertDataResponse || !insertDataResponse.MethodSuccess)
                return InternalServerError();

            if (!insertDataResponse.ReserveFundsSuccess)
                return Unauthorized();

            var tranId = insertDataResponse.TranId;
                        
            var skiptraceReq = clientRequest.AsSkiptraceRequest(tranId) ;
                                                                                    
            try
            {
                _logger.LogDebug($"Request Data For: {skiptraceReq}");

                var skiptraceRes = await new SkiptraceSearch(_tokenManager.Token, _httpClient, _logger).SkipTraceAsync(skiptraceReq);                

                if (!skiptraceRes.CheckForMatchingReferenceIds(skiptraceReq))
                {
                    _logger.LogError($"Service replied with referenceId:{skiptraceRes.ReferenceId} different than request referenceId:{skiptraceReq.referenceId}");
                    appendRequestDAL.UpdateDataAppendRequest(tranId, skiptraceRes.ToDBFormattedString(), false, false);
                    return InternalServerError();
                }

                if (!skiptraceRes.HasMatched)
                {
                    _logger.LogDebug($"NO MATCH: For{skiptraceReq}:{Environment.NewLine}service replied with:{Environment.NewLine}{skiptraceRes}");
                    appendRequestDAL.UpdateDataAppendRequest(tranId, skiptraceRes.ToDBFormattedString(), false, false);
                    return NotFound();
                }
                else
                {
                    var skipTraceClientResultSet = skiptraceRes.AsSkiptraceClientResultset();
                    _logger.LogInformation($"MATCH SUCCESS for {skiptraceReq.referenceId}, match BILLABLE: {skipTraceClientResultSet.IsBillable}");
                    _logger.LogDebug($"{Environment.NewLine}\tRequest:{skiptraceReq}{Environment.NewLine}\tResponse:{skipTraceClientResultSet}");
                    appendRequestDAL.UpdateDataAppendRequest(tranId, skiptraceRes.ToDBFormattedString(), true, skipTraceClientResultSet.IsBillable);
                    return Ok(skipTraceClientResultSet); 
                }                                
            }
            catch (Exception e)
            {
                _logger.LogError($"Skiptrace search failed with:{e}{Environment.NewLine}{e.InnerException}");
                return InternalServerError(e);
            }
        }
    }
}
