﻿using System;
using System.Web.Http;
using System.Web.Http.Description;
using DataAppendService.DAL;
using Microsoft.Extensions.Logging;

namespace DataAppendService.Controllers
{
    public class WalletController : ApiController
    {
        private readonly ILogger _logger;

        public WalletController(ILogger logger)
        {
            _logger = logger;
        }

        [HttpGet]
        [Route("api/Wallet/{pgid}")]
        [ResponseType(typeof(IHttpActionResult))]
        public IHttpActionResult Get(Guid pgid)
        {
            var walletDAL = new WalletDAL(_logger);
            var wallet = walletDAL.GetWalletAmount(pgid);

            if (null != wallet)
                return Ok(wallet);
            else
                return BadRequest();
        }
    }
}
