﻿using System;
using System.Web.Http;
using System.Web.Http.Description;
using Microsoft.Extensions.Logging;
using DataAppendService.Models;
using DataAppendService.DAL;

namespace DataAppendService.Controllers
{
    public class DepositController : ApiController
    {
        private readonly ILogger _logger;

        public DepositController(ILogger logger)
        {
            _logger = logger;
        }

        [HttpGet]        
        [Route("api/Deposit/{pgid}")]
        [ResponseType(typeof(IHttpActionResult))]
        public IHttpActionResult GetAllDeposits(Guid pgid)
        {
            var depositDal = new DepositDAL(_logger);
            var deposits = depositDal.GetAllDepositsFor(pgid);

            if (null != deposits)
                return Ok(deposits);
            else
                return BadRequest();
        }

        [HttpPost]
        [Route("api/Deposit/CreateDepositAnnouncement")]
        [ResponseType(typeof(IHttpActionResult))]
        public IHttpActionResult CreateDepositAnnouncement(AnnounceDeposit ad)
        {
            bool insertRes = true;
            var depositDal = new DepositDAL(_logger);
             
            var adInsert = new AnnounceDeposit() { Id = Guid.NewGuid(), PGID = ad.PGID, Amount = ad.Amount };
            
            insertRes = depositDal.InsertDepositAnnouncement(adInsert);
            
            //check for hash and throw badrequest if bad.

            if (insertRes)
                return Ok(adInsert);
            else
                return InternalServerError();
        }
        
        [HttpPost]
        [Route("api/Deposit/DepositFunds")]
        [ResponseType(typeof(IHttpActionResult))]
        public IHttpActionResult DepositFunds(AnnounceDeposit ad)
        {
            var depositDal = new DepositDAL(_logger);
            var deposit = depositDal.InsertDepositFromAnnouncement(ad);

            if (null != deposit)
                return Ok(deposit);
            else
                return BadRequest($"Create deposit from announcement failed for: {ad.Id}");
        }

        [HttpPost]
        [Route("api/Deposit/DepositFundsInsecure")]
        [ResponseType(typeof(IHttpActionResult))]
        public IHttpActionResult DepositFundsInsecure(Deposit d)
        {
            var depositDal = new DepositDAL(_logger);
            d.Created = DateTime.Now;
            var res = depositDal.InsertDepositInsecure(d);

            if (res)
                return Ok(d);
            else
                return BadRequest($"DepositFundsInsecure failed for: {d.PGID}");
        }        
    }
}
