using System.Web.Http;
using System.Net.Http;
using Unity;
using Unity.Lifetime;
using Unity.Injection;
using Unity.WebApi;
using Serilog;
using ILogger = Microsoft.Extensions.Logging.ILogger;
using DataAppendService.Providers.IDI;

namespace DataAppendService
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
			var container = new UnityContainer();
                                                            
            GlobalConfiguration.Configuration.DependencyResolver = new UnityDependencyResolver(container);

            container.RegisterFactory<ILogger>(unityContainer => CreateFactoryLogger(),
                new SingletonLifetimeManager());

            container.RegisterFactory<HttpClient>(_ => HttpClientFactory.Create());

            container.RegisterSingleton<ITokenManager, TokenManager>(
                new InjectionConstructor(
                        container.Resolve<HttpClient>(),
                        container.Resolve<ILogger>()
                )                
            );            
        }

        private static object CreateFactoryLogger()
        {
            var loggerFactory = new Microsoft.Extensions.Logging.LoggerFactory();
            
            var log = new LoggerConfiguration()
                .Enrich.FromLogContext() //enables scopes                
                .ReadFrom.AppSettings()
                .CreateLogger();

            loggerFactory.AddSerilog(log);

            return loggerFactory.CreateLogger("skip-trace-service");
        }
    }
}