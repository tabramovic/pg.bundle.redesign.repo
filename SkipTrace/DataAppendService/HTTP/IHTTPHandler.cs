﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAppendService.HTTP
{
    interface IHTTPHandler
    {
        Task<object> GetAsync(string suffix);

        Task<object> PostAsync(string suffix, string payload);
    }
}