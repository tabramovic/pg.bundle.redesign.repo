﻿using Microsoft.Extensions.Logging;
using System.Collections.Generic;

namespace DataAppendService.HTTP
{
    public class AppendDataHTTPHandler : HTTPHandler
    {
        public AppendDataHTTPHandler(string baseUrl, List<KeyValuePair<string, string>> defaultHeaders, ILogger logger) : base(baseUrl, defaultHeaders, logger)
        {

        }
    }
}