﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using System.Text;
using Microsoft.Extensions.Logging;

namespace DataAppendService.HTTP
{
    public abstract class HTTPHandler : IHTTPHandler
    {
        private readonly ILogger _logger;

        private string BaseURL { get; set; }
        private List<KeyValuePair<string, string>> DefaultHeaders { get; set; }

        public HTTPHandler(string baseUrl, List<KeyValuePair<string, string>> defaultHeaders, ILogger logger)
        {
            BaseURL = baseUrl;
            DefaultHeaders = defaultHeaders;
            _logger = logger;
        }

        public async Task<object> PostAsync(string suffix, string payload)
        {
            _logger.LogInformation($"POST {BaseURL}{suffix} for DefaultHeaders: {string.Join(",", (DefaultHeaders.Select(p => ("Key:" + p.Key + ", Value:" + p.Value)).ToArray()))}");

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(BaseURL);
                client.DefaultRequestHeaders.Accept.Clear();

                foreach (var kvp in DefaultHeaders)
                    client.DefaultRequestHeaders.Add(kvp.Key, kvp.Value);

                try
                {
                    var content = new StringContent(payload, Encoding.UTF8, "application/json");
                    var response = await client.PostAsync($"{BaseURL}{suffix}", content);
                    return await response.Content.ReadAsStringAsync();
                }
                catch (Exception exc)
                {
                    _logger.LogError($"POST {BaseURL}{suffix} for for DefaultHeaders: {string.Join(",", (DefaultHeaders.Select(p => ("Key:" + p.Key + ", Value:" + p.Value)).ToArray()))} failed!{Environment.NewLine}{exc}");
                    return null;
                }
            }
        }

        public async Task<object> GetAsync(string suffix)
        {
            _logger.LogInformation($"GET {BaseURL}{suffix} for DefaultHeaders: {string.Join(",", (DefaultHeaders.Select(p => ("Key:" + p.Key + ", Value:" + p.Value)).ToArray()))}");

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(BaseURL);
                client.DefaultRequestHeaders.Accept.Clear();

                foreach (var kvp in DefaultHeaders)
                    client.DefaultRequestHeaders.Add(kvp.Key, kvp.Value);

                try
                {
                    return await client.GetAsync($"{BaseURL}{suffix}");
                }
                catch (Exception exc)
                {
                    _logger.LogError($"GET {BaseURL}{suffix} for for DefaultHeaders: {string.Join(",", (DefaultHeaders.Select(p => ("Key:" + p.Key + ", Value:" + p.Value)).ToArray()))} failed!{Environment.NewLine}{exc}");
                    return null;
                }
            }
        }
    }
}