﻿using System;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Extensions.Logging;

namespace DataAppendService.DAL
{
    public class InsertDataAppendResponse
    {
        public bool ReserveFundsSuccess { get; set; }
        public bool Success { get; set; }
        public long TranId { get; set; }
        public string ErrorMsg { get; set; }
        public bool MethodSuccess { get; set; }
    }

    public class UpdateDataAppendResponse
    {
        public bool Success { get; set; }        
        public bool MethodSuccess { get; set; }
    }

    public class AppendRequestDAL : BaseDAL
    {
        public AppendRequestDAL(ILogger logger) : base (logger)
        {

        }
        public InsertDataAppendResponse InsertDataAppendRequest(Guid pgid, string lastname, string address, string zip, string querytype)
        {
            var response = new InsertDataAppendResponse();
            response.MethodSuccess = true;

            _logger.LogDebug($"InsertDataAppendRequest for pgid:{pgid}, lastname:{lastname}, address:{address}, zip:{zip}, querytype:{querytype} started.");

            try
            {
                using (SqlConnection conn = new SqlConnection(connString))
                {
                    conn.Open();
                    {
                        using (SqlCommand cmd =
                        new SqlCommand("SP_INSERT_MATCH_REQUEST", conn))
                        {
                            cmd.CommandType = CommandType.StoredProcedure;
                                                  
                            cmd.Parameters.AddWithValue("@PGID", pgid);
                            cmd.Parameters.AddWithValue("@Lastname", lastname);
                            cmd.Parameters.AddWithValue("@Address", address);
                            cmd.Parameters.AddWithValue("@ZIP", zip);
                            cmd.Parameters.AddWithValue("@QueryType", querytype);

                            var reserveFundsSuccessParam = cmd.Parameters.Add("@ReserveFundsSuccess", SqlDbType.Bit);
                            reserveFundsSuccessParam.Direction = ParameterDirection.Output;

                            var successParam = cmd.Parameters.Add("@Success", SqlDbType.Bit);
                            successParam.Direction = ParameterDirection.Output;

                            var tranIdParam = cmd.Parameters.Add("@TranId", SqlDbType.BigInt);
                            tranIdParam.Direction = ParameterDirection.Output;

                            var errorMsgParam = cmd.Parameters.Add("@ErrorMsg", SqlDbType.NVarChar, 1024);
                            errorMsgParam.Direction = ParameterDirection.Output;
                                                        
                            cmd.ExecuteNonQuery();

                            response.MethodSuccess = true;
                            response.ReserveFundsSuccess = DBNull.Value != reserveFundsSuccessParam.Value ? (bool)reserveFundsSuccessParam.Value : false;
                            response.Success = DBNull.Value != successParam.Value ? (bool)successParam.Value : false;
                            response.TranId = DBNull.Value != tranIdParam.Value ? (long)tranIdParam.Value : -1;
                            response.ErrorMsg = DBNull.Value != errorMsgParam.Value ? (string)errorMsgParam.Value : string.Empty;

                            //_logger.LogDebug($"InsertDataAppendRequest - executed SP_INSERT_MATCH_REQUEST SPL and inserted 1 record.");                            
                        }
                    }
                }
            }
            catch (Exception e)
            {
                _logger.LogError($"InsertDataAppendRequest for pgid:{pgid}, lastname:{lastname}, address:{address}, zip:{zip}, querytype:{querytype} failed with: {e}");
                response.MethodSuccess = false;
                response.Success = false;
            }

            _logger.LogDebug($"InsertDataAppendRequest acquired TranId:{response.TranId} for pgid:{pgid}, lastname:{lastname}, address:{address}, zip:{zip}, querytype:{querytype} and returned: MethodSuccess:{response.MethodSuccess}, Success:{response.Success}, , ReserveFundsSuccess:{response.ReserveFundsSuccess}, ErrorMsg:{(!string.IsNullOrEmpty(response.ErrorMsg) ? response.ErrorMsg : "-" )}");
            return response;
        }

        public UpdateDataAppendResponse UpdateDataAppendRequest(long tranId, string resultJsonPayload, bool successfulAppend, bool charge)
        {
            var response = new UpdateDataAppendResponse();
            response.MethodSuccess = true;
            
            try
            {
                using (SqlConnection conn = new SqlConnection(connString))
                {
                    conn.Open();
                    {
                        using (SqlCommand cmd = new SqlCommand("[SP_UPDATE_MATCH_REQUEST]", conn))
                        {
                            cmd.CommandType = CommandType.StoredProcedure;

                            /*
                                @TranId bigint,	
	                            @Result nvarchar,
	                            @SuccessfulMatch bit,                            
	                            @Charge bit,
	                            @Success bit = NULL OUTPUT
                            */

                            cmd.Parameters.AddWithValue("@TranId", tranId);
                            cmd.Parameters.AddWithValue("@Result", resultJsonPayload);
                            cmd.Parameters.AddWithValue("@SuccessfulMatch", successfulAppend);
                            cmd.Parameters.AddWithValue("@Charge", charge);
                            var successParam = cmd.Parameters.Add("@Success", SqlDbType.Bit);
                            successParam.Direction = ParameterDirection.Output;
                                                        
                            cmd.ExecuteNonQuery();
                            response.Success = DBNull.Value != successParam.Value && (bool)successParam.Value;                            
                        }
                    }
                }
            }
            catch (Exception e)
            {
                _logger.LogError($"UpdateDataAppendRequest for tranId:{tranId}, successfulAppend:{successfulAppend}, resultJsonPayload:{resultJsonPayload} failed with: {e}");
                response.Success = false;
                response.MethodSuccess = true;
            }

            _logger.LogInformation($"UpdateDataAppendRequest :: DB updated with resultset for tranId:{tranId}.");
            return response;
        }
    }
}