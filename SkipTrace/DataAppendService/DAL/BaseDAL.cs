﻿using System;
using System.Configuration;
using System.Data.SqlClient;
using Microsoft.Extensions.Logging;

namespace DataAppendService.DAL
{
    public class BaseDAL : IDisposable
    {
        protected ILogger _logger;
        public string connString = string.Empty;        

        public BaseDAL(ILogger logger)
        {
            _logger = logger;
            try { connString = ConfigurationManager.ConnectionStrings["DBConnection"].ConnectionString; }
            catch (Exception e)
            {
                _logger.LogError($"Can not find DBConnection in app.config: {e}");
            }
        }

        public bool TestConnection()
        {
            bool res = true;
            try
            {
                using (SqlConnection conn = new SqlConnection(connString))
                {
                    conn.Open();
                    _logger.LogInformation($"TestConnection OK");
                }
            }
            catch (Exception e)
            {
                _logger.LogError($"TestConnection failed: {e}");
                res = false;
            }

            return res;
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~BaseDAL() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}