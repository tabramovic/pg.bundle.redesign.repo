﻿using System;
using System.IO;
using System.Data.Common;

namespace DataAppendService.DAL
{
    public static class SqlExtensions
    {
        public static string GetSafeString(this DbDataReader reader, string colName)
        {
            return !reader.IsDBNull(reader.GetOrdinal(colName)) ? reader.GetString(reader.GetOrdinal(colName)) : string.Empty;
        }

        public static int GetSafeInt32(this DbDataReader reader, string colName)
        {
            return !reader.IsDBNull(reader.GetOrdinal(colName)) ? reader.GetInt32(reader.GetOrdinal(colName)) : 0;
        }

        public static long GetSafeInt64(this DbDataReader reader, string colName)
        {
            return !reader.IsDBNull(reader.GetOrdinal(colName)) ? reader.GetInt64(reader.GetOrdinal(colName)) : 0;
        }

        public static decimal GetSafeDecimal(this DbDataReader reader, string colName)
        {
            return !reader.IsDBNull(reader.GetOrdinal(colName)) ? reader.GetDecimal(reader.GetOrdinal(colName)) : 0;
        }

        public static DateTime GetSafeDateTime(this DbDataReader reader, string colName)
        {
            return !reader.IsDBNull(reader.GetOrdinal(colName)) ? reader.GetDateTime(reader.GetOrdinal(colName)) : DateTime.MinValue;
        }

        public static Guid GetSafeGuid(this DbDataReader reader, string colName)
        {
            return !reader.IsDBNull(reader.GetOrdinal(colName)) ? reader.GetGuid(reader.GetOrdinal(colName)) : Guid.Empty;
        }

        public static bool GetSafeBoolean(this DbDataReader reader, string colName)
        {
            return !reader.IsDBNull(reader.GetOrdinal(colName)) ? reader.GetBoolean(reader.GetOrdinal(colName)) : false;
        }

        public static Stream GetSafeStream(this DbDataReader reader, string colName)
        {
            return !reader.IsDBNull(reader.GetOrdinal(colName)) ? reader.GetStream(reader.GetOrdinal(colName)) : null;
        }

        public static byte[] GetSafeByteArray(this DbDataReader reader, string colName)
        {
            var ms = new MemoryStream();            
            var s = GetSafeStream(reader, colName);

            if (null == s)
                return null;
            
            s.CopyTo(ms);
            return ms.ToArray();            
        }
    }
}
