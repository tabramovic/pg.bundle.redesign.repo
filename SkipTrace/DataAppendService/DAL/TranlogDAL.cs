﻿using System;
using System.Collections.Generic;
using Microsoft.Extensions.Logging;
using DataAppendService.Models;
using System.Data.SqlClient;

namespace DataAppendService.DAL
{
    public class TranlogDAL : BaseDAL
    {        
        public TranlogDAL(ILogger logger) : base (logger)
        {
            
        }
        public List<Transaction> GetAllTransactionsFor(Guid pgid)
        {

            var transactions = new List<Transaction>();

            try
            {
                using (SqlConnection conn = new SqlConnection(connString))
                {
                    conn.Open();

                    using (SqlCommand cmd = new SqlCommand("SELECT [TID] ,[REQUEST_TS] ,[PGID] ,[LASTNAME] ,[ADDRESS], [ZIP], [QUERY_TYPE], [RESERVE_FUNDS_SUCCESS], [RESULT_TS] ,[SUCCESSFUL_MATCH] ,[CHARGED] FROM [TRANLOG] WHERE PGID = @pgid order by [REQUEST_TS] desc", conn))
                    {
                        cmd.Parameters.AddWithValue("@pgId", pgid);

                        string query = cmd.CommandText;
                        foreach (SqlParameter p in cmd.Parameters)
                            query = query.Replace(p.ParameterName, p.Value.ToString());

                        _logger.LogInformation($"GetAllTransactionsFor pgid:{pgid}, Query:{query}");

                        var dataReader = cmd.ExecuteReader();

                        while (dataReader.Read())
                        {
                            bool? reserveFundsSuccess = null;
                            if (dataReader.IsDBNull(dataReader.GetOrdinal("RESERVE_FUNDS_SUCCESS")))
                                reserveFundsSuccess = null;
                            else
                                reserveFundsSuccess = dataReader.GetSafeBoolean("RESERVE_FUNDS_SUCCESS");

                            DateTime? result_ts= null;
                            if (dataReader.IsDBNull(dataReader.GetOrdinal("RESULT_TS")))
                                result_ts = null;
                            else
                                result_ts = dataReader.GetSafeDateTime("RESULT_TS");

                            bool? successful_match = null;
                            if (dataReader.IsDBNull(dataReader.GetOrdinal("SUCCESSFUL_MATCH")))
                                successful_match = null;
                            else
                                successful_match = dataReader.GetSafeBoolean("SUCCESSFUL_MATCH");

                            bool? charged = null;
                            if (dataReader.IsDBNull(dataReader.GetOrdinal("CHARGED")))
                                charged = null;
                            else
                                charged = dataReader.GetSafeBoolean("CHARGED");

                            var t = new Transaction()
                            {
                                TID = dataReader.GetSafeInt64("TID"),
                                REQUEST_TS = dataReader.GetSafeDateTime("REQUEST_TS"),
                                PGID = dataReader.GetSafeGuid("PGID"),
                                LASTNAME = dataReader.GetSafeString("LASTNAME"),
                                ADDRESS = dataReader.GetSafeString("ADDRESS"),
                                ZIP = dataReader.GetSafeString("ZIP"),
                                QUERY_TYPE = dataReader.GetSafeString("QUERY_TYPE"),
                                RESERVE_FUNDS_SUCCESS = reserveFundsSuccess,
                                RESULT_TS = result_ts,
                                SUCCESSFUL_MATCH = successful_match,
                                CHARGED = charged
                            };

                            transactions.Add(t);
                        }

                        _logger.LogInformation($"GetAllTransactionsFor pgid:{pgid} response:{Environment.NewLine}Found: {transactions.Count} records.");
                    }
                }
            }
            catch (Exception exc)
            {
                _logger.LogError($"GetAllTransactionsFor for pgid:{pgid} failed with: {exc}");
                transactions = null;
            }

            return transactions;
        }
    }
}