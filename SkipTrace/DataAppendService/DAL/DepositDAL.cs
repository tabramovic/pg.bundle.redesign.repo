﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Extensions.Logging;
using DataAppendService.Models;

namespace DataAppendService.DAL
{
    public class DepositDAL : BaseDAL
    {
        public DepositDAL(ILogger logger) : base(logger)
        { }

        public bool InsertDepositAnnouncement(AnnounceDeposit ad)
        {
            var res = true;

            try
            {             
                using (SqlConnection conn = new SqlConnection(connString))
                {
                    conn.Open();                    
                    {
                        using (SqlCommand cmd =
                        new SqlCommand("insert into [Announcements] ([ID], [PGID], [Amount], [Created]) VALUES (@id, @pgid, @amount, @created)", conn))
                        {
                            cmd.Parameters.AddWithValue("@id", ad.Id);
                            cmd.Parameters.AddWithValue("@pgId", ad.PGID);
                            cmd.Parameters.AddWithValue("@amount", ad.Amount);
                            cmd.Parameters.AddWithValue("@created", ad.Created);
                            
                            string query = cmd.CommandText;
                            foreach (SqlParameter p in cmd.Parameters)
                                query = query.Replace(p.ParameterName, (null != p && null != p.Value ? p.Value.ToString() : "NOT_SET"));

                            _logger.LogInformation($"InsertDepositAnnouncement - INSERT :: {query}");

                            //rows number of record got inserted
                            int rows = cmd.ExecuteNonQuery();

                            _logger.LogInformation($"InsertDepositAnnouncement :: inserted {rows} records.");
                        }
                    }
                }
            }
            catch (Exception e)
            {
                _logger.LogError($"InsertDepositAnnouncement for {ad} failed with: {e}");
                res = false;
            }

            return res;
        }

        public Deposit InsertDepositFromAnnouncement(AnnounceDeposit ad)
        {            
            var createdDeposit = new Deposit();
            createdDeposit.AnnouncementId = ad.Id;

            try
            {
                using (SqlConnection conn = new SqlConnection(connString))
                {
                    conn.Open();
                    {
                        using (SqlCommand cmd =
                        new SqlCommand("UseDepositAnnouncement", conn))
                        {
                            cmd.CommandType = CommandType.StoredProcedure;

                            cmd.Parameters.AddWithValue("@newDepositId", createdDeposit.Id);
                            cmd.Parameters.AddWithValue("@announcementId", ad.Id);

                            Guid pgid = Guid.Empty;
                            var p1 = cmd.Parameters.AddWithValue("@pgId", pgid);
                            p1.Direction = ParameterDirection.Output;

                            long amount = 0;
                            var p2 = cmd.Parameters.AddWithValue("@amount", amount);
                            p2.Direction = ParameterDirection.Output;

                            DateTime? created = DateTime.MinValue;
                            var p3 = cmd.Parameters.AddWithValue("@created", created);
                            p3.Direction = ParameterDirection.Output;
                            
                            bool success = true;
                            var p5 = cmd.Parameters.AddWithValue("@success", success);
                            p5.Direction = ParameterDirection.Output;

                            string query = cmd.CommandText;
                            foreach (SqlParameter p in cmd.Parameters)
                                query += $" {p.ParameterName} - {(null != p && null != p.Value ? p.Value.ToString() : "NOT_SET")};";

                            _logger.LogInformation($"UseDepositAnnouncement - execute SPL :: {query}");

                            //rows number of record got inserted
                            var rows = cmd.ExecuteReader();

                            try { success = (bool)cmd.Parameters["@success"].Value; }
                            catch { success = false; }

                            if (success)
                            {
                                try { createdDeposit.PGID = (Guid)cmd.Parameters["@pgId"].Value; }
                                catch { createdDeposit.PGID = Guid.Empty; }

                                try { createdDeposit.Created = (DateTime)cmd.Parameters["@created"].Value; }
                                catch { createdDeposit.Created = null; }

                                try { createdDeposit.Amount = (long)cmd.Parameters["@amount"].Value; }
                                catch { createdDeposit.Amount = 0; }
                                
                            }
                            else
                            {
                                createdDeposit = null;
                            }

                            _logger.LogInformation($"InsertDepositAnnouncement :: inserted {rows} records.");
                        }
                    }
                }
            }
            catch (Exception e)
            {
                _logger.LogError($"InsertDepositAnnouncement for {ad} failed with: {e}");               
            }

            return createdDeposit;
        }


        public bool InsertDepositInsecure(Deposit d)
        {
            var res = true;

            try
            {
                using (SqlConnection conn = new SqlConnection(connString))
                {
                    conn.Open();
                    {
                        using (SqlCommand cmd =
                        new SqlCommand("insert into [Deposits] ([ID], [AnnouncementId], [PGID], [Amount], [Created]) VALUES (@id, @announcementId, @pgid, @amount, @created)", conn))
                        {
                            cmd.Parameters.AddWithValue("@id", d.Id);
                            cmd.Parameters.AddWithValue("@announcementId", DBNull.Value);
                            cmd.Parameters.AddWithValue("@pgId", d.PGID);
                            cmd.Parameters.AddWithValue("@amount", d.Amount);
                            cmd.Parameters.AddWithValue("@created", d.Created);

                            string query = cmd.CommandText;
                            foreach (SqlParameter p in cmd.Parameters)
                                query = query.Replace(p.ParameterName, (null != p && null != p.Value ? p.Value.ToString() : "NOT_SET"));

                            _logger.LogInformation($"InsertDepositInsecure - INSERT :: {query}");

                            //rows number of record got inserted
                            int rows = cmd.ExecuteNonQuery();

                            _logger.LogInformation($"InsertDepositAnnouncement :: inserted {rows} records.");
                        }
                    }
                }
            }
            catch (Exception e)
            {
                _logger.LogError($"InsertDepositAnnouncement for {d} failed with: {e}");
                res = false;
            }

            return res;
        }

        public List<Deposit> GetAllDepositsFor(Guid pgid)
        {

            List<Deposit> deposits = new List<Deposit>();

            try
            {
                using (SqlConnection conn = new SqlConnection(connString))
                {
                    conn.Open();

                    using (SqlCommand cmd = new SqlCommand("SELECT [ID],[AnnouncementId],[PGID],[Amount],[Created] FROM [Deposits] where PGID = @pgid order by [Created] desc", conn))
                    {
                        cmd.Parameters.AddWithValue("@pgId", pgid);

                        string query = cmd.CommandText;
                        foreach (SqlParameter p in cmd.Parameters)
                            query = query.Replace(p.ParameterName, p.Value.ToString());

                        _logger.LogInformation($"GetAllDepositsFor for :{query}");

                        var dataReader = cmd.ExecuteReader();

                        while (dataReader.Read())
                        {
                            Guid? announcementId = null;
                            if (dataReader.IsDBNull(dataReader.GetOrdinal("AnnouncementId")))
                                announcementId = null;
                            else
                                announcementId = dataReader.GetSafeGuid("AnnouncementId");
                            

                            var d = new Deposit()
                            {
                                Id = dataReader.GetSafeGuid("ID"),
                                AnnouncementId = announcementId,
                                PGID = dataReader.GetSafeGuid("PGID"),
                                Amount = dataReader.GetSafeInt64("Amount"),
                                Created = dataReader.GetSafeDateTime("Created")
                            };

                            deposits.Add(d);
                        }

                        _logger.LogInformation($"GetAllDepositsFor Response:{Environment.NewLine}Found: {deposits.Count} records.");
                    }
                }
            }
            catch (Exception exc)
            {
                _logger.LogError($"GetAllDepositsFor pgid:{pgid} failed: {exc}");
                deposits = null;
            }

            return deposits;
        }        
    }
}
