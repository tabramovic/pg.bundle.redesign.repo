﻿using System;
using System.Data.SqlClient;
using Microsoft.Extensions.Logging;
using DataAppendService.Models;


namespace DataAppendService.DAL
{
    public class WalletDAL : BaseDAL
    {
        public WalletDAL(ILogger logger) : base (logger)
        {

        }
        public Wallet GetWalletAmount(Guid pgid)
        {

            var wallet = new Wallet();

            try
            {
                using (SqlConnection conn = new SqlConnection(connString))
                {
                    conn.Open();

                    using (SqlCommand cmd = new SqlCommand("SELECT [Available], [Reserved] FROM [Wallet] WHERE [PGID] = @pgId", conn))
                    {
                        cmd.Parameters.AddWithValue("@pgId", pgid);

                        string query = cmd.CommandText;
                        foreach (SqlParameter p in cmd.Parameters)
                            query = query.Replace(p.ParameterName, p.Value.ToString());

                        _logger.LogInformation($"GetWalletAmount for :{query}");

                        var dataReader = cmd.ExecuteReader();

                        while (dataReader.Read())
                        {

                            if (dataReader.IsDBNull(dataReader.GetOrdinal("Available")))
                                wallet.Available = 0;
                            else
                                wallet.Available = dataReader.GetSafeInt32("Available");


                            if (dataReader.IsDBNull(dataReader.GetOrdinal("Reserved")))
                                wallet.Reserved = 0;
                            else
                                wallet.Reserved = dataReader.GetSafeInt32("Reserved");

                        }

                        _logger.LogInformation($"GetWalletAmount for pgid:{pgid} response: wallet.Available:{wallet.Available}, wallet.Reserved:{wallet.Reserved}.");
                    }
                }
            }
            catch (Exception exc)
            {
                _logger.LogError($"GetWalletAmount for pgid:{pgid} failed with: {exc}");
                wallet = null;
            }

            return wallet;
        }
    }
}