﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using System.Configuration;
using Microsoft.Extensions.Logging;
using DataAppendService.Lib.Utils;
using DataAppendService.Lib;

namespace DataAppendService.Providers.IDI
{
    public sealed class TokenManager : ITokenManager
    {
        public Guid Id { get; set; }

        private bool _isRefreshingToken = false;        
        private const int TokenRefreshPeriodInMS = 600_000;   //10 mins   

        private string _url;
        private string _clientId;
        private string _clientSecret;

        private object _getTokenPayload;

        private readonly HttpClient _httpClient;
        private readonly ILogger _logger;
        private string _token;

        public string Token => _token;
        public bool IsTokenAvailable => !string.IsNullOrEmpty(Token);
        
        public TokenManager(HttpClient httpClient, ILogger logger)
        {
            _httpClient = httpClient;
            _logger = logger;

            LoadFromConfig();

            _getTokenPayload = new { glba = "otheruse", dppa = "none" };

            _httpClient.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));            
            _httpClient.DefaultRequestHeaders.Add("authorization", GetAuthorization());

            Id = Guid.NewGuid();
        }

        public void StartRefreshingToken()
        {
            if (!_isRefreshingToken)
            {
                _logger.LogInformation("Started refreshing token procedure");
                _isRefreshingToken = true;
                Task.Factory.StartNew(RefreshToken, TaskCreationOptions.LongRunning);
            }
        }  
        
        private void LoadFromConfig()
        {
            _url = ConfigurationManager.AppSettings["skiptraceLoginUrl"];
            _clientId = ConfigurationManager.AppSettings["clientId"];
            _clientSecret =  ConfigurationManager.AppSettings["clientSecret"];
        }

        private async Task RefreshToken()
        {
            while (true)
            {
                try
                {                    
                    var response = await _httpClient.PostAsync(_url, _getTokenPayload.AsJSON());

                    response.EnsureSuccessStatusCode();
                    var res = await response.Content.ReadAsStringAsync();

                    _token = res;
                    _logger.LogInformation($"Retrieved token: {_token.Substring(0, 25)}...");
                }
                catch (Exception e)
                {
                    var innerExc = e.InnerException;
                    _logger.LogError($"Refresh Token failed with: {e}{Environment.NewLine}InnerExc: {(null != innerExc ? $"{innerExc}" : Consts.NotAvailable)}");
                }

                await Task.Delay(TokenRefreshPeriodInMS);
            }
        }
        
        private string GetAuthorization()
        {
            var cis = $"{_clientId}:{_clientSecret}";
            var base64EncodedCis = Base64Helpers.Encode(cis).Replace("\n", "");
            var authorization = $"Basic {base64EncodedCis}";

            return authorization;
        }        
    }
}