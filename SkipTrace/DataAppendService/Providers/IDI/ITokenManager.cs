﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataAppendService.Providers.IDI
{
    public interface ITokenManager
    {
        string Token { get; }
        bool IsTokenAvailable { get; }

        void StartRefreshingToken();
    }
}