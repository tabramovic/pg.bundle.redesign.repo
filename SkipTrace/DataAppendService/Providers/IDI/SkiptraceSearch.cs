﻿using System;
using System.Configuration;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using DataAppendService.Lib.Utils;
using Newtonsoft.Json;
using DataAppendService.Lib.Models.IDI;

namespace DataAppendService.Providers.IDI
{
    public class SkiptraceSearch : ISkiptraceSearch
    {
        private string _searchUrl = "https://api-test.idicore.com/search";

        private readonly string _token;
        private readonly HttpClient _httpClient;
        private readonly ILogger _logger;

        public SkiptraceSearch(string token, HttpClient httpClient, ILogger logger)
        {
            _token = token;
            _httpClient = httpClient;
            _logger = logger;

            _searchUrl = ConfigurationManager.AppSettings["skiptraceSearchUrl"];
        }
        public async Task<SkipTraceResponse> SkipTraceAsync(SkipTraceRequest req)
        {
            try
            {
                _httpClient.DefaultRequestHeaders?.Clear();
                _httpClient.DefaultRequestHeaders.Add("authorization", _token ?? string.Empty);

                var response = await _httpClient.PostAsync(_searchUrl, req.AsJSON());
                response.EnsureSuccessStatusCode();

                var res = await response.Content.ReadAsStringAsync();
                _logger.LogDebug($"DataProvider's raw response:{res}"); ;

                var respObj = JsonConvert.DeserializeObject<SkipTraceResponse>(res);                
                return respObj;
            }
            catch (Exception e)
            {
                _logger.LogError($"SkipTraceAsync failed with:{e},{Environment.NewLine}InnerExc:{e.InnerException}");                
                return new SkipTraceResponse();
            }
        }
    }
}
