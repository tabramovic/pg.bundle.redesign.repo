﻿using DataAppendService.Lib.Models.IDI;
using System.Threading.Tasks;

namespace DataAppendService.Providers.IDI
{
    public interface ISkiptraceSearch
    {
        Task<SkipTraceResponse> SkipTraceAsync(SkipTraceRequest req);
    }
}