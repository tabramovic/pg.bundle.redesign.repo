﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataAppendService.Helpers
{
    public static class Globals
    {
        public static string APIKey;
        public static string APIUrl;
        public static string DataZappAPIUrl;
        public static bool IsHashVerificationEnabled;
    }
}