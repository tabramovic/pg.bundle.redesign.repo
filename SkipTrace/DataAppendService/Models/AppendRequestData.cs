﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace DataAppendService.Models
{
    public enum AppendRequestType
    {
        eLandline = 0,
        eCellPhone = 1,
        eEmail = 2,        
        NOT_SET = 3,
    }

    public partial class AppendRequestData
    {
        [JsonProperty("dataSource")]
        public string DataSource { get; set; }

        [JsonProperty("appendSpecs")]
        public AppendSpec[] AppendSpecs { get; set; }

        [JsonProperty("sourceOutputFields")]
        public SourceOutputField[] SourceOutputFields { get; set; }

        public AppendRequestData(string lastName, string address, string zip, AppendRequestType requestType)
        {
            string sourceOutputFieldName = string.Empty;
            switch (requestType)
            {
                case AppendRequestType.eLandline:
                    DataSource = "ConsumerPlus";
                    sourceOutputFieldName = "Phone";
                    break;

                case AppendRequestType.eCellPhone:
                    DataSource = "GcpWireless";
                    sourceOutputFieldName = "Phone";
                    break;

                case AppendRequestType.eEmail:
                    DataSource = "ConsumerEmail";
                    sourceOutputFieldName = "Email";
                    break;

                default:
                    break;
            }


            var lastNameAppendSpec = new AppendSpec { Field = "Last", Value = lastName };
            var addressAppendSpec = new AppendSpec { Field = "Address", Value = address, Limit = 10 };
            var zipAppendSpec = new AppendSpec { Field = "ZIP", Value = zip };

            
            var sourceOutputField = new SourceOutputField() { Field = sourceOutputFieldName, MaxAppends = 1 };

            AppendSpecs = new[] { lastNameAppendSpec, addressAppendSpec, zipAppendSpec };
            SourceOutputFields = new[] { sourceOutputField };
        }

        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Converter.Settings);
        }
    }
}