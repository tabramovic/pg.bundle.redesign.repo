﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataAppendService.Models
{
    public class Deposit
    {
        public Guid Id { get; internal set; }
        public Guid? AnnouncementId { get; set; }
        public Guid PGID { get; set; }
        public long Amount { get; set; }
        public DateTime? Created { get; set; }

        public Deposit()
        {
            Id = Guid.NewGuid();
        }
    }
}