﻿using System.Linq;
using Newtonsoft.Json;

namespace DataAppendService.Models
{   
    public partial class AppendResponsePhoneData
    {
        [JsonProperty("status")]
        public long Status { get; set; }

        [JsonProperty("appendID")]
        public string AppendId { get; set; }

        [JsonProperty("appendedRecords")]
        public long AppendedRecords { get; set; }

        [JsonProperty("appendedFields")]
        public AppendedFields AppendedFields { get; set; }

        [JsonProperty("results")]
        public Result[] Results { get; set; }

        [JsonProperty("message")]
        public string Message{ get; set; }

        public override string ToString()
        {
            string[] ResultsAsStringArray = null != Results ? Results.Select(r => r.Phone).ToArray() : new string[0];
            return $"Status:{Status}, Message:{Message} AppendId:{AppendId}, nr. of AppendedRecords:{AppendedRecords}, AppendedFields:{AppendedFields}, Results:{string.Join(";", ResultsAsStringArray)}";
        }
    }

    public partial class AppendResponseEmailData
    {
        [JsonProperty("status")]
        public long Status { get; set; }

        [JsonProperty("appendID")]
        public string AppendId { get; set; }

        [JsonProperty("appendedRecords")]
        public long AppendedRecords { get; set; }

        [JsonProperty("appendedFields")]
        public AppendedEmailFields AppendedFields { get; set; }

        [JsonProperty("results")]
        public EmailResult[] Results { get; set; }

        [JsonProperty("message")]
        public string Message { get; set; }

        public override string ToString()
        {
            string[] ResultsAsStringArray = null != Results ? Results.Select(r => r.Email).ToArray() : new string[0];
            return $"Status:{Status}, Message:{Message} AppendId:{AppendId}, nr. of AppendedRecords:{AppendedRecords}, AppendedFields:{AppendedFields}, Results:{string.Join(";", ResultsAsStringArray)}";
        }
    }

    public partial class AppendedEmailFields
    {
        [JsonProperty("EMAIL")]
        public string Email { get; set; }

        public override string ToString()
        {
            return $"Email:{Email}";
        }
    }

    public partial class EmailResult
    {
        [JsonProperty("EMAIL")]
        public string Email { get; set; }

        public override string ToString()
        {
            return $"Email:{Email}";
        }
    }

    public partial class AppendedFields
    {
        [JsonProperty("PHONE")]
        public long Phone { get; set; }

        public override string ToString()
        {
            return $"Phone:{Phone}";
        }
    }

    public partial class Result
    {
        [JsonProperty("PHONE")]
        public string Phone { get; set; }

        public override string ToString()
        {
            return $"Phone:{Phone}";
        }
    }
}