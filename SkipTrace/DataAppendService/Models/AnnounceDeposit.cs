﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace DataAppendService.Models
{
    public class AnnounceDeposit
    {
        public Guid Id { get; set; }
        public Guid PGID { get; set; }
        public long Amount { get; set; }
        public DateTime Created { get; private set; }
        public DateTime? Used { get; set; }
        
        [IgnoreDataMember]
        public string Hash { get; set; }


        public AnnounceDeposit()
        {                        
            Created = DateTime.Now;
        }

        public bool HasValidHash()
        {
            return true;
        }


    }
}