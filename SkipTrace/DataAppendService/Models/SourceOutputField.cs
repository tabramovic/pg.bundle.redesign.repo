﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using DataAppendService.Helpers;


namespace DataAppendService.Models
{
    public partial class SourceOutputField
    {
        [JsonProperty("field")]
        public string Field { get; set; }

        [JsonProperty("maxAppends")]
        [JsonConverter(typeof(ParseStringConverter))]
        public long MaxAppends { get; set; }
    }
}