﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataAppendService.Models
{
    public class Wallet
    {
        public int Available { get; set; }
        public int Reserved { get; set; }
    }
}