﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataAppendService.Models
{
    public class Transaction
    {        
        public long TID { get; set; }
        public DateTime REQUEST_TS { get; set; }
        public Guid PGID { get; set; }
        public string LASTNAME { get; set; }
        public string ADDRESS { get; set; }
        public string ZIP { get; set; }
        public string QUERY_TYPE { get; set; }
        public bool? RESERVE_FUNDS_SUCCESS { get; set; } = null;
        public DateTime? RESULT_TS { get; set; } = null;
        public bool? SUCCESSFUL_MATCH { get; set; } = null;
        public bool? CHARGED { get; set; } = null;
    }
}