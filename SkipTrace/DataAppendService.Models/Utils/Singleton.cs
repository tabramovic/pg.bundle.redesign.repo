﻿using System;

namespace DataAppendService.Lib.Utils
{
    public class Singleton<T> where T : class, new()
    {
        private static readonly Lazy<T> lazy = new Lazy<T>(() => new T());

        public static T Instance { get { return lazy.Value; } }
        protected Singleton()
        {

        }
    }
}