﻿using System.Text;
using System.IO;
using System.IO.Compression;
using System;

namespace DataAppendService.Lib.Utils
{
    public static class GZipCompressionHelper
    {
        public static string CompressString(string str)
        {
            using (var ms = new MemoryStream(Encoding.UTF8.GetBytes(str)))
            {
                using (var ds = new DeflateStream(ms, CompressionMode.Compress))
                using (var writer = new BinaryWriter(ds, Encoding.UTF8))
                {
                    writer.Write(str);
                }
                ms.Flush();
                return Convert.ToBase64String(ms.ToArray());
            }                                    
        }


        public static string DeCompressString(string str)
        {
            using (var srcMs = new MemoryStream(Encoding.UTF8.GetBytes(str)))
            {
                using (var destMs = new MemoryStream())
                {
                    using (var deCompressedStream = new DeflateStream(destMs, CompressionMode.Decompress))
                    {
                        srcMs.CopyTo(deCompressedStream);
                        return Encoding.UTF8.GetString(destMs.ToArray());
                    }
                }
            }
        }
    }
}
