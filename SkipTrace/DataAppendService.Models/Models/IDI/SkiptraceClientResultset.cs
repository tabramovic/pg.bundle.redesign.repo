﻿using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAppendService.Lib.Models.IDI
{
    public class SkiptraceClientResultset
    {
        public class TransferHistory
        {
            public string TransferDate { get; set; }
            public string Seller { get; set; }
            public string Buyer { get; set; }
            public string TransferType { get; set; }
            public string LoanValue { get; set; }            
            public string LenderFirstName { get; set; }
            public string InterestType { get; set; }

        }
        public class Property
        {        
            public string Address { get; set; }           
            public List<string> Detail { get; set; }
            public List<List<string>> Transfers { get; set; }

            public bool HasProperty => !string.IsNullOrWhiteSpace(Address);
        }
        public class Address
        {
            public string FullStreet { get; set; }            
            public string City { get; set; }
            public string State { get; set; }
            public string ZIP { get; set; }
            //public string County { get; set; }

            public string FirstSeen { get; set; }
            public string LastSeen { get; set; }

            public bool IsRealAddress =>             
             !string.IsNullOrEmpty(City)
            || !string.IsNullOrEmpty(State)
            || !string.IsNullOrEmpty(ZIP);            

            public override string ToString()
            {
                return $"FullStreet:{FullStreet}, City:{City}, State:{State}, ZIP:{ZIP}, FirstSeen:{FirstSeen}, LastSeen:{LastSeen}";
            }
        }

        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string FullName { get; set; }
        public string Age { get; set; }

        public string Alias { get; set; }   //Full Name is the Rank 0 name.  Alias is the Rank 1 name.

        public List<List<string>> Addresses { get; set; }

        public List<string> MobilePhones { get; set; }
        public List<string> ResidentialPhones { get; set; }
        public List<string> Emails { get; set; }

        public List<Property> Properties { get; set; }

        public List<List<string>> RelativesInfo { get; set; }

        public bool HasName =>
               !string.IsNullOrEmpty(FirstName)
            || !string.IsNullOrEmpty(MiddleName)
            || !string.IsNullOrEmpty(LastName)
            || !string.IsNullOrEmpty(FullName);

        public bool IsBillable => HasAnyMobilePhone || HasAnyResidentialPhone || HasAnyEmail;
        public bool IsSkipTraced => HasAnyAddress || HasAnyMobilePhone || HasAnyResidentialPhone || HasAnyEmail || HasAnyProperties || HasAnyRelativesInfo;
        public bool HasAnyAddress => 
            null != Addresses 
            && Addresses.Any() 
            && Addresses.Any(addressList => HasFilledStringList(addressList));                
        
        public bool HasAnyMobilePhone => null != MobilePhones && MobilePhones.Any() && MobilePhones.Any(mp => true == !string.IsNullOrWhiteSpace(mp));
        public bool HasAnyResidentialPhone => null != ResidentialPhones && ResidentialPhones.Any() && ResidentialPhones.Any(rp => true == !string.IsNullOrWhiteSpace(rp));
        public bool HasAnyEmail => null != Emails && Emails.Any() && Emails.Any(e => true == !string.IsNullOrWhiteSpace(e));
        public bool HasAnyProperties => null != Properties && Properties.Any() && Properties.Any(p => p.HasProperty);
        public bool HasAnyRelativesInfo => null != RelativesInfo && RelativesInfo.Any() && RelativesInfo.Any(HasFilledStringList);

        public bool HasFilledStringList(List<string> list) => list.Any(el => !string.IsNullOrWhiteSpace(el));

        public override string ToString()
        {
            var sb = new StringBuilder();
            if (HasName)
            {
                sb.Append("First Name:").Append(FirstName ?? "-");

                if (!string.IsNullOrEmpty(MiddleName))
                    sb.Append(" Middle Name:").Append(MiddleName);

                sb.Append(" LastName:").Append(LastName ?? "-");
            }
            else
                sb.Append("No Name Found!");

            sb.Append("; ");

            if (HasAnyAddress)
                Addresses.ForEach(addr => sb.AppendLine(addr.ToString()));                
            else
                sb.Append("No Address Found!");

            if (HasAnyMobilePhone)
            {
                sb.AppendLine("Mobile Phones:");
                MobilePhones.ForEach(mobile => sb.AppendLine($"\t{mobile}"));
            }

            if (HasAnyResidentialPhone)
            {
                sb.AppendLine("Residential Phones:");
                ResidentialPhones.ForEach(residentialPhone => sb.AppendLine($"\t{residentialPhone}"));
            }
            sb.Append("; ");

            if (HasAnyEmail)
            {
                sb.AppendLine("Emails:");
                Emails.ForEach(email => sb.AppendLine($"\t{email}"));
            }
            
            return sb.ToString();
        }
    }
}