﻿using System;

namespace DataAppendService.Lib.Models.IDI
{
    public class ClientRequest
    {
        public Guid PGID { get; set; }
        public string RequestType { get; set; } //EM (EMail), LL (LandLine), CP (CellPhone) //null for ALL
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZIP { get; set; }
        public string Hash { get; set; }
        
        public bool IsValid()
        {
            var dataAsString = $"{Address}{ZIP}";
            var encrData = Utils.Crypto.EncryptString(dataAsString);
                        
            return Hash.Equals(encrData);            
        }

        public override string ToString()
        {
            return $"FirstName:{FirstName}, LastName:{LastName}, Address:{Address}, City:{City}, State:{State}, ZIP:{ZIP}";
        }
    }
}