﻿using System.Collections.Generic;

namespace DataAppendService.Lib.Models.IDI
{       
    public class SkipTraceRequest
    {
        public string lastName { get; set; }
        public string firstName { get; set; }
        public string address { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string zip { get; set; }
        public string referenceId { get; set; }
        public List<string> fields { get; set; }

        public override string ToString()
        {
            return $"referenceId:{referenceId} firstName:{firstName}, lastName:{lastName}, address:{address}, city:{city}, state:{state}, zip:{zip}";
        }
    }
}
