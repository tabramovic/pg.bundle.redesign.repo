﻿using System.Collections.Generic;
using System.Linq;

namespace DataAppendService.Lib.Models.IDI
{
    public static class PropertyExtensions
    {
        public static SkiptraceClientResultset.Property AsResultSetProperty (this DataAppendService.Lib.Models.IDI.Property property)
        {
            if (null == property)
                return new SkiptraceClientResultset.Property();

            var prop = new SkiptraceClientResultset.Property
            {       
                Address = property.Address.Data,                
                Detail = new List<string>() { 
                    !string.IsNullOrEmpty (property.Detail?.County) ? property.Detail.County : Consts.NotAvailable,
                    !string.IsNullOrEmpty (property.Detail?.UseCode) ? property.Detail.UseCode : Consts.NotAvailable,
                    !string.IsNullOrEmpty (property.Detail?.ParcelNumber) ? property.Detail.ParcelNumber : Consts.NotAvailable,
                    !string.IsNullOrEmpty (property.Detail?.LotSqft) ? property.Detail.LotSqft : Consts.NotAvailable,
                    !string.IsNullOrEmpty (property.Detail?.BuildingSqft) ? property.Detail.BuildingSqft : Consts.NotAvailable,
                    !string.IsNullOrEmpty (property.Detail?.YearBuilt) ? property.Detail.YearBuilt : Consts.NotAvailable,
                    !string.IsNullOrEmpty (property.Detail?.Bedrooms) ? property.Detail.Bedrooms : Consts.NotAvailable,
                    !string.IsNullOrEmpty (property.Detail?.Bathrooms) ? property.Detail.Bathrooms : Consts.NotAvailable,                    
                },
                Transfers = property.History.Select(h => h.AsTransferHistory()).Take(3).ToList()
            };

            return prop;
        }

        public static List<string> AsTransferHistory(this Lib.Models.IDI.History history)
        {
            var res = new List<string>();
            if (null == history)
                return res;

            var th = new SkiptraceClientResultset.TransferHistory
            {
                TransferDate = history.Detail?.TransferDate?.Data,
                Buyer = (null != history.Buyer && history.Buyer.Any()) ? history.Buyer[0]?.Name : string.Empty,
                Seller = (null != history.Seller && history.Seller.Any()) ? history.Seller[0]?.Name : string.Empty,
                TransferType = history.Detail?.TransferType,                
            };

            res.Add(th.TransferDate);
            res.Add(th.Seller);
            res.Add(th.Buyer);
            
            res.Add(th.TransferType);

            if (null != history.Loan && history.Loan.Any())
            {
                th.LoanValue = history.Loan[0].LoanValue;
                th.LenderFirstName = history.Loan[0].LenderFirstName;
                th.InterestType = history.Loan[0].longerestRateType;

                res.Add(!string.IsNullOrEmpty(th.LoanValue) ? th.LoanValue : Consts.NotAvailable);
                res.Add(!string.IsNullOrEmpty(th.LenderFirstName) ? th.LenderFirstName : Consts.NotAvailable);
                res.Add(!string.IsNullOrEmpty(th.InterestType) ? th.InterestType : Consts.NotAvailable);                                
            }
            else
            {
                res.Add(Consts.NotAvailable);
                res.Add(Consts.NotAvailable);
                res.Add(Consts.NotAvailable);
            }

            return res;
        }
    }
}