﻿using System.Collections.Generic;

namespace DataAppendService.Lib.Models.IDI
{
    public static class SkipTraceResponseExtensions
    {
        public static List<string> AsAddress(this AddressElement addressElement)
        {
            return new List<string> (){ 
                addressElement.Complete,
                
                addressElement.City,
                addressElement.State,
                addressElement.Zip,
                
                !string.IsNullOrEmpty(addressElement.Meta?.FirstSeen) ? addressElement.Meta.FirstSeen : Consts.NotAvailable,
                !string.IsNullOrEmpty(addressElement.Meta?.LastSeen) ? addressElement.Meta.LastSeen : Consts.NotAvailable,
            };
        }
    }
}