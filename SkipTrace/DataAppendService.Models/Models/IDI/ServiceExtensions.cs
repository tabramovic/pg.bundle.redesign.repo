﻿using System.Collections.Generic;
using System.Linq;

namespace DataAppendService.Lib.Models.IDI
{
    public static class ServiceExtensions
    {
        public static SkipTraceRequest AsSkiptraceRequest(this ClientRequest cr, long tranId)
        {
            var skiptraceRequest = new SkipTraceRequest
            {
                firstName = cr.FirstName,
                lastName = cr.LastName,
                address = cr.Address,
                city = cr.City,
                state = cr.State,
                zip = cr.ZIP,                
                fields = new System.Collections.Generic.List<string>() { "name", "phone", "address", "email", "dob", "property", "ip", "lien", "judgment", "relationship" },
                referenceId = tranId.ToString()
            };

            return skiptraceRequest;
        }

        public static bool CheckForMatchingReferenceIds(this SkipTraceResponse skiptraceResp, SkipTraceRequest skiptraceReq)
        {
            return skiptraceResp.ReferenceId == skiptraceReq.referenceId;
        }

        public static SkiptraceClientResultset AsSkiptraceClientResultset(this SkipTraceResponse skiptraceResponse)
        {
            var cliRes = new SkiptraceClientResultset();

            if (null == skiptraceResponse?.Result || !skiptraceResponse.Result.Any())
                return cliRes;

            var names = skiptraceResponse.Result[0].Name;
            if (null != names && names.Any())
            {
                var mostRelevantName = names[0];

                cliRes.FirstName = mostRelevantName.First;
                cliRes.MiddleName = mostRelevantName.Middle;
                cliRes.LastName = mostRelevantName.Last;
                cliRes.FullName = mostRelevantName.Data;

                if (names.Count > 1)
                    cliRes.Alias = names[1].Data;
            }

            var addresses = skiptraceResponse.Result[0].Address;
            if (null != addresses && addresses.Any())            
                cliRes.Addresses = addresses
                                        .OrderBy(addr => addr.Rank)
                                        .Take(3)
                                        .Select(addr => addr.AsAddress())
                                        .ToList();                                
            

            var emails = skiptraceResponse.Result[0].Email;
            if (null != emails && emails.Any())
            {
                var mostRelevantEmail = emails[0];

                cliRes.Emails = emails
                                    .OrderBy(email => email.Meta.Rank)
                                    .Take(3)
                                    .Select(email => email.Data)
                                    .ToList();
            }

            var phones = skiptraceResponse.Result[0].Phone;
            if (null != phones && phones.Any())
            {
                cliRes.MobilePhones = phones
                                        .Where(p => null != p.Type && "Mobile" == p.Type)
                                        .OrderBy(p => p.Meta.Rank)
                                        .Take(3)
                                        .Select(p => p.Number)
                                        .ToList();

                cliRes.ResidentialPhones = phones
                                            .Where(p => null != p.Type && "Residential" == p.Type)
                                            .OrderBy(p => p.Meta.Rank)
                                            .Take(2)
                                            .Select(p => p.Number)
                                            .ToList();
            }

            cliRes.Age = (null != skiptraceResponse.Result[0].Dob && skiptraceResponse.Result[0].Dob.Any()) ? skiptraceResponse.Result[0].Dob[0].Age : Consts.NotAvailable;

            cliRes.Properties = skiptraceResponse.Result[0].Property.Select(p => p.AsResultSetProperty()).Take(4).ToList();

            if (null != skiptraceResponse.Result[0].Relationship)
                cliRes.RelativesInfo = skiptraceResponse.Result[0].Relationship.Select(r => {

                    var rank = null != r.Meta ? r.Meta.Rank : 0;
                    var firstName = !string.IsNullOrEmpty(r.Name?.First) ? r.Name.First : Consts.NotAvailable;
                    var lastName = !string.IsNullOrEmpty(r.Name?.Last) ? r.Name.Last : Consts.NotAvailable;
                    var age = null != r.Dob ? r.Dob.Age.ToString() : Consts.NotAvailable;
                    var phone1 = null != r.Phone && r.Phone.Count > 0 ? r.Phone[0].Number : Consts.NotAvailable;
                    var phone2 = null != r.Phone && r.Phone.Count > 1 ? r.Phone[1].Number : Consts.NotAvailable;
                    var phone3 = null != r.Phone && r.Phone.Count > 2 ? r.Phone[2].Number : Consts.NotAvailable;

                    var res = new List<string>() { rank.ToString(), firstName, lastName, age, phone1, phone2, phone3 };
                    return res;
                })
                .Take(10)
                .ToList();
            
            return cliRes;
        }                  
    }
}