﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace DataAppendService.Lib.Models.IDI
{
    public partial class SkipTraceResponse 
    {
            public List<Result> Result { get; set; }
            public List<string> Fields { get; set; }
            public List<object> IncludeWeaklyMatched { get; set; }
            public Guid Id { get; set; }
            public Query Query { get; set; }
            public User User { get; set; }
            public string ReferenceId { get; set; }
            public string IdiCoreVersion { get; set; }

            public bool HasMatched =>
                                null != Result
                                && Result.Count > 0;

            public string ToDBFormattedString()
            {
                return $"Ref.Id:{ReferenceId}, Matched:{HasMatched}";
            }

            public override string ToString()
            {
                return $"ReferenceId:{ReferenceId}, HasMatched:{HasMatched}, Query:{Query}{Environment.NewLine}\t{ResultsAsString()}";
            }

            private string ResultsAsString()
            {
                var sb = new StringBuilder();

                sb.Append("ResultSet length: ").Append(Result?.Count ?? 0).Append(Environment.NewLine);

                var cnt = 1;
                if (null != Result)
                    Result.ForEach(r =>
                    {
                        sb
                        .Append("\t\t").Append(cnt).Append(".:")
                        .Append(Environment.NewLine)
                        .Append("\t\t\tNames count: ")
                        .Append(r.Name?.Count ?? 0)
                        .Append(Environment.NewLine)

                        .Append("\t\t\tAddress count: ")
                        .Append(r.Address?.Count ?? 0)
                        .Append(Environment.NewLine)

                        .Append("\t\t\tLandline count: ")
                        .Append(r.Business?.Count ?? 0)
                        .Append(Environment.NewLine)

                        .Append("\t\t\tPhone count: ")
                        .Append(r.Phone?.Count ?? 0)
                        .Append(Environment.NewLine)

                        .Append("\t\t\tEmail count: ")
                        .Append(r.Email?.Count ?? 0)
                        .Append(Environment.NewLine);

                        cnt++;
                    }
                    );

                return sb.ToString();
            }
    }

    public partial class Query
        {
            public List<object> Pidlist { get; set; }
            public List<object> NaicsCodes { get; set; }
            public List<object> Epoch { get; set; }
            public List<object> Addresses { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string Address { get; set; }
            public string City { get; set; }
            public string Zip { get; set; }
            public string State { get; set; }
        }

        public partial class Result
        {
            public List<object> Ssn { get; set; }
            public List<ResultName> Name { get; set; }
            public List<DobElement> Dob { get; set; }
            public List<AddressElement> Address { get; set; }
            public List<ResultPhone> Phone { get; set; }
            public List<object> Bankruptcy { get; set; }
            public List<object> Death { get; set; }
            public List<object> Employment { get; set; }
            public List<Property> Property { get; set; }
            public List<Relationship> Relationship { get; set; }
            public List<Email> Email { get; set; }
            public List<Email> Ip { get; set; }
            public List<object> Criminal { get; set; }
            public List<object> MotorVehicle { get; set; }
            public List<object> Judgment { get; set; }
            public List<object> Lien { get; set; }
            public List<object> Aircraft { get; set; }
            public List<object> Social { get; set; }
            public List<object> Relatives { get; set; }
            public List<object> Associates { get; set; }
            public List<object> AddressCities { get; set; }
            public List<object> AddressCounties { get; set; }
            public List<object> Foreclosure { get; set; }
            public List<object> Business { get; set; }
            public List<object> RelationshipDetail { get; set; }
            public List<object> Professional { get; set; }
            public string Pid { get; set; }
            public string MatchScore { get; set; }
            public Dictionary<string, string> SortScore { get; set; }
        }

        public partial class AddressElement
        {
            public AddressMeta Meta { get; set; }
            public string Data { get; set; }
            public string StreetNumber { get; set; }
            public string Street { get; set; }
            public string StreetSuffix { get; set; }
            public string City { get; set; }
            public string State { get; set; }
            public string Zip { get; set; }
            public string Zip4 { get; set; }
            public string DateRange { get; set; }
            public string Complete { get; set; }
            public string IsCass { get; set; }
            public string County { get; set; }
            public long Rank { get; set; }
            public string Latitude { get; set; }
            public string Longitude { get; set; }
            public string Ownership { get; set; }
            public string TopPoBox { get; set; }
            public string Predir { get; set; }
            public string AptNum { get; set; }
            public string AptName { get; set; }
        }

        public partial class AddressMeta
        {
            public List<object> Sources { get; set; }
            public string FirstSeen { get; set; }
            public string LastSeen { get; set; }
            public string Count { get; set; }
            public long Rank { get; set; }
            public string Score { get; set; }
        }

        public partial class DobElement
        {
            public AddressMeta Meta { get; set; }
            public Date Date { get; set; }
            public string Age { get; set; }
        }

        public partial class Date
        {
            public string Data { get; set; }
            public long Sortable { get; set; }
        }

        public partial class Email
        {
            public AddressMeta Meta { get; set; }
            public string Data { get; set; }
        }

        public partial class ResultName
        {
            public List<object> Pidlist { get; set; }
            public AddressMeta Meta { get; set; }
            public string Data { get; set; }
            public string First { get; set; }
            public string Last { get; set; }
            public string Middle { get; set; }
        }

        public partial class ResultPhone
        {
            public AddressMeta Meta { get; set; }
            public string Type { get; set; }
            public string Number { get; set; }
            public string Business { get; set; }
            public string HighFrequency { get; set; }
            public string Fake { get; set; }
            public string Disconnected { get; set; }
            public string ProviderName { get; set; }
        }

        public partial class Property
        {
            public List<Owner> Owner { get; set; }
            public List<History> History { get; set; }
            public List<object> Foreclosure { get; set; }
            public PropertyMeta Meta { get; set; }
            public string Eid { get; set; }
            public PropertyDetail Detail { get; set; }
            public Assessment Assessment { get; set; }
            public MailingAddressClass Address { get; set; }
            public MailingAddressClass MailingAddress { get; set; }
        }

        public partial class MailingAddressClass
        {
            public AddressMeta Meta { get; set; }
            public string Data { get; set; }
            public string StreetNumber { get; set; }
            public string Street { get; set; }
            public string StreetSuffix { get; set; }
            public string City { get; set; }
            public string State { get; set; }
            public string Zip { get; set; }
            public string Zip4 { get; set; }
            public string DateRange { get; set; }
            public string Complete { get; set; }
            public string County { get; set; }
        }

        public partial class Assessment
        {
            public string TaxYear { get; set; }
            public string AssessedValue { get; set; }
            public string MarketValue { get; set; }
            public string LandValue { get; set; }
            public string ImprovementValue { get; set; }
            public string TotalTax { get; set; }
            [JsonIgnore]
            public Date Date { get; set; }
            public string AssessorYear { get; set; }
        }

        public partial class PropertyDetail
        {
            public string State { get; set; }
            public string County { get; set; }
            public string ParcelNumber { get; set; }
            public string UseCode { get; set; }
            public string LotSqft { get; set; }
            public string BuildingSqft { get; set; }
            public string YearBuilt { get; set; }
            public string Bedrooms { get; set; }
            public string Bathrooms { get; set; }
            public string Municipality { get; set; }
            public string MunicipalityCode { get; set; }
        }

        public partial class History
        {
            public List<Owner> Buyer { get; set; }
            public List<Owner> Seller { get; set; }
            public List<Loan> Loan { get; set; }
            public HistoryDetail Detail { get; set; }
            public bool IsCurrentOwner { get; set; }
        }

        public partial class Owner
        {
            public List<PersonNameElement> PersonName { get; set; }
            public List<object> BusinessName { get; set; }
            public List<string> Pidlist { get; set; }
            public List<Person> People { get; set; }
            public List<object> Businesses { get; set; }
            public string Name { get; set; }
        }

        public partial class Person
        {
            public List<string> Pidlist { get; set; }
            public PersonNameElement Name { get; set; }
        }

        public partial class PersonNameElement
        {
            public List<string> Pidlist { get; set; }
            public PersonNameMeta Meta { get; set; }
            public string First { get; set; }
            public string Last { get; set; }
            public string Middle { get; set; }
        }

        public partial class PersonNameMeta
        {
            public List<object> Sources { get; set; }
        }

        public partial class HistoryDetail
        {
            public Date ReceiptDate { get; set; }
            public Date TransferDate { get; set; }
            public string TransferType { get; set; }
            public string DeedType { get; set; }
            public string Quitclaim { get; set; }
            public string ArmsLength { get; set; }
            public string DocNumber { get; set; }
            public string DocNumberFormat { get; set; }
            public string SalesPrice { get; set; }
            public string Distress { get; set; }
        }

        public partial class Loan
        {
            public string DocNumber { get; set; }
            public string LoanValue { get; set; }
            public string LenderFirstName { get; set; }
            public string LenderType { get; set; }
            public string longerestRateType { get; set; }
            public string LoanType { get; set; }
        }

        public partial class PropertyMeta
        {
            public List<object> Sources { get; set; }
            public string Count { get; set; }
        }

        public partial class Relationship
        {
            public List<RelationshipPhone> Phone { get; set; }
            public RelationshipName Name { get; set; }
            public string Type { get; set; }
            public RelationshipMeta Meta { get; set; }
            public RelationshipDob Dob { get; set; }
        }

        public partial class RelationshipDob
        {
            public PropertyMeta Meta { get; set; }
            public Date Date { get; set; }
            public string Age { get; set; }
        }

        public partial class RelationshipMeta
        {
            public List<object> Sources { get; set; }
            public long Rank { get; set; }
        }

        public partial class RelationshipName
        {
            public List<object> Pidlist { get; set; }
            public AddressMeta Meta { get; set; }
            public string First { get; set; }
            public string Last { get; set; }
            public string Pid { get; set; }
        }

        public partial class RelationshipPhone
        {
            public AddressMeta Meta { get; set; }
            public string Type { get; set; }
            public string Number { get; set; }
            public string ProviderName { get; set; }
        }

        public partial class User
        {
            public string Username { get; set; }
            public string Glba { get; set; }
            public string Dppa { get; set; }
            public string CompanyName { get; set; }
        }
                                            
}
