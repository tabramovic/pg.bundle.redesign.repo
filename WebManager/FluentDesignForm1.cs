﻿using DevExpress.XtraBars;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using WebManager.WS_REG;
using WebManager.DX.UC;
using WebManager.BLL;

namespace WebManager
{
    public partial class FluentDesignForm1 : DevExpress.XtraBars.FluentDesignSystem.FluentDesignForm
    {
        
        public FluentDesignForm1()
        {
            InitializeComponent();
            ucUsers.Instance.OnGetUserInfoDetailsRequested += ucUsers_OnGetUserInfoDetailsRequested;
        }

        private void ucUsers_OnGetUserInfoDetailsRequested(DisplayUserInfo displayUserInfo)
        {
            ShowUserInfoDetailsControl(displayUserInfo);
        }

        private void _getRegisteredUsersCtrlElem_Click(object sender, EventArgs e)
        {
            ShowUsersControl();
        }

        public void ShowUsersControl()
        {
            if (!container.Controls.Contains(ucUsers.Instance))
            {
                container.Controls.Add(ucUsers.Instance);
                ucUsers.Instance.Dock = DockStyle.Fill;
                ucUsers.Instance.BringToFront();
            }

            ucUsers.Instance.BringToFront();
        }

        private void _userRegistrationDetailsCtrlElem_Click(object sender, EventArgs e)
        {
            ShowUserInfoDetailsControl(null);
        }

        void ShowUserInfoDetailsControl(DisplayUserInfo displayUserInfo)
        {
            if (!container.Controls.Contains(ucUserDetails.Instance))
            {
                container.Controls.Add(ucUserDetails.Instance);
                ucUserDetails.Instance.Dock = DockStyle.Fill;
            }

            accordionMainMenu.OptionsMinimizing.State = DevExpress.XtraBars.Navigation.AccordionControlState.Minimized;
            ucUserDetails.Instance.BringToFront();

            if (null != displayUserInfo)
            {
                ucUserDetails.Instance.SetData(displayUserInfo);
            }
        }
    }
}
