﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

//Added
using WebManager.WSEnhancements;

namespace WebManager
{
    public partial class AddUDS : DevExpress.XtraEditors.XtraForm
    {
        string _password = string.Empty;
        Guid _pgId = Guid.Empty;

        public AddUDS()
        {
            InitializeComponent();

            AddEventHandler();
        }

        void AddEventHandler()
        {
            _close.Click += new EventHandler(_close_Click);
            _setInputs.Click += new EventHandler(_setInputs_Click);
            _setTopUps.Click += new EventHandler(_setTopUps_Click);
        }

        public void SetData(string password, Guid pgId)
        {
            _password = password;
            _pgId = pgId;
        }

        void _setTopUps_Click(object sender, EventArgs e)
        {
            WSE_Service wse = new WSE_Service();

            bool retVal = false;

            try
            {
                retVal = wse.InsertTopUps(_password, _pgId, (int)_topUps.Value);
            }
            catch { retVal = false; }
            MessageBox.Show((retVal ? "Success" : "Failed"));                         
        }

        void _setInputs_Click(object sender, EventArgs e)
        {
            WSE_Service wse = new WSE_Service();

            int months = 0;
            try
            {
                months = Convert.ToInt16(_months.SelectedItem.ToString());
            }
            catch { months = 0; }

            bool retVal = false;

            try
            {
                retVal = wse.InsertInputs(_password, _pgId, _date.Value.Date, months, (int)_amount.Value);
            }
            catch { retVal = false; }
            MessageBox.Show((retVal ? "Success" : "Failed"));            
        }

        void _close_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
