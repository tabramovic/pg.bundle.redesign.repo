using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using WebManager.WS_REG;
using WebManager.WR_SDC;
using System.Diagnostics;
using System.IO;
using System.Configuration;
using System.Net;

//Added
using WebManager.WSEnhancements;
using Newtonsoft.Json;
using WebManager.BLL;

namespace WebManager
{
    public class Form1 : DevExpress.XtraEditors.XtraForm
    {
        private bool asc = false;

		// This delegate enables asynchronous calls for setting
        // the text property on a TextBox control.
		delegate void SetResultTextCallback(string text);
		delegate void SetTextCallback(string text);
		delegate void ClearTextCallback();
		delegate void GetTextCallback(string text);
		delegate void GetResultTextCallback();
		delegate void GetRegistrationCallback();

		public delegate void SetResultText (string message);
		public event SetResultText SET_RESULT_TEXT;

		private Guid currentUserReg = Guid.Empty;
		private Guid currentUserId = Guid.Empty;

        private Service1 ws = null;
        private static Thread wsChecker_thd = null;
        private static ThreadStart wsChecker_thdst = null;

		private System.Windows.Forms.TabControl tc_WS_Data;
		private System.Windows.Forms.TabPage tp_RegisteredUsers;
		private System.Windows.Forms.TabPage tp_Details;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox tbStatus;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.ListView lvUsers;
		private System.Windows.Forms.TextBox tbRefresh;
		private System.Windows.Forms.Button bRefresh;
		private System.Windows.Forms.ColumnHeader ch_FirstName;
		private System.Windows.Forms.ColumnHeader ch_MiddleName;
		private System.Windows.Forms.ColumnHeader ch_LastName;
		private System.Windows.Forms.ColumnHeader ch_SerNr;
		private System.Windows.Forms.ColumnHeader ch_ActDate;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.TextBox tb_LN;
		private System.Windows.Forms.TextBox tb_MN;
		private System.Windows.Forms.TextBox tb_FN;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.TextBox tbBussPh;
		private System.Windows.Forms.Label label15;
		private System.Windows.Forms.TextBox tbEmail;
		private System.Windows.Forms.Label label14;
		private System.Windows.Forms.TextBox tbPhoneNo;
		private System.Windows.Forms.Label label13;
		private System.Windows.Forms.TextBox tbZIP;
		private System.Windows.Forms.TextBox tbState;
		private System.Windows.Forms.TextBox tbCity;
		private System.Windows.Forms.TextBox tbAddress;
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.TextBox tbCompany;
		private System.Windows.Forms.Label label17;
		private System.Windows.Forms.TextBox tbCellNo;
		private System.Windows.Forms.Label label16;
		private System.Windows.Forms.TextBox tbActDate;
		private System.Windows.Forms.Label label20;
		private System.Windows.Forms.TextBox tbSerialNr;
		private System.Windows.Forms.Label label19;
		private System.Windows.Forms.Label label18;
		private System.Windows.Forms.CheckBox cbMod5;
		private System.Windows.Forms.CheckBox cbMod4;
		private System.Windows.Forms.CheckBox cbMod3;
		private System.Windows.Forms.CheckBox cbMod2;
		private System.Windows.Forms.CheckBox cbMod1;
		private System.Windows.Forms.CheckBox cbMod0;
		private System.Windows.Forms.Label label21;
		private System.Windows.Forms.CheckBox cbMod20;
		private System.Windows.Forms.CheckBox cbMod19;
		private System.Windows.Forms.CheckBox cbMod18;
		private System.Windows.Forms.CheckBox cbMod17;
		private System.Windows.Forms.CheckBox cbMod16;
		private System.Windows.Forms.CheckBox cbMod15;
		private System.Windows.Forms.CheckBox cbMod14;
		private System.Windows.Forms.CheckBox cbMod13;
		private System.Windows.Forms.CheckBox cbMod12;
		private System.Windows.Forms.CheckBox cbMod11;
		private System.Windows.Forms.CheckBox cbMod10;
		private System.Windows.Forms.CheckBox cbMod9;
		private System.Windows.Forms.CheckBox cbMod8;
		private System.Windows.Forms.CheckBox cbMod7;
		private System.Windows.Forms.CheckBox cbMod6;
		private System.Windows.Forms.Button bUpdate;
		private System.Windows.Forms.TabPage tp_RegisterUser;
		private System.Windows.Forms.TextBox tb_Reg_Serial;
		private System.Windows.Forms.Label label35;
		private System.Windows.Forms.Label label36;
		private System.Windows.Forms.TextBox tb_Reg_Company;
		private System.Windows.Forms.Label label25;
		private System.Windows.Forms.TextBox tb_Reg_CellPh;
		private System.Windows.Forms.Label label26;
		private System.Windows.Forms.TextBox tb_Reg_Buss_Ph;
		private System.Windows.Forms.Label label27;
		private System.Windows.Forms.TextBox tb_Reg_Email;
		private System.Windows.Forms.Label label28;
		private System.Windows.Forms.TextBox tb_Reg_PhoneNo;
		private System.Windows.Forms.Label label29;
		private System.Windows.Forms.TextBox tb_Reg_ZIP;
		private System.Windows.Forms.TextBox tb_Reg_State;
		private System.Windows.Forms.TextBox tb_Reg_City;
		private System.Windows.Forms.TextBox tb_Reg_Addr;
		private System.Windows.Forms.Label label30;
		private System.Windows.Forms.Label label31;
		private System.Windows.Forms.Label label32;
		private System.Windows.Forms.Label label33;
		private System.Windows.Forms.Label label34;
		private System.Windows.Forms.TextBox tb_Reg_LN;
		private System.Windows.Forms.TextBox tb_Reg_MN;
		private System.Windows.Forms.TextBox tb_Reg_FN;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label22;
		private System.Windows.Forms.Label label23;
		private System.Windows.Forms.Label label24;
		private System.Windows.Forms.TextBox tbResult;
		private System.Windows.Forms.Button bRegister;
		private System.Windows.Forms.Button bClearAnswer;
		private System.Windows.Forms.TextBox tbPGID;
		private System.Windows.Forms.Label label37;
		private System.Windows.Forms.Button bSetDetails;
		private System.Windows.Forms.Button bCharge;
		private System.Windows.Forms.Button bStartIE;
		private System.Windows.Forms.TabPage tp_Comps;
		private System.Windows.Forms.TabPage tp_PP;
		private WebManager.UserControlPropertyProfileCompsListView userControlPropertyProfileCompsListView1;
		private WebManager.UserControlPropertyProfileComps userControlPropertyProfileComps1;
		private System.Windows.Forms.TabPage tp_FloodComps;
		private System.Windows.Forms.TextBox flood_result;
		private System.Windows.Forms.Button flood;
		private System.Windows.Forms.NumericUpDown floodCnt;
		private System.Windows.Forms.RadioButton rbWS;
		private System.Windows.Forms.RadioButton rbWS3;
		private System.Windows.Forms.Label label38;
		private System.Windows.Forms.TextBox tbTotal;
		private System.Windows.Forms.Button bFindUser;
		private System.Windows.Forms.LinkLabel linkLabel1;
		private System.Windows.Forms.LinkLabel linkLabel2;
		private System.Windows.Forms.ColumnHeader ch_UserId;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Label label39;
		private System.Windows.Forms.TextBox tbUserRegKey;
		private System.Windows.Forms.Button button2;
		private System.Windows.Forms.Button _saveUserInfo;
		private System.Windows.Forms.Button _queryAccount;
		private System.Windows.Forms.Label label40;
		private System.Windows.Forms.NumericUpDown _amount;
		private System.Windows.Forms.RadioButton _comps;
		private System.Windows.Forms.RadioButton _pp;
		private System.Windows.Forms.RadioButton _mm;
		private System.Windows.Forms.Button _queryDataRequest;
		private System.Windows.Forms.Button _chargeMM;
        private Button _filterUsers;
        private Button _nQA;
        private Button _addUSD;
        private GroupBox groupBox1;
        private Button getUDSStatus;
        private ListView _lvUDS;
        private ColumnHeader columnHeader1;
        private ContextMenuStrip _ctxAssignedInfusionID;
        private ToolStripMenuItem _updateInfusionID;
        private ToolStripMenuItem _deleteInfusionID;
        private ToolStripMenuItem _InsertInfusionID;
        private Button _getAssignedPGIDs;
        private Label label41;
        private TextBox _infusionID;
        private ListView _lvAssignedPGIDs;
        private ColumnHeader columnHeader2;
        private ContextMenuStrip _ctxAssignedPGID;
        private ToolStripMenuItem _deletePGID;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private ListView lvSerials;
        private ColumnHeader chSerial;
        private TextBox tbSerialThd;
        private Button bRegisterThd;
        private Button bAdd;
        private Button bClear;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.Utils.Behaviors.BehaviorManager behaviorManager1;
        private DevExpress.XtraEditors.TextEdit textEdit1;
        private DevExpress.XtraEditors.PanelControl rightDockFixedRightPanel;
        private DevExpress.XtraEditors.PanelControl floatingCentralPanel;
        private DevExpress.Utils.FlyoutPanel flyoutPanel;
        private DevExpress.Utils.FlyoutPanelControl flyoutPanelControl1;
        private DevExpress.XtraEditors.SimpleButton showFlyoutButton;
        private DevExpress.XtraEditors.Controls.CalendarControl calendarControl1;

        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraEditors.ButtonsPanelControl.ButtonImageOptions buttonImageOptions1 = new DevExpress.XtraEditors.ButtonsPanelControl.ButtonImageOptions();
            DevExpress.XtraEditors.ButtonsPanelControl.ButtonImageOptions buttonImageOptions2 = new DevExpress.XtraEditors.ButtonsPanelControl.ButtonImageOptions();
            this.tc_WS_Data = new System.Windows.Forms.TabControl();
            this.tp_RegisteredUsers = new System.Windows.Forms.TabPage();
            this._filterUsers = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.bFindUser = new System.Windows.Forms.Button();
            this.tbTotal = new System.Windows.Forms.TextBox();
            this.label38 = new System.Windows.Forms.Label();
            this.lvUsers = new System.Windows.Forms.ListView();
            this.ch_FirstName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ch_MiddleName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ch_LastName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ch_SerNr = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ch_ActDate = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ch_UserId = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.tbRefresh = new System.Windows.Forms.TextBox();
            this.bRefresh = new System.Windows.Forms.Button();
            this.tp_RegisterUser = new System.Windows.Forms.TabPage();
            this.floatingCentralPanel = new DevExpress.XtraEditors.PanelControl();
            this.rightDockFixedRightPanel = new DevExpress.XtraEditors.PanelControl();
            this.showFlyoutButton = new DevExpress.XtraEditors.SimpleButton();
            this.flyoutPanel = new DevExpress.Utils.FlyoutPanel();
            this.flyoutPanelControl1 = new DevExpress.Utils.FlyoutPanelControl();
            this.calendarControl1 = new DevExpress.XtraEditors.Controls.CalendarControl();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.textEdit1 = new DevExpress.XtraEditors.TextEdit();
            this.label34 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.bClearAnswer = new System.Windows.Forms.Button();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.bRegister = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.tb_Reg_Serial = new System.Windows.Forms.TextBox();
            this.tb_Reg_FN = new System.Windows.Forms.TextBox();
            this.label35 = new System.Windows.Forms.Label();
            this.tb_Reg_MN = new System.Windows.Forms.TextBox();
            this.label36 = new System.Windows.Forms.Label();
            this.tb_Reg_LN = new System.Windows.Forms.TextBox();
            this.tb_Reg_Company = new System.Windows.Forms.TextBox();
            this.label33 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.tb_Reg_CellPh = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.tb_Reg_Buss_Ph = new System.Windows.Forms.TextBox();
            this.tb_Reg_Addr = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.tb_Reg_City = new System.Windows.Forms.TextBox();
            this.tb_Reg_Email = new System.Windows.Forms.TextBox();
            this.tb_Reg_State = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.tb_Reg_ZIP = new System.Windows.Forms.TextBox();
            this.tb_Reg_PhoneNo = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.lvSerials = new System.Windows.Forms.ListView();
            this.chSerial = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.tbSerialThd = new System.Windows.Forms.TextBox();
            this.bRegisterThd = new System.Windows.Forms.Button();
            this.bAdd = new System.Windows.Forms.Button();
            this.bClear = new System.Windows.Forms.Button();
            this.tbResult = new System.Windows.Forms.TextBox();
            this.tp_Details = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this._lvAssignedPGIDs = new System.Windows.Forms.ListView();
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this._ctxAssignedPGID = new System.Windows.Forms.ContextMenuStrip(this.components);
            this._deletePGID = new System.Windows.Forms.ToolStripMenuItem();
            this._getAssignedPGIDs = new System.Windows.Forms.Button();
            this.label41 = new System.Windows.Forms.Label();
            this._infusionID = new System.Windows.Forms.TextBox();
            this._lvUDS = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this._ctxAssignedInfusionID = new System.Windows.Forms.ContextMenuStrip(this.components);
            this._updateInfusionID = new System.Windows.Forms.ToolStripMenuItem();
            this._deleteInfusionID = new System.Windows.Forms.ToolStripMenuItem();
            this._InsertInfusionID = new System.Windows.Forms.ToolStripMenuItem();
            this.getUDSStatus = new System.Windows.Forms.Button();
            this._addUSD = new System.Windows.Forms.Button();
            this._nQA = new System.Windows.Forms.Button();
            this._chargeMM = new System.Windows.Forms.Button();
            this.label40 = new System.Windows.Forms.Label();
            this._amount = new System.Windows.Forms.NumericUpDown();
            this._comps = new System.Windows.Forms.RadioButton();
            this._pp = new System.Windows.Forms.RadioButton();
            this._mm = new System.Windows.Forms.RadioButton();
            this._queryDataRequest = new System.Windows.Forms.Button();
            this._queryAccount = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.linkLabel2 = new System.Windows.Forms.LinkLabel();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.bCharge = new System.Windows.Forms.Button();
            this.bSetDetails = new System.Windows.Forms.Button();
            this.tbPGID = new System.Windows.Forms.TextBox();
            this.label37 = new System.Windows.Forms.Label();
            this.bUpdate = new System.Windows.Forms.Button();
            this.cbMod20 = new System.Windows.Forms.CheckBox();
            this.cbMod19 = new System.Windows.Forms.CheckBox();
            this.cbMod18 = new System.Windows.Forms.CheckBox();
            this.cbMod17 = new System.Windows.Forms.CheckBox();
            this.cbMod16 = new System.Windows.Forms.CheckBox();
            this.cbMod15 = new System.Windows.Forms.CheckBox();
            this.cbMod14 = new System.Windows.Forms.CheckBox();
            this.cbMod13 = new System.Windows.Forms.CheckBox();
            this.cbMod12 = new System.Windows.Forms.CheckBox();
            this.cbMod11 = new System.Windows.Forms.CheckBox();
            this.cbMod10 = new System.Windows.Forms.CheckBox();
            this.cbMod9 = new System.Windows.Forms.CheckBox();
            this.cbMod8 = new System.Windows.Forms.CheckBox();
            this.cbMod7 = new System.Windows.Forms.CheckBox();
            this.cbMod6 = new System.Windows.Forms.CheckBox();
            this.cbMod5 = new System.Windows.Forms.CheckBox();
            this.cbMod4 = new System.Windows.Forms.CheckBox();
            this.cbMod3 = new System.Windows.Forms.CheckBox();
            this.cbMod2 = new System.Windows.Forms.CheckBox();
            this.cbMod1 = new System.Windows.Forms.CheckBox();
            this.cbMod0 = new System.Windows.Forms.CheckBox();
            this.label21 = new System.Windows.Forms.Label();
            this.tbActDate = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.tbSerialNr = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.tbCompany = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.tbCellNo = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.tbBussPh = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.tbEmail = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.tbPhoneNo = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.tbZIP = new System.Windows.Forms.TextBox();
            this.tbState = new System.Windows.Forms.TextBox();
            this.tbCity = new System.Windows.Forms.TextBox();
            this.tbAddress = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.tb_LN = new System.Windows.Forms.TextBox();
            this.tb_MN = new System.Windows.Forms.TextBox();
            this.tb_FN = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.tp_Comps = new System.Windows.Forms.TabPage();
            this.userControlPropertyProfileCompsListView1 = new WebManager.UserControlPropertyProfileCompsListView();
            this.tp_PP = new System.Windows.Forms.TabPage();
            this.userControlPropertyProfileComps1 = new WebManager.UserControlPropertyProfileComps();
            this.tp_FloodComps = new System.Windows.Forms.TabPage();
            this.floodCnt = new System.Windows.Forms.NumericUpDown();
            this.flood = new System.Windows.Forms.Button();
            this.flood_result = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tbStatus = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.bStartIE = new System.Windows.Forms.Button();
            this.rbWS = new System.Windows.Forms.RadioButton();
            this.rbWS3 = new System.Windows.Forms.RadioButton();
            this.label39 = new System.Windows.Forms.Label();
            this.tbUserRegKey = new System.Windows.Forms.TextBox();
            this._saveUserInfo = new System.Windows.Forms.Button();
            this.behaviorManager1 = new DevExpress.Utils.Behaviors.BehaviorManager(this.components);
            this.tc_WS_Data.SuspendLayout();
            this.tp_RegisteredUsers.SuspendLayout();
            this.tp_RegisterUser.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.floatingCentralPanel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rightDockFixedRightPanel)).BeginInit();
            this.rightDockFixedRightPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.flyoutPanel)).BeginInit();
            this.flyoutPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.flyoutPanelControl1)).BeginInit();
            this.flyoutPanelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.calendarControl1.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            this.tp_Details.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this._ctxAssignedPGID.SuspendLayout();
            this._ctxAssignedInfusionID.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._amount)).BeginInit();
            this.tp_Comps.SuspendLayout();
            this.tp_PP.SuspendLayout();
            this.tp_FloodComps.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.floodCnt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.behaviorManager1)).BeginInit();
            this.SuspendLayout();
            // 
            // tc_WS_Data
            // 
            this.tc_WS_Data.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tc_WS_Data.Controls.Add(this.tp_RegisteredUsers);
            this.tc_WS_Data.Controls.Add(this.tp_RegisterUser);
            this.tc_WS_Data.Controls.Add(this.tp_Details);
            this.tc_WS_Data.Controls.Add(this.tp_Comps);
            this.tc_WS_Data.Controls.Add(this.tp_PP);
            this.tc_WS_Data.Controls.Add(this.tp_FloodComps);
            this.tc_WS_Data.Location = new System.Drawing.Point(30, 12);
            this.tc_WS_Data.Name = "tc_WS_Data";
            this.tc_WS_Data.SelectedIndex = 0;
            this.tc_WS_Data.Size = new System.Drawing.Size(1192, 626);
            this.tc_WS_Data.TabIndex = 0;
            // 
            // tp_RegisteredUsers
            // 
            this.tp_RegisteredUsers.Controls.Add(this._filterUsers);
            this.tp_RegisteredUsers.Controls.Add(this.button1);
            this.tp_RegisteredUsers.Controls.Add(this.bFindUser);
            this.tp_RegisteredUsers.Controls.Add(this.tbTotal);
            this.tp_RegisteredUsers.Controls.Add(this.label38);
            this.tp_RegisteredUsers.Controls.Add(this.lvUsers);
            this.tp_RegisteredUsers.Controls.Add(this.tbRefresh);
            this.tp_RegisteredUsers.Controls.Add(this.bRefresh);
            this.tp_RegisteredUsers.Location = new System.Drawing.Point(4, 22);
            this.tp_RegisteredUsers.Name = "tp_RegisteredUsers";
            this.tp_RegisteredUsers.Size = new System.Drawing.Size(1184, 600);
            this.tp_RegisteredUsers.TabIndex = 0;
            this.tp_RegisteredUsers.Text = "ProfitGrabber Registered Users";
            // 
            // _filterUsers
            // 
            this._filterUsers.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._filterUsers.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._filterUsers.Location = new System.Drawing.Point(428, 567);
            this._filterUsers.Name = "_filterUsers";
            this._filterUsers.Size = new System.Drawing.Size(155, 25);
            this._filterUsers.TabIndex = 7;
            this._filterUsers.Text = "FILTER USERS";
            this._filterUsers.Click += new System.EventHandler(this._filterUsers_Click);
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.button1.Location = new System.Drawing.Point(110, 567);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(312, 25);
            this.button1.TabIndex = 6;
            this.button1.Text = "Select PG Default user (Tomislav, reg. March, 01st)";
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // bFindUser
            // 
            this.bFindUser.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.bFindUser.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.bFindUser.Location = new System.Drawing.Point(8, 567);
            this.bFindUser.Name = "bFindUser";
            this.bFindUser.Size = new System.Drawing.Size(96, 25);
            this.bFindUser.TabIndex = 5;
            this.bFindUser.Text = "&Find User";
            this.bFindUser.Click += new System.EventHandler(this.bFindUser_Click);
            // 
            // tbTotal
            // 
            this.tbTotal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.tbTotal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbTotal.Location = new System.Drawing.Point(1080, 567);
            this.tbTotal.Name = "tbTotal";
            this.tbTotal.Size = new System.Drawing.Size(100, 21);
            this.tbTotal.TabIndex = 4;
            // 
            // label38
            // 
            this.label38.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label38.Location = new System.Drawing.Point(1032, 567);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(40, 25);
            this.label38.TabIndex = 3;
            this.label38.Text = "Count:";
            // 
            // lvUsers
            // 
            this.lvUsers.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lvUsers.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.ch_FirstName,
            this.ch_MiddleName,
            this.ch_LastName,
            this.ch_SerNr,
            this.ch_ActDate,
            this.ch_UserId});
            this.lvUsers.FullRowSelect = true;
            this.lvUsers.GridLines = true;
            this.lvUsers.Location = new System.Drawing.Point(7, 39);
            this.lvUsers.MultiSelect = false;
            this.lvUsers.Name = "lvUsers";
            this.lvUsers.Size = new System.Drawing.Size(1171, 519);
            this.lvUsers.TabIndex = 2;
            this.lvUsers.UseCompatibleStateImageBehavior = false;
            this.lvUsers.View = System.Windows.Forms.View.Details;
            this.lvUsers.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.lvUsers_ColumnClick);
            this.lvUsers.SelectedIndexChanged += new System.EventHandler(this.lvUsers_SelectedIndexChanged);
            this.lvUsers.DoubleClick += new System.EventHandler(this.lvUsers_DoubleClick);
            // 
            // ch_FirstName
            // 
            this.ch_FirstName.Text = "First Name";
            this.ch_FirstName.Width = 127;
            // 
            // ch_MiddleName
            // 
            this.ch_MiddleName.Text = "Middle Name";
            this.ch_MiddleName.Width = 121;
            // 
            // ch_LastName
            // 
            this.ch_LastName.Text = "Last Name";
            this.ch_LastName.Width = 187;
            // 
            // ch_SerNr
            // 
            this.ch_SerNr.Text = "Serial Number";
            this.ch_SerNr.Width = 101;
            // 
            // ch_ActDate
            // 
            this.ch_ActDate.Text = "Activation Date";
            this.ch_ActDate.Width = 99;
            // 
            // ch_UserId
            // 
            this.ch_UserId.Text = "UserId";
            // 
            // tbRefresh
            // 
            this.tbRefresh.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbRefresh.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbRefresh.Location = new System.Drawing.Point(104, 8);
            this.tbRefresh.Name = "tbRefresh";
            this.tbRefresh.Size = new System.Drawing.Size(1073, 21);
            this.tbRefresh.TabIndex = 1;
            // 
            // bRefresh
            // 
            this.bRefresh.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.bRefresh.Location = new System.Drawing.Point(6, 6);
            this.bRefresh.Name = "bRefresh";
            this.bRefresh.Size = new System.Drawing.Size(90, 25);
            this.bRefresh.TabIndex = 0;
            this.bRefresh.Text = "Get All Users";
            this.bRefresh.Click += new System.EventHandler(this.bRefresh_Click);
            // 
            // tp_RegisterUser
            // 
            this.tp_RegisterUser.Controls.Add(this.floatingCentralPanel);
            this.tp_RegisterUser.Controls.Add(this.rightDockFixedRightPanel);
            this.tp_RegisterUser.Controls.Add(this.groupControl2);
            this.tp_RegisterUser.Controls.Add(this.groupControl1);
            this.tp_RegisterUser.Controls.Add(this.tbResult);
            this.tp_RegisterUser.Location = new System.Drawing.Point(4, 22);
            this.tp_RegisterUser.Name = "tp_RegisterUser";
            this.tp_RegisterUser.Size = new System.Drawing.Size(1184, 658);
            this.tp_RegisterUser.TabIndex = 2;
            this.tp_RegisterUser.Text = "Register User";
            // 
            // floatingCentralPanel
            // 
            this.floatingCentralPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.floatingCentralPanel.Appearance.BackColor = System.Drawing.Color.DarkSalmon;
            this.floatingCentralPanel.Appearance.Options.UseBackColor = true;
            this.floatingCentralPanel.Location = new System.Drawing.Point(904, 0);
            this.floatingCentralPanel.Name = "floatingCentralPanel";
            this.floatingCentralPanel.Size = new System.Drawing.Size(11, 655);
            this.floatingCentralPanel.TabIndex = 59;
            // 
            // rightDockFixedRightPanel
            // 
            this.rightDockFixedRightPanel.Appearance.BackColor = System.Drawing.Color.MistyRose;
            this.rightDockFixedRightPanel.Appearance.Options.UseBackColor = true;
            this.rightDockFixedRightPanel.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Flat;
            this.rightDockFixedRightPanel.Controls.Add(this.showFlyoutButton);
            this.rightDockFixedRightPanel.Controls.Add(this.flyoutPanel);
            this.rightDockFixedRightPanel.Dock = System.Windows.Forms.DockStyle.Right;
            this.rightDockFixedRightPanel.Location = new System.Drawing.Point(921, 0);
            this.rightDockFixedRightPanel.Name = "rightDockFixedRightPanel";
            this.rightDockFixedRightPanel.Size = new System.Drawing.Size(263, 658);
            this.rightDockFixedRightPanel.TabIndex = 58;
            // 
            // showFlyoutButton
            // 
            this.showFlyoutButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.showFlyoutButton.Location = new System.Drawing.Point(185, 630);
            this.showFlyoutButton.Name = "showFlyoutButton";
            this.showFlyoutButton.Size = new System.Drawing.Size(75, 22);
            this.showFlyoutButton.TabIndex = 1;
            this.showFlyoutButton.Text = "Show Flyout";
            this.showFlyoutButton.Click += new System.EventHandler(this.showFlyoutButton_Click);
            // 
            // flyoutPanel
            // 
            this.flyoutPanel.AutoSize = true;
            this.flyoutPanel.Controls.Add(this.flyoutPanelControl1);
            this.flyoutPanel.Location = new System.Drawing.Point(0, 5);
            this.flyoutPanel.Name = "flyoutPanel";
            this.flyoutPanel.OptionsButtonPanel.ButtonPanelLocation = DevExpress.Utils.FlyoutPanelButtonPanelLocation.Top;
            this.flyoutPanel.OptionsButtonPanel.Buttons.AddRange(new DevExpress.XtraEditors.ButtonPanel.IBaseButton[] {
            new DevExpress.Utils.PeekFormButton("OK", true, buttonImageOptions1, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, null, -1, false),
            new DevExpress.Utils.PeekFormButton("Show", true, buttonImageOptions2, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, null, -1, false)});
            this.flyoutPanel.OptionsButtonPanel.ShowButtonPanel = true;
            this.flyoutPanel.OwnerControl = this.rightDockFixedRightPanel;
            this.flyoutPanel.Padding = new System.Windows.Forms.Padding(0, 30, 0, 0);
            this.flyoutPanel.Size = new System.Drawing.Size(258, 284);
            this.flyoutPanel.TabIndex = 0;
            // 
            // flyoutPanelControl1
            // 
            this.flyoutPanelControl1.Controls.Add(this.calendarControl1);
            this.flyoutPanelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flyoutPanelControl1.FlyoutPanel = this.flyoutPanel;
            this.flyoutPanelControl1.Location = new System.Drawing.Point(0, 30);
            this.flyoutPanelControl1.Name = "flyoutPanelControl1";
            this.flyoutPanelControl1.Size = new System.Drawing.Size(258, 254);
            this.flyoutPanelControl1.TabIndex = 0;
            // 
            // calendarControl1
            // 
            this.calendarControl1.AutoSize = false;
            this.calendarControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.calendarControl1.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.calendarControl1.CalendarView = DevExpress.XtraEditors.Repository.CalendarView.Fluent;
            this.calendarControl1.Location = new System.Drawing.Point(3, 5);
            this.calendarControl1.Name = "calendarControl1";
            this.calendarControl1.Size = new System.Drawing.Size(250, 244);
            this.calendarControl1.TabIndex = 0;
            this.calendarControl1.UseDirectXPaint = DevExpress.Utils.DefaultBoolean.True;
            // 
            // groupControl2
            // 
            this.groupControl2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.groupControl2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.groupControl2.Controls.Add(this.textEdit1);
            this.groupControl2.Controls.Add(this.label34);
            this.groupControl2.Controls.Add(this.label24);
            this.groupControl2.Controls.Add(this.bClearAnswer);
            this.groupControl2.Controls.Add(this.label23);
            this.groupControl2.Controls.Add(this.label22);
            this.groupControl2.Controls.Add(this.bRegister);
            this.groupControl2.Controls.Add(this.label2);
            this.groupControl2.Controls.Add(this.tb_Reg_Serial);
            this.groupControl2.Controls.Add(this.tb_Reg_FN);
            this.groupControl2.Controls.Add(this.label35);
            this.groupControl2.Controls.Add(this.tb_Reg_MN);
            this.groupControl2.Controls.Add(this.label36);
            this.groupControl2.Controls.Add(this.tb_Reg_LN);
            this.groupControl2.Controls.Add(this.tb_Reg_Company);
            this.groupControl2.Controls.Add(this.label33);
            this.groupControl2.Controls.Add(this.label25);
            this.groupControl2.Controls.Add(this.label32);
            this.groupControl2.Controls.Add(this.tb_Reg_CellPh);
            this.groupControl2.Controls.Add(this.label31);
            this.groupControl2.Controls.Add(this.label26);
            this.groupControl2.Controls.Add(this.label30);
            this.groupControl2.Controls.Add(this.tb_Reg_Buss_Ph);
            this.groupControl2.Controls.Add(this.tb_Reg_Addr);
            this.groupControl2.Controls.Add(this.label27);
            this.groupControl2.Controls.Add(this.tb_Reg_City);
            this.groupControl2.Controls.Add(this.tb_Reg_Email);
            this.groupControl2.Controls.Add(this.tb_Reg_State);
            this.groupControl2.Controls.Add(this.label28);
            this.groupControl2.Controls.Add(this.tb_Reg_ZIP);
            this.groupControl2.Controls.Add(this.tb_Reg_PhoneNo);
            this.groupControl2.Controls.Add(this.label29);
            this.groupControl2.GroupStyle = DevExpress.Utils.GroupStyle.Title;
            this.groupControl2.Location = new System.Drawing.Point(11, 16);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(660, 286);
            this.groupControl2.TabIndex = 57;
            this.groupControl2.Text = "REGISTRATION DETAILS";
            this.groupControl2.Paint += new System.Windows.Forms.PaintEventHandler(this.groupControl2_Paint);
            // 
            // textEdit1
            // 
            this.textEdit1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textEdit1.EditValue = "test stretch";
            this.textEdit1.Location = new System.Drawing.Point(289, 263);
            this.textEdit1.Name = "textEdit1";
            this.textEdit1.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.textEdit1.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit1.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.textEdit1.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textEdit1.Size = new System.Drawing.Size(90, 38);
            this.textEdit1.TabIndex = 56;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.Location = new System.Drawing.Point(216, 27);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(52, 13);
            this.label34.TabIndex = 26;
            this.label34.Text = "Address";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(6, 109);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(76, 13);
            this.label24.TabIndex = 7;
            this.label24.Text = "User Details";
            // 
            // bClearAnswer
            // 
            this.bClearAnswer.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.bClearAnswer.Location = new System.Drawing.Point(3, 236);
            this.bClearAnswer.Name = "bClearAnswer";
            this.bClearAnswer.Size = new System.Drawing.Size(75, 25);
            this.bClearAnswer.TabIndex = 55;
            this.bClearAnswer.Text = "Clear";
            this.bClearAnswer.Click += new System.EventHandler(this.bClearAnswer_Click);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(14, 137);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(62, 13);
            this.label23.TabIndex = 8;
            this.label23.Text = "First Name:";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(14, 167);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(71, 13);
            this.label22.TabIndex = 9;
            this.label22.Text = "Middle Name:";
            // 
            // bRegister
            // 
            this.bRegister.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.bRegister.Location = new System.Drawing.Point(435, 225);
            this.bRegister.Name = "bRegister";
            this.bRegister.Size = new System.Drawing.Size(215, 25);
            this.bRegister.TabIndex = 48;
            this.bRegister.Text = "Register Single User";
            this.bRegister.Visible = false;
            this.bRegister.Click += new System.EventHandler(this.bRegister_Click_1);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(14, 197);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(61, 13);
            this.label2.TabIndex = 10;
            this.label2.Text = "Last Name:";
            // 
            // tb_Reg_Serial
            // 
            this.tb_Reg_Serial.Location = new System.Drawing.Point(91, 54);
            this.tb_Reg_Serial.Name = "tb_Reg_Serial";
            this.tb_Reg_Serial.Size = new System.Drawing.Size(135, 21);
            this.tb_Reg_Serial.TabIndex = 47;
            this.tb_Reg_Serial.Visible = false;
            // 
            // tb_Reg_FN
            // 
            this.tb_Reg_FN.Location = new System.Drawing.Point(91, 135);
            this.tb_Reg_FN.Name = "tb_Reg_FN";
            this.tb_Reg_FN.Size = new System.Drawing.Size(135, 21);
            this.tb_Reg_FN.TabIndex = 11;
            this.tb_Reg_FN.Text = "_TEST_FN";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(14, 56);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(51, 13);
            this.label35.TabIndex = 46;
            this.label35.Text = "Serial Nr:";
            this.label35.Visible = false;
            // 
            // tb_Reg_MN
            // 
            this.tb_Reg_MN.Location = new System.Drawing.Point(91, 168);
            this.tb_Reg_MN.Name = "tb_Reg_MN";
            this.tb_Reg_MN.Size = new System.Drawing.Size(135, 21);
            this.tb_Reg_MN.TabIndex = 12;
            this.tb_Reg_MN.Text = "_TEST_MN";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.Location = new System.Drawing.Point(6, 27);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(118, 13);
            this.label36.TabIndex = 45;
            this.label36.Text = "Registration Details";
            // 
            // tb_Reg_LN
            // 
            this.tb_Reg_LN.Location = new System.Drawing.Point(91, 198);
            this.tb_Reg_LN.Name = "tb_Reg_LN";
            this.tb_Reg_LN.Size = new System.Drawing.Size(135, 21);
            this.tb_Reg_LN.TabIndex = 13;
            this.tb_Reg_LN.Text = "TEST_LN";
            // 
            // tb_Reg_Company
            // 
            this.tb_Reg_Company.Location = new System.Drawing.Point(289, 195);
            this.tb_Reg_Company.Name = "tb_Reg_Company";
            this.tb_Reg_Company.Size = new System.Drawing.Size(361, 21);
            this.tb_Reg_Company.TabIndex = 44;
            this.tb_Reg_Company.Text = "_TEST_COMPANY";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(233, 56);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(50, 13);
            this.label33.TabIndex = 27;
            this.label33.Text = "Address:";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(233, 198);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(56, 13);
            this.label25.TabIndex = 43;
            this.label25.Text = "Company:";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(233, 84);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(30, 13);
            this.label32.TabIndex = 28;
            this.label32.Text = "City:";
            // 
            // tb_Reg_CellPh
            // 
            this.tb_Reg_CellPh.Location = new System.Drawing.Point(470, 138);
            this.tb_Reg_CellPh.Name = "tb_Reg_CellPh";
            this.tb_Reg_CellPh.Size = new System.Drawing.Size(180, 21);
            this.tb_Reg_CellPh.TabIndex = 42;
            this.tb_Reg_CellPh.Text = "_TEST_CELL_NO";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(233, 112);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(37, 13);
            this.label31.TabIndex = 29;
            this.label31.Text = "State:";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(404, 140);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(51, 13);
            this.label26.TabIndex = 41;
            this.label26.Text = "Cell. Ph.:";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(233, 140);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(27, 13);
            this.label30.TabIndex = 30;
            this.label30.Text = "ZIP:";
            // 
            // tb_Reg_Buss_Ph
            // 
            this.tb_Reg_Buss_Ph.Location = new System.Drawing.Point(470, 110);
            this.tb_Reg_Buss_Ph.Name = "tb_Reg_Buss_Ph";
            this.tb_Reg_Buss_Ph.Size = new System.Drawing.Size(180, 21);
            this.tb_Reg_Buss_Ph.TabIndex = 40;
            this.tb_Reg_Buss_Ph.Text = "_TEST_BUSS_PH";
            // 
            // tb_Reg_Addr
            // 
            this.tb_Reg_Addr.Location = new System.Drawing.Point(289, 54);
            this.tb_Reg_Addr.Name = "tb_Reg_Addr";
            this.tb_Reg_Addr.Size = new System.Drawing.Size(361, 21);
            this.tb_Reg_Addr.TabIndex = 31;
            this.tb_Reg_Addr.Text = "_TEST_ADDR";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(404, 112);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(56, 13);
            this.label27.TabIndex = 39;
            this.label27.Text = "Buss. Ph.:";
            // 
            // tb_Reg_City
            // 
            this.tb_Reg_City.Location = new System.Drawing.Point(289, 82);
            this.tb_Reg_City.Name = "tb_Reg_City";
            this.tb_Reg_City.Size = new System.Drawing.Size(111, 21);
            this.tb_Reg_City.TabIndex = 32;
            this.tb_Reg_City.Text = "_TEST_CITY";
            // 
            // tb_Reg_Email
            // 
            this.tb_Reg_Email.Location = new System.Drawing.Point(289, 167);
            this.tb_Reg_Email.Name = "tb_Reg_Email";
            this.tb_Reg_Email.Size = new System.Drawing.Size(361, 21);
            this.tb_Reg_Email.TabIndex = 38;
            this.tb_Reg_Email.Text = "_TEST_EMAIL";
            // 
            // tb_Reg_State
            // 
            this.tb_Reg_State.Location = new System.Drawing.Point(289, 110);
            this.tb_Reg_State.Name = "tb_Reg_State";
            this.tb_Reg_State.Size = new System.Drawing.Size(111, 21);
            this.tb_Reg_State.TabIndex = 33;
            this.tb_Reg_State.Text = "_TEST_STATE";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(233, 169);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(35, 13);
            this.label28.TabIndex = 37;
            this.label28.Text = "Email:";
            // 
            // tb_Reg_ZIP
            // 
            this.tb_Reg_ZIP.Location = new System.Drawing.Point(289, 138);
            this.tb_Reg_ZIP.Name = "tb_Reg_ZIP";
            this.tb_Reg_ZIP.Size = new System.Drawing.Size(111, 21);
            this.tb_Reg_ZIP.TabIndex = 34;
            this.tb_Reg_ZIP.Text = "_TEST_ZIP";
            // 
            // tb_Reg_PhoneNo
            // 
            this.tb_Reg_PhoneNo.Location = new System.Drawing.Point(470, 83);
            this.tb_Reg_PhoneNo.Name = "tb_Reg_PhoneNo";
            this.tb_Reg_PhoneNo.Size = new System.Drawing.Size(180, 21);
            this.tb_Reg_PhoneNo.TabIndex = 36;
            this.tb_Reg_PhoneNo.Text = "_TEST_PHNO";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(404, 85);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(57, 13);
            this.label29.TabIndex = 35;
            this.label29.Text = "Phone No:";
            // 
            // groupControl1
            // 
            this.groupControl1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.groupControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.groupControl1.CaptionLocation = DevExpress.Utils.Locations.Top;
            this.groupControl1.Controls.Add(this.lvSerials);
            this.groupControl1.Controls.Add(this.tbSerialThd);
            this.groupControl1.Controls.Add(this.bRegisterThd);
            this.groupControl1.Controls.Add(this.bAdd);
            this.groupControl1.Controls.Add(this.bClear);
            this.groupControl1.GroupStyle = DevExpress.Utils.GroupStyle.Title;
            this.groupControl1.Location = new System.Drawing.Point(675, 16);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(223, 286);
            this.groupControl1.TabIndex = 56;
            this.groupControl1.Text = "REGISTER";
            // 
            // lvSerials
            // 
            this.lvSerials.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.chSerial});
            this.lvSerials.FullRowSelect = true;
            this.lvSerials.GridLines = true;
            this.lvSerials.Location = new System.Drawing.Point(10, 106);
            this.lvSerials.MultiSelect = false;
            this.lvSerials.Name = "lvSerials";
            this.lvSerials.Size = new System.Drawing.Size(210, 144);
            this.lvSerials.TabIndex = 52;
            this.lvSerials.UseCompatibleStateImageBehavior = false;
            this.lvSerials.View = System.Windows.Forms.View.Details;
            // 
            // chSerial
            // 
            this.chSerial.Text = "Serial Number";
            this.chSerial.Width = 187;
            // 
            // tbSerialThd
            // 
            this.tbSerialThd.Location = new System.Drawing.Point(10, 38);
            this.tbSerialThd.Name = "tbSerialThd";
            this.tbSerialThd.Size = new System.Drawing.Size(135, 21);
            this.tbSerialThd.TabIndex = 50;
            // 
            // bRegisterThd
            // 
            this.bRegisterThd.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.bRegisterThd.Location = new System.Drawing.Point(10, 69);
            this.bRegisterThd.Name = "bRegisterThd";
            this.bRegisterThd.Size = new System.Drawing.Size(135, 25);
            this.bRegisterThd.TabIndex = 54;
            this.bRegisterThd.Text = "Register Multiple";
            this.bRegisterThd.Click += new System.EventHandler(this.bRegisterThd_Click);
            // 
            // bAdd
            // 
            this.bAdd.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.bAdd.Location = new System.Drawing.Point(162, 34);
            this.bAdd.Name = "bAdd";
            this.bAdd.Size = new System.Drawing.Size(58, 26);
            this.bAdd.TabIndex = 51;
            this.bAdd.Text = "Add";
            this.bAdd.Click += new System.EventHandler(this.bAdd_Click);
            // 
            // bClear
            // 
            this.bClear.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.bClear.Location = new System.Drawing.Point(162, 69);
            this.bClear.Name = "bClear";
            this.bClear.Size = new System.Drawing.Size(58, 25);
            this.bClear.TabIndex = 53;
            this.bClear.Text = "Clear";
            this.bClear.Click += new System.EventHandler(this.bClear_Click);
            // 
            // tbResult
            // 
            this.tbResult.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.tbResult.Location = new System.Drawing.Point(11, 308);
            this.tbResult.Multiline = true;
            this.tbResult.Name = "tbResult";
            this.tbResult.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbResult.Size = new System.Drawing.Size(887, 347);
            this.tbResult.TabIndex = 49;
            // 
            // tp_Details
            // 
            this.tp_Details.Controls.Add(this.groupBox1);
            this.tp_Details.Controls.Add(this._addUSD);
            this.tp_Details.Controls.Add(this._nQA);
            this.tp_Details.Controls.Add(this._chargeMM);
            this.tp_Details.Controls.Add(this.label40);
            this.tp_Details.Controls.Add(this._amount);
            this.tp_Details.Controls.Add(this._comps);
            this.tp_Details.Controls.Add(this._pp);
            this.tp_Details.Controls.Add(this._mm);
            this.tp_Details.Controls.Add(this._queryDataRequest);
            this.tp_Details.Controls.Add(this._queryAccount);
            this.tp_Details.Controls.Add(this.button2);
            this.tp_Details.Controls.Add(this.linkLabel2);
            this.tp_Details.Controls.Add(this.linkLabel1);
            this.tp_Details.Controls.Add(this.bCharge);
            this.tp_Details.Controls.Add(this.bSetDetails);
            this.tp_Details.Controls.Add(this.tbPGID);
            this.tp_Details.Controls.Add(this.label37);
            this.tp_Details.Controls.Add(this.bUpdate);
            this.tp_Details.Controls.Add(this.cbMod20);
            this.tp_Details.Controls.Add(this.cbMod19);
            this.tp_Details.Controls.Add(this.cbMod18);
            this.tp_Details.Controls.Add(this.cbMod17);
            this.tp_Details.Controls.Add(this.cbMod16);
            this.tp_Details.Controls.Add(this.cbMod15);
            this.tp_Details.Controls.Add(this.cbMod14);
            this.tp_Details.Controls.Add(this.cbMod13);
            this.tp_Details.Controls.Add(this.cbMod12);
            this.tp_Details.Controls.Add(this.cbMod11);
            this.tp_Details.Controls.Add(this.cbMod10);
            this.tp_Details.Controls.Add(this.cbMod9);
            this.tp_Details.Controls.Add(this.cbMod8);
            this.tp_Details.Controls.Add(this.cbMod7);
            this.tp_Details.Controls.Add(this.cbMod6);
            this.tp_Details.Controls.Add(this.cbMod5);
            this.tp_Details.Controls.Add(this.cbMod4);
            this.tp_Details.Controls.Add(this.cbMod3);
            this.tp_Details.Controls.Add(this.cbMod2);
            this.tp_Details.Controls.Add(this.cbMod1);
            this.tp_Details.Controls.Add(this.cbMod0);
            this.tp_Details.Controls.Add(this.label21);
            this.tp_Details.Controls.Add(this.tbActDate);
            this.tp_Details.Controls.Add(this.label20);
            this.tp_Details.Controls.Add(this.tbSerialNr);
            this.tp_Details.Controls.Add(this.label19);
            this.tp_Details.Controls.Add(this.label18);
            this.tp_Details.Controls.Add(this.tbCompany);
            this.tp_Details.Controls.Add(this.label17);
            this.tp_Details.Controls.Add(this.tbCellNo);
            this.tp_Details.Controls.Add(this.label16);
            this.tp_Details.Controls.Add(this.tbBussPh);
            this.tp_Details.Controls.Add(this.label15);
            this.tp_Details.Controls.Add(this.tbEmail);
            this.tp_Details.Controls.Add(this.label14);
            this.tp_Details.Controls.Add(this.tbPhoneNo);
            this.tp_Details.Controls.Add(this.label13);
            this.tp_Details.Controls.Add(this.tbZIP);
            this.tp_Details.Controls.Add(this.tbState);
            this.tp_Details.Controls.Add(this.tbCity);
            this.tp_Details.Controls.Add(this.tbAddress);
            this.tp_Details.Controls.Add(this.label12);
            this.tp_Details.Controls.Add(this.label11);
            this.tp_Details.Controls.Add(this.label10);
            this.tp_Details.Controls.Add(this.label9);
            this.tp_Details.Controls.Add(this.label8);
            this.tp_Details.Controls.Add(this.tb_LN);
            this.tp_Details.Controls.Add(this.tb_MN);
            this.tp_Details.Controls.Add(this.tb_FN);
            this.tp_Details.Controls.Add(this.label7);
            this.tp_Details.Controls.Add(this.label6);
            this.tp_Details.Controls.Add(this.label5);
            this.tp_Details.Controls.Add(this.label4);
            this.tp_Details.Location = new System.Drawing.Point(4, 22);
            this.tp_Details.Name = "tp_Details";
            this.tp_Details.Size = new System.Drawing.Size(1184, 658);
            this.tp_Details.TabIndex = 1;
            this.tp_Details.Text = "User and Registration Details";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this._lvAssignedPGIDs);
            this.groupBox1.Controls.Add(this._getAssignedPGIDs);
            this.groupBox1.Controls.Add(this.label41);
            this.groupBox1.Controls.Add(this._infusionID);
            this.groupBox1.Controls.Add(this._lvUDS);
            this.groupBox1.Controls.Add(this.getUDSStatus);
            this.groupBox1.Location = new System.Drawing.Point(669, 10);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(310, 535);
            this.groupBox1.TabIndex = 71;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "UDS Infusion";
            // 
            // _lvAssignedPGIDs
            // 
            this._lvAssignedPGIDs.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._lvAssignedPGIDs.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader2});
            this._lvAssignedPGIDs.ContextMenuStrip = this._ctxAssignedPGID;
            this._lvAssignedPGIDs.Location = new System.Drawing.Point(16, 349);
            this._lvAssignedPGIDs.Name = "_lvAssignedPGIDs";
            this._lvAssignedPGIDs.Size = new System.Drawing.Size(289, 177);
            this._lvAssignedPGIDs.TabIndex = 67;
            this._lvAssignedPGIDs.UseCompatibleStateImageBehavior = false;
            this._lvAssignedPGIDs.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Assigned PG ID";
            this.columnHeader2.Width = 301;
            // 
            // _ctxAssignedPGID
            // 
            this._ctxAssignedPGID.ImageScalingSize = new System.Drawing.Size(20, 20);
            this._ctxAssignedPGID.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._deletePGID});
            this._ctxAssignedPGID.Name = "_ctxAssignedPGID";
            this._ctxAssignedPGID.Size = new System.Drawing.Size(108, 26);
            // 
            // _deletePGID
            // 
            this._deletePGID.Name = "_deletePGID";
            this._deletePGID.Size = new System.Drawing.Size(107, 22);
            this._deletePGID.Text = "Delete";
            this._deletePGID.Click += new System.EventHandler(this._deletePGID_Click);
            // 
            // _getAssignedPGIDs
            // 
            this._getAssignedPGIDs.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._getAssignedPGIDs.Location = new System.Drawing.Point(147, 319);
            this._getAssignedPGIDs.Name = "_getAssignedPGIDs";
            this._getAssignedPGIDs.Size = new System.Drawing.Size(75, 25);
            this._getAssignedPGIDs.TabIndex = 66;
            this._getAssignedPGIDs.Text = "SEARCH";
            this._getAssignedPGIDs.Click += new System.EventHandler(this._getAssignedPGIDs_Click);
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(16, 297);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(193, 13);
            this.label41.TabIndex = 65;
            this.label41.Text = "Get Assigned PG Users for Infusion ID:";
            // 
            // _infusionID
            // 
            this._infusionID.Location = new System.Drawing.Point(16, 321);
            this._infusionID.Name = "_infusionID";
            this._infusionID.Size = new System.Drawing.Size(126, 21);
            this._infusionID.TabIndex = 64;
            // 
            // _lvUDS
            // 
            this._lvUDS.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._lvUDS.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1});
            this._lvUDS.ContextMenuStrip = this._ctxAssignedInfusionID;
            this._lvUDS.Location = new System.Drawing.Point(16, 80);
            this._lvUDS.Name = "_lvUDS";
            this._lvUDS.Size = new System.Drawing.Size(289, 175);
            this._lvUDS.TabIndex = 63;
            this._lvUDS.UseCompatibleStateImageBehavior = false;
            this._lvUDS.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Assigned Infusion ID";
            this.columnHeader1.Width = 301;
            // 
            // _ctxAssignedInfusionID
            // 
            this._ctxAssignedInfusionID.ImageScalingSize = new System.Drawing.Size(20, 20);
            this._ctxAssignedInfusionID.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._updateInfusionID,
            this._deleteInfusionID,
            this._InsertInfusionID});
            this._ctxAssignedInfusionID.Name = "_ctxAssignedInfusionID";
            this._ctxAssignedInfusionID.Size = new System.Drawing.Size(131, 70);
            // 
            // _updateInfusionID
            // 
            this._updateInfusionID.Name = "_updateInfusionID";
            this._updateInfusionID.Size = new System.Drawing.Size(130, 22);
            this._updateInfusionID.Text = "Update";
            this._updateInfusionID.Click += new System.EventHandler(this._updateInfusionID_Click);
            // 
            // _deleteInfusionID
            // 
            this._deleteInfusionID.Name = "_deleteInfusionID";
            this._deleteInfusionID.Size = new System.Drawing.Size(130, 22);
            this._deleteInfusionID.Text = "Delete";
            this._deleteInfusionID.Click += new System.EventHandler(this._deleteInfusionID_Click);
            // 
            // _InsertInfusionID
            // 
            this._InsertInfusionID.Name = "_InsertInfusionID";
            this._InsertInfusionID.Size = new System.Drawing.Size(130, 22);
            this._InsertInfusionID.Text = "Insert New";
            this._InsertInfusionID.Click += new System.EventHandler(this._InsertInfusionID_Click);
            // 
            // getUDSStatus
            // 
            this.getUDSStatus.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.getUDSStatus.Location = new System.Drawing.Point(16, 22);
            this.getUDSStatus.Name = "getUDSStatus";
            this.getUDSStatus.Size = new System.Drawing.Size(289, 48);
            this.getUDSStatus.TabIndex = 62;
            this.getUDSStatus.Text = "Get Assigned InfusionId For CURRENT User";
            this.getUDSStatus.Click += new System.EventHandler(this.getUDSStatus_Click);
            // 
            // _addUSD
            // 
            this._addUSD.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._addUSD.Location = new System.Drawing.Point(407, 442);
            this._addUSD.Name = "_addUSD";
            this._addUSD.Size = new System.Drawing.Size(184, 48);
            this._addUSD.TabIndex = 70;
            this._addUSD.Text = "Add UDS Monthly Credits";
            // 
            // _nQA
            // 
            this._nQA.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._nQA.Location = new System.Drawing.Point(407, 495);
            this._nQA.Name = "_nQA";
            this._nQA.Size = new System.Drawing.Size(184, 50);
            this._nQA.TabIndex = 69;
            this._nQA.Text = "View Inputs and TopUps";
            // 
            // _chargeMM
            // 
            this._chargeMM.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._chargeMM.Location = new System.Drawing.Point(256, 241);
            this._chargeMM.Name = "_chargeMM";
            this._chargeMM.Size = new System.Drawing.Size(128, 25);
            this._chargeMM.TabIndex = 68;
            this._chargeMM.Text = "Charge MM (1 Unit)";
            this._chargeMM.Click += new System.EventHandler(this.ChargeMM);
            // 
            // label40
            // 
            this.label40.Location = new System.Drawing.Point(71, 490);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(56, 25);
            this.label40.TabIndex = 67;
            this.label40.Text = "AMOUNT";
            // 
            // _amount
            // 
            this._amount.Location = new System.Drawing.Point(134, 490);
            this._amount.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this._amount.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this._amount.Name = "_amount";
            this._amount.Size = new System.Drawing.Size(48, 21);
            this._amount.TabIndex = 66;
            this._amount.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // _comps
            // 
            this._comps.Location = new System.Drawing.Point(16, 521);
            this._comps.Name = "_comps";
            this._comps.Size = new System.Drawing.Size(64, 27);
            this._comps.TabIndex = 65;
            this._comps.Text = "COMPS";
            // 
            // _pp
            // 
            this._pp.Location = new System.Drawing.Point(18, 490);
            this._pp.Name = "_pp";
            this._pp.Size = new System.Drawing.Size(48, 26);
            this._pp.TabIndex = 64;
            this._pp.Text = "PP";
            // 
            // _mm
            // 
            this._mm.Checked = true;
            this._mm.Location = new System.Drawing.Point(18, 464);
            this._mm.Name = "_mm";
            this._mm.Size = new System.Drawing.Size(48, 26);
            this._mm.TabIndex = 63;
            this._mm.TabStop = true;
            this._mm.Text = "MM";
            // 
            // _queryDataRequest
            // 
            this._queryDataRequest.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._queryDataRequest.Location = new System.Drawing.Point(219, 495);
            this._queryDataRequest.Name = "_queryDataRequest";
            this._queryDataRequest.Size = new System.Drawing.Size(183, 50);
            this._queryDataRequest.TabIndex = 62;
            this._queryDataRequest.Text = "Query Data Request";
            this._queryDataRequest.Click += new System.EventHandler(this._queryDataRequest_Click);
            // 
            // _queryAccount
            // 
            this._queryAccount.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._queryAccount.Location = new System.Drawing.Point(219, 442);
            this._queryAccount.Name = "_queryAccount";
            this._queryAccount.Size = new System.Drawing.Size(183, 48);
            this._queryAccount.TabIndex = 61;
            this._queryAccount.Text = "Query Account";
            this._queryAccount.Click += new System.EventHandler(this._queryAccount_Click);
            // 
            // button2
            // 
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.button2.Location = new System.Drawing.Point(528, 241);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(136, 25);
            this.button2.TabIndex = 60;
            this.button2.Text = "Charge COMPS (22 Units)";
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // linkLabel2
            // 
            this.linkLabel2.Location = new System.Drawing.Point(424, 34);
            this.linkLabel2.Name = "linkLabel2";
            this.linkLabel2.Size = new System.Drawing.Size(100, 25);
            this.linkLabel2.TabIndex = 59;
            this.linkLabel2.TabStop = true;
            this.linkLabel2.Text = "My Account";
            this.linkLabel2.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel2_LinkClicked);
            // 
            // linkLabel1
            // 
            this.linkLabel1.Location = new System.Drawing.Point(320, 34);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(100, 25);
            this.linkLabel1.TabIndex = 58;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "Subscribe To Data";
            this.linkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // bCharge
            // 
            this.bCharge.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.bCharge.Location = new System.Drawing.Point(392, 241);
            this.bCharge.Name = "bCharge";
            this.bCharge.Size = new System.Drawing.Size(128, 25);
            this.bCharge.TabIndex = 57;
            this.bCharge.Text = "Charge PP (18 Units)";
            this.bCharge.Click += new System.EventHandler(this.bCharge_Click);
            // 
            // bSetDetails
            // 
            this.bSetDetails.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.bSetDetails.Location = new System.Drawing.Point(590, 277);
            this.bSetDetails.Name = "bSetDetails";
            this.bSetDetails.Size = new System.Drawing.Size(75, 25);
            this.bSetDetails.TabIndex = 56;
            this.bSetDetails.Text = "Set Details";
            this.bSetDetails.Click += new System.EventHandler(this.bSetDetails_Click);
            // 
            // tbPGID
            // 
            this.tbPGID.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbPGID.Location = new System.Drawing.Point(321, 10);
            this.tbPGID.Name = "tbPGID";
            this.tbPGID.ReadOnly = true;
            this.tbPGID.Size = new System.Drawing.Size(344, 20);
            this.tbPGID.TabIndex = 55;
            this.tbPGID.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.Location = new System.Drawing.Point(270, 13);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(45, 13);
            this.label37.TabIndex = 54;
            this.label37.Text = "PG ID:";
            // 
            // bUpdate
            // 
            this.bUpdate.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.bUpdate.Location = new System.Drawing.Point(487, 277);
            this.bUpdate.Name = "bUpdate";
            this.bUpdate.Size = new System.Drawing.Size(75, 25);
            this.bUpdate.TabIndex = 53;
            this.bUpdate.Text = "Get Details";
            this.bUpdate.Click += new System.EventHandler(this.bUpdate_Click);
            // 
            // cbMod20
            // 
            this.cbMod20.Location = new System.Drawing.Point(563, 407);
            this.cbMod20.Name = "cbMod20";
            this.cbMod20.Size = new System.Drawing.Size(62, 18);
            this.cbMod20.TabIndex = 52;
            this.cbMod20.Text = "Mod 20";
            // 
            // cbMod19
            // 
            this.cbMod19.Location = new System.Drawing.Point(563, 382);
            this.cbMod19.Name = "cbMod19";
            this.cbMod19.Size = new System.Drawing.Size(62, 19);
            this.cbMod19.TabIndex = 51;
            this.cbMod19.Text = "Mod 19";
            // 
            // cbMod18
            // 
            this.cbMod18.Location = new System.Drawing.Point(563, 358);
            this.cbMod18.Name = "cbMod18";
            this.cbMod18.Size = new System.Drawing.Size(62, 18);
            this.cbMod18.TabIndex = 50;
            this.cbMod18.Text = "Mod 18";
            // 
            // cbMod17
            // 
            this.cbMod17.Location = new System.Drawing.Point(563, 333);
            this.cbMod17.Name = "cbMod17";
            this.cbMod17.Size = new System.Drawing.Size(62, 18);
            this.cbMod17.TabIndex = 49;
            this.cbMod17.Text = "Mod 17";
            // 
            // cbMod16
            // 
            this.cbMod16.Location = new System.Drawing.Point(563, 308);
            this.cbMod16.Name = "cbMod16";
            this.cbMod16.Size = new System.Drawing.Size(62, 18);
            this.cbMod16.TabIndex = 48;
            this.cbMod16.Text = "Mod 16";
            // 
            // cbMod15
            // 
            this.cbMod15.Location = new System.Drawing.Point(394, 407);
            this.cbMod15.Name = "cbMod15";
            this.cbMod15.Size = new System.Drawing.Size(62, 18);
            this.cbMod15.TabIndex = 47;
            this.cbMod15.Text = "Mod 15";
            // 
            // cbMod14
            // 
            this.cbMod14.Location = new System.Drawing.Point(394, 382);
            this.cbMod14.Name = "cbMod14";
            this.cbMod14.Size = new System.Drawing.Size(62, 19);
            this.cbMod14.TabIndex = 46;
            this.cbMod14.Text = "Mod 14";
            // 
            // cbMod13
            // 
            this.cbMod13.Location = new System.Drawing.Point(394, 358);
            this.cbMod13.Name = "cbMod13";
            this.cbMod13.Size = new System.Drawing.Size(62, 18);
            this.cbMod13.TabIndex = 45;
            this.cbMod13.Text = "Mod 13";
            // 
            // cbMod12
            // 
            this.cbMod12.Location = new System.Drawing.Point(394, 333);
            this.cbMod12.Name = "cbMod12";
            this.cbMod12.Size = new System.Drawing.Size(62, 18);
            this.cbMod12.TabIndex = 44;
            this.cbMod12.Text = "Mod 12";
            // 
            // cbMod11
            // 
            this.cbMod11.Location = new System.Drawing.Point(394, 308);
            this.cbMod11.Name = "cbMod11";
            this.cbMod11.Size = new System.Drawing.Size(62, 18);
            this.cbMod11.TabIndex = 43;
            this.cbMod11.Text = "Mod 11";
            // 
            // cbMod10
            // 
            this.cbMod10.AutoSize = true;
            this.cbMod10.Location = new System.Drawing.Point(219, 407);
            this.cbMod10.Name = "cbMod10";
            this.cbMod10.Size = new System.Drawing.Size(170, 17);
            this.cbMod10.TabIndex = 42;
            this.cbMod10.Text = "MOD10-NoZIPFilter INTERNAL";
            // 
            // cbMod9
            // 
            this.cbMod9.Location = new System.Drawing.Point(219, 382);
            this.cbMod9.Name = "cbMod9";
            this.cbMod9.Size = new System.Drawing.Size(165, 19);
            this.cbMod9.TabIndex = 41;
            this.cbMod9.Text = "MOD 9 - Int. Comm. Mod.";
            // 
            // cbMod8
            // 
            this.cbMod8.Location = new System.Drawing.Point(219, 358);
            this.cbMod8.Name = "cbMod8";
            this.cbMod8.Size = new System.Drawing.Size(173, 18);
            this.cbMod8.TabIndex = 40;
            this.cbMod8.Text = "MOD 8 - Int. Short Sale Mod.";
            // 
            // cbMod7
            // 
            this.cbMod7.Location = new System.Drawing.Point(219, 333);
            this.cbMod7.Name = "cbMod7";
            this.cbMod7.Size = new System.Drawing.Size(149, 18);
            this.cbMod7.TabIndex = 39;
            this.cbMod7.Text = "Mod 7 - 123Sold";
            // 
            // cbMod6
            // 
            this.cbMod6.Location = new System.Drawing.Point(219, 308);
            this.cbMod6.Name = "cbMod6";
            this.cbMod6.Size = new System.Drawing.Size(157, 18);
            this.cbMod6.TabIndex = 38;
            this.cbMod6.Text = "Mod 6 - Network Version";
            // 
            // cbMod5
            // 
            this.cbMod5.Location = new System.Drawing.Point(27, 407);
            this.cbMod5.Name = "cbMod5";
            this.cbMod5.Size = new System.Drawing.Size(147, 18);
            this.cbMod5.TabIndex = 37;
            this.cbMod5.Text = "Mod 5 - Matching Module";
            // 
            // cbMod4
            // 
            this.cbMod4.Location = new System.Drawing.Point(27, 382);
            this.cbMod4.Name = "cbMod4";
            this.cbMod4.Size = new System.Drawing.Size(101, 19);
            this.cbMod4.TabIndex = 36;
            this.cbMod4.Text = "Mod 4 - Deal IQ";
            // 
            // cbMod3
            // 
            this.cbMod3.Location = new System.Drawing.Point(27, 358);
            this.cbMod3.Name = "cbMod3";
            this.cbMod3.Size = new System.Drawing.Size(147, 18);
            this.cbMod3.TabIndex = 35;
            this.cbMod3.Text = "Mod 3 - Get Forms Ready";
            // 
            // cbMod2
            // 
            this.cbMod2.Location = new System.Drawing.Point(27, 333);
            this.cbMod2.Name = "cbMod2";
            this.cbMod2.Size = new System.Drawing.Size(103, 18);
            this.cbMod2.TabIndex = 34;
            this.cbMod2.Text = "Mod 2 - COMPS";
            // 
            // cbMod1
            // 
            this.cbMod1.Location = new System.Drawing.Point(27, 308);
            this.cbMod1.Name = "cbMod1";
            this.cbMod1.Size = new System.Drawing.Size(122, 18);
            this.cbMod1.TabIndex = 33;
            this.cbMod1.Text = "Mod 1 - Prop. Profile";
            // 
            // cbMod0
            // 
            this.cbMod0.Location = new System.Drawing.Point(27, 283);
            this.cbMod0.Name = "cbMod0";
            this.cbMod0.Size = new System.Drawing.Size(113, 19);
            this.cbMod0.TabIndex = 32;
            this.cbMod0.Text = "Mod 0 - PG Active";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(19, 249);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(54, 13);
            this.label21.TabIndex = 31;
            this.label21.Text = "Modules";
            // 
            // tbActDate
            // 
            this.tbActDate.Location = new System.Drawing.Point(99, 68);
            this.tbActDate.Name = "tbActDate";
            this.tbActDate.ReadOnly = true;
            this.tbActDate.Size = new System.Drawing.Size(135, 21);
            this.tbActDate.TabIndex = 30;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(24, 70);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(57, 13);
            this.label20.TabIndex = 29;
            this.label20.Text = "Act. Date:";
            // 
            // tbSerialNr
            // 
            this.tbSerialNr.Location = new System.Drawing.Point(99, 39);
            this.tbSerialNr.Name = "tbSerialNr";
            this.tbSerialNr.ReadOnly = true;
            this.tbSerialNr.Size = new System.Drawing.Size(135, 21);
            this.tbSerialNr.TabIndex = 28;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(24, 42);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(51, 13);
            this.label19.TabIndex = 27;
            this.label19.Text = "Serial Nr:";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(16, 13);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(118, 13);
            this.label18.TabIndex = 26;
            this.label18.Text = "Registration Details";
            // 
            // tbCompany
            // 
            this.tbCompany.Location = new System.Drawing.Point(306, 204);
            this.tbCompany.Name = "tbCompany";
            this.tbCompany.Size = new System.Drawing.Size(361, 21);
            this.tbCompany.TabIndex = 25;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(252, 208);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(56, 13);
            this.label17.TabIndex = 24;
            this.label17.Text = "Company:";
            // 
            // tbCellNo
            // 
            this.tbCellNo.Location = new System.Drawing.Point(487, 146);
            this.tbCellNo.Name = "tbCellNo";
            this.tbCellNo.Size = new System.Drawing.Size(180, 21);
            this.tbCellNo.TabIndex = 23;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(423, 150);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(51, 13);
            this.label16.TabIndex = 22;
            this.label16.Text = "Cell. Ph.:";
            // 
            // tbBussPh
            // 
            this.tbBussPh.Location = new System.Drawing.Point(487, 118);
            this.tbBussPh.Name = "tbBussPh";
            this.tbBussPh.Size = new System.Drawing.Size(180, 21);
            this.tbBussPh.TabIndex = 21;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(423, 122);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(56, 13);
            this.label15.TabIndex = 20;
            this.label15.Text = "Buss. Ph.:";
            // 
            // tbEmail
            // 
            this.tbEmail.Location = new System.Drawing.Point(306, 176);
            this.tbEmail.Name = "tbEmail";
            this.tbEmail.Size = new System.Drawing.Size(361, 21);
            this.tbEmail.TabIndex = 19;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(252, 179);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(35, 13);
            this.label14.TabIndex = 18;
            this.label14.Text = "Email:";
            // 
            // tbPhoneNo
            // 
            this.tbPhoneNo.Location = new System.Drawing.Point(487, 92);
            this.tbPhoneNo.Name = "tbPhoneNo";
            this.tbPhoneNo.Size = new System.Drawing.Size(180, 21);
            this.tbPhoneNo.TabIndex = 17;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(423, 95);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(57, 13);
            this.label13.TabIndex = 16;
            this.label13.Text = "Phone No:";
            // 
            // tbZIP
            // 
            this.tbZIP.Location = new System.Drawing.Point(306, 146);
            this.tbZIP.Name = "tbZIP";
            this.tbZIP.Size = new System.Drawing.Size(111, 21);
            this.tbZIP.TabIndex = 15;
            // 
            // tbState
            // 
            this.tbState.Location = new System.Drawing.Point(306, 118);
            this.tbState.Name = "tbState";
            this.tbState.Size = new System.Drawing.Size(111, 21);
            this.tbState.TabIndex = 14;
            // 
            // tbCity
            // 
            this.tbCity.Location = new System.Drawing.Point(306, 90);
            this.tbCity.Name = "tbCity";
            this.tbCity.Size = new System.Drawing.Size(111, 21);
            this.tbCity.TabIndex = 13;
            // 
            // tbAddress
            // 
            this.tbAddress.Location = new System.Drawing.Point(306, 62);
            this.tbAddress.Name = "tbAddress";
            this.tbAddress.Size = new System.Drawing.Size(361, 21);
            this.tbAddress.TabIndex = 12;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(252, 150);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(27, 13);
            this.label12.TabIndex = 11;
            this.label12.Text = "ZIP:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(252, 122);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(37, 13);
            this.label11.TabIndex = 10;
            this.label11.Text = "State:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(252, 94);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(30, 13);
            this.label10.TabIndex = 9;
            this.label10.Text = "City:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(252, 66);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(50, 13);
            this.label9.TabIndex = 8;
            this.label9.Text = "Address:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(235, 37);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(52, 13);
            this.label8.TabIndex = 7;
            this.label8.Text = "Address";
            // 
            // tb_LN
            // 
            this.tb_LN.Location = new System.Drawing.Point(99, 214);
            this.tb_LN.Name = "tb_LN";
            this.tb_LN.Size = new System.Drawing.Size(135, 21);
            this.tb_LN.TabIndex = 6;
            // 
            // tb_MN
            // 
            this.tb_MN.Location = new System.Drawing.Point(99, 184);
            this.tb_MN.Name = "tb_MN";
            this.tb_MN.Size = new System.Drawing.Size(135, 21);
            this.tb_MN.TabIndex = 5;
            // 
            // tb_FN
            // 
            this.tb_FN.Location = new System.Drawing.Point(99, 151);
            this.tb_FN.Name = "tb_FN";
            this.tb_FN.Size = new System.Drawing.Size(135, 21);
            this.tb_FN.TabIndex = 4;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(24, 214);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(61, 13);
            this.label7.TabIndex = 3;
            this.label7.Text = "Last Name:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(24, 184);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(71, 13);
            this.label6.TabIndex = 2;
            this.label6.Text = "Middle Name:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(24, 154);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(62, 13);
            this.label5.TabIndex = 1;
            this.label5.Text = "First Name:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(16, 126);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(76, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "User Details";
            // 
            // tp_Comps
            // 
            this.tp_Comps.Controls.Add(this.userControlPropertyProfileCompsListView1);
            this.tp_Comps.Location = new System.Drawing.Point(4, 22);
            this.tp_Comps.Name = "tp_Comps";
            this.tp_Comps.Size = new System.Drawing.Size(1184, 658);
            this.tp_Comps.TabIndex = 3;
            this.tp_Comps.Text = "Comps";
            // 
            // userControlPropertyProfileCompsListView1
            // 
            this.userControlPropertyProfileCompsListView1.DBConn = null;
            this.userControlPropertyProfileCompsListView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.userControlPropertyProfileCompsListView1.Location = new System.Drawing.Point(0, 0);
            this.userControlPropertyProfileCompsListView1.Name = "userControlPropertyProfileCompsListView1";
            this.userControlPropertyProfileCompsListView1.Size = new System.Drawing.Size(1184, 658);
            this.userControlPropertyProfileCompsListView1.TabIndex = 0;
            // 
            // tp_PP
            // 
            this.tp_PP.Controls.Add(this.userControlPropertyProfileComps1);
            this.tp_PP.Location = new System.Drawing.Point(4, 22);
            this.tp_PP.Name = "tp_PP";
            this.tp_PP.Size = new System.Drawing.Size(1184, 658);
            this.tp_PP.TabIndex = 4;
            this.tp_PP.Text = "PropertyProfile";
            // 
            // userControlPropertyProfileComps1
            // 
            this.userControlPropertyProfileComps1.AutoSize = true;
            this.userControlPropertyProfileComps1.DBConn = null;
            this.userControlPropertyProfileComps1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.userControlPropertyProfileComps1.Location = new System.Drawing.Point(0, 0);
            this.userControlPropertyProfileComps1.Name = "userControlPropertyProfileComps1";
            this.userControlPropertyProfileComps1.Size = new System.Drawing.Size(1184, 658);
            this.userControlPropertyProfileComps1.TabIndex = 0;
            // 
            // tp_FloodComps
            // 
            this.tp_FloodComps.Controls.Add(this.floodCnt);
            this.tp_FloodComps.Controls.Add(this.flood);
            this.tp_FloodComps.Controls.Add(this.flood_result);
            this.tp_FloodComps.Location = new System.Drawing.Point(4, 22);
            this.tp_FloodComps.Name = "tp_FloodComps";
            this.tp_FloodComps.Size = new System.Drawing.Size(1184, 658);
            this.tp_FloodComps.TabIndex = 5;
            this.tp_FloodComps.Text = "Flood Comps";
            // 
            // floodCnt
            // 
            this.floodCnt.Location = new System.Drawing.Point(112, 9);
            this.floodCnt.Name = "floodCnt";
            this.floodCnt.Size = new System.Drawing.Size(120, 21);
            this.floodCnt.TabIndex = 2;
            this.floodCnt.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // flood
            // 
            this.flood.Location = new System.Drawing.Point(8, 9);
            this.flood.Name = "flood";
            this.flood.Size = new System.Drawing.Size(75, 24);
            this.flood.TabIndex = 1;
            this.flood.Text = "Flood";
            this.flood.Click += new System.EventHandler(this.flood_Click);
            // 
            // flood_result
            // 
            this.flood_result.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flood_result.Location = new System.Drawing.Point(8, 43);
            this.flood_result.Multiline = true;
            this.flood_result.Name = "flood_result";
            this.flood_result.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.flood_result.Size = new System.Drawing.Size(1158, 542);
            this.flood_result.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 689);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(121, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "PG Web Service Status:";
            // 
            // tbStatus
            // 
            this.tbStatus.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbStatus.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbStatus.Location = new System.Drawing.Point(138, 687);
            this.tbStatus.Name = "tbStatus";
            this.tbStatus.ReadOnly = true;
            this.tbStatus.Size = new System.Drawing.Size(1066, 21);
            this.tbStatus.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(482, 718);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(139, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "For details click on textbox.";
            // 
            // bStartIE
            // 
            this.bStartIE.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.bStartIE.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.bStartIE.Location = new System.Drawing.Point(1100, 718);
            this.bStartIE.Name = "bStartIE";
            this.bStartIE.Size = new System.Drawing.Size(104, 25);
            this.bStartIE.TabIndex = 6;
            this.bStartIE.Text = "Start IE";
            this.bStartIE.Click += new System.EventHandler(this.bStartIE_Click);
            // 
            // rbWS
            // 
            this.rbWS.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.rbWS.Checked = true;
            this.rbWS.Location = new System.Drawing.Point(965, 718);
            this.rbWS.Name = "rbWS";
            this.rbWS.Size = new System.Drawing.Size(40, 26);
            this.rbWS.TabIndex = 7;
            this.rbWS.TabStop = true;
            this.rbWS.Text = "WS";
            this.rbWS.Visible = false;
            this.rbWS.CheckedChanged += new System.EventHandler(this.rbWS_CheckedChanged);
            // 
            // rbWS3
            // 
            this.rbWS3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.rbWS3.Location = new System.Drawing.Point(1022, 718);
            this.rbWS3.Name = "rbWS3";
            this.rbWS3.Size = new System.Drawing.Size(48, 26);
            this.rbWS3.TabIndex = 8;
            this.rbWS3.Text = "WS3";
            this.rbWS3.Visible = false;
            this.rbWS3.CheckedChanged += new System.EventHandler(this.rbWS3_CheckedChanged);
            // 
            // label39
            // 
            this.label39.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label39.Location = new System.Drawing.Point(16, 718);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(176, 25);
            this.label39.TabIndex = 9;
            this.label39.Text = "Curr. selected User and Reg Key:";
            // 
            // tbUserRegKey
            // 
            this.tbUserRegKey.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.tbUserRegKey.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbUserRegKey.Location = new System.Drawing.Point(198, 715);
            this.tbUserRegKey.Name = "tbUserRegKey";
            this.tbUserRegKey.ReadOnly = true;
            this.tbUserRegKey.Size = new System.Drawing.Size(278, 21);
            this.tbUserRegKey.TabIndex = 10;
            // 
            // _saveUserInfo
            // 
            this._saveUserInfo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._saveUserInfo.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._saveUserInfo.Location = new System.Drawing.Point(16, 656);
            this._saveUserInfo.Name = "_saveUserInfo";
            this._saveUserInfo.Size = new System.Drawing.Size(1188, 25);
            this._saveUserInfo.TabIndex = 11;
            this._saveUserInfo.Text = "Save User Info";
            // 
            // Form1
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 14);
            this.ClientSize = new System.Drawing.Size(1215, 749);
            this.Controls.Add(this._saveUserInfo);
            this.Controls.Add(this.tbUserRegKey);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.tbStatus);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label39);
            this.Controls.Add(this.rbWS3);
            this.Controls.Add(this.rbWS);
            this.Controls.Add(this.bStartIE);
            this.Controls.Add(this.tc_WS_Data);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ProfitGrabber Web Manager";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tc_WS_Data.ResumeLayout(false);
            this.tp_RegisteredUsers.ResumeLayout(false);
            this.tp_RegisteredUsers.PerformLayout();
            this.tp_RegisterUser.ResumeLayout(false);
            this.tp_RegisterUser.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.floatingCentralPanel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rightDockFixedRightPanel)).EndInit();
            this.rightDockFixedRightPanel.ResumeLayout(false);
            this.rightDockFixedRightPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.flyoutPanel)).EndInit();
            this.flyoutPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.flyoutPanelControl1)).EndInit();
            this.flyoutPanelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.calendarControl1.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            this.groupControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            this.tp_Details.ResumeLayout(false);
            this.tp_Details.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this._ctxAssignedPGID.ResumeLayout(false);
            this._ctxAssignedInfusionID.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._amount)).EndInit();
            this.tp_Comps.ResumeLayout(false);
            this.tp_PP.ResumeLayout(false);
            this.tp_PP.PerformLayout();
            this.tp_FloodComps.ResumeLayout(false);
            this.tp_FloodComps.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.floodCnt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.behaviorManager1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}

        #endregion

        string _isURI = string.Empty;

        public Form1()
        {
			//SelectServer s = new SelectServer();

			//s.ShowDialog();

			Globals.URL_Registration = ConfigurationManager.AppSettings["REG"]; //s.server+"/ServiceRegistration.asmx";
            Globals.URL_DataComsumption = ConfigurationManager.AppSettings["SDC"]; //s.server+"/ServiceDataConsumption.asmx";
            _isURI = ConfigurationManager.AppSettings["UDSAPI"];

            InitializeComponent();
            this.ws = new Service1();			
			ws.Timeout = 90000;
            
			
			//SwapWS();
			
			
			this.AddEventHandlers();
            
            //Add checker threads
            wsChecker_thdst = new ThreadStart(TestWsStatus);
            wsChecker_thd = new Thread(wsChecker_thdst);
            wsChecker_thd.Start();            
        }
		
		[STAThread]
		static void Main()
		{
            //DevExpress.XtraEditors.WindowsFormsSettings.ScrollUIMode = DevExpress.XtraEditors.ScrollUIMode.Fluent;
			Application.EnableVisualStyles();
            Application.Run(new Form1());		

            //DX.UC.ucUsers.Instance.Subscribe();
            //RegisteredUsers.Instance.StartSynchronizationAsync();
            //var fdf = new FluentDesignForm1();
            //fdf.ShowUsersControl();
            //Application.Run(fdf);
        }

		

		protected void OnDisplayMessage(string msg)
		{
			if (null != SET_RESULT_TEXT)
			{                
				SET_RESULT_TEXT(msg);
			}
		}

		

        protected override void OnClosing(CancelEventArgs e)
        {
            Globals.TerminateThreads = true;
            base.OnClosing(e);
        }

        private void AddEventHandlers()
        {
            this.tbStatus.Click += new EventHandler(tbStatus_Click);
            this.bRegister.Click += new EventHandler(bRegister_Click);
			this.SET_RESULT_TEXT += new SetResultText(SET_TEXT_EH);
			_saveUserInfo.Click += new EventHandler(_saveUserInfo_Click);
            _nQA.Click += new EventHandler(On_nQA_Click);
            _addUSD.Click += new EventHandler(On_addUSD_Click);
        }
        
        
		void SET_TEXT_EH(string message)
		{
			SetResultText_(message);
		}

		private void SetResultText_(string text)
		{
			if (this.tbResult.InvokeRequired)
			{
				SetResultTextCallback d = new SetResultTextCallback(SetResultText_);
				this.Invoke(d, new object[] { text });
			}
			else
			{
				GetResultText_();
				this.tbResult.Text = Globals.tempMsg + System.Environment.NewLine + text;
			}
		}
		private void GetResultText_()
		{
			if (this.tbResult.InvokeRequired)
			{
				GetResultTextCallback d = new GetResultTextCallback(GetResultText_);
				this.Invoke(d);
			}
			else
			{
				Globals.tempMsg = this.tbResult.Text;
			}
		}




        void tbStatus_Click(object sender, EventArgs e)
        {
            MessageBox.Show(this.tbStatus.Text);
        }

        private void TestWsStatus()
        {
            while (false == Globals.TerminateThreads)
            {
                try
                {
                    this.ClearText();
                    this.SetText(ws.Url.ToString() + " " +  ws.TestIfAlive());
                }
                catch(Exception exc)
                {
                    this.SetText(exc.ToString());
                }

                if (true == Globals.TerminateThreads)
                    break;

                try
                {
                    this.SetText(", " + ws.TestConnection());
                }
                catch (Exception exc)
                {
                    this.SetText(", " + exc.ToString());
                }

                if (true == Globals.TerminateThreads)
                    break;

                Thread.Sleep(5000);  
            }
        }

        private void ClearText()
        {
            if (this.tbStatus.InvokeRequired)
            {
                ClearTextCallback d = new ClearTextCallback(ClearText);
                this.Invoke(d);
            }
            else
            {
                this.tbStatus.Text = string.Empty;
            }
        }

        private void SetText(string text)
        {
            // InvokeRequired required compares the thread ID of the
            // calling thread to the thread ID of the creating thread.
            // If these threads are different, it returns true.
            if (this.tbStatus.InvokeRequired)
            {
                SetTextCallback d = new SetTextCallback(SetText);
                this.Invoke(d, new object[] { text });
            }
            else
            {
                this.tbStatus.Text += text;
            }
        }

		private void RegisterSingleUser(string keyCode)
		{
			try
			{
				if (this.tb_Reg_FN.InvokeRequired)
				{
					GetTextCallback d = new GetTextCallback(RegisterSingleUser);
					this.Invoke(d, new object[] { keyCode });
				}
				else
				{
					return;
				}


				OnDisplayMessage(keyCode + ": WAITING... " + DateTime.Now.Hour + ":" + DateTime.Now.Minute + ":" + DateTime.Now.Second + ":" + DateTime.Now.Millisecond);
				WS_REG.User u = new WS_REG.User();
				u.FirstName = this.tb_Reg_FN.Text;
				u.MiddleName = this.tb_Reg_MN.Text;
				u.LastName = this.tb_Reg_LN.Text;

				u.Address = new WS_REG.Address();
				u.Address.SiteCity = this.tb_Reg_City.Text;
				u.Address.FullSiteStreetNumber = this.tb_Reg_Addr.Text;
				u.Address.SiteState = this.tb_Reg_State.Text;
				u.Address.SiteZIP = this.tb_Reg_ZIP.Text;
				u.Address.EMail = this.tb_Reg_Email.Text;
				u.Address.Company = this.tb_Reg_Company.Text;
				u.Address.PhoneBusiness = this.tb_Reg_Buss_Ph.Text;
				u.Address.PhoneCell = this.tb_Reg_CellPh.Text;
				u.Address.SiteCitySiteStateSiteZIP = this.tb_Reg_PhoneNo.Text;

				string msg = string.Empty;
				IList ilRes = null;

				try
				{
					WS_REG.Registration r = new WS_REG.Registration();
					r.KeyCode = keyCode;
					ilRes = this.ws.RegisterUser(u, r, out msg);
				}
				catch (Exception exc)
				{
					OnDisplayMessage(keyCode + ": " + exc.Message);
					return;
				}
                
				if (null != ilRes && ilRes.Count > 0)
				{
					if (null != ilRes[0])
					{
						OnDisplayMessage(keyCode + ": " + (string)ilRes[0]);
					}
					if (ilRes.Count == 1)
						return;

					if (null != ilRes[1])
					{
						WS_REG.User usr = (WS_REG.User)ilRes[1];
						/*string text = string.Empty;
						if (null != usr)
						{
							text = System.Environment.NewLine +
								usr.FirstName + ", " + usr.MiddleName + ", " + usr.LastName;
						}

						if (null != usr.Address)
						{
							Text += System.Environment.NewLine +
								usr.Address.FullSiteStreetNumber + ", " +
								usr.Address.SiteCity + ", " +
								usr.Address.SiteState + ", " +
								usr.Address.SiteZIP + ", " +
								usr.Address.SiteCitySiteStateSiteZIP + ", " +
								usr.Address.EMail + ", " +
								usr.Address.Company + ", " +
								usr.Address.PhoneBusiness + ", " +
								usr.Address.PhoneCell;
						}*/
						//OnDisplayMessage(keyCode + " " + text);
					}
				}
			}
			catch (Exception exc)
			{
				OnDisplayMessage(keyCode + ": " + exc.Message);
			}
		}


        void bRegister_Click(object sender, EventArgs e)
        {
            try
            {
                this.tbResult.Text = "WAITING...";
                WS_REG.User u = new WS_REG.User();
                u.FirstName = this.tb_Reg_FN.Text;
                u.MiddleName = this.tb_Reg_MN.Text;
                u.LastName = this.tb_Reg_LN.Text;

                u.Address = new WS_REG.Address();
                u.Address.SiteCity = this.tb_Reg_City.Text;
                u.Address.FullSiteStreetNumber = this.tb_Reg_Addr.Text;
                u.Address.SiteState = this.tb_Reg_State.Text;
                u.Address.SiteZIP = this.tb_Reg_ZIP.Text;
                u.Address.EMail = this.tb_Reg_Email.Text;
                u.Address.Company = this.tb_Reg_Company.Text;
                u.Address.PhoneBusiness = this.tb_Reg_Buss_Ph.Text;
                u.Address.PhoneCell = this.tb_Reg_CellPh.Text;
                u.Address.SiteCitySiteStateSiteZIP = this.tb_Reg_PhoneNo.Text;

                string msg = string.Empty;
				WS_REG.Registration r = new WS_REG.Registration();
				r.KeyCode = this.tb_Reg_Serial.Text;
                IList ilRes = this.ws.RegisterUser(u, r , out msg);

                this.tbResult.Text += msg;
                if (null != ilRes && ilRes.Count > 0)
                {
                    if (null != ilRes[0])
                    {
                        this.tbResult.Text += (string)ilRes[0];
                    }
                    if (ilRes.Count == 1)
                        return;

                    if (null != ilRes[1])
                    {
                        WS_REG.User usr = (WS_REG.User)ilRes[1];
                        if (null != usr)
                        {
                        this.tbResult.Text += System.Environment.NewLine +
                                usr.FirstName + ", " + usr.MiddleName + ", " + usr.LastName;
                        }

                        if (null != usr.Address)
                        {
                            this.tbResult.Text += System.Environment.NewLine +
                                usr.Address.FullSiteStreetNumber + ", " +
                                usr.Address.SiteCity + ", " +
                                usr.Address.SiteState + ", " +
                                usr.Address.SiteZIP + ", " +
                                usr.Address.SiteCitySiteStateSiteZIP + ", " +
                                usr.Address.EMail + ", " +
                                usr.Address.Company + ", " +
                                usr.Address.PhoneBusiness + ", " +
                                usr.Address.PhoneCell;
                        }
                    }
                }
            }
            catch (Exception exc)
            {
                this.tbResult.Text = exc.ToString();
            }            
        }

		private void bAdd_Click(object sender, EventArgs e)
		{
			this.lvSerials.Items.Add(new ListViewItem(tbSerialThd.Text));
			this.tbSerialThd.Clear();
		}

		private void bClear_Click(object sender, EventArgs e)
		{
			this.lvSerials.Items.Clear();
		}

		private void lvUsers_SelectedIndexChanged(object sender, EventArgs e)
		{
			
		}

		private void bRegisterThd_Click(object sender, EventArgs e)
		{
			Globals.alCnt = new ArrayList();
			Globals.alKeys = new ArrayList();
			ArrayList alThreads = new ArrayList();
			ThreadStart thd_st = new ThreadStart(RegMultipleUser);
            
			foreach (ListViewItem lvi in this.lvSerials.Items)
			{                
				Thread thd_RegisterMultiple = new Thread(thd_st);
				alThreads.Add(thd_RegisterMultiple);

				Globals.alKeys.Add(lvi.Text);
			}
            

			foreach (Thread t in alThreads)
			{                
				t.Start();
			}
            
		}

		private void RegMultipleUser()
		{
			try
			{
				int iTemp = -1;
				lock (Globals.alCnt)
				{
					Globals.alCnt.Add(1);
					iTemp = Globals.alCnt.Count - 1;
				}
				string regKey = string.Empty;
				lock (this.lvSerials)
				{                      
					regKey = (string)Globals.alKeys[iTemp];
				}
				RegisterSingleUser(regKey);
			}
			catch (Exception e)
			{
				string s = e.ToString();
			}
		}


		private void bClearAnswer_Click(object sender, EventArgs e)
		{
			this.tbResult.Clear();
		}

        ShortUserInfo[] sui = null;
		private void bRefresh_Click(object sender, EventArgs e)
		{
			try
			{
				this.lvUsers.Items.Clear();

				this.tbRefresh.Text = string.Empty;
				string msg = string.Empty;
                string testConn = ws.TestConnection();
				sui = ws.GetAllUsers("!aD0420", out msg);

				if (null != sui && sui.Length > 0)
				{
					for (int i = 0; i < sui.Length; i++)
					{
						ShortUserInfo usrInfo = (ShortUserInfo)sui[i];

						ListViewItem lvi = new ListViewItem(usrInfo.FirstName);
						lvi.SubItems.Add(usrInfo.MiddleName);
						lvi.SubItems.Add(usrInfo.LastName);
						lvi.SubItems.Add(usrInfo.SerialNumber);
						lvi.SubItems.Add(usrInfo.ActivationDate.ToString());
						lvi.SubItems.Add(usrInfo.UserId.ToString());
						lvi.Tag = usrInfo.RegistrationId;						
						this.lvUsers.Items.Add(lvi);

					}
				}
				
				this.tbTotal.Text = this.lvUsers.Items.Count.ToString();
				
			}
			catch (Exception  exc)
			{
				this.tbRefresh.Text = exc.Message;
			}
		}



		void DumpShortUserInfo()
		{
			try
			{								
				string msg;
				ShortUserInfo[] sui = ws.GetAllUsers("!aD0420", out msg);
				TextWriter dump = new StreamWriter(Application.StartupPath + @"\Users.csv");

				dump.Write("COUNTER" + ",");
				dump.Write("FIRST NAME" + ",");
				dump.Write("MIDDLE NAME" + ",");
				dump.Write("LAST NAME"+ ",");
				dump.Write("SERIAL NUMBER"+ ",");
				dump.Write("ACTIVATION DATE" + ",");
				dump.Write("USER ID" + ",");
				dump.WriteLine();

				if (null != sui && sui.Length > 0)
				{
					for (int i = 0; i < sui.Length; i++)
					{
						ShortUserInfo usrInfo = (ShortUserInfo)sui[i];
						dump.Write(i.ToString() + ",");
						dump.Write(usrInfo.FirstName + ",");
						dump.Write(usrInfo.MiddleName + ",");
						dump.Write(usrInfo.LastName + ",");
						dump.Write(usrInfo.SerialNumber + ",");
						dump.Write(usrInfo.ActivationDate.ToString() + ",");
						dump.Write(usrInfo.UserId.ToString() + ",");
						dump.WriteLine();
																																				
					}
				}
				dump.Flush();
				dump.Close();
				
				MessageBox.Show("Saved to " + Application.StartupPath + @"\Users.csv");
			}
			catch (Exception  exc)
			{
				MessageBox.Show(exc.Message);
			}
		}


		private void bRegister_Click_1(object sender, EventArgs e)
		{
			this.RegisterSingleUser(this.tb_Reg_Serial.Text);   
		}

		private void bUpdate_Click(object sender, EventArgs e)
		{
			if (Guid.Empty == this.currentUserReg)
			{
				MessageBox.Show("Select User first");
				return;
			}

			WS_REG.User u = null;
			string msg = string.Empty;
			try
			{
				//u = ws.GetUser(this.currentUserReg, out msg);
				u = ws.GetUser(this.currentUserId, out msg);
			}
			catch (Exception exc)
			{
				msg = exc.Message;
			}
			finally
			{
				if (string.Empty != msg)
				{
					MessageBox.Show(msg);
				}
			}

			if (null == u || null == u.Address)
			{
				return;
			}

			this.tbPGID.Text = u.IDUser.ToString();

			tb_FN.Text = u.FirstName;
			tb_MN.Text = u.MiddleName;
			tb_LN.Text = u.LastName;

			this.tbAddress.Text = u.Address.FullSiteStreetNumber;
			this.tbCity.Text = u.Address.SiteCity;
			this.tbState.Text = u.Address.SiteState;
			this.tbZIP.Text = u.Address.SiteZIP;
			this.tbPhoneNo.Text = u.Address.SiteCitySiteStateSiteZIP;
			this.tbBussPh.Text = u.Address.PhoneBusiness;
			this.tbCellNo.Text = u.Address.PhoneCell;
			this.tbEmail.Text = u.Address.EMail;
			this.tbCompany.Text = u.Address.Company;


			ActiveModules am = null;
			try
			{
				//am = ws.GetActiveModules(this.currentUserReg, out msg);
				am = ws.GetActiveModules(this.currentUserId, out msg);
				
			}
			catch (Exception exc)
			{
				msg = exc.Message;
			}
			finally
			{
				if (string.Empty != msg)
				{
					MessageBox.Show(msg);
				}
			}

			if (null == am)
			{
				return;
			}

			this.cbMod0.Checked = am.MOD_0;
			this.cbMod1.Checked = am.MOD_1;
			this.cbMod2.Checked = am.MOD_2;
			this.cbMod3.Checked = am.MOD_3;
			this.cbMod4.Checked = am.MOD_4;
			this.cbMod5.Checked = am.MOD_5;
			this.cbMod6.Checked = am.MOD_6;
			this.cbMod7.Checked = am.MOD_7;
			this.cbMod8.Checked = am.MOD_8;
			this.cbMod9.Checked = am.MOD_9;
			this.cbMod10.Checked = am.MOD_10;
			this.cbMod11.Checked = am.MOD_11;
			this.cbMod12.Checked = am.MOD_12;
			this.cbMod13.Checked = am.MOD_13;
			this.cbMod14.Checked = am.MOD_14;
			this.cbMod15.Checked = am.MOD_15;
			this.cbMod16.Checked = am.MOD_16;
			this.cbMod17.Checked = am.MOD_17;
			this.cbMod18.Checked = am.MOD_18;
			this.cbMod19.Checked = am.MOD_19;
			this.cbMod20.Checked = am.MOD_20;
		}

		private void bSetDetails_Click(object sender, EventArgs e)
		{
			ActiveModules am = new ActiveModules();

			am.IDRegistration = currentUserReg;

			am.MOD_0 = this.cbMod0.Checked;
			am.MOD_1 = this.cbMod1.Checked;
			am.MOD_2 = this.cbMod2.Checked;
			am.MOD_3 = this.cbMod3.Checked;
			am.MOD_4 = this.cbMod4.Checked;
			am.MOD_5 = this.cbMod5.Checked;
			am.MOD_6 = this.cbMod6.Checked;
			am.MOD_7 = this.cbMod7.Checked;
			am.MOD_8 = this.cbMod8.Checked;
			am.MOD_9 = this.cbMod9.Checked;
			am.MOD_10 = this.cbMod10.Checked;
			am.MOD_11 = this.cbMod11.Checked;
			am.MOD_12 = this.cbMod12.Checked;
			am.MOD_13 = this.cbMod13.Checked;
			am.MOD_14 = this.cbMod14.Checked;
			am.MOD_15 = this.cbMod15.Checked;
			am.MOD_16 = this.cbMod16.Checked;
			am.MOD_17 = this.cbMod17.Checked;
			am.MOD_18 = this.cbMod18.Checked;
			am.MOD_19 = this.cbMod19.Checked;
			am.MOD_20 = this.cbMod20.Checked;


			string msg = string.Empty;
			try
			{
				ws.SetActiveModules(am, "1asw32*a", out msg);
			}
			catch (Exception exc)
			{
				MessageBox.Show(exc.Message + System.Environment.NewLine + msg);
			}
			this.bUpdate_Click(sender, e);
		}

		private void bCharge_Click(object sender, EventArgs e)
		{
			string msg = string.Empty;
			try
			{
				//bool result = ws.ConfirmDataRequestSuccess(currentUserReg, 1, 0, out msg);
				bool result = ws.ConfirmDataRequestSuccess(currentUserId, 1, 1, out msg);
                if (true == result)
                {
                    MessageBox.Show("Charged Property Profile Search to:" + System.Environment.NewLine + "\tUser Id: " + currentUserId.ToString() +
                            System.Environment.NewLine + "\tUser: " + Globals.currentUser +
                            System.Environment.NewLine + "\tReg Key: " + Globals.currentRegKey);
                }
                else
                    MessageBox.Show(msg);
			}
			catch (Exception exc)
			{
				MessageBox.Show(exc.ToString() + System.Environment.NewLine + msg);
			}
		}

		private void bStartIE_Click(object sender, System.EventArgs e)
		{
			System.Diagnostics.Process.Start("iexplore.exe", ws.Url);
		}

		private void Form1_Load(object sender, System.EventArgs e)
		{
		}

		private void flood_req()
		{
			byte[] responseArray;
			string resultMessage;
			try
			{

				WR_SDC.ServiceDataConsumption sdc	= new WR_SDC.ServiceDataConsumption();
                


                WR_SDC.Registration rg = new WR_SDC.Registration();

				rg.KeyCode = "58L4-TI2F-1Q27-4S98";

				sdc.RequestDataPropertyProfile(
					rg,
					Globals.last_req,
					out responseArray, 
					out resultMessage);

				Encoding enc = Encoding.UTF8;

				if(responseArray != null)
					flood_result.Text += "query result returned, cnt:"+ responseArray.Length +  " data: "+ enc.GetString(responseArray, 0, 80)+"..."+System.Environment.NewLine;
				else
					flood_result.Text += "query returned null"+System.Environment.NewLine;

				flood_result.Text += resultMessage;
			}
			catch(Exception e)
			{
				flood_result.Text += "query exception: "+e.ToString().Substring(0,80)+"..."+System.Environment.NewLine;
			}
		}

		private void flood_Click(object sender, System.EventArgs e)
		{
			if(Globals.last_req == null) 
			{
				MessageBox.Show("Please make one query in comps or property profile before...");
				return;
			}

			flood_result.Clear();

			int i;
			for(i=0;i<floodCnt.Value;i++)
			{
				Thread t = new Thread(new ThreadStart(flood_req));
				
				t.Start();
			}
		}

		private void rbWS_CheckedChanged(object sender, System.EventArgs e)
		{
		
		}

		private void rbWS3_CheckedChanged(object sender, System.EventArgs e)
		{
			this.SwapWS();
		}

		private void SwapWS()
		{
			
			//	ws.Url = "http://208.112.123.225/ws/ServiceRegistration.asmx";				
			//	Globals.urlComps = "http://208.112.123.225/ws/ServiceDataConsumption.asmx";

			this.Text = Globals.URL_Registration + ", " + Globals.URL_DataComsumption;
			
		}

		private void lvUsers_ColumnClick(object sender, System.Windows.Forms.ColumnClickEventArgs e)
		{
			this.asc = !this.asc;
			if (true == this.asc)
				this.lvUsers.ListViewItemSorter = new ListViewItemComparer(e.Column);
			else
				this.lvUsers.ListViewItemSorter = new ListViewItemComparerDesc(e.Column);
		}

		private void bFindUser_Click(object sender, System.EventArgs e)
		{
			Key keyForm = new Key();
			keyForm.ShowDialog();

			string key = keyForm.SerialKey;
			for (int i = 0; i < this.lvUsers.Items.Count; i++)
			{
				if (key == this.lvUsers.Items[i].SubItems[3].Text)
				{
					this.lvUsers.Items[i].Selected = true;
					this.lvUsers.Items[i].Focused = true;	
					lvUsers_DoubleClick(null, null);
				}
			}
		}

		private void lvUsers_DoubleClick(object sender, System.EventArgs e)
		{
			if (null != this.lvUsers.SelectedItems && 0 < this.lvUsers.SelectedItems.Count)
			{
				ListViewItem lvi = this.lvUsers.SelectedItems[0];
				//MessageBox.Show(((Guid)(lvi.Tag)).ToString());

				this.currentUserReg = (Guid)lvi.Tag;
				this.currentUserId = new Guid(lvi.SubItems[5].Text);

				this.tc_WS_Data.SelectedTab = this.tc_WS_Data.TabPages[2];
				this.tbSerialNr.Text = lvi.SubItems[3].Text;
				this.tbActDate.Text = lvi.SubItems[4].Text;
				
				this.tb_FN.Text = lvi.SubItems[0].Text;
				this.tb_MN.Text = lvi.SubItems[1].Text;
				this.tb_LN.Text = lvi.SubItems[2].Text;

				Globals.currentUser = lvi.SubItems[0].Text + " " + lvi.SubItems[1].Text + " " + lvi.SubItems[2].Text;
				Globals.currentRegKey = lvi.SubItems[3].Text;

				this.tbPGID.Text = string.Empty;

				this.tbAddress.Text = string.Empty;
				this.tbCity.Text = string.Empty;
				this.tbState.Text = string.Empty;
				this.tbZIP.Text = string.Empty;
				this.tbPhoneNo.Text = string.Empty;
				this.tbBussPh.Text = string.Empty;
				this.tbCellNo.Text = string.Empty;
				this.tbEmail.Text = string.Empty;
				this.tbCompany.Text = string.Empty;

				foreach (object o in this.tc_WS_Data.TabPages[2].Controls)
				{
					if (o is CheckBox)
					{
						CheckBox cb = (CheckBox)o;
						cb.Checked = false;
					}
				}
				this.Update_UserName_RegKey_Textbox();
			}     
		}

		private void Update_UserName_RegKey_Textbox()
		{
			this.tbUserRegKey.Text = Globals.currentUser + ", " + Globals.currentRegKey;
		}

		private void linkLabel1_LinkClicked(object sender, System.Windows.Forms.LinkLabelLinkClickedEventArgs e)
		{
			if (string.Empty == this.tbPGID.Text)
			{
				MessageBox.Show("Get details first!");
				return;
			}
			Process.Start("IExplore.exe", "http://www.profitgrabber.com/account/subscribe_renew.cfm?IDUser={" + this.tbPGID.Text + "}");
		}

		private void linkLabel2_LinkClicked(object sender, System.Windows.Forms.LinkLabelLinkClickedEventArgs e)
		{
			if (string.Empty == this.tbPGID.Text)
			{
				MessageBox.Show("Get details first!");
				return;
			}
			Process.Start("IExplore.exe", "http://www.ProfitGrabber.com/Account/Home.cfm?GUID={" + this.tbPGID.Text + "}");
		}

		private void button1_Click(object sender, System.EventArgs e)
		{
			

			this.currentUserReg = new Guid("fd79c4b5-2dc9-42f3-afd5-91994962feea");
			this.currentUserId = new Guid("1c97d270-00d9-48d2-bde4-5194bc1b98c7");

			this.tc_WS_Data.SelectedTab = this.tc_WS_Data.TabPages[2];
			this.tbSerialNr.Text = "58L4-TI2F-1Q27-4S98";
			this.tbActDate.Text = string.Empty;
				
			this.tb_FN.Text = string.Empty;
			this.tb_MN.Text = string.Empty;
			this.tb_LN.Text = string.Empty;


			this.tbPGID.Text = string.Empty;

			this.tbAddress.Text = string.Empty;
			this.tbCity.Text = string.Empty;
			this.tbState.Text = string.Empty;
			this.tbZIP.Text = string.Empty;
			this.tbPhoneNo.Text = string.Empty;
			this.tbBussPh.Text = string.Empty;
			this.tbCellNo.Text = string.Empty;
			this.tbEmail.Text = string.Empty;
			this.tbCompany.Text = string.Empty;

			foreach (object o in this.tc_WS_Data.TabPages[2].Controls)
			{
				if (o is CheckBox)
				{
					CheckBox cb = (CheckBox)o;
					cb.Checked = false;
				}
			}
		}

		private void button2_Click(object sender, System.EventArgs e)
		{
			string msg = string.Empty;
			try
			{				
				bool result = ws.ConfirmDataRequestSuccess(currentUserId, 1, 0, out msg);
                if (true == result)
                {
                    MessageBox.Show("Charged COMPS to:" + System.Environment.NewLine + "\tUser Id: " + currentUserId.ToString() +
                        System.Environment.NewLine + "\tUser: " + Globals.currentUser +
                        System.Environment.NewLine + "\tReg Key: " + Globals.currentRegKey);
                }
                else
                    MessageBox.Show(msg);
			}
			catch (Exception exc)
			{
				MessageBox.Show(exc.ToString() + System.Environment.NewLine + msg);
			}
		}

		private void _saveUserInfo_Click(object sender, EventArgs e)
		{
			DumpShortUserInfo();
		}

		private void _queryAccount_Click(object sender, System.EventArgs e)
		{
			int month;
			int topup;
			WR_SDC.ServiceDataConsumption sdc = new WR_SDC.ServiceDataConsumption();
            
            sdc.QueryAcc(currentUserId, out month, out topup);

			MessageBox.Show("Month: " + month.ToString() + Environment.NewLine + "TopUp: " + topup.ToString());
		}

        void On_nQA_Click(object sender, EventArgs e)
        {
            WSE_Service wses = new WSE_Service();
            ReturnObject[] inputs = wses.GetInputs(currentUserId);
            ReturnObject[] topUps = wses.GetTopUps(currentUserId);
            AccountDetails ad = new AccountDetails();
            ad.ShowData(inputs, topUps);
            ad.ShowDialog();
        }

		private void _queryDataRequest_Click(object sender, System.EventArgs e)
		{
			//id : 0 - "Ultimate Data Source COMPS" 
			//id : 1 - "Ultimate Data Source Property Profile"
			//id : 2 - "Matching Module"

			int id = 0;
			int totalCost;
			int totalAmount;
			string outMsg;

			if (_mm.Checked)
				id = 2;
			else if (_pp.Checked)
				id = 1;
			else if (_comps.Checked)
				id = 0;

			WR_SDC.ServiceDataConsumption sdc = new WR_SDC.ServiceDataConsumption();
            
            bool bRes = sdc.QueryDataRequestSuccess(currentUserId, (int)_amount.Value, id, out totalCost, out totalAmount, out outMsg);

			MessageBox.Show(outMsg + Environment.NewLine + "Total Cost: " + totalCost.ToString() + Environment.NewLine + "Total Amount: " + totalAmount.ToString());
		}

		private void ChargeMM(object sender, System.EventArgs e)
		{
			string msg = string.Empty;
			try
			{				
				bool result = ws.ConfirmDataRequestSuccess(currentUserId, 1, 2, out msg);
                if (true == result)
                {
                    MessageBox.Show("Charged MM to:" + System.Environment.NewLine + "\tUser Id: " + currentUserId.ToString() +
                        System.Environment.NewLine + "\tUser: " + Globals.currentUser +
                        System.Environment.NewLine + "\tReg Key: " + Globals.currentRegKey);
                }
                else
                    MessageBox.Show(msg);
			}
			catch (Exception exc)
			{
				MessageBox.Show(exc.ToString() + System.Environment.NewLine + msg);
			}		
		}

        private void _filterUsers_Click(object sender, EventArgs e)
        {
            Filter f = new Filter();

            if (null != sui && 0 < sui.Length)
                f.SetData(sui);
            else
            {
                MessageBox.Show("Please Get All Users before filtering!");
                return;
            }

            if (DialogResult.OK == f.ShowDialog())
            {
                foreach (ListViewItem lvi in lvUsers.Items)
                {
                    if (((Guid)lvi.Tag) == f.SelectedUserReg)
                    {
                        lvi.Selected = true;
                        break;                       
                    }
                }

                lvUsers_DoubleClick(null, null);
            }            
        }

        string pwd = string.Empty;
        void On_addUSD_Click(object sender, EventArgs e)
        {
            if (string.Empty == pwd)
            {
                Set_Password sp = new Set_Password();
                sp.ShowDialog();
                pwd = sp.Password;
            }

            AddUDS auf = new AddUDS();
            auf.SetData(pwd, currentUserId);
            auf.ShowDialog();
            
        }

        public class UDSEntry
        {
            public Guid IDInfusion { get; set; }

            public Guid IDUser { get; set; }

            public int? IDInfusionContact { get; set; }
        }

        private void getUDSStatus_Click(object sender, EventArgs e)
        {                        
            try
            {
                _lvUDS.Items.Clear();
                
                using (WebClient wc = new WebClient())
                {
                    //wc.Headers["Content-Type"] = "application/json";
                    var reqParams = new System.Collections.Specialized.NameValueCollection();
                    //reqParams.Add("pgId", currentUserId.ToString());
                    
                    byte[] responseBytes = wc.UploadValues(_isURI + $"/api/UDS/GetEntries_pgId/{currentUserId}", "POST", reqParams);
                    string responseBody = Encoding.UTF8.GetString(responseBytes);
                    
                    var resArray = JsonConvert.DeserializeObject<UDSEntry[]>(responseBody);

                    if (null != resArray && 0 < resArray.Length)
                    {
                        foreach (UDSEntry item in resArray)
                        {
                            if (null != item.IDInfusionContact)
                            {
                                ListViewItem lvi = new ListViewItem(item.IDInfusionContact.ToString());
                                lvi.Tag = item.IDInfusionContact.ToString();
                                _lvUDS.Items.Add(lvi);
                            }
                        }
                    }
                    else
                    {
                        if (null != sender)
                            MessageBox.Show("No items found!");
                    }
                }
            }

            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }            
        }

        private void _updateInfusionID_Click(object sender, EventArgs e)
        {
            if (null != _lvUDS.SelectedItems && 1 == _lvUDS.SelectedItems.Count)
            {
                UpdateForm uf = new UpdateForm();
                if (DialogResult.OK == uf.ShowDialog())
                {
                    string newInfusionId = uf.InfusionId;
                    int infusionId = 0;
                    if (Int32.TryParse(newInfusionId, out infusionId))
                    {
                        if (UpdateInfusionRecord(infusionId, currentUserId.ToString()))
                        {
                            MessageBox.Show("Record Updated");
                            getUDSStatus_Click(null, null);
                        }
                        else
                            MessageBox.Show("Error in update, please check srv. logs!");
                    }
                    else
                        MessageBox.Show("infusionId must be number!");
                }
            }
            else
                MessageBox.Show("select record to update");
        }

        private void _deleteInfusionID_Click(object sender, EventArgs e)
        {            
            if (null != _lvUDS.SelectedItems && 1 == _lvUDS.SelectedItems.Count)
            {
                if (DialogResult.Yes != MessageBox.Show("Are you sure?", "Record delete", MessageBoxButtons.YesNo, MessageBoxIcon.Information, MessageBoxDefaultButton.Button2))
                    return;

                int infusionId = -1;
                if (Int32.TryParse((string)_lvUDS.SelectedItems[0].Tag, out infusionId))
                {
                    if (DeleteInfusionRecord(infusionId, currentUserId.ToString()))
                    {
                        MessageBox.Show("Record Deleted");
                        getUDSStatus_Click(null, null);
                    }
                    else
                        MessageBox.Show("Error in delete, please check srv. logs!");
                }
                else
                    MessageBox.Show("infusionId must be number!");
            }
            else
                MessageBox.Show("select record to delete");
        }

        private void _InsertInfusionID_Click(object sender, EventArgs e)
        {
            InsertForm insf = new InsertForm();

            if (DialogResult.OK == insf.ShowDialog())
            {
                int infusionId = -1;
                if (!Int32.TryParse(insf.InfusionID, out infusionId))
                {
                    MessageBox.Show("infusionId must be number!");
                    return;
                }

                Guid pgid;
                try
                {
                    pgid = new Guid(insf.PGId);
                }
                catch
                {
                    MessageBox.Show("PGID must be GUID!");
                    return;
                }

                if (InsertInfusionRecord(infusionId, pgid.ToString()))
                {
                    MessageBox.Show("Record Inserted");
                    getUDSStatus_Click(null, null);
                }
                else
                    MessageBox.Show("Error in delete, please check srv. logs!");
            }
        }

        private void _getAssignedPGIDs_Click(object sender, EventArgs e)
        {
            _lvAssignedPGIDs.Items.Clear();

            
            int infusionId = -1;
            if (Int32.TryParse(_infusionID.Text, out infusionId))
            {
                UDSEntry[] entries = null;
                try
                {
                    using (WebClient wc = new WebClient())
                    {
                        var reqParams = new System.Collections.Specialized.NameValueCollection();
                        
                        
                        byte[] responseBytes = wc.UploadValues(_isURI + $"/api/UDS/GetEntries_infusionId/{infusionId}", "POST", reqParams);
                        string responseBody = Encoding.UTF8.GetString(responseBytes);

                        entries = JsonConvert.DeserializeObject<UDSEntry[]>(responseBody);
                    }                        
                }
                catch (Exception exc)
                {
                    entries = null;
                    MessageBox.Show(exc.Message);
                }

                if (null != entries && 0 < entries.Length)
                {
                    foreach (UDSEntry item in entries)
                    {
                        ListViewItem lvi = new ListViewItem(item.IDUser.ToString());
                        lvi.Tag = item.IDUser;

                        _lvAssignedPGIDs.Items.Add(lvi);
                    }
                }
                else
                {
                    if (null != sender)
                        MessageBox.Show("No items found!");
                }
            }
            else
                MessageBox.Show("infusionId must be number!");
        }

        private void _updatePGID_Click(object sender, EventArgs e)
        {

        }
        
        private void _deletePGID_Click(object sender, EventArgs e)
        {            
            if (null != _lvAssignedPGIDs.SelectedItems && 1 == _lvAssignedPGIDs.SelectedItems.Count)
            {
                if (DialogResult.Yes != MessageBox.Show("Are you sure?", "Record delete", MessageBoxButtons.YesNo, MessageBoxIcon.Information, MessageBoxDefaultButton.Button2))
                    return;

                int infusionId = -1;
                if (Int32.TryParse(_infusionID.Text, out infusionId))
                {
                    if (DeleteInfusionRecord(infusionId, _lvAssignedPGIDs.SelectedItems[0].Tag.ToString()))
                    {
                        MessageBox.Show("Record Deleted");
                        _getAssignedPGIDs_Click(null, null);
                    }
                    else
                        MessageBox.Show("Error in delete, please check srv. logs!");
                }
                else
                    MessageBox.Show("infusionId must be number!");
            }
            else
                MessageBox.Show("select record to delete");
        }

        private void _insertPGID_Click(object sender, EventArgs e)
        {

        }

        bool DeleteInfusionRecord(int infusionId, string pgId)
        {                        
            int result = 0;            
            bool bRes = false;

            try
            {
                using (WebClient wc = new WebClient())
                {
                    var reqParams = new System.Collections.Specialized.NameValueCollection();
                    reqParams.Add("IDUser", pgId.ToString());
                    reqParams.Add("IDInfusionContact", infusionId.ToString());

                    byte[] responseBytes = wc.UploadValues(_isURI + "/api/UDS/DeleteFromUDS", "POST", reqParams);
                    string responseBody = Encoding.UTF8.GetString(responseBytes);

                    result = JsonConvert.DeserializeObject<int>(responseBody);
                }

                //service.DeleteFromUDS(pgId, infusionId, true, out result, out resultSpecified);

                if (1 == result)
                    bRes = true;
            }
            catch (Exception exc)
            {                
                MessageBox.Show(exc.Message);
            }

            return bRes;
        }

        bool UpdateInfusionRecord(int infusionId, string pgId)
        {            
            int result = 0;
            bool resultSpecified = false;
            bool bRes = false;

            try
            {
                using (WebClient wc = new WebClient())
                {
                    var reqParams = new System.Collections.Specialized.NameValueCollection();
                    reqParams.Add("IDUser", pgId.ToString());
                    reqParams.Add("IDInfusionContact", infusionId.ToString());

                    byte[] responseBytes = wc.UploadValues(_isURI + $"/api/UDS/UpdateUDS", "POST", reqParams);
                    string responseBody = Encoding.UTF8.GetString(responseBytes);

                    result = JsonConvert.DeserializeObject<int>(responseBody);
                }

                //service.UpdateUDS(pgId, infusionId, true, out result, out resultSpecified);

                if (1 == result)
                    bRes = true;
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }

            return bRes;
        }

        bool InsertInfusionRecord(int infusionId, string pgId)
        {            
            int result = 0;
            bool resultSpecified = false;
            bool bRes = false;

            try
            {
                using (WebClient wc = new WebClient())
                {
                    var reqParams = new System.Collections.Specialized.NameValueCollection();
                    reqParams.Add("IDUser", pgId.ToString());
                    reqParams.Add("IDInfusionContact", infusionId.ToString());


                    byte[] responseBytes = wc.UploadValues(_isURI + "/api/UDS/InsertUDS", "POST", reqParams);
                    string responseBody = Encoding.UTF8.GetString(responseBytes);

                    result = JsonConvert.DeserializeObject<int>(responseBody);
                }

                //service.InsertUDS(pgId, infusionId, true, out result, out resultSpecified);

                if (1 == result)
                    bRes = true;
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }

            return bRes;
        }

        private void groupControl2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void showFlyoutButton_Click(object sender, EventArgs e)
        {
            flyoutPanel.ShowPopup();
        }
    }
}