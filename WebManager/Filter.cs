﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

//Added
using WebManager.WS_REG;

namespace WebManager
{
    public partial class Filter : Form
    {
        //DATA
        ShortUserInfo[] _sui = null;
        ListViewColumnSorter lvwColumnSorter;
        Guid _selectedUserReg;

        //CTR
        public Filter()
        {
            InitializeComponent();           
            lvwColumnSorter = new ListViewColumnSorter();
            
            AddEventHandlers();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            FillData(_sui);                                               
        }

        //PROPERTIES
        public Guid SelectedUserReg
        {
            get { return _selectedUserReg; }
        }


        //METHODS
        void AssignColumnSorter(bool assign)
        {
            if (assign)
            {
                _filteredView.ColumnClick += new ColumnClickEventHandler(On_FilteredView_ColumnClick);
                _filteredView.ListViewItemSorter = lvwColumnSorter;
            }
            else
            {
                _filteredView.ColumnClick -= new ColumnClickEventHandler(On_FilteredView_ColumnClick);
                _filteredView.ListViewItemSorter = null;
            }
        }

        public void SetData(ShortUserInfo[] sui)
        {
            _sui = sui;
        }

        void AddEventHandlers()
        {                        
            _fn.TextChanged += new EventHandler(On_Name_TextChanged);
            _mn.TextChanged += new EventHandler(On_Name_TextChanged);
            _ln.TextChanged += new EventHandler(On_Name_TextChanged);

            _filteredView.DoubleClick += new EventHandler(On_FilteredView_DoubleClick);
            _ok.Click += new EventHandler(On_Ok_Click);
            _cancel.Click += new EventHandler(On_Cancel_Click);            
        }

        void On_Cancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        void On_Ok_Click(object sender, EventArgs e)
        {

            foreach (ListViewItem lvi in _filteredView.Items)
            {
                if (lvi.Selected)
                {
                    try { _selectedUserReg = (Guid)lvi.Tag; }
                    catch { _selectedUserReg = Guid.Empty; }
                    break;
                }
            }            

            this.Close();
        }

        void On_FilteredView_DoubleClick(object sender, EventArgs e)
        {
            try { _selectedUserReg = (Guid)((ListView)sender).SelectedItems[0].Tag; }
            catch { _selectedUserReg = Guid.Empty; }

            this.DialogResult = DialogResult.OK;
            this.Close();
        }                        

        void FillData(ShortUserInfo[] sui)
        {
            if (null == sui)
                return;

            AssignColumnSorter(false);

            _filteredView.Items.Clear();

            foreach (ShortUserInfo info in sui)
            {
                ListViewItem lvi = new ListViewItem(info.FirstName);
                lvi.SubItems.Add(info.MiddleName);
                lvi.SubItems.Add(info.LastName);
                lvi.SubItems.Add(info.UserId.ToString());
                lvi.SubItems.Add(info.ActivationDate.ToShortDateString() + "," + info.ActivationDate.ToShortTimeString());

                lvi.Tag = info.RegistrationId;

                _filteredView.Items.Add(lvi);
            }

            AssignColumnSorter(true);
        }                

        void On_Name_TextChanged(object sender, EventArgs e)
        {
            string fnPart = _fn.Text.ToUpper();
            string mnPart = _mn.Text.ToUpper();
            string lnPart = _ln.Text.ToUpper();

            List<ShortUserInfo> filteredUserList = new List<ShortUserInfo>();

            foreach (ShortUserInfo info in _sui)
            {
                bool fnPresent = true;
                bool mnPresent = true;
                bool lnPresent = true;

                if (null == info.FirstName || (string.Empty != fnPart && 0 != info.FirstName.ToUpper().IndexOf(fnPart)))
                    fnPresent = false;

                if (null == info.MiddleName || (string.Empty != mnPart && 0 != info.MiddleName.ToUpper().IndexOf(mnPart)))
                    mnPresent = false;

                if (null == info.LastName || (string.Empty != lnPart && 0 != info.LastName.ToUpper().IndexOf(lnPart)))
                    lnPresent = false;

                if (fnPresent && mnPresent && lnPresent)
                    filteredUserList.Add(info);
            }

            FillData(filteredUserList.ToArray());
        }

        void On_FilteredView_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            // Determine if clicked column is already the column that is being sorted.
            if (e.Column == lvwColumnSorter.SortColumn)
            {
                // Reverse the current sort direction for this column.
                if (lvwColumnSorter.Order == SortOrder.Ascending)
                {
                    lvwColumnSorter.Order = SortOrder.Descending;
                }
                else
                {
                    lvwColumnSorter.Order = SortOrder.Ascending;
                }
            }
            else
            {
                // Set the column number that is to be sorted; default to ascending.
                lvwColumnSorter.SortColumn = e.Column;
                lvwColumnSorter.Order = SortOrder.Ascending;
            }

            // Perform the sort with these new sort options.
            _filteredView.Sort();
        }       
    }
}
