using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;
using System.IO;
using System.Xml.Serialization;
using System.Web;
using System.Text;
using System.Xml;
using System.Net;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Drawing.Printing;

namespace WebManager
{
    /// <summary>
    /// Summary description for UserControlPropertyProfileCompsListView.
    /// </summary>
    public class UserControlPropertyProfileCompsListView : System.Windows.Forms.UserControl
    {
        #region . variables .

        private DTD.Response.RESPONSE_GROUP response;
		private DTD.Response._DATA_PROVIDER_COMPARABLE_SALES[] comps;
		private bool asc = false;
		private double originalSQFT;
		private comps[] dbComps = null;
		private SqlConnection _dbConn = null;
		private bool _compsInDb = false;
		private decimal _estimatedValue = 0;

		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
		private System.Windows.Forms.Label labelRequestParameters;
		private System.Windows.Forms.Button buttonGetComps;
		private System.Windows.Forms.ListView listViewPropertyProfilesComps;
		private System.Windows.Forms.ColumnHeader columnHeaderTag;
		private System.Windows.Forms.ColumnHeader columnHeaderDistnace;
		private System.Windows.Forms.ColumnHeader columnHeaderSubdivision;
		private System.Windows.Forms.ColumnHeader columnHeaderCity;
		private System.Windows.Forms.ColumnHeader columnHeaderSQFT;
		private System.Windows.Forms.ColumnHeader columnHeaderYB;
		private System.Windows.Forms.ColumnHeader columnHeaderRooms;
		private System.Windows.Forms.ColumnHeader columnHeaderBedrooms;
		private System.Windows.Forms.ColumnHeader columnHeaderBath;
		private System.Windows.Forms.ColumnHeader columnHeaderSaleDate;
		private System.Windows.Forms.ColumnHeader columnHeaderSalePrice;
		private System.Windows.Forms.ColumnHeader columnHeaderLotArea;
		private System.Windows.Forms.ColumnHeader columnHeaderAssesed;
		private System.Windows.Forms.ColumnHeader columnHeaderStreetName;
		private System.Windows.Forms.ColumnHeader columnHeaderZIP;
		private System.Windows.Forms.ColumnHeader columnHeaderPool;
		private System.Windows.Forms.Button buttonInvert;
		private System.Windows.Forms.Button buttonUntagAll;
		private System.Windows.Forms.Button buttonTagAll;
		private System.Windows.Forms.ComboBox comboBoxSqFtRange;
		private System.Windows.Forms.ComboBox comboBoxSearchRadius;
		private System.Windows.Forms.ComboBox comboBoxSalesInLast;
		private System.Windows.Forms.TextBox textBoxEstValue;
		private System.Windows.Forms.TextBox textBoxEstValuePrivate;
		private System.Windows.Forms.TextBox textBoxDate;
		private System.Windows.Forms.Button buttonRecalculate;
		private System.Windows.Forms.ColumnHeader columnHeader1;
		private System.Windows.Forms.ColumnHeader columnHeaderStreetNum;
		private System.Windows.Forms.ColumnHeader columnHeaderSqFtPrice;
		private System.Windows.Forms.TextBox textBoxStateA;
		private System.Windows.Forms.TextBox textBoxZipA;
		private System.Windows.Forms.TextBox textBoxUnitNoA;
		private System.Windows.Forms.TextBox textBoxStreetA;
		private System.Windows.Forms.TextBox textBoxStreetNoA;
		private System.Windows.Forms.Label label76;
		private System.Windows.Forms.TextBox textBoxCountyA;
		private System.Windows.Forms.Label label74;
		private System.Windows.Forms.Label label75;
		private System.Windows.Forms.TextBox textBoxPreDirA;
		private System.Windows.Forms.TextBox textBoxSuffixA;
		private System.Windows.Forms.TextBox textBoxPostDirA;
		private System.Windows.Forms.Label label79;
		private System.Windows.Forms.Button bPrint;
        private DevExpress.XtraLayout.LayoutControl UserControlPropertyProfileCompsListViewlayoutControl1ConvertedLayout;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem comboBoxSqFtRangeitem;
        private DevExpress.XtraLayout.LayoutControlItem textBoxSuffixAitem;
        private DevExpress.XtraLayout.LayoutControlItem textBoxPostDirAitem;
        private DevExpress.XtraLayout.LayoutControlItem textBoxPreDirAitem;
        private DevExpress.XtraLayout.LayoutControlItem textBoxStateAitem;
        private DevExpress.XtraLayout.LayoutControlItem textBoxCountyAitem;
        private DevExpress.XtraLayout.LayoutControlItem textBoxZipAitem;
        private DevExpress.XtraLayout.LayoutControlItem textBoxStreetAitem;
        private DevExpress.XtraLayout.LayoutControlItem textBoxStreetNoAitem;
        private DevExpress.XtraLayout.LayoutControlItem buttonGetCompsitem;
        private DevExpress.XtraLayout.LayoutControlItem textBoxOrigSqFtitem;
        private DevExpress.XtraLayout.LayoutControlItem comboBoxSearchRadiusitem;
        private DevExpress.XtraLayout.LayoutControlItem textBoxDateitem;
        private DevExpress.XtraLayout.LayoutControlItem comboBoxSalesInLastitem;
        private DevExpress.XtraLayout.LayoutControlItem textBoxEstValuePrivateitem;
        private DevExpress.XtraLayout.LayoutControlItem textBoxEstValueitem;
        private DevExpress.XtraLayout.LayoutControlItem bPrintitem;
        private DevExpress.XtraLayout.LayoutControlItem label74item;
        private DevExpress.XtraLayout.LayoutControlItem label75item;
        private DevExpress.XtraLayout.LayoutControlItem textBoxUnitNoAitem;
        private DevExpress.XtraLayout.LayoutControlItem label76item;
        private DevExpress.XtraLayout.LayoutControlItem buttonInvertitem;
        private DevExpress.XtraLayout.LayoutControlItem buttonTagAllitem;
        private DevExpress.XtraLayout.LayoutControlItem buttonUntagAllitem;
        private DevExpress.XtraLayout.LayoutControlItem listViewPropertyProfilesCompsitem;
        private DevExpress.XtraLayout.LayoutControlItem labelRequestParametersitem;
        private DevExpress.XtraLayout.LayoutControlItem buttonRecalculateitem;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private System.Windows.Forms.TextBox textBoxOrigSqFt;

		
		#endregion
		#region . constructror .
		public UserControlPropertyProfileCompsListView()
		{
			//TODO: Define if we need this here !!!
			System.Threading.Thread.CurrentThread.CurrentCulture=System.Globalization.CultureInfo.CreateSpecificCulture("EN-US");

			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

			this.bPrint.Click += new EventHandler(bPrint_Click);

			//TA:++ 15.03.2005
			//Change: Commented out "clearForm();" statement
			//clearForm();

			//TA:-- 15.03.2005
			//this.dummyFill();

			comboBoxSearchRadius.Items.Clear();
			comboBoxSqFtRange.Items.Clear();
			comboBoxSalesInLast.Items.Clear();


			comboBoxSearchRadius.Items.AddRange(new string[] {
																 "0.1"
																 ,"0.2"
																 ,"0.3"
																 ,"0.4"																 
																 ,"0.5"
																 ,"0.6"
																 ,"0.7"
																 ,"0.8"
																 ,"0.9"
																 ,"1.0"
																 ,"1.1"
																 ,"1.2"
																 ,"1.3"
																 ,"1.4"
																 ,"1.5"
																 ,"1.6"
																 ,"1.7"
																 ,"1.8"
																 ,"1.9"
																 ,"2.0"
																 ,"2.1"
																 ,"2.2"
																 ,"2.3"
																 ,"2.4"
																 ,"2.5"
																 ,"2.6"
																 ,"2.7"
																 ,"2.8"
																 ,"2.9"
																 ,"3.0"
															 });
			comboBoxSqFtRange.Items.AddRange(new string[] {
															  "+/-10%"
															  ,"+/-15%"
															  ,"+/-20%"
															  ,"+/-25%"
															  ,"+/-30%"
															  ,"+/-35%"
															  ,"+/-40%"
															  ,"+/-45%"
															  ,"+/-50%"
															  ,"+/-55%"});
			comboBoxSalesInLast.Items.AddRange(new string[] {
																"24 mo."
																,"20 mo."
																,"15 mo."
																,"12 mo."
																,"10 mo."
																,"8 mo."
																,"6 mo."
															});

			comboBoxSqFtRange.SelectedIndex = 3;
			comboBoxSearchRadius.SelectedIndex = 9;			
			comboBoxSalesInLast.SelectedIndex = 3;

		}
		#endregion
		#region . connection .
		public SqlConnection DBConn
		{
			get { return this._dbConn; }
			set { this._dbConn = value; }
		}
		#endregion
		#region . clear form .
		public void clearForm()
		{
			listViewPropertyProfilesComps.Items.Clear();
			foreach (object obj in this.Controls)
			{
				if (obj.GetType() == typeof(TextBox))
					((TextBox)obj).Text = "";
			}
			textBoxEstValue.Text=getMoney("0");
			textBoxEstValuePrivate.Text=getMoney("0");
			dbComps=null;
			_compsInDb = false;
		}
		#endregion . clear form .
		#region . start .
		public void setInitialData(
			  string StreetNo
			, string PreDir
			, string StreetName
			, string PostDir
			, string StreetSuffix
			, string UnitOrAppartment
			, string ZIP
			, string County
			, string State
			, double SqFt
			, Guid PropertyGuid
			, SqlConnection dbConn
			)
		{
			clearForm();
			originalSQFT = SqFt;
			_dbConn = dbConn;

			textBoxStreetNoA.Text =StreetNo;
			textBoxPreDirA.Text = PreDir;
			textBoxStreetA.Text = StreetName;
			textBoxPostDirA.Text = PostDir;
			textBoxSuffixA.Text = StreetSuffix;
			textBoxUnitNoA.Text = UnitOrAppartment;
			textBoxZipA.Text = ZIP;
			textBoxCountyA.Text = County;
			textBoxStateA.Text = State;
			textBoxOrigSqFt.Text = getNumber(originalSQFT.ToString());
			
			//TODO: Set response...
			//response = 

//SASA TODO
//			dbComps = comps.Select(_dbConn, PropertyGuid);

			if (dbComps.Length==0)
			{
				dbComps = new comps[1];
				dbComps[0] = new comps(PropertyGuid, DateTime.Now, 0, 0,  null, "");
				_compsInDb=false;
			}
			else
			{
				MemoryStream stream = new MemoryStream(dbComps[0].P_xmlStream);
				XmlSerializer serializer = new XmlSerializer(typeof(DTD.Response.RESPONSE_GROUP));
				response = (DTD.Response.RESPONSE_GROUP)serializer.Deserialize(stream);

				dumpObject(response, "_COMPS_RESPONSE.xml");

				if (CheckValid())
				{
					comps = response.
						RESPONSE.
						RESPONSE_DATA[0].
						PROPERTY_INFORMATION_RESPONSE.
						_PROPERTY_INFORMATION[0].
						_DATA_PROVIDER_COMPARABLE_SALES;
					try
					{
						textBoxDate.Text = dbComps[0].P_dateOfRetrieve.ToShortDateString();
					}
					catch{}
					try
					{
						textBoxEstValue.Text = getMoney(dbComps[0].P_estimatedValue.ToString());}
					catch{}
					try
					{
						textBoxEstValuePrivate.Text = getMoney(dbComps[0].P_privateEstimatedValue.ToString());}
					catch{}

				}
				FillList();
				try
				{
					string[] selectedArr = dbComps[0].P_taggedCSV.Split(new char[]{','});
					foreach(ListViewItem lvi in listViewPropertyProfilesComps.Items)
					{
						if(isInStringArray(selectedArr, lvi.SubItems[18].Text))
						{
							lvi.Checked=true;
						}
					}
				}
				catch
				{
					//intended! no need to break if error...
				}
				_compsInDb=true;
			}
		}
		#endregion
		#region . CheckValid .
		public bool CheckValid()
		{
			DTD.Response.STATUS status;
			bool error = false;

			try 
			{
				status = response.RESPONSE.STATUS[0];
				if (!status._Condition.Trim().StartsWith("SUCCES"))
				{

					MessageBox.Show(status._Description, status._Condition);
					error = true;
				}
			}
			catch{};
			try 
			{
				status =response.RESPONSE.RESPONSE_DATA[0].PROPERTY_INFORMATION_RESPONSE.STATUS;
				if (!status._Condition.Trim().StartsWith("SUCCES"))
				{
					MessageBox.Show(status._Description, status._Condition);
					error = true;
				}
			}
			catch{};
			try 
			{
				status = response.RESPONSE.RESPONSE_DATA[0].PROPERTY_INFORMATION_RESPONSE._PRODUCT[0].STATUS;
				if (!status._Condition.Trim().StartsWith("SUCCES"))
				{
					MessageBox.Show(status._Description, status._Condition);
					error = true;
				}
			}
			catch{};
			try 
			{
				DTD.Response._MULTIPLE_RECORDS[] multipla 
					= response.RESPONSE.RESPONSE_DATA[0].PROPERTY_INFORMATION_RESPONSE._PROPERTY_INFORMATION[0]._MULTIPLE_RECORDS;
				
				FormMultipleProperiesFound form = new FormMultipleProperiesFound();

				form.lvFill(multipla);
				form.ShowDialog();

				int iterator = form.selected;
				if (iterator>=0)
				{
				
					try
					{
						textBoxStreetNoA.Text = getSecureString(multipla[iterator].PROPERTY._PARSED_STREET_ADDRESS._HouseNumber);
					}
					catch{}
					try
					{
						textBoxPreDirA.Text = getSecureString(multipla[iterator].PROPERTY._PARSED_STREET_ADDRESS._DirectionPrefix);
					}
					catch{}
					try
					{
						textBoxStreetA.Text = getSecureString(multipla[iterator].PROPERTY._PARSED_STREET_ADDRESS._StreetName);
					}
					catch{}
					try
					{
						textBoxPostDirA.Text = getSecureString(multipla[iterator].PROPERTY._PARSED_STREET_ADDRESS._DirectionSuffix);
					}
					catch{}
					try
					{
						textBoxSuffixA.Text = getSecureString(multipla[iterator].PROPERTY._PARSED_STREET_ADDRESS._StreetSuffix);
					}
					catch{}
					try
					{
						textBoxUnitNoA.Text = getSecureString(multipla[iterator].PROPERTY._PARSED_STREET_ADDRESS._ApartmentOrUnit);
					}
					catch{}
					try
					{
						textBoxZipA.Text = getSecureString(multipla[iterator].PROPERTY._PostalCode);
					}
					catch{}
					try
					{
						textBoxCountyA.Text = getSecureString(multipla[iterator].PROPERTY._County);
					}
					catch{}
					try
					{
						textBoxStateA.Text = getSecureString(multipla[iterator].PROPERTY._State);
					}
					catch{}
					try
					{
						//textbox = getSecureString(multipla[iterator].PROPERTY._AssessorsParcelIdentifier);
					}
					catch{}
				}
				error = true;
			
			}
			catch{};


			listViewPropertyProfilesComps.Items.Clear();

			return !error;

			//FillList();
			
		}
		#endregion
		#region . getComps .

		private void buttonGetComps_reenginered_Click(object sender, EventArgs e) 
		{
			return;
		}


		private void buttonGetComps_Click(object sender, System.EventArgs e)
		{
			if (textBoxZipA.Text.Trim().Equals("")
				&&
				(textBoxCountyA.Text.Trim().Equals("")
				||
				textBoxStateA.Text.Trim().Equals("")
				)
				) 
			{
				MessageBox.Show("Please enter ZIP or County & State info!","Address Info Incomplete!");
				return;
			}
/*
			DialogResult dlg = 
				MessageBox.Show
				(
				"You will incur charge or credit reduction!\nDo you want to continue?"
				, "Warning!"
				, MessageBoxButtons.YesNo
				, MessageBoxIcon.Warning
				);
			if (dlg == DialogResult.No)
			{
				return;
			}
*/
			string log_filename = @".\COMPS_usage_trace.log";
			TextWriter txtwrtr = new StreamWriter(log_filename,
				true, Encoding.UTF8);
			txtwrtr.WriteLine
				(
				DateTime.Now.ToString("yyyy.MM.dd HH:mm:ss")
				+ " " +
				"COMPS Requested"
				);
			txtwrtr.Flush();
			txtwrtr.Close();

			DTD.Request.REQUEST_GROUP request = buildRequest();
			//dumpObject(request, "_COMPS_REQUEST.xml");

			XmlSerializer requestSerializer  
				= new XmlSerializer(typeof(DTD.Request.REQUEST_GROUP));
			XmlSerializer responseSerializer  
				= new XmlSerializer(typeof(DTD.Response.RESPONSE_GROUP));

			MemoryStream requestStream = new MemoryStream();
			MemoryStream responseStream = new MemoryStream();

			// IMPORTANT  -  we do not need a namespace!
			System.Xml.Serialization.XmlSerializerNamespaces nmspace = new XmlSerializerNamespaces();
			nmspace.Add("","");
			// IMPORTANT  -  we do not need a namespace!
				
			System.Xml.XmlWriter writer = new XmlTextWriter(requestStream, System.Text.UTF8Encoding.UTF8);
			//writer.Formatting = System.Xml.Formatting.Indented;
			writer.WriteStartDocument();
			//TODO: define string for request..
			writer.WriteDocType("REQUEST_GROUP", null, "C2DRequestv2.0.dtd",null);
			requestSerializer.Serialize(writer, request,nmspace);
			writer.Flush();
	
			//requestSerializer.Serialize(requestStream, request);
			byte[] requestArray = null;
			byte[] responseArray = null;
			string resultMessage = string.Empty;
			try
			{
				Cursor.Current = Cursors.WaitCursor;
				Application.DoEvents();

				int count =(int)requestStream.Length-3;
				requestArray = new byte[count];
				long newPos = requestStream.Seek(0, SeekOrigin.Begin);
		
				//TA++:08.04.2005
				byte[] reqStream = requestStream.ToArray();
				for (int i = 3; i < requestStream.Length; i++)
				{
					requestArray[i - 3] = (byte)reqStream[i];
				}
				//TA--:08.04.2005
				writer.Close();

				// ++ web

				WR_SDC.ServiceDataConsumption sdc = new WR_SDC.ServiceDataConsumption();
                sdc.Url = System.Configuration.ConfigurationManager.AppSettings["SDC"];

                WR_SDC.Registration rg = new WR_SDC.Registration();

				//rg.KeyCode = "58L4-TI2F-1Q27-4S98";
				rg.KeyCode = Globals.currentRegKey;

				Globals.last_req = requestArray;

				sdc.RequestDataCOMPS(
					rg,
					requestArray,
					out responseArray, 
					out resultMessage);

				if (null == responseArray)
				{
					if (string.Empty == resultMessage)
					{
						MessageBox.Show(this, "Web services did not answer correctly...", "Web Services", MessageBoxButtons.OK, MessageBoxIcon.Information);
					}
					else
					{
						MessageBox.Show(this, resultMessage, "Web Services", MessageBoxButtons.OK, MessageBoxIcon.Information);
					}
					return;
				}

				if (string.Empty != resultMessage)
				{
					MessageBox.Show(resultMessage);
					return;
				}


				sdc = null;

				if (0==responseArray.Length)
				{
					//TA++28.06.2005
					//COMPS
					//BEGIN
					//OLD++
					//throw new Exception
						//(resultMessage + " - at GetComps");
					//OLD--
					//NEW++
					string message = string.Empty;
					int iPos1 = resultMessage.IndexOf("#");
					int iPos2 = resultMessage.IndexOf("#", iPos1 + 1);
					if (-1 != iPos1 && -1 != iPos2)
						message = resultMessage.Substring(iPos1 + 1, iPos2 - (iPos1 + 1));
					else
						message = resultMessage;
					MessageBox.Show(this, message, string.Empty, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
					return;
					//NEW--
					//END
				}
				else
				{
					txtwrtr = new StreamWriter(log_filename,
						true, Encoding.UTF8);
					txtwrtr.WriteLine
						(
						DateTime.Now.ToString("yyyy.MM.dd HH:mm:ss")
						+ " " +
						"COMPS Request Successful!"
						);
					txtwrtr.Flush();
					txtwrtr.Close();
				}
		
				// -- web

				// ++ local
				//			SoftwareRegistration.ClassLibrary.WebServiceCore.Connect(requestArray,
				//				out responseArray);
				// -- local

				//			System.IO.FileStream fs = new FileStream("response2.xml", FileMode.Create);
				//			fs.Write((byte[])responseStream.ToArray(), 0, (int)responseStream.Length);
				//			fs.Flush();
				//			fs.Close();

				responseStream.Write(responseArray,0,responseArray.Length);

				responseStream.Position = 0;
				response = (DTD.Response.RESPONSE_GROUP)responseSerializer.Deserialize(responseStream);
				if (CheckValid())
				{

					//TODO: SAVE DATA AND STORE COMPS
					DateTime storedDate = DateTime.Now;
					if (null == dbComps || null == dbComps[0])
					{
						dbComps = new comps[1];
						dbComps[0] = new comps(new Guid(), DateTime.Now, 0, 0,  null,"");
					}

					dbComps[0].P_xmlStream = responseStream.ToArray();
					dbComps[0].P_dateOfRetrieve = storedDate;
					dbComps[0].P_taggedCSV="";
					dbComps[0].P_estimatedValue=0;
					textBoxEstValue.Text=getMoney("0");

					textBoxDate.Text = storedDate.ToShortDateString();
					comps = response.
						RESPONSE.
						RESPONSE_DATA[0].
						PROPERTY_INFORMATION_RESPONSE.
						_PROPERTY_INFORMATION[0].
						_DATA_PROVIDER_COMPARABLE_SALES;

					FillList();
				}
			}
			catch(Exception ex)
			{
				MessageBox.Show(this, "Too many connections on server.\nTry again later.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Information);
//#if DEBUG
				MessageBox.Show(ex.Message + "\n" + ex.InnerException);
//#endif
//				throw ex;
			}
			finally
			{
				Cursor.Current = Cursors.Default;
			}
			return;
			#region . commented ... useful .			
			/*
				 *  EXAMPLE HOW TO CONVERT STRING TO OBJECT
				 *  NOTE - ALWAYS GO DOWN TO MEMORY STREAM!!!
				 * 
				string xml = 
					"<?xml version='1.0' encoding='UTF-8'?>"
					+"<RESPONSE_GROUP>"
					+"<RESPONDING_PARTY/>"
					+"<RESPOND_TO_PARTY _Name='null'/>"
					+"<RESPONSE ResponseDateTime='Wed Feb 23 01:58:59 PST 2005' InternalAccountIdentifier='null' _JobIdentifier='null'>"
					+"<RESPONSE_DATA>"
					+"<PROPERTY_INFORMATION_RESPONSE>"
					+"<STATUS _Condition='FAILURE           ' _Code='0005' _Description='XML IS NOT WELL-FORMED OR IS INVALID. ERROR ON LINE 1: CONTENT IS NOT ALLOWED IN PROLOG.'/>"
					+"</PROPERTY_INFORMATION_RESPONSE>"
					+"</RESPONSE_DATA>"
					+"</RESPONSE>"
					+"</RESPONSE_GROUP>";

				System.IO.MemoryStream stream = new MemoryStream();
				stream.Write(UTF8Encoding.UTF8.GetBytes(xml),0,UTF8Encoding.UTF8.GetByteCount(xml));
				stream.Seek(0,SeekOrigin.Begin);
			
				System.Xml.Serialization.XmlSerializer deser 
					= new XmlSerializer(typeof(DTD.Response.RESPONSE_GROUP));

				DTD.Response.RESPONSE_GROUP x 
					= deser.Deserialize(stream)
					as DTD.Response.RESPONSE_GROUP;
				*/
		
	
			//RESPONSE_GROUP y = DeserializeXmlFile(@"c:\xml\dcrr.xml");
			

			// dump request to see how it looks

			/*
			string sNow = DateTime.Now.ToString("yyMMddHHmmss");

			DTD.Request.REQUEST_GROUP rqg = buildRequest();

			//			dumpObject(rqg, "request.xml");
			//return;
			// create serializers
			XmlSerializer requestSerializer = 
				new XmlSerializer(typeof(DTD.Request.REQUEST_GROUP));
			XmlSerializer responseSerializer = 
				new XmlSerializer(typeof(DTD.Response.RESPONSE_GROUP));

			MemoryStream stream = new MemoryStream();
			UTF8Encoding encoding = new UTF8Encoding();


			//xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
			// IMPORTANT  -  we do not need a namespace!
			System.Xml.Serialization.XmlSerializerNamespaces nmspace = new XmlSerializerNamespaces();
			nmspace.Add("","");
			// IMPORTANT  -  we do not need a namespace!
				
			System.Xml.XmlTextWriter xmlWriter = new XmlTextWriter(stream, encoding);
			xmlWriter.Formatting = System.Xml.Formatting.Indented;
			xmlWriter.WriteStartDocument();
			xmlWriter.WriteDocType("REQUEST_GROUP", null, "C2DRequestv2.0.PropertyProfileComps",null);
			requestSerializer.Serialize(xmlWriter, rqg, nmspace);
			xmlWriter.Flush();
			//xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx



			//get previusly prepared object
			//XmlDocument xmldoc = new XmlDocument();
			//xmldoc.Load(@"request_short_comparable_report.xml");
			//string xml = xmldoc.InnerXml.ToString();

			// get serialized object ready for sending
			//requestSerializer.Serialize(stream,rqg);
			string xml = encoding.GetString(stream.ToArray());
			xml = 
				//"xml=" + 
				//HttpUtility.UrlEncode(xml);
				xml;

			Trace.WriteLine("sent: " + xml);
			Trace.Flush();

			//xxxxxxxxxxxxxxxxxxxxxxxxxxx
			System.IO.FileStream edc = new FileStream(sNow + "request.xml",System.IO.FileMode.Create);
			edc.Write(stream.ToArray(), 0, (int)stream.Length);
			edc.Flush();
			edc.Close();
			//xxxxxxxxxxxxxxxxxxxxxxxxxxx


			try
			{
				Trace.WriteLine("deserializing");
				DTD.Response.RESPONSE_GROUP rsg 
					= responseSerializer.Deserialize(responseStream) 
					as DTD.Response.RESPONSE_GROUP;

				response.Close();
				stream.Close();

				
				Trace.WriteLine("serializing response");
				dumpObject(rsg, sNow + "response.xml");


				//RESPONSE_GROUP rsg = DeserializeXmlFile(@"response.xml");
				
				Trace.WriteLine("Filling form");
				if (null == rsg)
				{
					Trace.WriteLine("null == rsg");
				}
				Start(rsg,1200);
				Trace.WriteLine("Filled form");


				//_PROPERTY_INFORMATION x = 
				//	y.RESPONSE.RESPONSE_DATA[0].PROPERTY_INFORMATION_RESPONSE.Items[1]
				//	as _PROPERTY_INFORMATION;
				//comps =  x._DATA_PROVIDER_COMPARABLE_SALES;
				//lvFill();
			}
			catch (Exception wex)
			{
				System.Diagnostics.Debug.WriteLine(wex.Message);
				MessageBox.Show(wex.Message);
				Trace.WriteLine("exception " + wex.Message );
				Trace.WriteLine(wex.StackTrace);
				Trace.WriteLine(wex.InnerException);
				Trace.WriteLine(wex.Source);
				throw wex;
			}

			return;
			
			*/
			#endregion . commented ... useful .			
		}
		#endregion
		#region . buildRequest .
		private DTD.Request.REQUEST_GROUP buildRequest()
		{

			DTD.Request.REQUEST_GROUP rqg 
				= new DTD.Request.REQUEST_GROUP();

			DTD.Request._SEARCH_CRITERIA sc
				= new DTD.Request._SEARCH_CRITERIA();

			DTD.Request._COMPARABLE_SEARCH cmps
				= new DTD.Request._COMPARABLE_SEARCH();

			DTD.Request._PROPERTY_CRITERIA pc
				= new DTD.Request._PROPERTY_CRITERIA();


			rqg.REQUEST 
				= new DTD.Request.REQUEST();
			rqg.REQUEST.REQUESTDATA 
				= new DTD.Request.REQUESTDATA[1];
			rqg.REQUEST.REQUESTDATA[0] 
				= new DTD.Request.REQUESTDATA();
			rqg.REQUEST.REQUESTDATA[0].PROPERTY_INFORMATION_REQUEST 
				= new DTD.Request.PROPERTY_INFORMATION_REQUEST();
			rqg.REQUEST.REQUESTDATA[0].PROPERTY_INFORMATION_REQUEST._SEARCH_CRITERIA 
				= new DTD.Request._SEARCH_CRITERIA();

			DTD.Request._CONNECT2DATA_PRODUCT c2dp
				= new DTD.Request._CONNECT2DATA_PRODUCT();

			c2dp._DetailedComparableReport 
				= DTD.Request._CONNECT2DATA_PRODUCT_DetailedComparableReport.Y;

			rqg.REQUEST.REQUESTDATA[0].PROPERTY_INFORMATION_REQUEST._CONNECT2DATA_PRODUCT = c2dp;
			
			rqg.REQUEST.REQUESTDATA[0].PROPERTY_INFORMATION_REQUEST._ActionType 
				= DTD.Request.PROPERTY_INFORMATION_REQUEST_ActionType.Submit;
			rqg.REQUEST.REQUESTDATA[0].PROPERTY_INFORMATION_REQUEST._ActionTypeSpecified = true;

			DTD.Request._RESPONSE_CRITERIA rescrit 
				= new DTD.Request._RESPONSE_CRITERIA();

			rescrit._NumberComparablesType 
				= DTD.Request._RESPONSE_CRITERIA_NumberComparablesType.Item50;
			rescrit._NumberCompFarmRecordsType
				= DTD.Request._RESPONSE_CRITERIA_NumberCompFarmRecordsType.Item100;
			rescrit._NumberSubjectPropertiesType
				= DTD.Request._RESPONSE_CRITERIA_NumberSubjectPropertiesType.Item100;



			rqg.REQUEST.REQUESTDATA[0].PROPERTY_INFORMATION_REQUEST._RESPONSE_CRITERIA
				= rescrit;
 
			pc.PARSED_STREET_ADDRESS = new DTD.Request.PARSED_STREET_ADDRESS();

			pc.PARSED_STREET_ADDRESS._StreetName = textBoxStreetA.Text;
			pc.PARSED_STREET_ADDRESS._HouseNumber = textBoxStreetNoA.Text;
			pc.PARSED_STREET_ADDRESS._ApartmentOrUnit = textBoxUnitNoA.Text;

			pc.PARSED_STREET_ADDRESS._DirectionPrefix = textBoxPreDirA.Text;
			pc.PARSED_STREET_ADDRESS._DirectionSuffix = textBoxPostDirA.Text;
			pc.PARSED_STREET_ADDRESS._StreetSuffix = textBoxSuffixA.Text;

			pc._PostalCode = textBoxZipA.Text;

			pc._County = textBoxCountyA.Text;
			pc._State = textBoxStateA.Text;

			/*
			pc._StreetAddress = textBoxAddress.Text;
			pc._PostalCode = textBoxSiteZIP.Text;
			int a = textBoxSiteCityST.Text.IndexOf("/");
			if (a == -1) 
			{
				a = textBoxSiteCityST.Text.Length;
			}
			else
			{
				pc._State = textBoxSiteCityST.Text.Substring(a+1,textBoxSiteCityST.Text.Length-1-a);
			}
			pc._City = textBoxSiteCityST.Text.Substring(0,a);
			*/

			DTD.Request._LAND_USE lu = new DTD.Request._LAND_USE();
			//lu._ResidentialType 
			//	= DTD.Request._LAND_USE_ResidentialType.SingleFamilyResidential;
			//lu._ResidentialTypeSpecified = true;

			//pc._LAND_USE = new DTD.Request._LAND_USE[1];
			//pc._LAND_USE[0] = lu;

			rqg.REQUEST.REQUESTDATA[0].PROPERTY_INFORMATION_REQUEST._PROPERTY_CRITERIA = pc;
			
			//cmps._PoolOptionType = DTD.Request._COMPARABLE_SEARCH_PoolOptionType.PropertiesWithAndWithoutPools;
			cmps._GeographicConstraintTypeSpecified = true;
			cmps._GeographicConstraintType = DTD.Request._COMPARABLE_SEARCH_GeographicConstraintType.SameCity;
			
			cmps._MonthsBackNumber=comboBoxSalesInLast.Text.TrimEnd(new char[] {' ','m','o', '.'});
			cmps._LivingAreaVariancePercent=comboBoxSqFtRange.Text.Trim(new char[] {'+','-','/', '%',' '});
			cmps._DistanceFromSubjectNumber=comboBoxSearchRadius.Text;

			DTD.Request._LAND_USE lu2 = new DTD.Request._LAND_USE();
			lu2._SameAsSubjectType 
				= DTD.Request._LAND_USE_SameAsSubjectType.Yes;
			lu2._SameAsSubjectTypeSpecified=true;

			cmps._LAND_USE = new DTD.Request._LAND_USE[1];
			cmps._LAND_USE[0] = lu2;

			rqg.REQUEST.REQUESTDATA[0].PROPERTY_INFORMATION_REQUEST._SEARCH_CRITERIA.Item 
				= cmps;

			rqg.REQUESTING_PARTY 
				= new DTD.Request.REQUESTING_PARTY[1];
			
			rqg.MISMOVersionID = "2.1";
			
			/*
			rqg.REQUESTING_PARTY[0] 
				= new DTD.Request.REQUESTING_PARTY();
			
			rqg.REQUESTING_PARTY[0]._Name="Blue Sky Lending"; 
			rqg.REQUESTING_PARTY[0]._StreetAddress="123 Main"; 
			rqg.REQUESTING_PARTY[0]._City="Anaheim"; 
			rqg.REQUESTING_PARTY[0]._State="Ca"; 
			rqg.REQUESTING_PARTY[0]._PostalCode="92840";

			rqg.REQUESTING_PARTY[0].PREFERRED_RESPONSE
				= new DTD.Request.PREFERRED_RESPONSE[1];

			rqg.REQUESTING_PARTY[0].PREFERRED_RESPONSE[0]
				= new DTD.Request.PREFERRED_RESPONSE();

			rqg.REQUESTING_PARTY[0].PREFERRED_RESPONSE[0]._Format="XML";
			rqg.REQUESTING_PARTY[0].PREFERRED_RESPONSE[0]._DestinationDescription="";

			rqg.RECEIVING_PARTY = new DTD.Request.RECEIVING_PARTY();

			rqg.RECEIVING_PARTY._Name="Blue Sky Lending"; 
			rqg.RECEIVING_PARTY._StreetAddress="123 Main";
			rqg.RECEIVING_PARTY._City="Anaheim";
			rqg.RECEIVING_PARTY._State="Ca";
			rqg.RECEIVING_PARTY._PostalCode="92840";
			*/
			
			//rqg.REQUEST.LoginAccountIdentifier="PROFGRAB";
			//rqg.REQUEST.LoginAccountPassword="C2STAGE"; 
			rqg.REQUEST.LoginAccountIdentifier="PROFT1";
			//rqg.REQUEST.LoginAccountPassword="GRABR1"; 
			rqg.REQUEST.LoginAccountPassword="TKS1GRABR"; 
			rqg.REQUEST._JobIdentifier="TEST MASTER V2.0"; 
			rqg.REQUEST._RecordIdentifier="12345A8";
			rqg.REQUEST._HVERequestTypeSpecified = true;
			rqg.REQUEST._HVERequestType 
				= DTD.Request.REQUEST_HVERequestType.Item02;
			rqg.REQUEST._HVCustomerIdentifier="";

			return rqg;
		}
		#endregion
		#region . dumpObject to file .
		private void dumpObject(object theObject, string filename)
		{
			System.Xml.Serialization.XmlSerializer serializer
				= new System.Xml.Serialization.XmlSerializer(theObject.GetType());

			// IMPORTANT  -  we do not need a namespace!
			System.Xml.Serialization.XmlSerializerNamespaces nmspace = new XmlSerializerNamespaces();
			nmspace.Add("","");
			// IMPORTANT  -  we do not need a namespace!
				
			System.Xml.XmlTextWriter writer = new XmlTextWriter(filename, System.Text.UTF8Encoding.UTF8);
			writer.Formatting = System.Xml.Formatting.Indented;
			writer.WriteStartDocument();
			//TODO: define string for request..
			writer.WriteDocType("REQUEST_GROUP", null, "C2DRequestv2.0.PropertyProfileComps",null);
			serializer.Serialize(writer, theObject,nmspace);
			writer.Flush();
			writer.Close();
		}
		#endregion
		#region . deserialize .
		private DTD.Response.RESPONSE_GROUP DeserializeXmlFile(string path)
		{
			DTD.Response.RESPONSE_GROUP x ;
			FileStream fs = new FileStream(path, FileMode.Open);
			
			XmlSerializer serializer = new XmlSerializer(typeof(DTD.Response.RESPONSE_GROUP));
			x = (DTD.Response.RESPONSE_GROUP)serializer.Deserialize(fs);;
			fs.Close();
			return x;
		}
		#endregion
		#region ˇ FillList ˇ
		private void FillList()
		{
			ListViewItem lvItem;
			for(
				int iterator = comps.GetLowerBound(0); 
				iterator <= comps.GetUpperBound(0); 
				iterator++
				)
			{
				string cel01="";
				string cel02="";
				string cel03="";
				string cel04="";
				string cel05="";
				string cel06="";
				string cel07="";
				string cel08="";
				string cel09="";
				string cel10="";
				string cel11="";
				string cel12="";
				string cel13="";
				string cel14="";
				string cel15="";
				string cel16="";
				string cel17="";
				string cel18="";


				try
				{
					cel01 = getSecureString(comps[iterator]._DistanceFromSubjectNumber);
				}
				catch{}
				try
				{
					cel02 = getSecureString(comps[iterator].PROPERTY._SubdivisionIdentifier);
				}
				catch{}
				try
				{
					cel03 = getNumber(getSecureString(comps[iterator].PROPERTY._PROPERTY_CHARACTERISTICS._IMPROVEMENTS._ROOM_COUNT._TotalLivingAreaSquareFeetNumber));
				}
				catch{}
				try
				{
					cel04 = getSecureString(comps[iterator].PROPERTY._PROPERTY_CHARACTERISTICS._IMPROVEMENTS._GENERAL_DESCRIPTION._YearBuiltDateIdentifier);
				}
				catch{}
				try
				{
					cel05 = getSecureString(comps[iterator].PROPERTY._PROPERTY_CHARACTERISTICS._IMPROVEMENTS._ROOM_COUNT._TotalRoomCount);
				}
				catch{}
				try
				{
					cel06 = getSecureString(comps[iterator].PROPERTY._PROPERTY_CHARACTERISTICS._IMPROVEMENTS._ROOM_COUNT._TotalBedroomsCount);
				}
				catch{}
				try
				{
					cel07 = getSecureString(comps[iterator].PROPERTY._PROPERTY_CHARACTERISTICS._IMPROVEMENTS._ROOM_COUNT._TotalBathsCount);
				}
				catch{}
				try
				{
					cel08 = getSecureString(comps[iterator].PROPERTY._PROPERTY_CHARACTERISTICS._IMPROVEMENTS._FEATURES._POOL._HasFeatureIndicator);
				}
				catch{}
				try
				{
					cel09 = getDate(getSecureString(comps[iterator].PROPERTY._PROPERTY_HISTORY._SALES_HISTORY[0]._LastSalesDate));
				}
				catch{}
				try
				{
					cel10 = getMoney(getSecureString(comps[iterator].PROPERTY._PROPERTY_HISTORY._SALES_HISTORY[0]._LastSalesPriceAmount));
				}
				catch{}
				try
				{
					cel11 = getMoney(getSecureString(comps[iterator].PROPERTY._PROPERTY_HISTORY._SALES_HISTORY[0]._PricePerSquareFootAmount));
				}
				catch{}
				try
				{
					cel12 = getNumber(getSecureString(comps[iterator].PROPERTY._PROPERTY_CHARACTERISTICS._SITE._DIMENSIONS._LotAreaSquareFeetNumber));
				}
				catch{}
				try
				{
					cel13 = getMoney(getSecureString(comps[iterator].PROPERTY._PROPERTY_TAX._TotalAssessedValueAmount));
				}
				catch{}
				try
				{
					cel14 = getSecureString(comps[iterator].PROPERTY._PARSED_STREET_ADDRESS._HouseNumber);
				}
				catch{}
				try
				{
					cel15 = getSecureString(comps[iterator].PROPERTY._PARSED_STREET_ADDRESS._StreetName);					
				}
				catch{}
				try
				{
					cel16 = getSecureString(comps[iterator].PROPERTY._PostalCode);
				}
				catch{}
				try
				{
					cel17 = getSecureString(comps[iterator].PROPERTY._City);
				}
				catch{}
				try
				{
					cel18 = getSecureString(comps[iterator]._ComparableNumber);
				}
				catch{}

				lvItem = new ListViewItem
					(new string[] {
									  ""
									  ,cel01
									  ,cel02
									  ,cel03
									  ,cel04
									  ,cel05
									  ,cel06
									  ,cel07
									  ,cel08
									  ,cel09
									  ,cel10
									  ,cel11
									  ,cel12
									  ,cel13
									  ,cel14
									  ,cel15
									  ,cel16
									  ,cel17
									  ,cel18
				});
				listViewPropertyProfilesComps.Items.Add(lvItem);
			}
		}
		#endregion
		#region ˇ lvPropertyItems_ColumnClick ˇ
/*
		private void lvPropertyItems_ColumnClick(object sender, System.Windows.Forms.ColumnClickEventArgs e)

		{

			// Set the ListViewItemSorter property to a new ListViewItemComparer 

			// object. Setting this property immediately sorts the 

			// ListView using the ListViewItemComparer object.                

			this.asc = !this.asc;

			if (true == this.asc)

				this.listViewPropertyProfilesComps.ListViewItemSorter = new ListViewItemComparer(e.Column);

			else

				this.listViewPropertyProfilesComps.ListViewItemSorter = new ListViewItemComparerDesc(e.Column);

 

		}
*/
		#endregion ˇ lvPropertyItems_ColumnClick ˇ
		#region . tagging buttons .
		private void buttonTagAll_Click(object sender, System.EventArgs e)
		{
			foreach(ListViewItem lvi in listViewPropertyProfilesComps.Items)
			{
				lvi.Checked = true;
			}
		}

		private void buttonUntagAll_Click(object sender, System.EventArgs e)
		{
			foreach(ListViewItem lvi in listViewPropertyProfilesComps.Items)
			{
				lvi.Checked = false;
			}
		
		}

		private void buttonInvert_Click(object sender, System.EventArgs e)
		{
			foreach(ListViewItem lvi in listViewPropertyProfilesComps.Items)
			{
				lvi.Checked = !lvi.Checked;
			}
		}
		#endregion
		#region . recalculate .
		private void buttonRecalculate_Click(object sender, System.EventArgs e)
		{
			if (0==listViewPropertyProfilesComps.Items.Count)
			{
				return;
			}
			string strSelected = "";
			double dorigsqft;
			try
			{
				dorigsqft = Convert.ToDouble(textBoxOrigSqFt.Text);
//				if (0==dorigsqft)
//					throw new Exception();
			}
			catch
			{
				MessageBox.Show("Please enter the Subject Property Sq. Ft. before Recalculating");
				textBoxEstValue.Text = getMoney("0");
				return;
			}

			double estimate = 0;
			int counter = 0;
			try
			{
				foreach(ListViewItem lvi in listViewPropertyProfilesComps.Items)
				{
					if (lvi.Checked)
					{
						string price =lvi.SubItems[10].Text.Replace("$","");

						string sqft = lvi.SubItems[3].Text;
						double dprice;
						double dsqft;
						try
						{
							dprice = Convert.ToDouble(price);
							dsqft = Convert.ToDouble(sqft);

						}
						catch
						{
							MessageBox.Show("Please Tag only the properties that have the Sales Price and Sq. Ft.!");
							textBoxEstValue.Text = "";
							return;
						}

						estimate = estimate + (dprice / dsqft);
						counter++;
						strSelected = strSelected + lvi.SubItems[18].Text + ",";
					}
				}
				if (counter==0)
				{
					MessageBox.Show("You must select (tag) at least one property!");
					textBoxEstValue.Text = "";
					return;
				}
				strSelected = strSelected.Substring(0,strSelected.Length-1);
				
				estimate = estimate / counter;
				estimate = estimate * dorigsqft;
				_estimatedValue = (decimal)estimate;
				
				dbComps[0].P_taggedCSV = strSelected;
				dbComps[0].P_estimatedValue = (decimal)estimate;
				
				textBoxEstValue.Text = getMoney(estimate.ToString());
/*				//TA++, 04.04.2005. Added due to additional user request - as quickly as possible
				try
				{
					object parent = this.Parent;
					if (parent is TabPage)
					{
						TabPage tp = (TabPage)parent;
						parent = tp.Parent;
						if (parent is TabControl)
						{
							TabControl tc = (TabControl)parent;
							parent = tc.Parent;
							if (parent is Panel)
							{
								Panel p = (Panel)parent;
								parent = p.Parent;
								if (parent is ContactScreen)
								{
									ContactScreen sc = (ContactScreen)parent;
									System.IFormatProvider format =	new System.Globalization.CultureInfo("en-US", true);
									sc.ARV = _estimatedValue.ToString("c", format);
								}
							}
						}
					}
				}
				catch (Exception)
				{
					//No Action !!!
				}
				//TA--, 04.04.2005. Added due to additional user request - as quickly as possible
*/			}
			catch
			{
				MessageBox.Show("Error calculating data", "", MessageBoxButtons.OK, MessageBoxIcon.Stop);
				textBoxEstValue.Text = "Error";
			}
		}
		#endregion
		#region . estimatedValue .
		public decimal estimatedValue
		{
			get {return _estimatedValue;}
		}
		#endregion
		#region . save data to db .
		public bool Save(Guid PropertyGuid)
		{
			bool success = false;		
			/*
			try
			{
				dbComps[0].P_estimatedValue = decimal.Parse(textBoxEstValue.Text,System.Globalization.NumberStyles.Currency);
			}
			catch{}
			*/
			try
			{
				dbComps[0].P_privateEstimatedValue = decimal.Parse(textBoxEstValuePrivate.Text,System.Globalization.NumberStyles.Currency);
			}
			catch{}

			try
			{
				if (null != dbComps && null != dbComps[0] && null != dbComps[0].P_xmlStream)//TA++: 02.04.2004. - To Prevent Runtime Crashes
				{
					dbComps[0].P_propertyItemId = PropertyGuid;
					
					if (null != this._dbConn)
					{
						if (_compsInDb) 
							dbComps[0].Update(_dbConn);
						else
						{
							dbComps[0].Insert(_dbConn);		
							_compsInDb=true;
						}
					}
					else
					{
						success = false;
						return success;
					}
				}
				success = true;
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.StackTrace,"Problem: Data not saved.");
			}
			return success;
		}
		#endregion
		#region . dummyfill .
		private void dummyFill()
		{ 
			System.Windows.Forms.ListViewItem listViewItem1 = new System.Windows.Forms.ListViewItem(new string[] {"","0.2","Warner Ranch","Chandler","1,834","1997","6","3","2,5","11/11/04","$280,000.00","7,650","$178,000.00"});
			System.Windows.Forms.ListViewItem listViewItem2 = new System.Windows.Forms.ListViewItem(new string[] {"","0.3","Warner Ranch","Chandler","2,214","1997","6","3","2,5","24/06/04","$276,000.00","6,850","$171,000.00"});
			System.Windows.Forms.ListViewItem listViewItem3 = new System.Windows.Forms.ListViewItem(new string[] {"","0.3","Warner Ranch","Chandler","2,066","1998","7","4","3","05/10/04","$266,000.00","7,230","$181,000.00"});
			System.Windows.Forms.ListViewItem listViewItem4 = new System.Windows.Forms.ListViewItem(new string[] {"","0.5","Warner Ranch","Chandler","2,066","1997","7","4","3","08/09/04","$258,000.00","7,660","$174,000.00"});
			System.Windows.Forms.ListViewItem listViewItem5 = new System.Windows.Forms.ListViewItem(new string[] {"","0.6","Pecan Grove","Chandler","2,116","1996","7","4","3","02/10/04","$269,000.00","8,040","$169,000.00"});
			System.Windows.Forms.ListViewItem listViewItem6 = new System.Windows.Forms.ListViewItem(new string[] {"","0.7","Pecan Grove","Chandler","1,905","1996","6","3","2,5","17/11/04","$262,000.00","7,880","$170,500.00"});
			System.Windows.Forms.ListViewItem listViewItem7 = new System.Windows.Forms.ListViewItem(new string[] {"","0.7","Pecan Grove","Chandler","2,116","1997","6","3","2","13/07/04","$291,000.00","6,950","$173,500.00"});
			System.Windows.Forms.ListViewItem listViewItem8 = new System.Windows.Forms.ListViewItem(new string[] {"","0.7","Parke Tempe","Chandler","2,050","1998","7","4","2","12/10/04","$290,000.00","6,720","$178,200.00"});
			System.Windows.Forms.ListViewItem listViewItem9 = new System.Windows.Forms.ListViewItem(new string[] {"","0.8","Parke Tempe","Chandler","2,050","1997","7","4","2","06/08/04","$267,000.00","7,550","$178,000.00"});
			System.Windows.Forms.ListViewItem listViewItem10 = new System.Windows.Forms.ListViewItem(new string[] {"","0.8","Parke Tempe","Chandler","2,180","1997","7","4","2","19/11/04","$280,000.00","7,320","$168,300.00"});
			listViewPropertyProfilesComps.Items.Add(listViewItem1); 
			listViewPropertyProfilesComps.Items.Add(listViewItem2); 
			listViewPropertyProfilesComps.Items.Add(listViewItem3); 
			listViewPropertyProfilesComps.Items.Add(listViewItem4); 
			listViewPropertyProfilesComps.Items.Add(listViewItem5); 
			listViewPropertyProfilesComps.Items.Add(listViewItem6); 
			listViewPropertyProfilesComps.Items.Add(listViewItem7); 
			listViewPropertyProfilesComps.Items.Add(listViewItem8); 
			listViewPropertyProfilesComps.Items.Add(listViewItem9); 
			listViewPropertyProfilesComps.Items.Add(listViewItem10); 
		}
		#endregion
		#region . open property .
		private void listViewPropertyProfilesComps_DoubleClick(object sender, System.EventArgs e)
		{
			listViewPropertyProfilesComps.SelectedItems[0].Checked = !listViewPropertyProfilesComps.SelectedItems[0].Checked;
			int selected = int.Parse(listViewPropertyProfilesComps.SelectedItems[0].SubItems[18].Text);
			Form x = new Form();

			UserControlPropertyProfileComps pc = new UserControlPropertyProfileComps();

			x.Controls.Add(pc);
			pc.fillComps(
				comps[selected-1].PROPERTY
				,comps[selected-1]._ComparableNumber
				,listViewPropertyProfilesComps.SelectedItems[0].SubItems[1].Text
				);
			x.Height=pc.Height;
			x.Width=pc.Width;
			pc.prepareComparable();
			pc.Dock = DockStyle.Fill;
			x.Text="Property";
			x.ShowDialog();
			
		}
		#endregion
		#region . num dat parsing .
		private string getSecureString(object str)
		{
			string SecureString = "";
			try
			{
				SecureString = str.ToString();
			}
			catch
			{}
			return SecureString;
		}
		private string getMoney(string sMoney)
		{
			try
			{
				sMoney = Convert.ToDouble(sMoney).ToString("C");
			}
			catch
			{
			}

			return sMoney;
		}

		private string getDecimal(string sDecimal)
		{
			try
			{
				sDecimal = Convert.ToDouble(sDecimal).ToString("N");
			}
			catch
			{
			}

			return sDecimal;
		}
		private string getNumber(string sDecimal)
		{
			try
			{
				sDecimal = Convert.ToDouble(sDecimal).ToString("D");
			}
			catch
			{
			}

			return sDecimal;
		}

		private string getDate(string sDate)
		{
			if (sDate.Length==8)
			{
				try
				{
					DateTime d = new DateTime
						(
						(int.Parse(sDate.Substring(0,4))),
						(int.Parse(sDate.Substring(4,2))),
						(int.Parse(sDate.Substring(6,2)))
						);

					sDate = d.ToShortDateString();
				}
				catch
				{
				}
			}

			return sDate;
		}


		#endregion
		#region . isInStringArray .
		private bool isInStringArray(string[] arr, string str)
		{
			foreach(string x in arr)
			{
				if (x.Equals(str))
					return true;
			}
			return false;
		}
		#endregion
		#region Dispose
		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}
		#endregion
		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.labelRequestParameters = new System.Windows.Forms.Label();
            this.listViewPropertyProfilesComps = new System.Windows.Forms.ListView();
            this.columnHeaderTag = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderDistnace = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderSubdivision = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderSQFT = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderYB = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderRooms = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderBedrooms = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderBath = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderPool = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderSaleDate = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderSalePrice = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderSqFtPrice = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderLotArea = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderAssesed = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderStreetNum = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderStreetName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderZIP = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderCity = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.buttonGetComps = new System.Windows.Forms.Button();
            this.buttonInvert = new System.Windows.Forms.Button();
            this.buttonUntagAll = new System.Windows.Forms.Button();
            this.buttonTagAll = new System.Windows.Forms.Button();
            this.textBoxEstValue = new System.Windows.Forms.TextBox();
            this.textBoxEstValuePrivate = new System.Windows.Forms.TextBox();
            this.comboBoxSqFtRange = new System.Windows.Forms.ComboBox();
            this.comboBoxSearchRadius = new System.Windows.Forms.ComboBox();
            this.comboBoxSalesInLast = new System.Windows.Forms.ComboBox();
            this.textBoxDate = new System.Windows.Forms.TextBox();
            this.buttonRecalculate = new System.Windows.Forms.Button();
            this.textBoxOrigSqFt = new System.Windows.Forms.TextBox();
            this.textBoxStateA = new System.Windows.Forms.TextBox();
            this.textBoxCountyA = new System.Windows.Forms.TextBox();
            this.textBoxZipA = new System.Windows.Forms.TextBox();
            this.textBoxUnitNoA = new System.Windows.Forms.TextBox();
            this.textBoxStreetA = new System.Windows.Forms.TextBox();
            this.textBoxStreetNoA = new System.Windows.Forms.TextBox();
            this.label76 = new System.Windows.Forms.Label();
            this.label74 = new System.Windows.Forms.Label();
            this.label75 = new System.Windows.Forms.Label();
            this.textBoxPreDirA = new System.Windows.Forms.TextBox();
            this.textBoxSuffixA = new System.Windows.Forms.TextBox();
            this.textBoxPostDirA = new System.Windows.Forms.TextBox();
            this.label79 = new System.Windows.Forms.Label();
            this.bPrint = new System.Windows.Forms.Button();
            this.UserControlPropertyProfileCompsListViewlayoutControl1ConvertedLayout = new DevExpress.XtraLayout.LayoutControl();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.comboBoxSqFtRangeitem = new DevExpress.XtraLayout.LayoutControlItem();
            this.textBoxSuffixAitem = new DevExpress.XtraLayout.LayoutControlItem();
            this.textBoxPostDirAitem = new DevExpress.XtraLayout.LayoutControlItem();
            this.textBoxPreDirAitem = new DevExpress.XtraLayout.LayoutControlItem();
            this.textBoxStateAitem = new DevExpress.XtraLayout.LayoutControlItem();
            this.textBoxCountyAitem = new DevExpress.XtraLayout.LayoutControlItem();
            this.textBoxZipAitem = new DevExpress.XtraLayout.LayoutControlItem();
            this.textBoxStreetAitem = new DevExpress.XtraLayout.LayoutControlItem();
            this.textBoxStreetNoAitem = new DevExpress.XtraLayout.LayoutControlItem();
            this.buttonGetCompsitem = new DevExpress.XtraLayout.LayoutControlItem();
            this.textBoxOrigSqFtitem = new DevExpress.XtraLayout.LayoutControlItem();
            this.comboBoxSearchRadiusitem = new DevExpress.XtraLayout.LayoutControlItem();
            this.textBoxDateitem = new DevExpress.XtraLayout.LayoutControlItem();
            this.comboBoxSalesInLastitem = new DevExpress.XtraLayout.LayoutControlItem();
            this.textBoxEstValuePrivateitem = new DevExpress.XtraLayout.LayoutControlItem();
            this.textBoxEstValueitem = new DevExpress.XtraLayout.LayoutControlItem();
            this.bPrintitem = new DevExpress.XtraLayout.LayoutControlItem();
            this.label74item = new DevExpress.XtraLayout.LayoutControlItem();
            this.label75item = new DevExpress.XtraLayout.LayoutControlItem();
            this.textBoxUnitNoAitem = new DevExpress.XtraLayout.LayoutControlItem();
            this.label76item = new DevExpress.XtraLayout.LayoutControlItem();
            this.buttonInvertitem = new DevExpress.XtraLayout.LayoutControlItem();
            this.buttonTagAllitem = new DevExpress.XtraLayout.LayoutControlItem();
            this.buttonUntagAllitem = new DevExpress.XtraLayout.LayoutControlItem();
            this.listViewPropertyProfilesCompsitem = new DevExpress.XtraLayout.LayoutControlItem();
            this.labelRequestParametersitem = new DevExpress.XtraLayout.LayoutControlItem();
            this.buttonRecalculateitem = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.UserControlPropertyProfileCompsListViewlayoutControl1ConvertedLayout)).BeginInit();
            this.UserControlPropertyProfileCompsListViewlayoutControl1ConvertedLayout.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxSqFtRangeitem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxSuffixAitem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxPostDirAitem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxPreDirAitem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxStateAitem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxCountyAitem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxZipAitem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxStreetAitem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxStreetNoAitem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonGetCompsitem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxOrigSqFtitem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxSearchRadiusitem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxDateitem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxSalesInLastitem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxEstValuePrivateitem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxEstValueitem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bPrintitem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label74item)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label75item)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxUnitNoAitem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label76item)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonInvertitem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonTagAllitem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonUntagAllitem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.listViewPropertyProfilesCompsitem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelRequestParametersitem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonRecalculateitem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            this.SuspendLayout();
            // 
            // labelRequestParameters
            // 
            this.labelRequestParameters.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelRequestParameters.Location = new System.Drawing.Point(2, 2);
            this.labelRequestParameters.Name = "labelRequestParameters";
            this.labelRequestParameters.Size = new System.Drawing.Size(1014, 20);
            this.labelRequestParameters.TabIndex = 1;
            this.labelRequestParameters.Text = "REQUEST PARAMETERS:";
            // 
            // listViewPropertyProfilesComps
            // 
            this.listViewPropertyProfilesComps.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listViewPropertyProfilesComps.CheckBoxes = true;
            this.listViewPropertyProfilesComps.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeaderTag,
            this.columnHeaderDistnace,
            this.columnHeaderSubdivision,
            this.columnHeaderSQFT,
            this.columnHeaderYB,
            this.columnHeaderRooms,
            this.columnHeaderBedrooms,
            this.columnHeaderBath,
            this.columnHeaderPool,
            this.columnHeaderSaleDate,
            this.columnHeaderSalePrice,
            this.columnHeaderSqFtPrice,
            this.columnHeaderLotArea,
            this.columnHeaderAssesed,
            this.columnHeaderStreetNum,
            this.columnHeaderStreetName,
            this.columnHeaderZIP,
            this.columnHeaderCity,
            this.columnHeader1});
            this.listViewPropertyProfilesComps.FullRowSelect = true;
            this.listViewPropertyProfilesComps.GridLines = true;
            this.listViewPropertyProfilesComps.Location = new System.Drawing.Point(2, 91);
            this.listViewPropertyProfilesComps.MultiSelect = false;
            this.listViewPropertyProfilesComps.Name = "listViewPropertyProfilesComps";
            this.listViewPropertyProfilesComps.Size = new System.Drawing.Size(2773, 264);
            this.listViewPropertyProfilesComps.TabIndex = 92;
            this.listViewPropertyProfilesComps.UseCompatibleStateImageBehavior = false;
            this.listViewPropertyProfilesComps.View = System.Windows.Forms.View.Details;
            this.listViewPropertyProfilesComps.DoubleClick += new System.EventHandler(this.listViewPropertyProfilesComps_DoubleClick);
            // 
            // columnHeaderTag
            // 
            this.columnHeaderTag.Text = "Tag";
            this.columnHeaderTag.Width = 33;
            // 
            // columnHeaderDistnace
            // 
            this.columnHeaderDistnace.Text = "Distance";
            this.columnHeaderDistnace.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.columnHeaderDistnace.Width = 43;
            // 
            // columnHeaderSubdivision
            // 
            this.columnHeaderSubdivision.Text = "Subdivision";
            this.columnHeaderSubdivision.Width = 72;
            // 
            // columnHeaderSQFT
            // 
            this.columnHeaderSQFT.Text = "SQFT";
            this.columnHeaderSQFT.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // columnHeaderYB
            // 
            this.columnHeaderYB.Text = "YB";
            this.columnHeaderYB.Width = 40;
            // 
            // columnHeaderRooms
            // 
            this.columnHeaderRooms.Text = "Rooms";
            this.columnHeaderRooms.Width = 40;
            // 
            // columnHeaderBedrooms
            // 
            this.columnHeaderBedrooms.Text = "Bedrooms";
            this.columnHeaderBedrooms.Width = 40;
            // 
            // columnHeaderBath
            // 
            this.columnHeaderBath.Text = "Baths";
            this.columnHeaderBath.Width = 40;
            // 
            // columnHeaderPool
            // 
            this.columnHeaderPool.Text = "Pool";
            this.columnHeaderPool.Width = 40;
            // 
            // columnHeaderSaleDate
            // 
            this.columnHeaderSaleDate.Text = "Sale Date";
            this.columnHeaderSaleDate.Width = 80;
            // 
            // columnHeaderSalePrice
            // 
            this.columnHeaderSalePrice.Text = "Sale Price";
            this.columnHeaderSalePrice.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.columnHeaderSalePrice.Width = 80;
            // 
            // columnHeaderSqFtPrice
            // 
            this.columnHeaderSqFtPrice.Text = "$/sq.ft.";
            this.columnHeaderSqFtPrice.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // columnHeaderLotArea
            // 
            this.columnHeaderLotArea.Text = "Lot Area";
            this.columnHeaderLotArea.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // columnHeaderAssesed
            // 
            this.columnHeaderAssesed.Text = "Assessed Value";
            this.columnHeaderAssesed.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.columnHeaderAssesed.Width = 80;
            // 
            // columnHeaderStreetNum
            // 
            this.columnHeaderStreetNum.Text = "Street #";
            // 
            // columnHeaderStreetName
            // 
            this.columnHeaderStreetName.Text = "Street Name";
            this.columnHeaderStreetName.Width = 77;
            // 
            // columnHeaderZIP
            // 
            this.columnHeaderZIP.Text = "ZIP";
            // 
            // columnHeaderCity
            // 
            this.columnHeaderCity.Text = "City";
            // 
            // columnHeader1
            // 
            this.columnHeader1.Width = 0;
            // 
            // buttonGetComps
            // 
            this.buttonGetComps.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.buttonGetComps.ForeColor = System.Drawing.SystemColors.ControlText;
            this.buttonGetComps.Location = new System.Drawing.Point(2508, 66);
            this.buttonGetComps.Name = "buttonGetComps";
            this.buttonGetComps.Size = new System.Drawing.Size(267, 21);
            this.buttonGetComps.TabIndex = 91;
            this.buttonGetComps.Text = "Get Comps";
            this.buttonGetComps.Click += new System.EventHandler(this.buttonGetComps_Click);
            // 
            // buttonInvert
            // 
            this.buttonInvert.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonInvert.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.buttonInvert.ForeColor = System.Drawing.SystemColors.ControlText;
            this.buttonInvert.Location = new System.Drawing.Point(763, 359);
            this.buttonInvert.Name = "buttonInvert";
            this.buttonInvert.Size = new System.Drawing.Size(393, 20);
            this.buttonInvert.TabIndex = 95;
            this.buttonInvert.Text = "Invert Tagged";
            this.buttonInvert.Click += new System.EventHandler(this.buttonInvert_Click);
            // 
            // buttonUntagAll
            // 
            this.buttonUntagAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonUntagAll.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.buttonUntagAll.ForeColor = System.Drawing.SystemColors.ControlText;
            this.buttonUntagAll.Location = new System.Drawing.Point(366, 359);
            this.buttonUntagAll.Name = "buttonUntagAll";
            this.buttonUntagAll.Size = new System.Drawing.Size(393, 20);
            this.buttonUntagAll.TabIndex = 94;
            this.buttonUntagAll.Text = "Untag All";
            this.buttonUntagAll.Click += new System.EventHandler(this.buttonUntagAll_Click);
            // 
            // buttonTagAll
            // 
            this.buttonTagAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonTagAll.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.buttonTagAll.ForeColor = System.Drawing.SystemColors.ControlText;
            this.buttonTagAll.Location = new System.Drawing.Point(2, 359);
            this.buttonTagAll.Name = "buttonTagAll";
            this.buttonTagAll.Size = new System.Drawing.Size(360, 20);
            this.buttonTagAll.TabIndex = 93;
            this.buttonTagAll.Text = "Tag all";
            this.buttonTagAll.Click += new System.EventHandler(this.buttonTagAll_Click);
            // 
            // textBoxEstValue
            // 
            this.textBoxEstValue.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxEstValue.Location = new System.Drawing.Point(325, 383);
            this.textBoxEstValue.Name = "textBoxEstValue";
            this.textBoxEstValue.Size = new System.Drawing.Size(2450, 20);
            this.textBoxEstValue.TabIndex = 99;
            // 
            // textBoxEstValuePrivate
            // 
            this.textBoxEstValuePrivate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxEstValuePrivate.Location = new System.Drawing.Point(325, 407);
            this.textBoxEstValuePrivate.Name = "textBoxEstValuePrivate";
            this.textBoxEstValuePrivate.Size = new System.Drawing.Size(2450, 20);
            this.textBoxEstValuePrivate.TabIndex = 100;
            // 
            // comboBoxSqFtRange
            // 
            this.comboBoxSqFtRange.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxSqFtRange.Location = new System.Drawing.Point(986, 66);
            this.comboBoxSqFtRange.Name = "comboBoxSqFtRange";
            this.comboBoxSqFtRange.Size = new System.Drawing.Size(499, 21);
            this.comboBoxSqFtRange.TabIndex = 88;
            // 
            // comboBoxSearchRadius
            // 
            this.comboBoxSearchRadius.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxSearchRadius.Location = new System.Drawing.Point(2160, 66);
            this.comboBoxSearchRadius.Name = "comboBoxSearchRadius";
            this.comboBoxSearchRadius.Size = new System.Drawing.Size(21, 21);
            this.comboBoxSearchRadius.TabIndex = 90;
            // 
            // comboBoxSalesInLast
            // 
            this.comboBoxSalesInLast.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxSalesInLast.Location = new System.Drawing.Point(1812, 66);
            this.comboBoxSalesInLast.Name = "comboBoxSalesInLast";
            this.comboBoxSalesInLast.Size = new System.Drawing.Size(21, 21);
            this.comboBoxSalesInLast.TabIndex = 89;
            // 
            // textBoxDate
            // 
            this.textBoxDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxDate.Location = new System.Drawing.Point(2144, 359);
            this.textBoxDate.Name = "textBoxDate";
            this.textBoxDate.Size = new System.Drawing.Size(631, 20);
            this.textBoxDate.TabIndex = 98;
            // 
            // buttonRecalculate
            // 
            this.buttonRecalculate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonRecalculate.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.buttonRecalculate.ForeColor = System.Drawing.Color.Black;
            this.buttonRecalculate.Location = new System.Drawing.Point(1160, 359);
            this.buttonRecalculate.Name = "buttonRecalculate";
            this.buttonRecalculate.Size = new System.Drawing.Size(326, 20);
            this.buttonRecalculate.TabIndex = 96;
            this.buttonRecalculate.Text = "Recalculate";
            this.buttonRecalculate.Click += new System.EventHandler(this.buttonRecalculate_Click);
            // 
            // textBoxOrigSqFt
            // 
            this.textBoxOrigSqFt.Location = new System.Drawing.Point(325, 66);
            this.textBoxOrigSqFt.Name = "textBoxOrigSqFt";
            this.textBoxOrigSqFt.Size = new System.Drawing.Size(334, 20);
            this.textBoxOrigSqFt.TabIndex = 87;
            // 
            // textBoxStateA
            // 
            this.textBoxStateA.Location = new System.Drawing.Point(2755, 42);
            this.textBoxStateA.MaxLength = 2;
            this.textBoxStateA.Name = "textBoxStateA";
            this.textBoxStateA.Size = new System.Drawing.Size(20, 20);
            this.textBoxStateA.TabIndex = 76;
            // 
            // textBoxCountyA
            // 
            this.textBoxCountyA.Location = new System.Drawing.Point(2408, 42);
            this.textBoxCountyA.Name = "textBoxCountyA";
            this.textBoxCountyA.Size = new System.Drawing.Size(20, 20);
            this.textBoxCountyA.TabIndex = 75;
            // 
            // textBoxZipA
            // 
            this.textBoxZipA.Location = new System.Drawing.Point(2061, 42);
            this.textBoxZipA.Name = "textBoxZipA";
            this.textBoxZipA.Size = new System.Drawing.Size(20, 20);
            this.textBoxZipA.TabIndex = 74;
            this.textBoxZipA.Text = "90210";
            // 
            // textBoxUnitNoA
            // 
            this.textBoxUnitNoA.Location = new System.Drawing.Point(1714, 2);
            this.textBoxUnitNoA.Name = "textBoxUnitNoA";
            this.textBoxUnitNoA.Size = new System.Drawing.Size(20, 20);
            this.textBoxUnitNoA.TabIndex = 73;
            // 
            // textBoxStreetA
            // 
            this.textBoxStreetA.Location = new System.Drawing.Point(696, 26);
            this.textBoxStreetA.Name = "textBoxStreetA";
            this.textBoxStreetA.Size = new System.Drawing.Size(320, 20);
            this.textBoxStreetA.TabIndex = 72;
            this.textBoxStreetA.Text = "SUNSET";
            // 
            // textBoxStreetNoA
            // 
            this.textBoxStreetNoA.Location = new System.Drawing.Point(2, 26);
            this.textBoxStreetNoA.Name = "textBoxStreetNoA";
            this.textBoxStreetNoA.Size = new System.Drawing.Size(20, 20);
            this.textBoxStreetNoA.TabIndex = 71;
            // 
            // label76
            // 
            this.label76.Location = new System.Drawing.Point(1738, 2);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(226, 36);
            this.label76.TabIndex = 70;
            this.label76.Text = "ZIP";
            // 
            // label74
            // 
            this.label74.Location = new System.Drawing.Point(2236, 2);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(539, 36);
            this.label74.TabIndex = 78;
            this.label74.Text = "State (i.e. CA)";
            // 
            // label75
            // 
            this.label75.Location = new System.Drawing.Point(1968, 2);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(264, 36);
            this.label75.TabIndex = 77;
            this.label75.Text = "County";
            // 
            // textBoxPreDirA
            // 
            this.textBoxPreDirA.BackColor = System.Drawing.Color.LightGray;
            this.textBoxPreDirA.Location = new System.Drawing.Point(672, 26);
            this.textBoxPreDirA.Name = "textBoxPreDirA";
            this.textBoxPreDirA.Size = new System.Drawing.Size(20, 20);
            this.textBoxPreDirA.TabIndex = 84;
            this.textBoxPreDirA.TabStop = false;
            // 
            // textBoxSuffixA
            // 
            this.textBoxSuffixA.BackColor = System.Drawing.Color.LightGray;
            this.textBoxSuffixA.Location = new System.Drawing.Point(1367, 42);
            this.textBoxSuffixA.Name = "textBoxSuffixA";
            this.textBoxSuffixA.Size = new System.Drawing.Size(20, 20);
            this.textBoxSuffixA.TabIndex = 86;
            this.textBoxSuffixA.TabStop = false;
            // 
            // textBoxPostDirA
            // 
            this.textBoxPostDirA.BackColor = System.Drawing.Color.LightGray;
            this.textBoxPostDirA.Location = new System.Drawing.Point(1343, 42);
            this.textBoxPostDirA.Name = "textBoxPostDirA";
            this.textBoxPostDirA.Size = new System.Drawing.Size(20, 20);
            this.textBoxPostDirA.TabIndex = 85;
            this.textBoxPostDirA.TabStop = false;
            // 
            // label79
            // 
            this.label79.Location = new System.Drawing.Point(1020, 2);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(690, 36);
            this.label79.TabIndex = 82;
            this.label79.Text = "Post-dir";
            // 
            // bPrint
            // 
            this.bPrint.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.bPrint.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.bPrint.ForeColor = System.Drawing.SystemColors.ControlText;
            this.bPrint.Location = new System.Drawing.Point(1490, 359);
            this.bPrint.Name = "bPrint";
            this.bPrint.Size = new System.Drawing.Size(327, 20);
            this.bPrint.TabIndex = 103;
            this.bPrint.Text = "Print COMPS";
            // 
            // UserControlPropertyProfileCompsListViewlayoutControl1ConvertedLayout
            // 
            this.UserControlPropertyProfileCompsListViewlayoutControl1ConvertedLayout.Controls.Add(this.comboBoxSqFtRange);
            this.UserControlPropertyProfileCompsListViewlayoutControl1ConvertedLayout.Controls.Add(this.textBoxSuffixA);
            this.UserControlPropertyProfileCompsListViewlayoutControl1ConvertedLayout.Controls.Add(this.textBoxPostDirA);
            this.UserControlPropertyProfileCompsListViewlayoutControl1ConvertedLayout.Controls.Add(this.textBoxPreDirA);
            this.UserControlPropertyProfileCompsListViewlayoutControl1ConvertedLayout.Controls.Add(this.textBoxStateA);
            this.UserControlPropertyProfileCompsListViewlayoutControl1ConvertedLayout.Controls.Add(this.textBoxCountyA);
            this.UserControlPropertyProfileCompsListViewlayoutControl1ConvertedLayout.Controls.Add(this.textBoxZipA);
            this.UserControlPropertyProfileCompsListViewlayoutControl1ConvertedLayout.Controls.Add(this.textBoxStreetA);
            this.UserControlPropertyProfileCompsListViewlayoutControl1ConvertedLayout.Controls.Add(this.textBoxStreetNoA);
            this.UserControlPropertyProfileCompsListViewlayoutControl1ConvertedLayout.Controls.Add(this.buttonGetComps);
            this.UserControlPropertyProfileCompsListViewlayoutControl1ConvertedLayout.Controls.Add(this.textBoxOrigSqFt);
            this.UserControlPropertyProfileCompsListViewlayoutControl1ConvertedLayout.Controls.Add(this.comboBoxSearchRadius);
            this.UserControlPropertyProfileCompsListViewlayoutControl1ConvertedLayout.Controls.Add(this.textBoxDate);
            this.UserControlPropertyProfileCompsListViewlayoutControl1ConvertedLayout.Controls.Add(this.comboBoxSalesInLast);
            this.UserControlPropertyProfileCompsListViewlayoutControl1ConvertedLayout.Controls.Add(this.textBoxEstValuePrivate);
            this.UserControlPropertyProfileCompsListViewlayoutControl1ConvertedLayout.Controls.Add(this.textBoxEstValue);
            this.UserControlPropertyProfileCompsListViewlayoutControl1ConvertedLayout.Controls.Add(this.bPrint);
            this.UserControlPropertyProfileCompsListViewlayoutControl1ConvertedLayout.Controls.Add(this.label74);
            this.UserControlPropertyProfileCompsListViewlayoutControl1ConvertedLayout.Controls.Add(this.label75);
            this.UserControlPropertyProfileCompsListViewlayoutControl1ConvertedLayout.Controls.Add(this.textBoxUnitNoA);
            this.UserControlPropertyProfileCompsListViewlayoutControl1ConvertedLayout.Controls.Add(this.label76);
            this.UserControlPropertyProfileCompsListViewlayoutControl1ConvertedLayout.Controls.Add(this.buttonInvert);
            this.UserControlPropertyProfileCompsListViewlayoutControl1ConvertedLayout.Controls.Add(this.buttonTagAll);
            this.UserControlPropertyProfileCompsListViewlayoutControl1ConvertedLayout.Controls.Add(this.buttonUntagAll);
            this.UserControlPropertyProfileCompsListViewlayoutControl1ConvertedLayout.Controls.Add(this.listViewPropertyProfilesComps);
            this.UserControlPropertyProfileCompsListViewlayoutControl1ConvertedLayout.Controls.Add(this.labelRequestParameters);
            this.UserControlPropertyProfileCompsListViewlayoutControl1ConvertedLayout.Controls.Add(this.buttonRecalculate);
            this.UserControlPropertyProfileCompsListViewlayoutControl1ConvertedLayout.Controls.Add(this.label79);
            this.UserControlPropertyProfileCompsListViewlayoutControl1ConvertedLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.UserControlPropertyProfileCompsListViewlayoutControl1ConvertedLayout.Location = new System.Drawing.Point(0, 0);
            this.UserControlPropertyProfileCompsListViewlayoutControl1ConvertedLayout.Name = "UserControlPropertyProfileCompsListViewlayoutControl1ConvertedLayout";
            this.UserControlPropertyProfileCompsListViewlayoutControl1ConvertedLayout.Root = this.layoutControlGroup1;
            this.UserControlPropertyProfileCompsListViewlayoutControl1ConvertedLayout.Size = new System.Drawing.Size(688, 446);
            this.UserControlPropertyProfileCompsListViewlayoutControl1ConvertedLayout.TabIndex = 104;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.comboBoxSqFtRangeitem,
            this.textBoxSuffixAitem,
            this.textBoxPostDirAitem,
            this.textBoxPreDirAitem,
            this.textBoxStateAitem,
            this.textBoxCountyAitem,
            this.textBoxZipAitem,
            this.textBoxStreetAitem,
            this.textBoxStreetNoAitem,
            this.buttonGetCompsitem,
            this.textBoxOrigSqFtitem,
            this.comboBoxSearchRadiusitem,
            this.textBoxDateitem,
            this.comboBoxSalesInLastitem,
            this.textBoxEstValuePrivateitem,
            this.textBoxEstValueitem,
            this.bPrintitem,
            this.label74item,
            this.label75item,
            this.textBoxUnitNoAitem,
            this.label76item,
            this.buttonInvertitem,
            this.buttonTagAllitem,
            this.buttonUntagAllitem,
            this.listViewPropertyProfilesCompsitem,
            this.labelRequestParametersitem,
            this.buttonRecalculateitem,
            this.layoutControlItem1});
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(2777, 429);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // comboBoxSqFtRangeitem
            // 
            this.comboBoxSqFtRangeitem.Control = this.comboBoxSqFtRange;
            this.comboBoxSqFtRangeitem.Location = new System.Drawing.Point(661, 64);
            this.comboBoxSqFtRangeitem.Name = "comboBoxSqFtRangeitem";
            this.comboBoxSqFtRangeitem.Size = new System.Drawing.Size(826, 25);
            this.comboBoxSqFtRangeitem.Text = "SEARCH CRITERIA:";
            this.comboBoxSqFtRangeitem.TextLocation = DevExpress.Utils.Locations.Left;
            this.comboBoxSqFtRangeitem.TextSize = new System.Drawing.Size(320, 13);
            // 
            // textBoxSuffixAitem
            // 
            this.textBoxSuffixAitem.Control = this.textBoxSuffixA;
            this.textBoxSuffixAitem.Location = new System.Drawing.Point(1365, 40);
            this.textBoxSuffixAitem.Name = "textBoxSuffixAitem";
            this.textBoxSuffixAitem.Size = new System.Drawing.Size(347, 24);
            this.textBoxSuffixAitem.Text = "Unit No.";
            this.textBoxSuffixAitem.TextLocation = DevExpress.Utils.Locations.Right;
            this.textBoxSuffixAitem.TextSize = new System.Drawing.Size(320, 13);
            // 
            // textBoxPostDirAitem
            // 
            this.textBoxPostDirAitem.Control = this.textBoxPostDirA;
            this.textBoxPostDirAitem.Location = new System.Drawing.Point(1018, 40);
            this.textBoxPostDirAitem.Name = "textBoxPostDirAitem";
            this.textBoxPostDirAitem.Size = new System.Drawing.Size(347, 24);
            this.textBoxPostDirAitem.Text = "Street Name";
            this.textBoxPostDirAitem.TextLocation = DevExpress.Utils.Locations.Left;
            this.textBoxPostDirAitem.TextSize = new System.Drawing.Size(320, 13);
            // 
            // textBoxPreDirAitem
            // 
            this.textBoxPreDirAitem.Control = this.textBoxPreDirA;
            this.textBoxPreDirAitem.Location = new System.Drawing.Point(347, 24);
            this.textBoxPreDirAitem.Name = "textBoxPreDirAitem";
            this.textBoxPreDirAitem.Size = new System.Drawing.Size(347, 40);
            this.textBoxPreDirAitem.Text = "Street No.";
            this.textBoxPreDirAitem.TextLocation = DevExpress.Utils.Locations.Left;
            this.textBoxPreDirAitem.TextSize = new System.Drawing.Size(320, 13);
            // 
            // textBoxStateAitem
            // 
            this.textBoxStateAitem.Control = this.textBoxStateA;
            this.textBoxStateAitem.Location = new System.Drawing.Point(2430, 40);
            this.textBoxStateAitem.Name = "textBoxStateAitem";
            this.textBoxStateAitem.Size = new System.Drawing.Size(347, 24);
            this.textBoxStateAitem.Text = "&&";
            this.textBoxStateAitem.TextLocation = DevExpress.Utils.Locations.Left;
            this.textBoxStateAitem.TextSize = new System.Drawing.Size(320, 13);
            // 
            // textBoxCountyAitem
            // 
            this.textBoxCountyAitem.Control = this.textBoxCountyA;
            this.textBoxCountyAitem.Location = new System.Drawing.Point(2083, 40);
            this.textBoxCountyAitem.Name = "textBoxCountyAitem";
            this.textBoxCountyAitem.Size = new System.Drawing.Size(347, 24);
            this.textBoxCountyAitem.Text = "or";
            this.textBoxCountyAitem.TextLocation = DevExpress.Utils.Locations.Left;
            this.textBoxCountyAitem.TextSize = new System.Drawing.Size(320, 13);
            // 
            // textBoxZipAitem
            // 
            this.textBoxZipAitem.Control = this.textBoxZipA;
            this.textBoxZipAitem.Location = new System.Drawing.Point(1736, 40);
            this.textBoxZipAitem.Name = "textBoxZipAitem";
            this.textBoxZipAitem.Size = new System.Drawing.Size(347, 24);
            this.textBoxZipAitem.Text = "Suffix";
            this.textBoxZipAitem.TextLocation = DevExpress.Utils.Locations.Left;
            this.textBoxZipAitem.TextSize = new System.Drawing.Size(320, 13);
            // 
            // textBoxStreetAitem
            // 
            this.textBoxStreetAitem.Control = this.textBoxStreetA;
            this.textBoxStreetAitem.Location = new System.Drawing.Point(694, 24);
            this.textBoxStreetAitem.Name = "textBoxStreetAitem";
            this.textBoxStreetAitem.Size = new System.Drawing.Size(324, 40);
            this.textBoxStreetAitem.Text = "Sq.Ft.";
            this.textBoxStreetAitem.TextLocation = DevExpress.Utils.Locations.Bottom;
            this.textBoxStreetAitem.TextSize = new System.Drawing.Size(320, 13);
            // 
            // textBoxStreetNoAitem
            // 
            this.textBoxStreetNoAitem.Control = this.textBoxStreetNoA;
            this.textBoxStreetNoAitem.Location = new System.Drawing.Point(0, 24);
            this.textBoxStreetNoAitem.Name = "textBoxStreetNoAitem";
            this.textBoxStreetNoAitem.Size = new System.Drawing.Size(347, 40);
            this.textBoxStreetNoAitem.Text = "Pre-dir";
            this.textBoxStreetNoAitem.TextLocation = DevExpress.Utils.Locations.Right;
            this.textBoxStreetNoAitem.TextSize = new System.Drawing.Size(320, 13);
            // 
            // buttonGetCompsitem
            // 
            this.buttonGetCompsitem.Control = this.buttonGetComps;
            this.buttonGetCompsitem.Location = new System.Drawing.Point(2183, 64);
            this.buttonGetCompsitem.Name = "buttonGetCompsitem";
            this.buttonGetCompsitem.Size = new System.Drawing.Size(594, 25);
            this.buttonGetCompsitem.Text = "Search Radius";
            this.buttonGetCompsitem.TextLocation = DevExpress.Utils.Locations.Left;
            this.buttonGetCompsitem.TextSize = new System.Drawing.Size(320, 13);
            // 
            // textBoxOrigSqFtitem
            // 
            this.textBoxOrigSqFtitem.Control = this.textBoxOrigSqFt;
            this.textBoxOrigSqFtitem.Location = new System.Drawing.Point(0, 64);
            this.textBoxOrigSqFtitem.Name = "textBoxOrigSqFtitem";
            this.textBoxOrigSqFtitem.Size = new System.Drawing.Size(661, 25);
            this.textBoxOrigSqFtitem.Text = "SUBJECT AREA:";
            this.textBoxOrigSqFtitem.TextLocation = DevExpress.Utils.Locations.Left;
            this.textBoxOrigSqFtitem.TextSize = new System.Drawing.Size(320, 13);
            // 
            // comboBoxSearchRadiusitem
            // 
            this.comboBoxSearchRadiusitem.Control = this.comboBoxSearchRadius;
            this.comboBoxSearchRadiusitem.Location = new System.Drawing.Point(1835, 64);
            this.comboBoxSearchRadiusitem.Name = "comboBoxSearchRadiusitem";
            this.comboBoxSearchRadiusitem.Size = new System.Drawing.Size(348, 25);
            this.comboBoxSearchRadiusitem.Text = "Sales in Last";
            this.comboBoxSearchRadiusitem.TextLocation = DevExpress.Utils.Locations.Left;
            this.comboBoxSearchRadiusitem.TextSize = new System.Drawing.Size(320, 13);
            // 
            // textBoxDateitem
            // 
            this.textBoxDateitem.Control = this.textBoxDate;
            this.textBoxDateitem.Location = new System.Drawing.Point(1819, 357);
            this.textBoxDateitem.Name = "textBoxDateitem";
            this.textBoxDateitem.Size = new System.Drawing.Size(958, 24);
            this.textBoxDateitem.Text = "COMPS Date";
            this.textBoxDateitem.TextLocation = DevExpress.Utils.Locations.Left;
            this.textBoxDateitem.TextSize = new System.Drawing.Size(320, 13);
            // 
            // comboBoxSalesInLastitem
            // 
            this.comboBoxSalesInLastitem.Control = this.comboBoxSalesInLast;
            this.comboBoxSalesInLastitem.Location = new System.Drawing.Point(1487, 64);
            this.comboBoxSalesInLastitem.Name = "comboBoxSalesInLastitem";
            this.comboBoxSalesInLastitem.Size = new System.Drawing.Size(348, 25);
            this.comboBoxSalesInLastitem.Text = "Sq.Ft. range:";
            this.comboBoxSalesInLastitem.TextLocation = DevExpress.Utils.Locations.Left;
            this.comboBoxSalesInLastitem.TextSize = new System.Drawing.Size(320, 13);
            // 
            // textBoxEstValuePrivateitem
            // 
            this.textBoxEstValuePrivateitem.Control = this.textBoxEstValuePrivate;
            this.textBoxEstValuePrivateitem.Location = new System.Drawing.Point(0, 405);
            this.textBoxEstValuePrivateitem.Name = "textBoxEstValuePrivateitem";
            this.textBoxEstValuePrivateitem.Size = new System.Drawing.Size(2777, 24);
            this.textBoxEstValuePrivateitem.Text = "Your Estimated Value from other Sources:";
            this.textBoxEstValuePrivateitem.TextLocation = DevExpress.Utils.Locations.Left;
            this.textBoxEstValuePrivateitem.TextSize = new System.Drawing.Size(320, 13);
            // 
            // textBoxEstValueitem
            // 
            this.textBoxEstValueitem.Control = this.textBoxEstValue;
            this.textBoxEstValueitem.Location = new System.Drawing.Point(0, 381);
            this.textBoxEstValueitem.Name = "textBoxEstValueitem";
            this.textBoxEstValueitem.Size = new System.Drawing.Size(2777, 24);
            this.textBoxEstValueitem.Text = "Estimated Value of your Subject Property based on Tagged COMPS";
            this.textBoxEstValueitem.TextLocation = DevExpress.Utils.Locations.Left;
            this.textBoxEstValueitem.TextSize = new System.Drawing.Size(320, 13);
            // 
            // bPrintitem
            // 
            this.bPrintitem.Control = this.bPrint;
            this.bPrintitem.Location = new System.Drawing.Point(1488, 357);
            this.bPrintitem.Name = "bPrintitem";
            this.bPrintitem.Size = new System.Drawing.Size(331, 24);
            this.bPrintitem.TextSize = new System.Drawing.Size(0, 0);
            this.bPrintitem.TextVisible = false;
            // 
            // label74item
            // 
            this.label74item.Control = this.label74;
            this.label74item.Location = new System.Drawing.Point(2234, 0);
            this.label74item.Name = "label74item";
            this.label74item.Size = new System.Drawing.Size(543, 40);
            this.label74item.TextSize = new System.Drawing.Size(0, 0);
            this.label74item.TextVisible = false;
            // 
            // label75item
            // 
            this.label75item.Control = this.label75;
            this.label75item.Location = new System.Drawing.Point(1966, 0);
            this.label75item.Name = "label75item";
            this.label75item.Size = new System.Drawing.Size(268, 40);
            this.label75item.TextSize = new System.Drawing.Size(0, 0);
            this.label75item.TextVisible = false;
            // 
            // textBoxUnitNoAitem
            // 
            this.textBoxUnitNoAitem.Control = this.textBoxUnitNoA;
            this.textBoxUnitNoAitem.Location = new System.Drawing.Point(1712, 0);
            this.textBoxUnitNoAitem.Name = "textBoxUnitNoAitem";
            this.textBoxUnitNoAitem.Size = new System.Drawing.Size(24, 64);
            this.textBoxUnitNoAitem.TextSize = new System.Drawing.Size(0, 0);
            this.textBoxUnitNoAitem.TextVisible = false;
            // 
            // label76item
            // 
            this.label76item.Control = this.label76;
            this.label76item.Location = new System.Drawing.Point(1736, 0);
            this.label76item.Name = "label76item";
            this.label76item.Size = new System.Drawing.Size(230, 40);
            this.label76item.TextSize = new System.Drawing.Size(0, 0);
            this.label76item.TextVisible = false;
            // 
            // buttonInvertitem
            // 
            this.buttonInvertitem.Control = this.buttonInvert;
            this.buttonInvertitem.Location = new System.Drawing.Point(761, 357);
            this.buttonInvertitem.Name = "buttonInvertitem";
            this.buttonInvertitem.Size = new System.Drawing.Size(397, 24);
            this.buttonInvertitem.TextSize = new System.Drawing.Size(0, 0);
            this.buttonInvertitem.TextVisible = false;
            // 
            // buttonTagAllitem
            // 
            this.buttonTagAllitem.Control = this.buttonTagAll;
            this.buttonTagAllitem.Location = new System.Drawing.Point(0, 357);
            this.buttonTagAllitem.Name = "buttonTagAllitem";
            this.buttonTagAllitem.Size = new System.Drawing.Size(364, 24);
            this.buttonTagAllitem.TextSize = new System.Drawing.Size(0, 0);
            this.buttonTagAllitem.TextVisible = false;
            // 
            // buttonUntagAllitem
            // 
            this.buttonUntagAllitem.Control = this.buttonUntagAll;
            this.buttonUntagAllitem.Location = new System.Drawing.Point(364, 357);
            this.buttonUntagAllitem.Name = "buttonUntagAllitem";
            this.buttonUntagAllitem.Size = new System.Drawing.Size(397, 24);
            this.buttonUntagAllitem.TextSize = new System.Drawing.Size(0, 0);
            this.buttonUntagAllitem.TextVisible = false;
            // 
            // listViewPropertyProfilesCompsitem
            // 
            this.listViewPropertyProfilesCompsitem.Control = this.listViewPropertyProfilesComps;
            this.listViewPropertyProfilesCompsitem.Location = new System.Drawing.Point(0, 89);
            this.listViewPropertyProfilesCompsitem.Name = "listViewPropertyProfilesCompsitem";
            this.listViewPropertyProfilesCompsitem.Size = new System.Drawing.Size(2777, 268);
            this.listViewPropertyProfilesCompsitem.TextSize = new System.Drawing.Size(0, 0);
            this.listViewPropertyProfilesCompsitem.TextVisible = false;
            // 
            // labelRequestParametersitem
            // 
            this.labelRequestParametersitem.Control = this.labelRequestParameters;
            this.labelRequestParametersitem.Location = new System.Drawing.Point(0, 0);
            this.labelRequestParametersitem.Name = "labelRequestParametersitem";
            this.labelRequestParametersitem.Size = new System.Drawing.Size(1018, 24);
            this.labelRequestParametersitem.TextSize = new System.Drawing.Size(0, 0);
            this.labelRequestParametersitem.TextVisible = false;
            // 
            // buttonRecalculateitem
            // 
            this.buttonRecalculateitem.Control = this.buttonRecalculate;
            this.buttonRecalculateitem.Location = new System.Drawing.Point(1158, 357);
            this.buttonRecalculateitem.Name = "buttonRecalculateitem";
            this.buttonRecalculateitem.Size = new System.Drawing.Size(330, 24);
            this.buttonRecalculateitem.TextSize = new System.Drawing.Size(0, 0);
            this.buttonRecalculateitem.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.label79;
            this.layoutControlItem1.Location = new System.Drawing.Point(1018, 0);
            this.layoutControlItem1.Name = "label79item";
            this.layoutControlItem1.Size = new System.Drawing.Size(694, 40);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // UserControlPropertyProfileCompsListView
            // 
            this.Controls.Add(this.UserControlPropertyProfileCompsListViewlayoutControl1ConvertedLayout);
            this.Name = "UserControlPropertyProfileCompsListView";
            this.Size = new System.Drawing.Size(688, 446);
            ((System.ComponentModel.ISupportInitialize)(this.UserControlPropertyProfileCompsListViewlayoutControl1ConvertedLayout)).EndInit();
            this.UserControlPropertyProfileCompsListViewlayoutControl1ConvertedLayout.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxSqFtRangeitem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxSuffixAitem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxPostDirAitem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxPreDirAitem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxStateAitem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxCountyAitem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxZipAitem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxStreetAitem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxStreetNoAitem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonGetCompsitem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxOrigSqFtitem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxSearchRadiusitem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxDateitem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxSalesInLastitem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxEstValuePrivateitem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxEstValueitem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bPrintitem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label74item)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label75item)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxUnitNoAitem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label76item)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonInvertitem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonTagAllitem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonUntagAllitem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.listViewPropertyProfilesCompsitem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelRequestParametersitem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonRecalculateitem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private void bPrint_Click(object sender, EventArgs e)
		{
			if ((this.textBoxOrigSqFt.Text == string.Empty) || (this.textBoxOrigSqFt.Text == "0"))
			{
				MessageBox.Show(this, "Enter Subject Area Square Footage!", "Missing data!", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}

			try
			{
				if (string.Empty == textBoxStreetA.Text || string.Empty == this.textBoxStreetNoA.Text)
				{
					MessageBox.Show(this, "The address fields are empty, please re-enter!", "Missing data!", MessageBoxButtons.OK, MessageBoxIcon.Information);
					return;
				}
			}
			catch {}

			if (this.listViewPropertyProfilesComps.Items.Count == 0)
			{
				MessageBox.Show(this, "Select the comparable properties by tagging them!!", "Select properties!", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}

			int iTagged = 0;
			for (int i = 0; i < this.listViewPropertyProfilesComps.Items.Count; i++)
			{
				if (true == this.listViewPropertyProfilesComps.Items[i].Checked)
				{
					iTagged++;
					break;
				}
			}

			if (0 == iTagged)
			{
				MessageBox.Show(this, "Select the comparable properties by tagging them!!", "Select properties!", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
			
			try 
			{				
				PrintDocument tmpprndoc = new PrintDocument();
				tmpprndoc.PrintPage += new PrintPageEventHandler(Multi_PrintPage);
				PrintPreviewDialog tmpprdiag = new PrintPreviewDialog();								
				tmpprdiag.Document = tmpprndoc;
			
				tmpprdiag.ShowDialog();

			}
			catch (Exception)
			{
				//silent catch (not a mistake)
				//when no printer installed -> Print -> Cancel print -> Cancel would crash app.
			}	
		}

		

		private DTD.Response.PROPERTY[] GetTaggedComps()
		{
			int iTaggedComps = 0;
			ArrayList alProperties = new ArrayList();
			for (int i = 0; i < this.listViewPropertyProfilesComps.Items.Count; i++)
			{
				if (true == ((ListViewItem)(this.listViewPropertyProfilesComps.Items[i])).Checked) 
				{
					iTaggedComps++;
					alProperties.Add(comps[i].PROPERTY);
				}
			}
			DTD.Response.PROPERTY[] property = new DTD.Response.PROPERTY[iTaggedComps];
			property = (DTD.Response.PROPERTY[]) alProperties.ToArray(typeof(DTD.Response.PROPERTY));
			return property;
		}
		int iPageCnt = 0;
		DTD.Response.PROPERTY[] selectedProperties = null;

		private void Multi_PrintPage(object sender, PrintPageEventArgs e)
		{			
			//if (null == selectedProperties)
			selectedProperties = this.GetTaggedComps();
			if (null != selectedProperties)
			{
				string sFor = string.Empty;
				sFor += textBoxStreetNoA.Text;
				if (sFor != string.Empty)
					sFor += " ";

				if (textBoxPreDirA.Text != string.Empty)
				{
					sFor += textBoxPreDirA.Text + " "; 
				}

				if (textBoxStreetA.Text != string.Empty)
				{
					sFor += textBoxStreetA.Text + " "; 
				}

				if (textBoxPostDirA.Text != string.Empty)
				{
					sFor += textBoxPostDirA.Text + " "; 
				}

				if (textBoxSuffixA.Text != string.Empty)
				{
					sFor += textBoxSuffixA.Text + " "; 
				}

				if (textBoxUnitNoA.Text != string.Empty)
				{
					sFor += textBoxUnitNoA.Text + " "; 
				}

				
				/*
				 * TA++ 27.Nov.2005
				 * RQ 0118
				if (textBoxZipA.Text != string.Empty)
				{
					if (sFor != string.Empty)
						sFor += ", ";
					sFor +=  textBoxZipA.Text + " "; 
				}

				if (textBoxCountyA.Text != string.Empty)
				{
					sFor += textBoxCountyA.Text + " "; 
				}

				if (textBoxStateA.Text != string.Empty)
				{
					sFor += textBoxStateA.Text + " "; 
				}
				*/
				 
/*				ApplicationHelper.PrintMultipleDataReport(e, selectedProperties, iPageCnt++,					 
					string.Empty,
					sFor,
					textBoxOrigSqFt.Text, 
					string.Empty, 
					comboBoxSqFtRange.SelectedItem.ToString(), 
					comboBoxSalesInLast.SelectedItem.ToString(), 
					comboBoxSearchRadius.SelectedItem.ToString(), 
					textBoxEstValue.Text, 
					selectedProperties.Length.ToString()
					);
*/				if (iPageCnt == selectedProperties.Length)
					iPageCnt = 0;
			}
		}

	}
	#region . comps .
	public class comps
	{
		#region ˇ Data ˇ
		private Guid propertyItemId;
		private DateTime dateOfRetrieve;
		private Decimal estimatedValue;
		private Decimal privateEstimatedValue;
		private Byte[] xmlStream;
		private String taggedCSV;
		#endregion ˇ Data ˇ
		#region ˇ CTR ˇ
		public comps()
		{
		}

		public comps(Guid propertyItemId, DateTime dateOfRetrieve,
			Decimal estimatedValue, Decimal privateEstimatedValue, Byte[] xmlStream,
			String taggedCSV)
		{
			this.propertyItemId = propertyItemId;
			this.dateOfRetrieve = dateOfRetrieve;
			this.estimatedValue = estimatedValue;
			this.privateEstimatedValue = privateEstimatedValue;
			this.xmlStream = xmlStream;
			this.taggedCSV = taggedCSV;
		}
		#endregion ˇ CTR ˇ
		#region ˇ Properties ˇ
		#region ˇ  P_propertyItemIdˇ
		[Description("propertyItemId")]
		public Guid P_propertyItemId
		{
			get
			{
				return this.propertyItemId;
			}
			set
			{
				this.propertyItemId = value;
			}
		}
		#endregion ˇ  P_propertyItemIdˇ
		#region ˇ  P_dateOfRetrieveˇ
		[Description("dateOfRetrieve")]
		public DateTime P_dateOfRetrieve
		{
			get
			{
				return this.dateOfRetrieve;
			}
			set
			{
				this.dateOfRetrieve = value;
			}
		}
		#endregion ˇ  P_dateOfRetrieveˇ
		#region ˇ  P_estimatedValueˇ
		[Description("estimatedValue")]
		public Decimal P_estimatedValue
		{
			get
			{
				return this.estimatedValue;
			}
			set
			{
				this.estimatedValue = value;
			}
		}
		#endregion ˇ  P_estimatedValueˇ
		#region ˇ  P_privateEstimatedValueˇ
		[Description("privateEstimatedValue")]
		public Decimal P_privateEstimatedValue
		{
			get
			{
				return this.privateEstimatedValue;
			}
			set
			{
				this.privateEstimatedValue = value;
			}
		}
		#endregion ˇ  P_privateEstimatedValueˇ
		#region ˇ  P_xmlStreamˇ
		[Description("xmlStream")]
		public Byte[] P_xmlStream
		{
			get
			{
				return this.xmlStream;
			}
			set
			{
				this.xmlStream = value;
			}
		}
		#endregion ˇ  P_xmlStreamˇ
		#region ˇ  P_taggedCSVˇ
		[Description("taggedCSV")]
		public String P_taggedCSV
		{
			get
			{
				return this.taggedCSV;
			}
			set
			{
				this.taggedCSV = value;
			}
		}
		#endregion ˇ  P_taggedCSVˇ
		#endregion ˇ Properties ˇ
		#region ˇ Methods ˇ
		public int Insert(SqlConnection dbConn)
		{
			int retVal = 0;

			try
			{
				SqlCommand sp_insert_comps = new
					SqlCommand("insert_comps", dbConn);

				sp_insert_comps.CommandType =
					CommandType.StoredProcedure;

	
				sp_insert_comps.Parameters.Add("@propertyItemId", this.propertyItemId);
	
				sp_insert_comps.Parameters.Add("@dateOfRetrieve", this.dateOfRetrieve);
	
				sp_insert_comps.Parameters.Add("@estimatedValue", this.estimatedValue);
	
				sp_insert_comps.Parameters.Add("@privateEstimatedValue",
					this.privateEstimatedValue);
				sp_insert_comps.Parameters.Add("@xmlStream",
					this.xmlStream);
				sp_insert_comps.Parameters.Add("@taggedCSV",
					this.taggedCSV);
				retVal = sp_insert_comps.ExecuteNonQuery();
			}
			catch (Exception exc)
			{
//				throw exc;
			}
			return retVal;
		}

		public int Update(SqlConnection dbConn)
		{
			int retVal = 0;

			try
			{
				SqlCommand sp_update_comps = new
					SqlCommand("update_comps", dbConn);

				sp_update_comps.CommandType =
					CommandType.StoredProcedure;

	
				sp_update_comps.Parameters.Add("@propertyItemId", this.propertyItemId);
	
				sp_update_comps.Parameters.Add("@dateOfRetrieve", this.dateOfRetrieve);
	
				sp_update_comps.Parameters.Add("@estimatedValue", this.estimatedValue);
	
				sp_update_comps.Parameters.Add("@privateEstimatedValue",
					this.privateEstimatedValue);
				sp_update_comps.Parameters.Add("@xmlStream",
					this.xmlStream);
				sp_update_comps.Parameters.Add("@taggedCSV",
					this.taggedCSV);
				retVal = sp_update_comps.ExecuteNonQuery();
			}
			catch (Exception exc)
			{
//				throw exc;
			}
			return retVal;
		}

		public int Delete(SqlConnection dbConn)
		{
			int retVal = 0;

			try
			{
				SqlCommand sp_delete_comps = new
					SqlCommand("delete_comps", dbConn);

				sp_delete_comps.CommandType =
					CommandType.StoredProcedure;

	
				sp_delete_comps.Parameters.Add("@propertyItemId", this.propertyItemId);
				retVal = sp_delete_comps.ExecuteNonQuery();
			}
			catch (Exception exc)
			{
//				throw exc;
			}
			return retVal;
		}

		public static comps[] Select(SqlConnection dbConn, params
			object[] list)
		{
			ArrayList alObjects = new ArrayList();
			SqlDataReader reader = null;
			comps tempcomps = null;
			try
			{
				SqlCommand sp_select_comps = new
					SqlCommand("select_comps", dbConn);

				sp_select_comps.CommandType =
					CommandType.StoredProcedure;

				if (null != list && 0 != list.Length)
				{
	
					sp_select_comps.Parameters.Add("@propertyItemId", (System.Guid)list[0]);
				}
				else
				{
	
					sp_select_comps.Parameters.Add("@propertyItemId", DBNull.Value);
				}
				reader = sp_select_comps.ExecuteReader();
				while (reader.Read())
				{
					System.Data.SqlTypes.SqlBinary
						sqlBinary4 = reader.GetSqlBinary(4);
					tempcomps = new
						comps(reader.GetGuid(0), reader.GetDateTime(1), reader.GetDecimal(2),
						reader.GetDecimal(3), (byte[])sqlBinary4.Value, reader.GetString(5));
					alObjects.Add(tempcomps);
				}
			}
			catch (Exception exc)
			{
//				throw exc;
			}
			finally
			{
				reader.Close();
			}
			return (comps[]) alObjects.ToArray(typeof(comps));
		}
		#endregion ˇ Methods ˇ
	}
	#endregion
	#region ˇ COMMENTED OUT ˇ
	/*
	public class comps
	{
		#region ˇ Data ˇ

		private Guid propertyItemId;

		private DateTime dateOfRetrieve;

		private Decimal estimatedValue;

		private Decimal privateEstimatedValue;

		private Byte[] xmlStream;

		#endregion ˇ Data ˇ
		#region ˇ CTR ˇ

		public comps()

		{

		}

 

		public comps(Guid propertyItemId, DateTime dateOfRetrieve, Decimal estimatedValue, Decimal privateEstimatedValue, Byte[] xmlStream)

		{

			this.propertyItemId = propertyItemId;

			this.dateOfRetrieve = dateOfRetrieve;

			this.estimatedValue = estimatedValue;

			this.privateEstimatedValue = privateEstimatedValue;

			this.xmlStream = xmlStream;

		}

		#endregion ˇ CTR ˇ
		#region ˇ Properties ˇ

		#region ˇ  P_propertyItemIdˇ

		public Guid P_propertyItemId

		{

			get

			{

				return this.propertyItemId;

			}

			set

			{

				this.propertyItemId = value;

			}

		}

		#endregion ˇ  P_propertyItemIdˇ

		#region ˇ  P_dateOfRetrieveˇ

		public DateTime P_dateOfRetrieve

		{

			get

			{

				return this.dateOfRetrieve;

			}

			set

			{

				this.dateOfRetrieve = value;

			}

		}

		#endregion ˇ  P_dateOfRetrieveˇ

		#region ˇ  P_estimatedValueˇ

		public Decimal P_estimatedValue

		{

			get

			{

				return this.estimatedValue;

			}

			set

			{

				this.estimatedValue = value;

			}

		}

		#endregion ˇ  P_estimatedValueˇ

		#region ˇ  P_privateEstimatedValueˇ

		public Decimal P_privateEstimatedValue

		{

			get

			{

				return this.privateEstimatedValue;

			}

			set

			{

				this.privateEstimatedValue = value;

			}

		}

		#endregion ˇ  P_privateEstimatedValueˇ

		#region ˇ  P_xmlStreamˇ

		public Byte[] P_xmlStream

		{

			get

			{

				return this.xmlStream;

			}

			set

			{

				this.xmlStream = value;

			}

		}

		#endregion ˇ  P_xmlStreamˇ

		#endregion ˇ Properties ˇ
		#region ˇ Methods ˇ

		public void Insert(SqlConnection dbConn)

		{

			try

			{

				SqlCommand sp_insert_comps = new SqlCommand("insert_comps", dbConn);

 

				sp_insert_comps.CommandType = CommandType.StoredProcedure;

 

				sp_insert_comps.Parameters.Add("@propertyItemId", this.propertyItemId);

				sp_insert_comps.Parameters.Add("@dateOfRetrieve", this.dateOfRetrieve);

				sp_insert_comps.Parameters.Add("@estimatedValue", this.estimatedValue);

				sp_insert_comps.Parameters.Add("@privateEstimatedValue", this.privateEstimatedValue);

				sp_insert_comps.Parameters.Add("@xmlStream", this.xmlStream);

				sp_insert_comps.ExecuteNonQuery();

			}

			catch (Exception exc)

			{

				throw exc;

			}

		}

 

		public void Update(SqlConnection dbConn)

		{

			try

			{

				SqlCommand sp_update_comps = new SqlCommand("update_comps", dbConn);

 

				sp_update_comps.CommandType = CommandType.StoredProcedure;

 

				sp_update_comps.Parameters.Add("@propertyItemId", this.propertyItemId);

				sp_update_comps.Parameters.Add("@dateOfRetrieve", this.dateOfRetrieve);

				sp_update_comps.Parameters.Add("@estimatedValue", this.estimatedValue);

				sp_update_comps.Parameters.Add("@privateEstimatedValue", this.privateEstimatedValue);

				sp_update_comps.Parameters.Add("@xmlStream", this.xmlStream);

				sp_update_comps.ExecuteNonQuery();

			}

			catch (Exception exc)

			{

				throw exc;

			}

		}

 

		public void Delete(SqlConnection dbConn)

		{

			try

			{

				SqlCommand sp_delete_comps = new SqlCommand("delete_comps", dbConn);

 

				sp_delete_comps.CommandType = CommandType.StoredProcedure;

 

				sp_delete_comps.Parameters.Add("@propertyItemId", this.propertyItemId);

				sp_delete_comps.ExecuteNonQuery();

			}

			catch (Exception exc)

			{

				throw exc;

			}

		}

 
		public static comps[] Select(SqlConnection dbConn, bool getAllRecords,  System.Guid propertyItemId)
		{
			ArrayList alObjects = new ArrayList();
			SqlDataReader reader = null;
			comps tempcomps = null;
			try
			{
				SqlCommand sp_select_comps = new SqlCommand("select_comps", dbConn);

				sp_select_comps.CommandType = CommandType.StoredProcedure;

				if (true == getAllRecords)
				{
					sp_select_comps.Parameters.Add("@propertyItemId", DBNull.Value);
					reader = sp_select_comps.ExecuteReader();
				}
				else
				{
					sp_select_comps.Parameters.Add("@propertyItemId", propertyItemId);
					reader = sp_select_comps.ExecuteReader();
				}
				while (reader.Read())
				{
					System.Data.SqlTypes.SqlBinary sqlBinary4 = reader.GetSqlBinary(4);
					tempcomps = new comps(reader.GetGuid(0), reader.GetDateTime(1), reader.GetDecimal(2), reader.GetDecimal(3), (byte[])sqlBinary4.Value);
					alObjects.Add(tempcomps);
				}
			}
			catch (Exception exc)
			{
				throw exc;
			}
			finally
			{
				reader.Close();
			}
			return (comps[]) alObjects.ToArray(typeof(comps));
		}
		#endregion ˇ Methods ˇ
	}
	*/
	
	/*
	#region ˇ Class ListViewItemComparer ˇ

	// Implements the manual sorting of items by columns.

	class ListViewItemComparer : IComparer

	{

		private int col;

		public ListViewItemComparer()

		{

			col = 0;

		}

		public ListViewItemComparer(int column)

		{

			col = column;

		}

		public int Compare(object x, object y)

		{

			return String.Compare(((ListViewItem)x).SubItems[col].Text, ((ListViewItem)y).SubItems[col].Text);

		}

	}

	#endregion ˇ Class ListViewItemComparer ˇ
	#region ˇ Class ListViewItemComparerDesc ˇ

	// Implements the manual sorting of items by columns.

	class ListViewItemComparerDesc : IComparer

	{

		private int col;

		public ListViewItemComparerDesc()

		{

			col = 0;

		}

		public ListViewItemComparerDesc(int column)

		{

			col = column;

		}

		public int Compare(object x, object y)

		{

			return String.Compare(((ListViewItem)y).SubItems[col].Text, ((ListViewItem)x).SubItems[col].Text);

		}

	}

	#endregion ˇ Class ListViewItemComparerDesc ˇ
	*/
	#endregion ˇ COMMENTED OUT ˇ
}
