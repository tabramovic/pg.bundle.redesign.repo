﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.ComponentModel.DataAnnotations;



using DevExpress.Utils;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;

using WebManager.WS_REG;
using WebManager.BLL;

namespace WebManager.DX.UC
{
    public partial class ucUsers : DevExpress.XtraEditors.XtraUserControl
    {
        public delegate void GetUserInfoDetails(DisplayUserInfo displayUserInfo);
        public event GetUserInfoDetails OnGetUserInfoDetailsRequested;

        private static ucUsers _instance;
        List<ShortUserInfo> _registeredUsers = new List<ShortUserInfo>();
        bool _subscribed = false;

        public ucUsers()
        {
            InitializeComponent();
            Subscribe();
        }

        public void Subscribe()
        {
            if (!_subscribed)
            {
                RegisteredUsers.Instance.OnDataRetrieved += Instance_OnDataRetrieved;
                _subscribed = true;
            }
        }
        
        private void Instance_OnDataRetrieved(List<ShortUserInfo> users)
        {
            _registeredUsers = users;
            SetDataSource();
            if (null != users && users.Any())
                SetStatusLabel("Synchronized");
        }

        public static ucUsers Instance
        {
            get
            {
                if (null == _instance)
                    _instance = new ucUsers();

                return _instance;
            }
        }

        void windowsUIButtonPanel_ButtonClick(object sender, DevExpress.XtraBars.Docking2010.ButtonEventArgs e)
        {
            if (e.Button.Properties.Caption == "Print") gridControl.ShowRibbonPrintPreview();
        }

       
        void ucUsers_Load(object sender, EventArgs e)
        {
            SetDataSource();
        }

        delegate void SetStatusLabelAsync(string status);
        void SetStatusLabel(string newStatus)
        {
            if (InvokeRequired)
                BeginInvoke(new SetStatusLabelAsync(SetStatusLabel), new object[] { newStatus });
            else
                statusLabel.Text = newStatus;
        }

        delegate void SetDataSourceAsync();
        void SetDataSource()
        {
            if (InvokeRequired)
                BeginInvoke(new SetDataSourceAsync(SetDataSource));
            else
            {
                var displayList = CastToDisplayUserInfo(_registeredUsers);
                var bindingList = new BindingList<DisplayUserInfo>(displayList);
                var source = new BindingSource(bindingList, null);
                gridControl.DataSource = source;                 
                gridControl.RefreshDataSource();
            }
        }

        Func<int, ShortUserInfo, DisplayUserInfo> AsDisplayUserInfo = (i, sui) => new DisplayUserInfo(i, sui.FirstName, sui.MiddleName, sui.LastName, sui.SerialNumber, sui.ActivationDate.Date, sui.RegistrationId);

        public List<DisplayUserInfo> CastToDisplayUserInfo(List<ShortUserInfo> input)
        {
            List<DisplayUserInfo> res = new List<DisplayUserInfo>();

            var cnt = 1;
            foreach (var i in input)
            {
                res.Add(AsDisplayUserInfo(cnt, i));
                cnt++;
            }

            return res;
        }

        private void gridView1_DoubleClick(object sender, EventArgs e)
        {
            int rowHandle = 0;

            DevExpress.Utils.DXMouseEventArgs ea = e as DevExpress.Utils.DXMouseEventArgs;
            GridView view = (GridView)sender;
            var info = view.CalcHitInfo(ea.Location);

            if (info.InRow || info.InRowCell)
            {
                rowHandle = info.RowHandle;

                DisplayUserInfo displayUserInfo = view.GetRow(rowHandle) as DisplayUserInfo;
                //MessageBox.Show(displayUserInfo.ToString());

                if (null != OnGetUserInfoDetailsRequested)
                    OnGetUserInfoDetailsRequested(displayUserInfo);
            }                        
        }       
    }

    public class DisplayUserInfo
    {
        public int RowNr { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string SerialNr { get; set; }
        public DateTime ActivationDate { get; set; }
        public string RegistrationId { get; set; }

        public DisplayUserInfo(int rowNr, string firstName, string middleName, string lastName, string serialNr, DateTime activationDate, Guid registrationId)
        {
            RowNr = rowNr;
            FirstName = firstName;
            MiddleName = middleName;
            LastName = lastName;
            SerialNr = serialNr;
            ActivationDate = activationDate;
            RegistrationId = registrationId.ToString();
        }

        public override string ToString()
        {
            return $"{RowNr}. {FirstName} {MiddleName} {LastName} {SerialNr} {ActivationDate} {RegistrationId}";
        }
    }
}
