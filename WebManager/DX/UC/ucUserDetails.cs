﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace WebManager.DX.UC
{
    public partial class ucUserDetails : DevExpress.XtraEditors.XtraUserControl
    {
        private static ucUserDetails _instance = null;

        public ucUserDetails()
        {
            InitializeComponent();
        }

        public static ucUserDetails Instance
        {
            get
            {
                if (null == _instance)                                    
                    _instance = new ucUserDetails();

                return _instance;
            }
        }

        public void SetData(DisplayUserInfo dui)
        {
            firstName.Text = dui.FirstName;
            middleName.Text = dui.MiddleName;
            lastName.Text = dui.LastName;
            pgId.Text = dui.RegistrationId;
            serialNr.Text = dui.SerialNr;
            activationDate.Text = $"{dui.ActivationDate.ToShortDateString()} {dui.ActivationDate.ToShortTimeString()}";
        }
    }
}
