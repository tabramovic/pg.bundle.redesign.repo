﻿namespace WebManager.DX.UC
{
    partial class ucUserDetails
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.windowsUIView1 = new DevExpress.XtraBars.Docking2010.Views.WindowsUI.WindowsUIView(this.components);
            this.upperPanel = new DevExpress.XtraEditors.PanelControl();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.activationDate = new DevExpress.XtraEditors.TextEdit();
            this.pgId = new DevExpress.XtraEditors.TextEdit();
            this.serialNr = new DevExpress.XtraEditors.TextEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.bottomPanel = new DevExpress.XtraEditors.PanelControl();
            this.groupControl4 = new DevExpress.XtraEditors.GroupControl();
            this.textEdit4 = new DevExpress.XtraEditors.TextEdit();
            this.rightPanel = new DevExpress.XtraEditors.PanelControl();
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.textEdit11 = new DevExpress.XtraEditors.TextEdit();
            this.centralPanel = new DevExpress.XtraEditors.PanelControl();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.zip = new DevExpress.XtraEditors.TextEdit();
            this.company = new DevExpress.XtraEditors.TextEdit();
            this.email = new DevExpress.XtraEditors.TextEdit();
            this.cellPhoneNr = new DevExpress.XtraEditors.TextEdit();
            this.businessPhoneNr = new DevExpress.XtraEditors.TextEdit();
            this.phoneNr = new DevExpress.XtraEditors.TextEdit();
            this.state = new DevExpress.XtraEditors.TextEdit();
            this.city = new DevExpress.XtraEditors.TextEdit();
            this.address = new DevExpress.XtraEditors.TextEdit();
            this.lastName = new DevExpress.XtraEditors.TextEdit();
            this.middleName = new DevExpress.XtraEditors.TextEdit();
            this.firstName = new DevExpress.XtraEditors.TextEdit();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.label2 = new System.Windows.Forms.Label();
            this.separatorControl1 = new DevExpress.XtraEditors.SeparatorControl();
            this.checkEdit1 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit2 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit3 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit4 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit5 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit6 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit7 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit8 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit9 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit10 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit11 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit12 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit13 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit14 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit15 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit16 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit17 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit18 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit19 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit20 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit21 = new DevExpress.XtraEditors.CheckEdit();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton3 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton4 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton5 = new DevExpress.XtraEditors.SimpleButton();
            this.radioGroup1 = new DevExpress.XtraEditors.RadioGroup();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton3 = new System.Windows.Forms.RadioButton();
            this.separatorControl2 = new DevExpress.XtraEditors.SeparatorControl();
            this.spinEdit1 = new DevExpress.XtraEditors.SpinEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.simpleButton6 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton7 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton8 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton9 = new DevExpress.XtraEditors.SimpleButton();
            this.button1 = new System.Windows.Forms.Button();
            this.simpleButton10 = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.label3 = new System.Windows.Forms.Label();
            this.textEdit1 = new DevExpress.XtraEditors.TextEdit();
            this.simpleButton11 = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            ((System.ComponentModel.ISupportInitialize)(this.windowsUIView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.upperPanel)).BeginInit();
            this.upperPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.activationDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pgId.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.serialNr.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bottomPanel)).BeginInit();
            this.bottomPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).BeginInit();
            this.groupControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rightPanel)).BeginInit();
            this.rightPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            this.groupControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit11.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.centralPanel)).BeginInit();
            this.centralPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.zip.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.company.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.email.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cellPhoneNr.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.businessPhoneNr.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.phoneNr.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.state.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.city.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.address.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lastName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.middleName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.firstName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.separatorControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit7.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit8.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit9.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit10.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit11.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit12.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit13.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit14.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit15.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit16.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit17.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit18.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit19.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit20.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit21.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.separatorControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            this.SuspendLayout();
            // 
            // upperPanel
            // 
            this.upperPanel.Controls.Add(this.groupControl1);
            this.upperPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.upperPanel.Location = new System.Drawing.Point(0, 0);
            this.upperPanel.Name = "upperPanel";
            this.upperPanel.Size = new System.Drawing.Size(948, 100);
            this.upperPanel.TabIndex = 0;
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.activationDate);
            this.groupControl1.Controls.Add(this.pgId);
            this.groupControl1.Controls.Add(this.serialNr);
            this.groupControl1.Controls.Add(this.labelControl3);
            this.groupControl1.Controls.Add(this.labelControl2);
            this.groupControl1.Controls.Add(this.labelControl1);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl1.Location = new System.Drawing.Point(2, 2);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(944, 96);
            this.groupControl1.TabIndex = 0;
            this.groupControl1.Text = "REGISTRATION DETAILS";
            // 
            // activationDate
            // 
            this.activationDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.activationDate.Location = new System.Drawing.Point(746, 47);
            this.activationDate.Name = "activationDate";
            this.activationDate.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.activationDate.Properties.Appearance.Options.UseFont = true;
            this.activationDate.Size = new System.Drawing.Size(193, 42);
            this.activationDate.TabIndex = 5;
            // 
            // pgId
            // 
            this.pgId.Location = new System.Drawing.Point(230, 47);
            this.pgId.Name = "pgId";
            this.pgId.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pgId.Properties.Appearance.Options.UseFont = true;
            this.pgId.Size = new System.Drawing.Size(387, 42);
            this.pgId.TabIndex = 4;
            // 
            // serialNr
            // 
            this.serialNr.EditValue = "";
            this.serialNr.Location = new System.Drawing.Point(6, 47);
            this.serialNr.Name = "serialNr";
            this.serialNr.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.serialNr.Properties.Appearance.Options.UseFont = true;
            this.serialNr.Size = new System.Drawing.Size(218, 42);
            this.serialNr.TabIndex = 3;
            // 
            // labelControl3
            // 
            this.labelControl3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl3.Location = new System.Drawing.Point(746, 25);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(91, 13);
            this.labelControl3.TabIndex = 2;
            this.labelControl3.Text = "ACTIVATION DATE";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(230, 25);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(27, 13);
            this.labelControl2.TabIndex = 1;
            this.labelControl2.Text = "PG ID";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(6, 27);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(56, 13);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "SERIAL NR.";
            // 
            // bottomPanel
            // 
            this.bottomPanel.Controls.Add(this.groupControl4);
            this.bottomPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.bottomPanel.Location = new System.Drawing.Point(0, 785);
            this.bottomPanel.Name = "bottomPanel";
            this.bottomPanel.Size = new System.Drawing.Size(948, 100);
            this.bottomPanel.TabIndex = 1;
            // 
            // groupControl4
            // 
            this.groupControl4.Controls.Add(this.textEdit4);
            this.groupControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl4.Location = new System.Drawing.Point(2, 2);
            this.groupControl4.Name = "groupControl4";
            this.groupControl4.Size = new System.Drawing.Size(944, 96);
            this.groupControl4.TabIndex = 0;
            this.groupControl4.Text = "SERVICE STATUS";
            // 
            // textEdit4
            // 
            this.textEdit4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textEdit4.EditValue = "";
            this.textEdit4.Location = new System.Drawing.Point(6, 44);
            this.textEdit4.Name = "textEdit4";
            this.textEdit4.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit4.Properties.Appearance.Options.UseFont = true;
            this.textEdit4.Size = new System.Drawing.Size(933, 38);
            this.textEdit4.TabIndex = 23;
            // 
            // rightPanel
            // 
            this.rightPanel.Controls.Add(this.groupControl3);
            this.rightPanel.Dock = System.Windows.Forms.DockStyle.Right;
            this.rightPanel.Location = new System.Drawing.Point(748, 100);
            this.rightPanel.Name = "rightPanel";
            this.rightPanel.Size = new System.Drawing.Size(200, 685);
            this.rightPanel.TabIndex = 3;
            // 
            // groupControl3
            // 
            this.groupControl3.Controls.Add(this.gridControl2);
            this.groupControl3.Controls.Add(this.simpleButton11);
            this.groupControl3.Controls.Add(this.textEdit1);
            this.groupControl3.Controls.Add(this.label3);
            this.groupControl3.Controls.Add(this.gridControl1);
            this.groupControl3.Controls.Add(this.simpleButton10);
            this.groupControl3.Controls.Add(this.textEdit11);
            this.groupControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl3.Location = new System.Drawing.Point(2, 2);
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.Size = new System.Drawing.Size(196, 681);
            this.groupControl3.TabIndex = 0;
            this.groupControl3.Text = "UDS INFUSION";
            // 
            // textEdit11
            // 
            this.textEdit11.EditValue = "ASDF-ASDF-ASDF-ASDF";
            this.textEdit11.Location = new System.Drawing.Point(-114, 125);
            this.textEdit11.Name = "textEdit11";
            this.textEdit11.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit11.Properties.Appearance.Options.UseFont = true;
            this.textEdit11.Size = new System.Drawing.Size(105, 38);
            this.textEdit11.TabIndex = 17;
            // 
            // centralPanel
            // 
            this.centralPanel.Controls.Add(this.groupControl2);
            this.centralPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.centralPanel.Location = new System.Drawing.Point(0, 100);
            this.centralPanel.Name = "centralPanel";
            this.centralPanel.Size = new System.Drawing.Size(748, 685);
            this.centralPanel.TabIndex = 4;
            // 
            // groupControl2
            // 
            this.groupControl2.Controls.Add(this.simpleButton9);
            this.groupControl2.Controls.Add(this.simpleButton8);
            this.groupControl2.Controls.Add(this.simpleButton7);
            this.groupControl2.Controls.Add(this.simpleButton6);
            this.groupControl2.Controls.Add(this.label1);
            this.groupControl2.Controls.Add(this.spinEdit1);
            this.groupControl2.Controls.Add(this.separatorControl2);
            this.groupControl2.Controls.Add(this.radioButton3);
            this.groupControl2.Controls.Add(this.radioButton2);
            this.groupControl2.Controls.Add(this.radioButton1);
            this.groupControl2.Controls.Add(this.radioGroup1);
            this.groupControl2.Controls.Add(this.simpleButton5);
            this.groupControl2.Controls.Add(this.simpleButton4);
            this.groupControl2.Controls.Add(this.simpleButton3);
            this.groupControl2.Controls.Add(this.simpleButton2);
            this.groupControl2.Controls.Add(this.simpleButton1);
            this.groupControl2.Controls.Add(this.checkEdit21);
            this.groupControl2.Controls.Add(this.checkEdit16);
            this.groupControl2.Controls.Add(this.checkEdit17);
            this.groupControl2.Controls.Add(this.checkEdit18);
            this.groupControl2.Controls.Add(this.checkEdit19);
            this.groupControl2.Controls.Add(this.checkEdit20);
            this.groupControl2.Controls.Add(this.checkEdit11);
            this.groupControl2.Controls.Add(this.checkEdit12);
            this.groupControl2.Controls.Add(this.checkEdit13);
            this.groupControl2.Controls.Add(this.checkEdit14);
            this.groupControl2.Controls.Add(this.checkEdit15);
            this.groupControl2.Controls.Add(this.checkEdit6);
            this.groupControl2.Controls.Add(this.checkEdit7);
            this.groupControl2.Controls.Add(this.checkEdit8);
            this.groupControl2.Controls.Add(this.checkEdit9);
            this.groupControl2.Controls.Add(this.checkEdit10);
            this.groupControl2.Controls.Add(this.checkEdit5);
            this.groupControl2.Controls.Add(this.checkEdit4);
            this.groupControl2.Controls.Add(this.checkEdit3);
            this.groupControl2.Controls.Add(this.checkEdit2);
            this.groupControl2.Controls.Add(this.checkEdit1);
            this.groupControl2.Controls.Add(this.separatorControl1);
            this.groupControl2.Controls.Add(this.zip);
            this.groupControl2.Controls.Add(this.company);
            this.groupControl2.Controls.Add(this.email);
            this.groupControl2.Controls.Add(this.cellPhoneNr);
            this.groupControl2.Controls.Add(this.businessPhoneNr);
            this.groupControl2.Controls.Add(this.phoneNr);
            this.groupControl2.Controls.Add(this.state);
            this.groupControl2.Controls.Add(this.city);
            this.groupControl2.Controls.Add(this.address);
            this.groupControl2.Controls.Add(this.lastName);
            this.groupControl2.Controls.Add(this.middleName);
            this.groupControl2.Controls.Add(this.firstName);
            this.groupControl2.Controls.Add(this.labelControl14);
            this.groupControl2.Controls.Add(this.labelControl13);
            this.groupControl2.Controls.Add(this.labelControl12);
            this.groupControl2.Controls.Add(this.labelControl11);
            this.groupControl2.Controls.Add(this.labelControl10);
            this.groupControl2.Controls.Add(this.labelControl9);
            this.groupControl2.Controls.Add(this.labelControl8);
            this.groupControl2.Controls.Add(this.labelControl7);
            this.groupControl2.Controls.Add(this.labelControl6);
            this.groupControl2.Controls.Add(this.labelControl5);
            this.groupControl2.Controls.Add(this.labelControl4);
            this.groupControl2.Controls.Add(this.label2);
            this.groupControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl2.Location = new System.Drawing.Point(2, 2);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(744, 681);
            this.groupControl2.TabIndex = 0;
            this.groupControl2.Text = "USER DETAILS";
            // 
            // zip
            // 
            this.zip.EditValue = "";
            this.zip.Location = new System.Drawing.Point(634, 120);
            this.zip.Name = "zip";
            this.zip.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.zip.Properties.Appearance.Options.UseFont = true;
            this.zip.Size = new System.Drawing.Size(64, 38);
            this.zip.TabIndex = 22;
            // 
            // company
            // 
            this.company.EditValue = "";
            this.company.Location = new System.Drawing.Point(230, 245);
            this.company.Name = "company";
            this.company.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.company.Properties.Appearance.Options.UseFont = true;
            this.company.Size = new System.Drawing.Size(218, 38);
            this.company.TabIndex = 21;
            // 
            // email
            // 
            this.email.EditValue = "";
            this.email.Location = new System.Drawing.Point(6, 245);
            this.email.Name = "email";
            this.email.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.email.Properties.Appearance.Options.UseFont = true;
            this.email.Size = new System.Drawing.Size(218, 38);
            this.email.TabIndex = 20;
            // 
            // cellPhoneNr
            // 
            this.cellPhoneNr.EditValue = "";
            this.cellPhoneNr.Location = new System.Drawing.Point(454, 182);
            this.cellPhoneNr.Name = "cellPhoneNr";
            this.cellPhoneNr.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cellPhoneNr.Properties.Appearance.Options.UseFont = true;
            this.cellPhoneNr.Size = new System.Drawing.Size(218, 38);
            this.cellPhoneNr.TabIndex = 19;
            // 
            // businessPhoneNr
            // 
            this.businessPhoneNr.EditValue = "";
            this.businessPhoneNr.Location = new System.Drawing.Point(230, 182);
            this.businessPhoneNr.Name = "businessPhoneNr";
            this.businessPhoneNr.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.businessPhoneNr.Properties.Appearance.Options.UseFont = true;
            this.businessPhoneNr.Size = new System.Drawing.Size(218, 38);
            this.businessPhoneNr.TabIndex = 18;
            // 
            // phoneNr
            // 
            this.phoneNr.EditValue = "";
            this.phoneNr.Location = new System.Drawing.Point(6, 182);
            this.phoneNr.Name = "phoneNr";
            this.phoneNr.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.phoneNr.Properties.Appearance.Options.UseFont = true;
            this.phoneNr.Size = new System.Drawing.Size(218, 38);
            this.phoneNr.TabIndex = 17;
            // 
            // state
            // 
            this.state.EditValue = "";
            this.state.Location = new System.Drawing.Point(457, 120);
            this.state.Name = "state";
            this.state.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.state.Properties.Appearance.Options.UseFont = true;
            this.state.Size = new System.Drawing.Size(55, 38);
            this.state.TabIndex = 16;
            // 
            // city
            // 
            this.city.EditValue = "";
            this.city.Location = new System.Drawing.Point(230, 120);
            this.city.Name = "city";
            this.city.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.city.Properties.Appearance.Options.UseFont = true;
            this.city.Size = new System.Drawing.Size(218, 38);
            this.city.TabIndex = 15;
            // 
            // address
            // 
            this.address.EditValue = "";
            this.address.Location = new System.Drawing.Point(6, 120);
            this.address.Name = "address";
            this.address.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.address.Properties.Appearance.Options.UseFont = true;
            this.address.Size = new System.Drawing.Size(218, 38);
            this.address.TabIndex = 14;
            // 
            // lastName
            // 
            this.lastName.EditValue = "";
            this.lastName.Location = new System.Drawing.Point(457, 52);
            this.lastName.Name = "lastName";
            this.lastName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lastName.Properties.Appearance.Options.UseFont = true;
            this.lastName.Size = new System.Drawing.Size(282, 42);
            this.lastName.TabIndex = 13;
            // 
            // middleName
            // 
            this.middleName.EditValue = "";
            this.middleName.Location = new System.Drawing.Point(230, 53);
            this.middleName.Name = "middleName";
            this.middleName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.middleName.Properties.Appearance.Options.UseFont = true;
            this.middleName.Size = new System.Drawing.Size(218, 42);
            this.middleName.TabIndex = 12;
            // 
            // firstName
            // 
            this.firstName.EditValue = "";
            this.firstName.Location = new System.Drawing.Point(6, 52);
            this.firstName.Name = "firstName";
            this.firstName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.firstName.Properties.Appearance.Options.UseFont = true;
            this.firstName.Size = new System.Drawing.Size(218, 42);
            this.firstName.TabIndex = 6;
            // 
            // labelControl14
            // 
            this.labelControl14.Location = new System.Drawing.Point(230, 226);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(49, 13);
            this.labelControl14.TabIndex = 11;
            this.labelControl14.Text = "COMPANY";
            // 
            // labelControl13
            // 
            this.labelControl13.Location = new System.Drawing.Point(9, 226);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(30, 13);
            this.labelControl13.TabIndex = 10;
            this.labelControl13.Text = "EMAIL";
            // 
            // labelControl12
            // 
            this.labelControl12.Location = new System.Drawing.Point(457, 168);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(81, 13);
            this.labelControl12.TabIndex = 9;
            this.labelControl12.Text = "CELL PHONE NR.";
            // 
            // labelControl11
            // 
            this.labelControl11.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.labelControl11.Location = new System.Drawing.Point(230, 168);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(106, 13);
            this.labelControl11.TabIndex = 8;
            this.labelControl11.Text = "BUSINESS PHONE NR.";
            // 
            // labelControl10
            // 
            this.labelControl10.Location = new System.Drawing.Point(9, 168);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(55, 13);
            this.labelControl10.TabIndex = 7;
            this.labelControl10.Text = "PHONE NR.";
            // 
            // labelControl9
            // 
            this.labelControl9.Location = new System.Drawing.Point(634, 106);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(16, 13);
            this.labelControl9.TabIndex = 6;
            this.labelControl9.Text = "ZIP";
            // 
            // labelControl8
            // 
            this.labelControl8.Location = new System.Drawing.Point(457, 106);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(31, 13);
            this.labelControl8.TabIndex = 5;
            this.labelControl8.Text = "STATE";
            // 
            // labelControl7
            // 
            this.labelControl7.Location = new System.Drawing.Point(230, 106);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(23, 13);
            this.labelControl7.TabIndex = 4;
            this.labelControl7.Text = "CITY";
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(9, 106);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(46, 13);
            this.labelControl6.TabIndex = 3;
            this.labelControl6.Text = "ADDRESS";
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(457, 34);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(55, 13);
            this.labelControl5.TabIndex = 2;
            this.labelControl5.Text = "LAST NAME";
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(230, 34);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(68, 13);
            this.labelControl4.TabIndex = 1;
            this.labelControl4.Text = "MIDDLE NAME";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 36);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "FIRST NAME";
            // 
            // separatorControl1
            // 
            this.separatorControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.separatorControl1.Location = new System.Drawing.Point(6, 289);
            this.separatorControl1.Name = "separatorControl1";
            this.separatorControl1.Size = new System.Drawing.Size(733, 23);
            this.separatorControl1.TabIndex = 23;
            // 
            // checkEdit1
            // 
            this.checkEdit1.Location = new System.Drawing.Point(9, 345);
            this.checkEdit1.Name = "checkEdit1";
            this.checkEdit1.Properties.Caption = "Mod 1 - Prop.";
            this.checkEdit1.Size = new System.Drawing.Size(129, 44);
            this.checkEdit1.TabIndex = 25;
            // 
            // checkEdit2
            // 
            this.checkEdit2.Location = new System.Drawing.Point(9, 389);
            this.checkEdit2.Name = "checkEdit2";
            this.checkEdit2.Properties.Caption = "Mod 2 - COMPS";
            this.checkEdit2.Size = new System.Drawing.Size(129, 44);
            this.checkEdit2.TabIndex = 26;
            // 
            // checkEdit3
            // 
            this.checkEdit3.Location = new System.Drawing.Point(9, 433);
            this.checkEdit3.Name = "checkEdit3";
            this.checkEdit3.Properties.Caption = "Mod 3 - Get Forms Ready";
            this.checkEdit3.Size = new System.Drawing.Size(183, 44);
            this.checkEdit3.TabIndex = 27;
            // 
            // checkEdit4
            // 
            this.checkEdit4.Location = new System.Drawing.Point(9, 477);
            this.checkEdit4.Name = "checkEdit4";
            this.checkEdit4.Properties.Caption = "Mod 4 - Deal IQ";
            this.checkEdit4.Size = new System.Drawing.Size(129, 44);
            this.checkEdit4.TabIndex = 28;
            // 
            // checkEdit5
            // 
            this.checkEdit5.Location = new System.Drawing.Point(9, 521);
            this.checkEdit5.Name = "checkEdit5";
            this.checkEdit5.Properties.Caption = "Mod 5- Matching Module";
            this.checkEdit5.Size = new System.Drawing.Size(170, 44);
            this.checkEdit5.TabIndex = 29;
            // 
            // checkEdit6
            // 
            this.checkEdit6.Location = new System.Drawing.Point(180, 521);
            this.checkEdit6.Name = "checkEdit6";
            this.checkEdit6.Properties.Caption = "Mod 10 - No ZIP Filter Internal";
            this.checkEdit6.Size = new System.Drawing.Size(192, 44);
            this.checkEdit6.TabIndex = 34;
            // 
            // checkEdit7
            // 
            this.checkEdit7.Location = new System.Drawing.Point(180, 477);
            this.checkEdit7.Name = "checkEdit7";
            this.checkEdit7.Properties.Caption = "Mod 9 - Int. Comm. Module";
            this.checkEdit7.Size = new System.Drawing.Size(192, 44);
            this.checkEdit7.TabIndex = 33;
            // 
            // checkEdit8
            // 
            this.checkEdit8.Location = new System.Drawing.Point(180, 433);
            this.checkEdit8.Name = "checkEdit8";
            this.checkEdit8.Properties.Caption = "Mod 8 - Int. Short Sale Mod.";
            this.checkEdit8.Size = new System.Drawing.Size(192, 44);
            this.checkEdit8.TabIndex = 32;
            // 
            // checkEdit9
            // 
            this.checkEdit9.Location = new System.Drawing.Point(180, 389);
            this.checkEdit9.Name = "checkEdit9";
            this.checkEdit9.Properties.Caption = "Mod 7 - 123Sold";
            this.checkEdit9.Size = new System.Drawing.Size(129, 44);
            this.checkEdit9.TabIndex = 31;
            // 
            // checkEdit10
            // 
            this.checkEdit10.Location = new System.Drawing.Point(180, 345);
            this.checkEdit10.Name = "checkEdit10";
            this.checkEdit10.Properties.Caption = "Mod 6 - Network Version";
            this.checkEdit10.Size = new System.Drawing.Size(167, 44);
            this.checkEdit10.TabIndex = 30;
            // 
            // checkEdit11
            // 
            this.checkEdit11.Location = new System.Drawing.Point(372, 521);
            this.checkEdit11.Name = "checkEdit11";
            this.checkEdit11.Properties.Caption = "Mod 15";
            this.checkEdit11.Size = new System.Drawing.Size(85, 44);
            this.checkEdit11.TabIndex = 39;
            // 
            // checkEdit12
            // 
            this.checkEdit12.Location = new System.Drawing.Point(372, 477);
            this.checkEdit12.Name = "checkEdit12";
            this.checkEdit12.Properties.Caption = "Mod 14";
            this.checkEdit12.Size = new System.Drawing.Size(85, 44);
            this.checkEdit12.TabIndex = 38;
            // 
            // checkEdit13
            // 
            this.checkEdit13.Location = new System.Drawing.Point(372, 433);
            this.checkEdit13.Name = "checkEdit13";
            this.checkEdit13.Properties.Caption = "Mod 13";
            this.checkEdit13.Size = new System.Drawing.Size(85, 44);
            this.checkEdit13.TabIndex = 37;
            // 
            // checkEdit14
            // 
            this.checkEdit14.Location = new System.Drawing.Point(372, 389);
            this.checkEdit14.Name = "checkEdit14";
            this.checkEdit14.Properties.Caption = "Mod 12";
            this.checkEdit14.Size = new System.Drawing.Size(85, 44);
            this.checkEdit14.TabIndex = 36;
            // 
            // checkEdit15
            // 
            this.checkEdit15.Location = new System.Drawing.Point(372, 345);
            this.checkEdit15.Name = "checkEdit15";
            this.checkEdit15.Properties.Caption = "Mod 11";
            this.checkEdit15.Size = new System.Drawing.Size(85, 44);
            this.checkEdit15.TabIndex = 35;
            // 
            // checkEdit16
            // 
            this.checkEdit16.Location = new System.Drawing.Point(463, 521);
            this.checkEdit16.Name = "checkEdit16";
            this.checkEdit16.Properties.Caption = "Mod 20";
            this.checkEdit16.Size = new System.Drawing.Size(85, 44);
            this.checkEdit16.TabIndex = 44;
            // 
            // checkEdit17
            // 
            this.checkEdit17.Location = new System.Drawing.Point(463, 477);
            this.checkEdit17.Name = "checkEdit17";
            this.checkEdit17.Properties.Caption = "Mod 19";
            this.checkEdit17.Size = new System.Drawing.Size(85, 44);
            this.checkEdit17.TabIndex = 43;
            // 
            // checkEdit18
            // 
            this.checkEdit18.Location = new System.Drawing.Point(463, 433);
            this.checkEdit18.Name = "checkEdit18";
            this.checkEdit18.Properties.Caption = "Mod 18";
            this.checkEdit18.Size = new System.Drawing.Size(85, 44);
            this.checkEdit18.TabIndex = 42;
            // 
            // checkEdit19
            // 
            this.checkEdit19.Location = new System.Drawing.Point(463, 389);
            this.checkEdit19.Name = "checkEdit19";
            this.checkEdit19.Properties.Caption = "Mod 17";
            this.checkEdit19.Size = new System.Drawing.Size(85, 44);
            this.checkEdit19.TabIndex = 41;
            // 
            // checkEdit20
            // 
            this.checkEdit20.Location = new System.Drawing.Point(463, 345);
            this.checkEdit20.Name = "checkEdit20";
            this.checkEdit20.Properties.Caption = "Mod 16";
            this.checkEdit20.Size = new System.Drawing.Size(85, 44);
            this.checkEdit20.TabIndex = 40;
            // 
            // checkEdit21
            // 
            this.checkEdit21.Location = new System.Drawing.Point(9, 307);
            this.checkEdit21.Name = "checkEdit21";
            this.checkEdit21.Properties.Caption = "Mod 0 - PG Active";
            this.checkEdit21.Size = new System.Drawing.Size(129, 44);
            this.checkEdit21.TabIndex = 45;
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(577, 307);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(164, 44);
            this.simpleButton1.TabIndex = 46;
            this.simpleButton1.Text = "GET DETAILS";
            // 
            // simpleButton2
            // 
            this.simpleButton2.Location = new System.Drawing.Point(577, 357);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(164, 44);
            this.simpleButton2.TabIndex = 47;
            this.simpleButton2.Text = "SET DETAILS";
            // 
            // simpleButton3
            // 
            this.simpleButton3.Location = new System.Drawing.Point(577, 407);
            this.simpleButton3.Name = "simpleButton3";
            this.simpleButton3.Size = new System.Drawing.Size(164, 44);
            this.simpleButton3.TabIndex = 48;
            this.simpleButton3.Text = "Charge MM 1 unit";
            // 
            // simpleButton4
            // 
            this.simpleButton4.Location = new System.Drawing.Point(577, 457);
            this.simpleButton4.Name = "simpleButton4";
            this.simpleButton4.Size = new System.Drawing.Size(164, 44);
            this.simpleButton4.TabIndex = 49;
            this.simpleButton4.Text = "Charge PP 18 units";
            // 
            // simpleButton5
            // 
            this.simpleButton5.Location = new System.Drawing.Point(577, 507);
            this.simpleButton5.Name = "simpleButton5";
            this.simpleButton5.Size = new System.Drawing.Size(164, 44);
            this.simpleButton5.TabIndex = 50;
            this.simpleButton5.Text = "Charge COMPS 22 units";
            // 
            // radioGroup1
            // 
            this.radioGroup1.Location = new System.Drawing.Point(9, 605);
            this.radioGroup1.Name = "radioGroup1";
            this.radioGroup1.Size = new System.Drawing.Size(183, 45);
            this.radioGroup1.TabIndex = 51;
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Location = new System.Drawing.Point(22, 619);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(41, 17);
            this.radioButton1.TabIndex = 52;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "MM";
            this.radioButton1.UseVisualStyleBackColor = true;
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(69, 619);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(37, 17);
            this.radioButton2.TabIndex = 53;
            this.radioButton2.TabStop = true;
            this.radioButton2.Text = "PP";
            this.radioButton2.UseVisualStyleBackColor = true;
            // 
            // radioButton3
            // 
            this.radioButton3.AutoSize = true;
            this.radioButton3.Location = new System.Drawing.Point(119, 619);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new System.Drawing.Size(60, 17);
            this.radioButton3.TabIndex = 54;
            this.radioButton3.TabStop = true;
            this.radioButton3.Text = "COMPS";
            this.radioButton3.UseVisualStyleBackColor = true;
            // 
            // separatorControl2
            // 
            this.separatorControl2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.separatorControl2.Location = new System.Drawing.Point(6, 563);
            this.separatorControl2.Name = "separatorControl2";
            this.separatorControl2.Size = new System.Drawing.Size(733, 23);
            this.separatorControl2.TabIndex = 55;
            // 
            // spinEdit1
            // 
            this.spinEdit1.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEdit1.Location = new System.Drawing.Point(270, 595);
            this.spinEdit1.Name = "spinEdit1";
            this.spinEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spinEdit1.Properties.IsFloatValue = false;
            this.spinEdit1.Properties.Mask.EditMask = "N00";
            this.spinEdit1.Size = new System.Drawing.Size(100, 66);
            this.spinEdit1.TabIndex = 56;
            this.spinEdit1.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Hand;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(212, 620);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 13);
            this.label1.TabIndex = 57;
            this.label1.Text = "AMOUNT";
            // 
            // simpleButton6
            // 
            this.simpleButton6.Location = new System.Drawing.Point(384, 581);
            this.simpleButton6.Name = "simpleButton6";
            this.simpleButton6.Size = new System.Drawing.Size(164, 44);
            this.simpleButton6.TabIndex = 58;
            this.simpleButton6.Text = "Query Account";
            // 
            // simpleButton7
            // 
            this.simpleButton7.Location = new System.Drawing.Point(384, 630);
            this.simpleButton7.Name = "simpleButton7";
            this.simpleButton7.Size = new System.Drawing.Size(164, 44);
            this.simpleButton7.TabIndex = 59;
            this.simpleButton7.Text = "Query Data Request";
            // 
            // simpleButton8
            // 
            this.simpleButton8.Location = new System.Drawing.Point(575, 578);
            this.simpleButton8.Name = "simpleButton8";
            this.simpleButton8.Size = new System.Drawing.Size(164, 44);
            this.simpleButton8.TabIndex = 60;
            this.simpleButton8.Text = "Add UDS Monthly Credits";
            // 
            // simpleButton9
            // 
            this.simpleButton9.Location = new System.Drawing.Point(575, 628);
            this.simpleButton9.Name = "simpleButton9";
            this.simpleButton9.Size = new System.Drawing.Size(164, 44);
            this.simpleButton9.TabIndex = 61;
            this.simpleButton9.Text = "View Inputs and TopUps";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(0, 0);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 5;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // simpleButton10
            // 
            this.simpleButton10.Location = new System.Drawing.Point(5, 50);
            this.simpleButton10.Name = "simpleButton10";
            this.simpleButton10.Size = new System.Drawing.Size(186, 45);
            this.simpleButton10.TabIndex = 47;
            this.simpleButton10.Text = "Get Assigned InfusionId \r\nfor CURRENT User";
            // 
            // gridControl1
            // 
            this.gridControl1.Location = new System.Drawing.Point(5, 101);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(400, 200);
            this.gridControl1.TabIndex = 48;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(5, 352);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(193, 13);
            this.label3.TabIndex = 49;
            this.label3.Text = "Get Assigned PG Users for Infusion ID:";
            // 
            // textEdit1
            // 
            this.textEdit1.EditValue = "";
            this.textEdit1.Location = new System.Drawing.Point(5, 373);
            this.textEdit1.Name = "textEdit1";
            this.textEdit1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit1.Properties.Appearance.Options.UseFont = true;
            this.textEdit1.Size = new System.Drawing.Size(186, 42);
            this.textEdit1.TabIndex = 50;
            // 
            // simpleButton11
            // 
            this.simpleButton11.Location = new System.Drawing.Point(5, 421);
            this.simpleButton11.Name = "simpleButton11";
            this.simpleButton11.Size = new System.Drawing.Size(186, 45);
            this.simpleButton11.TabIndex = 51;
            this.simpleButton11.Text = "SEARCH";
            // 
            // gridControl2
            // 
            this.gridControl2.Location = new System.Drawing.Point(5, 472);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.Size = new System.Drawing.Size(400, 200);
            this.gridControl2.TabIndex = 52;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // gridView2
            // 
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.Name = "gridView2";
            // 
            // ucUserDetails
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.button1);
            this.Controls.Add(this.centralPanel);
            this.Controls.Add(this.rightPanel);
            this.Controls.Add(this.bottomPanel);
            this.Controls.Add(this.upperPanel);
            this.Name = "ucUserDetails";
            this.Size = new System.Drawing.Size(948, 885);
            ((System.ComponentModel.ISupportInitialize)(this.windowsUIView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.upperPanel)).EndInit();
            this.upperPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.activationDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pgId.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.serialNr.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bottomPanel)).EndInit();
            this.bottomPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).EndInit();
            this.groupControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.textEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rightPanel)).EndInit();
            this.rightPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            this.groupControl3.ResumeLayout(false);
            this.groupControl3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit11.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.centralPanel)).EndInit();
            this.centralPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            this.groupControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.zip.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.company.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.email.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cellPhoneNr.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.businessPhoneNr.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.phoneNr.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.state.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.city.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.address.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lastName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.middleName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.firstName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.separatorControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit7.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit8.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit9.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit10.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit11.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit12.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit13.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit14.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit15.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit16.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit17.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit18.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit19.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit20.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit21.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.separatorControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        
        private DevExpress.XtraBars.Docking2010.Views.WindowsUI.WindowsUIView windowsUIView1;
        private DevExpress.XtraEditors.PanelControl upperPanel;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.PanelControl bottomPanel;
        private DevExpress.XtraEditors.GroupControl groupControl4;
        private DevExpress.XtraEditors.PanelControl rightPanel;
        private DevExpress.XtraEditors.GroupControl groupControl3;
        private DevExpress.XtraEditors.PanelControl centralPanel;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.TextEdit activationDate;
        private DevExpress.XtraEditors.TextEdit pgId;
        private DevExpress.XtraEditors.TextEdit serialNr;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit textEdit11;
        private DevExpress.XtraEditors.TextEdit zip;
        private DevExpress.XtraEditors.TextEdit company;
        private DevExpress.XtraEditors.TextEdit email;
        private DevExpress.XtraEditors.TextEdit cellPhoneNr;
        private DevExpress.XtraEditors.TextEdit businessPhoneNr;
        private DevExpress.XtraEditors.TextEdit phoneNr;
        private DevExpress.XtraEditors.TextEdit state;
        private DevExpress.XtraEditors.TextEdit city;
        private DevExpress.XtraEditors.TextEdit address;
        private DevExpress.XtraEditors.TextEdit lastName;
        private DevExpress.XtraEditors.TextEdit middleName;
        private DevExpress.XtraEditors.TextEdit firstName;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private System.Windows.Forms.Label label2;
        private DevExpress.XtraEditors.TextEdit textEdit4;
        private DevExpress.XtraEditors.CheckEdit checkEdit2;
        private DevExpress.XtraEditors.CheckEdit checkEdit1;
        private DevExpress.XtraEditors.SeparatorControl separatorControl1;
        private DevExpress.XtraEditors.SimpleButton simpleButton9;
        private DevExpress.XtraEditors.SimpleButton simpleButton8;
        private DevExpress.XtraEditors.SimpleButton simpleButton7;
        private DevExpress.XtraEditors.SimpleButton simpleButton6;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.SpinEdit spinEdit1;
        private DevExpress.XtraEditors.SeparatorControl separatorControl2;
        private System.Windows.Forms.RadioButton radioButton3;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton1;
        private DevExpress.XtraEditors.RadioGroup radioGroup1;
        private DevExpress.XtraEditors.SimpleButton simpleButton5;
        private DevExpress.XtraEditors.SimpleButton simpleButton4;
        private DevExpress.XtraEditors.SimpleButton simpleButton3;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.CheckEdit checkEdit21;
        private DevExpress.XtraEditors.CheckEdit checkEdit16;
        private DevExpress.XtraEditors.CheckEdit checkEdit17;
        private DevExpress.XtraEditors.CheckEdit checkEdit18;
        private DevExpress.XtraEditors.CheckEdit checkEdit19;
        private DevExpress.XtraEditors.CheckEdit checkEdit20;
        private DevExpress.XtraEditors.CheckEdit checkEdit11;
        private DevExpress.XtraEditors.CheckEdit checkEdit12;
        private DevExpress.XtraEditors.CheckEdit checkEdit13;
        private DevExpress.XtraEditors.CheckEdit checkEdit14;
        private DevExpress.XtraEditors.CheckEdit checkEdit15;
        private DevExpress.XtraEditors.CheckEdit checkEdit6;
        private DevExpress.XtraEditors.CheckEdit checkEdit7;
        private DevExpress.XtraEditors.CheckEdit checkEdit8;
        private DevExpress.XtraEditors.CheckEdit checkEdit9;
        private DevExpress.XtraEditors.CheckEdit checkEdit10;
        private DevExpress.XtraEditors.CheckEdit checkEdit5;
        private DevExpress.XtraEditors.CheckEdit checkEdit4;
        private DevExpress.XtraEditors.CheckEdit checkEdit3;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraEditors.SimpleButton simpleButton11;
        private DevExpress.XtraEditors.TextEdit textEdit1;
        private System.Windows.Forms.Label label3;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.SimpleButton simpleButton10;
        private System.Windows.Forms.Button button1;
    }
}
