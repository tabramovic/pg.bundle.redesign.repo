﻿namespace WebManager
{
    partial class FluentDesignForm1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WebManager.SplashScreen1), true, true, true);
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FluentDesignForm1));
            this.container = new DevExpress.XtraBars.FluentDesignSystem.FluentDesignFormContainer();
            this.accordionMainMenu = new DevExpress.XtraBars.Navigation.AccordionControl();
            this.accordionContentContainer1 = new DevExpress.XtraBars.Navigation.AccordionContentContainer();
            this.calendarControl1 = new DevExpress.XtraEditors.Controls.CalendarControl();
            this._registeredUsersLabel = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this._getRegisteredUsersCtrlElem = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this._registerUserCtrlElem = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this._userRegistrationDetailsCtrlElem = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.@__dataProviderLabel = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this._compsCtrlElem = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this._propertyProfileCtrlElem = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this._floodCompsCtrlElem = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.accordionControlElement1 = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.accordionControlElement2 = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.fluentDesignFormControl1 = new DevExpress.XtraBars.FluentDesignSystem.FluentDesignFormControl();
            this.barStaticItem1 = new DevExpress.XtraBars.BarStaticItem();
            this.behaviorManager1 = new DevExpress.Utils.Behaviors.BehaviorManager(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.accordionMainMenu)).BeginInit();
            this.accordionMainMenu.SuspendLayout();
            this.accordionContentContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.calendarControl1.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fluentDesignFormControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.behaviorManager1)).BeginInit();
            this.SuspendLayout();
            // 
            // splashScreenManager1
            // 
            splashScreenManager1.ClosingDelay = 3000;
            // 
            // container
            // 
            this.container.Dock = System.Windows.Forms.DockStyle.Fill;
            this.container.Location = new System.Drawing.Point(385, 51);
            this.container.Name = "container";
            this.container.Size = new System.Drawing.Size(727, 848);
            this.container.TabIndex = 0;
            // 
            // accordionMainMenu
            // 
            this.accordionMainMenu.Controls.Add(this.accordionContentContainer1);
            this.accordionMainMenu.Dock = System.Windows.Forms.DockStyle.Left;
            this.accordionMainMenu.Elements.AddRange(new DevExpress.XtraBars.Navigation.AccordionControlElement[] {
            this._registeredUsersLabel,
            this.@__dataProviderLabel,
            this.accordionControlElement1});
            this.accordionMainMenu.Location = new System.Drawing.Point(0, 51);
            this.accordionMainMenu.Name = "accordionMainMenu";
            this.accordionMainMenu.ScrollBarMode = DevExpress.XtraBars.Navigation.ScrollBarMode.Fluent;
            this.accordionMainMenu.Size = new System.Drawing.Size(385, 848);
            this.accordionMainMenu.TabIndex = 1;
            this.accordionMainMenu.ViewType = DevExpress.XtraBars.Navigation.AccordionControlViewType.HamburgerMenu;
            // 
            // accordionContentContainer1
            // 
            this.accordionContentContainer1.Controls.Add(this.calendarControl1);
            this.accordionContentContainer1.Name = "accordionContentContainer1";
            this.accordionContentContainer1.Size = new System.Drawing.Size(368, 411);
            this.accordionContentContainer1.TabIndex = 2;
            // 
            // calendarControl1
            // 
            this.calendarControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.calendarControl1.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.calendarControl1.CalendarView = DevExpress.XtraEditors.Repository.CalendarView.Fluent;
            this.calendarControl1.Location = new System.Drawing.Point(3, -1);
            this.calendarControl1.Name = "calendarControl1";
            this.calendarControl1.ShowFooter = false;
            this.calendarControl1.ShowMonthHeaders = false;
            this.calendarControl1.ShowToolTips = false;
            this.calendarControl1.Size = new System.Drawing.Size(559, 398);
            this.calendarControl1.TabIndex = 0;
            this.calendarControl1.UseDirectXPaint = DevExpress.Utils.DefaultBoolean.True;
            // 
            // _registeredUsersLabel
            // 
            this._registeredUsersLabel.Elements.AddRange(new DevExpress.XtraBars.Navigation.AccordionControlElement[] {
            this._getRegisteredUsersCtrlElem,
            this._registerUserCtrlElem,
            this._userRegistrationDetailsCtrlElem});
            this._registeredUsersLabel.Expanded = true;
            this._registeredUsersLabel.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("_registeredUsersLabel.ImageOptions.SvgImage")));
            this._registeredUsersLabel.Name = "_registeredUsersLabel";
            this._registeredUsersLabel.Text = "ProfitGrabber Users";
            // 
            // _getRegisteredUsersCtrlElem
            // 
            this._getRegisteredUsersCtrlElem.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("_getRegisteredUsersCtrlElem.ImageOptions.SvgImage")));
            this._getRegisteredUsersCtrlElem.Name = "_getRegisteredUsersCtrlElem";
            this._getRegisteredUsersCtrlElem.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this._getRegisteredUsersCtrlElem.Text = "Get Registered Users";
            this._getRegisteredUsersCtrlElem.Click += new System.EventHandler(this._getRegisteredUsersCtrlElem_Click);
            // 
            // _registerUserCtrlElem
            // 
            this._registerUserCtrlElem.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("_registerUserCtrlElem.ImageOptions.SvgImage")));
            this._registerUserCtrlElem.Name = "_registerUserCtrlElem";
            this._registerUserCtrlElem.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this._registerUserCtrlElem.Text = "Register User";
            this._registerUserCtrlElem.Visible = false;
            // 
            // _userRegistrationDetailsCtrlElem
            // 
            this._userRegistrationDetailsCtrlElem.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("_userRegistrationDetailsCtrlElem.ImageOptions.SvgImage")));
            this._userRegistrationDetailsCtrlElem.Name = "_userRegistrationDetailsCtrlElem";
            this._userRegistrationDetailsCtrlElem.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this._userRegistrationDetailsCtrlElem.Text = "User and Registration Details";
            this._userRegistrationDetailsCtrlElem.Click += new System.EventHandler(this._userRegistrationDetailsCtrlElem_Click);
            // 
            // __dataProviderLabel
            // 
            this.@__dataProviderLabel.Elements.AddRange(new DevExpress.XtraBars.Navigation.AccordionControlElement[] {
            this._compsCtrlElem,
            this._propertyProfileCtrlElem,
            this._floodCompsCtrlElem});
            this.@__dataProviderLabel.Expanded = true;
            this.@__dataProviderLabel.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("__dataProviderLabel.ImageOptions.SvgImage")));
            this.@__dataProviderLabel.Name = "__dataProviderLabel";
            this.@__dataProviderLabel.Text = "Data Provider";
            this.@__dataProviderLabel.Visible = false;
            // 
            // _compsCtrlElem
            // 
            this._compsCtrlElem.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("_compsCtrlElem.ImageOptions.SvgImage")));
            this._compsCtrlElem.Name = "_compsCtrlElem";
            this._compsCtrlElem.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this._compsCtrlElem.Text = "COMPS";
            // 
            // _propertyProfileCtrlElem
            // 
            this._propertyProfileCtrlElem.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("_propertyProfileCtrlElem.ImageOptions.SvgImage")));
            this._propertyProfileCtrlElem.Name = "_propertyProfileCtrlElem";
            this._propertyProfileCtrlElem.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this._propertyProfileCtrlElem.Text = "Property Profile";
            // 
            // _floodCompsCtrlElem
            // 
            this._floodCompsCtrlElem.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("_floodCompsCtrlElem.ImageOptions.SvgImage")));
            this._floodCompsCtrlElem.Name = "_floodCompsCtrlElem";
            this._floodCompsCtrlElem.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this._floodCompsCtrlElem.Text = "Flood COMPS";
            // 
            // accordionControlElement1
            // 
            this.accordionControlElement1.Elements.AddRange(new DevExpress.XtraBars.Navigation.AccordionControlElement[] {
            this.accordionControlElement2});
            this.accordionControlElement1.Expanded = true;
            this.accordionControlElement1.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("accordionControlElement1.ImageOptions.SvgImage")));
            this.accordionControlElement1.Name = "accordionControlElement1";
            this.accordionControlElement1.Text = "Acryllic Design";
            this.accordionControlElement1.Visible = false;
            // 
            // accordionControlElement2
            // 
            this.accordionControlElement2.ContentContainer = this.accordionContentContainer1;
            this.accordionControlElement2.Expanded = true;
            this.accordionControlElement2.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("accordionControlElement2.ImageOptions.SvgImage")));
            this.accordionControlElement2.Name = "accordionControlElement2";
            this.accordionControlElement2.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.accordionControlElement2.Text = "Date Control";
            // 
            // fluentDesignFormControl1
            // 
            this.fluentDesignFormControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.fluentDesignFormControl1.FluentDesignForm = this;
            this.fluentDesignFormControl1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barStaticItem1});
            this.fluentDesignFormControl1.Location = new System.Drawing.Point(0, 0);
            this.fluentDesignFormControl1.Name = "fluentDesignFormControl1";
            this.fluentDesignFormControl1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.fluentDesignFormControl1.Size = new System.Drawing.Size(1112, 51);
            this.fluentDesignFormControl1.TabIndex = 2;
            this.fluentDesignFormControl1.TabStop = false;
            this.fluentDesignFormControl1.TitleItemLinks.Add(this.barStaticItem1);
            // 
            // barStaticItem1
            // 
            this.barStaticItem1.Caption = "PG WEB MANAGER";
            this.barStaticItem1.Id = 0;
            this.barStaticItem1.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("barStaticItem1.ImageOptions.SvgImage")));
            this.barStaticItem1.Name = "barStaticItem1";
            // 
            // FluentDesignForm1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(1112, 899);
            this.ControlContainer = this.container;
            this.Controls.Add(this.container);
            this.Controls.Add(this.accordionMainMenu);
            this.Controls.Add(this.fluentDesignFormControl1);
            this.FluentDesignFormControl = this.fluentDesignFormControl1;
            this.FormBorderEffect = DevExpress.XtraEditors.FormBorderEffect.None;
            this.Name = "FluentDesignForm1";
            this.NavigationControl = this.accordionMainMenu;
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            ((System.ComponentModel.ISupportInitialize)(this.accordionMainMenu)).EndInit();
            this.accordionMainMenu.ResumeLayout(false);
            this.accordionContentContainer1.ResumeLayout(false);
            this.accordionContentContainer1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.calendarControl1.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fluentDesignFormControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.behaviorManager1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private DevExpress.XtraBars.FluentDesignSystem.FluentDesignFormContainer container;
        private DevExpress.XtraBars.Navigation.AccordionControl accordionMainMenu;
        private DevExpress.XtraBars.Navigation.AccordionControlElement _registeredUsersLabel;
        private DevExpress.XtraBars.FluentDesignSystem.FluentDesignFormControl fluentDesignFormControl1;
        private DevExpress.XtraBars.Navigation.AccordionControlElement _getRegisteredUsersCtrlElem;
        private DevExpress.XtraBars.Navigation.AccordionControlElement __dataProviderLabel;
        private DevExpress.XtraBars.Navigation.AccordionControlElement _propertyProfileCtrlElem;
        private DevExpress.XtraBars.Navigation.AccordionControlElement _registerUserCtrlElem;
        private DevExpress.XtraBars.Navigation.AccordionControlElement _userRegistrationDetailsCtrlElem;
        private DevExpress.XtraBars.Navigation.AccordionControlElement _compsCtrlElem;
        private DevExpress.XtraBars.Navigation.AccordionControlElement _floodCompsCtrlElem;
        private DevExpress.XtraBars.BarStaticItem barStaticItem1;
        private DevExpress.Utils.Behaviors.BehaviorManager behaviorManager1;
        private DevExpress.XtraBars.Navigation.AccordionContentContainer accordionContentContainer1;
        private DevExpress.XtraEditors.Controls.CalendarControl calendarControl1;
        private DevExpress.XtraBars.Navigation.AccordionControlElement accordionControlElement1;
        private DevExpress.XtraBars.Navigation.AccordionControlElement accordionControlElement2;
    }
}