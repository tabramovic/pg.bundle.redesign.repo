using System;
using System.Collections;
using System.Windows.Forms;

namespace WebManager
{
	#region � Class ListViewItemComparer �
	// Implements the manual sorting of items by columns.
	class ListViewItemComparer : IComparer
	{
		private int col;
		public ListViewItemComparer()
		{
			col = 0;
		}
		public ListViewItemComparer(int column)
		{
			col = column;
		}
		public int Compare(object x, object y)
		{
			return String.Compare(((ListViewItem)x).SubItems[col].Text, ((ListViewItem)y).SubItems[col].Text);
		}
	}
	#endregion � Class ListViewItemComparer �
	#region � Class ListViewItemComparerDesc �
	// Implements the manual sorting of items by columns.
	class ListViewItemComparerDesc : IComparer
	{
		private int col;
		public ListViewItemComparerDesc()
		{
			col = 0;
		}
		public ListViewItemComparerDesc(int column)
		{
			col = column;
		}
		public int Compare(object x, object y)
		{
			return String.Compare(((ListViewItem)y).SubItems[col].Text, ((ListViewItem)x).SubItems[col].Text);
		}
	}
	#endregion � Class ListViewItemComparerDesc �
}
