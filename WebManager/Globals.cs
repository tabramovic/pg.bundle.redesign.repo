using System;
using System.Text;
using System.Collections;

namespace WebManager
{
	public class Globals
	{
		public static bool TerminateThreads = false;
		public static string key = string.Empty;
		public static string tempMsg = string.Empty;
		public static ArrayList alKeys = new ArrayList();
		public static ArrayList alCnt = new ArrayList();
				
		//public static string URL_Registration = "http://195.29.139.219/WS_PG/ServiceRegistration.asmx";
		//public static string URL_DataComsumption = "http://195.29.139.219/WS_PG/ServiceDataConsumption.asmx";

		public static string URL_Registration = "http://www.profitgrabber.com/ws/ServiceRegistration.asmx";
		public static string URL_DataComsumption = "http://www.profitgrabber.com/ws/servicedataconsumption.asmx";

		public static byte[] last_req;

		public static string currentUser = string.Empty;
		public static string currentRegKey = string.Empty;
	}
}
