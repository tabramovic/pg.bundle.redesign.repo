﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebManager.WS_REG;

namespace WebManager.BLL
{
    

    public sealed class RegisteredUsers
    {
        private static RegisteredUsers _instance;

        List<ShortUserInfo> _shortUserInfoList = null;
        readonly Service1 ws = null;

        eSyncState synchState = eSyncState.NotInitialized;

        public delegate void DataRetrieved(List<ShortUserInfo> users);
        public event DataRetrieved OnDataRetrieved;
        public eSyncState SynchState { get { return synchState; } }

        public RegisteredUsers()
        {
            _shortUserInfoList = new List<ShortUserInfo>();
            ws = new Service1();
        }
        
        public static RegisteredUsers Instance
        {
            get
            {
                if (null == _instance)
                    _instance = new RegisteredUsers();

                return _instance;
            }
        }

        public string TestConnection()
        {            
            return ws.TestConnection();
        }

        async Task<List<ShortUserInfo>> GetAllShortUserInfo()
        {
            string msg = string.Empty;
            List<ShortUserInfo> shortUserInfoList = new List<ShortUserInfo>();

            await Task.Run(() => 
            {
                shortUserInfoList = ws.GetAllUsers("!aD0420", out msg).ToList();
            });

            return shortUserInfoList;
        }

        public async void StartSynchronizationAsync()
        {
            synchState = eSyncState.Synchronizing;
            _shortUserInfoList = await GetAllShortUserInfo();
            synchState = eSyncState.Synchronized;

            if (null != OnDataRetrieved)
                OnDataRetrieved(_shortUserInfoList);
        }
    }
}
