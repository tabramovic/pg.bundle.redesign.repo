﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebManager.BLL
{
    public enum eSyncState
    {
        /// <summary>
        /// Not started, unknown connection
        /// </summary>
        NotInitialized,

        /// <summary>
        /// Test Connection failed
        /// </summary>
        ConnectionFailed,

        /// <summary>
        /// Connection OK, ready for synchronization
        /// </summary>
        Ready,        

        /// <summary>
        /// Synchronizing
        /// </summary>
        Synchronizing,

        /// <summary>
        /// Synchronized
        /// </summary>
        Synchronized
    }
}
