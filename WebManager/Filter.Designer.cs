﻿namespace WebManager
{
    partial class Filter
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._filteredView = new System.Windows.Forms.ListView();
            this.columnHeader1 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader2 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader3 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader4 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader5 = new System.Windows.Forms.ColumnHeader();
            this._fn = new System.Windows.Forms.TextBox();
            this._mn = new System.Windows.Forms.TextBox();
            this._ln = new System.Windows.Forms.TextBox();
            this._ok = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this._cancel = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // _filteredView
            // 
            this._filteredView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this._filteredView.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._filteredView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader5});
            this._filteredView.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._filteredView.FullRowSelect = true;
            this._filteredView.GridLines = true;
            this._filteredView.Location = new System.Drawing.Point(12, 38);
            this._filteredView.MultiSelect = false;
            this._filteredView.Name = "_filteredView";
            this._filteredView.Size = new System.Drawing.Size(1008, 428);
            this._filteredView.TabIndex = 3;
            this._filteredView.UseCompatibleStateImageBehavior = false;
            this._filteredView.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "First Name";
            this.columnHeader1.Width = 164;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Middle Name";
            this.columnHeader2.Width = 123;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Last Name";
            this.columnHeader3.Width = 201;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "User ID";
            this.columnHeader4.Width = 301;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Act. Date";
            this.columnHeader5.Width = 189;
            // 
            // _fn
            // 
            this._fn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._fn.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._fn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._fn.Location = new System.Drawing.Point(12, 472);
            this._fn.Name = "_fn";
            this._fn.Size = new System.Drawing.Size(161, 26);
            this._fn.TabIndex = 0;
            // 
            // _mn
            // 
            this._mn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._mn.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._mn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._mn.Location = new System.Drawing.Point(179, 472);
            this._mn.Name = "_mn";
            this._mn.Size = new System.Drawing.Size(116, 26);
            this._mn.TabIndex = 1;
            // 
            // _ln
            // 
            this._ln.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._ln.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._ln.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._ln.Location = new System.Drawing.Point(301, 472);
            this._ln.Name = "_ln";
            this._ln.Size = new System.Drawing.Size(201, 26);
            this._ln.TabIndex = 2;
            // 
            // _ok
            // 
            this._ok.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._ok.DialogResult = System.Windows.Forms.DialogResult.OK;
            this._ok.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._ok.Location = new System.Drawing.Point(928, 472);
            this._ok.Name = "_ok";
            this._ok.Size = new System.Drawing.Size(92, 26);
            this._ok.TabIndex = 4;
            this._ok.Text = "OK";
            this._ok.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(9, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(382, 16);
            this.label1.TabIndex = 5;
            this.label1.Text = "Double click on list view to select filtered contact and close form";
            // 
            // _cancel
            // 
            this._cancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._cancel.DialogResult = System.Windows.Forms.DialogResult.OK;
            this._cancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._cancel.Location = new System.Drawing.Point(830, 472);
            this._cancel.Name = "_cancel";
            this._cancel.Size = new System.Drawing.Size(92, 26);
            this._cancel.TabIndex = 6;
            this._cancel.Text = "Cancel";
            this._cancel.UseVisualStyleBackColor = true;
            // 
            // Filter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1032, 510);
            this.Controls.Add(this._cancel);
            this.Controls.Add(this.label1);
            this.Controls.Add(this._ok);
            this.Controls.Add(this._ln);
            this.Controls.Add(this._mn);
            this.Controls.Add(this._fn);
            this.Controls.Add(this._filteredView);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Filter";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Filter contacts";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView _filteredView;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.TextBox _fn;
        private System.Windows.Forms.TextBox _mn;
        private System.Windows.Forms.TextBox _ln;
        private System.Windows.Forms.Button _ok;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button _cancel;
        private System.Windows.Forms.ColumnHeader columnHeader5;
    }
}