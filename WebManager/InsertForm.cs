﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace WebManager
{
    public partial class InsertForm : Form
    {
        public string InfusionID
        {
            get {
                return _infusionId.Text; 
            }
        }

        public string PGId
        {
            get { return _pgId.Text; }
        }

        public InsertForm()
        {
            InitializeComponent();
        }
    }
}
