﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

//Added
using WebManager.WSEnhancements;

namespace WebManager
{
    public partial class AccountDetails : Form
    {
        public AccountDetails()
        {
            InitializeComponent();
            AddEventHandlers();
        }

        void AddEventHandlers()
        {
            _close.Click += new EventHandler(_close_Click);    
        }

        void _close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public void ShowData(ReturnObject[] inputs, ReturnObject[] topUps)
        {
            if (null != inputs)
            {
                foreach (ReturnObject ro in inputs)
                {
                    ListViewItem lvi = new ListViewItem();
                    lvi.Text = ro.TimeStamp;
                    lvi.SubItems.Add(ro.Val);

                    _inputs.Items.Add(lvi);
                }
            }

            if (null != topUps)
            {
                foreach (ReturnObject ro in topUps)
                {
                    ListViewItem lvi = new ListViewItem();
                    lvi.Text = ro.TimeStamp;
                    lvi.SubItems.Add(ro.Val);

                    _topUps.Items.Add(lvi);
                }
            }
        }
    }
}
