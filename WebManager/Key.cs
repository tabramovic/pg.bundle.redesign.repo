using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace WebManager
{
	/// <summary>
	/// Summary description for Key.
	/// </summary>
	public class Key : System.Windows.Forms.Form
	{
		public string SerialKey = string.Empty;
		private System.Windows.Forms.TextBox tb1;
		private System.Windows.Forms.TextBox tb2;
		private System.Windows.Forms.TextBox tb3;
		private System.Windows.Forms.TextBox tb4;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Button bOK;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public Key()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.tb1 = new System.Windows.Forms.TextBox();
			this.tb2 = new System.Windows.Forms.TextBox();
			this.tb3 = new System.Windows.Forms.TextBox();
			this.tb4 = new System.Windows.Forms.TextBox();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.bOK = new System.Windows.Forms.Button();
			this.groupBox1.SuspendLayout();
			this.SuspendLayout();
			// 
			// tb1
			// 
			this.tb1.Location = new System.Drawing.Point(16, 24);
			this.tb1.MaxLength = 4;
			this.tb1.Name = "tb1";
			this.tb1.Size = new System.Drawing.Size(56, 20);
			this.tb1.TabIndex = 0;
			this.tb1.Text = "";
			this.tb1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// tb2
			// 
			this.tb2.Location = new System.Drawing.Point(88, 24);
			this.tb2.MaxLength = 4;
			this.tb2.Name = "tb2";
			this.tb2.Size = new System.Drawing.Size(56, 20);
			this.tb2.TabIndex = 1;
			this.tb2.Text = "";
			this.tb2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// tb3
			// 
			this.tb3.Location = new System.Drawing.Point(160, 24);
			this.tb3.MaxLength = 4;
			this.tb3.Name = "tb3";
			this.tb3.Size = new System.Drawing.Size(56, 20);
			this.tb3.TabIndex = 2;
			this.tb3.Text = "";
			this.tb3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// tb4
			// 
			this.tb4.Location = new System.Drawing.Point(232, 24);
			this.tb4.MaxLength = 4;
			this.tb4.Name = "tb4";
			this.tb4.Size = new System.Drawing.Size(56, 20);
			this.tb4.TabIndex = 3;
			this.tb4.Text = "";
			this.tb4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.tb1);
			this.groupBox1.Controls.Add(this.tb2);
			this.groupBox1.Controls.Add(this.tb3);
			this.groupBox1.Controls.Add(this.tb4);
			this.groupBox1.Location = new System.Drawing.Point(8, 8);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(304, 64);
			this.groupBox1.TabIndex = 4;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Reg Key";
			// 
			// bOK
			// 
			this.bOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.bOK.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.bOK.Location = new System.Drawing.Point(240, 80);
			this.bOK.Name = "bOK";
			this.bOK.TabIndex = 5;
			this.bOK.Text = "OK";
			this.bOK.Click += new System.EventHandler(this.bOK_Click);
			// 
			// Key
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(328, 118);
			this.Controls.Add(this.bOK);
			this.Controls.Add(this.groupBox1);
			this.Name = "Key";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Key";
			this.groupBox1.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		private void bOK_Click(object sender, System.EventArgs e)
		{
			string key = string.Empty;
			key += this.tb1.Text + "-";
			key += this.tb2.Text + "-";
			key += this.tb3.Text + "-";
			key += this.tb4.Text;

			this.SerialKey = key.ToUpper();
			this.Close();			
		}
	}
}
