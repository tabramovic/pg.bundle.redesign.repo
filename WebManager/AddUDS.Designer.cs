﻿namespace WebManager
{
    partial class AddUDS
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this._setInputs = new System.Windows.Forms.Button();
            this._date = new System.Windows.Forms.DateTimePicker();
            this._months = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this._amount = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this._setTopUps = new System.Windows.Forms.Button();
            this._topUps = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this._close = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._amount)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._topUps)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this._setInputs);
            this.groupBox1.Controls.Add(this._date);
            this.groupBox1.Controls.Add(this._months);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this._amount);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(539, 231);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Inputs";
            // 
            // _setInputs
            // 
            this._setInputs.Location = new System.Drawing.Point(362, 34);
            this._setInputs.Name = "_setInputs";
            this._setInputs.Size = new System.Drawing.Size(75, 23);
            this._setInputs.TabIndex = 5;
            this._setInputs.Text = "Set Inputs";
            this._setInputs.UseVisualStyleBackColor = true;
            // 
            // _date
            // 
            this._date.Location = new System.Drawing.Point(146, 34);
            this._date.Name = "_date";
            this._date.Size = new System.Drawing.Size(200, 20);
            this._date.TabIndex = 4;
            // 
            // _months
            // 
            this._months.FormattingEnabled = true;
            this._months.Items.AddRange(new object[] {
            "1",
            "3",
            "6",
            "12"});
            this._months.Location = new System.Drawing.Point(59, 75);
            this._months.Name = "_months";
            this._months.Size = new System.Drawing.Size(66, 21);
            this._months.TabIndex = 3;
            this._months.Text = "1";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 78);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(45, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Months:";
            // 
            // _amount
            // 
            this._amount.Location = new System.Drawing.Point(59, 34);
            this._amount.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this._amount.Name = "_amount";
            this._amount.Size = new System.Drawing.Size(66, 20);
            this._amount.TabIndex = 1;
            this._amount.Value = new decimal(new int[] {
            500,
            0,
            0,
            0});
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Amount:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this._setTopUps);
            this.groupBox2.Controls.Add(this._topUps);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Location = new System.Drawing.Point(12, 249);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(539, 94);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "TopUps";
            // 
            // _setTopUps
            // 
            this._setTopUps.Location = new System.Drawing.Point(146, 29);
            this._setTopUps.Name = "_setTopUps";
            this._setTopUps.Size = new System.Drawing.Size(75, 23);
            this._setTopUps.TabIndex = 8;
            this._setTopUps.Text = "Set TopUps";
            this._setTopUps.UseVisualStyleBackColor = true;
            // 
            // _topUps
            // 
            this._topUps.Location = new System.Drawing.Point(59, 29);
            this._topUps.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this._topUps.Name = "_topUps";
            this._topUps.Size = new System.Drawing.Size(66, 20);
            this._topUps.TabIndex = 7;
            this._topUps.Value = new decimal(new int[] {
            500,
            0,
            0,
            0});
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 29);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(46, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Amount:";
            // 
            // _close
            // 
            this._close.Location = new System.Drawing.Point(476, 356);
            this._close.Name = "_close";
            this._close.Size = new System.Drawing.Size(75, 23);
            this._close.TabIndex = 9;
            this._close.Text = "Close";
            this._close.UseVisualStyleBackColor = true;
            // 
            // AddUDS
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(563, 391);
            this.Controls.Add(this._close);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "AddUDS";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Add UDS Monthly Credits";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._amount)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._topUps)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button _setInputs;
        private System.Windows.Forms.DateTimePicker _date;
        private System.Windows.Forms.ComboBox _months;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown _amount;
        private System.Windows.Forms.Button _setTopUps;
        private System.Windows.Forms.NumericUpDown _topUps;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button _close;
    }
}