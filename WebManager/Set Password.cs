﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace WebManager
{
    public partial class Set_Password : Form
    {
        public Set_Password()
        {
            InitializeComponent();
            _close.Click += new EventHandler(on_close_Click);
        }

        void on_close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public string Password
        {
            get { return _pwd.Text; }
        }
    }
}
