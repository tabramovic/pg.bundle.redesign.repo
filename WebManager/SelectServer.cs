using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace WebManager
{
	/// <summary>
	/// Summary description for SelectServer.
	/// </summary>
	public class SelectServer : System.Windows.Forms.Form
	{
		private System.Windows.Forms.ComboBox serverList;
		private System.Windows.Forms.Button ok;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public string server
		{
			get
			{
				return serverList.Text;
			}
		}

		public SelectServer()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();


            serverList.Items.Add("http://www.profitgrabber.com/ws");
            serverList.Items.Add("http://www.profitgrabber.com:10080/WS");
			
			

			serverList.Text = (string)serverList.Items[0];

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.serverList = new System.Windows.Forms.ComboBox();
			this.ok = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// serverList
			// 
			this.serverList.Location = new System.Drawing.Point(8, 8);
			this.serverList.Name = "serverList";
			this.serverList.Size = new System.Drawing.Size(272, 21);
			this.serverList.TabIndex = 0;
			this.serverList.Text = "comboBox1";
			// 
			// ok
			// 
			this.ok.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.ok.Location = new System.Drawing.Point(200, 40);
			this.ok.Name = "ok";
			this.ok.TabIndex = 1;
			this.ok.Text = "OK";
			// 
			// SelectServer
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(282, 71);
			this.ControlBox = false;
			this.Controls.Add(this.ok);
			this.Controls.Add(this.serverList);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "SelectServer";
			this.Text = "SelectServer";
			this.ResumeLayout(false);

		}
		#endregion
	}
}
