﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace WebManager
{
    public partial class UpdateForm : Form
    {
        public string InfusionId {
            get { return _tbInfusionId.Text; }
        }

        public UpdateForm()
        {
            InitializeComponent();
        }
    }
}
