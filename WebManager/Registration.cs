//------------------------------------------------------------------------------
// <autogenerated>
//     This code was generated by NHibernate.
//     Runtime Version: v1.1.4322
//
//     Changes to this file may cause incorrect behavior and will be lost if 
//     the code is regenerated.
// </autogenerated>
//------------------------------------------------------------------------------

using System;

namespace WebManager.DataConsumption.DataClasses
{

 /// <summary>
 /// POJO for Registration
 /// </summary>
 /// <remark>
 /// This class is autogenerated
 /// </remark>
 [Serializable]
 public class Registration
 {

	#region Fields

	/// <summary>
	/// Holder for KeyCode
	/// </summary>
	private String keyCode;
	

	
	#endregion

	#region Properties

	/// <summary>
	/// Get/set for KeyCode
	/// </summary>
	public String KeyCode
	{
		get
		{
			return this.keyCode;
		}
		set
		{
			this.keyCode = value;
		}
	}
	

	
	#endregion
 }
}