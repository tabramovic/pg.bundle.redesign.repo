﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using Google.Apis.Sheets.v4;
using Google.Apis.Sheets.v4.Data;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Services;


namespace ProfitGrabber.Sheets.Repo
{
    public static class SheetsServiceExtensions
    {
        public static SheetsService Get(this UserCredential credential, string applicationName)
        {
            return new SheetsService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = applicationName,
            });
        }

        public static async Task<string> Create(this SheetsService sheetsService, string spreadSheetTitleHeader, string dataSheetTitle)
        {
            var requestBody = new Spreadsheet
            {
                Properties = new SpreadsheetProperties
                {
                    Title = $"{spreadSheetTitleHeader}"
                }
            };

            var dataSheet = new Sheet
            {
                Properties = new SheetProperties
                {
                    Title = dataSheetTitle
                }
            };

            requestBody.Sheets = new List<Sheet> { dataSheet };
            SpreadsheetsResource.CreateRequest request = sheetsService.Spreadsheets.Create(requestBody);

            Spreadsheet response = await request.ExecuteAsync();
            return response.SpreadsheetId;
        }

        public static async Task<AppendValuesResponse> Append(this SheetsService sheetsService, string spreadsheetId, string dataSheetTitle, IList<IList<object>> data)
        {
            if (null == data?[0] || 0 == data?[0].Count)
                throw new ArgumentNullException();

            var maxRange = data[0].Count;

            // The A1 notation of a range to search for a logical table of data.            
            string range = $"{dataSheetTitle}!A1:A{maxRange}";

            SpreadsheetsResource.ValuesResource.AppendRequest.ValueInputOptionEnum valueInputOption =
                SpreadsheetsResource.ValuesResource.AppendRequest.ValueInputOptionEnum.RAW;

            SpreadsheetsResource.ValuesResource.AppendRequest.InsertDataOptionEnum insertDataOption =
                SpreadsheetsResource.ValuesResource.AppendRequest.InsertDataOptionEnum.INSERTROWS;

            ValueRange requestBody = new ValueRange
            {
                Range = range,
                Values = data
            };

            SpreadsheetsResource.ValuesResource.AppendRequest request = sheetsService.Spreadsheets.Values.Append(requestBody, spreadsheetId, range);
            request.ValueInputOption = valueInputOption;
            request.InsertDataOption = insertDataOption;

            AppendValuesResponse response = await request.ExecuteAsync();

            return response;
        }
    }
}
