﻿using Google.Apis.Auth.OAuth2;
using Google.Apis.Sheets.v4;
using Google.Apis.Sheets.v4.Data;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace ProfitGrabber.Sheets.Repo
{
    public class GoogleSheetsClient
    {
        static string[] Scopes = { SheetsService.Scope.Spreadsheets };
        static string ApplicationName = "ProfitGrabber Pro";
        public async Task<AppendValuesResponse> CreateDocument(string clientId, string clientSecret, string spreadSheetTitle, string dataSheetTitle, IList<IList<object>> data, string emailAddress)
        {
            var secrets = new ClientSecrets()
            {
                ClientId = clientId,
                ClientSecret = clientSecret
            };
            
            //UserCredential uc = UserCredentialFactory.Get(); --NEW
            UserCredential uc = await GoogleWebAuthorizationBroker.AuthorizeAsync(secrets, Scopes, emailAddress, CancellationToken.None);
            
            var sheetsService = uc.Get(ApplicationName);

            var createdSpreadSheetId = await sheetsService.Create(spreadSheetTitle, dataSheetTitle);
            var appendValueResponse = await sheetsService.Append(createdSpreadSheetId, dataSheetTitle, data);

            return appendValueResponse;
        }
    }
}
