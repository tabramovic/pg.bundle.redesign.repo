﻿using Newtonsoft.Json;
using System.Net.Http;
using System.Text;

namespace ProfitGrabber.RVM.Extensions
{
    public static class ContentExtensions
    {
        public static StringContent AsJSON(this object o)
            => new StringContent(JsonConvert.SerializeObject(o), Encoding.UTF8, "application/json");

        public static string AsJWTToken(this string token)
            => $"JWT {token}";
    }
}
