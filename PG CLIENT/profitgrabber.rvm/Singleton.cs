﻿using System;
using System.Reflection;
using System.Threading;

namespace ProfitGrabber.RVM
{

    public class Singleton<T> where T : class
    {
        private static readonly Lazy<T>
            Lazy =
                new Lazy<T>
                    (() =>
                    {

                        var constructor = typeof(T).GetConstructor(BindingFlags.Instance | BindingFlags.NonPublic, null, new Type[0], null);

                        if (constructor == null || constructor.IsAssembly)
                            throw new InvalidOperationException(
                                "Constructor is not private or protected for type " + typeof(T).Name);

                        return (T)constructor.Invoke(null);
                    }
                    , LazyThreadSafetyMode.ExecutionAndPublication
                );

        public static T Instance => Lazy.Value;

        public Singleton()
        {
        }
    }
}