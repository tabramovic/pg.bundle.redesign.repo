﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using ProfitGrabber.RVM.DropCowboy.DTO.Response.GetAllLists;

namespace ProfitGrabber.RVM.DropCowboy.UOW
{
    public static class GetAllLists
    {
        public static async Task<GetAllListsResponse[]> Execute(string teamId, string teamSecret)
        {
            using (var client = new HttpClient())
            {
                var url = $"{DCConsts.DropCowboyListURL}";

                try
                {
                    client.DefaultRequestHeaders.Add("x-team-id", teamId);
                    client.DefaultRequestHeaders.Add("x-secret", teamSecret);

                    var response = await client.GetAsync(url);

                    response.EnsureSuccessStatusCode();
                    var res = await response.Content.ReadAsStringAsync();

                    //string unescapedString = JsonConvert.DeserializeObject<GetAllListsResponse[]>(res);

                    return GetAllListsResponse.FromJson(res);
                }
                catch (HttpRequestException hre)
                {
                    //log error
                    throw;
                }
                catch (Exception e)
                {
                    //log error
                    throw;
                }
            }
        }
    }
}
