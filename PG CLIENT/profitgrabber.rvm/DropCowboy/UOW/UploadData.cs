﻿using ProfitGrabber.RVM.DropCowboy.DTO.Request.AddContactsToList;
using ProfitGrabber.RVM.Extensions;
using ProfitGrabber.RVM.DropCowboy.DTO.Response.AddContactsToList;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace ProfitGrabber.RVM.DropCowboy.UOW
{
    public static class UploadData
    {
        public static async Task<AddContactsToListResponse> Execute(string teamId, string teamSecret, string listId, AddContactToListRequest req)
        {
            using (var client = new HttpClient())
            {
                var url = $"{DCConsts.DropCowboyListURL}/{listId}/contacts";

                try
                {
                    client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                    client.DefaultRequestHeaders.Add("x-team-id", teamId);
                    client.DefaultRequestHeaders.Add("x-secret", teamSecret);

                    var response = await client.PostAsync(url, req.AsJSON());

                    response.EnsureSuccessStatusCode();
                    var res = await response.Content.ReadAsStringAsync();

                    return AddContactsToListResponse.FromJson(res);
                }
                catch (HttpRequestException hre)
                {
                    //log error
                    throw;
                }
                catch (Exception e)
                {
                    //log error
                    throw;
                }
            }
        }
    }    
}
