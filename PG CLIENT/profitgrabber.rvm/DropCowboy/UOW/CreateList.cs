﻿using ProfitGrabber.RVM.DropCowboy.DTO.Request.CreateList;
using ProfitGrabber.RVM.DropCowboy.DTO.Response.CreateList;
using ProfitGrabber.RVM.Extensions;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace ProfitGrabber.RVM.DropCowboy.UOW
{
    public static class CreateList
    {
        public static async Task<CreateListResponse> Execute(string teamId, string teamSecret, string listName)
        {
            var createListRequest = new CreateListRequest
            {
                ListName = listName
            };

            using (var client = new HttpClient())
            {
                var url = $"{DCConsts.DropCowboyListURL}";

                try
                {
                    client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                    client.DefaultRequestHeaders.Add("x-team-id", teamId);
                    client.DefaultRequestHeaders.Add("x-secret", teamSecret);

                    var response = await client.PostAsync(url, createListRequest.AsJSON());

                    response.EnsureSuccessStatusCode();
                    var res = await response.Content.ReadAsStringAsync();

                    return DropCowboy.DTO.Response.CreateList.CreateListResponse.FromJson(res);
                }
                catch (HttpRequestException hre)
                {
                    //log error
                    throw;
                }
                catch (Exception e)
                {
                    //log error
                    throw;
                }
            }
        }
    }
}
