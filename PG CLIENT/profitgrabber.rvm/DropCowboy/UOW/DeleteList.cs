﻿using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace ProfitGrabber.RVM.DropCowboy.UOW
{
    public static class DeleteList
    {
        public static async Task<bool> Execute(string teamId, string teamSecret, string listId)
        {            
            using (var client = new HttpClient())
            {
                var url = $"{DCConsts.DropCowboyListURL}/{listId}";

                try
                {
                    client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                    client.DefaultRequestHeaders.Add("x-team-id", teamId);
                    client.DefaultRequestHeaders.Add("x-secret", teamSecret);

                    var response = await client.DeleteAsync(url);

                    response.EnsureSuccessStatusCode();
                    var res = await response.Content.ReadAsStringAsync();

                    return true;
                }
                catch (HttpRequestException hre)
                {
                    //log error
                    throw;
                }
                catch (Exception e)
                {
                    //log error
                    throw;
                }
            }
        }
    }
}
