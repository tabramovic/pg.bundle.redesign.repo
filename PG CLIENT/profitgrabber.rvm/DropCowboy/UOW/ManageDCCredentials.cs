﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProfitGrabber.RVM.DropCowboy.UOW
{    
    public class ManageDCCredentials
    {
        const string FileName = "dc.dat";

        public static bool Save(string path, string[] lines)
        {
            try
            {
                File.WriteAllLines($"{path}{FileName}", lines);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static string[] Load(string path)
        {
            try
            {
                var lines = File.ReadAllLines($"{path}{FileName}");

                if (2 == lines.Count())
                    return lines;

                else 
                    return null;
            }
            catch
            {
                return null;
            }
        }
    }
}
