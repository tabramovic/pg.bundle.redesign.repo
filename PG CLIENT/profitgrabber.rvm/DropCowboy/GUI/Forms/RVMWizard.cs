﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using System.Windows.Forms;
using ProfitGrabber.RVM.DropCowboy.DTO.Response.GetAllLists;
using ProfitGrabber.RVM.DropCowboy.GUI.Forms;
using System.Diagnostics;
using ProfitGrabber.RVM.DropCowboy.UOW;
using System.Linq;

namespace ProfitGrabber.RVM.DropCowboy.Forms
{
    public partial class RVMWizard : Form
    {        
        private bool _ascName = true;
        private bool _ascTime = true;
        private string _teamId { get; set; }
        private string _secret { get; set; }
        public ProfitGrabber.RVM.DropCowboy.DTO.Request.AddContactsToList.AddContactToListRequest Request { get; set; }
        private string GroupName { get; }
        public string CredentialsPath { get; }

        public RVMWizard(string groupName, string credentialsPath)
        {
            InitializeComponent();

            AddEventHandlers();

            this.Text = "DROPCOWBOY Ringless Voicemail and Text Message Wizard";
            GroupName = groupName;
            CredentialsPath = credentialsPath;
        }
        
        private void AddEventHandlers()
        {
            bLogIn.Click += BLogIn_Click;
            bFinish.Click += BFinish_Click;
            bCreateAccount.Click += BCreateAccount_Click;
            _listsLv.ColumnClick += _listsLv_ColumnClick;
            _createNewList.Click += _createNewList_Click;
            _renameList.Click += _renameList_Click;
            _deleteList.Click += _deleteList_Click;
            _uploadData.Click += _uploadData_Click;
            _uploadGroup.Click += _uploadGroup_Click;
            _sendTextOrRVM.Click += _sendTextOrRVM_Click;
        }

        private void _sendTextOrRVM_Click(object sender, EventArgs e)
        {
            Process.Start(DCConsts.DCLogInURL);
        }

        protected override async void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            var res = ManageDCCredentials.Load(CredentialsPath);

            if (null != res && 2 == res.Length)
            {
                _teamId = _teamIdTb.Text = res[0];
                _secret = _teamSecretTb.Text = res[1];

                try {
                    await UpdateLists();
                    var createdListId = await CreateOrUpdateList(true, GroupName);
                    var uploadRes = await UOW.UploadData.Execute(_teamId, _secret, createdListId.ToString(), Request);
                    await UpdateLists();

                    if (null != uploadRes)
                        MessageBox.Show($"There were {uploadRes.AcceptedCount} of {Request.Values.Length} contacts with phone numbers.{Environment.NewLine}{uploadRes.AcceptedCount} contacts were accepted by Drop Cowboy.{Environment.NewLine}{Environment.NewLine}To send Text or RVM to this group, login to your Drop Cowboy account at DropCowboy.com or right click on the Group in Profit Grabber and select Send Text/RVM from the menu.", "ProfitGrabber Pro", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }    //for HTTP exceptions
                catch { }
            }            
        }

        private void BCreateAccount_Click(object sender, EventArgs e)
        {
            Process.Start(DCConsts.OptInURL);
        }

        private async void _uploadData_Click(object sender, EventArgs e)
        {
            if (1 == _listsLv.SelectedItems.Count && !string.IsNullOrEmpty(_listsLv?.SelectedItems[0]?.Text))
            {
                if (DialogResult.Yes == MessageBox.Show($"Are you sure you want to assign {Request.Values.Length} contacts to {_listsLv?.SelectedItems[0]?.Text} list?", "ProfitGrabber Pro", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Information, MessageBoxDefaultButton.Button2))
                {
                    try
                    {
                        var res = await UOW.UploadData.Execute(_teamId, _secret, ((GetAllListsResponse)(_listsLv?.SelectedItems[0].Tag)).ListId.ToString(), Request);
                        await UpdateLists();
                        MessageBox.Show($"Service accepted {res.AcceptedCount}/{Request.Values.Length} records.", "ProfitGrabber Pro", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    catch (HttpRequestException hre)
                    {
                        //log error
                        MessageBox.Show("Incorrect DropCowboy team id-secret pair.", "ProfitGrabber Pro", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        _listsLv.Enabled = false;
                        throw;
                    }
                    catch (Exception exc)
                    {
                        //log error
                        MessageBox.Show(exc.Message, "Communication failure", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        throw;
                    }
                }
            }
            else
                MessageBox.Show("Select list to assingn contacts to.", "ProfitGrabber Pro", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private async void _deleteList_Click(object sender, EventArgs e)
        {
            if (1 == _listsLv.SelectedItems.Count && !string.IsNullOrEmpty(_listsLv?.SelectedItems[0]?.Text))
            {
                if (DialogResult.Yes == MessageBox.Show($"Are you sure you want to delete {_listsLv?.SelectedItems[0]?.Text} list?", "ProfitGrabber Pro", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Information, MessageBoxDefaultButton.Button2))
                {
                    try
                    {
                        await UOW.DeleteList.Execute(_teamId, _secret, ((GetAllListsResponse)(_listsLv?.SelectedItems[0].Tag)).ListId.ToString());
                        await UpdateLists();
                    }
                    catch (HttpRequestException hre)
                    {
                        //log error
                        MessageBox.Show("Incorrect DropCowboy team id-secret pair.", "ProfitGrabber Pro", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        _listsLv.Enabled = false;
                        throw;
                    }
                    catch (Exception exc)
                    {
                        //log error
                        MessageBox.Show(exc.Message, "Communication failure", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        throw;
                    }
                }
            }
            else
                MessageBox.Show("Select list to delete.", "ProfitGrabber Pro", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private async void _renameList_Click(object sender, EventArgs e)
        {
            if (1 == _listsLv.SelectedItems.Count && !string.IsNullOrEmpty(_listsLv?.SelectedItems[0]?.Text))
            {
                var addOrUpdateListForm = new AddOrUpdateList(false, _listsLv?.SelectedItems[0]?.Text);
                if (DialogResult.OK == addOrUpdateListForm.ShowDialog())
                {
                    var newListName = addOrUpdateListForm.ListName;
                    try
                    {
                        await UOW.RenameList.Execute(_teamId, _secret, newListName, ((GetAllListsResponse)(_listsLv?.SelectedItems[0].Tag)).ListId.ToString());
                        await UpdateLists();
                    }
                    catch (HttpRequestException hre)
                    {
                        //log error
                        MessageBox.Show("Incorrect DropCowboy team id-secret pair.", "ProfitGrabber Pro", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        _listsLv.Enabled = false;
                        throw;
                    }
                    catch (Exception exc)
                    {
                        //log error
                        MessageBox.Show(exc.Message, "Communication failure", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        throw;
                    }
                }
            }
            else
                MessageBox.Show("Select list to rename.", "ProfitGrabber Pro", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private async void _uploadGroup_Click(object sender, EventArgs e)
        {
            var createdListId = await CreateOrUpdateList(true, GroupName);

            if (null != createdListId)
            {
                try
                {
                    var res = await UOW.UploadData.Execute(_teamId, _secret, createdListId.ToString(), Request);
                    await UpdateLists();
                    MessageBox.Show($"Service accepted {res.AcceptedCount}/{Request.Values.Length} contacts.", "ProfitGrabber Pro", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Process.Start(DCConsts.DCLogInURL);
                }
                catch (HttpRequestException hre)
                {
                    //log error
                    MessageBox.Show("Incorrect DropCowboy team id-secret pair.", "ProfitGrabber Pro", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    _listsLv.Enabled = false;
                    throw;
                }
                catch (Exception exc)
                {
                    //log error
                    MessageBox.Show(exc.Message, "Communication failure", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    throw;
                }
            }


        }

        private async void _createNewList_Click(object sender, EventArgs e)
        {
            await CreateOrUpdateList(true);            
        }
        
        private async void BLogIn_Click(object sender, EventArgs e)
        {
            _teamId = _teamIdTb.Text;
            _secret = _teamSecretTb.Text;

            try 
            {
                var res = ManageDCCredentials.Save(CredentialsPath, new string[] { _teamId, _secret });
                //await UpdateLists();
                var createdListId = await CreateOrUpdateList(true, GroupName);
                var uploadRes = await UOW.UploadData.Execute(_teamId, _secret, createdListId.ToString(), Request);
                await UpdateLists();

                if (null != uploadRes)
                    MessageBox.Show($"There were {uploadRes.AcceptedCount} of {Request.Values.Length} contacts with phone numbers.{Environment.NewLine}{uploadRes.AcceptedCount} contacts were accepted by Drop Cowboy.{Environment.NewLine}{Environment.NewLine}To send Text or RVM to this group, login to your Drop Cowboy account at DropCowboy.com or right click on the Group in Profit Grabber and select Send Text/RVM from the menu.", "ProfitGrabber Pro", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }    
            catch { }   //for HTTP exceptions
        }
                
        private void _listsLv_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            SortLists(e.Column);
        }
       
        private void BFinish_Click(object sender, EventArgs e)
        {
            if (DialogResult.Yes == MessageBox.Show("Are you sure you want to exit Ringless Voice Message Wizard?", "ProfitGrabber Pro", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2))
                this.Close();
        }

        void SortLists(int columnIdx)
        {
            if (0 == columnIdx)
            {
                _ascName = !_ascName;
                _listsLv.ListViewItemSorter = new DCListsComparer(0, _ascName);
            }
            else
            {
                _ascTime = !_ascTime;
                _listsLv.ListViewItemSorter = new DCListsComparer(1, _ascTime);
            }

            _listsLv.Sort();
        }

        private async Task UpdateLists()
        {
            try
            {
                var allLists = await DropCowboy.UOW.GetAllLists.Execute(_teamId, _secret);                
                UpdateCampaignsListView(new List<GetAllListsResponse>(allLists).OrderByDescending(i => i.CreatedAt).ToArray());
                _listsLv.Enabled = true;
            }
            catch (HttpRequestException hre)
            {
                //log error
                MessageBox.Show("Incorrect DropCowboy team id-secret pair.", "ProfitGrabber Pro", MessageBoxButtons.OK, MessageBoxIcon.Information);
                _listsLv.Enabled = false;
                throw;
            }
            catch (Exception exc)
            {
                //log error
                MessageBox.Show(exc.Message, "Communication failure", MessageBoxButtons.OK, MessageBoxIcon.Information);
                throw;
            }
        }

        async Task<Guid?> CreateOrUpdateList(bool create, string listName = null)
        {
            var addListForm = new AddOrUpdateList(create, listName);

            if (DialogResult.OK == addListForm.ShowDialog())
            {
                try
                {
                    var res = await RVM.DropCowboy.UOW.CreateList.Execute(_teamId, _secret, addListForm.ListName);
                    //await UpdateLists();
                    return res.ListId;
                }
                catch (HttpRequestException hre)
                {
                    //log error
                    MessageBox.Show("Incorrect DropCowboy team id-secret pair.", "ProfitGrabber Pro", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    _listsLv.Enabled = false;
                    return null;
                }
                catch (Exception exc)
                {
                    //log error
                    MessageBox.Show(exc.Message, "Communication failure", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return null;
                }
            }

            return null;
        }

        private delegate void UpdateCampaignsListViewAsync(GetAllListsResponse[] allLists);
        private void UpdateCampaignsListView(GetAllListsResponse[] allLists)
        {
            if (InvokeRequired)
                BeginInvoke(new UpdateCampaignsListViewAsync(UpdateCampaignsListView), new object[] { allLists });

            _listsLv.Items.Clear();

            var lvis = new List<ListViewItem>();
            foreach (var list in allLists)
            {
                var lvi = new ListViewItem(list.ListName);

                var createdAt = FromUnixTime(list.CreatedAt);                                                
                lvi.SubItems.Add(createdAt.ToString());
                lvi.SubItems.Add(list.ContactCount.ToString());

                lvi.Tag = list;
                lvis.Add(lvi);
            }

            _listsLv.Items.AddRange(lvis.ToArray());            
        }

        public static DateTime FromUnixTime(long unixTime)
        {
            return epoch.AddMilliseconds(unixTime);
        }
        private static readonly DateTime epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
    }

    internal class DCListsComparer : IComparer
    {
        bool _asc;
        int _idx;

        public DCListsComparer(int idx, bool asc)
        {
            _idx = idx;
            _asc = asc;
        }

        public int Compare(object x, object y)
        {
            int res;

            ListViewItem X = x as ListViewItem;
            ListViewItem Y = y as ListViewItem;

            GetAllListsResponse A = X.Tag as GetAllListsResponse;
            GetAllListsResponse B = Y.Tag as GetAllListsResponse;

            if (1 == _idx)
                res = A.CreatedAt.CompareTo(B.CreatedAt);
            else
                res = A.ListName.CompareTo(B.ListName);

            if (_asc)
                return res;
            else
                return -res;
        }
    }
}
