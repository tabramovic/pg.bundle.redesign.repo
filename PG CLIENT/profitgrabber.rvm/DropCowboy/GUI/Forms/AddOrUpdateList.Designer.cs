﻿namespace ProfitGrabber.RVM.DropCowboy.GUI.Forms
{
    partial class AddOrUpdateList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this._listName = new WindowsApplication.AcquisitionCalculator.TextBox();
            this._headerLbl = new System.Windows.Forms.Label();
            this._okBtn = new System.Windows.Forms.Button();
            this._cancelBtn = new System.Windows.Forms.Button();
            this._header2Lbl = new System.Windows.Forms.Label();
            this._header3Lbl = new System.Windows.Forms.Label();
            this._header4Lbl = new System.Windows.Forms.Label();
            this._header2aLbl = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(31, 194);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 20);
            this.label1.TabIndex = 4;
            this.label1.Text = "Name:";
            // 
            // _listName
            // 
            this._listName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._listName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._listName.Location = new System.Drawing.Point(103, 191);
            this._listName.Name = "_listName";
            this._listName.Size = new System.Drawing.Size(518, 26);
            this._listName.TabIndex = 3;
            // 
            // _headerLbl
            // 
            this._headerLbl.AutoSize = true;
            this._headerLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._headerLbl.Location = new System.Drawing.Point(13, 9);
            this._headerLbl.Name = "_headerLbl";
            this._headerLbl.Size = new System.Drawing.Size(606, 20);
            this._headerLbl.TabIndex = 5;
            this._headerLbl.Text = "Choose a name that correlates well with the Profit Grabber group you are uploadin" +
    "g.  ";
            // 
            // _okBtn
            // 
            this._okBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._okBtn.DialogResult = System.Windows.Forms.DialogResult.OK;
            this._okBtn.Location = new System.Drawing.Point(465, 241);
            this._okBtn.Name = "_okBtn";
            this._okBtn.Size = new System.Drawing.Size(75, 23);
            this._okBtn.TabIndex = 6;
            this._okBtn.Text = "OK";
            this._okBtn.UseVisualStyleBackColor = true;
            // 
            // _cancelBtn
            // 
            this._cancelBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._cancelBtn.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this._cancelBtn.Location = new System.Drawing.Point(546, 241);
            this._cancelBtn.Name = "_cancelBtn";
            this._cancelBtn.Size = new System.Drawing.Size(75, 23);
            this._cancelBtn.TabIndex = 7;
            this._cancelBtn.Text = "Cancel";
            this._cancelBtn.UseVisualStyleBackColor = true;
            // 
            // _header2Lbl
            // 
            this._header2Lbl.AutoSize = true;
            this._header2Lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._header2Lbl.Location = new System.Drawing.Point(13, 39);
            this._header2Lbl.Name = "_header2Lbl";
            this._header2Lbl.Size = new System.Drawing.Size(616, 20);
            this._header2Lbl.TabIndex = 8;
            this._header2Lbl.Text = "You may want to add parent groups to the name also to better correlate, similar t" +
    "o this:";
            // 
            // _header3Lbl
            // 
            this._header3Lbl.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._header3Lbl.AutoSize = true;
            this._header3Lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._header3Lbl.Location = new System.Drawing.Point(13, 123);
            this._header3Lbl.Name = "_header3Lbl";
            this._header3Lbl.Size = new System.Drawing.Size(309, 20);
            this._header3Lbl.TabIndex = 9;
            this._header3Lbl.Text = "Click OK to upload the list to Drop Cowboy.";
            // 
            // _header4Lbl
            // 
            this._header4Lbl.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._header4Lbl.AutoSize = true;
            this._header4Lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._header4Lbl.Location = new System.Drawing.Point(13, 153);
            this._header4Lbl.Name = "_header4Lbl";
            this._header4Lbl.Size = new System.Drawing.Size(372, 20);
            this._header4Lbl.TabIndex = 10;
            this._header4Lbl.Text = "Only contacts with phone numbers will be uploaded.";
            // 
            // _header2aLbl
            // 
            this._header2aLbl.AutoSize = true;
            this._header2aLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._header2aLbl.Location = new System.Drawing.Point(58, 69);
            this._header2aLbl.Name = "_header2aLbl";
            this._header2aLbl.Size = new System.Drawing.Size(326, 20);
            this._header2aLbl.TabIndex = 11;
            this._header2aLbl.Text = "MLS/Expired/Expired MLS Week 3 - matched";
            // 
            // AddOrUpdateList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.ClientSize = new System.Drawing.Size(642, 276);
            this.Controls.Add(this._header2aLbl);
            this.Controls.Add(this._header4Lbl);
            this.Controls.Add(this._header3Lbl);
            this.Controls.Add(this._header2Lbl);
            this.Controls.Add(this._cancelBtn);
            this.Controls.Add(this._okBtn);
            this.Controls.Add(this._headerLbl);
            this.Controls.Add(this.label1);
            this.Controls.Add(this._listName);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "AddOrUpdateList";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "AddOrUpdateList";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private WindowsApplication.AcquisitionCalculator.TextBox _listName;
        private System.Windows.Forms.Label _headerLbl;
        private System.Windows.Forms.Button _okBtn;
        private System.Windows.Forms.Button _cancelBtn;
        private System.Windows.Forms.Label _header2Lbl;
        private System.Windows.Forms.Label _header3Lbl;
        private System.Windows.Forms.Label _header4Lbl;
        private System.Windows.Forms.Label _header2aLbl;
    }
}