﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProfitGrabber.RVM.DropCowboy.GUI.Forms
{
    public partial class AddOrUpdateList : Form
    {
        public string ListName
        {
            get { return _listName.Text; }
            set { _listName.Text = value; }
        }

        public AddOrUpdateList(bool addList, string listName = null)
        {
            InitializeComponent();

            if (addList)
            {
                this.Text = "Create new List";
                this._okBtn.Text = "Upload";
                if (!string.IsNullOrEmpty(listName))
                   _listName.Text = listName;
            }
            else
            {                
                this.Text = "Update Name for List";
                _header3Lbl.Visible = _header4Lbl.Visible = false;
                _listName.Text = listName;
            }
        }
    }
}
