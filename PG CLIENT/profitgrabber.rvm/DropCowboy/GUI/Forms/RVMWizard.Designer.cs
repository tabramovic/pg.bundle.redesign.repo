﻿namespace ProfitGrabber.RVM.DropCowboy.Forms
{
    partial class RVMWizard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.footerPanel = new System.Windows.Forms.Panel();
            this.bFinish = new System.Windows.Forms.Button();
            this.bodyPanel = new System.Windows.Forms.Panel();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this._sendTextOrRVM = new System.Windows.Forms.Button();
            this.bCreateAccount = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.bUpload = new System.Windows.Forms.Button();
            this.bCreateNewListBtn = new System.Windows.Forms.Button();
            this._listsLv = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this._uploadGroup = new System.Windows.Forms.ToolStripMenuItem();
            this._createNewList = new System.Windows.Forms.ToolStripMenuItem();
            this._renameList = new System.Windows.Forms.ToolStripMenuItem();
            this._deleteList = new System.Windows.Forms.ToolStripMenuItem();
            this._uploadData = new System.Windows.Forms.ToolStripMenuItem();
            this.bLogIn = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this._teamSecretTb = new System.Windows.Forms.TextBox();
            this._teamIdTb = new System.Windows.Forms.TextBox();
            this.footerPanel.SuspendLayout();
            this.bodyPanel.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // footerPanel
            // 
            this.footerPanel.Controls.Add(this.bFinish);
            this.footerPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.footerPanel.Location = new System.Drawing.Point(0, 601);
            this.footerPanel.Name = "footerPanel";
            this.footerPanel.Size = new System.Drawing.Size(978, 32);
            this.footerPanel.TabIndex = 0;
            // 
            // bFinish
            // 
            this.bFinish.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.bFinish.Location = new System.Drawing.Point(891, 4);
            this.bFinish.Name = "bFinish";
            this.bFinish.Size = new System.Drawing.Size(75, 23);
            this.bFinish.TabIndex = 1;
            this.bFinish.Text = "Close";
            this.bFinish.UseVisualStyleBackColor = true;
            // 
            // bodyPanel
            // 
            this.bodyPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.bodyPanel.Controls.Add(this.label6);
            this.bodyPanel.Controls.Add(this.label5);
            this.bodyPanel.Controls.Add(this._sendTextOrRVM);
            this.bodyPanel.Controls.Add(this.bCreateAccount);
            this.bodyPanel.Controls.Add(this.label4);
            this.bodyPanel.Controls.Add(this.label3);
            this.bodyPanel.Controls.Add(this.bUpload);
            this.bodyPanel.Controls.Add(this.bCreateNewListBtn);
            this.bodyPanel.Controls.Add(this._listsLv);
            this.bodyPanel.Controls.Add(this.bLogIn);
            this.bodyPanel.Controls.Add(this.label2);
            this.bodyPanel.Controls.Add(this.label1);
            this.bodyPanel.Controls.Add(this._teamSecretTb);
            this.bodyPanel.Controls.Add(this._teamIdTb);
            this.bodyPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bodyPanel.Location = new System.Drawing.Point(0, 0);
            this.bodyPanel.Name = "bodyPanel";
            this.bodyPanel.Size = new System.Drawing.Size(978, 601);
            this.bodyPanel.TabIndex = 1;
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Red;
            this.label6.Location = new System.Drawing.Point(297, 173);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(494, 20);
            this.label6.TabIndex = 13;
            this.label6.Text = "CLICK HERE TO RECEIVE YOUR SPECIAL DISCOUNT!  >>";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(12, 40);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(883, 20);
            this.label5.TabIndex = 12;
            this.label5.Text = "The Team ID and Secret can be found after you login to your DropCowboy.com accoun" +
    "t, click on My Account and select API.";
            // 
            // _sendTextOrRVM
            // 
            this._sendTextOrRVM.Location = new System.Drawing.Point(846, 558);
            this._sendTextOrRVM.Name = "_sendTextOrRVM";
            this._sendTextOrRVM.Size = new System.Drawing.Size(120, 29);
            this._sendTextOrRVM.TabIndex = 11;
            this._sendTextOrRVM.Text = "Send Text/RVM";
            this._sendTextOrRVM.UseVisualStyleBackColor = true;
            // 
            // bCreateAccount
            // 
            this.bCreateAccount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.bCreateAccount.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bCreateAccount.Location = new System.Drawing.Point(797, 169);
            this.bCreateAccount.Name = "bCreateAccount";
            this.bCreateAccount.Size = new System.Drawing.Size(169, 29);
            this.bCreateAccount.TabIndex = 10;
            this.bCreateAccount.Text = "Create Account";
            this.bCreateAccount.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(12, 149);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(700, 20);
            this.label4.TabIndex = 9;
            this.label4.Text = "If not, Please create a Drop Cowboy account now! DO NOT go directly to their webs" +
    "ite.";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(12, 11);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(751, 20);
            this.label3.TabIndex = 8;
            this.label3.Text = "If you have a Drop Cowboy account already setup, enter your Team ID and Secret an" +
    "d Login:";
            // 
            // bUpload
            // 
            this.bUpload.Enabled = false;
            this.bUpload.Location = new System.Drawing.Point(698, 558);
            this.bUpload.Name = "bUpload";
            this.bUpload.Size = new System.Drawing.Size(120, 29);
            this.bUpload.TabIndex = 7;
            this.bUpload.Text = "Upload Data";
            this.bUpload.UseVisualStyleBackColor = true;
            this.bUpload.Visible = false;
            // 
            // bCreateNewListBtn
            // 
            this.bCreateNewListBtn.Enabled = false;
            this.bCreateNewListBtn.Location = new System.Drawing.Point(16, 558);
            this.bCreateNewListBtn.Name = "bCreateNewListBtn";
            this.bCreateNewListBtn.Size = new System.Drawing.Size(120, 29);
            this.bCreateNewListBtn.TabIndex = 6;
            this.bCreateNewListBtn.Text = "Create New List";
            this.bCreateNewListBtn.UseVisualStyleBackColor = true;
            this.bCreateNewListBtn.Visible = false;
            // 
            // _listsLv
            // 
            this._listsLv.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3});
            this._listsLv.ContextMenuStrip = this.contextMenuStrip1;
            this._listsLv.Enabled = false;
            this._listsLv.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._listsLv.FullRowSelect = true;
            this._listsLv.GridLines = true;
            this._listsLv.HideSelection = false;
            this._listsLv.Location = new System.Drawing.Point(16, 210);
            this._listsLv.Name = "_listsLv";
            this._listsLv.Size = new System.Drawing.Size(950, 336);
            this._listsLv.TabIndex = 5;
            this._listsLv.UseCompatibleStateImageBehavior = false;
            this._listsLv.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "List Name";
            this.columnHeader1.Width = 445;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Created";
            this.columnHeader2.Width = 356;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "# of Contacts";
            this.columnHeader3.Width = 113;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._uploadGroup,
            this._createNewList,
            this._renameList,
            this._deleteList,
            this._uploadData});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(221, 114);
            // 
            // _uploadGroup
            // 
            this._uploadGroup.Name = "_uploadGroup";
            this._uploadGroup.Size = new System.Drawing.Size(220, 22);
            this._uploadGroup.Text = "Upload Group for Text/RVM";
            this._uploadGroup.Visible = false;
            // 
            // _createNewList
            // 
            this._createNewList.Name = "_createNewList";
            this._createNewList.Size = new System.Drawing.Size(220, 22);
            this._createNewList.Text = "Create New List";
            this._createNewList.Visible = false;
            // 
            // _renameList
            // 
            this._renameList.Name = "_renameList";
            this._renameList.Size = new System.Drawing.Size(220, 22);
            this._renameList.Text = "Rename List";
            // 
            // _deleteList
            // 
            this._deleteList.Name = "_deleteList";
            this._deleteList.Size = new System.Drawing.Size(220, 22);
            this._deleteList.Text = "Delete List";
            // 
            // _uploadData
            // 
            this._uploadData.Name = "_uploadData";
            this._uploadData.Size = new System.Drawing.Size(220, 22);
            this._uploadData.Text = "Upload Data";
            this._uploadData.Visible = false;
            // 
            // bLogIn
            // 
            this.bLogIn.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bLogIn.Location = new System.Drawing.Point(522, 103);
            this.bLogIn.Name = "bLogIn";
            this.bLogIn.Size = new System.Drawing.Size(120, 29);
            this.bLogIn.TabIndex = 4;
            this.bLogIn.Text = "LogIn";
            this.bLogIn.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(56, 107);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 20);
            this.label2.TabIndex = 3;
            this.label2.Text = "Secret:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(56, 72);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 20);
            this.label1.TabIndex = 2;
            this.label1.Text = "Team Id:";
            // 
            // _teamSecretTb
            // 
            this._teamSecretTb.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._teamSecretTb.Location = new System.Drawing.Point(141, 104);
            this._teamSecretTb.Name = "_teamSecretTb";
            this._teamSecretTb.Size = new System.Drawing.Size(366, 26);
            this._teamSecretTb.TabIndex = 1;
            // 
            // _teamIdTb
            // 
            this._teamIdTb.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._teamIdTb.Location = new System.Drawing.Point(141, 69);
            this._teamIdTb.Name = "_teamIdTb";
            this._teamIdTb.Size = new System.Drawing.Size(366, 26);
            this._teamIdTb.TabIndex = 0;
            // 
            // RVMWizard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.ClientSize = new System.Drawing.Size(978, 633);
            this.Controls.Add(this.bodyPanel);
            this.Controls.Add(this.footerPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "RVMWizard";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "DROPCOWBOY Ringless Voice Drop Wizard";
            this.footerPanel.ResumeLayout(false);
            this.bodyPanel.ResumeLayout(false);
            this.bodyPanel.PerformLayout();
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel footerPanel;
        private System.Windows.Forms.Button bFinish;
        private System.Windows.Forms.Panel bodyPanel;
        private System.Windows.Forms.Button bUpload;
        private System.Windows.Forms.Button bCreateNewListBtn;
        private System.Windows.Forms.ListView _listsLv;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.Button bLogIn;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox _teamSecretTb;
        private System.Windows.Forms.TextBox _teamIdTb;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem _createNewList;
        private System.Windows.Forms.ToolStripMenuItem _renameList;
        private System.Windows.Forms.ToolStripMenuItem _deleteList;
        private System.Windows.Forms.ToolStripMenuItem _uploadData;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.Button bCreateAccount;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ToolStripMenuItem _uploadGroup;
        private System.Windows.Forms.Button _sendTextOrRVM;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
    }
}