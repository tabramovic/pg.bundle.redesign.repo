﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProfitGrabber.RVM.DropCowboy
{
    public class DCConsts
    {
        public static string DropCowboyListURL = "https://api.dropcowboy.com/v1/list";
        public const string OptInURL = "https://www.dropcowboy.com/integrations/profit-grabber-pro?lead_source=profitgrabber";
        public const string DCLogInURL = "https://app.dropcowboy.com#/login";
    }
}
