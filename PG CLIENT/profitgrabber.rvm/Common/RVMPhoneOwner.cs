﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProfitGrabber.RVM.Common
{
    public class RVMPhoneOwner
    {
        public string PhoneNr { get; set; }        
        public string Address { get; set; }
        
        public override string ToString()
        {
            return $"{PhoneNr}, {Address}";
        }
    }
}
