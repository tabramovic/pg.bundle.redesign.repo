﻿namespace ProfitGrabber.RVM.Stratics.DTO.Response.GetAudioFilesList
{
    using System;
    using System.Collections.Generic;

    using System.Globalization;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;

    public partial class Response
    {
        [JsonProperty("Name")]
        public string Name { get; set; }

        [JsonProperty("DateLastModified")]
        public DateTimeOffset DateLastModified { get; set; }

        [JsonProperty("Size")]
        public long Size { get; set; }

        [JsonProperty("Type")]
        public string Type { get; set; }
    }

    public partial class Response
    {
        public static Response[] FromJson(string json) => JsonConvert.DeserializeObject<Response[]>(json);
    }

    public static class Serialize
    {
        public static string ToJson(this Response[] self) => JsonConvert.SerializeObject(self);
    }

    internal static class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters =
            {
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }
}

