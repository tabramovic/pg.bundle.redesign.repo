﻿using Newtonsoft.Json;
using ProfitGrabber.RVM.Extensions;

namespace ProfitGrabber.RVM.Stratics.DTO.Request.Token
{
    public class Request
    {
        private string _email;
        private string _password;

        [JsonProperty("email")]
        public string Email { get { return _email.CreateMD5(); } }

        [JsonProperty("password")]
        public string Password { get { return _password.CreateMD5(); } }

        public Request(string email, string password)
        {
            _email = email;
            _password = password;
        }
    }
}
