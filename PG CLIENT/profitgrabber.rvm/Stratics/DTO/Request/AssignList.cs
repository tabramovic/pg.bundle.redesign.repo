﻿
// <auto-generated />
//
// To parse this JSON data, add NuGet 'Newtonsoft.Json' then do:
//
//    using QuickType;
//
//    var Request = Request.FromJson(jsonString);

namespace ProfitGrabber.RVM.Stratics.DTO.Request.AssignList
{
    using System;
    using System.Collections.Generic;

    using System.Globalization;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;

    public partial class Request
    {
        [JsonProperty("list")]
        public long List { get; set; }
    }

    public partial class Request
    {
        public static Request FromJson(string json) => JsonConvert.DeserializeObject<Request>(json);
    }

    public static class Serialize
    {
        public static string ToJson(this Request self) => JsonConvert.SerializeObject(self);
    }

    internal static class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters =
            {
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }
}
