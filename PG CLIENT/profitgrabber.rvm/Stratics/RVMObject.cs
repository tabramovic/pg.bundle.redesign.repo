﻿using System;
using System.Threading.Tasks;
using ProfitGrabber.RVM.Stratics.UOW;
using ProfitGrabber.RVM.Extensions;



namespace ProfitGrabber.RVM.Stratics
{
    public class RVMObject : Singleton<RVMObjectImpl>
    {

    }

    public class RVMObjectImpl
    {
        RVMObjectImpl()
        {
        }

        public string FileUri { get; set; }

        public string Username { get; set; }
        public string Password { get; set; }

        public DTO.Response.Token.Response Credentials { get; set; }
        public DTO.Response.AccountDetails.Response AccountDetails { get; set; }
        public DTO.Response.CallerId.Response CallerIdsWithDetails { get; set; }
        public DTO.Response.UploadPhoneList.Response UploadPhoneListResult { get; set; }
        public DTO.Response.PhoneListStatus.Response PhoneListStatus { get; set; }
        public DTO.Response.CreateSchedule.Response CreatedSchedule { get; set; }
        public DTO.Response.GetAudioFilesList.Response[] AudioFiles { get; set; }
        public DTO.Response.CreateCampaign.Response CreateCampaignResult { get; set; }
        public DTO.Response.CreateSMSCampaign.Response CreateSMSCampaignResult { get; set; }
        public DTO.Response.AssignList.Response AssingListResponse { get; set; }

        public string AuthHash
        {
            get { return $"{Username}{Password}".CreateMD5(); }
        }

        public async Task<DTO.Response.Token.Response> Login()
        {
            try { Credentials = await GetAuthenticationToken.Execute(new DTO.Request.Token.Request(Username, Password)); }
            catch { Credentials = null; }
                        
            return Credentials;            
        }

        public async Task<DTO.Response.AccountDetails.Response> GetAccountDetails()
        {
            try { AccountDetails = await UOW.GetAccountDetails.Execute(Credentials.Token.AsJWTToken(), Credentials.RootAccounts[0]); }
            catch { AccountDetails = null; }
            return AccountDetails;
        }

        public async Task<DTO.Response.CallerId.Response> GetCallerIdsWithDetails()
        {
            try { CallerIdsWithDetails = await GetCallerIds.Execute(Credentials.Token.AsJWTToken(), Credentials.RootAccounts[0]); }
            catch { CallerIdsWithDetails = null; }
            return CallerIdsWithDetails;
        }

        public async Task<DTO.Response.UploadPhoneList.Response> UploadPhoneList()
        {
            try { UploadPhoneListResult = await UOW.UploadPhoneList.Execute(Credentials.Token.AsJWTToken(), Credentials.RootAccounts[0], FileUri); }
            catch { UploadPhoneListResult = null; }
            return UploadPhoneListResult;
        }

        public async Task<DTO.Response.PhoneListStatus.Response> GetPhoneListStatus()
        {
            try { PhoneListStatus = await UOW.GetPhoneListStatus.Execute(Credentials.Token.AsJWTToken(), Credentials.RootAccounts[0], UploadPhoneListResult.Data.Token); }
            catch { PhoneListStatus = null; }
            return PhoneListStatus;
        }

        public async Task<DTO.Response.CreateSchedule.Response> CreateSchedule(DTO.Request.CreateSchedule.Request createScheduleReq)
        {
            var now = DateTime.Now;
            createScheduleReq.ScheduleName = $"{now:yyyy-MM-dd HH:mm:ss}";
            createScheduleReq.Account = Credentials.RootAccounts[0];

            try { CreatedSchedule = await UOW.CreateSchedule.Execute(Credentials.Token.AsJWTToken(), createScheduleReq); }
            catch { CreatedSchedule = null; }

            return CreatedSchedule;
        }

        public async Task<DTO.Response.GetAudioFilesList.Response[]> GetAudioFiles()
        {
            try { AudioFiles = await GetAudioFilesList.Execute(Credentials.RootAccounts[0], AuthHash); }
            catch { AudioFiles = null; }
            return AudioFiles;
        }

        public async Task<DTO.Response.CreateCampaign.Response> CreateRVMCampaign(DTO.Request.CreateCampaign.Request createCampaignReq)
        {
            var now = DateTime.Now;
            createCampaignReq.Account = Credentials.RootAccounts[0];
            createCampaignReq.Name = $"PG-Campaign-{now:yyyy-MM-dd HH:mm:ss}";
            createCampaignReq.Schedule = CreatedSchedule.Data.ScheduleId;
            createCampaignReq.Audio = $"{Credentials.RootAccounts[0]}/" + createCampaignReq.Audio;
            createCampaignReq.MaxPorts = 100;
            createCampaignReq.Callerids = new long[] { CallerIdsWithDetails.Data[0].CalleridId };

            try { CreateCampaignResult = await CreateCampaign.Execute(Credentials.Token.AsJWTToken(), createCampaignReq); }
            catch { CreateCampaignResult = null; }
            return CreateCampaignResult;
        }

        public async Task<DTO.Response.AssignList.Response> AssignListToRVMCampaign()
        {
            var listToAssign = new ProfitGrabber.RVM.Stratics.DTO.Request.AssignList.Request
            {
                List = PhoneListStatus.Data.ListId
            };

            try { AssingListResponse = await AssignList.Execute(Credentials.Token.AsJWTToken(), CreateCampaignResult.RvmData.RinglessvmId, listToAssign); }
            catch { AssingListResponse = null; }
            return AssingListResponse;
        }

        public async Task<DTO.Response.CreateSMSCampaign.Response> CreateTextCampaign(DTO.Request.CreateSMSCampaign.Request createCampaignReq)
        {
            var now = DateTime.Now;
            createCampaignReq.AccountId = Credentials.RootAccounts[0];
            createCampaignReq.Name = $"PG-SMS-Campaign-{now:yyyy-MM-dd HH:mm:ss}";
            //createCampaignReq.Message = "GET FROM GUI";
            createCampaignReq.ScheduleId = CreatedSchedule.Data.ScheduleId;
            //createCampaignReq.StartDate = "2020-02-14";//get from GUI
            //createCampaignReq.EndDate = "2020-02-17";//get from GUI
            createCampaignReq.SmsPerMin = 25;
            createCampaignReq.Lists = new long[] { PhoneListStatus.Data.ListId };
            createCampaignReq.IsPaused = true;
            
            try { CreateSMSCampaignResult = await CreateSMSCampaign.Execute(Credentials.Token.AsJWTToken(), createCampaignReq); }
            catch { CreateSMSCampaignResult = null; }
            return CreateSMSCampaignResult;
        }


    }
}
