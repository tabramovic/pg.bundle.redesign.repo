﻿using System.Net.Http;
using ProfitGrabber.RVM.Extensions;
using System.Threading.Tasks;
using System;

namespace ProfitGrabber.RVM.Stratics.UOW
{
    public static class GetPhoneListStatus
    {
        public static async Task<DTO.Response.PhoneListStatus.Response> Execute(string jwtToken, long rootAccountId, string phoneListToken)
        {
            using (var client = new HttpClient())
            {
                var url = $"{Consts.StraticsAPIURL}{Consts.GetPhoneListStatusSuffix1}{phoneListToken}{Consts.GetPhoneListStatusSuffix2}";

                try
                {
                    client.DefaultRequestHeaders.Add("Authorization", jwtToken);
                    var response = await client.GetAsync(url);

                    response.EnsureSuccessStatusCode();
                    var res = await response.Content.ReadAsStringAsync();
                    return DTO.Response.PhoneListStatus.Response.FromJson(res);
                }
                catch (HttpRequestException hre)
                {
                    //log error
                    throw;
                }
                catch (Exception e)
                {
                    //log error
                    throw;
                }
            }
        }
    }
}
