﻿using System.Net.Http;
using ProfitGrabber.RVM.Extensions;
using System.Threading.Tasks;
using System;

namespace ProfitGrabber.RVM.Stratics.UOW
{
    public static class GetSchedules
    {
        public static async Task<DTO.Response.GetSchedule.Response> Execute(string jwtToken, long rootAccountId)
        {
            using (var client = new HttpClient())
            {
                var url = $"{Consts.StraticsAPIURL}{Consts.GetAllSchedulesSuffix}{rootAccountId}";

                try
                {
                    client.DefaultRequestHeaders.Add("Authorization", jwtToken);
                    var response = await client.GetAsync(url);

                    response.EnsureSuccessStatusCode();
                    var res = await response.Content.ReadAsStringAsync();
                    return DTO.Response.GetSchedule.Response.FromJson(res);
                }
                catch (HttpRequestException hre)
                {
                    //log error
                    throw;
                }
                catch (Exception e)
                {
                    //log error
                    throw;
                }
            }
        }
    }
}
