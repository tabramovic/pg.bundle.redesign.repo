﻿using System.Net.Http;
using ProfitGrabber.RVM.Extensions;
using System.Threading.Tasks;
using System;

namespace ProfitGrabber.RVM.Stratics.UOW
{   
    public static class GetCallerIds
    {
        public static async Task<DTO.Response.CallerId.Response> Execute(string jwtToken, long rootAccountId)
        {
            using (var client = new HttpClient())
            {
                var url = $"{Consts.StraticsAPIURL}{Consts.GetCallerIdsURLSuffix}{rootAccountId}";

                try
                {
                    client.DefaultRequestHeaders.Add("Authorization", jwtToken);
                    var response = await client.GetAsync(url);

                    response.EnsureSuccessStatusCode();
                    var res = await response.Content.ReadAsStringAsync();
                    return DTO.Response.CallerId.Response.FromJson(res);
                }
                catch (HttpRequestException hre)
                {
                    //log error
                    throw;
                }
                catch (Exception e)
                {
                    //log error
                    throw;
                }
            }
        }
    }
}
