﻿using System.Net.Http;
using ProfitGrabber.RVM.Extensions;
using System.Threading.Tasks;
using System;
using System.Collections.Generic;

namespace ProfitGrabber.RVM.Stratics.UOW
{
    public static class GetAudioFilesList
    {
        public static async Task<DTO.Response.GetAudioFilesList.Response[]> Execute(long rootAccountId, string authhash)
        {
            using (var client = new HttpClient())
            {
                var url = $"{Consts.StraticsAudioAPIURL}{rootAccountId}/";

                try
                {
                    client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/x-www-form-urlencoded"));
                    
                    var request = new Dictionary<string, string>();
                    request.Add("account_id", rootAccountId.ToString());
                    request.Add("authhash", authhash.ToLower());

                    var response = await client.PostAsync(url, new FormUrlEncodedContent(request));

                    response.EnsureSuccessStatusCode();
                    var res = await response.Content.ReadAsStringAsync();

                    return DTO.Response.GetAudioFilesList.Response.FromJson(res);
                }
                catch (HttpRequestException hre)
                {
                    //log error
                    throw;
                }
                catch (Exception e)
                {
                    //log error
                    throw;
                }
            }
        }
    }
}
