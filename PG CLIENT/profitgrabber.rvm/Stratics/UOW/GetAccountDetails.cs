﻿using System.Net.Http;
using ProfitGrabber.RVM.Extensions;
using System.Threading.Tasks;
using System;

namespace ProfitGrabber.RVM.Stratics.UOW
{
    public static class GetAccountDetails
    {
        public static async Task<DTO.Response.AccountDetails.Response> Execute(string jwtToken, long rootAccountId)
        {
            using (var client = new HttpClient())
            {
                var url = $"{Consts.StraticsAPIURL}{Consts.GetAccountDetailsURLSuffix}{rootAccountId}";

                try
                {
                    client.DefaultRequestHeaders.Add("Authorization", jwtToken);
                    var response = await client.GetAsync(url);

                    response.EnsureSuccessStatusCode();
                    var res = await response.Content.ReadAsStringAsync();
                    return DTO.Response.AccountDetails.Response.FromJson(res);
                }
                catch (HttpRequestException hre)
                {
                    //log error
                    throw;
                }
                catch (Exception e)
                {
                    //log error
                    throw;
                }
            }
        }
    }
}
