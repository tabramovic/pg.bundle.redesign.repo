﻿using System.Net.Http;
using ProfitGrabber.RVM.Extensions;
using System.Threading.Tasks;
using System;
using Newtonsoft.Json;
using System.IO;

namespace ProfitGrabber.RVM.Stratics.UOW
{
    public static class UploadPhoneList
    {
        public static async Task<DTO.Response.UploadPhoneList.Response> Execute(string jwtToken, long rootAccountId, string fileUri)
        {
            using (var client = new HttpClient())
            {
                var url = $"{Consts.UploadPhoneListURL}";

                try
                {
                    client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                    client.DefaultRequestHeaders.Add("Authorization", jwtToken);
                    
                    
                    var multiForm = new MultipartFormDataContent();

                    //FileStream fs = File.OpenRead(@"C:\TEMP\PHONELIST.CSV");
                    FileStream fs = File.OpenRead(fileUri);
                    //multiForm.Add(new StreamContent(fs), "file", Path.GetFileName(@"C:\TEMP\PHONELIST.CSV"));
                    multiForm.Add(new StreamContent(fs), "file", Path.GetFileName(fileUri));

                    multiForm.Add(new StringContent(rootAccountId.ToString()), "account");
                    multiForm.Add(new StringContent(Guid.NewGuid().ToString()), "list_name");
                    multiForm.Add(new StringContent("0"), "split_timezones");
                    multiForm.Add(new StringContent("{\"lead_phone\":1,\"address1\":2,\"aux_data1\":2}"), "list_map");
                    multiForm.Add(new StringContent("2"), "suppress_cell_phones");
                                        
                    var response = await client.PostAsync(url, multiForm);
                    
                    response.EnsureSuccessStatusCode();
                    var res = await response.Content.ReadAsStringAsync();
                    
                    return DTO.Response.UploadPhoneList.Response.FromJson(res);
                }
                catch (HttpRequestException hre)
                {
                    //log error
                    throw;
                }
                catch (Exception e)
                {
                    //log error
                    throw;
                }
            }
        }
    }
}
