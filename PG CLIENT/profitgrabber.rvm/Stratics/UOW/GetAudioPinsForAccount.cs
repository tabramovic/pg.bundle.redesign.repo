﻿using System.Net.Http;
using ProfitGrabber.RVM.Extensions;
using System.Threading.Tasks;
using System;
using Newtonsoft.Json;


namespace ProfitGrabber.RVM.Stratics.UOW
{    
    public static class GetAudioPinsForAccount
    {
        public static async Task<DTO.Response.AudioPinsForAccount.Response[]> Execute(string jwtToken, long rootAccountId)
        {
            using (var client = new HttpClient())
            {
                var url = $"{Consts.StraticsAPIURL}{Consts.GetAudioPinsForAccountURLSuffix}{rootAccountId}";

                try
                {
                    client.DefaultRequestHeaders.Add("Authorization", jwtToken);
                    var response = await client.GetAsync(url);

                    response.EnsureSuccessStatusCode();
                    var res = await response.Content.ReadAsStringAsync();

                    string unescapedString = JsonConvert.DeserializeObject<string>(res);
                                        
                    return DTO.Response.AudioPinsForAccount.Response.FromJson(unescapedString);
                }
                catch (HttpRequestException hre)
                {
                    //log error
                    throw;
                }
                catch (Exception e)
                {
                    //log error
                    throw;
                }
            }
        }
    }
}
