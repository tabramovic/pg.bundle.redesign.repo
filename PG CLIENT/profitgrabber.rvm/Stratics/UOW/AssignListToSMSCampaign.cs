﻿using System.Net.Http;
using ProfitGrabber.RVM.Extensions;
using System.Threading.Tasks;
using System;

namespace ProfitGrabber.RVM.Stratics.UOW
{   
    public static class AssignListToSMSCampaign
    {
        public static async Task<DTO.Response.AssignListToSMSCampaign.Response[]> Execute(string jwtToken, long campaignId, DTO.Request.AssignListToSMSCampaign.Request assignListReq)
        {
            using (var client = new HttpClient())
            {
                var url = $"{Consts.StraticsAPIURL}{Consts.AssingListToSMSCampaignSuffix1}{campaignId}{Consts.AssingListToSMSCampaignSuffix2}";

                try
                {
                    client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                    client.DefaultRequestHeaders.Add("Authorization", jwtToken);

                    var response = await client.PostAsync(url, assignListReq.AsJSON());

                    response.EnsureSuccessStatusCode();
                    var res = await response.Content.ReadAsStringAsync();

                    return DTO.Response.AssignListToSMSCampaign.Response.FromJson(res);
                }
                catch (HttpRequestException hre)
                {
                    //log error
                    throw;
                }
                catch (Exception e)
                {
                    //log error
                    throw;
                }
            }
        }
    }
}
