﻿using System.Net.Http;
using ProfitGrabber.RVM.Extensions;
using System.Threading.Tasks;
using System;

namespace ProfitGrabber.RVM.Stratics.UOW
{
    public static class GetAuthenticationToken
    {
        public static async Task<DTO.Response.Token.Response> Execute (DTO.Request.Token.Request tokenRequest)
        {            
            using (var client = new HttpClient())
            {
                var url = $"{Consts.StraticsAPIURL}{Consts.GetAuthenticationTokenURLSuffix}";
                
                try
                {                    
                    var response = await client.PostAsync(url, tokenRequest.AsJSON());

                    response.EnsureSuccessStatusCode();
                    var res = await response.Content.ReadAsStringAsync();
                    return DTO.Response.Token.Response.FromJson(res);
                }
                catch (HttpRequestException hre)
                {
                    //log error
                    throw;
                }
                catch (Exception e)
                {
                    //log error
                    throw;
                }
            }
        }
    }
}
