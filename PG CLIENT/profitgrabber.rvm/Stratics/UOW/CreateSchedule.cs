﻿using System.Net.Http;
using ProfitGrabber.RVM.Extensions;
using System.Threading.Tasks;
using System;
using Newtonsoft.Json;
using System.IO;

namespace ProfitGrabber.RVM.Stratics.UOW
{
    public static class CreateSchedule
    {
        public static async Task<DTO.Response.CreateSchedule.Response> Execute(string jwtToken, DTO.Request.CreateSchedule.Request createScheduleReq)
        {
            using (var client = new HttpClient())
            {
                var url = $"{Consts.StraticsAPIURL}{Consts.CreateScheduleSuffix}";

                try
                {
                    client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                    client.DefaultRequestHeaders.Add("Authorization", jwtToken);

                    var response = await client.PostAsync(url, createScheduleReq.AsJSON());

                    response.EnsureSuccessStatusCode();
                    var res = await response.Content.ReadAsStringAsync();

                    return DTO.Response.CreateSchedule.Response.FromJson(res);
                }
                catch (HttpRequestException hre)
                {
                    //log error
                    throw;
                }
                catch (Exception e)
                {
                    //log error
                    throw;
                }
            }
        }
    }
}
