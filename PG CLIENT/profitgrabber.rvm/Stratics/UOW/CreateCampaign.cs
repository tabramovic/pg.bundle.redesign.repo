﻿using System.Net.Http;
using ProfitGrabber.RVM.Extensions;
using System.Threading.Tasks;
using System;

namespace ProfitGrabber.RVM.Stratics.UOW
{
    public static class CreateCampaign
    {
        public static async Task<DTO.Response.CreateCampaign.Response> Execute(string jwtToken, DTO.Request.CreateCampaign.Request createCampaignReq)
        {
            using (var client = new HttpClient())
            {
                var url = $"{Consts.StraticsAPIURL}{Consts.CreateCampaignSuffix}";

                try
                {
                    client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                    client.DefaultRequestHeaders.Add("Authorization", jwtToken);

                    var response = await client.PostAsync(url, createCampaignReq.AsJSON());

                    response.EnsureSuccessStatusCode();
                    var res = await response.Content.ReadAsStringAsync();

                    return DTO.Response.CreateCampaign.Response.FromJson(res);
                }
                catch (HttpRequestException hre)
                {
                    //log error
                    throw;
                }
                catch (Exception e)
                {
                    //log error
                    throw;
                }
            }
        }
    }
}
