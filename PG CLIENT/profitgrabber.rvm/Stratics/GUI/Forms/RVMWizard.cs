﻿using System;
using System.Windows.Forms;
using ProfitGrabber.RVM.Stratics.GUI.UCs;

namespace ProfitGrabber.RVM.Stratics.Forms
{
    public partial class RVMWizard : Form
    {
        eWizardType wizardType;    
        public RVMWizard(eWizardType type = eWizardType.RinglessVoiceDrop)
        {
            InitializeComponent();

            AddEventHandlers();

            bPrev.Visible = false;
            bNext.Enabled = false;

            wizardType = type;
            if (eWizardType.SMS == wizardType)            
                this.Text = "STRATICS Text Message Wizard";
            
            Step1UC.Instance.ResetControl();
            Step2UC.Instance.ResetControl();
            Step3UC.Instance.ResetControl();
            Step3SMSUC.Instance.ResetControl();

            Step1UC.Instance.OnActivePhoneList += Instance_OnActivePhoneList;
            //Step2UC.Instance.OnCreatedSchedule += Instance_OnCreatedSchedule;
            Step3UC.Instance.OnRVMCompleted += Instance_OnRVMCompleted;
            Step3SMSUC.Instance.OnSMSCompleted += Instance_OnSMSCompleted;

            Step1UC.Instance.Dock = DockStyle.Fill;            
            bodyPanel.Controls.Add(Step1UC.Instance);
        }
        
        public string FilePath { get { return RVMObject.Instance.FileUri; } set { RVMObject.Instance.FileUri = value; } }
        
        private void AddEventHandlers()
        {
            bNext.Click += BNext_Click;
            bPrev.Click += BPrev_Click;
            bCancel.Click += BCancel_Click;
        }

        private void BCancel_Click(object sender, EventArgs e)
        {
            if (DialogResult.Yes == MessageBox.Show("Are you sure you want to exit Ringless Voice Message Wizard?", "ProfitGrabber Pro", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2))
                this.Close();
        }

        private void BPrev_Click(object sender, EventArgs e)
        {
            if (bodyPanel.Controls[0] is Step3SMS)
            {
                bodyPanel.Controls.Remove(Step3SMSUC.Instance);
                bodyPanel.Controls.Add(Step2UC.Instance);
                Step2UC.Instance.Dock = DockStyle.Fill;
                bPrev.Enabled = bPrev.Visible = false;
                bNext.Text = "Next";
                bNext.Enabled = true;
                return;
            }

            if (bodyPanel.Controls[0] is Step3)
            {
                bodyPanel.Controls.Remove(Step3UC.Instance);
                bodyPanel.Controls.Add(Step2UC.Instance);
                Step2UC.Instance.Dock = DockStyle.Fill;
                bPrev.Enabled = bPrev.Visible = false;
                bNext.Text = "Next";
                bNext.Enabled = true;
                return;
            }            
        }

        private async void BNext_Click(object sender, EventArgs e)
        {
           var res = await (bodyPanel.Controls[0] as IStep).NextStep();

            if (bodyPanel.Controls[0] is Step1)
            {
                bodyPanel.Controls.Remove(Step1UC.Instance);
                bodyPanel.Controls.Add(Step2UC.Instance);
                Step2UC.Instance.Dock = DockStyle.Fill;                
                return;
            }

            if (bodyPanel.Controls[0] is Step2)
            {
                if (!res)
                    return;

                bodyPanel.Controls.Remove(Step2UC.Instance);

                if (eWizardType.RinglessVoiceDrop == wizardType)
                {
                    bodyPanel.Controls.Add(Step3UC.Instance);
                    Step3UC.Instance.Dock = DockStyle.Fill;
                }
                else
                {
                    bodyPanel.Controls.Add(Step3SMSUC.Instance);
                    Step3SMSUC.Instance.Dock = DockStyle.Fill;
                }
                bNext.Enabled = false;
                bPrev.Enabled = bPrev.Visible = true;
                return;
            }

            if (bodyPanel.Controls[0] is Step3)
            {                
                this.Close();
            }

            if (bodyPanel.Controls[0] is Step3SMS)
            {
                this.Close();
            }
        }
        
        private void Instance_OnActivePhoneList()
        {
            bNext.Enabled = true;            
        }

        private void Instance_OnRVMCompleted()
        {
            bNext.Enabled = true;
            bNext.Text = "Finish";
        }

        private void Instance_OnSMSCompleted()
        {
            bNext.Enabled = true;
            bNext.Text = "Finish";
        }
    }
}

namespace ProfitGrabber.RVM.Stratics
{
    public enum eWizardType
    {
        RinglessVoiceDrop,
        SMS
    }
}
