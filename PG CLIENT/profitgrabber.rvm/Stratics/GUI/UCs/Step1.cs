﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ProfitGrabber.RVM.Stratics.GUI.UCs
{
    public partial class Step1 : UserControl, IStep
    {
        static string VerticalSeparator = $"{Environment.NewLine}{Environment.NewLine}";

        public event ActivePhoneList OnActivePhoneList;
        public delegate void ActivePhoneList();

        Step1()
        {
            InitializeComponent();
            AddEventHandlers();
        }
        
        void AddEventHandlers()
        {
            bLogIn.Click += BLogIn_Click;
        }

        private async void BLogIn_Click(object sender, EventArgs e)
        {
            await OnLogIn();
        }

        private async Task OnLogIn()
        {
            RVMObject.Instance.Username = tbUsername.Text;
            RVMObject.Instance.Password = tbPwd.Text;

            DTO.Response.Token.Response token;
            DTO.Response.AccountDetails.Response accountDetails;
            DTO.Response.CallerId.Response callerIdsWithDetails;
            DTO.Response.UploadPhoneList.Response uploadPhoneListRes;
            DTO.Response.PhoneListStatus.Response uploadedPhoneListStatus;

            label_AccountDetails.Text = $"1. LOGIN";
            token = await RVMObject.Instance.Login();
            if (null == token)
            {
                label_AccountDetails.Text += $"{Environment.NewLine}Incorrect login or password.";
                return;
            }
            
            label_AccountDetails.Text += $"{Environment.NewLine}";
            label_AccountDetails.Text += $"Account Owner:{token.User.FirstName} {token.User.LastName} ({token.User.Email})";
            label_AccountDetails.Text += $"{Environment.NewLine}";            
            label_AccountDetails.Text += $"Available RootAccounts:\t{string.Join(",", token.RootAccounts.Select(ra => ra).ToArray())}";
            label_AccountDetails.Text += $"{Environment.NewLine}";

            label_AccountDetails.Text += VerticalSeparator;


            label_AccountDetails.Text += $"2. GET ACCOUNT DETAILS";
            do { accountDetails = await RVMObject.Instance.GetAccountDetails(); label_AccountDetails.Text += "."; }
            while (null == accountDetails);

            label_AccountDetails.Text += $"{Environment.NewLine}";
            label_AccountDetails.Text += $"AccountId: {accountDetails.AccountId}, AccountName:{accountDetails.AccountName}, Type:{accountDetails.AccountType}, CanUseRVM:{accountDetails.CanUseRinglessvm}";
            label_AccountDetails.Text += $"{Environment.NewLine}";

            label_AccountDetails.Text += VerticalSeparator;


            label_AccountDetails.Text += $"3. GET CALLER ID";
            do { callerIdsWithDetails = await RVMObject.Instance.GetCallerIdsWithDetails(); label_AccountDetails.Text += "."; }
            while (null == callerIdsWithDetails);
            label_AccountDetails.Text += $"{Environment.NewLine}";            
            label_AccountDetails.Text += $"Account:{callerIdsWithDetails.Data[0].Account}, CallerId: {callerIdsWithDetails.Data[0].CalleridId}, Phone:{callerIdsWithDetails.Data[0].Phone}, State:{callerIdsWithDetails.Data[0].State}";
            label_AccountDetails.Text += $"{Environment.NewLine}";

            label_AccountDetails.Text += VerticalSeparator;

            label_AccountDetails.Text += $"4. UPLOADING PHONE LIST";
            do { uploadPhoneListRes = await RVMObject.Instance.UploadPhoneList(); label_AccountDetails.Text += "."; }
            while (null == uploadPhoneListRes);

            label_AccountDetails.Text += $"{Environment.NewLine}";
            label_AccountDetails.Text += $"Uploaded at:{uploadPhoneListRes.Data.UploadedDate.ToLocalTime()}, ListState:{uploadPhoneListRes.Data.ListState}";
            label_AccountDetails.Text += $"{Environment.NewLine}";

            label_AccountDetails.Text += VerticalSeparator;


            label_AccountDetails.Text += $"5. CHECKING PHONE LIST STATUS";
                        
            do 
            {
                await Task.Delay(1000);
                uploadedPhoneListStatus = await RVMObject.Instance.GetPhoneListStatus();

                if (null != uploadedPhoneListStatus)
                {
                    label_AccountDetails.Text += $"{Environment.NewLine}";
                    label_AccountDetails.Text += $"ListState:{uploadedPhoneListStatus.Data.ListState}";
                }
                else
                    label_AccountDetails.Text += ".";

            }
            while (null == uploadedPhoneListStatus || uploadedPhoneListStatus.Data.ListState != "ACTIVE");

            if (null != OnActivePhoneList)
                OnActivePhoneList();
        }

        public void ResetControl()
        {
            label_AccountDetails.Text = string.Empty;
        }


        public Task<bool> NextStep()
        {            
            return Task.FromResult<bool>(true);
        }
    }

    public class Step1UC : Singleton<Step1>
    {

    }
}
