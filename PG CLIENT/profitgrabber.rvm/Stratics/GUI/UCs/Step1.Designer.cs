﻿namespace ProfitGrabber.RVM.Stratics.GUI.UCs
{
    partial class Step1
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Step1));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.bLogIn = new System.Windows.Forms.Button();
            this.tbUsername = new System.Windows.Forms.TextBox();
            this.tbPwd = new System.Windows.Forms.TextBox();
            this.label_AccountDetails = new System.Windows.Forms.Label();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.straticsLogo = new System.Windows.Forms.PictureBox();
            this.footerLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.straticsLogo)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(2, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Username:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 60);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Password:";
            // 
            // bLogIn
            // 
            this.bLogIn.Location = new System.Drawing.Point(112, 83);
            this.bLogIn.Name = "bLogIn";
            this.bLogIn.Size = new System.Drawing.Size(75, 23);
            this.bLogIn.TabIndex = 6;
            this.bLogIn.Text = "LogIn";
            this.bLogIn.UseVisualStyleBackColor = true;
            // 
            // tbUsername
            // 
            this.tbUsername.Location = new System.Drawing.Point(66, 31);
            this.tbUsername.Name = "tbUsername";
            this.tbUsername.Size = new System.Drawing.Size(121, 20);
            this.tbUsername.TabIndex = 4;
            // 
            // tbPwd
            // 
            this.tbPwd.Location = new System.Drawing.Point(66, 57);
            this.tbPwd.Name = "tbPwd";
            this.tbPwd.PasswordChar = '*';
            this.tbPwd.Size = new System.Drawing.Size(121, 20);
            this.tbPwd.TabIndex = 5;
            // 
            // label_AccountDetails
            // 
            this.label_AccountDetails.AutoSize = true;
            this.label_AccountDetails.Location = new System.Drawing.Point(6, 121);
            this.label_AccountDetails.Name = "label_AccountDetails";
            this.label_AccountDetails.Size = new System.Drawing.Size(10, 13);
            this.label_AccountDetails.TabIndex = 6;
            this.label_AccountDetails.Text = "-";
            // 
            // imageList1
            // 
            this.imageList1.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            this.imageList1.ImageSize = new System.Drawing.Size(16, 16);
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // straticsLogo
            // 
            this.straticsLogo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.straticsLogo.Image = ((System.Drawing.Image)(resources.GetObject("straticsLogo.Image")));
            this.straticsLogo.Location = new System.Drawing.Point(278, 3);
            this.straticsLogo.Name = "straticsLogo";
            this.straticsLogo.Size = new System.Drawing.Size(140, 61);
            this.straticsLogo.TabIndex = 7;
            this.straticsLogo.TabStop = false;
            // 
            // footerLabel
            // 
            this.footerLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.footerLabel.AutoSize = true;
            this.footerLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.footerLabel.Location = new System.Drawing.Point(6, 205);
            this.footerLabel.Name = "footerLabel";
            this.footerLabel.Size = new System.Drawing.Size(276, 13);
            this.footerLabel.TabIndex = 8;
            this.footerLabel.Text = "Button Next becomes active once processing completes.";
            // 
            // Step1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.Controls.Add(this.footerLabel);
            this.Controls.Add(this.straticsLogo);
            this.Controls.Add(this.label_AccountDetails);
            this.Controls.Add(this.tbPwd);
            this.Controls.Add(this.tbUsername);
            this.Controls.Add(this.bLogIn);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Step1";
            this.Size = new System.Drawing.Size(421, 228);
            ((System.ComponentModel.ISupportInitialize)(this.straticsLogo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button bLogIn;
        private System.Windows.Forms.TextBox tbUsername;
        private System.Windows.Forms.TextBox tbPwd;
        private System.Windows.Forms.Label label_AccountDetails;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.PictureBox straticsLogo;
        private System.Windows.Forms.Label footerLabel;
    }
}
