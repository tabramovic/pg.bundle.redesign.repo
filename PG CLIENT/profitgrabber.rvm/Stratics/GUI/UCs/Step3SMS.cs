﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading.Tasks;
using System.Diagnostics;

namespace ProfitGrabber.RVM.Stratics.GUI.UCs
{
    public partial class Step3SMS : UserControl, IStep
    {
        public event SMSCompleted OnSMSCompleted;
        public delegate void SMSCompleted();

        const string OptOutSuffix = " Reply STOP for opt-out.";
        int smsMaxCnt = 160;
        int smsReserved = OptOutSuffix.Length;
        

        Step3SMS()
        {
            InitializeComponent();

            _campaignStartDate.Value = DateTime.Now.Date;
            _campaignEndDate.Value = _campaignStartDate.Value.AddDays(1).Date;

            AddEventHandlers();

            DisplayRemainingCharactersCount(CalculateRemainingCharactersCount());
        }

        void AddEventHandlers()
        {
            _sms.TextChanged += _sms_TextChanged;
            _publish.Click += _publish_Click;
            insertAddress.Click += InsertAddress_Click;
            insertFirstName.Click += InsertFirstName_Click;
            insertLastName.Click += InsertLastName_Click;
        }
        
        void _sms_TextChanged(object sender, EventArgs e)
        {
            DisplayRemainingCharactersCount(CalculateRemainingCharactersCount());            
        }

        private void InsertFirstName_Click(object sender, EventArgs e)
        {
            _sms.AppendText("{first_name}");            
        }

        private void InsertLastName_Click(object sender, EventArgs e)
        {
            _sms.AppendText("{last_name}");
        }

        private void InsertAddress_Click(object sender, EventArgs e)
        {
            _sms.AppendText("{address1}");
        }

        int CalculateRemainingCharactersCount()
        {            
            var cnt = _sms.Text.Length;
            return smsMaxCnt - smsReserved - cnt;
        }

        void DisplayRemainingCharactersCount(int remaining)
        {
            _charCnt.ForeColor = Color.Black; 

            if (remaining < 0)
                _charCnt.ForeColor = Color.Red;

            _charCnt.Text = remaining.ToString();
        }

        public void ResetControl()
        {
            _sms.Text = string.Empty;
            _labelStatus.Text = string.Empty;
            DisplayRemainingCharactersCount(CalculateRemainingCharactersCount());
        }

        private async void _publish_Click(object sender, EventArgs e)
        {
            var remainingCharactersCount = CalculateRemainingCharactersCount();
            if (CalculateRemainingCharactersCount() < 0)
            {
                MessageBox.Show($"Text message is {Math.Abs(remainingCharactersCount)} characters too long.", "Profitgrabber Pro", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            DTO.Request.CreateSMSCampaign.Request createCampaignReq = new DTO.Request.CreateSMSCampaign.Request()
            {
                Message = $"{_sms.Text}{OptOutSuffix}",
                StartDate = _campaignStartDate.Value.ToString("yyyy-MM-dd"),
                EndDate = _campaignEndDate.Value.ToString("yyyy-MM-dd"),
            };

            var createCampaignRes = await RVMObject.Instance.CreateTextCampaign(createCampaignReq);
            string delimiter = "   ";
            _labelStatus.Text = $"Created Campaign:{Environment.NewLine}{Environment.NewLine}{delimiter}Name:{createCampaignRes.Name},{Environment.NewLine}{delimiter}Id:{createCampaignRes.Id}{Environment.NewLine}{delimiter}Created:{createCampaignRes.CreateDate}{Environment.NewLine}{delimiter}Paused:{createCampaignRes.IsPaused}.";

            if (null != OnSMSCompleted)
                OnSMSCompleted();
        }

        public async Task<bool> NextStep()
        {
            Process p = new Process();
            p.StartInfo.FileName = Utils.GetDefaultBrowser();
            p.StartInfo.Arguments = "https://ivr.straticsnetworks.com";
            p.Start();

            return await Task.FromResult<bool>(true);
        }
    }

    public class Step3SMSUC : Singleton<Step3SMS>
    {

    }
}
