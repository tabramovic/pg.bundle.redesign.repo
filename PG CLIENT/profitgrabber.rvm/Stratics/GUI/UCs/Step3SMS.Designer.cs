﻿namespace ProfitGrabber.RVM.Stratics.GUI.UCs
{
    partial class Step3SMS
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this._labelStatus = new System.Windows.Forms.Label();
            this._publish = new System.Windows.Forms.Button();
            this._campaignEndDate = new System.Windows.Forms.DateTimePicker();
            this._campaignStartDate = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this._charCnt = new System.Windows.Forms.Label();
            this._sms = new WindowsApplication.AcquisitionCalculator.TextBox();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.insertAddress = new System.Windows.Forms.ToolStripMenuItem();
            this.insertFirstName = new System.Windows.Forms.ToolStripMenuItem();
            this.insertLastName = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // _labelStatus
            // 
            this._labelStatus.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._labelStatus.AutoSize = true;
            this._labelStatus.Location = new System.Drawing.Point(15, 267);
            this._labelStatus.Name = "_labelStatus";
            this._labelStatus.Size = new System.Drawing.Size(40, 13);
            this._labelStatus.TabIndex = 13;
            this._labelStatus.Text = "Status:";
            // 
            // _publish
            // 
            this._publish.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._publish.Location = new System.Drawing.Point(470, 236);
            this._publish.Name = "_publish";
            this._publish.Size = new System.Drawing.Size(112, 23);
            this._publish.TabIndex = 12;
            this._publish.Text = "Publish Campaign";
            this._publish.UseVisualStyleBackColor = true;
            // 
            // _campaignEndDate
            // 
            this._campaignEndDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._campaignEndDate.Location = new System.Drawing.Point(148, 237);
            this._campaignEndDate.Name = "_campaignEndDate";
            this._campaignEndDate.Size = new System.Drawing.Size(200, 20);
            this._campaignEndDate.TabIndex = 11;
            // 
            // _campaignStartDate
            // 
            this._campaignStartDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._campaignStartDate.Location = new System.Drawing.Point(148, 208);
            this._campaignStartDate.Name = "_campaignStartDate";
            this._campaignStartDate.Size = new System.Drawing.Size(200, 20);
            this._campaignStartDate.TabIndex = 10;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(15, 241);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(124, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "Set Campaign End Date:";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 211);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(127, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Set Campaign Start Date:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(110, 13);
            this.label1.TabIndex = 14;
            this.label1.Text = "Type-in text message:";
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(441, 149);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(106, 13);
            this.label4.TabIndex = 16;
            this.label4.Text = "Available characters:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(36, 149);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(373, 13);
            this.label5.TabIndex = 17;
            this.label5.Text = "Following text will be appended to your text message: Reply STOP for opt-out.";
            // 
            // _charCnt
            // 
            this._charCnt.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._charCnt.AutoSize = true;
            this._charCnt.Location = new System.Drawing.Point(553, 149);
            this._charCnt.Name = "_charCnt";
            this._charCnt.Size = new System.Drawing.Size(25, 13);
            this._charCnt.TabIndex = 18;
            this._charCnt.Text = "160";
            // 
            // _sms
            // 
            this._sms.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._sms.ContextMenuStrip = this.contextMenuStrip1;
            this._sms.Location = new System.Drawing.Point(39, 47);
            this._sms.Multiline = true;
            this._sms.Name = "_sms";
            this._sms.Size = new System.Drawing.Size(543, 90);
            this._sms.TabIndex = 15;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.insertAddress,
            this.insertFirstName,
            this.insertLastName});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(164, 92);
            // 
            // insertAddress
            // 
            this.insertAddress.Name = "insertAddress";
            this.insertAddress.Size = new System.Drawing.Size(163, 22);
            this.insertAddress.Text = "Insert Address";
            // 
            // insertFirstName
            // 
            this.insertFirstName.Enabled = false;
            this.insertFirstName.Name = "insertFirstName";
            this.insertFirstName.Size = new System.Drawing.Size(163, 22);
            this.insertFirstName.Text = "Insert First Name";
            this.insertFirstName.Visible = false;
            // 
            // insertLastName
            // 
            this.insertLastName.Enabled = false;
            this.insertLastName.Name = "insertLastName";
            this.insertLastName.Size = new System.Drawing.Size(163, 22);
            this.insertLastName.Text = "Insert Last Name";
            this.insertLastName.Visible = false;
            // 
            // Step3SMS
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.Controls.Add(this._charCnt);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this._sms);
            this.Controls.Add(this.label1);
            this.Controls.Add(this._labelStatus);
            this.Controls.Add(this._publish);
            this.Controls.Add(this._campaignEndDate);
            this.Controls.Add(this._campaignStartDate);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Name = "Step3SMS";
            this.Size = new System.Drawing.Size(597, 401);
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label _labelStatus;
        private System.Windows.Forms.Button _publish;
        private System.Windows.Forms.DateTimePicker _campaignEndDate;
        private System.Windows.Forms.DateTimePicker _campaignStartDate;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private WindowsApplication.AcquisitionCalculator.TextBox _sms;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label _charCnt;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem insertAddress;
        private System.Windows.Forms.ToolStripMenuItem insertFirstName;
        private System.Windows.Forms.ToolStripMenuItem insertLastName;
    }
}
