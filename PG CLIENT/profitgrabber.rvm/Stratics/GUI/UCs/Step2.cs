﻿using System;
using System.Windows.Forms;
using System.Threading.Tasks;

namespace ProfitGrabber.RVM.Stratics.GUI.UCs
{
    public partial class Step2 : UserControl, IStep
    {

        public event CreatedSchedule OnCreatedSchedule;
        public delegate void CreatedSchedule();

        Step2()
        {
            InitializeComponent();            
        }

        bool TestValidInputs()
        {
            return
                    MonStart.Value.Hour >= 8
                && TueStart.Value.Hour >= 8
                && WedStart.Value.Hour >= 8
                && ThuStart.Value.Hour >= 8
                && FriStart.Value.Hour >= 8
                && SatStart.Value.Hour >= 8
                && SunStart.Value.Hour >= 8
                && MonEnd.Value.Hour <= 21
                && TueEnd.Value.Hour <= 21
                && WedEnd.Value.Hour <= 21
                && ThuEnd.Value.Hour <= 21
                && FriEnd.Value.Hour <= 21
                && SatEnd.Value.Hour <= 21
                && SunEnd.Value.Hour <= 21;
        }
        
        public async Task<bool> NextStep()
        {
            if (!TestValidInputs())
            {
                MessageBox.Show($"Start and End times must fall between 08:00:00 and 21:00:00");
                return await Task.FromResult<bool>(false);
            }

            var createScheduleRequest = new DTO.Request.CreateSchedule.Request
            {
                ScheduleName = string.Empty,
                Account = 0,
                MonStart = MonStart.Value.ToString("HH:mm:ss"),
                TueStart = TueStart.Value.ToString("HH:mm:ss"),
                WedStart = WedStart.Value.ToString("HH:mm:ss"),
                ThuStart = ThuStart.Value.ToString("HH:mm:ss"),
                FriStart = FriStart.Value.ToString("HH:mm:ss"),
                SatStart = SatStart.Value.ToString("HH:mm:ss"),
                SunStart = SunStart.Value.ToString("HH:mm:ss"),
                MonEnd = MonEnd.Value.ToString("HH:mm:ss"),
                TueEnd = TueEnd.Value.ToString("HH:mm:ss"),
                WedEnd = WedEnd.Value.ToString("HH:mm:ss"),
                ThuEnd = ThuEnd.Value.ToString("HH:mm:ss"),
                FriEnd = FriEnd.Value.ToString("HH:mm:ss"),
                SatEnd = SatEnd.Value.ToString("HH:mm:ss"),
                SunEnd = SunEnd.Value.ToString("HH:mm:ss"),
            };

            DTO.Response.CreateSchedule.Response createdSchedule;

            do { createdSchedule = await RVMObject.Instance.CreateSchedule(createScheduleRequest); }
            while (null == createdSchedule);

            if (null != createdSchedule)
            {
                if (null != OnCreatedSchedule)
                    OnCreatedSchedule();
            }

            return await Task.FromResult<bool>(true);
        }        

        public void ResetControl()
        {
            MonStart.Value = DateTime.Now.Date.AddHours(8);
            TueStart.Value = DateTime.Now.Date.AddHours(8);
            WedStart.Value = DateTime.Now.Date.AddHours(8);
            ThuStart.Value = DateTime.Now.Date.AddHours(8);
            FriStart.Value = DateTime.Now.Date.AddHours(8);
            SatStart.Value = DateTime.Now.Date.AddHours(8);
            SunStart.Value = DateTime.Now.Date.AddHours(8);
            MonEnd.Value = DateTime.Now.Date.AddHours(21);
            TueEnd.Value = DateTime.Now.Date.AddHours(21);
            WedEnd.Value = DateTime.Now.Date.AddHours(21);
            ThuEnd.Value = DateTime.Now.Date.AddHours(21);
            FriEnd.Value = DateTime.Now.Date.AddHours(21);
            SatEnd.Value = DateTime.Now.Date.AddHours(21);
            SunEnd.Value = DateTime.Now.Date.AddHours(21);
        }
    }

    public class Step2UC : Singleton<Step2>
    {
        
    }    
}
