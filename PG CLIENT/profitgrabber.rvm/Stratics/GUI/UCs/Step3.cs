﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading.Tasks;
using System.Diagnostics;

namespace ProfitGrabber.RVM.Stratics.GUI.UCs
{
    public partial class Step3 : UserControl, IStep
    {
        public event RVMCompleted OnRVMCompleted;
        public delegate void RVMCompleted();

        DTO.Response.GetAudioFilesList.Response[] AudioFiles { get; set; }
        bool col0Asc = true;
        bool col1Asc = true;

        Step3()
        {
            InitializeComponent();
            _campaignStartDate.Value = DateTime.Now.Date;
            _campaignEndDate.Value = _campaignStartDate.Value.AddDays(1).Date;

            _audioFileListView.ColumnClick += _audioFileListView_ColumnClick;
            _publish.Click += _publish_Click;           
        }
        
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            Task t = Task.Run(async () => AudioFiles = await RVMObject.Instance.GetAudioFiles());
            Task.WaitAll(t);

            FillListView(AudioFiles);
        }

        private void _audioFileListView_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            List<ListViewItem> items;
            if (e.Column == 0)
            {
                if (col0Asc)                
                    items = _audioFileListView.Items.Cast<ListViewItem>().OrderBy(item => item.SubItems[0].Text).ToList();
                else
                    items = _audioFileListView.Items.Cast<ListViewItem>().OrderByDescending(item => item.SubItems[0].Text).ToList();

                col0Asc = !col0Asc;

                _audioFileListView.Items.Clear();
                _audioFileListView.Items.AddRange(items.ToArray());
            }
            else
            {
                if (col1Asc)
                    items = _audioFileListView.Items.Cast<ListViewItem>().OrderBy(item => item.Tag).ToList();
                else
                    items = _audioFileListView.Items.Cast<ListViewItem>().OrderByDescending(item => item.Tag).ToList();

                col1Asc = !col1Asc;

                _audioFileListView.Items.Clear();
                _audioFileListView.Items.AddRange(items.ToArray());
            }
        }

        void FillListView(DTO.Response.GetAudioFilesList.Response[] audioFileList)
        {
            var rows = new List<ListViewItem>();
            foreach (var af in audioFileList)
            {
                ListViewItem lvi = new ListViewItem(af.Name);
                lvi.SubItems.Add(af.DateLastModified.ToLocalTime().ToString("yyyy-MM-dd HH:mm"));
                lvi.Tag = af.DateLastModified;

                rows.Add(lvi);                
            }

            _audioFileListView.Items.AddRange(rows.ToArray());
        }

        private async void _publish_Click(object sender, EventArgs e)
        {
            if (null == _audioFileListView.SelectedItems || 0 == _audioFileListView.SelectedItems.Count)
            {
                MessageBox.Show("Please select audio message");
                return;
            }
                
            var audioMsgAsStr = _audioFileListView.SelectedItems[0].SubItems[0].Text;
            var suffixIdx = audioMsgAsStr.IndexOf(".wav");
            audioMsgAsStr = audioMsgAsStr.Substring(0, suffixIdx);

            var now = DateTime.Now;
            var createCampaignRequest = new DTO.Request.CreateCampaign.Request
            {
                Account = 0,
                Name = string.Empty,
                Schedule = 0,
                Audio = audioMsgAsStr,
                StartDate = _campaignStartDate.Value.ToString("yyyy-MM-dd"),
                EndDate = _campaignEndDate.Value.ToString("yyyy-MM-dd"),
                MaxPorts = 0,
                Callerids = new long[0]
            };

            var createCampaignRes = await RVMObject.Instance.CreateRVMCampaign(createCampaignRequest);

            _labelStatus.Text = $"Created Campaign: Name:{createCampaignRes.RvmData.Name}, Id: {createCampaignRes.RvmData.RinglessvmId}, Status:{createCampaignRes.RvmData.Status}";

            var assignListToCampaignStatus = await RVMObject.Instance.AssignListToRVMCampaign();
            _labelStatus.Text += $"{Environment.NewLine}";
            _labelStatus.Text += $"Assigned List:{assignListToCampaignStatus.Data.List} to Campaign:{assignListToCampaignStatus.Data.Ringlessvm}";

            if (null != OnRVMCompleted)
                OnRVMCompleted();
        }

        public void ResetControl()
        {
            _labelStatus.Text = string.Empty;
            _audioFileListView.Items.Clear();
        }

        public async Task<bool> NextStep()
        {                        
            Process p = new Process();
            p.StartInfo.FileName = Utils.GetDefaultBrowser(); 
            p.StartInfo.Arguments = "https://ivr.straticsnetworks.com";
            p.Start();

            return await Task.FromResult<bool>(true);
        }
    }

    public class Step3UC : Singleton<Step3>
    {

    }
}
