﻿namespace ProfitGrabber.RVM.Stratics.GUI.UCs
{
    partial class Step2
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.MonStart = new System.Windows.Forms.DateTimePicker();
            this.MonEnd = new System.Windows.Forms.DateTimePicker();
            this.TueEnd = new System.Windows.Forms.DateTimePicker();
            this.TueStart = new System.Windows.Forms.DateTimePicker();
            this.WedEnd = new System.Windows.Forms.DateTimePicker();
            this.WedStart = new System.Windows.Forms.DateTimePicker();
            this.ThuEnd = new System.Windows.Forms.DateTimePicker();
            this.ThuStart = new System.Windows.Forms.DateTimePicker();
            this.FriEnd = new System.Windows.Forms.DateTimePicker();
            this.FriStart = new System.Windows.Forms.DateTimePicker();
            this.SatEnd = new System.Windows.Forms.DateTimePicker();
            this.SatStart = new System.Windows.Forms.DateTimePicker();
            this.SunEnd = new System.Windows.Forms.DateTimePicker();
            this.SunStart = new System.Windows.Forms.DateTimePicker();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 103);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(45, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Monday";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 134);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(48, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Tuesday";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 165);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(64, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Wednesday";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(9, 196);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(51, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Thursday";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(9, 227);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(35, 13);
            this.label5.TabIndex = 5;
            this.label5.Text = "Friday";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(9, 258);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(49, 13);
            this.label6.TabIndex = 6;
            this.label6.Text = "Saturday";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(9, 289);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(43, 13);
            this.label7.TabIndex = 7;
            this.label7.Text = "Sunday";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(99, 72);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(29, 13);
            this.label8.TabIndex = 8;
            this.label8.Text = "Start";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(179, 72);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(26, 13);
            this.label9.TabIndex = 9;
            this.label9.Text = "End";
            // 
            // MonStart
            // 
            this.MonStart.CustomFormat = "HH:mm";
            this.MonStart.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.MonStart.Location = new System.Drawing.Point(102, 97);
            this.MonStart.Name = "MonStart";
            this.MonStart.ShowUpDown = true;
            this.MonStart.Size = new System.Drawing.Size(55, 20);
            this.MonStart.TabIndex = 42;
            this.MonStart.Value = new System.DateTime(2019, 11, 10, 8, 0, 0, 0);
            // 
            // MonEnd
            // 
            this.MonEnd.CustomFormat = "HH:mm";
            this.MonEnd.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.MonEnd.Location = new System.Drawing.Point(182, 97);
            this.MonEnd.Name = "MonEnd";
            this.MonEnd.ShowUpDown = true;
            this.MonEnd.Size = new System.Drawing.Size(55, 20);
            this.MonEnd.TabIndex = 43;
            this.MonEnd.Value = new System.DateTime(2019, 11, 10, 21, 0, 0, 0);
            // 
            // TueEnd
            // 
            this.TueEnd.CustomFormat = "HH:mm";
            this.TueEnd.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.TueEnd.Location = new System.Drawing.Point(182, 128);
            this.TueEnd.Name = "TueEnd";
            this.TueEnd.ShowUpDown = true;
            this.TueEnd.Size = new System.Drawing.Size(55, 20);
            this.TueEnd.TabIndex = 45;
            this.TueEnd.Value = new System.DateTime(2019, 11, 10, 21, 0, 0, 0);
            // 
            // TueStart
            // 
            this.TueStart.CustomFormat = "HH:mm";
            this.TueStart.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.TueStart.Location = new System.Drawing.Point(102, 128);
            this.TueStart.Name = "TueStart";
            this.TueStart.ShowUpDown = true;
            this.TueStart.Size = new System.Drawing.Size(55, 20);
            this.TueStart.TabIndex = 44;
            this.TueStart.Value = new System.DateTime(2019, 11, 10, 8, 0, 0, 0);
            // 
            // WedEnd
            // 
            this.WedEnd.CustomFormat = "HH:mm";
            this.WedEnd.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.WedEnd.Location = new System.Drawing.Point(182, 159);
            this.WedEnd.Name = "WedEnd";
            this.WedEnd.ShowUpDown = true;
            this.WedEnd.Size = new System.Drawing.Size(55, 20);
            this.WedEnd.TabIndex = 47;
            this.WedEnd.Value = new System.DateTime(2019, 11, 10, 21, 0, 0, 0);
            // 
            // WedStart
            // 
            this.WedStart.CustomFormat = "HH:mm";
            this.WedStart.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.WedStart.Location = new System.Drawing.Point(102, 159);
            this.WedStart.Name = "WedStart";
            this.WedStart.ShowUpDown = true;
            this.WedStart.Size = new System.Drawing.Size(55, 20);
            this.WedStart.TabIndex = 46;
            this.WedStart.Value = new System.DateTime(2019, 11, 10, 8, 0, 0, 0);
            // 
            // ThuEnd
            // 
            this.ThuEnd.CustomFormat = "HH:mm";
            this.ThuEnd.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.ThuEnd.Location = new System.Drawing.Point(182, 190);
            this.ThuEnd.Name = "ThuEnd";
            this.ThuEnd.ShowUpDown = true;
            this.ThuEnd.Size = new System.Drawing.Size(55, 20);
            this.ThuEnd.TabIndex = 49;
            this.ThuEnd.Value = new System.DateTime(2019, 11, 10, 21, 0, 0, 0);
            // 
            // ThuStart
            // 
            this.ThuStart.CustomFormat = "HH:mm";
            this.ThuStart.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.ThuStart.Location = new System.Drawing.Point(102, 190);
            this.ThuStart.Name = "ThuStart";
            this.ThuStart.ShowUpDown = true;
            this.ThuStart.Size = new System.Drawing.Size(55, 20);
            this.ThuStart.TabIndex = 48;
            this.ThuStart.Value = new System.DateTime(2019, 11, 10, 8, 0, 0, 0);
            // 
            // FriEnd
            // 
            this.FriEnd.CustomFormat = "HH:mm";
            this.FriEnd.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.FriEnd.Location = new System.Drawing.Point(182, 221);
            this.FriEnd.Name = "FriEnd";
            this.FriEnd.ShowUpDown = true;
            this.FriEnd.Size = new System.Drawing.Size(55, 20);
            this.FriEnd.TabIndex = 51;
            this.FriEnd.Value = new System.DateTime(2019, 11, 10, 21, 0, 0, 0);
            // 
            // FriStart
            // 
            this.FriStart.CustomFormat = "HH:mm";
            this.FriStart.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.FriStart.Location = new System.Drawing.Point(102, 221);
            this.FriStart.Name = "FriStart";
            this.FriStart.ShowUpDown = true;
            this.FriStart.Size = new System.Drawing.Size(55, 20);
            this.FriStart.TabIndex = 50;
            this.FriStart.Value = new System.DateTime(2019, 11, 10, 8, 0, 0, 0);
            // 
            // SatEnd
            // 
            this.SatEnd.CustomFormat = "HH:mm";
            this.SatEnd.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.SatEnd.Location = new System.Drawing.Point(182, 252);
            this.SatEnd.Name = "SatEnd";
            this.SatEnd.ShowUpDown = true;
            this.SatEnd.Size = new System.Drawing.Size(55, 20);
            this.SatEnd.TabIndex = 53;
            this.SatEnd.Value = new System.DateTime(2019, 11, 10, 21, 0, 0, 0);
            // 
            // SatStart
            // 
            this.SatStart.CustomFormat = "HH:mm";
            this.SatStart.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.SatStart.Location = new System.Drawing.Point(102, 252);
            this.SatStart.Name = "SatStart";
            this.SatStart.ShowUpDown = true;
            this.SatStart.Size = new System.Drawing.Size(55, 20);
            this.SatStart.TabIndex = 52;
            this.SatStart.Value = new System.DateTime(2019, 11, 10, 8, 0, 0, 0);
            // 
            // SunEnd
            // 
            this.SunEnd.CustomFormat = "HH:mm";
            this.SunEnd.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.SunEnd.Location = new System.Drawing.Point(182, 283);
            this.SunEnd.Name = "SunEnd";
            this.SunEnd.ShowUpDown = true;
            this.SunEnd.Size = new System.Drawing.Size(55, 20);
            this.SunEnd.TabIndex = 55;
            this.SunEnd.Value = new System.DateTime(2019, 11, 10, 21, 0, 0, 0);
            // 
            // SunStart
            // 
            this.SunStart.CustomFormat = "HH:mm";
            this.SunStart.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.SunStart.Location = new System.Drawing.Point(102, 283);
            this.SunStart.Name = "SunStart";
            this.SunStart.ShowUpDown = true;
            this.SunStart.Size = new System.Drawing.Size(55, 20);
            this.SunStart.TabIndex = 54;
            this.SunStart.Value = new System.DateTime(2019, 11, 10, 8, 0, 0, 0);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(10, 19);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(82, 13);
            this.label10.TabIndex = 56;
            this.label10.Text = "RVM Scheduler";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(99, 46);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(295, 13);
            this.label11.TabIndex = 57;
            this.label11.Text = "Start and End times must fall between 08:00:00 and 21:00:00";
            // 
            // Step2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.SunEnd);
            this.Controls.Add(this.SunStart);
            this.Controls.Add(this.SatEnd);
            this.Controls.Add(this.SatStart);
            this.Controls.Add(this.FriEnd);
            this.Controls.Add(this.FriStart);
            this.Controls.Add(this.ThuEnd);
            this.Controls.Add(this.ThuStart);
            this.Controls.Add(this.WedEnd);
            this.Controls.Add(this.WedStart);
            this.Controls.Add(this.TueEnd);
            this.Controls.Add(this.TueStart);
            this.Controls.Add(this.MonEnd);
            this.Controls.Add(this.MonStart);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Step2";
            this.Size = new System.Drawing.Size(422, 318);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.DateTimePicker MonStart;
        private System.Windows.Forms.DateTimePicker MonEnd;
        private System.Windows.Forms.DateTimePicker TueEnd;
        private System.Windows.Forms.DateTimePicker TueStart;
        private System.Windows.Forms.DateTimePicker WedEnd;
        private System.Windows.Forms.DateTimePicker WedStart;
        private System.Windows.Forms.DateTimePicker ThuEnd;
        private System.Windows.Forms.DateTimePicker ThuStart;
        private System.Windows.Forms.DateTimePicker FriEnd;
        private System.Windows.Forms.DateTimePicker FriStart;
        private System.Windows.Forms.DateTimePicker SatEnd;
        private System.Windows.Forms.DateTimePicker SatStart;
        private System.Windows.Forms.DateTimePicker SunEnd;
        private System.Windows.Forms.DateTimePicker SunStart;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
    }
}
