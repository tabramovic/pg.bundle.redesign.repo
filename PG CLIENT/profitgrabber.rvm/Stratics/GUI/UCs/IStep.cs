﻿using System.Threading.Tasks;

namespace ProfitGrabber.RVM.Stratics.GUI.UCs
{
    public interface IStep
    {
        Task<bool> NextStep();
    }
}
