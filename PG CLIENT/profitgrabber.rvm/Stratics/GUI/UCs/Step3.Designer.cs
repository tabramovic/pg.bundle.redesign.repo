﻿namespace ProfitGrabber.RVM.Stratics.GUI.UCs
{
    partial class Step3
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._audioFileListView = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this._campaignStartDate = new System.Windows.Forms.DateTimePicker();
            this._campaignEndDate = new System.Windows.Forms.DateTimePicker();
            this._publish = new System.Windows.Forms.Button();
            this._labelStatus = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // _audioFileListView
            // 
            this._audioFileListView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._audioFileListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2});
            this._audioFileListView.FullRowSelect = true;
            this._audioFileListView.Location = new System.Drawing.Point(18, 43);
            this._audioFileListView.Name = "_audioFileListView";
            this._audioFileListView.Size = new System.Drawing.Size(526, 237);
            this._audioFileListView.TabIndex = 0;
            this._audioFileListView.UseCompatibleStateImageBehavior = false;
            this._audioFileListView.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Audio File Name";
            this.columnHeader1.Width = 368;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Last Modified";
            this.columnHeader2.Width = 145;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(113, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Select Audio Message";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 297);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(127, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Set Campaign Start Date:";
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(15, 327);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(124, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Set Campaign End Date:";
            // 
            // _campaignStartDate
            // 
            this._campaignStartDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._campaignStartDate.Location = new System.Drawing.Point(148, 294);
            this._campaignStartDate.Name = "_campaignStartDate";
            this._campaignStartDate.Size = new System.Drawing.Size(200, 20);
            this._campaignStartDate.TabIndex = 4;
            // 
            // _campaignEndDate
            // 
            this._campaignEndDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._campaignEndDate.Location = new System.Drawing.Point(148, 323);
            this._campaignEndDate.Name = "_campaignEndDate";
            this._campaignEndDate.Size = new System.Drawing.Size(200, 20);
            this._campaignEndDate.TabIndex = 5;
            // 
            // _publish
            // 
            this._publish.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._publish.Location = new System.Drawing.Point(432, 322);
            this._publish.Name = "_publish";
            this._publish.Size = new System.Drawing.Size(112, 23);
            this._publish.TabIndex = 6;
            this._publish.Text = "Publish Campaign";
            this._publish.UseVisualStyleBackColor = true;
            // 
            // _labelStatus
            // 
            this._labelStatus.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._labelStatus.AutoSize = true;
            this._labelStatus.Location = new System.Drawing.Point(15, 353);
            this._labelStatus.Name = "_labelStatus";
            this._labelStatus.Size = new System.Drawing.Size(40, 13);
            this._labelStatus.TabIndex = 7;
            this._labelStatus.Text = "Status:";
            // 
            // Step3
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.Controls.Add(this._labelStatus);
            this.Controls.Add(this._publish);
            this.Controls.Add(this._campaignEndDate);
            this.Controls.Add(this._campaignStartDate);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this._audioFileListView);
            this.Name = "Step3";
            this.Size = new System.Drawing.Size(559, 405);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView _audioFileListView;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker _campaignStartDate;
        private System.Windows.Forms.DateTimePicker _campaignEndDate;
        private System.Windows.Forms.Button _publish;
        private System.Windows.Forms.Label _labelStatus;
    }
}
