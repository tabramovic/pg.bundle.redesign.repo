﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProfitGrabber.RVM.Stratics
{
    public static class Consts
    {
        public const string UploadPhoneListURL = "https://ivrplatform.proxy.beeceptor.com/api/phonelists";
        public const string StraticsAPIURL = "https://api.ivr-platform.com/api/";
        public const string StraticsAudioAPIURL = "https://audio.ivr-platform.com/";

        public const string GetAuthenticationTokenURLSuffix = "token-auth";
        public const string GetAccountDetailsURLSuffix = "accounts/";
        public const string GetCallerIdsURLSuffix = "callerids/listByAccount?account=";
        public const string GetAudioPinsForAccountURLSuffix = "audio/pins/listByAccount?account=";
        public const string GetPhoneListStatusSuffix1 = "phonelists/";
        public const string GetPhoneListStatusSuffix2 = "/checkstatus";
        public const string GetAllSchedulesSuffix = "schedule/listByAccount?account=";
        public const string CreateScheduleSuffix = "schedule";
        public const string CreateSMSCampaignSuffix = "sms";
        public const string CreateCampaignSuffix = "rvms";
        public const string AssingListSuffix1 = "rvms/";
        public const string AssingListToSMSCampaignSuffix1 = "sms/";
        public const string AssingListSuffix2 = "/assignlist";
        public const string AssingListToSMSCampaignSuffix2 = "/lists";
    }
}
