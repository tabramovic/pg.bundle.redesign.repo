﻿
namespace ProfitGrabber.RVM.Forms
{
    partial class PhoneSelector
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._stMobileRank0 = new System.Windows.Forms.CheckBox();
            this._stMobileRank1 = new System.Windows.Forms.CheckBox();
            this._stMobileRank2 = new System.Windows.Forms.CheckBox();
            this._bOK = new System.Windows.Forms.Button();
            this._bCancel = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // _stMobileRank0
            // 
            this._stMobileRank0.AutoSize = true;
            this._stMobileRank0.Checked = true;
            this._stMobileRank0.CheckState = System.Windows.Forms.CheckState.Checked;
            this._stMobileRank0.Enabled = false;
            this._stMobileRank0.Location = new System.Drawing.Point(30, 82);
            this._stMobileRank0.Name = "_stMobileRank0";
            this._stMobileRank0.Size = new System.Drawing.Size(183, 17);
            this._stMobileRank0.TabIndex = 0;
            this._stMobileRank0.Text = "Skiptraced Mobile Phone Rank 0";
            this._stMobileRank0.UseVisualStyleBackColor = true;
            // 
            // _stMobileRank1
            // 
            this._stMobileRank1.AutoSize = true;
            this._stMobileRank1.Location = new System.Drawing.Point(30, 105);
            this._stMobileRank1.Name = "_stMobileRank1";
            this._stMobileRank1.Size = new System.Drawing.Size(183, 17);
            this._stMobileRank1.TabIndex = 1;
            this._stMobileRank1.Text = "Skiptraced Mobile Phone Rank 1";
            this._stMobileRank1.UseVisualStyleBackColor = true;
            // 
            // _stMobileRank2
            // 
            this._stMobileRank2.AutoSize = true;
            this._stMobileRank2.Location = new System.Drawing.Point(30, 128);
            this._stMobileRank2.Name = "_stMobileRank2";
            this._stMobileRank2.Size = new System.Drawing.Size(183, 17);
            this._stMobileRank2.TabIndex = 2;
            this._stMobileRank2.Text = "Skiptraced Mobile Phone Rank 2";
            this._stMobileRank2.UseVisualStyleBackColor = true;
            // 
            // _bOK
            // 
            this._bOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._bOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this._bOK.Location = new System.Drawing.Point(374, 185);
            this._bOK.Name = "_bOK";
            this._bOK.Size = new System.Drawing.Size(75, 23);
            this._bOK.TabIndex = 3;
            this._bOK.Text = "OK";
            this._bOK.UseVisualStyleBackColor = true;
            // 
            // _bCancel
            // 
            this._bCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._bCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this._bCancel.Location = new System.Drawing.Point(455, 185);
            this._bCancel.Name = "_bCancel";
            this._bCancel.Size = new System.Drawing.Size(75, 23);
            this._bCancel.TabIndex = 4;
            this._bCancel.Text = "Cancel";
            this._bCancel.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(368, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Please select if you want to upload more than one skiptraced phone number.";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(27, 35);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(264, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "The most likely match (Rank 0) is already pre-selected.";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(27, 55);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(443, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Rank 2 is the least likely to be an accurate phone number but still has potential" +
    " to be correct.";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(48, 157);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(469, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "For most people we recommend selecting rank 0 and 1.  You can adjust according to" +
    " your budget.";
            // 
            // PhoneSelector
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.ClientSize = new System.Drawing.Size(542, 220);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this._bCancel);
            this.Controls.Add(this._bOK);
            this.Controls.Add(this._stMobileRank2);
            this.Controls.Add(this._stMobileRank1);
            this.Controls.Add(this._stMobileRank0);
            this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "PhoneSelector";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Select skip traced phone numbers for upload to DropCowboy";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox _stMobileRank0;
        private System.Windows.Forms.CheckBox _stMobileRank1;
        private System.Windows.Forms.CheckBox _stMobileRank2;
        private System.Windows.Forms.Button _bOK;
        private System.Windows.Forms.Button _bCancel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
    }
}