﻿using System;
using System.Windows.Forms;

namespace ProfitGrabber.RVM.Forms
{
    public partial class PhoneSelector : Form
    {
        public PhoneSelector()
        {
            InitializeComponent();
        }

        public Tuple<bool, bool, bool> SelectedPhoneTypesFlags =>        
            new Tuple<bool, bool, bool>(true, _stMobileRank1.Checked, _stMobileRank2.Checked);        
    }
}
