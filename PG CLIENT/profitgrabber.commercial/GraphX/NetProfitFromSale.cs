﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using ProfitGrabber.Commercial.Objects;
using ProfitGrabber.Commercial.Generators;
using ZedGraph;


namespace ProfitGrabber.Commercial.GraphX
{
    public partial class NetProfitFromSale : Form
    {
        NetProfitOrTaxesDeductionGenerator _gen = new NetProfitOrTaxesDeductionGenerator();
        int _nrOfYears = 1;


        public NetProfitFromSale(NetProfitOrTaxesDeductionGenerator gen, int nrOfYears)
        {
            InitializeComponent();

            _gen = gen;
            _nrOfYears = nrOfYears;
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            CreateGraph(zedGraphControl1);
        }

        // Build the Chart
        private void CreateGraph(ZedGraphControl zgc)
        {
            // get a reference to the GraphPane
            GraphPane pane = zgc.GraphPane;

            // Set the Titles
            pane.Title.Text = "NET PROFIT FROM SALE ANALISYS";
            pane.XAxis.Title.Text = "Years";
            pane.YAxis.Title.Text = "NET ($)";

            LineItem netSaleProfitLossCurve = null;
            LineItem netIncomeProfitLossCurve = null;
            LineItem netProfitLossCurve = null;                        

            List<double> xVals = new List<double>();
            for (int i = 1; i <= _nrOfYears; i++)
                xVals.Add((double)i);

            _gen.InitGenerator(MultiyearAnalysisObject.Instance, ExpensesObject.Instance, LoansObject.Instance, _nrOfYears);
            _gen.GenerateValues(_nrOfYears);            

            List<double> yVals = new List<double>();

            if (null != _gen.NetSaleFromProfitLoss)
            {
                foreach (decimal decVal in _gen.NetSaleFromProfitLoss)
                    yVals.Add((double)decVal);

                netSaleProfitLossCurve = pane.AddCurve("NET SALE PROFIT (LOSS)", xVals.ToArray(), yVals.ToArray(), Color.Red, SymbolType.Diamond);
            }

            if (null != _gen.NetIncomeProfitLossCumulative)
            {
                yVals = new List<double>();
                foreach (decimal decVal in _gen.NetIncomeProfitLossCumulative)
                    yVals.Add((double)decVal);

                netIncomeProfitLossCurve = pane.AddCurve("NET INCOME PROFIT (LOSS)", xVals.ToArray(), yVals.ToArray(), Color.Blue, SymbolType.Circle);
            }

            if (null != _gen.NetProfitLoss)
            {
                yVals = new List<double>();
                foreach (decimal decVal in _gen.NetProfitLoss)
                    yVals.Add((double)decVal);

                netProfitLossCurve = pane.AddCurve("NET PROFIT (LOSS)", xVals.ToArray(), yVals.ToArray(), Color.Green, SymbolType.Square);
            }

            
            // Add gridlines to the plot, and make them gray
            pane.XAxis.MajorGrid.IsVisible = true;
            pane.YAxis.MajorGrid.IsVisible = true;

            pane.XAxis.MajorGrid.Color = Color.LightGray;
            pane.YAxis.MajorGrid.Color = Color.LightGray;

            // Make curves thicker
            netSaleProfitLossCurve.Line.Width = 4.0F;
            netIncomeProfitLossCurve.Line.Width = 4.0F;
            netProfitLossCurve.Line.Width = 4.0F;
            

            // Increase the symbol sizes, and fill them with solid white
            netSaleProfitLossCurve.Symbol.Size = 8.0F;
            netSaleProfitLossCurve.Symbol.Fill = new Fill(Color.White);

            netIncomeProfitLossCurve.Symbol.Size = 8.0F;
            netIncomeProfitLossCurve.Symbol.Fill = new Fill(Color.White);

            netProfitLossCurve.Symbol.Size = 8.0F;
            netProfitLossCurve.Symbol.Fill = new Fill(Color.White);
            

            // Add a background gradient fill to the axis frame
            pane.Chart.Fill = new Fill(Color.White, Color.Yellow, 45.0F);

            // Tell ZedGraph to refigure the
            // axes since the data have changed
            zgc.AxisChange();
        }
    }
}
