﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using ProfitGrabber.Commercial.Generators;
using ZedGraph;

namespace ProfitGrabber.Commercial.GraphX
{
    public partial class CashFlow : Form
    {
        CashFlowGenerator _cfGen = null;
        int _nrOfYears = 1;

        public CashFlow(CashFlowGenerator cfGen, int nrOfYears)
        {
            InitializeComponent();

            _cfGen = cfGen;
            _nrOfYears = nrOfYears;
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            CreateGraph(zedGraphControl1);
        }

        // Build the Chart
        private void CreateGraph(ZedGraphControl zgc)
        {
            // get a reference to the GraphPane
            GraphPane pane = zgc.GraphPane;

            // Set the Titles
            pane.Title.Text = "CASH FLOW ANALISYS";
            pane.XAxis.Title.Text = "Years";
            pane.YAxis.Title.Text = "CASH FLOW ($)";

            LineItem gsiCurve = null;
            LineItem goiCurve = null;
            LineItem noiCurve = null;
            LineItem cfbtCurve = null;
            LineItem cfatCurve = null;

            List<decimal> gsiValuesPerYears;
            List<decimal> grossOperatingIncomePerYears;
            List<decimal> netOperatingIncomePerYears;
            List<double> cashFlowBeforeTaxesPerYears;
            List<double> cashFlowAfterTaxesPerYears;

            List<double> xVals = new List<double>();
            for (int i = 1; i <= _nrOfYears; i++)
                xVals.Add((double)i);

            _cfGen.InitCurvesForGraph(_nrOfYears, out gsiValuesPerYears, out grossOperatingIncomePerYears, out netOperatingIncomePerYears, out cashFlowBeforeTaxesPerYears, out cashFlowAfterTaxesPerYears);

            List<double> yVals = new List<double>();

            if (null != gsiValuesPerYears)
            {
                foreach (decimal decVal in gsiValuesPerYears)
                    yVals.Add((double)decVal);

                gsiCurve = pane.AddCurve("GSI", xVals.ToArray(), yVals.ToArray(), Color.Red, SymbolType.Diamond);
            }

            if (null != grossOperatingIncomePerYears)
            {
                yVals = new List<double>();
                foreach (decimal decVal in grossOperatingIncomePerYears)
                    yVals.Add((double)decVal);

                goiCurve = pane.AddCurve("GOI", xVals.ToArray(), yVals.ToArray(), Color.Blue, SymbolType.Circle);
            }

            if (null != netOperatingIncomePerYears)
            {
                yVals = new List<double>();
                foreach (decimal decVal in netOperatingIncomePerYears)
                    yVals.Add((double)decVal);

                noiCurve = pane.AddCurve("NOI", xVals.ToArray(), yVals.ToArray(), Color.Green, SymbolType.Square);
            }

            if (null != cashFlowBeforeTaxesPerYears)
            {
                cfbtCurve = pane.AddCurve("CFBT", xVals.ToArray(), cashFlowBeforeTaxesPerYears.ToArray(), Color.Black, SymbolType.Star);
            }

            if (null != cashFlowAfterTaxesPerYears)
            {
                cfatCurve = pane.AddCurve("CFAT", xVals.ToArray(), cashFlowAfterTaxesPerYears.ToArray(), Color.Cyan, SymbolType.Triangle);
            }

            // Add gridlines to the plot, and make them gray
            pane.XAxis.MajorGrid.IsVisible = true;
            pane.YAxis.MajorGrid.IsVisible = true;

            pane.XAxis.MajorGrid.Color = Color.LightGray;
            pane.YAxis.MajorGrid.Color = Color.LightGray;

            // Make curves thicker
            gsiCurve.Line.Width = 4.0F;
            goiCurve.Line.Width = 4.0F;
            noiCurve.Line.Width = 4.0F;
            cfbtCurve.Line.Width = 4.0F;
            cfatCurve.Line.Width = 4.0F;
            

            // Increase the symbol sizes, and fill them with solid white
            gsiCurve.Symbol.Size = 8.0F;
            gsiCurve.Symbol.Fill = new Fill(Color.White);

            goiCurve.Symbol.Size = 8.0F;
            goiCurve.Symbol.Fill = new Fill(Color.White);

            noiCurve.Symbol.Size = 8.0F;
            noiCurve.Symbol.Fill = new Fill(Color.White);

            cfbtCurve.Symbol.Size = 8.0F;
            cfbtCurve.Symbol.Fill = new Fill(Color.White);

            cfatCurve.Symbol.Size = 8.0F;
            cfatCurve.Symbol.Fill = new Fill(Color.White);

            // Add a background gradient fill to the axis frame
            pane.Chart.Fill = new Fill(Color.White, Color.Yellow, 45.0F);

            // Tell ZedGraph to refigure the
            // axes since the data have changed
            zgc.AxisChange();
        }
    }
}
