using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

using ZedGraph;
using ProfitGrabber.Commercial.Objects;

namespace ProfitGrabber.Commercial.GraphX
{
	/// <summary>
	/// Summary description for CashFlowVsCashOnCashReturn.
	/// </summary>
	public class CashFlowVsCashOnCashReturn : System.Windows.Forms.Form
	{
		PointPairList _curve1 = new PointPairList();
		PointPairList _curve2 = new PointPairList();

		private ZedGraph.ZedGraphControl zedGraphControl1;
		private ZedGraph.ZedGraphControl zedGraphControl2;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public CashFlowVsCashOnCashReturn()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();			
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.zedGraphControl1 = new ZedGraph.ZedGraphControl();
			this.zedGraphControl2 = new ZedGraph.ZedGraphControl();
			this.SuspendLayout();
			// 
			// zedGraphControl1
			// 
			this.zedGraphControl1.Location = new System.Drawing.Point(0, 0);
			this.zedGraphControl1.Name = "zedGraphControl1";
			this.zedGraphControl1.ScrollGrace = 0;
			this.zedGraphControl1.ScrollMaxX = 0;
			this.zedGraphControl1.ScrollMaxY = 0;
			this.zedGraphControl1.ScrollMaxY2 = 0;
			this.zedGraphControl1.ScrollMinX = 0;
			this.zedGraphControl1.ScrollMinY = 0;
			this.zedGraphControl1.ScrollMinY2 = 0;
			this.zedGraphControl1.Size = new System.Drawing.Size(800, 290);
			this.zedGraphControl1.TabIndex = 0;
			// 
			// zedGraphControl2
			// 
			this.zedGraphControl2.Location = new System.Drawing.Point(0, 288);
			this.zedGraphControl2.Name = "zedGraphControl2";
			this.zedGraphControl2.ScrollGrace = 0;
			this.zedGraphControl2.ScrollMaxX = 0;
			this.zedGraphControl2.ScrollMaxY = 0;
			this.zedGraphControl2.ScrollMaxY2 = 0;
			this.zedGraphControl2.ScrollMinX = 0;
			this.zedGraphControl2.ScrollMinY = 0;
			this.zedGraphControl2.ScrollMinY2 = 0;
			this.zedGraphControl2.Size = new System.Drawing.Size(800, 290);
			this.zedGraphControl2.TabIndex = 1;
			// 
			// CashFlowVsCashOnCashReturn
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(794, 574);
			this.Controls.Add(this.zedGraphControl2);
			this.Controls.Add(this.zedGraphControl1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "CashFlowVsCashOnCashReturn";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.ResumeLayout(false);

		}
		#endregion

		public void SetDataForGSIRange(double gsi, double vacancyPerc, double otherIncome, double operatingExpensesPerc, double debtService, double downPayment, double gsiDelta)
		{
			// get a reference to the GraphPane
			GraphPane myPane = zedGraphControl1.GraphPane;
			GraphPane myPane2 = zedGraphControl2.GraphPane;

			// Set the Titles
			myPane.Title.Text = "CASH FLOW vs. GROSS SCHEDULED INCOME RANGE";
			myPane.XAxis.Title.Text = "GSI ($)";
			myPane.YAxis.Title.Text = "Cash Flow ($)";

			myPane2.Title.Text = "CASH-on-CASH RETURN vs. GROSS SCHEDULED INCOME RANGE";
			myPane2.XAxis.Title.Text = "GSI ($)";
			myPane2.YAxis.Title.Text = "Cash-on-Cash Return (%)";

			//cash flow = GSI - (%Vacancy * GSI) + OtherIncome - (%OperatingExpenses * GSI) - DebtService			
			//cash on cash return = downpayment / cashflow

			double[] gsiPoints = new double[5] {0, 0, 0, 0, 0};

			gsiPoints[0] = gsi * (100 - gsiDelta) / 100;
			gsiPoints[1] = gsi * (100 - gsiDelta / 2) / 100;
			gsiPoints[2] = gsi;
			gsiPoints[3] = gsi * (100 + gsiDelta / 2) / 100;
			gsiPoints[4] = gsi * (100 + gsiDelta) / 100;

			for (int i = 0; i < 5; i++)
			{
				double tempGsi = gsiPoints[i];
				double cashFlow = tempGsi - ((vacancyPerc / 100) * tempGsi) + otherIncome - ((operatingExpensesPerc / 100) * tempGsi) - debtService;
				_curve1.Add(gsiPoints[i], cashFlow);
				
				if (0 != cashFlow)
					_curve2.Add(gsiPoints[i], (cashFlow / downPayment) * 100);
			}
		}

		public void SetDataForOERange(double gsi, double vacancyPerc, double otherIncome, double operatingExpensesPerc, double debtService, double downPayment, double operatingExpensesPercDelta)
		{

			// get a reference to the GraphPane
			GraphPane myPane = zedGraphControl1.GraphPane;
			GraphPane myPane2 = zedGraphControl2.GraphPane;

			// Set the Titles
			myPane.Title.Text = "CASH FLOW vs. OPERATING EXPENSES RANGE";
			myPane.XAxis.Title.Text = "Operating Expenses (%)";
			myPane.YAxis.Title.Text = "Cash Flow ($)";

			myPane2.Title.Text = "CASH-on-CASH RETURN vs. OPERATING EXPENSES RANGE";
			myPane2.XAxis.Title.Text = "Operating Expenses (%)";
			myPane2.YAxis.Title.Text = "Cash-on-Cash Return (%)";

			double[] oePoints = new double[5] {0, 0, 0, 0, 0};

			oePoints[0] = operatingExpensesPerc - operatingExpensesPercDelta;
			oePoints[1] = operatingExpensesPerc - (operatingExpensesPercDelta / 2);
			oePoints[2] = operatingExpensesPerc;
			oePoints[3] = operatingExpensesPerc + (operatingExpensesPercDelta / 2);
			oePoints[4] = operatingExpensesPerc + operatingExpensesPercDelta;

						
			for (int i = 0; i < 5; i++)
			{				
				double tempOEPerc = oePoints[i];
				//Cash Flow = GSI - %Vacancy*GSI + OtherIncome - %OperatingExpenses*GSI � annualDebtService.
				double cashFlow = gsi - ((vacancyPerc / 100) * gsi) + otherIncome - ((tempOEPerc / 100) * gsi) - debtService;
				_curve1.Add(oePoints[i], cashFlow);
				
				//CoC Return = Cash Flow / Down Payment
				if (0 != cashFlow)
					_curve2.Add(oePoints[i], (cashFlow / downPayment) * 100);
			}
		}

		public void SetDataForLRRange(double gsi, double vacancyPerc, double otherIncome, double operatingExpensesPerc, double debtService, double downPayment, double loanRateChange)
		{

			// get a reference to the GraphPane
			GraphPane myPane = zedGraphControl1.GraphPane;
			GraphPane myPane2 = zedGraphControl2.GraphPane;

			// Set the Titles
			myPane.Title.Text = "CASH FLOW vs. 1st LOAN RATE RANGE";
			myPane.XAxis.Title.Text = "Loan Rate (%)";
			myPane.YAxis.Title.Text = "Cash Flow ($)";

			myPane2.Title.Text = "CASH-on-CASH RETURN vs. 1st LOAN RATE RANGE";
			myPane2.XAxis.Title.Text = "Loan Rate (%)";
			myPane2.YAxis.Title.Text = "Cash-on-Cash Return (%)";

			double[] loanRatePoints = new double[5] {0, 0, 0, 0, 0};

			//TAKE 1st LOAN PERC
			decimal currPerc = 0;
			try { currPerc = LoansObject.Instance.LoansProperty[0].RatePercValue; }
			catch {currPerc = 0;}

			if (0 != currPerc)	//continue
			{
				loanRatePoints[0] = (double)currPerc - loanRateChange;
				loanRatePoints[1] = (double)currPerc - (loanRateChange / 2);
				loanRatePoints[2] = (double)currPerc;
				loanRatePoints[3] = (double)currPerc + (loanRateChange / 2);
				loanRatePoints[4] = (double)currPerc + loanRateChange;


				bool fixedLoan = ("Fixed" == LoansObject.Instance.LoansProperty[0].LoanTypeProperty ? true : false);
				decimal amountValue = LoansObject.Instance.LoansProperty[0].AmountValue;
				decimal amortTermValue = LoansObject.Instance.LoansProperty[0].AmortTermValue;

				decimal secondLoanAmount = 12 * LoansObject.Instance.LoansProperty[1].MonthlyPaymentValue;
				decimal thirdLoanAmount = 12 * LoansObject.Instance.LoansProperty[2].MonthlyPaymentValue;

				for (int i = 0; i < 5; i++)
				{														
					decimal ratePercValue = (decimal)loanRatePoints[i];
					
					double newAnnualPayment = 12 * LoansObject.Instance.CalculatePeriodicPaymentAmount(fixedLoan, amountValue, ratePercValue, amortTermValue);
					newAnnualPayment += (double)secondLoanAmount;
					newAnnualPayment += (double)thirdLoanAmount;
					
					//Cash Flow = GSI - %Vacancy*GSI + OtherIncome - %OperatingExpenses*GSI � annualDebtService.
					double cashFlow = gsi - (vacancyPerc / 100) * gsi + otherIncome - (operatingExpensesPerc / 100 * gsi) - newAnnualPayment;
					_curve1.Add(loanRatePoints[i], cashFlow);
				
					//CoC Return = Cash Flow / Down Payment
					if (0 != cashFlow)
						_curve2.Add(loanRatePoints[i], (cashFlow / downPayment) * 100);
				}

			}						
		}
		
		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad (e);

			// Setup the graph
			CreateGraph( zedGraphControl1, zedGraphControl2 );			
		}

		// Build the Chart
		private void CreateGraph( ZedGraphControl zgc, ZedGraphControl zgc2 )
		{			
			GraphPane myPane = zgc.GraphPane;
			GraphPane myPane2 = zgc2.GraphPane;
			
	
			///////////////////////
			myPane.XAxis.MajorGrid.IsVisible = true;						
			myPane.YAxis.MajorGrid.IsVisible = true;
						
			myPane.XAxis.MajorGrid.Color = Color.LightGray;
			myPane.YAxis.MajorGrid.Color = Color.LightGray;

			myPane2.XAxis.MajorGrid.IsVisible = true;						
			myPane2.YAxis.MajorGrid.IsVisible = true;
						
			myPane2.XAxis.MajorGrid.Color = Color.LightGray;
			myPane2.YAxis.MajorGrid.Color = Color.LightGray;

			LineItem myCurve = myPane.AddCurve( string.Empty, _curve1, Color.Red, SymbolType.Diamond );
			LineItem myCurve2 = myPane2.AddCurve( string.Empty, _curve2, Color.Red, SymbolType.Star );

			// Make both curves thicker
			myCurve.Line.Width = 4.0F;			
			myCurve2.Line.Width = 4.0F;			
   
			// Fill the area under the curves
			myCurve.Line.Fill = new Fill( Color.White, Color.Blue, 45F );
			myCurve2.Line.Fill = new Fill( Color.White, Color.Blue, 45F );
   
			// Increase the symbol sizes, and fill them with solid white
			myCurve.Symbol.Size = 8.0F;			
			myCurve2.Symbol.Size = 8.0F;			

			myCurve.Symbol.Fill = new Fill( Color.White );			
			myCurve2.Symbol.Fill = new Fill( Color.White );			
   
			// Add a background gradient fill to the axis frame
			myPane.Chart.Fill = new Fill( Color.White, 	Color.Yellow, 45.0F );
			myPane2.Chart.Fill = new Fill( Color.White, 	Color.Yellow, 45.0F );
																										
			zgc.AxisChange();
			zgc2.AxisChange();
		}
	}
}
