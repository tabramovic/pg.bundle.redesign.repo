using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

using ZedGraph;

namespace ProfitGrabber.Commercial.GraphX
{
	/// <summary>
	/// Summary description for CapRateVsPriceValue.
	/// </summary>
	public class CapRateVsPriceValue : System.Windows.Forms.Form
	{
		PointPairList _points = new PointPairList();
		//Point[] _points = new Point[5] { new Point(0, 0), new Point(0, 0), new Point(0, 0), new Point(0, 0), new Point(0, 0) };

		private ZedGraph.ZedGraphControl zedGraphControl1;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public CapRateVsPriceValue()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();			
		}		

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.zedGraphControl1 = new ZedGraph.ZedGraphControl();
			this.SuspendLayout();
			// 
			// zedGraphControl1
			// 
			this.zedGraphControl1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.zedGraphControl1.Location = new System.Drawing.Point(0, 0);
			this.zedGraphControl1.Name = "zedGraphControl1";
			this.zedGraphControl1.ScrollGrace = 0;
			this.zedGraphControl1.ScrollMaxX = 0;
			this.zedGraphControl1.ScrollMaxY = 0;
			this.zedGraphControl1.ScrollMaxY2 = 0;
			this.zedGraphControl1.ScrollMinX = 0;
			this.zedGraphControl1.ScrollMinY = 0;
			this.zedGraphControl1.ScrollMinY2 = 0;
			this.zedGraphControl1.Size = new System.Drawing.Size(694, 474);
			this.zedGraphControl1.TabIndex = 0;
			// 
			// CapRateVsPriceValue
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(694, 474);
			this.Controls.Add(this.zedGraphControl1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "CapRateVsPriceValue";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.ResumeLayout(false);

		}
		#endregion

		public void SetData(double actualCapRate, double delta, double noi)
		{
			//PRICE = NOI / CAP RATE			

			_points.Add(actualCapRate - delta, noi / ((actualCapRate - delta) / 100));
			_points.Add(actualCapRate - delta / 2, noi / ((actualCapRate - delta / 2) / 100));
			_points.Add(actualCapRate, noi / (actualCapRate / 100));
			_points.Add(actualCapRate + delta / 2, noi / ((actualCapRate + delta / 2) / 100));
			_points.Add(actualCapRate + delta, noi / ((actualCapRate + delta) / 100));
		}

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad (e);

			// Setup the graph
			CreateGraph( zedGraphControl1 );
		}

		// Build the Chart
		private void CreateGraph( ZedGraphControl zgc )
		{
			// get a reference to the GraphPane
			GraphPane myPane = zgc.GraphPane;

			// Set the Titles
			myPane.Title.Text = "Price / Value vs. Cap Rate";
			myPane.XAxis.Title.Text = "Cap Rate (%)";
			myPane.YAxis.Title.Text = "Price or Value ($)";
												
			// Generate a red curve with diamond
			// symbols, and "Porsche" in the legend
			LineItem myCurve = myPane.AddCurve( string.Empty, _points, Color.Red, SymbolType.Diamond );

			// Add gridlines to the plot, and make them gray
			
			myPane.XAxis.MajorGrid.IsVisible = true;						
			myPane.YAxis.MajorGrid.IsVisible = true;
						
			myPane.XAxis.MajorGrid.Color = Color.LightGray;
			myPane.YAxis.MajorGrid.Color = Color.LightGray;			

			// Make both curves thicker
			myCurve.Line.Width = 4.0F;			
   
			// Fill the area under the curves
			myCurve.Line.Fill = new Fill( Color.White, Color.Blue, 45F );			
   
			// Increase the symbol sizes, and fill them with solid white
			myCurve.Symbol.Size = 8.0F;			
			myCurve.Symbol.Fill = new Fill( Color.White );			
   
			// Add a background gradient fill to the axis frame
			myPane.Chart.Fill = new Fill( Color.White, 	Color.Yellow, 45.0F );
			
			// Tell ZedGraph to refigure the
			// axes since the data have changed
			zgc.AxisChange();
		}

	}
}
