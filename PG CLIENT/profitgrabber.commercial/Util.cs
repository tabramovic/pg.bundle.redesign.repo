﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitGrabber.Commercial
{
    public enum ReportOriginator
    {
        BuildingInfo = 0, 
        IncomeSummary = 1,
        IncomeRentRoll = 2,
        ExpensesSummary = 3,
        ExpensesDetails = 4,
        LoanInfo = 5,
        FinancialAnalisys = 6,
        CashFlow = 7,
        NetProfitFromSale = 8,
        TaxesAndDeductions = 9,
        NetProfitGraph = 10,
    }

    public class Util
    {
        public static decimal GetDecimalFromString(string text)
        {
            decimal retVal = 0;
            try
            {
                if (null != text && string.Empty != text)
                {
                    retVal = Convert.ToDecimal(text);
                }
                else
                {
                    retVal = 0;
                }
            }
            catch
            {
                retVal = 0;
            }
            return retVal;
        }
    }
}
