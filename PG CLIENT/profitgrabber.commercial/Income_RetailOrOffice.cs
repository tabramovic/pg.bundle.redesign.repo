﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Printing;
using System.Data;
using System.Text;
using System.Windows.Forms;

//Added
using ProfitGrabber.Common;
using ProfitGrabber.Commercial.Objects;
using ProfitGrabber.Commercial.Generators;

namespace ProfitGrabber.Commercial
{
    public partial class Income_RetailOrOffice : UserControl
    {
        IAutoSave _autoSave = null;
        const string CurrFormatter = "#,##0.00";
        string _address = string.Empty;

        IncomeGenerator _iGen = new IncomeGenerator();

        public Income_RetailOrOffice()
        {
            InitializeComponent();
            InitIncomeListViews();
            AddEventHandlers();
            HandleListViewExes();
        }

        protected override void OnLoad(EventArgs e)
        {
            //base.OnLoad(e);
            HandleListViewExes();
        }

        public void SetAutoSaveInterface(IAutoSave iAutoSaveIfc)
        {
            _autoSave = iAutoSaveIfc;
            AddAutoSaveEventHandlers();
        }

        #region ADD / REMOVE EVENT HANDLERS
        void AddEventHandlers()
        {
            this._I_PI_VacancyOrCreditLossPerc.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.I_P_VacancyPerc_OnKeyPressed);
            this._I_PI_VacancyOrCreditLossPerc.Leave += new System.EventHandler(this.I_P_VacancyPerc_OnTextBoxLeave);
            this._I_PI_OtherIncome.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.I_P_OtherIncome_OnKeyPressed);
            this._I_PI_OtherIncome.Leave += new System.EventHandler(this.I_P_OtherIncome_OnTextBoxLeave);
            this._I_PI_GrossRent.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.I_P_GSI_OnKeyPressed);
            this._I_AI_VacancyOrCreditLossPerc.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.I_AI_LossPerc_OnKeyPressed);
            this._I_AI_VacancyOrCreditLossPerc.Leave += new System.EventHandler(this.I_AI_LossPerc_OnTextBoxLeave);
            this._I_AI_VacancyOrCreditLossPerc.Enter += new System.EventHandler(this.OnTextBoxEnter);
            this._I_AI_VacancyOrCreditLoss.Enter += new System.EventHandler(this.OnTextBoxEnter);
            this._I_AI_GrossRent.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.I_AI_GSI_OnKeyPressed);
            this._I_AI_GrossRent.Leave += new System.EventHandler(this.I_AI_GSI_OnTextBoxLeave);
            this._I_AI_GrossRent.Enter += new System.EventHandler(this.OnTextBoxEnter);

            //List View
            _I_Summary.OnListViewExItemChanged += new ListViewEx.ListViewExItemChanged(On_I_Summary_OnListViewExItemChanged);
            _I_Summary.OnListViewExItemEntered += new ListViewEx.ListViewExItemEntered(On_I_Summary_OnListViewExItemEntered);

            //Space Area
            _I_S_Space.KeyPress += new KeyPressEventHandler(_I_S_Space_OnKeyPressed);
            _I_S_Space.Leave += new EventHandler(_I_S_Space_OnTextBoxLeave);
            _I_S_Comments.KeyPress += new KeyPressEventHandler(_I_S_Comments_OnKeyPressed);
            _I_S_Comments.Leave += new EventHandler(_I_S_Comments_OnTextBoxLeave);
            _I_S_L_Length.KeyPress += new KeyPressEventHandler(On_I_S_L_Length_KeyPress);
            _I_S_L_Length.Leave += new EventHandler(On_I_S_L_Length_Leave); 
            _I_S_L_Type.SelectedIndexChanged += new EventHandler(On_I_S_L_Type_SelectedIndexChanged);
            _I_S_L_StartDate.ValueChanged += new EventHandler(On_I_S_L_StartDate_ValueChanged);
            _I_S_L_EndDate.ValueChanged += new EventHandler(On_I_S_L_EndDate_ValueChanged);
            _I_S_L_LeaseComments.KeyPress += new KeyPressEventHandler(On_I_S_L_LeaseComments_KeyPress);
            _I_S_L_LeaseComments.Leave += new EventHandler(On_I_S_L_LeaseComments_Leave);
            _I_S_F_TIAllowance.KeyPress += new KeyPressEventHandler(On_I_S_F_TIAllowance_KeyPress);
            _I_S_F_TIAllowance.Leave += new EventHandler(On_I_S_F_TIAllowance_Leave);
            _I_S_F_ExpensesStop.KeyPress += new KeyPressEventHandler(On_I_S_F_ExpensesStop_KeyPress);
            _I_S_F_ExpensesStop.Leave += new EventHandler(On_I_S_F_ExpensesStop_Leave);
            _I_S_F_NNNCharges.KeyPress += new KeyPressEventHandler(On_I_S_F_NNNCharges_KeyPress);
            _I_S_F_NNNCharges.Leave += new EventHandler(On_I_S_F_NNNCharges_Leave);
            _I_S_F_CAMCharges.KeyPress += new KeyPressEventHandler(On_I_S_F_CAMCharges_KeyPress);
            _I_S_F_CAMCharges.Leave += new EventHandler(On_I_S_F_CAMCharges_Leave);
            _I_S_F_TennantComment.KeyPress += new KeyPressEventHandler(On_I_S_F_TennantComment_KeyPress);
            _I_S_F_TennantComment.Leave += new EventHandler(On_I_S_F_TennantComment_Leave);

            //REPORTS
            _printIncomeReport.Click += new EventHandler(On_PrintIncomeReport_Click);
        }
        
        void RemoveEventHandlers()
        {
            this._I_PI_VacancyOrCreditLossPerc.KeyPress -= new System.Windows.Forms.KeyPressEventHandler(this.I_P_VacancyPerc_OnKeyPressed);
            this._I_PI_VacancyOrCreditLossPerc.Leave -= new System.EventHandler(this.I_P_VacancyPerc_OnTextBoxLeave);
            this._I_PI_OtherIncome.KeyPress -= new System.Windows.Forms.KeyPressEventHandler(this.I_P_OtherIncome_OnKeyPressed);
            this._I_PI_OtherIncome.Leave -= new System.EventHandler(this.I_P_OtherIncome_OnTextBoxLeave);
            this._I_PI_GrossRent.KeyPress -= new System.Windows.Forms.KeyPressEventHandler(this.I_P_GSI_OnKeyPressed);
            this._I_AI_VacancyOrCreditLossPerc.KeyPress -= new System.Windows.Forms.KeyPressEventHandler(this.I_AI_LossPerc_OnKeyPressed);
            this._I_AI_VacancyOrCreditLossPerc.Leave -= new System.EventHandler(this.I_AI_LossPerc_OnTextBoxLeave);
            this._I_AI_VacancyOrCreditLossPerc.Enter -= new System.EventHandler(this.OnTextBoxEnter);
            this._I_AI_VacancyOrCreditLoss.Enter -= new System.EventHandler(this.OnTextBoxEnter);
            this._I_AI_GrossRent.KeyPress -= new System.Windows.Forms.KeyPressEventHandler(this.I_AI_GSI_OnKeyPressed);
            this._I_AI_GrossRent.Leave -= new System.EventHandler(this.I_AI_GSI_OnTextBoxLeave);
            this._I_AI_GrossRent.Enter -= new System.EventHandler(this.OnTextBoxEnter);

            //List View
            _I_Summary.OnListViewExItemChanged -= new ListViewEx.ListViewExItemChanged(On_I_Summary_OnListViewExItemChanged);
            _I_Summary.OnListViewExItemEntered += new ListViewEx.ListViewExItemEntered(On_I_Summary_OnListViewExItemEntered);

            //Space Area
            _I_S_Space.KeyPress -= new KeyPressEventHandler(_I_S_Space_OnKeyPressed);
            _I_S_Space.Leave -= new EventHandler(_I_S_Space_OnTextBoxLeave);
            _I_S_Comments.KeyPress -= new KeyPressEventHandler(_I_S_Comments_OnKeyPressed);
            _I_S_Comments.Leave -= new EventHandler(_I_S_Comments_OnTextBoxLeave);
            _I_S_L_Length.KeyPress -= new KeyPressEventHandler(On_I_S_L_Length_KeyPress);
            _I_S_L_Length.Leave -= new EventHandler(On_I_S_L_Length_Leave);
            _I_S_L_Type.SelectedIndexChanged -= new EventHandler(On_I_S_L_Type_SelectedIndexChanged);
            _I_S_L_StartDate.ValueChanged -= new EventHandler(On_I_S_L_StartDate_ValueChanged);
            _I_S_L_EndDate.ValueChanged -= new EventHandler(On_I_S_L_EndDate_ValueChanged);
            _I_S_L_LeaseComments.KeyPress -= new KeyPressEventHandler(On_I_S_L_LeaseComments_KeyPress);
            _I_S_L_LeaseComments.Leave -= new EventHandler(On_I_S_L_LeaseComments_Leave);
            _I_S_F_TIAllowance.KeyPress -= new KeyPressEventHandler(On_I_S_F_TIAllowance_KeyPress);
            _I_S_F_TIAllowance.Leave -= new EventHandler(On_I_S_F_TIAllowance_Leave);
            _I_S_F_ExpensesStop.KeyPress -= new KeyPressEventHandler(On_I_S_F_ExpensesStop_KeyPress);
            _I_S_F_ExpensesStop.Leave -= new EventHandler(On_I_S_F_ExpensesStop_Leave);
            _I_S_F_NNNCharges.KeyPress -= new KeyPressEventHandler(On_I_S_F_NNNCharges_KeyPress);
            _I_S_F_NNNCharges.Leave -= new EventHandler(On_I_S_F_NNNCharges_Leave);
            _I_S_F_CAMCharges.KeyPress -= new KeyPressEventHandler(On_I_S_F_CAMCharges_KeyPress);
            _I_S_F_CAMCharges.Leave -= new EventHandler(On_I_S_F_CAMCharges_Leave);
            _I_S_F_TennantComment.KeyPress -= new KeyPressEventHandler(On_I_S_F_TennantComment_KeyPress);
            _I_S_F_TennantComment.Leave -= new EventHandler(On_I_S_F_TennantComment_Leave); 
           
            //REPORTS
            _printIncomeReport.Click -= new EventHandler(On_PrintIncomeReport_Click);

        }
        #endregion                

        int _currentRow = -1;

        #region EVENT HANDLERS
        void On_I_Summary_OnListViewExItemChanged(int row, int col, string val)
        {
            if (-1 != row && -1 != col)
            {
                _currentRow = row;
                //MAP TO OBJECT MODEL
                switch (col)
                {
                    case 0:
                        IncomeObject.Instance.RetailOfficeSummaryObject[row].Space = val;
                        SetSpaceExt(val);
                        break;

                    case 1:
                        IncomeObject.Instance.RetailOfficeSummaryObject[row].Occupied = val;
                        break;

                    case 2:
                        IncomeObject.Instance.RetailOfficeSummaryObject[row].Tennant = val;
                        break;

                    case 3:
                        IncomeObject.Instance.RetailOfficeSummaryObject[row].Floor= val;
                        break;

                    case 4:
                        IncomeObject.Instance.RetailOfficeSummaryObject[row].SqFt = val;                        
                        break;

                    case 5:
                        IncomeObject.Instance.RetailOfficeSummaryObject[row].AR_usdPerMonth = val;                        
                        _I_Summary.Items[row].SubItems[6].Text = IncomeObject.Instance.RetailOfficeSummaryObject[row].P_AR_UsdPerYearPerItem.ToString(CurrFormatter);
                        _I_Summary.Items[row].SubItems[7].Text = IncomeObject.Instance.RetailOfficeSummaryObject[row].P_AR_UsdPerSqFtPerMonthPerItem.ToString(CurrFormatter);
                        _I_Summary.Items[row].SubItems[8].Text = IncomeObject.Instance.RetailOfficeSummaryObject[row].P_AR_UsdPerSqFtPerYearPerItem.ToString(CurrFormatter);

                        _I_Summary.Items[row].SubItems[5].Text = IncomeObject.Instance.RetailOfficeSummaryObject[row].P_AR_UsdPerMonthPerItem.ToString(CurrFormatter);
                        break;

                    case 6:
                        IncomeObject.Instance.RetailOfficeSummaryObject[row].AR_usdPerYear = val;
                        _I_Summary.Items[row].SubItems[5].Text = IncomeObject.Instance.RetailOfficeSummaryObject[row].P_AR_UsdPerMonthPerItem.ToString(CurrFormatter);
                        _I_Summary.Items[row].SubItems[7].Text = IncomeObject.Instance.RetailOfficeSummaryObject[row].P_AR_UsdPerSqFtPerMonthPerItem.ToString(CurrFormatter);
                        _I_Summary.Items[row].SubItems[8].Text = IncomeObject.Instance.RetailOfficeSummaryObject[row].P_AR_UsdPerSqFtPerYearPerItem.ToString(CurrFormatter);

                        _I_Summary.Items[row].SubItems[6].Text = IncomeObject.Instance.RetailOfficeSummaryObject[row].P_AR_UsdPerYearPerItem.ToString(CurrFormatter);
                        break;

                    case 7:
                        IncomeObject.Instance.RetailOfficeSummaryObject[row].AR_usdPerSqFtPerMonth = val;
                        _I_Summary.Items[row].SubItems[5].Text = IncomeObject.Instance.RetailOfficeSummaryObject[row].P_AR_UsdPerMonthPerItem.ToString(CurrFormatter);
                        _I_Summary.Items[row].SubItems[6].Text = IncomeObject.Instance.RetailOfficeSummaryObject[row].P_AR_UsdPerYearPerItem.ToString(CurrFormatter);
                        _I_Summary.Items[row].SubItems[8].Text = IncomeObject.Instance.RetailOfficeSummaryObject[row].P_AR_UsdPerSqFtPerYearPerItem.ToString(CurrFormatter);

                        _I_Summary.Items[row].SubItems[7].Text = IncomeObject.Instance.RetailOfficeSummaryObject[row].P_AR_UsdPerSqFtPerMonthPerItem.ToString(CurrFormatter);                        
                        break;

                    case 8:
                        IncomeObject.Instance.RetailOfficeSummaryObject[row].AR_usdPerSqFtPerYear = val;
                        _I_Summary.Items[row].SubItems[5].Text = IncomeObject.Instance.RetailOfficeSummaryObject[row].P_AR_UsdPerMonthPerItem.ToString(CurrFormatter);
                        _I_Summary.Items[row].SubItems[6].Text = IncomeObject.Instance.RetailOfficeSummaryObject[row].P_AR_UsdPerYearPerItem.ToString(CurrFormatter);
                        _I_Summary.Items[row].SubItems[7].Text = IncomeObject.Instance.RetailOfficeSummaryObject[row].P_AR_UsdPerSqFtPerMonthPerItem.ToString(CurrFormatter);

                        _I_Summary.Items[row].SubItems[8].Text = IncomeObject.Instance.RetailOfficeSummaryObject[row].P_AR_UsdPerSqFtPerYearPerItem.ToString(CurrFormatter);
                        break;

                    case 9:
                        IncomeObject.Instance.RetailOfficeSummaryObject[row].PR_usdPerMonth = val;                        
                        _I_Summary.Items[row].SubItems[10].Text = IncomeObject.Instance.RetailOfficeSummaryObject[row].P_PR_UsdPerYearPerItem.ToString(CurrFormatter);
                        _I_Summary.Items[row].SubItems[11].Text = IncomeObject.Instance.RetailOfficeSummaryObject[row].P_PR_UsdPerSqFtPerMonthPerItem.ToString(CurrFormatter);
                        _I_Summary.Items[row].SubItems[12].Text = IncomeObject.Instance.RetailOfficeSummaryObject[row].P_PR_UsdPerSqFtPerYearPerItem.ToString(CurrFormatter);

                        _I_Summary.Items[row].SubItems[9].Text = IncomeObject.Instance.RetailOfficeSummaryObject[row].P_PR_UsdPerMonthPerItem.ToString(CurrFormatter);
                        break;

                    case 10:
                        IncomeObject.Instance.RetailOfficeSummaryObject[row].PR_usdPerYear = val;
                        _I_Summary.Items[row].SubItems[9].Text = IncomeObject.Instance.RetailOfficeSummaryObject[row].P_PR_UsdPerMonthPerItem.ToString(CurrFormatter);
                        _I_Summary.Items[row].SubItems[11].Text = IncomeObject.Instance.RetailOfficeSummaryObject[row].P_PR_UsdPerSqFtPerMonthPerItem.ToString(CurrFormatter);
                        _I_Summary.Items[row].SubItems[12].Text = IncomeObject.Instance.RetailOfficeSummaryObject[row].P_PR_UsdPerSqFtPerYearPerItem.ToString(CurrFormatter);

                        _I_Summary.Items[row].SubItems[10].Text = IncomeObject.Instance.RetailOfficeSummaryObject[row].P_PR_UsdPerYearPerItem.ToString(CurrFormatter);
                        break;

                    case 11:
                        IncomeObject.Instance.RetailOfficeSummaryObject[row].PR_usdPerSqFtPerMonth = val;
                        _I_Summary.Items[row].SubItems[9].Text = IncomeObject.Instance.RetailOfficeSummaryObject[row].P_PR_UsdPerMonthPerItem.ToString(CurrFormatter);
                        _I_Summary.Items[row].SubItems[10].Text = IncomeObject.Instance.RetailOfficeSummaryObject[row].P_PR_UsdPerYearPerItem.ToString(CurrFormatter);
                        _I_Summary.Items[row].SubItems[12].Text = IncomeObject.Instance.RetailOfficeSummaryObject[row].P_PR_UsdPerSqFtPerYearPerItem.ToString(CurrFormatter);

                        _I_Summary.Items[row].SubItems[11].Text = IncomeObject.Instance.RetailOfficeSummaryObject[row].P_PR_UsdPerSqFtPerMonthPerItem.ToString(CurrFormatter);                        
                        break;

                    case 12:
                        IncomeObject.Instance.RetailOfficeSummaryObject[row].PR_usdPerSqFtPerYear = val;
                        _I_Summary.Items[row].SubItems[9].Text = IncomeObject.Instance.RetailOfficeSummaryObject[row].P_PR_UsdPerMonthPerItem.ToString(CurrFormatter);
                        _I_Summary.Items[row].SubItems[10].Text = IncomeObject.Instance.RetailOfficeSummaryObject[row].P_PR_UsdPerYearPerItem.ToString(CurrFormatter);
                        _I_Summary.Items[row].SubItems[11].Text = IncomeObject.Instance.RetailOfficeSummaryObject[row].P_PR_UsdPerSqFtPerMonthPerItem.ToString(CurrFormatter);

                        _I_Summary.Items[row].SubItems[12].Text = IncomeObject.Instance.RetailOfficeSummaryObject[row].P_PR_UsdPerSqFtPerYearPerItem.ToString(CurrFormatter);
                        break;

                }

                _I_UTS_TotalOccupied.Text = IncomeObject.Instance.GetTotalOccupied().ToString(CurrFormatter);
                _I_UTS_TotalSqFt.Text = IncomeObject.Instance.GetTotalSqFt().ToString(CurrFormatter);
                _I_S_Total_AR_UsdPerMonth.Text = IncomeObject.Instance.GetTotal_AR_UsdPerMonth().ToString(CurrFormatter);
                _I_S_Total_AR_UsdPerYear.Text = IncomeObject.Instance.GetTotal_AR_UsdPerYear().ToString(CurrFormatter);
                _I_S_Total_AR_UsdPerSqFtPerMonth.Text = IncomeObject.Instance.GetTotal_AR_UsdPerMonthPerSqFt().ToString(CurrFormatter);
                _I_S_Total_PR_UsdPerMonth.Text = IncomeObject.Instance.GetTotal_AR_UsdPerYearPerSqFt().ToString(CurrFormatter);
                _I_UTS_Total_AR_UsdPerSqFt.Text = IncomeObject.Instance.GetTotal_PR_UsdPerMonth().ToString(CurrFormatter);
                _I_S_Total_PR_UsdPerYear.Text = IncomeObject.Instance.GetTotal_PR_UsdPerYear().ToString(CurrFormatter);
                _I_S_Total_PR_UsdPerSqFtPerMonth.Text = IncomeObject.Instance.GetTotal_PR_UsdPerMonthPerSqFt().ToString(CurrFormatter);
                _I_S_Total_PR_UsdPerSqftPerYear.Text = IncomeObject.Instance.GetTotal_PR_UsdPerYearPerSqFt().ToString(CurrFormatter);                
            }
        }

        void On_I_Summary_OnListViewExItemEntered(int row, int col)
        {
            //SPACE AREA
            DateTime endDate = DateTime.Now;
            try { endDate = Convert.ToDateTime(IncomeObject.Instance.RetailOfficeSummaryObject[row].SpaceDetails.LeaseEndDate); }
            catch { endDate = DateTime.Now; }
            _I_S_L_EndDate.Value = endDate;

            DateTime startDate = DateTime.Now;
            try { startDate = Convert.ToDateTime(IncomeObject.Instance.RetailOfficeSummaryObject[row].SpaceDetails.LeaseStartDate); }
            catch { startDate = DateTime.Now; }
            _I_S_L_StartDate.Value = startDate;

            _I_S_L_Type.SelectedText = IncomeObject.Instance.RetailOfficeSummaryObject[row].SpaceDetails.LeaseType;
            _I_S_F_TennantComment.Text = IncomeObject.Instance.RetailOfficeSummaryObject[row].SpaceDetails.FinancialComments;
            _I_S_F_CAMCharges.Text = IncomeObject.Instance.RetailOfficeSummaryObject[row].SpaceDetails.FinancialCAMCharges;
            _I_S_F_NNNCharges.Text = IncomeObject.Instance.RetailOfficeSummaryObject[row].SpaceDetails.FinancialNNNCharges;
            _I_S_F_ExpensesStop.Text = IncomeObject.Instance.RetailOfficeSummaryObject[row].SpaceDetails.FinancialExpensesStop;
            _I_S_F_TIAllowance.Text = IncomeObject.Instance.RetailOfficeSummaryObject[row].SpaceDetails.FinancialTIAllowance;
            _I_S_L_LeaseComments.Text = IncomeObject.Instance.RetailOfficeSummaryObject[row].SpaceDetails.LeaseComments;
            _I_S_L_Length.Text = IncomeObject.Instance.RetailOfficeSummaryObject[row].SpaceDetails.LeaseLength;
            _I_S_Comments.Text = IncomeObject.Instance.RetailOfficeSummaryObject[row].SpaceDetails.Comments;
            _I_S_Space.Text = IncomeObject.Instance.RetailOfficeSummaryObject[row].SpaceDetails.P_Space;
        }
        
        void On_I_S_L_EndDate_ValueChanged(object sender, EventArgs e)
        {
            HandleEndDate();
        }
        
        void On_I_S_L_StartDate_ValueChanged(object sender, EventArgs e)
        {
            HandleStartDate();
        }
        
        void On_I_S_L_Type_SelectedIndexChanged(object sender, EventArgs e)
        {
            HandleType();
        }        

        void On_I_S_F_TennantComment_Leave(object sender, EventArgs e)
        {
            HandleTennantComments();
        }

        void On_I_S_F_TennantComment_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13 || e.KeyChar == 27)
                HandleTennantComments();
        }
        
        void On_I_S_F_CAMCharges_Leave(object sender, EventArgs e)
        {
            HandleCAMCharges();
        }

        void On_I_S_F_CAMCharges_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13 || e.KeyChar == 27)
                HandleCAMCharges(); 
        }
        
        void On_I_S_F_NNNCharges_Leave(object sender, EventArgs e)
        {
            HandleNNNCharges();
        }

        void On_I_S_F_NNNCharges_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13 || e.KeyChar == 27)
                HandleNNNCharges();
        }
       
        void On_I_S_F_ExpensesStop_Leave(object sender, EventArgs e)
        {
            HandleExpensesStop();
        }

        void On_I_S_F_ExpensesStop_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13 || e.KeyChar == 27)
                HandleExpensesStop();
        }
       
        void On_I_S_F_TIAllowance_Leave(object sender, EventArgs e)
        {
            HandleAllowance();
        }

        void On_I_S_F_TIAllowance_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13 || e.KeyChar == 27)
                HandleAllowance();
        }        

        void On_I_S_L_LeaseComments_Leave(object sender, EventArgs e)
        {
            HandleLeaseComments();
        }

        void On_I_S_L_LeaseComments_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13 || e.KeyChar == 27)
                HandleLeaseComments();
        }        

        void On_I_S_L_Length_Leave(object sender, EventArgs e)
        {
            HandleLength();
        }

        void On_I_S_L_Length_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13 || e.KeyChar == 27)
                HandleLength();
        }        

        void _I_S_Comments_OnTextBoxLeave(object sender, EventArgs e)
        {
            HandleComments();
        }

        void _I_S_Comments_OnKeyPressed(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13 || e.KeyChar == 27)
                HandleComments();
        }
        
        void _I_S_Space_OnTextBoxLeave(object sender, EventArgs e)
        {
            HandleSpace();
        }

        void _I_S_Space_OnKeyPressed(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13 || e.KeyChar == 27)
                HandleSpace();
        }
        #endregion

        #region INIT LISTVIEW
        void InitIncomeListViews()
        {
            for (int i = 0; i < IncomeObject.UTSMaxItems; i++)
            {
                ListViewItem lvi = new ListViewItem();
                lvi.SubItems.Add(string.Empty);
                lvi.SubItems.Add(string.Empty);
                lvi.SubItems.Add(string.Empty);
                lvi.SubItems.Add(string.Empty);
                lvi.SubItems.Add(string.Empty);
                lvi.SubItems.Add(string.Empty);
                lvi.SubItems.Add(string.Empty);
                lvi.SubItems.Add(string.Empty);
                lvi.SubItems.Add(string.Empty);
                lvi.SubItems.Add(string.Empty);
                lvi.SubItems.Add(string.Empty);
                lvi.SubItems.Add(string.Empty);

                //Add Tag
                lvi.Tag = i;

                //Color readonly fields
                _I_Summary.Items.Add(lvi);

            }            
        }
        #endregion

        #region HANDLE LIST VIEWS
        void HandleListViewExes()
        {
            //UNIT TYPE SUMMARY
            _I_Summary.AddSubItem = true;
            _I_Summary.HideComboAfterSelChange = true;

            //Add EDITABLE cells
            _I_Summary.AddEditableCell(-1, 0);
            _I_Summary.AddEditableCell(-1, 2);
            _I_Summary.AddEditableCell(-1, 4);
            _I_Summary.AddEditableCell(-1, 5);
            _I_Summary.AddEditableCell(-1, 6);
            _I_Summary.AddEditableCell(-1, 7);
            _I_Summary.AddEditableCell(-1, 8);
            _I_Summary.AddEditableCell(-1, 9);
            _I_Summary.AddEditableCell(-1, 10);
            _I_Summary.AddEditableCell(-1, 11);
            _I_Summary.AddEditableCell(-1, 12);

            //Create data for COMBOBOX
            StringCollection occupied = new StringCollection();
            occupied.AddRange(new string[] { string.Empty, "Yes", "No" });

            StringCollection floor = new StringCollection();
            floor.AddRange(new string[] { string.Empty, "1", "2", "3", "4", "5", "6", "7", "8", "9", "10" });

            //Set COMBOBOXES
            _I_Summary.AddComboBoxCell(-1, 1, occupied);
            _I_Summary.AddComboBoxCell(-1, 3, floor);            
        }
        #endregion

        #region INCOME TEXTBOX HANDLERS
        private void I_AI_GSI_OnTextBoxLeave(object sender, System.EventArgs e)
        {
            HandleActualIncomes_GSI();
        }

        private void I_AI_GSI_OnKeyPressed(object sender, System.Windows.Forms.KeyPressEventArgs e)
        {
            if (e.KeyChar == 13 || e.KeyChar == 27)
                HandleActualIncomes_GSI();
        }

        void HandleActualIncomes_GSI()
        {
            //SET NEW PROPERTY (take from GUI)
            IncomeObject.Instance.ActualIncomeObject.GrossRent = _I_AI_GrossRent.Text;

            try
            {
                //TRY TO CONVERT TO CURR FORMATTER
                decimal dTemp = decimal.Parse(_I_AI_GrossRent.Text);
                IncomeObject.Instance.ActualIncomeObject.GrossRent = dTemp.ToString(CurrFormatter);
                _I_AI_GrossRent.Text = IncomeObject.Instance.ActualIncomeObject.GrossRent;
            }
            catch
            {
                //AS IS
                IncomeObject.Instance.ActualIncomeObject.GrossRent = _I_AI_GrossRent.Text;
            }

            IncomeObject.Instance.ActualIncomeObject.VacancyOrCreditLossPerc = _I_AI_VacancyOrCreditLossPerc.Text;


            //CALCULATE THE OTHER and set to GUI			
            _I_AI_VacancyOrCreditLoss.Text = IncomeObject.Instance.ActualIncomeObject.VacancyOrCreditLossProperty.ToString(CurrFormatter);
            _I_AI_OtherIncome.Text = IncomeObject.Instance.ActualIncomeObject.OtherIncomeProperty.ToString(CurrFormatter);
            _I_AI_OperatingIncome.Text = IncomeObject.Instance.ActualIncomeObject.OperatingIncomeProperty.ToString(CurrFormatter);

            //FORCE 0.00 FORMAT
            _I_AI_GrossRent.Text = IncomeObject.Instance.ActualIncomeObject.GrossRentProperty.ToString(CurrFormatter);
        }

        private void I_AI_LossPerc_OnTextBoxLeave(object sender, System.EventArgs e)
        {
            HandleActualIncomes_LossPerc();
        }

        private void I_AI_LossPerc_OnKeyPressed(object sender, System.Windows.Forms.KeyPressEventArgs e)
        {
            if (e.KeyChar == 13 || e.KeyChar == 27)
                HandleActualIncomes_LossPerc();
        }

        void HandleActualIncomes_LossPerc()
        {
            //SET NEW PROPERTY (take from GUI)			
            try
            {
                //TRY TO CONVERT TO CURR FORMATTER
                decimal dTemp = decimal.Parse(_I_AI_GrossRent.Text);
                IncomeObject.Instance.ActualIncomeObject.GrossRent = dTemp.ToString(CurrFormatter);
                _I_AI_GrossRent.Text = IncomeObject.Instance.ActualIncomeObject.GrossRent;
            }
            catch
            {
                //AS IS
                IncomeObject.Instance.ActualIncomeObject.GrossRent = _I_AI_GrossRent.Text;
            }

            IncomeObject.Instance.ActualIncomeObject.VacancyOrCreditLossPerc = _I_AI_VacancyOrCreditLossPerc.Text;

            //CALCULATE THE OTHER and set to GUI
            _I_AI_VacancyOrCreditLoss.Text = (IncomeObject.Instance.ActualIncomeObject.VacancyOrCreditLossProperty).ToString(CurrFormatter);
            _I_AI_OtherIncome.Text = IncomeObject.Instance.ActualIncomeObject.OtherIncomeProperty.ToString(CurrFormatter);
            _I_AI_OperatingIncome.Text = IncomeObject.Instance.ActualIncomeObject.OperatingIncomeProperty.ToString(CurrFormatter);

            //FORCE 0.00 FORMAT
            _I_AI_GrossRent.Text = IncomeObject.Instance.ActualIncomeObject.GrossRentProperty.ToString(CurrFormatter);
        }          

        private void I_P_GSI_OnTextBoxLeave(object sender, System.EventArgs e)
        {
            HandleActualIncomes_P_GSI();
        }

        private void I_P_GSI_OnKeyPressed(object sender, System.Windows.Forms.KeyPressEventArgs e)
        {
            if (e.KeyChar == 13 || e.KeyChar == 27)
                HandleActualIncomes_P_GSI();
        }

        void HandleActualIncomes_P_GSI()
        {
            //SET PROPERTIES
            IncomeObject.Instance.ProformaIncomeObject.GrossRentProperty = _I_PI_GrossRent.Text;

            //CALCULATE THE OTHER and set to GUI
            _I_PI_VacancyOrCreditLoss.Text = IncomeObject.Instance.ProformaIncomeObject.VacancyOrCreditloss.ToString(CurrFormatter);
            _I_PI_OperatingIncome.Text = IncomeObject.Instance.ProformaIncomeObject.OperatingIncome.ToString(CurrFormatter);
            _I_PI_OtherIncome.Text = IncomeObject.Instance.ProformaIncomeObject.OtherIncome.ToString(CurrFormatter);

            //FORCE 0.00 FORMAT
            _I_PI_GrossRent.Text = IncomeObject.Instance.ProformaIncomeObject.GrossRent.ToString(CurrFormatter);
        }

        private void I_P_VacancyPerc_OnTextBoxLeave(object sender, System.EventArgs e)
        {
            HandleActualIncomes_P_VacancyPerc();
        }

        private void I_P_VacancyPerc_OnKeyPressed(object sender, System.Windows.Forms.KeyPressEventArgs e)
        {
            if (e.KeyChar == 13 || e.KeyChar == 27)
                HandleActualIncomes_P_VacancyPerc();
        }

        void HandleActualIncomes_P_VacancyPerc()
        {
            //SET PROPERTIES
            IncomeObject.Instance.ProformaIncomeObject.VacancyOrCreditLossPercProperty = _I_PI_VacancyOrCreditLossPerc.Text;

            //CALCULATE THE OTHER and set to GUI
            _I_PI_VacancyOrCreditLoss.Text = IncomeObject.Instance.ProformaIncomeObject.VacancyOrCreditloss.ToString(CurrFormatter);
            _I_PI_OperatingIncome.Text = IncomeObject.Instance.ProformaIncomeObject.OperatingIncome.ToString(CurrFormatter);
            _I_PI_OtherIncome.Text = IncomeObject.Instance.ProformaIncomeObject.OtherIncome.ToString(CurrFormatter);
        }

        private void I_P_OtherIncome_OnTextBoxLeave(object sender, System.EventArgs e)
        {
            HandleActualIncomes_P_OtherPerc();
        }

        private void I_P_OtherIncome_OnKeyPressed(object sender, System.Windows.Forms.KeyPressEventArgs e)
        {
            if (e.KeyChar == 13 || e.KeyChar == 27)
                HandleActualIncomes_P_OtherPerc();
        }

        void HandleActualIncomes_P_OtherPerc()
        {
            //SET PROPERTIES
            IncomeObject.Instance.ProformaIncomeObject.OtherIncomeProperty = _I_PI_OtherIncome.Text;

            //CALCULATE THE OTHER and set to GUI
            _I_PI_VacancyOrCreditLoss.Text = IncomeObject.Instance.ProformaIncomeObject.VacancyOrCreditloss.ToString(CurrFormatter);
            _I_PI_OperatingIncome.Text = IncomeObject.Instance.ProformaIncomeObject.OperatingIncome.ToString(CurrFormatter);
            _I_PI_OtherIncome.Text = IncomeObject.Instance.ProformaIncomeObject.OtherIncome.ToString(CurrFormatter);
        }
        #endregion        

        #region MAP TO OBJECT MODEL
        void HandleStartDate()
        {
            if (-1 != _currentRow)
                IncomeObject.Instance.RetailOfficeSummaryObject[_currentRow].SpaceDetails.LeaseStartDate = _I_S_L_StartDate.Value.ToShortDateString();
        }
        
        void HandleType()
        {
            if (-1 != _currentRow)
                IncomeObject.Instance.RetailOfficeSummaryObject[_currentRow].SpaceDetails.LeaseType = _I_S_L_Type.SelectedText;
        }
        
        void HandleTennantComments()
        {
            if (-1 != _currentRow)
                IncomeObject.Instance.RetailOfficeSummaryObject[_currentRow].SpaceDetails.FinancialComments = _I_S_F_TennantComment.Text;
        }
        
        void HandleCAMCharges()
        {
            if (-1 != _currentRow)
                IncomeObject.Instance.RetailOfficeSummaryObject[_currentRow].SpaceDetails.FinancialCAMCharges = _I_S_F_CAMCharges.Text;
        }
        
        void HandleNNNCharges()
        {
            if (-1 != _currentRow)
                IncomeObject.Instance.RetailOfficeSummaryObject[_currentRow].SpaceDetails.FinancialNNNCharges = _I_S_F_NNNCharges.Text;
        }
        
        void HandleExpensesStop()
        {
            if (-1 != _currentRow)
                IncomeObject.Instance.RetailOfficeSummaryObject[_currentRow].SpaceDetails.FinancialExpensesStop = _I_S_F_ExpensesStop.Text;
        }
        
        void HandleAllowance()
        {
            if (-1 != _currentRow)
                IncomeObject.Instance.RetailOfficeSummaryObject[_currentRow].SpaceDetails.FinancialTIAllowance = _I_S_F_TIAllowance.Text;
        }
        
        void HandleLeaseComments()
        {
            if (-1 != _currentRow)
                IncomeObject.Instance.RetailOfficeSummaryObject[_currentRow].SpaceDetails.LeaseComments = _I_S_L_LeaseComments.Text;
        }
        
        void HandleEndDate()
        {
            if (-1 != _currentRow)
                IncomeObject.Instance.RetailOfficeSummaryObject[_currentRow].SpaceDetails.LeaseEndDate = _I_S_L_EndDate.Value.ToShortDateString();
        }

        void HandleLength()
        {
            if (-1 != _currentRow)
                IncomeObject.Instance.RetailOfficeSummaryObject[_currentRow].SpaceDetails.LeaseLength = _I_S_L_Length.Text;
        }

        void HandleComments()
        {
            if (-1 != _currentRow)
                IncomeObject.Instance.RetailOfficeSummaryObject[_currentRow].SpaceDetails.Comments = _I_S_Comments.Text;
        }

        void SetSpaceExt(string space)
        {
            _I_S_Space.Text = space;
            HandleSpace();
        }

        void HandleSpace()
        {
            if (-1 != _currentRow)
                IncomeObject.Instance.RetailOfficeSummaryObject[_currentRow].SpaceDetails.P_Space = _I_S_Space.Text;
        }
        #endregion

        #region SET DATA
        public void SetData(IncomeObject obj)
        {
            try
            {
                for (int i = 0; i < IncomeObject.ROMaxItems; i++)
                {
                    _I_Summary.Items[i].SubItems[0].Text = IncomeObject.Instance.RetailOfficeSummaryObject[i].Space;
                    _I_Summary.Items[i].SubItems[1].Text = IncomeObject.Instance.RetailOfficeSummaryObject[i].Occupied;
                    _I_Summary.Items[i].SubItems[2].Text = IncomeObject.Instance.RetailOfficeSummaryObject[i].Tennant;
                    _I_Summary.Items[i].SubItems[3].Text = IncomeObject.Instance.RetailOfficeSummaryObject[i].Floor;
                    _I_Summary.Items[i].SubItems[4].Text = IncomeObject.Instance.RetailOfficeSummaryObject[i].SqFt;
                    _I_Summary.Items[i].SubItems[5].Text = IncomeObject.Instance.RetailOfficeSummaryObject[i].AR_usdPerMonth;
                    _I_Summary.Items[i].SubItems[6].Text = IncomeObject.Instance.RetailOfficeSummaryObject[i].AR_usdPerYear;
                    _I_Summary.Items[i].SubItems[7].Text = IncomeObject.Instance.RetailOfficeSummaryObject[i].AR_usdPerSqFtPerMonth;
                    _I_Summary.Items[i].SubItems[8].Text = IncomeObject.Instance.RetailOfficeSummaryObject[i].AR_usdPerSqFtPerYear;
                    _I_Summary.Items[i].SubItems[9].Text = IncomeObject.Instance.RetailOfficeSummaryObject[i].PR_usdPerMonth;
                    _I_Summary.Items[i].SubItems[10].Text = IncomeObject.Instance.RetailOfficeSummaryObject[i].PR_usdPerYear;
                    _I_Summary.Items[i].SubItems[11].Text = IncomeObject.Instance.RetailOfficeSummaryObject[i].PR_usdPerSqFtPerMonth;
                    _I_Summary.Items[i].SubItems[12].Text = IncomeObject.Instance.RetailOfficeSummaryObject[i].PR_usdPerSqFtPerYear;                    
                }

                _I_UTS_TotalOccupied.Text = IncomeObject.Instance.GetTotalOccupied().ToString(CurrFormatter);
                _I_UTS_TotalSqFt.Text = IncomeObject.Instance.GetTotalSqFt().ToString(CurrFormatter);
                _I_S_Total_AR_UsdPerMonth.Text = IncomeObject.Instance.GetTotal_AR_UsdPerMonth().ToString(CurrFormatter);
                _I_S_Total_AR_UsdPerYear.Text = IncomeObject.Instance.GetTotal_AR_UsdPerYear().ToString(CurrFormatter);
                _I_S_Total_AR_UsdPerSqFtPerMonth.Text = IncomeObject.Instance.GetTotal_AR_UsdPerMonthPerSqFt().ToString(CurrFormatter);
                _I_S_Total_PR_UsdPerMonth.Text = IncomeObject.Instance.GetTotal_AR_UsdPerYearPerSqFt().ToString(CurrFormatter);
                _I_UTS_Total_AR_UsdPerSqFt.Text = IncomeObject.Instance.GetTotal_PR_UsdPerMonth().ToString(CurrFormatter);
                _I_S_Total_PR_UsdPerYear.Text = IncomeObject.Instance.GetTotal_PR_UsdPerYear().ToString(CurrFormatter);
                _I_S_Total_PR_UsdPerSqFtPerMonth.Text = IncomeObject.Instance.GetTotal_PR_UsdPerMonthPerSqFt().ToString(CurrFormatter);
                _I_S_Total_PR_UsdPerSqftPerYear.Text = IncomeObject.Instance.GetTotal_PR_UsdPerYearPerSqFt().ToString(CurrFormatter);
                               
                _I_AI_OtherIncome.Text = obj.ActualIncomeObject.OtherIncomeProperty.ToString(CurrFormatter); ;
                _I_PI_GrossRent.Text = IncomeObject.Instance.ProformaIncomeObject.GrossRentProperty;

                
                //ACTUAL INCOME
                _I_AI_VacancyOrCreditLossPerc.Text = obj.ActualIncomeObject.VacancyOrCreditLossPerc;
                _I_AI_VacancyOrCreditLoss.Text = obj.ActualIncomeObject.VacancyOrCreditLossProperty.ToString(CurrFormatter);
                _I_AI_OtherIncome.Text = obj.ActualIncomeObject.OtherIncomeProperty.ToString(CurrFormatter);
                _I_AI_OperatingIncome.Text = obj.ActualIncomeObject.OperatingIncomeProperty.ToString(CurrFormatter);
                _I_AI_GrossRent.Text = obj.ActualIncomeObject.GrossRentProperty.ToString(CurrFormatter);

                //PROFORMA INCOME
                _I_PI_VacancyOrCreditLoss.Text = obj.ProformaIncomeObject.VacancyOrCreditloss.ToString(CurrFormatter);
                _I_PI_VacancyOrCreditLossPerc.Text = (obj.ProformaIncomeObject.VacancyOrCreditlossPerc * 100).ToString(CurrFormatter);
                _I_PI_OperatingIncome.Text = obj.ProformaIncomeObject.OperatingIncome.ToString(CurrFormatter);
                _I_PI_OtherIncome.Text = obj.ProformaIncomeObject.OtherIncome.ToString(CurrFormatter);
                _I_PI_GrossRent.Text = obj.ProformaIncomeObject.GrossRent.ToString(CurrFormatter);

                //SPACE AREA
                DateTime endDate = DateTime.Now;
                try { endDate = Convert.ToDateTime(IncomeObject.Instance.RetailOfficeSummaryObject[_currentRow].SpaceDetails.LeaseEndDate); }
                catch { endDate = DateTime.Now; }
                _I_S_L_EndDate.Value = endDate;

                DateTime startDate = DateTime.Now;
                try { startDate = Convert.ToDateTime(IncomeObject.Instance.RetailOfficeSummaryObject[_currentRow].SpaceDetails.LeaseStartDate); }
                catch { startDate = DateTime.Now; }
                _I_S_L_StartDate.Value = startDate;

                if (-1 != _currentRow)
                {
                    _I_S_L_Type.SelectedText = IncomeObject.Instance.RetailOfficeSummaryObject[_currentRow].SpaceDetails.LeaseType;
                    _I_S_F_TennantComment.Text = IncomeObject.Instance.RetailOfficeSummaryObject[_currentRow].SpaceDetails.Comments;
                    _I_S_F_CAMCharges.Text = IncomeObject.Instance.RetailOfficeSummaryObject[_currentRow].SpaceDetails.FinancialCAMCharges;
                    _I_S_F_NNNCharges.Text = IncomeObject.Instance.RetailOfficeSummaryObject[_currentRow].SpaceDetails.FinancialNNNCharges;
                    _I_S_F_ExpensesStop.Text = IncomeObject.Instance.RetailOfficeSummaryObject[_currentRow].SpaceDetails.FinancialExpensesStop;
                    _I_S_F_TIAllowance.Text = IncomeObject.Instance.RetailOfficeSummaryObject[_currentRow].SpaceDetails.FinancialTIAllowance;
                    _I_S_L_LeaseComments.Text = IncomeObject.Instance.RetailOfficeSummaryObject[_currentRow].SpaceDetails.LeaseComments;
                    _I_S_L_Length.Text = IncomeObject.Instance.RetailOfficeSummaryObject[_currentRow].SpaceDetails.LeaseLength;
                    _I_S_Comments.Text = IncomeObject.Instance.RetailOfficeSummaryObject[_currentRow].SpaceDetails.Comments;
                    _I_S_Space.Text = IncomeObject.Instance.RetailOfficeSummaryObject[_currentRow].SpaceDetails.P_Space;
                }
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.ToString());
            }
        }
        #endregion

        #region ON TEXT BOX ENTER (DISABLED)
        private void OnTextBoxEnter(object sender, System.EventArgs e)
        {
        }
        #endregion

        #region AUTOSAVE
        void AddAutoSaveEventHandlers()
        {
            ArrayList objectsFound = new ArrayList();
            FindAutoSaveObjects(this.Controls, ref objectsFound);

            foreach (object o in objectsFound)
            {
                if (o is TextBox)
                {
                    ((TextBox)o).Leave += new EventHandler(On_TextBox_Leave);

                    if (!((TextBox)o).ReadOnly)
                        ((TextBox)o).TextChanged += new EventHandler(On_TextChanged);
                }

                if (o is ComboBox)
                    ((ComboBox)o).SelectedIndexChanged += new EventHandler(On_SelectedIndexChanged);

                if (o is DateTimePicker)
                    ((DateTimePicker)o).ValueChanged += new EventHandler(On_SelectedIndexChanged);// we use combobox event handler
            }
        }

        void On_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (null != _autoSave)
                _autoSave.TriggerSelectedIndexChanged();
        }

        void On_TextChanged(object sender, EventArgs e)
        {
            if (null != _autoSave)
                _autoSave.TriggerTextBoxChanged();
        }

        void On_TextBox_Leave(object sender, EventArgs e)
        {
            if (null != _autoSave)
                _autoSave.TriggerTextBoxLeave();
        }

        void FindAutoSaveObjects(System.Windows.Forms.Control.ControlCollection ctrls, ref ArrayList triggerObjects)
        {
            foreach (object o in ctrls)
            {
                if (o is GroupBox)
                    FindAutoSaveObjects(((GroupBox)o).Controls, ref triggerObjects);

                if (o is Panel)
                    FindAutoSaveObjects(((Panel)o).Controls, ref triggerObjects);

                if (o is TabControl)
                    FindAutoSaveObjects(((TabControl)o).Controls, ref triggerObjects);

                if (o is TextBox)
                    triggerObjects.Add(o);

                if (o is ComboBox)
                    triggerObjects.Add(o);

                if (o is DateTimePicker)
                    triggerObjects.Add(o);

                if (o is CommercialControl)
                    FindAutoSaveObjects(((CommercialControl)o).Controls, ref triggerObjects);
            }
        }
        #endregion

        //REPORTS
        void On_PrintIncomeReport_Click(object sender, EventArgs e)
        {
            PageSettings pageSettings = null;
            PageSetupDialog pageSetupDialog = new PageSetupDialog();
            pageSetupDialog.PageSettings = new PageSettings();
            pageSetupDialog.PrinterSettings = new PrinterSettings();
            pageSetupDialog.ShowNetwork = false;

            pageSetupDialog.PageSettings.PaperSize = new PaperSize("A4", 827, 1169);
            pageSetupDialog.PageSettings.Margins = new Margins(0, 0, 0, 0);


            //if (DialogResult.OK == pageSetupDialog.ShowDialog())
            //{
            //    pageSettings = new PageSettings(pageSetupDialog.PrinterSettings);
            //    pageSettings.PaperSize = pageSetupDialog.PageSettings.PaperSize;
            //    pageSettings.Margins = pageSetupDialog.PageSettings.Margins;								
            //}

            PrintDialog pd = new PrintDialog();
            pd.UseEXDialog = true;

            PrintDocument tmpprndoc = new PrintDocument();

            
            tmpprndoc.DefaultPageSettings.Landscape = true;
            tmpprndoc.PrintPage += new PrintPageEventHandler(On_PrintIncomeSummary);
            tmpprndoc.EndPrint += new PrintEventHandler(On_PrintIncome_EndPrint);
            

            pd.Document = tmpprndoc;
            PrintPreviewDialogEx tmpprdiag = new PrintPreviewDialogEx();

            tmpprdiag.Document = tmpprndoc;
            //tmpprdiag.Document.DefaultPageSettings = pageSettings;
            tmpprdiag.ShowDialog();
        }

        private void On_PrintIncomeSummary(object sender, PrintPageEventArgs e)
        {
            _iGen.GenerateRetailOrOfficeSummary(e, IncomeObject.Instance, _address);
        }

        void On_PrintIncome_EndPrint(object sender, PrintEventArgs e)
        {
            _iGen.ResetGen();
        }      
        
    }
}
