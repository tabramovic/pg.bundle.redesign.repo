﻿namespace ProfitGrabber.Commercial
{
    partial class Income_Multifamily
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Income_Multifamily));
            this._printRentRoll = new System.Windows.Forms.Button();
            this._printIncomeSummary = new System.Windows.Forms.Button();
            this._I_AI_VacancyOrCreditLossPerc = new System.Windows.Forms.TextBox();
            this._I_PI_VacancyOrCreditLossPerc = new System.Windows.Forms.TextBox();
            this._I_OI_Total = new System.Windows.Forms.TextBox();
            this._I_OI_Other = new System.Windows.Forms.TextBox();
            this._I_OI_Vending = new System.Windows.Forms.TextBox();
            this._I_OI_Laundry = new System.Windows.Forms.TextBox();
            this._I_OI_Parking = new System.Windows.Forms.TextBox();
            this._I_AI_OperatingIncome = new System.Windows.Forms.TextBox();
            this._I_AI_OtherIncome = new System.Windows.Forms.TextBox();
            this._I_AI_VacancyOrCreditLoss = new System.Windows.Forms.TextBox();
            this._I_AI_GrossRent = new System.Windows.Forms.TextBox();
            this._I_PI_OperatingIncome = new System.Windows.Forms.TextBox();
            this._I_PI_OtherIncome = new System.Windows.Forms.TextBox();
            this._I_PI_VacancyOrCreditLoss = new System.Windows.Forms.TextBox();
            this._I_PI_GrossRent = new System.Windows.Forms.TextBox();
            this._i_floater = new System.Windows.Forms.PictureBox();
            this._I_unitTypeSummary = new ProfitGrabber.Commercial.ListViewEx();
            this.columnHeader23 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader24 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader25 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader26 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader27 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader28 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader29 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader30 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader31 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader32 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader33 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader34 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader35 = new System.Windows.Forms.ColumnHeader();
            this._calculateFromRentRoll = new System.Windows.Forms.Button();
            this._CalculateFromUnitTypeSummary = new System.Windows.Forms.Button();
            this.label106 = new System.Windows.Forms.Label();
            this.label107 = new System.Windows.Forms.Label();
            this.label108 = new System.Windows.Forms.Label();
            this.label109 = new System.Windows.Forms.Label();
            this.label110 = new System.Windows.Forms.Label();
            this.label111 = new System.Windows.Forms.Label();
            this.label112 = new System.Windows.Forms.Label();
            this.label113 = new System.Windows.Forms.Label();
            this.label114 = new System.Windows.Forms.Label();
            this.label96 = new System.Windows.Forms.Label();
            this.label97 = new System.Windows.Forms.Label();
            this.label98 = new System.Windows.Forms.Label();
            this.label99 = new System.Windows.Forms.Label();
            this.label100 = new System.Windows.Forms.Label();
            this.label101 = new System.Windows.Forms.Label();
            this.label102 = new System.Windows.Forms.Label();
            this.label103 = new System.Windows.Forms.Label();
            this.label95 = new System.Windows.Forms.Label();
            this.label93 = new System.Windows.Forms.Label();
            this.label92 = new System.Windows.Forms.Label();
            this.label91 = new System.Windows.Forms.Label();
            this.label90 = new System.Windows.Forms.Label();
            this.label89 = new System.Windows.Forms.Label();
            this.label88 = new System.Windows.Forms.Label();
            this.label87 = new System.Windows.Forms.Label();
            this.label86 = new System.Windows.Forms.Label();
            this.label85 = new System.Windows.Forms.Label();
            this.label83 = new System.Windows.Forms.Label();
            this._I_rentRoll = new ProfitGrabber.Commercial.ListViewEx();
            this.columnHeader1 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader2 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader3 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader4 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader5 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader6 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader7 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader8 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader9 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader10 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader11 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader12 = new System.Windows.Forms.ColumnHeader();
            this.label70 = new System.Windows.Forms.Label();
            this.label69 = new System.Windows.Forms.Label();
            this.label68 = new System.Windows.Forms.Label();
            this.label67 = new System.Windows.Forms.Label();
            this._I_UTS_Total_MR_USDperSqFt = new System.Windows.Forms.TextBox();
            this._I_UTS_Total_MR_High = new System.Windows.Forms.TextBox();
            this._I_UTS_Total_MR_Low = new System.Windows.Forms.TextBox();
            this._I_UTS_Total_AR_UsdPerSqFt = new System.Windows.Forms.TextBox();
            this._I_UTS_Total_AR_High = new System.Windows.Forms.TextBox();
            this._I_UTS_Total_AR_Low = new System.Windows.Forms.TextBox();
            this._I_UTS_Total_SQ_High = new System.Windows.Forms.TextBox();
            this._I_UTS_Total_SQ_Low = new System.Windows.Forms.TextBox();
            this._I_UTS_TotalOccupancy = new System.Windows.Forms.TextBox();
            this._I_UTS_TotalUnits = new System.Windows.Forms.TextBox();
            this.label66 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.label104 = new System.Windows.Forms.Label();
            this.label105 = new System.Windows.Forms.Label();
            this.label115 = new System.Windows.Forms.Label();
            this.label84 = new System.Windows.Forms.Label();
            this._I_RR_TotalMarketRent = new System.Windows.Forms.TextBox();
            this._I_RR_TotalTotalRent = new System.Windows.Forms.TextBox();
            this._I_RR_TotalSec8Rent = new System.Windows.Forms.TextBox();
            this._I_RR_TotalTennantRent = new System.Windows.Forms.TextBox();
            this._I_RR_TotalSecDep = new System.Windows.Forms.TextBox();
            this._I_RR_TotalSqFt = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this._i_floater)).BeginInit();
            this.SuspendLayout();
            // 
            // _printRentRoll
            // 
            this._printRentRoll.Location = new System.Drawing.Point(711, 353);
            this._printRentRoll.Name = "_printRentRoll";
            this._printRentRoll.Size = new System.Drawing.Size(134, 23);
            this._printRentRoll.TabIndex = 333;
            this._printRentRoll.Text = "Print RentRoll";
            this._printRentRoll.UseVisualStyleBackColor = true;
            // 
            // _printIncomeSummary
            // 
            this._printIncomeSummary.Location = new System.Drawing.Point(711, 221);
            this._printIncomeSummary.Name = "_printIncomeSummary";
            this._printIncomeSummary.Size = new System.Drawing.Size(134, 23);
            this._printIncomeSummary.TabIndex = 332;
            this._printIncomeSummary.Text = "Print Income Summary";
            this._printIncomeSummary.UseVisualStyleBackColor = true;
            // 
            // _I_AI_VacancyOrCreditLossPerc
            // 
            this._I_AI_VacancyOrCreditLossPerc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._I_AI_VacancyOrCreditLossPerc.Location = new System.Drawing.Point(545, 435);
            this._I_AI_VacancyOrCreditLossPerc.Name = "_I_AI_VacancyOrCreditLossPerc";
            this._I_AI_VacancyOrCreditLossPerc.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this._I_AI_VacancyOrCreditLossPerc.Size = new System.Drawing.Size(56, 20);
            this._I_AI_VacancyOrCreditLossPerc.TabIndex = 279;
            // 
            // _I_PI_VacancyOrCreditLossPerc
            // 
            this._I_PI_VacancyOrCreditLossPerc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._I_PI_VacancyOrCreditLossPerc.Location = new System.Drawing.Point(825, 435);
            this._I_PI_VacancyOrCreditLossPerc.Name = "_I_PI_VacancyOrCreditLossPerc";
            this._I_PI_VacancyOrCreditLossPerc.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this._I_PI_VacancyOrCreditLossPerc.Size = new System.Drawing.Size(54, 20);
            this._I_PI_VacancyOrCreditLossPerc.TabIndex = 284;
            // 
            // _I_OI_Total
            // 
            this._I_OI_Total.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._I_OI_Total.Location = new System.Drawing.Point(89, 495);
            this._I_OI_Total.Name = "_I_OI_Total";
            this._I_OI_Total.ReadOnly = true;
            this._I_OI_Total.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this._I_OI_Total.Size = new System.Drawing.Size(65, 20);
            this._I_OI_Total.TabIndex = 271;
            this._I_OI_Total.TabStop = false;
            // 
            // _I_OI_Other
            // 
            this._I_OI_Other.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._I_OI_Other.Location = new System.Drawing.Point(89, 475);
            this._I_OI_Other.Name = "_I_OI_Other";
            this._I_OI_Other.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this._I_OI_Other.Size = new System.Drawing.Size(65, 20);
            this._I_OI_Other.TabIndex = 270;
            // 
            // _I_OI_Vending
            // 
            this._I_OI_Vending.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._I_OI_Vending.Location = new System.Drawing.Point(89, 455);
            this._I_OI_Vending.Name = "_I_OI_Vending";
            this._I_OI_Vending.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this._I_OI_Vending.Size = new System.Drawing.Size(65, 20);
            this._I_OI_Vending.TabIndex = 268;
            // 
            // _I_OI_Laundry
            // 
            this._I_OI_Laundry.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._I_OI_Laundry.Location = new System.Drawing.Point(89, 435);
            this._I_OI_Laundry.Name = "_I_OI_Laundry";
            this._I_OI_Laundry.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this._I_OI_Laundry.Size = new System.Drawing.Size(65, 20);
            this._I_OI_Laundry.TabIndex = 266;
            // 
            // _I_OI_Parking
            // 
            this._I_OI_Parking.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._I_OI_Parking.Location = new System.Drawing.Point(89, 415);
            this._I_OI_Parking.Name = "_I_OI_Parking";
            this._I_OI_Parking.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this._I_OI_Parking.Size = new System.Drawing.Size(65, 20);
            this._I_OI_Parking.TabIndex = 264;
            // 
            // _I_AI_OperatingIncome
            // 
            this._I_AI_OperatingIncome.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._I_AI_OperatingIncome.Location = new System.Drawing.Point(481, 475);
            this._I_AI_OperatingIncome.Name = "_I_AI_OperatingIncome";
            this._I_AI_OperatingIncome.ReadOnly = true;
            this._I_AI_OperatingIncome.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this._I_AI_OperatingIncome.Size = new System.Drawing.Size(65, 20);
            this._I_AI_OperatingIncome.TabIndex = 281;
            this._I_AI_OperatingIncome.TabStop = false;
            // 
            // _I_AI_OtherIncome
            // 
            this._I_AI_OtherIncome.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._I_AI_OtherIncome.Location = new System.Drawing.Point(481, 455);
            this._I_AI_OtherIncome.Name = "_I_AI_OtherIncome";
            this._I_AI_OtherIncome.ReadOnly = true;
            this._I_AI_OtherIncome.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this._I_AI_OtherIncome.Size = new System.Drawing.Size(65, 20);
            this._I_AI_OtherIncome.TabIndex = 280;
            this._I_AI_OtherIncome.TabStop = false;
            // 
            // _I_AI_VacancyOrCreditLoss
            // 
            this._I_AI_VacancyOrCreditLoss.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._I_AI_VacancyOrCreditLoss.Location = new System.Drawing.Point(481, 435);
            this._I_AI_VacancyOrCreditLoss.Name = "_I_AI_VacancyOrCreditLoss";
            this._I_AI_VacancyOrCreditLoss.ReadOnly = true;
            this._I_AI_VacancyOrCreditLoss.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this._I_AI_VacancyOrCreditLoss.Size = new System.Drawing.Size(65, 20);
            this._I_AI_VacancyOrCreditLoss.TabIndex = 278;
            this._I_AI_VacancyOrCreditLoss.TabStop = false;
            // 
            // _I_AI_GrossRent
            // 
            this._I_AI_GrossRent.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._I_AI_GrossRent.Location = new System.Drawing.Point(481, 415);
            this._I_AI_GrossRent.Name = "_I_AI_GrossRent";
            this._I_AI_GrossRent.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this._I_AI_GrossRent.Size = new System.Drawing.Size(65, 20);
            this._I_AI_GrossRent.TabIndex = 277;
            // 
            // _I_PI_OperatingIncome
            // 
            this._I_PI_OperatingIncome.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._I_PI_OperatingIncome.Location = new System.Drawing.Point(761, 475);
            this._I_PI_OperatingIncome.Name = "_I_PI_OperatingIncome";
            this._I_PI_OperatingIncome.ReadOnly = true;
            this._I_PI_OperatingIncome.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this._I_PI_OperatingIncome.Size = new System.Drawing.Size(65, 20);
            this._I_PI_OperatingIncome.TabIndex = 286;
            this._I_PI_OperatingIncome.TabStop = false;
            // 
            // _I_PI_OtherIncome
            // 
            this._I_PI_OtherIncome.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._I_PI_OtherIncome.Location = new System.Drawing.Point(761, 455);
            this._I_PI_OtherIncome.Name = "_I_PI_OtherIncome";
            this._I_PI_OtherIncome.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this._I_PI_OtherIncome.Size = new System.Drawing.Size(65, 20);
            this._I_PI_OtherIncome.TabIndex = 285;
            // 
            // _I_PI_VacancyOrCreditLoss
            // 
            this._I_PI_VacancyOrCreditLoss.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._I_PI_VacancyOrCreditLoss.Location = new System.Drawing.Point(761, 435);
            this._I_PI_VacancyOrCreditLoss.Name = "_I_PI_VacancyOrCreditLoss";
            this._I_PI_VacancyOrCreditLoss.ReadOnly = true;
            this._I_PI_VacancyOrCreditLoss.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this._I_PI_VacancyOrCreditLoss.Size = new System.Drawing.Size(65, 20);
            this._I_PI_VacancyOrCreditLoss.TabIndex = 283;
            this._I_PI_VacancyOrCreditLoss.TabStop = false;
            // 
            // _I_PI_GrossRent
            // 
            this._I_PI_GrossRent.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._I_PI_GrossRent.Location = new System.Drawing.Point(761, 415);
            this._I_PI_GrossRent.Name = "_I_PI_GrossRent";
            this._I_PI_GrossRent.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this._I_PI_GrossRent.Size = new System.Drawing.Size(65, 20);
            this._I_PI_GrossRent.TabIndex = 282;
            // 
            // _i_floater
            // 
            this._i_floater.Image = ((System.Drawing.Image)(resources.GetObject("_i_floater.Image")));
            this._i_floater.Location = new System.Drawing.Point(873, 7);
            this._i_floater.Name = "_i_floater";
            this._i_floater.Size = new System.Drawing.Size(16, 16);
            this._i_floater.TabIndex = 331;
            this._i_floater.TabStop = false;
            // 
            // _I_unitTypeSummary
            // 
            this._I_unitTypeSummary.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._I_unitTypeSummary.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader23,
            this.columnHeader24,
            this.columnHeader25,
            this.columnHeader26,
            this.columnHeader27,
            this.columnHeader28,
            this.columnHeader29,
            this.columnHeader30,
            this.columnHeader31,
            this.columnHeader32,
            this.columnHeader33,
            this.columnHeader34,
            this.columnHeader35});
            this._I_unitTypeSummary.FullRowSelect = true;
            this._I_unitTypeSummary.GridLines = true;
            this._I_unitTypeSummary.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this._I_unitTypeSummary.Location = new System.Drawing.Point(41, 31);
            this._I_unitTypeSummary.Name = "_I_unitTypeSummary";
            this._I_unitTypeSummary.Size = new System.Drawing.Size(804, 163);
            this._I_unitTypeSummary.TabIndex = 259;
            this._I_unitTypeSummary.UseCompatibleStateImageBehavior = false;
            this._I_unitTypeSummary.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader23
            // 
            this.columnHeader23.Text = "#Units";
            this.columnHeader23.Width = 52;
            // 
            // columnHeader24
            // 
            this.columnHeader24.Text = "Bedrooms";
            this.columnHeader24.Width = 61;
            // 
            // columnHeader25
            // 
            this.columnHeader25.Text = "Baths";
            this.columnHeader25.Width = 46;
            // 
            // columnHeader26
            // 
            this.columnHeader26.Text = "Floor Plan";
            this.columnHeader26.Width = 104;
            // 
            // columnHeader27
            // 
            this.columnHeader27.Text = "% Occupied";
            this.columnHeader27.Width = 69;
            // 
            // columnHeader28
            // 
            this.columnHeader28.Text = "Low";
            this.columnHeader28.Width = 56;
            // 
            // columnHeader29
            // 
            this.columnHeader29.Text = "High";
            this.columnHeader29.Width = 54;
            // 
            // columnHeader30
            // 
            this.columnHeader30.Text = "Low";
            this.columnHeader30.Width = 56;
            // 
            // columnHeader31
            // 
            this.columnHeader31.Text = "High";
            this.columnHeader31.Width = 56;
            // 
            // columnHeader32
            // 
            this.columnHeader32.Text = "$/SqFt:";
            this.columnHeader32.Width = 59;
            // 
            // columnHeader33
            // 
            this.columnHeader33.Text = "Low";
            this.columnHeader33.Width = 55;
            // 
            // columnHeader34
            // 
            this.columnHeader34.Text = "High";
            // 
            // columnHeader35
            // 
            this.columnHeader35.Text = "$/SqFt";
            this.columnHeader35.Width = 54;
            // 
            // _calculateFromRentRoll
            // 
            this._calculateFromRentRoll.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._calculateFromRentRoll.Location = new System.Drawing.Point(177, 463);
            this._calculateFromRentRoll.Name = "_calculateFromRentRoll";
            this._calculateFromRentRoll.Size = new System.Drawing.Size(96, 48);
            this._calculateFromRentRoll.TabIndex = 307;
            this._calculateFromRentRoll.Text = "Calculate from RENT ROLL";
            this._calculateFromRentRoll.Visible = false;
            // 
            // _CalculateFromUnitTypeSummary
            // 
            this._CalculateFromUnitTypeSummary.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._CalculateFromUnitTypeSummary.Location = new System.Drawing.Point(177, 415);
            this._CalculateFromUnitTypeSummary.Name = "_CalculateFromUnitTypeSummary";
            this._CalculateFromUnitTypeSummary.Size = new System.Drawing.Size(96, 48);
            this._CalculateFromUnitTypeSummary.TabIndex = 274;
            this._CalculateFromUnitTypeSummary.Text = "Calculate from UNIT TYPE SUMMARY";
            // 
            // label106
            // 
            this.label106.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label106.Location = new System.Drawing.Point(751, 479);
            this.label106.Name = "label106";
            this.label106.Size = new System.Drawing.Size(16, 23);
            this.label106.TabIndex = 328;
            this.label106.Text = "$";
            // 
            // label107
            // 
            this.label107.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label107.Location = new System.Drawing.Point(751, 458);
            this.label107.Name = "label107";
            this.label107.Size = new System.Drawing.Size(16, 23);
            this.label107.TabIndex = 327;
            this.label107.Text = "$";
            // 
            // label108
            // 
            this.label108.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label108.Location = new System.Drawing.Point(751, 437);
            this.label108.Name = "label108";
            this.label108.Size = new System.Drawing.Size(16, 23);
            this.label108.TabIndex = 326;
            this.label108.Text = "$";
            // 
            // label109
            // 
            this.label109.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label109.Location = new System.Drawing.Point(751, 418);
            this.label109.Name = "label109";
            this.label109.Size = new System.Drawing.Size(16, 23);
            this.label109.TabIndex = 325;
            this.label109.Text = "$";
            // 
            // label110
            // 
            this.label110.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label110.Location = new System.Drawing.Point(589, 479);
            this.label110.Name = "label110";
            this.label110.Size = new System.Drawing.Size(162, 23);
            this.label110.TabIndex = 324;
            this.label110.Text = "Gross Operating Income (GOI):";
            // 
            // label111
            // 
            this.label111.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label111.Location = new System.Drawing.Point(675, 458);
            this.label111.Name = "label111";
            this.label111.Size = new System.Drawing.Size(76, 23);
            this.label111.TabIndex = 323;
            this.label111.Text = "Other Income:";
            // 
            // label112
            // 
            this.label112.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label112.Location = new System.Drawing.Point(639, 437);
            this.label112.Name = "label112";
            this.label112.Size = new System.Drawing.Size(112, 23);
            this.label112.TabIndex = 322;
            this.label112.Text = "Vacancy/Credit Loss:";
            // 
            // label113
            // 
            this.label113.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label113.Location = new System.Drawing.Point(655, 418);
            this.label113.Name = "label113";
            this.label113.Size = new System.Drawing.Size(96, 23);
            this.label113.TabIndex = 321;
            this.label113.Text = "Gross Rent (GSI):";
            // 
            // label114
            // 
            this.label114.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label114.Location = new System.Drawing.Point(657, 391);
            this.label114.Name = "label114";
            this.label114.Size = new System.Drawing.Size(200, 23);
            this.label114.TabIndex = 329;
            this.label114.Text = "PROFORMA INCOME (annual):";
            // 
            // label96
            // 
            this.label96.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label96.Location = new System.Drawing.Point(471, 479);
            this.label96.Name = "label96";
            this.label96.Size = new System.Drawing.Size(16, 23);
            this.label96.TabIndex = 318;
            this.label96.Text = "$";
            // 
            // label97
            // 
            this.label97.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label97.Location = new System.Drawing.Point(471, 458);
            this.label97.Name = "label97";
            this.label97.Size = new System.Drawing.Size(16, 23);
            this.label97.TabIndex = 317;
            this.label97.Text = "$";
            // 
            // label98
            // 
            this.label98.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label98.Location = new System.Drawing.Point(471, 437);
            this.label98.Name = "label98";
            this.label98.Size = new System.Drawing.Size(16, 23);
            this.label98.TabIndex = 316;
            this.label98.Text = "$";
            // 
            // label99
            // 
            this.label99.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label99.Location = new System.Drawing.Point(471, 418);
            this.label99.Name = "label99";
            this.label99.Size = new System.Drawing.Size(16, 23);
            this.label99.TabIndex = 315;
            this.label99.Text = "$";
            // 
            // label100
            // 
            this.label100.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label100.Location = new System.Drawing.Point(309, 479);
            this.label100.Name = "label100";
            this.label100.Size = new System.Drawing.Size(162, 23);
            this.label100.TabIndex = 314;
            this.label100.Text = "Gross Operating Income (GOI):";
            // 
            // label101
            // 
            this.label101.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label101.Location = new System.Drawing.Point(395, 458);
            this.label101.Name = "label101";
            this.label101.Size = new System.Drawing.Size(76, 23);
            this.label101.TabIndex = 313;
            this.label101.Text = "Other Income:";
            // 
            // label102
            // 
            this.label102.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label102.Location = new System.Drawing.Point(359, 437);
            this.label102.Name = "label102";
            this.label102.Size = new System.Drawing.Size(112, 23);
            this.label102.TabIndex = 312;
            this.label102.Text = "Vacancy/Credit Loss:";
            // 
            // label103
            // 
            this.label103.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label103.Location = new System.Drawing.Point(375, 418);
            this.label103.Name = "label103";
            this.label103.Size = new System.Drawing.Size(96, 23);
            this.label103.TabIndex = 311;
            this.label103.Text = "Gross Rent (GSI):";
            // 
            // label95
            // 
            this.label95.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label95.Location = new System.Drawing.Point(79, 498);
            this.label95.Name = "label95";
            this.label95.Size = new System.Drawing.Size(16, 23);
            this.label95.TabIndex = 310;
            this.label95.Text = "$";
            // 
            // label93
            // 
            this.label93.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label93.Location = new System.Drawing.Point(79, 478);
            this.label93.Name = "label93";
            this.label93.Size = new System.Drawing.Size(16, 23);
            this.label93.TabIndex = 309;
            this.label93.Text = "$";
            // 
            // label92
            // 
            this.label92.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label92.Location = new System.Drawing.Point(79, 458);
            this.label92.Name = "label92";
            this.label92.Size = new System.Drawing.Size(16, 23);
            this.label92.TabIndex = 308;
            this.label92.Text = "$";
            // 
            // label91
            // 
            this.label91.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label91.Location = new System.Drawing.Point(79, 438);
            this.label91.Name = "label91";
            this.label91.Size = new System.Drawing.Size(16, 23);
            this.label91.TabIndex = 306;
            this.label91.Text = "$";
            // 
            // label90
            // 
            this.label90.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label90.Location = new System.Drawing.Point(79, 418);
            this.label90.Name = "label90";
            this.label90.Size = new System.Drawing.Size(16, 23);
            this.label90.TabIndex = 305;
            this.label90.Text = "$";
            // 
            // label89
            // 
            this.label89.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label89.Location = new System.Drawing.Point(25, 498);
            this.label89.Name = "label89";
            this.label89.Size = new System.Drawing.Size(48, 23);
            this.label89.TabIndex = 304;
            this.label89.Text = "Total:";
            this.label89.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label88
            // 
            this.label88.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label88.Location = new System.Drawing.Point(25, 478);
            this.label88.Name = "label88";
            this.label88.Size = new System.Drawing.Size(48, 23);
            this.label88.TabIndex = 303;
            this.label88.Text = "Other:";
            this.label88.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label87
            // 
            this.label87.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label87.Location = new System.Drawing.Point(24, 458);
            this.label87.Name = "label87";
            this.label87.Size = new System.Drawing.Size(49, 23);
            this.label87.TabIndex = 302;
            this.label87.Text = "Vending:";
            this.label87.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label86
            // 
            this.label86.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label86.Location = new System.Drawing.Point(25, 437);
            this.label86.Name = "label86";
            this.label86.Size = new System.Drawing.Size(48, 23);
            this.label86.TabIndex = 301;
            this.label86.Text = "Laundry:";
            this.label86.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label85
            // 
            this.label85.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label85.Location = new System.Drawing.Point(25, 418);
            this.label85.Name = "label85";
            this.label85.Size = new System.Drawing.Size(48, 23);
            this.label85.TabIndex = 300;
            this.label85.Text = "Parking:";
            this.label85.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label83
            // 
            this.label83.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label83.Location = new System.Drawing.Point(9, 391);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(168, 23);
            this.label83.TabIndex = 292;
            this.label83.Text = "OTHER INCOME (annual):";
            // 
            // _I_rentRoll
            // 
            this._I_rentRoll.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._I_rentRoll.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader5,
            this.columnHeader6,
            this.columnHeader7,
            this.columnHeader8,
            this.columnHeader9,
            this.columnHeader10,
            this.columnHeader11,
            this.columnHeader12});
            this._I_rentRoll.FullRowSelect = true;
            this._I_rentRoll.GridLines = true;
            this._I_rentRoll.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this._I_rentRoll.Location = new System.Drawing.Point(38, 255);
            this._I_rentRoll.MultiSelect = false;
            this._I_rentRoll.Name = "_I_rentRoll";
            this._I_rentRoll.Size = new System.Drawing.Size(856, 97);
            this._I_rentRoll.TabIndex = 262;
            this._I_rentRoll.UseCompatibleStateImageBehavior = false;
            this._I_rentRoll.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Unit#";
            this.columnHeader1.Width = 47;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Tenant Name";
            this.columnHeader2.Width = 94;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Bed/Bath";
            this.columnHeader3.Width = 59;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "SqFt";
            this.columnHeader4.Width = 51;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Sec. Dep.";
            this.columnHeader5.Width = 64;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "Tenant Rent";
            this.columnHeader6.Width = 80;
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "Sec. 8 Rent";
            this.columnHeader7.Width = 71;
            // 
            // columnHeader8
            // 
            this.columnHeader8.Text = "Total Rent";
            this.columnHeader8.Width = 65;
            // 
            // columnHeader9
            // 
            this.columnHeader9.Text = "Market Rent";
            this.columnHeader9.Width = 73;
            // 
            // columnHeader10
            // 
            this.columnHeader10.Text = "Occup. Date:";
            this.columnHeader10.Width = 78;
            // 
            // columnHeader11
            // 
            this.columnHeader11.Text = "Lease Term";
            this.columnHeader11.Width = 68;
            // 
            // columnHeader12
            // 
            this.columnHeader12.Text = "Comment";
            this.columnHeader12.Width = 84;
            // 
            // label70
            // 
            this.label70.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.label70.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label70.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label70.Location = new System.Drawing.Point(38, 231);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(168, 23);
            this.label70.TabIndex = 291;
            this.label70.Text = "RENT ROLL:";
            // 
            // label69
            // 
            this.label69.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label69.Location = new System.Drawing.Point(664, 7);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(88, 23);
            this.label69.TabIndex = 290;
            this.label69.Text = "Market Rent:";
            // 
            // label68
            // 
            this.label68.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label68.Location = new System.Drawing.Point(505, 7);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(88, 23);
            this.label68.TabIndex = 289;
            this.label68.Text = "Actual Rent:";
            // 
            // label67
            // 
            this.label67.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label67.Location = new System.Drawing.Point(385, 7);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(88, 23);
            this.label67.TabIndex = 288;
            this.label67.Text = "Square Feet:";
            // 
            // _I_UTS_Total_MR_USDperSqFt
            // 
            this._I_UTS_Total_MR_USDperSqFt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._I_UTS_Total_MR_USDperSqFt.Location = new System.Drawing.Point(769, 195);
            this._I_UTS_Total_MR_USDperSqFt.Name = "_I_UTS_Total_MR_USDperSqFt";
            this._I_UTS_Total_MR_USDperSqFt.ReadOnly = true;
            this._I_UTS_Total_MR_USDperSqFt.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this._I_UTS_Total_MR_USDperSqFt.Size = new System.Drawing.Size(57, 20);
            this._I_UTS_Total_MR_USDperSqFt.TabIndex = 276;
            this._I_UTS_Total_MR_USDperSqFt.TabStop = false;
            // 
            // _I_UTS_Total_MR_High
            // 
            this._I_UTS_Total_MR_High.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._I_UTS_Total_MR_High.Location = new System.Drawing.Point(711, 195);
            this._I_UTS_Total_MR_High.Name = "_I_UTS_Total_MR_High";
            this._I_UTS_Total_MR_High.ReadOnly = true;
            this._I_UTS_Total_MR_High.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this._I_UTS_Total_MR_High.Size = new System.Drawing.Size(58, 20);
            this._I_UTS_Total_MR_High.TabIndex = 275;
            this._I_UTS_Total_MR_High.TabStop = false;
            // 
            // _I_UTS_Total_MR_Low
            // 
            this._I_UTS_Total_MR_Low.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._I_UTS_Total_MR_Low.Location = new System.Drawing.Point(656, 195);
            this._I_UTS_Total_MR_Low.Name = "_I_UTS_Total_MR_Low";
            this._I_UTS_Total_MR_Low.ReadOnly = true;
            this._I_UTS_Total_MR_Low.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this._I_UTS_Total_MR_Low.Size = new System.Drawing.Size(55, 20);
            this._I_UTS_Total_MR_Low.TabIndex = 273;
            this._I_UTS_Total_MR_Low.TabStop = false;
            // 
            // _I_UTS_Total_AR_UsdPerSqFt
            // 
            this._I_UTS_Total_AR_UsdPerSqFt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._I_UTS_Total_AR_UsdPerSqFt.Location = new System.Drawing.Point(597, 195);
            this._I_UTS_Total_AR_UsdPerSqFt.Name = "_I_UTS_Total_AR_UsdPerSqFt";
            this._I_UTS_Total_AR_UsdPerSqFt.ReadOnly = true;
            this._I_UTS_Total_AR_UsdPerSqFt.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this._I_UTS_Total_AR_UsdPerSqFt.Size = new System.Drawing.Size(59, 20);
            this._I_UTS_Total_AR_UsdPerSqFt.TabIndex = 272;
            this._I_UTS_Total_AR_UsdPerSqFt.TabStop = false;
            // 
            // _I_UTS_Total_AR_High
            // 
            this._I_UTS_Total_AR_High.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._I_UTS_Total_AR_High.Location = new System.Drawing.Point(540, 195);
            this._I_UTS_Total_AR_High.Name = "_I_UTS_Total_AR_High";
            this._I_UTS_Total_AR_High.ReadOnly = true;
            this._I_UTS_Total_AR_High.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this._I_UTS_Total_AR_High.Size = new System.Drawing.Size(57, 20);
            this._I_UTS_Total_AR_High.TabIndex = 269;
            this._I_UTS_Total_AR_High.TabStop = false;
            // 
            // _I_UTS_Total_AR_Low
            // 
            this._I_UTS_Total_AR_Low.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._I_UTS_Total_AR_Low.Location = new System.Drawing.Point(485, 195);
            this._I_UTS_Total_AR_Low.Name = "_I_UTS_Total_AR_Low";
            this._I_UTS_Total_AR_Low.ReadOnly = true;
            this._I_UTS_Total_AR_Low.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this._I_UTS_Total_AR_Low.Size = new System.Drawing.Size(55, 20);
            this._I_UTS_Total_AR_Low.TabIndex = 267;
            this._I_UTS_Total_AR_Low.TabStop = false;
            // 
            // _I_UTS_Total_SQ_High
            // 
            this._I_UTS_Total_SQ_High.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._I_UTS_Total_SQ_High.Location = new System.Drawing.Point(430, 195);
            this._I_UTS_Total_SQ_High.Name = "_I_UTS_Total_SQ_High";
            this._I_UTS_Total_SQ_High.ReadOnly = true;
            this._I_UTS_Total_SQ_High.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this._I_UTS_Total_SQ_High.Size = new System.Drawing.Size(55, 20);
            this._I_UTS_Total_SQ_High.TabIndex = 265;
            this._I_UTS_Total_SQ_High.TabStop = false;
            // 
            // _I_UTS_Total_SQ_Low
            // 
            this._I_UTS_Total_SQ_Low.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._I_UTS_Total_SQ_Low.Location = new System.Drawing.Point(375, 195);
            this._I_UTS_Total_SQ_Low.Name = "_I_UTS_Total_SQ_Low";
            this._I_UTS_Total_SQ_Low.ReadOnly = true;
            this._I_UTS_Total_SQ_Low.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this._I_UTS_Total_SQ_Low.Size = new System.Drawing.Size(55, 20);
            this._I_UTS_Total_SQ_Low.TabIndex = 263;
            this._I_UTS_Total_SQ_Low.TabStop = false;
            // 
            // _I_UTS_TotalOccupancy
            // 
            this._I_UTS_TotalOccupancy.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._I_UTS_TotalOccupancy.Location = new System.Drawing.Point(304, 195);
            this._I_UTS_TotalOccupancy.Name = "_I_UTS_TotalOccupancy";
            this._I_UTS_TotalOccupancy.ReadOnly = true;
            this._I_UTS_TotalOccupancy.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this._I_UTS_TotalOccupancy.Size = new System.Drawing.Size(71, 20);
            this._I_UTS_TotalOccupancy.TabIndex = 261;
            this._I_UTS_TotalOccupancy.TabStop = false;
            // 
            // _I_UTS_TotalUnits
            // 
            this._I_UTS_TotalUnits.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._I_UTS_TotalUnits.Location = new System.Drawing.Point(41, 195);
            this._I_UTS_TotalUnits.Name = "_I_UTS_TotalUnits";
            this._I_UTS_TotalUnits.ReadOnly = true;
            this._I_UTS_TotalUnits.Size = new System.Drawing.Size(56, 20);
            this._I_UTS_TotalUnits.TabIndex = 260;
            // 
            // label66
            // 
            this.label66.Location = new System.Drawing.Point(1, 198);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(38, 23);
            this.label66.TabIndex = 287;
            this.label66.Text = "Total:";
            this.label66.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label52
            // 
            this.label52.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.label52.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label52.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label52.Location = new System.Drawing.Point(40, 7);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(168, 23);
            this.label52.TabIndex = 258;
            this.label52.Text = "UNIT TYPE SUMMARY";
            // 
            // label104
            // 
            this.label104.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label104.Location = new System.Drawing.Point(377, 391);
            this.label104.Name = "label104";
            this.label104.Size = new System.Drawing.Size(168, 23);
            this.label104.TabIndex = 319;
            this.label104.Text = "ACTUAL INCOME (annual):";
            // 
            // label105
            // 
            this.label105.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label105.Location = new System.Drawing.Point(601, 439);
            this.label105.Name = "label105";
            this.label105.Size = new System.Drawing.Size(16, 23);
            this.label105.TabIndex = 320;
            this.label105.Text = "%";
            // 
            // label115
            // 
            this.label115.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label115.Location = new System.Drawing.Point(879, 439);
            this.label115.Name = "label115";
            this.label115.Size = new System.Drawing.Size(16, 23);
            this.label115.TabIndex = 330;
            this.label115.Text = "%";
            // 
            // label84
            // 
            this.label84.Location = new System.Drawing.Point(185, 353);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(38, 23);
            this.label84.TabIndex = 299;
            this.label84.Text = "Total:";
            this.label84.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _I_RR_TotalMarketRent
            // 
            this._I_RR_TotalMarketRent.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._I_RR_TotalMarketRent.Location = new System.Drawing.Point(564, 353);
            this._I_RR_TotalMarketRent.Name = "_I_RR_TotalMarketRent";
            this._I_RR_TotalMarketRent.ReadOnly = true;
            this._I_RR_TotalMarketRent.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this._I_RR_TotalMarketRent.Size = new System.Drawing.Size(72, 20);
            this._I_RR_TotalMarketRent.TabIndex = 298;
            this._I_RR_TotalMarketRent.TabStop = false;
            // 
            // _I_RR_TotalTotalRent
            // 
            this._I_RR_TotalTotalRent.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._I_RR_TotalTotalRent.Location = new System.Drawing.Point(499, 353);
            this._I_RR_TotalTotalRent.Name = "_I_RR_TotalTotalRent";
            this._I_RR_TotalTotalRent.ReadOnly = true;
            this._I_RR_TotalTotalRent.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this._I_RR_TotalTotalRent.Size = new System.Drawing.Size(65, 20);
            this._I_RR_TotalTotalRent.TabIndex = 297;
            this._I_RR_TotalTotalRent.TabStop = false;
            // 
            // _I_RR_TotalSec8Rent
            // 
            this._I_RR_TotalSec8Rent.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._I_RR_TotalSec8Rent.Location = new System.Drawing.Point(429, 353);
            this._I_RR_TotalSec8Rent.Name = "_I_RR_TotalSec8Rent";
            this._I_RR_TotalSec8Rent.ReadOnly = true;
            this._I_RR_TotalSec8Rent.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this._I_RR_TotalSec8Rent.Size = new System.Drawing.Size(70, 20);
            this._I_RR_TotalSec8Rent.TabIndex = 296;
            this._I_RR_TotalSec8Rent.TabStop = false;
            // 
            // _I_RR_TotalTennantRent
            // 
            this._I_RR_TotalTennantRent.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._I_RR_TotalTennantRent.Location = new System.Drawing.Point(350, 353);
            this._I_RR_TotalTennantRent.Name = "_I_RR_TotalTennantRent";
            this._I_RR_TotalTennantRent.ReadOnly = true;
            this._I_RR_TotalTennantRent.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this._I_RR_TotalTennantRent.Size = new System.Drawing.Size(79, 20);
            this._I_RR_TotalTennantRent.TabIndex = 295;
            this._I_RR_TotalTennantRent.TabStop = false;
            // 
            // _I_RR_TotalSecDep
            // 
            this._I_RR_TotalSecDep.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._I_RR_TotalSecDep.Location = new System.Drawing.Point(284, 353);
            this._I_RR_TotalSecDep.Name = "_I_RR_TotalSecDep";
            this._I_RR_TotalSecDep.ReadOnly = true;
            this._I_RR_TotalSecDep.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this._I_RR_TotalSecDep.Size = new System.Drawing.Size(66, 20);
            this._I_RR_TotalSecDep.TabIndex = 294;
            this._I_RR_TotalSecDep.TabStop = false;
            // 
            // _I_RR_TotalSqFt
            // 
            this._I_RR_TotalSqFt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._I_RR_TotalSqFt.Location = new System.Drawing.Point(230, 353);
            this._I_RR_TotalSqFt.Name = "_I_RR_TotalSqFt";
            this._I_RR_TotalSqFt.ReadOnly = true;
            this._I_RR_TotalSqFt.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this._I_RR_TotalSqFt.Size = new System.Drawing.Size(54, 20);
            this._I_RR_TotalSqFt.TabIndex = 293;
            this._I_RR_TotalSqFt.TabStop = false;
            // 
            // Income_Multifamily
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.Controls.Add(this._printRentRoll);
            this.Controls.Add(this._printIncomeSummary);
            this.Controls.Add(this._I_AI_VacancyOrCreditLossPerc);
            this.Controls.Add(this._I_PI_VacancyOrCreditLossPerc);
            this.Controls.Add(this._I_OI_Total);
            this.Controls.Add(this._I_OI_Other);
            this.Controls.Add(this._I_OI_Vending);
            this.Controls.Add(this._I_OI_Laundry);
            this.Controls.Add(this._I_OI_Parking);
            this.Controls.Add(this._I_AI_OperatingIncome);
            this.Controls.Add(this._I_AI_OtherIncome);
            this.Controls.Add(this._I_AI_VacancyOrCreditLoss);
            this.Controls.Add(this._I_AI_GrossRent);
            this.Controls.Add(this._I_PI_OperatingIncome);
            this.Controls.Add(this._I_PI_OtherIncome);
            this.Controls.Add(this._I_PI_VacancyOrCreditLoss);
            this.Controls.Add(this._I_PI_GrossRent);
            this.Controls.Add(this._i_floater);
            this.Controls.Add(this._I_unitTypeSummary);
            this.Controls.Add(this._calculateFromRentRoll);
            this.Controls.Add(this._CalculateFromUnitTypeSummary);
            this.Controls.Add(this.label106);
            this.Controls.Add(this.label107);
            this.Controls.Add(this.label108);
            this.Controls.Add(this.label109);
            this.Controls.Add(this.label110);
            this.Controls.Add(this.label111);
            this.Controls.Add(this.label112);
            this.Controls.Add(this.label113);
            this.Controls.Add(this.label114);
            this.Controls.Add(this.label96);
            this.Controls.Add(this.label97);
            this.Controls.Add(this.label98);
            this.Controls.Add(this.label99);
            this.Controls.Add(this.label100);
            this.Controls.Add(this.label101);
            this.Controls.Add(this.label102);
            this.Controls.Add(this.label103);
            this.Controls.Add(this.label95);
            this.Controls.Add(this.label93);
            this.Controls.Add(this.label92);
            this.Controls.Add(this.label91);
            this.Controls.Add(this.label90);
            this.Controls.Add(this.label89);
            this.Controls.Add(this.label88);
            this.Controls.Add(this.label87);
            this.Controls.Add(this.label86);
            this.Controls.Add(this.label85);
            this.Controls.Add(this.label83);
            this.Controls.Add(this._I_rentRoll);
            this.Controls.Add(this.label70);
            this.Controls.Add(this.label69);
            this.Controls.Add(this.label68);
            this.Controls.Add(this.label67);
            this.Controls.Add(this._I_UTS_Total_MR_USDperSqFt);
            this.Controls.Add(this._I_UTS_Total_MR_High);
            this.Controls.Add(this._I_UTS_Total_MR_Low);
            this.Controls.Add(this._I_UTS_Total_AR_UsdPerSqFt);
            this.Controls.Add(this._I_UTS_Total_AR_High);
            this.Controls.Add(this._I_UTS_Total_AR_Low);
            this.Controls.Add(this._I_UTS_Total_SQ_High);
            this.Controls.Add(this._I_UTS_Total_SQ_Low);
            this.Controls.Add(this._I_UTS_TotalOccupancy);
            this.Controls.Add(this._I_UTS_TotalUnits);
            this.Controls.Add(this.label66);
            this.Controls.Add(this.label52);
            this.Controls.Add(this.label104);
            this.Controls.Add(this.label105);
            this.Controls.Add(this.label115);
            this.Controls.Add(this.label84);
            this.Controls.Add(this._I_RR_TotalMarketRent);
            this.Controls.Add(this._I_RR_TotalTotalRent);
            this.Controls.Add(this._I_RR_TotalSec8Rent);
            this.Controls.Add(this._I_RR_TotalTennantRent);
            this.Controls.Add(this._I_RR_TotalSecDep);
            this.Controls.Add(this._I_RR_TotalSqFt);
            this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.Name = "Income_Multifamily";
            this.Size = new System.Drawing.Size(897, 529);
            ((System.ComponentModel.ISupportInitialize)(this._i_floater)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button _printRentRoll;
        private System.Windows.Forms.Button _printIncomeSummary;
        private System.Windows.Forms.TextBox _I_AI_VacancyOrCreditLossPerc;
        private System.Windows.Forms.TextBox _I_PI_VacancyOrCreditLossPerc;
        private System.Windows.Forms.TextBox _I_OI_Total;
        private System.Windows.Forms.TextBox _I_OI_Other;
        private System.Windows.Forms.TextBox _I_OI_Vending;
        private System.Windows.Forms.TextBox _I_OI_Laundry;
        private System.Windows.Forms.TextBox _I_OI_Parking;
        private System.Windows.Forms.TextBox _I_AI_OperatingIncome;
        private System.Windows.Forms.TextBox _I_AI_OtherIncome;
        private System.Windows.Forms.TextBox _I_AI_VacancyOrCreditLoss;
        private System.Windows.Forms.TextBox _I_AI_GrossRent;
        private System.Windows.Forms.TextBox _I_PI_OperatingIncome;
        private System.Windows.Forms.TextBox _I_PI_OtherIncome;
        private System.Windows.Forms.TextBox _I_PI_VacancyOrCreditLoss;
        private System.Windows.Forms.TextBox _I_PI_GrossRent;
        private System.Windows.Forms.PictureBox _i_floater;
        private ListViewEx _I_unitTypeSummary;
        private System.Windows.Forms.ColumnHeader columnHeader23;
        private System.Windows.Forms.ColumnHeader columnHeader24;
        private System.Windows.Forms.ColumnHeader columnHeader25;
        private System.Windows.Forms.ColumnHeader columnHeader26;
        private System.Windows.Forms.ColumnHeader columnHeader27;
        private System.Windows.Forms.ColumnHeader columnHeader28;
        private System.Windows.Forms.ColumnHeader columnHeader29;
        private System.Windows.Forms.ColumnHeader columnHeader30;
        private System.Windows.Forms.ColumnHeader columnHeader31;
        private System.Windows.Forms.ColumnHeader columnHeader32;
        private System.Windows.Forms.ColumnHeader columnHeader33;
        private System.Windows.Forms.ColumnHeader columnHeader34;
        private System.Windows.Forms.ColumnHeader columnHeader35;
        private System.Windows.Forms.Button _calculateFromRentRoll;
        private System.Windows.Forms.Button _CalculateFromUnitTypeSummary;
        private System.Windows.Forms.Label label106;
        private System.Windows.Forms.Label label107;
        private System.Windows.Forms.Label label108;
        private System.Windows.Forms.Label label109;
        private System.Windows.Forms.Label label110;
        private System.Windows.Forms.Label label111;
        private System.Windows.Forms.Label label112;
        private System.Windows.Forms.Label label113;
        private System.Windows.Forms.Label label114;
        private System.Windows.Forms.Label label96;
        private System.Windows.Forms.Label label97;
        private System.Windows.Forms.Label label98;
        private System.Windows.Forms.Label label99;
        private System.Windows.Forms.Label label100;
        private System.Windows.Forms.Label label101;
        private System.Windows.Forms.Label label102;
        private System.Windows.Forms.Label label103;
        private System.Windows.Forms.Label label95;
        private System.Windows.Forms.Label label93;
        private System.Windows.Forms.Label label92;
        private System.Windows.Forms.Label label91;
        private System.Windows.Forms.Label label90;
        private System.Windows.Forms.Label label89;
        private System.Windows.Forms.Label label88;
        private System.Windows.Forms.Label label87;
        private System.Windows.Forms.Label label86;
        private System.Windows.Forms.Label label85;
        private System.Windows.Forms.Label label83;
        private ListViewEx _I_rentRoll;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.ColumnHeader columnHeader8;
        private System.Windows.Forms.ColumnHeader columnHeader9;
        private System.Windows.Forms.ColumnHeader columnHeader10;
        private System.Windows.Forms.ColumnHeader columnHeader11;
        private System.Windows.Forms.ColumnHeader columnHeader12;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.TextBox _I_UTS_Total_MR_USDperSqFt;
        private System.Windows.Forms.TextBox _I_UTS_Total_MR_High;
        private System.Windows.Forms.TextBox _I_UTS_Total_MR_Low;
        private System.Windows.Forms.TextBox _I_UTS_Total_AR_UsdPerSqFt;
        private System.Windows.Forms.TextBox _I_UTS_Total_AR_High;
        private System.Windows.Forms.TextBox _I_UTS_Total_AR_Low;
        private System.Windows.Forms.TextBox _I_UTS_Total_SQ_High;
        private System.Windows.Forms.TextBox _I_UTS_Total_SQ_Low;
        private System.Windows.Forms.TextBox _I_UTS_TotalOccupancy;
        private System.Windows.Forms.TextBox _I_UTS_TotalUnits;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label104;
        private System.Windows.Forms.Label label105;
        private System.Windows.Forms.Label label115;
        private System.Windows.Forms.Label label84;
        private System.Windows.Forms.TextBox _I_RR_TotalMarketRent;
        private System.Windows.Forms.TextBox _I_RR_TotalTotalRent;
        private System.Windows.Forms.TextBox _I_RR_TotalSec8Rent;
        private System.Windows.Forms.TextBox _I_RR_TotalTennantRent;
        private System.Windows.Forms.TextBox _I_RR_TotalSecDep;
        private System.Windows.Forms.TextBox _I_RR_TotalSqFt;
    }
}
