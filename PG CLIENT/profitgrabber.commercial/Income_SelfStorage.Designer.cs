﻿namespace ProfitGrabber.Commercial
{
    partial class Income_SelfStorage
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._I_S_MR_UsdPerSqFt = new System.Windows.Forms.TextBox();
            this._I_S_MR_High = new System.Windows.Forms.TextBox();
            this._I_S_MR_Low = new System.Windows.Forms.TextBox();
            this._I_S_AR_UsdPerSqFt = new System.Windows.Forms.TextBox();
            this._I_S_AR_High = new System.Windows.Forms.TextBox();
            this._I_S_AR_Low = new System.Windows.Forms.TextBox();
            this._I_S_TotalSqFt = new System.Windows.Forms.TextBox();
            this._I_S_TotalOccupancy = new System.Windows.Forms.TextBox();
            this._I_S_TotalUnits = new System.Windows.Forms.TextBox();
            this.label66 = new System.Windows.Forms.Label();
            this._I_AI_VacancyOrCreditLossPerc = new System.Windows.Forms.TextBox();
            this._I_PI_VacancyOrCreditLossPerc = new System.Windows.Forms.TextBox();
            this._I_AI_OperatingIncome = new System.Windows.Forms.TextBox();
            this._I_AI_OtherIncome = new System.Windows.Forms.TextBox();
            this._I_AI_VacancyOrCreditLoss = new System.Windows.Forms.TextBox();
            this._I_AI_GrossRent = new System.Windows.Forms.TextBox();
            this._I_PI_OperatingIncome = new System.Windows.Forms.TextBox();
            this._I_PI_OtherIncome = new System.Windows.Forms.TextBox();
            this._I_PI_VacancyOrCreditLoss = new System.Windows.Forms.TextBox();
            this._I_PI_GrossRent = new System.Windows.Forms.TextBox();
            this.label106 = new System.Windows.Forms.Label();
            this.label107 = new System.Windows.Forms.Label();
            this.label108 = new System.Windows.Forms.Label();
            this.label109 = new System.Windows.Forms.Label();
            this.label110 = new System.Windows.Forms.Label();
            this.label111 = new System.Windows.Forms.Label();
            this.label112 = new System.Windows.Forms.Label();
            this.label113 = new System.Windows.Forms.Label();
            this.label114 = new System.Windows.Forms.Label();
            this.label96 = new System.Windows.Forms.Label();
            this.label97 = new System.Windows.Forms.Label();
            this.label98 = new System.Windows.Forms.Label();
            this.label99 = new System.Windows.Forms.Label();
            this.label100 = new System.Windows.Forms.Label();
            this.label101 = new System.Windows.Forms.Label();
            this.label102 = new System.Windows.Forms.Label();
            this.label103 = new System.Windows.Forms.Label();
            this.label104 = new System.Windows.Forms.Label();
            this.label105 = new System.Windows.Forms.Label();
            this.label115 = new System.Windows.Forms.Label();
            this.label68 = new System.Windows.Forms.Label();
            this.label69 = new System.Windows.Forms.Label();
            this._printIncomeReport = new System.Windows.Forms.Button();
            this._I_Summary = new ProfitGrabber.Commercial.ListViewEx();
            this.columnHeader23 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader24 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader25 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader26 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader27 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader28 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader29 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader30 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader31 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader32 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader33 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader34 = new System.Windows.Forms.ColumnHeader();
            this.SuspendLayout();
            // 
            // _I_S_MR_UsdPerSqFt
            // 
            this._I_S_MR_UsdPerSqFt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._I_S_MR_UsdPerSqFt.Location = new System.Drawing.Point(655, 331);
            this._I_S_MR_UsdPerSqFt.Name = "_I_S_MR_UsdPerSqFt";
            this._I_S_MR_UsdPerSqFt.ReadOnly = true;
            this._I_S_MR_UsdPerSqFt.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this._I_S_MR_UsdPerSqFt.Size = new System.Drawing.Size(59, 20);
            this._I_S_MR_UsdPerSqFt.TabIndex = 296;
            this._I_S_MR_UsdPerSqFt.TabStop = false;
            // 
            // _I_S_MR_High
            // 
            this._I_S_MR_High.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._I_S_MR_High.Location = new System.Drawing.Point(591, 331);
            this._I_S_MR_High.Name = "_I_S_MR_High";
            this._I_S_MR_High.ReadOnly = true;
            this._I_S_MR_High.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this._I_S_MR_High.Size = new System.Drawing.Size(65, 20);
            this._I_S_MR_High.TabIndex = 295;
            this._I_S_MR_High.TabStop = false;
            // 
            // _I_S_MR_Low
            // 
            this._I_S_MR_Low.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._I_S_MR_Low.Location = new System.Drawing.Point(530, 331);
            this._I_S_MR_Low.Name = "_I_S_MR_Low";
            this._I_S_MR_Low.ReadOnly = true;
            this._I_S_MR_Low.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this._I_S_MR_Low.Size = new System.Drawing.Size(62, 20);
            this._I_S_MR_Low.TabIndex = 294;
            this._I_S_MR_Low.TabStop = false;
            // 
            // _I_S_AR_UsdPerSqFt
            // 
            this._I_S_AR_UsdPerSqFt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._I_S_AR_UsdPerSqFt.Location = new System.Drawing.Point(476, 331);
            this._I_S_AR_UsdPerSqFt.Name = "_I_S_AR_UsdPerSqFt";
            this._I_S_AR_UsdPerSqFt.ReadOnly = true;
            this._I_S_AR_UsdPerSqFt.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this._I_S_AR_UsdPerSqFt.Size = new System.Drawing.Size(55, 20);
            this._I_S_AR_UsdPerSqFt.TabIndex = 293;
            this._I_S_AR_UsdPerSqFt.TabStop = false;
            // 
            // _I_S_AR_High
            // 
            this._I_S_AR_High.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._I_S_AR_High.Location = new System.Drawing.Point(421, 331);
            this._I_S_AR_High.Name = "_I_S_AR_High";
            this._I_S_AR_High.ReadOnly = true;
            this._I_S_AR_High.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this._I_S_AR_High.Size = new System.Drawing.Size(56, 20);
            this._I_S_AR_High.TabIndex = 292;
            this._I_S_AR_High.TabStop = false;
            // 
            // _I_S_AR_Low
            // 
            this._I_S_AR_Low.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._I_S_AR_Low.Location = new System.Drawing.Point(364, 331);
            this._I_S_AR_Low.Name = "_I_S_AR_Low";
            this._I_S_AR_Low.ReadOnly = true;
            this._I_S_AR_Low.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this._I_S_AR_Low.Size = new System.Drawing.Size(58, 20);
            this._I_S_AR_Low.TabIndex = 291;
            this._I_S_AR_Low.TabStop = false;
            // 
            // _I_S_TotalSqFt
            // 
            this._I_S_TotalSqFt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._I_S_TotalSqFt.Location = new System.Drawing.Point(295, 331);
            this._I_S_TotalSqFt.Name = "_I_S_TotalSqFt";
            this._I_S_TotalSqFt.ReadOnly = true;
            this._I_S_TotalSqFt.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this._I_S_TotalSqFt.Size = new System.Drawing.Size(70, 20);
            this._I_S_TotalSqFt.TabIndex = 290;
            this._I_S_TotalSqFt.TabStop = false;
            // 
            // _I_S_TotalOccupancy
            // 
            this._I_S_TotalOccupancy.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._I_S_TotalOccupancy.Location = new System.Drawing.Point(190, 331);
            this._I_S_TotalOccupancy.Name = "_I_S_TotalOccupancy";
            this._I_S_TotalOccupancy.ReadOnly = true;
            this._I_S_TotalOccupancy.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this._I_S_TotalOccupancy.Size = new System.Drawing.Size(106, 20);
            this._I_S_TotalOccupancy.TabIndex = 289;
            this._I_S_TotalOccupancy.TabStop = false;
            // 
            // _I_S_TotalUnits
            // 
            this._I_S_TotalUnits.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._I_S_TotalUnits.Location = new System.Drawing.Point(31, 331);
            this._I_S_TotalUnits.Name = "_I_S_TotalUnits";
            this._I_S_TotalUnits.ReadOnly = true;
            this._I_S_TotalUnits.Size = new System.Drawing.Size(56, 20);
            this._I_S_TotalUnits.TabIndex = 288;
            // 
            // label66
            // 
            this.label66.Location = new System.Drawing.Point(-5, 334);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(38, 23);
            this.label66.TabIndex = 298;
            this.label66.Text = "Total:";
            this.label66.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // _I_AI_VacancyOrCreditLossPerc
            // 
            this._I_AI_VacancyOrCreditLossPerc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._I_AI_VacancyOrCreditLossPerc.Location = new System.Drawing.Point(515, 413);
            this._I_AI_VacancyOrCreditLossPerc.Name = "_I_AI_VacancyOrCreditLossPerc";
            this._I_AI_VacancyOrCreditLossPerc.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this._I_AI_VacancyOrCreditLossPerc.Size = new System.Drawing.Size(56, 20);
            this._I_AI_VacancyOrCreditLossPerc.TabIndex = 333;
            // 
            // _I_PI_VacancyOrCreditLossPerc
            // 
            this._I_PI_VacancyOrCreditLossPerc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._I_PI_VacancyOrCreditLossPerc.Location = new System.Drawing.Point(795, 413);
            this._I_PI_VacancyOrCreditLossPerc.Name = "_I_PI_VacancyOrCreditLossPerc";
            this._I_PI_VacancyOrCreditLossPerc.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this._I_PI_VacancyOrCreditLossPerc.Size = new System.Drawing.Size(54, 20);
            this._I_PI_VacancyOrCreditLossPerc.TabIndex = 338;
            // 
            // _I_AI_OperatingIncome
            // 
            this._I_AI_OperatingIncome.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._I_AI_OperatingIncome.Location = new System.Drawing.Point(451, 453);
            this._I_AI_OperatingIncome.Name = "_I_AI_OperatingIncome";
            this._I_AI_OperatingIncome.ReadOnly = true;
            this._I_AI_OperatingIncome.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this._I_AI_OperatingIncome.Size = new System.Drawing.Size(65, 20);
            this._I_AI_OperatingIncome.TabIndex = 335;
            this._I_AI_OperatingIncome.TabStop = false;
            // 
            // _I_AI_OtherIncome
            // 
            this._I_AI_OtherIncome.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._I_AI_OtherIncome.Location = new System.Drawing.Point(451, 433);
            this._I_AI_OtherIncome.Name = "_I_AI_OtherIncome";
            this._I_AI_OtherIncome.ReadOnly = true;
            this._I_AI_OtherIncome.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this._I_AI_OtherIncome.Size = new System.Drawing.Size(65, 20);
            this._I_AI_OtherIncome.TabIndex = 334;
            this._I_AI_OtherIncome.TabStop = false;
            // 
            // _I_AI_VacancyOrCreditLoss
            // 
            this._I_AI_VacancyOrCreditLoss.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._I_AI_VacancyOrCreditLoss.Location = new System.Drawing.Point(451, 413);
            this._I_AI_VacancyOrCreditLoss.Name = "_I_AI_VacancyOrCreditLoss";
            this._I_AI_VacancyOrCreditLoss.ReadOnly = true;
            this._I_AI_VacancyOrCreditLoss.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this._I_AI_VacancyOrCreditLoss.Size = new System.Drawing.Size(65, 20);
            this._I_AI_VacancyOrCreditLoss.TabIndex = 332;
            this._I_AI_VacancyOrCreditLoss.TabStop = false;
            // 
            // _I_AI_GrossRent
            // 
            this._I_AI_GrossRent.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._I_AI_GrossRent.Location = new System.Drawing.Point(451, 393);
            this._I_AI_GrossRent.Name = "_I_AI_GrossRent";
            this._I_AI_GrossRent.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this._I_AI_GrossRent.Size = new System.Drawing.Size(65, 20);
            this._I_AI_GrossRent.TabIndex = 331;
            // 
            // _I_PI_OperatingIncome
            // 
            this._I_PI_OperatingIncome.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._I_PI_OperatingIncome.Location = new System.Drawing.Point(731, 453);
            this._I_PI_OperatingIncome.Name = "_I_PI_OperatingIncome";
            this._I_PI_OperatingIncome.ReadOnly = true;
            this._I_PI_OperatingIncome.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this._I_PI_OperatingIncome.Size = new System.Drawing.Size(65, 20);
            this._I_PI_OperatingIncome.TabIndex = 340;
            this._I_PI_OperatingIncome.TabStop = false;
            // 
            // _I_PI_OtherIncome
            // 
            this._I_PI_OtherIncome.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._I_PI_OtherIncome.Location = new System.Drawing.Point(731, 433);
            this._I_PI_OtherIncome.Name = "_I_PI_OtherIncome";
            this._I_PI_OtherIncome.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this._I_PI_OtherIncome.Size = new System.Drawing.Size(65, 20);
            this._I_PI_OtherIncome.TabIndex = 339;
            // 
            // _I_PI_VacancyOrCreditLoss
            // 
            this._I_PI_VacancyOrCreditLoss.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._I_PI_VacancyOrCreditLoss.Location = new System.Drawing.Point(731, 413);
            this._I_PI_VacancyOrCreditLoss.Name = "_I_PI_VacancyOrCreditLoss";
            this._I_PI_VacancyOrCreditLoss.ReadOnly = true;
            this._I_PI_VacancyOrCreditLoss.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this._I_PI_VacancyOrCreditLoss.Size = new System.Drawing.Size(65, 20);
            this._I_PI_VacancyOrCreditLoss.TabIndex = 337;
            this._I_PI_VacancyOrCreditLoss.TabStop = false;
            // 
            // _I_PI_GrossRent
            // 
            this._I_PI_GrossRent.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._I_PI_GrossRent.Location = new System.Drawing.Point(731, 393);
            this._I_PI_GrossRent.Name = "_I_PI_GrossRent";
            this._I_PI_GrossRent.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this._I_PI_GrossRent.Size = new System.Drawing.Size(65, 20);
            this._I_PI_GrossRent.TabIndex = 336;
            // 
            // label106
            // 
            this.label106.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label106.Location = new System.Drawing.Point(721, 457);
            this.label106.Name = "label106";
            this.label106.Size = new System.Drawing.Size(16, 23);
            this.label106.TabIndex = 358;
            this.label106.Text = "$";
            // 
            // label107
            // 
            this.label107.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label107.Location = new System.Drawing.Point(721, 436);
            this.label107.Name = "label107";
            this.label107.Size = new System.Drawing.Size(16, 23);
            this.label107.TabIndex = 357;
            this.label107.Text = "$";
            // 
            // label108
            // 
            this.label108.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label108.Location = new System.Drawing.Point(721, 415);
            this.label108.Name = "label108";
            this.label108.Size = new System.Drawing.Size(16, 23);
            this.label108.TabIndex = 356;
            this.label108.Text = "$";
            // 
            // label109
            // 
            this.label109.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label109.Location = new System.Drawing.Point(721, 396);
            this.label109.Name = "label109";
            this.label109.Size = new System.Drawing.Size(16, 23);
            this.label109.TabIndex = 355;
            this.label109.Text = "$";
            // 
            // label110
            // 
            this.label110.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label110.Location = new System.Drawing.Point(559, 457);
            this.label110.Name = "label110";
            this.label110.Size = new System.Drawing.Size(162, 23);
            this.label110.TabIndex = 354;
            this.label110.Text = "Gross Operating Income (GOI):";
            // 
            // label111
            // 
            this.label111.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label111.Location = new System.Drawing.Point(645, 436);
            this.label111.Name = "label111";
            this.label111.Size = new System.Drawing.Size(76, 23);
            this.label111.TabIndex = 353;
            this.label111.Text = "Other Income:";
            // 
            // label112
            // 
            this.label112.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label112.Location = new System.Drawing.Point(609, 415);
            this.label112.Name = "label112";
            this.label112.Size = new System.Drawing.Size(112, 23);
            this.label112.TabIndex = 352;
            this.label112.Text = "Vacancy/Credit Loss:";
            // 
            // label113
            // 
            this.label113.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label113.Location = new System.Drawing.Point(625, 396);
            this.label113.Name = "label113";
            this.label113.Size = new System.Drawing.Size(96, 23);
            this.label113.TabIndex = 351;
            this.label113.Text = "Gross Rent (GSI):";
            // 
            // label114
            // 
            this.label114.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label114.Location = new System.Drawing.Point(627, 369);
            this.label114.Name = "label114";
            this.label114.Size = new System.Drawing.Size(200, 23);
            this.label114.TabIndex = 359;
            this.label114.Text = "PROFORMA INCOME (annual):";
            // 
            // label96
            // 
            this.label96.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label96.Location = new System.Drawing.Point(441, 457);
            this.label96.Name = "label96";
            this.label96.Size = new System.Drawing.Size(16, 23);
            this.label96.TabIndex = 348;
            this.label96.Text = "$";
            // 
            // label97
            // 
            this.label97.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label97.Location = new System.Drawing.Point(441, 436);
            this.label97.Name = "label97";
            this.label97.Size = new System.Drawing.Size(16, 23);
            this.label97.TabIndex = 347;
            this.label97.Text = "$";
            // 
            // label98
            // 
            this.label98.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label98.Location = new System.Drawing.Point(441, 415);
            this.label98.Name = "label98";
            this.label98.Size = new System.Drawing.Size(16, 23);
            this.label98.TabIndex = 346;
            this.label98.Text = "$";
            // 
            // label99
            // 
            this.label99.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label99.Location = new System.Drawing.Point(441, 396);
            this.label99.Name = "label99";
            this.label99.Size = new System.Drawing.Size(16, 23);
            this.label99.TabIndex = 345;
            this.label99.Text = "$";
            // 
            // label100
            // 
            this.label100.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label100.Location = new System.Drawing.Point(279, 457);
            this.label100.Name = "label100";
            this.label100.Size = new System.Drawing.Size(162, 23);
            this.label100.TabIndex = 344;
            this.label100.Text = "Gross Operating Income (GOI):";
            // 
            // label101
            // 
            this.label101.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label101.Location = new System.Drawing.Point(365, 436);
            this.label101.Name = "label101";
            this.label101.Size = new System.Drawing.Size(76, 23);
            this.label101.TabIndex = 343;
            this.label101.Text = "Other Income:";
            // 
            // label102
            // 
            this.label102.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label102.Location = new System.Drawing.Point(329, 415);
            this.label102.Name = "label102";
            this.label102.Size = new System.Drawing.Size(112, 23);
            this.label102.TabIndex = 342;
            this.label102.Text = "Vacancy/Credit Loss:";
            // 
            // label103
            // 
            this.label103.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label103.Location = new System.Drawing.Point(345, 396);
            this.label103.Name = "label103";
            this.label103.Size = new System.Drawing.Size(96, 23);
            this.label103.TabIndex = 341;
            this.label103.Text = "Gross Rent (GSI):";
            // 
            // label104
            // 
            this.label104.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label104.Location = new System.Drawing.Point(347, 369);
            this.label104.Name = "label104";
            this.label104.Size = new System.Drawing.Size(168, 23);
            this.label104.TabIndex = 349;
            this.label104.Text = "ACTUAL INCOME (annual):";
            // 
            // label105
            // 
            this.label105.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label105.Location = new System.Drawing.Point(571, 417);
            this.label105.Name = "label105";
            this.label105.Size = new System.Drawing.Size(16, 23);
            this.label105.TabIndex = 350;
            this.label105.Text = "%";
            // 
            // label115
            // 
            this.label115.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label115.Location = new System.Drawing.Point(849, 417);
            this.label115.Name = "label115";
            this.label115.Size = new System.Drawing.Size(16, 23);
            this.label115.TabIndex = 360;
            this.label115.Text = "%";
            // 
            // label68
            // 
            this.label68.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label68.Location = new System.Drawing.Point(365, 5);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(88, 23);
            this.label68.TabIndex = 361;
            this.label68.Text = "Actual Rent:";
            // 
            // label69
            // 
            this.label69.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label69.Location = new System.Drawing.Point(527, 5);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(88, 23);
            this.label69.TabIndex = 362;
            this.label69.Text = "Market Rent:";
            // 
            // _printIncomeReport
            // 
            this._printIncomeReport.Location = new System.Drawing.Point(759, 479);
            this._printIncomeReport.Name = "_printIncomeReport";
            this._printIncomeReport.Size = new System.Drawing.Size(125, 41);
            this._printIncomeReport.TabIndex = 363;
            this._printIncomeReport.Text = "Print Income Report";
            this._printIncomeReport.UseVisualStyleBackColor = true;
            // 
            // _I_Summary
            // 
            this._I_Summary.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._I_Summary.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader23,
            this.columnHeader24,
            this.columnHeader25,
            this.columnHeader26,
            this.columnHeader27,
            this.columnHeader28,
            this.columnHeader29,
            this.columnHeader30,
            this.columnHeader31,
            this.columnHeader32,
            this.columnHeader33,
            this.columnHeader34});
            this._I_Summary.FullRowSelect = true;
            this._I_Summary.GridLines = true;
            this._I_Summary.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this._I_Summary.Location = new System.Drawing.Point(31, 31);
            this._I_Summary.Name = "_I_Summary";
            this._I_Summary.Size = new System.Drawing.Size(836, 294);
            this._I_Summary.TabIndex = 260;
            this._I_Summary.UseCompatibleStateImageBehavior = false;
            this._I_Summary.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader23
            // 
            this.columnHeader23.Text = "#Units";
            this.columnHeader23.Width = 52;
            // 
            // columnHeader24
            // 
            this.columnHeader24.Text = "Size";
            this.columnHeader24.Width = 61;
            // 
            // columnHeader25
            // 
            this.columnHeader25.Text = "Floor";
            this.columnHeader25.Width = 46;
            // 
            // columnHeader26
            // 
            this.columnHeader26.Text = "% Occupancy";
            this.columnHeader26.Width = 104;
            // 
            // columnHeader27
            // 
            this.columnHeader27.Text = "SqFt";
            this.columnHeader27.Width = 69;
            // 
            // columnHeader28
            // 
            this.columnHeader28.Text = "Low ($)";
            this.columnHeader28.Width = 56;
            // 
            // columnHeader29
            // 
            this.columnHeader29.Text = "High ($)";
            this.columnHeader29.Width = 54;
            // 
            // columnHeader30
            // 
            this.columnHeader30.Text = "$/SqFt";
            this.columnHeader30.Width = 56;
            // 
            // columnHeader31
            // 
            this.columnHeader31.Text = "Low ($)";
            this.columnHeader31.Width = 61;
            // 
            // columnHeader32
            // 
            this.columnHeader32.Text = "High ($)";
            this.columnHeader32.Width = 64;
            // 
            // columnHeader33
            // 
            this.columnHeader33.Text = "$/SqFt";
            this.columnHeader33.Width = 57;
            // 
            // columnHeader34
            // 
            this.columnHeader34.Text = "Comment";
            this.columnHeader34.Width = 105;
            // 
            // Income_SelfStorage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.Controls.Add(this._printIncomeReport);
            this.Controls.Add(this.label69);
            this.Controls.Add(this.label68);
            this.Controls.Add(this._I_AI_VacancyOrCreditLossPerc);
            this.Controls.Add(this._I_PI_VacancyOrCreditLossPerc);
            this.Controls.Add(this._I_AI_OperatingIncome);
            this.Controls.Add(this._I_AI_OtherIncome);
            this.Controls.Add(this._I_AI_VacancyOrCreditLoss);
            this.Controls.Add(this._I_AI_GrossRent);
            this.Controls.Add(this._I_PI_OperatingIncome);
            this.Controls.Add(this._I_PI_OtherIncome);
            this.Controls.Add(this._I_PI_VacancyOrCreditLoss);
            this.Controls.Add(this._I_PI_GrossRent);
            this.Controls.Add(this.label106);
            this.Controls.Add(this.label107);
            this.Controls.Add(this.label108);
            this.Controls.Add(this.label109);
            this.Controls.Add(this.label110);
            this.Controls.Add(this.label111);
            this.Controls.Add(this.label112);
            this.Controls.Add(this.label113);
            this.Controls.Add(this.label114);
            this.Controls.Add(this.label96);
            this.Controls.Add(this.label97);
            this.Controls.Add(this.label98);
            this.Controls.Add(this.label99);
            this.Controls.Add(this.label100);
            this.Controls.Add(this.label101);
            this.Controls.Add(this.label102);
            this.Controls.Add(this.label103);
            this.Controls.Add(this.label104);
            this.Controls.Add(this.label105);
            this.Controls.Add(this.label115);
            this.Controls.Add(this._I_S_MR_UsdPerSqFt);
            this.Controls.Add(this._I_S_MR_High);
            this.Controls.Add(this._I_S_MR_Low);
            this.Controls.Add(this._I_S_AR_UsdPerSqFt);
            this.Controls.Add(this._I_S_AR_High);
            this.Controls.Add(this._I_S_AR_Low);
            this.Controls.Add(this._I_S_TotalSqFt);
            this.Controls.Add(this._I_S_TotalOccupancy);
            this.Controls.Add(this._I_S_TotalUnits);
            this.Controls.Add(this.label66);
            this.Controls.Add(this._I_Summary);
            this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.Name = "Income_SelfStorage";
            this.Size = new System.Drawing.Size(897, 529);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private ListViewEx _I_Summary;
        private System.Windows.Forms.ColumnHeader columnHeader23;
        private System.Windows.Forms.ColumnHeader columnHeader24;
        private System.Windows.Forms.ColumnHeader columnHeader25;
        private System.Windows.Forms.ColumnHeader columnHeader26;
        private System.Windows.Forms.ColumnHeader columnHeader27;
        private System.Windows.Forms.ColumnHeader columnHeader28;
        private System.Windows.Forms.ColumnHeader columnHeader29;
        private System.Windows.Forms.ColumnHeader columnHeader30;
        private System.Windows.Forms.ColumnHeader columnHeader31;
        private System.Windows.Forms.ColumnHeader columnHeader32;
        private System.Windows.Forms.ColumnHeader columnHeader33;
        private System.Windows.Forms.ColumnHeader columnHeader34;
        private System.Windows.Forms.TextBox _I_S_MR_UsdPerSqFt;
        private System.Windows.Forms.TextBox _I_S_MR_High;
        private System.Windows.Forms.TextBox _I_S_MR_Low;
        private System.Windows.Forms.TextBox _I_S_AR_UsdPerSqFt;
        private System.Windows.Forms.TextBox _I_S_AR_High;
        private System.Windows.Forms.TextBox _I_S_AR_Low;
        private System.Windows.Forms.TextBox _I_S_TotalSqFt;
        private System.Windows.Forms.TextBox _I_S_TotalOccupancy;
        private System.Windows.Forms.TextBox _I_S_TotalUnits;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.TextBox _I_AI_VacancyOrCreditLossPerc;
        private System.Windows.Forms.TextBox _I_PI_VacancyOrCreditLossPerc;
        private System.Windows.Forms.TextBox _I_AI_OperatingIncome;
        private System.Windows.Forms.TextBox _I_AI_OtherIncome;
        private System.Windows.Forms.TextBox _I_AI_VacancyOrCreditLoss;
        private System.Windows.Forms.TextBox _I_AI_GrossRent;
        private System.Windows.Forms.TextBox _I_PI_OperatingIncome;
        private System.Windows.Forms.TextBox _I_PI_OtherIncome;
        private System.Windows.Forms.TextBox _I_PI_VacancyOrCreditLoss;
        private System.Windows.Forms.TextBox _I_PI_GrossRent;
        private System.Windows.Forms.Label label106;
        private System.Windows.Forms.Label label107;
        private System.Windows.Forms.Label label108;
        private System.Windows.Forms.Label label109;
        private System.Windows.Forms.Label label110;
        private System.Windows.Forms.Label label111;
        private System.Windows.Forms.Label label112;
        private System.Windows.Forms.Label label113;
        private System.Windows.Forms.Label label114;
        private System.Windows.Forms.Label label96;
        private System.Windows.Forms.Label label97;
        private System.Windows.Forms.Label label98;
        private System.Windows.Forms.Label label99;
        private System.Windows.Forms.Label label100;
        private System.Windows.Forms.Label label101;
        private System.Windows.Forms.Label label102;
        private System.Windows.Forms.Label label103;
        private System.Windows.Forms.Label label104;
        private System.Windows.Forms.Label label105;
        private System.Windows.Forms.Label label115;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.Button _printIncomeReport;
    }
}
