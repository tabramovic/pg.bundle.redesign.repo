using System;
using System.Text;
using System.Data;
using System.Data.SqlClient; 
using ProfitGrabber.Commercial;
using ProfitGrabber.Commercial.Objects;

namespace ProfitGrabber.Commercial.DALC
{
	/// <summary>
	/// Summary description for DataManager.
	/// </summary>
	public sealed class DataManager
	{
		static readonly DataManager _instance = new DataManager();

		static DataManager()
		{		
		}

		DataManager()
		{		
		}

		public static DataManager Instance
		{
			get { return _instance; }
		}

		bool _existingDBLayer = false;
		SqlConnection _connection;

		public void SetConnection(SqlConnection conn)
		{
			if (null == _connection)
			{
				_connection = conn;
				_existingDBLayer = TestIfExistsDBLayer();

				if (!_existingDBLayer)
					CreateDBLayer();
			}
		}

		public bool TestIfExistsDBLayer()
		{	
			bool retVal = false;
			SqlDataReader reader = null;			
			string cmdText = "SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'CommercialInfo'";

			try
			{
				SqlCommand testForLayer = new SqlCommand(cmdText, _connection);
				reader = testForLayer.ExecuteReader();
				while (reader.Read())
				{
					retVal = true;
					break;
				}				
			}
			catch 
			{
				retVal = false;
			}
			finally
			{
				reader.Close();
			}
			return retVal;
		}

		void CreateDBLayer()
		{
			CreateTable();
			CreateInsertSPL();
			CreateUpdateSPL();
		}

		bool CreateTable()
		{	
			bool retVal = false;
			string cmdText = "CREATE TABLE CommercialInfo (piId uniqueidentifier PRIMARY KEY, buildingInfo image, income image, expenses image, loans image, financialAnalisys image, multiyearAnalisys image)";

			try
			{
				SqlCommand createLayer = new SqlCommand(cmdText, _connection);
				createLayer.ExecuteNonQuery();
				retVal = true;
			}
			catch 
			{
				retVal = false;
			}
			return retVal;		
		}

		void CreateInsertSPL()
		{			
			try
			{
				StringBuilder sb = new StringBuilder();
				sb.Append("CREATE PROCEDURE InsertCommercials ");
				sb.Append("( ");
				sb.Append("@propertyItemId uniqueidentifier, ");
				sb.Append("@buildingInfoStream image, ");
				sb.Append("@incomeStream image, ");
				sb.Append("@expensesStream image, ");
				sb.Append("@loansStream image, ");
				sb.Append("@financialAnalisysStream image, ");
                sb.Append("@multiYearAnalisysStream image ");
				sb.Append(") ");
				sb.Append("AS ");
                sb.Append("INSERT INTO CommercialInfo (piId , buildingInfo, income, expenses, loans, financialanalisys, multiyearAnalisys) ");
                sb.Append("VALUES ( @propertyItemId , @buildingInfoStream , @incomeStream , @expensesStream , @loansStream , @financialAnalisysStream, @multiYearAnalisysStream) ");
					
				string splText = sb.ToString();
				SqlCommand createSPL = new SqlCommand(splText, _connection);
				createSPL.ExecuteNonQuery();
				
			}
			catch (Exception exc)
			{
				System.Windows.Forms.MessageBox.Show("Can't create INSERT procedure for COMMERCIALS module" + System.Environment.NewLine + exc.Message);
			}			
		}

		void CreateUpdateSPL()
		{			
			try
			{				
				StringBuilder sb = new StringBuilder();
				sb.Append("CREATE PROCEDURE UpdateCommercials ");
				sb.Append("( ");
				sb.Append("@propertyItemId uniqueidentifier, ");
				sb.Append("@buildingInfoStream image, ");
				sb.Append("@incomeStream image, ");
				sb.Append("@expensesStream image, ");
				sb.Append("@loansStream image, ");
				sb.Append("@financialAnalisysStream image, ");
                sb.Append("@multiYearAnalisysStream image ");
				sb.Append(") ");
				sb.Append("AS ");
                sb.Append("UPDATE CommercialInfo SET buildingInfo=@buildingInfoStream, income=@incomeStream, expenses=@expensesStream , loans=@loansStream, financialAnalisys=@financialAnalisysStream, multiyearAnalisys=@multiYearAnalisysStream where piId=@propertyItemId; ");				
					
				string splText = sb.ToString();
				SqlCommand createSPL = new SqlCommand(splText, _connection);
				createSPL.ExecuteNonQuery();
				
			}
			catch (Exception exc)
			{
				System.Windows.Forms.MessageBox.Show("Can't create UPDATE procedure for COMMERCIALS module" + System.Environment.NewLine + exc.Message);
			}			
		}


		public void GetData(Guid propertyItem)
		{
            string cmdText = "SELECT buildingInfo, income, expenses, loans, financialAnalisys, multiyearAnalisys FROM CommercialInfo WHERE piId='" + propertyItem.ToString() + "'";
			SqlDataReader reader = null;	
			byte[] buildingInfo = null;
			byte[] income = null;
			byte[] expenses = null;
			byte[] loans = null;
			byte[] financialAnalisys = null;
            byte[] multiyearAnalisys = null;

			try
			{
				SqlCommand getData = new SqlCommand(cmdText, _connection);
				reader = getData.ExecuteReader();

				if (reader.HasRows)
				{
					while (reader.Read())
					{
						buildingInfo = !reader.IsDBNull(0) ? (byte[])reader.GetValue(0) : null;
						income = !reader.IsDBNull(1) ? (byte[])reader.GetValue(1) : null;
						expenses = !reader.IsDBNull(2) ? (byte[])reader.GetValue(2) : null;
						loans = !reader.IsDBNull(3) ? (byte[])reader.GetValue(3) : null;
						financialAnalisys = !reader.IsDBNull(4) ? (byte[])reader.GetValue(4) : null;
                        multiyearAnalisys = !reader.IsDBNull(5) ? (byte[])reader.GetValue(5) : null;

						BuildingInfoObject.Instance.DeserializeFromByteArray(buildingInfo);
						IncomeObject.Instance.DeserializeFromByteArray(income);
						ExpensesObject.Instance.DeserializeFromByteArray(expenses);
						LoansObject.Instance.DeserializeFromByteArray(loans);
						FinancialInfoObject.Instance.DeserializeFromByteArray(financialAnalisys);
                        MultiyearAnalysisObject.Instance.DeserializeFromByteArray(multiyearAnalisys);

					}				
				}
				else
				{
					BuildingInfoObject.Instance.ResetObject();
					IncomeObject.Instance.ResetObject();
					ExpensesObject.Instance.ResetObject();
					LoansObject.Instance.ResetObject();
					FinancialInfoObject.Instance.ResetObject();
                    MultiyearAnalysisObject.Instance.ResetObject();
				}
			}
			catch
			{
				buildingInfo = null;
				income = null;
				expenses = null;
				loans = null;
				financialAnalisys = null;
			}
			finally
			{
                if (null != reader)
				    reader.Close();
			}
		}

		public bool SaveData(Guid propertyItem)
		{
			if (!TestForExistingRecord(propertyItem))
				return InsertData(propertyItem);
			else
				return UpdateData(propertyItem);					
		}

		bool TestForExistingRecord(Guid propertyItem)
		{
			bool retVal = false;

			SqlDataReader reader = null;			
			string cmdText = "SELECT piId FROM CommercialInfo where piId='" + propertyItem.ToString() + "'";

			try
			{
                if (null == _connection)
                    return retVal;

                SqlCommand testForExistingRecord = new SqlCommand(cmdText, _connection);
				reader = testForExistingRecord.ExecuteReader();
				while (reader.Read())
				{
					retVal = true;
					break;
				}				
			}
			catch 
			{
				retVal = false;
			}
			finally
			{
				if (null != reader)
                    reader.Close();
			}
			return retVal;
		}

		bool InsertData(Guid propertyItem)
		{
			bool retVal = false;
			
			try
			{
                if (null == _connection)
                    return retVal;

				SqlCommand sp_insert_comps = new SqlCommand("InsertCommercials", _connection);
				sp_insert_comps.CommandType = CommandType.StoredProcedure;
	
				sp_insert_comps.Parameters.Add("@propertyItemId", propertyItem);	
				sp_insert_comps.Parameters.Add("@buildingInfoStream", BuildingInfoObject.Instance.SerializeToByteArray() );	
				sp_insert_comps.Parameters.Add("@incomeStream", IncomeObject.Instance.SerializeToByteArray() );	
				sp_insert_comps.Parameters.Add("@expensesStream", ExpensesObject.Instance.SerializeToByteArray() );
				sp_insert_comps.Parameters.Add("@loansStream", LoansObject.Instance.SerializeToByteArray());
				sp_insert_comps.Parameters.Add("@financialAnalisysStream", FinancialInfoObject.Instance.SerializeToByteArray() );
                sp_insert_comps.Parameters.Add("@multiYearAnalisysStream", MultiyearAnalysisObject.Instance.SerializeToByteArray());
				sp_insert_comps.ExecuteNonQuery();

				retVal = true;
			}
			catch 
			{
				retVal = false;
			}
			return retVal;
		}

		bool UpdateData(Guid propertyItem)
		{
			bool retVal = false;			

			try
			{							
				SqlCommand sp_insert_comps = new SqlCommand("UpdateCommercials", _connection);
				sp_insert_comps.CommandType = CommandType.StoredProcedure;
	
				sp_insert_comps.Parameters.Add("@propertyItemId", propertyItem);	
				sp_insert_comps.Parameters.Add("@buildingInfoStream", BuildingInfoObject.Instance.SerializeToByteArray() );	
				sp_insert_comps.Parameters.Add("@incomeStream", IncomeObject.Instance.SerializeToByteArray() );	
				sp_insert_comps.Parameters.Add("@expensesStream", ExpensesObject.Instance.SerializeToByteArray() );
				sp_insert_comps.Parameters.Add("@loansStream", LoansObject.Instance.SerializeToByteArray());
				sp_insert_comps.Parameters.Add("@financialAnalisysStream", FinancialInfoObject.Instance.SerializeToByteArray() );
                sp_insert_comps.Parameters.Add("@multiYearAnalisysStream", MultiyearAnalysisObject.Instance.SerializeToByteArray());
				sp_insert_comps.ExecuteNonQuery();

				retVal = true;
			}
			catch 
			{
				retVal = false;
			}
			return retVal;
		}

		bool DeleteData(Guid propertyItem)
		{
			bool retVal = false;
			string cmdText = "DELETE FROM CommercialInfo WHERE piId = '" + propertyItem.ToString() + "'";
			
			try
			{
				SqlCommand deleteData = new SqlCommand(cmdText, _connection);
				deleteData.ExecuteNonQuery();
				retVal = true;
			}
			catch 
			{
				retVal = false;
			}
			return retVal;
		}
	}
}
