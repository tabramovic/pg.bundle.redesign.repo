﻿namespace ProfitGrabber.Commercial
{
    partial class Income_RetailOrOffice
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label68 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this._I_S_Total_PR_UsdPerSqftPerYear = new System.Windows.Forms.TextBox();
            this._I_S_Total_PR_UsdPerSqFtPerMonth = new System.Windows.Forms.TextBox();
            this._I_S_Total_PR_UsdPerYear = new System.Windows.Forms.TextBox();
            this._I_UTS_Total_AR_UsdPerSqFt = new System.Windows.Forms.TextBox();
            this._I_S_Total_PR_UsdPerMonth = new System.Windows.Forms.TextBox();
            this._I_S_Total_AR_UsdPerSqFtPerMonth = new System.Windows.Forms.TextBox();
            this._I_S_Total_AR_UsdPerYear = new System.Windows.Forms.TextBox();
            this._I_S_Total_AR_UsdPerMonth = new System.Windows.Forms.TextBox();
            this._I_UTS_TotalSqFt = new System.Windows.Forms.TextBox();
            this._I_UTS_TotalOccupied = new System.Windows.Forms.TextBox();
            this.label66 = new System.Windows.Forms.Label();
            this._I_AI_VacancyOrCreditLossPerc = new System.Windows.Forms.TextBox();
            this._I_AI_OperatingIncome = new System.Windows.Forms.TextBox();
            this._I_AI_OtherIncome = new System.Windows.Forms.TextBox();
            this._I_AI_VacancyOrCreditLoss = new System.Windows.Forms.TextBox();
            this._I_AI_GrossRent = new System.Windows.Forms.TextBox();
            this.label96 = new System.Windows.Forms.Label();
            this.label97 = new System.Windows.Forms.Label();
            this.label98 = new System.Windows.Forms.Label();
            this.label99 = new System.Windows.Forms.Label();
            this.label100 = new System.Windows.Forms.Label();
            this.label101 = new System.Windows.Forms.Label();
            this.label102 = new System.Windows.Forms.Label();
            this.label103 = new System.Windows.Forms.Label();
            this.label104 = new System.Windows.Forms.Label();
            this.label105 = new System.Windows.Forms.Label();
            this._I_PI_VacancyOrCreditLossPerc = new System.Windows.Forms.TextBox();
            this._I_PI_OperatingIncome = new System.Windows.Forms.TextBox();
            this._I_PI_OtherIncome = new System.Windows.Forms.TextBox();
            this._I_PI_VacancyOrCreditLoss = new System.Windows.Forms.TextBox();
            this._I_PI_GrossRent = new System.Windows.Forms.TextBox();
            this.label106 = new System.Windows.Forms.Label();
            this.label107 = new System.Windows.Forms.Label();
            this.label108 = new System.Windows.Forms.Label();
            this.label109 = new System.Windows.Forms.Label();
            this.label110 = new System.Windows.Forms.Label();
            this.label111 = new System.Windows.Forms.Label();
            this.label112 = new System.Windows.Forms.Label();
            this.label113 = new System.Windows.Forms.Label();
            this.label114 = new System.Windows.Forms.Label();
            this.label115 = new System.Windows.Forms.Label();
            this._spaceGroupBox = new System.Windows.Forms.GroupBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this._I_S_F_CAMCharges = new System.Windows.Forms.TextBox();
            this._I_S_F_NNNCharges = new System.Windows.Forms.TextBox();
            this._I_S_F_ExpensesStop = new System.Windows.Forms.TextBox();
            this._I_S_F_TIAllowance = new System.Windows.Forms.TextBox();
            this._I_S_L_RenewalOption = new System.Windows.Forms.ComboBox();
            this._I_S_L_EndDate = new System.Windows.Forms.DateTimePicker();
            this._I_S_L_StartDate = new System.Windows.Forms.DateTimePicker();
            this._I_S_L_Length = new System.Windows.Forms.TextBox();
            this._I_S_L_Type = new System.Windows.Forms.ComboBox();
            this._I_S_F_TennantComment = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this._I_S_L_LeaseComments = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this._I_S_Comments = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this._I_S_Space = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this._printIncomeReport = new System.Windows.Forms.Button();
            this._I_Summary = new ProfitGrabber.Commercial.ListViewEx();
            this.columnHeader23 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader24 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader25 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader26 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader27 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader28 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader29 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader30 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader31 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader32 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader33 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader34 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader35 = new System.Windows.Forms.ColumnHeader();
            this._spaceGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // label68
            // 
            this.label68.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label68.Location = new System.Drawing.Point(328, 11);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(88, 23);
            this.label68.TabIndex = 290;
            this.label68.Text = "Actual Rate:";
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(595, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(120, 23);
            this.label1.TabIndex = 291;
            this.label1.Text = "Proforma Rate:";
            // 
            // _I_S_Total_PR_UsdPerSqftPerYear
            // 
            this._I_S_Total_PR_UsdPerSqftPerYear.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._I_S_Total_PR_UsdPerSqftPerYear.Location = new System.Drawing.Point(788, 206);
            this._I_S_Total_PR_UsdPerSqftPerYear.Name = "_I_S_Total_PR_UsdPerSqftPerYear";
            this._I_S_Total_PR_UsdPerSqftPerYear.ReadOnly = true;
            this._I_S_Total_PR_UsdPerSqftPerYear.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this._I_S_Total_PR_UsdPerSqftPerYear.Size = new System.Drawing.Size(77, 20);
            this._I_S_Total_PR_UsdPerSqftPerYear.TabIndex = 301;
            this._I_S_Total_PR_UsdPerSqftPerYear.TabStop = false;
            // 
            // _I_S_Total_PR_UsdPerSqFtPerMonth
            // 
            this._I_S_Total_PR_UsdPerSqFtPerMonth.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._I_S_Total_PR_UsdPerSqFtPerMonth.Location = new System.Drawing.Point(705, 206);
            this._I_S_Total_PR_UsdPerSqFtPerMonth.Name = "_I_S_Total_PR_UsdPerSqFtPerMonth";
            this._I_S_Total_PR_UsdPerSqFtPerMonth.ReadOnly = true;
            this._I_S_Total_PR_UsdPerSqFtPerMonth.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this._I_S_Total_PR_UsdPerSqFtPerMonth.Size = new System.Drawing.Size(84, 20);
            this._I_S_Total_PR_UsdPerSqFtPerMonth.TabIndex = 300;
            this._I_S_Total_PR_UsdPerSqFtPerMonth.TabStop = false;
            // 
            // _I_S_Total_PR_UsdPerYear
            // 
            this._I_S_Total_PR_UsdPerYear.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._I_S_Total_PR_UsdPerYear.Location = new System.Drawing.Point(650, 206);
            this._I_S_Total_PR_UsdPerYear.Name = "_I_S_Total_PR_UsdPerYear";
            this._I_S_Total_PR_UsdPerYear.ReadOnly = true;
            this._I_S_Total_PR_UsdPerYear.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this._I_S_Total_PR_UsdPerYear.Size = new System.Drawing.Size(56, 20);
            this._I_S_Total_PR_UsdPerYear.TabIndex = 299;
            this._I_S_Total_PR_UsdPerYear.TabStop = false;
            // 
            // _I_UTS_Total_AR_UsdPerSqFt
            // 
            this._I_UTS_Total_AR_UsdPerSqFt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._I_UTS_Total_AR_UsdPerSqFt.Location = new System.Drawing.Point(594, 206);
            this._I_UTS_Total_AR_UsdPerSqFt.Name = "_I_UTS_Total_AR_UsdPerSqFt";
            this._I_UTS_Total_AR_UsdPerSqFt.ReadOnly = true;
            this._I_UTS_Total_AR_UsdPerSqFt.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this._I_UTS_Total_AR_UsdPerSqFt.Size = new System.Drawing.Size(57, 20);
            this._I_UTS_Total_AR_UsdPerSqFt.TabIndex = 298;
            this._I_UTS_Total_AR_UsdPerSqFt.TabStop = false;
            // 
            // _I_S_Total_PR_UsdPerMonth
            // 
            this._I_S_Total_PR_UsdPerMonth.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._I_S_Total_PR_UsdPerMonth.Location = new System.Drawing.Point(520, 206);
            this._I_S_Total_PR_UsdPerMonth.Name = "_I_S_Total_PR_UsdPerMonth";
            this._I_S_Total_PR_UsdPerMonth.ReadOnly = true;
            this._I_S_Total_PR_UsdPerMonth.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this._I_S_Total_PR_UsdPerMonth.Size = new System.Drawing.Size(75, 20);
            this._I_S_Total_PR_UsdPerMonth.TabIndex = 297;
            this._I_S_Total_PR_UsdPerMonth.TabStop = false;
            // 
            // _I_S_Total_AR_UsdPerSqFtPerMonth
            // 
            this._I_S_Total_AR_UsdPerSqFtPerMonth.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._I_S_Total_AR_UsdPerSqFtPerMonth.Location = new System.Drawing.Point(436, 206);
            this._I_S_Total_AR_UsdPerSqFtPerMonth.Name = "_I_S_Total_AR_UsdPerSqFtPerMonth";
            this._I_S_Total_AR_UsdPerSqFtPerMonth.ReadOnly = true;
            this._I_S_Total_AR_UsdPerSqFtPerMonth.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this._I_S_Total_AR_UsdPerSqFtPerMonth.Size = new System.Drawing.Size(85, 20);
            this._I_S_Total_AR_UsdPerSqFtPerMonth.TabIndex = 296;
            this._I_S_Total_AR_UsdPerSqFtPerMonth.TabStop = false;
            // 
            // _I_S_Total_AR_UsdPerYear
            // 
            this._I_S_Total_AR_UsdPerYear.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._I_S_Total_AR_UsdPerYear.Location = new System.Drawing.Point(383, 206);
            this._I_S_Total_AR_UsdPerYear.Name = "_I_S_Total_AR_UsdPerYear";
            this._I_S_Total_AR_UsdPerYear.ReadOnly = true;
            this._I_S_Total_AR_UsdPerYear.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this._I_S_Total_AR_UsdPerYear.Size = new System.Drawing.Size(54, 20);
            this._I_S_Total_AR_UsdPerYear.TabIndex = 295;
            this._I_S_Total_AR_UsdPerYear.TabStop = false;
            // 
            // _I_S_Total_AR_UsdPerMonth
            // 
            this._I_S_Total_AR_UsdPerMonth.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._I_S_Total_AR_UsdPerMonth.Location = new System.Drawing.Point(326, 206);
            this._I_S_Total_AR_UsdPerMonth.Name = "_I_S_Total_AR_UsdPerMonth";
            this._I_S_Total_AR_UsdPerMonth.ReadOnly = true;
            this._I_S_Total_AR_UsdPerMonth.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this._I_S_Total_AR_UsdPerMonth.Size = new System.Drawing.Size(58, 20);
            this._I_S_Total_AR_UsdPerMonth.TabIndex = 294;
            this._I_S_Total_AR_UsdPerMonth.TabStop = false;
            // 
            // _I_UTS_TotalSqFt
            // 
            this._I_UTS_TotalSqFt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._I_UTS_TotalSqFt.Location = new System.Drawing.Point(256, 206);
            this._I_UTS_TotalSqFt.Name = "_I_UTS_TotalSqFt";
            this._I_UTS_TotalSqFt.ReadOnly = true;
            this._I_UTS_TotalSqFt.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this._I_UTS_TotalSqFt.Size = new System.Drawing.Size(71, 20);
            this._I_UTS_TotalSqFt.TabIndex = 293;
            this._I_UTS_TotalSqFt.TabStop = false;
            // 
            // _I_UTS_TotalOccupied
            // 
            this._I_UTS_TotalOccupied.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._I_UTS_TotalOccupied.Location = new System.Drawing.Point(77, 206);
            this._I_UTS_TotalOccupied.Name = "_I_UTS_TotalOccupied";
            this._I_UTS_TotalOccupied.ReadOnly = true;
            this._I_UTS_TotalOccupied.Size = new System.Drawing.Size(60, 20);
            this._I_UTS_TotalOccupied.TabIndex = 292;
            // 
            // label66
            // 
            this.label66.Location = new System.Drawing.Point(3, 208);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(38, 23);
            this.label66.TabIndex = 302;
            this.label66.Text = "Total:";
            this.label66.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // _I_AI_VacancyOrCreditLossPerc
            // 
            this._I_AI_VacancyOrCreditLossPerc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._I_AI_VacancyOrCreditLossPerc.Location = new System.Drawing.Point(809, 286);
            this._I_AI_VacancyOrCreditLossPerc.Name = "_I_AI_VacancyOrCreditLossPerc";
            this._I_AI_VacancyOrCreditLossPerc.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this._I_AI_VacancyOrCreditLossPerc.Size = new System.Drawing.Size(56, 20);
            this._I_AI_VacancyOrCreditLossPerc.TabIndex = 323;
            // 
            // _I_AI_OperatingIncome
            // 
            this._I_AI_OperatingIncome.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._I_AI_OperatingIncome.Location = new System.Drawing.Point(745, 326);
            this._I_AI_OperatingIncome.Name = "_I_AI_OperatingIncome";
            this._I_AI_OperatingIncome.ReadOnly = true;
            this._I_AI_OperatingIncome.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this._I_AI_OperatingIncome.Size = new System.Drawing.Size(65, 20);
            this._I_AI_OperatingIncome.TabIndex = 325;
            this._I_AI_OperatingIncome.TabStop = false;
            // 
            // _I_AI_OtherIncome
            // 
            this._I_AI_OtherIncome.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._I_AI_OtherIncome.Location = new System.Drawing.Point(745, 306);
            this._I_AI_OtherIncome.Name = "_I_AI_OtherIncome";
            this._I_AI_OtherIncome.ReadOnly = true;
            this._I_AI_OtherIncome.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this._I_AI_OtherIncome.Size = new System.Drawing.Size(65, 20);
            this._I_AI_OtherIncome.TabIndex = 324;
            this._I_AI_OtherIncome.TabStop = false;
            // 
            // _I_AI_VacancyOrCreditLoss
            // 
            this._I_AI_VacancyOrCreditLoss.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._I_AI_VacancyOrCreditLoss.Location = new System.Drawing.Point(745, 286);
            this._I_AI_VacancyOrCreditLoss.Name = "_I_AI_VacancyOrCreditLoss";
            this._I_AI_VacancyOrCreditLoss.ReadOnly = true;
            this._I_AI_VacancyOrCreditLoss.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this._I_AI_VacancyOrCreditLoss.Size = new System.Drawing.Size(65, 20);
            this._I_AI_VacancyOrCreditLoss.TabIndex = 322;
            this._I_AI_VacancyOrCreditLoss.TabStop = false;
            // 
            // _I_AI_GrossRent
            // 
            this._I_AI_GrossRent.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._I_AI_GrossRent.Location = new System.Drawing.Point(745, 266);
            this._I_AI_GrossRent.Name = "_I_AI_GrossRent";
            this._I_AI_GrossRent.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this._I_AI_GrossRent.Size = new System.Drawing.Size(65, 20);
            this._I_AI_GrossRent.TabIndex = 321;
            // 
            // label96
            // 
            this.label96.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label96.Location = new System.Drawing.Point(735, 330);
            this.label96.Name = "label96";
            this.label96.Size = new System.Drawing.Size(16, 23);
            this.label96.TabIndex = 333;
            this.label96.Text = "$";
            // 
            // label97
            // 
            this.label97.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label97.Location = new System.Drawing.Point(735, 309);
            this.label97.Name = "label97";
            this.label97.Size = new System.Drawing.Size(16, 23);
            this.label97.TabIndex = 332;
            this.label97.Text = "$";
            // 
            // label98
            // 
            this.label98.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label98.Location = new System.Drawing.Point(735, 288);
            this.label98.Name = "label98";
            this.label98.Size = new System.Drawing.Size(16, 23);
            this.label98.TabIndex = 331;
            this.label98.Text = "$";
            // 
            // label99
            // 
            this.label99.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label99.Location = new System.Drawing.Point(735, 269);
            this.label99.Name = "label99";
            this.label99.Size = new System.Drawing.Size(16, 23);
            this.label99.TabIndex = 330;
            this.label99.Text = "$";
            // 
            // label100
            // 
            this.label100.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label100.Location = new System.Drawing.Point(573, 330);
            this.label100.Name = "label100";
            this.label100.Size = new System.Drawing.Size(162, 23);
            this.label100.TabIndex = 329;
            this.label100.Text = "Gross Operating Income (GOI):";
            // 
            // label101
            // 
            this.label101.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label101.Location = new System.Drawing.Point(659, 309);
            this.label101.Name = "label101";
            this.label101.Size = new System.Drawing.Size(76, 23);
            this.label101.TabIndex = 328;
            this.label101.Text = "Other Income:";
            // 
            // label102
            // 
            this.label102.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label102.Location = new System.Drawing.Point(623, 288);
            this.label102.Name = "label102";
            this.label102.Size = new System.Drawing.Size(112, 23);
            this.label102.TabIndex = 327;
            this.label102.Text = "Vacancy/Credit Loss:";
            // 
            // label103
            // 
            this.label103.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label103.Location = new System.Drawing.Point(639, 269);
            this.label103.Name = "label103";
            this.label103.Size = new System.Drawing.Size(96, 23);
            this.label103.TabIndex = 326;
            this.label103.Text = "Gross Rent (GSI):";
            // 
            // label104
            // 
            this.label104.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label104.Location = new System.Drawing.Point(641, 242);
            this.label104.Name = "label104";
            this.label104.Size = new System.Drawing.Size(168, 23);
            this.label104.TabIndex = 334;
            this.label104.Text = "ACTUAL INCOME (annual):";
            // 
            // label105
            // 
            this.label105.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label105.Location = new System.Drawing.Point(865, 290);
            this.label105.Name = "label105";
            this.label105.Size = new System.Drawing.Size(16, 23);
            this.label105.TabIndex = 335;
            this.label105.Text = "%";
            // 
            // _I_PI_VacancyOrCreditLossPerc
            // 
            this._I_PI_VacancyOrCreditLossPerc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._I_PI_VacancyOrCreditLossPerc.Location = new System.Drawing.Point(809, 409);
            this._I_PI_VacancyOrCreditLossPerc.Name = "_I_PI_VacancyOrCreditLossPerc";
            this._I_PI_VacancyOrCreditLossPerc.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this._I_PI_VacancyOrCreditLossPerc.Size = new System.Drawing.Size(54, 20);
            this._I_PI_VacancyOrCreditLossPerc.TabIndex = 338;
            // 
            // _I_PI_OperatingIncome
            // 
            this._I_PI_OperatingIncome.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._I_PI_OperatingIncome.Location = new System.Drawing.Point(745, 449);
            this._I_PI_OperatingIncome.Name = "_I_PI_OperatingIncome";
            this._I_PI_OperatingIncome.ReadOnly = true;
            this._I_PI_OperatingIncome.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this._I_PI_OperatingIncome.Size = new System.Drawing.Size(65, 20);
            this._I_PI_OperatingIncome.TabIndex = 340;
            this._I_PI_OperatingIncome.TabStop = false;
            // 
            // _I_PI_OtherIncome
            // 
            this._I_PI_OtherIncome.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._I_PI_OtherIncome.Location = new System.Drawing.Point(745, 429);
            this._I_PI_OtherIncome.Name = "_I_PI_OtherIncome";
            this._I_PI_OtherIncome.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this._I_PI_OtherIncome.Size = new System.Drawing.Size(65, 20);
            this._I_PI_OtherIncome.TabIndex = 339;
            // 
            // _I_PI_VacancyOrCreditLoss
            // 
            this._I_PI_VacancyOrCreditLoss.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._I_PI_VacancyOrCreditLoss.Location = new System.Drawing.Point(745, 409);
            this._I_PI_VacancyOrCreditLoss.Name = "_I_PI_VacancyOrCreditLoss";
            this._I_PI_VacancyOrCreditLoss.ReadOnly = true;
            this._I_PI_VacancyOrCreditLoss.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this._I_PI_VacancyOrCreditLoss.Size = new System.Drawing.Size(65, 20);
            this._I_PI_VacancyOrCreditLoss.TabIndex = 337;
            this._I_PI_VacancyOrCreditLoss.TabStop = false;
            // 
            // _I_PI_GrossRent
            // 
            this._I_PI_GrossRent.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._I_PI_GrossRent.Location = new System.Drawing.Point(745, 389);
            this._I_PI_GrossRent.Name = "_I_PI_GrossRent";
            this._I_PI_GrossRent.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this._I_PI_GrossRent.Size = new System.Drawing.Size(65, 20);
            this._I_PI_GrossRent.TabIndex = 336;
            // 
            // label106
            // 
            this.label106.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label106.Location = new System.Drawing.Point(735, 453);
            this.label106.Name = "label106";
            this.label106.Size = new System.Drawing.Size(16, 23);
            this.label106.TabIndex = 348;
            this.label106.Text = "$";
            // 
            // label107
            // 
            this.label107.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label107.Location = new System.Drawing.Point(735, 432);
            this.label107.Name = "label107";
            this.label107.Size = new System.Drawing.Size(16, 23);
            this.label107.TabIndex = 347;
            this.label107.Text = "$";
            // 
            // label108
            // 
            this.label108.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label108.Location = new System.Drawing.Point(735, 411);
            this.label108.Name = "label108";
            this.label108.Size = new System.Drawing.Size(16, 23);
            this.label108.TabIndex = 346;
            this.label108.Text = "$";
            // 
            // label109
            // 
            this.label109.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label109.Location = new System.Drawing.Point(735, 392);
            this.label109.Name = "label109";
            this.label109.Size = new System.Drawing.Size(16, 23);
            this.label109.TabIndex = 345;
            this.label109.Text = "$";
            // 
            // label110
            // 
            this.label110.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label110.Location = new System.Drawing.Point(573, 453);
            this.label110.Name = "label110";
            this.label110.Size = new System.Drawing.Size(162, 23);
            this.label110.TabIndex = 344;
            this.label110.Text = "Gross Operating Income (GOI):";
            // 
            // label111
            // 
            this.label111.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label111.Location = new System.Drawing.Point(659, 432);
            this.label111.Name = "label111";
            this.label111.Size = new System.Drawing.Size(76, 23);
            this.label111.TabIndex = 343;
            this.label111.Text = "Other Income:";
            // 
            // label112
            // 
            this.label112.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label112.Location = new System.Drawing.Point(623, 411);
            this.label112.Name = "label112";
            this.label112.Size = new System.Drawing.Size(112, 23);
            this.label112.TabIndex = 342;
            this.label112.Text = "Vacancy/Credit Loss:";
            // 
            // label113
            // 
            this.label113.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label113.Location = new System.Drawing.Point(639, 392);
            this.label113.Name = "label113";
            this.label113.Size = new System.Drawing.Size(96, 23);
            this.label113.TabIndex = 341;
            this.label113.Text = "Gross Rent (GSI):";
            // 
            // label114
            // 
            this.label114.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label114.Location = new System.Drawing.Point(641, 365);
            this.label114.Name = "label114";
            this.label114.Size = new System.Drawing.Size(200, 23);
            this.label114.TabIndex = 349;
            this.label114.Text = "PROFORMA INCOME (annual):";
            // 
            // label115
            // 
            this.label115.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label115.Location = new System.Drawing.Point(863, 413);
            this.label115.Name = "label115";
            this.label115.Size = new System.Drawing.Size(16, 23);
            this.label115.TabIndex = 350;
            this.label115.Text = "%";
            // 
            // _spaceGroupBox
            // 
            this._spaceGroupBox.Controls.Add(this.label20);
            this._spaceGroupBox.Controls.Add(this.label19);
            this._spaceGroupBox.Controls.Add(this.label18);
            this._spaceGroupBox.Controls.Add(this.label17);
            this._spaceGroupBox.Controls.Add(this._I_S_F_CAMCharges);
            this._spaceGroupBox.Controls.Add(this._I_S_F_NNNCharges);
            this._spaceGroupBox.Controls.Add(this._I_S_F_ExpensesStop);
            this._spaceGroupBox.Controls.Add(this._I_S_F_TIAllowance);
            this._spaceGroupBox.Controls.Add(this._I_S_L_RenewalOption);
            this._spaceGroupBox.Controls.Add(this._I_S_L_EndDate);
            this._spaceGroupBox.Controls.Add(this._I_S_L_StartDate);
            this._spaceGroupBox.Controls.Add(this._I_S_L_Length);
            this._spaceGroupBox.Controls.Add(this._I_S_L_Type);
            this._spaceGroupBox.Controls.Add(this._I_S_F_TennantComment);
            this._spaceGroupBox.Controls.Add(this.label16);
            this._spaceGroupBox.Controls.Add(this._I_S_L_LeaseComments);
            this._spaceGroupBox.Controls.Add(this.label15);
            this._spaceGroupBox.Controls.Add(this.label11);
            this._spaceGroupBox.Controls.Add(this.label12);
            this._spaceGroupBox.Controls.Add(this.label13);
            this._spaceGroupBox.Controls.Add(this.label14);
            this._spaceGroupBox.Controls.Add(this.label10);
            this._spaceGroupBox.Controls.Add(this.label9);
            this._spaceGroupBox.Controls.Add(this.label8);
            this._spaceGroupBox.Controls.Add(this.label7);
            this._spaceGroupBox.Controls.Add(this.label6);
            this._spaceGroupBox.Controls.Add(this.label5);
            this._spaceGroupBox.Controls.Add(this.label4);
            this._spaceGroupBox.Controls.Add(this._I_S_Comments);
            this._spaceGroupBox.Controls.Add(this.label3);
            this._spaceGroupBox.Controls.Add(this._I_S_Space);
            this._spaceGroupBox.Controls.Add(this.label2);
            this._spaceGroupBox.Location = new System.Drawing.Point(32, 232);
            this._spaceGroupBox.Name = "_spaceGroupBox";
            this._spaceGroupBox.Size = new System.Drawing.Size(494, 294);
            this._spaceGroupBox.TabIndex = 351;
            this._spaceGroupBox.TabStop = false;
            this._spaceGroupBox.Text = "Space";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(348, 117);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(13, 13);
            this.label20.TabIndex = 382;
            this.label20.Text = "$";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(348, 159);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(13, 13);
            this.label19.TabIndex = 381;
            this.label19.Text = "$";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(348, 138);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(13, 13);
            this.label18.TabIndex = 380;
            this.label18.Text = "$";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(348, 180);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(13, 13);
            this.label17.TabIndex = 379;
            this.label17.Text = "$";
            // 
            // _I_S_F_CAMCharges
            // 
            this._I_S_F_CAMCharges.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._I_S_F_CAMCharges.Location = new System.Drawing.Point(362, 178);
            this._I_S_F_CAMCharges.Name = "_I_S_F_CAMCharges";
            this._I_S_F_CAMCharges.Size = new System.Drawing.Size(108, 20);
            this._I_S_F_CAMCharges.TabIndex = 378;
            // 
            // _I_S_F_NNNCharges
            // 
            this._I_S_F_NNNCharges.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._I_S_F_NNNCharges.Location = new System.Drawing.Point(362, 157);
            this._I_S_F_NNNCharges.Name = "_I_S_F_NNNCharges";
            this._I_S_F_NNNCharges.Size = new System.Drawing.Size(108, 20);
            this._I_S_F_NNNCharges.TabIndex = 377;
            // 
            // _I_S_F_ExpensesStop
            // 
            this._I_S_F_ExpensesStop.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._I_S_F_ExpensesStop.Location = new System.Drawing.Point(362, 136);
            this._I_S_F_ExpensesStop.Name = "_I_S_F_ExpensesStop";
            this._I_S_F_ExpensesStop.Size = new System.Drawing.Size(108, 20);
            this._I_S_F_ExpensesStop.TabIndex = 376;
            // 
            // _I_S_F_TIAllowance
            // 
            this._I_S_F_TIAllowance.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._I_S_F_TIAllowance.Location = new System.Drawing.Point(362, 115);
            this._I_S_F_TIAllowance.Name = "_I_S_F_TIAllowance";
            this._I_S_F_TIAllowance.Size = new System.Drawing.Size(108, 20);
            this._I_S_F_TIAllowance.TabIndex = 375;
            // 
            // _I_S_L_RenewalOption
            // 
            this._I_S_L_RenewalOption.FormattingEnabled = true;
            this._I_S_L_RenewalOption.Items.AddRange(new object[] {
            "Yes",
            "No"});
            this._I_S_L_RenewalOption.Location = new System.Drawing.Point(102, 200);
            this._I_S_L_RenewalOption.Name = "_I_S_L_RenewalOption";
            this._I_S_L_RenewalOption.Size = new System.Drawing.Size(55, 21);
            this._I_S_L_RenewalOption.TabIndex = 374;
            // 
            // _I_S_L_EndDate
            // 
            this._I_S_L_EndDate.CustomFormat = "dd MMM yyyy";
            this._I_S_L_EndDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this._I_S_L_EndDate.Location = new System.Drawing.Point(102, 178);
            this._I_S_L_EndDate.Name = "_I_S_L_EndDate";
            this._I_S_L_EndDate.Size = new System.Drawing.Size(102, 20);
            this._I_S_L_EndDate.TabIndex = 373;
            // 
            // _I_S_L_StartDate
            // 
            this._I_S_L_StartDate.CustomFormat = "dd MMM yyyy";
            this._I_S_L_StartDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this._I_S_L_StartDate.Location = new System.Drawing.Point(102, 156);
            this._I_S_L_StartDate.Name = "_I_S_L_StartDate";
            this._I_S_L_StartDate.Size = new System.Drawing.Size(102, 20);
            this._I_S_L_StartDate.TabIndex = 372;
            // 
            // _I_S_L_Length
            // 
            this._I_S_L_Length.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._I_S_L_Length.Location = new System.Drawing.Point(102, 136);
            this._I_S_L_Length.Name = "_I_S_L_Length";
            this._I_S_L_Length.Size = new System.Drawing.Size(102, 20);
            this._I_S_L_Length.TabIndex = 371;
            // 
            // _I_S_L_Type
            // 
            this._I_S_L_Type.FormattingEnabled = true;
            this._I_S_L_Type.Items.AddRange(new object[] {
            "Full Service",
            "Medical",
            "NNN",
            "Office",
            "Per Month",
            "Retail",
            "Warehouse"});
            this._I_S_L_Type.Location = new System.Drawing.Point(102, 115);
            this._I_S_L_Type.Name = "_I_S_L_Type";
            this._I_S_L_Type.Size = new System.Drawing.Size(102, 21);
            this._I_S_L_Type.TabIndex = 370;
            // 
            // _I_S_F_TennantComment
            // 
            this._I_S_F_TennantComment.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._I_S_F_TennantComment.Location = new System.Drawing.Point(254, 241);
            this._I_S_F_TennantComment.Multiline = true;
            this._I_S_F_TennantComment.Name = "_I_S_F_TennantComment";
            this._I_S_F_TennantComment.Size = new System.Drawing.Size(232, 47);
            this._I_S_F_TennantComment.TabIndex = 369;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(251, 228);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(91, 13);
            this.label16.TabIndex = 368;
            this.label16.Text = "Tenant Comment:";
            // 
            // _I_S_L_LeaseComments
            // 
            this._I_S_L_LeaseComments.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._I_S_L_LeaseComments.Location = new System.Drawing.Point(6, 241);
            this._I_S_L_LeaseComments.Multiline = true;
            this._I_S_L_LeaseComments.Name = "_I_S_L_LeaseComments";
            this._I_S_L_LeaseComments.Size = new System.Drawing.Size(232, 47);
            this._I_S_L_LeaseComments.TabIndex = 367;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(6, 228);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(91, 13);
            this.label15.TabIndex = 366;
            this.label15.Text = "Lease Comments:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(251, 180);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(75, 13);
            this.label11.TabIndex = 365;
            this.label11.Text = "CAM Charges:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(250, 159);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(76, 13);
            this.label12.TabIndex = 364;
            this.label12.Text = "NNN Charges:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(250, 138);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(81, 13);
            this.label13.TabIndex = 363;
            this.label13.Text = "Expenses Stop:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(251, 117);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(99, 13);
            this.label14.TabIndex = 362;
            this.label14.Text = "TI Allowance/SqFt:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(7, 203);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(86, 13);
            this.label10.TabIndex = 361;
            this.label10.Text = "Renewal Option:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 182);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(55, 13);
            this.label9.TabIndex = 360;
            this.label9.Text = "End Date:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 160);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(58, 13);
            this.label8.TabIndex = 359;
            this.label8.Text = "Start Date:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 138);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(43, 13);
            this.label7.TabIndex = 358;
            this.label7.Text = "Length:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 118);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(34, 13);
            this.label6.TabIndex = 357;
            this.label6.Text = "Type:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(251, 98);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(58, 13);
            this.label5.TabIndex = 356;
            this.label5.Text = "Financial";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(6, 98);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(41, 13);
            this.label4.TabIndex = 355;
            this.label4.Text = "Lease";
            // 
            // _I_S_Comments
            // 
            this._I_S_Comments.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._I_S_Comments.Location = new System.Drawing.Point(235, 18);
            this._I_S_Comments.Multiline = true;
            this._I_S_Comments.Name = "_I_S_Comments";
            this._I_S_Comments.Size = new System.Drawing.Size(251, 72);
            this._I_S_Comments.TabIndex = 354;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(177, 20);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 13);
            this.label3.TabIndex = 353;
            this.label3.Text = "Comments:";
            // 
            // _I_S_Space
            // 
            this._I_S_Space.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._I_S_Space.Location = new System.Drawing.Point(46, 18);
            this._I_S_Space.Name = "_I_S_Space";
            this._I_S_Space.ReadOnly = true;
            this._I_S_Space.Size = new System.Drawing.Size(65, 20);
            this._I_S_Space.TabIndex = 352;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 20);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Space:";
            // 
            // _printIncomeReport
            // 
            this._printIncomeReport.Location = new System.Drawing.Point(745, 479);
            this._printIncomeReport.Name = "_printIncomeReport";
            this._printIncomeReport.Size = new System.Drawing.Size(125, 41);
            this._printIncomeReport.TabIndex = 352;
            this._printIncomeReport.Text = "Print Income Report";
            this._printIncomeReport.UseVisualStyleBackColor = true;
            // 
            // _I_Summary
            // 
            this._I_Summary.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._I_Summary.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader23,
            this.columnHeader24,
            this.columnHeader25,
            this.columnHeader26,
            this.columnHeader27,
            this.columnHeader28,
            this.columnHeader29,
            this.columnHeader30,
            this.columnHeader31,
            this.columnHeader32,
            this.columnHeader33,
            this.columnHeader34,
            this.columnHeader35});
            this._I_Summary.FullRowSelect = true;
            this._I_Summary.GridLines = true;
            this._I_Summary.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this._I_Summary.Location = new System.Drawing.Point(31, 37);
            this._I_Summary.Name = "_I_Summary";
            this._I_Summary.Size = new System.Drawing.Size(855, 163);
            this._I_Summary.TabIndex = 260;
            this._I_Summary.UseCompatibleStateImageBehavior = false;
            this._I_Summary.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader23
            // 
            this.columnHeader23.Text = "Space";
            this.columnHeader23.Width = 47;
            // 
            // columnHeader24
            // 
            this.columnHeader24.Text = "Occupied";
            this.columnHeader24.Width = 58;
            // 
            // columnHeader25
            // 
            this.columnHeader25.Text = "Tenant";
            this.columnHeader25.Width = 76;
            // 
            // columnHeader26
            // 
            this.columnHeader26.Text = "Floor";
            this.columnHeader26.Width = 44;
            // 
            // columnHeader27
            // 
            this.columnHeader27.Text = "SqFt.";
            this.columnHeader27.Width = 69;
            // 
            // columnHeader28
            // 
            this.columnHeader28.Text = "$/Month";
            this.columnHeader28.Width = 56;
            // 
            // columnHeader29
            // 
            this.columnHeader29.Text = "$/Year";
            this.columnHeader29.Width = 54;
            // 
            // columnHeader30
            // 
            this.columnHeader30.Text = "$/SqFt/Month";
            this.columnHeader30.Width = 86;
            // 
            // columnHeader31
            // 
            this.columnHeader31.Text = "$/SqFt/Year";
            this.columnHeader31.Width = 73;
            // 
            // columnHeader32
            // 
            this.columnHeader32.Text = "$/Month";
            this.columnHeader32.Width = 55;
            // 
            // columnHeader33
            // 
            this.columnHeader33.Text = "$/Year";
            this.columnHeader33.Width = 55;
            // 
            // columnHeader34
            // 
            this.columnHeader34.Text = "$/SqFt/Month";
            this.columnHeader34.Width = 83;
            // 
            // columnHeader35
            // 
            this.columnHeader35.Text = "$/SqFt/Year";
            this.columnHeader35.Width = 75;
            // 
            // Income_RetailOrOffice
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.Controls.Add(this._printIncomeReport);
            this.Controls.Add(this._spaceGroupBox);
            this.Controls.Add(this._I_PI_VacancyOrCreditLossPerc);
            this.Controls.Add(this._I_PI_OperatingIncome);
            this.Controls.Add(this._I_PI_OtherIncome);
            this.Controls.Add(this._I_PI_VacancyOrCreditLoss);
            this.Controls.Add(this._I_PI_GrossRent);
            this.Controls.Add(this.label106);
            this.Controls.Add(this.label107);
            this.Controls.Add(this.label108);
            this.Controls.Add(this.label109);
            this.Controls.Add(this.label110);
            this.Controls.Add(this.label111);
            this.Controls.Add(this.label112);
            this.Controls.Add(this.label113);
            this.Controls.Add(this.label114);
            this.Controls.Add(this.label115);
            this.Controls.Add(this._I_AI_VacancyOrCreditLossPerc);
            this.Controls.Add(this._I_AI_OperatingIncome);
            this.Controls.Add(this._I_AI_OtherIncome);
            this.Controls.Add(this._I_AI_VacancyOrCreditLoss);
            this.Controls.Add(this._I_AI_GrossRent);
            this.Controls.Add(this.label96);
            this.Controls.Add(this.label97);
            this.Controls.Add(this.label98);
            this.Controls.Add(this.label99);
            this.Controls.Add(this.label100);
            this.Controls.Add(this.label101);
            this.Controls.Add(this.label102);
            this.Controls.Add(this.label103);
            this.Controls.Add(this.label104);
            this.Controls.Add(this.label105);
            this.Controls.Add(this._I_S_Total_PR_UsdPerSqftPerYear);
            this.Controls.Add(this._I_S_Total_PR_UsdPerSqFtPerMonth);
            this.Controls.Add(this._I_S_Total_PR_UsdPerYear);
            this.Controls.Add(this._I_UTS_Total_AR_UsdPerSqFt);
            this.Controls.Add(this._I_S_Total_PR_UsdPerMonth);
            this.Controls.Add(this._I_S_Total_AR_UsdPerSqFtPerMonth);
            this.Controls.Add(this._I_S_Total_AR_UsdPerYear);
            this.Controls.Add(this._I_S_Total_AR_UsdPerMonth);
            this.Controls.Add(this._I_UTS_TotalSqFt);
            this.Controls.Add(this._I_UTS_TotalOccupied);
            this.Controls.Add(this.label66);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label68);
            this.Controls.Add(this._I_Summary);
            this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.Name = "Income_RetailOrOffice";
            this.Size = new System.Drawing.Size(897, 529);
            this._spaceGroupBox.ResumeLayout(false);
            this._spaceGroupBox.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private ListViewEx _I_Summary;
        private System.Windows.Forms.ColumnHeader columnHeader23;
        private System.Windows.Forms.ColumnHeader columnHeader24;
        private System.Windows.Forms.ColumnHeader columnHeader25;
        private System.Windows.Forms.ColumnHeader columnHeader26;
        private System.Windows.Forms.ColumnHeader columnHeader27;
        private System.Windows.Forms.ColumnHeader columnHeader28;
        private System.Windows.Forms.ColumnHeader columnHeader29;
        private System.Windows.Forms.ColumnHeader columnHeader30;
        private System.Windows.Forms.ColumnHeader columnHeader31;
        private System.Windows.Forms.ColumnHeader columnHeader32;
        private System.Windows.Forms.ColumnHeader columnHeader33;
        private System.Windows.Forms.ColumnHeader columnHeader34;
        private System.Windows.Forms.ColumnHeader columnHeader35;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox _I_S_Total_PR_UsdPerSqftPerYear;
        private System.Windows.Forms.TextBox _I_S_Total_PR_UsdPerSqFtPerMonth;
        private System.Windows.Forms.TextBox _I_S_Total_PR_UsdPerYear;
        private System.Windows.Forms.TextBox _I_UTS_Total_AR_UsdPerSqFt;
        private System.Windows.Forms.TextBox _I_S_Total_PR_UsdPerMonth;
        private System.Windows.Forms.TextBox _I_S_Total_AR_UsdPerSqFtPerMonth;
        private System.Windows.Forms.TextBox _I_S_Total_AR_UsdPerYear;
        private System.Windows.Forms.TextBox _I_S_Total_AR_UsdPerMonth;
        private System.Windows.Forms.TextBox _I_UTS_TotalSqFt;
        private System.Windows.Forms.TextBox _I_UTS_TotalOccupied;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.TextBox _I_AI_VacancyOrCreditLossPerc;
        private System.Windows.Forms.TextBox _I_AI_OperatingIncome;
        private System.Windows.Forms.TextBox _I_AI_OtherIncome;
        private System.Windows.Forms.TextBox _I_AI_VacancyOrCreditLoss;
        private System.Windows.Forms.TextBox _I_AI_GrossRent;
        private System.Windows.Forms.Label label96;
        private System.Windows.Forms.Label label97;
        private System.Windows.Forms.Label label98;
        private System.Windows.Forms.Label label99;
        private System.Windows.Forms.Label label100;
        private System.Windows.Forms.Label label101;
        private System.Windows.Forms.Label label102;
        private System.Windows.Forms.Label label103;
        private System.Windows.Forms.Label label104;
        private System.Windows.Forms.Label label105;
        private System.Windows.Forms.TextBox _I_PI_VacancyOrCreditLossPerc;
        private System.Windows.Forms.TextBox _I_PI_OperatingIncome;
        private System.Windows.Forms.TextBox _I_PI_OtherIncome;
        private System.Windows.Forms.TextBox _I_PI_VacancyOrCreditLoss;
        private System.Windows.Forms.TextBox _I_PI_GrossRent;
        private System.Windows.Forms.Label label106;
        private System.Windows.Forms.Label label107;
        private System.Windows.Forms.Label label108;
        private System.Windows.Forms.Label label109;
        private System.Windows.Forms.Label label110;
        private System.Windows.Forms.Label label111;
        private System.Windows.Forms.Label label112;
        private System.Windows.Forms.Label label113;
        private System.Windows.Forms.Label label114;
        private System.Windows.Forms.Label label115;
        private System.Windows.Forms.GroupBox _spaceGroupBox;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox _I_S_Comments;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox _I_S_Space;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox _I_S_F_TennantComment;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox _I_S_L_LeaseComments;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.ComboBox _I_S_L_RenewalOption;
        private System.Windows.Forms.DateTimePicker _I_S_L_EndDate;
        private System.Windows.Forms.DateTimePicker _I_S_L_StartDate;
        private System.Windows.Forms.TextBox _I_S_L_Length;
        private System.Windows.Forms.ComboBox _I_S_L_Type;
        private System.Windows.Forms.TextBox _I_S_F_TIAllowance;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox _I_S_F_CAMCharges;
        private System.Windows.Forms.TextBox _I_S_F_NNNCharges;
        private System.Windows.Forms.TextBox _I_S_F_ExpensesStop;
        private System.Windows.Forms.Button _printIncomeReport;
    }
}
