using System;
using System.Collections;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization.Formatters;
using System.Runtime.Serialization;
using System.IO;


namespace ProfitGrabber.Commercial.Objects
{
	/// <summary>
	/// Summary description for FinancialInfoObject.
	/// </summary>
	[Serializable]
	public sealed class FinancialInfoObject
	{
		static FinancialInfoObject _instance = new	FinancialInfoObject();

		static FinancialInfoObject()
		{			
		}

		FinancialInfoObject()
		{			
		}

		public static FinancialInfoObject Instance
		{
			get { return _instance; }
		}
		
		FA_Data[] _data = new FA_Data[3] { new FA_Data(), new FA_Data(), new FA_Data() };

		/// <summary>
		/// 0 = ACTUAL
		/// 1 = PROFORMA
		/// 2 = SCENARIO
		/// </summary>
		public FA_Data[] Data
		{
			get { return _data; }
			set { _data = value; }
		}

		public void ResetObject()
		{
			_instance = new FinancialInfoObject();
		}

		public byte[] SerializeToByteArray()
		{			
			try
			{
				BinaryFormatter bf = new BinaryFormatter();
				MemoryStream ms = new MemoryStream();
				bf.Serialize(ms, FinancialInfoObject.Instance);				
				return ms.ToArray();
			}
			catch 
			{
				return null;
			}
		}

		public FinancialInfoObject DeserializeFromByteArray(byte[] bytes)
		{
			
			try 
			{				
				MemoryStream ms = new MemoryStream(bytes);
				BinaryFormatter bf = new BinaryFormatter();

				ms.Position = 0;
				object obj = bf.Deserialize(ms);			
				
				_instance = (FinancialInfoObject)obj;
				return (FinancialInfoObject)obj;
			}
			catch
			{
				return null;
			}
		}

		public Stream SerializeToStream()
		{
			IFormatter formatter = new BinaryFormatter();
			Stream stream = new FileStream(@"c:\FIO.bin", FileMode.Create, FileAccess.Write, FileShare.None);
			formatter.Serialize(stream, FinancialInfoObject.Instance);
			stream.Close();

			return stream;
		}

		public FinancialInfoObject DeserializeFromStream()
		{
			IFormatter formatter = new BinaryFormatter();
			Stream stream = new FileStream(@"c:\FIO.bin", FileMode.Open, FileAccess.Read, FileShare.Read);
			FinancialInfoObject obj = (FinancialInfoObject) formatter.Deserialize(stream);
			stream.Close();
			_instance = obj;
			return obj;
		}

	}

	[Serializable]
	public class FA_Data
	{
		//DATA
		string _price = string.Empty;
		string _gsiAnnual = string.Empty;
		string _vacancyCreditLoss = string.Empty;
		string _vacancyCreditLossPerc = string.Empty;
		string _otherIncome = string.Empty;
		string _goiAnnual = string.Empty;
		string _operatingExpenses = string.Empty;
		string _operatingExpensesPerc = string.Empty;		
		string _netOperatingIncome = string.Empty;
		string _capRate = string.Empty;
		string _debtService = string.Empty;
		string _debtCoverageRatio = string.Empty;
		string _cashFlowPreTax = string.Empty;
		string _cashOnCashReturn = string.Empty;
		string _comments = string.Empty;

		//DERIVED DATA
		decimal _priceValue = 0;
		decimal _gsiAnnualValue = 0;
		decimal _vacancyCreditLossValue = 0;
		decimal _vacancyCreditLossPercValue = 0;
		decimal _otherIncomeValue = 0;
		decimal _goiAnnualValue = 0;
		decimal _operatingExpensesValue = 0;
		decimal _operatingExpensesPercValue = 0;		
		decimal _netOperatingIncomeValue = 0;
		decimal _capRateValue = 0;
		decimal _debtServiceValue = 0;
		decimal _debtCoverageRatioValue = 0;
		decimal _cashFlowPreTaxValue = 0;
		decimal _cashOnCashReturnValue = 0;

		public FA_Data()
		{}

		public FA_Data(FA_Data other)
		{
			this._price = other._price;
			this._gsiAnnual = other._gsiAnnual;
			this._vacancyCreditLoss = other._vacancyCreditLoss;
			this._vacancyCreditLossPerc = other._vacancyCreditLossPerc;
			this._otherIncome = other._otherIncome;
			this._goiAnnual = other._goiAnnual;
			this._operatingExpenses = other._operatingExpenses;
			this._operatingExpensesPerc = other._operatingExpensesPerc;
			this._netOperatingIncome = other._netOperatingIncome;
			this._capRate = other._capRate;
			this._debtService = other._debtService;
			this._debtCoverageRatio = other._debtCoverageRatio;
			this._cashFlowPreTax = other._cashFlowPreTax;
			this._cashOnCashReturn = other._cashOnCashReturn;
			this._comments = other._comments;

			this._priceValue = other._priceValue;
			this._gsiAnnualValue = other._gsiAnnualValue;
			this._vacancyCreditLossValue = other._vacancyCreditLossValue;
			this._vacancyCreditLossPercValue = other._vacancyCreditLossPercValue;
			this._otherIncomeValue = other._otherIncomeValue;
			this._goiAnnualValue = other._goiAnnualValue;
			this._operatingExpensesValue = other._operatingExpensesValue;
			this._operatingExpensesPercValue = other._operatingExpensesPercValue;
			this._netOperatingIncomeValue = other._netOperatingIncomeValue;
			this._capRateValue = other._capRateValue;
			this._debtServiceValue = other._debtServiceValue;
			this._debtCoverageRatioValue = other._debtCoverageRatioValue;
			this._cashFlowPreTaxValue = other._cashFlowPreTaxValue;
			this._cashOnCashReturnValue = other._cashOnCashReturnValue;
		}

		#region DERIVED PROPERTIES
		public System.Decimal PriceValueProperty
		{
			get
			{
				_priceValue = 0;

				try { _priceValue = Convert.ToDecimal(_price); }
				catch {_priceValue = 0;}

				return _priceValue;
			}			
		}

		public System.Decimal GsiAnnualValueProperty
		{
			get
			{
				_gsiAnnualValue = 0;

				try { _gsiAnnualValue = Convert.ToDecimal(_gsiAnnual); }
				catch {_gsiAnnualValue = 0;}

				return _gsiAnnualValue;
			}			
		}

		public System.Decimal VacancyCreditLossValueProperty
		{
			get
			{
				_vacancyCreditLossValue = 0;

				try { _vacancyCreditLossValue = Convert.ToDecimal(_vacancyCreditLoss); }
				catch {_vacancyCreditLossValue = 0;}

				return _vacancyCreditLossValue;
			}			
		}

		public System.Decimal VacancyCreditLossPercValueProperty
		{
			get
			{
				_vacancyCreditLossPercValue = 0;

				try { _vacancyCreditLossPercValue = Convert.ToDecimal(_vacancyCreditLossPerc); }
				catch {_vacancyCreditLossPercValue = 0;}

				return _vacancyCreditLossPercValue;
			}			
		}

		public System.Decimal OtherIncomeValueProperty
		{
			get
			{
				_otherIncomeValue = 0;

				try { _otherIncomeValue = Convert.ToDecimal(_otherIncome); }
				catch {_otherIncomeValue = 0;}

				return _otherIncomeValue;
			}			
		}

		public System.Decimal GoiAnnualValueProperty
		{
			get
			{
				_goiAnnualValue = 0;

				try { _goiAnnualValue = Convert.ToDecimal(_goiAnnual); }
				catch {_otherIncomeValue = 0;}

				return _goiAnnualValue;
			}			
		}

		public System.Decimal OperatingExpensesValueProperty
		{
			get
			{
				_operatingExpensesValue = 0;

				try { _operatingExpensesValue = Convert.ToDecimal(_operatingExpenses); }
				catch {_operatingExpensesValue = 0;}

				return _operatingExpensesValue;
			}		
		}

		public System.Decimal OperatingExpensesPercValueProperty
		{
			get
			{
				_operatingExpensesPercValue = 0;

				try { _operatingExpensesPercValue = Convert.ToDecimal(_operatingExpensesPerc); }
				catch { _operatingExpensesPercValue = 0; }

				return _operatingExpensesPercValue;
			}			
		}

		public System.Decimal NetOperatingIncomeValueProperty
		{
			get
			{
				_netOperatingIncomeValue = 0;

				try { _netOperatingIncomeValue = Convert.ToDecimal(_netOperatingIncome); }
				catch {_netOperatingIncomeValue = 0;}

				return _netOperatingIncomeValue;
			}			
		}

		public System.Decimal CapRateValueProperty
		{
			get
			{
				_capRateValue = 0;

				try { _capRateValue = Convert.ToDecimal(_capRate); }
				catch {_capRateValue = 0;}

				return _capRateValue;
			}			
		}

		public System.Decimal DebtServiceValueProperty
		{
			get
			{
				_debtServiceValue = 0;

				try { _debtServiceValue = Convert.ToDecimal(_debtService); }
				catch {_debtServiceValue = 0;}

				return _debtServiceValue;
			}			
		}

		public System.Decimal DebtCoverageRatioValueProperty
		{
			get
			{
				_debtCoverageRatioValue = 0;

				try { _debtCoverageRatioValue = Convert.ToDecimal(_debtCoverageRatio); }
				catch {_debtCoverageRatioValue = 0;}

				return _debtCoverageRatioValue;
			}			
		}

		public System.Decimal CashFlowPreTaxValueProperty
		{
			get
			{
				_cashFlowPreTaxValue = 0;

				try { _cashFlowPreTaxValue = Convert.ToDecimal(_cashFlowPreTax); }
				catch {_cashFlowPreTaxValue = 0;}

				return _cashFlowPreTaxValue;
			}			
		}

		public System.Decimal CashOnCashReturnValueProperty
		{
			get
			{
				_cashOnCashReturnValue = 0;

				try { _cashOnCashReturnValue = Convert.ToDecimal(_cashOnCashReturn); }
				catch {_cashOnCashReturnValue = 0;}

				return _cashOnCashReturnValue;
			}			
		}

		#endregion
		
		#region PROPERTIES
		public System.String PriceProperty
		{
			get
			{
				return _price;
			}
			set
			{
				_price = value;
			}
		}

		public System.String GsiAnnualProperty
		{
			get
			{
				return _gsiAnnual;
			}
			set
			{
				_gsiAnnual = value;
			}
		}

		public System.String VacancyCreditLossProperty
		{
			get
			{
				return _vacancyCreditLoss;
			}
			set
			{
				_vacancyCreditLoss = value;
			}
		}

		public System.String VacancyCreditLossPercProperty
		{
			get
			{
				return _vacancyCreditLossPerc;
			}
			set
			{
				_vacancyCreditLossPerc = value;
			}
		}

		public System.String OtherIncomeProperty
		{
			get
			{
				return _otherIncome;
			}
			set
			{
				_otherIncome = value;
			}
		}

		public System.String GoiAnnualProperty
		{
			get
			{
				return _goiAnnual;
			}
			set
			{
				_goiAnnual = value;
			}
		}

		public System.String OperatingExpensesProperty
		{
			get
			{
				return _operatingExpenses;
			}
			set
			{
				_operatingExpenses = value;
			}
		}

		public System.String OperatingExpensesPercProperty
		{
			get
			{
				return _operatingExpensesPerc;
			}
			set
			{
				_operatingExpensesPerc = value;
			}
		}

		public System.String NetOperatingIncomeProperty
		{
			get
			{
				return _netOperatingIncome;
			}
			set
			{
				_netOperatingIncome = value;
			}
		}

		public System.String CapRateProperty
		{
			get
			{
				return _capRate;
			}
			set
			{
				_capRate = value;
			}
		}


		public System.String DebtServiceProperty
		{
			get
			{
				return _debtService;
			}
			set
			{
				_debtService = value;
			}
		}

		public System.String DebtCoverageRatioProperty
		{
			get
			{
				return _debtCoverageRatio;
			}
			set
			{
				_debtCoverageRatio = value;
			}
		}

		public System.String CashFlowPreTaxProperty
		{
			get
			{
				return _cashFlowPreTax;
			}
			set
			{
				_cashFlowPreTax = value;
			}
		}

		public System.String CashOnCashReturnProperty
		{
			get
			{
				return _cashOnCashReturn;
			}
			set
			{
				_cashOnCashReturn = value;
			}
		}

		public System.String CommentsProperty
		{
			get
			{
				return _comments;
			}
			set
			{
				_comments = value;
			}
		}
		#endregion

	}
}
