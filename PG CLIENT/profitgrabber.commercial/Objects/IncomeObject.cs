using System;
using System.Collections;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization.Formatters;
using System.Runtime.Serialization;
using System.IO;


namespace ProfitGrabber.Commercial.Objects
{
	/// <summary>
	/// Summary description for IncomeObject.
	/// </summary>
	[Serializable]
	public sealed class IncomeObject
	{
		#region SINGLETON
		static IncomeObject _instance = new IncomeObject();

		static IncomeObject()
		{			
		}

		IncomeObject()
		{
			for (int i = 0; i < UTSMaxItems; i++)
				_unitTypeSummaries[i] = new UnitTypeSummary();

			for (int i = 0; i < RRMaxItems; i++)
				_rentRolls[i] = new RentRoll();

            for (int i = 0; i < ROMaxItems; i++)
                _retailOfficeSummaries[i] = new RetailOfficeSummary();

            for (int i = 0; i < SSMaxItems; i++)
                _selfStorageSummaries[i] = new SelfStorageSummary();
		}

		public static IncomeObject Instance
		{
			get {return _instance;}			
		}
		#endregion

		public const int UTSMaxItems = 20;
		public const int RRMaxItems = 50;
        public const int ROMaxItems = 20;
        public const int SSMaxItems = 20;
	
        //COMMON
		ActualIncome _actualIncomeObject = new ActualIncome();		
		ProformaIncome _proformaIncomeObject = new ProformaIncome();

        //MULTIFAMILY
        UnitTypeSummary[] _unitTypeSummaries = new UnitTypeSummary[UTSMaxItems];
        RentRoll[] _rentRolls = new RentRoll[RRMaxItems];
        OtherIncome _otherIncomeObject = new OtherIncome();	

        //RETAIL - OFFICE
        RetailOfficeSummary[] _retailOfficeSummaries = new RetailOfficeSummary[ROMaxItems];        

        //SELFSTORAGE
        SelfStorageSummary[] _selfStorageSummaries = new SelfStorageSummary[SSMaxItems];

		
		#region DATA
		public UnitTypeSummary[] UnitTypeSummaries
		{
			get
			{
				return _unitTypeSummaries;
			}
			set
			{
				_unitTypeSummaries = value;
			}
		}

		public RentRoll[] RentRolls
		{
			get
			{
				return _rentRolls;
			}
			set
			{
				_rentRolls = value;
			}
		}

		public ProfitGrabber.Commercial.Objects.OtherIncome OtherIncomeObject
		{
			get
			{
				return _otherIncomeObject;
			}
			set
			{
				_otherIncomeObject = value;
			}
		}

		public ProfitGrabber.Commercial.Objects.ActualIncome ActualIncomeObject
		{
			get
			{
				return _actualIncomeObject;
			}
			set
			{
				_actualIncomeObject = value;
			}
		}

		public ProfitGrabber.Commercial.Objects.ProformaIncome ProformaIncomeObject
		{
			get
			{
				return _proformaIncomeObject;
			}
			set
			{
				_proformaIncomeObject = value;
			}
		}

        public RetailOfficeSummary[] RetailOfficeSummaryObject
        {
            get { return _retailOfficeSummaries; }
            set { _retailOfficeSummaries = value; }
        }        

        public SelfStorageSummary[] SelfStorageSummaryObject
        {
            get { return _selfStorageSummaries; }
            set { _selfStorageSummaries = value; }
        }
		#endregion

		#region DERIVED DATA FOR UNIT TYPE SUMMARY		
		public int UTS_TotalNumberOfUnits
		{
			get 
			{
				int total = 0;
				for (int i = 0; i < IncomeObject.UTSMaxItems; i++)
				{
					int nrOfUnits = 0;
					try 
                    {
                        if (string.Empty != UnitTypeSummaries[i].NrOfUnitsProperty)
                            nrOfUnits = Convert.ToInt32(UnitTypeSummaries[i].NrOfUnitsProperty);
                    }
					catch {nrOfUnits = 0;} 

					total += nrOfUnits;
				}
				return total;
			}
		}

		
		public decimal UTS_WeightedAverageTotalOccupancy
		{
			get 
			{
				decimal result = 0;
				int sumOfUnits = 0;

				for (int i = 0; i < IncomeObject.UTSMaxItems; i++)
				{
					if (UnitTypeSummaries[i].IsValid)
					{
						result += UnitTypeSummaries[i].RelOccupancy;
						sumOfUnits += UnitTypeSummaries[i].NumberOfUnits;
					}
				}

				if (0 != sumOfUnits)
					return result / sumOfUnits;
				else 
					return 0;
			}
		}

		
		public decimal UTS_WeightedAverageTotalSqFtLow
		{
			get 
			{
				decimal result = 0;

				for (int i = 0; i < IncomeObject.UTSMaxItems; i++)
				{
					if (UnitTypeSummaries[i].IsValid)
						result += UnitTypeSummaries[i].SqFtLowPerItem;
				}

				return result;
			}
		}

		
		public decimal UTS_WeightedAverageTotalSqFtHigh
		{
			get 
			{
				decimal result = 0;

				for (int i = 0; i < IncomeObject.UTSMaxItems; i++)
				{
					if (UnitTypeSummaries[i].IsValid)
						result += UnitTypeSummaries[i].SqFtHighPerItem;
				}

				return result;
			}
		}


		public decimal UTS_SumTotalARLow
		{
			get 
			{
				decimal result = 0;

				for (int i = 0; i < IncomeObject.UTSMaxItems; i++)
				{
					if (UnitTypeSummaries[i].IsValid)
						result += UnitTypeSummaries[i].ARLowPerItem;
				}

				return result;
			}
		}


		public decimal UTS_SumTotalARHigh
		{
			get 
			{
				decimal result = 0;

				for (int i = 0; i < IncomeObject.UTSMaxItems; i++)
				{
					if (UnitTypeSummaries[i].IsValid)
						result += UnitTypeSummaries[i].ARHighPerItem;
				}

				return result;
			}
		}


		public decimal UTS_WeightedAverageARUSDPerSqFt
		{
			get 
			{
				decimal result = 0;
				int sumOfUnits = 0;
				
				for (int i = 0; i < IncomeObject.UTSMaxItems; i++)
				{
					if (0 != UnitTypeSummaries[i].AvgSquareFeet)
					{
						if (UnitTypeSummaries[i].IsValid)
						{
							result += (UnitTypeSummaries[i].AvgRent / UnitTypeSummaries[i].AvgSquareFeet) * UnitTypeSummaries[i].NumberOfUnits;							
							sumOfUnits += UnitTypeSummaries[i].NumberOfUnits;
						}
					}
				}

				if (0 != sumOfUnits)
					return result / sumOfUnits;
				else
					return 0;
			}
		}

				
		public decimal UTS_SumTotalMRLow
		{
			get 
			{
				decimal result = 0;

				for (int i = 0; i < IncomeObject.UTSMaxItems; i++)
				{
					if (UnitTypeSummaries[i].IsValid)
						result += UnitTypeSummaries[i].MRLowPerItem;
				}

				return result;
			}
		}


		public decimal UTS_SumTotalMRHigh
		{
			get 
			{
				decimal result = 0;

				for (int i = 0; i < IncomeObject.UTSMaxItems; i++)
				{
					if (UnitTypeSummaries[i].IsValid)
						result += UnitTypeSummaries[i].MRHighPerItem;
				}

				return result;
			}
		}


		public decimal UTS_WeightedAverageMRUSDPerSqFt
		{
			get 
			{
				decimal result = 0;
				int sumOfUnits = 0;

				for (int i = 0; i < IncomeObject.UTSMaxItems; i++)
				{
					if (UnitTypeSummaries[i].IsValid)
					{
						result += UnitTypeSummaries[i].MarketRentPerItem * UnitTypeSummaries[i].NumberOfUnits;
						sumOfUnits += UnitTypeSummaries[i].NumberOfUnits;
					}
				}

				if (0 != sumOfUnits)
					return result / sumOfUnits;
				else 
					return 0;
			}
		}
		#endregion

		#region DERIVED DATA FOR RENT ROLL
		public decimal RR_TotalSqft
		{
			get
			{
				decimal res = 0;				
				for (int i = 0; i < RRMaxItems; i++)
					res += _rentRolls[i].IsValid ? _rentRolls[i].SqFtPerItem : 0;

				return res;
			}
		}

		public decimal RR_TotalSecDep
		{
			get
			{
				decimal res = 0;				
				for (int i = 0; i < RRMaxItems; i++)
					res += _rentRolls[i].IsValid ? _rentRolls[i].SecDepPerItem: 0;

				return res;
			}
		}

		public decimal RR_TotalTennantRent
		{
			get
			{
				decimal res = 0;				
				for (int i = 0; i < RRMaxItems; i++)
					res += _rentRolls[i].IsValid ? _rentRolls[i].TennantRentPerItem : 0;

				return res;
			}
		}

		public decimal RR_TotalSec8Rent
		{
			get
			{
				decimal res = 0;				
				for (int i = 0; i < RRMaxItems; i++)
					res += _rentRolls[i].IsValid ? _rentRolls[i].Sec8PerItem : 0;

				return res;
			}
		}

		public decimal RR_TotalTotalRent
		{
			get
			{
				decimal res = 0;				
				for (int i = 0; i < RRMaxItems; i++)
					res += _rentRolls[i].IsValid ? _rentRolls[i].TotalRentProperty : 0;

				return res;
			}
		}

		public decimal RR_TotalMarketRent
		{
			get
			{
				decimal res = 0;				
				for (int i = 0; i < RRMaxItems; i++)
					res += _rentRolls[i].IsValid ? _rentRolls[i].MarketRentPerItem : 0;

				return res;
			}
		}
		#endregion

        #region DERIVED DATA FOR RETAIL OFFICE SUMMARY

        public decimal GetTotalOccupied()
        {
            decimal occupied = 0;            
            for (int i = 0; i < ROMaxItems; i++)
            {
                if ("Yes" == _retailOfficeSummaries[i].Occupied)
                    occupied += Util.GetDecimalFromString(_retailOfficeSummaries[i].SqFt);
            }
            return (0 != GetTotalSqFt()) ? occupied / GetTotalSqFt() : 0;
        }

        public decimal GetTotalSqFt()
        {
            decimal occupied = 0;
            for (int i = 0; i < ROMaxItems; i++)
            {                
                occupied += Util.GetDecimalFromString(_retailOfficeSummaries[i].SqFt);
            }
            return occupied;
        }

        public decimal GetTotal_AR_UsdPerMonth()
        {
            decimal val = 0;

            for (int i = 0; i < ROMaxItems; i++)
            {
                val += _retailOfficeSummaries[i].P_AR_UsdPerMonthPerItem;
            }

            return val;
        }

        public decimal GetTotal_AR_UsdPerYear()
        {
            decimal val = 0;

            for (int i = 0; i < ROMaxItems; i++)
            {
                val += _retailOfficeSummaries[i].P_AR_UsdPerYearPerItem;
            }

            return val;
        }

        public decimal GetTotal_AR_UsdPerMonthPerSqFt()
        {
            return (0 != GetTotalSqFt()) ? GetTotal_AR_UsdPerMonth() / GetTotalSqFt() : 0;
        }

        public decimal GetTotal_AR_UsdPerYearPerSqFt()
        {
            return (0 != GetTotalSqFt()) ? GetTotal_AR_UsdPerYear() / GetTotalSqFt() : 0;
        }

        public decimal GetTotal_PR_UsdPerMonth()
        {
            decimal val = 0;

            for (int i = 0; i < ROMaxItems; i++)
            {
                val += _retailOfficeSummaries[i].P_PR_UsdPerMonthPerItem;
            }

            return val;
        }

        public decimal GetTotal_PR_UsdPerYear()
        {
            decimal val = 0;

            for (int i = 0; i < ROMaxItems; i++)
            {
                val += _retailOfficeSummaries[i].P_PR_UsdPerYearPerItem;
            }

            return val;
        }

        public decimal GetTotal_PR_UsdPerMonthPerSqFt()
        {
            return (0 != GetTotalSqFt()) ? GetTotal_PR_UsdPerMonth() / GetTotalSqFt() : 0;
        }

        public decimal GetTotal_PR_UsdPerYearPerSqFt()
        {
            return (0 != GetTotalSqFt()) ? GetTotal_PR_UsdPerYear() / GetTotalSqFt() : 0;
        }

        #endregion

        #region DERIVED DATA FOR SELF STORAGE
        public decimal GetTotal_SS_Units()
        {
            decimal total = 0;
            for (int i = 0; i < IncomeObject.SSMaxItems; i++)
            {                
                total += Util.GetDecimalFromString(_selfStorageSummaries[i].Units);
            }
            return total;
        }

        public decimal GetTotal_SS_Occupancy()
        {            
			decimal weightedValue = 0;
			decimal weight = 0;

            for (int i = 0; i < IncomeObject.SSMaxItems; i++)
			{
                weightedValue += Util.GetDecimalFromString(_selfStorageSummaries[i].Occupancy) * Util.GetDecimalFromString(_selfStorageSummaries[i].Units);
                weight += Util.GetDecimalFromString(_selfStorageSummaries[i].Units);                
			}

            if (0 != weight)
                return weightedValue / weight;
			else 
				return 0;			
        }

        public decimal GetTotal_SS_SqFt()
        {
            decimal result = 0;            

            for (int i = 0; i < IncomeObject.SSMaxItems; i++)
            {
                result += _selfStorageSummaries[i].Sqft * Util.GetDecimalFromString(_selfStorageSummaries[i].Units);                
            }
            
            return result;            
        }

        public decimal GetTotal_SS_AR_Low()
        {
            decimal result = 0;

            for (int i = 0; i < IncomeObject.SSMaxItems; i++)
            {
                result += Util.GetDecimalFromString(_selfStorageSummaries[i].AR_Low) * Util.GetDecimalFromString(_selfStorageSummaries[i].Units);
            }

            return result;    
        }

        public decimal GetTotal_SS_AR_High()
        {
            decimal result = 0;

            for (int i = 0; i < IncomeObject.SSMaxItems; i++)
            {
                result += Util.GetDecimalFromString(_selfStorageSummaries[i].AR_High) * Util.GetDecimalFromString(_selfStorageSummaries[i].Units);
            }

            return result;    
        }

        public decimal GetTotal_SS_AR_UsdPerSqFt()
        {
            decimal weightedValue = 0;
            decimal weight = 0;

            for (int i = 0; i < IncomeObject.SSMaxItems; i++)
            {
                weightedValue += _selfStorageSummaries[i].ARPerItem * Util.GetDecimalFromString(_selfStorageSummaries[i].Units);
                weight += Util.GetDecimalFromString(_selfStorageSummaries[i].Units);
            }

            if (0 != weight)
                return weightedValue / weight;
            else
                return 0;			
        }

        public decimal GetTotal_SS_MR_Low()
        {
            decimal result = 0;

            for (int i = 0; i < IncomeObject.SSMaxItems; i++)
            {
                result += Util.GetDecimalFromString(_selfStorageSummaries[i].MR_Low) * Util.GetDecimalFromString(_selfStorageSummaries[i].Units);
            }

            return result;    
        }

        public decimal GetTotal_SS_MR_High()
        {
            decimal result = 0;

            for (int i = 0; i < IncomeObject.SSMaxItems; i++)
            {
                result += Util.GetDecimalFromString(_selfStorageSummaries[i].MR_High) * Util.GetDecimalFromString(_selfStorageSummaries[i].Units);
            }

            return result;    
        }

        public decimal GetTotal_SS_MR_UsdPerSqFt()
        {
            decimal weightedValue = 0;
            decimal weight = 0;

            for (int i = 0; i < IncomeObject.SSMaxItems; i++)
            {
                weightedValue += _selfStorageSummaries[i].MRPerItem * Util.GetDecimalFromString(_selfStorageSummaries[i].Units);
                weight += Util.GetDecimalFromString(_selfStorageSummaries[i].Units);
            }

            if (0 != weight)
                return weightedValue / weight;
            else
                return 0;
        }

        #endregion

        #region RESET OBJECT
        public void ResetObject()
		{
			_instance = new IncomeObject();
        }
        #endregion

        #region (DE)SERIALIZE
        public byte[] SerializeToByteArray()
		{			
			try
			{
				BinaryFormatter bf = new BinaryFormatter();
				MemoryStream ms = new MemoryStream();
				bf.Serialize(ms, IncomeObject.Instance);				
				return ms.ToArray();
			}
			catch 
			{
				return null;
			}
		}

		public IncomeObject DeserializeFromByteArray(byte[] bytes)
		{
			
			try 
			{				
				MemoryStream ms = new MemoryStream(bytes);
				BinaryFormatter bf = new BinaryFormatter();

				ms.Position = 0;
				object obj = bf.Deserialize(ms);			
				
				_instance = (IncomeObject)obj;
				return (IncomeObject)obj;
			}
			catch
			{
				return null;
			}
		}
		
		public Stream SerializeToStream()
		{
			IFormatter formatter = new BinaryFormatter();
			Stream stream = new FileStream(@"c:\IO.bin", FileMode.Create, FileAccess.Write, FileShare.None);
			formatter.Serialize(stream, IncomeObject.Instance);
			stream.Close();

			return stream;
		}

		public IncomeObject DeserializeFromStream()
		{
			IFormatter formatter = new BinaryFormatter();
			Stream stream = new FileStream(@"c:\IO.bin", FileMode.Open, FileAccess.Read, FileShare.Read);
			IncomeObject obj = (IncomeObject) formatter.Deserialize(stream);
			stream.Close();

			IncomeObject._instance = obj;
			return obj;
        }
        #endregion

    }

    //COMMON TO ALL

    #region ACTUAL INCOME
    [Serializable]
    public class ActualIncome
    {
        string _grossRent = string.Empty;
        string _vacancyOrCreditLoss = string.Empty;
        string _vacancyOrCreditLossPerc = string.Empty;
        decimal _otherIncome = 0;
        decimal _operatingIncome = 0;

        public string GrossRent
        {
            get { return _grossRent; }
            set { _grossRent = value; }
        }

        public string VacancyOrCreditLoss
        {
            get { return _vacancyOrCreditLoss; }
            set { _vacancyOrCreditLoss = value; }
        }

        public string VacancyOrCreditLossPerc
        {
            get { return _vacancyOrCreditLossPerc; }
            set { _vacancyOrCreditLossPerc = value; }
        }

        public void CalculateValuesFromUTS(out decimal grossRent, out decimal vacancyOrCreditLoss, out decimal vacancyOrCreditLossPerc, out decimal otherIncome, out decimal operatingIncome)
        {
            grossRent = ((IncomeObject.Instance.UTS_SumTotalARLow + IncomeObject.Instance.UTS_SumTotalARHigh) / 2) * 12;
            vacancyOrCreditLossPerc = 100 - IncomeObject.Instance.UTS_WeightedAverageTotalOccupancy;
            vacancyOrCreditLoss = grossRent * vacancyOrCreditLossPerc / 100;
            otherIncome = OtherIncomeProperty;
            operatingIncome = grossRent - vacancyOrCreditLoss + otherIncome;

        }

        #region PROPERTIES
        public System.Decimal GrossRentProperty
        {
            get
            {
                decimal grossRent = 0;
                try
                {
                    if (string.Empty != GrossRent)
                        grossRent = Convert.ToDecimal(GrossRent);
                }
                catch { }

                return grossRent;
            }
        }

        public System.Decimal VacancyOrCreditLossProperty
        {
            get
            {
                decimal vacancyOrCreditLoss = 0;
                try
                {
                    vacancyOrCreditLoss = GrossRentProperty * VacancyOrCreditLossPercProperty;
                }
                catch { }

                return vacancyOrCreditLoss;
            }
        }

        public System.Decimal VacancyOrCreditLossPercProperty
        {
            get
            {
                decimal vacancyOrCreditLossPerc = 0;

                try
                {
                    if (string.Empty != _vacancyOrCreditLossPerc)
                        vacancyOrCreditLossPerc = Convert.ToDecimal(_vacancyOrCreditLossPerc) / 100;
                }
                catch { }
                return vacancyOrCreditLossPerc;
            }
        }

        public System.Decimal OtherIncomeProperty
        {
            get
            {
                _otherIncome = IncomeObject.Instance.OtherIncomeObject.TotalProperty;
                return _otherIncome;
            }
            set
            {
                _otherIncome = value;
            }
        }

        public System.Decimal OperatingIncomeProperty
        {
            get
            {
                _operatingIncome = GrossRentProperty - VacancyOrCreditLossProperty + OtherIncomeProperty;
                return _operatingIncome;
            }
            set
            {
                _operatingIncome = value;
            }
        }

        #endregion

        public ActualIncome()
        { }
    }

    #endregion

    #region PROFORMA INCOME
    [Serializable]
    public class ProformaIncome
    {
        string _grossRent = string.Empty;
        string _vacancyOrCreditLoss = string.Empty;
        string _vacancyOrCreditLossPerc = string.Empty;
        string _otherIncome = string.Empty;
        string _operatingIncome = string.Empty;

        public ProformaIncome()
        { }

        public void CalculateFromUnitTypeSummary(out decimal gsi)
        {
            gsi = IncomeObject.Instance.UTS_SumTotalMRHigh * 12;
        }

        #region PROPERTIES
        public string GrossRentProperty
        {
            get
            {
                return _grossRent;
            }
            set
            {
                _grossRent = value;
            }
        }

        public string VacancyOrCreditLossProperty
        {
            get
            {
                return _vacancyOrCreditLoss;
            }
            set
            {
                _vacancyOrCreditLoss = value;
            }
        }

        public string VacancyOrCreditLossPercProperty
        {
            get
            {
                return _vacancyOrCreditLossPerc;
            }
            set
            {
                _vacancyOrCreditLossPerc = value;
            }
        }

        public string OtherIncomeProperty
        {
            get
            {
                return _otherIncome;
            }
            set
            {
                _otherIncome = value;
            }
        }

        public string OperatingIncomeProperty
        {
            get
            {
                return _operatingIncome;
            }
            set
            {
                _operatingIncome = value;
            }
        }

        #endregion

        public decimal GrossRent
        {
            get
            {
                decimal grossRent = 0;
                try
                {
                    if (string.Empty != _grossRent)
                        grossRent = Convert.ToDecimal(_grossRent);
                }
                catch { }

                return grossRent;
            }
        }

        public decimal VacancyOrCreditloss
        {
            get
            {
                decimal vacancy = 0;
                try
                {
                    vacancy = VacancyOrCreditlossPerc * GrossRent;
                }
                catch { }

                return vacancy;
            }
        }

        public decimal VacancyOrCreditlossPerc
        {
            get
            {
                decimal vacancyPerc = 0;
                try
                {
                    if (string.Empty != _vacancyOrCreditLossPerc)
                        vacancyPerc = Convert.ToDecimal(_vacancyOrCreditLossPerc) / 100;
                }
                catch { }

                return vacancyPerc;
            }
        }

        public decimal OtherIncome
        {
            get
            {
                decimal otherIncome = 0;
                try
                {
                    if (string.Empty != _otherIncome)
                        otherIncome = Convert.ToDecimal(_otherIncome);
                }
                catch { }

                return otherIncome;
            }
        }

        public decimal OperatingIncome
        {
            get
            {
                decimal operatingIncome = 0;
                try
                {
                    operatingIncome = GrossRent - VacancyOrCreditloss + OtherIncome;
                }
                catch { }

                return operatingIncome;
            }
        }

    }
    #endregion

    //MULTIFAMILY

	#region UNIT TYPE SUMMARY
	[Serializable]
	public class UnitTypeSummary
	{
		string _nrOfUnits = string.Empty;
		string _nrOfBedrooms = string.Empty;
		string _nrOfBaths = string.Empty;
		string _floorPlan = string.Empty;
		string _occupancy = string.Empty;
		string _SF_Low = string.Empty;
		string _SF_High = string.Empty;
		string _AR_Low = string.Empty;
		string _AR_High = string.Empty;
		string _AR_USDperSqFt = string.Empty;
		string _MR_Low = string.Empty;
		string _MR_High = string.Empty;
		string _MR_USDperSqFt = string.Empty;

		#region PROPERTIES
		public string NrOfUnitsProperty
		{
			get
			{
				return _nrOfUnits;
			}
			set
			{
				_nrOfUnits = value;
			}
		}		
		public string NrOfBedroomsProperty
		{
			get
			{
				return _nrOfBedrooms;
			}
			set
			{
				_nrOfBedrooms = value;
			}
		}
		
		public string NrOfBathsProperty
		{
			get
			{
				return _nrOfBaths;
			}
			set
			{
				_nrOfBaths = value;
			}
		}
		
		public string FloorPlanProperty
		{
			get
			{
				return _floorPlan;
			}
			set
			{
				_floorPlan = value;
			}
		}
		
		public string OccupancyProperty
		{
			get
			{
				return _occupancy;
			}
			set
			{
				_occupancy = value;
			}
		}
		
		public string SF_LowProperty
		{
			get
			{
				return _SF_Low;
			}
			set
			{
				_SF_Low = value;
			}
		}
		
		public string SF_HighProperty
		{
			get
			{
				return _SF_High;
			}
			set
			{
				_SF_High = value;
			}
		}
		
		public string AR_LowProperty
		{
			get
			{
				return _AR_Low;
			}
			set
			{
				_AR_Low = value;
			}
		}
		
		public string AR_HighProperty
		{
			get
			{
				return _AR_High;
			}
			set
			{
				_AR_High = value;
			}
		}
		
		public string AR_USDperSqFtProperty
		{
			get
			{
				return _AR_USDperSqFt;
			}
			set
			{
				_AR_USDperSqFt = value;
			}
		}
		
		public string MR_LowProperty
		{
			get
			{
				return _MR_Low;
			}
			set
			{
				_MR_Low = value;
			}
		}
		
		public string MR_HighProperty
		{
			get
			{
				return _MR_High;
			}
			set
			{
				_MR_High = value;
			}
		}
		
		public string MR_USDperSqFtProperty
		{
			get
			{
				return _MR_USDperSqFt;
			}
			set
			{
				_MR_USDperSqFt = value;
			}
		}

		#endregion

		public UnitTypeSummary()
		{}

		public bool IsValid
		{
			get 
			{
				if (string.Empty != _nrOfUnits)
					 return true;
				 else
					 return false;
			}
		}		

		public decimal AvgSquareFeet
		{
			get 
			{
				decimal sqftLow = 0;
				decimal sqftHigh = 0;
				decimal retVal = 0;

				try 
                {
                    if (string.Empty != SF_LowProperty)
                        sqftLow = Convert.ToDecimal(SF_LowProperty);
                }
				catch{}

				try 
                {
                    if (string.Empty != SF_HighProperty)
                    sqftHigh = Convert.ToDecimal(SF_HighProperty);
                }
				catch{}

				try
				{
					retVal = ((sqftLow + sqftHigh) / 2);
				}
				catch 
				{}
				return retVal;
			}			
		}


		public decimal AvgRent
		{
			get 
			{
				decimal rentLow = 0;
				decimal rentHigh = 0;
				
				decimal retVal = 0;

				try 
                {
                    if (string.Empty != AR_LowProperty)
                        rentLow = Convert.ToDecimal(AR_LowProperty);
                }
				catch{}

				try 
                {
                    if (string.Empty != AR_HighProperty)
                        rentHigh = Convert.ToDecimal(AR_HighProperty);
                }
				catch{}
				
				try
				{
					retVal = ((rentLow + rentHigh) / 2);
				}
				catch 
				{}
				return retVal;
			}			
		}

		
		public int NumberOfUnits
		{
			get
			{
				int numberOfUnits = 0;
				try {numberOfUnits = Convert.ToInt32(_nrOfUnits);}
				catch {}
				return numberOfUnits;
			}
		}

		
		public decimal RelOccupancy
		{
			get 
			{
				decimal occupancy = 0;
				
				try {occupancy = Convert.ToDecimal(_occupancy);}
				catch{}
				
				return occupancy * NumberOfUnits;
			}
		}

		
		public decimal SqFtLowPerItem
		{
			get 
			{
				decimal sqftLow = 0;
				try {sqftLow = Convert.ToDecimal(_SF_Low);}
				catch{}

				return sqftLow * NumberOfUnits;
			}
		}

		
		public decimal SqFtHighPerItem
		{
			get 
			{
				decimal sqftHigh = 0;
				try {sqftHigh = Convert.ToDecimal(_SF_High);}
				catch{}

				return sqftHigh * NumberOfUnits;
			}
		}

		
		public decimal ARLowPerItem
		{
			get 
			{
				decimal arLow = 0;
				try {arLow = Convert.ToDecimal(_AR_Low);}
				catch{}

				return arLow * NumberOfUnits;
			}
		}

		
		public decimal ARHighPerItem
		{
			get 
			{
				decimal arHigh = 0;
				try {arHigh = Convert.ToDecimal(_AR_High);}
				catch{}

				return arHigh * NumberOfUnits;
			}
		}		

		
		public decimal MRLowPerItem
		{
			get 
			{
				decimal mrLow = 0;
				try {mrLow = Convert.ToDecimal(_MR_Low);}
				catch{}

				return mrLow * NumberOfUnits;
			}
		}

		
		public decimal MRHighPerItem
		{
			get 
			{
				decimal mrHigh = 0;
				try {mrHigh = Convert.ToDecimal(_MR_High);}
				catch{}

				return mrHigh * NumberOfUnits;
			}
		}		
		
		
		public decimal ActualRentPerItem
		{
			get 
			{
				decimal retVal = 0;
				
				try 
                {
                    if (0 != AvgSquareFeet)
                        retVal = AvgRent / AvgSquareFeet; 
                }
				catch {}
				return retVal;
			}
		}

		
		public decimal HighMarketRent
		{
			get 
			{
				decimal rentHigh = 0;

				try 
                {
                    if (string.Empty != MR_HighProperty)
                        rentHigh = Convert.ToDecimal(MR_HighProperty);
                }
				catch{}
								
				return rentHigh;
			}			
		}
		
		
		public decimal MarketRentPerItem
		{
			get 
			{												
				decimal retVal = 0;								
				
				try 
                { 
                    if (0 != AvgSquareFeet)
                        retVal = HighMarketRent / AvgSquareFeet; 
                }
				catch 
				{}
				return retVal;
			}
		}
	}

	#endregion
	
	#region RENT ROLL
	[Serializable]
	public class RentRoll
	{
		string _unitNr = string.Empty;
		string _tennantName = string.Empty;
		string _bedBath = string.Empty;
		string _sqft = string.Empty;
		string _secDep = string.Empty;
		string _tennantRent = string.Empty;
		string _sec8Rent = string.Empty;
		decimal _totalRent = 0;
		string _marketRent = string.Empty;
		string _occupDate = string.Empty;
		string _leaseTerm = string.Empty;
		string _comment = string.Empty;

		public RentRoll()
		{}

		#region PROPERTIES
		public string UnitNrProperty
		{
			get
			{
				return _unitNr;
			}
			set
			{
				_unitNr = value;
			}
		}
		
		public System.String TennantNameProperty
		{
			get
			{
				return _tennantName;
			}
			set
			{
				_tennantName = value;
			}
		}
		
		public System.String BedBathProperty
		{
			get
			{
				return _bedBath;
			}
			set
			{
				_bedBath = value;
			}
		}
		
		public string SqftProperty
		{
			get
			{
				return _sqft;
			}
			set
			{
				_sqft = value;
			}
		}
		
		public string SecDepProperty
		{
			get
			{
				return _secDep;
			}
			set
			{
				_secDep = value;
			}
		}
		
		public string TennantRentProperty
		{
			get
			{
				return _tennantRent;
			}
			set
			{
				_tennantRent = value;
			}
		}
		
		public string Sec8RentProperty
		{
			get
			{
				return _sec8Rent;
			}
			set
			{
				_sec8Rent = value;
			}
		}
		
		public decimal TotalRentProperty
		{
			get
			{				
				decimal tennRent = 0;
				decimal sec8Rent = 0;

				try 
                {
                    if (string.Empty != _tennantRent)
                        tennRent = Convert.ToDecimal(_tennantRent);
                }
				catch {}

				try
                {
                    if (string.Empty != _sec8Rent)
                        sec8Rent = Convert.ToDecimal(_sec8Rent);
                }
				catch{}
				
				_totalRent = tennRent + sec8Rent;
				return _totalRent;
			}
			set
			{
				_totalRent = value;
			}
		}
		
		public string MarketRentProperty
		{
			get
			{
				return _marketRent;
			}
			set
			{
				_marketRent = value;
			}
		}
		
		public string OccupDateProperty
		{
			get
			{
				return _occupDate;
			}
			set
			{
				_occupDate = value;
			}
		}
		
		public System.String LeaseTermProperty
		{
			get
			{
				return _leaseTerm;
			}
			set
			{
				_leaseTerm = value;
			}
		}
		
		public System.String CommentProperty
		{
			get
			{
				return _comment;
			}
			set
			{
				_comment = value;
			}
		}

		#endregion		

		public bool IsValid
		{
			get { return (string.Empty != _unitNr) ? true : false; }
		}

		public decimal SqFtPerItem
		{
			get 
			{
				decimal res = 0;
				try 
                {
                    if (string.Empty != _sqft)
                        res = Convert.ToDecimal(_sqft);
                }
				catch {}
				return res;
			}
		}

		public decimal SecDepPerItem
		{
			get 
			{
				decimal res = 0;
				try 
                {
                    if (string.Empty != _secDep)
                        res = Convert.ToDecimal(_secDep);
                }
				catch {}
				return res;
			}
		}

		public decimal TennantRentPerItem
		{
			get 
			{
				decimal res = 0;
				try 
                {
                    if (string.Empty != _tennantRent)
                        res = Convert.ToDecimal(_tennantRent);
                }
				catch {}
				return res;
			}
		}

		public decimal Sec8PerItem
		{
			get 
			{
				decimal res = 0;
				try 
                {
                    if (string.Empty != _sec8Rent)
                        res = Convert.ToDecimal(_sec8Rent);
                }
				catch {}
				return res;
			}
		}

		public decimal MarketRentPerItem
		{
			get 
			{
				decimal res = 0;
				try 
                {
                    if (string.Empty != _marketRent)
                        res = Convert.ToDecimal(_marketRent);
                }
				catch {}
				return res;
			}
		}
		 
	}
	#endregion

	#region OTHER INCOME
	[Serializable]
	public class OtherIncome
	{
		string _parking = string.Empty;
		string _laundry = string.Empty;
		string _vending = string.Empty;
		string _other = string.Empty;
		decimal _total = 0;

		decimal _parkingValue = 0;
		decimal _laundryValue = 0;
		decimal _vendingValue = 0;
		decimal _otherValue = 0;

		public OtherIncome()
		{
		}


		public decimal ParkingValue
		{
			get 
			{
				_parkingValue = 0;
				try 
				{
                    if (string.Empty != _parking)
					    _parkingValue = Convert.ToDecimal(_parking);
				}
				catch {_parkingValue = 0;}

				return _parkingValue;
			}
		}

		public decimal LaundryValue
		{
			get 
			{
				_laundryValue = 0;

				try 
                {
                    if (string.Empty != _laundry)
                        _laundryValue = Convert.ToDecimal(_laundry); 
                }
				catch {_laundryValue = 0;}

				return _laundryValue;
			}
		}

		public decimal VendingValue
		{
			get 
			{
				_vendingValue = 0;

				try 
                {
                    if (string.Empty != _vending)
                        _vendingValue = Convert.ToDecimal(_vending);
                }
				catch {_vendingValue = 0;}

				return _vendingValue;
			}
		}

		public decimal OtherValue
		{
			get 
			{
				_otherValue = 0;

				try 
                {
                    if (string.Empty != _other)
                        _otherValue = Convert.ToDecimal(_other);
                }
				catch {_otherValue = 0;}

				return _otherValue;
			}
		}


		#region PROPERTIES
		public string ParkingProperty
		{
			get
			{
				return _parking;
			}
			set
			{
				_parking = value;
			}
		}
		
		public string LaundryProperty
		{
			get
			{
				return _laundry;
			}
			set
			{
				_laundry = value;
			}
		}
		
		public string VendingProperty
		{
			get
			{
				return _vending;
			}
			set
			{
				_vending = value;
			}
		}
		
		public string OtherProperty
		{
			get
			{
				return _other;
			}
			set
			{
				_other = value;
			}
		}
		
		public System.Decimal TotalProperty
		{
			get
			{
				_total = 0;
				decimal parking = 0;
				decimal laundry = 0;
				decimal vending = 0;
				decimal other = 0;

				try 
                {
                    if (string.Empty != _parking)
                        parking = Convert.ToDecimal(_parking);
                }
				catch{}
				try 
                {
                    if (string.Empty != _laundry)
                        laundry = Convert.ToDecimal(_laundry);
                }
				catch{}
				try 
                {
                    if (string.Empty != _vending)
                        vending = Convert.ToDecimal(_vending);
                }
				catch{}
				try 
                {
                    if (string.Empty != _other)
                        other = Convert.ToDecimal(_other);
                }
				catch{}

				_total = parking + laundry + vending + other;
				return _total;
			}
			set
			{
				_total = value;
			}
		}

		#endregion	
	}
	#endregion

    //RETAIL - OFFICE
    #region RETAIL OFFICE SUMMARY
    [Serializable]
    public class RetailOfficeSummary
    {
        string _space = string.Empty;
        string _occupied = string.Empty;
        string _tennant = string.Empty;
        string _floor = string.Empty;
        string _sqFt = string.Empty;
        string _AR_usdPerMonth = string.Empty;
        string _AR_usdPerYear = string.Empty;
        string _AR_usdPerSqFtPerMonth = string.Empty;
        string _AR_usdPerSqFtPerYear = string.Empty;
        string _PR_usdPerMonth = string.Empty;
        string _PR_usdPerYear = string.Empty;
        string _PR_usdPerSqFtPerMonth = string.Empty;
        string _PR_usdPerSqFtPerYear = string.Empty;
        Space _spaceDetails = new Space();

        public RetailOfficeSummary()
        { }

        public bool IsValid
        {
            get { return (string.Empty != _space) ? true : false; }
        }

        public string Space
        {
            get { return _space; }
            set { _space = value; }
        }

        public Space SpaceDetails
        {
            get { return _spaceDetails; }
            set { _spaceDetails = value; }
        }

        public string Occupied
        {
            get { return _occupied; }
            set { _occupied = value; }
        }


        public string Tennant
        {
            get { return _tennant; }
            set { _tennant = value; }
        }


        public string Floor
        {
            get { return _floor; }
            set { _floor = value; }
        }


        public string SqFt
        {
            get { return _sqFt; }
            set { _sqFt = value; }
        }


        public string AR_usdPerMonth
        {
            get { return _AR_usdPerMonth; }
            set 
            { 
                _AR_usdPerMonth = value;

                decimal val = 0;

                // $/Year
                try
                {
                    if (string.Empty != AR_usdPerMonth)
                        val = Convert.ToDecimal(AR_usdPerMonth);

                    val = val * 12;
                    _AR_usdPerYear = val.ToString();
                }
                catch { }

                // $/Month/SqFt
                try
                {
                    if (string.Empty != SqFt)
                        val = Convert.ToDecimal(SqFt);

                    if (0 != val)
                    {
                        decimal val1 = P_AR_UsdPerMonthPerItem / val;
                        _AR_usdPerSqFtPerMonth = val1.ToString();                        
                    }                    
                }
                catch { }

                // $/Year/SqFt
                try
                {
                    if (string.Empty != SqFt)
                        val = Convert.ToDecimal(SqFt);

                    if (0 != val)
                    {
                        decimal val1 = P_AR_UsdPerYearPerItem / val;
                        _AR_usdPerSqFtPerYear = val1.ToString();
                    }
                }
                catch { }
                

            }
        }


        public string AR_usdPerYear
        {
            get { return _AR_usdPerYear; }
            set 
            { 
                _AR_usdPerYear = value;

                decimal val = 0;
                // $/Month
                try
                {
                    if (string.Empty != AR_usdPerYear)
                        val = Convert.ToDecimal(AR_usdPerYear);

                    val = val / 12;
                    _AR_usdPerMonth = val.ToString();
                }
                catch { }

                // $/Month/SqFt
                try
                {
                    if (string.Empty != SqFt)
                        val = Convert.ToDecimal(SqFt);

                    if (0 != val)
                    {
                        decimal val1 = P_AR_UsdPerMonthPerItem / val;
                        _AR_usdPerSqFtPerMonth = val1.ToString();                        
                    }                    
                }
                catch { }

                // $/Year/SqFt
                try
                {
                    if (string.Empty != SqFt)
                        val = Convert.ToDecimal(SqFt);

                    if (0 != val)
                    {
                        decimal val1 = P_AR_UsdPerYearPerItem / val;
                        _AR_usdPerSqFtPerYear = val1.ToString();                     
                    }                    
                }
                catch { }
            }
        }


        public string AR_usdPerSqFtPerMonth
        {
            get { return _AR_usdPerSqFtPerMonth; }
            set 
            { 
                _AR_usdPerSqFtPerMonth = value;
                decimal val = 0;
                decimal sqft = 0;
                
                // $/Month
                try
                {
                    if (string.Empty != SqFt)
                        sqft = Convert.ToDecimal(SqFt);

                    if (string.Empty != _AR_usdPerSqFtPerMonth)
                        val = Convert.ToDecimal(_AR_usdPerSqFtPerMonth);

                    val = val * sqft;
                    _AR_usdPerMonth = val.ToString();
                }
                catch { }

                // $/Year
                try
                {
                    if (string.Empty != _AR_usdPerMonth)
                        val = Convert.ToDecimal(_AR_usdPerMonth);

                    val = val * 12;
                    _AR_usdPerYear = val.ToString();
                }
                catch { }                

                // $/Year/SqFt
                try
                {
                    if (string.Empty != SqFt)
                        val = Convert.ToDecimal(SqFt);

                    if (0 != val)
                    {
                        decimal val1 = P_AR_UsdPerYearPerItem / val;
                        _AR_usdPerSqFtPerYear = val1.ToString();                        
                    }                    
                }
                catch { }
            }
        }


        public string AR_usdPerSqFtPerYear
        {
            get { return _AR_usdPerSqFtPerYear; }
            set 
            { 
                _AR_usdPerSqFtPerYear = value;

                decimal sqft = 0;
                decimal val = 0;

                // $/Month
                try
                {
                    if (string.Empty != SqFt)
                        sqft = Convert.ToDecimal(SqFt);

                    if (string.Empty != _AR_usdPerSqFtPerYear)
                        val = Convert.ToDecimal(_AR_usdPerSqFtPerYear);

                    val = (val / 12) / (sqft);
                    _AR_usdPerMonth = val.ToString();
                }
                catch { }

                // $/Year
                try
                {
                    if (string.Empty != _AR_usdPerMonth)
                        val = Convert.ToDecimal(_AR_usdPerMonth);

                    val = val * 12;
                    _AR_usdPerYear = val.ToString();
                }
                catch { }

                // $/Month/SqFt
                try
                {                    
                    
                    decimal val1 = P_AR_UsdPerSqFtPerYearPerItem / 12;
                    _AR_usdPerSqFtPerMonth = val1.ToString();                       
                                        
                }
                catch { }
            }
        }


        public string PR_usdPerMonth
        {
            get { return _PR_usdPerMonth; }
            set
            {
                _PR_usdPerMonth = value;

                decimal val = 0;

                // $/Year
                try
                {
                    if (string.Empty != PR_usdPerMonth)
                        val = Convert.ToDecimal(PR_usdPerMonth);

                    val = val * 12;
                    _PR_usdPerYear = val.ToString();
                }
                catch { }

                // $/Month/SqFt
                try
                {
                    if (string.Empty != SqFt)
                        val = Convert.ToDecimal(SqFt);

                    if (0 != val)
                    {
                        decimal val1 = P_PR_UsdPerMonthPerItem / val;
                        _PR_usdPerSqFtPerMonth = val1.ToString();
                    }
                }
                catch { }

                // $/Year/SqFt
                try
                {
                    if (string.Empty != SqFt)
                        val = Convert.ToDecimal(SqFt);

                    if (0 != val)
                    {
                        decimal val1 = P_PR_UsdPerYearPerItem / val;
                        _PR_usdPerSqFtPerYear = val1.ToString();
                    }
                }
                catch { }


            }
        }


        public string PR_usdPerYear
        {
            get { return _PR_usdPerYear; }
            set
            {
                _PR_usdPerYear = value;

                decimal val = 0;
                // $/Month
                try
                {
                    if (string.Empty != PR_usdPerYear)
                        val = Convert.ToDecimal(PR_usdPerYear);

                    val = val / 12;
                    _PR_usdPerMonth = val.ToString();
                }
                catch { }

                // $/Month/SqFt
                try
                {
                    if (string.Empty != SqFt)
                        val = Convert.ToDecimal(SqFt);

                    if (0 != val)
                    {
                        decimal val1 = P_PR_UsdPerMonthPerItem / val;
                        _PR_usdPerSqFtPerMonth = val1.ToString();
                    }
                }
                catch { }

                // $/Year/SqFt
                try
                {
                    if (string.Empty != SqFt)
                        val = Convert.ToDecimal(SqFt);

                    if (0 != val)
                    {
                        decimal val1 = P_PR_UsdPerYearPerItem / val;
                        _PR_usdPerSqFtPerYear = val1.ToString();
                    }
                }
                catch { }
            }
        }


        public string PR_usdPerSqFtPerMonth
        {
            get { return _PR_usdPerSqFtPerMonth; }
            set
            {
                _PR_usdPerSqFtPerMonth = value;
                decimal val = 0;
                decimal sqft = 0;

                // $/Month
                try
                {
                    if (string.Empty != SqFt)
                        sqft = Convert.ToDecimal(SqFt);

                    if (string.Empty != _PR_usdPerSqFtPerMonth)
                        val = Convert.ToDecimal(_PR_usdPerSqFtPerMonth);

                    val = val * sqft;
                    _PR_usdPerMonth = val.ToString();
                }
                catch { }

                // $/Year
                try
                {
                    if (string.Empty != _PR_usdPerMonth)
                        val = Convert.ToDecimal(_PR_usdPerMonth);

                    val = val * 12;
                    _PR_usdPerYear = val.ToString();
                }
                catch { }

                // $/Year/SqFt
                try
                {
                    if (string.Empty != SqFt)
                        val = Convert.ToDecimal(SqFt);

                    if (0 != val)
                    {
                        decimal val1 = P_PR_UsdPerYearPerItem / val;
                        _PR_usdPerSqFtPerYear = val1.ToString();
                    }
                }
                catch { }
            }
        }


        public string PR_usdPerSqFtPerYear
        {
            get { return _PR_usdPerSqFtPerYear; }
            set
            {
                _PR_usdPerSqFtPerYear = value;

                decimal sqft = 0;
                decimal val = 0;

                // $/Month
                try
                {
                    if (string.Empty != SqFt)
                        sqft = Convert.ToDecimal(SqFt);

                    if (string.Empty != _PR_usdPerSqFtPerYear)
                        val = Convert.ToDecimal(_PR_usdPerSqFtPerYear);

                    val = (val / 12) / (sqft);
                    _PR_usdPerMonth = val.ToString();
                }
                catch { }

                // $/Year
                try
                {
                    if (string.Empty != _PR_usdPerMonth)
                        val = Convert.ToDecimal(_PR_usdPerMonth);

                    val = val * 12;
                    _PR_usdPerYear = val.ToString();
                }
                catch { }

                // $/Month/SqFt
                try
                {

                    decimal val1 = P_PR_UsdPerSqFtPerYearPerItem / 12;
                    _PR_usdPerSqFtPerMonth = val1.ToString();

                }
                catch { }
            }
        }

       

        public decimal P_AR_UsdPerYearPerItem
        {
            get { return Util.GetDecimalFromString(AR_usdPerYear); }
        }

        public decimal P_AR_UsdPerMonthPerItem
        {
            get { return Util.GetDecimalFromString(AR_usdPerMonth); }            
        }

        public decimal P_AR_UsdPerSqFtPerMonthPerItem
        {
            get { return Util.GetDecimalFromString(AR_usdPerSqFtPerMonth); }            
        }

        public decimal P_AR_UsdPerSqFtPerYearPerItem
        {
            get { return Util.GetDecimalFromString(AR_usdPerSqFtPerYear); }
        }

        public decimal P_PR_UsdPerYearPerItem
        {
            get { return Util.GetDecimalFromString(PR_usdPerYear); }
        }

        public decimal P_PR_UsdPerMonthPerItem
        {
            get { return Util.GetDecimalFromString(PR_usdPerMonth); }
        }

        public decimal P_PR_UsdPerSqFtPerMonthPerItem
        {
            get { return Util.GetDecimalFromString(PR_usdPerSqFtPerMonth); }
        }

        public decimal P_PR_UsdPerSqFtPerYearPerItem
        {
            get { return Util.GetDecimalFromString(PR_usdPerSqFtPerYear); }
        }

        

    }
    #endregion

    #region SPACE
    [Serializable]
    public class Space
    {
        string _space = string.Empty;
        string _comments = string.Empty;
        string _leaseType = string.Empty;
        string _leaseLength = string.Empty;
        string _leaseStartDate = string.Empty;
        string _leaseEndDate = string.Empty;
        string _leaseRenewalOption = string.Empty;
        string _leaseComments = string.Empty;
        string _financialTIAllowance = string.Empty;
        string _financialExpensesStop = string.Empty;
        string _financialNNNCharges = string.Empty;
        string _financialCAMCharges = string.Empty;
        string _financialComments = string.Empty;

        public Space()
        { }

        public string P_Space
        {
            get { return _space; }
            set { _space = value; }
        }

        public string Comments
        {
            get { return _comments; }
            set { _comments = value; }
        }

        public string LeaseType
        {
            get { return _leaseType; }
            set { _leaseType = value; }
        }

        public string LeaseLength
        {
            get { return _leaseLength; }
            set { _leaseLength = value; }
        }

        public string LeaseStartDate
        {
            get { return _leaseStartDate; }
            set { _leaseStartDate = value; }
        }

        public string LeaseEndDate
        {
            get { return _leaseEndDate; }
            set { _leaseEndDate = value; }
        }

        public string LeaseRenewalOption
        {
            get { return _leaseRenewalOption; }
            set { _leaseRenewalOption = value; }
        }

        public string LeaseComments
        {
            get { return _leaseComments; }
            set { _leaseComments = value; }
        }

        public string FinancialTIAllowance
        {
            get { return _financialTIAllowance; }
            set { _financialTIAllowance = value; }
        }

        public string FinancialExpensesStop
        {
            get { return _financialExpensesStop; }
            set { _financialExpensesStop = value; }
        }

        public string FinancialNNNCharges
        {
            get { return _financialNNNCharges; }
            set { _financialNNNCharges = value; }
        }

        public string FinancialCAMCharges
        {
            get { return _financialCAMCharges; }
            set { _financialCAMCharges = value; }
        }

        public string FinancialComments
        {
            get { return _financialComments; }
            set { _financialComments = value; }
        }
    }
    #endregion

    //SELF STORAGE
    #region SELF STORAGE SUMMARY
    [Serializable]
    public class SelfStorageSummary
    {
        string _units = string.Empty;
        string _size = string.Empty;
        string _floor = string.Empty;
        string _occupancy = string.Empty;
        string _sqFt = string.Empty;
        string _AR_Low = string.Empty;
        string _AR_High = string.Empty;
        string _AR_UsdPerSqFt = string.Empty;
        string _MR_Low = string.Empty;
        string _MR_High = string.Empty;
        string _MR_UsdPerSqFt = string.Empty;
        string _comment = string.Empty;

        public SelfStorageSummary()
        { }

        public bool IsValid
        {
            get { return (string.Empty != _units) ? true : false; }
        }

        public string Units
        {
            get { return _units; }
            set { _units = value; }
        }

        public string Size 
        {
            get { return _size; }
            set { _size = value; }
        }

        public string Floor
        {
            get { return _floor; }
            set { _floor = value; }
        }

        public string Occupancy 
        {
            get { return _occupancy; }
            set { _occupancy = value; }
        }

        public string SqFt 
        {
            get { return _sqFt; }
            set { _sqFt = value; }
        }

        public string AR_Low 
        {
            get { return _AR_Low; }
            set { _AR_Low = value; }
        }

        public string AR_High 
        {
            get { return _AR_High; }
            set { _AR_High = value; }
        }

        public string AR_UsdPerSqFt 
        {
            get { return _AR_UsdPerSqFt; }
            set { _AR_UsdPerSqFt = value; }
        }

        public string MR_Low 
        {
            get { return _MR_Low; }
            set { _MR_Low = value; }
        }

        public string MR_High 
        {
            get { return _MR_High; }
            set { _MR_High = value; }
        }

        public string MR_UsdPerSqFt 
        {
            get { return _MR_UsdPerSqFt; }
            set { _MR_UsdPerSqFt = value; }
        }

        public string Comment 
        {
            get { return _comment; }
            set { _comment = value; }
        }        
               
        public int NumberOfUnits
        {
            get
            {
                int numberOfUnits = 0;
                try { numberOfUnits = Convert.ToInt32(_units); }
                catch { }
                return numberOfUnits;
            }
        }

        public decimal RelOccupancy
        {
            get
            {
                decimal occupancy = 0;

                try { occupancy = Convert.ToDecimal(_occupancy); }
                catch { }

                return occupancy * NumberOfUnits;
            }
        }

        public decimal AR_SqFtLowPerItem
        {
            get
            {
                decimal sqftLow = 0;
                try { sqftLow = Convert.ToDecimal(_AR_Low); }
                catch { }

                return sqftLow * NumberOfUnits;
            }
        }

        public decimal AR_SqFtHighPerItem
        {
            get
            {
                decimal sqftHigh = 0;
                try { sqftHigh = Convert.ToDecimal(_AR_High); }
                catch { }

                return sqftHigh * NumberOfUnits;
            }
        }

        public decimal ARLowPerItem
        {
            get
            {
                decimal arLow = 0;
                try { arLow = Convert.ToDecimal(_AR_Low); }
                catch { }

                return arLow;
            }
        }

        public decimal ARHighPerItem
        {
            get
            {
                decimal arHigh = 0;
                try { arHigh = Convert.ToDecimal(_AR_High); }
                catch { }

                return arHigh;
            }
        }

        public decimal MRLowPerItem
        {
            get
            {
                decimal mrLow = 0;
                try { mrLow = Convert.ToDecimal(_MR_Low); }
                catch { }

                return mrLow;
            }
        }

        public decimal MRHighPerItem
        {
            get
            {
                decimal mrHigh = 0;
                try { mrHigh = Convert.ToDecimal(_MR_High); }
                catch { }

                return mrHigh;
            }
        }

        public decimal Sqft
        {
            get { return Util.GetDecimalFromString(_sqFt);}
        }
        
        public decimal ARPerItem
        {
            get
            {
                decimal retVal = 0;

                try
                {
                    if (0 != Sqft)
                        retVal = (ARLowPerItem + ARHighPerItem) / (Sqft * 2);
                }
                catch { }
                return retVal;
            }
        }

        public decimal MRPerItem
        {
            get
            {
                decimal retVal = 0;

                try
                {
                    if (0 != Sqft)
                        retVal = (MRLowPerItem + MRHighPerItem) / (Sqft * 2);
                }
                catch { }
                return retVal;
            }
        }        
    }

    #endregion
}
