using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization.Formatters;
using System.Runtime.Serialization;
using System.IO;

namespace ProfitGrabber.Commercial.Objects
{
	/// <summary>
	/// Summary description for BuildingInfoObject.
	/// </summary>
	[Serializable]
	public class BuildingInfoObject
	{
		static BuildingInfoObject _instance = new BuildingInfoObject();

		BuildingInfoObject()
		{			
		}

		static BuildingInfoObject()
		{			
		}

		public static BuildingInfoObject Instance
		{
			get { return _instance; }
		}

		#region DATA
		string _propertyName = string.Empty;
		string _propertyType = string.Empty;
		string _class = string.Empty;
		string _apn = string.Empty;
		decimal _propertyValue = 0;
		string _yearBuilt = string.Empty;
		string _rehabYear = string.Empty;

		string _nrOfUnits = string.Empty;
		string _nrOfFloors = string.Empty;
		string _nrOfStructures = string.Empty;
		int _totalSqFt = 0;
		int _netRentableSqFt = 0;
		int _lotAcres = 0;
		int _lotSqFt = 0;
		string _roofAge = string.Empty;
		string _roofType = string.Empty;
		
		string _plumbingType = string.Empty;
		string _plumbingAge = string.Empty;
		string _electricalAge = string.Empty;
		string _sidingType = string.Empty;
		string _windowType = string.Empty;
		string _windowAge = string.Empty;

		string _parkingRatio = string.Empty;
		decimal _parkingLotSpaces = 0;
		decimal _parkingCoveredSpaces = 0;
		decimal _parkingGarageSpaces = 0;
		decimal _parkingTotalSpaces = 0;
		decimal _parkingLotFee = 0;
		decimal _parkingCoveredFee = 0;
		decimal _parkingGarageFee = 0;
		decimal _parkingTotalFee = 0;
		string _parkingComments = string.Empty;

		string _landLease = string.Empty;
		string _landLeaseOption = string.Empty;
		string _lessor = string.Empty;
		string _startDate = string.Empty;
		string _endDate = string.Empty;
		decimal _rent = 0;
		string _landLeaseComments = string.Empty;
		string _recentImprovements = string.Empty;
		string _improvementsNeeded = string.Empty;
		string _propertyDescription = string.Empty;
		string _locationDescription = string.Empty;
		string _amenities = string.Empty;
		string _landscaping = string.Empty;
		#endregion

		public int NumberOfUnitsValue
		{
			get
			{
				int val = 0;
				try 
				{
					val = Convert.ToInt32(_nrOfUnits);
				}
				catch 
				{val = 0;}
				return val;
			}
		}

		#region PROPERTIES
		public System.String PropertyName
		{
			get
			{
				return _propertyName;
			}
			set
			{
				_propertyName = value;
			}
		}

		
		public System.String PropertyType
		{
			get
			{
				return _propertyType;
			}
			set
			{
				_propertyType = value;
			}
		}

		
		public System.String Class
		{
			get
			{
				return _class;
			}
			set
			{
				_class = value;
			}
		}

		
		public System.String Apn
		{
			get
			{
				return _apn;
			}
			set
			{
				_apn = value;
			}
		}
		
		public System.Decimal PropertyValue
		{
			get
			{
				return _propertyValue;
			}
			set
			{
				_propertyValue = value;
			}
		}
		
		public string YearBuilt
		{
			get
			{
				return _yearBuilt;
			}
			set
			{
				_yearBuilt = value;
			}
		}

		
		public string RehabYear
		{
			get
			{
				return _rehabYear;
			}
			set
			{
				_rehabYear = value;
			}
		}

		public string NrOfUnits
		{
			get
			{
				return _nrOfUnits;
			}
			set
			{
				_nrOfUnits = value;
			}
		}

		public string NrOfFloors
		{
			get
			{
				return _nrOfFloors;
			}
			set
			{
				_nrOfFloors = value;
			}
		}

		public string NrOfStructures
		{
			get
			{
				return _nrOfStructures;
			}
			set
			{
				_nrOfStructures = value;
			}
		}

		public System.Int32 TotalSqFt
		{
			get
			{
				return _totalSqFt;
			}
			set
			{
				_totalSqFt = value;
			}
		}

		public System.Int32 NetRentableSqFt
		{
			get
			{
				return _netRentableSqFt;
			}
			set
			{
				_netRentableSqFt = value;
			}
		}

		public System.Int32 LotAcres
		{
			get
			{
				return _lotAcres;
			}
			set
			{
				_lotAcres = value;
			}
		}

		public int LotSqFt
		{
			get {return _lotSqFt;}
			set {_lotSqFt = value;}
		}

		public string RoofAge
		{
			get
			{
				return _roofAge;
			}
			set
			{
				_roofAge = value;
			}
		}

		public System.String RoofType
		{
			get
			{
				return _roofType;
			}
			set
			{
				_roofType = value;
			}
		}

		public System.String PlumbingType
		{
			get
			{
				return _plumbingType;
			}
			set
			{
				_plumbingType = value;
			}
		}

		public string PlumbingAge
		{
			get
			{
				return _plumbingAge;
			}
			set
			{
				_plumbingAge = value;
			}
		}

		public string ElectricalAge
		{
			get
			{
				return _electricalAge;
			}
			set
			{
				_electricalAge = value;
			}
		}

		public System.String SidingType
		{
			get
			{
				return _sidingType;
			}
			set
			{
				_sidingType = value;
			}
		}

		public System.String WindowType
		{
			get
			{
				return _windowType;
			}
			set
			{
				_windowType = value;
			}
		}

		public string WindowAge
		{
			get
			{
				return _windowAge;
			}
			set
			{
				_windowAge = value;
			}
		}

		public string ParkingRatio
		{
			get
			{
				int nrOfUnits = 0;
				try 
				{
					if (string.Empty != NrOfUnits)
                        nrOfUnits = Convert.ToInt32(NrOfUnits);

                    if (0 == nrOfUnits)
                        return "NaN";

					decimal parkingRatio = ParkingTotalSpaces / nrOfUnits;
					return parkingRatio.ToString("0.00");
				}
				catch {return "NaN";}								
			}
			set
			{
				_parkingRatio = value;
			}
		}

		public System.Decimal ParkingLotSpaces
		{
			get
			{
				return _parkingLotSpaces;
			}
			set
			{
				_parkingLotSpaces = value;
			}
		}

		public System.Decimal ParkingCoveredSpaces
		{
			get
			{
				return _parkingCoveredSpaces;
			}
			set
			{
				_parkingCoveredSpaces = value;
			}
		}

		public System.Decimal ParkingGarageSpaces
		{
			get
			{
				return _parkingGarageSpaces;
			}
			set
			{
				_parkingGarageSpaces = value;
			}
		}

		public System.Decimal ParkingTotalSpaces
		{
			get
			{
				_parkingTotalSpaces = _parkingLotSpaces + _parkingCoveredSpaces + _parkingGarageSpaces;
				return _parkingTotalSpaces;
			}
			set
			{
				_parkingTotalSpaces = value;
			}
		}

		public System.Decimal ParkingLotFee
		{
			get
			{
				return _parkingLotFee;
			}
			set
			{
				_parkingLotFee = value;
			}
		}

		public System.Decimal ParkingCoveredFee
		{
			get
			{
				return _parkingCoveredFee;
			}
			set
			{
				_parkingCoveredFee = value;
			}
		}

		public System.Decimal ParkingGarageFee
		{
			get
			{
				return _parkingGarageFee;
			}
			set
			{
				_parkingGarageFee = value;
			}
		}

		public System.Decimal ParkingTotalFee
		{
			get
			{
				_parkingTotalFee = (_parkingLotFee * _parkingLotSpaces) + (_parkingCoveredFee * _parkingCoveredSpaces) + (_parkingGarageFee * ParkingGarageSpaces);
				return _parkingTotalFee;
			}
			set
			{
				_parkingTotalFee = value;
			}
		}

		public System.String ParkingComments
		{
			get
			{
				return _parkingComments;
			}
			set
			{
				_parkingComments = value;
			}
		}

		public System.String LandLease
		{
			get
			{
				return _landLease;
			}
			set
			{
				_landLease = value;
			}
		}

		public System.String LandLeaseOption
		{
			get
			{
				return _landLeaseOption;
			}
			set
			{
				_landLeaseOption = value;
			}
		}

		public System.String Lessor
		{
			get
			{
				return _lessor;
			}
			set
			{
				_lessor = value;
			}
		}

		public System.String StartDate
		{
			get
			{
				return _startDate;
			}
			set
			{
				_startDate = value;
			}
		}

		public System.String EndDate
		{
			get
			{
				return _endDate;
			}
			set
			{
				_endDate = value;
			}
		}

		public System.Decimal Rent
		{
			get
			{
				return _rent;
			}
			set
			{
				_rent = value;
			}
		}

		public System.String LandLeaseComments
		{
			get
			{
				return _landLeaseComments;
			}
			set
			{
				_landLeaseComments = value;
			}
		}

		public System.String RecentImprovements
		{
			get
			{
				return _recentImprovements;
			}
			set
			{
				_recentImprovements = value;
			}
		}

		public string ImprovementsNeeded 
		{
			get {return _improvementsNeeded;}
			set {_improvementsNeeded = value;}
		}

		public System.String PropertyDescription
		{
			get
			{
				return _propertyDescription;
			}
			set
			{
				_propertyDescription = value;
			}
		}

		public System.String LocationDescription
		{
			get
			{
				return _locationDescription;
			}
			set
			{
				_locationDescription = value;
			}
		}

		public System.String Amenities
		{
			get
			{
				return _amenities;
			}
			set
			{
				_amenities = value;
			}
		}

		public System.String Landscaping
		{
			get
			{
				return _landscaping;
			}
			set
			{
				_landscaping = value;
			}
		}
		#endregion

        public int GetIncomeControlIndex()
        {
            if (null == _propertyType || string.Empty == _propertyType)
                return -1;

            switch (_propertyType)
            {
                case "Multifamily":
                    return 0;
                case "Retail":
                    return 1;
                case "Office":
                    return 2;
                case "Selfstorage":
                    return 3;
                default:
                    return -1;
            }
        }

		public void ResetObject()
		{
			_instance = new BuildingInfoObject();
		}

		public byte[] SerializeToByteArray()
		{		
			try
			{
				BinaryFormatter bf = new BinaryFormatter();
				MemoryStream ms = new MemoryStream();
				bf.Serialize(ms, BuildingInfoObject.Instance);				
				return ms.ToArray();
			}
			catch
			{
				return null;
			}
		}

		public BuildingInfoObject DeserializeFromByteArray(byte[] bytes)
		{
			try 
			{				
				MemoryStream ms = new MemoryStream(bytes);
				BinaryFormatter bf = new BinaryFormatter();

				ms.Position = 0;
				object obj = bf.Deserialize(ms);			
				
				_instance = (BuildingInfoObject)obj;
				return (BuildingInfoObject)obj;
			}
			catch
			{
				return null;
			}
		}

		public Stream SerializeToStream()
		{
			IFormatter formatter = new BinaryFormatter();
			Stream stream = new FileStream(@"c:\BIO.bin", FileMode.Create, FileAccess.Write, FileShare.None);
			formatter.Serialize(stream, BuildingInfoObject.Instance);
			stream.Close();
			return stream;
		}
		
		public BuildingInfoObject DeserializeFromStream()
		{
		
			IFormatter formatter = new BinaryFormatter();
			Stream stream = new FileStream(@"c:\BIO.bin", FileMode.Open, FileAccess.Read, FileShare.Read);
			BuildingInfoObject obj = (BuildingInfoObject) formatter.Deserialize(stream);			
			stream.Close();

			return obj;		
		}				
	}
}
