using System;
using System.Collections;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization.Formatters;
using System.Runtime.Serialization;
using System.IO;


namespace ProfitGrabber.Commercial.Objects
{
	/// <summary>
	/// Summary description for LoansObject.
	/// </summary>
	[Serializable]
	public sealed class LoansObject
	{
		static LoansObject _instance = new LoansObject();

		static LoansObject()
		{			
		}

		LoansObject()
		{				
		}

		public static LoansObject Instance
		{
			get	{ return _instance; }
		}

		string _purchasePrice = string.Empty;
		string _downPayment = string.Empty;
		string _downPaymentPerc = string.Empty;
		string _firstAssumable = string.Empty;
		string _secondAssumable = string.Empty;
		string _sellerCarryback = string.Empty;
		string _totalPaymentsMonthly = string.Empty;
		string _totalPaymentsAnnual = string.Empty;
		Loan[] _loans = new Loan[3] { new Loan(), new Loan(), new Loan() };

		//DERIVED DATA
		decimal _purchasePriceValue = 0;
		decimal _downPaymentValue = 0;
		decimal _downPaymentPercValue = 0;

		public decimal TotalOfAllLoans
		{
			get 
			{
				decimal res = 0;
				
				try { res += _loans[0].AmountValue; }
				catch {}
				try { res += _loans[1].AmountValue; }
				catch {}
				try { res += _loans[2].AmountValue; }
				catch {}

				return res;
			}
		}

		public decimal BlendedRatePerc
		{
			get 
			{
				decimal res = 0;

                if (0 != TotalOfAllLoans)
                {
                    try { res += _loans[0].RatePercValue * _loans[0].AmountValue / TotalOfAllLoans; }
                    catch { }
                    try { res += _loans[1].RatePercValue * _loans[1].AmountValue / TotalOfAllLoans; }
                    catch { }
                    try { res += _loans[2].RatePercValue * _loans[2].AmountValue / TotalOfAllLoans; }
                    catch { }
                }

				return res;
			}
		}

		public decimal CLTVPerc
		{
			get 
			{
				decimal res = 0;

				try { res += _loans[0].LtvPercValue; }
				catch {}
				try { res += _loans[1].LtvPercValue; }
				catch {}
				try { res += _loans[2].LtvPercValue; }
				catch {}

				return res;
			}
		}

		public decimal TotalMonthlyPayments
		{
			get 
			{
				decimal res = 0;

				try { res += _loans[0].MonthlyPaymentValue; }
				catch {}
				try { res += _loans[1].MonthlyPaymentValue; }
				catch {}
				try { res += _loans[2].MonthlyPaymentValue; }
				catch {}

				return res;
			}
		}

		public decimal TotalAnnualDebtService
		{
			get 
			{				
				return 12 * TotalMonthlyPayments;
			}
		}

		public decimal DCR
		{
			get {return 0;}
		}

		#region DERIVED PROPERTIES
		public System.Decimal PurchasePriceValue
		{
			get
			{
				_purchasePriceValue = 0;

				try 
                { 
                    if (string.Empty != _purchasePrice)
                        _purchasePriceValue = Convert.ToDecimal(_purchasePrice); 
                }
				catch { _purchasePriceValue = 0; }

				return _purchasePriceValue;
			}			
		}


		public System.Decimal DownPaymentValue
		{
			get
			{
				_downPaymentValue = 0;

				try 
                {
                    if (string.Empty != _downPayment)
                        _downPaymentValue = Convert.ToDecimal(_downPayment); 
                }
				catch { _downPaymentValue = 0; }

				return _downPaymentValue;
			}			
		}
		
		public System.Decimal DownPaymentPercValue
		{
			get
			{
				_downPaymentPercValue = 0;

				try 
                {
                    if (string.Empty != _downPaymentPerc)
                        _downPaymentPercValue = Convert.ToDecimal(_downPaymentPerc); 
                }
				catch { _downPaymentPercValue = 0; }

				return _downPaymentPercValue;
			}			
		}

		#endregion

		#region PROPERTIES
		public System.String PurchasePriceProperty
		{
			get
			{
				return _purchasePrice;
			}
			set
			{
				_purchasePrice = value;				
			}
		}
		
        public System.String DownPaymentProperty
		{
			get
			{
				return _downPayment;
			}
			set
			{
				_downPayment = value;
				try 
				{ 
					_downPaymentPercValue = DownPaymentValue / PurchasePriceValue * 100; 
					_downPaymentPerc = _downPaymentPercValue.ToString("0.00");
				}
				catch {_downPaymentPercValue = 0;}
			}
		}
		
        public System.String DownPaymentPercProperty
		{
			get
			{
				return _downPaymentPerc;
			}
			set
			{
				_downPaymentPerc = value;
				try
				{
					_downPaymentValue = PurchasePriceValue * DownPaymentPercValue / 100;
					_downPayment = _downPaymentValue.ToString("0.00");
				}
				catch {_downPaymentValue = 0;}
			}
		}		
		public System.String FirstAssumableProperty
		{
			get
			{
				return _firstAssumable;
			}
			set
			{
				_firstAssumable = value;
			}
		}		
        public System.String SecondAssumableProperty
		{
			get
			{
				return _secondAssumable;
			}
			set
			{
				_secondAssumable = value;
			}
		}
		
		public System.String SellerCarrybackProperty
		{
			get
			{
				return _sellerCarryback;
			}
			set
			{
				_sellerCarryback = value;
			}
		}
		
		public System.String TotalPaymentsMonthlyProperty
		{
			get
			{
				return _totalPaymentsMonthly;
			}
			set
			{
				_totalPaymentsMonthly = value;
			}
		}
		
		public System.String TotalPaymentsAnnualProperty
		{
			get
			{
				return _totalPaymentsAnnual;
			}
			set
			{
				_totalPaymentsAnnual = value;
			}
		}
		
		public Loan[] LoansProperty
		{
			get
			{
				return _loans;
			}
			set
			{
				_loans = value;
			}
		}
		#endregion

		public double CalculatePeriodicPaymentAmount (bool fixedRatePayments, decimal amountValue, decimal ratePercValue, decimal amortTermValue)
		{
			double res = 0;
			
			if (fixedRatePayments)
			{
				try 
				{ 
					res = (double)(amountValue * ratePercValue / (12 * 100)) / (1 -  Math.Pow((1 + (double)(ratePercValue / (12 * 100))), (double)(-1 * amortTermValue * 12))); 					
				}
				catch {res = 0;}
			}
			else
			{
				res =  (double)((ratePercValue / 100 * amountValue) / 12);
			}

			return res;
		}

		public void ResetObject()
		{
			_instance = new LoansObject();
		}

		public byte[] SerializeToByteArray()
		{			
			try
			{
				BinaryFormatter bf = new BinaryFormatter();
				MemoryStream ms = new MemoryStream();
				bf.Serialize(ms, LoansObject.Instance);				
				return ms.ToArray();
			}
			catch 
			{
				return null;
			}
		}

		public LoansObject DeserializeFromByteArray(byte[] bytes)
		{
			
			try 
			{				
				MemoryStream ms = new MemoryStream(bytes);
				BinaryFormatter bf = new BinaryFormatter();

				ms.Position = 0;
				object obj = bf.Deserialize(ms);			
				
				_instance = (LoansObject)obj;
				return (LoansObject)obj;
			}
			catch
			{
				return null;
			}
		}

		public Stream SerializeToStream()
		{
			IFormatter formatter = new BinaryFormatter();
			Stream stream = new FileStream(@"c:\LO.bin", FileMode.Create, FileAccess.Write, FileShare.None);
			formatter.Serialize(stream, LoansObject.Instance);
			stream.Close();

			return stream;
		}

		public LoansObject DeserializeFromStream()
		{
			IFormatter formatter = new BinaryFormatter();
			Stream stream = new FileStream(@"c:\LO.bin", FileMode.Open, FileAccess.Read, FileShare.Read);
			LoansObject obj = (LoansObject) formatter.Deserialize(stream);
			stream.Close();
			_instance = obj;
			return obj;
		}
	}

	[Serializable]
	public class Loan
	{
		string _loanType = string.Empty;
		string _ltvPerc = string.Empty;
		string _amount = string.Empty;
		string _amortTerm = string.Empty;
		string _ratePerc = string.Empty;
		string _monthlyPayment = string.Empty;
		string _pointsPerc = string.Empty;
		string _pointsUSD = string.Empty;
		string _firstPaymentDate = string.Empty;
		string _lastPaymentDate = string.Empty;
		string _lender = string.Empty;
		LoanRateAdjustments _lrAdjustments = new LoanRateAdjustments();

		//DERIVED DATA
		decimal _ltvPercValue = 0;
		decimal _amountValue = 0;
		decimal _amortTermValue = 0;
		decimal _ratePercValue = 0;
		decimal _monthlyPaymentValue = 0;
		decimal _pointsPercValue = 0;
		decimal _pointsUSDValue = 0;	
	

		public double CalculatePeriodicPaymentAmount (bool fixedRatePayments)
		{
			double res = 0;
			
			if (fixedRatePayments)
			{
				try 
				{ 
					res = (double)(AmountValue * RatePercValue / (12 * 100)) / (1 -  Math.Pow((1 + (double)(RatePercValue / (12 * 100))), (double)(-1 * AmortTermValue * 12))); 

					double res2 = 0;
					double res3 = 0;
					double res4 = 0;

					res2 = (double)(AmountValue * (RatePercValue / (12 * 100))) *  Math.Pow((double)(1 + RatePercValue / (12 * 100)), (double)AmortTermValue * 12);
					res3 = Math.Pow((double)(1 + RatePercValue / (12 * 100)), (double)AmortTermValue * 12) - 1;
					res4 = res2 / res3;
				}
				catch {res = 0;}
			}
			else
			{
				res =  (double)((RatePercValue / 100 * AmountValue) / 12);
			}

			return res;
		}


		#region PROPERTIES
		public System.String LoanTypeProperty
		{
			get
			{
				return _loanType;
			}
			set
			{
				_loanType = value;
			}
		}
		
		public System.String LtvPercProperty
		{
			get
			{
				return _ltvPerc;
			}
			set
			{
				_ltvPerc = value;
			}
		}
		
		public System.String AmountProperty
		{
			get
			{
				return _amount;
			}
			set
			{
				_amount = value;
			}
		}
		
		public System.String AmortTermProperty
		{
			get
			{
				return _amortTerm;
			}
			set
			{
				_amortTerm = value;
			}
		}
		
		public System.String RatePercProperty
		{
			get
			{
				return _ratePerc;
			}
			set
			{
				_ratePerc = value;
			}
		}
		
		public System.String MonthlyPaymentProperty
		{
			get
			{
				return _monthlyPayment;
			}
			set
			{
				_monthlyPayment = value;
			}
		}
		
		public System.String PointsPercProperty
		{
			get
			{
				return _pointsPerc;
			}
			set
			{
				_pointsPerc = value;
			}
		}
		
		public System.String PointsUSDProperty
		{
			get
			{
				return _pointsUSD;
			}
			set
			{
				_pointsUSD = value;
			}
		}
		
		public System.String FirstPaymentDateProperty
		{
			get
			{
				return _firstPaymentDate;
			}
			set
			{
				_firstPaymentDate = value;
			}
		}
		
		public System.String LastPaymentDateProperty
		{
			get
			{
				return _lastPaymentDate;
			}
			set
			{
				_lastPaymentDate = value;
			}
		}
		
		public System.String LenderProperty
		{
			get
			{
				return _lender;
			}
			set
			{
				_lender = value;
			}
		}
		
		public ProfitGrabber.Commercial.Objects.LoanRateAdjustments LrAdjustmentsProperty
		{
			get
			{
				return _lrAdjustments;
			}
			set
			{
				_lrAdjustments = value;
			}
		}
		#endregion

		#region DERIVED PROPERTIES
		public System.Decimal LtvPercValue
		{
			get
			{	
				_ltvPercValue = 0;

				try 
                { 
                    if (string.Empty != _ltvPerc)
                        _ltvPercValue = Convert.ToDecimal(_ltvPerc); 
                }
				catch { _ltvPercValue = 0; }

				return _ltvPercValue;
			}			
		}		
		public System.Decimal AmountValue
		{
			get
			{
				_amountValue = 0;

				try
                { 
                    if (string.Empty != _amount)
                        _amountValue = Convert.ToDecimal(_amount); 
                }
				catch { _amountValue = 0; }

				return _amountValue;
			}			
		}		
		public System.Decimal AmortTermValue
		{
			get
			{	
				_amortTermValue = 0;

				try 
                {
                    if (string.Empty != _amortTerm)
                    _amortTermValue = Convert.ToDecimal(_amortTerm); 
                }
				catch { _amortTermValue = 0; }

				return _amortTermValue;
			}			
		}
		
		public System.Decimal RatePercValue
		{
			get
			{
				_ratePercValue = 0;

				try 
                {
                    if (string.Empty != _ratePerc)
                    _ratePercValue = Convert.ToDecimal(_ratePerc); 
                }
				catch{ _ratePercValue = 0; }

				return _ratePercValue;
			}			
		}		
		public System.Decimal MonthlyPaymentValue
		{
			get
			{
				_monthlyPaymentValue = 0;

				try 
                {
                    if (string.Empty != _monthlyPayment)
                        _monthlyPaymentValue = Convert.ToDecimal(_monthlyPayment); 
                }
				catch { _monthlyPaymentValue = 0; }

				return _monthlyPaymentValue;
			}		
		}
		
		public System.Decimal PointsPercValue
		{
			get
			{
				_pointsPercValue = 0;

				try 
                {
                    if (string.Empty != _pointsPerc)
                        _pointsPercValue = Convert.ToDecimal(_pointsPerc); 
                }
				catch{ _pointsPercValue = 0; }

				return _pointsPercValue;
			}				
		}
		
		public System.Decimal PointsUSDValue
		{
			get
			{
				_pointsUSDValue = 0;

				try 
                {
                    if (string.Empty != _pointsUSD)
                        _pointsUSDValue = Convert.ToDecimal(_pointsUSD); 
                }
				catch{ _pointsUSDValue = 0; }

				return _pointsUSDValue;
			}			
		}
		#endregion

		public Loan()
		{
		}

        public override string ToString()
        {
            string retVal = string.Empty;
            
            retVal += LoanTypeProperty;
            if (string.Empty != retVal)
                retVal += ", ";

            retVal += AmountProperty + "$";
            if (string.Empty != retVal && null != AmortTermProperty && string.Empty != AmortTermProperty.Trim())
            {
                retVal += ", ";
                retVal += AmortTermProperty + "yr";
            }

            return retVal;
        }
	}

	[Serializable]
	public class LoanRateAdjustments
	{
		string _initialFixedPeriodYrs = string.Empty;
		string _initialRateCapPerc = string.Empty;
		string _annualCapPerc = string.Empty;
		string _lifeTimeCapPerc = string.Empty;
		string[] _rates = new string[10] { string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty };

		//DERIVED DATA
		decimal _initialFixedPeriodYrsValue = 0;
		decimal _initialRateCapPercValue = 0;
		decimal _annualCapPercValue = 0;
		decimal _lifeTimeCapPercValue = 0;
		decimal[] _ratesValue = new decimal[10] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
		
		#region PROPERTIES
		public System.String InitialFixedPeriodYrsProperty
		{
			get
			{
				return _initialFixedPeriodYrs;
			}
			set
			{
				_initialFixedPeriodYrs = value;
			}
		}

		
		public System.String InitialRateCapPercProperty
		{
			get
			{
				return _initialRateCapPerc;
			}
			set
			{
				_initialRateCapPerc = value;
			}
		}

		
		public System.String AnnualCapPercProperty
		{
			get
			{
				return _annualCapPerc;
			}
			set
			{
				_annualCapPerc = value;
			}
		}

		
		public System.String LifeTimeCapPercProperty
		{
			get
			{
				return _lifeTimeCapPerc;
			}
			set
			{
				_lifeTimeCapPerc = value;
			}
		}


		
		public string[] RatesProperty
		{
			get
			{
				return _rates;
			}
			set
			{
				_rates = value;
			}
		}
		#endregion

		#region DERIVED PROPERTIES
		public System.Decimal InitialFixedPeriodYrsValue
		{
			get
			{
				_initialFixedPeriodYrsValue = 0;
				
				try 
                {
                    if (string.Empty != _initialFixedPeriodYrs)
                        _initialFixedPeriodYrsValue = Convert.ToDecimal(_initialFixedPeriodYrs);
                }
				catch{_initialFixedPeriodYrsValue = 0;}

				return _initialFixedPeriodYrsValue;
			}			
		}
		
		public System.Decimal InitialRateCapPercValue
		{
			get
			{
				_initialRateCapPercValue = 0;

				try 
                {
                    if (string.Empty != _initialRateCapPerc)
                        _initialRateCapPercValue = Convert.ToDecimal(_initialRateCapPerc);
                }
				catch{_initialRateCapPercValue = 0;}

				return _initialRateCapPercValue;
			}			
		}
		
		public System.Decimal AnnualCapPercValue
		{
			get
			{
				_annualCapPercValue = 0;

				try 
                {
                    if (string.Empty != _annualCapPerc)
                        _annualCapPercValue = Convert.ToDecimal(_annualCapPerc);
                }
				catch{_annualCapPercValue = 0;}

				return _annualCapPercValue;
			}			
		}
		
		public System.Decimal LifeTimeCapPercValue
		{
			get
			{
				_lifeTimeCapPercValue = 0;

				try 
                {
                    if (string.Empty != _lifeTimeCapPerc)
                        _lifeTimeCapPercValue = Convert.ToDecimal(_lifeTimeCapPerc);
                }
				catch{_lifeTimeCapPercValue = 0;}

				return _lifeTimeCapPercValue;
			}		
		}

		decimal[] RatesValue
		{
			get 
			{
				//INIT ARRAY
				_ratesValue = new decimal[10];
				for (int i = 0;  i < 10; i++)				
					_ratesValue[i] = 0;

				for (int i = 0; i < 10; i++)
				{
					try 
                    {
                        if (string.Empty != _rates[i])
                            _ratesValue[i] = Convert.ToDecimal(_rates[i]);
                        else
                            _ratesValue[i] = 0;
                    }
					catch {_ratesValue[i] = 0;}
				}
				
				return _ratesValue;
			}
		}
		#endregion
		
		public LoanRateAdjustments()
		{
		}
	}
}
