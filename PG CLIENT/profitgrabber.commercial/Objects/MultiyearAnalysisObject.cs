﻿using System;
using System.Collections;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization.Formatters;
using System.Runtime.Serialization;
using System.IO;

namespace ProfitGrabber.Commercial.Objects
{
    [Serializable]
    public sealed class MultiyearAnalysisObject
    {
        static MultiyearAnalysisObject _instance = new MultiyearAnalysisObject();

        MultiyearAnalysisObject()
        { }

        static MultiyearAnalysisObject()
        { }

        public static MultiyearAnalysisObject Instance
        {
            get { return _instance; }
        }

        #region DATA
        string _purchasePrice = string.Empty;
        string _initialInvestments = string.Empty;
        string _downPayement = string.Empty;
        string _points = string.Empty;
        string _otherAcquisitionCosts = string.Empty;

        string _gsiAnnual = string.Empty;        
        string _gsiAnnualPerc = string.Empty;        
        string _vacancyCreditLoss = string.Empty;        
        string _vacancyCreditLossPerc = string.Empty;        
        string _otherIncome = string.Empty;        
        string _otherIncomePerc = string.Empty;        
        string _operatingExpenses = string.Empty;        
        string _operatingExpensesPerc = string.Empty;

        string _annualInflationChange = string.Empty;
        string _annualOperatingExpensesChange = string.Empty;
        string _annualVacancyChange = string.Empty;
        string _annualIncomeChange = string.Empty;

        string _federalRate = string.Empty;
        string _stateRate = string.Empty;
        string _capitalGainsRate = string.Empty;
        string _depreciableYears = string.Empty;
        string _buildingValue = string.Empty;

        string _saleCosts = string.Empty;
        string _saleCapRate = string.Empty;

        string[] _operatingExpensesPercs = new string[10];        
        string[] _vacancyPercs = new string[10];        
        string[] _incomeChangePercs = new string[10];
        #endregion

        #region PROPERTIES
        #region 1
        public string PurchasePrice
        {
            get { return _purchasePrice; }
            set { _purchasePrice = value; }
        }

        public string InitialInvestments
        {
            get { return _initialInvestments; }
            set { _initialInvestments = value; }
        }

        public string DownPayement
        {
            get { return _downPayement; }
            set { _downPayement = value; }
        }

        public string Points
        {
            get { return _points; }
            set { _points = value; }
        }

        public string OtherAcquisitionCosts
        {
            get { return _otherAcquisitionCosts; }
            set { _otherAcquisitionCosts = value; }
        }


        
        #endregion

        #region 2
        public string GsiAnnual
        {
            get { return _gsiAnnual; }
            set { _gsiAnnual = value; }
        }

        public string GsiAnnualPerc
        {
            get { return _gsiAnnualPerc; }
            set { _gsiAnnualPerc = value; }
        }

        public string VacancyCreditLoss
        {
            get { return _vacancyCreditLoss; }
            set { _vacancyCreditLoss = value; }
        }

        public string VacancyCreditLossPerc
        {
            get { return _vacancyCreditLossPerc; }
            set { _vacancyCreditLossPerc = value; }
        }

        public string OtherIncome
        {
            get { return _otherIncome; }
            set { _otherIncome = value; }
        }

        public string OtherIncomePerc
        {
            get { return _otherIncomePerc; }
            set { _otherIncomePerc = value; }
        }

        public string OperatingExpenses
        {
            get { return _operatingExpenses; }
            set { _operatingExpenses = value; }
        }

        public string OperatingExpensesPerc
        {
            get { return _operatingExpensesPerc; }
            set { _operatingExpensesPerc = value; }
        }
        #endregion

        #region 3
        public string AnnualInflationChange
        {
            get { return _annualInflationChange; }
            set { _annualInflationChange = value; }
        }

        public string AnnualOperatingExpensesChange
        {
            get { return _annualOperatingExpensesChange; }
            set { _annualOperatingExpensesChange = value; }
        }

        public string AnnualVacancyChange
        {
            get { return _annualVacancyChange; }
            set { _annualVacancyChange = value; }
        }

        public string AnnualIncomeChange
        {
            get { return _annualIncomeChange; }
            set { _annualIncomeChange = value; }
        }
        #endregion

        #region 4
        public string FederalRate
        {
            get { return _federalRate; }
            set { _federalRate = value; }
        }


        public string StateRate
        {
            get { return _stateRate; }
            set { _stateRate = value; }
        }


        public string CapitalGainsRate
        {
            get { return _capitalGainsRate; }
            set { _capitalGainsRate = value; }
        }


        public string DepreciableYears
        {
            get { return _depreciableYears; }
            set { _depreciableYears = value; }
        }


        public string BuildingValue
        {
            get { return _buildingValue; }
            set { _buildingValue = value; }
        }
        #endregion

        #region 5
        public string SaleCosts
        {
            get { return _saleCosts; }
            set { _saleCosts = value; }
        }

        public string SaleCapRate
        {
            get { return _saleCapRate; }
            set { _saleCapRate = value; }
        }
        #endregion

        #region 6
        public string[] OperatingExpensesPercs
        {
            get { return _operatingExpensesPercs; }
            set { _operatingExpensesPercs = value; }
        }

        public string[] VacancyPercs
        {
            get { return _vacancyPercs; }
            set { _vacancyPercs = value; }
        }

        public string[] IncomeChangePercs
        {
            get { return _incomeChangePercs; }
            set { _incomeChangePercs = value; }
        }
        #endregion
        #endregion

        public void ResetObject()
        {
            _instance = new MultiyearAnalysisObject();
        }

        public byte[] SerializeToByteArray()
        {
            try
            {
                BinaryFormatter bf = new BinaryFormatter();
                MemoryStream ms = new MemoryStream();
                bf.Serialize(ms, MultiyearAnalysisObject.Instance);
                return ms.ToArray();
            }
            catch
            {
                return null;
            }
        }

        public MultiyearAnalysisObject DeserializeFromByteArray(byte[] bytes)
        {

            try
            {
                MemoryStream ms = new MemoryStream(bytes);
                BinaryFormatter bf = new BinaryFormatter();

                ms.Position = 0;
                object obj = bf.Deserialize(ms);

                _instance = (MultiyearAnalysisObject)obj;
                return (MultiyearAnalysisObject)obj;
            }
            catch
            {
                return null;
            }
        }
    }

    public class CustomYearItem
    {
        int _value;
        string _text;

        public CustomYearItem()
        { }

        public CustomYearItem(int value)
        {
            _value = value;
        }

        public int Value
        {
            get { return _value; }
        }

        public string Text
        {
            get
            {
                if (1 == _value)
                    return _value.ToString() + " year";
                else
                    return _value.ToString() + " years";
            }
        }

        public override string ToString()
        {
            return Text;
        }
    }

    public class CustomItem
    {
        int _changeValue;
        string _text;

        public CustomItem()
        { }

        public CustomItem(int changeValue)
        {
            _changeValue = changeValue;
            SetText(changeValue);
        }

        void SetText(int changeValue)
        {
            if (0 == changeValue)
                _text = "No Change";
            else if (0 < changeValue)
                _text = "Increase " + Math.Abs(changeValue).ToString() + "%";
            else
                _text = "Decrease " + changeValue.ToString() + "%";
        }

        public int ChangeValue
        {
            get { return _changeValue; }
        }

        public override string ToString()
        {
            return _text;
        }

    }
}
