using System;
using System.Collections;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization.Formatters;
using System.Runtime.Serialization;
using System.IO;


namespace ProfitGrabber.Commercial.Objects
{
	[Serializable]
	public enum ExpensesTypes
	{
		Administrative = 1,		
		Management = 2,
		Utilities = 4,
		Insurance = 8,
		Operating = 16,			
	}

	/// <summary>
	/// Summary description for ExpensesObject.
	/// </summary>
	[Serializable]
	public sealed class ExpensesObject
	{
		static ExpensesObject _instance = new ExpensesObject();

		ExpensesObject()
		{			
		}

		static ExpensesObject()
		{			
		}

		public static ExpensesObject Instance
		{
			get { return _instance; }
		}
		
		public const int MaxExpensesItems = 34;
		
		UtilityTypes _utilityTypes = new UtilityTypes();
		CapitalAdditions _capitalAdditions = new CapitalAdditions();
		ExpensesItems _expensesItems = new ExpensesItems();
		TotalExpensesItems _totalExpensesItems = new TotalExpensesItems();

		#region PROPERTIES
		public ProfitGrabber.Commercial.Objects.UtilityTypes UtilityTypesProperty
		{
			get
			{
				return _utilityTypes;
			}
			set
			{
				_utilityTypes = value;
			}
		}

		public ProfitGrabber.Commercial.Objects.CapitalAdditions CapitalAdditionsProperty
		{
			get
			{
				return _capitalAdditions;
			}
			set
			{
				_capitalAdditions = value;
			}
		}

		public ProfitGrabber.Commercial.Objects.ExpensesItems ExpensesItemsProperty
		{
			get
			{
				return _expensesItems;
			}
			set
			{
				_expensesItems = value;
			}
		}

		public ProfitGrabber.Commercial.Objects.TotalExpensesItems TotalExpensesItemsProperty
		{
			get
			{
				return _totalExpensesItems;
			}
			set
			{
				_totalExpensesItems = value;
			}
		}
		#endregion

		public void ResetObject()
		{
			_instance = new ExpensesObject();
		}

		public byte[] SerializeToByteArray()
		{			
			try
			{
				BinaryFormatter bf = new BinaryFormatter();
				MemoryStream ms = new MemoryStream();
				bf.Serialize(ms, ExpensesObject.Instance);				
				return ms.ToArray();
			}
			catch 
			{
				return null;
			}
		}

		public ExpensesObject DeserializeFromByteArray(byte[] bytes)
		{
			
			try 
			{				
				MemoryStream ms = new MemoryStream(bytes);
				BinaryFormatter bf = new BinaryFormatter();

				ms.Position = 0;
				object obj = bf.Deserialize(ms);			
				
				_instance = (ExpensesObject)obj;
				return (ExpensesObject)obj;
			}
			catch
			{
				return null;
			}
		}

		public Stream SerializeToStream()
		{
			IFormatter formatter = new BinaryFormatter();
			Stream stream = new FileStream(@"c:\EO.bin", FileMode.Create, FileAccess.Write, FileShare.None);
			formatter.Serialize(stream, ExpensesObject.Instance);
			stream.Close();

			return stream;
		}

		public ExpensesObject DeserializeFromStream()
		{
			IFormatter formatter = new BinaryFormatter();
			Stream stream = new FileStream(@"c:\EO.bin", FileMode.Open, FileAccess.Read, FileShare.Read);
			ExpensesObject obj = (ExpensesObject) formatter.Deserialize(stream);
			stream.Close();
			_instance = obj;
			return obj;
		}	
	}

	#region UTILITY TYPES
	[Serializable]
	public class UtilityTypes
	{
		string _electricMeter = string.Empty;
		string _heat = string.Empty;
		string _hotWater = string.Empty;

		public UtilityTypes() 
		{}

		#region PROPERTIES
		public System.String ElectricMeterProperty
		{
			get
			{
				return _electricMeter;
			}
			set
			{
				_electricMeter = value;
			}
		}
			
		public System.String HeatProperty
		{
			get
			{
				return _heat;
			}
			set
			{
				_heat = value;
			}
		}
			
		public System.String HotWaterProperty
		{
			get
			{
				return _hotWater;
			}
			set
			{
				_hotWater = value;
			}
		}

		#endregion						
	}
	#endregion

	[Serializable]
	public class CapitalAdditions
	{
		CostType[] years = new CostType[10];

		public CapitalAdditions()
		{
			for (int i = 0; i < 10; i++)
				years[i] = new CostType();
		}

		public CostType[]  yearsProperty
		{
			get
			{
				return years;
			}
			set
			{
				years = value;
			}
		}			
	}

	[Serializable]	
	public class CostType
	{
		string _cost;
		string _type;
			
		public CostType()
		{}

		#region PROPERTIES
		public System.String _costProperty
		{
			get
			{
				return _cost;
			}
			set
			{
				_cost = value;
			}
		}
			
		public System.String _typeProperty
		{
			get
			{
				return _type;
			}
			set
			{
				_type = value;
			}
		}
		#endregion			
	}

	[Serializable]
	public class ExpensesItems
	{		
		ExpensesItem[] _items = new ExpensesItem[43];

		public ExpensesItems()
		{
			for (int i = 0; i < 43; i++)
				_items[i] = new ExpensesItem();
		}

		public void GetExpensesTypesTotal(out decimal usdPerMonthValueGroupTotal, out decimal usdPerYearValueGroupTotal, out decimal usdPerSqFtValueGroupTotal, out decimal usdPerUnitValueGroupTotal)
		{
			usdPerMonthValueGroupTotal = 0;
			usdPerYearValueGroupTotal = 0;
			usdPerSqFtValueGroupTotal = 0;
			usdPerUnitValueGroupTotal = 0;

			decimal usdPerMonthValueTotal = 0;
			decimal usdPerYearValueTotal = 0;
			decimal usdPerSqFtValueTotal = 0;
			decimal usdPerUnitValueTotal = 0;			
			
			GetExpensesTotal(ExpensesTypes.Administrative, out usdPerMonthValueTotal, out usdPerYearValueTotal, out usdPerSqFtValueTotal, out usdPerUnitValueTotal);
			usdPerMonthValueGroupTotal += usdPerMonthValueTotal;
			usdPerYearValueGroupTotal += usdPerYearValueTotal;
			usdPerSqFtValueGroupTotal += usdPerSqFtValueTotal;
			usdPerUnitValueGroupTotal += usdPerUnitValueTotal;

			GetExpensesTotal(ExpensesTypes.Insurance, out usdPerMonthValueTotal, out usdPerYearValueTotal, out usdPerSqFtValueTotal, out usdPerUnitValueTotal);
			usdPerMonthValueGroupTotal += usdPerMonthValueTotal;
			usdPerYearValueGroupTotal += usdPerYearValueTotal;
			usdPerSqFtValueGroupTotal += usdPerSqFtValueTotal;
			usdPerUnitValueGroupTotal += usdPerUnitValueTotal;

			GetExpensesTotal(ExpensesTypes.Management, out usdPerMonthValueTotal, out usdPerYearValueTotal, out usdPerSqFtValueTotal, out usdPerUnitValueTotal);
			usdPerMonthValueGroupTotal += usdPerMonthValueTotal;
			usdPerYearValueGroupTotal += usdPerYearValueTotal;
			usdPerSqFtValueGroupTotal += usdPerSqFtValueTotal;
			usdPerUnitValueGroupTotal += usdPerUnitValueTotal;

			GetExpensesTotal(ExpensesTypes.Operating, out usdPerMonthValueTotal, out usdPerYearValueTotal, out usdPerSqFtValueTotal, out usdPerUnitValueTotal);
			usdPerMonthValueGroupTotal += usdPerMonthValueTotal;
			usdPerYearValueGroupTotal += usdPerYearValueTotal;
			usdPerSqFtValueGroupTotal += usdPerSqFtValueTotal;
			usdPerUnitValueGroupTotal += usdPerUnitValueTotal;

			GetExpensesTotal(ExpensesTypes.Utilities, out usdPerMonthValueTotal, out usdPerYearValueTotal, out usdPerSqFtValueTotal, out usdPerUnitValueTotal);
			usdPerMonthValueGroupTotal += usdPerMonthValueTotal;
			usdPerYearValueGroupTotal += usdPerYearValueTotal;
			usdPerSqFtValueGroupTotal += usdPerSqFtValueTotal;
			usdPerUnitValueGroupTotal += usdPerUnitValueTotal;

		}

		public decimal GetTotalYearExpensesForAllTypes()
		{
			decimal result = 0;
			
			for (int i = 0; i <= 33; i++)
			{
				result += _items[i].USDPerYearValue;
			}

			return result;
		}

		public void GetExpensesTotal(ExpensesTypes type, out decimal usdPerMonthValueTotal, out decimal usdPerYearValueTotal, out decimal usdPerSqFtValueTotal, out decimal usdPerUnitValueTotal)
		{
			usdPerMonthValueTotal = 0;
			usdPerYearValueTotal = 0;
			usdPerSqFtValueTotal = 0;
			usdPerUnitValueTotal = 0;

			int lowLimit = 0;
			int highLimit = 0;

			switch (type)
			{
				case ExpensesTypes.Administrative:
					lowLimit = 0;
					highLimit = 5;
					break;

				case ExpensesTypes.Management:
					lowLimit = 6;
					highLimit = 8;
					break;

				case ExpensesTypes.Utilities:
					lowLimit = 9;
					highLimit = 13;
					break;

				case ExpensesTypes.Insurance:
					lowLimit = 14;
					highLimit = 19;
					break;

				case ExpensesTypes.Operating:
					lowLimit = 20;
					highLimit = 33;
					break;
			}

			for (int i = lowLimit; i <= highLimit; i++)
			{
				usdPerMonthValueTotal += _items[i].USDPerMonthValue;
				usdPerYearValueTotal += _items[i].USDPerYearValue;
				usdPerSqFtValueTotal += _items[i].USDPerSqFtValue;
				usdPerUnitValueTotal += _items[i].USDPerUnitValue;
			}
		}
		
		#region GET DESCRIPTOR FOR EXPENESES ITEM
		public void GetDescriptor(int itemNr, out string type, out string desc)
		{
			type = desc = string.Empty;

			if (0 <= itemNr && itemNr <=5)
				type = "Administrative";
			else if (6 <= itemNr && itemNr <= 8)
				type = "Management";
			else if (9 <= itemNr && itemNr <= 13)
				type = "Utilities";
			else if (14 <= itemNr && itemNr <= 19)
				type = "Insurance";
			else if (20 <= itemNr && itemNr <= 33)
				type = "Operating";


			switch (itemNr)
			{
				case 0:
					desc = "Accounting";
					break;
				case 1:
					desc = "Advertising and Marketing";
					break;
				case 2:
					desc = "Legal Services";
					break;
				case 3:
					desc = "Licenses";
					break;
				case 4:
					desc = "Payroll";
					break;
				case 5:
					desc = "Supplies";
					break;
				case 6:
					desc = "Off-Site Management";
					break;
				case 7:
					desc = "On-site Mananagement";
					break;
				case 8:
					desc = "Leasing Commissions";
					break;
				case 9:
					desc = "Electricity";
					break;
				case 10:
					desc = "Gas";
					break;
				case 11:
					desc = "Trash";
					break;
				case 12:
					desc = "Water and Sewer";
					break;
				case 13:
					desc = "Other Utilities";
					break;
				case 14:
					desc = "Fire";
					break;
				case 15:
					desc = "Flood";
					break;
				case 16:
					desc = "Liability";
					break;
				case 17:
					desc = "Mortgage";
					break;
				case 18:
					desc = "Workmen's Comp";
					break;
				case 19:
					desc = "Other";
					break;
				case 20:
					desc = "Elevator Maintenance";
					break;
				case 21:
					desc = "Interior Cleaning";
					break;
				case 22:
					desc = "Interior Painting";
					break;
				case 23:
					desc = "Janitorial Services";
					break;
				case 24:
					desc = "Landscape Maintenance";
					break;
				case 25:
					desc = "Pest Control";
					break;
				case 26:
					desc = "Pool Maintenance";
					break;
				case 27:
					desc = "Real Estate Taxes";
					break;
				case 28:
					desc = "Repairs and Maintenance";
					break;
				case 29:
					desc = "Replacement Reserves";
					break;
				case 30:
					desc = "Reserves";
					break;
				case 31:
					desc = "Security";
					break;
				case 32:
					desc = "Snow";
					break;
				case 33:
					desc = "Miscellaneous";
					break;
			}
		}

		#endregion

		#region PROPERTIES
		public ExpensesItem[] Items 
		{
			get
			{
				return _items;
			}

			set
			{
				_items = value;
			}
		}
		#endregion
	}

	
	[Serializable]
	public class TotalExpensesItems
	{
		TotalExpensesItem[] _items = new TotalExpensesItem[6] {new TotalExpensesItem(), new TotalExpensesItem(), new TotalExpensesItem(), new TotalExpensesItem(), new TotalExpensesItem(), new TotalExpensesItem() };

		public TotalExpensesItems()
		{}

		#region PROPERTIES
		public TotalExpensesItem[] Items
		{
			get 
			{
				return _items;
			}
			set
			{
				_items = value;
			}
		}
		#endregion
	}

	
	[Serializable]
	public class ExpensesItem
	{
		string _type = string.Empty;
		string _desc = string.Empty;
		string _usdPerMonth = string.Empty;
		string _usdPerYear = string.Empty;
		string _usdPerSqFt = string.Empty;
		string _usdPerUnit = string.Empty;
		string _percentOfTotal = string.Empty;
		string _paidBy = string.Empty;
		string _highLow = string.Empty;
		string _comment = string.Empty;

		decimal _usdPerMonthValue = 0;
		decimal _usdPerYearValue = 0;
		decimal _usdPerSqFtValue = 0;
		decimal _usdPerUnitValue = 0;
		decimal _percentOfTotalValue = 0;
						
		public ExpensesItem()
		{}

		#region PROPERTIES
		public System.String TypeProperty
		{
			get
			{
				return _type;
			}
			set
			{
				_type = value;
			}
		}
						
		public System.String DescProperty
		{
			get
			{
				return _desc;
			}
			set
			{
				_desc = value;
			}
		}
			
		public System.String UsdPerMonthProperty
		{
			get
			{
				return _usdPerMonth;
			}
			set
			{
				_usdPerMonth = value;
				_usdPerMonthValue = 0;
				try 
				{
					_usdPerMonthValue = Convert.ToDecimal(_usdPerMonth);
				}
				catch 
				{
					_usdPerMonthValue = 0;
				}

				_usdPerYearValue = _usdPerMonthValue * 12;
				_usdPerSqFtValue = (0 < BuildingInfoObject.Instance.TotalSqFt) ? _usdPerYearValue / BuildingInfoObject.Instance.TotalSqFt : 0;
				_usdPerUnitValue = (0 < BuildingInfoObject.Instance.NumberOfUnitsValue) ? _usdPerYearValue / BuildingInfoObject.Instance.NumberOfUnitsValue : 0;
			}
		}
			
		public System.String UsdPerYearProperty
		{
			get
			{
				return _usdPerYear;
			}
			set
			{
				_usdPerYear = value;
				_usdPerYearValue = 0;
				try
				{
					_usdPerYearValue = Convert.ToDecimal(_usdPerYear);
				}
				catch 
				{
					_usdPerYearValue = 0;
				}
				_usdPerMonthValue = _usdPerYearValue / 12;
				_usdPerSqFtValue = (0 < BuildingInfoObject.Instance.TotalSqFt) ? _usdPerYearValue / BuildingInfoObject.Instance.TotalSqFt : 0;
				_usdPerUnitValue = (0 < BuildingInfoObject.Instance.NumberOfUnitsValue) ? _usdPerYearValue / BuildingInfoObject.Instance.NumberOfUnitsValue : 0;
			}
		}
			
		public System.String UsdPerSqFtProperty
		{
			get
			{
				return _usdPerSqFt;				
			}
			set
			{
				_usdPerSqFt = value;
				_usdPerSqFtValue = 0;
				try
				{
					_usdPerSqFtValue = Convert.ToDecimal(_usdPerSqFt);
				}
				catch {_usdPerSqFtValue  = 0;}
				_usdPerYearValue = _usdPerSqFtValue * BuildingInfoObject.Instance.TotalSqFt;
				_usdPerMonthValue = _usdPerYearValue / 12;				
				_usdPerUnitValue = (0 < BuildingInfoObject.Instance.NumberOfUnitsValue) ? _usdPerYearValue / BuildingInfoObject.Instance.NumberOfUnitsValue : 0;
			}
		}
			
		public System.String UsdPerUnitProperty
		{
			get
			{
				return _usdPerUnit;
			}
			set
			{				
				_usdPerUnit = value;
				_usdPerUnitValue = 0;

				try
				{
					_usdPerUnitValue	= Convert.ToDecimal(_usdPerUnit);
				}
				catch {_usdPerUnitValue = 0;}
				_usdPerYearValue = _usdPerUnitValue * BuildingInfoObject.Instance.NumberOfUnitsValue;
				_usdPerSqFtValue = (0 < BuildingInfoObject.Instance.TotalSqFt) ? _usdPerYearValue / BuildingInfoObject.Instance.TotalSqFt : 0;				
				_usdPerMonthValue = _usdPerYearValue / 12;
			}
		}
			
		public System.String PercentOfTotalProperty
		{
			get
			{
				return _percentOfTotal;
			}
			set
			{
				_percentOfTotal = value;
			}
		}
			
		public System.String PaidByProperty
		{
			get
			{
				return _paidBy;
			}
			set
			{
				_paidBy = value;
			}
		}
			
		public System.String HighLowProperty
		{
			get
			{
				return _highLow;
			}
			set
			{
				_highLow = value;
			}
		}
			
		public System.String CommentProperty
		{
			get
			{
				return _comment;
			}
			set
			{
				_comment = value;
			}
		}
		#endregion

		public decimal USDPerMonthValue
		{
			get
			{
				return _usdPerMonthValue;
			}
		}

		public decimal USDPerYearValue
		{
			get
			{
				return _usdPerYearValue;
			}
		}

		public decimal USDPerSqFtValue
		{
			get
			{
				return _usdPerSqFtValue;
			}
		}

		public decimal USDPerUnitValue
		{
			get
			{
				return _usdPerUnitValue;
			}
		}			
	}


	[Serializable]
	public class TotalExpensesItem
	{
		string _type = string.Empty;
		string _desc = string.Empty;
		string _usdPerMonth = string.Empty;
		string _usdPerYear = string.Empty;
		string _usdPerSqFt = string.Empty;
		string _usdPerUnit = string.Empty;
		string _percentOfTotal = string.Empty;
			
						
		public TotalExpensesItem()
		{}

		#region PROPERTIES
		public System.String TypeProperty
		{
			get
			{
				return _type;
			}
			set
			{
				_type = value;
			}
		}
						
		public System.String DescProperty
		{
			get
			{
				return _desc;
			}
			set
			{
				_desc = value;
			}
		}
			
		public System.String UsdPerMonthProperty
		{
			get
			{
				return _usdPerMonth;
			}
			set
			{
				_usdPerMonth = value;
			}
		}
			
		public System.String UsdPerYearProperty
		{
			get
			{
				return _usdPerYear;
			}
			set
			{
				_usdPerYear = value;
			}
		}
			
		public System.String UsdPerSqFtProperty
		{
			get
			{
				return _usdPerSqFt;
			}
			set
			{
				_usdPerSqFt = value;
			}
		}
			
		public System.String UsdPerUnitProperty
		{
			get
			{
				return _usdPerUnit;
			}
			set
			{
				_usdPerUnit = value;
			}
		}
			
		public System.String PercentOfTotalProperty
		{
			get
			{
				return _percentOfTotal;
			}
			set
			{
				_percentOfTotal = value;
			}
		}						
		#endregion
	}
}
