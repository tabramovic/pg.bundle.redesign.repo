﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Printing;
using System.Data;
using System.Text;
using System.Windows.Forms;

//Added
using ProfitGrabber.Common;
using ProfitGrabber.Commercial.Objects;
using ProfitGrabber.Commercial.Generators;

namespace ProfitGrabber.Commercial
{
    public partial class Income_Multifamily : UserControl
    {
        IAutoSave _autoSave = null;
        const string CurrFormatter = "#,##0.00";
        string _address = string.Empty;

        IncomeGenerator _iGen = new IncomeGenerator();

        public Income_Multifamily()
        {
            InitializeComponent();
            InitIncomeListViews();		
            AddEventHandlers();
            HandleListViewExes();
        }

        public void SetAutoSaveInterface(IAutoSave iAutoSaveIfc)
        {
            _autoSave = iAutoSaveIfc;
            AddAutoSaveEventHandlers();
        }

        void AddEventHandlers()
        {
            _I_unitTypeSummary.OnListViewExItemChanged += new ProfitGrabber.Commercial.ListViewEx.ListViewExItemChanged(I_unitTypeSummary_OnListViewExItemChanged);
            _I_rentRoll.OnListViewExItemChanged += new ProfitGrabber.Commercial.ListViewEx.ListViewExItemChanged(I_rentRoll_OnListViewExItemChanged);

            _CalculateFromUnitTypeSummary.Click += new EventHandler(On_CalculateFromUnitTypeSummary_Click);
            //_calculateFromRentRoll.Click += new EventHandler(On_CalculateFromRentRoll_Click);

            _printIncomeSummary.Click += new EventHandler(On_PrintReports_Click);
            _printRentRoll.Click += new EventHandler(On_PrintReports_Click);

            this._I_PI_VacancyOrCreditLossPerc.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.I_P_VacancyPerc_OnKeyPressed);
            this._I_PI_VacancyOrCreditLossPerc.Leave += new System.EventHandler(this.I_P_VacancyPerc_OnTextBoxLeave);
            this._I_PI_OtherIncome.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.I_P_OtherIncome_OnKeyPressed);
            this._I_PI_OtherIncome.Leave += new System.EventHandler(this.I_P_OtherIncome_OnTextBoxLeave);
            this._I_PI_GrossRent.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.I_P_GSI_OnKeyPressed);
            this._I_PI_GrossRent.Leave += new System.EventHandler(this.I_P_GSI_OnTextBoxLeave);
            this._I_AI_VacancyOrCreditLossPerc.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.I_AI_LossPerc_OnKeyPressed);
            this._I_AI_VacancyOrCreditLossPerc.Leave += new System.EventHandler(this.I_AI_LossPerc_OnTextBoxLeave);
            this._I_AI_VacancyOrCreditLossPerc.Enter += new System.EventHandler(this.OnTextBoxEnter);
            this._I_AI_VacancyOrCreditLoss.Enter += new System.EventHandler(this.OnTextBoxEnter);
            this._I_AI_GrossRent.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.I_AI_GSI_OnKeyPressed);
            this._I_AI_GrossRent.Leave += new System.EventHandler(this.I_AI_GSI_OnTextBoxLeave);
            this._I_AI_GrossRent.Enter += new System.EventHandler(this.OnTextBoxEnter);
            this._I_OI_Other.KeyDown += new System.Windows.Forms.KeyEventHandler(this.I_OI_KeyDown);
            this._I_OI_Other.Leave += new System.EventHandler(this.I_OnTextBoxLeave);
            this._I_OI_Other.Enter += new System.EventHandler(this.OnTextBoxEnter);
            this._I_OI_Vending.KeyDown += new System.Windows.Forms.KeyEventHandler(this.I_OI_KeyDown);
            this._I_OI_Vending.Leave += new System.EventHandler(this.I_OnTextBoxLeave);
            this._I_OI_Vending.Enter += new System.EventHandler(this.OnTextBoxEnter);
            this._I_OI_Laundry.KeyDown += new System.Windows.Forms.KeyEventHandler(this.I_OI_KeyDown);
            this._I_OI_Laundry.Leave += new System.EventHandler(this.I_OnTextBoxLeave);
            this._I_OI_Laundry.Enter += new System.EventHandler(this.OnTextBoxEnter);
            this._I_OI_Parking.KeyDown += new System.Windows.Forms.KeyEventHandler(this.I_OI_KeyDown);
            this._I_OI_Parking.Leave += new System.EventHandler(this.I_OnTextBoxLeave);
            this._I_OI_Parking.Enter += new System.EventHandler(this.OnTextBoxEnter);
        }

        void RemoveEventHandlers()
        {
            _I_unitTypeSummary.OnListViewExItemChanged -= new ProfitGrabber.Commercial.ListViewEx.ListViewExItemChanged(I_unitTypeSummary_OnListViewExItemChanged);
            _I_rentRoll.OnListViewExItemChanged -= new ProfitGrabber.Commercial.ListViewEx.ListViewExItemChanged(I_rentRoll_OnListViewExItemChanged);

            _CalculateFromUnitTypeSummary.Click -= new EventHandler(On_CalculateFromUnitTypeSummary_Click);
            //_calculateFromRentRoll.Click -= new EventHandler(On_CalculateFromRentRoll_Click);

            _printIncomeSummary.Click -= new EventHandler(On_PrintReports_Click);
            _printRentRoll.Click -= new EventHandler(On_PrintReports_Click);

            this._I_PI_VacancyOrCreditLossPerc.KeyPress -= new System.Windows.Forms.KeyPressEventHandler(this.I_P_VacancyPerc_OnKeyPressed);
            this._I_PI_VacancyOrCreditLossPerc.Leave -= new System.EventHandler(this.I_P_VacancyPerc_OnTextBoxLeave);
            this._I_PI_OtherIncome.KeyPress -= new System.Windows.Forms.KeyPressEventHandler(this.I_P_OtherIncome_OnKeyPressed);
            this._I_PI_OtherIncome.Leave -= new System.EventHandler(this.I_P_OtherIncome_OnTextBoxLeave);
            this._I_PI_GrossRent.KeyPress -= new System.Windows.Forms.KeyPressEventHandler(this.I_P_GSI_OnKeyPressed);
            this._I_PI_GrossRent.Leave -= new System.EventHandler(this.I_P_GSI_OnTextBoxLeave);
            this._I_AI_VacancyOrCreditLossPerc.KeyPress -= new System.Windows.Forms.KeyPressEventHandler(this.I_AI_LossPerc_OnKeyPressed);
            this._I_AI_VacancyOrCreditLossPerc.Leave -= new System.EventHandler(this.I_AI_LossPerc_OnTextBoxLeave);
            this._I_AI_VacancyOrCreditLossPerc.Enter -= new System.EventHandler(this.OnTextBoxEnter);
            this._I_AI_VacancyOrCreditLoss.Enter -= new System.EventHandler(this.OnTextBoxEnter);
            this._I_AI_GrossRent.KeyPress -= new System.Windows.Forms.KeyPressEventHandler(this.I_AI_GSI_OnKeyPressed);
            this._I_AI_GrossRent.Leave -= new System.EventHandler(this.I_AI_GSI_OnTextBoxLeave);
            this._I_AI_GrossRent.Enter -= new System.EventHandler(this.OnTextBoxEnter);
            this._I_OI_Other.KeyDown -= new System.Windows.Forms.KeyEventHandler(this.I_OI_KeyDown);
            this._I_OI_Other.Leave -= new System.EventHandler(this.I_OnTextBoxLeave);
            this._I_OI_Other.Enter -= new System.EventHandler(this.OnTextBoxEnter);
            this._I_OI_Vending.KeyDown -= new System.Windows.Forms.KeyEventHandler(this.I_OI_KeyDown);
            this._I_OI_Vending.Leave -= new System.EventHandler(this.I_OnTextBoxLeave);
            this._I_OI_Vending.Enter -= new System.EventHandler(this.OnTextBoxEnter);
            this._I_OI_Laundry.KeyDown -= new System.Windows.Forms.KeyEventHandler(this.I_OI_KeyDown);
            this._I_OI_Laundry.Leave -= new System.EventHandler(this.I_OnTextBoxLeave);
            this._I_OI_Laundry.Enter -= new System.EventHandler(this.OnTextBoxEnter);
            this._I_OI_Parking.KeyDown -= new System.Windows.Forms.KeyEventHandler(this.I_OI_KeyDown);
            this._I_OI_Parking.Leave -= new System.EventHandler(this.I_OnTextBoxLeave);
            this._I_OI_Parking.Enter -= new System.EventHandler(this.OnTextBoxEnter);
        }

        protected override void OnLoad(EventArgs e)
        {
            //base.OnLoad(e);
            HandleListViewExes();
        }

        #region HANDLE LIST VIEWS
        void HandleListViewExes()
        {
            //UNIT TYPE SUMMARY
            _I_unitTypeSummary.AddSubItem = true;
            _I_unitTypeSummary.HideComboAfterSelChange = true;

            //Add EDITABLE cells
            _I_unitTypeSummary.AddEditableCell(-1, 0);
            _I_unitTypeSummary.AddEditableCell(-1, 3);
            _I_unitTypeSummary.AddEditableCell(-1, 4);
            _I_unitTypeSummary.AddEditableCell(-1, 5);
            _I_unitTypeSummary.AddEditableCell(-1, 6);
            _I_unitTypeSummary.AddEditableCell(-1, 7);
            _I_unitTypeSummary.AddEditableCell(-1, 8);
            _I_unitTypeSummary.AddEditableCell(-1, 10);
            _I_unitTypeSummary.AddEditableCell(-1, 11);

            //Create data for COMBOBOX
            StringCollection bedrooms = new StringCollection();
            bedrooms.AddRange(new string[] { string.Empty, "0", "1", "2", "3", "4" });

            StringCollection baths = new StringCollection();
            baths.AddRange(new string[] { string.Empty, "1", "1.5", "2", "2.5", "3" });

            //Set COMBOBOXES
            _I_unitTypeSummary.AddComboBoxCell(-1, 1, bedrooms);
            _I_unitTypeSummary.AddComboBoxCell(-1, 2, baths);

            //RENT ROLL
            _I_rentRoll.AddSubItem = true;
            _I_rentRoll.HideComboAfterSelChange = true;

            //Add EDITABLE cells
            _I_rentRoll.AddEditableCell(-1, 0);
            _I_rentRoll.AddEditableCell(-1, 1);
            _I_rentRoll.AddEditableCell(-1, 3);
            _I_rentRoll.AddEditableCell(-1, 4);
            _I_rentRoll.AddEditableCell(-1, 5);
            _I_rentRoll.AddEditableCell(-1, 6);
            _I_rentRoll.AddEditableCell(-1, 7);
            _I_rentRoll.AddEditableCell(-1, 8);
            _I_rentRoll.AddEditableCell(-1, 9);
            _I_rentRoll.AddEditableCell(-1, 10);
            _I_rentRoll.AddEditableCell(-1, 11);

            //Create data for COMBOBOX
            StringCollection bedBath = new StringCollection();
            bedBath.AddRange(new string[] { string.Empty, "0/1", "1/1", "1/2", "2/1", "2/2", "2/3", "3/1", "3/2", "4/1", "4/2", "4/3" });

            //Set COMBOBOXES
            _I_rentRoll.AddComboBoxCell(-1, 2, bedBath);

        }
        #endregion

        #region INIT LISTVIEW
        void InitIncomeListViews()
        {
            for (int i = 0; i < IncomeObject.UTSMaxItems; i++)
            {
                ListViewItem lvi = new ListViewItem();
                lvi.SubItems.Add(string.Empty);
                lvi.SubItems.Add(string.Empty);
                lvi.SubItems.Add(string.Empty);
                lvi.SubItems.Add(string.Empty);
                lvi.SubItems.Add(string.Empty);
                lvi.SubItems.Add(string.Empty);
                lvi.SubItems.Add(string.Empty);
                lvi.SubItems.Add(string.Empty);
                lvi.SubItems.Add(string.Empty);
                lvi.SubItems.Add(string.Empty);
                lvi.SubItems.Add(string.Empty);
                lvi.SubItems.Add(string.Empty);

                //Add Tag
                lvi.Tag = i;

                //Color readonly fields
                _I_unitTypeSummary.Items.Add(lvi);
                _I_unitTypeSummary.Items[i].UseItemStyleForSubItems = false;
                _I_unitTypeSummary.Items[i].SubItems[9].BackColor = Color.LightGray;
                _I_unitTypeSummary.Items[i].SubItems[12].BackColor = Color.LightGray;
            }

            for (int i = 0; i < IncomeObject.RRMaxItems; i++)
            {
                ListViewItem lvi = new ListViewItem();
                lvi.SubItems.Add(string.Empty);
                lvi.SubItems.Add(string.Empty);
                lvi.SubItems.Add(string.Empty);
                lvi.SubItems.Add(string.Empty);
                lvi.SubItems.Add(string.Empty);
                lvi.SubItems.Add(string.Empty);
                lvi.SubItems.Add(string.Empty);
                lvi.SubItems.Add(string.Empty);
                lvi.SubItems.Add(string.Empty);
                lvi.SubItems.Add(string.Empty);
                lvi.SubItems.Add(string.Empty);

                //Add Tag
                lvi.Tag = i;

                //Color readonly fields
                _I_rentRoll.Items.Add(lvi);
                _I_rentRoll.Items[i].UseItemStyleForSubItems = false;
                _I_rentRoll.Items[i].SubItems[7].BackColor = Color.LightGray;
            }
        }
        #endregion

        #region INCOME = MAP TO OBJECT MODEL
        private void I_unitTypeSummary_OnListViewExItemChanged(int row, int col, string val)
        {
            if (-1 != row && -1 != col)
            {
                //MAP TO OBJECT MODEL
                switch (col)
                {
                    case 0:
                        IncomeObject.Instance.UnitTypeSummaries[row].NrOfUnitsProperty = val;
                        break;

                    case 1:
                        IncomeObject.Instance.UnitTypeSummaries[row].NrOfBedroomsProperty = val;
                        break;

                    case 2:
                        IncomeObject.Instance.UnitTypeSummaries[row].NrOfBathsProperty = val;
                        break;

                    case 3:
                        IncomeObject.Instance.UnitTypeSummaries[row].FloorPlanProperty = val;
                        break;

                    case 4:
                        IncomeObject.Instance.UnitTypeSummaries[row].OccupancyProperty = val;
                        break;

                    case 5:
                        IncomeObject.Instance.UnitTypeSummaries[row].SF_LowProperty = val;
                        _I_unitTypeSummary.Items[row].SubItems[9].Text = IncomeObject.Instance.UnitTypeSummaries[row].ActualRentPerItem.ToString(CurrFormatter);
                        _I_unitTypeSummary.Items[row].SubItems[12].Text = IncomeObject.Instance.UnitTypeSummaries[row].MarketRentPerItem.ToString(CurrFormatter);

                        break;

                    case 6:
                        IncomeObject.Instance.UnitTypeSummaries[row].SF_HighProperty = val;
                        _I_unitTypeSummary.Items[row].SubItems[9].Text = IncomeObject.Instance.UnitTypeSummaries[row].ActualRentPerItem.ToString(CurrFormatter);
                        _I_unitTypeSummary.Items[row].SubItems[12].Text = IncomeObject.Instance.UnitTypeSummaries[row].MarketRentPerItem.ToString(CurrFormatter);
                        break;

                    case 7:
                        IncomeObject.Instance.UnitTypeSummaries[row].AR_LowProperty = val;
                        _I_unitTypeSummary.Items[row].SubItems[9].Text = IncomeObject.Instance.UnitTypeSummaries[row].ActualRentPerItem.ToString(CurrFormatter);
                        break;

                    case 8:
                        IncomeObject.Instance.UnitTypeSummaries[row].AR_HighProperty = val;
                        _I_unitTypeSummary.Items[row].SubItems[9].Text = IncomeObject.Instance.UnitTypeSummaries[row].ActualRentPerItem.ToString(CurrFormatter);
                        break;

                    case 10:
                        IncomeObject.Instance.UnitTypeSummaries[row].MR_LowProperty = val;
                        break;

                    case 11:
                        IncomeObject.Instance.UnitTypeSummaries[row].MR_HighProperty = val;
                        _I_unitTypeSummary.Items[row].SubItems[12].Text = IncomeObject.Instance.UnitTypeSummaries[row].MarketRentPerItem.ToString(CurrFormatter);
                        break;
                }

                _I_UTS_TotalUnits.Text = IncomeObject.Instance.UTS_TotalNumberOfUnits.ToString();
                _I_UTS_TotalOccupancy.Text = IncomeObject.Instance.UTS_WeightedAverageTotalOccupancy.ToString("0.00") + "%";
                _I_UTS_Total_SQ_Low.Text = IncomeObject.Instance.UTS_WeightedAverageTotalSqFtLow.ToString(CurrFormatter);
                _I_UTS_Total_SQ_High.Text = IncomeObject.Instance.UTS_WeightedAverageTotalSqFtHigh.ToString(CurrFormatter);
                _I_UTS_Total_AR_Low.Text = IncomeObject.Instance.UTS_SumTotalARLow.ToString(CurrFormatter);
                _I_UTS_Total_AR_High.Text = IncomeObject.Instance.UTS_SumTotalARHigh.ToString(CurrFormatter);
                _I_UTS_Total_AR_UsdPerSqFt.Text = IncomeObject.Instance.UTS_WeightedAverageARUSDPerSqFt.ToString(CurrFormatter);
                _I_UTS_Total_MR_Low.Text = IncomeObject.Instance.UTS_SumTotalMRLow.ToString(CurrFormatter);
                _I_UTS_Total_MR_High.Text = IncomeObject.Instance.UTS_SumTotalMRHigh.ToString(CurrFormatter);
                _I_UTS_Total_MR_USDperSqFt.Text = IncomeObject.Instance.UTS_WeightedAverageMRUSDPerSqFt.ToString(CurrFormatter);
            }
        }

        private void I_rentRoll_OnListViewExItemChanged(int row, int col, string val)
        {
            if (-1 != row && -1 != col)
            {
                //MAP TO OBJECT MODEL
                switch (col)
                {
                    case 0:
                        IncomeObject.Instance.RentRolls[row].UnitNrProperty = val;
                        break;

                    case 1:
                        IncomeObject.Instance.RentRolls[row].TennantNameProperty = val;
                        break;

                    case 2:
                        IncomeObject.Instance.RentRolls[row].BedBathProperty = val;
                        break;

                    case 3:
                        IncomeObject.Instance.RentRolls[row].SqftProperty = val;
                        _I_RR_TotalSqFt.Text = IncomeObject.Instance.RR_TotalSqft.ToString(CurrFormatter);
                        break;

                    case 4:
                        IncomeObject.Instance.RentRolls[row].SecDepProperty = val;
                        _I_RR_TotalSecDep.Text = IncomeObject.Instance.RR_TotalSecDep.ToString(CurrFormatter);
                        break;

                    case 5:
                        IncomeObject.Instance.RentRolls[row].TennantRentProperty = val;
                        _I_rentRoll.Items[row].SubItems[7].Text = IncomeObject.Instance.RentRolls[row].TotalRentProperty.ToString(CurrFormatter);
                        _I_RR_TotalTennantRent.Text = IncomeObject.Instance.RR_TotalTennantRent.ToString(CurrFormatter);
                        _I_RR_TotalTotalRent.Text = IncomeObject.Instance.RR_TotalTotalRent.ToString(CurrFormatter);
                        break;

                    case 6:
                        IncomeObject.Instance.RentRolls[row].Sec8RentProperty = val;
                        _I_rentRoll.Items[row].SubItems[7].Text = IncomeObject.Instance.RentRolls[row].TotalRentProperty.ToString(CurrFormatter);
                        _I_RR_TotalSec8Rent.Text = IncomeObject.Instance.RR_TotalSec8Rent.ToString(CurrFormatter);
                        _I_RR_TotalTotalRent.Text = IncomeObject.Instance.RR_TotalTotalRent.ToString(CurrFormatter);
                        break;

                    case 8:
                        IncomeObject.Instance.RentRolls[row].MarketRentProperty = val;
                        _I_RR_TotalMarketRent.Text = IncomeObject.Instance.RR_TotalMarketRent.ToString(CurrFormatter);
                        break;

                    case 9:
                        IncomeObject.Instance.RentRolls[row].OccupDateProperty = val;
                        break;

                    case 10:
                        IncomeObject.Instance.RentRolls[row].LeaseTermProperty = val;
                        break;

                    case 11:
                        IncomeObject.Instance.RentRolls[row].CommentProperty = val;
                        break;
                }
            }
        }
        #endregion

        #region INCOME = CALCULATION
        private void On_CalculateFromUnitTypeSummary_Click(object sender, EventArgs e)
        {
            decimal grossRent;
            decimal vacancyOrCreditLoss;
            decimal vacancyOrCreditLossPerc;
            decimal otherIncome;
            decimal operatingIncome;

            IncomeObject.Instance.ActualIncomeObject.CalculateValuesFromUTS(out grossRent, out vacancyOrCreditLoss, out vacancyOrCreditLossPerc, out otherIncome, out operatingIncome);

            IncomeObject.Instance.ActualIncomeObject.GrossRent = _I_AI_GrossRent.Text = grossRent.ToString(CurrFormatter);
            IncomeObject.Instance.ActualIncomeObject.VacancyOrCreditLoss = _I_AI_VacancyOrCreditLoss.Text = vacancyOrCreditLoss.ToString(CurrFormatter);
            IncomeObject.Instance.ActualIncomeObject.VacancyOrCreditLossPerc = _I_AI_VacancyOrCreditLossPerc.Text = vacancyOrCreditLossPerc.ToString(CurrFormatter);
            _I_AI_OtherIncome.Text = otherIncome.ToString(CurrFormatter);
            IncomeObject.Instance.ActualIncomeObject.OtherIncomeProperty = otherIncome;


            decimal gsi;
            IncomeObject.Instance.ProformaIncomeObject.CalculateFromUnitTypeSummary(out gsi);
            _I_PI_GrossRent.Text = gsi.ToString(CurrFormatter);
            IncomeObject.Instance.ProformaIncomeObject.GrossRentProperty = gsi.ToString();

            //FORCE RECALC:
            HandleActualIncomes_LossPerc();
        }
        #endregion

        #region INCOME TEXTBOX HANDLERS
        private void I_AI_GSI_OnTextBoxLeave(object sender, System.EventArgs e)
        {
            HandleActualIncomes_GSI();
        }

        private void I_AI_GSI_OnKeyPressed(object sender, System.Windows.Forms.KeyPressEventArgs e)
        {
            if (e.KeyChar == 13 || e.KeyChar == 27)
                HandleActualIncomes_GSI();
        }

        void HandleActualIncomes_GSI()
        {
            //SET NEW PROPERTY (take from GUI)
            IncomeObject.Instance.ActualIncomeObject.GrossRent = _I_AI_GrossRent.Text;

            try
            {
                //TRY TO CONVERT TO CURR FORMATTER
                decimal dTemp = decimal.Parse(_I_AI_GrossRent.Text);
                IncomeObject.Instance.ActualIncomeObject.GrossRent = dTemp.ToString(CurrFormatter);
                _I_AI_GrossRent.Text = IncomeObject.Instance.ActualIncomeObject.GrossRent;
            }
            catch
            {
                //AS IS
                IncomeObject.Instance.ActualIncomeObject.GrossRent = _I_AI_GrossRent.Text;
            }

            IncomeObject.Instance.ActualIncomeObject.VacancyOrCreditLossPerc = _I_AI_VacancyOrCreditLossPerc.Text;


            //CALCULATE THE OTHER and set to GUI			
            _I_AI_VacancyOrCreditLoss.Text = IncomeObject.Instance.ActualIncomeObject.VacancyOrCreditLossProperty.ToString(CurrFormatter);
            _I_AI_OtherIncome.Text = IncomeObject.Instance.ActualIncomeObject.OtherIncomeProperty.ToString(CurrFormatter);
            _I_AI_OperatingIncome.Text = IncomeObject.Instance.ActualIncomeObject.OperatingIncomeProperty.ToString(CurrFormatter);

            //FORCE 0.00 FORMAT
            _I_AI_GrossRent.Text = IncomeObject.Instance.ActualIncomeObject.GrossRentProperty.ToString(CurrFormatter);
        }

        private void I_AI_LossPerc_OnTextBoxLeave(object sender, System.EventArgs e)
        {
            HandleActualIncomes_LossPerc();
        }

        private void I_AI_LossPerc_OnKeyPressed(object sender, System.Windows.Forms.KeyPressEventArgs e)
        {
            if (e.KeyChar == 13 || e.KeyChar == 27)
                HandleActualIncomes_LossPerc();
        }

        void HandleActualIncomes_LossPerc()
        {
            //SET NEW PROPERTY (take from GUI)			
            try
            {
                //TRY TO CONVERT TO CURR FORMATTER
                decimal dTemp = decimal.Parse(_I_AI_GrossRent.Text);
                IncomeObject.Instance.ActualIncomeObject.GrossRent = dTemp.ToString(CurrFormatter);
                _I_AI_GrossRent.Text = IncomeObject.Instance.ActualIncomeObject.GrossRent;
            }
            catch
            {
                //AS IS
                IncomeObject.Instance.ActualIncomeObject.GrossRent = _I_AI_GrossRent.Text;
            }

            IncomeObject.Instance.ActualIncomeObject.VacancyOrCreditLossPerc = _I_AI_VacancyOrCreditLossPerc.Text;

            //CALCULATE THE OTHER and set to GUI
            _I_AI_VacancyOrCreditLoss.Text = (IncomeObject.Instance.ActualIncomeObject.VacancyOrCreditLossProperty).ToString(CurrFormatter);
            _I_AI_OtherIncome.Text = IncomeObject.Instance.ActualIncomeObject.OtherIncomeProperty.ToString(CurrFormatter);
            _I_AI_OperatingIncome.Text = IncomeObject.Instance.ActualIncomeObject.OperatingIncomeProperty.ToString(CurrFormatter);

            //FORCE 0.00 FORMAT
            _I_AI_GrossRent.Text = IncomeObject.Instance.ActualIncomeObject.GrossRentProperty.ToString(CurrFormatter);
        }

        private void I_OI_KeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                HandleOtherIncome();
        }

        private void I_OnTextBoxLeave(object sender, System.EventArgs e)
        {
            HandleOtherIncome();
        }

        void HandleOtherIncome()
        {
            //SET PROPERTIES
            try
            {
                //TRY TO CONVERT TO CURR FORMATTER
                decimal dTemp = decimal.Parse(_I_OI_Parking.Text);
                IncomeObject.Instance.OtherIncomeObject.ParkingProperty = dTemp.ToString(CurrFormatter);
                _I_OI_Parking.Text = IncomeObject.Instance.OtherIncomeObject.ParkingProperty;
            }
            catch
            {
                //AS IS
                IncomeObject.Instance.OtherIncomeObject.ParkingProperty = _I_OI_Parking.Text;
            }

            try
            {
                decimal dTemp = decimal.Parse(_I_OI_Laundry.Text);
                IncomeObject.Instance.OtherIncomeObject.LaundryProperty = dTemp.ToString(CurrFormatter);
                _I_OI_Laundry.Text = IncomeObject.Instance.OtherIncomeObject.LaundryProperty;
            }
            catch
            {
                IncomeObject.Instance.OtherIncomeObject.LaundryProperty = _I_OI_Laundry.Text;
            }

            try
            {
                decimal dTemp = decimal.Parse(_I_OI_Vending.Text);
                IncomeObject.Instance.OtherIncomeObject.VendingProperty = dTemp.ToString(CurrFormatter);
                _I_OI_Vending.Text = IncomeObject.Instance.OtherIncomeObject.VendingProperty;
            }
            catch
            {
                IncomeObject.Instance.OtherIncomeObject.VendingProperty = _I_OI_Vending.Text;
            }

            try
            {
                decimal dTemp = decimal.Parse(_I_OI_Other.Text);
                IncomeObject.Instance.OtherIncomeObject.OtherProperty = dTemp.ToString(CurrFormatter);
                _I_OI_Other.Text = IncomeObject.Instance.OtherIncomeObject.OtherProperty;
            }
            catch
            {
                IncomeObject.Instance.OtherIncomeObject.OtherProperty = _I_OI_Other.Text;
            }

            //SET SUM
            _I_OI_Total.Text = IncomeObject.Instance.OtherIncomeObject.TotalProperty.ToString(CurrFormatter);
        }

        private void I_P_GSI_OnTextBoxLeave(object sender, System.EventArgs e)
        {
            HandleActualIncomes_P_GSI();
        }

        private void I_P_GSI_OnKeyPressed(object sender, System.Windows.Forms.KeyPressEventArgs e)
        {
            if (e.KeyChar == 13 || e.KeyChar == 27)
                HandleActualIncomes_P_GSI();
        }

        void HandleActualIncomes_P_GSI()
        {
            //SET PROPERTIES
            IncomeObject.Instance.ProformaIncomeObject.GrossRentProperty = _I_PI_GrossRent.Text;

            //CALCULATE THE OTHER and set to GUI
            _I_PI_VacancyOrCreditLoss.Text = IncomeObject.Instance.ProformaIncomeObject.VacancyOrCreditloss.ToString(CurrFormatter);
            _I_PI_OperatingIncome.Text = IncomeObject.Instance.ProformaIncomeObject.OperatingIncome.ToString(CurrFormatter);
            _I_PI_OtherIncome.Text = IncomeObject.Instance.ProformaIncomeObject.OtherIncome.ToString(CurrFormatter);

            //FORCE 0.00 FORMAT
            _I_PI_GrossRent.Text = IncomeObject.Instance.ProformaIncomeObject.GrossRent.ToString(CurrFormatter);
        }

        private void I_P_VacancyPerc_OnTextBoxLeave(object sender, System.EventArgs e)
        {
            HandleActualIncomes_P_VacancyPerc();
        }

        private void I_P_VacancyPerc_OnKeyPressed(object sender, System.Windows.Forms.KeyPressEventArgs e)
        {
            if (e.KeyChar == 13 || e.KeyChar == 27)
                HandleActualIncomes_P_VacancyPerc();
        }

        void HandleActualIncomes_P_VacancyPerc()
        {
            //SET PROPERTIES
            IncomeObject.Instance.ProformaIncomeObject.VacancyOrCreditLossPercProperty = _I_PI_VacancyOrCreditLossPerc.Text;

            //CALCULATE THE OTHER and set to GUI
            _I_PI_VacancyOrCreditLoss.Text = IncomeObject.Instance.ProformaIncomeObject.VacancyOrCreditloss.ToString(CurrFormatter);
            _I_PI_OperatingIncome.Text = IncomeObject.Instance.ProformaIncomeObject.OperatingIncome.ToString(CurrFormatter);
            _I_PI_OtherIncome.Text = IncomeObject.Instance.ProformaIncomeObject.OtherIncome.ToString(CurrFormatter);
        }

        private void I_P_OtherIncome_OnTextBoxLeave(object sender, System.EventArgs e)
        {
            HandleActualIncomes_P_OtherPerc();
        }

        private void I_P_OtherIncome_OnKeyPressed(object sender, System.Windows.Forms.KeyPressEventArgs e)
        {
            if (e.KeyChar == 13 || e.KeyChar == 27)
                HandleActualIncomes_P_OtherPerc();
        }

        void HandleActualIncomes_P_OtherPerc()
        {
            //SET PROPERTIES
            IncomeObject.Instance.ProformaIncomeObject.OtherIncomeProperty = _I_PI_OtherIncome.Text;

            //CALCULATE THE OTHER and set to GUI
            _I_PI_VacancyOrCreditLoss.Text = IncomeObject.Instance.ProformaIncomeObject.VacancyOrCreditloss.ToString(CurrFormatter);
            _I_PI_OperatingIncome.Text = IncomeObject.Instance.ProformaIncomeObject.OperatingIncome.ToString(CurrFormatter);
            _I_PI_OtherIncome.Text = IncomeObject.Instance.ProformaIncomeObject.OtherIncome.ToString(CurrFormatter);
        }
        #endregion

        #region SET DATA
        public void SetData(IncomeObject obj)
        {
            try
            {
                //UNIT TYPE SUMMARY
                for (int i = 0; i < obj.UnitTypeSummaries.Length; i++)
                {
                    _I_unitTypeSummary.Items[i].SubItems[0].Text = obj.UnitTypeSummaries[i].NrOfUnitsProperty;
                    _I_unitTypeSummary.Items[i].SubItems[1].Text = obj.UnitTypeSummaries[i].NrOfBedroomsProperty;
                    _I_unitTypeSummary.Items[i].SubItems[2].Text = obj.UnitTypeSummaries[i].NrOfBathsProperty;
                    _I_unitTypeSummary.Items[i].SubItems[3].Text = obj.UnitTypeSummaries[i].FloorPlanProperty;
                    _I_unitTypeSummary.Items[i].SubItems[4].Text = obj.UnitTypeSummaries[i].OccupancyProperty;
                    _I_unitTypeSummary.Items[i].SubItems[5].Text = obj.UnitTypeSummaries[i].SF_LowProperty;
                    _I_unitTypeSummary.Items[i].SubItems[6].Text = obj.UnitTypeSummaries[i].SF_HighProperty;
                    _I_unitTypeSummary.Items[i].SubItems[7].Text = obj.UnitTypeSummaries[i].AR_LowProperty;
                    _I_unitTypeSummary.Items[i].SubItems[8].Text = obj.UnitTypeSummaries[i].AR_HighProperty;
                    _I_unitTypeSummary.Items[i].SubItems[9].Text = obj.UnitTypeSummaries[i].ActualRentPerItem.ToString(CurrFormatter);
                    _I_unitTypeSummary.Items[i].SubItems[10].Text = obj.UnitTypeSummaries[i].MR_LowProperty;
                    _I_unitTypeSummary.Items[i].SubItems[11].Text = obj.UnitTypeSummaries[i].MR_HighProperty;
                    _I_unitTypeSummary.Items[i].SubItems[12].Text = obj.UnitTypeSummaries[i].MarketRentPerItem.ToString(CurrFormatter);
                }

                _I_UTS_TotalUnits.Text = obj.UTS_TotalNumberOfUnits.ToString();
                _I_UTS_TotalOccupancy.Text = obj.UTS_WeightedAverageTotalOccupancy.ToString("0.00") + "%";
                _I_UTS_Total_SQ_Low.Text = obj.UTS_WeightedAverageTotalSqFtLow.ToString(CurrFormatter);
                _I_UTS_Total_SQ_High.Text = obj.UTS_WeightedAverageTotalSqFtHigh.ToString(CurrFormatter);
                _I_UTS_Total_AR_Low.Text = obj.UTS_SumTotalARLow.ToString(CurrFormatter);
                _I_UTS_Total_AR_High.Text = obj.UTS_SumTotalARHigh.ToString(CurrFormatter);
                _I_UTS_Total_AR_UsdPerSqFt.Text = obj.UTS_WeightedAverageARUSDPerSqFt.ToString(CurrFormatter);
                _I_UTS_Total_MR_Low.Text = obj.UTS_SumTotalMRLow.ToString(CurrFormatter);
                _I_UTS_Total_MR_High.Text = obj.UTS_SumTotalMRHigh.ToString(CurrFormatter);
                _I_UTS_Total_MR_USDperSqFt.Text = obj.UTS_WeightedAverageMRUSDPerSqFt.ToString(CurrFormatter);

                //RENT ROLL
                for (int i = 0; i < IncomeObject.Instance.RentRolls.Length; i++)
                {
                    _I_rentRoll.Items[i].SubItems[0].Text = obj.RentRolls[i].UnitNrProperty;
                    _I_rentRoll.Items[i].SubItems[1].Text = obj.RentRolls[i].TennantNameProperty;
                    _I_rentRoll.Items[i].SubItems[2].Text = obj.RentRolls[i].BedBathProperty;
                    _I_rentRoll.Items[i].SubItems[3].Text = obj.RentRolls[i].SqftProperty;
                    _I_rentRoll.Items[i].SubItems[4].Text = obj.RentRolls[i].SecDepProperty;
                    _I_rentRoll.Items[i].SubItems[5].Text = obj.RentRolls[i].TennantRentProperty;
                    _I_rentRoll.Items[i].SubItems[6].Text = obj.RentRolls[i].Sec8RentProperty;
                    _I_rentRoll.Items[i].SubItems[7].Text = obj.RentRolls[i].TotalRentProperty.ToString(CurrFormatter);
                    _I_rentRoll.Items[i].SubItems[8].Text = obj.RentRolls[i].MarketRentProperty;
                    _I_rentRoll.Items[i].SubItems[9].Text = obj.RentRolls[i].OccupDateProperty;
                    _I_rentRoll.Items[i].SubItems[10].Text = obj.RentRolls[i].LeaseTermProperty;
                    _I_rentRoll.Items[i].SubItems[11].Text = obj.RentRolls[i].CommentProperty;

                }

                _I_RR_TotalSqFt.Text = obj.RR_TotalSqft.ToString(CurrFormatter);
                _I_RR_TotalSecDep.Text = obj.RR_TotalSecDep.ToString(CurrFormatter);

                _I_RR_TotalTennantRent.Text = obj.RR_TotalTennantRent.ToString(CurrFormatter);
                _I_RR_TotalTotalRent.Text = obj.RR_TotalTotalRent.ToString(CurrFormatter);

                _I_RR_TotalSec8Rent.Text = obj.RR_TotalSec8Rent.ToString(CurrFormatter);
                _I_RR_TotalTotalRent.Text = obj.RR_TotalTotalRent.ToString(CurrFormatter);
                _I_RR_TotalMarketRent.Text = obj.RR_TotalMarketRent.ToString(CurrFormatter);

                _I_AI_OtherIncome.Text = obj.ActualIncomeObject.OtherIncomeProperty.ToString(CurrFormatter); ;
                _I_PI_GrossRent.Text = IncomeObject.Instance.ProformaIncomeObject.GrossRentProperty;

                //OTHER INCOME
                _I_OI_Parking.Text = obj.OtherIncomeObject.ParkingProperty;
                _I_OI_Laundry.Text = obj.OtherIncomeObject.LaundryProperty;
                _I_OI_Vending.Text = obj.OtherIncomeObject.VendingProperty;
                _I_OI_Other.Text = obj.OtherIncomeObject.OtherProperty;
                _I_OI_Total.Text = obj.OtherIncomeObject.TotalProperty.ToString(CurrFormatter);

                //ACTUAL INCOME
                _I_AI_VacancyOrCreditLossPerc.Text = obj.ActualIncomeObject.VacancyOrCreditLossPerc;
                _I_AI_VacancyOrCreditLoss.Text = obj.ActualIncomeObject.VacancyOrCreditLossProperty.ToString(CurrFormatter);
                _I_AI_OtherIncome.Text = obj.ActualIncomeObject.OtherIncomeProperty.ToString(CurrFormatter);
                _I_AI_OperatingIncome.Text = obj.ActualIncomeObject.OperatingIncomeProperty.ToString(CurrFormatter);
                _I_AI_GrossRent.Text = obj.ActualIncomeObject.GrossRentProperty.ToString(CurrFormatter);

                //PROFORMA INCOME
                _I_PI_VacancyOrCreditLoss.Text = obj.ProformaIncomeObject.VacancyOrCreditloss.ToString(CurrFormatter);
                _I_PI_VacancyOrCreditLossPerc.Text = (obj.ProformaIncomeObject.VacancyOrCreditlossPerc * 100).ToString(CurrFormatter);
                _I_PI_OperatingIncome.Text = obj.ProformaIncomeObject.OperatingIncome.ToString(CurrFormatter);
                _I_PI_OtherIncome.Text = obj.ProformaIncomeObject.OtherIncome.ToString(CurrFormatter);
                _I_PI_GrossRent.Text = obj.ProformaIncomeObject.GrossRent.ToString(CurrFormatter);
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.ToString());
            }
        }
        #endregion

        #region REPORTS
        void On_PrintReports_Click(object sender, EventArgs e)
        {
            if (sender == _printIncomeSummary)
            {
                PrintReports(ReportOriginator.IncomeSummary, e);
            }
            else if (sender == _printRentRoll)
            {
                PrintReports(ReportOriginator.IncomeRentRoll, e);
            }            
        }

        void PrintReports(ReportOriginator ro, EventArgs e)
        {
            PageSettings pageSettings = null;
            PageSetupDialog pageSetupDialog = new PageSetupDialog();
            pageSetupDialog.PageSettings = new PageSettings();
            pageSetupDialog.PrinterSettings = new PrinterSettings();
            pageSetupDialog.ShowNetwork = false;

            pageSetupDialog.PageSettings.PaperSize = new PaperSize("A4", 827, 1169);
            pageSetupDialog.PageSettings.Margins = new Margins(0, 0, 0, 0);


            //if (DialogResult.OK == pageSetupDialog.ShowDialog())
            //{
            //    pageSettings = new PageSettings(pageSetupDialog.PrinterSettings);
            //    pageSettings.PaperSize = pageSetupDialog.PageSettings.PaperSize;
            //    pageSettings.Margins = pageSetupDialog.PageSettings.Margins;								
            //}

            PrintDialog pd = new PrintDialog();
            pd.UseEXDialog = true;

            PrintDocument tmpprndoc = new PrintDocument();

            if (ReportOriginator.IncomeSummary == ro)
            {
                tmpprndoc.DefaultPageSettings.Landscape = true;
                tmpprndoc.PrintPage += new PrintPageEventHandler(On_PrintIncomeSummary);
                tmpprndoc.EndPrint += new PrintEventHandler(On_PrintIncome_EndPrint);
            }
            else if (ReportOriginator.IncomeRentRoll == ro)
            {
                tmpprndoc.DefaultPageSettings.Landscape = true;
                tmpprndoc.PrintPage += new PrintPageEventHandler(On_PrintRentRoll);
                tmpprndoc.EndPrint += new PrintEventHandler(On_PrintIncome_EndPrint);
            }
            

            pd.Document = tmpprndoc;
            PrintPreviewDialogEx tmpprdiag = new PrintPreviewDialogEx();

            tmpprdiag.Document = tmpprndoc;
            //tmpprdiag.Document.DefaultPageSettings = pageSettings;
            tmpprdiag.ShowDialog();
        }

        private void On_PrintIncomeSummary(object sender, PrintPageEventArgs e)
        {
            _iGen.GenerateSummary(e, IncomeObject.Instance, _address);
        }

        private void On_PrintRentRoll(object sender, PrintPageEventArgs e)
        {
            _iGen.GenerateRentRoll(e, IncomeObject.Instance, _address);
        }

        void On_PrintIncome_EndPrint(object sender, PrintEventArgs e)
        {
            _iGen.ResetGen();
        }
        #endregion

        private void OnTextBoxEnter(object sender, System.EventArgs e)
        {
            //			TextBox tb = (TextBox)sender;
            //			tb.BackColor = Color.LightCyan;
        }

        #region AUTOSAVE
        void AddAutoSaveEventHandlers()
        {
            ArrayList objectsFound = new ArrayList();
            FindAutoSaveObjects(this.Controls, ref objectsFound);

            foreach (object o in objectsFound)
            {
                if (o is TextBox)
                {
                    ((TextBox)o).Leave += new EventHandler(On_TextBox_Leave);

                    if (!((TextBox)o).ReadOnly)
                        ((TextBox)o).TextChanged += new EventHandler(On_TextChanged);
                }

                if (o is ComboBox)
                    ((ComboBox)o).SelectedIndexChanged += new EventHandler(On_SelectedIndexChanged);

                if (o is DateTimePicker)
                    ((DateTimePicker)o).ValueChanged += new EventHandler(On_SelectedIndexChanged);// we use combobox event handler
            }
        }

        void On_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (null != _autoSave)
                _autoSave.TriggerSelectedIndexChanged();
        }

        void On_TextChanged(object sender, EventArgs e)
        {
            if (null != _autoSave)
                _autoSave.TriggerTextBoxChanged();
        }

        void On_TextBox_Leave(object sender, EventArgs e)
        {
            if (null != _autoSave)
                _autoSave.TriggerTextBoxLeave();
        }

        void FindAutoSaveObjects(System.Windows.Forms.Control.ControlCollection ctrls, ref ArrayList triggerObjects)
        {
            foreach (object o in ctrls)
            {
                if (o is GroupBox)
                    FindAutoSaveObjects(((GroupBox)o).Controls, ref triggerObjects);

                if (o is Panel)
                    FindAutoSaveObjects(((Panel)o).Controls, ref triggerObjects);

                if (o is TabControl)
                    FindAutoSaveObjects(((TabControl)o).Controls, ref triggerObjects);

                if (o is TextBox)
                    triggerObjects.Add(o);

                if (o is ComboBox)
                    triggerObjects.Add(o);

                if (o is DateTimePicker)
                    triggerObjects.Add(o);

                if (o is CommercialControl)
                    FindAutoSaveObjects(((CommercialControl)o).Controls, ref triggerObjects);
            }
        }
        #endregion
    }
}
