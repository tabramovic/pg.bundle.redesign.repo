using System;
using System.Collections.Generic;
using System.Collections;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Printing;
using System.Data;
using System.Windows.Forms;

using ProfitGrabber.Commercial.Objects;
using ProfitGrabber.Commercial.GraphX;
using ProfitGrabber.Commercial.Floaters;
using ProfitGrabber.Commercial.Generators;
using ProfitGrabber.Common;


namespace ProfitGrabber.Commercial
{
	/// <summary>
	/// Summary description for CommercialControl.
	/// </summary>
	public class CommercialControl : System.Windows.Forms.UserControl
	{
		const string CurrFormatter = "#,##0.00";
        string _address = string.Empty;
        
		BuildingInfoFloater _bi_FloaterForm = null;
		IncomeFloater _i_FloaterForm = null;
		ExpensesFloater _e_FloaterForm = null;
		LoansFloater _l_FloaterForm = null;
		FinancialAnalisysFloater _fa_FloaterForm = null;

        BuildingInfoGenerator _biGen = new BuildingInfoGenerator();
        IncomeGenerator _iGen = new IncomeGenerator();
        ExpensesGenerator _eGen = new ExpensesGenerator();
        FinancialAnalisysGenerator _fGen = new FinancialAnalisysGenerator();
        LoansInfoGenerator _lGen = new LoansInfoGenerator();
        CashFlowGenerator _cfGen = new CashFlowGenerator();
        NetProfitOrTaxesDeductionGenerator _npotdGen = new NetProfitOrTaxesDeductionGenerator();

        IAutoSave _autosaveInterface = null;

        public void SetAutoSaveInterface(IAutoSave autoSaveIfc)
        {
            _autosaveInterface = autoSaveIfc;
        }

		#region DATA
		private System.Windows.Forms.TabControl _masterTabControl;
        private System.Windows.Forms.TabPage _buildingInfoPage;
		private System.Windows.Forms.TabPage _expensesTabPage;
		private System.Windows.Forms.TabPage _loansTabPage;
		private System.Windows.Forms.TabPage _financialAnalysisTabPage;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.Label label13;
		private System.Windows.Forms.Label label14;
		private System.Windows.Forms.Label label15;
		private System.Windows.Forms.Label label16;
		private System.Windows.Forms.Label label17;
		private System.Windows.Forms.Label label18;
		private System.Windows.Forms.Label label19;
		private System.Windows.Forms.Label label20;
		private System.Windows.Forms.Label label21;
		private System.Windows.Forms.Label label22;
		private System.Windows.Forms.Label label23;
		private System.Windows.Forms.Label label24;
		private System.Windows.Forms.Label label25;
		private System.Windows.Forms.Label label26;
		private System.Windows.Forms.Label label27;
		private System.Windows.Forms.Label label28;
		private System.Windows.Forms.Label label29;
		private System.Windows.Forms.Label label30;
		private System.Windows.Forms.Label label31;
		private System.Windows.Forms.Label label32;
		private System.Windows.Forms.Label label33;
		private System.Windows.Forms.Label label34;
		private System.Windows.Forms.Label label35;
		private System.Windows.Forms.Label label36;
		private System.Windows.Forms.Label label37;
		private System.Windows.Forms.Label label38;
		private System.Windows.Forms.Label label39;
		private System.Windows.Forms.Label label40;
		private System.Windows.Forms.Label label41;
		private System.Windows.Forms.Label label42;
		private System.Windows.Forms.Label label43;
		private System.Windows.Forms.Label label44;
		private System.Windows.Forms.Label label45;
		private System.Windows.Forms.Label label46;
		private System.Windows.Forms.Label label47;
		private System.Windows.Forms.Label label48;
		private System.Windows.Forms.Label label49;
		private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label51;
		private System.Windows.Forms.Label label116;
		private System.Windows.Forms.Label label117;
		private System.Windows.Forms.Label label118;
		private System.Windows.Forms.Label label119;
		private System.Windows.Forms.Label label120;
		private System.Windows.Forms.Label label121;
		private System.Windows.Forms.Label label122;
		private System.Windows.Forms.Label label123;
		private System.Windows.Forms.Label label124;
		private System.Windows.Forms.Label label125;
		private System.Windows.Forms.Label label126;
		private System.Windows.Forms.Label label127;
		private System.Windows.Forms.Label label128;
		private System.Windows.Forms.Label label129;
		private System.Windows.Forms.Label label130;
		private System.Windows.Forms.Label label131;
		private System.Windows.Forms.Label label132;
		private System.Windows.Forms.Label label133;
		private System.Windows.Forms.ColumnHeader columnHeader13;
		private System.Windows.Forms.ColumnHeader columnHeader14;
		private System.Windows.Forms.ColumnHeader columnHeader15;
		private System.Windows.Forms.ColumnHeader columnHeader16;
		private System.Windows.Forms.ColumnHeader columnHeader17;
		private System.Windows.Forms.ColumnHeader columnHeader18;
		private System.Windows.Forms.ColumnHeader columnHeader19;
		private System.Windows.Forms.ColumnHeader columnHeader20;
		private System.Windows.Forms.ColumnHeader columnHeader21;
		private System.Windows.Forms.ColumnHeader columnHeader22;
		private System.Windows.Forms.Label label134;
		private System.Windows.Forms.Label label135;
		private System.Windows.Forms.Label label136;
		private System.Windows.Forms.Label label137;
		private System.Windows.Forms.Label label138;
		private System.Windows.Forms.Label label139;
        private System.Windows.Forms.Label label140;
		private System.Windows.Forms.Label label71;
		private System.Windows.Forms.Label label72;
		private System.Windows.Forms.Label label73;
		private System.Windows.Forms.Label label74;
		private System.Windows.Forms.Label label75;
		private System.Windows.Forms.Label label76;
		private System.Windows.Forms.Label label77;
		private System.Windows.Forms.Label label78;
		private System.Windows.Forms.Label label79;
		private System.Windows.Forms.Label label80;
		private System.Windows.Forms.Label label81;
		private System.Windows.Forms.Label label82;
		private System.Windows.Forms.Label label94;
		private System.Windows.Forms.Label label141;
		private System.Windows.Forms.Label label142;
		private System.Windows.Forms.Label label143;
		private System.Windows.Forms.Label label144;
		private System.Windows.Forms.Label label145;
		private System.Windows.Forms.Label label146;
		private System.Windows.Forms.Label label147;
		private System.Windows.Forms.Label label148;
		private System.Windows.Forms.Label label149;
		private System.Windows.Forms.Label label150;
		private System.Windows.Forms.Label label151;
		private System.Windows.Forms.Label label152;
		private System.Windows.Forms.Label label153;
		private System.Windows.Forms.Label label154;
		private System.Windows.Forms.Label label155;
		private System.Windows.Forms.Label label156;
		private System.Windows.Forms.Label label157;
		private System.Windows.Forms.Label label158;
		private System.Windows.Forms.Label label159;
		private System.Windows.Forms.Label label160;
		private System.Windows.Forms.Label label161;
		private System.Windows.Forms.Label label162;
		private System.Windows.Forms.Label label163;
		private System.Windows.Forms.Label label164;
		private System.Windows.Forms.Label label165;
		private System.Windows.Forms.Label label166;
		private System.Windows.Forms.Label label167;
		private System.Windows.Forms.Label label168;
		private System.Windows.Forms.Label label169;
		private System.Windows.Forms.Label label170;
		private System.Windows.Forms.Label label171;
		private System.Windows.Forms.Label label172;
		private System.Windows.Forms.Label label173;
		private System.Windows.Forms.Label label174;
		private System.Windows.Forms.Label label175;
		private System.Windows.Forms.Label label176;
		private System.Windows.Forms.Label label177;
		private System.Windows.Forms.Label label178;
		private System.Windows.Forms.Label label179;
		private System.Windows.Forms.Label label180;
		private System.Windows.Forms.Label label181;
		private System.Windows.Forms.Label label182;
		private System.Windows.Forms.Label label183;
		private System.Windows.Forms.Label label184;
		private System.Windows.Forms.Label label185;
		private System.Windows.Forms.Label label186;
		private System.Windows.Forms.Label label187;
		private System.Windows.Forms.Label label188;
		private System.Windows.Forms.Label label189;
		private System.Windows.Forms.Label label190;
		private System.Windows.Forms.Label label191;
		private System.Windows.Forms.Label label192;
		private System.Windows.Forms.Label label193;
		private System.Windows.Forms.Label label194;
		private System.Windows.Forms.Label label195;
		private System.Windows.Forms.Label label196;
		private System.Windows.Forms.Label label197;
		private System.Windows.Forms.Label label198;
		private System.Windows.Forms.Label label199;
		private System.Windows.Forms.Label label200;
		private System.Windows.Forms.Label label201;
		private System.Windows.Forms.Label label202;
		private System.Windows.Forms.Label label203;
		private System.Windows.Forms.Label label204;
		private System.Windows.Forms.Label label205;
		private System.Windows.Forms.Label label206;
		private System.Windows.Forms.Label label207;
		private System.Windows.Forms.Label label208;
		private System.Windows.Forms.Label label209;
		private System.Windows.Forms.Label label210;
		private System.Windows.Forms.Label label211;
		private System.Windows.Forms.Label label212;
		private System.Windows.Forms.Label label213;
		private System.Windows.Forms.Label label214;
		private System.Windows.Forms.Label label215;
		private System.Windows.Forms.Label label216;
		private System.Windows.Forms.Label label217;
		private System.Windows.Forms.Label label218;
		private System.Windows.Forms.Label label219;
		private System.Windows.Forms.Label label220;
		private System.Windows.Forms.Label label221;
		private System.Windows.Forms.Label label222;
		private System.Windows.Forms.Label label223;
		private System.Windows.Forms.Label label224;
		private System.Windows.Forms.Label label225;
		private System.Windows.Forms.Label label226;
		private System.Windows.Forms.Label label227;
		private System.Windows.Forms.Label label228;
		private System.Windows.Forms.Label label229;
		private System.Windows.Forms.Label label230;
		private System.Windows.Forms.Label label231;
		private System.Windows.Forms.Label label232;
		private System.Windows.Forms.Label label233;
		private System.Windows.Forms.Label label234;
		private System.Windows.Forms.Label label235;
		private System.Windows.Forms.Label label236;
		private System.Windows.Forms.Label label237;
		private System.Windows.Forms.Label label238;
		private System.Windows.Forms.Label label239;
		private System.Windows.Forms.Label label240;
		private System.Windows.Forms.Label label241;
		private System.Windows.Forms.Label label242;
		private System.Windows.Forms.Label label243;
		private System.Windows.Forms.Label label244;
		private System.Windows.Forms.Label label245;
		private System.Windows.Forms.Label label246;
		private System.Windows.Forms.Label label247;
		private System.Windows.Forms.Label label248;
		private System.Windows.Forms.Label label249;
		private System.Windows.Forms.Label label250;
		private System.Windows.Forms.Label label251;
		private System.Windows.Forms.Label label252;
		private System.Windows.Forms.Label label253;
		private System.Windows.Forms.Label label254;
		private System.Windows.Forms.Label label255;
		private System.Windows.Forms.Label label256;
		private System.Windows.Forms.Label label257;
		private System.Windows.Forms.Label label258;
		private System.Windows.Forms.Label label259;
		private System.Windows.Forms.Label label260;
		private System.Windows.Forms.Label label261;
		private System.Windows.Forms.Label label262;
		private System.Windows.Forms.Label label263;
		private System.Windows.Forms.Label label264;
		private System.Windows.Forms.Label label265;
		private System.Windows.Forms.Label label266;
		private System.Windows.Forms.Label label267;
		private System.Windows.Forms.Label label268;
		private System.Windows.Forms.Label label269;
		private System.Windows.Forms.Label label270;
		private System.Windows.Forms.Label label271;
		private System.Windows.Forms.Label label272;
		private System.Windows.Forms.Label label273;
		private System.Windows.Forms.Label label274;
		private System.Windows.Forms.Label label275;
		private System.Windows.Forms.Label label276;
		private System.Windows.Forms.Label label277;
		private System.Windows.Forms.Label label278;
		private System.Windows.Forms.Label label279;
		private System.Windows.Forms.Label label280;
		private System.Windows.Forms.Label label281;
		private System.Windows.Forms.Label label282;
		private System.Windows.Forms.Label label283;
		private System.Windows.Forms.Label label284;
		private System.Windows.Forms.Label label285;
		private System.Windows.Forms.Label label286;
		private System.Windows.Forms.Label label287;
		private System.Windows.Forms.Label label288;
		private System.Windows.Forms.Label label289;
		private System.Windows.Forms.Label label291;
		private System.Windows.Forms.Label label292;
		private System.Windows.Forms.Label label294;
		private System.Windows.Forms.Label label296;
		private System.Windows.Forms.Label label298;
		private System.Windows.Forms.Label label300;
		private System.Windows.Forms.Label label301;
		private System.Windows.Forms.Label label303;
		private System.Windows.Forms.Label label304;
		private System.Windows.Forms.Label label306;
		private System.Windows.Forms.Label label308;
		private System.Windows.Forms.Label label310;
		private System.Windows.Forms.Label label312;
		private System.Windows.Forms.Label label313;
		private System.Windows.Forms.Label label315;
		private System.Windows.Forms.Label label316;
		private System.Windows.Forms.Label label318;
		private System.Windows.Forms.Label label320;
		private System.Windows.Forms.Label label322;
		private System.Windows.Forms.Label label290;
		private System.Windows.Forms.Label label293;
		private System.Windows.Forms.Label label295;
		private System.Windows.Forms.Label label299;
		private System.Windows.Forms.Label label302;
        private System.Windows.Forms.Label label305;
		private System.Windows.Forms.TextBox _BI_windowType;
		private System.Windows.Forms.TextBox _BI_roofType;
		private System.Windows.Forms.TextBox _BI_landscaping;
		private System.Windows.Forms.TextBox _BI_amenities;
		private System.Windows.Forms.TextBox _BI_locationDescription;
		private System.Windows.Forms.TextBox _BI_propertyDescription;
		private System.Windows.Forms.TextBox _BI_improvementsNeeded;
		private System.Windows.Forms.TextBox _BI_recentImprovements;
		private System.Windows.Forms.TextBox _BI_landLeaseComments;
		private System.Windows.Forms.TextBox _BI_rent;
		private System.Windows.Forms.TextBox _BI_endDate;
		private System.Windows.Forms.TextBox _BI_startDate;
		private System.Windows.Forms.TextBox _BI_lessor;
		private System.Windows.Forms.ComboBox _BI_option;
		private System.Windows.Forms.ComboBox _BI_landLease;
		private System.Windows.Forms.TextBox _BI_comments;
		private System.Windows.Forms.TextBox _BI_parkingRatio;
		private System.Windows.Forms.TextBox _BI_totalFee;
		private System.Windows.Forms.TextBox _BI_garageFee;
		private System.Windows.Forms.TextBox _BI_coveredFee;
		private System.Windows.Forms.TextBox _BI_lotFee;
		private System.Windows.Forms.TextBox _BI_totalSpaces;
		private System.Windows.Forms.TextBox _BI_garageSpaces;
		private System.Windows.Forms.TextBox _BI_coveredSpaces;
		private System.Windows.Forms.TextBox _BI_lotSpaces;
		private System.Windows.Forms.ComboBox _BI_windowsAge;
		private System.Windows.Forms.ComboBox _BI_sidingType;
		private System.Windows.Forms.ComboBox _BI_electricalAge;
		private System.Windows.Forms.ComboBox _BI_plumbingAge;
		private System.Windows.Forms.ComboBox _BI_plumbingType;
		private System.Windows.Forms.ComboBox _BI_roofAge;
		private System.Windows.Forms.TextBox _BI_lotSqft;
		private System.Windows.Forms.TextBox _BI_lotAcres;
		private System.Windows.Forms.TextBox _BI_totalSqFt;
		private System.Windows.Forms.ComboBox _BI_structures;
		private System.Windows.Forms.ComboBox _BI_floors;
		private System.Windows.Forms.ComboBox _BI_units;
		private System.Windows.Forms.ComboBox _BI_rehabYear;
		private System.Windows.Forms.ComboBox _BI_yearBuilt;
		private System.Windows.Forms.TextBox _BI_propertyValue;
		private System.Windows.Forms.TextBox _BI_apn;
		private System.Windows.Forms.ComboBox _BI_class;
		private System.Windows.Forms.ComboBox _BI_propertyType;
		private System.Windows.Forms.TextBox _BI_propertyName;
		private System.Windows.Forms.TextBox _BI_netRentableSqFt;
        private System.Windows.Forms.Label label53;
		private System.Windows.Forms.TextBox _E_TotalPercOfTotal_6;
		private System.Windows.Forms.TextBox _E_TotalPercOfTotal_1;
		private System.Windows.Forms.TextBox _E_TotalPercOfTotal_5;
		private System.Windows.Forms.TextBox _E_TotalPercOfTotal_4;
		private System.Windows.Forms.TextBox _E_TotalPercOfTotal_3;
		private System.Windows.Forms.TextBox _E_TotalPercOfTotal_2;
		private System.Windows.Forms.TextBox _E_TotalUsdPerUnit_6;
		private System.Windows.Forms.TextBox _E_TotalUsdPerUnit_1;
		private System.Windows.Forms.TextBox _E_TotalUsdPerUnit_5;
		private System.Windows.Forms.TextBox _E_TotalUsdPerUnit_4;
		private System.Windows.Forms.TextBox _E_TotalUsdPerUnit_3;
		private System.Windows.Forms.TextBox _E_TotalUsdPerUnit_2;
		private System.Windows.Forms.TextBox _E_TotalUsdPerSqft_6;
		private System.Windows.Forms.TextBox _E_TotalUsdPerSqft_1;
		private System.Windows.Forms.TextBox _E_TotalUsdPerSqft_5;
		private System.Windows.Forms.TextBox _E_TotalUsdPerSqft_4;
		private System.Windows.Forms.TextBox _E_TotalUsdPerSqft_3;
		private System.Windows.Forms.TextBox _E_TotalUsdPerSqft_2;
		private System.Windows.Forms.TextBox _E_TotalUsdPerYear_6;
		private System.Windows.Forms.TextBox _E_TotalUsdPerYear_1;
		private System.Windows.Forms.TextBox _E_TotalUsdPerYear_5;
		private System.Windows.Forms.TextBox _E_TotalUsdPerYear_4;
		private System.Windows.Forms.TextBox _E_TotalUsdPerYear_3;
		private System.Windows.Forms.TextBox _E_TotalUsdPerYear_2;
		private System.Windows.Forms.TextBox _E_TotalUsdPerMonth_6;
		private System.Windows.Forms.TextBox _E_TotalUsdPerMonth_1;
		private System.Windows.Forms.TextBox _E_TotalUsdPerMonth_5;
		private System.Windows.Forms.TextBox _E_TotalUsdPerMonth_4;
		private System.Windows.Forms.TextBox _E_TotalUsdPerMonth_3;
		private System.Windows.Forms.TextBox _E_TotalUsdPerMonth_2;
		private System.Windows.Forms.TextBox _E_TotalDesc_6;
		private System.Windows.Forms.TextBox _E_TotalDesc_1;
		private System.Windows.Forms.TextBox _E_TotalDesc_5;
		private System.Windows.Forms.TextBox _E_TotalDesc_4;
		private System.Windows.Forms.TextBox _E_TotalDesc_3;
		private System.Windows.Forms.TextBox _E_TotalDesc_2;
		private System.Windows.Forms.TextBox _E_TotalType_6;
		private System.Windows.Forms.TextBox _E_TotalType_1;
		private System.Windows.Forms.TextBox _E_TotalType_5;
		private System.Windows.Forms.TextBox _E_TotalType_4;
		private System.Windows.Forms.TextBox _E_TotalType_3;
		private System.Windows.Forms.TextBox _E_TotalType_2;
		private ProfitGrabber.Commercial.ListViewEx _E_expenses;
		private System.Windows.Forms.TextBox _E_CA_Type_Year10;
		private System.Windows.Forms.TextBox _E_CA_Cost_Year10;
		private System.Windows.Forms.TextBox _E_CA_Type_Year9;
		private System.Windows.Forms.TextBox _E_CA_Cost_Year9;
		private System.Windows.Forms.TextBox _E_CA_Type_Year8;
		private System.Windows.Forms.TextBox _E_CA_Cost_Year8;
		private System.Windows.Forms.TextBox _E_CA_Type_Year7;
		private System.Windows.Forms.TextBox _E_CA_Cost_Year7;
		private System.Windows.Forms.TextBox _E_CA_Type_Year6;
		private System.Windows.Forms.TextBox _E_CA_Cost_Year6;
		private System.Windows.Forms.TextBox _E_CA_Type_Year5;
		private System.Windows.Forms.TextBox _E_CA_Cost_Year5;
		private System.Windows.Forms.TextBox _E_CA_Type_Year4;
		private System.Windows.Forms.TextBox _E_CA_Cost_Year4;
		private System.Windows.Forms.TextBox _E_CA_Type_Year3;
		private System.Windows.Forms.TextBox _E_CA_Cost_Year3;
		private System.Windows.Forms.TextBox _E_CA_Type_Year2;
		private System.Windows.Forms.TextBox _E_CA_Cost_Year2;
		private System.Windows.Forms.TextBox _E_CA_Type_Year1;
		private System.Windows.Forms.TextBox _E_CA_Cost_Year1;
		private System.Windows.Forms.ComboBox _E_UT_HotWater;
		private System.Windows.Forms.ComboBox _E_UT_Heat;
		private System.Windows.Forms.ComboBox _E_UT_ElectricityMeter;
		private System.Windows.Forms.TextBox _L_3L_RA_LifeTimeCap;
		private System.Windows.Forms.TextBox _L_3L_RA_AnnualCap;
		private System.Windows.Forms.TextBox _L_3L_RA_InitialRateCap;
		private System.Windows.Forms.TextBox _L_3L_RA_InitFixedPeriod;
		private System.Windows.Forms.TextBox _L_3L_RA_RateYear10;
		private System.Windows.Forms.TextBox _L_3L_RA_RateYear9;
		private System.Windows.Forms.TextBox _L_3L_RA_RateYear8;
		private System.Windows.Forms.TextBox _L_3L_RA_RateYear7;
		private System.Windows.Forms.TextBox _L_3L_RA_RateYear6;
		private System.Windows.Forms.TextBox _L_3L_RA_RateYear5;
		private System.Windows.Forms.TextBox _L_3L_RA_RateYear4;
		private System.Windows.Forms.TextBox _L_3L_RA_RateYear3;
		private System.Windows.Forms.TextBox _L_3L_RA_RateYear2;
		private System.Windows.Forms.TextBox _L_3L_RA_RateYear1;
		private System.Windows.Forms.TextBox _L_3L_Lender;
		private System.Windows.Forms.TextBox _L_3L_LastPaymentDate;
		private System.Windows.Forms.TextBox _L_3L_FirstPaymentDate;
		private System.Windows.Forms.TextBox _L_3L_Points;
		private System.Windows.Forms.TextBox _L_3L_PointsPerc;
		private System.Windows.Forms.TextBox _L_3L_MonthlyPayment;
		private System.Windows.Forms.TextBox _L_3L_Rate;
		private System.Windows.Forms.TextBox _L_3L_AmortTerm;
		private System.Windows.Forms.TextBox _L_3L_Amount;
		private System.Windows.Forms.TextBox _L_3L_LTVPerc;
		private System.Windows.Forms.ComboBox _L_3L_Type;
		private System.Windows.Forms.TextBox _L_2L_RA_LifeTimeCap;
		private System.Windows.Forms.TextBox _L_2L_RA_AnnualCap;
		private System.Windows.Forms.TextBox _L_2L_RA_InitialRateCap;
		private System.Windows.Forms.TextBox _L_2L_RA_InitFixedPeriod;
		private System.Windows.Forms.TextBox _L_2L_RA_RateYear10;
		private System.Windows.Forms.TextBox _L_2L_RA_RateYear9;
		private System.Windows.Forms.TextBox _L_2L_RA_RateYear8;
		private System.Windows.Forms.TextBox _L_2L_RA_RateYear7;
		private System.Windows.Forms.TextBox _L_2L_RA_RateYear6;
		private System.Windows.Forms.TextBox _L_2L_RA_RateYear5;
		private System.Windows.Forms.TextBox _L_2L_RA_RateYear4;
		private System.Windows.Forms.TextBox _L_2L_RA_RateYear3;
		private System.Windows.Forms.TextBox _L_2L_RA_RateYear2;
		private System.Windows.Forms.TextBox _L_2L_RA_RateYear1;
		private System.Windows.Forms.TextBox _L_2L_Lender;
		private System.Windows.Forms.TextBox _L_2L_LastPaymentDate;
		private System.Windows.Forms.TextBox _L_2L_FirstPaymentDate;
		private System.Windows.Forms.TextBox _L_2L_Points;
		private System.Windows.Forms.TextBox _L_2L_PointsPerc;
		private System.Windows.Forms.TextBox _L_2L_MonthlyPayment;
		private System.Windows.Forms.TextBox _L_2L_Rate;
		private System.Windows.Forms.TextBox _L_2L_AmortTerm;
		private System.Windows.Forms.TextBox _L_2L_Amount;
		private System.Windows.Forms.TextBox _L_2L_LTVPerc;
		private System.Windows.Forms.ComboBox _L_2L_Type;
		private System.Windows.Forms.TextBox _L_1L_RA_LifeTimeCap;
		private System.Windows.Forms.TextBox _L_1L_RA_AnnualCap;
		private System.Windows.Forms.TextBox _L_1L_RA_InitialRateCap;
		private System.Windows.Forms.TextBox _L_1L_RA_InitFixedPeriod;
		private System.Windows.Forms.TextBox _L_1L_RA_RateYear10;
		private System.Windows.Forms.TextBox _L_1L_RA_RateYear9;
		private System.Windows.Forms.TextBox _L_1L_RA_RateYear8;
		private System.Windows.Forms.TextBox _L_1L_RA_RateYear7;
		private System.Windows.Forms.TextBox _L_1L_RA_RateYear6;
		private System.Windows.Forms.TextBox _L_1L_RA_RateYear5;
		private System.Windows.Forms.TextBox _L_1L_RA_RateYear4;
		private System.Windows.Forms.TextBox _L_1L_RA_RateYear3;
		private System.Windows.Forms.TextBox _L_1L_RA_RateYear2;
		private System.Windows.Forms.TextBox _L_1L_RA_RateYear1;
		private System.Windows.Forms.TextBox _L_1L_Lender;
		private System.Windows.Forms.TextBox _L_1L_LastPaymentDate;
		private System.Windows.Forms.TextBox _L_1L_FirstPaymentDate;
		private System.Windows.Forms.TextBox _L_1L_Points;
		private System.Windows.Forms.TextBox _L_1L_PointsPerc;
		private System.Windows.Forms.TextBox _L_1L_MonthlyPayment;
		private System.Windows.Forms.TextBox _L_1L_Rate;
		private System.Windows.Forms.TextBox _L_1L_AmortTerm;
		private System.Windows.Forms.TextBox _L_1L_Amount;
		private System.Windows.Forms.TextBox _L_1L_LTVPerc;
		private System.Windows.Forms.ComboBox _L_1L_Type;
		private System.Windows.Forms.TextBox _L_TotalPaymentsAnnual;
		private System.Windows.Forms.TextBox _L_TotalPaymentsMonthly;
		private System.Windows.Forms.TextBox _L_DCR;
		private System.Windows.Forms.TextBox _L_CLTVPerc;
		private System.Windows.Forms.TextBox _L_BlendedRate;
		private System.Windows.Forms.ComboBox _L_SellerCarryback;
		private System.Windows.Forms.ComboBox _L_Assumable;
		private System.Windows.Forms.ComboBox _L_1stAssumable;
		private System.Windows.Forms.ComboBox _L_DownPaymentPerc;
		private System.Windows.Forms.TextBox _L_DownPayment;
		private System.Windows.Forms.TextBox _L_PurchasePrice;
		private System.Windows.Forms.TextBox _FA_A_OperatingExpensesPerc;
		private System.Windows.Forms.TextBox _FA_S_CashOnCashReturn;
		private System.Windows.Forms.TextBox _FA_S_CashFlow;
		private System.Windows.Forms.TextBox _FA_S_DebtCoverageRatio;
		private System.Windows.Forms.TextBox _FA_S_DebtService;
		private System.Windows.Forms.TextBox _FA_S_CapRate;
		private System.Windows.Forms.TextBox _FA_S_NetOperatingIncome;
		private System.Windows.Forms.TextBox _FA_S_OperatingExpenses;
		private System.Windows.Forms.TextBox _FA_S_GOIAnnual;
		private System.Windows.Forms.TextBox _FA_S_GSIAnnual;
		private System.Windows.Forms.TextBox _FA_S_Price;
		private System.Windows.Forms.Button _FA_S_copyFromProforma;
		private System.Windows.Forms.TextBox _FA_P_VacancyCreditLossPerc;
		private System.Windows.Forms.TextBox _FA_P_CashOnCashReturn;
		private System.Windows.Forms.TextBox _FA_P_CashFlow;
		private System.Windows.Forms.TextBox _FA_P_DebtCoverageRatio;
		private System.Windows.Forms.TextBox _FA_P_DebtService;
		private System.Windows.Forms.TextBox _FA_P_CapRate;
		private System.Windows.Forms.TextBox _FA_P_NetOperatingIncome;
		private System.Windows.Forms.TextBox _FA_P_OperatingExpenses;
		private System.Windows.Forms.TextBox _FA_P_GOIAnnual;
		private System.Windows.Forms.TextBox _FA_P_OtherIncome;
		private System.Windows.Forms.TextBox _FA_P_VacancyCreditLoss;
		private System.Windows.Forms.TextBox _FA_P_GSIAnnual;
		private System.Windows.Forms.TextBox _FA_P_Price;
		private System.Windows.Forms.Button _FA_P_copyFromActual;
		private System.Windows.Forms.Button _FA_P_copyEfromExpenses;
		private System.Windows.Forms.Button _FA_P_copyPIfromIncome;
		private System.Windows.Forms.TextBox _FA_A_VacancyCreditLossPerc;
		private System.Windows.Forms.TextBox _FA_A_CashOnCashReturn;
		private System.Windows.Forms.TextBox _FA_A_CashFlow;
		private System.Windows.Forms.TextBox _FA_A_DebtCoverageRatio;
		private System.Windows.Forms.TextBox _FA_A_DebtService;
		private System.Windows.Forms.TextBox _FA_A_CapRate;
		private System.Windows.Forms.TextBox _FA_A_NetOperatingIncome;
		private System.Windows.Forms.TextBox _FA_A_OperatingExpenses;
		private System.Windows.Forms.TextBox _FA_A_GOIAnnual;
		private System.Windows.Forms.TextBox _FA_A_OtherIncome;
		private System.Windows.Forms.TextBox _FA_A_VacancyCreditLoss;
		private System.Windows.Forms.TextBox _FA_A_GSIAnnual;
		private System.Windows.Forms.TextBox _FA_A_Price;
		private System.Windows.Forms.Button _FA_copyPDfromLoan;
		private System.Windows.Forms.Button _FA_copyEfromExpenses;
		private System.Windows.Forms.Button _FA_copyAIfromIncome;
		private System.Windows.Forms.TextBox _FA_S_VacancyCreditLossPerc;
		private System.Windows.Forms.TextBox _FA_S_OtherIncome;
		private System.Windows.Forms.TextBox _FA_S_VacancyCreditLoss;
		private System.Windows.Forms.Button _FA_Graph4;
		private System.Windows.Forms.Button _FA_Graph3;
		private System.Windows.Forms.Button _FA_Graph2;
		private System.Windows.Forms.Button _FA_Graph1;
		private System.Windows.Forms.Button _FA_S_copyFromActual;
		private System.Windows.Forms.TextBox _FA_P_OperatingExpensesPerc;
		private System.Windows.Forms.TextBox _FA_S_OperatingExpensesPerc;
		private System.Windows.Forms.NumericUpDown _FA_CR_Change_NUD;
		private System.Windows.Forms.NumericUpDown _FA_GSI_Range_NUD;
		private System.Windows.Forms.NumericUpDown _FA_OE_Range_NUD;
        private System.Windows.Forms.NumericUpDown _FA_LR_Range_NUD;
		private System.Windows.Forms.TextBox _FA_S_Comment;
		private System.Windows.Forms.TextBox _FA_P_Comment;
		private System.Windows.Forms.TextBox _FA_A_Comment;
        private System.Windows.Forms.PictureBox _bi_floater;
		private System.Windows.Forms.PictureBox _e_floater;
		private System.Windows.Forms.PictureBox _l_floater;
		private System.Windows.Forms.PictureBox _fa_floater;
        private TabPage _multiYearAnalysis;
        private Label label56;
        private Label label55;
        private TextBox _MA_OtherAcquisitionCosts;
        private TextBox _MA_Points;
        private TextBox _MA_DownPayment;
        private TextBox _MA_InitialInvestment;
        private TextBox _MA_PurchasePrice;
        private Button _ma_copyFromLoanTab;
        private Label label54;
        private Label label307;
        private Label label297;
        private Label label65;
        private Label label64;
        private Label label63;
        private Label label62;
        private Label label61;
        private Label label60;
        private Label label59;
        private Label label58;
        private Label label57;
        private Label label327;
        private Label label326;
        private Label label325;
        private Label label324;
        private Label label323;
        private Label label321;
        private Label label319;
        private Label label317;
        private Label label314;
        private Label label311;
        private TextBox _MA_S1_OtherIncomePerc;
        private TextBox _MA_S1_GSI_Perc;
        private TextBox _MA_S1_OperatingExpenses;
        private TextBox _MA_S1_OtherIncome;
        private TextBox _MA_S1_Vacancy;
        private TextBox _MA_S1_GSI;
        private Button _MA_CopyFromProforma;
        private Button _MA_CopyFromActual;
        private Label label309;
        private TextBox _MA_TI_CapitalGainsRate;
        private Label label332;
        private Label label333;
        private TextBox _MA_TI_StateRate;
        private Label label330;
        private Label label331;
        private TextBox _MA_TI_FederalRate;
        private Label label329;
        private Label label328;
        private TextBox _MA_TI_BuildingValue;
        private Label label335;
        private Label label336;
        private ComboBox _MA_RS_MARG_AnalysisPeriod;
        private Label label334;
        private Label label339;
        private CheckBox _MA_RS_MARG_TaxesAndDeductions;
        private CheckBox _MA_RS_MARG_NetProfitGraph;
        private CheckBox _MA_RS_MARG_NetProfitFromSale;
        private CheckBox _MA_RS_MARG_CashFlowGraph;
        private CheckBox _MA_RS_MARG_CashFlow;
        private CheckBox _MA_RS_MARG_SelectAll;
        private CheckBox _MA_RS_IAR_FinancialAnalisys;
        private CheckBox _MA_RS_IAR_LoanSummary;
        private CheckBox _MA_RS_IAR_InitialExpensesSummary;
        private CheckBox _MA_RS_IAR_InitialIncomeSummary;
        private CheckBox _MA_RS_IAR_BuildingInfo;
        private CheckBox _MA_RS_IAR_SelectAll;
        private Label label342;
        private Label label341;
        private ComboBox _MA_S2_IncomePercChange;
        private ComboBox _MA_S2_VacancyPercChange;
        private ComboBox _MA_S2_OperatingExpensesPercChange;
        private ComboBox _MA_S2_InflationPercChange;
        private Label label340;
        private Button _MA_RS_MARG_PrintSelectedReports;
        private Label label352;
        private Label label351;
        private TextBox _MA_S3_IncomeChange_Y1;
        private TextBox _MA_S3_Vacancy_Y1;
        private TextBox _MA_S3_OpEx_Y1;
        private TextBox _MA_S3_IncomeChange_Y10;
        private TextBox _MA_S3_Vacancy_Y10;
        private TextBox _MA_S3_OpEx_Y10;
        private TextBox _MA_S3_IncomeChange_Y9;
        private TextBox _MA_S3_Vacancy_Y9;
        private TextBox _MA_S3_OpEx_Y9;
        private TextBox _MA_S3_IncomeChange_Y8;
        private TextBox _MA_S3_Vacancy_Y8;
        private TextBox _MA_S3_OpEx_Y8;
        private TextBox _MA_S3_IncomeChange_Y7;
        private TextBox _MA_S3_Vacancy_Y7;
        private TextBox _MA_S3_OpEx_Y7;
        private TextBox _MA_S3_IncomeChange_Y6;
        private TextBox _MA_S3_Vacancy_Y6;
        private TextBox _MA_S3_OpEx_Y6;
        private TextBox _MA_S3_IncomeChange_Y5;
        private TextBox _MA_S3_Vacancy_Y5;
        private TextBox _MA_S3_OpEx_Y5;
        private TextBox _MA_S3_IncomeChange_Y4;
        private TextBox _MA_S3_Vacancy_Y4;
        private TextBox _MA_S3_OpEx_Y4;
        private TextBox _MA_S3_IncomeChange_Y3;
        private TextBox _MA_S3_Vacancy_Y3;
        private TextBox _MA_S3_OpEx_Y3;
        private TextBox _MA_S3_IncomeChange_Y2;
        private TextBox _MA_S3_Vacancy_Y2;
        private TextBox _MA_S3_OpEx_Y2;
        private Label label350;
        private TextBox _MA_SI_SaleCapRate;
        private Label label346;
        private Label label347;
        private TextBox _MA_SI_SaleCosts;
        private Label label348;
        private Label label349;
        private Label label345;
        private Label label344;
        private Label label343;
        private Label label363;
        private Label label362;
        private Label label361;
        private Label label360;
        private Label label359;
        private Label label358;
        private Label label357;
        private Label label356;
        private Label label355;
        private Label label354;
        private Label label353;
        private TextBox _MA_S1_OperatingExpensesPerc;
        private Label label365;
        private TextBox _MA_S1_VacancyPerc;
        private Label label364;
        private ComboBox _MA_TI_DepreciableYears;
        private GroupBox groupBox1;
        private GroupBox groupBox2;
        private Button _printBuildingInfo;
        private Button _printExpensesSummary;
        private Button _PrintExpensesDetails;
        private Button _printFinancialAnalysis;
        private Button _printLoanInfo;
        private TabPage _generalIncome;
        private Income_Dummy income_Dummy1;
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
		#endregion DATA

		#region CTR and DISPOSE
		public CommercialControl()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();
			//AddEventHandlers();
			InitIncomeListViews();		
			InitExpensesListViews();
            Init_MA_DropDownBoxes();
		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}
		#endregion

		#region PROTECTED OVERRIDES
		protected override void OnPaint(PaintEventArgs e)
		{
//			e.Graphics.DrawLine(Pens.Red, new Point(1, 1), new Point(this.Size.Width - 2, 1));
//			e.Graphics.DrawLine(Pens.Red, new Point(1, 1), new Point(1, this.Size.Height - 2));
//			e.Graphics.DrawLine(Pens.Red, new Point(1, this.Size.Height - 2), new Point(this.Size.Width - 2, this.Size.Height - 2));
//			e.Graphics.DrawLine(Pens.Red, new Point(this.Size.Width - 2, 1), new Point(this.Size.Width - 2,  this.Size.Height - 2));
			base.OnPaint (e);
		}

		protected override void OnLoad(EventArgs e)
		{
			//base.OnLoad (e);
			HandleListViewExes();
		}
		#endregion

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CommercialControl));
            this._masterTabControl = new System.Windows.Forms.TabControl();
            this._buildingInfoPage = new System.Windows.Forms.TabPage();
            this._printBuildingInfo = new System.Windows.Forms.Button();
            this._bi_floater = new System.Windows.Forms.PictureBox();
            this.label53 = new System.Windows.Forms.Label();
            this._BI_windowType = new System.Windows.Forms.TextBox();
            this._BI_roofType = new System.Windows.Forms.TextBox();
            this._BI_landscaping = new System.Windows.Forms.TextBox();
            this._BI_amenities = new System.Windows.Forms.TextBox();
            this._BI_locationDescription = new System.Windows.Forms.TextBox();
            this._BI_propertyDescription = new System.Windows.Forms.TextBox();
            this.label51 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this._BI_improvementsNeeded = new System.Windows.Forms.TextBox();
            this.label47 = new System.Windows.Forms.Label();
            this._BI_recentImprovements = new System.Windows.Forms.TextBox();
            this.label46 = new System.Windows.Forms.Label();
            this._BI_landLeaseComments = new System.Windows.Forms.TextBox();
            this._BI_rent = new System.Windows.Forms.TextBox();
            this.label45 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this._BI_endDate = new System.Windows.Forms.TextBox();
            this._BI_startDate = new System.Windows.Forms.TextBox();
            this.label43 = new System.Windows.Forms.Label();
            this._BI_lessor = new System.Windows.Forms.TextBox();
            this.label42 = new System.Windows.Forms.Label();
            this._BI_option = new System.Windows.Forms.ComboBox();
            this._BI_landLease = new System.Windows.Forms.ComboBox();
            this.label41 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this._BI_comments = new System.Windows.Forms.TextBox();
            this._BI_parkingRatio = new System.Windows.Forms.TextBox();
            this.label38 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this._BI_totalFee = new System.Windows.Forms.TextBox();
            this._BI_garageFee = new System.Windows.Forms.TextBox();
            this._BI_coveredFee = new System.Windows.Forms.TextBox();
            this._BI_lotFee = new System.Windows.Forms.TextBox();
            this._BI_totalSpaces = new System.Windows.Forms.TextBox();
            this._BI_garageSpaces = new System.Windows.Forms.TextBox();
            this._BI_coveredSpaces = new System.Windows.Forms.TextBox();
            this._BI_lotSpaces = new System.Windows.Forms.TextBox();
            this.label34 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this._BI_windowsAge = new System.Windows.Forms.ComboBox();
            this._BI_sidingType = new System.Windows.Forms.ComboBox();
            this._BI_electricalAge = new System.Windows.Forms.ComboBox();
            this._BI_plumbingAge = new System.Windows.Forms.ComboBox();
            this._BI_plumbingType = new System.Windows.Forms.ComboBox();
            this._BI_roofAge = new System.Windows.Forms.ComboBox();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this._BI_lotSqft = new System.Windows.Forms.TextBox();
            this._BI_lotAcres = new System.Windows.Forms.TextBox();
            this._BI_netRentableSqFt = new System.Windows.Forms.TextBox();
            this._BI_totalSqFt = new System.Windows.Forms.TextBox();
            this._BI_structures = new System.Windows.Forms.ComboBox();
            this._BI_floors = new System.Windows.Forms.ComboBox();
            this._BI_units = new System.Windows.Forms.ComboBox();
            this._BI_rehabYear = new System.Windows.Forms.ComboBox();
            this._BI_yearBuilt = new System.Windows.Forms.ComboBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this._BI_propertyValue = new System.Windows.Forms.TextBox();
            this._BI_apn = new System.Windows.Forms.TextBox();
            this._BI_class = new System.Windows.Forms.ComboBox();
            this._BI_propertyType = new System.Windows.Forms.ComboBox();
            this._BI_propertyName = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this._generalIncome = new System.Windows.Forms.TabPage();
            this._expensesTabPage = new System.Windows.Forms.TabPage();
            this._printExpensesSummary = new System.Windows.Forms.Button();
            this._PrintExpensesDetails = new System.Windows.Forms.Button();
            this._e_floater = new System.Windows.Forms.PictureBox();
            this.label140 = new System.Windows.Forms.Label();
            this.label139 = new System.Windows.Forms.Label();
            this.label138 = new System.Windows.Forms.Label();
            this.label137 = new System.Windows.Forms.Label();
            this.label136 = new System.Windows.Forms.Label();
            this.label135 = new System.Windows.Forms.Label();
            this._E_TotalPercOfTotal_6 = new System.Windows.Forms.TextBox();
            this._E_TotalPercOfTotal_1 = new System.Windows.Forms.TextBox();
            this._E_TotalPercOfTotal_5 = new System.Windows.Forms.TextBox();
            this._E_TotalPercOfTotal_4 = new System.Windows.Forms.TextBox();
            this._E_TotalPercOfTotal_3 = new System.Windows.Forms.TextBox();
            this._E_TotalPercOfTotal_2 = new System.Windows.Forms.TextBox();
            this._E_TotalUsdPerUnit_6 = new System.Windows.Forms.TextBox();
            this._E_TotalUsdPerUnit_1 = new System.Windows.Forms.TextBox();
            this._E_TotalUsdPerUnit_5 = new System.Windows.Forms.TextBox();
            this._E_TotalUsdPerUnit_4 = new System.Windows.Forms.TextBox();
            this._E_TotalUsdPerUnit_3 = new System.Windows.Forms.TextBox();
            this._E_TotalUsdPerUnit_2 = new System.Windows.Forms.TextBox();
            this._E_TotalUsdPerSqft_6 = new System.Windows.Forms.TextBox();
            this._E_TotalUsdPerSqft_1 = new System.Windows.Forms.TextBox();
            this._E_TotalUsdPerSqft_5 = new System.Windows.Forms.TextBox();
            this._E_TotalUsdPerSqft_4 = new System.Windows.Forms.TextBox();
            this._E_TotalUsdPerSqft_3 = new System.Windows.Forms.TextBox();
            this._E_TotalUsdPerSqft_2 = new System.Windows.Forms.TextBox();
            this._E_TotalUsdPerYear_6 = new System.Windows.Forms.TextBox();
            this._E_TotalUsdPerYear_1 = new System.Windows.Forms.TextBox();
            this._E_TotalUsdPerYear_5 = new System.Windows.Forms.TextBox();
            this._E_TotalUsdPerYear_4 = new System.Windows.Forms.TextBox();
            this._E_TotalUsdPerYear_3 = new System.Windows.Forms.TextBox();
            this._E_TotalUsdPerYear_2 = new System.Windows.Forms.TextBox();
            this._E_TotalUsdPerMonth_6 = new System.Windows.Forms.TextBox();
            this._E_TotalUsdPerMonth_1 = new System.Windows.Forms.TextBox();
            this._E_TotalUsdPerMonth_5 = new System.Windows.Forms.TextBox();
            this._E_TotalUsdPerMonth_4 = new System.Windows.Forms.TextBox();
            this._E_TotalUsdPerMonth_3 = new System.Windows.Forms.TextBox();
            this._E_TotalUsdPerMonth_2 = new System.Windows.Forms.TextBox();
            this._E_TotalDesc_6 = new System.Windows.Forms.TextBox();
            this._E_TotalDesc_1 = new System.Windows.Forms.TextBox();
            this._E_TotalDesc_5 = new System.Windows.Forms.TextBox();
            this._E_TotalDesc_4 = new System.Windows.Forms.TextBox();
            this._E_TotalDesc_3 = new System.Windows.Forms.TextBox();
            this._E_TotalDesc_2 = new System.Windows.Forms.TextBox();
            this.label134 = new System.Windows.Forms.Label();
            this._E_TotalType_6 = new System.Windows.Forms.TextBox();
            this._E_TotalType_1 = new System.Windows.Forms.TextBox();
            this._E_TotalType_5 = new System.Windows.Forms.TextBox();
            this._E_TotalType_4 = new System.Windows.Forms.TextBox();
            this._E_TotalType_3 = new System.Windows.Forms.TextBox();
            this._E_TotalType_2 = new System.Windows.Forms.TextBox();
            this.label133 = new System.Windows.Forms.Label();
            this.label132 = new System.Windows.Forms.Label();
            this.label131 = new System.Windows.Forms.Label();
            this.label130 = new System.Windows.Forms.Label();
            this.label129 = new System.Windows.Forms.Label();
            this.label128 = new System.Windows.Forms.Label();
            this.label127 = new System.Windows.Forms.Label();
            this.label126 = new System.Windows.Forms.Label();
            this.label125 = new System.Windows.Forms.Label();
            this.label124 = new System.Windows.Forms.Label();
            this.label123 = new System.Windows.Forms.Label();
            this.label122 = new System.Windows.Forms.Label();
            this.label121 = new System.Windows.Forms.Label();
            this._E_CA_Type_Year10 = new System.Windows.Forms.TextBox();
            this._E_CA_Cost_Year10 = new System.Windows.Forms.TextBox();
            this._E_CA_Type_Year9 = new System.Windows.Forms.TextBox();
            this._E_CA_Cost_Year9 = new System.Windows.Forms.TextBox();
            this._E_CA_Type_Year8 = new System.Windows.Forms.TextBox();
            this._E_CA_Cost_Year8 = new System.Windows.Forms.TextBox();
            this._E_CA_Type_Year7 = new System.Windows.Forms.TextBox();
            this._E_CA_Cost_Year7 = new System.Windows.Forms.TextBox();
            this._E_CA_Type_Year6 = new System.Windows.Forms.TextBox();
            this._E_CA_Cost_Year6 = new System.Windows.Forms.TextBox();
            this._E_CA_Type_Year5 = new System.Windows.Forms.TextBox();
            this._E_CA_Cost_Year5 = new System.Windows.Forms.TextBox();
            this._E_CA_Type_Year4 = new System.Windows.Forms.TextBox();
            this._E_CA_Cost_Year4 = new System.Windows.Forms.TextBox();
            this._E_CA_Type_Year3 = new System.Windows.Forms.TextBox();
            this._E_CA_Cost_Year3 = new System.Windows.Forms.TextBox();
            this._E_CA_Type_Year2 = new System.Windows.Forms.TextBox();
            this._E_CA_Cost_Year2 = new System.Windows.Forms.TextBox();
            this._E_CA_Type_Year1 = new System.Windows.Forms.TextBox();
            this._E_CA_Cost_Year1 = new System.Windows.Forms.TextBox();
            this.label120 = new System.Windows.Forms.Label();
            this._E_UT_HotWater = new System.Windows.Forms.ComboBox();
            this.label119 = new System.Windows.Forms.Label();
            this._E_UT_Heat = new System.Windows.Forms.ComboBox();
            this.label118 = new System.Windows.Forms.Label();
            this._E_UT_ElectricityMeter = new System.Windows.Forms.ComboBox();
            this.label117 = new System.Windows.Forms.Label();
            this.label116 = new System.Windows.Forms.Label();
            this._loansTabPage = new System.Windows.Forms.TabPage();
            this._printLoanInfo = new System.Windows.Forms.Button();
            this._l_floater = new System.Windows.Forms.PictureBox();
            this._L_3L_RA_LifeTimeCap = new System.Windows.Forms.TextBox();
            this._L_3L_RA_AnnualCap = new System.Windows.Forms.TextBox();
            this.label197 = new System.Windows.Forms.Label();
            this.label198 = new System.Windows.Forms.Label();
            this._L_3L_RA_InitialRateCap = new System.Windows.Forms.TextBox();
            this._L_3L_RA_InitFixedPeriod = new System.Windows.Forms.TextBox();
            this.label199 = new System.Windows.Forms.Label();
            this.label200 = new System.Windows.Forms.Label();
            this.label201 = new System.Windows.Forms.Label();
            this.label202 = new System.Windows.Forms.Label();
            this.label203 = new System.Windows.Forms.Label();
            this.label204 = new System.Windows.Forms.Label();
            this.label205 = new System.Windows.Forms.Label();
            this.label206 = new System.Windows.Forms.Label();
            this.label207 = new System.Windows.Forms.Label();
            this.label208 = new System.Windows.Forms.Label();
            this.label209 = new System.Windows.Forms.Label();
            this.label210 = new System.Windows.Forms.Label();
            this.label211 = new System.Windows.Forms.Label();
            this.label212 = new System.Windows.Forms.Label();
            this._L_3L_RA_RateYear10 = new System.Windows.Forms.TextBox();
            this._L_3L_RA_RateYear9 = new System.Windows.Forms.TextBox();
            this._L_3L_RA_RateYear8 = new System.Windows.Forms.TextBox();
            this._L_3L_RA_RateYear7 = new System.Windows.Forms.TextBox();
            this._L_3L_RA_RateYear6 = new System.Windows.Forms.TextBox();
            this._L_3L_RA_RateYear5 = new System.Windows.Forms.TextBox();
            this._L_3L_RA_RateYear4 = new System.Windows.Forms.TextBox();
            this._L_3L_RA_RateYear3 = new System.Windows.Forms.TextBox();
            this._L_3L_RA_RateYear2 = new System.Windows.Forms.TextBox();
            this._L_3L_RA_RateYear1 = new System.Windows.Forms.TextBox();
            this._L_3L_Lender = new System.Windows.Forms.TextBox();
            this._L_3L_LastPaymentDate = new System.Windows.Forms.TextBox();
            this._L_3L_FirstPaymentDate = new System.Windows.Forms.TextBox();
            this._L_3L_Points = new System.Windows.Forms.TextBox();
            this._L_3L_PointsPerc = new System.Windows.Forms.TextBox();
            this._L_3L_MonthlyPayment = new System.Windows.Forms.TextBox();
            this._L_3L_Rate = new System.Windows.Forms.TextBox();
            this._L_3L_AmortTerm = new System.Windows.Forms.TextBox();
            this._L_3L_Amount = new System.Windows.Forms.TextBox();
            this._L_3L_LTVPerc = new System.Windows.Forms.TextBox();
            this._L_3L_Type = new System.Windows.Forms.ComboBox();
            this.label213 = new System.Windows.Forms.Label();
            this.label214 = new System.Windows.Forms.Label();
            this.label215 = new System.Windows.Forms.Label();
            this.label216 = new System.Windows.Forms.Label();
            this.label217 = new System.Windows.Forms.Label();
            this.label218 = new System.Windows.Forms.Label();
            this.label219 = new System.Windows.Forms.Label();
            this.label220 = new System.Windows.Forms.Label();
            this.label221 = new System.Windows.Forms.Label();
            this.label222 = new System.Windows.Forms.Label();
            this.label223 = new System.Windows.Forms.Label();
            this.label224 = new System.Windows.Forms.Label();
            this._L_2L_RA_LifeTimeCap = new System.Windows.Forms.TextBox();
            this._L_2L_RA_AnnualCap = new System.Windows.Forms.TextBox();
            this.label169 = new System.Windows.Forms.Label();
            this.label170 = new System.Windows.Forms.Label();
            this._L_2L_RA_InitialRateCap = new System.Windows.Forms.TextBox();
            this._L_2L_RA_InitFixedPeriod = new System.Windows.Forms.TextBox();
            this.label171 = new System.Windows.Forms.Label();
            this.label172 = new System.Windows.Forms.Label();
            this.label173 = new System.Windows.Forms.Label();
            this.label174 = new System.Windows.Forms.Label();
            this.label175 = new System.Windows.Forms.Label();
            this.label176 = new System.Windows.Forms.Label();
            this.label177 = new System.Windows.Forms.Label();
            this.label178 = new System.Windows.Forms.Label();
            this.label179 = new System.Windows.Forms.Label();
            this.label180 = new System.Windows.Forms.Label();
            this.label181 = new System.Windows.Forms.Label();
            this.label182 = new System.Windows.Forms.Label();
            this.label183 = new System.Windows.Forms.Label();
            this.label184 = new System.Windows.Forms.Label();
            this._L_2L_RA_RateYear10 = new System.Windows.Forms.TextBox();
            this._L_2L_RA_RateYear9 = new System.Windows.Forms.TextBox();
            this._L_2L_RA_RateYear8 = new System.Windows.Forms.TextBox();
            this._L_2L_RA_RateYear7 = new System.Windows.Forms.TextBox();
            this._L_2L_RA_RateYear6 = new System.Windows.Forms.TextBox();
            this._L_2L_RA_RateYear5 = new System.Windows.Forms.TextBox();
            this._L_2L_RA_RateYear4 = new System.Windows.Forms.TextBox();
            this._L_2L_RA_RateYear3 = new System.Windows.Forms.TextBox();
            this._L_2L_RA_RateYear2 = new System.Windows.Forms.TextBox();
            this._L_2L_RA_RateYear1 = new System.Windows.Forms.TextBox();
            this._L_2L_Lender = new System.Windows.Forms.TextBox();
            this._L_2L_LastPaymentDate = new System.Windows.Forms.TextBox();
            this._L_2L_FirstPaymentDate = new System.Windows.Forms.TextBox();
            this._L_2L_Points = new System.Windows.Forms.TextBox();
            this._L_2L_PointsPerc = new System.Windows.Forms.TextBox();
            this._L_2L_MonthlyPayment = new System.Windows.Forms.TextBox();
            this._L_2L_Rate = new System.Windows.Forms.TextBox();
            this._L_2L_AmortTerm = new System.Windows.Forms.TextBox();
            this._L_2L_Amount = new System.Windows.Forms.TextBox();
            this._L_2L_LTVPerc = new System.Windows.Forms.TextBox();
            this._L_2L_Type = new System.Windows.Forms.ComboBox();
            this.label185 = new System.Windows.Forms.Label();
            this.label186 = new System.Windows.Forms.Label();
            this.label187 = new System.Windows.Forms.Label();
            this.label188 = new System.Windows.Forms.Label();
            this.label189 = new System.Windows.Forms.Label();
            this.label190 = new System.Windows.Forms.Label();
            this.label191 = new System.Windows.Forms.Label();
            this.label192 = new System.Windows.Forms.Label();
            this.label193 = new System.Windows.Forms.Label();
            this.label194 = new System.Windows.Forms.Label();
            this.label195 = new System.Windows.Forms.Label();
            this.label196 = new System.Windows.Forms.Label();
            this._L_1L_RA_LifeTimeCap = new System.Windows.Forms.TextBox();
            this._L_1L_RA_AnnualCap = new System.Windows.Forms.TextBox();
            this.label167 = new System.Windows.Forms.Label();
            this.label168 = new System.Windows.Forms.Label();
            this._L_1L_RA_InitialRateCap = new System.Windows.Forms.TextBox();
            this._L_1L_RA_InitFixedPeriod = new System.Windows.Forms.TextBox();
            this.label166 = new System.Windows.Forms.Label();
            this.label165 = new System.Windows.Forms.Label();
            this.label164 = new System.Windows.Forms.Label();
            this.label153 = new System.Windows.Forms.Label();
            this.label154 = new System.Windows.Forms.Label();
            this.label155 = new System.Windows.Forms.Label();
            this.label156 = new System.Windows.Forms.Label();
            this.label157 = new System.Windows.Forms.Label();
            this.label158 = new System.Windows.Forms.Label();
            this.label159 = new System.Windows.Forms.Label();
            this.label160 = new System.Windows.Forms.Label();
            this.label161 = new System.Windows.Forms.Label();
            this.label162 = new System.Windows.Forms.Label();
            this.label163 = new System.Windows.Forms.Label();
            this._L_1L_RA_RateYear10 = new System.Windows.Forms.TextBox();
            this._L_1L_RA_RateYear9 = new System.Windows.Forms.TextBox();
            this._L_1L_RA_RateYear8 = new System.Windows.Forms.TextBox();
            this._L_1L_RA_RateYear7 = new System.Windows.Forms.TextBox();
            this._L_1L_RA_RateYear6 = new System.Windows.Forms.TextBox();
            this._L_1L_RA_RateYear5 = new System.Windows.Forms.TextBox();
            this._L_1L_RA_RateYear4 = new System.Windows.Forms.TextBox();
            this._L_1L_RA_RateYear3 = new System.Windows.Forms.TextBox();
            this._L_1L_RA_RateYear2 = new System.Windows.Forms.TextBox();
            this._L_1L_RA_RateYear1 = new System.Windows.Forms.TextBox();
            this._L_1L_Lender = new System.Windows.Forms.TextBox();
            this._L_1L_LastPaymentDate = new System.Windows.Forms.TextBox();
            this._L_1L_FirstPaymentDate = new System.Windows.Forms.TextBox();
            this._L_1L_Points = new System.Windows.Forms.TextBox();
            this._L_1L_PointsPerc = new System.Windows.Forms.TextBox();
            this._L_1L_MonthlyPayment = new System.Windows.Forms.TextBox();
            this._L_1L_Rate = new System.Windows.Forms.TextBox();
            this._L_1L_AmortTerm = new System.Windows.Forms.TextBox();
            this._L_1L_Amount = new System.Windows.Forms.TextBox();
            this._L_1L_LTVPerc = new System.Windows.Forms.TextBox();
            this._L_1L_Type = new System.Windows.Forms.ComboBox();
            this.label152 = new System.Windows.Forms.Label();
            this.label151 = new System.Windows.Forms.Label();
            this.label150 = new System.Windows.Forms.Label();
            this.label149 = new System.Windows.Forms.Label();
            this.label148 = new System.Windows.Forms.Label();
            this.label147 = new System.Windows.Forms.Label();
            this.label146 = new System.Windows.Forms.Label();
            this.label145 = new System.Windows.Forms.Label();
            this.label144 = new System.Windows.Forms.Label();
            this.label143 = new System.Windows.Forms.Label();
            this.label142 = new System.Windows.Forms.Label();
            this.label141 = new System.Windows.Forms.Label();
            this._L_TotalPaymentsAnnual = new System.Windows.Forms.TextBox();
            this._L_TotalPaymentsMonthly = new System.Windows.Forms.TextBox();
            this.label94 = new System.Windows.Forms.Label();
            this._L_DCR = new System.Windows.Forms.TextBox();
            this._L_CLTVPerc = new System.Windows.Forms.TextBox();
            this._L_BlendedRate = new System.Windows.Forms.TextBox();
            this.label82 = new System.Windows.Forms.Label();
            this.label81 = new System.Windows.Forms.Label();
            this.label80 = new System.Windows.Forms.Label();
            this.label79 = new System.Windows.Forms.Label();
            this.label78 = new System.Windows.Forms.Label();
            this._L_SellerCarryback = new System.Windows.Forms.ComboBox();
            this._L_Assumable = new System.Windows.Forms.ComboBox();
            this._L_1stAssumable = new System.Windows.Forms.ComboBox();
            this.label77 = new System.Windows.Forms.Label();
            this.label76 = new System.Windows.Forms.Label();
            this.label75 = new System.Windows.Forms.Label();
            this._L_DownPaymentPerc = new System.Windows.Forms.ComboBox();
            this._L_DownPayment = new System.Windows.Forms.TextBox();
            this._L_PurchasePrice = new System.Windows.Forms.TextBox();
            this.label74 = new System.Windows.Forms.Label();
            this.label73 = new System.Windows.Forms.Label();
            this.label72 = new System.Windows.Forms.Label();
            this.label71 = new System.Windows.Forms.Label();
            this._financialAnalysisTabPage = new System.Windows.Forms.TabPage();
            this._printFinancialAnalysis = new System.Windows.Forms.Button();
            this._fa_floater = new System.Windows.Forms.PictureBox();
            this._FA_LR_Range_NUD = new System.Windows.Forms.NumericUpDown();
            this._FA_OE_Range_NUD = new System.Windows.Forms.NumericUpDown();
            this._FA_GSI_Range_NUD = new System.Windows.Forms.NumericUpDown();
            this._FA_CR_Change_NUD = new System.Windows.Forms.NumericUpDown();
            this.label305 = new System.Windows.Forms.Label();
            this.label302 = new System.Windows.Forms.Label();
            this.label299 = new System.Windows.Forms.Label();
            this.label295 = new System.Windows.Forms.Label();
            this.label293 = new System.Windows.Forms.Label();
            this.label290 = new System.Windows.Forms.Label();
            this.label322 = new System.Windows.Forms.Label();
            this.label320 = new System.Windows.Forms.Label();
            this.label318 = new System.Windows.Forms.Label();
            this.label316 = new System.Windows.Forms.Label();
            this.label315 = new System.Windows.Forms.Label();
            this.label313 = new System.Windows.Forms.Label();
            this.label312 = new System.Windows.Forms.Label();
            this.label310 = new System.Windows.Forms.Label();
            this.label308 = new System.Windows.Forms.Label();
            this.label306 = new System.Windows.Forms.Label();
            this.label304 = new System.Windows.Forms.Label();
            this.label303 = new System.Windows.Forms.Label();
            this.label301 = new System.Windows.Forms.Label();
            this.label300 = new System.Windows.Forms.Label();
            this.label298 = new System.Windows.Forms.Label();
            this.label296 = new System.Windows.Forms.Label();
            this.label294 = new System.Windows.Forms.Label();
            this.label292 = new System.Windows.Forms.Label();
            this.label291 = new System.Windows.Forms.Label();
            this.label289 = new System.Windows.Forms.Label();
            this.label288 = new System.Windows.Forms.Label();
            this.label287 = new System.Windows.Forms.Label();
            this.label286 = new System.Windows.Forms.Label();
            this.label285 = new System.Windows.Forms.Label();
            this.label284 = new System.Windows.Forms.Label();
            this.label283 = new System.Windows.Forms.Label();
            this.label282 = new System.Windows.Forms.Label();
            this.label281 = new System.Windows.Forms.Label();
            this._FA_S_OperatingExpensesPerc = new System.Windows.Forms.TextBox();
            this.label280 = new System.Windows.Forms.Label();
            this._FA_P_OperatingExpensesPerc = new System.Windows.Forms.TextBox();
            this.label279 = new System.Windows.Forms.Label();
            this._FA_A_OperatingExpensesPerc = new System.Windows.Forms.TextBox();
            this.label277 = new System.Windows.Forms.Label();
            this.label278 = new System.Windows.Forms.Label();
            this._FA_Graph4 = new System.Windows.Forms.Button();
            this.label275 = new System.Windows.Forms.Label();
            this.label276 = new System.Windows.Forms.Label();
            this._FA_Graph3 = new System.Windows.Forms.Button();
            this.label273 = new System.Windows.Forms.Label();
            this.label274 = new System.Windows.Forms.Label();
            this._FA_Graph2 = new System.Windows.Forms.Button();
            this._FA_S_Comment = new System.Windows.Forms.TextBox();
            this.label258 = new System.Windows.Forms.Label();
            this.label259 = new System.Windows.Forms.Label();
            this.label260 = new System.Windows.Forms.Label();
            this.label261 = new System.Windows.Forms.Label();
            this.label262 = new System.Windows.Forms.Label();
            this.label263 = new System.Windows.Forms.Label();
            this.label264 = new System.Windows.Forms.Label();
            this.label265 = new System.Windows.Forms.Label();
            this.label266 = new System.Windows.Forms.Label();
            this.label267 = new System.Windows.Forms.Label();
            this.label268 = new System.Windows.Forms.Label();
            this._FA_S_VacancyCreditLossPerc = new System.Windows.Forms.TextBox();
            this.label269 = new System.Windows.Forms.Label();
            this.label270 = new System.Windows.Forms.Label();
            this.label271 = new System.Windows.Forms.Label();
            this._FA_S_CashOnCashReturn = new System.Windows.Forms.TextBox();
            this._FA_S_CashFlow = new System.Windows.Forms.TextBox();
            this._FA_S_DebtCoverageRatio = new System.Windows.Forms.TextBox();
            this._FA_S_DebtService = new System.Windows.Forms.TextBox();
            this._FA_S_CapRate = new System.Windows.Forms.TextBox();
            this._FA_S_NetOperatingIncome = new System.Windows.Forms.TextBox();
            this._FA_S_OperatingExpenses = new System.Windows.Forms.TextBox();
            this._FA_S_GOIAnnual = new System.Windows.Forms.TextBox();
            this._FA_S_OtherIncome = new System.Windows.Forms.TextBox();
            this._FA_S_VacancyCreditLoss = new System.Windows.Forms.TextBox();
            this._FA_S_GSIAnnual = new System.Windows.Forms.TextBox();
            this._FA_S_Price = new System.Windows.Forms.TextBox();
            this._FA_S_copyFromProforma = new System.Windows.Forms.Button();
            this._FA_S_copyFromActual = new System.Windows.Forms.Button();
            this.label272 = new System.Windows.Forms.Label();
            this._FA_P_Comment = new System.Windows.Forms.TextBox();
            this.label243 = new System.Windows.Forms.Label();
            this.label244 = new System.Windows.Forms.Label();
            this.label245 = new System.Windows.Forms.Label();
            this.label246 = new System.Windows.Forms.Label();
            this.label247 = new System.Windows.Forms.Label();
            this.label248 = new System.Windows.Forms.Label();
            this.label249 = new System.Windows.Forms.Label();
            this.label250 = new System.Windows.Forms.Label();
            this.label251 = new System.Windows.Forms.Label();
            this.label252 = new System.Windows.Forms.Label();
            this.label253 = new System.Windows.Forms.Label();
            this._FA_P_VacancyCreditLossPerc = new System.Windows.Forms.TextBox();
            this.label254 = new System.Windows.Forms.Label();
            this.label255 = new System.Windows.Forms.Label();
            this.label256 = new System.Windows.Forms.Label();
            this._FA_P_CashOnCashReturn = new System.Windows.Forms.TextBox();
            this._FA_P_CashFlow = new System.Windows.Forms.TextBox();
            this._FA_P_DebtCoverageRatio = new System.Windows.Forms.TextBox();
            this._FA_P_DebtService = new System.Windows.Forms.TextBox();
            this._FA_P_CapRate = new System.Windows.Forms.TextBox();
            this._FA_P_NetOperatingIncome = new System.Windows.Forms.TextBox();
            this._FA_P_OperatingExpenses = new System.Windows.Forms.TextBox();
            this._FA_P_GOIAnnual = new System.Windows.Forms.TextBox();
            this._FA_P_OtherIncome = new System.Windows.Forms.TextBox();
            this._FA_P_VacancyCreditLoss = new System.Windows.Forms.TextBox();
            this._FA_P_GSIAnnual = new System.Windows.Forms.TextBox();
            this._FA_P_Price = new System.Windows.Forms.TextBox();
            this._FA_P_copyFromActual = new System.Windows.Forms.Button();
            this._FA_P_copyEfromExpenses = new System.Windows.Forms.Button();
            this._FA_P_copyPIfromIncome = new System.Windows.Forms.Button();
            this.label257 = new System.Windows.Forms.Label();
            this.label242 = new System.Windows.Forms.Label();
            this.label241 = new System.Windows.Forms.Label();
            this._FA_Graph1 = new System.Windows.Forms.Button();
            this.label240 = new System.Windows.Forms.Label();
            this._FA_A_Comment = new System.Windows.Forms.TextBox();
            this.label239 = new System.Windows.Forms.Label();
            this.label238 = new System.Windows.Forms.Label();
            this.label237 = new System.Windows.Forms.Label();
            this.label236 = new System.Windows.Forms.Label();
            this.label235 = new System.Windows.Forms.Label();
            this.label234 = new System.Windows.Forms.Label();
            this.label233 = new System.Windows.Forms.Label();
            this.label232 = new System.Windows.Forms.Label();
            this.label231 = new System.Windows.Forms.Label();
            this.label230 = new System.Windows.Forms.Label();
            this.label229 = new System.Windows.Forms.Label();
            this._FA_A_VacancyCreditLossPerc = new System.Windows.Forms.TextBox();
            this.label228 = new System.Windows.Forms.Label();
            this.label227 = new System.Windows.Forms.Label();
            this.label226 = new System.Windows.Forms.Label();
            this._FA_A_CashOnCashReturn = new System.Windows.Forms.TextBox();
            this._FA_A_CashFlow = new System.Windows.Forms.TextBox();
            this._FA_A_DebtCoverageRatio = new System.Windows.Forms.TextBox();
            this._FA_A_DebtService = new System.Windows.Forms.TextBox();
            this._FA_A_CapRate = new System.Windows.Forms.TextBox();
            this._FA_A_NetOperatingIncome = new System.Windows.Forms.TextBox();
            this._FA_A_OperatingExpenses = new System.Windows.Forms.TextBox();
            this._FA_A_GOIAnnual = new System.Windows.Forms.TextBox();
            this._FA_A_OtherIncome = new System.Windows.Forms.TextBox();
            this._FA_A_VacancyCreditLoss = new System.Windows.Forms.TextBox();
            this._FA_A_GSIAnnual = new System.Windows.Forms.TextBox();
            this._FA_A_Price = new System.Windows.Forms.TextBox();
            this._FA_copyPDfromLoan = new System.Windows.Forms.Button();
            this._FA_copyEfromExpenses = new System.Windows.Forms.Button();
            this._FA_copyAIfromIncome = new System.Windows.Forms.Button();
            this.label225 = new System.Windows.Forms.Label();
            this._multiYearAnalysis = new System.Windows.Forms.TabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label339 = new System.Windows.Forms.Label();
            this._MA_RS_MARG_AnalysisPeriod = new System.Windows.Forms.ComboBox();
            this._MA_RS_MARG_SelectAll = new System.Windows.Forms.CheckBox();
            this._MA_RS_MARG_CashFlow = new System.Windows.Forms.CheckBox();
            this._MA_RS_MARG_CashFlowGraph = new System.Windows.Forms.CheckBox();
            this._MA_RS_MARG_NetProfitFromSale = new System.Windows.Forms.CheckBox();
            this._MA_RS_MARG_NetProfitGraph = new System.Windows.Forms.CheckBox();
            this._MA_RS_MARG_TaxesAndDeductions = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this._MA_RS_IAR_SelectAll = new System.Windows.Forms.CheckBox();
            this._MA_RS_IAR_BuildingInfo = new System.Windows.Forms.CheckBox();
            this._MA_RS_IAR_InitialIncomeSummary = new System.Windows.Forms.CheckBox();
            this._MA_RS_IAR_InitialExpensesSummary = new System.Windows.Forms.CheckBox();
            this._MA_RS_IAR_LoanSummary = new System.Windows.Forms.CheckBox();
            this._MA_RS_IAR_FinancialAnalisys = new System.Windows.Forms.CheckBox();
            this._MA_S2_IncomePercChange = new System.Windows.Forms.ComboBox();
            this._MA_S2_VacancyPercChange = new System.Windows.Forms.ComboBox();
            this._MA_S2_OperatingExpensesPercChange = new System.Windows.Forms.ComboBox();
            this._MA_S2_InflationPercChange = new System.Windows.Forms.ComboBox();
            this._MA_TI_DepreciableYears = new System.Windows.Forms.ComboBox();
            this._MA_S1_OperatingExpensesPerc = new System.Windows.Forms.TextBox();
            this.label365 = new System.Windows.Forms.Label();
            this._MA_S1_VacancyPerc = new System.Windows.Forms.TextBox();
            this.label364 = new System.Windows.Forms.Label();
            this._MA_S3_OpEx_Y1 = new System.Windows.Forms.TextBox();
            this._MA_S3_OpEx_Y10 = new System.Windows.Forms.TextBox();
            this._MA_S3_OpEx_Y9 = new System.Windows.Forms.TextBox();
            this._MA_S3_OpEx_Y8 = new System.Windows.Forms.TextBox();
            this._MA_S3_OpEx_Y7 = new System.Windows.Forms.TextBox();
            this._MA_S3_OpEx_Y6 = new System.Windows.Forms.TextBox();
            this._MA_S3_OpEx_Y5 = new System.Windows.Forms.TextBox();
            this._MA_S3_OpEx_Y4 = new System.Windows.Forms.TextBox();
            this._MA_S3_OpEx_Y3 = new System.Windows.Forms.TextBox();
            this._MA_S3_OpEx_Y2 = new System.Windows.Forms.TextBox();
            this.label363 = new System.Windows.Forms.Label();
            this.label362 = new System.Windows.Forms.Label();
            this.label361 = new System.Windows.Forms.Label();
            this.label360 = new System.Windows.Forms.Label();
            this.label359 = new System.Windows.Forms.Label();
            this.label358 = new System.Windows.Forms.Label();
            this.label357 = new System.Windows.Forms.Label();
            this.label356 = new System.Windows.Forms.Label();
            this.label355 = new System.Windows.Forms.Label();
            this.label354 = new System.Windows.Forms.Label();
            this.label353 = new System.Windows.Forms.Label();
            this.label352 = new System.Windows.Forms.Label();
            this.label351 = new System.Windows.Forms.Label();
            this._MA_S3_IncomeChange_Y1 = new System.Windows.Forms.TextBox();
            this._MA_S3_Vacancy_Y1 = new System.Windows.Forms.TextBox();
            this._MA_S3_IncomeChange_Y10 = new System.Windows.Forms.TextBox();
            this._MA_S3_Vacancy_Y10 = new System.Windows.Forms.TextBox();
            this._MA_S3_IncomeChange_Y9 = new System.Windows.Forms.TextBox();
            this._MA_S3_Vacancy_Y9 = new System.Windows.Forms.TextBox();
            this._MA_S3_IncomeChange_Y8 = new System.Windows.Forms.TextBox();
            this._MA_S3_Vacancy_Y8 = new System.Windows.Forms.TextBox();
            this._MA_S3_IncomeChange_Y7 = new System.Windows.Forms.TextBox();
            this._MA_S3_Vacancy_Y7 = new System.Windows.Forms.TextBox();
            this._MA_S3_IncomeChange_Y6 = new System.Windows.Forms.TextBox();
            this._MA_S3_Vacancy_Y6 = new System.Windows.Forms.TextBox();
            this._MA_S3_IncomeChange_Y5 = new System.Windows.Forms.TextBox();
            this._MA_S3_Vacancy_Y5 = new System.Windows.Forms.TextBox();
            this._MA_S3_IncomeChange_Y4 = new System.Windows.Forms.TextBox();
            this._MA_S3_Vacancy_Y4 = new System.Windows.Forms.TextBox();
            this._MA_S3_IncomeChange_Y3 = new System.Windows.Forms.TextBox();
            this._MA_S3_Vacancy_Y3 = new System.Windows.Forms.TextBox();
            this._MA_S3_IncomeChange_Y2 = new System.Windows.Forms.TextBox();
            this._MA_S3_Vacancy_Y2 = new System.Windows.Forms.TextBox();
            this.label350 = new System.Windows.Forms.Label();
            this._MA_SI_SaleCapRate = new System.Windows.Forms.TextBox();
            this.label346 = new System.Windows.Forms.Label();
            this.label347 = new System.Windows.Forms.Label();
            this._MA_SI_SaleCosts = new System.Windows.Forms.TextBox();
            this.label348 = new System.Windows.Forms.Label();
            this.label349 = new System.Windows.Forms.Label();
            this.label345 = new System.Windows.Forms.Label();
            this.label344 = new System.Windows.Forms.Label();
            this.label343 = new System.Windows.Forms.Label();
            this.label342 = new System.Windows.Forms.Label();
            this.label341 = new System.Windows.Forms.Label();
            this.label340 = new System.Windows.Forms.Label();
            this._MA_RS_MARG_PrintSelectedReports = new System.Windows.Forms.Button();
            this._MA_TI_BuildingValue = new System.Windows.Forms.TextBox();
            this.label335 = new System.Windows.Forms.Label();
            this.label336 = new System.Windows.Forms.Label();
            this.label334 = new System.Windows.Forms.Label();
            this._MA_TI_CapitalGainsRate = new System.Windows.Forms.TextBox();
            this.label332 = new System.Windows.Forms.Label();
            this.label333 = new System.Windows.Forms.Label();
            this._MA_TI_StateRate = new System.Windows.Forms.TextBox();
            this.label330 = new System.Windows.Forms.Label();
            this.label331 = new System.Windows.Forms.Label();
            this._MA_TI_FederalRate = new System.Windows.Forms.TextBox();
            this.label329 = new System.Windows.Forms.Label();
            this._MA_S1_OtherIncomePerc = new System.Windows.Forms.TextBox();
            this._MA_S1_GSI_Perc = new System.Windows.Forms.TextBox();
            this.label328 = new System.Windows.Forms.Label();
            this._MA_OtherAcquisitionCosts = new System.Windows.Forms.TextBox();
            this._MA_Points = new System.Windows.Forms.TextBox();
            this._MA_DownPayment = new System.Windows.Forms.TextBox();
            this._MA_InitialInvestment = new System.Windows.Forms.TextBox();
            this._MA_PurchasePrice = new System.Windows.Forms.TextBox();
            this._MA_S1_OperatingExpenses = new System.Windows.Forms.TextBox();
            this._MA_S1_OtherIncome = new System.Windows.Forms.TextBox();
            this._MA_S1_Vacancy = new System.Windows.Forms.TextBox();
            this._MA_S1_GSI = new System.Windows.Forms.TextBox();
            this.label327 = new System.Windows.Forms.Label();
            this.label326 = new System.Windows.Forms.Label();
            this.label325 = new System.Windows.Forms.Label();
            this.label324 = new System.Windows.Forms.Label();
            this.label323 = new System.Windows.Forms.Label();
            this.label321 = new System.Windows.Forms.Label();
            this.label319 = new System.Windows.Forms.Label();
            this.label317 = new System.Windows.Forms.Label();
            this.label314 = new System.Windows.Forms.Label();
            this.label311 = new System.Windows.Forms.Label();
            this._MA_CopyFromProforma = new System.Windows.Forms.Button();
            this._MA_CopyFromActual = new System.Windows.Forms.Button();
            this.label309 = new System.Windows.Forms.Label();
            this.label307 = new System.Windows.Forms.Label();
            this.label297 = new System.Windows.Forms.Label();
            this.label65 = new System.Windows.Forms.Label();
            this.label64 = new System.Windows.Forms.Label();
            this.label63 = new System.Windows.Forms.Label();
            this.label62 = new System.Windows.Forms.Label();
            this.label61 = new System.Windows.Forms.Label();
            this.label60 = new System.Windows.Forms.Label();
            this.label59 = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this.label57 = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.label55 = new System.Windows.Forms.Label();
            this._ma_copyFromLoanTab = new System.Windows.Forms.Button();
            this.label54 = new System.Windows.Forms.Label();
            this.income_Dummy1 = new ProfitGrabber.Commercial.Income_Dummy();
            this._E_expenses = new ProfitGrabber.Commercial.ListViewEx();
            this.columnHeader13 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader14 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader15 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader16 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader17 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader18 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader19 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader20 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader21 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader22 = new System.Windows.Forms.ColumnHeader();
            this._masterTabControl.SuspendLayout();
            this._buildingInfoPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._bi_floater)).BeginInit();
            this._generalIncome.SuspendLayout();
            this._expensesTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._e_floater)).BeginInit();
            this._loansTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._l_floater)).BeginInit();
            this._financialAnalysisTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._fa_floater)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._FA_LR_Range_NUD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._FA_OE_Range_NUD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._FA_GSI_Range_NUD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._FA_CR_Change_NUD)).BeginInit();
            this._multiYearAnalysis.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // _masterTabControl
            // 
            this._masterTabControl.Controls.Add(this._buildingInfoPage);
            this._masterTabControl.Controls.Add(this._generalIncome);
            this._masterTabControl.Controls.Add(this._expensesTabPage);
            this._masterTabControl.Controls.Add(this._loansTabPage);
            this._masterTabControl.Controls.Add(this._financialAnalysisTabPage);
            this._masterTabControl.Controls.Add(this._multiYearAnalysis);
            this._masterTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this._masterTabControl.Location = new System.Drawing.Point(0, 0);
            this._masterTabControl.Name = "_masterTabControl";
            this._masterTabControl.SelectedIndex = 0;
            this._masterTabControl.Size = new System.Drawing.Size(905, 555);
            this._masterTabControl.TabIndex = 0;
            // 
            // _buildingInfoPage
            // 
            this._buildingInfoPage.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this._buildingInfoPage.Controls.Add(this._printBuildingInfo);
            this._buildingInfoPage.Controls.Add(this._bi_floater);
            this._buildingInfoPage.Controls.Add(this.label53);
            this._buildingInfoPage.Controls.Add(this._BI_windowType);
            this._buildingInfoPage.Controls.Add(this._BI_roofType);
            this._buildingInfoPage.Controls.Add(this._BI_landscaping);
            this._buildingInfoPage.Controls.Add(this._BI_amenities);
            this._buildingInfoPage.Controls.Add(this._BI_locationDescription);
            this._buildingInfoPage.Controls.Add(this._BI_propertyDescription);
            this._buildingInfoPage.Controls.Add(this.label51);
            this._buildingInfoPage.Controls.Add(this.label50);
            this._buildingInfoPage.Controls.Add(this.label49);
            this._buildingInfoPage.Controls.Add(this.label48);
            this._buildingInfoPage.Controls.Add(this._BI_improvementsNeeded);
            this._buildingInfoPage.Controls.Add(this.label47);
            this._buildingInfoPage.Controls.Add(this._BI_recentImprovements);
            this._buildingInfoPage.Controls.Add(this.label46);
            this._buildingInfoPage.Controls.Add(this._BI_landLeaseComments);
            this._buildingInfoPage.Controls.Add(this._BI_rent);
            this._buildingInfoPage.Controls.Add(this.label45);
            this._buildingInfoPage.Controls.Add(this.label44);
            this._buildingInfoPage.Controls.Add(this._BI_endDate);
            this._buildingInfoPage.Controls.Add(this._BI_startDate);
            this._buildingInfoPage.Controls.Add(this.label43);
            this._buildingInfoPage.Controls.Add(this._BI_lessor);
            this._buildingInfoPage.Controls.Add(this.label42);
            this._buildingInfoPage.Controls.Add(this._BI_option);
            this._buildingInfoPage.Controls.Add(this._BI_landLease);
            this._buildingInfoPage.Controls.Add(this.label41);
            this._buildingInfoPage.Controls.Add(this.label40);
            this._buildingInfoPage.Controls.Add(this.label39);
            this._buildingInfoPage.Controls.Add(this._BI_comments);
            this._buildingInfoPage.Controls.Add(this._BI_parkingRatio);
            this._buildingInfoPage.Controls.Add(this.label38);
            this._buildingInfoPage.Controls.Add(this.label37);
            this._buildingInfoPage.Controls.Add(this.label36);
            this._buildingInfoPage.Controls.Add(this.label35);
            this._buildingInfoPage.Controls.Add(this._BI_totalFee);
            this._buildingInfoPage.Controls.Add(this._BI_garageFee);
            this._buildingInfoPage.Controls.Add(this._BI_coveredFee);
            this._buildingInfoPage.Controls.Add(this._BI_lotFee);
            this._buildingInfoPage.Controls.Add(this._BI_totalSpaces);
            this._buildingInfoPage.Controls.Add(this._BI_garageSpaces);
            this._buildingInfoPage.Controls.Add(this._BI_coveredSpaces);
            this._buildingInfoPage.Controls.Add(this._BI_lotSpaces);
            this._buildingInfoPage.Controls.Add(this.label34);
            this._buildingInfoPage.Controls.Add(this.label33);
            this._buildingInfoPage.Controls.Add(this.label32);
            this._buildingInfoPage.Controls.Add(this.label31);
            this._buildingInfoPage.Controls.Add(this.label30);
            this._buildingInfoPage.Controls.Add(this.label29);
            this._buildingInfoPage.Controls.Add(this.label28);
            this._buildingInfoPage.Controls.Add(this.label27);
            this._buildingInfoPage.Controls.Add(this.label26);
            this._buildingInfoPage.Controls.Add(this.label25);
            this._buildingInfoPage.Controls.Add(this._BI_windowsAge);
            this._buildingInfoPage.Controls.Add(this._BI_sidingType);
            this._buildingInfoPage.Controls.Add(this._BI_electricalAge);
            this._buildingInfoPage.Controls.Add(this._BI_plumbingAge);
            this._buildingInfoPage.Controls.Add(this._BI_plumbingType);
            this._buildingInfoPage.Controls.Add(this._BI_roofAge);
            this._buildingInfoPage.Controls.Add(this.label24);
            this._buildingInfoPage.Controls.Add(this.label23);
            this._buildingInfoPage.Controls.Add(this.label22);
            this._buildingInfoPage.Controls.Add(this.label21);
            this._buildingInfoPage.Controls.Add(this.label20);
            this._buildingInfoPage.Controls.Add(this.label19);
            this._buildingInfoPage.Controls.Add(this.label18);
            this._buildingInfoPage.Controls.Add(this.label17);
            this._buildingInfoPage.Controls.Add(this._BI_lotSqft);
            this._buildingInfoPage.Controls.Add(this._BI_lotAcres);
            this._buildingInfoPage.Controls.Add(this._BI_netRentableSqFt);
            this._buildingInfoPage.Controls.Add(this._BI_totalSqFt);
            this._buildingInfoPage.Controls.Add(this._BI_structures);
            this._buildingInfoPage.Controls.Add(this._BI_floors);
            this._buildingInfoPage.Controls.Add(this._BI_units);
            this._buildingInfoPage.Controls.Add(this._BI_rehabYear);
            this._buildingInfoPage.Controls.Add(this._BI_yearBuilt);
            this._buildingInfoPage.Controls.Add(this.label16);
            this._buildingInfoPage.Controls.Add(this.label15);
            this._buildingInfoPage.Controls.Add(this.label14);
            this._buildingInfoPage.Controls.Add(this.label13);
            this._buildingInfoPage.Controls.Add(this.label12);
            this._buildingInfoPage.Controls.Add(this.label11);
            this._buildingInfoPage.Controls.Add(this.label10);
            this._buildingInfoPage.Controls.Add(this.label9);
            this._buildingInfoPage.Controls.Add(this.label8);
            this._buildingInfoPage.Controls.Add(this.label7);
            this._buildingInfoPage.Controls.Add(this.label6);
            this._buildingInfoPage.Controls.Add(this._BI_propertyValue);
            this._buildingInfoPage.Controls.Add(this._BI_apn);
            this._buildingInfoPage.Controls.Add(this._BI_class);
            this._buildingInfoPage.Controls.Add(this._BI_propertyType);
            this._buildingInfoPage.Controls.Add(this._BI_propertyName);
            this._buildingInfoPage.Controls.Add(this.label5);
            this._buildingInfoPage.Controls.Add(this.label4);
            this._buildingInfoPage.Controls.Add(this.label3);
            this._buildingInfoPage.Controls.Add(this.label2);
            this._buildingInfoPage.Controls.Add(this.label1);
            this._buildingInfoPage.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this._buildingInfoPage.Location = new System.Drawing.Point(4, 22);
            this._buildingInfoPage.Name = "_buildingInfoPage";
            this._buildingInfoPage.Size = new System.Drawing.Size(897, 529);
            this._buildingInfoPage.TabIndex = 0;
            this._buildingInfoPage.Text = "Building Info";
            // 
            // _printBuildingInfo
            // 
            this._printBuildingInfo.Location = new System.Drawing.Point(811, 24);
            this._printBuildingInfo.Name = "_printBuildingInfo";
            this._printBuildingInfo.Size = new System.Drawing.Size(75, 23);
            this._printBuildingInfo.TabIndex = 95;
            this._printBuildingInfo.Text = "Print";
            this._printBuildingInfo.UseVisualStyleBackColor = true;
            // 
            // _bi_floater
            // 
            this._bi_floater.Image = ((System.Drawing.Image)(resources.GetObject("_bi_floater.Image")));
            this._bi_floater.Location = new System.Drawing.Point(872, 8);
            this._bi_floater.Name = "_bi_floater";
            this._bi_floater.Size = new System.Drawing.Size(16, 16);
            this._bi_floater.TabIndex = 94;
            this._bi_floater.TabStop = false;
            // 
            // label53
            // 
            this.label53.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label53.Location = new System.Drawing.Point(128, 208);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(88, 23);
            this.label53.TabIndex = 93;
            this.label53.Text = "spaces per unit";
            // 
            // _BI_windowType
            // 
            this._BI_windowType.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._BI_windowType.Location = new System.Drawing.Point(672, 144);
            this._BI_windowType.Name = "_BI_windowType";
            this._BI_windowType.Size = new System.Drawing.Size(112, 20);
            this._BI_windowType.TabIndex = 25;
            // 
            // _BI_roofType
            // 
            this._BI_roofType.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._BI_roofType.Location = new System.Drawing.Point(96, 144);
            this._BI_roofType.Name = "_BI_roofType";
            this._BI_roofType.Size = new System.Drawing.Size(120, 20);
            this._BI_roofType.TabIndex = 20;
            // 
            // _BI_landscaping
            // 
            this._BI_landscaping.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._BI_landscaping.Location = new System.Drawing.Point(680, 392);
            this._BI_landscaping.Multiline = true;
            this._BI_landscaping.Name = "_BI_landscaping";
            this._BI_landscaping.Size = new System.Drawing.Size(208, 120);
            this._BI_landscaping.TabIndex = 58;
            // 
            // _BI_amenities
            // 
            this._BI_amenities.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._BI_amenities.Location = new System.Drawing.Point(456, 392);
            this._BI_amenities.Multiline = true;
            this._BI_amenities.Name = "_BI_amenities";
            this._BI_amenities.Size = new System.Drawing.Size(208, 120);
            this._BI_amenities.TabIndex = 57;
            // 
            // _BI_locationDescription
            // 
            this._BI_locationDescription.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._BI_locationDescription.Location = new System.Drawing.Point(232, 392);
            this._BI_locationDescription.Multiline = true;
            this._BI_locationDescription.Name = "_BI_locationDescription";
            this._BI_locationDescription.Size = new System.Drawing.Size(208, 120);
            this._BI_locationDescription.TabIndex = 56;
            // 
            // _BI_propertyDescription
            // 
            this._BI_propertyDescription.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._BI_propertyDescription.Location = new System.Drawing.Point(8, 392);
            this._BI_propertyDescription.Multiline = true;
            this._BI_propertyDescription.Name = "_BI_propertyDescription";
            this._BI_propertyDescription.Size = new System.Drawing.Size(208, 120);
            this._BI_propertyDescription.TabIndex = 55;
            // 
            // label51
            // 
            this.label51.Location = new System.Drawing.Point(680, 368);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(120, 24);
            this.label51.TabIndex = 92;
            this.label51.Text = "Landscaping:";
            // 
            // label50
            // 
            this.label50.Location = new System.Drawing.Point(456, 368);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(120, 24);
            this.label50.TabIndex = 91;
            this.label50.Text = "Amenities:";
            // 
            // label49
            // 
            this.label49.Location = new System.Drawing.Point(232, 368);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(120, 24);
            this.label49.TabIndex = 90;
            this.label49.Text = "Location Description:";
            // 
            // label48
            // 
            this.label48.Location = new System.Drawing.Point(8, 368);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(120, 24);
            this.label48.TabIndex = 89;
            this.label48.Text = "Property Description:";
            // 
            // _BI_improvementsNeeded
            // 
            this._BI_improvementsNeeded.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._BI_improvementsNeeded.Location = new System.Drawing.Point(680, 296);
            this._BI_improvementsNeeded.Multiline = true;
            this._BI_improvementsNeeded.Name = "_BI_improvementsNeeded";
            this._BI_improvementsNeeded.Size = new System.Drawing.Size(208, 56);
            this._BI_improvementsNeeded.TabIndex = 51;
            // 
            // label47
            // 
            this.label47.Location = new System.Drawing.Point(680, 280);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(216, 32);
            this.label47.TabIndex = 87;
            this.label47.Text = "Improvements Needed (deferred maint.):";
            // 
            // _BI_recentImprovements
            // 
            this._BI_recentImprovements.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._BI_recentImprovements.Location = new System.Drawing.Point(680, 224);
            this._BI_recentImprovements.Multiline = true;
            this._BI_recentImprovements.Name = "_BI_recentImprovements";
            this._BI_recentImprovements.Size = new System.Drawing.Size(208, 45);
            this._BI_recentImprovements.TabIndex = 50;
            // 
            // label46
            // 
            this.label46.Location = new System.Drawing.Point(680, 208);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(120, 23);
            this.label46.TabIndex = 85;
            this.label46.Text = "Recent Improvements:";
            // 
            // _BI_landLeaseComments
            // 
            this._BI_landLeaseComments.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._BI_landLeaseComments.Location = new System.Drawing.Point(528, 320);
            this._BI_landLeaseComments.Name = "_BI_landLeaseComments";
            this._BI_landLeaseComments.Size = new System.Drawing.Size(144, 20);
            this._BI_landLeaseComments.TabIndex = 46;
            // 
            // _BI_rent
            // 
            this._BI_rent.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._BI_rent.Location = new System.Drawing.Point(528, 296);
            this._BI_rent.Name = "_BI_rent";
            this._BI_rent.Size = new System.Drawing.Size(72, 20);
            this._BI_rent.TabIndex = 45;
            // 
            // label45
            // 
            this.label45.Location = new System.Drawing.Point(456, 328);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(64, 23);
            this.label45.TabIndex = 82;
            this.label45.Text = "Comments:";
            this.label45.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label44
            // 
            this.label44.Location = new System.Drawing.Point(456, 304);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(64, 23);
            this.label44.TabIndex = 81;
            this.label44.Text = "Rent $:";
            this.label44.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // _BI_endDate
            // 
            this._BI_endDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._BI_endDate.Location = new System.Drawing.Point(600, 272);
            this._BI_endDate.Name = "_BI_endDate";
            this._BI_endDate.Size = new System.Drawing.Size(72, 20);
            this._BI_endDate.TabIndex = 44;
            // 
            // _BI_startDate
            // 
            this._BI_startDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._BI_startDate.Location = new System.Drawing.Point(528, 272);
            this._BI_startDate.Name = "_BI_startDate";
            this._BI_startDate.Size = new System.Drawing.Size(72, 20);
            this._BI_startDate.TabIndex = 43;
            // 
            // label43
            // 
            this.label43.Location = new System.Drawing.Point(424, 280);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(96, 23);
            this.label43.TabIndex = 78;
            this.label43.Text = "Start / End Dates:";
            this.label43.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // _BI_lessor
            // 
            this._BI_lessor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._BI_lessor.Location = new System.Drawing.Point(528, 248);
            this._BI_lessor.Name = "_BI_lessor";
            this._BI_lessor.Size = new System.Drawing.Size(144, 20);
            this._BI_lessor.TabIndex = 42;
            // 
            // label42
            // 
            this.label42.Location = new System.Drawing.Point(456, 256);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(64, 23);
            this.label42.TabIndex = 76;
            this.label42.Text = "Lessor:";
            this.label42.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // _BI_option
            // 
            this._BI_option.Items.AddRange(new object[] {
            "Yes",
            "No"});
            this._BI_option.Location = new System.Drawing.Point(552, 224);
            this._BI_option.Name = "_BI_option";
            this._BI_option.Size = new System.Drawing.Size(72, 21);
            this._BI_option.TabIndex = 41;
            // 
            // _BI_landLease
            // 
            this._BI_landLease.Items.AddRange(new object[] {
            "Yes",
            "No"});
            this._BI_landLease.Location = new System.Drawing.Point(456, 224);
            this._BI_landLease.Name = "_BI_landLease";
            this._BI_landLease.Size = new System.Drawing.Size(72, 21);
            this._BI_landLease.TabIndex = 40;
            // 
            // label41
            // 
            this.label41.Location = new System.Drawing.Point(552, 208);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(88, 23);
            this.label41.TabIndex = 73;
            this.label41.Text = "Option:";
            // 
            // label40
            // 
            this.label40.Location = new System.Drawing.Point(456, 208);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(88, 23);
            this.label40.TabIndex = 72;
            this.label40.Text = "Land Lease:";
            // 
            // label39
            // 
            this.label39.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.Location = new System.Drawing.Point(272, 272);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(88, 23);
            this.label39.TabIndex = 71;
            this.label39.Text = "Comments:";
            // 
            // _BI_comments
            // 
            this._BI_comments.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._BI_comments.Location = new System.Drawing.Point(272, 296);
            this._BI_comments.Multiline = true;
            this._BI_comments.Name = "_BI_comments";
            this._BI_comments.Size = new System.Drawing.Size(144, 40);
            this._BI_comments.TabIndex = 39;
            // 
            // _BI_parkingRatio
            // 
            this._BI_parkingRatio.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._BI_parkingRatio.Location = new System.Drawing.Point(88, 205);
            this._BI_parkingRatio.Name = "_BI_parkingRatio";
            this._BI_parkingRatio.ReadOnly = true;
            this._BI_parkingRatio.Size = new System.Drawing.Size(40, 20);
            this._BI_parkingRatio.TabIndex = 30;
            this._BI_parkingRatio.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label38
            // 
            this.label38.Location = new System.Drawing.Point(168, 317);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(16, 23);
            this.label38.TabIndex = 68;
            this.label38.Text = "$";
            this.label38.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label37
            // 
            this.label37.Location = new System.Drawing.Point(168, 293);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(16, 23);
            this.label37.TabIndex = 67;
            this.label37.Text = "$";
            this.label37.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label36
            // 
            this.label36.Location = new System.Drawing.Point(168, 269);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(16, 23);
            this.label36.TabIndex = 66;
            this.label36.Text = "$";
            this.label36.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label35
            // 
            this.label35.Location = new System.Drawing.Point(168, 245);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(16, 23);
            this.label35.TabIndex = 65;
            this.label35.Text = "$";
            this.label35.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // _BI_totalFee
            // 
            this._BI_totalFee.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._BI_totalFee.Location = new System.Drawing.Point(184, 320);
            this._BI_totalFee.Name = "_BI_totalFee";
            this._BI_totalFee.Size = new System.Drawing.Size(72, 20);
            this._BI_totalFee.TabIndex = 38;
            // 
            // _BI_garageFee
            // 
            this._BI_garageFee.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._BI_garageFee.Location = new System.Drawing.Point(184, 296);
            this._BI_garageFee.Name = "_BI_garageFee";
            this._BI_garageFee.Size = new System.Drawing.Size(72, 20);
            this._BI_garageFee.TabIndex = 36;
            // 
            // _BI_coveredFee
            // 
            this._BI_coveredFee.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._BI_coveredFee.Location = new System.Drawing.Point(184, 272);
            this._BI_coveredFee.Name = "_BI_coveredFee";
            this._BI_coveredFee.Size = new System.Drawing.Size(72, 20);
            this._BI_coveredFee.TabIndex = 34;
            // 
            // _BI_lotFee
            // 
            this._BI_lotFee.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._BI_lotFee.Location = new System.Drawing.Point(184, 248);
            this._BI_lotFee.Name = "_BI_lotFee";
            this._BI_lotFee.Size = new System.Drawing.Size(72, 20);
            this._BI_lotFee.TabIndex = 32;
            // 
            // _BI_totalSpaces
            // 
            this._BI_totalSpaces.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._BI_totalSpaces.Location = new System.Drawing.Point(88, 320);
            this._BI_totalSpaces.Name = "_BI_totalSpaces";
            this._BI_totalSpaces.Size = new System.Drawing.Size(72, 20);
            this._BI_totalSpaces.TabIndex = 37;
            // 
            // _BI_garageSpaces
            // 
            this._BI_garageSpaces.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._BI_garageSpaces.Location = new System.Drawing.Point(88, 296);
            this._BI_garageSpaces.Name = "_BI_garageSpaces";
            this._BI_garageSpaces.Size = new System.Drawing.Size(72, 20);
            this._BI_garageSpaces.TabIndex = 35;
            // 
            // _BI_coveredSpaces
            // 
            this._BI_coveredSpaces.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._BI_coveredSpaces.Location = new System.Drawing.Point(88, 272);
            this._BI_coveredSpaces.Name = "_BI_coveredSpaces";
            this._BI_coveredSpaces.Size = new System.Drawing.Size(72, 20);
            this._BI_coveredSpaces.TabIndex = 33;
            // 
            // _BI_lotSpaces
            // 
            this._BI_lotSpaces.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._BI_lotSpaces.Location = new System.Drawing.Point(88, 248);
            this._BI_lotSpaces.Name = "_BI_lotSpaces";
            this._BI_lotSpaces.Size = new System.Drawing.Size(72, 20);
            this._BI_lotSpaces.TabIndex = 31;
            // 
            // label34
            // 
            this.label34.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.Location = new System.Drawing.Point(16, 323);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(64, 23);
            this.label34.TabIndex = 56;
            this.label34.Text = "Total:";
            this.label34.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label33
            // 
            this.label33.Location = new System.Drawing.Point(16, 299);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(64, 23);
            this.label33.TabIndex = 55;
            this.label33.Text = "Garage:";
            this.label33.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label32
            // 
            this.label32.Location = new System.Drawing.Point(16, 275);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(64, 23);
            this.label32.TabIndex = 54;
            this.label32.Text = "Covered:";
            this.label32.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label31
            // 
            this.label31.Location = new System.Drawing.Point(16, 251);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(64, 23);
            this.label31.TabIndex = 53;
            this.label31.Text = "Lot:";
            this.label31.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label30
            // 
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(184, 232);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(88, 23);
            this.label30.TabIndex = 52;
            this.label30.Text = "Fee (Annual):";
            // 
            // label29
            // 
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(88, 232);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(88, 23);
            this.label29.TabIndex = 51;
            this.label29.Text = "#Spaces:";
            // 
            // label28
            // 
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(16, 232);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(64, 23);
            this.label28.TabIndex = 50;
            this.label28.Text = "Type:";
            this.label28.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label27
            // 
            this.label27.Location = new System.Drawing.Point(8, 208);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(75, 23);
            this.label27.TabIndex = 49;
            this.label27.Text = "Parking Ratio:";
            // 
            // label26
            // 
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(424, 184);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(112, 23);
            this.label26.TabIndex = 48;
            this.label26.Text = "LAND LEASE:";
            // 
            // label25
            // 
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(8, 184);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(112, 23);
            this.label25.TabIndex = 47;
            this.label25.Text = "PARKING:";
            // 
            // _BI_windowsAge
            // 
            this._BI_windowsAge.Items.AddRange(new object[] {
            "1950",
            "1951",
            "1952",
            "1953",
            "1954",
            "1955",
            "1956",
            "1957",
            "1958",
            "1959",
            "1960",
            "1961",
            "1962",
            "1963",
            "1964",
            "1965",
            "1966",
            "1967",
            "1968",
            "1969",
            "1970",
            "1971",
            "1972",
            "1973",
            "1974",
            "1975",
            "1976",
            "1977",
            "1978",
            "1979",
            "1980",
            "1981",
            "1982",
            "1983",
            "1984",
            "1985",
            "1986",
            "1987",
            "1988",
            "1989",
            "1990",
            "1991",
            "1992",
            "1993",
            "1994",
            "1995",
            "1996",
            "1997",
            "1998",
            "1999",
            "2000",
            "2001",
            "2002",
            "2003",
            "2004",
            "2005",
            "2006",
            "2007",
            "2008",
            "2009",
            "2010"});
            this._BI_windowsAge.Location = new System.Drawing.Point(792, 144);
            this._BI_windowsAge.Name = "_BI_windowsAge";
            this._BI_windowsAge.Size = new System.Drawing.Size(72, 21);
            this._BI_windowsAge.TabIndex = 26;
            // 
            // _BI_sidingType
            // 
            this._BI_sidingType.Items.AddRange(new object[] {
            "Aluminum",
            "Brick",
            "Shingles",
            "Steel",
            "Stucco",
            "Vinyl",
            "Wood"});
            this._BI_sidingType.Location = new System.Drawing.Point(560, 144);
            this._BI_sidingType.Name = "_BI_sidingType";
            this._BI_sidingType.Size = new System.Drawing.Size(104, 21);
            this._BI_sidingType.TabIndex = 24;
            // 
            // _BI_electricalAge
            // 
            this._BI_electricalAge.Items.AddRange(new object[] {
            "1950",
            "1951",
            "1952",
            "1953",
            "1954",
            "1955",
            "1956",
            "1957",
            "1958",
            "1959",
            "1960",
            "1961",
            "1962",
            "1963",
            "1964",
            "1965",
            "1966",
            "1967",
            "1968",
            "1969",
            "1970",
            "1971",
            "1972",
            "1973",
            "1974",
            "1975",
            "1976",
            "1977",
            "1978",
            "1979",
            "1980",
            "1981",
            "1982",
            "1983",
            "1984",
            "1985",
            "1986",
            "1987",
            "1988",
            "1989",
            "1990",
            "1991",
            "1992",
            "1993",
            "1994",
            "1995",
            "1996",
            "1997",
            "1998",
            "1999",
            "2000",
            "2001",
            "2002",
            "2003",
            "2004",
            "2005",
            "2006",
            "2007",
            "2008",
            "2009",
            "2010"});
            this._BI_electricalAge.Location = new System.Drawing.Point(440, 144);
            this._BI_electricalAge.Name = "_BI_electricalAge";
            this._BI_electricalAge.Size = new System.Drawing.Size(72, 21);
            this._BI_electricalAge.TabIndex = 23;
            // 
            // _BI_plumbingAge
            // 
            this._BI_plumbingAge.Items.AddRange(new object[] {
            "1950",
            "1951",
            "1952",
            "1953",
            "1954",
            "1955",
            "1956",
            "1957",
            "1958",
            "1959",
            "1960",
            "1961",
            "1962",
            "1963",
            "1964",
            "1965",
            "1966",
            "1967",
            "1968",
            "1969",
            "1970",
            "1971",
            "1972",
            "1973",
            "1974",
            "1975",
            "1976",
            "1977",
            "1978",
            "1979",
            "1980",
            "1981",
            "1982",
            "1983",
            "1984",
            "1985",
            "1986",
            "1987",
            "1988",
            "1989",
            "1990",
            "1991",
            "1992",
            "1993",
            "1994",
            "1995",
            "1996",
            "1997",
            "1998",
            "1999",
            "2000",
            "2001",
            "2002",
            "2003",
            "2004",
            "2005",
            "2006",
            "2007",
            "2008",
            "2009",
            "2010"});
            this._BI_plumbingAge.Location = new System.Drawing.Point(336, 144);
            this._BI_plumbingAge.Name = "_BI_plumbingAge";
            this._BI_plumbingAge.Size = new System.Drawing.Size(72, 21);
            this._BI_plumbingAge.TabIndex = 22;
            // 
            // _BI_plumbingType
            // 
            this._BI_plumbingType.Items.AddRange(new object[] {
            "Cast Iron",
            "Copper",
            "Galvanized Steel",
            "PB (grey plastic)",
            "CPVC (cream plastic)",
            "PEX"});
            this._BI_plumbingType.Location = new System.Drawing.Point(224, 144);
            this._BI_plumbingType.Name = "_BI_plumbingType";
            this._BI_plumbingType.Size = new System.Drawing.Size(104, 21);
            this._BI_plumbingType.TabIndex = 21;
            // 
            // _BI_roofAge
            // 
            this._BI_roofAge.Items.AddRange(new object[] {
            "1950",
            "1951",
            "1952",
            "1953",
            "1954",
            "1955",
            "1956",
            "1957",
            "1958",
            "1959",
            "1960",
            "1961",
            "1962",
            "1963",
            "1964",
            "1965",
            "1966",
            "1967",
            "1968",
            "1969",
            "1970",
            "1971",
            "1972",
            "1973",
            "1974",
            "1975",
            "1976",
            "1977",
            "1978",
            "1979",
            "1980",
            "1981",
            "1982",
            "1983",
            "1984",
            "1985",
            "1986",
            "1987",
            "1988",
            "1989",
            "1990",
            "1991",
            "1992",
            "1993",
            "1994",
            "1995",
            "1996",
            "1997",
            "1998",
            "1999",
            "2000",
            "2001",
            "2002",
            "2003",
            "2004",
            "2005",
            "2006",
            "2007",
            "2008",
            "2009",
            "2010"});
            this._BI_roofAge.Location = new System.Drawing.Point(16, 144);
            this._BI_roofAge.Name = "_BI_roofAge";
            this._BI_roofAge.Size = new System.Drawing.Size(72, 21);
            this._BI_roofAge.TabIndex = 19;
            // 
            // label24
            // 
            this.label24.Location = new System.Drawing.Point(792, 128);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(96, 23);
            this.label24.TabIndex = 37;
            this.label24.Text = "Windows Age:";
            // 
            // label23
            // 
            this.label23.Location = new System.Drawing.Point(672, 128);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(104, 23);
            this.label23.TabIndex = 36;
            this.label23.Text = "Window Type:";
            // 
            // label22
            // 
            this.label22.Location = new System.Drawing.Point(560, 128);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(104, 23);
            this.label22.TabIndex = 35;
            this.label22.Text = "Siding Type:";
            // 
            // label21
            // 
            this.label21.Location = new System.Drawing.Point(440, 128);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(104, 23);
            this.label21.TabIndex = 34;
            this.label21.Text = "Electrical Age:";
            // 
            // label20
            // 
            this.label20.Location = new System.Drawing.Point(336, 128);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(96, 23);
            this.label20.TabIndex = 33;
            this.label20.Text = "Plumbing Age:";
            // 
            // label19
            // 
            this.label19.Location = new System.Drawing.Point(224, 128);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(104, 23);
            this.label19.TabIndex = 32;
            this.label19.Text = "Plumbing Type:";
            // 
            // label18
            // 
            this.label18.Location = new System.Drawing.Point(96, 128);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(72, 23);
            this.label18.TabIndex = 31;
            this.label18.Text = "Roof Type:";
            // 
            // label17
            // 
            this.label17.Location = new System.Drawing.Point(16, 128);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(72, 23);
            this.label17.TabIndex = 30;
            this.label17.Text = "Roof Age:";
            // 
            // _BI_lotSqft
            // 
            this._BI_lotSqft.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._BI_lotSqft.Location = new System.Drawing.Point(776, 96);
            this._BI_lotSqft.Name = "_BI_lotSqft";
            this._BI_lotSqft.Size = new System.Drawing.Size(64, 20);
            this._BI_lotSqft.TabIndex = 18;
            // 
            // _BI_lotAcres
            // 
            this._BI_lotAcres.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._BI_lotAcres.Location = new System.Drawing.Point(696, 96);
            this._BI_lotAcres.Name = "_BI_lotAcres";
            this._BI_lotAcres.Size = new System.Drawing.Size(72, 20);
            this._BI_lotAcres.TabIndex = 17;
            // 
            // _BI_netRentableSqFt
            // 
            this._BI_netRentableSqFt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._BI_netRentableSqFt.Location = new System.Drawing.Point(560, 96);
            this._BI_netRentableSqFt.Name = "_BI_netRentableSqFt";
            this._BI_netRentableSqFt.Size = new System.Drawing.Size(88, 20);
            this._BI_netRentableSqFt.TabIndex = 16;
            // 
            // _BI_totalSqFt
            // 
            this._BI_totalSqFt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._BI_totalSqFt.Location = new System.Drawing.Point(440, 96);
            this._BI_totalSqFt.Name = "_BI_totalSqFt";
            this._BI_totalSqFt.Size = new System.Drawing.Size(88, 20);
            this._BI_totalSqFt.TabIndex = 15;
            // 
            // _BI_structures
            // 
            this._BI_structures.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10"});
            this._BI_structures.Location = new System.Drawing.Point(336, 96);
            this._BI_structures.Name = "_BI_structures";
            this._BI_structures.Size = new System.Drawing.Size(72, 21);
            this._BI_structures.TabIndex = 14;
            // 
            // _BI_floors
            // 
            this._BI_floors.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10"});
            this._BI_floors.Location = new System.Drawing.Point(256, 96);
            this._BI_floors.Name = "_BI_floors";
            this._BI_floors.Size = new System.Drawing.Size(72, 21);
            this._BI_floors.TabIndex = 13;
            // 
            // _BI_units
            // 
            this._BI_units.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20",
            "21",
            "22",
            "23",
            "24",
            "25",
            "26",
            "27",
            "28",
            "29",
            "30",
            "31",
            "32",
            "33",
            "34",
            "35",
            "36",
            "37",
            "38",
            "39",
            "40",
            "41",
            "42",
            "43",
            "44",
            "45",
            "46",
            "47",
            "48",
            "49",
            "50",
            "51",
            "52",
            "53",
            "54",
            "55",
            "56",
            "57",
            "58",
            "59",
            "60",
            "61",
            "62",
            "63",
            "64",
            "65",
            "66",
            "67",
            "68",
            "69",
            "70",
            "71",
            "72",
            "73",
            "74",
            "75",
            "76",
            "77",
            "78",
            "79",
            "80",
            "81",
            "82",
            "83",
            "84",
            "85",
            "86",
            "87",
            "88",
            "89",
            "90",
            "91",
            "92",
            "93",
            "94",
            "95",
            "96",
            "97",
            "98",
            "99",
            "100",
            "101",
            "102",
            "103",
            "104",
            "105",
            "106",
            "107",
            "108",
            "109",
            "110",
            "111",
            "112",
            "113",
            "114",
            "115",
            "116",
            "117",
            "118",
            "119",
            "120",
            "121",
            "122",
            "123",
            "124",
            "125",
            "126",
            "127",
            "128",
            "129",
            "130",
            "131",
            "132",
            "133",
            "134",
            "135",
            "136",
            "137",
            "138",
            "139",
            "140",
            "141",
            "142",
            "143",
            "144",
            "145",
            "146",
            "147",
            "148",
            "149",
            "150",
            "151",
            "152",
            "153",
            "154",
            "155",
            "156",
            "157",
            "158",
            "159",
            "160",
            "161",
            "162",
            "163",
            "164",
            "165",
            "166",
            "167",
            "168",
            "169",
            "170",
            "171",
            "172",
            "173",
            "174",
            "175",
            "176",
            "177",
            "178",
            "179",
            "180",
            "181",
            "182",
            "183",
            "184",
            "185",
            "186",
            "187",
            "188",
            "189",
            "190",
            "191",
            "192",
            "193",
            "194",
            "195",
            "196",
            "197",
            "198",
            "199",
            "200",
            "201",
            "202",
            "203",
            "204",
            "205",
            "206",
            "207",
            "208",
            "209",
            "210",
            "211",
            "212",
            "213",
            "214",
            "215",
            "216",
            "217",
            "218",
            "219",
            "220",
            "221",
            "222",
            "223",
            "224",
            "225",
            "226",
            "227",
            "228",
            "229",
            "230",
            "231",
            "232",
            "233",
            "234",
            "235",
            "236",
            "237",
            "238",
            "239",
            "240",
            "241",
            "242",
            "243",
            "244",
            "245",
            "246",
            "247",
            "248",
            "249",
            "250",
            "251",
            "252",
            "253",
            "254",
            "255",
            "256",
            "257",
            "258",
            "259",
            "260",
            "261",
            "262",
            "263",
            "264",
            "265",
            "266",
            "267",
            "268",
            "269",
            "270",
            "271",
            "272",
            "273",
            "274",
            "275",
            "276",
            "277",
            "278",
            "279",
            "280",
            "281",
            "282",
            "283",
            "284",
            "285",
            "286",
            "287",
            "288",
            "289",
            "290",
            "291",
            "292",
            "293",
            "294",
            "295",
            "296",
            "297",
            "298",
            "299",
            "300",
            "301",
            "302",
            "303",
            "304",
            "305",
            "306",
            "307",
            "308",
            "309",
            "310",
            "311",
            "312",
            "313",
            "314",
            "315",
            "316",
            "317",
            "318",
            "319",
            "320",
            "321",
            "322",
            "323",
            "324",
            "325",
            "326",
            "327",
            "328",
            "329",
            "330",
            "331",
            "332",
            "333",
            "334",
            "335",
            "336",
            "337",
            "338",
            "339",
            "340",
            "341",
            "342",
            "343",
            "344",
            "345",
            "346",
            "347",
            "348",
            "349",
            "350",
            "351",
            "352",
            "353",
            "354",
            "355",
            "356",
            "357",
            "358",
            "359",
            "360",
            "361",
            "362",
            "363",
            "364",
            "365",
            "366",
            "367",
            "368",
            "369",
            "370",
            "371",
            "372",
            "373",
            "374",
            "375",
            "376",
            "377",
            "378",
            "379",
            "380",
            "381",
            "382",
            "383",
            "384",
            "385",
            "386",
            "387",
            "388",
            "389",
            "390",
            "391",
            "392",
            "393",
            "394",
            "395",
            "396",
            "397",
            "398",
            "399",
            "400",
            "401",
            "402",
            "403",
            "404",
            "405",
            "406",
            "407",
            "408",
            "409",
            "410",
            "411",
            "412",
            "413",
            "414",
            "415",
            "416",
            "417",
            "418",
            "419",
            "420",
            "421",
            "422",
            "423",
            "424",
            "425",
            "426",
            "427",
            "428",
            "429",
            "430",
            "431",
            "432",
            "433",
            "434",
            "435",
            "436",
            "437",
            "438",
            "439",
            "440",
            "441",
            "442",
            "443",
            "444",
            "445",
            "446",
            "447",
            "448",
            "449",
            "450",
            "451",
            "452",
            "453",
            "454",
            "455",
            "456",
            "457",
            "458",
            "459",
            "460",
            "461",
            "462",
            "463",
            "464",
            "465",
            "466",
            "467",
            "468",
            "469",
            "470",
            "471",
            "472",
            "473",
            "474",
            "475",
            "476",
            "477",
            "478",
            "479",
            "480",
            "481",
            "482",
            "483",
            "484",
            "485",
            "486",
            "487",
            "488",
            "489",
            "490",
            "491",
            "492",
            "493",
            "494",
            "495",
            "496",
            "497",
            "498",
            "499",
            "500"});
            this._BI_units.Location = new System.Drawing.Point(176, 96);
            this._BI_units.Name = "_BI_units";
            this._BI_units.Size = new System.Drawing.Size(72, 21);
            this._BI_units.TabIndex = 12;
            // 
            // _BI_rehabYear
            // 
            this._BI_rehabYear.Items.AddRange(new object[] {
            "1970",
            "1971",
            "1972",
            "1973",
            "1974",
            "1975",
            "1976",
            "1977",
            "1978",
            "1979",
            "1980",
            "1981",
            "1982",
            "1983",
            "1984",
            "1985",
            "1986",
            "1987",
            "1988",
            "1989",
            "1990",
            "1991",
            "1992",
            "1993",
            "1994",
            "1995",
            "1996",
            "1997",
            "1998",
            "1999",
            "2000",
            "2001",
            "2002",
            "2003",
            "2004",
            "2005",
            "2006",
            "2007",
            "2008",
            "2009",
            "2010"});
            this._BI_rehabYear.Location = new System.Drawing.Point(96, 96);
            this._BI_rehabYear.Name = "_BI_rehabYear";
            this._BI_rehabYear.Size = new System.Drawing.Size(72, 21);
            this._BI_rehabYear.TabIndex = 11;
            // 
            // _BI_yearBuilt
            // 
            this._BI_yearBuilt.Items.AddRange(new object[] {
            "1950",
            "1951",
            "1952",
            "1953",
            "1954",
            "1955",
            "1956",
            "1957",
            "1958",
            "1959",
            "1960",
            "1961",
            "1962",
            "1963",
            "1964",
            "1965",
            "1966",
            "1967",
            "1968",
            "1969",
            "1970",
            "1971",
            "1972",
            "1973",
            "1974",
            "1975",
            "1976",
            "1977",
            "1978",
            "1979",
            "1980",
            "1981",
            "1982",
            "1983",
            "1984",
            "1985",
            "1986",
            "1987",
            "1988",
            "1989",
            "1990",
            "1991",
            "1992",
            "1993",
            "1994",
            "1995",
            "1996",
            "1997",
            "1998",
            "1999",
            "2000",
            "2001",
            "2002",
            "2003",
            "2004",
            "2005",
            "2006",
            "2007",
            "2008",
            "2009",
            "2010"});
            this._BI_yearBuilt.Location = new System.Drawing.Point(16, 96);
            this._BI_yearBuilt.Name = "_BI_yearBuilt";
            this._BI_yearBuilt.Size = new System.Drawing.Size(72, 21);
            this._BI_yearBuilt.TabIndex = 10;
            // 
            // label16
            // 
            this.label16.Location = new System.Drawing.Point(776, 80);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(72, 23);
            this.label16.TabIndex = 20;
            this.label16.Text = "Lot SqFt:";
            // 
            // label15
            // 
            this.label15.Location = new System.Drawing.Point(696, 80);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(72, 23);
            this.label15.TabIndex = 19;
            this.label15.Text = "Lot Acres:";
            // 
            // label14
            // 
            this.label14.Location = new System.Drawing.Point(560, 80);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(112, 23);
            this.label14.TabIndex = 18;
            this.label14.Text = "Net Rentable SqFt:";
            // 
            // label13
            // 
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(440, 80);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(88, 23);
            this.label13.TabIndex = 17;
            this.label13.Text = "Total SqFt:";
            // 
            // label12
            // 
            this.label12.Location = new System.Drawing.Point(336, 80);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(88, 23);
            this.label12.TabIndex = 16;
            this.label12.Text = "# Structures:";
            // 
            // label11
            // 
            this.label11.Location = new System.Drawing.Point(264, 80);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(72, 23);
            this.label11.TabIndex = 15;
            this.label11.Text = "# Floors:";
            // 
            // label10
            // 
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(184, 80);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(72, 23);
            this.label10.TabIndex = 14;
            this.label10.Text = "# Units:";
            // 
            // label9
            // 
            this.label9.Location = new System.Drawing.Point(96, 80);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(80, 23);
            this.label9.TabIndex = 13;
            this.label9.Text = "Rehab Year:";
            // 
            // label8
            // 
            this.label8.Location = new System.Drawing.Point(16, 80);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(72, 23);
            this.label8.TabIndex = 12;
            this.label8.Text = "Year Built:";
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(8, 64);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(112, 23);
            this.label7.TabIndex = 11;
            this.label7.Text = "CONSTRUCTION:";
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(645, 24);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(16, 23);
            this.label6.TabIndex = 10;
            this.label6.Text = "$";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // _BI_propertyValue
            // 
            this._BI_propertyValue.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._BI_propertyValue.Location = new System.Drawing.Point(661, 27);
            this._BI_propertyValue.Name = "_BI_propertyValue";
            this._BI_propertyValue.Size = new System.Drawing.Size(144, 20);
            this._BI_propertyValue.TabIndex = 5;
            // 
            // _BI_apn
            // 
            this._BI_apn.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._BI_apn.Location = new System.Drawing.Point(496, 27);
            this._BI_apn.Name = "_BI_apn";
            this._BI_apn.Size = new System.Drawing.Size(144, 20);
            this._BI_apn.TabIndex = 4;
            // 
            // _BI_class
            // 
            this._BI_class.Items.AddRange(new object[] {
            "A",
            "B",
            "C",
            "D"});
            this._BI_class.Location = new System.Drawing.Point(392, 27);
            this._BI_class.Name = "_BI_class";
            this._BI_class.Size = new System.Drawing.Size(96, 21);
            this._BI_class.TabIndex = 3;
            // 
            // _BI_propertyType
            // 
            this._BI_propertyType.Items.AddRange(new object[] {
            "Multifamily",
            "Retail",
            "Office",
            "Selfstorage"});
            this._BI_propertyType.Location = new System.Drawing.Point(8, 26);
            this._BI_propertyType.Name = "_BI_propertyType";
            this._BI_propertyType.Size = new System.Drawing.Size(176, 21);
            this._BI_propertyType.TabIndex = 1;
            // 
            // _BI_propertyName
            // 
            this._BI_propertyName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._BI_propertyName.Location = new System.Drawing.Point(192, 27);
            this._BI_propertyName.Name = "_BI_propertyName";
            this._BI_propertyName.Size = new System.Drawing.Size(192, 20);
            this._BI_propertyName.TabIndex = 2;
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(661, 8);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(144, 23);
            this.label5.TabIndex = 4;
            this.label5.Text = "Property Value (list price):";
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(496, 8);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(64, 23);
            this.label4.TabIndex = 3;
            this.label4.Text = "APN:";
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(392, 8);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(64, 23);
            this.label3.TabIndex = 2;
            this.label3.Text = "Class:";
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(8, 8);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(100, 23);
            this.label2.TabIndex = 1;
            this.label2.Text = "Property Type:";
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(192, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 23);
            this.label1.TabIndex = 0;
            this.label1.Text = "Property Name:";
            // 
            // _generalIncome
            // 
            this._generalIncome.Controls.Add(this.income_Dummy1);
            this._generalIncome.Location = new System.Drawing.Point(4, 22);
            this._generalIncome.Name = "_generalIncome";
            this._generalIncome.Size = new System.Drawing.Size(897, 529);
            this._generalIncome.TabIndex = 6;
            this._generalIncome.Text = "Income";
            this._generalIncome.UseVisualStyleBackColor = true;
            // 
            // _expensesTabPage
            // 
            this._expensesTabPage.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this._expensesTabPage.Controls.Add(this._printExpensesSummary);
            this._expensesTabPage.Controls.Add(this._PrintExpensesDetails);
            this._expensesTabPage.Controls.Add(this._e_floater);
            this._expensesTabPage.Controls.Add(this.label140);
            this._expensesTabPage.Controls.Add(this.label139);
            this._expensesTabPage.Controls.Add(this.label138);
            this._expensesTabPage.Controls.Add(this.label137);
            this._expensesTabPage.Controls.Add(this.label136);
            this._expensesTabPage.Controls.Add(this.label135);
            this._expensesTabPage.Controls.Add(this._E_TotalPercOfTotal_6);
            this._expensesTabPage.Controls.Add(this._E_TotalPercOfTotal_1);
            this._expensesTabPage.Controls.Add(this._E_TotalPercOfTotal_5);
            this._expensesTabPage.Controls.Add(this._E_TotalPercOfTotal_4);
            this._expensesTabPage.Controls.Add(this._E_TotalPercOfTotal_3);
            this._expensesTabPage.Controls.Add(this._E_TotalPercOfTotal_2);
            this._expensesTabPage.Controls.Add(this._E_TotalUsdPerUnit_6);
            this._expensesTabPage.Controls.Add(this._E_TotalUsdPerUnit_1);
            this._expensesTabPage.Controls.Add(this._E_TotalUsdPerUnit_5);
            this._expensesTabPage.Controls.Add(this._E_TotalUsdPerUnit_4);
            this._expensesTabPage.Controls.Add(this._E_TotalUsdPerUnit_3);
            this._expensesTabPage.Controls.Add(this._E_TotalUsdPerUnit_2);
            this._expensesTabPage.Controls.Add(this._E_TotalUsdPerSqft_6);
            this._expensesTabPage.Controls.Add(this._E_TotalUsdPerSqft_1);
            this._expensesTabPage.Controls.Add(this._E_TotalUsdPerSqft_5);
            this._expensesTabPage.Controls.Add(this._E_TotalUsdPerSqft_4);
            this._expensesTabPage.Controls.Add(this._E_TotalUsdPerSqft_3);
            this._expensesTabPage.Controls.Add(this._E_TotalUsdPerSqft_2);
            this._expensesTabPage.Controls.Add(this._E_TotalUsdPerYear_6);
            this._expensesTabPage.Controls.Add(this._E_TotalUsdPerYear_1);
            this._expensesTabPage.Controls.Add(this._E_TotalUsdPerYear_5);
            this._expensesTabPage.Controls.Add(this._E_TotalUsdPerYear_4);
            this._expensesTabPage.Controls.Add(this._E_TotalUsdPerYear_3);
            this._expensesTabPage.Controls.Add(this._E_TotalUsdPerYear_2);
            this._expensesTabPage.Controls.Add(this._E_TotalUsdPerMonth_6);
            this._expensesTabPage.Controls.Add(this._E_TotalUsdPerMonth_1);
            this._expensesTabPage.Controls.Add(this._E_TotalUsdPerMonth_5);
            this._expensesTabPage.Controls.Add(this._E_TotalUsdPerMonth_4);
            this._expensesTabPage.Controls.Add(this._E_TotalUsdPerMonth_3);
            this._expensesTabPage.Controls.Add(this._E_TotalUsdPerMonth_2);
            this._expensesTabPage.Controls.Add(this._E_TotalDesc_6);
            this._expensesTabPage.Controls.Add(this._E_TotalDesc_1);
            this._expensesTabPage.Controls.Add(this._E_TotalDesc_5);
            this._expensesTabPage.Controls.Add(this._E_TotalDesc_4);
            this._expensesTabPage.Controls.Add(this._E_TotalDesc_3);
            this._expensesTabPage.Controls.Add(this._E_TotalDesc_2);
            this._expensesTabPage.Controls.Add(this.label134);
            this._expensesTabPage.Controls.Add(this._E_TotalType_6);
            this._expensesTabPage.Controls.Add(this._E_TotalType_1);
            this._expensesTabPage.Controls.Add(this._E_TotalType_5);
            this._expensesTabPage.Controls.Add(this._E_TotalType_4);
            this._expensesTabPage.Controls.Add(this._E_TotalType_3);
            this._expensesTabPage.Controls.Add(this._E_TotalType_2);
            this._expensesTabPage.Controls.Add(this._E_expenses);
            this._expensesTabPage.Controls.Add(this.label133);
            this._expensesTabPage.Controls.Add(this.label132);
            this._expensesTabPage.Controls.Add(this.label131);
            this._expensesTabPage.Controls.Add(this.label130);
            this._expensesTabPage.Controls.Add(this.label129);
            this._expensesTabPage.Controls.Add(this.label128);
            this._expensesTabPage.Controls.Add(this.label127);
            this._expensesTabPage.Controls.Add(this.label126);
            this._expensesTabPage.Controls.Add(this.label125);
            this._expensesTabPage.Controls.Add(this.label124);
            this._expensesTabPage.Controls.Add(this.label123);
            this._expensesTabPage.Controls.Add(this.label122);
            this._expensesTabPage.Controls.Add(this.label121);
            this._expensesTabPage.Controls.Add(this._E_CA_Type_Year10);
            this._expensesTabPage.Controls.Add(this._E_CA_Cost_Year10);
            this._expensesTabPage.Controls.Add(this._E_CA_Type_Year9);
            this._expensesTabPage.Controls.Add(this._E_CA_Cost_Year9);
            this._expensesTabPage.Controls.Add(this._E_CA_Type_Year8);
            this._expensesTabPage.Controls.Add(this._E_CA_Cost_Year8);
            this._expensesTabPage.Controls.Add(this._E_CA_Type_Year7);
            this._expensesTabPage.Controls.Add(this._E_CA_Cost_Year7);
            this._expensesTabPage.Controls.Add(this._E_CA_Type_Year6);
            this._expensesTabPage.Controls.Add(this._E_CA_Cost_Year6);
            this._expensesTabPage.Controls.Add(this._E_CA_Type_Year5);
            this._expensesTabPage.Controls.Add(this._E_CA_Cost_Year5);
            this._expensesTabPage.Controls.Add(this._E_CA_Type_Year4);
            this._expensesTabPage.Controls.Add(this._E_CA_Cost_Year4);
            this._expensesTabPage.Controls.Add(this._E_CA_Type_Year3);
            this._expensesTabPage.Controls.Add(this._E_CA_Cost_Year3);
            this._expensesTabPage.Controls.Add(this._E_CA_Type_Year2);
            this._expensesTabPage.Controls.Add(this._E_CA_Cost_Year2);
            this._expensesTabPage.Controls.Add(this._E_CA_Type_Year1);
            this._expensesTabPage.Controls.Add(this._E_CA_Cost_Year1);
            this._expensesTabPage.Controls.Add(this.label120);
            this._expensesTabPage.Controls.Add(this._E_UT_HotWater);
            this._expensesTabPage.Controls.Add(this.label119);
            this._expensesTabPage.Controls.Add(this._E_UT_Heat);
            this._expensesTabPage.Controls.Add(this.label118);
            this._expensesTabPage.Controls.Add(this._E_UT_ElectricityMeter);
            this._expensesTabPage.Controls.Add(this.label117);
            this._expensesTabPage.Controls.Add(this.label116);
            this._expensesTabPage.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this._expensesTabPage.Location = new System.Drawing.Point(4, 22);
            this._expensesTabPage.Name = "_expensesTabPage";
            this._expensesTabPage.Size = new System.Drawing.Size(897, 529);
            this._expensesTabPage.TabIndex = 2;
            this._expensesTabPage.Text = "Expenses";
            // 
            // _printExpensesSummary
            // 
            this._printExpensesSummary.Location = new System.Drawing.Point(616, 446);
            this._printExpensesSummary.Name = "_printExpensesSummary";
            this._printExpensesSummary.Size = new System.Drawing.Size(151, 23);
            this._printExpensesSummary.TabIndex = 97;
            this._printExpensesSummary.Text = "Print Expenses Summary";
            this._printExpensesSummary.UseVisualStyleBackColor = true;
            // 
            // _PrintExpensesDetails
            // 
            this._PrintExpensesDetails.Location = new System.Drawing.Point(616, 408);
            this._PrintExpensesDetails.Name = "_PrintExpensesDetails";
            this._PrintExpensesDetails.Size = new System.Drawing.Size(151, 23);
            this._PrintExpensesDetails.TabIndex = 96;
            this._PrintExpensesDetails.Text = "Print Expenses Details";
            this._PrintExpensesDetails.UseVisualStyleBackColor = true;
            // 
            // _e_floater
            // 
            this._e_floater.Image = ((System.Drawing.Image)(resources.GetObject("_e_floater.Image")));
            this._e_floater.Location = new System.Drawing.Point(872, 8);
            this._e_floater.Name = "_e_floater";
            this._e_floater.Size = new System.Drawing.Size(16, 16);
            this._e_floater.TabIndex = 95;
            this._e_floater.TabStop = false;
            // 
            // label140
            // 
            this.label140.Location = new System.Drawing.Point(528, 384);
            this.label140.Name = "label140";
            this.label140.Size = new System.Drawing.Size(72, 23);
            this.label140.TabIndex = 91;
            this.label140.Text = "% of Total:";
            this.label140.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label139
            // 
            this.label139.Location = new System.Drawing.Point(448, 384);
            this.label139.Name = "label139";
            this.label139.Size = new System.Drawing.Size(88, 23);
            this.label139.TabIndex = 90;
            this.label139.Text = "$/Unit (annual):";
            this.label139.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label138
            // 
            this.label138.Location = new System.Drawing.Point(360, 384);
            this.label138.Name = "label138";
            this.label138.Size = new System.Drawing.Size(88, 23);
            this.label138.TabIndex = 89;
            this.label138.Text = "$/SqFt (annual):";
            this.label138.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label137
            // 
            this.label137.Location = new System.Drawing.Point(296, 384);
            this.label137.Name = "label137";
            this.label137.Size = new System.Drawing.Size(64, 23);
            this.label137.TabIndex = 88;
            this.label137.Text = "$/Year:";
            this.label137.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label136
            // 
            this.label136.Location = new System.Drawing.Point(232, 384);
            this.label136.Name = "label136";
            this.label136.Size = new System.Drawing.Size(64, 23);
            this.label136.TabIndex = 87;
            this.label136.Text = "$/Month:";
            this.label136.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label135
            // 
            this.label135.Location = new System.Drawing.Point(120, 384);
            this.label135.Name = "label135";
            this.label135.Size = new System.Drawing.Size(64, 23);
            this.label135.TabIndex = 86;
            this.label135.Text = "Description:";
            this.label135.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // _E_TotalPercOfTotal_6
            // 
            this._E_TotalPercOfTotal_6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._E_TotalPercOfTotal_6.Location = new System.Drawing.Point(528, 503);
            this._E_TotalPercOfTotal_6.Name = "_E_TotalPercOfTotal_6";
            this._E_TotalPercOfTotal_6.ReadOnly = true;
            this._E_TotalPercOfTotal_6.Size = new System.Drawing.Size(64, 20);
            this._E_TotalPercOfTotal_6.TabIndex = 85;
            // 
            // _E_TotalPercOfTotal_1
            // 
            this._E_TotalPercOfTotal_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._E_TotalPercOfTotal_1.Location = new System.Drawing.Point(528, 408);
            this._E_TotalPercOfTotal_1.Name = "_E_TotalPercOfTotal_1";
            this._E_TotalPercOfTotal_1.ReadOnly = true;
            this._E_TotalPercOfTotal_1.Size = new System.Drawing.Size(64, 20);
            this._E_TotalPercOfTotal_1.TabIndex = 84;
            // 
            // _E_TotalPercOfTotal_5
            // 
            this._E_TotalPercOfTotal_5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._E_TotalPercOfTotal_5.Location = new System.Drawing.Point(528, 484);
            this._E_TotalPercOfTotal_5.Name = "_E_TotalPercOfTotal_5";
            this._E_TotalPercOfTotal_5.ReadOnly = true;
            this._E_TotalPercOfTotal_5.Size = new System.Drawing.Size(64, 20);
            this._E_TotalPercOfTotal_5.TabIndex = 83;
            // 
            // _E_TotalPercOfTotal_4
            // 
            this._E_TotalPercOfTotal_4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._E_TotalPercOfTotal_4.Location = new System.Drawing.Point(528, 465);
            this._E_TotalPercOfTotal_4.Name = "_E_TotalPercOfTotal_4";
            this._E_TotalPercOfTotal_4.ReadOnly = true;
            this._E_TotalPercOfTotal_4.Size = new System.Drawing.Size(64, 20);
            this._E_TotalPercOfTotal_4.TabIndex = 82;
            // 
            // _E_TotalPercOfTotal_3
            // 
            this._E_TotalPercOfTotal_3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._E_TotalPercOfTotal_3.Location = new System.Drawing.Point(528, 446);
            this._E_TotalPercOfTotal_3.Name = "_E_TotalPercOfTotal_3";
            this._E_TotalPercOfTotal_3.ReadOnly = true;
            this._E_TotalPercOfTotal_3.Size = new System.Drawing.Size(64, 20);
            this._E_TotalPercOfTotal_3.TabIndex = 81;
            // 
            // _E_TotalPercOfTotal_2
            // 
            this._E_TotalPercOfTotal_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._E_TotalPercOfTotal_2.Location = new System.Drawing.Point(528, 427);
            this._E_TotalPercOfTotal_2.Name = "_E_TotalPercOfTotal_2";
            this._E_TotalPercOfTotal_2.ReadOnly = true;
            this._E_TotalPercOfTotal_2.Size = new System.Drawing.Size(64, 20);
            this._E_TotalPercOfTotal_2.TabIndex = 80;
            // 
            // _E_TotalUsdPerUnit_6
            // 
            this._E_TotalUsdPerUnit_6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._E_TotalUsdPerUnit_6.Location = new System.Drawing.Point(440, 503);
            this._E_TotalUsdPerUnit_6.Name = "_E_TotalUsdPerUnit_6";
            this._E_TotalUsdPerUnit_6.ReadOnly = true;
            this._E_TotalUsdPerUnit_6.Size = new System.Drawing.Size(88, 20);
            this._E_TotalUsdPerUnit_6.TabIndex = 79;
            // 
            // _E_TotalUsdPerUnit_1
            // 
            this._E_TotalUsdPerUnit_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._E_TotalUsdPerUnit_1.Location = new System.Drawing.Point(440, 408);
            this._E_TotalUsdPerUnit_1.Name = "_E_TotalUsdPerUnit_1";
            this._E_TotalUsdPerUnit_1.ReadOnly = true;
            this._E_TotalUsdPerUnit_1.Size = new System.Drawing.Size(88, 20);
            this._E_TotalUsdPerUnit_1.TabIndex = 78;
            // 
            // _E_TotalUsdPerUnit_5
            // 
            this._E_TotalUsdPerUnit_5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._E_TotalUsdPerUnit_5.Location = new System.Drawing.Point(440, 484);
            this._E_TotalUsdPerUnit_5.Name = "_E_TotalUsdPerUnit_5";
            this._E_TotalUsdPerUnit_5.ReadOnly = true;
            this._E_TotalUsdPerUnit_5.Size = new System.Drawing.Size(88, 20);
            this._E_TotalUsdPerUnit_5.TabIndex = 77;
            // 
            // _E_TotalUsdPerUnit_4
            // 
            this._E_TotalUsdPerUnit_4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._E_TotalUsdPerUnit_4.Location = new System.Drawing.Point(440, 465);
            this._E_TotalUsdPerUnit_4.Name = "_E_TotalUsdPerUnit_4";
            this._E_TotalUsdPerUnit_4.ReadOnly = true;
            this._E_TotalUsdPerUnit_4.Size = new System.Drawing.Size(88, 20);
            this._E_TotalUsdPerUnit_4.TabIndex = 76;
            // 
            // _E_TotalUsdPerUnit_3
            // 
            this._E_TotalUsdPerUnit_3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._E_TotalUsdPerUnit_3.Location = new System.Drawing.Point(440, 446);
            this._E_TotalUsdPerUnit_3.Name = "_E_TotalUsdPerUnit_3";
            this._E_TotalUsdPerUnit_3.ReadOnly = true;
            this._E_TotalUsdPerUnit_3.Size = new System.Drawing.Size(88, 20);
            this._E_TotalUsdPerUnit_3.TabIndex = 75;
            // 
            // _E_TotalUsdPerUnit_2
            // 
            this._E_TotalUsdPerUnit_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._E_TotalUsdPerUnit_2.Location = new System.Drawing.Point(440, 427);
            this._E_TotalUsdPerUnit_2.Name = "_E_TotalUsdPerUnit_2";
            this._E_TotalUsdPerUnit_2.ReadOnly = true;
            this._E_TotalUsdPerUnit_2.Size = new System.Drawing.Size(88, 20);
            this._E_TotalUsdPerUnit_2.TabIndex = 74;
            // 
            // _E_TotalUsdPerSqft_6
            // 
            this._E_TotalUsdPerSqft_6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._E_TotalUsdPerSqft_6.Location = new System.Drawing.Point(360, 503);
            this._E_TotalUsdPerSqft_6.Name = "_E_TotalUsdPerSqft_6";
            this._E_TotalUsdPerSqft_6.ReadOnly = true;
            this._E_TotalUsdPerSqft_6.Size = new System.Drawing.Size(80, 20);
            this._E_TotalUsdPerSqft_6.TabIndex = 73;
            // 
            // _E_TotalUsdPerSqft_1
            // 
            this._E_TotalUsdPerSqft_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._E_TotalUsdPerSqft_1.Location = new System.Drawing.Point(360, 408);
            this._E_TotalUsdPerSqft_1.Name = "_E_TotalUsdPerSqft_1";
            this._E_TotalUsdPerSqft_1.ReadOnly = true;
            this._E_TotalUsdPerSqft_1.Size = new System.Drawing.Size(80, 20);
            this._E_TotalUsdPerSqft_1.TabIndex = 72;
            // 
            // _E_TotalUsdPerSqft_5
            // 
            this._E_TotalUsdPerSqft_5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._E_TotalUsdPerSqft_5.Location = new System.Drawing.Point(360, 484);
            this._E_TotalUsdPerSqft_5.Name = "_E_TotalUsdPerSqft_5";
            this._E_TotalUsdPerSqft_5.ReadOnly = true;
            this._E_TotalUsdPerSqft_5.Size = new System.Drawing.Size(80, 20);
            this._E_TotalUsdPerSqft_5.TabIndex = 71;
            // 
            // _E_TotalUsdPerSqft_4
            // 
            this._E_TotalUsdPerSqft_4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._E_TotalUsdPerSqft_4.Location = new System.Drawing.Point(360, 465);
            this._E_TotalUsdPerSqft_4.Name = "_E_TotalUsdPerSqft_4";
            this._E_TotalUsdPerSqft_4.ReadOnly = true;
            this._E_TotalUsdPerSqft_4.Size = new System.Drawing.Size(80, 20);
            this._E_TotalUsdPerSqft_4.TabIndex = 70;
            // 
            // _E_TotalUsdPerSqft_3
            // 
            this._E_TotalUsdPerSqft_3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._E_TotalUsdPerSqft_3.Location = new System.Drawing.Point(360, 446);
            this._E_TotalUsdPerSqft_3.Name = "_E_TotalUsdPerSqft_3";
            this._E_TotalUsdPerSqft_3.ReadOnly = true;
            this._E_TotalUsdPerSqft_3.Size = new System.Drawing.Size(80, 20);
            this._E_TotalUsdPerSqft_3.TabIndex = 69;
            // 
            // _E_TotalUsdPerSqft_2
            // 
            this._E_TotalUsdPerSqft_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._E_TotalUsdPerSqft_2.Location = new System.Drawing.Point(360, 427);
            this._E_TotalUsdPerSqft_2.Name = "_E_TotalUsdPerSqft_2";
            this._E_TotalUsdPerSqft_2.ReadOnly = true;
            this._E_TotalUsdPerSqft_2.Size = new System.Drawing.Size(80, 20);
            this._E_TotalUsdPerSqft_2.TabIndex = 68;
            // 
            // _E_TotalUsdPerYear_6
            // 
            this._E_TotalUsdPerYear_6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._E_TotalUsdPerYear_6.Location = new System.Drawing.Point(296, 503);
            this._E_TotalUsdPerYear_6.Name = "_E_TotalUsdPerYear_6";
            this._E_TotalUsdPerYear_6.ReadOnly = true;
            this._E_TotalUsdPerYear_6.Size = new System.Drawing.Size(64, 20);
            this._E_TotalUsdPerYear_6.TabIndex = 67;
            // 
            // _E_TotalUsdPerYear_1
            // 
            this._E_TotalUsdPerYear_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._E_TotalUsdPerYear_1.Location = new System.Drawing.Point(296, 408);
            this._E_TotalUsdPerYear_1.Name = "_E_TotalUsdPerYear_1";
            this._E_TotalUsdPerYear_1.ReadOnly = true;
            this._E_TotalUsdPerYear_1.Size = new System.Drawing.Size(64, 20);
            this._E_TotalUsdPerYear_1.TabIndex = 66;
            // 
            // _E_TotalUsdPerYear_5
            // 
            this._E_TotalUsdPerYear_5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._E_TotalUsdPerYear_5.Location = new System.Drawing.Point(296, 484);
            this._E_TotalUsdPerYear_5.Name = "_E_TotalUsdPerYear_5";
            this._E_TotalUsdPerYear_5.ReadOnly = true;
            this._E_TotalUsdPerYear_5.Size = new System.Drawing.Size(64, 20);
            this._E_TotalUsdPerYear_5.TabIndex = 65;
            // 
            // _E_TotalUsdPerYear_4
            // 
            this._E_TotalUsdPerYear_4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._E_TotalUsdPerYear_4.Location = new System.Drawing.Point(296, 465);
            this._E_TotalUsdPerYear_4.Name = "_E_TotalUsdPerYear_4";
            this._E_TotalUsdPerYear_4.ReadOnly = true;
            this._E_TotalUsdPerYear_4.Size = new System.Drawing.Size(64, 20);
            this._E_TotalUsdPerYear_4.TabIndex = 64;
            // 
            // _E_TotalUsdPerYear_3
            // 
            this._E_TotalUsdPerYear_3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._E_TotalUsdPerYear_3.Location = new System.Drawing.Point(296, 446);
            this._E_TotalUsdPerYear_3.Name = "_E_TotalUsdPerYear_3";
            this._E_TotalUsdPerYear_3.ReadOnly = true;
            this._E_TotalUsdPerYear_3.Size = new System.Drawing.Size(64, 20);
            this._E_TotalUsdPerYear_3.TabIndex = 63;
            // 
            // _E_TotalUsdPerYear_2
            // 
            this._E_TotalUsdPerYear_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._E_TotalUsdPerYear_2.Location = new System.Drawing.Point(296, 427);
            this._E_TotalUsdPerYear_2.Name = "_E_TotalUsdPerYear_2";
            this._E_TotalUsdPerYear_2.ReadOnly = true;
            this._E_TotalUsdPerYear_2.Size = new System.Drawing.Size(64, 20);
            this._E_TotalUsdPerYear_2.TabIndex = 62;
            // 
            // _E_TotalUsdPerMonth_6
            // 
            this._E_TotalUsdPerMonth_6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._E_TotalUsdPerMonth_6.Location = new System.Drawing.Point(232, 503);
            this._E_TotalUsdPerMonth_6.Name = "_E_TotalUsdPerMonth_6";
            this._E_TotalUsdPerMonth_6.ReadOnly = true;
            this._E_TotalUsdPerMonth_6.Size = new System.Drawing.Size(64, 20);
            this._E_TotalUsdPerMonth_6.TabIndex = 61;
            // 
            // _E_TotalUsdPerMonth_1
            // 
            this._E_TotalUsdPerMonth_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._E_TotalUsdPerMonth_1.Location = new System.Drawing.Point(232, 408);
            this._E_TotalUsdPerMonth_1.Name = "_E_TotalUsdPerMonth_1";
            this._E_TotalUsdPerMonth_1.ReadOnly = true;
            this._E_TotalUsdPerMonth_1.Size = new System.Drawing.Size(64, 20);
            this._E_TotalUsdPerMonth_1.TabIndex = 60;
            // 
            // _E_TotalUsdPerMonth_5
            // 
            this._E_TotalUsdPerMonth_5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._E_TotalUsdPerMonth_5.Location = new System.Drawing.Point(232, 484);
            this._E_TotalUsdPerMonth_5.Name = "_E_TotalUsdPerMonth_5";
            this._E_TotalUsdPerMonth_5.ReadOnly = true;
            this._E_TotalUsdPerMonth_5.Size = new System.Drawing.Size(64, 20);
            this._E_TotalUsdPerMonth_5.TabIndex = 59;
            // 
            // _E_TotalUsdPerMonth_4
            // 
            this._E_TotalUsdPerMonth_4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._E_TotalUsdPerMonth_4.Location = new System.Drawing.Point(232, 465);
            this._E_TotalUsdPerMonth_4.Name = "_E_TotalUsdPerMonth_4";
            this._E_TotalUsdPerMonth_4.ReadOnly = true;
            this._E_TotalUsdPerMonth_4.Size = new System.Drawing.Size(64, 20);
            this._E_TotalUsdPerMonth_4.TabIndex = 58;
            // 
            // _E_TotalUsdPerMonth_3
            // 
            this._E_TotalUsdPerMonth_3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._E_TotalUsdPerMonth_3.Location = new System.Drawing.Point(232, 446);
            this._E_TotalUsdPerMonth_3.Name = "_E_TotalUsdPerMonth_3";
            this._E_TotalUsdPerMonth_3.ReadOnly = true;
            this._E_TotalUsdPerMonth_3.Size = new System.Drawing.Size(64, 20);
            this._E_TotalUsdPerMonth_3.TabIndex = 57;
            // 
            // _E_TotalUsdPerMonth_2
            // 
            this._E_TotalUsdPerMonth_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._E_TotalUsdPerMonth_2.Location = new System.Drawing.Point(232, 427);
            this._E_TotalUsdPerMonth_2.Name = "_E_TotalUsdPerMonth_2";
            this._E_TotalUsdPerMonth_2.ReadOnly = true;
            this._E_TotalUsdPerMonth_2.Size = new System.Drawing.Size(64, 20);
            this._E_TotalUsdPerMonth_2.TabIndex = 56;
            // 
            // _E_TotalDesc_6
            // 
            this._E_TotalDesc_6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._E_TotalDesc_6.Location = new System.Drawing.Point(120, 503);
            this._E_TotalDesc_6.Name = "_E_TotalDesc_6";
            this._E_TotalDesc_6.ReadOnly = true;
            this._E_TotalDesc_6.Size = new System.Drawing.Size(112, 20);
            this._E_TotalDesc_6.TabIndex = 55;
            this._E_TotalDesc_6.Text = "Total";
            // 
            // _E_TotalDesc_1
            // 
            this._E_TotalDesc_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._E_TotalDesc_1.Location = new System.Drawing.Point(120, 408);
            this._E_TotalDesc_1.Name = "_E_TotalDesc_1";
            this._E_TotalDesc_1.ReadOnly = true;
            this._E_TotalDesc_1.Size = new System.Drawing.Size(112, 20);
            this._E_TotalDesc_1.TabIndex = 54;
            this._E_TotalDesc_1.Text = "Total";
            // 
            // _E_TotalDesc_5
            // 
            this._E_TotalDesc_5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._E_TotalDesc_5.Location = new System.Drawing.Point(120, 484);
            this._E_TotalDesc_5.Name = "_E_TotalDesc_5";
            this._E_TotalDesc_5.ReadOnly = true;
            this._E_TotalDesc_5.Size = new System.Drawing.Size(112, 20);
            this._E_TotalDesc_5.TabIndex = 53;
            this._E_TotalDesc_5.Text = "Total";
            // 
            // _E_TotalDesc_4
            // 
            this._E_TotalDesc_4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._E_TotalDesc_4.Location = new System.Drawing.Point(120, 465);
            this._E_TotalDesc_4.Name = "_E_TotalDesc_4";
            this._E_TotalDesc_4.ReadOnly = true;
            this._E_TotalDesc_4.Size = new System.Drawing.Size(112, 20);
            this._E_TotalDesc_4.TabIndex = 52;
            this._E_TotalDesc_4.Text = "Total";
            // 
            // _E_TotalDesc_3
            // 
            this._E_TotalDesc_3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._E_TotalDesc_3.Location = new System.Drawing.Point(120, 446);
            this._E_TotalDesc_3.Name = "_E_TotalDesc_3";
            this._E_TotalDesc_3.ReadOnly = true;
            this._E_TotalDesc_3.Size = new System.Drawing.Size(112, 20);
            this._E_TotalDesc_3.TabIndex = 51;
            this._E_TotalDesc_3.Text = "Total";
            // 
            // _E_TotalDesc_2
            // 
            this._E_TotalDesc_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._E_TotalDesc_2.Location = new System.Drawing.Point(120, 427);
            this._E_TotalDesc_2.Name = "_E_TotalDesc_2";
            this._E_TotalDesc_2.ReadOnly = true;
            this._E_TotalDesc_2.Size = new System.Drawing.Size(112, 20);
            this._E_TotalDesc_2.TabIndex = 50;
            this._E_TotalDesc_2.Text = "Total";
            // 
            // label134
            // 
            this.label134.Location = new System.Drawing.Point(8, 384);
            this.label134.Name = "label134";
            this.label134.Size = new System.Drawing.Size(64, 23);
            this.label134.TabIndex = 49;
            this.label134.Text = "Type:";
            this.label134.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // _E_TotalType_6
            // 
            this._E_TotalType_6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._E_TotalType_6.Location = new System.Drawing.Point(8, 503);
            this._E_TotalType_6.Name = "_E_TotalType_6";
            this._E_TotalType_6.ReadOnly = true;
            this._E_TotalType_6.Size = new System.Drawing.Size(112, 20);
            this._E_TotalType_6.TabIndex = 48;
            this._E_TotalType_6.Text = "All Types";
            // 
            // _E_TotalType_1
            // 
            this._E_TotalType_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._E_TotalType_1.Location = new System.Drawing.Point(8, 408);
            this._E_TotalType_1.Name = "_E_TotalType_1";
            this._E_TotalType_1.ReadOnly = true;
            this._E_TotalType_1.Size = new System.Drawing.Size(112, 20);
            this._E_TotalType_1.TabIndex = 47;
            this._E_TotalType_1.Text = "Administrative";
            // 
            // _E_TotalType_5
            // 
            this._E_TotalType_5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._E_TotalType_5.Location = new System.Drawing.Point(8, 484);
            this._E_TotalType_5.Name = "_E_TotalType_5";
            this._E_TotalType_5.ReadOnly = true;
            this._E_TotalType_5.Size = new System.Drawing.Size(112, 20);
            this._E_TotalType_5.TabIndex = 46;
            this._E_TotalType_5.Text = "Operating";
            // 
            // _E_TotalType_4
            // 
            this._E_TotalType_4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._E_TotalType_4.Location = new System.Drawing.Point(8, 465);
            this._E_TotalType_4.Name = "_E_TotalType_4";
            this._E_TotalType_4.ReadOnly = true;
            this._E_TotalType_4.Size = new System.Drawing.Size(112, 20);
            this._E_TotalType_4.TabIndex = 45;
            this._E_TotalType_4.Text = "Insurance";
            // 
            // _E_TotalType_3
            // 
            this._E_TotalType_3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._E_TotalType_3.Location = new System.Drawing.Point(8, 446);
            this._E_TotalType_3.Name = "_E_TotalType_3";
            this._E_TotalType_3.ReadOnly = true;
            this._E_TotalType_3.Size = new System.Drawing.Size(112, 20);
            this._E_TotalType_3.TabIndex = 44;
            this._E_TotalType_3.Text = "Utilities";
            // 
            // _E_TotalType_2
            // 
            this._E_TotalType_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._E_TotalType_2.Location = new System.Drawing.Point(8, 427);
            this._E_TotalType_2.Name = "_E_TotalType_2";
            this._E_TotalType_2.ReadOnly = true;
            this._E_TotalType_2.Size = new System.Drawing.Size(112, 20);
            this._E_TotalType_2.TabIndex = 43;
            this._E_TotalType_2.Text = "Management";
            // 
            // label133
            // 
            this.label133.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label133.Location = new System.Drawing.Point(0, 75);
            this.label133.Name = "label133";
            this.label133.Size = new System.Drawing.Size(112, 23);
            this.label133.TabIndex = 41;
            this.label133.Text = "EXPENSES:";
            this.label133.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label132
            // 
            this.label132.Location = new System.Drawing.Point(829, 19);
            this.label132.Name = "label132";
            this.label132.Size = new System.Drawing.Size(48, 23);
            this.label132.TabIndex = 40;
            this.label132.Text = "Year 10";
            this.label132.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // label131
            // 
            this.label131.Location = new System.Drawing.Point(776, 19);
            this.label131.Name = "label131";
            this.label131.Size = new System.Drawing.Size(40, 23);
            this.label131.TabIndex = 39;
            this.label131.Text = "Year 9";
            this.label131.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // label130
            // 
            this.label130.Location = new System.Drawing.Point(718, 19);
            this.label130.Name = "label130";
            this.label130.Size = new System.Drawing.Size(40, 23);
            this.label130.TabIndex = 38;
            this.label130.Text = "Year 8";
            this.label130.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // label129
            // 
            this.label129.Location = new System.Drawing.Point(660, 19);
            this.label129.Name = "label129";
            this.label129.Size = new System.Drawing.Size(40, 23);
            this.label129.TabIndex = 37;
            this.label129.Text = "Year 7";
            this.label129.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // label128
            // 
            this.label128.Location = new System.Drawing.Point(603, 19);
            this.label128.Name = "label128";
            this.label128.Size = new System.Drawing.Size(40, 23);
            this.label128.TabIndex = 36;
            this.label128.Text = "Year 6";
            this.label128.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // label127
            // 
            this.label127.Location = new System.Drawing.Point(542, 19);
            this.label127.Name = "label127";
            this.label127.Size = new System.Drawing.Size(40, 23);
            this.label127.TabIndex = 35;
            this.label127.Text = "Year 5";
            this.label127.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // label126
            // 
            this.label126.Location = new System.Drawing.Point(482, 19);
            this.label126.Name = "label126";
            this.label126.Size = new System.Drawing.Size(40, 23);
            this.label126.TabIndex = 34;
            this.label126.Text = "Year 4";
            this.label126.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // label125
            // 
            this.label125.Location = new System.Drawing.Point(420, 19);
            this.label125.Name = "label125";
            this.label125.Size = new System.Drawing.Size(40, 23);
            this.label125.TabIndex = 33;
            this.label125.Text = "Year 3";
            this.label125.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // label124
            // 
            this.label124.Location = new System.Drawing.Point(359, 19);
            this.label124.Name = "label124";
            this.label124.Size = new System.Drawing.Size(40, 23);
            this.label124.TabIndex = 32;
            this.label124.Text = "Year 2";
            this.label124.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // label123
            // 
            this.label123.Location = new System.Drawing.Point(298, 19);
            this.label123.Name = "label123";
            this.label123.Size = new System.Drawing.Size(40, 23);
            this.label123.TabIndex = 31;
            this.label123.Text = "Year 1";
            this.label123.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // label122
            // 
            this.label122.Location = new System.Drawing.Point(248, 67);
            this.label122.Name = "label122";
            this.label122.Size = new System.Drawing.Size(32, 23);
            this.label122.TabIndex = 30;
            this.label122.Text = "Type:";
            // 
            // label121
            // 
            this.label121.Location = new System.Drawing.Point(248, 43);
            this.label121.Name = "label121";
            this.label121.Size = new System.Drawing.Size(32, 23);
            this.label121.TabIndex = 29;
            this.label121.Text = "Cost:";
            // 
            // _E_CA_Type_Year10
            // 
            this._E_CA_Type_Year10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._E_CA_Type_Year10.Location = new System.Drawing.Point(825, 67);
            this._E_CA_Type_Year10.Name = "_E_CA_Type_Year10";
            this._E_CA_Type_Year10.Size = new System.Drawing.Size(58, 20);
            this._E_CA_Type_Year10.TabIndex = 28;
            // 
            // _E_CA_Cost_Year10
            // 
            this._E_CA_Cost_Year10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._E_CA_Cost_Year10.Location = new System.Drawing.Point(825, 43);
            this._E_CA_Cost_Year10.Name = "_E_CA_Cost_Year10";
            this._E_CA_Cost_Year10.Size = new System.Drawing.Size(58, 20);
            this._E_CA_Cost_Year10.TabIndex = 27;
            // 
            // _E_CA_Type_Year9
            // 
            this._E_CA_Type_Year9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._E_CA_Type_Year9.Location = new System.Drawing.Point(767, 67);
            this._E_CA_Type_Year9.Name = "_E_CA_Type_Year9";
            this._E_CA_Type_Year9.Size = new System.Drawing.Size(58, 20);
            this._E_CA_Type_Year9.TabIndex = 26;
            // 
            // _E_CA_Cost_Year9
            // 
            this._E_CA_Cost_Year9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._E_CA_Cost_Year9.Location = new System.Drawing.Point(767, 43);
            this._E_CA_Cost_Year9.Name = "_E_CA_Cost_Year9";
            this._E_CA_Cost_Year9.Size = new System.Drawing.Size(58, 20);
            this._E_CA_Cost_Year9.TabIndex = 25;
            // 
            // _E_CA_Type_Year8
            // 
            this._E_CA_Type_Year8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._E_CA_Type_Year8.Location = new System.Drawing.Point(709, 67);
            this._E_CA_Type_Year8.Name = "_E_CA_Type_Year8";
            this._E_CA_Type_Year8.Size = new System.Drawing.Size(58, 20);
            this._E_CA_Type_Year8.TabIndex = 24;
            // 
            // _E_CA_Cost_Year8
            // 
            this._E_CA_Cost_Year8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._E_CA_Cost_Year8.Location = new System.Drawing.Point(709, 43);
            this._E_CA_Cost_Year8.Name = "_E_CA_Cost_Year8";
            this._E_CA_Cost_Year8.Size = new System.Drawing.Size(58, 20);
            this._E_CA_Cost_Year8.TabIndex = 23;
            // 
            // _E_CA_Type_Year7
            // 
            this._E_CA_Type_Year7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._E_CA_Type_Year7.Location = new System.Drawing.Point(651, 67);
            this._E_CA_Type_Year7.Name = "_E_CA_Type_Year7";
            this._E_CA_Type_Year7.Size = new System.Drawing.Size(58, 20);
            this._E_CA_Type_Year7.TabIndex = 22;
            // 
            // _E_CA_Cost_Year7
            // 
            this._E_CA_Cost_Year7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._E_CA_Cost_Year7.Location = new System.Drawing.Point(651, 43);
            this._E_CA_Cost_Year7.Name = "_E_CA_Cost_Year7";
            this._E_CA_Cost_Year7.Size = new System.Drawing.Size(58, 20);
            this._E_CA_Cost_Year7.TabIndex = 21;
            // 
            // _E_CA_Type_Year6
            // 
            this._E_CA_Type_Year6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._E_CA_Type_Year6.Location = new System.Drawing.Point(593, 67);
            this._E_CA_Type_Year6.Name = "_E_CA_Type_Year6";
            this._E_CA_Type_Year6.Size = new System.Drawing.Size(58, 20);
            this._E_CA_Type_Year6.TabIndex = 20;
            // 
            // _E_CA_Cost_Year6
            // 
            this._E_CA_Cost_Year6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._E_CA_Cost_Year6.Location = new System.Drawing.Point(593, 43);
            this._E_CA_Cost_Year6.Name = "_E_CA_Cost_Year6";
            this._E_CA_Cost_Year6.Size = new System.Drawing.Size(58, 20);
            this._E_CA_Cost_Year6.TabIndex = 19;
            // 
            // _E_CA_Type_Year5
            // 
            this._E_CA_Type_Year5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._E_CA_Type_Year5.Location = new System.Drawing.Point(532, 67);
            this._E_CA_Type_Year5.Name = "_E_CA_Type_Year5";
            this._E_CA_Type_Year5.Size = new System.Drawing.Size(61, 20);
            this._E_CA_Type_Year5.TabIndex = 18;
            // 
            // _E_CA_Cost_Year5
            // 
            this._E_CA_Cost_Year5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._E_CA_Cost_Year5.Location = new System.Drawing.Point(532, 43);
            this._E_CA_Cost_Year5.Name = "_E_CA_Cost_Year5";
            this._E_CA_Cost_Year5.Size = new System.Drawing.Size(61, 20);
            this._E_CA_Cost_Year5.TabIndex = 17;
            // 
            // _E_CA_Type_Year4
            // 
            this._E_CA_Type_Year4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._E_CA_Type_Year4.Location = new System.Drawing.Point(471, 67);
            this._E_CA_Type_Year4.Name = "_E_CA_Type_Year4";
            this._E_CA_Type_Year4.Size = new System.Drawing.Size(61, 20);
            this._E_CA_Type_Year4.TabIndex = 16;
            // 
            // _E_CA_Cost_Year4
            // 
            this._E_CA_Cost_Year4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._E_CA_Cost_Year4.Location = new System.Drawing.Point(471, 43);
            this._E_CA_Cost_Year4.Name = "_E_CA_Cost_Year4";
            this._E_CA_Cost_Year4.Size = new System.Drawing.Size(61, 20);
            this._E_CA_Cost_Year4.TabIndex = 15;
            // 
            // _E_CA_Type_Year3
            // 
            this._E_CA_Type_Year3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._E_CA_Type_Year3.Location = new System.Drawing.Point(410, 67);
            this._E_CA_Type_Year3.Name = "_E_CA_Type_Year3";
            this._E_CA_Type_Year3.Size = new System.Drawing.Size(61, 20);
            this._E_CA_Type_Year3.TabIndex = 14;
            // 
            // _E_CA_Cost_Year3
            // 
            this._E_CA_Cost_Year3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._E_CA_Cost_Year3.Location = new System.Drawing.Point(410, 43);
            this._E_CA_Cost_Year3.Name = "_E_CA_Cost_Year3";
            this._E_CA_Cost_Year3.Size = new System.Drawing.Size(61, 20);
            this._E_CA_Cost_Year3.TabIndex = 13;
            // 
            // _E_CA_Type_Year2
            // 
            this._E_CA_Type_Year2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._E_CA_Type_Year2.Location = new System.Drawing.Point(349, 67);
            this._E_CA_Type_Year2.Name = "_E_CA_Type_Year2";
            this._E_CA_Type_Year2.Size = new System.Drawing.Size(61, 20);
            this._E_CA_Type_Year2.TabIndex = 12;
            // 
            // _E_CA_Cost_Year2
            // 
            this._E_CA_Cost_Year2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._E_CA_Cost_Year2.Location = new System.Drawing.Point(349, 43);
            this._E_CA_Cost_Year2.Name = "_E_CA_Cost_Year2";
            this._E_CA_Cost_Year2.Size = new System.Drawing.Size(61, 20);
            this._E_CA_Cost_Year2.TabIndex = 11;
            // 
            // _E_CA_Type_Year1
            // 
            this._E_CA_Type_Year1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._E_CA_Type_Year1.Location = new System.Drawing.Point(288, 67);
            this._E_CA_Type_Year1.Name = "_E_CA_Type_Year1";
            this._E_CA_Type_Year1.Size = new System.Drawing.Size(61, 20);
            this._E_CA_Type_Year1.TabIndex = 10;
            // 
            // _E_CA_Cost_Year1
            // 
            this._E_CA_Cost_Year1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._E_CA_Cost_Year1.Location = new System.Drawing.Point(288, 43);
            this._E_CA_Cost_Year1.Name = "_E_CA_Cost_Year1";
            this._E_CA_Cost_Year1.Size = new System.Drawing.Size(61, 20);
            this._E_CA_Cost_Year1.TabIndex = 9;
            // 
            // label120
            // 
            this.label120.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label120.Location = new System.Drawing.Point(272, 8);
            this.label120.Name = "label120";
            this.label120.Size = new System.Drawing.Size(264, 23);
            this.label120.TabIndex = 8;
            this.label120.Text = "CAPITAL ADDITIONS (IMPROVEMENTS)";
            // 
            // _E_UT_HotWater
            // 
            this._E_UT_HotWater.Items.AddRange(new object[] {
            "Electric",
            "Gas"});
            this._E_UT_HotWater.Location = new System.Drawing.Point(168, 42);
            this._E_UT_HotWater.Name = "_E_UT_HotWater";
            this._E_UT_HotWater.Size = new System.Drawing.Size(72, 21);
            this._E_UT_HotWater.TabIndex = 7;
            // 
            // label119
            // 
            this.label119.Location = new System.Drawing.Point(168, 26);
            this.label119.Name = "label119";
            this.label119.Size = new System.Drawing.Size(64, 23);
            this.label119.TabIndex = 6;
            this.label119.Text = "Hot Water:";
            // 
            // _E_UT_Heat
            // 
            this._E_UT_Heat.Items.AddRange(new object[] {
            "Electric",
            "Gas"});
            this._E_UT_Heat.Location = new System.Drawing.Point(96, 42);
            this._E_UT_Heat.Name = "_E_UT_Heat";
            this._E_UT_Heat.Size = new System.Drawing.Size(72, 21);
            this._E_UT_Heat.TabIndex = 5;
            // 
            // label118
            // 
            this.label118.Location = new System.Drawing.Point(96, 26);
            this.label118.Name = "label118";
            this.label118.Size = new System.Drawing.Size(64, 23);
            this.label118.TabIndex = 4;
            this.label118.Text = "Heat:";
            // 
            // _E_UT_ElectricityMeter
            // 
            this._E_UT_ElectricityMeter.Items.AddRange(new object[] {
            "Individual",
            "Master"});
            this._E_UT_ElectricityMeter.Location = new System.Drawing.Point(8, 42);
            this._E_UT_ElectricityMeter.Name = "_E_UT_ElectricityMeter";
            this._E_UT_ElectricityMeter.Size = new System.Drawing.Size(88, 21);
            this._E_UT_ElectricityMeter.TabIndex = 3;
            // 
            // label117
            // 
            this.label117.Location = new System.Drawing.Point(8, 26);
            this.label117.Name = "label117";
            this.label117.Size = new System.Drawing.Size(96, 23);
            this.label117.TabIndex = 2;
            this.label117.Text = "Electricity Meter:";
            // 
            // label116
            // 
            this.label116.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label116.Location = new System.Drawing.Point(0, 8);
            this.label116.Name = "label116";
            this.label116.Size = new System.Drawing.Size(112, 23);
            this.label116.TabIndex = 1;
            this.label116.Text = "UTILITY TYPES:";
            // 
            // _loansTabPage
            // 
            this._loansTabPage.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this._loansTabPage.Controls.Add(this._printLoanInfo);
            this._loansTabPage.Controls.Add(this._l_floater);
            this._loansTabPage.Controls.Add(this._L_3L_RA_LifeTimeCap);
            this._loansTabPage.Controls.Add(this._L_3L_RA_AnnualCap);
            this._loansTabPage.Controls.Add(this.label197);
            this._loansTabPage.Controls.Add(this.label198);
            this._loansTabPage.Controls.Add(this._L_3L_RA_InitialRateCap);
            this._loansTabPage.Controls.Add(this._L_3L_RA_InitFixedPeriod);
            this._loansTabPage.Controls.Add(this.label199);
            this._loansTabPage.Controls.Add(this.label200);
            this._loansTabPage.Controls.Add(this.label201);
            this._loansTabPage.Controls.Add(this.label202);
            this._loansTabPage.Controls.Add(this.label203);
            this._loansTabPage.Controls.Add(this.label204);
            this._loansTabPage.Controls.Add(this.label205);
            this._loansTabPage.Controls.Add(this.label206);
            this._loansTabPage.Controls.Add(this.label207);
            this._loansTabPage.Controls.Add(this.label208);
            this._loansTabPage.Controls.Add(this.label209);
            this._loansTabPage.Controls.Add(this.label210);
            this._loansTabPage.Controls.Add(this.label211);
            this._loansTabPage.Controls.Add(this.label212);
            this._loansTabPage.Controls.Add(this._L_3L_RA_RateYear10);
            this._loansTabPage.Controls.Add(this._L_3L_RA_RateYear9);
            this._loansTabPage.Controls.Add(this._L_3L_RA_RateYear8);
            this._loansTabPage.Controls.Add(this._L_3L_RA_RateYear7);
            this._loansTabPage.Controls.Add(this._L_3L_RA_RateYear6);
            this._loansTabPage.Controls.Add(this._L_3L_RA_RateYear5);
            this._loansTabPage.Controls.Add(this._L_3L_RA_RateYear4);
            this._loansTabPage.Controls.Add(this._L_3L_RA_RateYear3);
            this._loansTabPage.Controls.Add(this._L_3L_RA_RateYear2);
            this._loansTabPage.Controls.Add(this._L_3L_RA_RateYear1);
            this._loansTabPage.Controls.Add(this._L_3L_Lender);
            this._loansTabPage.Controls.Add(this._L_3L_LastPaymentDate);
            this._loansTabPage.Controls.Add(this._L_3L_FirstPaymentDate);
            this._loansTabPage.Controls.Add(this._L_3L_Points);
            this._loansTabPage.Controls.Add(this._L_3L_PointsPerc);
            this._loansTabPage.Controls.Add(this._L_3L_MonthlyPayment);
            this._loansTabPage.Controls.Add(this._L_3L_Rate);
            this._loansTabPage.Controls.Add(this._L_3L_AmortTerm);
            this._loansTabPage.Controls.Add(this._L_3L_Amount);
            this._loansTabPage.Controls.Add(this._L_3L_LTVPerc);
            this._loansTabPage.Controls.Add(this._L_3L_Type);
            this._loansTabPage.Controls.Add(this.label213);
            this._loansTabPage.Controls.Add(this.label214);
            this._loansTabPage.Controls.Add(this.label215);
            this._loansTabPage.Controls.Add(this.label216);
            this._loansTabPage.Controls.Add(this.label217);
            this._loansTabPage.Controls.Add(this.label218);
            this._loansTabPage.Controls.Add(this.label219);
            this._loansTabPage.Controls.Add(this.label220);
            this._loansTabPage.Controls.Add(this.label221);
            this._loansTabPage.Controls.Add(this.label222);
            this._loansTabPage.Controls.Add(this.label223);
            this._loansTabPage.Controls.Add(this.label224);
            this._loansTabPage.Controls.Add(this._L_2L_RA_LifeTimeCap);
            this._loansTabPage.Controls.Add(this._L_2L_RA_AnnualCap);
            this._loansTabPage.Controls.Add(this.label169);
            this._loansTabPage.Controls.Add(this.label170);
            this._loansTabPage.Controls.Add(this._L_2L_RA_InitialRateCap);
            this._loansTabPage.Controls.Add(this._L_2L_RA_InitFixedPeriod);
            this._loansTabPage.Controls.Add(this.label171);
            this._loansTabPage.Controls.Add(this.label172);
            this._loansTabPage.Controls.Add(this.label173);
            this._loansTabPage.Controls.Add(this.label174);
            this._loansTabPage.Controls.Add(this.label175);
            this._loansTabPage.Controls.Add(this.label176);
            this._loansTabPage.Controls.Add(this.label177);
            this._loansTabPage.Controls.Add(this.label178);
            this._loansTabPage.Controls.Add(this.label179);
            this._loansTabPage.Controls.Add(this.label180);
            this._loansTabPage.Controls.Add(this.label181);
            this._loansTabPage.Controls.Add(this.label182);
            this._loansTabPage.Controls.Add(this.label183);
            this._loansTabPage.Controls.Add(this.label184);
            this._loansTabPage.Controls.Add(this._L_2L_RA_RateYear10);
            this._loansTabPage.Controls.Add(this._L_2L_RA_RateYear9);
            this._loansTabPage.Controls.Add(this._L_2L_RA_RateYear8);
            this._loansTabPage.Controls.Add(this._L_2L_RA_RateYear7);
            this._loansTabPage.Controls.Add(this._L_2L_RA_RateYear6);
            this._loansTabPage.Controls.Add(this._L_2L_RA_RateYear5);
            this._loansTabPage.Controls.Add(this._L_2L_RA_RateYear4);
            this._loansTabPage.Controls.Add(this._L_2L_RA_RateYear3);
            this._loansTabPage.Controls.Add(this._L_2L_RA_RateYear2);
            this._loansTabPage.Controls.Add(this._L_2L_RA_RateYear1);
            this._loansTabPage.Controls.Add(this._L_2L_Lender);
            this._loansTabPage.Controls.Add(this._L_2L_LastPaymentDate);
            this._loansTabPage.Controls.Add(this._L_2L_FirstPaymentDate);
            this._loansTabPage.Controls.Add(this._L_2L_Points);
            this._loansTabPage.Controls.Add(this._L_2L_PointsPerc);
            this._loansTabPage.Controls.Add(this._L_2L_MonthlyPayment);
            this._loansTabPage.Controls.Add(this._L_2L_Rate);
            this._loansTabPage.Controls.Add(this._L_2L_AmortTerm);
            this._loansTabPage.Controls.Add(this._L_2L_Amount);
            this._loansTabPage.Controls.Add(this._L_2L_LTVPerc);
            this._loansTabPage.Controls.Add(this._L_2L_Type);
            this._loansTabPage.Controls.Add(this.label185);
            this._loansTabPage.Controls.Add(this.label186);
            this._loansTabPage.Controls.Add(this.label187);
            this._loansTabPage.Controls.Add(this.label188);
            this._loansTabPage.Controls.Add(this.label189);
            this._loansTabPage.Controls.Add(this.label190);
            this._loansTabPage.Controls.Add(this.label191);
            this._loansTabPage.Controls.Add(this.label192);
            this._loansTabPage.Controls.Add(this.label193);
            this._loansTabPage.Controls.Add(this.label194);
            this._loansTabPage.Controls.Add(this.label195);
            this._loansTabPage.Controls.Add(this.label196);
            this._loansTabPage.Controls.Add(this._L_1L_RA_LifeTimeCap);
            this._loansTabPage.Controls.Add(this._L_1L_RA_AnnualCap);
            this._loansTabPage.Controls.Add(this.label167);
            this._loansTabPage.Controls.Add(this.label168);
            this._loansTabPage.Controls.Add(this._L_1L_RA_InitialRateCap);
            this._loansTabPage.Controls.Add(this._L_1L_RA_InitFixedPeriod);
            this._loansTabPage.Controls.Add(this.label166);
            this._loansTabPage.Controls.Add(this.label165);
            this._loansTabPage.Controls.Add(this.label164);
            this._loansTabPage.Controls.Add(this.label153);
            this._loansTabPage.Controls.Add(this.label154);
            this._loansTabPage.Controls.Add(this.label155);
            this._loansTabPage.Controls.Add(this.label156);
            this._loansTabPage.Controls.Add(this.label157);
            this._loansTabPage.Controls.Add(this.label158);
            this._loansTabPage.Controls.Add(this.label159);
            this._loansTabPage.Controls.Add(this.label160);
            this._loansTabPage.Controls.Add(this.label161);
            this._loansTabPage.Controls.Add(this.label162);
            this._loansTabPage.Controls.Add(this.label163);
            this._loansTabPage.Controls.Add(this._L_1L_RA_RateYear10);
            this._loansTabPage.Controls.Add(this._L_1L_RA_RateYear9);
            this._loansTabPage.Controls.Add(this._L_1L_RA_RateYear8);
            this._loansTabPage.Controls.Add(this._L_1L_RA_RateYear7);
            this._loansTabPage.Controls.Add(this._L_1L_RA_RateYear6);
            this._loansTabPage.Controls.Add(this._L_1L_RA_RateYear5);
            this._loansTabPage.Controls.Add(this._L_1L_RA_RateYear4);
            this._loansTabPage.Controls.Add(this._L_1L_RA_RateYear3);
            this._loansTabPage.Controls.Add(this._L_1L_RA_RateYear2);
            this._loansTabPage.Controls.Add(this._L_1L_RA_RateYear1);
            this._loansTabPage.Controls.Add(this._L_1L_Lender);
            this._loansTabPage.Controls.Add(this._L_1L_LastPaymentDate);
            this._loansTabPage.Controls.Add(this._L_1L_FirstPaymentDate);
            this._loansTabPage.Controls.Add(this._L_1L_Points);
            this._loansTabPage.Controls.Add(this._L_1L_PointsPerc);
            this._loansTabPage.Controls.Add(this._L_1L_MonthlyPayment);
            this._loansTabPage.Controls.Add(this._L_1L_Rate);
            this._loansTabPage.Controls.Add(this._L_1L_AmortTerm);
            this._loansTabPage.Controls.Add(this._L_1L_Amount);
            this._loansTabPage.Controls.Add(this._L_1L_LTVPerc);
            this._loansTabPage.Controls.Add(this._L_1L_Type);
            this._loansTabPage.Controls.Add(this.label152);
            this._loansTabPage.Controls.Add(this.label151);
            this._loansTabPage.Controls.Add(this.label150);
            this._loansTabPage.Controls.Add(this.label149);
            this._loansTabPage.Controls.Add(this.label148);
            this._loansTabPage.Controls.Add(this.label147);
            this._loansTabPage.Controls.Add(this.label146);
            this._loansTabPage.Controls.Add(this.label145);
            this._loansTabPage.Controls.Add(this.label144);
            this._loansTabPage.Controls.Add(this.label143);
            this._loansTabPage.Controls.Add(this.label142);
            this._loansTabPage.Controls.Add(this.label141);
            this._loansTabPage.Controls.Add(this._L_TotalPaymentsAnnual);
            this._loansTabPage.Controls.Add(this._L_TotalPaymentsMonthly);
            this._loansTabPage.Controls.Add(this.label94);
            this._loansTabPage.Controls.Add(this._L_DCR);
            this._loansTabPage.Controls.Add(this._L_CLTVPerc);
            this._loansTabPage.Controls.Add(this._L_BlendedRate);
            this._loansTabPage.Controls.Add(this.label82);
            this._loansTabPage.Controls.Add(this.label81);
            this._loansTabPage.Controls.Add(this.label80);
            this._loansTabPage.Controls.Add(this.label79);
            this._loansTabPage.Controls.Add(this.label78);
            this._loansTabPage.Controls.Add(this._L_SellerCarryback);
            this._loansTabPage.Controls.Add(this._L_Assumable);
            this._loansTabPage.Controls.Add(this._L_1stAssumable);
            this._loansTabPage.Controls.Add(this.label77);
            this._loansTabPage.Controls.Add(this.label76);
            this._loansTabPage.Controls.Add(this.label75);
            this._loansTabPage.Controls.Add(this._L_DownPaymentPerc);
            this._loansTabPage.Controls.Add(this._L_DownPayment);
            this._loansTabPage.Controls.Add(this._L_PurchasePrice);
            this._loansTabPage.Controls.Add(this.label74);
            this._loansTabPage.Controls.Add(this.label73);
            this._loansTabPage.Controls.Add(this.label72);
            this._loansTabPage.Controls.Add(this.label71);
            this._loansTabPage.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this._loansTabPage.Location = new System.Drawing.Point(4, 22);
            this._loansTabPage.Name = "_loansTabPage";
            this._loansTabPage.Size = new System.Drawing.Size(897, 529);
            this._loansTabPage.TabIndex = 3;
            this._loansTabPage.Text = "Loans";
            // 
            // _printLoanInfo
            // 
            this._printLoanInfo.Location = new System.Drawing.Point(782, 32);
            this._printLoanInfo.Name = "_printLoanInfo";
            this._printLoanInfo.Size = new System.Drawing.Size(106, 23);
            this._printLoanInfo.TabIndex = 182;
            this._printLoanInfo.Text = "Print Loan Info";
            this._printLoanInfo.UseVisualStyleBackColor = true;
            // 
            // _l_floater
            // 
            this._l_floater.Image = ((System.Drawing.Image)(resources.GetObject("_l_floater.Image")));
            this._l_floater.Location = new System.Drawing.Point(872, 8);
            this._l_floater.Name = "_l_floater";
            this._l_floater.Size = new System.Drawing.Size(16, 16);
            this._l_floater.TabIndex = 181;
            this._l_floater.TabStop = false;
            // 
            // _L_3L_RA_LifeTimeCap
            // 
            this._L_3L_RA_LifeTimeCap.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._L_3L_RA_LifeTimeCap.Location = new System.Drawing.Point(318, 504);
            this._L_3L_RA_LifeTimeCap.Name = "_L_3L_RA_LifeTimeCap";
            this._L_3L_RA_LifeTimeCap.ReadOnly = true;
            this._L_3L_RA_LifeTimeCap.Size = new System.Drawing.Size(60, 20);
            this._L_3L_RA_LifeTimeCap.TabIndex = 156;
            this._L_3L_RA_LifeTimeCap.TabStop = false;
            // 
            // _L_3L_RA_AnnualCap
            // 
            this._L_3L_RA_AnnualCap.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._L_3L_RA_AnnualCap.Location = new System.Drawing.Point(318, 480);
            this._L_3L_RA_AnnualCap.Name = "_L_3L_RA_AnnualCap";
            this._L_3L_RA_AnnualCap.ReadOnly = true;
            this._L_3L_RA_AnnualCap.Size = new System.Drawing.Size(60, 20);
            this._L_3L_RA_AnnualCap.TabIndex = 154;
            this._L_3L_RA_AnnualCap.TabStop = false;
            // 
            // label197
            // 
            this.label197.Location = new System.Drawing.Point(222, 504);
            this.label197.Name = "label197";
            this.label197.Size = new System.Drawing.Size(96, 23);
            this.label197.TabIndex = 180;
            this.label197.Text = "Lifetime Cap (%)";
            this.label197.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label198
            // 
            this.label198.Location = new System.Drawing.Point(222, 480);
            this.label198.Name = "label198";
            this.label198.Size = new System.Drawing.Size(96, 23);
            this.label198.TabIndex = 179;
            this.label198.Text = "Annual Cap (%):";
            this.label198.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // _L_3L_RA_InitialRateCap
            // 
            this._L_3L_RA_InitialRateCap.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._L_3L_RA_InitialRateCap.Location = new System.Drawing.Point(142, 504);
            this._L_3L_RA_InitialRateCap.Name = "_L_3L_RA_InitialRateCap";
            this._L_3L_RA_InitialRateCap.ReadOnly = true;
            this._L_3L_RA_InitialRateCap.Size = new System.Drawing.Size(60, 20);
            this._L_3L_RA_InitialRateCap.TabIndex = 155;
            this._L_3L_RA_InitialRateCap.TabStop = false;
            // 
            // _L_3L_RA_InitFixedPeriod
            // 
            this._L_3L_RA_InitFixedPeriod.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._L_3L_RA_InitFixedPeriod.Location = new System.Drawing.Point(142, 480);
            this._L_3L_RA_InitFixedPeriod.Name = "_L_3L_RA_InitFixedPeriod";
            this._L_3L_RA_InitFixedPeriod.ReadOnly = true;
            this._L_3L_RA_InitFixedPeriod.Size = new System.Drawing.Size(60, 20);
            this._L_3L_RA_InitFixedPeriod.TabIndex = 153;
            this._L_3L_RA_InitFixedPeriod.TabStop = false;
            // 
            // label199
            // 
            this.label199.Location = new System.Drawing.Point(14, 504);
            this.label199.Name = "label199";
            this.label199.Size = new System.Drawing.Size(128, 23);
            this.label199.TabIndex = 176;
            this.label199.Text = "Initial Rate Cap (%)";
            this.label199.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label200
            // 
            this.label200.Location = new System.Drawing.Point(14, 480);
            this.label200.Name = "label200";
            this.label200.Size = new System.Drawing.Size(128, 23);
            this.label200.TabIndex = 175;
            this.label200.Text = "Initial Fixed Period (Yrs):";
            this.label200.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label201
            // 
            this.label201.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label201.Location = new System.Drawing.Point(6, 456);
            this.label201.Name = "label201";
            this.label201.Size = new System.Drawing.Size(258, 23);
            this.label201.TabIndex = 174;
            this.label201.Text = "3rd Loan Rate Adjustments (if Adjustable):";
            // 
            // label202
            // 
            this.label202.Location = new System.Drawing.Point(843, 456);
            this.label202.Name = "label202";
            this.label202.Size = new System.Drawing.Size(48, 23);
            this.label202.TabIndex = 173;
            this.label202.Text = "Year 10";
            this.label202.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // label203
            // 
            this.label203.Location = new System.Drawing.Point(806, 456);
            this.label203.Name = "label203";
            this.label203.Size = new System.Drawing.Size(40, 23);
            this.label203.TabIndex = 172;
            this.label203.Text = "Year 9";
            this.label203.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // label204
            // 
            this.label204.Location = new System.Drawing.Point(766, 456);
            this.label204.Name = "label204";
            this.label204.Size = new System.Drawing.Size(40, 23);
            this.label204.TabIndex = 171;
            this.label204.Text = "Year 8";
            this.label204.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // label205
            // 
            this.label205.Location = new System.Drawing.Point(726, 456);
            this.label205.Name = "label205";
            this.label205.Size = new System.Drawing.Size(40, 23);
            this.label205.TabIndex = 170;
            this.label205.Text = "Year 7";
            this.label205.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // label206
            // 
            this.label206.Location = new System.Drawing.Point(686, 456);
            this.label206.Name = "label206";
            this.label206.Size = new System.Drawing.Size(40, 23);
            this.label206.TabIndex = 169;
            this.label206.Text = "Year 6";
            this.label206.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // label207
            // 
            this.label207.Location = new System.Drawing.Point(646, 456);
            this.label207.Name = "label207";
            this.label207.Size = new System.Drawing.Size(40, 23);
            this.label207.TabIndex = 168;
            this.label207.Text = "Year 5";
            this.label207.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // label208
            // 
            this.label208.Location = new System.Drawing.Point(606, 456);
            this.label208.Name = "label208";
            this.label208.Size = new System.Drawing.Size(40, 23);
            this.label208.TabIndex = 167;
            this.label208.Text = "Year 4";
            this.label208.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // label209
            // 
            this.label209.Location = new System.Drawing.Point(566, 456);
            this.label209.Name = "label209";
            this.label209.Size = new System.Drawing.Size(40, 23);
            this.label209.TabIndex = 166;
            this.label209.Text = "Year 3";
            this.label209.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // label210
            // 
            this.label210.Location = new System.Drawing.Point(526, 456);
            this.label210.Name = "label210";
            this.label210.Size = new System.Drawing.Size(40, 23);
            this.label210.TabIndex = 165;
            this.label210.Text = "Year 2";
            this.label210.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // label211
            // 
            this.label211.Location = new System.Drawing.Point(486, 456);
            this.label211.Name = "label211";
            this.label211.Size = new System.Drawing.Size(40, 23);
            this.label211.TabIndex = 164;
            this.label211.Text = "Year 1";
            this.label211.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // label212
            // 
            this.label212.Location = new System.Drawing.Point(422, 480);
            this.label212.Name = "label212";
            this.label212.Size = new System.Drawing.Size(56, 23);
            this.label212.TabIndex = 163;
            this.label212.Text = "Rate (%):";
            this.label212.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // _L_3L_RA_RateYear10
            // 
            this._L_3L_RA_RateYear10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._L_3L_RA_RateYear10.Location = new System.Drawing.Point(846, 480);
            this._L_3L_RA_RateYear10.Name = "_L_3L_RA_RateYear10";
            this._L_3L_RA_RateYear10.ReadOnly = true;
            this._L_3L_RA_RateYear10.Size = new System.Drawing.Size(40, 20);
            this._L_3L_RA_RateYear10.TabIndex = 166;
            this._L_3L_RA_RateYear10.TabStop = false;
            // 
            // _L_3L_RA_RateYear9
            // 
            this._L_3L_RA_RateYear9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._L_3L_RA_RateYear9.Location = new System.Drawing.Point(806, 480);
            this._L_3L_RA_RateYear9.Name = "_L_3L_RA_RateYear9";
            this._L_3L_RA_RateYear9.ReadOnly = true;
            this._L_3L_RA_RateYear9.Size = new System.Drawing.Size(40, 20);
            this._L_3L_RA_RateYear9.TabIndex = 165;
            this._L_3L_RA_RateYear9.TabStop = false;
            // 
            // _L_3L_RA_RateYear8
            // 
            this._L_3L_RA_RateYear8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._L_3L_RA_RateYear8.Location = new System.Drawing.Point(766, 480);
            this._L_3L_RA_RateYear8.Name = "_L_3L_RA_RateYear8";
            this._L_3L_RA_RateYear8.ReadOnly = true;
            this._L_3L_RA_RateYear8.Size = new System.Drawing.Size(40, 20);
            this._L_3L_RA_RateYear8.TabIndex = 164;
            this._L_3L_RA_RateYear8.TabStop = false;
            // 
            // _L_3L_RA_RateYear7
            // 
            this._L_3L_RA_RateYear7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._L_3L_RA_RateYear7.Location = new System.Drawing.Point(726, 480);
            this._L_3L_RA_RateYear7.Name = "_L_3L_RA_RateYear7";
            this._L_3L_RA_RateYear7.ReadOnly = true;
            this._L_3L_RA_RateYear7.Size = new System.Drawing.Size(40, 20);
            this._L_3L_RA_RateYear7.TabIndex = 163;
            this._L_3L_RA_RateYear7.TabStop = false;
            // 
            // _L_3L_RA_RateYear6
            // 
            this._L_3L_RA_RateYear6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._L_3L_RA_RateYear6.Location = new System.Drawing.Point(686, 480);
            this._L_3L_RA_RateYear6.Name = "_L_3L_RA_RateYear6";
            this._L_3L_RA_RateYear6.ReadOnly = true;
            this._L_3L_RA_RateYear6.Size = new System.Drawing.Size(40, 20);
            this._L_3L_RA_RateYear6.TabIndex = 162;
            this._L_3L_RA_RateYear6.TabStop = false;
            // 
            // _L_3L_RA_RateYear5
            // 
            this._L_3L_RA_RateYear5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._L_3L_RA_RateYear5.Location = new System.Drawing.Point(646, 480);
            this._L_3L_RA_RateYear5.Name = "_L_3L_RA_RateYear5";
            this._L_3L_RA_RateYear5.ReadOnly = true;
            this._L_3L_RA_RateYear5.Size = new System.Drawing.Size(40, 20);
            this._L_3L_RA_RateYear5.TabIndex = 161;
            this._L_3L_RA_RateYear5.TabStop = false;
            // 
            // _L_3L_RA_RateYear4
            // 
            this._L_3L_RA_RateYear4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._L_3L_RA_RateYear4.Location = new System.Drawing.Point(606, 480);
            this._L_3L_RA_RateYear4.Name = "_L_3L_RA_RateYear4";
            this._L_3L_RA_RateYear4.ReadOnly = true;
            this._L_3L_RA_RateYear4.Size = new System.Drawing.Size(40, 20);
            this._L_3L_RA_RateYear4.TabIndex = 160;
            this._L_3L_RA_RateYear4.TabStop = false;
            // 
            // _L_3L_RA_RateYear3
            // 
            this._L_3L_RA_RateYear3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._L_3L_RA_RateYear3.Location = new System.Drawing.Point(566, 480);
            this._L_3L_RA_RateYear3.Name = "_L_3L_RA_RateYear3";
            this._L_3L_RA_RateYear3.ReadOnly = true;
            this._L_3L_RA_RateYear3.Size = new System.Drawing.Size(40, 20);
            this._L_3L_RA_RateYear3.TabIndex = 159;
            this._L_3L_RA_RateYear3.TabStop = false;
            // 
            // _L_3L_RA_RateYear2
            // 
            this._L_3L_RA_RateYear2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._L_3L_RA_RateYear2.Location = new System.Drawing.Point(526, 480);
            this._L_3L_RA_RateYear2.Name = "_L_3L_RA_RateYear2";
            this._L_3L_RA_RateYear2.ReadOnly = true;
            this._L_3L_RA_RateYear2.Size = new System.Drawing.Size(40, 20);
            this._L_3L_RA_RateYear2.TabIndex = 158;
            this._L_3L_RA_RateYear2.TabStop = false;
            // 
            // _L_3L_RA_RateYear1
            // 
            this._L_3L_RA_RateYear1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._L_3L_RA_RateYear1.Location = new System.Drawing.Point(486, 480);
            this._L_3L_RA_RateYear1.Name = "_L_3L_RA_RateYear1";
            this._L_3L_RA_RateYear1.ReadOnly = true;
            this._L_3L_RA_RateYear1.Size = new System.Drawing.Size(40, 20);
            this._L_3L_RA_RateYear1.TabIndex = 157;
            this._L_3L_RA_RateYear1.TabStop = false;
            // 
            // _L_3L_Lender
            // 
            this._L_3L_Lender.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._L_3L_Lender.Location = new System.Drawing.Point(790, 432);
            this._L_3L_Lender.Name = "_L_3L_Lender";
            this._L_3L_Lender.Size = new System.Drawing.Size(96, 20);
            this._L_3L_Lender.TabIndex = 152;
            // 
            // _L_3L_LastPaymentDate
            // 
            this._L_3L_LastPaymentDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._L_3L_LastPaymentDate.Location = new System.Drawing.Point(686, 432);
            this._L_3L_LastPaymentDate.Name = "_L_3L_LastPaymentDate";
            this._L_3L_LastPaymentDate.Size = new System.Drawing.Size(104, 20);
            this._L_3L_LastPaymentDate.TabIndex = 151;
            // 
            // _L_3L_FirstPaymentDate
            // 
            this._L_3L_FirstPaymentDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._L_3L_FirstPaymentDate.Location = new System.Drawing.Point(582, 432);
            this._L_3L_FirstPaymentDate.Name = "_L_3L_FirstPaymentDate";
            this._L_3L_FirstPaymentDate.Size = new System.Drawing.Size(104, 20);
            this._L_3L_FirstPaymentDate.TabIndex = 150;
            // 
            // _L_3L_Points
            // 
            this._L_3L_Points.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._L_3L_Points.Location = new System.Drawing.Point(510, 432);
            this._L_3L_Points.Name = "_L_3L_Points";
            this._L_3L_Points.Size = new System.Drawing.Size(72, 20);
            this._L_3L_Points.TabIndex = 149;
            // 
            // _L_3L_PointsPerc
            // 
            this._L_3L_PointsPerc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._L_3L_PointsPerc.Location = new System.Drawing.Point(438, 432);
            this._L_3L_PointsPerc.Name = "_L_3L_PointsPerc";
            this._L_3L_PointsPerc.Size = new System.Drawing.Size(72, 20);
            this._L_3L_PointsPerc.TabIndex = 148;
            // 
            // _L_3L_MonthlyPayment
            // 
            this._L_3L_MonthlyPayment.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._L_3L_MonthlyPayment.Location = new System.Drawing.Point(350, 432);
            this._L_3L_MonthlyPayment.Name = "_L_3L_MonthlyPayment";
            this._L_3L_MonthlyPayment.Size = new System.Drawing.Size(88, 20);
            this._L_3L_MonthlyPayment.TabIndex = 147;
            // 
            // _L_3L_Rate
            // 
            this._L_3L_Rate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._L_3L_Rate.Location = new System.Drawing.Point(286, 432);
            this._L_3L_Rate.Name = "_L_3L_Rate";
            this._L_3L_Rate.Size = new System.Drawing.Size(64, 20);
            this._L_3L_Rate.TabIndex = 146;
            // 
            // _L_3L_AmortTerm
            // 
            this._L_3L_AmortTerm.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._L_3L_AmortTerm.Location = new System.Drawing.Point(190, 432);
            this._L_3L_AmortTerm.Name = "_L_3L_AmortTerm";
            this._L_3L_AmortTerm.ReadOnly = true;
            this._L_3L_AmortTerm.Size = new System.Drawing.Size(96, 20);
            this._L_3L_AmortTerm.TabIndex = 145;
            this._L_3L_AmortTerm.TabStop = false;
            // 
            // _L_3L_Amount
            // 
            this._L_3L_Amount.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._L_3L_Amount.Location = new System.Drawing.Point(120, 432);
            this._L_3L_Amount.Name = "_L_3L_Amount";
            this._L_3L_Amount.Size = new System.Drawing.Size(72, 20);
            this._L_3L_Amount.TabIndex = 144;
            // 
            // _L_3L_LTVPerc
            // 
            this._L_3L_LTVPerc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._L_3L_LTVPerc.Location = new System.Drawing.Point(80, 432);
            this._L_3L_LTVPerc.Name = "_L_3L_LTVPerc";
            this._L_3L_LTVPerc.Size = new System.Drawing.Size(40, 20);
            this._L_3L_LTVPerc.TabIndex = 143;
            // 
            // _L_3L_Type
            // 
            this._L_3L_Type.Items.AddRange(new object[] {
            "Fixed",
            "Adjustable",
            "IO"});
            this._L_3L_Type.Location = new System.Drawing.Point(6, 432);
            this._L_3L_Type.Name = "_L_3L_Type";
            this._L_3L_Type.Size = new System.Drawing.Size(74, 21);
            this._L_3L_Type.TabIndex = 142;
            // 
            // label213
            // 
            this.label213.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label213.Location = new System.Drawing.Point(790, 416);
            this.label213.Name = "label213";
            this.label213.Size = new System.Drawing.Size(64, 23);
            this.label213.TabIndex = 141;
            this.label213.Text = "Lender:";
            // 
            // label214
            // 
            this.label214.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label214.Location = new System.Drawing.Point(686, 416);
            this.label214.Name = "label214";
            this.label214.Size = new System.Drawing.Size(104, 23);
            this.label214.TabIndex = 140;
            this.label214.Text = "Last Payment Date:";
            // 
            // label215
            // 
            this.label215.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label215.Location = new System.Drawing.Point(582, 416);
            this.label215.Name = "label215";
            this.label215.Size = new System.Drawing.Size(104, 23);
            this.label215.TabIndex = 139;
            this.label215.Text = "1st Payment Date:";
            // 
            // label216
            // 
            this.label216.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label216.Location = new System.Drawing.Point(510, 416);
            this.label216.Name = "label216";
            this.label216.Size = new System.Drawing.Size(72, 23);
            this.label216.TabIndex = 138;
            this.label216.Text = "Points ($):";
            // 
            // label217
            // 
            this.label217.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label217.Location = new System.Drawing.Point(438, 416);
            this.label217.Name = "label217";
            this.label217.Size = new System.Drawing.Size(72, 23);
            this.label217.TabIndex = 137;
            this.label217.Text = "Points (%):";
            // 
            // label218
            // 
            this.label218.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label218.Location = new System.Drawing.Point(350, 416);
            this.label218.Name = "label218";
            this.label218.Size = new System.Drawing.Size(88, 23);
            this.label218.TabIndex = 136;
            this.label218.Text = "Monthly Pymnt:";
            // 
            // label219
            // 
            this.label219.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label219.Location = new System.Drawing.Point(286, 416);
            this.label219.Name = "label219";
            this.label219.Size = new System.Drawing.Size(64, 23);
            this.label219.TabIndex = 135;
            this.label219.Text = "Rate (%)";
            // 
            // label220
            // 
            this.label220.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label220.Location = new System.Drawing.Point(190, 416);
            this.label220.Name = "label220";
            this.label220.Size = new System.Drawing.Size(96, 23);
            this.label220.TabIndex = 134;
            this.label220.Text = "Amort Term (Yrs):";
            // 
            // label221
            // 
            this.label221.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label221.Location = new System.Drawing.Point(118, 416);
            this.label221.Name = "label221";
            this.label221.Size = new System.Drawing.Size(72, 23);
            this.label221.TabIndex = 133;
            this.label221.Text = "Amount ($):";
            // 
            // label222
            // 
            this.label222.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label222.Location = new System.Drawing.Point(80, 416);
            this.label222.Name = "label222";
            this.label222.Size = new System.Drawing.Size(48, 23);
            this.label222.TabIndex = 132;
            this.label222.Text = "% LTV:";
            // 
            // label223
            // 
            this.label223.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label223.Location = new System.Drawing.Point(6, 416);
            this.label223.Name = "label223";
            this.label223.Size = new System.Drawing.Size(40, 23);
            this.label223.TabIndex = 131;
            this.label223.Text = "Type:";
            // 
            // label224
            // 
            this.label224.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label224.Location = new System.Drawing.Point(6, 392);
            this.label224.Name = "label224";
            this.label224.Size = new System.Drawing.Size(216, 23);
            this.label224.TabIndex = 130;
            this.label224.Text = "3rd LOAN:";
            // 
            // _L_2L_RA_LifeTimeCap
            // 
            this._L_2L_RA_LifeTimeCap.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._L_2L_RA_LifeTimeCap.Location = new System.Drawing.Point(318, 360);
            this._L_2L_RA_LifeTimeCap.Name = "_L_2L_RA_LifeTimeCap";
            this._L_2L_RA_LifeTimeCap.ReadOnly = true;
            this._L_2L_RA_LifeTimeCap.Size = new System.Drawing.Size(60, 20);
            this._L_2L_RA_LifeTimeCap.TabIndex = 103;
            this._L_2L_RA_LifeTimeCap.TabStop = false;
            // 
            // _L_2L_RA_AnnualCap
            // 
            this._L_2L_RA_AnnualCap.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._L_2L_RA_AnnualCap.Location = new System.Drawing.Point(318, 336);
            this._L_2L_RA_AnnualCap.Name = "_L_2L_RA_AnnualCap";
            this._L_2L_RA_AnnualCap.ReadOnly = true;
            this._L_2L_RA_AnnualCap.Size = new System.Drawing.Size(60, 20);
            this._L_2L_RA_AnnualCap.TabIndex = 101;
            this._L_2L_RA_AnnualCap.TabStop = false;
            // 
            // label169
            // 
            this.label169.Location = new System.Drawing.Point(222, 360);
            this.label169.Name = "label169";
            this.label169.Size = new System.Drawing.Size(96, 23);
            this.label169.TabIndex = 127;
            this.label169.Text = "Lifetime Cap (%)";
            this.label169.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label170
            // 
            this.label170.Location = new System.Drawing.Point(222, 336);
            this.label170.Name = "label170";
            this.label170.Size = new System.Drawing.Size(96, 23);
            this.label170.TabIndex = 126;
            this.label170.Text = "Annual Cap (%):";
            this.label170.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // _L_2L_RA_InitialRateCap
            // 
            this._L_2L_RA_InitialRateCap.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._L_2L_RA_InitialRateCap.Location = new System.Drawing.Point(142, 360);
            this._L_2L_RA_InitialRateCap.Name = "_L_2L_RA_InitialRateCap";
            this._L_2L_RA_InitialRateCap.ReadOnly = true;
            this._L_2L_RA_InitialRateCap.Size = new System.Drawing.Size(60, 20);
            this._L_2L_RA_InitialRateCap.TabIndex = 102;
            this._L_2L_RA_InitialRateCap.TabStop = false;
            // 
            // _L_2L_RA_InitFixedPeriod
            // 
            this._L_2L_RA_InitFixedPeriod.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._L_2L_RA_InitFixedPeriod.Location = new System.Drawing.Point(142, 336);
            this._L_2L_RA_InitFixedPeriod.Name = "_L_2L_RA_InitFixedPeriod";
            this._L_2L_RA_InitFixedPeriod.ReadOnly = true;
            this._L_2L_RA_InitFixedPeriod.Size = new System.Drawing.Size(60, 20);
            this._L_2L_RA_InitFixedPeriod.TabIndex = 100;
            this._L_2L_RA_InitFixedPeriod.TabStop = false;
            // 
            // label171
            // 
            this.label171.Location = new System.Drawing.Point(14, 360);
            this.label171.Name = "label171";
            this.label171.Size = new System.Drawing.Size(128, 23);
            this.label171.TabIndex = 123;
            this.label171.Text = "Initial Rate Cap (%)";
            this.label171.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label172
            // 
            this.label172.Location = new System.Drawing.Point(14, 336);
            this.label172.Name = "label172";
            this.label172.Size = new System.Drawing.Size(128, 23);
            this.label172.TabIndex = 122;
            this.label172.Text = "Initial Fixed Period (Yrs):";
            this.label172.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label173
            // 
            this.label173.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label173.Location = new System.Drawing.Point(6, 312);
            this.label173.Name = "label173";
            this.label173.Size = new System.Drawing.Size(258, 23);
            this.label173.TabIndex = 121;
            this.label173.Text = "2nd Loan Rate Adjustments (if Adjustable):";
            // 
            // label174
            // 
            this.label174.Location = new System.Drawing.Point(843, 315);
            this.label174.Name = "label174";
            this.label174.Size = new System.Drawing.Size(48, 23);
            this.label174.TabIndex = 120;
            this.label174.Text = "Year 10";
            this.label174.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // label175
            // 
            this.label175.Location = new System.Drawing.Point(806, 315);
            this.label175.Name = "label175";
            this.label175.Size = new System.Drawing.Size(40, 23);
            this.label175.TabIndex = 119;
            this.label175.Text = "Year 9";
            this.label175.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // label176
            // 
            this.label176.Location = new System.Drawing.Point(766, 315);
            this.label176.Name = "label176";
            this.label176.Size = new System.Drawing.Size(40, 23);
            this.label176.TabIndex = 118;
            this.label176.Text = "Year 8";
            this.label176.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // label177
            // 
            this.label177.Location = new System.Drawing.Point(726, 315);
            this.label177.Name = "label177";
            this.label177.Size = new System.Drawing.Size(40, 23);
            this.label177.TabIndex = 117;
            this.label177.Text = "Year 7";
            this.label177.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // label178
            // 
            this.label178.Location = new System.Drawing.Point(686, 315);
            this.label178.Name = "label178";
            this.label178.Size = new System.Drawing.Size(40, 23);
            this.label178.TabIndex = 116;
            this.label178.Text = "Year 6";
            this.label178.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // label179
            // 
            this.label179.Location = new System.Drawing.Point(646, 315);
            this.label179.Name = "label179";
            this.label179.Size = new System.Drawing.Size(40, 23);
            this.label179.TabIndex = 115;
            this.label179.Text = "Year 5";
            this.label179.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // label180
            // 
            this.label180.Location = new System.Drawing.Point(606, 315);
            this.label180.Name = "label180";
            this.label180.Size = new System.Drawing.Size(40, 23);
            this.label180.TabIndex = 114;
            this.label180.Text = "Year 4";
            this.label180.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // label181
            // 
            this.label181.Location = new System.Drawing.Point(566, 315);
            this.label181.Name = "label181";
            this.label181.Size = new System.Drawing.Size(40, 23);
            this.label181.TabIndex = 113;
            this.label181.Text = "Year 3";
            this.label181.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // label182
            // 
            this.label182.Location = new System.Drawing.Point(526, 315);
            this.label182.Name = "label182";
            this.label182.Size = new System.Drawing.Size(40, 23);
            this.label182.TabIndex = 112;
            this.label182.Text = "Year 2";
            this.label182.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // label183
            // 
            this.label183.Location = new System.Drawing.Point(486, 315);
            this.label183.Name = "label183";
            this.label183.Size = new System.Drawing.Size(40, 23);
            this.label183.TabIndex = 111;
            this.label183.Text = "Year 1";
            this.label183.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // label184
            // 
            this.label184.Location = new System.Drawing.Point(422, 339);
            this.label184.Name = "label184";
            this.label184.Size = new System.Drawing.Size(56, 23);
            this.label184.TabIndex = 110;
            this.label184.Text = "Rate (%):";
            this.label184.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // _L_2L_RA_RateYear10
            // 
            this._L_2L_RA_RateYear10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._L_2L_RA_RateYear10.Location = new System.Drawing.Point(846, 339);
            this._L_2L_RA_RateYear10.Name = "_L_2L_RA_RateYear10";
            this._L_2L_RA_RateYear10.ReadOnly = true;
            this._L_2L_RA_RateYear10.Size = new System.Drawing.Size(40, 20);
            this._L_2L_RA_RateYear10.TabIndex = 113;
            this._L_2L_RA_RateYear10.TabStop = false;
            // 
            // _L_2L_RA_RateYear9
            // 
            this._L_2L_RA_RateYear9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._L_2L_RA_RateYear9.Location = new System.Drawing.Point(806, 339);
            this._L_2L_RA_RateYear9.Name = "_L_2L_RA_RateYear9";
            this._L_2L_RA_RateYear9.ReadOnly = true;
            this._L_2L_RA_RateYear9.Size = new System.Drawing.Size(40, 20);
            this._L_2L_RA_RateYear9.TabIndex = 112;
            this._L_2L_RA_RateYear9.TabStop = false;
            // 
            // _L_2L_RA_RateYear8
            // 
            this._L_2L_RA_RateYear8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._L_2L_RA_RateYear8.Location = new System.Drawing.Point(766, 339);
            this._L_2L_RA_RateYear8.Name = "_L_2L_RA_RateYear8";
            this._L_2L_RA_RateYear8.ReadOnly = true;
            this._L_2L_RA_RateYear8.Size = new System.Drawing.Size(40, 20);
            this._L_2L_RA_RateYear8.TabIndex = 111;
            this._L_2L_RA_RateYear8.TabStop = false;
            // 
            // _L_2L_RA_RateYear7
            // 
            this._L_2L_RA_RateYear7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._L_2L_RA_RateYear7.Location = new System.Drawing.Point(726, 339);
            this._L_2L_RA_RateYear7.Name = "_L_2L_RA_RateYear7";
            this._L_2L_RA_RateYear7.ReadOnly = true;
            this._L_2L_RA_RateYear7.Size = new System.Drawing.Size(40, 20);
            this._L_2L_RA_RateYear7.TabIndex = 110;
            this._L_2L_RA_RateYear7.TabStop = false;
            // 
            // _L_2L_RA_RateYear6
            // 
            this._L_2L_RA_RateYear6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._L_2L_RA_RateYear6.Location = new System.Drawing.Point(686, 339);
            this._L_2L_RA_RateYear6.Name = "_L_2L_RA_RateYear6";
            this._L_2L_RA_RateYear6.ReadOnly = true;
            this._L_2L_RA_RateYear6.Size = new System.Drawing.Size(40, 20);
            this._L_2L_RA_RateYear6.TabIndex = 109;
            this._L_2L_RA_RateYear6.TabStop = false;
            // 
            // _L_2L_RA_RateYear5
            // 
            this._L_2L_RA_RateYear5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._L_2L_RA_RateYear5.Location = new System.Drawing.Point(646, 339);
            this._L_2L_RA_RateYear5.Name = "_L_2L_RA_RateYear5";
            this._L_2L_RA_RateYear5.ReadOnly = true;
            this._L_2L_RA_RateYear5.Size = new System.Drawing.Size(40, 20);
            this._L_2L_RA_RateYear5.TabIndex = 108;
            this._L_2L_RA_RateYear5.TabStop = false;
            // 
            // _L_2L_RA_RateYear4
            // 
            this._L_2L_RA_RateYear4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._L_2L_RA_RateYear4.Location = new System.Drawing.Point(606, 339);
            this._L_2L_RA_RateYear4.Name = "_L_2L_RA_RateYear4";
            this._L_2L_RA_RateYear4.ReadOnly = true;
            this._L_2L_RA_RateYear4.Size = new System.Drawing.Size(40, 20);
            this._L_2L_RA_RateYear4.TabIndex = 107;
            this._L_2L_RA_RateYear4.TabStop = false;
            // 
            // _L_2L_RA_RateYear3
            // 
            this._L_2L_RA_RateYear3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._L_2L_RA_RateYear3.Location = new System.Drawing.Point(566, 339);
            this._L_2L_RA_RateYear3.Name = "_L_2L_RA_RateYear3";
            this._L_2L_RA_RateYear3.ReadOnly = true;
            this._L_2L_RA_RateYear3.Size = new System.Drawing.Size(40, 20);
            this._L_2L_RA_RateYear3.TabIndex = 106;
            this._L_2L_RA_RateYear3.TabStop = false;
            // 
            // _L_2L_RA_RateYear2
            // 
            this._L_2L_RA_RateYear2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._L_2L_RA_RateYear2.Location = new System.Drawing.Point(526, 339);
            this._L_2L_RA_RateYear2.Name = "_L_2L_RA_RateYear2";
            this._L_2L_RA_RateYear2.ReadOnly = true;
            this._L_2L_RA_RateYear2.Size = new System.Drawing.Size(40, 20);
            this._L_2L_RA_RateYear2.TabIndex = 105;
            this._L_2L_RA_RateYear2.TabStop = false;
            // 
            // _L_2L_RA_RateYear1
            // 
            this._L_2L_RA_RateYear1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._L_2L_RA_RateYear1.Location = new System.Drawing.Point(486, 339);
            this._L_2L_RA_RateYear1.Name = "_L_2L_RA_RateYear1";
            this._L_2L_RA_RateYear1.ReadOnly = true;
            this._L_2L_RA_RateYear1.Size = new System.Drawing.Size(40, 20);
            this._L_2L_RA_RateYear1.TabIndex = 104;
            this._L_2L_RA_RateYear1.TabStop = false;
            // 
            // _L_2L_Lender
            // 
            this._L_2L_Lender.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._L_2L_Lender.Location = new System.Drawing.Point(790, 291);
            this._L_2L_Lender.Name = "_L_2L_Lender";
            this._L_2L_Lender.Size = new System.Drawing.Size(96, 20);
            this._L_2L_Lender.TabIndex = 99;
            // 
            // _L_2L_LastPaymentDate
            // 
            this._L_2L_LastPaymentDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._L_2L_LastPaymentDate.Location = new System.Drawing.Point(686, 291);
            this._L_2L_LastPaymentDate.Name = "_L_2L_LastPaymentDate";
            this._L_2L_LastPaymentDate.Size = new System.Drawing.Size(104, 20);
            this._L_2L_LastPaymentDate.TabIndex = 98;
            // 
            // _L_2L_FirstPaymentDate
            // 
            this._L_2L_FirstPaymentDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._L_2L_FirstPaymentDate.Location = new System.Drawing.Point(582, 291);
            this._L_2L_FirstPaymentDate.Name = "_L_2L_FirstPaymentDate";
            this._L_2L_FirstPaymentDate.Size = new System.Drawing.Size(104, 20);
            this._L_2L_FirstPaymentDate.TabIndex = 97;
            // 
            // _L_2L_Points
            // 
            this._L_2L_Points.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._L_2L_Points.Location = new System.Drawing.Point(510, 291);
            this._L_2L_Points.Name = "_L_2L_Points";
            this._L_2L_Points.Size = new System.Drawing.Size(72, 20);
            this._L_2L_Points.TabIndex = 96;
            // 
            // _L_2L_PointsPerc
            // 
            this._L_2L_PointsPerc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._L_2L_PointsPerc.Location = new System.Drawing.Point(438, 291);
            this._L_2L_PointsPerc.Name = "_L_2L_PointsPerc";
            this._L_2L_PointsPerc.Size = new System.Drawing.Size(72, 20);
            this._L_2L_PointsPerc.TabIndex = 95;
            // 
            // _L_2L_MonthlyPayment
            // 
            this._L_2L_MonthlyPayment.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._L_2L_MonthlyPayment.Location = new System.Drawing.Point(350, 291);
            this._L_2L_MonthlyPayment.Name = "_L_2L_MonthlyPayment";
            this._L_2L_MonthlyPayment.Size = new System.Drawing.Size(88, 20);
            this._L_2L_MonthlyPayment.TabIndex = 94;
            // 
            // _L_2L_Rate
            // 
            this._L_2L_Rate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._L_2L_Rate.Location = new System.Drawing.Point(286, 291);
            this._L_2L_Rate.Name = "_L_2L_Rate";
            this._L_2L_Rate.Size = new System.Drawing.Size(64, 20);
            this._L_2L_Rate.TabIndex = 93;
            // 
            // _L_2L_AmortTerm
            // 
            this._L_2L_AmortTerm.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._L_2L_AmortTerm.Location = new System.Drawing.Point(190, 291);
            this._L_2L_AmortTerm.Name = "_L_2L_AmortTerm";
            this._L_2L_AmortTerm.ReadOnly = true;
            this._L_2L_AmortTerm.Size = new System.Drawing.Size(96, 20);
            this._L_2L_AmortTerm.TabIndex = 92;
            this._L_2L_AmortTerm.TabStop = false;
            // 
            // _L_2L_Amount
            // 
            this._L_2L_Amount.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._L_2L_Amount.Location = new System.Drawing.Point(120, 291);
            this._L_2L_Amount.Name = "_L_2L_Amount";
            this._L_2L_Amount.Size = new System.Drawing.Size(72, 20);
            this._L_2L_Amount.TabIndex = 91;
            // 
            // _L_2L_LTVPerc
            // 
            this._L_2L_LTVPerc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._L_2L_LTVPerc.Location = new System.Drawing.Point(80, 291);
            this._L_2L_LTVPerc.Name = "_L_2L_LTVPerc";
            this._L_2L_LTVPerc.Size = new System.Drawing.Size(40, 20);
            this._L_2L_LTVPerc.TabIndex = 90;
            // 
            // _L_2L_Type
            // 
            this._L_2L_Type.Items.AddRange(new object[] {
            "Fixed",
            "Adjustable",
            "IO"});
            this._L_2L_Type.Location = new System.Drawing.Point(6, 291);
            this._L_2L_Type.Name = "_L_2L_Type";
            this._L_2L_Type.Size = new System.Drawing.Size(74, 21);
            this._L_2L_Type.TabIndex = 89;
            // 
            // label185
            // 
            this.label185.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label185.Location = new System.Drawing.Point(790, 275);
            this.label185.Name = "label185";
            this.label185.Size = new System.Drawing.Size(64, 23);
            this.label185.TabIndex = 88;
            this.label185.Text = "Lender:";
            // 
            // label186
            // 
            this.label186.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label186.Location = new System.Drawing.Point(686, 275);
            this.label186.Name = "label186";
            this.label186.Size = new System.Drawing.Size(104, 23);
            this.label186.TabIndex = 87;
            this.label186.Text = "Last Payment Date:";
            // 
            // label187
            // 
            this.label187.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label187.Location = new System.Drawing.Point(582, 275);
            this.label187.Name = "label187";
            this.label187.Size = new System.Drawing.Size(104, 23);
            this.label187.TabIndex = 86;
            this.label187.Text = "1st Payment Date:";
            // 
            // label188
            // 
            this.label188.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label188.Location = new System.Drawing.Point(510, 275);
            this.label188.Name = "label188";
            this.label188.Size = new System.Drawing.Size(72, 23);
            this.label188.TabIndex = 85;
            this.label188.Text = "Points ($):";
            // 
            // label189
            // 
            this.label189.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label189.Location = new System.Drawing.Point(438, 275);
            this.label189.Name = "label189";
            this.label189.Size = new System.Drawing.Size(72, 23);
            this.label189.TabIndex = 84;
            this.label189.Text = "Points (%):";
            // 
            // label190
            // 
            this.label190.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label190.Location = new System.Drawing.Point(350, 275);
            this.label190.Name = "label190";
            this.label190.Size = new System.Drawing.Size(88, 23);
            this.label190.TabIndex = 83;
            this.label190.Text = "Monthly Pymnt:";
            // 
            // label191
            // 
            this.label191.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label191.Location = new System.Drawing.Point(286, 275);
            this.label191.Name = "label191";
            this.label191.Size = new System.Drawing.Size(64, 23);
            this.label191.TabIndex = 82;
            this.label191.Text = "Rate (%)";
            // 
            // label192
            // 
            this.label192.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label192.Location = new System.Drawing.Point(190, 275);
            this.label192.Name = "label192";
            this.label192.Size = new System.Drawing.Size(96, 23);
            this.label192.TabIndex = 81;
            this.label192.Text = "Amort Term (Yrs):";
            // 
            // label193
            // 
            this.label193.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label193.Location = new System.Drawing.Point(118, 275);
            this.label193.Name = "label193";
            this.label193.Size = new System.Drawing.Size(72, 23);
            this.label193.TabIndex = 80;
            this.label193.Text = "Amount ($):";
            // 
            // label194
            // 
            this.label194.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label194.Location = new System.Drawing.Point(80, 275);
            this.label194.Name = "label194";
            this.label194.Size = new System.Drawing.Size(48, 23);
            this.label194.TabIndex = 79;
            this.label194.Text = "% LTV:";
            // 
            // label195
            // 
            this.label195.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label195.Location = new System.Drawing.Point(6, 275);
            this.label195.Name = "label195";
            this.label195.Size = new System.Drawing.Size(40, 23);
            this.label195.TabIndex = 78;
            this.label195.Text = "Type:";
            // 
            // label196
            // 
            this.label196.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label196.Location = new System.Drawing.Point(6, 248);
            this.label196.Name = "label196";
            this.label196.Size = new System.Drawing.Size(216, 23);
            this.label196.TabIndex = 77;
            this.label196.Text = "2nd LOAN:";
            // 
            // _L_1L_RA_LifeTimeCap
            // 
            this._L_1L_RA_LifeTimeCap.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._L_1L_RA_LifeTimeCap.Location = new System.Drawing.Point(320, 216);
            this._L_1L_RA_LifeTimeCap.Name = "_L_1L_RA_LifeTimeCap";
            this._L_1L_RA_LifeTimeCap.ReadOnly = true;
            this._L_1L_RA_LifeTimeCap.Size = new System.Drawing.Size(60, 20);
            this._L_1L_RA_LifeTimeCap.TabIndex = 50;
            this._L_1L_RA_LifeTimeCap.TabStop = false;
            // 
            // _L_1L_RA_AnnualCap
            // 
            this._L_1L_RA_AnnualCap.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._L_1L_RA_AnnualCap.Location = new System.Drawing.Point(320, 192);
            this._L_1L_RA_AnnualCap.Name = "_L_1L_RA_AnnualCap";
            this._L_1L_RA_AnnualCap.ReadOnly = true;
            this._L_1L_RA_AnnualCap.Size = new System.Drawing.Size(60, 20);
            this._L_1L_RA_AnnualCap.TabIndex = 48;
            this._L_1L_RA_AnnualCap.TabStop = false;
            // 
            // label167
            // 
            this.label167.Location = new System.Drawing.Point(224, 216);
            this.label167.Name = "label167";
            this.label167.Size = new System.Drawing.Size(96, 23);
            this.label167.TabIndex = 74;
            this.label167.Text = "Lifetime Cap (%)";
            this.label167.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label168
            // 
            this.label168.Location = new System.Drawing.Point(224, 192);
            this.label168.Name = "label168";
            this.label168.Size = new System.Drawing.Size(96, 23);
            this.label168.TabIndex = 73;
            this.label168.Text = "Annual Cap (%):";
            this.label168.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // _L_1L_RA_InitialRateCap
            // 
            this._L_1L_RA_InitialRateCap.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._L_1L_RA_InitialRateCap.Location = new System.Drawing.Point(144, 216);
            this._L_1L_RA_InitialRateCap.Name = "_L_1L_RA_InitialRateCap";
            this._L_1L_RA_InitialRateCap.ReadOnly = true;
            this._L_1L_RA_InitialRateCap.Size = new System.Drawing.Size(60, 20);
            this._L_1L_RA_InitialRateCap.TabIndex = 49;
            this._L_1L_RA_InitialRateCap.TabStop = false;
            // 
            // _L_1L_RA_InitFixedPeriod
            // 
            this._L_1L_RA_InitFixedPeriod.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._L_1L_RA_InitFixedPeriod.Location = new System.Drawing.Point(144, 192);
            this._L_1L_RA_InitFixedPeriod.Name = "_L_1L_RA_InitFixedPeriod";
            this._L_1L_RA_InitFixedPeriod.ReadOnly = true;
            this._L_1L_RA_InitFixedPeriod.Size = new System.Drawing.Size(60, 20);
            this._L_1L_RA_InitFixedPeriod.TabIndex = 47;
            this._L_1L_RA_InitFixedPeriod.TabStop = false;
            // 
            // label166
            // 
            this.label166.Location = new System.Drawing.Point(16, 216);
            this.label166.Name = "label166";
            this.label166.Size = new System.Drawing.Size(128, 23);
            this.label166.TabIndex = 70;
            this.label166.Text = "Initial Rate Cap (%)";
            this.label166.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label165
            // 
            this.label165.Location = new System.Drawing.Point(16, 192);
            this.label165.Name = "label165";
            this.label165.Size = new System.Drawing.Size(128, 23);
            this.label165.TabIndex = 69;
            this.label165.Text = "Initial Fixed Period (Yrs):";
            this.label165.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label164
            // 
            this.label164.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label164.Location = new System.Drawing.Point(8, 168);
            this.label164.Name = "label164";
            this.label164.Size = new System.Drawing.Size(240, 23);
            this.label164.TabIndex = 68;
            this.label164.Text = "1st Loan Rate Adjustments (if Adjustable):";
            // 
            // label153
            // 
            this.label153.Location = new System.Drawing.Point(845, 171);
            this.label153.Name = "label153";
            this.label153.Size = new System.Drawing.Size(48, 23);
            this.label153.TabIndex = 67;
            this.label153.Text = "Year 10";
            this.label153.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // label154
            // 
            this.label154.Location = new System.Drawing.Point(808, 171);
            this.label154.Name = "label154";
            this.label154.Size = new System.Drawing.Size(40, 23);
            this.label154.TabIndex = 66;
            this.label154.Text = "Year 9";
            this.label154.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // label155
            // 
            this.label155.Location = new System.Drawing.Point(768, 171);
            this.label155.Name = "label155";
            this.label155.Size = new System.Drawing.Size(40, 23);
            this.label155.TabIndex = 65;
            this.label155.Text = "Year 8";
            this.label155.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // label156
            // 
            this.label156.Location = new System.Drawing.Point(728, 171);
            this.label156.Name = "label156";
            this.label156.Size = new System.Drawing.Size(40, 23);
            this.label156.TabIndex = 64;
            this.label156.Text = "Year 7";
            this.label156.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // label157
            // 
            this.label157.Location = new System.Drawing.Point(688, 171);
            this.label157.Name = "label157";
            this.label157.Size = new System.Drawing.Size(40, 23);
            this.label157.TabIndex = 63;
            this.label157.Text = "Year 6";
            this.label157.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // label158
            // 
            this.label158.Location = new System.Drawing.Point(648, 171);
            this.label158.Name = "label158";
            this.label158.Size = new System.Drawing.Size(40, 23);
            this.label158.TabIndex = 62;
            this.label158.Text = "Year 5";
            this.label158.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // label159
            // 
            this.label159.Location = new System.Drawing.Point(608, 171);
            this.label159.Name = "label159";
            this.label159.Size = new System.Drawing.Size(40, 23);
            this.label159.TabIndex = 61;
            this.label159.Text = "Year 4";
            this.label159.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // label160
            // 
            this.label160.Location = new System.Drawing.Point(568, 171);
            this.label160.Name = "label160";
            this.label160.Size = new System.Drawing.Size(40, 23);
            this.label160.TabIndex = 60;
            this.label160.Text = "Year 3";
            this.label160.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // label161
            // 
            this.label161.Location = new System.Drawing.Point(528, 171);
            this.label161.Name = "label161";
            this.label161.Size = new System.Drawing.Size(40, 23);
            this.label161.TabIndex = 59;
            this.label161.Text = "Year 2";
            this.label161.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // label162
            // 
            this.label162.Location = new System.Drawing.Point(488, 171);
            this.label162.Name = "label162";
            this.label162.Size = new System.Drawing.Size(40, 23);
            this.label162.TabIndex = 58;
            this.label162.Text = "Year 1";
            this.label162.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // label163
            // 
            this.label163.Location = new System.Drawing.Point(424, 195);
            this.label163.Name = "label163";
            this.label163.Size = new System.Drawing.Size(56, 23);
            this.label163.TabIndex = 57;
            this.label163.Text = "Rate (%):";
            this.label163.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // _L_1L_RA_RateYear10
            // 
            this._L_1L_RA_RateYear10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._L_1L_RA_RateYear10.Location = new System.Drawing.Point(848, 195);
            this._L_1L_RA_RateYear10.Name = "_L_1L_RA_RateYear10";
            this._L_1L_RA_RateYear10.ReadOnly = true;
            this._L_1L_RA_RateYear10.Size = new System.Drawing.Size(40, 20);
            this._L_1L_RA_RateYear10.TabIndex = 60;
            this._L_1L_RA_RateYear10.TabStop = false;
            // 
            // _L_1L_RA_RateYear9
            // 
            this._L_1L_RA_RateYear9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._L_1L_RA_RateYear9.Location = new System.Drawing.Point(808, 195);
            this._L_1L_RA_RateYear9.Name = "_L_1L_RA_RateYear9";
            this._L_1L_RA_RateYear9.ReadOnly = true;
            this._L_1L_RA_RateYear9.Size = new System.Drawing.Size(40, 20);
            this._L_1L_RA_RateYear9.TabIndex = 59;
            this._L_1L_RA_RateYear9.TabStop = false;
            // 
            // _L_1L_RA_RateYear8
            // 
            this._L_1L_RA_RateYear8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._L_1L_RA_RateYear8.Location = new System.Drawing.Point(768, 195);
            this._L_1L_RA_RateYear8.Name = "_L_1L_RA_RateYear8";
            this._L_1L_RA_RateYear8.ReadOnly = true;
            this._L_1L_RA_RateYear8.Size = new System.Drawing.Size(40, 20);
            this._L_1L_RA_RateYear8.TabIndex = 58;
            this._L_1L_RA_RateYear8.TabStop = false;
            // 
            // _L_1L_RA_RateYear7
            // 
            this._L_1L_RA_RateYear7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._L_1L_RA_RateYear7.Location = new System.Drawing.Point(728, 195);
            this._L_1L_RA_RateYear7.Name = "_L_1L_RA_RateYear7";
            this._L_1L_RA_RateYear7.ReadOnly = true;
            this._L_1L_RA_RateYear7.Size = new System.Drawing.Size(40, 20);
            this._L_1L_RA_RateYear7.TabIndex = 57;
            this._L_1L_RA_RateYear7.TabStop = false;
            // 
            // _L_1L_RA_RateYear6
            // 
            this._L_1L_RA_RateYear6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._L_1L_RA_RateYear6.Location = new System.Drawing.Point(688, 195);
            this._L_1L_RA_RateYear6.Name = "_L_1L_RA_RateYear6";
            this._L_1L_RA_RateYear6.ReadOnly = true;
            this._L_1L_RA_RateYear6.Size = new System.Drawing.Size(40, 20);
            this._L_1L_RA_RateYear6.TabIndex = 56;
            this._L_1L_RA_RateYear6.TabStop = false;
            // 
            // _L_1L_RA_RateYear5
            // 
            this._L_1L_RA_RateYear5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._L_1L_RA_RateYear5.Location = new System.Drawing.Point(648, 195);
            this._L_1L_RA_RateYear5.Name = "_L_1L_RA_RateYear5";
            this._L_1L_RA_RateYear5.ReadOnly = true;
            this._L_1L_RA_RateYear5.Size = new System.Drawing.Size(40, 20);
            this._L_1L_RA_RateYear5.TabIndex = 55;
            this._L_1L_RA_RateYear5.TabStop = false;
            // 
            // _L_1L_RA_RateYear4
            // 
            this._L_1L_RA_RateYear4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._L_1L_RA_RateYear4.Location = new System.Drawing.Point(608, 195);
            this._L_1L_RA_RateYear4.Name = "_L_1L_RA_RateYear4";
            this._L_1L_RA_RateYear4.ReadOnly = true;
            this._L_1L_RA_RateYear4.Size = new System.Drawing.Size(40, 20);
            this._L_1L_RA_RateYear4.TabIndex = 54;
            this._L_1L_RA_RateYear4.TabStop = false;
            // 
            // _L_1L_RA_RateYear3
            // 
            this._L_1L_RA_RateYear3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._L_1L_RA_RateYear3.Location = new System.Drawing.Point(568, 195);
            this._L_1L_RA_RateYear3.Name = "_L_1L_RA_RateYear3";
            this._L_1L_RA_RateYear3.ReadOnly = true;
            this._L_1L_RA_RateYear3.Size = new System.Drawing.Size(40, 20);
            this._L_1L_RA_RateYear3.TabIndex = 53;
            this._L_1L_RA_RateYear3.TabStop = false;
            // 
            // _L_1L_RA_RateYear2
            // 
            this._L_1L_RA_RateYear2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._L_1L_RA_RateYear2.Location = new System.Drawing.Point(528, 195);
            this._L_1L_RA_RateYear2.Name = "_L_1L_RA_RateYear2";
            this._L_1L_RA_RateYear2.ReadOnly = true;
            this._L_1L_RA_RateYear2.Size = new System.Drawing.Size(40, 20);
            this._L_1L_RA_RateYear2.TabIndex = 52;
            this._L_1L_RA_RateYear2.TabStop = false;
            // 
            // _L_1L_RA_RateYear1
            // 
            this._L_1L_RA_RateYear1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._L_1L_RA_RateYear1.Location = new System.Drawing.Point(488, 195);
            this._L_1L_RA_RateYear1.Name = "_L_1L_RA_RateYear1";
            this._L_1L_RA_RateYear1.ReadOnly = true;
            this._L_1L_RA_RateYear1.Size = new System.Drawing.Size(40, 20);
            this._L_1L_RA_RateYear1.TabIndex = 51;
            this._L_1L_RA_RateYear1.TabStop = false;
            // 
            // _L_1L_Lender
            // 
            this._L_1L_Lender.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._L_1L_Lender.Location = new System.Drawing.Point(792, 147);
            this._L_1L_Lender.Name = "_L_1L_Lender";
            this._L_1L_Lender.Size = new System.Drawing.Size(96, 20);
            this._L_1L_Lender.TabIndex = 46;
            // 
            // _L_1L_LastPaymentDate
            // 
            this._L_1L_LastPaymentDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._L_1L_LastPaymentDate.Location = new System.Drawing.Point(688, 147);
            this._L_1L_LastPaymentDate.Name = "_L_1L_LastPaymentDate";
            this._L_1L_LastPaymentDate.Size = new System.Drawing.Size(104, 20);
            this._L_1L_LastPaymentDate.TabIndex = 45;
            // 
            // _L_1L_FirstPaymentDate
            // 
            this._L_1L_FirstPaymentDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._L_1L_FirstPaymentDate.Location = new System.Drawing.Point(584, 147);
            this._L_1L_FirstPaymentDate.Name = "_L_1L_FirstPaymentDate";
            this._L_1L_FirstPaymentDate.Size = new System.Drawing.Size(104, 20);
            this._L_1L_FirstPaymentDate.TabIndex = 44;
            // 
            // _L_1L_Points
            // 
            this._L_1L_Points.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._L_1L_Points.Location = new System.Drawing.Point(512, 147);
            this._L_1L_Points.Name = "_L_1L_Points";
            this._L_1L_Points.Size = new System.Drawing.Size(72, 20);
            this._L_1L_Points.TabIndex = 43;
            // 
            // _L_1L_PointsPerc
            // 
            this._L_1L_PointsPerc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._L_1L_PointsPerc.Location = new System.Drawing.Point(440, 147);
            this._L_1L_PointsPerc.Name = "_L_1L_PointsPerc";
            this._L_1L_PointsPerc.Size = new System.Drawing.Size(72, 20);
            this._L_1L_PointsPerc.TabIndex = 42;
            // 
            // _L_1L_MonthlyPayment
            // 
            this._L_1L_MonthlyPayment.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._L_1L_MonthlyPayment.Location = new System.Drawing.Point(352, 147);
            this._L_1L_MonthlyPayment.Name = "_L_1L_MonthlyPayment";
            this._L_1L_MonthlyPayment.Size = new System.Drawing.Size(88, 20);
            this._L_1L_MonthlyPayment.TabIndex = 41;
            // 
            // _L_1L_Rate
            // 
            this._L_1L_Rate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._L_1L_Rate.Location = new System.Drawing.Point(288, 147);
            this._L_1L_Rate.Name = "_L_1L_Rate";
            this._L_1L_Rate.Size = new System.Drawing.Size(64, 20);
            this._L_1L_Rate.TabIndex = 40;
            // 
            // _L_1L_AmortTerm
            // 
            this._L_1L_AmortTerm.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._L_1L_AmortTerm.Location = new System.Drawing.Point(192, 147);
            this._L_1L_AmortTerm.Name = "_L_1L_AmortTerm";
            this._L_1L_AmortTerm.ReadOnly = true;
            this._L_1L_AmortTerm.Size = new System.Drawing.Size(96, 20);
            this._L_1L_AmortTerm.TabIndex = 39;
            this._L_1L_AmortTerm.TabStop = false;
            // 
            // _L_1L_Amount
            // 
            this._L_1L_Amount.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._L_1L_Amount.Location = new System.Drawing.Point(120, 147);
            this._L_1L_Amount.Name = "_L_1L_Amount";
            this._L_1L_Amount.Size = new System.Drawing.Size(72, 20);
            this._L_1L_Amount.TabIndex = 38;
            // 
            // _L_1L_LTVPerc
            // 
            this._L_1L_LTVPerc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._L_1L_LTVPerc.Location = new System.Drawing.Point(80, 147);
            this._L_1L_LTVPerc.Name = "_L_1L_LTVPerc";
            this._L_1L_LTVPerc.Size = new System.Drawing.Size(40, 20);
            this._L_1L_LTVPerc.TabIndex = 37;
            // 
            // _L_1L_Type
            // 
            this._L_1L_Type.Items.AddRange(new object[] {
            "Fixed",
            "Adjustable",
            "IO"});
            this._L_1L_Type.Location = new System.Drawing.Point(8, 147);
            this._L_1L_Type.Name = "_L_1L_Type";
            this._L_1L_Type.Size = new System.Drawing.Size(72, 21);
            this._L_1L_Type.TabIndex = 36;
            // 
            // label152
            // 
            this.label152.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label152.Location = new System.Drawing.Point(792, 128);
            this.label152.Name = "label152";
            this.label152.Size = new System.Drawing.Size(64, 23);
            this.label152.TabIndex = 35;
            this.label152.Text = "Lender:";
            // 
            // label151
            // 
            this.label151.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label151.Location = new System.Drawing.Point(688, 128);
            this.label151.Name = "label151";
            this.label151.Size = new System.Drawing.Size(104, 23);
            this.label151.TabIndex = 34;
            this.label151.Text = "Last Payment Date:";
            // 
            // label150
            // 
            this.label150.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label150.Location = new System.Drawing.Point(584, 128);
            this.label150.Name = "label150";
            this.label150.Size = new System.Drawing.Size(104, 23);
            this.label150.TabIndex = 33;
            this.label150.Text = "1st Payment Date:";
            // 
            // label149
            // 
            this.label149.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label149.Location = new System.Drawing.Point(512, 128);
            this.label149.Name = "label149";
            this.label149.Size = new System.Drawing.Size(72, 23);
            this.label149.TabIndex = 32;
            this.label149.Text = "Points ($):";
            // 
            // label148
            // 
            this.label148.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label148.Location = new System.Drawing.Point(440, 128);
            this.label148.Name = "label148";
            this.label148.Size = new System.Drawing.Size(72, 23);
            this.label148.TabIndex = 31;
            this.label148.Text = "Points (%):";
            // 
            // label147
            // 
            this.label147.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label147.Location = new System.Drawing.Point(352, 128);
            this.label147.Name = "label147";
            this.label147.Size = new System.Drawing.Size(88, 23);
            this.label147.TabIndex = 30;
            this.label147.Text = "Monthly Pymnt:";
            // 
            // label146
            // 
            this.label146.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label146.Location = new System.Drawing.Point(288, 128);
            this.label146.Name = "label146";
            this.label146.Size = new System.Drawing.Size(64, 23);
            this.label146.TabIndex = 29;
            this.label146.Text = "Rate (%)";
            // 
            // label145
            // 
            this.label145.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label145.Location = new System.Drawing.Point(192, 128);
            this.label145.Name = "label145";
            this.label145.Size = new System.Drawing.Size(96, 23);
            this.label145.TabIndex = 28;
            this.label145.Text = "Amort Term (Yrs):";
            // 
            // label144
            // 
            this.label144.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label144.Location = new System.Drawing.Point(120, 128);
            this.label144.Name = "label144";
            this.label144.Size = new System.Drawing.Size(72, 23);
            this.label144.TabIndex = 27;
            this.label144.Text = "Amount ($):";
            // 
            // label143
            // 
            this.label143.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label143.Location = new System.Drawing.Point(80, 128);
            this.label143.Name = "label143";
            this.label143.Size = new System.Drawing.Size(48, 23);
            this.label143.TabIndex = 26;
            this.label143.Text = "% LTV:";
            // 
            // label142
            // 
            this.label142.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label142.Location = new System.Drawing.Point(8, 128);
            this.label142.Name = "label142";
            this.label142.Size = new System.Drawing.Size(40, 23);
            this.label142.TabIndex = 25;
            this.label142.Text = "Type:";
            // 
            // label141
            // 
            this.label141.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label141.Location = new System.Drawing.Point(8, 104);
            this.label141.Name = "label141";
            this.label141.Size = new System.Drawing.Size(216, 23);
            this.label141.TabIndex = 24;
            this.label141.Text = "1st LOAN:";
            // 
            // _L_TotalPaymentsAnnual
            // 
            this._L_TotalPaymentsAnnual.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._L_TotalPaymentsAnnual.Location = new System.Drawing.Point(632, 94);
            this._L_TotalPaymentsAnnual.Name = "_L_TotalPaymentsAnnual";
            this._L_TotalPaymentsAnnual.ReadOnly = true;
            this._L_TotalPaymentsAnnual.Size = new System.Drawing.Size(100, 20);
            this._L_TotalPaymentsAnnual.TabIndex = 23;
            // 
            // _L_TotalPaymentsMonthly
            // 
            this._L_TotalPaymentsMonthly.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._L_TotalPaymentsMonthly.Location = new System.Drawing.Point(632, 71);
            this._L_TotalPaymentsMonthly.Name = "_L_TotalPaymentsMonthly";
            this._L_TotalPaymentsMonthly.ReadOnly = true;
            this._L_TotalPaymentsMonthly.Size = new System.Drawing.Size(100, 20);
            this._L_TotalPaymentsMonthly.TabIndex = 22;
            // 
            // label94
            // 
            this.label94.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label94.Location = new System.Drawing.Point(480, 97);
            this.label94.Name = "label94";
            this.label94.Size = new System.Drawing.Size(144, 23);
            this.label94.TabIndex = 21;
            this.label94.Text = "Total Payments (Annual): $";
            this.label94.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // _L_DCR
            // 
            this._L_DCR.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._L_DCR.Location = new System.Drawing.Point(632, 48);
            this._L_DCR.Name = "_L_DCR";
            this._L_DCR.ReadOnly = true;
            this._L_DCR.Size = new System.Drawing.Size(100, 20);
            this._L_DCR.TabIndex = 20;
            this._L_DCR.TabStop = false;
            // 
            // _L_CLTVPerc
            // 
            this._L_CLTVPerc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._L_CLTVPerc.Location = new System.Drawing.Point(520, 48);
            this._L_CLTVPerc.Name = "_L_CLTVPerc";
            this._L_CLTVPerc.ReadOnly = true;
            this._L_CLTVPerc.Size = new System.Drawing.Size(100, 20);
            this._L_CLTVPerc.TabIndex = 19;
            this._L_CLTVPerc.TabStop = false;
            // 
            // _L_BlendedRate
            // 
            this._L_BlendedRate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._L_BlendedRate.Location = new System.Drawing.Point(408, 48);
            this._L_BlendedRate.Name = "_L_BlendedRate";
            this._L_BlendedRate.ReadOnly = true;
            this._L_BlendedRate.Size = new System.Drawing.Size(100, 20);
            this._L_BlendedRate.TabIndex = 18;
            this._L_BlendedRate.TabStop = false;
            // 
            // label82
            // 
            this.label82.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label82.Location = new System.Drawing.Point(472, 75);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(152, 23);
            this.label82.TabIndex = 17;
            this.label82.Text = "Total Payments (Monthly): $";
            this.label82.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label81
            // 
            this.label81.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label81.Location = new System.Drawing.Point(632, 32);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(104, 23);
            this.label81.TabIndex = 16;
            this.label81.Text = "DCR";
            // 
            // label80
            // 
            this.label80.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label80.Location = new System.Drawing.Point(520, 32);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(104, 23);
            this.label80.TabIndex = 15;
            this.label80.Text = "CLTV (%)";
            // 
            // label79
            // 
            this.label79.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label79.Location = new System.Drawing.Point(408, 32);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(104, 23);
            this.label79.TabIndex = 14;
            this.label79.Text = "Blended Rate (%)";
            // 
            // label78
            // 
            this.label78.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label78.Location = new System.Drawing.Point(408, 8);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(216, 23);
            this.label78.TabIndex = 13;
            this.label78.Text = "TOTAL DEBT SERVICE SUMMARY:";
            // 
            // _L_SellerCarryback
            // 
            this._L_SellerCarryback.Items.AddRange(new object[] {
            "Yes",
            "No"});
            this._L_SellerCarryback.Location = new System.Drawing.Point(240, 64);
            this._L_SellerCarryback.Name = "_L_SellerCarryback";
            this._L_SellerCarryback.Size = new System.Drawing.Size(88, 21);
            this._L_SellerCarryback.TabIndex = 12;
            // 
            // _L_Assumable
            // 
            this._L_Assumable.Items.AddRange(new object[] {
            "Yes",
            "No"});
            this._L_Assumable.Location = new System.Drawing.Point(128, 64);
            this._L_Assumable.Name = "_L_Assumable";
            this._L_Assumable.Size = new System.Drawing.Size(88, 21);
            this._L_Assumable.TabIndex = 11;
            // 
            // _L_1stAssumable
            // 
            this._L_1stAssumable.Items.AddRange(new object[] {
            "Yes",
            "No"});
            this._L_1stAssumable.Location = new System.Drawing.Point(24, 64);
            this._L_1stAssumable.Name = "_L_1stAssumable";
            this._L_1stAssumable.Size = new System.Drawing.Size(88, 21);
            this._L_1stAssumable.TabIndex = 10;
            // 
            // label77
            // 
            this.label77.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label77.Location = new System.Drawing.Point(240, 48);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(104, 23);
            this.label77.TabIndex = 9;
            this.label77.Text = "Seller Carryback?";
            // 
            // label76
            // 
            this.label76.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label76.Location = new System.Drawing.Point(128, 48);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(104, 23);
            this.label76.TabIndex = 8;
            this.label76.Text = "2nd Assumable?";
            // 
            // label75
            // 
            this.label75.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label75.Location = new System.Drawing.Point(24, 48);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(104, 23);
            this.label75.TabIndex = 7;
            this.label75.Text = "1st Assumable?";
            // 
            // _L_DownPaymentPerc
            // 
            this._L_DownPaymentPerc.Items.AddRange(new object[] {
            "10",
            "15",
            "20",
            "25",
            "30",
            "35",
            "40",
            "45",
            "50"});
            this._L_DownPaymentPerc.Location = new System.Drawing.Point(128, 24);
            this._L_DownPaymentPerc.Name = "_L_DownPaymentPerc";
            this._L_DownPaymentPerc.Size = new System.Drawing.Size(96, 21);
            this._L_DownPaymentPerc.TabIndex = 5;
            // 
            // _L_DownPayment
            // 
            this._L_DownPayment.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._L_DownPayment.Location = new System.Drawing.Point(240, 24);
            this._L_DownPayment.Name = "_L_DownPayment";
            this._L_DownPayment.Size = new System.Drawing.Size(100, 20);
            this._L_DownPayment.TabIndex = 6;
            // 
            // _L_PurchasePrice
            // 
            this._L_PurchasePrice.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._L_PurchasePrice.Location = new System.Drawing.Point(24, 24);
            this._L_PurchasePrice.Name = "_L_PurchasePrice";
            this._L_PurchasePrice.Size = new System.Drawing.Size(100, 20);
            this._L_PurchasePrice.TabIndex = 4;
            // 
            // label74
            // 
            this.label74.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label74.Location = new System.Drawing.Point(12, 24);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(12, 23);
            this.label74.TabIndex = 3;
            this.label74.Text = "$";
            // 
            // label73
            // 
            this.label73.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label73.Location = new System.Drawing.Point(128, 8);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(104, 23);
            this.label73.TabIndex = 2;
            this.label73.Text = "Down Payment (%)";
            // 
            // label72
            // 
            this.label72.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label72.Location = new System.Drawing.Point(240, 8);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(104, 23);
            this.label72.TabIndex = 1;
            this.label72.Text = "Down Payment ($)";
            // 
            // label71
            // 
            this.label71.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label71.Location = new System.Drawing.Point(24, 8);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(104, 23);
            this.label71.TabIndex = 0;
            this.label71.Text = "Purchase Price";
            // 
            // _financialAnalysisTabPage
            // 
            this._financialAnalysisTabPage.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this._financialAnalysisTabPage.Controls.Add(this._printFinancialAnalysis);
            this._financialAnalysisTabPage.Controls.Add(this._fa_floater);
            this._financialAnalysisTabPage.Controls.Add(this._FA_LR_Range_NUD);
            this._financialAnalysisTabPage.Controls.Add(this._FA_OE_Range_NUD);
            this._financialAnalysisTabPage.Controls.Add(this._FA_GSI_Range_NUD);
            this._financialAnalysisTabPage.Controls.Add(this._FA_CR_Change_NUD);
            this._financialAnalysisTabPage.Controls.Add(this.label305);
            this._financialAnalysisTabPage.Controls.Add(this.label302);
            this._financialAnalysisTabPage.Controls.Add(this.label299);
            this._financialAnalysisTabPage.Controls.Add(this.label295);
            this._financialAnalysisTabPage.Controls.Add(this.label293);
            this._financialAnalysisTabPage.Controls.Add(this.label290);
            this._financialAnalysisTabPage.Controls.Add(this.label322);
            this._financialAnalysisTabPage.Controls.Add(this.label320);
            this._financialAnalysisTabPage.Controls.Add(this.label318);
            this._financialAnalysisTabPage.Controls.Add(this.label316);
            this._financialAnalysisTabPage.Controls.Add(this.label315);
            this._financialAnalysisTabPage.Controls.Add(this.label313);
            this._financialAnalysisTabPage.Controls.Add(this.label312);
            this._financialAnalysisTabPage.Controls.Add(this.label310);
            this._financialAnalysisTabPage.Controls.Add(this.label308);
            this._financialAnalysisTabPage.Controls.Add(this.label306);
            this._financialAnalysisTabPage.Controls.Add(this.label304);
            this._financialAnalysisTabPage.Controls.Add(this.label303);
            this._financialAnalysisTabPage.Controls.Add(this.label301);
            this._financialAnalysisTabPage.Controls.Add(this.label300);
            this._financialAnalysisTabPage.Controls.Add(this.label298);
            this._financialAnalysisTabPage.Controls.Add(this.label296);
            this._financialAnalysisTabPage.Controls.Add(this.label294);
            this._financialAnalysisTabPage.Controls.Add(this.label292);
            this._financialAnalysisTabPage.Controls.Add(this.label291);
            this._financialAnalysisTabPage.Controls.Add(this.label289);
            this._financialAnalysisTabPage.Controls.Add(this.label288);
            this._financialAnalysisTabPage.Controls.Add(this.label287);
            this._financialAnalysisTabPage.Controls.Add(this.label286);
            this._financialAnalysisTabPage.Controls.Add(this.label285);
            this._financialAnalysisTabPage.Controls.Add(this.label284);
            this._financialAnalysisTabPage.Controls.Add(this.label283);
            this._financialAnalysisTabPage.Controls.Add(this.label282);
            this._financialAnalysisTabPage.Controls.Add(this.label281);
            this._financialAnalysisTabPage.Controls.Add(this._FA_S_OperatingExpensesPerc);
            this._financialAnalysisTabPage.Controls.Add(this.label280);
            this._financialAnalysisTabPage.Controls.Add(this._FA_P_OperatingExpensesPerc);
            this._financialAnalysisTabPage.Controls.Add(this.label279);
            this._financialAnalysisTabPage.Controls.Add(this._FA_A_OperatingExpensesPerc);
            this._financialAnalysisTabPage.Controls.Add(this.label277);
            this._financialAnalysisTabPage.Controls.Add(this.label278);
            this._financialAnalysisTabPage.Controls.Add(this._FA_Graph4);
            this._financialAnalysisTabPage.Controls.Add(this.label275);
            this._financialAnalysisTabPage.Controls.Add(this.label276);
            this._financialAnalysisTabPage.Controls.Add(this._FA_Graph3);
            this._financialAnalysisTabPage.Controls.Add(this.label273);
            this._financialAnalysisTabPage.Controls.Add(this.label274);
            this._financialAnalysisTabPage.Controls.Add(this._FA_Graph2);
            this._financialAnalysisTabPage.Controls.Add(this._FA_S_Comment);
            this._financialAnalysisTabPage.Controls.Add(this.label258);
            this._financialAnalysisTabPage.Controls.Add(this.label259);
            this._financialAnalysisTabPage.Controls.Add(this.label260);
            this._financialAnalysisTabPage.Controls.Add(this.label261);
            this._financialAnalysisTabPage.Controls.Add(this.label262);
            this._financialAnalysisTabPage.Controls.Add(this.label263);
            this._financialAnalysisTabPage.Controls.Add(this.label264);
            this._financialAnalysisTabPage.Controls.Add(this.label265);
            this._financialAnalysisTabPage.Controls.Add(this.label266);
            this._financialAnalysisTabPage.Controls.Add(this.label267);
            this._financialAnalysisTabPage.Controls.Add(this.label268);
            this._financialAnalysisTabPage.Controls.Add(this._FA_S_VacancyCreditLossPerc);
            this._financialAnalysisTabPage.Controls.Add(this.label269);
            this._financialAnalysisTabPage.Controls.Add(this.label270);
            this._financialAnalysisTabPage.Controls.Add(this.label271);
            this._financialAnalysisTabPage.Controls.Add(this._FA_S_CashOnCashReturn);
            this._financialAnalysisTabPage.Controls.Add(this._FA_S_CashFlow);
            this._financialAnalysisTabPage.Controls.Add(this._FA_S_DebtCoverageRatio);
            this._financialAnalysisTabPage.Controls.Add(this._FA_S_DebtService);
            this._financialAnalysisTabPage.Controls.Add(this._FA_S_CapRate);
            this._financialAnalysisTabPage.Controls.Add(this._FA_S_NetOperatingIncome);
            this._financialAnalysisTabPage.Controls.Add(this._FA_S_OperatingExpenses);
            this._financialAnalysisTabPage.Controls.Add(this._FA_S_GOIAnnual);
            this._financialAnalysisTabPage.Controls.Add(this._FA_S_OtherIncome);
            this._financialAnalysisTabPage.Controls.Add(this._FA_S_VacancyCreditLoss);
            this._financialAnalysisTabPage.Controls.Add(this._FA_S_GSIAnnual);
            this._financialAnalysisTabPage.Controls.Add(this._FA_S_Price);
            this._financialAnalysisTabPage.Controls.Add(this._FA_S_copyFromProforma);
            this._financialAnalysisTabPage.Controls.Add(this._FA_S_copyFromActual);
            this._financialAnalysisTabPage.Controls.Add(this.label272);
            this._financialAnalysisTabPage.Controls.Add(this._FA_P_Comment);
            this._financialAnalysisTabPage.Controls.Add(this.label243);
            this._financialAnalysisTabPage.Controls.Add(this.label244);
            this._financialAnalysisTabPage.Controls.Add(this.label245);
            this._financialAnalysisTabPage.Controls.Add(this.label246);
            this._financialAnalysisTabPage.Controls.Add(this.label247);
            this._financialAnalysisTabPage.Controls.Add(this.label248);
            this._financialAnalysisTabPage.Controls.Add(this.label249);
            this._financialAnalysisTabPage.Controls.Add(this.label250);
            this._financialAnalysisTabPage.Controls.Add(this.label251);
            this._financialAnalysisTabPage.Controls.Add(this.label252);
            this._financialAnalysisTabPage.Controls.Add(this.label253);
            this._financialAnalysisTabPage.Controls.Add(this._FA_P_VacancyCreditLossPerc);
            this._financialAnalysisTabPage.Controls.Add(this.label254);
            this._financialAnalysisTabPage.Controls.Add(this.label255);
            this._financialAnalysisTabPage.Controls.Add(this.label256);
            this._financialAnalysisTabPage.Controls.Add(this._FA_P_CashOnCashReturn);
            this._financialAnalysisTabPage.Controls.Add(this._FA_P_CashFlow);
            this._financialAnalysisTabPage.Controls.Add(this._FA_P_DebtCoverageRatio);
            this._financialAnalysisTabPage.Controls.Add(this._FA_P_DebtService);
            this._financialAnalysisTabPage.Controls.Add(this._FA_P_CapRate);
            this._financialAnalysisTabPage.Controls.Add(this._FA_P_NetOperatingIncome);
            this._financialAnalysisTabPage.Controls.Add(this._FA_P_OperatingExpenses);
            this._financialAnalysisTabPage.Controls.Add(this._FA_P_GOIAnnual);
            this._financialAnalysisTabPage.Controls.Add(this._FA_P_OtherIncome);
            this._financialAnalysisTabPage.Controls.Add(this._FA_P_VacancyCreditLoss);
            this._financialAnalysisTabPage.Controls.Add(this._FA_P_GSIAnnual);
            this._financialAnalysisTabPage.Controls.Add(this._FA_P_Price);
            this._financialAnalysisTabPage.Controls.Add(this._FA_P_copyFromActual);
            this._financialAnalysisTabPage.Controls.Add(this._FA_P_copyEfromExpenses);
            this._financialAnalysisTabPage.Controls.Add(this._FA_P_copyPIfromIncome);
            this._financialAnalysisTabPage.Controls.Add(this.label257);
            this._financialAnalysisTabPage.Controls.Add(this.label242);
            this._financialAnalysisTabPage.Controls.Add(this.label241);
            this._financialAnalysisTabPage.Controls.Add(this._FA_Graph1);
            this._financialAnalysisTabPage.Controls.Add(this.label240);
            this._financialAnalysisTabPage.Controls.Add(this._FA_A_Comment);
            this._financialAnalysisTabPage.Controls.Add(this.label239);
            this._financialAnalysisTabPage.Controls.Add(this.label238);
            this._financialAnalysisTabPage.Controls.Add(this.label237);
            this._financialAnalysisTabPage.Controls.Add(this.label236);
            this._financialAnalysisTabPage.Controls.Add(this.label235);
            this._financialAnalysisTabPage.Controls.Add(this.label234);
            this._financialAnalysisTabPage.Controls.Add(this.label233);
            this._financialAnalysisTabPage.Controls.Add(this.label232);
            this._financialAnalysisTabPage.Controls.Add(this.label231);
            this._financialAnalysisTabPage.Controls.Add(this.label230);
            this._financialAnalysisTabPage.Controls.Add(this.label229);
            this._financialAnalysisTabPage.Controls.Add(this._FA_A_VacancyCreditLossPerc);
            this._financialAnalysisTabPage.Controls.Add(this.label228);
            this._financialAnalysisTabPage.Controls.Add(this.label227);
            this._financialAnalysisTabPage.Controls.Add(this.label226);
            this._financialAnalysisTabPage.Controls.Add(this._FA_A_CashOnCashReturn);
            this._financialAnalysisTabPage.Controls.Add(this._FA_A_CashFlow);
            this._financialAnalysisTabPage.Controls.Add(this._FA_A_DebtCoverageRatio);
            this._financialAnalysisTabPage.Controls.Add(this._FA_A_DebtService);
            this._financialAnalysisTabPage.Controls.Add(this._FA_A_CapRate);
            this._financialAnalysisTabPage.Controls.Add(this._FA_A_NetOperatingIncome);
            this._financialAnalysisTabPage.Controls.Add(this._FA_A_OperatingExpenses);
            this._financialAnalysisTabPage.Controls.Add(this._FA_A_GOIAnnual);
            this._financialAnalysisTabPage.Controls.Add(this._FA_A_OtherIncome);
            this._financialAnalysisTabPage.Controls.Add(this._FA_A_VacancyCreditLoss);
            this._financialAnalysisTabPage.Controls.Add(this._FA_A_GSIAnnual);
            this._financialAnalysisTabPage.Controls.Add(this._FA_A_Price);
            this._financialAnalysisTabPage.Controls.Add(this._FA_copyPDfromLoan);
            this._financialAnalysisTabPage.Controls.Add(this._FA_copyEfromExpenses);
            this._financialAnalysisTabPage.Controls.Add(this._FA_copyAIfromIncome);
            this._financialAnalysisTabPage.Controls.Add(this.label225);
            this._financialAnalysisTabPage.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this._financialAnalysisTabPage.Location = new System.Drawing.Point(4, 22);
            this._financialAnalysisTabPage.Name = "_financialAnalysisTabPage";
            this._financialAnalysisTabPage.Size = new System.Drawing.Size(897, 529);
            this._financialAnalysisTabPage.TabIndex = 4;
            this._financialAnalysisTabPage.Text = "Financial Analysis";
            // 
            // _printFinancialAnalysis
            // 
            this._printFinancialAnalysis.Location = new System.Drawing.Point(835, 32);
            this._printFinancialAnalysis.Name = "_printFinancialAnalysis";
            this._printFinancialAnalysis.Size = new System.Drawing.Size(58, 23);
            this._printFinancialAnalysis.TabIndex = 172;
            this._printFinancialAnalysis.Text = "Print";
            this._printFinancialAnalysis.UseVisualStyleBackColor = true;
            // 
            // _fa_floater
            // 
            this._fa_floater.Image = ((System.Drawing.Image)(resources.GetObject("_fa_floater.Image")));
            this._fa_floater.Location = new System.Drawing.Point(872, 8);
            this._fa_floater.Name = "_fa_floater";
            this._fa_floater.Size = new System.Drawing.Size(16, 16);
            this._fa_floater.TabIndex = 171;
            this._fa_floater.TabStop = false;
            // 
            // _FA_LR_Range_NUD
            // 
            this._FA_LR_Range_NUD.Location = new System.Drawing.Point(832, 505);
            this._FA_LR_Range_NUD.Name = "_FA_LR_Range_NUD";
            this._FA_LR_Range_NUD.Size = new System.Drawing.Size(40, 20);
            this._FA_LR_Range_NUD.TabIndex = 170;
            this._FA_LR_Range_NUD.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            // 
            // _FA_OE_Range_NUD
            // 
            this._FA_OE_Range_NUD.Location = new System.Drawing.Point(616, 505);
            this._FA_OE_Range_NUD.Name = "_FA_OE_Range_NUD";
            this._FA_OE_Range_NUD.Size = new System.Drawing.Size(40, 20);
            this._FA_OE_Range_NUD.TabIndex = 169;
            this._FA_OE_Range_NUD.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // _FA_GSI_Range_NUD
            // 
            this._FA_GSI_Range_NUD.Location = new System.Drawing.Point(344, 505);
            this._FA_GSI_Range_NUD.Name = "_FA_GSI_Range_NUD";
            this._FA_GSI_Range_NUD.Size = new System.Drawing.Size(40, 20);
            this._FA_GSI_Range_NUD.TabIndex = 168;
            this._FA_GSI_Range_NUD.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // _FA_CR_Change_NUD
            // 
            this._FA_CR_Change_NUD.Location = new System.Drawing.Point(136, 505);
            this._FA_CR_Change_NUD.Name = "_FA_CR_Change_NUD";
            this._FA_CR_Change_NUD.Size = new System.Drawing.Size(40, 20);
            this._FA_CR_Change_NUD.TabIndex = 167;
            this._FA_CR_Change_NUD.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            // 
            // label305
            // 
            this.label305.Location = new System.Drawing.Point(706, 209);
            this.label305.Name = "label305";
            this.label305.Size = new System.Drawing.Size(16, 23);
            this.label305.TabIndex = 166;
            this.label305.Text = "$";
            this.label305.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label302
            // 
            this.label302.Location = new System.Drawing.Point(706, 149);
            this.label302.Name = "label302";
            this.label302.Size = new System.Drawing.Size(16, 23);
            this.label302.TabIndex = 165;
            this.label302.Text = "$";
            this.label302.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label299
            // 
            this.label299.Location = new System.Drawing.Point(419, 209);
            this.label299.Name = "label299";
            this.label299.Size = new System.Drawing.Size(16, 23);
            this.label299.TabIndex = 164;
            this.label299.Text = "$";
            this.label299.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label295
            // 
            this.label295.Location = new System.Drawing.Point(419, 149);
            this.label295.Name = "label295";
            this.label295.Size = new System.Drawing.Size(16, 23);
            this.label295.TabIndex = 163;
            this.label295.Text = "$";
            this.label295.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label293
            // 
            this.label293.Location = new System.Drawing.Point(128, 209);
            this.label293.Name = "label293";
            this.label293.Size = new System.Drawing.Size(16, 23);
            this.label293.TabIndex = 162;
            this.label293.Text = "$";
            this.label293.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label290
            // 
            this.label290.Location = new System.Drawing.Point(128, 149);
            this.label290.Name = "label290";
            this.label290.Size = new System.Drawing.Size(16, 23);
            this.label290.TabIndex = 161;
            this.label290.Text = "$";
            this.label290.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label322
            // 
            this.label322.Location = new System.Drawing.Point(706, 310);
            this.label322.Name = "label322";
            this.label322.Size = new System.Drawing.Size(16, 23);
            this.label322.TabIndex = 160;
            this.label322.Text = "$";
            this.label322.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label320
            // 
            this.label320.Location = new System.Drawing.Point(706, 269);
            this.label320.Name = "label320";
            this.label320.Size = new System.Drawing.Size(16, 23);
            this.label320.TabIndex = 158;
            this.label320.Text = "$";
            this.label320.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label318
            // 
            this.label318.Location = new System.Drawing.Point(706, 229);
            this.label318.Name = "label318";
            this.label318.Size = new System.Drawing.Size(16, 23);
            this.label318.TabIndex = 156;
            this.label318.Text = "$";
            this.label318.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label316
            // 
            this.label316.Location = new System.Drawing.Point(706, 190);
            this.label316.Name = "label316";
            this.label316.Size = new System.Drawing.Size(16, 23);
            this.label316.TabIndex = 154;
            this.label316.Text = "$";
            this.label316.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label315
            // 
            this.label315.Location = new System.Drawing.Point(706, 169);
            this.label315.Name = "label315";
            this.label315.Size = new System.Drawing.Size(16, 23);
            this.label315.TabIndex = 153;
            this.label315.Text = "$";
            this.label315.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label313
            // 
            this.label313.Location = new System.Drawing.Point(706, 129);
            this.label313.Name = "label313";
            this.label313.Size = new System.Drawing.Size(16, 23);
            this.label313.TabIndex = 151;
            this.label313.Text = "$";
            this.label313.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label312
            // 
            this.label312.Location = new System.Drawing.Point(706, 109);
            this.label312.Name = "label312";
            this.label312.Size = new System.Drawing.Size(16, 23);
            this.label312.TabIndex = 150;
            this.label312.Text = "$";
            this.label312.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label310
            // 
            this.label310.Location = new System.Drawing.Point(419, 310);
            this.label310.Name = "label310";
            this.label310.Size = new System.Drawing.Size(16, 23);
            this.label310.TabIndex = 148;
            this.label310.Text = "$";
            this.label310.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label308
            // 
            this.label308.Location = new System.Drawing.Point(419, 269);
            this.label308.Name = "label308";
            this.label308.Size = new System.Drawing.Size(16, 23);
            this.label308.TabIndex = 146;
            this.label308.Text = "$";
            this.label308.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label306
            // 
            this.label306.Location = new System.Drawing.Point(419, 229);
            this.label306.Name = "label306";
            this.label306.Size = new System.Drawing.Size(16, 23);
            this.label306.TabIndex = 144;
            this.label306.Text = "$";
            this.label306.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label304
            // 
            this.label304.Location = new System.Drawing.Point(419, 190);
            this.label304.Name = "label304";
            this.label304.Size = new System.Drawing.Size(16, 23);
            this.label304.TabIndex = 142;
            this.label304.Text = "$";
            this.label304.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label303
            // 
            this.label303.Location = new System.Drawing.Point(419, 169);
            this.label303.Name = "label303";
            this.label303.Size = new System.Drawing.Size(16, 23);
            this.label303.TabIndex = 141;
            this.label303.Text = "$";
            this.label303.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label301
            // 
            this.label301.Location = new System.Drawing.Point(419, 129);
            this.label301.Name = "label301";
            this.label301.Size = new System.Drawing.Size(16, 23);
            this.label301.TabIndex = 139;
            this.label301.Text = "$";
            this.label301.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label300
            // 
            this.label300.Location = new System.Drawing.Point(419, 109);
            this.label300.Name = "label300";
            this.label300.Size = new System.Drawing.Size(16, 23);
            this.label300.TabIndex = 138;
            this.label300.Text = "$";
            this.label300.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label298
            // 
            this.label298.Location = new System.Drawing.Point(128, 310);
            this.label298.Name = "label298";
            this.label298.Size = new System.Drawing.Size(16, 23);
            this.label298.TabIndex = 136;
            this.label298.Text = "$";
            this.label298.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label296
            // 
            this.label296.Location = new System.Drawing.Point(128, 269);
            this.label296.Name = "label296";
            this.label296.Size = new System.Drawing.Size(16, 23);
            this.label296.TabIndex = 134;
            this.label296.Text = "$";
            this.label296.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label294
            // 
            this.label294.Location = new System.Drawing.Point(128, 229);
            this.label294.Name = "label294";
            this.label294.Size = new System.Drawing.Size(16, 23);
            this.label294.TabIndex = 132;
            this.label294.Text = "$";
            this.label294.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label292
            // 
            this.label292.Location = new System.Drawing.Point(128, 190);
            this.label292.Name = "label292";
            this.label292.Size = new System.Drawing.Size(16, 23);
            this.label292.TabIndex = 130;
            this.label292.Text = "$";
            this.label292.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label291
            // 
            this.label291.Location = new System.Drawing.Point(128, 169);
            this.label291.Name = "label291";
            this.label291.Size = new System.Drawing.Size(16, 23);
            this.label291.TabIndex = 129;
            this.label291.Text = "$";
            this.label291.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label289
            // 
            this.label289.Location = new System.Drawing.Point(128, 129);
            this.label289.Name = "label289";
            this.label289.Size = new System.Drawing.Size(16, 23);
            this.label289.TabIndex = 127;
            this.label289.Text = "$";
            this.label289.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label288
            // 
            this.label288.Location = new System.Drawing.Point(128, 109);
            this.label288.Name = "label288";
            this.label288.Size = new System.Drawing.Size(16, 23);
            this.label288.TabIndex = 126;
            this.label288.Text = "$";
            this.label288.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label287
            // 
            this.label287.Location = new System.Drawing.Point(802, 336);
            this.label287.Name = "label287";
            this.label287.Size = new System.Drawing.Size(16, 23);
            this.label287.TabIndex = 125;
            this.label287.Text = "%";
            // 
            // label286
            // 
            this.label286.Location = new System.Drawing.Point(802, 253);
            this.label286.Name = "label286";
            this.label286.Size = new System.Drawing.Size(16, 23);
            this.label286.TabIndex = 124;
            this.label286.Text = "%";
            // 
            // label285
            // 
            this.label285.Location = new System.Drawing.Point(515, 336);
            this.label285.Name = "label285";
            this.label285.Size = new System.Drawing.Size(16, 23);
            this.label285.TabIndex = 123;
            this.label285.Text = "%";
            // 
            // label284
            // 
            this.label284.Location = new System.Drawing.Point(515, 256);
            this.label284.Name = "label284";
            this.label284.Size = new System.Drawing.Size(16, 23);
            this.label284.TabIndex = 122;
            this.label284.Text = "%";
            // 
            // label283
            // 
            this.label283.Location = new System.Drawing.Point(224, 336);
            this.label283.Name = "label283";
            this.label283.Size = new System.Drawing.Size(16, 23);
            this.label283.TabIndex = 121;
            this.label283.Text = "%";
            // 
            // label282
            // 
            this.label282.Location = new System.Drawing.Point(224, 256);
            this.label282.Name = "label282";
            this.label282.Size = new System.Drawing.Size(16, 23);
            this.label282.TabIndex = 120;
            this.label282.Text = "%";
            // 
            // label281
            // 
            this.label281.Location = new System.Drawing.Point(842, 216);
            this.label281.Name = "label281";
            this.label281.Size = new System.Drawing.Size(16, 23);
            this.label281.TabIndex = 119;
            this.label281.Text = "%";
            // 
            // _FA_S_OperatingExpensesPerc
            // 
            this._FA_S_OperatingExpensesPerc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._FA_S_OperatingExpensesPerc.Location = new System.Drawing.Point(802, 212);
            this._FA_S_OperatingExpensesPerc.Name = "_FA_S_OperatingExpensesPerc";
            this._FA_S_OperatingExpensesPerc.Size = new System.Drawing.Size(40, 20);
            this._FA_S_OperatingExpensesPerc.TabIndex = 69;
            this._FA_S_OperatingExpensesPerc.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label280
            // 
            this.label280.Location = new System.Drawing.Point(555, 216);
            this.label280.Name = "label280";
            this.label280.Size = new System.Drawing.Size(16, 23);
            this.label280.TabIndex = 117;
            this.label280.Text = "%";
            // 
            // _FA_P_OperatingExpensesPerc
            // 
            this._FA_P_OperatingExpensesPerc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._FA_P_OperatingExpensesPerc.Location = new System.Drawing.Point(515, 212);
            this._FA_P_OperatingExpensesPerc.Name = "_FA_P_OperatingExpensesPerc";
            this._FA_P_OperatingExpensesPerc.Size = new System.Drawing.Size(40, 20);
            this._FA_P_OperatingExpensesPerc.TabIndex = 40;
            this._FA_P_OperatingExpensesPerc.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label279
            // 
            this.label279.Location = new System.Drawing.Point(264, 215);
            this.label279.Name = "label279";
            this.label279.Size = new System.Drawing.Size(16, 23);
            this.label279.TabIndex = 115;
            this.label279.Text = "%";
            // 
            // _FA_A_OperatingExpensesPerc
            // 
            this._FA_A_OperatingExpensesPerc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._FA_A_OperatingExpensesPerc.Location = new System.Drawing.Point(224, 212);
            this._FA_A_OperatingExpensesPerc.Name = "_FA_A_OperatingExpensesPerc";
            this._FA_A_OperatingExpensesPerc.Size = new System.Drawing.Size(40, 20);
            this._FA_A_OperatingExpensesPerc.TabIndex = 11;
            this._FA_A_OperatingExpensesPerc.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label277
            // 
            this.label277.Location = new System.Drawing.Point(872, 504);
            this.label277.Name = "label277";
            this.label277.Size = new System.Drawing.Size(16, 23);
            this.label277.TabIndex = 113;
            this.label277.Text = "%";
            this.label277.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label278
            // 
            this.label278.Location = new System.Drawing.Point(688, 503);
            this.label278.Name = "label278";
            this.label278.Size = new System.Drawing.Size(136, 23);
            this.label278.TabIndex = 112;
            this.label278.Text = "1st Loan Rate Range: +/-";
            this.label278.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _FA_Graph4
            // 
            this._FA_Graph4.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._FA_Graph4.Location = new System.Drawing.Point(680, 480);
            this._FA_Graph4.Name = "_FA_Graph4";
            this._FA_Graph4.Size = new System.Drawing.Size(208, 24);
            this._FA_Graph4.TabIndex = 95;
            this._FA_Graph4.Text = "Cash Flow and Cash-on-Cash Return vs.";
            this._FA_Graph4.Click += new System.EventHandler(this._FA_Graph4_Click);
            // 
            // label275
            // 
            this.label275.Location = new System.Drawing.Point(656, 504);
            this.label275.Name = "label275";
            this.label275.Size = new System.Drawing.Size(16, 23);
            this.label275.TabIndex = 109;
            this.label275.Text = "%";
            this.label275.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label276
            // 
            this.label276.Location = new System.Drawing.Point(440, 503);
            this.label276.Name = "label276";
            this.label276.Size = new System.Drawing.Size(176, 23);
            this.label276.TabIndex = 108;
            this.label276.Text = "% Operating Expenses Range: +/-";
            this.label276.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _FA_Graph3
            // 
            this._FA_Graph3.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._FA_Graph3.Location = new System.Drawing.Point(448, 480);
            this._FA_Graph3.Name = "_FA_Graph3";
            this._FA_Graph3.Size = new System.Drawing.Size(208, 24);
            this._FA_Graph3.TabIndex = 90;
            this._FA_Graph3.Text = "Cash Flow and Cash-on-Cash Return vs.";
            this._FA_Graph3.Click += new System.EventHandler(this._FA_Graph3_Click);
            // 
            // label273
            // 
            this.label273.Location = new System.Drawing.Point(384, 504);
            this.label273.Name = "label273";
            this.label273.Size = new System.Drawing.Size(16, 23);
            this.label273.TabIndex = 105;
            this.label273.Text = "%";
            this.label273.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label274
            // 
            this.label274.Location = new System.Drawing.Point(216, 503);
            this.label274.Name = "label274";
            this.label274.Size = new System.Drawing.Size(120, 23);
            this.label274.TabIndex = 104;
            this.label274.Text = "GSI Range: +/-";
            this.label274.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _FA_Graph2
            // 
            this._FA_Graph2.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._FA_Graph2.Location = new System.Drawing.Point(216, 480);
            this._FA_Graph2.Name = "_FA_Graph2";
            this._FA_Graph2.Size = new System.Drawing.Size(208, 24);
            this._FA_Graph2.TabIndex = 85;
            this._FA_Graph2.Text = "Cash Flow and Cash-on-Cash Return vs.";
            this._FA_Graph2.Click += new System.EventHandler(this._FA_Graph2_Click);
            // 
            // _FA_S_Comment
            // 
            this._FA_S_Comment.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._FA_S_Comment.Location = new System.Drawing.Point(594, 384);
            this._FA_S_Comment.Multiline = true;
            this._FA_S_Comment.Name = "_FA_S_Comment";
            this._FA_S_Comment.Size = new System.Drawing.Size(232, 72);
            this._FA_S_Comment.TabIndex = 77;
            // 
            // label258
            // 
            this.label258.Location = new System.Drawing.Point(594, 360);
            this.label258.Name = "label258";
            this.label258.Size = new System.Drawing.Size(120, 23);
            this.label258.TabIndex = 100;
            this.label258.Text = "Comments:";
            this.label258.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label259
            // 
            this.label259.Location = new System.Drawing.Point(586, 330);
            this.label259.Name = "label259";
            this.label259.Size = new System.Drawing.Size(120, 23);
            this.label259.TabIndex = 99;
            this.label259.Text = "Cash on Cash Return:";
            this.label259.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label260
            // 
            this.label260.Location = new System.Drawing.Point(586, 308);
            this.label260.Name = "label260";
            this.label260.Size = new System.Drawing.Size(120, 23);
            this.label260.TabIndex = 98;
            this.label260.Text = "Cash Flow (pretax):";
            this.label260.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label261
            // 
            this.label261.Location = new System.Drawing.Point(586, 288);
            this.label261.Name = "label261";
            this.label261.Size = new System.Drawing.Size(120, 23);
            this.label261.TabIndex = 97;
            this.label261.Text = "Debt Coverage Ratio:";
            this.label261.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label262
            // 
            this.label262.Location = new System.Drawing.Point(586, 270);
            this.label262.Name = "label262";
            this.label262.Size = new System.Drawing.Size(120, 23);
            this.label262.TabIndex = 96;
            this.label262.Text = "Debt Service:";
            this.label262.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label263
            // 
            this.label263.Location = new System.Drawing.Point(586, 249);
            this.label263.Name = "label263";
            this.label263.Size = new System.Drawing.Size(120, 23);
            this.label263.TabIndex = 95;
            this.label263.Text = "Cap Rate:";
            this.label263.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label264
            // 
            this.label264.Location = new System.Drawing.Point(586, 229);
            this.label264.Name = "label264";
            this.label264.Size = new System.Drawing.Size(120, 23);
            this.label264.TabIndex = 94;
            this.label264.Text = "Net Operating Income:";
            this.label264.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label265
            // 
            this.label265.Location = new System.Drawing.Point(594, 208);
            this.label265.Name = "label265";
            this.label265.Size = new System.Drawing.Size(112, 23);
            this.label265.TabIndex = 93;
            this.label265.Text = "Operating Expenses:";
            this.label265.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label266
            // 
            this.label266.Location = new System.Drawing.Point(594, 190);
            this.label266.Name = "label266";
            this.label266.Size = new System.Drawing.Size(112, 23);
            this.label266.TabIndex = 92;
            this.label266.Text = "GOI (Annual):";
            this.label266.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label267
            // 
            this.label267.Location = new System.Drawing.Point(594, 170);
            this.label267.Name = "label267";
            this.label267.Size = new System.Drawing.Size(112, 23);
            this.label267.TabIndex = 91;
            this.label267.Text = "Other Income:";
            this.label267.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label268
            // 
            this.label268.Location = new System.Drawing.Point(842, 152);
            this.label268.Name = "label268";
            this.label268.Size = new System.Drawing.Size(16, 23);
            this.label268.TabIndex = 90;
            this.label268.Text = "%";
            // 
            // _FA_S_VacancyCreditLossPerc
            // 
            this._FA_S_VacancyCreditLossPerc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._FA_S_VacancyCreditLossPerc.Location = new System.Drawing.Point(802, 152);
            this._FA_S_VacancyCreditLossPerc.Name = "_FA_S_VacancyCreditLossPerc";
            this._FA_S_VacancyCreditLossPerc.Size = new System.Drawing.Size(40, 20);
            this._FA_S_VacancyCreditLossPerc.TabIndex = 65;
            this._FA_S_VacancyCreditLossPerc.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label269
            // 
            this.label269.Location = new System.Drawing.Point(594, 149);
            this.label269.Name = "label269";
            this.label269.Size = new System.Drawing.Size(112, 23);
            this.label269.TabIndex = 88;
            this.label269.Text = "Vacancy/Credit Loss:";
            this.label269.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label270
            // 
            this.label270.Location = new System.Drawing.Point(606, 128);
            this.label270.Name = "label270";
            this.label270.Size = new System.Drawing.Size(100, 23);
            this.label270.TabIndex = 87;
            this.label270.Text = "GSI (Annual):";
            this.label270.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label271
            // 
            this.label271.Location = new System.Drawing.Point(606, 109);
            this.label271.Name = "label271";
            this.label271.Size = new System.Drawing.Size(100, 23);
            this.label271.TabIndex = 86;
            this.label271.Text = "Price/Value:";
            this.label271.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _FA_S_CashOnCashReturn
            // 
            this._FA_S_CashOnCashReturn.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._FA_S_CashOnCashReturn.Location = new System.Drawing.Point(722, 332);
            this._FA_S_CashOnCashReturn.Name = "_FA_S_CashOnCashReturn";
            this._FA_S_CashOnCashReturn.ReadOnly = true;
            this._FA_S_CashOnCashReturn.Size = new System.Drawing.Size(80, 20);
            this._FA_S_CashOnCashReturn.TabIndex = 76;
            this._FA_S_CashOnCashReturn.TabStop = false;
            // 
            // _FA_S_CashFlow
            // 
            this._FA_S_CashFlow.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._FA_S_CashFlow.Location = new System.Drawing.Point(722, 312);
            this._FA_S_CashFlow.Name = "_FA_S_CashFlow";
            this._FA_S_CashFlow.ReadOnly = true;
            this._FA_S_CashFlow.Size = new System.Drawing.Size(80, 20);
            this._FA_S_CashFlow.TabIndex = 75;
            this._FA_S_CashFlow.TabStop = false;
            // 
            // _FA_S_DebtCoverageRatio
            // 
            this._FA_S_DebtCoverageRatio.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._FA_S_DebtCoverageRatio.Location = new System.Drawing.Point(722, 292);
            this._FA_S_DebtCoverageRatio.Name = "_FA_S_DebtCoverageRatio";
            this._FA_S_DebtCoverageRatio.ReadOnly = true;
            this._FA_S_DebtCoverageRatio.Size = new System.Drawing.Size(80, 20);
            this._FA_S_DebtCoverageRatio.TabIndex = 74;
            this._FA_S_DebtCoverageRatio.TabStop = false;
            // 
            // _FA_S_DebtService
            // 
            this._FA_S_DebtService.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._FA_S_DebtService.Location = new System.Drawing.Point(722, 272);
            this._FA_S_DebtService.Name = "_FA_S_DebtService";
            this._FA_S_DebtService.Size = new System.Drawing.Size(80, 20);
            this._FA_S_DebtService.TabIndex = 73;
            // 
            // _FA_S_CapRate
            // 
            this._FA_S_CapRate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._FA_S_CapRate.Location = new System.Drawing.Point(722, 252);
            this._FA_S_CapRate.Name = "_FA_S_CapRate";
            this._FA_S_CapRate.Size = new System.Drawing.Size(80, 20);
            this._FA_S_CapRate.TabIndex = 72;
            // 
            // _FA_S_NetOperatingIncome
            // 
            this._FA_S_NetOperatingIncome.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._FA_S_NetOperatingIncome.Location = new System.Drawing.Point(722, 232);
            this._FA_S_NetOperatingIncome.Name = "_FA_S_NetOperatingIncome";
            this._FA_S_NetOperatingIncome.ReadOnly = true;
            this._FA_S_NetOperatingIncome.Size = new System.Drawing.Size(80, 20);
            this._FA_S_NetOperatingIncome.TabIndex = 70;
            this._FA_S_NetOperatingIncome.TabStop = false;
            // 
            // _FA_S_OperatingExpenses
            // 
            this._FA_S_OperatingExpenses.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._FA_S_OperatingExpenses.Location = new System.Drawing.Point(722, 212);
            this._FA_S_OperatingExpenses.Name = "_FA_S_OperatingExpenses";
            this._FA_S_OperatingExpenses.Size = new System.Drawing.Size(80, 20);
            this._FA_S_OperatingExpenses.TabIndex = 68;
            // 
            // _FA_S_GOIAnnual
            // 
            this._FA_S_GOIAnnual.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._FA_S_GOIAnnual.Location = new System.Drawing.Point(722, 192);
            this._FA_S_GOIAnnual.Name = "_FA_S_GOIAnnual";
            this._FA_S_GOIAnnual.ReadOnly = true;
            this._FA_S_GOIAnnual.Size = new System.Drawing.Size(80, 20);
            this._FA_S_GOIAnnual.TabIndex = 67;
            this._FA_S_GOIAnnual.TabStop = false;
            // 
            // _FA_S_OtherIncome
            // 
            this._FA_S_OtherIncome.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._FA_S_OtherIncome.Location = new System.Drawing.Point(722, 172);
            this._FA_S_OtherIncome.Name = "_FA_S_OtherIncome";
            this._FA_S_OtherIncome.Size = new System.Drawing.Size(80, 20);
            this._FA_S_OtherIncome.TabIndex = 66;
            // 
            // _FA_S_VacancyCreditLoss
            // 
            this._FA_S_VacancyCreditLoss.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._FA_S_VacancyCreditLoss.Location = new System.Drawing.Point(722, 152);
            this._FA_S_VacancyCreditLoss.Name = "_FA_S_VacancyCreditLoss";
            this._FA_S_VacancyCreditLoss.Size = new System.Drawing.Size(80, 20);
            this._FA_S_VacancyCreditLoss.TabIndex = 64;
            // 
            // _FA_S_GSIAnnual
            // 
            this._FA_S_GSIAnnual.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._FA_S_GSIAnnual.Location = new System.Drawing.Point(722, 132);
            this._FA_S_GSIAnnual.Name = "_FA_S_GSIAnnual";
            this._FA_S_GSIAnnual.Size = new System.Drawing.Size(80, 20);
            this._FA_S_GSIAnnual.TabIndex = 63;
            // 
            // _FA_S_Price
            // 
            this._FA_S_Price.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._FA_S_Price.Location = new System.Drawing.Point(722, 112);
            this._FA_S_Price.Name = "_FA_S_Price";
            this._FA_S_Price.Size = new System.Drawing.Size(80, 20);
            this._FA_S_Price.TabIndex = 62;
            // 
            // _FA_S_copyFromProforma
            // 
            this._FA_S_copyFromProforma.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._FA_S_copyFromProforma.Location = new System.Drawing.Point(594, 56);
            this._FA_S_copyFromProforma.Name = "_FA_S_copyFromProforma";
            this._FA_S_copyFromProforma.Size = new System.Drawing.Size(232, 23);
            this._FA_S_copyFromProforma.TabIndex = 61;
            this._FA_S_copyFromProforma.Text = "Copy from PROFORMA";
            // 
            // _FA_S_copyFromActual
            // 
            this._FA_S_copyFromActual.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._FA_S_copyFromActual.Location = new System.Drawing.Point(594, 32);
            this._FA_S_copyFromActual.Name = "_FA_S_copyFromActual";
            this._FA_S_copyFromActual.Size = new System.Drawing.Size(232, 23);
            this._FA_S_copyFromActual.TabIndex = 60;
            this._FA_S_copyFromActual.Text = "Copy from ACTUAL";
            // 
            // label272
            // 
            this.label272.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label272.Location = new System.Drawing.Point(594, 8);
            this.label272.Name = "label272";
            this.label272.Size = new System.Drawing.Size(100, 23);
            this.label272.TabIndex = 70;
            this.label272.Text = "SCENARIO :";
            // 
            // _FA_P_Comment
            // 
            this._FA_P_Comment.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._FA_P_Comment.Location = new System.Drawing.Point(307, 384);
            this._FA_P_Comment.Multiline = true;
            this._FA_P_Comment.Name = "_FA_P_Comment";
            this._FA_P_Comment.Size = new System.Drawing.Size(232, 72);
            this._FA_P_Comment.TabIndex = 47;
            // 
            // label243
            // 
            this.label243.Location = new System.Drawing.Point(307, 360);
            this.label243.Name = "label243";
            this.label243.Size = new System.Drawing.Size(120, 23);
            this.label243.TabIndex = 68;
            this.label243.Text = "Comments:";
            this.label243.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label244
            // 
            this.label244.Location = new System.Drawing.Point(299, 330);
            this.label244.Name = "label244";
            this.label244.Size = new System.Drawing.Size(120, 23);
            this.label244.TabIndex = 67;
            this.label244.Text = "Cash on Cash Return:";
            this.label244.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label245
            // 
            this.label245.Location = new System.Drawing.Point(299, 308);
            this.label245.Name = "label245";
            this.label245.Size = new System.Drawing.Size(120, 23);
            this.label245.TabIndex = 66;
            this.label245.Text = "Cash Flow (pretax):";
            this.label245.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label246
            // 
            this.label246.Location = new System.Drawing.Point(299, 288);
            this.label246.Name = "label246";
            this.label246.Size = new System.Drawing.Size(120, 23);
            this.label246.TabIndex = 65;
            this.label246.Text = "Debt Coverage Ratio:";
            this.label246.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label247
            // 
            this.label247.Location = new System.Drawing.Point(299, 270);
            this.label247.Name = "label247";
            this.label247.Size = new System.Drawing.Size(120, 23);
            this.label247.TabIndex = 64;
            this.label247.Text = "Debt Service:";
            this.label247.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label248
            // 
            this.label248.Location = new System.Drawing.Point(299, 249);
            this.label248.Name = "label248";
            this.label248.Size = new System.Drawing.Size(120, 23);
            this.label248.TabIndex = 63;
            this.label248.Text = "Cap Rate:";
            this.label248.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label249
            // 
            this.label249.Location = new System.Drawing.Point(299, 229);
            this.label249.Name = "label249";
            this.label249.Size = new System.Drawing.Size(120, 23);
            this.label249.TabIndex = 62;
            this.label249.Text = "Net Operating Income:";
            this.label249.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label250
            // 
            this.label250.Location = new System.Drawing.Point(307, 208);
            this.label250.Name = "label250";
            this.label250.Size = new System.Drawing.Size(112, 23);
            this.label250.TabIndex = 61;
            this.label250.Text = "Operating Expenses:";
            this.label250.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label251
            // 
            this.label251.Location = new System.Drawing.Point(307, 190);
            this.label251.Name = "label251";
            this.label251.Size = new System.Drawing.Size(112, 23);
            this.label251.TabIndex = 60;
            this.label251.Text = "GOI (Annual):";
            this.label251.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label252
            // 
            this.label252.Location = new System.Drawing.Point(307, 170);
            this.label252.Name = "label252";
            this.label252.Size = new System.Drawing.Size(112, 23);
            this.label252.TabIndex = 59;
            this.label252.Text = "Other Income:";
            this.label252.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label253
            // 
            this.label253.Location = new System.Drawing.Point(555, 152);
            this.label253.Name = "label253";
            this.label253.Size = new System.Drawing.Size(16, 23);
            this.label253.TabIndex = 58;
            this.label253.Text = "%";
            // 
            // _FA_P_VacancyCreditLossPerc
            // 
            this._FA_P_VacancyCreditLossPerc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._FA_P_VacancyCreditLossPerc.Location = new System.Drawing.Point(515, 152);
            this._FA_P_VacancyCreditLossPerc.Name = "_FA_P_VacancyCreditLossPerc";
            this._FA_P_VacancyCreditLossPerc.Size = new System.Drawing.Size(40, 20);
            this._FA_P_VacancyCreditLossPerc.TabIndex = 36;
            this._FA_P_VacancyCreditLossPerc.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label254
            // 
            this.label254.Location = new System.Drawing.Point(307, 149);
            this.label254.Name = "label254";
            this.label254.Size = new System.Drawing.Size(112, 23);
            this.label254.TabIndex = 56;
            this.label254.Text = "Vacancy/Credit Loss:";
            this.label254.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label255
            // 
            this.label255.Location = new System.Drawing.Point(319, 128);
            this.label255.Name = "label255";
            this.label255.Size = new System.Drawing.Size(100, 23);
            this.label255.TabIndex = 55;
            this.label255.Text = "GSI (Annual):";
            this.label255.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label256
            // 
            this.label256.Location = new System.Drawing.Point(319, 109);
            this.label256.Name = "label256";
            this.label256.Size = new System.Drawing.Size(100, 23);
            this.label256.TabIndex = 54;
            this.label256.Text = "Price/Value:";
            this.label256.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _FA_P_CashOnCashReturn
            // 
            this._FA_P_CashOnCashReturn.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._FA_P_CashOnCashReturn.Location = new System.Drawing.Point(435, 332);
            this._FA_P_CashOnCashReturn.Name = "_FA_P_CashOnCashReturn";
            this._FA_P_CashOnCashReturn.ReadOnly = true;
            this._FA_P_CashOnCashReturn.Size = new System.Drawing.Size(80, 20);
            this._FA_P_CashOnCashReturn.TabIndex = 46;
            this._FA_P_CashOnCashReturn.TabStop = false;
            // 
            // _FA_P_CashFlow
            // 
            this._FA_P_CashFlow.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._FA_P_CashFlow.Location = new System.Drawing.Point(435, 312);
            this._FA_P_CashFlow.Name = "_FA_P_CashFlow";
            this._FA_P_CashFlow.ReadOnly = true;
            this._FA_P_CashFlow.Size = new System.Drawing.Size(80, 20);
            this._FA_P_CashFlow.TabIndex = 45;
            this._FA_P_CashFlow.TabStop = false;
            // 
            // _FA_P_DebtCoverageRatio
            // 
            this._FA_P_DebtCoverageRatio.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._FA_P_DebtCoverageRatio.Location = new System.Drawing.Point(435, 292);
            this._FA_P_DebtCoverageRatio.Name = "_FA_P_DebtCoverageRatio";
            this._FA_P_DebtCoverageRatio.ReadOnly = true;
            this._FA_P_DebtCoverageRatio.Size = new System.Drawing.Size(80, 20);
            this._FA_P_DebtCoverageRatio.TabIndex = 44;
            this._FA_P_DebtCoverageRatio.TabStop = false;
            // 
            // _FA_P_DebtService
            // 
            this._FA_P_DebtService.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._FA_P_DebtService.Location = new System.Drawing.Point(435, 272);
            this._FA_P_DebtService.Name = "_FA_P_DebtService";
            this._FA_P_DebtService.Size = new System.Drawing.Size(80, 20);
            this._FA_P_DebtService.TabIndex = 43;
            // 
            // _FA_P_CapRate
            // 
            this._FA_P_CapRate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._FA_P_CapRate.Location = new System.Drawing.Point(435, 252);
            this._FA_P_CapRate.Name = "_FA_P_CapRate";
            this._FA_P_CapRate.Size = new System.Drawing.Size(80, 20);
            this._FA_P_CapRate.TabIndex = 42;
            // 
            // _FA_P_NetOperatingIncome
            // 
            this._FA_P_NetOperatingIncome.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._FA_P_NetOperatingIncome.Location = new System.Drawing.Point(435, 232);
            this._FA_P_NetOperatingIncome.Name = "_FA_P_NetOperatingIncome";
            this._FA_P_NetOperatingIncome.ReadOnly = true;
            this._FA_P_NetOperatingIncome.Size = new System.Drawing.Size(80, 20);
            this._FA_P_NetOperatingIncome.TabIndex = 41;
            this._FA_P_NetOperatingIncome.TabStop = false;
            // 
            // _FA_P_OperatingExpenses
            // 
            this._FA_P_OperatingExpenses.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._FA_P_OperatingExpenses.Location = new System.Drawing.Point(435, 212);
            this._FA_P_OperatingExpenses.Name = "_FA_P_OperatingExpenses";
            this._FA_P_OperatingExpenses.Size = new System.Drawing.Size(80, 20);
            this._FA_P_OperatingExpenses.TabIndex = 39;
            // 
            // _FA_P_GOIAnnual
            // 
            this._FA_P_GOIAnnual.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._FA_P_GOIAnnual.Location = new System.Drawing.Point(435, 192);
            this._FA_P_GOIAnnual.Name = "_FA_P_GOIAnnual";
            this._FA_P_GOIAnnual.ReadOnly = true;
            this._FA_P_GOIAnnual.Size = new System.Drawing.Size(80, 20);
            this._FA_P_GOIAnnual.TabIndex = 38;
            this._FA_P_GOIAnnual.TabStop = false;
            // 
            // _FA_P_OtherIncome
            // 
            this._FA_P_OtherIncome.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._FA_P_OtherIncome.Location = new System.Drawing.Point(435, 172);
            this._FA_P_OtherIncome.Name = "_FA_P_OtherIncome";
            this._FA_P_OtherIncome.Size = new System.Drawing.Size(80, 20);
            this._FA_P_OtherIncome.TabIndex = 37;
            // 
            // _FA_P_VacancyCreditLoss
            // 
            this._FA_P_VacancyCreditLoss.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._FA_P_VacancyCreditLoss.Location = new System.Drawing.Point(435, 152);
            this._FA_P_VacancyCreditLoss.Name = "_FA_P_VacancyCreditLoss";
            this._FA_P_VacancyCreditLoss.Size = new System.Drawing.Size(80, 20);
            this._FA_P_VacancyCreditLoss.TabIndex = 35;
            // 
            // _FA_P_GSIAnnual
            // 
            this._FA_P_GSIAnnual.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._FA_P_GSIAnnual.Location = new System.Drawing.Point(435, 132);
            this._FA_P_GSIAnnual.Name = "_FA_P_GSIAnnual";
            this._FA_P_GSIAnnual.Size = new System.Drawing.Size(80, 20);
            this._FA_P_GSIAnnual.TabIndex = 34;
            // 
            // _FA_P_Price
            // 
            this._FA_P_Price.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._FA_P_Price.Location = new System.Drawing.Point(435, 112);
            this._FA_P_Price.Name = "_FA_P_Price";
            this._FA_P_Price.Size = new System.Drawing.Size(80, 20);
            this._FA_P_Price.TabIndex = 33;
            // 
            // _FA_P_copyFromActual
            // 
            this._FA_P_copyFromActual.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._FA_P_copyFromActual.Location = new System.Drawing.Point(307, 80);
            this._FA_P_copyFromActual.Name = "_FA_P_copyFromActual";
            this._FA_P_copyFromActual.Size = new System.Drawing.Size(232, 23);
            this._FA_P_copyFromActual.TabIndex = 32;
            this._FA_P_copyFromActual.Text = "Copy from ACTUAL";
            // 
            // _FA_P_copyEfromExpenses
            // 
            this._FA_P_copyEfromExpenses.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._FA_P_copyEfromExpenses.Location = new System.Drawing.Point(307, 56);
            this._FA_P_copyEfromExpenses.Name = "_FA_P_copyEfromExpenses";
            this._FA_P_copyEfromExpenses.Size = new System.Drawing.Size(232, 23);
            this._FA_P_copyEfromExpenses.TabIndex = 31;
            this._FA_P_copyEfromExpenses.Text = "Copy EXPENSES from Expenses Tab";
            // 
            // _FA_P_copyPIfromIncome
            // 
            this._FA_P_copyPIfromIncome.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._FA_P_copyPIfromIncome.Location = new System.Drawing.Point(307, 32);
            this._FA_P_copyPIfromIncome.Name = "_FA_P_copyPIfromIncome";
            this._FA_P_copyPIfromIncome.Size = new System.Drawing.Size(232, 23);
            this._FA_P_copyPIfromIncome.TabIndex = 30;
            this._FA_P_copyPIfromIncome.Text = "Copy PROFORMA INCOME from Income Tab";
            // 
            // label257
            // 
            this.label257.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label257.Location = new System.Drawing.Point(307, 8);
            this.label257.Name = "label257";
            this.label257.Size = new System.Drawing.Size(100, 23);
            this.label257.TabIndex = 38;
            this.label257.Text = "PROFORMA:";
            // 
            // label242
            // 
            this.label242.Location = new System.Drawing.Point(176, 504);
            this.label242.Name = "label242";
            this.label242.Size = new System.Drawing.Size(16, 23);
            this.label242.TabIndex = 37;
            this.label242.Text = "%";
            this.label242.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label241
            // 
            this.label241.Location = new System.Drawing.Point(8, 503);
            this.label241.Name = "label241";
            this.label241.Size = new System.Drawing.Size(120, 23);
            this.label241.TabIndex = 36;
            this.label241.Text = "Cap Rate Change: +/-";
            this.label241.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _FA_Graph1
            // 
            this._FA_Graph1.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._FA_Graph1.Location = new System.Drawing.Point(8, 480);
            this._FA_Graph1.Name = "_FA_Graph1";
            this._FA_Graph1.Size = new System.Drawing.Size(184, 23);
            this._FA_Graph1.TabIndex = 80;
            this._FA_Graph1.Text = "Price/Value vs. Cap Rate";
            this._FA_Graph1.Click += new System.EventHandler(this._FA_Graph1_Click);
            // 
            // label240
            // 
            this.label240.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label240.Location = new System.Drawing.Point(8, 464);
            this.label240.Name = "label240";
            this.label240.Size = new System.Drawing.Size(100, 23);
            this.label240.TabIndex = 33;
            this.label240.Text = "GRAPHS:";
            // 
            // _FA_A_Comment
            // 
            this._FA_A_Comment.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._FA_A_Comment.Location = new System.Drawing.Point(16, 384);
            this._FA_A_Comment.Multiline = true;
            this._FA_A_Comment.Name = "_FA_A_Comment";
            this._FA_A_Comment.Size = new System.Drawing.Size(232, 72);
            this._FA_A_Comment.TabIndex = 18;
            // 
            // label239
            // 
            this.label239.Location = new System.Drawing.Point(16, 360);
            this.label239.Name = "label239";
            this.label239.Size = new System.Drawing.Size(120, 23);
            this.label239.TabIndex = 31;
            this.label239.Text = "Comments:";
            this.label239.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label238
            // 
            this.label238.Location = new System.Drawing.Point(8, 330);
            this.label238.Name = "label238";
            this.label238.Size = new System.Drawing.Size(120, 23);
            this.label238.TabIndex = 30;
            this.label238.Text = "Cash on Cash Return:";
            this.label238.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label237
            // 
            this.label237.Location = new System.Drawing.Point(8, 308);
            this.label237.Name = "label237";
            this.label237.Size = new System.Drawing.Size(120, 23);
            this.label237.TabIndex = 29;
            this.label237.Text = "Cash Flow (pretax):";
            this.label237.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label236
            // 
            this.label236.Location = new System.Drawing.Point(8, 288);
            this.label236.Name = "label236";
            this.label236.Size = new System.Drawing.Size(120, 23);
            this.label236.TabIndex = 28;
            this.label236.Text = "Debt Coverage Ratio:";
            this.label236.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label235
            // 
            this.label235.Location = new System.Drawing.Point(8, 270);
            this.label235.Name = "label235";
            this.label235.Size = new System.Drawing.Size(120, 23);
            this.label235.TabIndex = 27;
            this.label235.Text = "Debt Service:";
            this.label235.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label234
            // 
            this.label234.Location = new System.Drawing.Point(8, 249);
            this.label234.Name = "label234";
            this.label234.Size = new System.Drawing.Size(120, 23);
            this.label234.TabIndex = 26;
            this.label234.Text = "Cap Rate:";
            this.label234.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label233
            // 
            this.label233.Location = new System.Drawing.Point(8, 229);
            this.label233.Name = "label233";
            this.label233.Size = new System.Drawing.Size(120, 23);
            this.label233.TabIndex = 25;
            this.label233.Text = "Net Operating Income:";
            this.label233.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label232
            // 
            this.label232.Location = new System.Drawing.Point(16, 208);
            this.label232.Name = "label232";
            this.label232.Size = new System.Drawing.Size(112, 23);
            this.label232.TabIndex = 24;
            this.label232.Text = "Operating Expenses:";
            this.label232.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label231
            // 
            this.label231.Location = new System.Drawing.Point(16, 190);
            this.label231.Name = "label231";
            this.label231.Size = new System.Drawing.Size(112, 23);
            this.label231.TabIndex = 23;
            this.label231.Text = "GOI (Annual):";
            this.label231.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label230
            // 
            this.label230.Location = new System.Drawing.Point(16, 170);
            this.label230.Name = "label230";
            this.label230.Size = new System.Drawing.Size(112, 23);
            this.label230.TabIndex = 22;
            this.label230.Text = "Other Income:";
            this.label230.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label229
            // 
            this.label229.Location = new System.Drawing.Point(264, 155);
            this.label229.Name = "label229";
            this.label229.Size = new System.Drawing.Size(16, 23);
            this.label229.TabIndex = 21;
            this.label229.Text = "%";
            // 
            // _FA_A_VacancyCreditLossPerc
            // 
            this._FA_A_VacancyCreditLossPerc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._FA_A_VacancyCreditLossPerc.Location = new System.Drawing.Point(224, 152);
            this._FA_A_VacancyCreditLossPerc.Name = "_FA_A_VacancyCreditLossPerc";
            this._FA_A_VacancyCreditLossPerc.Size = new System.Drawing.Size(40, 20);
            this._FA_A_VacancyCreditLossPerc.TabIndex = 7;
            this._FA_A_VacancyCreditLossPerc.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label228
            // 
            this.label228.Location = new System.Drawing.Point(16, 149);
            this.label228.Name = "label228";
            this.label228.Size = new System.Drawing.Size(112, 23);
            this.label228.TabIndex = 19;
            this.label228.Text = "Vacancy/Credit Loss:";
            this.label228.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label227
            // 
            this.label227.Location = new System.Drawing.Point(28, 128);
            this.label227.Name = "label227";
            this.label227.Size = new System.Drawing.Size(100, 23);
            this.label227.TabIndex = 18;
            this.label227.Text = "GSI (Annual):";
            this.label227.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label226
            // 
            this.label226.Location = new System.Drawing.Point(28, 109);
            this.label226.Name = "label226";
            this.label226.Size = new System.Drawing.Size(100, 23);
            this.label226.TabIndex = 17;
            this.label226.Text = "Price/Value:";
            this.label226.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _FA_A_CashOnCashReturn
            // 
            this._FA_A_CashOnCashReturn.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._FA_A_CashOnCashReturn.Location = new System.Drawing.Point(144, 332);
            this._FA_A_CashOnCashReturn.Name = "_FA_A_CashOnCashReturn";
            this._FA_A_CashOnCashReturn.ReadOnly = true;
            this._FA_A_CashOnCashReturn.Size = new System.Drawing.Size(80, 20);
            this._FA_A_CashOnCashReturn.TabIndex = 17;
            this._FA_A_CashOnCashReturn.TabStop = false;
            // 
            // _FA_A_CashFlow
            // 
            this._FA_A_CashFlow.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._FA_A_CashFlow.Location = new System.Drawing.Point(144, 312);
            this._FA_A_CashFlow.Name = "_FA_A_CashFlow";
            this._FA_A_CashFlow.ReadOnly = true;
            this._FA_A_CashFlow.Size = new System.Drawing.Size(80, 20);
            this._FA_A_CashFlow.TabIndex = 16;
            this._FA_A_CashFlow.TabStop = false;
            // 
            // _FA_A_DebtCoverageRatio
            // 
            this._FA_A_DebtCoverageRatio.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._FA_A_DebtCoverageRatio.Location = new System.Drawing.Point(144, 292);
            this._FA_A_DebtCoverageRatio.Name = "_FA_A_DebtCoverageRatio";
            this._FA_A_DebtCoverageRatio.ReadOnly = true;
            this._FA_A_DebtCoverageRatio.Size = new System.Drawing.Size(80, 20);
            this._FA_A_DebtCoverageRatio.TabIndex = 15;
            this._FA_A_DebtCoverageRatio.TabStop = false;
            // 
            // _FA_A_DebtService
            // 
            this._FA_A_DebtService.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._FA_A_DebtService.Location = new System.Drawing.Point(144, 272);
            this._FA_A_DebtService.Name = "_FA_A_DebtService";
            this._FA_A_DebtService.Size = new System.Drawing.Size(80, 20);
            this._FA_A_DebtService.TabIndex = 14;
            // 
            // _FA_A_CapRate
            // 
            this._FA_A_CapRate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._FA_A_CapRate.Location = new System.Drawing.Point(144, 252);
            this._FA_A_CapRate.Name = "_FA_A_CapRate";
            this._FA_A_CapRate.Size = new System.Drawing.Size(80, 20);
            this._FA_A_CapRate.TabIndex = 13;
            // 
            // _FA_A_NetOperatingIncome
            // 
            this._FA_A_NetOperatingIncome.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._FA_A_NetOperatingIncome.Location = new System.Drawing.Point(144, 232);
            this._FA_A_NetOperatingIncome.Name = "_FA_A_NetOperatingIncome";
            this._FA_A_NetOperatingIncome.ReadOnly = true;
            this._FA_A_NetOperatingIncome.Size = new System.Drawing.Size(80, 20);
            this._FA_A_NetOperatingIncome.TabIndex = 12;
            this._FA_A_NetOperatingIncome.TabStop = false;
            // 
            // _FA_A_OperatingExpenses
            // 
            this._FA_A_OperatingExpenses.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._FA_A_OperatingExpenses.Location = new System.Drawing.Point(144, 212);
            this._FA_A_OperatingExpenses.Name = "_FA_A_OperatingExpenses";
            this._FA_A_OperatingExpenses.Size = new System.Drawing.Size(80, 20);
            this._FA_A_OperatingExpenses.TabIndex = 10;
            // 
            // _FA_A_GOIAnnual
            // 
            this._FA_A_GOIAnnual.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._FA_A_GOIAnnual.Location = new System.Drawing.Point(144, 192);
            this._FA_A_GOIAnnual.Name = "_FA_A_GOIAnnual";
            this._FA_A_GOIAnnual.ReadOnly = true;
            this._FA_A_GOIAnnual.Size = new System.Drawing.Size(80, 20);
            this._FA_A_GOIAnnual.TabIndex = 9;
            this._FA_A_GOIAnnual.TabStop = false;
            // 
            // _FA_A_OtherIncome
            // 
            this._FA_A_OtherIncome.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._FA_A_OtherIncome.Location = new System.Drawing.Point(144, 172);
            this._FA_A_OtherIncome.Name = "_FA_A_OtherIncome";
            this._FA_A_OtherIncome.Size = new System.Drawing.Size(80, 20);
            this._FA_A_OtherIncome.TabIndex = 8;
            // 
            // _FA_A_VacancyCreditLoss
            // 
            this._FA_A_VacancyCreditLoss.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._FA_A_VacancyCreditLoss.Location = new System.Drawing.Point(144, 152);
            this._FA_A_VacancyCreditLoss.Name = "_FA_A_VacancyCreditLoss";
            this._FA_A_VacancyCreditLoss.Size = new System.Drawing.Size(80, 20);
            this._FA_A_VacancyCreditLoss.TabIndex = 6;
            // 
            // _FA_A_GSIAnnual
            // 
            this._FA_A_GSIAnnual.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._FA_A_GSIAnnual.Location = new System.Drawing.Point(144, 132);
            this._FA_A_GSIAnnual.Name = "_FA_A_GSIAnnual";
            this._FA_A_GSIAnnual.Size = new System.Drawing.Size(80, 20);
            this._FA_A_GSIAnnual.TabIndex = 5;
            // 
            // _FA_A_Price
            // 
            this._FA_A_Price.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._FA_A_Price.Location = new System.Drawing.Point(144, 112);
            this._FA_A_Price.Name = "_FA_A_Price";
            this._FA_A_Price.Size = new System.Drawing.Size(80, 20);
            this._FA_A_Price.TabIndex = 4;
            // 
            // _FA_copyPDfromLoan
            // 
            this._FA_copyPDfromLoan.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._FA_copyPDfromLoan.Location = new System.Drawing.Point(16, 80);
            this._FA_copyPDfromLoan.Name = "_FA_copyPDfromLoan";
            this._FA_copyPDfromLoan.Size = new System.Drawing.Size(232, 23);
            this._FA_copyPDfromLoan.TabIndex = 3;
            this._FA_copyPDfromLoan.Text = "Copy Price and Debt from Loan Tab";
            // 
            // _FA_copyEfromExpenses
            // 
            this._FA_copyEfromExpenses.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._FA_copyEfromExpenses.Location = new System.Drawing.Point(16, 56);
            this._FA_copyEfromExpenses.Name = "_FA_copyEfromExpenses";
            this._FA_copyEfromExpenses.Size = new System.Drawing.Size(232, 23);
            this._FA_copyEfromExpenses.TabIndex = 2;
            this._FA_copyEfromExpenses.Text = "Copy EXPENSES from Expenses Tab";
            // 
            // _FA_copyAIfromIncome
            // 
            this._FA_copyAIfromIncome.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._FA_copyAIfromIncome.Location = new System.Drawing.Point(16, 32);
            this._FA_copyAIfromIncome.Name = "_FA_copyAIfromIncome";
            this._FA_copyAIfromIncome.Size = new System.Drawing.Size(232, 23);
            this._FA_copyAIfromIncome.TabIndex = 1;
            this._FA_copyAIfromIncome.Text = "Copy ACTUAL INCOME from Income Tab";
            // 
            // label225
            // 
            this.label225.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label225.Location = new System.Drawing.Point(16, 8);
            this.label225.Name = "label225";
            this.label225.Size = new System.Drawing.Size(100, 23);
            this.label225.TabIndex = 0;
            this.label225.Text = "ACTUAL:";
            // 
            // _multiYearAnalysis
            // 
            this._multiYearAnalysis.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this._multiYearAnalysis.Controls.Add(this.groupBox2);
            this._multiYearAnalysis.Controls.Add(this.groupBox1);
            this._multiYearAnalysis.Controls.Add(this._MA_S2_IncomePercChange);
            this._multiYearAnalysis.Controls.Add(this._MA_S2_VacancyPercChange);
            this._multiYearAnalysis.Controls.Add(this._MA_S2_OperatingExpensesPercChange);
            this._multiYearAnalysis.Controls.Add(this._MA_S2_InflationPercChange);
            this._multiYearAnalysis.Controls.Add(this._MA_TI_DepreciableYears);
            this._multiYearAnalysis.Controls.Add(this._MA_S1_OperatingExpensesPerc);
            this._multiYearAnalysis.Controls.Add(this.label365);
            this._multiYearAnalysis.Controls.Add(this._MA_S1_VacancyPerc);
            this._multiYearAnalysis.Controls.Add(this.label364);
            this._multiYearAnalysis.Controls.Add(this._MA_S3_OpEx_Y1);
            this._multiYearAnalysis.Controls.Add(this._MA_S3_OpEx_Y10);
            this._multiYearAnalysis.Controls.Add(this._MA_S3_OpEx_Y9);
            this._multiYearAnalysis.Controls.Add(this._MA_S3_OpEx_Y8);
            this._multiYearAnalysis.Controls.Add(this._MA_S3_OpEx_Y7);
            this._multiYearAnalysis.Controls.Add(this._MA_S3_OpEx_Y6);
            this._multiYearAnalysis.Controls.Add(this._MA_S3_OpEx_Y5);
            this._multiYearAnalysis.Controls.Add(this._MA_S3_OpEx_Y4);
            this._multiYearAnalysis.Controls.Add(this._MA_S3_OpEx_Y3);
            this._multiYearAnalysis.Controls.Add(this._MA_S3_OpEx_Y2);
            this._multiYearAnalysis.Controls.Add(this.label363);
            this._multiYearAnalysis.Controls.Add(this.label362);
            this._multiYearAnalysis.Controls.Add(this.label361);
            this._multiYearAnalysis.Controls.Add(this.label360);
            this._multiYearAnalysis.Controls.Add(this.label359);
            this._multiYearAnalysis.Controls.Add(this.label358);
            this._multiYearAnalysis.Controls.Add(this.label357);
            this._multiYearAnalysis.Controls.Add(this.label356);
            this._multiYearAnalysis.Controls.Add(this.label355);
            this._multiYearAnalysis.Controls.Add(this.label354);
            this._multiYearAnalysis.Controls.Add(this.label353);
            this._multiYearAnalysis.Controls.Add(this.label352);
            this._multiYearAnalysis.Controls.Add(this.label351);
            this._multiYearAnalysis.Controls.Add(this._MA_S3_IncomeChange_Y1);
            this._multiYearAnalysis.Controls.Add(this._MA_S3_Vacancy_Y1);
            this._multiYearAnalysis.Controls.Add(this._MA_S3_IncomeChange_Y10);
            this._multiYearAnalysis.Controls.Add(this._MA_S3_Vacancy_Y10);
            this._multiYearAnalysis.Controls.Add(this._MA_S3_IncomeChange_Y9);
            this._multiYearAnalysis.Controls.Add(this._MA_S3_Vacancy_Y9);
            this._multiYearAnalysis.Controls.Add(this._MA_S3_IncomeChange_Y8);
            this._multiYearAnalysis.Controls.Add(this._MA_S3_Vacancy_Y8);
            this._multiYearAnalysis.Controls.Add(this._MA_S3_IncomeChange_Y7);
            this._multiYearAnalysis.Controls.Add(this._MA_S3_Vacancy_Y7);
            this._multiYearAnalysis.Controls.Add(this._MA_S3_IncomeChange_Y6);
            this._multiYearAnalysis.Controls.Add(this._MA_S3_Vacancy_Y6);
            this._multiYearAnalysis.Controls.Add(this._MA_S3_IncomeChange_Y5);
            this._multiYearAnalysis.Controls.Add(this._MA_S3_Vacancy_Y5);
            this._multiYearAnalysis.Controls.Add(this._MA_S3_IncomeChange_Y4);
            this._multiYearAnalysis.Controls.Add(this._MA_S3_Vacancy_Y4);
            this._multiYearAnalysis.Controls.Add(this._MA_S3_IncomeChange_Y3);
            this._multiYearAnalysis.Controls.Add(this._MA_S3_Vacancy_Y3);
            this._multiYearAnalysis.Controls.Add(this._MA_S3_IncomeChange_Y2);
            this._multiYearAnalysis.Controls.Add(this._MA_S3_Vacancy_Y2);
            this._multiYearAnalysis.Controls.Add(this.label350);
            this._multiYearAnalysis.Controls.Add(this._MA_SI_SaleCapRate);
            this._multiYearAnalysis.Controls.Add(this.label346);
            this._multiYearAnalysis.Controls.Add(this.label347);
            this._multiYearAnalysis.Controls.Add(this._MA_SI_SaleCosts);
            this._multiYearAnalysis.Controls.Add(this.label348);
            this._multiYearAnalysis.Controls.Add(this.label349);
            this._multiYearAnalysis.Controls.Add(this.label345);
            this._multiYearAnalysis.Controls.Add(this.label344);
            this._multiYearAnalysis.Controls.Add(this.label343);
            this._multiYearAnalysis.Controls.Add(this.label342);
            this._multiYearAnalysis.Controls.Add(this.label341);
            this._multiYearAnalysis.Controls.Add(this.label340);
            this._multiYearAnalysis.Controls.Add(this._MA_RS_MARG_PrintSelectedReports);
            this._multiYearAnalysis.Controls.Add(this._MA_TI_BuildingValue);
            this._multiYearAnalysis.Controls.Add(this.label335);
            this._multiYearAnalysis.Controls.Add(this.label336);
            this._multiYearAnalysis.Controls.Add(this.label334);
            this._multiYearAnalysis.Controls.Add(this._MA_TI_CapitalGainsRate);
            this._multiYearAnalysis.Controls.Add(this.label332);
            this._multiYearAnalysis.Controls.Add(this.label333);
            this._multiYearAnalysis.Controls.Add(this._MA_TI_StateRate);
            this._multiYearAnalysis.Controls.Add(this.label330);
            this._multiYearAnalysis.Controls.Add(this.label331);
            this._multiYearAnalysis.Controls.Add(this._MA_TI_FederalRate);
            this._multiYearAnalysis.Controls.Add(this.label329);
            this._multiYearAnalysis.Controls.Add(this._MA_S1_OtherIncomePerc);
            this._multiYearAnalysis.Controls.Add(this._MA_S1_GSI_Perc);
            this._multiYearAnalysis.Controls.Add(this.label328);
            this._multiYearAnalysis.Controls.Add(this._MA_OtherAcquisitionCosts);
            this._multiYearAnalysis.Controls.Add(this._MA_Points);
            this._multiYearAnalysis.Controls.Add(this._MA_DownPayment);
            this._multiYearAnalysis.Controls.Add(this._MA_InitialInvestment);
            this._multiYearAnalysis.Controls.Add(this._MA_PurchasePrice);
            this._multiYearAnalysis.Controls.Add(this._MA_S1_OperatingExpenses);
            this._multiYearAnalysis.Controls.Add(this._MA_S1_OtherIncome);
            this._multiYearAnalysis.Controls.Add(this._MA_S1_Vacancy);
            this._multiYearAnalysis.Controls.Add(this._MA_S1_GSI);
            this._multiYearAnalysis.Controls.Add(this.label327);
            this._multiYearAnalysis.Controls.Add(this.label326);
            this._multiYearAnalysis.Controls.Add(this.label325);
            this._multiYearAnalysis.Controls.Add(this.label324);
            this._multiYearAnalysis.Controls.Add(this.label323);
            this._multiYearAnalysis.Controls.Add(this.label321);
            this._multiYearAnalysis.Controls.Add(this.label319);
            this._multiYearAnalysis.Controls.Add(this.label317);
            this._multiYearAnalysis.Controls.Add(this.label314);
            this._multiYearAnalysis.Controls.Add(this.label311);
            this._multiYearAnalysis.Controls.Add(this._MA_CopyFromProforma);
            this._multiYearAnalysis.Controls.Add(this._MA_CopyFromActual);
            this._multiYearAnalysis.Controls.Add(this.label309);
            this._multiYearAnalysis.Controls.Add(this.label307);
            this._multiYearAnalysis.Controls.Add(this.label297);
            this._multiYearAnalysis.Controls.Add(this.label65);
            this._multiYearAnalysis.Controls.Add(this.label64);
            this._multiYearAnalysis.Controls.Add(this.label63);
            this._multiYearAnalysis.Controls.Add(this.label62);
            this._multiYearAnalysis.Controls.Add(this.label61);
            this._multiYearAnalysis.Controls.Add(this.label60);
            this._multiYearAnalysis.Controls.Add(this.label59);
            this._multiYearAnalysis.Controls.Add(this.label58);
            this._multiYearAnalysis.Controls.Add(this.label57);
            this._multiYearAnalysis.Controls.Add(this.label56);
            this._multiYearAnalysis.Controls.Add(this.label55);
            this._multiYearAnalysis.Controls.Add(this._ma_copyFromLoanTab);
            this._multiYearAnalysis.Controls.Add(this.label54);
            this._multiYearAnalysis.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this._multiYearAnalysis.Location = new System.Drawing.Point(4, 22);
            this._multiYearAnalysis.Name = "_multiYearAnalysis";
            this._multiYearAnalysis.Size = new System.Drawing.Size(897, 529);
            this._multiYearAnalysis.TabIndex = 5;
            this._multiYearAnalysis.Text = "Multiyear Analysis";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label339);
            this.groupBox2.Controls.Add(this._MA_RS_MARG_AnalysisPeriod);
            this.groupBox2.Controls.Add(this._MA_RS_MARG_SelectAll);
            this.groupBox2.Controls.Add(this._MA_RS_MARG_CashFlow);
            this.groupBox2.Controls.Add(this._MA_RS_MARG_CashFlowGraph);
            this.groupBox2.Controls.Add(this._MA_RS_MARG_NetProfitFromSale);
            this.groupBox2.Controls.Add(this._MA_RS_MARG_NetProfitGraph);
            this.groupBox2.Controls.Add(this._MA_RS_MARG_TaxesAndDeductions);
            this.groupBox2.Location = new System.Drawing.Point(638, 254);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(220, 161);
            this.groupBox2.TabIndex = 49;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Multiyear Analysis Reports and Graphs";
            // 
            // label339
            // 
            this.label339.AutoSize = true;
            this.label339.Location = new System.Drawing.Point(30, 16);
            this.label339.Name = "label339";
            this.label339.Size = new System.Drawing.Size(78, 13);
            this.label339.TabIndex = 68;
            this.label339.Text = "Analysis Period";
            // 
            // _MA_RS_MARG_AnalysisPeriod
            // 
            this._MA_RS_MARG_AnalysisPeriod.FormattingEnabled = true;
            this._MA_RS_MARG_AnalysisPeriod.Location = new System.Drawing.Point(33, 32);
            this._MA_RS_MARG_AnalysisPeriod.Name = "_MA_RS_MARG_AnalysisPeriod";
            this._MA_RS_MARG_AnalysisPeriod.Size = new System.Drawing.Size(68, 21);
            this._MA_RS_MARG_AnalysisPeriod.TabIndex = 50;
            // 
            // _MA_RS_MARG_SelectAll
            // 
            this._MA_RS_MARG_SelectAll.AutoSize = true;
            this._MA_RS_MARG_SelectAll.Checked = true;
            this._MA_RS_MARG_SelectAll.CheckState = System.Windows.Forms.CheckState.Checked;
            this._MA_RS_MARG_SelectAll.Enabled = false;
            this._MA_RS_MARG_SelectAll.Location = new System.Drawing.Point(144, 28);
            this._MA_RS_MARG_SelectAll.Name = "_MA_RS_MARG_SelectAll";
            this._MA_RS_MARG_SelectAll.Size = new System.Drawing.Size(70, 17);
            this._MA_RS_MARG_SelectAll.TabIndex = 51;
            this._MA_RS_MARG_SelectAll.Text = "Select All";
            this._MA_RS_MARG_SelectAll.UseVisualStyleBackColor = true;
            this._MA_RS_MARG_SelectAll.Visible = false;
            // 
            // _MA_RS_MARG_CashFlow
            // 
            this._MA_RS_MARG_CashFlow.AutoSize = true;
            this._MA_RS_MARG_CashFlow.Location = new System.Drawing.Point(16, 60);
            this._MA_RS_MARG_CashFlow.Name = "_MA_RS_MARG_CashFlow";
            this._MA_RS_MARG_CashFlow.Size = new System.Drawing.Size(191, 17);
            this._MA_RS_MARG_CashFlow.TabIndex = 5;
            this._MA_RS_MARG_CashFlow.Text = "Cash Flow (annual and cumulative)";
            this._MA_RS_MARG_CashFlow.UseVisualStyleBackColor = true;
            // 
            // _MA_RS_MARG_CashFlowGraph
            // 
            this._MA_RS_MARG_CashFlowGraph.AutoSize = true;
            this._MA_RS_MARG_CashFlowGraph.Location = new System.Drawing.Point(16, 79);
            this._MA_RS_MARG_CashFlowGraph.Name = "_MA_RS_MARG_CashFlowGraph";
            this._MA_RS_MARG_CashFlowGraph.Size = new System.Drawing.Size(116, 17);
            this._MA_RS_MARG_CashFlowGraph.TabIndex = 54;
            this._MA_RS_MARG_CashFlowGraph.Text = "Cash Flow GRAPH";
            this._MA_RS_MARG_CashFlowGraph.UseVisualStyleBackColor = true;
            // 
            // _MA_RS_MARG_NetProfitFromSale
            // 
            this._MA_RS_MARG_NetProfitFromSale.AutoSize = true;
            this._MA_RS_MARG_NetProfitFromSale.Location = new System.Drawing.Point(16, 99);
            this._MA_RS_MARG_NetProfitFromSale.Name = "_MA_RS_MARG_NetProfitFromSale";
            this._MA_RS_MARG_NetProfitFromSale.Size = new System.Drawing.Size(181, 17);
            this._MA_RS_MARG_NetProfitFromSale.TabIndex = 55;
            this._MA_RS_MARG_NetProfitFromSale.Text = "Net Profit from Sale (on any year)";
            this._MA_RS_MARG_NetProfitFromSale.UseVisualStyleBackColor = true;
            // 
            // _MA_RS_MARG_NetProfitGraph
            // 
            this._MA_RS_MARG_NetProfitGraph.AutoSize = true;
            this._MA_RS_MARG_NetProfitGraph.Location = new System.Drawing.Point(16, 117);
            this._MA_RS_MARG_NetProfitGraph.Name = "_MA_RS_MARG_NetProfitGraph";
            this._MA_RS_MARG_NetProfitGraph.Size = new System.Drawing.Size(111, 17);
            this._MA_RS_MARG_NetProfitGraph.TabIndex = 66;
            this._MA_RS_MARG_NetProfitGraph.Text = "Net Profit GRAPH";
            this._MA_RS_MARG_NetProfitGraph.UseVisualStyleBackColor = true;
            // 
            // _MA_RS_MARG_TaxesAndDeductions
            // 
            this._MA_RS_MARG_TaxesAndDeductions.AutoSize = true;
            this._MA_RS_MARG_TaxesAndDeductions.Location = new System.Drawing.Point(16, 136);
            this._MA_RS_MARG_TaxesAndDeductions.Name = "_MA_RS_MARG_TaxesAndDeductions";
            this._MA_RS_MARG_TaxesAndDeductions.Size = new System.Drawing.Size(174, 17);
            this._MA_RS_MARG_TaxesAndDeductions.TabIndex = 56;
            this._MA_RS_MARG_TaxesAndDeductions.Text = "Taxes and Deductions (annual)";
            this._MA_RS_MARG_TaxesAndDeductions.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this._MA_RS_IAR_SelectAll);
            this.groupBox1.Controls.Add(this._MA_RS_IAR_BuildingInfo);
            this.groupBox1.Controls.Add(this._MA_RS_IAR_InitialIncomeSummary);
            this.groupBox1.Controls.Add(this._MA_RS_IAR_InitialExpensesSummary);
            this.groupBox1.Controls.Add(this._MA_RS_IAR_LoanSummary);
            this.groupBox1.Controls.Add(this._MA_RS_IAR_FinancialAnalisys);
            this.groupBox1.Location = new System.Drawing.Point(638, 78);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(220, 119);
            this.groupBox1.TabIndex = 39;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Initial Analysis Reports";
            // 
            // _MA_RS_IAR_SelectAll
            // 
            this._MA_RS_IAR_SelectAll.AutoSize = true;
            this._MA_RS_IAR_SelectAll.Checked = true;
            this._MA_RS_IAR_SelectAll.CheckState = System.Windows.Forms.CheckState.Checked;
            this._MA_RS_IAR_SelectAll.Enabled = false;
            this._MA_RS_IAR_SelectAll.Location = new System.Drawing.Point(144, 19);
            this._MA_RS_IAR_SelectAll.Name = "_MA_RS_IAR_SelectAll";
            this._MA_RS_IAR_SelectAll.Size = new System.Drawing.Size(70, 17);
            this._MA_RS_IAR_SelectAll.TabIndex = 40;
            this._MA_RS_IAR_SelectAll.Text = "Select All";
            this._MA_RS_IAR_SelectAll.UseVisualStyleBackColor = true;
            this._MA_RS_IAR_SelectAll.Visible = false;
            // 
            // _MA_RS_IAR_BuildingInfo
            // 
            this._MA_RS_IAR_BuildingInfo.AutoSize = true;
            this._MA_RS_IAR_BuildingInfo.Location = new System.Drawing.Point(16, 20);
            this._MA_RS_IAR_BuildingInfo.Name = "_MA_RS_IAR_BuildingInfo";
            this._MA_RS_IAR_BuildingInfo.Size = new System.Drawing.Size(84, 17);
            this._MA_RS_IAR_BuildingInfo.TabIndex = 41;
            this._MA_RS_IAR_BuildingInfo.Text = "Building Info";
            this._MA_RS_IAR_BuildingInfo.UseVisualStyleBackColor = true;
            // 
            // _MA_RS_IAR_InitialIncomeSummary
            // 
            this._MA_RS_IAR_InitialIncomeSummary.AutoSize = true;
            this._MA_RS_IAR_InitialIncomeSummary.Location = new System.Drawing.Point(16, 39);
            this._MA_RS_IAR_InitialIncomeSummary.Name = "_MA_RS_IAR_InitialIncomeSummary";
            this._MA_RS_IAR_InitialIncomeSummary.Size = new System.Drawing.Size(134, 17);
            this._MA_RS_IAR_InitialIncomeSummary.TabIndex = 42;
            this._MA_RS_IAR_InitialIncomeSummary.Text = "Initial Income Summary";
            this._MA_RS_IAR_InitialIncomeSummary.UseVisualStyleBackColor = true;
            // 
            // _MA_RS_IAR_InitialExpensesSummary
            // 
            this._MA_RS_IAR_InitialExpensesSummary.AutoSize = true;
            this._MA_RS_IAR_InitialExpensesSummary.Location = new System.Drawing.Point(16, 59);
            this._MA_RS_IAR_InitialExpensesSummary.Name = "_MA_RS_IAR_InitialExpensesSummary";
            this._MA_RS_IAR_InitialExpensesSummary.Size = new System.Drawing.Size(145, 17);
            this._MA_RS_IAR_InitialExpensesSummary.TabIndex = 43;
            this._MA_RS_IAR_InitialExpensesSummary.Text = "Initial Expenses Summary";
            this._MA_RS_IAR_InitialExpensesSummary.UseVisualStyleBackColor = true;
            // 
            // _MA_RS_IAR_LoanSummary
            // 
            this._MA_RS_IAR_LoanSummary.AutoSize = true;
            this._MA_RS_IAR_LoanSummary.Location = new System.Drawing.Point(16, 77);
            this._MA_RS_IAR_LoanSummary.Name = "_MA_RS_IAR_LoanSummary";
            this._MA_RS_IAR_LoanSummary.Size = new System.Drawing.Size(96, 17);
            this._MA_RS_IAR_LoanSummary.TabIndex = 44;
            this._MA_RS_IAR_LoanSummary.Text = "Loan Summary";
            this._MA_RS_IAR_LoanSummary.UseVisualStyleBackColor = true;
            // 
            // _MA_RS_IAR_FinancialAnalisys
            // 
            this._MA_RS_IAR_FinancialAnalisys.AutoSize = true;
            this._MA_RS_IAR_FinancialAnalisys.Location = new System.Drawing.Point(16, 96);
            this._MA_RS_IAR_FinancialAnalisys.Name = "_MA_RS_IAR_FinancialAnalisys";
            this._MA_RS_IAR_FinancialAnalisys.Size = new System.Drawing.Size(109, 17);
            this._MA_RS_IAR_FinancialAnalisys.TabIndex = 45;
            this._MA_RS_IAR_FinancialAnalisys.Text = "Financial Analysis";
            this._MA_RS_IAR_FinancialAnalisys.UseVisualStyleBackColor = true;
            // 
            // _MA_S2_IncomePercChange
            // 
            this._MA_S2_IncomePercChange.FormattingEnabled = true;
            this._MA_S2_IncomePercChange.Location = new System.Drawing.Point(237, 375);
            this._MA_S2_IncomePercChange.Name = "_MA_S2_IncomePercChange";
            this._MA_S2_IncomePercChange.Size = new System.Drawing.Size(97, 21);
            this._MA_S2_IncomePercChange.TabIndex = 63;
            // 
            // _MA_S2_VacancyPercChange
            // 
            this._MA_S2_VacancyPercChange.FormattingEnabled = true;
            this._MA_S2_VacancyPercChange.Location = new System.Drawing.Point(237, 348);
            this._MA_S2_VacancyPercChange.Name = "_MA_S2_VacancyPercChange";
            this._MA_S2_VacancyPercChange.Size = new System.Drawing.Size(97, 21);
            this._MA_S2_VacancyPercChange.TabIndex = 62;
            // 
            // _MA_S2_OperatingExpensesPercChange
            // 
            this._MA_S2_OperatingExpensesPercChange.FormattingEnabled = true;
            this._MA_S2_OperatingExpensesPercChange.Location = new System.Drawing.Point(237, 321);
            this._MA_S2_OperatingExpensesPercChange.Name = "_MA_S2_OperatingExpensesPercChange";
            this._MA_S2_OperatingExpensesPercChange.Size = new System.Drawing.Size(97, 21);
            this._MA_S2_OperatingExpensesPercChange.TabIndex = 61;
            // 
            // _MA_S2_InflationPercChange
            // 
            this._MA_S2_InflationPercChange.FormattingEnabled = true;
            this._MA_S2_InflationPercChange.Location = new System.Drawing.Point(237, 294);
            this._MA_S2_InflationPercChange.Name = "_MA_S2_InflationPercChange";
            this._MA_S2_InflationPercChange.Size = new System.Drawing.Size(97, 21);
            this._MA_S2_InflationPercChange.TabIndex = 60;
            // 
            // _MA_TI_DepreciableYears
            // 
            this._MA_TI_DepreciableYears.FormattingEnabled = true;
            this._MA_TI_DepreciableYears.Items.AddRange(new object[] {
            "27.5",
            "39"});
            this._MA_TI_DepreciableYears.Location = new System.Drawing.Point(436, 204);
            this._MA_TI_DepreciableYears.Name = "_MA_TI_DepreciableYears";
            this._MA_TI_DepreciableYears.Size = new System.Drawing.Size(68, 21);
            this._MA_TI_DepreciableYears.TabIndex = 33;
            // 
            // _MA_S1_OperatingExpensesPerc
            // 
            this._MA_S1_OperatingExpensesPerc.Location = new System.Drawing.Point(223, 229);
            this._MA_S1_OperatingExpensesPerc.Name = "_MA_S1_OperatingExpensesPerc";
            this._MA_S1_OperatingExpensesPerc.Size = new System.Drawing.Size(41, 20);
            this._MA_S1_OperatingExpensesPerc.TabIndex = 19;
            // 
            // label365
            // 
            this.label365.AutoSize = true;
            this.label365.Location = new System.Drawing.Point(262, 232);
            this.label365.Name = "label365";
            this.label365.Size = new System.Drawing.Size(15, 13);
            this.label365.TabIndex = 133;
            this.label365.Text = "%";
            // 
            // _MA_S1_VacancyPerc
            // 
            this._MA_S1_VacancyPerc.Location = new System.Drawing.Point(223, 177);
            this._MA_S1_VacancyPerc.Name = "_MA_S1_VacancyPerc";
            this._MA_S1_VacancyPerc.Size = new System.Drawing.Size(41, 20);
            this._MA_S1_VacancyPerc.TabIndex = 15;
            // 
            // label364
            // 
            this.label364.AutoSize = true;
            this.label364.Location = new System.Drawing.Point(262, 180);
            this.label364.Name = "label364";
            this.label364.Size = new System.Drawing.Size(15, 13);
            this.label364.TabIndex = 131;
            this.label364.Text = "%";
            // 
            // _MA_S3_OpEx_Y1
            // 
            this._MA_S3_OpEx_Y1.Location = new System.Drawing.Point(151, 450);
            this._MA_S3_OpEx_Y1.Name = "_MA_S3_OpEx_Y1";
            this._MA_S3_OpEx_Y1.Size = new System.Drawing.Size(64, 20);
            this._MA_S3_OpEx_Y1.TabIndex = 80;
            // 
            // _MA_S3_OpEx_Y10
            // 
            this._MA_S3_OpEx_Y10.Location = new System.Drawing.Point(781, 450);
            this._MA_S3_OpEx_Y10.Name = "_MA_S3_OpEx_Y10";
            this._MA_S3_OpEx_Y10.Size = new System.Drawing.Size(64, 20);
            this._MA_S3_OpEx_Y10.TabIndex = 107;
            // 
            // _MA_S3_OpEx_Y9
            // 
            this._MA_S3_OpEx_Y9.Location = new System.Drawing.Point(711, 450);
            this._MA_S3_OpEx_Y9.Name = "_MA_S3_OpEx_Y9";
            this._MA_S3_OpEx_Y9.Size = new System.Drawing.Size(64, 20);
            this._MA_S3_OpEx_Y9.TabIndex = 104;
            // 
            // _MA_S3_OpEx_Y8
            // 
            this._MA_S3_OpEx_Y8.Location = new System.Drawing.Point(641, 450);
            this._MA_S3_OpEx_Y8.Name = "_MA_S3_OpEx_Y8";
            this._MA_S3_OpEx_Y8.Size = new System.Drawing.Size(64, 20);
            this._MA_S3_OpEx_Y8.TabIndex = 101;
            // 
            // _MA_S3_OpEx_Y7
            // 
            this._MA_S3_OpEx_Y7.Location = new System.Drawing.Point(571, 450);
            this._MA_S3_OpEx_Y7.Name = "_MA_S3_OpEx_Y7";
            this._MA_S3_OpEx_Y7.Size = new System.Drawing.Size(64, 20);
            this._MA_S3_OpEx_Y7.TabIndex = 98;
            // 
            // _MA_S3_OpEx_Y6
            // 
            this._MA_S3_OpEx_Y6.Location = new System.Drawing.Point(501, 450);
            this._MA_S3_OpEx_Y6.Name = "_MA_S3_OpEx_Y6";
            this._MA_S3_OpEx_Y6.Size = new System.Drawing.Size(64, 20);
            this._MA_S3_OpEx_Y6.TabIndex = 95;
            // 
            // _MA_S3_OpEx_Y5
            // 
            this._MA_S3_OpEx_Y5.Location = new System.Drawing.Point(431, 450);
            this._MA_S3_OpEx_Y5.Name = "_MA_S3_OpEx_Y5";
            this._MA_S3_OpEx_Y5.Size = new System.Drawing.Size(64, 20);
            this._MA_S3_OpEx_Y5.TabIndex = 92;
            // 
            // _MA_S3_OpEx_Y4
            // 
            this._MA_S3_OpEx_Y4.Location = new System.Drawing.Point(361, 450);
            this._MA_S3_OpEx_Y4.Name = "_MA_S3_OpEx_Y4";
            this._MA_S3_OpEx_Y4.Size = new System.Drawing.Size(64, 20);
            this._MA_S3_OpEx_Y4.TabIndex = 89;
            // 
            // _MA_S3_OpEx_Y3
            // 
            this._MA_S3_OpEx_Y3.Location = new System.Drawing.Point(291, 450);
            this._MA_S3_OpEx_Y3.Name = "_MA_S3_OpEx_Y3";
            this._MA_S3_OpEx_Y3.Size = new System.Drawing.Size(64, 20);
            this._MA_S3_OpEx_Y3.TabIndex = 86;
            // 
            // _MA_S3_OpEx_Y2
            // 
            this._MA_S3_OpEx_Y2.Location = new System.Drawing.Point(221, 450);
            this._MA_S3_OpEx_Y2.Name = "_MA_S3_OpEx_Y2";
            this._MA_S3_OpEx_Y2.Size = new System.Drawing.Size(64, 20);
            this._MA_S3_OpEx_Y2.TabIndex = 83;
            // 
            // label363
            // 
            this.label363.AutoSize = true;
            this.label363.Location = new System.Drawing.Point(49, 505);
            this.label363.Name = "label363";
            this.label363.Size = new System.Drawing.Size(96, 13);
            this.label363.TabIndex = 129;
            this.label363.Text = "% Income Change:";
            this.label363.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label362
            // 
            this.label362.AutoSize = true;
            this.label362.Location = new System.Drawing.Point(82, 479);
            this.label362.Name = "label362";
            this.label362.Size = new System.Drawing.Size(63, 13);
            this.label362.TabIndex = 128;
            this.label362.Text = "% Vacancy:";
            this.label362.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label361
            // 
            this.label361.AutoSize = true;
            this.label361.Location = new System.Drawing.Point(34, 453);
            this.label361.Name = "label361";
            this.label361.Size = new System.Drawing.Size(116, 13);
            this.label361.TabIndex = 127;
            this.label361.Text = "% Operating Expenses:";
            this.label361.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label360
            // 
            this.label360.AutoSize = true;
            this.label360.Location = new System.Drawing.Point(790, 434);
            this.label360.Name = "label360";
            this.label360.Size = new System.Drawing.Size(47, 13);
            this.label360.TabIndex = 126;
            this.label360.Text = "Year 10:";
            this.label360.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label359
            // 
            this.label359.AutoSize = true;
            this.label359.Location = new System.Drawing.Point(721, 434);
            this.label359.Name = "label359";
            this.label359.Size = new System.Drawing.Size(41, 13);
            this.label359.TabIndex = 125;
            this.label359.Text = "Year 9:";
            this.label359.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label358
            // 
            this.label358.AutoSize = true;
            this.label358.Location = new System.Drawing.Point(652, 434);
            this.label358.Name = "label358";
            this.label358.Size = new System.Drawing.Size(41, 13);
            this.label358.TabIndex = 124;
            this.label358.Text = "Year 8:";
            this.label358.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label357
            // 
            this.label357.AutoSize = true;
            this.label357.Location = new System.Drawing.Point(582, 434);
            this.label357.Name = "label357";
            this.label357.Size = new System.Drawing.Size(41, 13);
            this.label357.TabIndex = 123;
            this.label357.Text = "Year 7:";
            this.label357.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label356
            // 
            this.label356.AutoSize = true;
            this.label356.Location = new System.Drawing.Point(513, 434);
            this.label356.Name = "label356";
            this.label356.Size = new System.Drawing.Size(41, 13);
            this.label356.TabIndex = 122;
            this.label356.Text = "Year 6:";
            this.label356.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label355
            // 
            this.label355.AutoSize = true;
            this.label355.Location = new System.Drawing.Point(444, 434);
            this.label355.Name = "label355";
            this.label355.Size = new System.Drawing.Size(41, 13);
            this.label355.TabIndex = 121;
            this.label355.Text = "Year 5:";
            this.label355.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label354
            // 
            this.label354.AutoSize = true;
            this.label354.Location = new System.Drawing.Point(373, 434);
            this.label354.Name = "label354";
            this.label354.Size = new System.Drawing.Size(41, 13);
            this.label354.TabIndex = 120;
            this.label354.Text = "Year 4:";
            this.label354.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label353
            // 
            this.label353.AutoSize = true;
            this.label353.Location = new System.Drawing.Point(308, 434);
            this.label353.Name = "label353";
            this.label353.Size = new System.Drawing.Size(41, 13);
            this.label353.TabIndex = 119;
            this.label353.Text = "Year 3:";
            this.label353.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label352
            // 
            this.label352.AutoSize = true;
            this.label352.Location = new System.Drawing.Point(233, 434);
            this.label352.Name = "label352";
            this.label352.Size = new System.Drawing.Size(41, 13);
            this.label352.TabIndex = 118;
            this.label352.Text = "Year 2:";
            this.label352.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label351
            // 
            this.label351.AutoSize = true;
            this.label351.Location = new System.Drawing.Point(162, 434);
            this.label351.Name = "label351";
            this.label351.Size = new System.Drawing.Size(41, 13);
            this.label351.TabIndex = 117;
            this.label351.Text = "Year 1:";
            this.label351.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // _MA_S3_IncomeChange_Y1
            // 
            this._MA_S3_IncomeChange_Y1.Location = new System.Drawing.Point(151, 502);
            this._MA_S3_IncomeChange_Y1.Name = "_MA_S3_IncomeChange_Y1";
            this._MA_S3_IncomeChange_Y1.Size = new System.Drawing.Size(64, 20);
            this._MA_S3_IncomeChange_Y1.TabIndex = 82;
            // 
            // _MA_S3_Vacancy_Y1
            // 
            this._MA_S3_Vacancy_Y1.Location = new System.Drawing.Point(151, 476);
            this._MA_S3_Vacancy_Y1.Name = "_MA_S3_Vacancy_Y1";
            this._MA_S3_Vacancy_Y1.Size = new System.Drawing.Size(64, 20);
            this._MA_S3_Vacancy_Y1.TabIndex = 81;
            // 
            // _MA_S3_IncomeChange_Y10
            // 
            this._MA_S3_IncomeChange_Y10.Location = new System.Drawing.Point(781, 502);
            this._MA_S3_IncomeChange_Y10.Name = "_MA_S3_IncomeChange_Y10";
            this._MA_S3_IncomeChange_Y10.Size = new System.Drawing.Size(64, 20);
            this._MA_S3_IncomeChange_Y10.TabIndex = 109;
            // 
            // _MA_S3_Vacancy_Y10
            // 
            this._MA_S3_Vacancy_Y10.Location = new System.Drawing.Point(781, 476);
            this._MA_S3_Vacancy_Y10.Name = "_MA_S3_Vacancy_Y10";
            this._MA_S3_Vacancy_Y10.Size = new System.Drawing.Size(64, 20);
            this._MA_S3_Vacancy_Y10.TabIndex = 108;
            // 
            // _MA_S3_IncomeChange_Y9
            // 
            this._MA_S3_IncomeChange_Y9.Location = new System.Drawing.Point(711, 502);
            this._MA_S3_IncomeChange_Y9.Name = "_MA_S3_IncomeChange_Y9";
            this._MA_S3_IncomeChange_Y9.Size = new System.Drawing.Size(64, 20);
            this._MA_S3_IncomeChange_Y9.TabIndex = 106;
            // 
            // _MA_S3_Vacancy_Y9
            // 
            this._MA_S3_Vacancy_Y9.Location = new System.Drawing.Point(711, 476);
            this._MA_S3_Vacancy_Y9.Name = "_MA_S3_Vacancy_Y9";
            this._MA_S3_Vacancy_Y9.Size = new System.Drawing.Size(64, 20);
            this._MA_S3_Vacancy_Y9.TabIndex = 105;
            // 
            // _MA_S3_IncomeChange_Y8
            // 
            this._MA_S3_IncomeChange_Y8.Location = new System.Drawing.Point(641, 502);
            this._MA_S3_IncomeChange_Y8.Name = "_MA_S3_IncomeChange_Y8";
            this._MA_S3_IncomeChange_Y8.Size = new System.Drawing.Size(64, 20);
            this._MA_S3_IncomeChange_Y8.TabIndex = 103;
            // 
            // _MA_S3_Vacancy_Y8
            // 
            this._MA_S3_Vacancy_Y8.Location = new System.Drawing.Point(641, 476);
            this._MA_S3_Vacancy_Y8.Name = "_MA_S3_Vacancy_Y8";
            this._MA_S3_Vacancy_Y8.Size = new System.Drawing.Size(64, 20);
            this._MA_S3_Vacancy_Y8.TabIndex = 102;
            // 
            // _MA_S3_IncomeChange_Y7
            // 
            this._MA_S3_IncomeChange_Y7.Location = new System.Drawing.Point(571, 502);
            this._MA_S3_IncomeChange_Y7.Name = "_MA_S3_IncomeChange_Y7";
            this._MA_S3_IncomeChange_Y7.Size = new System.Drawing.Size(64, 20);
            this._MA_S3_IncomeChange_Y7.TabIndex = 100;
            // 
            // _MA_S3_Vacancy_Y7
            // 
            this._MA_S3_Vacancy_Y7.Location = new System.Drawing.Point(571, 476);
            this._MA_S3_Vacancy_Y7.Name = "_MA_S3_Vacancy_Y7";
            this._MA_S3_Vacancy_Y7.Size = new System.Drawing.Size(64, 20);
            this._MA_S3_Vacancy_Y7.TabIndex = 99;
            // 
            // _MA_S3_IncomeChange_Y6
            // 
            this._MA_S3_IncomeChange_Y6.Location = new System.Drawing.Point(501, 502);
            this._MA_S3_IncomeChange_Y6.Name = "_MA_S3_IncomeChange_Y6";
            this._MA_S3_IncomeChange_Y6.Size = new System.Drawing.Size(64, 20);
            this._MA_S3_IncomeChange_Y6.TabIndex = 97;
            // 
            // _MA_S3_Vacancy_Y6
            // 
            this._MA_S3_Vacancy_Y6.Location = new System.Drawing.Point(501, 476);
            this._MA_S3_Vacancy_Y6.Name = "_MA_S3_Vacancy_Y6";
            this._MA_S3_Vacancy_Y6.Size = new System.Drawing.Size(64, 20);
            this._MA_S3_Vacancy_Y6.TabIndex = 96;
            // 
            // _MA_S3_IncomeChange_Y5
            // 
            this._MA_S3_IncomeChange_Y5.Location = new System.Drawing.Point(431, 502);
            this._MA_S3_IncomeChange_Y5.Name = "_MA_S3_IncomeChange_Y5";
            this._MA_S3_IncomeChange_Y5.Size = new System.Drawing.Size(64, 20);
            this._MA_S3_IncomeChange_Y5.TabIndex = 94;
            // 
            // _MA_S3_Vacancy_Y5
            // 
            this._MA_S3_Vacancy_Y5.Location = new System.Drawing.Point(431, 476);
            this._MA_S3_Vacancy_Y5.Name = "_MA_S3_Vacancy_Y5";
            this._MA_S3_Vacancy_Y5.Size = new System.Drawing.Size(64, 20);
            this._MA_S3_Vacancy_Y5.TabIndex = 93;
            // 
            // _MA_S3_IncomeChange_Y4
            // 
            this._MA_S3_IncomeChange_Y4.Location = new System.Drawing.Point(361, 502);
            this._MA_S3_IncomeChange_Y4.Name = "_MA_S3_IncomeChange_Y4";
            this._MA_S3_IncomeChange_Y4.Size = new System.Drawing.Size(64, 20);
            this._MA_S3_IncomeChange_Y4.TabIndex = 91;
            // 
            // _MA_S3_Vacancy_Y4
            // 
            this._MA_S3_Vacancy_Y4.Location = new System.Drawing.Point(361, 476);
            this._MA_S3_Vacancy_Y4.Name = "_MA_S3_Vacancy_Y4";
            this._MA_S3_Vacancy_Y4.Size = new System.Drawing.Size(64, 20);
            this._MA_S3_Vacancy_Y4.TabIndex = 90;
            // 
            // _MA_S3_IncomeChange_Y3
            // 
            this._MA_S3_IncomeChange_Y3.Location = new System.Drawing.Point(291, 502);
            this._MA_S3_IncomeChange_Y3.Name = "_MA_S3_IncomeChange_Y3";
            this._MA_S3_IncomeChange_Y3.Size = new System.Drawing.Size(64, 20);
            this._MA_S3_IncomeChange_Y3.TabIndex = 88;
            // 
            // _MA_S3_Vacancy_Y3
            // 
            this._MA_S3_Vacancy_Y3.Location = new System.Drawing.Point(291, 476);
            this._MA_S3_Vacancy_Y3.Name = "_MA_S3_Vacancy_Y3";
            this._MA_S3_Vacancy_Y3.Size = new System.Drawing.Size(64, 20);
            this._MA_S3_Vacancy_Y3.TabIndex = 87;
            // 
            // _MA_S3_IncomeChange_Y2
            // 
            this._MA_S3_IncomeChange_Y2.Location = new System.Drawing.Point(221, 502);
            this._MA_S3_IncomeChange_Y2.Name = "_MA_S3_IncomeChange_Y2";
            this._MA_S3_IncomeChange_Y2.Size = new System.Drawing.Size(64, 20);
            this._MA_S3_IncomeChange_Y2.TabIndex = 85;
            // 
            // _MA_S3_Vacancy_Y2
            // 
            this._MA_S3_Vacancy_Y2.Location = new System.Drawing.Point(221, 476);
            this._MA_S3_Vacancy_Y2.Name = "_MA_S3_Vacancy_Y2";
            this._MA_S3_Vacancy_Y2.Size = new System.Drawing.Size(64, 20);
            this._MA_S3_Vacancy_Y2.TabIndex = 84;
            // 
            // label350
            // 
            this.label350.AutoSize = true;
            this.label350.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label350.Location = new System.Drawing.Point(13, 413);
            this.label350.Name = "label350";
            this.label350.Size = new System.Drawing.Size(168, 13);
            this.label350.TabIndex = 86;
            this.label350.Text = "Step 3: Adjust Changes If Needed";
            // 
            // _MA_SI_SaleCapRate
            // 
            this._MA_SI_SaleCapRate.Location = new System.Drawing.Point(435, 349);
            this._MA_SI_SaleCapRate.Name = "_MA_SI_SaleCapRate";
            this._MA_SI_SaleCapRate.Size = new System.Drawing.Size(68, 20);
            this._MA_SI_SaleCapRate.TabIndex = 65;
            // 
            // label346
            // 
            this.label346.AutoSize = true;
            this.label346.Location = new System.Drawing.Point(501, 352);
            this.label346.Name = "label346";
            this.label346.Size = new System.Drawing.Size(15, 13);
            this.label346.TabIndex = 85;
            this.label346.Text = "%";
            // 
            // label347
            // 
            this.label347.AutoSize = true;
            this.label347.Location = new System.Drawing.Point(432, 335);
            this.label347.Name = "label347";
            this.label347.Size = new System.Drawing.Size(79, 13);
            this.label347.TabIndex = 83;
            this.label347.Text = "Sale Cap Rate:";
            this.label347.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // _MA_SI_SaleCosts
            // 
            this._MA_SI_SaleCosts.Location = new System.Drawing.Point(435, 312);
            this._MA_SI_SaleCosts.Name = "_MA_SI_SaleCosts";
            this._MA_SI_SaleCosts.Size = new System.Drawing.Size(68, 20);
            this._MA_SI_SaleCosts.TabIndex = 64;
            // 
            // label348
            // 
            this.label348.AutoSize = true;
            this.label348.Location = new System.Drawing.Point(501, 315);
            this.label348.Name = "label348";
            this.label348.Size = new System.Drawing.Size(15, 13);
            this.label348.TabIndex = 82;
            this.label348.Text = "%";
            // 
            // label349
            // 
            this.label349.AutoSize = true;
            this.label349.Location = new System.Drawing.Point(432, 298);
            this.label349.Name = "label349";
            this.label349.Size = new System.Drawing.Size(60, 13);
            this.label349.TabIndex = 80;
            this.label349.Text = "Sale Costs:";
            this.label349.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label345
            // 
            this.label345.AutoSize = true;
            this.label345.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label345.Location = new System.Drawing.Point(432, 279);
            this.label345.Name = "label345";
            this.label345.Size = new System.Drawing.Size(128, 13);
            this.label345.TabIndex = 79;
            this.label345.Text = "SALE INFORMATION";
            // 
            // label344
            // 
            this.label344.AutoSize = true;
            this.label344.Location = new System.Drawing.Point(80, 378);
            this.label344.Name = "label344";
            this.label344.Size = new System.Drawing.Size(153, 13);
            this.label344.TabIndex = 78;
            this.label344.Text = "% of annual change in Income:";
            // 
            // label343
            // 
            this.label343.AutoSize = true;
            this.label343.Location = new System.Drawing.Point(73, 351);
            this.label343.Name = "label343";
            this.label343.Size = new System.Drawing.Size(160, 13);
            this.label343.TabIndex = 77;
            this.label343.Text = "% of annual change in Vacancy:";
            // 
            // label342
            // 
            this.label342.AutoSize = true;
            this.label342.Location = new System.Drawing.Point(15, 324);
            this.label342.Name = "label342";
            this.label342.Size = new System.Drawing.Size(213, 13);
            this.label342.TabIndex = 76;
            this.label342.Text = "% of annual change in Operating Expenses:";
            // 
            // label341
            // 
            this.label341.AutoSize = true;
            this.label341.Location = new System.Drawing.Point(128, 298);
            this.label341.Name = "label341";
            this.label341.Size = new System.Drawing.Size(105, 13);
            this.label341.TabIndex = 75;
            this.label341.Text = "% of annual Inflation:";
            // 
            // label340
            // 
            this.label340.AutoSize = true;
            this.label340.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label340.Location = new System.Drawing.Point(13, 275);
            this.label340.Name = "label340";
            this.label340.Size = new System.Drawing.Size(150, 13);
            this.label340.TabIndex = 70;
            this.label340.Text = "Step 2: Select Annual Change";
            // 
            // _MA_RS_MARG_PrintSelectedReports
            // 
            this._MA_RS_MARG_PrintSelectedReports.Location = new System.Drawing.Point(662, 204);
            this._MA_RS_MARG_PrintSelectedReports.Name = "_MA_RS_MARG_PrintSelectedReports";
            this._MA_RS_MARG_PrintSelectedReports.Size = new System.Drawing.Size(173, 27);
            this._MA_RS_MARG_PrintSelectedReports.TabIndex = 69;
            this._MA_RS_MARG_PrintSelectedReports.Text = "PRINT SELECTED REPORTS";
            this._MA_RS_MARG_PrintSelectedReports.UseVisualStyleBackColor = true;
            // 
            // _MA_TI_BuildingValue
            // 
            this._MA_TI_BuildingValue.Location = new System.Drawing.Point(435, 242);
            this._MA_TI_BuildingValue.Name = "_MA_TI_BuildingValue";
            this._MA_TI_BuildingValue.Size = new System.Drawing.Size(68, 20);
            this._MA_TI_BuildingValue.TabIndex = 34;
            // 
            // label335
            // 
            this.label335.AutoSize = true;
            this.label335.Location = new System.Drawing.Point(423, 245);
            this.label335.Name = "label335";
            this.label335.Size = new System.Drawing.Size(13, 13);
            this.label335.TabIndex = 53;
            this.label335.Text = "$";
            // 
            // label336
            // 
            this.label336.AutoSize = true;
            this.label336.Location = new System.Drawing.Point(432, 228);
            this.label336.Name = "label336";
            this.label336.Size = new System.Drawing.Size(143, 13);
            this.label336.TabIndex = 51;
            this.label336.Text = "Building (Depreciable) Value:";
            this.label336.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label334
            // 
            this.label334.AutoSize = true;
            this.label334.Location = new System.Drawing.Point(432, 190);
            this.label334.Name = "label334";
            this.label334.Size = new System.Drawing.Size(85, 13);
            this.label334.TabIndex = 49;
            this.label334.Text = "Depreciable Yrs:";
            this.label334.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // _MA_TI_CapitalGainsRate
            // 
            this._MA_TI_CapitalGainsRate.Location = new System.Drawing.Point(435, 166);
            this._MA_TI_CapitalGainsRate.Name = "_MA_TI_CapitalGainsRate";
            this._MA_TI_CapitalGainsRate.Size = new System.Drawing.Size(68, 20);
            this._MA_TI_CapitalGainsRate.TabIndex = 32;
            // 
            // label332
            // 
            this.label332.AutoSize = true;
            this.label332.Location = new System.Drawing.Point(501, 169);
            this.label332.Name = "label332";
            this.label332.Size = new System.Drawing.Size(15, 13);
            this.label332.TabIndex = 48;
            this.label332.Text = "%";
            // 
            // label333
            // 
            this.label333.AutoSize = true;
            this.label333.Location = new System.Drawing.Point(432, 152);
            this.label333.Name = "label333";
            this.label333.Size = new System.Drawing.Size(98, 13);
            this.label333.TabIndex = 46;
            this.label333.Text = "Capital Gains Rate:";
            this.label333.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // _MA_TI_StateRate
            // 
            this._MA_TI_StateRate.Location = new System.Drawing.Point(435, 128);
            this._MA_TI_StateRate.Name = "_MA_TI_StateRate";
            this._MA_TI_StateRate.Size = new System.Drawing.Size(68, 20);
            this._MA_TI_StateRate.TabIndex = 31;
            // 
            // label330
            // 
            this.label330.AutoSize = true;
            this.label330.Location = new System.Drawing.Point(501, 131);
            this.label330.Name = "label330";
            this.label330.Size = new System.Drawing.Size(15, 13);
            this.label330.TabIndex = 45;
            this.label330.Text = "%";
            // 
            // label331
            // 
            this.label331.AutoSize = true;
            this.label331.Location = new System.Drawing.Point(432, 114);
            this.label331.Name = "label331";
            this.label331.Size = new System.Drawing.Size(61, 13);
            this.label331.TabIndex = 43;
            this.label331.Text = "State Rate:";
            this.label331.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // _MA_TI_FederalRate
            // 
            this._MA_TI_FederalRate.Location = new System.Drawing.Point(435, 91);
            this._MA_TI_FederalRate.Name = "_MA_TI_FederalRate";
            this._MA_TI_FederalRate.Size = new System.Drawing.Size(68, 20);
            this._MA_TI_FederalRate.TabIndex = 30;
            // 
            // label329
            // 
            this.label329.AutoSize = true;
            this.label329.Location = new System.Drawing.Point(501, 94);
            this.label329.Name = "label329";
            this.label329.Size = new System.Drawing.Size(15, 13);
            this.label329.TabIndex = 42;
            this.label329.Text = "%";
            // 
            // _MA_S1_OtherIncomePerc
            // 
            this._MA_S1_OtherIncomePerc.Location = new System.Drawing.Point(223, 203);
            this._MA_S1_OtherIncomePerc.Name = "_MA_S1_OtherIncomePerc";
            this._MA_S1_OtherIncomePerc.Size = new System.Drawing.Size(41, 20);
            this._MA_S1_OtherIncomePerc.TabIndex = 17;
            this._MA_S1_OtherIncomePerc.Visible = false;
            // 
            // _MA_S1_GSI_Perc
            // 
            this._MA_S1_GSI_Perc.Location = new System.Drawing.Point(223, 151);
            this._MA_S1_GSI_Perc.Name = "_MA_S1_GSI_Perc";
            this._MA_S1_GSI_Perc.Size = new System.Drawing.Size(41, 20);
            this._MA_S1_GSI_Perc.TabIndex = 13;
            this._MA_S1_GSI_Perc.Visible = false;
            // 
            // label328
            // 
            this.label328.AutoSize = true;
            this.label328.Location = new System.Drawing.Point(432, 77);
            this.label328.Name = "label328";
            this.label328.Size = new System.Drawing.Size(71, 13);
            this.label328.TabIndex = 39;
            this.label328.Text = "Federal Rate:";
            this.label328.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // _MA_OtherAcquisitionCosts
            // 
            this._MA_OtherAcquisitionCosts.Location = new System.Drawing.Point(724, 27);
            this._MA_OtherAcquisitionCosts.Name = "_MA_OtherAcquisitionCosts";
            this._MA_OtherAcquisitionCosts.Size = new System.Drawing.Size(100, 20);
            this._MA_OtherAcquisitionCosts.TabIndex = 6;
            // 
            // _MA_Points
            // 
            this._MA_Points.Location = new System.Drawing.Point(585, 27);
            this._MA_Points.Name = "_MA_Points";
            this._MA_Points.Size = new System.Drawing.Size(100, 20);
            this._MA_Points.TabIndex = 5;
            // 
            // _MA_DownPayment
            // 
            this._MA_DownPayment.Location = new System.Drawing.Point(444, 27);
            this._MA_DownPayment.Name = "_MA_DownPayment";
            this._MA_DownPayment.Size = new System.Drawing.Size(100, 20);
            this._MA_DownPayment.TabIndex = 4;
            // 
            // _MA_InitialInvestment
            // 
            this._MA_InitialInvestment.Location = new System.Drawing.Point(311, 27);
            this._MA_InitialInvestment.Name = "_MA_InitialInvestment";
            this._MA_InitialInvestment.Size = new System.Drawing.Size(100, 20);
            this._MA_InitialInvestment.TabIndex = 3;
            // 
            // _MA_PurchasePrice
            // 
            this._MA_PurchasePrice.Location = new System.Drawing.Point(174, 27);
            this._MA_PurchasePrice.Name = "_MA_PurchasePrice";
            this._MA_PurchasePrice.Size = new System.Drawing.Size(100, 20);
            this._MA_PurchasePrice.TabIndex = 2;
            // 
            // _MA_S1_OperatingExpenses
            // 
            this._MA_S1_OperatingExpenses.Location = new System.Drawing.Point(131, 229);
            this._MA_S1_OperatingExpenses.Name = "_MA_S1_OperatingExpenses";
            this._MA_S1_OperatingExpenses.Size = new System.Drawing.Size(90, 20);
            this._MA_S1_OperatingExpenses.TabIndex = 18;
            // 
            // _MA_S1_OtherIncome
            // 
            this._MA_S1_OtherIncome.Location = new System.Drawing.Point(131, 203);
            this._MA_S1_OtherIncome.Name = "_MA_S1_OtherIncome";
            this._MA_S1_OtherIncome.Size = new System.Drawing.Size(90, 20);
            this._MA_S1_OtherIncome.TabIndex = 16;
            // 
            // _MA_S1_Vacancy
            // 
            this._MA_S1_Vacancy.Location = new System.Drawing.Point(131, 177);
            this._MA_S1_Vacancy.Name = "_MA_S1_Vacancy";
            this._MA_S1_Vacancy.Size = new System.Drawing.Size(90, 20);
            this._MA_S1_Vacancy.TabIndex = 14;
            // 
            // _MA_S1_GSI
            // 
            this._MA_S1_GSI.Location = new System.Drawing.Point(131, 151);
            this._MA_S1_GSI.Name = "_MA_S1_GSI";
            this._MA_S1_GSI.Size = new System.Drawing.Size(90, 20);
            this._MA_S1_GSI.TabIndex = 12;
            // 
            // label327
            // 
            this.label327.AutoSize = true;
            this.label327.Location = new System.Drawing.Point(12, 232);
            this.label327.Name = "label327";
            this.label327.Size = new System.Drawing.Size(105, 13);
            this.label327.TabIndex = 38;
            this.label327.Text = "Operating Expenses:";
            this.label327.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label326
            // 
            this.label326.AutoSize = true;
            this.label326.Location = new System.Drawing.Point(43, 206);
            this.label326.Name = "label326";
            this.label326.Size = new System.Drawing.Size(74, 13);
            this.label326.TabIndex = 37;
            this.label326.Text = "Other Income:";
            this.label326.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label325
            // 
            this.label325.AutoSize = true;
            this.label325.Location = new System.Drawing.Point(8, 180);
            this.label325.Name = "label325";
            this.label325.Size = new System.Drawing.Size(109, 13);
            this.label325.TabIndex = 36;
            this.label325.Text = "Vacancy/Credit Loss:";
            this.label325.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label324
            // 
            this.label324.AutoSize = true;
            this.label324.Location = new System.Drawing.Point(47, 154);
            this.label324.Name = "label324";
            this.label324.Size = new System.Drawing.Size(70, 13);
            this.label324.TabIndex = 35;
            this.label324.Text = "GSI (Annual):";
            this.label324.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label323
            // 
            this.label323.AutoSize = true;
            this.label323.Location = new System.Drawing.Point(262, 206);
            this.label323.Name = "label323";
            this.label323.Size = new System.Drawing.Size(15, 13);
            this.label323.TabIndex = 34;
            this.label323.Text = "%";
            this.label323.Visible = false;
            // 
            // label321
            // 
            this.label321.AutoSize = true;
            this.label321.Location = new System.Drawing.Point(262, 154);
            this.label321.Name = "label321";
            this.label321.Size = new System.Drawing.Size(15, 13);
            this.label321.TabIndex = 33;
            this.label321.Text = "%";
            this.label321.Visible = false;
            // 
            // label319
            // 
            this.label319.AutoSize = true;
            this.label319.Location = new System.Drawing.Point(119, 232);
            this.label319.Name = "label319";
            this.label319.Size = new System.Drawing.Size(13, 13);
            this.label319.TabIndex = 32;
            this.label319.Text = "$";
            // 
            // label317
            // 
            this.label317.AutoSize = true;
            this.label317.Location = new System.Drawing.Point(119, 206);
            this.label317.Name = "label317";
            this.label317.Size = new System.Drawing.Size(13, 13);
            this.label317.TabIndex = 31;
            this.label317.Text = "$";
            // 
            // label314
            // 
            this.label314.AutoSize = true;
            this.label314.Location = new System.Drawing.Point(119, 180);
            this.label314.Name = "label314";
            this.label314.Size = new System.Drawing.Size(13, 13);
            this.label314.TabIndex = 30;
            this.label314.Text = "$";
            // 
            // label311
            // 
            this.label311.AutoSize = true;
            this.label311.Location = new System.Drawing.Point(119, 154);
            this.label311.Name = "label311";
            this.label311.Size = new System.Drawing.Size(13, 13);
            this.label311.TabIndex = 29;
            this.label311.Text = "$";
            // 
            // _MA_CopyFromProforma
            // 
            this._MA_CopyFromProforma.Location = new System.Drawing.Point(7, 122);
            this._MA_CopyFromProforma.Name = "_MA_CopyFromProforma";
            this._MA_CopyFromProforma.Size = new System.Drawing.Size(267, 23);
            this._MA_CopyFromProforma.TabIndex = 11;
            this._MA_CopyFromProforma.Text = "Copy from PROFORMA on Financial Analysis tab";
            this._MA_CopyFromProforma.UseVisualStyleBackColor = true;
            // 
            // _MA_CopyFromActual
            // 
            this._MA_CopyFromActual.Location = new System.Drawing.Point(7, 93);
            this._MA_CopyFromActual.Name = "_MA_CopyFromActual";
            this._MA_CopyFromActual.Size = new System.Drawing.Size(267, 23);
            this._MA_CopyFromActual.TabIndex = 10;
            this._MA_CopyFromActual.Text = "Copy from ACTUAL on Financial Analysis tab";
            this._MA_CopyFromActual.UseVisualStyleBackColor = true;
            // 
            // label309
            // 
            this.label309.AutoSize = true;
            this.label309.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label309.Location = new System.Drawing.Point(13, 77);
            this.label309.Name = "label309";
            this.label309.Size = new System.Drawing.Size(243, 13);
            this.label309.TabIndex = 20;
            this.label309.Text = "Step 1: Enter or Copy Initial Income and Expenses";
            // 
            // label307
            // 
            this.label307.AutoSize = true;
            this.label307.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label307.Location = new System.Drawing.Point(638, 62);
            this.label307.Name = "label307";
            this.label307.Size = new System.Drawing.Size(163, 13);
            this.label307.TabIndex = 19;
            this.label307.Text = "REPORT SPECIFICATIONS";
            // 
            // label297
            // 
            this.label297.AutoSize = true;
            this.label297.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label297.Location = new System.Drawing.Point(423, 62);
            this.label297.Name = "label297";
            this.label297.Size = new System.Drawing.Size(121, 13);
            this.label297.TabIndex = 18;
            this.label297.Text = "TAX INFORMATION";
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label65.Location = new System.Drawing.Point(3, 62);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(263, 13);
            this.label65.TabIndex = 17;
            this.label65.Text = "ANNUAL INCOME AND EXPENSE CHANGES";
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Location = new System.Drawing.Point(721, 11);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(122, 13);
            this.label64.TabIndex = 16;
            this.label64.Text = "Other Acquistition Costs:";
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Location = new System.Drawing.Point(582, 11);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(39, 13);
            this.label63.TabIndex = 15;
            this.label63.Text = "Points:";
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Location = new System.Drawing.Point(441, 11);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(82, 13);
            this.label62.TabIndex = 14;
            this.label62.Text = "Down Payment:";
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Location = new System.Drawing.Point(308, 11);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(89, 13);
            this.label61.TabIndex = 13;
            this.label61.Text = "Initial Investment:";
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Location = new System.Drawing.Point(171, 11);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(82, 13);
            this.label60.TabIndex = 12;
            this.label60.Text = "Purchase Price:";
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Location = new System.Drawing.Point(711, 30);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(13, 13);
            this.label59.TabIndex = 11;
            this.label59.Text = "$";
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Location = new System.Drawing.Point(573, 30);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(13, 13);
            this.label58.TabIndex = 10;
            this.label58.Text = "$";
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Location = new System.Drawing.Point(432, 30);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(13, 13);
            this.label57.TabIndex = 9;
            this.label57.Text = "$";
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Location = new System.Drawing.Point(299, 30);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(13, 13);
            this.label56.TabIndex = 8;
            this.label56.Text = "$";
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Location = new System.Drawing.Point(162, 30);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(13, 13);
            this.label55.TabIndex = 7;
            this.label55.Text = "$";
            // 
            // _ma_copyFromLoanTab
            // 
            this._ma_copyFromLoanTab.Location = new System.Drawing.Point(6, 25);
            this._ma_copyFromLoanTab.Name = "_ma_copyFromLoanTab";
            this._ma_copyFromLoanTab.Size = new System.Drawing.Size(132, 23);
            this._ma_copyFromLoanTab.TabIndex = 1;
            this._ma_copyFromLoanTab.Text = "Copy from Loan Tab";
            this._ma_copyFromLoanTab.UseVisualStyleBackColor = true;
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label54.Location = new System.Drawing.Point(3, 9);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(119, 13);
            this.label54.TabIndex = 0;
            this.label54.Text = "PURCHASE COSTS";
            // 
            // income_Dummy1
            // 
            this.income_Dummy1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.income_Dummy1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.income_Dummy1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.income_Dummy1.Location = new System.Drawing.Point(0, 0);
            this.income_Dummy1.Name = "income_Dummy1";
            this.income_Dummy1.Size = new System.Drawing.Size(897, 529);
            this.income_Dummy1.TabIndex = 0;
            // 
            // _E_expenses
            // 
            this._E_expenses.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._E_expenses.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader13,
            this.columnHeader14,
            this.columnHeader15,
            this.columnHeader16,
            this.columnHeader17,
            this.columnHeader18,
            this.columnHeader19,
            this.columnHeader20,
            this.columnHeader21,
            this.columnHeader22});
            this._E_expenses.FullRowSelect = true;
            this._E_expenses.GridLines = true;
            this._E_expenses.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this._E_expenses.Location = new System.Drawing.Point(8, 99);
            this._E_expenses.MultiSelect = false;
            this._E_expenses.Name = "_E_expenses";
            this._E_expenses.Size = new System.Drawing.Size(880, 277);
            this._E_expenses.TabIndex = 42;
            this._E_expenses.UseCompatibleStateImageBehavior = false;
            this._E_expenses.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader13
            // 
            this.columnHeader13.Text = "Type:";
            this.columnHeader13.Width = 103;
            // 
            // columnHeader14
            // 
            this.columnHeader14.Text = "Description:";
            this.columnHeader14.Width = 118;
            // 
            // columnHeader15
            // 
            this.columnHeader15.Text = "$/month";
            // 
            // columnHeader16
            // 
            this.columnHeader16.Text = "$/year";
            // 
            // columnHeader17
            // 
            this.columnHeader17.Text = "$/SqFt (annual)";
            this.columnHeader17.Width = 86;
            // 
            // columnHeader18
            // 
            this.columnHeader18.Text = "$/Unit (annual)";
            this.columnHeader18.Width = 91;
            // 
            // columnHeader19
            // 
            this.columnHeader19.Text = "% of Total";
            this.columnHeader19.Width = 64;
            // 
            // columnHeader20
            // 
            this.columnHeader20.Text = "Paid By:";
            // 
            // columnHeader21
            // 
            this.columnHeader21.Text = "High / Low:";
            this.columnHeader21.Width = 70;
            // 
            // columnHeader22
            // 
            this.columnHeader22.Text = "Comment:";
            this.columnHeader22.Width = 140;
            // 
            // CommercialControl
            // 
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.Controls.Add(this._masterTabControl);
            this.Name = "CommercialControl";
            this.Size = new System.Drawing.Size(905, 555);
            this._masterTabControl.ResumeLayout(false);
            this._buildingInfoPage.ResumeLayout(false);
            this._buildingInfoPage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._bi_floater)).EndInit();
            this._generalIncome.ResumeLayout(false);
            this._expensesTabPage.ResumeLayout(false);
            this._expensesTabPage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._e_floater)).EndInit();
            this._loansTabPage.ResumeLayout(false);
            this._loansTabPage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._l_floater)).EndInit();
            this._financialAnalysisTabPage.ResumeLayout(false);
            this._financialAnalysisTabPage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._fa_floater)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._FA_LR_Range_NUD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._FA_OE_Range_NUD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._FA_GSI_Range_NUD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._FA_CR_Change_NUD)).EndInit();
            this._multiYearAnalysis.ResumeLayout(false);
            this._multiYearAnalysis.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

		}
		#endregion

		#region ADD EVENT HANDLERS
		public void AddEventHandlers()
		{    
            //Phase#3 - additional propertrty types
            _BI_propertyType.SelectedIndexChanged += new EventHandler(On_BI_propertyType_SelectedIndexChanged);
                    
            //REPORTS
            _printBuildingInfo.Click += new EventHandler(On_PrintReports_Click);
            //_printIncomeSummary.Click += new EventHandler(On_PrintReports_Click);
            //_printRentRoll.Click += new EventHandler(On_PrintReports_Click);
            _PrintExpensesDetails.Click += new EventHandler(On_PrintReports_Click);
            _printExpensesSummary.Click += new EventHandler(On_PrintReports_Click);
            _printFinancialAnalysis.Click += new EventHandler(On_PrintReports_Click);
            _printLoanInfo.Click += new EventHandler(On_PrintReports_Click);
            _MA_RS_MARG_PrintSelectedReports.Click += new EventHandler(On_MA_PrintSelectedReports_Click);

			//FLOATERS
			_bi_floater.MouseHover += new EventHandler(On_bi_floater_MouseHover);
            //_i_floater.MouseHover += new EventHandler(On_i_floater_MouseHover);
			_e_floater.MouseHover += new EventHandler(On_e_floater_MouseHover);
			_l_floater.MouseHover += new EventHandler(On_l_floater_MouseHover);
			_fa_floater.MouseHover += new EventHandler(On_fa_floater_MouseHover);

			_bi_floater.MouseLeave += new EventHandler(On_bi_floater_MouseLeave);
            //_i_floater.MouseLeave += new EventHandler(On_i_floater_MouseLeave);
			_e_floater.MouseLeave += new EventHandler(On_e_floater_MouseLeave);
			_l_floater.MouseLeave += new EventHandler(On_l_floater_MouseLeave);
			_fa_floater.MouseLeave += new EventHandler(On_fa_floater_MouseLeave);
			//FLOATERS END

			_BI_propertyValue.KeyDown += new KeyEventHandler(On_BI_propertyValue_KeyDown);
			_BI_propertyValue.LostFocus += new EventHandler(On_BI_propertyValue_LostFocus);

            //_CalculateFromUnitTypeSummary.Click += new EventHandler(On_CalculateFromUnitTypeSummary_Click);
            //_calculateFromRentRoll.Click += new EventHandler(On_CalculateFromRentRoll_Click);

            //_I_unitTypeSummary.OnListViewExItemChanged += new ProfitGrabber.Commercial.ListViewEx.ListViewExItemChanged(I_unitTypeSummary_OnListViewExItemChanged);
            //_I_rentRoll.OnListViewExItemChanged += new ProfitGrabber.Commercial.ListViewEx.ListViewExItemChanged(I_rentRoll_OnListViewExItemChanged);
			_E_expenses.OnListViewExItemChanged += new ProfitGrabber.Commercial.ListViewEx.ListViewExItemChanged(E_expenses_OnListViewExItemChanged);

			//LOANS
			_L_1L_Type.SelectedValueChanged += new EventHandler(On_L_1L_Type_SelectedValueChanged);
			_L_2L_Type.SelectedValueChanged += new EventHandler(On_L_2L_Type_SelectedValueChanged);
			_L_3L_Type.SelectedValueChanged += new EventHandler(On_L_3L_Type_SelectedValueChanged);

			_L_PurchasePrice.KeyDown += new KeyEventHandler(On_L_PurchasePrice_KeyDown);
			_L_PurchasePrice.LostFocus += new EventHandler(On_L_PurchasePrice_LostFocus);
			_L_DownPayment.KeyDown += new KeyEventHandler(On_L_DownPayment_KeyDown);
			_L_DownPayment.LostFocus += new EventHandler(On_L_DownPayment_LostFocus);
			_L_DownPaymentPerc.KeyDown += new KeyEventHandler(On_L_DownPaymentPerc_KeyDown);
			_L_DownPaymentPerc.SelectedValueChanged += new EventHandler(On_L_DownPaymentPerc_SelectedValueChanged);

			//FINANCIAL ANALYSIS
			_FA_copyAIfromIncome.Click += new EventHandler(On_FA_copyAIfromIncome_Click);
			_FA_copyEfromExpenses.Click += new EventHandler(On_FA_copyEfromExpenses_Click);
			_FA_copyPDfromLoan.Click += new EventHandler(On_FA_copyPDfromLoan_Click);

			_FA_P_copyPIfromIncome.Click += new EventHandler(On_FA_P_copyPIfromIncome_Click);
			_FA_P_copyEfromExpenses.Click += new EventHandler(On_FA_P_copyEfromExpenses_Click);
			_FA_P_copyFromActual.Click += new EventHandler(On_FA_P_copyFromActual_Click);

			_FA_S_copyFromActual.Click += new EventHandler(On_FA_S_copyFromActual_Click);
			_FA_S_copyFromProforma.Click += new EventHandler(On_FA_S_copyFromProforma_Click);

            _ma_copyFromLoanTab.Click += new EventHandler(On_MA_copyFromLoanTab_Click);
            _MA_CopyFromActual.Click += new EventHandler(On_MA_CopyFromActual_Click);
            _MA_CopyFromProforma.Click += new EventHandler(On_MA_CopyFromProforma_Click);

			#region DESIGNER GENERATED

			this._BI_windowType.Leave += new System.EventHandler(this.OnTextBoxLeave);
			this._BI_windowType.Enter += new System.EventHandler(this.OnTextBoxEnter);
			this._BI_roofType.Leave += new System.EventHandler(this.OnTextBoxLeave);
			this._BI_roofType.Enter += new System.EventHandler(this.OnTextBoxEnter);
			this._BI_landscaping.Leave += new System.EventHandler(this.OnTextBoxLeave);
			this._BI_landscaping.Enter += new System.EventHandler(this.OnTextBoxEnter);
			this._BI_amenities.Leave += new System.EventHandler(this.OnTextBoxLeave);
			this._BI_amenities.Enter += new System.EventHandler(this.OnTextBoxEnter);
			this._BI_locationDescription.Leave += new System.EventHandler(this.OnTextBoxLeave);
			this._BI_locationDescription.Enter += new System.EventHandler(this.OnTextBoxEnter);
			this._BI_propertyDescription.Leave += new System.EventHandler(this.OnTextBoxLeave);
			this._BI_propertyDescription.Enter += new System.EventHandler(this.OnTextBoxEnter);
			this._BI_improvementsNeeded.Leave += new System.EventHandler(this.OnTextBoxLeave);
			this._BI_improvementsNeeded.Enter += new System.EventHandler(this.OnTextBoxEnter);
			this._BI_recentImprovements.Leave += new System.EventHandler(this.OnTextBoxLeave);
			this._BI_recentImprovements.Enter += new System.EventHandler(this.OnTextBoxEnter);
			this._BI_landLeaseComments.Leave += new System.EventHandler(this.OnTextBoxLeave);
			this._BI_landLeaseComments.Enter += new System.EventHandler(this.OnTextBoxEnter);
			this._BI_rent.KeyDown += new System.Windows.Forms.KeyEventHandler(this._BI_OnKeyDown);
			this._BI_rent.Leave += new System.EventHandler(this.OnTextBoxLeave);
			this._BI_rent.Enter += new System.EventHandler(this.OnTextBoxEnter);
			this._BI_endDate.Leave += new System.EventHandler(this.OnTextBoxLeave);
			this._BI_endDate.Enter += new System.EventHandler(this.OnTextBoxEnter);
			this._BI_startDate.Leave += new System.EventHandler(this.OnTextBoxLeave);
			this._BI_startDate.Enter += new System.EventHandler(this.OnTextBoxEnter);
			this._BI_lessor.Leave += new System.EventHandler(this.OnTextBoxLeave);
			this._BI_lessor.Enter += new System.EventHandler(this.OnTextBoxEnter);
			this._BI_option.SelectedValueChanged += new System.EventHandler(this.OnCheckBoxSelectedValueChanged);
			this._BI_option.Leave += new System.EventHandler(this.OnCheckBoxLeave);
			this._BI_landLease.SelectedValueChanged += new System.EventHandler(this.OnCheckBoxSelectedValueChanged);
			this._BI_landLease.Leave += new System.EventHandler(this.OnCheckBoxLeave);
			this._BI_comments.Leave += new System.EventHandler(this.OnTextBoxLeave);
			this._BI_comments.Enter += new System.EventHandler(this.OnTextBoxEnter);
			this._BI_parkingRatio.Leave += new System.EventHandler(this.OnTextBoxLeave);
			this._BI_parkingRatio.Enter += new System.EventHandler(this.OnTextBoxEnter);
			this._BI_totalFee.Leave += new System.EventHandler(this.OnTextBoxLeave);
			this._BI_totalFee.Enter += new System.EventHandler(this.OnTextBoxEnter);
			this._BI_garageFee.KeyDown += new System.Windows.Forms.KeyEventHandler(this._BI_OnKeyDown);
			this._BI_garageFee.Leave += new System.EventHandler(this.OnTextBoxLeave);
			this._BI_garageFee.Enter += new System.EventHandler(this.OnTextBoxEnter);
			this._BI_coveredFee.KeyDown += new System.Windows.Forms.KeyEventHandler(this._BI_OnKeyDown);
			this._BI_coveredFee.Leave += new System.EventHandler(this.OnTextBoxLeave);
			this._BI_coveredFee.Enter += new System.EventHandler(this.OnTextBoxEnter);
			this._BI_lotFee.KeyDown += new System.Windows.Forms.KeyEventHandler(this._BI_OnKeyDown);
			this._BI_lotFee.Leave += new System.EventHandler(this.OnTextBoxLeave);
			this._BI_lotFee.Enter += new System.EventHandler(this.OnTextBoxEnter);
			this._BI_totalSpaces.Leave += new System.EventHandler(this.OnTextBoxLeave);
			this._BI_totalSpaces.Enter += new System.EventHandler(this.OnTextBoxEnter);
			this._BI_garageSpaces.KeyDown += new System.Windows.Forms.KeyEventHandler(this._BI_OnKeyDown);
			this._BI_garageSpaces.Leave += new System.EventHandler(this.OnTextBoxLeave);
			this._BI_garageSpaces.Enter += new System.EventHandler(this.OnTextBoxEnter);
			this._BI_coveredSpaces.KeyDown += new System.Windows.Forms.KeyEventHandler(this._BI_OnKeyDown);
			this._BI_coveredSpaces.Leave += new System.EventHandler(this.OnTextBoxLeave);
			this._BI_coveredSpaces.Enter += new System.EventHandler(this.OnTextBoxEnter);
			this._BI_lotSpaces.KeyDown += new System.Windows.Forms.KeyEventHandler(this._BI_OnKeyDown);
			this._BI_lotSpaces.Leave += new System.EventHandler(this.OnTextBoxLeave);
			this._BI_lotSpaces.Enter += new System.EventHandler(this.OnTextBoxEnter);
			this._BI_windowsAge.SelectedValueChanged += new System.EventHandler(this.OnCheckBoxSelectedValueChanged);
			this._BI_windowsAge.Leave += new System.EventHandler(this.OnCheckBoxLeave);
			this._BI_sidingType.SelectedValueChanged += new System.EventHandler(this.OnCheckBoxSelectedValueChanged);
			this._BI_sidingType.Leave += new System.EventHandler(this.OnCheckBoxLeave);
			this._BI_electricalAge.SelectedValueChanged += new System.EventHandler(this.OnCheckBoxSelectedValueChanged);
			this._BI_electricalAge.Leave += new System.EventHandler(this.OnCheckBoxLeave);
			this._BI_plumbingAge.SelectedValueChanged += new System.EventHandler(this.OnCheckBoxSelectedValueChanged);
			this._BI_plumbingAge.Leave += new System.EventHandler(this.OnCheckBoxLeave);
			this._BI_plumbingType.SelectedValueChanged += new System.EventHandler(this.OnCheckBoxSelectedValueChanged);
			this._BI_plumbingType.Leave += new System.EventHandler(this.OnCheckBoxLeave);
			this._BI_roofAge.Leave += new System.EventHandler(this.OnCheckBoxLeave);
			this._BI_lotSqft.Leave += new System.EventHandler(this.OnTextBoxLeave);
			this._BI_lotSqft.Enter += new System.EventHandler(this.OnTextBoxEnter);
			this._BI_lotAcres.Leave += new System.EventHandler(this.OnTextBoxLeave);
			this._BI_lotAcres.Enter += new System.EventHandler(this.OnTextBoxEnter);
			this._BI_netRentableSqFt.Leave += new System.EventHandler(this.OnTextBoxLeave);
			this._BI_netRentableSqFt.Enter += new System.EventHandler(this.OnTextBoxEnter);
			this._BI_totalSqFt.Leave += new System.EventHandler(this.OnTextBoxLeave);
			this._BI_totalSqFt.Enter += new System.EventHandler(this.OnTextBoxEnter);
			this._BI_structures.SelectedValueChanged += new System.EventHandler(this.OnCheckBoxSelectedValueChanged);
			this._BI_structures.Leave += new System.EventHandler(this.OnCheckBoxLeave);
			this._BI_floors.SelectedValueChanged += new System.EventHandler(this.OnCheckBoxSelectedValueChanged);
			this._BI_floors.Leave += new System.EventHandler(this.OnCheckBoxLeave);
			this._BI_units.SelectedValueChanged += new System.EventHandler(this.OnCheckBoxSelectedValueChanged);
			this._BI_units.Leave += new System.EventHandler(this.OnCheckBoxLeave);
			this._BI_rehabYear.SelectedValueChanged += new System.EventHandler(this.OnCheckBoxSelectedValueChanged);
			this._BI_rehabYear.Leave += new System.EventHandler(this.OnCheckBoxLeave);
			this._BI_yearBuilt.SelectedValueChanged += new System.EventHandler(this.OnCheckBoxSelectedValueChanged);
			this._BI_yearBuilt.Leave += new System.EventHandler(this.OnCheckBoxLeave);
			this._BI_propertyValue.KeyDown += new System.Windows.Forms.KeyEventHandler(this._BI_OnKeyDown);
			this._BI_propertyValue.Leave += new System.EventHandler(this.OnTextBoxLeave);
			this._BI_propertyValue.Enter += new System.EventHandler(this.OnTextBoxEnter);
			this._BI_apn.Leave += new System.EventHandler(this.OnTextBoxLeave);
			this._BI_apn.Enter += new System.EventHandler(this.OnTextBoxEnter);
			this._BI_class.Leave += new System.EventHandler(this.OnCheckBoxLeave);
			this._BI_propertyType.Leave += new System.EventHandler(this.OnCheckBoxLeave);
			this._BI_propertyName.Leave += new System.EventHandler(this.OnTextBoxLeave);
			this._BI_propertyName.Enter += new System.EventHandler(this.OnTextBoxEnter);
            //this._I_PI_VacancyOrCreditLossPerc.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.I_P_VacancyPerc_OnKeyPressed);
            //this._I_PI_VacancyOrCreditLossPerc.Leave += new System.EventHandler(this.I_P_VacancyPerc_OnTextBoxLeave);
            //this._I_PI_OtherIncome.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.I_P_OtherIncome_OnKeyPressed);
            //this._I_PI_OtherIncome.Leave += new System.EventHandler(this.I_P_OtherIncome_OnTextBoxLeave);
            //this._I_PI_GrossRent.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.I_P_GSI_OnKeyPressed);
            //this._I_PI_GrossRent.Leave += new System.EventHandler(this.I_P_GSI_OnTextBoxLeave);
            //this._I_AI_VacancyOrCreditLossPerc.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.I_AI_LossPerc_OnKeyPressed);
            //this._I_AI_VacancyOrCreditLossPerc.Leave += new System.EventHandler(this.I_AI_LossPerc_OnTextBoxLeave);
            //this._I_AI_VacancyOrCreditLossPerc.Enter += new System.EventHandler(this.OnTextBoxEnter);
            //this._I_AI_VacancyOrCreditLoss.Enter += new System.EventHandler(this.OnTextBoxEnter);
            //this._I_AI_GrossRent.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.I_AI_GSI_OnKeyPressed);
            //this._I_AI_GrossRent.Leave += new System.EventHandler(this.I_AI_GSI_OnTextBoxLeave);
            //this._I_AI_GrossRent.Enter += new System.EventHandler(this.OnTextBoxEnter);
            //this._I_OI_Other.KeyDown += new System.Windows.Forms.KeyEventHandler(this.I_OI_KeyDown);
            //this._I_OI_Other.Leave += new System.EventHandler(this.I_OnTextBoxLeave);
            //this._I_OI_Other.Enter += new System.EventHandler(this.OnTextBoxEnter);
            //this._I_OI_Vending.KeyDown += new System.Windows.Forms.KeyEventHandler(this.I_OI_KeyDown);
            //this._I_OI_Vending.Leave += new System.EventHandler(this.I_OnTextBoxLeave);
            //this._I_OI_Vending.Enter += new System.EventHandler(this.OnTextBoxEnter);
            //this._I_OI_Laundry.KeyDown += new System.Windows.Forms.KeyEventHandler(this.I_OI_KeyDown);
            //this._I_OI_Laundry.Leave += new System.EventHandler(this.I_OnTextBoxLeave);
            //this._I_OI_Laundry.Enter += new System.EventHandler(this.OnTextBoxEnter);
            //this._I_OI_Parking.KeyDown += new System.Windows.Forms.KeyEventHandler(this.I_OI_KeyDown);
            //this._I_OI_Parking.Leave += new System.EventHandler(this.I_OnTextBoxLeave);
            //this._I_OI_Parking.Enter += new System.EventHandler(this.OnTextBoxEnter);
			this._E_CA_Type_Year10.KeyDown += new System.Windows.Forms.KeyEventHandler(this._E_CA_OnKeyDown);
			this._E_CA_Type_Year10.Leave += new System.EventHandler(this._E_CA_OnLeave);
			this._E_CA_Cost_Year10.KeyDown += new System.Windows.Forms.KeyEventHandler(this._E_CA_OnKeyDown);
			this._E_CA_Cost_Year10.Leave += new System.EventHandler(this._E_CA_OnLeave);
			this._E_CA_Type_Year9.KeyDown += new System.Windows.Forms.KeyEventHandler(this._E_CA_OnKeyDown);
			this._E_CA_Type_Year9.Leave += new System.EventHandler(this._E_CA_OnLeave);
			this._E_CA_Cost_Year9.KeyDown += new System.Windows.Forms.KeyEventHandler(this._E_CA_OnKeyDown);
			this._E_CA_Cost_Year9.Leave += new System.EventHandler(this._E_CA_OnLeave);
			this._E_CA_Type_Year8.KeyDown += new System.Windows.Forms.KeyEventHandler(this._E_CA_OnKeyDown);
			this._E_CA_Type_Year8.Leave += new System.EventHandler(this._E_CA_OnLeave);
			this._E_CA_Cost_Year8.KeyDown += new System.Windows.Forms.KeyEventHandler(this._E_CA_OnKeyDown);
			this._E_CA_Cost_Year8.Leave += new System.EventHandler(this._E_CA_OnLeave);
			this._E_CA_Type_Year7.KeyDown += new System.Windows.Forms.KeyEventHandler(this._E_CA_OnKeyDown);
			this._E_CA_Type_Year7.Leave += new System.EventHandler(this._E_CA_OnLeave);
			this._E_CA_Cost_Year7.KeyDown += new System.Windows.Forms.KeyEventHandler(this._E_CA_OnKeyDown);
			this._E_CA_Cost_Year7.Leave += new System.EventHandler(this._E_CA_OnLeave);
			this._E_CA_Type_Year6.KeyDown += new System.Windows.Forms.KeyEventHandler(this._E_CA_OnKeyDown);
			this._E_CA_Type_Year6.Leave += new System.EventHandler(this._E_CA_OnLeave);
			this._E_CA_Cost_Year6.KeyDown += new System.Windows.Forms.KeyEventHandler(this._E_CA_OnKeyDown);
			this._E_CA_Cost_Year6.Leave += new System.EventHandler(this._E_CA_OnLeave);
			this._E_CA_Type_Year5.KeyDown += new System.Windows.Forms.KeyEventHandler(this._E_CA_OnKeyDown);
			this._E_CA_Type_Year5.Leave += new System.EventHandler(this._E_CA_OnLeave);
			this._E_CA_Cost_Year5.KeyDown += new System.Windows.Forms.KeyEventHandler(this._E_CA_OnKeyDown);
			this._E_CA_Cost_Year5.Leave += new System.EventHandler(this._E_CA_OnLeave);
			this._E_CA_Type_Year4.KeyDown += new System.Windows.Forms.KeyEventHandler(this._E_CA_OnKeyDown);
			this._E_CA_Type_Year4.Leave += new System.EventHandler(this._E_CA_OnLeave);
			this._E_CA_Cost_Year4.KeyDown += new System.Windows.Forms.KeyEventHandler(this._E_CA_OnKeyDown);
			this._E_CA_Cost_Year4.Leave += new System.EventHandler(this._E_CA_OnLeave);
			this._E_CA_Type_Year3.KeyDown += new System.Windows.Forms.KeyEventHandler(this._E_CA_OnKeyDown);
			this._E_CA_Type_Year3.Leave += new System.EventHandler(this._E_CA_OnLeave);
			this._E_CA_Cost_Year3.KeyDown += new System.Windows.Forms.KeyEventHandler(this._E_CA_OnKeyDown);
			this._E_CA_Cost_Year3.Leave += new System.EventHandler(this._E_CA_OnLeave);
			this._E_CA_Type_Year2.KeyDown += new System.Windows.Forms.KeyEventHandler(this._E_CA_OnKeyDown);
			this._E_CA_Type_Year2.Leave += new System.EventHandler(this._E_CA_OnLeave);
			this._E_CA_Cost_Year2.KeyDown += new System.Windows.Forms.KeyEventHandler(this._E_CA_OnKeyDown);
			this._E_CA_Cost_Year2.Leave += new System.EventHandler(this._E_CA_OnLeave);
			this._E_CA_Type_Year1.KeyDown += new System.Windows.Forms.KeyEventHandler(this._E_CA_OnKeyDown);
			this._E_CA_Type_Year1.Leave += new System.EventHandler(this._E_CA_OnLeave);
			this._E_CA_Cost_Year1.KeyDown += new System.Windows.Forms.KeyEventHandler(this._E_CA_OnKeyDown);
			this._E_CA_Cost_Year1.Leave += new System.EventHandler(this._E_CA_OnLeave);
			this._E_UT_HotWater.SelectedIndexChanged += new System.EventHandler(this._E_UT_HotWaterSelectedIndexChanged);
			this._E_UT_Heat.SelectedIndexChanged += new System.EventHandler(this._E_UT_Heat_SelectedIndexChanged);
			this._E_UT_ElectricityMeter.SelectedIndexChanged += new System.EventHandler(this._E_UT_ElMeter_SelectedIndexChanged);
			this._L_3L_RA_LifeTimeCap.TextChanged += new System.EventHandler(this.OnLoansTextChanged);
			this._L_3L_RA_AnnualCap.TextChanged += new System.EventHandler(this.OnLoansTextChanged);
			this._L_3L_RA_InitialRateCap.TextChanged += new System.EventHandler(this.OnLoansTextChanged);
			this._L_3L_RA_InitFixedPeriod.TextChanged += new System.EventHandler(this.OnLoansTextChanged);
			this._L_3L_RA_RateYear10.TextChanged += new System.EventHandler(this.OnLoansTextChanged);
			this._L_3L_RA_RateYear9.TextChanged += new System.EventHandler(this.OnLoansTextChanged);
			this._L_3L_RA_RateYear8.TextChanged += new System.EventHandler(this.OnLoansTextChanged);
			this._L_3L_RA_RateYear7.TextChanged += new System.EventHandler(this.OnLoansTextChanged);
			this._L_3L_RA_RateYear6.TextChanged += new System.EventHandler(this.OnLoansTextChanged);
			this._L_3L_RA_RateYear5.TextChanged += new System.EventHandler(this.OnLoansTextChanged);
			this._L_3L_RA_RateYear4.TextChanged += new System.EventHandler(this.OnLoansTextChanged);
			this._L_3L_RA_RateYear3.TextChanged += new System.EventHandler(this.OnLoansTextChanged);
			this._L_3L_RA_RateYear2.TextChanged += new System.EventHandler(this.OnLoansTextChanged);
			this._L_3L_RA_RateYear1.TextChanged += new System.EventHandler(this.OnLoansTextChanged);
			this._L_3L_Lender.TextChanged += new System.EventHandler(this.OnLoansTextChanged);
			this._L_3L_LastPaymentDate.TextChanged += new System.EventHandler(this.OnLoansTextChanged);
			this._L_3L_FirstPaymentDate.TextChanged += new System.EventHandler(this.OnLoansTextChanged);
			this._L_3L_Points.KeyDown += new System.Windows.Forms.KeyEventHandler(this.L_Points_KeyDown);
			this._L_3L_Points.Leave += new System.EventHandler(this.L_Points_Leave);
			this._L_3L_PointsPerc.KeyDown += new System.Windows.Forms.KeyEventHandler(this.L_PointsPercKeyDown);
			this._L_3L_PointsPerc.Leave += new System.EventHandler(this.L_PointsPercLeave);
			this._L_3L_MonthlyPayment.TextChanged += new System.EventHandler(this.OnLoansTextChanged);
			this._L_3L_Rate.TextChanged += new System.EventHandler(this.OnLoansTextChanged);
			this._L_3L_AmortTerm.TextChanged += new System.EventHandler(this.OnLoansTextChanged);
			this._L_3L_Amount.KeyDown += new System.Windows.Forms.KeyEventHandler(this._L_OnAmountKeyDown);
			this._L_3L_Amount.TextChanged += new System.EventHandler(this.OnLoansTextChanged);
			this._L_3L_Amount.Leave += new System.EventHandler(this._L_OnAmountLeave);
			this._L_3L_LTVPerc.KeyDown += new System.Windows.Forms.KeyEventHandler(this._L_OnLTVKeyDown);
			this._L_3L_LTVPerc.Leave += new System.EventHandler(this._L_OnLTVLeave);
			this._L_3L_Type.SelectedValueChanged += new System.EventHandler(this.OnLoanTypeChanged);
			this._L_2L_RA_LifeTimeCap.TextChanged += new System.EventHandler(this.OnLoansTextChanged);
			this._L_2L_RA_AnnualCap.TextChanged += new System.EventHandler(this.OnLoansTextChanged);
			this._L_2L_RA_InitialRateCap.TextChanged += new System.EventHandler(this.OnLoansTextChanged);
			this._L_2L_RA_InitFixedPeriod.TextChanged += new System.EventHandler(this.OnLoansTextChanged);
			this._L_2L_RA_RateYear10.TextChanged += new System.EventHandler(this.OnLoansTextChanged);
			this._L_2L_RA_RateYear9.TextChanged += new System.EventHandler(this.OnLoansTextChanged);
			this._L_2L_RA_RateYear8.TextChanged += new System.EventHandler(this.OnLoansTextChanged);
			this._L_2L_RA_RateYear7.TextChanged += new System.EventHandler(this.OnLoansTextChanged);
			this._L_2L_RA_RateYear6.TextChanged += new System.EventHandler(this.OnLoansTextChanged);
			this._L_2L_RA_RateYear5.TextChanged += new System.EventHandler(this.OnLoansTextChanged);
			this._L_2L_RA_RateYear4.TextChanged += new System.EventHandler(this.OnLoansTextChanged);
			this._L_2L_RA_RateYear3.TextChanged += new System.EventHandler(this.OnLoansTextChanged);
			this._L_2L_RA_RateYear2.TextChanged += new System.EventHandler(this.OnLoansTextChanged);
			this._L_2L_RA_RateYear1.TextChanged += new System.EventHandler(this.OnLoansTextChanged);
			this._L_2L_Lender.TextChanged += new System.EventHandler(this.OnLoansTextChanged);
			this._L_2L_LastPaymentDate.TextChanged += new System.EventHandler(this.OnLoansTextChanged);
			this._L_2L_FirstPaymentDate.TextChanged += new System.EventHandler(this.OnLoansTextChanged);
			this._L_2L_Points.KeyDown += new System.Windows.Forms.KeyEventHandler(this.L_Points_KeyDown);
			this._L_2L_Points.Leave += new System.EventHandler(this.L_Points_Leave);
			this._L_2L_PointsPerc.KeyDown += new System.Windows.Forms.KeyEventHandler(this.L_PointsPercKeyDown);
			this._L_2L_PointsPerc.Leave += new System.EventHandler(this.L_PointsPercLeave);
			this._L_2L_MonthlyPayment.TextChanged += new System.EventHandler(this.OnLoansTextChanged);
			this._L_2L_Rate.TextChanged += new System.EventHandler(this.OnLoansTextChanged);
			this._L_2L_AmortTerm.TextChanged += new System.EventHandler(this.OnLoansTextChanged);
			this._L_2L_Amount.KeyDown += new System.Windows.Forms.KeyEventHandler(this._L_OnAmountKeyDown);
			this._L_2L_Amount.TextChanged += new System.EventHandler(this.OnLoansTextChanged);
			this._L_2L_Amount.Leave += new System.EventHandler(this._L_OnAmountLeave);
			this._L_2L_LTVPerc.KeyDown += new System.Windows.Forms.KeyEventHandler(this._L_OnLTVKeyDown);
			this._L_2L_LTVPerc.Leave += new System.EventHandler(this._L_OnLTVLeave);
			this._L_2L_Type.SelectedValueChanged += new System.EventHandler(this.OnLoanTypeChanged);
			this._L_1L_RA_LifeTimeCap.TextChanged += new System.EventHandler(this.OnLoansTextChanged);
			this._L_1L_RA_AnnualCap.TextChanged += new System.EventHandler(this.OnLoansTextChanged);
			this._L_1L_RA_InitialRateCap.TextChanged += new System.EventHandler(this.OnLoansTextChanged);
			this._L_1L_RA_InitFixedPeriod.TextChanged += new System.EventHandler(this.OnLoansTextChanged);
			this._L_1L_RA_RateYear10.TextChanged += new System.EventHandler(this.OnLoansTextChanged);
			this._L_1L_RA_RateYear9.TextChanged += new System.EventHandler(this.OnLoansTextChanged);
			this._L_1L_RA_RateYear8.TextChanged += new System.EventHandler(this.OnLoansTextChanged);
			this._L_1L_RA_RateYear7.TextChanged += new System.EventHandler(this.OnLoansTextChanged);
			this._L_1L_RA_RateYear6.TextChanged += new System.EventHandler(this.OnLoansTextChanged);
			this._L_1L_RA_RateYear5.TextChanged += new System.EventHandler(this.OnLoansTextChanged);
			this._L_1L_RA_RateYear4.TextChanged += new System.EventHandler(this.OnLoansTextChanged);
			this._L_1L_RA_RateYear3.TextChanged += new System.EventHandler(this.OnLoansTextChanged);
			this._L_1L_RA_RateYear2.TextChanged += new System.EventHandler(this.OnLoansTextChanged);
			this._L_1L_RA_RateYear1.TextChanged += new System.EventHandler(this.OnLoansTextChanged);
			this._L_1L_Lender.TextChanged += new System.EventHandler(this.OnLoansTextChanged);
			this._L_1L_LastPaymentDate.TextChanged += new System.EventHandler(this.OnLoansTextChanged);
			this._L_1L_FirstPaymentDate.TextChanged += new System.EventHandler(this.OnLoansTextChanged);
			this._L_1L_Points.KeyDown += new System.Windows.Forms.KeyEventHandler(this.L_Points_KeyDown);
			this._L_1L_Points.Leave += new System.EventHandler(this.L_Points_Leave);
			this._L_1L_PointsPerc.KeyDown += new System.Windows.Forms.KeyEventHandler(this.L_PointsPercKeyDown);
			this._L_1L_PointsPerc.Leave += new System.EventHandler(this.L_PointsPercLeave);
			this._L_1L_MonthlyPayment.TextChanged += new System.EventHandler(this.OnLoansTextChanged);
			this._L_1L_Rate.TextChanged += new System.EventHandler(this.OnLoansTextChanged);
			this._L_1L_AmortTerm.TextChanged += new System.EventHandler(this.OnLoansTextChanged);
			this._L_1L_Amount.KeyDown += new System.Windows.Forms.KeyEventHandler(this._L_OnAmountKeyDown);
			this._L_1L_Amount.TextChanged += new System.EventHandler(this.OnLoansTextChanged);
			this._L_1L_Amount.Leave += new System.EventHandler(this._L_OnAmountLeave);
			this._L_1L_LTVPerc.KeyDown += new System.Windows.Forms.KeyEventHandler(this._L_OnLTVKeyDown);
			this._L_1L_LTVPerc.Leave += new System.EventHandler(this._L_OnLTVLeave);
			this._L_1L_Type.SelectedValueChanged += new System.EventHandler(this.OnLoanTypeChanged);
			this._L_SellerCarryback.SelectedIndexChanged += new System.EventHandler(this._L_SellerCBSelectedIndexChanged);
			this._L_Assumable.SelectedIndexChanged += new System.EventHandler(this._L_SecondAssumableSelectedIndexChanged);
			this._L_1stAssumable.SelectedIndexChanged += new System.EventHandler(this._L_FirstAssumableSelectedIndexChanged);
			this._FA_S_OperatingExpensesPerc.KeyDown += new System.Windows.Forms.KeyEventHandler(this._FA_S_OnKeyDown);
			this._FA_S_OperatingExpensesPerc.Leave += new System.EventHandler(this._FA_S_OnLeave);
			this._FA_P_OperatingExpensesPerc.KeyDown += new System.Windows.Forms.KeyEventHandler(this._FA_P_OnKeyDown);
			this._FA_P_OperatingExpensesPerc.Leave += new System.EventHandler(this._FA_P_OnLeave);
			this._FA_A_OperatingExpensesPerc.KeyDown += new System.Windows.Forms.KeyEventHandler(this._FA_A_OnKeyDown);
			this._FA_A_OperatingExpensesPerc.Leave += new System.EventHandler(this._FA_A_OnLeave);
			this._FA_S_Comment.KeyDown += new System.Windows.Forms.KeyEventHandler(this._FA_S_OnKeyDown);
			this._FA_S_Comment.Leave += new System.EventHandler(this._FA_S_OnLeave);
			this._FA_S_VacancyCreditLossPerc.KeyDown += new System.Windows.Forms.KeyEventHandler(this._FA_S_OnKeyDown);
			this._FA_S_VacancyCreditLossPerc.Leave += new System.EventHandler(this._FA_S_OnLeave);
			this._FA_S_DebtService.KeyDown += new System.Windows.Forms.KeyEventHandler(this._FA_S_OnKeyDown);
			this._FA_S_DebtService.Leave += new System.EventHandler(this._FA_S_OnLeave);
			this._FA_S_CapRate.KeyDown += new System.Windows.Forms.KeyEventHandler(this._FA_S_OnKeyDown);
			this._FA_S_CapRate.Leave += new System.EventHandler(this._FA_S_OnLeave);
			this._FA_S_OperatingExpenses.KeyDown += new System.Windows.Forms.KeyEventHandler(this._FA_S_OnKeyDown);
			this._FA_S_OperatingExpenses.Leave += new System.EventHandler(this._FA_S_OnLeave);
			this._FA_S_OtherIncome.KeyDown += new System.Windows.Forms.KeyEventHandler(this._FA_S_OnKeyDown);
			this._FA_S_OtherIncome.Leave += new System.EventHandler(this._FA_S_OnLeave);
			this._FA_S_VacancyCreditLoss.KeyDown += new System.Windows.Forms.KeyEventHandler(this._FA_S_OnKeyDown);
			this._FA_S_VacancyCreditLoss.Leave += new System.EventHandler(this._FA_S_OnLeave);
			this._FA_S_GSIAnnual.KeyDown += new System.Windows.Forms.KeyEventHandler(this._FA_S_OnKeyDown);
			this._FA_S_GSIAnnual.Leave += new System.EventHandler(this._FA_S_OnLeave);
			this._FA_S_Price.KeyDown += new System.Windows.Forms.KeyEventHandler(this._FA_S_OnKeyDown);
			this._FA_S_Price.Leave += new System.EventHandler(this._FA_S_OnLeave);
			this._FA_P_Comment.KeyDown += new System.Windows.Forms.KeyEventHandler(this._FA_P_OnKeyDown);
			this._FA_P_Comment.Leave += new System.EventHandler(this._FA_P_OnLeave);
			this._FA_P_VacancyCreditLossPerc.KeyDown += new System.Windows.Forms.KeyEventHandler(this._FA_P_OnKeyDown);
			this._FA_P_VacancyCreditLossPerc.Leave += new System.EventHandler(this._FA_P_OnLeave);
			this._FA_P_DebtService.KeyDown += new System.Windows.Forms.KeyEventHandler(this._FA_P_OnKeyDown);
			this._FA_P_DebtService.Leave += new System.EventHandler(this._FA_P_OnLeave);
			this._FA_P_CapRate.KeyDown += new System.Windows.Forms.KeyEventHandler(this._FA_P_OnKeyDown);
			this._FA_P_CapRate.Leave += new System.EventHandler(this._FA_P_OnLeave);
			this._FA_P_OperatingExpenses.KeyDown += new System.Windows.Forms.KeyEventHandler(this._FA_P_OnKeyDown);
			this._FA_P_OperatingExpenses.Leave += new System.EventHandler(this._FA_P_OnLeave);
			this._FA_P_OtherIncome.KeyDown += new System.Windows.Forms.KeyEventHandler(this._FA_P_OnKeyDown);
			this._FA_P_OtherIncome.Leave += new System.EventHandler(this._FA_P_OnLeave);
			this._FA_P_VacancyCreditLoss.KeyDown += new System.Windows.Forms.KeyEventHandler(this._FA_P_OnKeyDown);
			this._FA_P_VacancyCreditLoss.Leave += new System.EventHandler(this._FA_P_OnLeave);
			this._FA_P_GSIAnnual.KeyDown += new System.Windows.Forms.KeyEventHandler(this._FA_P_OnKeyDown);
			this._FA_P_GSIAnnual.Leave += new System.EventHandler(this._FA_P_OnLeave);
			this._FA_P_Price.KeyDown += new System.Windows.Forms.KeyEventHandler(this._FA_P_OnKeyDown);
			this._FA_P_Price.Leave += new System.EventHandler(this._FA_P_OnLeave);
			this._FA_A_Comment.KeyDown += new System.Windows.Forms.KeyEventHandler(this._FA_A_OnKeyDown);
			this._FA_A_Comment.Leave += new System.EventHandler(this._FA_A_OnLeave);
			this._FA_A_VacancyCreditLossPerc.KeyDown += new System.Windows.Forms.KeyEventHandler(this._FA_A_OnKeyDown);
			this._FA_A_VacancyCreditLossPerc.Leave += new System.EventHandler(this._FA_A_OnLeave);
			this._FA_A_DebtService.KeyDown += new System.Windows.Forms.KeyEventHandler(this._FA_A_OnKeyDown);
			this._FA_A_DebtService.Leave += new System.EventHandler(this._FA_A_OnLeave);
			this._FA_A_CapRate.KeyDown += new System.Windows.Forms.KeyEventHandler(this._FA_A_OnKeyDown);
			this._FA_A_CapRate.Leave += new System.EventHandler(this._FA_A_OnLeave);
			this._FA_A_OperatingExpenses.KeyDown += new System.Windows.Forms.KeyEventHandler(this._FA_A_OnKeyDown);
			this._FA_A_OperatingExpenses.Leave += new System.EventHandler(this._FA_A_OnLeave);
			this._FA_A_OtherIncome.KeyDown += new System.Windows.Forms.KeyEventHandler(this._FA_A_OnKeyDown);
			this._FA_A_OtherIncome.Leave += new System.EventHandler(this._FA_A_OnLeave);
			this._FA_A_VacancyCreditLoss.KeyDown += new System.Windows.Forms.KeyEventHandler(this._FA_A_OnKeyDown);
			this._FA_A_VacancyCreditLoss.Leave += new System.EventHandler(this._FA_A_OnLeave);
			this._FA_A_GSIAnnual.KeyDown += new System.Windows.Forms.KeyEventHandler(this._FA_A_OnKeyDown);
			this._FA_A_GSIAnnual.Leave += new System.EventHandler(this._FA_A_OnLeave);
			this._FA_A_Price.KeyDown += new System.Windows.Forms.KeyEventHandler(this._FA_A_OnKeyDown);
			this._FA_A_Price.Leave += new System.EventHandler(this._FA_A_OnLeave);

            //MULTIYEAR ANALYSIS
            this._MA_PurchasePrice.KeyDown += new KeyEventHandler(this._MA_KeyDown);
            this._MA_PurchasePrice.Leave += new EventHandler(this._MA_Leave);
            this._MA_InitialInvestment.KeyDown += new KeyEventHandler(this._MA_KeyDown);
            this._MA_InitialInvestment.Leave += new EventHandler(this._MA_Leave);
            this._MA_DownPayment.KeyDown += new KeyEventHandler(this._MA_DownPayment_KeyDown);
            this._MA_DownPayment.Leave += new EventHandler(this._MA_DownPayment_Leave);
            this._MA_Points.KeyDown += new KeyEventHandler(this._MA_Points_KeyDown);
            this._MA_Points.Leave += new EventHandler(this._MA_Points_Leave);
            this._MA_OtherAcquisitionCosts.KeyDown += new KeyEventHandler(this._MA_OtherAcquisitionCosts_KeyDown);
            this._MA_OtherAcquisitionCosts.Leave += new EventHandler(this._MA_OtherAcquisitionCosts_Leave);
            this._MA_S1_GSI.KeyDown += new KeyEventHandler(this._MA_GSI_KeyDown);
            this._MA_S1_GSI.Leave += new EventHandler(this._MA_GSI_Leave);
            this._MA_S1_GSI_Perc.KeyDown += new KeyEventHandler(this._MA_KeyDown);
            this._MA_S1_GSI_Perc.Leave += new EventHandler(this._MA_Leave);
            this._MA_S1_Vacancy.KeyDown += new KeyEventHandler(this._MA_Vacancy_KeyDown);
            this._MA_S1_Vacancy.Leave += new EventHandler(this._MA_Vacancy_Leave);
            this._MA_S1_VacancyPerc.KeyDown += new KeyEventHandler(this._MA_VacancyPerc_KeyDown);
            this._MA_S1_VacancyPerc.Leave += new EventHandler(this._MA_VacancyPerc_Leave);            
            this._MA_S1_OtherIncome.KeyDown += new KeyEventHandler(this._MA_KeyDown);
            this._MA_S1_OtherIncome.Leave += new EventHandler(this._MA_Leave);
            this._MA_S1_OtherIncomePerc.KeyDown += new KeyEventHandler(this._MA_KeyDown);
            this._MA_S1_OtherIncomePerc.Leave += new EventHandler(this._MA_Leave);
            this._MA_S1_OperatingExpenses.KeyDown += new KeyEventHandler(this._MA_OperatingExpenses_KeyDown);
            this._MA_S1_OperatingExpenses.Leave += new EventHandler(this._MA_OperatingExpenses_Leave);
            this._MA_S1_OperatingExpensesPerc.KeyDown += new KeyEventHandler(this._MA_OperatingExpensesPerc_KeyDown);
            this._MA_S1_OperatingExpensesPerc.Leave += new EventHandler(this._MA_OperatingExpensesPerc_Leave);
            this._MA_TI_FederalRate.KeyDown += new KeyEventHandler(this._MA_KeyDown);
            this._MA_TI_FederalRate.Leave += new EventHandler(this._MA_Leave);
            this._MA_TI_StateRate.KeyDown += new KeyEventHandler(this._MA_KeyDown);
            this._MA_TI_StateRate.Leave += new EventHandler(this._MA_Leave);
            this._MA_TI_CapitalGainsRate.KeyDown += new KeyEventHandler(this._MA_KeyDown);
            this._MA_TI_CapitalGainsRate.Leave += new EventHandler(this._MA_Leave);
            this._MA_TI_BuildingValue.KeyDown += new KeyEventHandler(this._MA_KeyDown);
            this._MA_TI_BuildingValue.Leave += new EventHandler(this._MA_Leave);
            this._MA_SI_SaleCosts.KeyDown += new KeyEventHandler(this._MA_KeyDown);
            this._MA_SI_SaleCosts.Leave += new EventHandler(this._MA_Leave);
            this._MA_SI_SaleCapRate.KeyDown += new KeyEventHandler(this._MA_KeyDown);
            this._MA_SI_SaleCapRate.Leave += new EventHandler(this._MA_Leave);
            this._MA_S3_OpEx_Y1.KeyDown += new KeyEventHandler(this._MA_KeyDown);
            this._MA_S3_OpEx_Y1.Leave += new EventHandler(this._MA_Leave);
            this._MA_S3_OpEx_Y2.KeyDown += new KeyEventHandler(this._MA_KeyDown);
            this._MA_S3_OpEx_Y2.Leave += new EventHandler(this._MA_Leave);
            this._MA_S3_OpEx_Y3.KeyDown += new KeyEventHandler(this._MA_KeyDown);
            this._MA_S3_OpEx_Y3.Leave += new EventHandler(this._MA_Leave);
            this._MA_S3_OpEx_Y4.KeyDown += new KeyEventHandler(this._MA_KeyDown);
            this._MA_S3_OpEx_Y4.Leave += new EventHandler(this._MA_Leave);
            this._MA_S3_OpEx_Y5.KeyDown += new KeyEventHandler(this._MA_KeyDown);
            this._MA_S3_OpEx_Y5.Leave += new EventHandler(this._MA_Leave);
            this._MA_S3_OpEx_Y6.KeyDown += new KeyEventHandler(this._MA_KeyDown);
            this._MA_S3_OpEx_Y6.Leave += new EventHandler(this._MA_Leave);
            this._MA_S3_OpEx_Y7.KeyDown += new KeyEventHandler(this._MA_KeyDown);
            this._MA_S3_OpEx_Y7.Leave += new EventHandler(this._MA_Leave);
            this._MA_S3_OpEx_Y8.KeyDown += new KeyEventHandler(this._MA_KeyDown);
            this._MA_S3_OpEx_Y8.Leave += new EventHandler(this._MA_Leave);
            this._MA_S3_OpEx_Y9.KeyDown += new KeyEventHandler(this._MA_KeyDown);
            this._MA_S3_OpEx_Y9.Leave += new EventHandler(this._MA_Leave);
            this._MA_S3_OpEx_Y10.KeyDown += new KeyEventHandler(this._MA_KeyDown);
            this._MA_S3_OpEx_Y10.Leave += new EventHandler(this._MA_Leave);
            this._MA_S3_Vacancy_Y1.KeyDown += new KeyEventHandler(this._MA_KeyDown);
            this._MA_S3_Vacancy_Y1.Leave += new EventHandler(this._MA_Leave);
            this._MA_S3_Vacancy_Y2.KeyDown += new KeyEventHandler(this._MA_KeyDown);
            this._MA_S3_Vacancy_Y2.Leave += new EventHandler(this._MA_Leave);
            this._MA_S3_Vacancy_Y3.KeyDown += new KeyEventHandler(this._MA_KeyDown);
            this._MA_S3_Vacancy_Y3.Leave += new EventHandler(this._MA_Leave);
            this._MA_S3_Vacancy_Y4.KeyDown += new KeyEventHandler(this._MA_KeyDown);
            this._MA_S3_Vacancy_Y4.Leave += new EventHandler(this._MA_Leave);
            this._MA_S3_Vacancy_Y5.KeyDown += new KeyEventHandler(this._MA_KeyDown);
            this._MA_S3_Vacancy_Y5.Leave += new EventHandler(this._MA_Leave);
            this._MA_S3_Vacancy_Y6.KeyDown += new KeyEventHandler(this._MA_KeyDown);
            this._MA_S3_Vacancy_Y6.Leave += new EventHandler(this._MA_Leave);
            this._MA_S3_Vacancy_Y7.KeyDown += new KeyEventHandler(this._MA_KeyDown);
            this._MA_S3_Vacancy_Y7.Leave += new EventHandler(this._MA_Leave);
            this._MA_S3_Vacancy_Y8.KeyDown += new KeyEventHandler(this._MA_KeyDown);
            this._MA_S3_Vacancy_Y8.Leave += new EventHandler(this._MA_Leave);
            this._MA_S3_Vacancy_Y9.KeyDown += new KeyEventHandler(this._MA_KeyDown);
            this._MA_S3_Vacancy_Y9.Leave += new EventHandler(this._MA_Leave);
            this._MA_S3_Vacancy_Y10.KeyDown += new KeyEventHandler(this._MA_KeyDown);
            this._MA_S3_Vacancy_Y10.Leave += new EventHandler(this._MA_Leave);
            this._MA_S3_IncomeChange_Y1.KeyDown += new KeyEventHandler(this._MA_KeyDown);
            this._MA_S3_IncomeChange_Y1.Leave += new EventHandler(this._MA_Leave);
            this._MA_S3_IncomeChange_Y2.KeyDown += new KeyEventHandler(this._MA_KeyDown);
            this._MA_S3_IncomeChange_Y2.Leave += new EventHandler(this._MA_Leave);
            this._MA_S3_IncomeChange_Y3.KeyDown += new KeyEventHandler(this._MA_KeyDown);
            this._MA_S3_IncomeChange_Y3.Leave += new EventHandler(this._MA_Leave);
            this._MA_S3_IncomeChange_Y4.KeyDown += new KeyEventHandler(this._MA_KeyDown);
            this._MA_S3_IncomeChange_Y4.Leave += new EventHandler(this._MA_Leave);
            this._MA_S3_IncomeChange_Y5.KeyDown += new KeyEventHandler(this._MA_KeyDown);
            this._MA_S3_IncomeChange_Y5.Leave += new EventHandler(this._MA_Leave);
            this._MA_S3_IncomeChange_Y6.KeyDown += new KeyEventHandler(this._MA_KeyDown);
            this._MA_S3_IncomeChange_Y6.Leave += new EventHandler(this._MA_Leave);
            this._MA_S3_IncomeChange_Y7.KeyDown += new KeyEventHandler(this._MA_KeyDown);
            this._MA_S3_IncomeChange_Y7.Leave += new EventHandler(this._MA_Leave);
            this._MA_S3_IncomeChange_Y8.KeyDown += new KeyEventHandler(this._MA_KeyDown);
            this._MA_S3_IncomeChange_Y8.Leave += new EventHandler(this._MA_Leave);
            this._MA_S3_IncomeChange_Y9.KeyDown += new KeyEventHandler(this._MA_KeyDown);
            this._MA_S3_IncomeChange_Y9.Leave += new EventHandler(this._MA_Leave);
            this._MA_S3_IncomeChange_Y10.KeyDown += new KeyEventHandler(this._MA_KeyDown);
            this._MA_S3_IncomeChange_Y10.Leave += new EventHandler(this._MA_Leave);
            this._MA_S2_InflationPercChange.Leave += new EventHandler(On_MA_DropDown_Leave);
            this._MA_TI_DepreciableYears.Leave += new EventHandler(On_MA_DropDown_Leave);
            this._MA_S2_OperatingExpensesPercChange.Leave += new EventHandler(On_MA_DropDown_Leave);
            this._MA_S2_VacancyPercChange.Leave += new EventHandler(On_MA_DropDown_Leave);
            this._MA_S2_IncomePercChange.Leave += new EventHandler(On_MA_DropDown_Leave);
            this._MA_TI_DepreciableYears.Leave += new EventHandler(On_MA_DropDown_Leave);
            this._MA_RS_MARG_AnalysisPeriod.Leave += new EventHandler(On_MA_DropDown_Leave);

            _MA_S2_OperatingExpensesPercChange.SelectedIndexChanged += new EventHandler(On_MA_S2_OperatingExpensesPercChange_SelectedIndexChanged);
            _MA_S2_VacancyPercChange.SelectedIndexChanged += new EventHandler(On_MA_S2_VacancyPercChange_SelectedIndexChanged);
            _MA_S2_IncomePercChange.SelectedIndexChanged += new EventHandler(On_MA_S2_IncomePercChange_SelectedIndexChanged);

            _MA_RS_IAR_SelectAll.CheckedChanged += new EventHandler(On_MA_RS_IAR_CheckedChanged);
            _MA_RS_IAR_BuildingInfo.CheckedChanged += new EventHandler(On_MA_RS_IAR_CheckedChanged);
            _MA_RS_IAR_InitialIncomeSummary.CheckedChanged += new EventHandler(On_MA_RS_IAR_CheckedChanged);
            _MA_RS_IAR_InitialExpensesSummary.CheckedChanged += new EventHandler(On_MA_RS_IAR_CheckedChanged);
            _MA_RS_IAR_LoanSummary.CheckedChanged += new EventHandler(On_MA_RS_IAR_CheckedChanged);
            _MA_RS_IAR_FinancialAnalisys.CheckedChanged += new EventHandler(On_MA_RS_IAR_CheckedChanged);

            _MA_RS_MARG_SelectAll.CheckedChanged += new EventHandler(On_MA_RS_MARG_CheckedChanged);
            _MA_RS_MARG_CashFlow.CheckedChanged += new EventHandler(On_MA_RS_MARG_CheckedChanged);
            _MA_RS_MARG_CashFlowGraph.CheckedChanged += new EventHandler(On_MA_RS_MARG_CheckedChanged);
            _MA_RS_MARG_NetProfitFromSale.CheckedChanged += new EventHandler(On_MA_RS_MARG_CheckedChanged);
            _MA_RS_MARG_NetProfitGraph.CheckedChanged += new EventHandler(On_MA_RS_MARG_CheckedChanged);
            _MA_RS_MARG_TaxesAndDeductions.CheckedChanged += new EventHandler(On_MA_RS_MARG_CheckedChanged);

			#endregion
		}                                                       
		#endregion

		#region REMOVE EVENT HANDLERS
		public void RemoveEventHandlers()
		{
            //Phase#3 - additional propertrty types
            _BI_propertyType.SelectedIndexChanged -= new EventHandler(On_BI_propertyType_SelectedIndexChanged);

            //REPORTS
            _printBuildingInfo.Click -= new EventHandler(On_PrintReports_Click);
            //_printIncomeSummary.Click -= new EventHandler(On_PrintReports_Click);
            //_printRentRoll.Click -= new EventHandler(On_PrintReports_Click);
            _PrintExpensesDetails.Click -= new EventHandler(On_PrintReports_Click);
            _printExpensesSummary.Click -= new EventHandler(On_PrintReports_Click);
            _printFinancialAnalysis.Click -= new EventHandler(On_PrintReports_Click);
            _printLoanInfo.Click -= new EventHandler(On_PrintReports_Click);
            _MA_RS_MARG_PrintSelectedReports.Click -= new EventHandler(On_MA_PrintSelectedReports_Click);
            
            
            //FLOATERS
			_bi_floater.MouseHover -= new EventHandler(On_bi_floater_MouseHover);
            //_i_floater.MouseHover -= new EventHandler(On_i_floater_MouseHover);
			_e_floater.MouseHover -= new EventHandler(On_e_floater_MouseHover);
			_l_floater.MouseHover -= new EventHandler(On_l_floater_MouseHover);
			_fa_floater.MouseHover -= new EventHandler(On_fa_floater_MouseHover);

			_bi_floater.MouseLeave -= new EventHandler(On_bi_floater_MouseLeave);
            //_i_floater.MouseLeave -= new EventHandler(On_i_floater_MouseLeave);
			_e_floater.MouseLeave -= new EventHandler(On_e_floater_MouseLeave);
			_l_floater.MouseLeave -= new EventHandler(On_l_floater_MouseLeave);
			_fa_floater.MouseLeave -= new EventHandler(On_fa_floater_MouseLeave);
			//FLOATERS END


			_BI_propertyValue.KeyDown -= new KeyEventHandler(On_BI_propertyValue_KeyDown);
			_BI_propertyValue.LostFocus -= new EventHandler(On_BI_propertyValue_LostFocus);

            //_CalculateFromUnitTypeSummary.Click -= new EventHandler(On_CalculateFromUnitTypeSummary_Click);
            //_calculateFromRentRoll.Click -= new EventHandler(On_CalculateFromRentRoll_Click);

            //_I_unitTypeSummary.OnListViewExItemChanged -= new ProfitGrabber.Commercial.ListViewEx.ListViewExItemChanged(I_unitTypeSummary_OnListViewExItemChanged);
            //_I_rentRoll.OnListViewExItemChanged -= new ProfitGrabber.Commercial.ListViewEx.ListViewExItemChanged(I_rentRoll_OnListViewExItemChanged);
			_E_expenses.OnListViewExItemChanged -= new ProfitGrabber.Commercial.ListViewEx.ListViewExItemChanged(E_expenses_OnListViewExItemChanged);

			//LOANS
			_L_1L_Type.SelectedValueChanged -= new EventHandler(On_L_1L_Type_SelectedValueChanged);
			_L_2L_Type.SelectedValueChanged -= new EventHandler(On_L_2L_Type_SelectedValueChanged);
			_L_3L_Type.SelectedValueChanged -= new EventHandler(On_L_3L_Type_SelectedValueChanged);

			_L_PurchasePrice.KeyDown -= new KeyEventHandler(On_L_PurchasePrice_KeyDown);
			_L_PurchasePrice.LostFocus -= new EventHandler(On_L_PurchasePrice_LostFocus);
			_L_DownPayment.KeyDown -= new KeyEventHandler(On_L_DownPayment_KeyDown);
			_L_DownPayment.LostFocus -= new EventHandler(On_L_DownPayment_LostFocus);
			_L_DownPaymentPerc.KeyDown -= new KeyEventHandler(On_L_DownPaymentPerc_KeyDown);
			_L_DownPaymentPerc.SelectedValueChanged -= new EventHandler(On_L_DownPaymentPerc_SelectedValueChanged);

			//FINANCIAL ANALYSIS
			_FA_copyAIfromIncome.Click -= new EventHandler(On_FA_copyAIfromIncome_Click);
			_FA_copyEfromExpenses.Click -= new EventHandler(On_FA_copyEfromExpenses_Click);
			_FA_copyPDfromLoan.Click -= new EventHandler(On_FA_copyPDfromLoan_Click);

			_FA_P_copyPIfromIncome.Click -= new EventHandler(On_FA_P_copyPIfromIncome_Click);
			_FA_P_copyEfromExpenses.Click -= new EventHandler(On_FA_P_copyEfromExpenses_Click);
			_FA_P_copyFromActual.Click -= new EventHandler(On_FA_P_copyFromActual_Click);

			_FA_S_copyFromActual.Click -= new EventHandler(On_FA_S_copyFromActual_Click);
			_FA_S_copyFromProforma.Click -= new EventHandler(On_FA_S_copyFromProforma_Click);

			#region DESIGNER GENERATED

			this._BI_windowType.Leave -= new System.EventHandler(this.OnTextBoxLeave);
			this._BI_windowType.Enter -= new System.EventHandler(this.OnTextBoxEnter);
			this._BI_roofType.Leave -= new System.EventHandler(this.OnTextBoxLeave);
			this._BI_roofType.Enter -= new System.EventHandler(this.OnTextBoxEnter);
			this._BI_landscaping.Leave -= new System.EventHandler(this.OnTextBoxLeave);
			this._BI_landscaping.Enter -= new System.EventHandler(this.OnTextBoxEnter);
			this._BI_amenities.Leave -= new System.EventHandler(this.OnTextBoxLeave);
			this._BI_amenities.Enter -= new System.EventHandler(this.OnTextBoxEnter);
			this._BI_locationDescription.Leave -= new System.EventHandler(this.OnTextBoxLeave);
			this._BI_locationDescription.Enter -= new System.EventHandler(this.OnTextBoxEnter);
			this._BI_propertyDescription.Leave -= new System.EventHandler(this.OnTextBoxLeave);
			this._BI_propertyDescription.Enter -= new System.EventHandler(this.OnTextBoxEnter);
			this._BI_improvementsNeeded.Leave -= new System.EventHandler(this.OnTextBoxLeave);
			this._BI_improvementsNeeded.Enter -= new System.EventHandler(this.OnTextBoxEnter);
			this._BI_recentImprovements.Leave -= new System.EventHandler(this.OnTextBoxLeave);
			this._BI_recentImprovements.Enter -= new System.EventHandler(this.OnTextBoxEnter);
			this._BI_landLeaseComments.Leave -= new System.EventHandler(this.OnTextBoxLeave);
			this._BI_landLeaseComments.Enter -= new System.EventHandler(this.OnTextBoxEnter);
			this._BI_rent.KeyDown -= new System.Windows.Forms.KeyEventHandler(this._BI_OnKeyDown);
			this._BI_rent.Leave -= new System.EventHandler(this.OnTextBoxLeave);
			this._BI_rent.Enter -= new System.EventHandler(this.OnTextBoxEnter);
			this._BI_endDate.Leave -= new System.EventHandler(this.OnTextBoxLeave);
			this._BI_endDate.Enter -= new System.EventHandler(this.OnTextBoxEnter);
			this._BI_startDate.Leave -= new System.EventHandler(this.OnTextBoxLeave);
			this._BI_startDate.Enter -= new System.EventHandler(this.OnTextBoxEnter);
			this._BI_lessor.Leave -= new System.EventHandler(this.OnTextBoxLeave);
			this._BI_lessor.Enter -= new System.EventHandler(this.OnTextBoxEnter);
			this._BI_option.SelectedValueChanged -= new System.EventHandler(this.OnCheckBoxSelectedValueChanged);
			this._BI_option.Leave -= new System.EventHandler(this.OnCheckBoxLeave);
			this._BI_landLease.SelectedValueChanged -= new System.EventHandler(this.OnCheckBoxSelectedValueChanged);
			this._BI_landLease.Leave -= new System.EventHandler(this.OnCheckBoxLeave);
			this._BI_comments.Leave -= new System.EventHandler(this.OnTextBoxLeave);
			this._BI_comments.Enter -= new System.EventHandler(this.OnTextBoxEnter);
			this._BI_parkingRatio.Leave -= new System.EventHandler(this.OnTextBoxLeave);
			this._BI_parkingRatio.Enter -= new System.EventHandler(this.OnTextBoxEnter);
			this._BI_totalFee.Leave -= new System.EventHandler(this.OnTextBoxLeave);
			this._BI_totalFee.Enter -= new System.EventHandler(this.OnTextBoxEnter);
			this._BI_garageFee.KeyDown -= new System.Windows.Forms.KeyEventHandler(this._BI_OnKeyDown);
			this._BI_garageFee.Leave -= new System.EventHandler(this.OnTextBoxLeave);
			this._BI_garageFee.Enter -= new System.EventHandler(this.OnTextBoxEnter);
			this._BI_coveredFee.KeyDown -= new System.Windows.Forms.KeyEventHandler(this._BI_OnKeyDown);
			this._BI_coveredFee.Leave -= new System.EventHandler(this.OnTextBoxLeave);
			this._BI_coveredFee.Enter -= new System.EventHandler(this.OnTextBoxEnter);
			this._BI_lotFee.KeyDown -= new System.Windows.Forms.KeyEventHandler(this._BI_OnKeyDown);
			this._BI_lotFee.Leave -= new System.EventHandler(this.OnTextBoxLeave);
			this._BI_lotFee.Enter -= new System.EventHandler(this.OnTextBoxEnter);
			this._BI_totalSpaces.Leave -= new System.EventHandler(this.OnTextBoxLeave);
			this._BI_totalSpaces.Enter -= new System.EventHandler(this.OnTextBoxEnter);
			this._BI_garageSpaces.KeyDown -= new System.Windows.Forms.KeyEventHandler(this._BI_OnKeyDown);
			this._BI_garageSpaces.Leave -= new System.EventHandler(this.OnTextBoxLeave);
			this._BI_garageSpaces.Enter -= new System.EventHandler(this.OnTextBoxEnter);
			this._BI_coveredSpaces.KeyDown -= new System.Windows.Forms.KeyEventHandler(this._BI_OnKeyDown);
			this._BI_coveredSpaces.Leave -= new System.EventHandler(this.OnTextBoxLeave);
			this._BI_coveredSpaces.Enter -= new System.EventHandler(this.OnTextBoxEnter);
			this._BI_lotSpaces.KeyDown -= new System.Windows.Forms.KeyEventHandler(this._BI_OnKeyDown);
			this._BI_lotSpaces.Leave -= new System.EventHandler(this.OnTextBoxLeave);
			this._BI_lotSpaces.Enter -= new System.EventHandler(this.OnTextBoxEnter);
			this._BI_windowsAge.SelectedValueChanged -= new System.EventHandler(this.OnCheckBoxSelectedValueChanged);
			this._BI_windowsAge.Leave -= new System.EventHandler(this.OnCheckBoxLeave);
			this._BI_sidingType.SelectedValueChanged -= new System.EventHandler(this.OnCheckBoxSelectedValueChanged);
			this._BI_sidingType.Leave -= new System.EventHandler(this.OnCheckBoxLeave);
			this._BI_electricalAge.SelectedValueChanged -= new System.EventHandler(this.OnCheckBoxSelectedValueChanged);
			this._BI_electricalAge.Leave -= new System.EventHandler(this.OnCheckBoxLeave);
			this._BI_plumbingAge.SelectedValueChanged -= new System.EventHandler(this.OnCheckBoxSelectedValueChanged);
			this._BI_plumbingAge.Leave -= new System.EventHandler(this.OnCheckBoxLeave);
			this._BI_plumbingType.SelectedValueChanged -= new System.EventHandler(this.OnCheckBoxSelectedValueChanged);
			this._BI_plumbingType.Leave -= new System.EventHandler(this.OnCheckBoxLeave);
			this._BI_roofAge.Leave -= new System.EventHandler(this.OnCheckBoxLeave);
			this._BI_lotSqft.Leave -= new System.EventHandler(this.OnTextBoxLeave);
			this._BI_lotSqft.Enter -= new System.EventHandler(this.OnTextBoxEnter);
			this._BI_lotAcres.Leave -= new System.EventHandler(this.OnTextBoxLeave);
			this._BI_lotAcres.Enter -= new System.EventHandler(this.OnTextBoxEnter);
			this._BI_netRentableSqFt.Leave -= new System.EventHandler(this.OnTextBoxLeave);
			this._BI_netRentableSqFt.Enter -= new System.EventHandler(this.OnTextBoxEnter);
			this._BI_totalSqFt.Leave -= new System.EventHandler(this.OnTextBoxLeave);
			this._BI_totalSqFt.Enter -= new System.EventHandler(this.OnTextBoxEnter);
			this._BI_structures.SelectedValueChanged -= new System.EventHandler(this.OnCheckBoxSelectedValueChanged);
			this._BI_structures.Leave -= new System.EventHandler(this.OnCheckBoxLeave);
			this._BI_floors.SelectedValueChanged -= new System.EventHandler(this.OnCheckBoxSelectedValueChanged);
			this._BI_floors.Leave -= new System.EventHandler(this.OnCheckBoxLeave);
			this._BI_units.SelectedValueChanged -= new System.EventHandler(this.OnCheckBoxSelectedValueChanged);
			this._BI_units.Leave -= new System.EventHandler(this.OnCheckBoxLeave);
			this._BI_rehabYear.SelectedValueChanged -= new System.EventHandler(this.OnCheckBoxSelectedValueChanged);
			this._BI_rehabYear.Leave -= new System.EventHandler(this.OnCheckBoxLeave);
			this._BI_yearBuilt.SelectedValueChanged -= new System.EventHandler(this.OnCheckBoxSelectedValueChanged);
			this._BI_yearBuilt.Leave -= new System.EventHandler(this.OnCheckBoxLeave);
			this._BI_propertyValue.KeyDown -= new System.Windows.Forms.KeyEventHandler(this._BI_OnKeyDown);
			this._BI_propertyValue.Leave -= new System.EventHandler(this.OnTextBoxLeave);
			this._BI_propertyValue.Enter -= new System.EventHandler(this.OnTextBoxEnter);
			this._BI_apn.Leave -= new System.EventHandler(this.OnTextBoxLeave);
			this._BI_apn.Enter -= new System.EventHandler(this.OnTextBoxEnter);
			this._BI_class.Leave -= new System.EventHandler(this.OnCheckBoxLeave);
			this._BI_propertyType.Leave -= new System.EventHandler(this.OnCheckBoxLeave);
			this._BI_propertyName.Leave -= new System.EventHandler(this.OnTextBoxLeave);
			this._BI_propertyName.Enter -= new System.EventHandler(this.OnTextBoxEnter);
            //this._I_PI_VacancyOrCreditLossPerc.KeyPress -= new System.Windows.Forms.KeyPressEventHandler(this.I_P_VacancyPerc_OnKeyPressed);
            //this._I_PI_VacancyOrCreditLossPerc.Leave -= new System.EventHandler(this.I_P_VacancyPerc_OnTextBoxLeave);
            //this._I_PI_OtherIncome.KeyPress -= new System.Windows.Forms.KeyPressEventHandler(this.I_P_OtherIncome_OnKeyPressed);
            //this._I_PI_OtherIncome.Leave -= new System.EventHandler(this.I_P_OtherIncome_OnTextBoxLeave);
            //this._I_PI_GrossRent.KeyPress -= new System.Windows.Forms.KeyPressEventHandler(this.I_P_GSI_OnKeyPressed);
            //this._I_PI_GrossRent.Leave -= new System.EventHandler(this.I_P_GSI_OnTextBoxLeave);
            //this._I_AI_VacancyOrCreditLossPerc.KeyPress -= new System.Windows.Forms.KeyPressEventHandler(this.I_AI_LossPerc_OnKeyPressed);
            //this._I_AI_VacancyOrCreditLossPerc.Leave -= new System.EventHandler(this.I_AI_LossPerc_OnTextBoxLeave);
            //this._I_AI_VacancyOrCreditLossPerc.Enter -= new System.EventHandler(this.OnTextBoxEnter);
            //this._I_AI_VacancyOrCreditLoss.Enter -= new System.EventHandler(this.OnTextBoxEnter);
            //this._I_AI_GrossRent.KeyPress -= new System.Windows.Forms.KeyPressEventHandler(this.I_AI_GSI_OnKeyPressed);
            //this._I_AI_GrossRent.Leave -= new System.EventHandler(this.I_AI_GSI_OnTextBoxLeave);
            //this._I_AI_GrossRent.Enter -= new System.EventHandler(this.OnTextBoxEnter);
            //this._I_OI_Other.KeyDown -= new System.Windows.Forms.KeyEventHandler(this.I_OI_KeyDown);
            //this._I_OI_Other.Leave -= new System.EventHandler(this.I_OnTextBoxLeave);
            //this._I_OI_Other.Enter -= new System.EventHandler(this.OnTextBoxEnter);
            //this._I_OI_Vending.KeyDown -= new System.Windows.Forms.KeyEventHandler(this.I_OI_KeyDown);
            //this._I_OI_Vending.Leave -= new System.EventHandler(this.I_OnTextBoxLeave);
            //this._I_OI_Vending.Enter -= new System.EventHandler(this.OnTextBoxEnter);
            //this._I_OI_Laundry.KeyDown -= new System.Windows.Forms.KeyEventHandler(this.I_OI_KeyDown);
            //this._I_OI_Laundry.Leave -= new System.EventHandler(this.I_OnTextBoxLeave);
            //this._I_OI_Laundry.Enter -= new System.EventHandler(this.OnTextBoxEnter);
            //this._I_OI_Parking.KeyDown -= new System.Windows.Forms.KeyEventHandler(this.I_OI_KeyDown);
            //this._I_OI_Parking.Leave -= new System.EventHandler(this.I_OnTextBoxLeave);
            //this._I_OI_Parking.Enter -= new System.EventHandler(this.OnTextBoxEnter);
			this._E_CA_Type_Year10.KeyDown -= new System.Windows.Forms.KeyEventHandler(this._E_CA_OnKeyDown);
			this._E_CA_Type_Year10.Leave -= new System.EventHandler(this._E_CA_OnLeave);
			this._E_CA_Cost_Year10.KeyDown -= new System.Windows.Forms.KeyEventHandler(this._E_CA_OnKeyDown);
			this._E_CA_Cost_Year10.Leave -= new System.EventHandler(this._E_CA_OnLeave);
			this._E_CA_Type_Year9.KeyDown -= new System.Windows.Forms.KeyEventHandler(this._E_CA_OnKeyDown);
			this._E_CA_Type_Year9.Leave -= new System.EventHandler(this._E_CA_OnLeave);
			this._E_CA_Cost_Year9.KeyDown -= new System.Windows.Forms.KeyEventHandler(this._E_CA_OnKeyDown);
			this._E_CA_Cost_Year9.Leave -= new System.EventHandler(this._E_CA_OnLeave);
			this._E_CA_Type_Year8.KeyDown -= new System.Windows.Forms.KeyEventHandler(this._E_CA_OnKeyDown);
			this._E_CA_Type_Year8.Leave -= new System.EventHandler(this._E_CA_OnLeave);
			this._E_CA_Cost_Year8.KeyDown -= new System.Windows.Forms.KeyEventHandler(this._E_CA_OnKeyDown);
			this._E_CA_Cost_Year8.Leave -= new System.EventHandler(this._E_CA_OnLeave);
			this._E_CA_Type_Year7.KeyDown -= new System.Windows.Forms.KeyEventHandler(this._E_CA_OnKeyDown);
			this._E_CA_Type_Year7.Leave -= new System.EventHandler(this._E_CA_OnLeave);
			this._E_CA_Cost_Year7.KeyDown -= new System.Windows.Forms.KeyEventHandler(this._E_CA_OnKeyDown);
			this._E_CA_Cost_Year7.Leave -= new System.EventHandler(this._E_CA_OnLeave);
			this._E_CA_Type_Year6.KeyDown -= new System.Windows.Forms.KeyEventHandler(this._E_CA_OnKeyDown);
			this._E_CA_Type_Year6.Leave -= new System.EventHandler(this._E_CA_OnLeave);
			this._E_CA_Cost_Year6.KeyDown -= new System.Windows.Forms.KeyEventHandler(this._E_CA_OnKeyDown);
			this._E_CA_Cost_Year6.Leave -= new System.EventHandler(this._E_CA_OnLeave);
			this._E_CA_Type_Year5.KeyDown -= new System.Windows.Forms.KeyEventHandler(this._E_CA_OnKeyDown);
			this._E_CA_Type_Year5.Leave -= new System.EventHandler(this._E_CA_OnLeave);
			this._E_CA_Cost_Year5.KeyDown -= new System.Windows.Forms.KeyEventHandler(this._E_CA_OnKeyDown);
			this._E_CA_Cost_Year5.Leave -= new System.EventHandler(this._E_CA_OnLeave);
			this._E_CA_Type_Year4.KeyDown -= new System.Windows.Forms.KeyEventHandler(this._E_CA_OnKeyDown);
			this._E_CA_Type_Year4.Leave -= new System.EventHandler(this._E_CA_OnLeave);
			this._E_CA_Cost_Year4.KeyDown -= new System.Windows.Forms.KeyEventHandler(this._E_CA_OnKeyDown);
			this._E_CA_Cost_Year4.Leave -= new System.EventHandler(this._E_CA_OnLeave);
			this._E_CA_Type_Year3.KeyDown -= new System.Windows.Forms.KeyEventHandler(this._E_CA_OnKeyDown);
			this._E_CA_Type_Year3.Leave -= new System.EventHandler(this._E_CA_OnLeave);
			this._E_CA_Cost_Year3.KeyDown -= new System.Windows.Forms.KeyEventHandler(this._E_CA_OnKeyDown);
			this._E_CA_Cost_Year3.Leave -= new System.EventHandler(this._E_CA_OnLeave);
			this._E_CA_Type_Year2.KeyDown -= new System.Windows.Forms.KeyEventHandler(this._E_CA_OnKeyDown);
			this._E_CA_Type_Year2.Leave -= new System.EventHandler(this._E_CA_OnLeave);
			this._E_CA_Cost_Year2.KeyDown -= new System.Windows.Forms.KeyEventHandler(this._E_CA_OnKeyDown);
			this._E_CA_Cost_Year2.Leave -= new System.EventHandler(this._E_CA_OnLeave);
			this._E_CA_Type_Year1.KeyDown -= new System.Windows.Forms.KeyEventHandler(this._E_CA_OnKeyDown);
			this._E_CA_Type_Year1.Leave -= new System.EventHandler(this._E_CA_OnLeave);
			this._E_CA_Cost_Year1.KeyDown -= new System.Windows.Forms.KeyEventHandler(this._E_CA_OnKeyDown);
			this._E_CA_Cost_Year1.Leave -= new System.EventHandler(this._E_CA_OnLeave);
			this._E_UT_HotWater.SelectedIndexChanged -= new System.EventHandler(this._E_UT_HotWaterSelectedIndexChanged);
			this._E_UT_Heat.SelectedIndexChanged -= new System.EventHandler(this._E_UT_Heat_SelectedIndexChanged);
			this._E_UT_ElectricityMeter.SelectedIndexChanged -= new System.EventHandler(this._E_UT_ElMeter_SelectedIndexChanged);
			this._L_3L_RA_LifeTimeCap.TextChanged -= new System.EventHandler(this.OnLoansTextChanged);
			this._L_3L_RA_AnnualCap.TextChanged -= new System.EventHandler(this.OnLoansTextChanged);
			this._L_3L_RA_InitialRateCap.TextChanged -= new System.EventHandler(this.OnLoansTextChanged);
			this._L_3L_RA_InitFixedPeriod.TextChanged -= new System.EventHandler(this.OnLoansTextChanged);
			this._L_3L_RA_RateYear10.TextChanged -= new System.EventHandler(this.OnLoansTextChanged);
			this._L_3L_RA_RateYear9.TextChanged -= new System.EventHandler(this.OnLoansTextChanged);
			this._L_3L_RA_RateYear8.TextChanged -= new System.EventHandler(this.OnLoansTextChanged);
			this._L_3L_RA_RateYear7.TextChanged -= new System.EventHandler(this.OnLoansTextChanged);
			this._L_3L_RA_RateYear6.TextChanged -= new System.EventHandler(this.OnLoansTextChanged);
			this._L_3L_RA_RateYear5.TextChanged -= new System.EventHandler(this.OnLoansTextChanged);
			this._L_3L_RA_RateYear4.TextChanged -= new System.EventHandler(this.OnLoansTextChanged);
			this._L_3L_RA_RateYear3.TextChanged -= new System.EventHandler(this.OnLoansTextChanged);
			this._L_3L_RA_RateYear2.TextChanged -= new System.EventHandler(this.OnLoansTextChanged);
			this._L_3L_RA_RateYear1.TextChanged -= new System.EventHandler(this.OnLoansTextChanged);
			this._L_3L_Lender.TextChanged -= new System.EventHandler(this.OnLoansTextChanged);
			this._L_3L_LastPaymentDate.TextChanged -= new System.EventHandler(this.OnLoansTextChanged);
			this._L_3L_FirstPaymentDate.TextChanged -= new System.EventHandler(this.OnLoansTextChanged);
			this._L_3L_Points.KeyDown -= new System.Windows.Forms.KeyEventHandler(this.L_Points_KeyDown);
			this._L_3L_Points.Leave -= new System.EventHandler(this.L_Points_Leave);
			this._L_3L_PointsPerc.KeyDown -= new System.Windows.Forms.KeyEventHandler(this.L_PointsPercKeyDown);
			this._L_3L_PointsPerc.Leave -= new System.EventHandler(this.L_PointsPercLeave);
			this._L_3L_MonthlyPayment.TextChanged -= new System.EventHandler(this.OnLoansTextChanged);
			this._L_3L_Rate.TextChanged -= new System.EventHandler(this.OnLoansTextChanged);
			this._L_3L_AmortTerm.TextChanged -= new System.EventHandler(this.OnLoansTextChanged);
			this._L_3L_Amount.KeyDown -= new System.Windows.Forms.KeyEventHandler(this._L_OnAmountKeyDown);
			this._L_3L_Amount.TextChanged -= new System.EventHandler(this.OnLoansTextChanged);
			this._L_3L_Amount.Leave -= new System.EventHandler(this._L_OnAmountLeave);
			this._L_3L_LTVPerc.KeyDown -= new System.Windows.Forms.KeyEventHandler(this._L_OnLTVKeyDown);
			this._L_3L_LTVPerc.Leave -= new System.EventHandler(this._L_OnLTVLeave);
			this._L_3L_Type.SelectedValueChanged -= new System.EventHandler(this.OnLoanTypeChanged);
			this._L_2L_RA_LifeTimeCap.TextChanged -= new System.EventHandler(this.OnLoansTextChanged);
			this._L_2L_RA_AnnualCap.TextChanged -= new System.EventHandler(this.OnLoansTextChanged);
			this._L_2L_RA_InitialRateCap.TextChanged -= new System.EventHandler(this.OnLoansTextChanged);
			this._L_2L_RA_InitFixedPeriod.TextChanged -= new System.EventHandler(this.OnLoansTextChanged);
			this._L_2L_RA_RateYear10.TextChanged -= new System.EventHandler(this.OnLoansTextChanged);
			this._L_2L_RA_RateYear9.TextChanged -= new System.EventHandler(this.OnLoansTextChanged);
			this._L_2L_RA_RateYear8.TextChanged -= new System.EventHandler(this.OnLoansTextChanged);
			this._L_2L_RA_RateYear7.TextChanged -= new System.EventHandler(this.OnLoansTextChanged);
			this._L_2L_RA_RateYear6.TextChanged -= new System.EventHandler(this.OnLoansTextChanged);
			this._L_2L_RA_RateYear5.TextChanged -= new System.EventHandler(this.OnLoansTextChanged);
			this._L_2L_RA_RateYear4.TextChanged -= new System.EventHandler(this.OnLoansTextChanged);
			this._L_2L_RA_RateYear3.TextChanged -= new System.EventHandler(this.OnLoansTextChanged);
			this._L_2L_RA_RateYear2.TextChanged -= new System.EventHandler(this.OnLoansTextChanged);
			this._L_2L_RA_RateYear1.TextChanged -= new System.EventHandler(this.OnLoansTextChanged);
			this._L_2L_Lender.TextChanged -= new System.EventHandler(this.OnLoansTextChanged);
			this._L_2L_LastPaymentDate.TextChanged -= new System.EventHandler(this.OnLoansTextChanged);
			this._L_2L_FirstPaymentDate.TextChanged -= new System.EventHandler(this.OnLoansTextChanged);
			this._L_2L_Points.KeyDown -= new System.Windows.Forms.KeyEventHandler(this.L_Points_KeyDown);
			this._L_2L_Points.Leave -= new System.EventHandler(this.L_Points_Leave);
			this._L_2L_PointsPerc.KeyDown -= new System.Windows.Forms.KeyEventHandler(this.L_PointsPercKeyDown);
			this._L_2L_PointsPerc.Leave -= new System.EventHandler(this.L_PointsPercLeave);
			this._L_2L_MonthlyPayment.TextChanged -= new System.EventHandler(this.OnLoansTextChanged);
			this._L_2L_Rate.TextChanged -= new System.EventHandler(this.OnLoansTextChanged);
			this._L_2L_AmortTerm.TextChanged -= new System.EventHandler(this.OnLoansTextChanged);
			this._L_2L_Amount.KeyDown -= new System.Windows.Forms.KeyEventHandler(this._L_OnAmountKeyDown);
			this._L_2L_Amount.TextChanged -= new System.EventHandler(this.OnLoansTextChanged);
			this._L_2L_Amount.Leave -= new System.EventHandler(this._L_OnAmountLeave);
			this._L_2L_LTVPerc.KeyDown -= new System.Windows.Forms.KeyEventHandler(this._L_OnLTVKeyDown);
			this._L_2L_LTVPerc.Leave -= new System.EventHandler(this._L_OnLTVLeave);
			this._L_2L_Type.SelectedValueChanged -= new System.EventHandler(this.OnLoanTypeChanged);
			this._L_1L_RA_LifeTimeCap.TextChanged -= new System.EventHandler(this.OnLoansTextChanged);
			this._L_1L_RA_AnnualCap.TextChanged -= new System.EventHandler(this.OnLoansTextChanged);
			this._L_1L_RA_InitialRateCap.TextChanged -= new System.EventHandler(this.OnLoansTextChanged);
			this._L_1L_RA_InitFixedPeriod.TextChanged -= new System.EventHandler(this.OnLoansTextChanged);
			this._L_1L_RA_RateYear10.TextChanged -= new System.EventHandler(this.OnLoansTextChanged);
			this._L_1L_RA_RateYear9.TextChanged -= new System.EventHandler(this.OnLoansTextChanged);
			this._L_1L_RA_RateYear8.TextChanged -= new System.EventHandler(this.OnLoansTextChanged);
			this._L_1L_RA_RateYear7.TextChanged -= new System.EventHandler(this.OnLoansTextChanged);
			this._L_1L_RA_RateYear6.TextChanged -= new System.EventHandler(this.OnLoansTextChanged);
			this._L_1L_RA_RateYear5.TextChanged -= new System.EventHandler(this.OnLoansTextChanged);
			this._L_1L_RA_RateYear4.TextChanged -= new System.EventHandler(this.OnLoansTextChanged);
			this._L_1L_RA_RateYear3.TextChanged -= new System.EventHandler(this.OnLoansTextChanged);
			this._L_1L_RA_RateYear2.TextChanged -= new System.EventHandler(this.OnLoansTextChanged);
			this._L_1L_RA_RateYear1.TextChanged -= new System.EventHandler(this.OnLoansTextChanged);
			this._L_1L_Lender.TextChanged -= new System.EventHandler(this.OnLoansTextChanged);
			this._L_1L_LastPaymentDate.TextChanged -= new System.EventHandler(this.OnLoansTextChanged);
			this._L_1L_FirstPaymentDate.TextChanged -= new System.EventHandler(this.OnLoansTextChanged);
			this._L_1L_Points.KeyDown -= new System.Windows.Forms.KeyEventHandler(this.L_Points_KeyDown);
			this._L_1L_Points.Leave -= new System.EventHandler(this.L_Points_Leave);
			this._L_1L_PointsPerc.KeyDown -= new System.Windows.Forms.KeyEventHandler(this.L_PointsPercKeyDown);
			this._L_1L_PointsPerc.Leave -= new System.EventHandler(this.L_PointsPercLeave);
			this._L_1L_MonthlyPayment.TextChanged -= new System.EventHandler(this.OnLoansTextChanged);
			this._L_1L_Rate.TextChanged -= new System.EventHandler(this.OnLoansTextChanged);
			this._L_1L_AmortTerm.TextChanged -= new System.EventHandler(this.OnLoansTextChanged);
			this._L_1L_Amount.KeyDown -= new System.Windows.Forms.KeyEventHandler(this._L_OnAmountKeyDown);
			this._L_1L_Amount.TextChanged -= new System.EventHandler(this.OnLoansTextChanged);
			this._L_1L_Amount.Leave -= new System.EventHandler(this._L_OnAmountLeave);
			this._L_1L_LTVPerc.KeyDown -= new System.Windows.Forms.KeyEventHandler(this._L_OnLTVKeyDown);
			this._L_1L_LTVPerc.Leave -= new System.EventHandler(this._L_OnLTVLeave);
			this._L_1L_Type.SelectedValueChanged -= new System.EventHandler(this.OnLoanTypeChanged);
			this._L_SellerCarryback.SelectedIndexChanged -= new System.EventHandler(this._L_SellerCBSelectedIndexChanged);
			this._L_Assumable.SelectedIndexChanged -= new System.EventHandler(this._L_SecondAssumableSelectedIndexChanged);
			this._L_1stAssumable.SelectedIndexChanged -= new System.EventHandler(this._L_FirstAssumableSelectedIndexChanged);
			this._FA_S_OperatingExpensesPerc.KeyDown -= new System.Windows.Forms.KeyEventHandler(this._FA_S_OnKeyDown);
			this._FA_S_OperatingExpensesPerc.Leave -= new System.EventHandler(this._FA_S_OnLeave);
			this._FA_P_OperatingExpensesPerc.KeyDown -= new System.Windows.Forms.KeyEventHandler(this._FA_P_OnKeyDown);
			this._FA_P_OperatingExpensesPerc.Leave -= new System.EventHandler(this._FA_P_OnLeave);
			this._FA_A_OperatingExpensesPerc.KeyDown -= new System.Windows.Forms.KeyEventHandler(this._FA_A_OnKeyDown);
			this._FA_A_OperatingExpensesPerc.Leave -= new System.EventHandler(this._FA_A_OnLeave);
			this._FA_S_Comment.KeyDown -= new System.Windows.Forms.KeyEventHandler(this._FA_S_OnKeyDown);
			this._FA_S_Comment.Leave -= new System.EventHandler(this._FA_S_OnLeave);
			this._FA_S_VacancyCreditLossPerc.KeyDown -= new System.Windows.Forms.KeyEventHandler(this._FA_S_OnKeyDown);
			this._FA_S_VacancyCreditLossPerc.Leave -= new System.EventHandler(this._FA_S_OnLeave);
			this._FA_S_DebtService.KeyDown -= new System.Windows.Forms.KeyEventHandler(this._FA_S_OnKeyDown);
			this._FA_S_DebtService.Leave -= new System.EventHandler(this._FA_S_OnLeave);
			this._FA_S_CapRate.KeyDown -= new System.Windows.Forms.KeyEventHandler(this._FA_S_OnKeyDown);
			this._FA_S_CapRate.Leave -= new System.EventHandler(this._FA_S_OnLeave);
			this._FA_S_OperatingExpenses.KeyDown -= new System.Windows.Forms.KeyEventHandler(this._FA_S_OnKeyDown);
			this._FA_S_OperatingExpenses.Leave -= new System.EventHandler(this._FA_S_OnLeave);
			this._FA_S_OtherIncome.KeyDown -= new System.Windows.Forms.KeyEventHandler(this._FA_S_OnKeyDown);
			this._FA_S_OtherIncome.Leave -= new System.EventHandler(this._FA_S_OnLeave);
			this._FA_S_VacancyCreditLoss.KeyDown -= new System.Windows.Forms.KeyEventHandler(this._FA_S_OnKeyDown);
			this._FA_S_VacancyCreditLoss.Leave -= new System.EventHandler(this._FA_S_OnLeave);
			this._FA_S_GSIAnnual.KeyDown -= new System.Windows.Forms.KeyEventHandler(this._FA_S_OnKeyDown);
			this._FA_S_GSIAnnual.Leave -= new System.EventHandler(this._FA_S_OnLeave);
			this._FA_S_Price.KeyDown -= new System.Windows.Forms.KeyEventHandler(this._FA_S_OnKeyDown);
			this._FA_S_Price.Leave -= new System.EventHandler(this._FA_S_OnLeave);
			this._FA_P_Comment.KeyDown -= new System.Windows.Forms.KeyEventHandler(this._FA_P_OnKeyDown);
			this._FA_P_Comment.Leave -= new System.EventHandler(this._FA_P_OnLeave);
			this._FA_P_VacancyCreditLossPerc.KeyDown -= new System.Windows.Forms.KeyEventHandler(this._FA_P_OnKeyDown);
			this._FA_P_VacancyCreditLossPerc.Leave -= new System.EventHandler(this._FA_P_OnLeave);
			this._FA_P_DebtService.KeyDown -= new System.Windows.Forms.KeyEventHandler(this._FA_P_OnKeyDown);
			this._FA_P_DebtService.Leave -= new System.EventHandler(this._FA_P_OnLeave);
			this._FA_P_CapRate.KeyDown -= new System.Windows.Forms.KeyEventHandler(this._FA_P_OnKeyDown);
			this._FA_P_CapRate.Leave -= new System.EventHandler(this._FA_P_OnLeave);
			this._FA_P_OperatingExpenses.KeyDown -= new System.Windows.Forms.KeyEventHandler(this._FA_P_OnKeyDown);
			this._FA_P_OperatingExpenses.Leave -= new System.EventHandler(this._FA_P_OnLeave);
			this._FA_P_OtherIncome.KeyDown -= new System.Windows.Forms.KeyEventHandler(this._FA_P_OnKeyDown);
			this._FA_P_OtherIncome.Leave -= new System.EventHandler(this._FA_P_OnLeave);
			this._FA_P_VacancyCreditLoss.KeyDown -= new System.Windows.Forms.KeyEventHandler(this._FA_P_OnKeyDown);
			this._FA_P_VacancyCreditLoss.Leave -= new System.EventHandler(this._FA_P_OnLeave);
			this._FA_P_GSIAnnual.KeyDown -= new System.Windows.Forms.KeyEventHandler(this._FA_P_OnKeyDown);
			this._FA_P_GSIAnnual.Leave -= new System.EventHandler(this._FA_P_OnLeave);
			this._FA_P_Price.KeyDown -= new System.Windows.Forms.KeyEventHandler(this._FA_P_OnKeyDown);
			this._FA_P_Price.Leave -= new System.EventHandler(this._FA_P_OnLeave);
			this._FA_A_Comment.KeyDown -= new System.Windows.Forms.KeyEventHandler(this._FA_A_OnKeyDown);
			this._FA_A_Comment.Leave -= new System.EventHandler(this._FA_A_OnLeave);
			this._FA_A_VacancyCreditLossPerc.KeyDown -= new System.Windows.Forms.KeyEventHandler(this._FA_A_OnKeyDown);
			this._FA_A_VacancyCreditLossPerc.Leave -= new System.EventHandler(this._FA_A_OnLeave);
			this._FA_A_DebtService.KeyDown -= new System.Windows.Forms.KeyEventHandler(this._FA_A_OnKeyDown);
			this._FA_A_DebtService.Leave -= new System.EventHandler(this._FA_A_OnLeave);
			this._FA_A_CapRate.KeyDown -= new System.Windows.Forms.KeyEventHandler(this._FA_A_OnKeyDown);
			this._FA_A_CapRate.Leave -= new System.EventHandler(this._FA_A_OnLeave);
			this._FA_A_OperatingExpenses.KeyDown -= new System.Windows.Forms.KeyEventHandler(this._FA_A_OnKeyDown);
			this._FA_A_OperatingExpenses.Leave -= new System.EventHandler(this._FA_A_OnLeave);
			this._FA_A_OtherIncome.KeyDown -= new System.Windows.Forms.KeyEventHandler(this._FA_A_OnKeyDown);
			this._FA_A_OtherIncome.Leave -= new System.EventHandler(this._FA_A_OnLeave);
			this._FA_A_VacancyCreditLoss.KeyDown -= new System.Windows.Forms.KeyEventHandler(this._FA_A_OnKeyDown);
			this._FA_A_VacancyCreditLoss.Leave -= new System.EventHandler(this._FA_A_OnLeave);
			this._FA_A_GSIAnnual.KeyDown -= new System.Windows.Forms.KeyEventHandler(this._FA_A_OnKeyDown);
			this._FA_A_GSIAnnual.Leave -= new System.EventHandler(this._FA_A_OnLeave);
			this._FA_A_Price.KeyDown -= new System.Windows.Forms.KeyEventHandler(this._FA_A_OnKeyDown);
			this._FA_A_Price.Leave -= new System.EventHandler(this._FA_A_OnLeave);

            //MULTIYEAR ANALYSIS
            this._MA_PurchasePrice.KeyDown -= new KeyEventHandler(this._MA_KeyDown);
            this._MA_PurchasePrice.Leave -= new EventHandler(this._MA_Leave);
            this._MA_InitialInvestment.KeyDown -= new KeyEventHandler(this._MA_KeyDown);
            this._MA_InitialInvestment.Leave -= new EventHandler(this._MA_Leave);

            this._MA_DownPayment.KeyDown += new KeyEventHandler(this._MA_DownPayment_KeyDown);
            this._MA_DownPayment.Leave += new EventHandler(this._MA_DownPayment_Leave);
            this._MA_Points.KeyDown += new KeyEventHandler(this._MA_Points_KeyDown);
            this._MA_Points.Leave += new EventHandler(this._MA_Points_Leave);
            this._MA_OtherAcquisitionCosts.KeyDown += new KeyEventHandler(this._MA_OtherAcquisitionCosts_KeyDown);
            this._MA_OtherAcquisitionCosts.Leave += new EventHandler(this._MA_OtherAcquisitionCosts_Leave);
                        
            this._MA_S1_GSI.KeyDown -= new KeyEventHandler(this._MA_GSI_KeyDown);
            this._MA_S1_GSI.Leave -= new EventHandler(this._MA_GSI_Leave);
            this._MA_S1_GSI_Perc.KeyDown -= new KeyEventHandler(this._MA_KeyDown);
            this._MA_S1_GSI_Perc.Leave -= new EventHandler(this._MA_Leave);
            this._MA_S1_Vacancy.KeyDown -= new KeyEventHandler(this._MA_Vacancy_KeyDown);
            this._MA_S1_Vacancy.Leave -= new EventHandler(this._MA_Vacancy_Leave);
            this._MA_S1_VacancyPerc.KeyDown -= new KeyEventHandler(this._MA_VacancyPerc_KeyDown);
            this._MA_S1_VacancyPerc.Leave -= new EventHandler(this._MA_VacancyPerc_Leave);            
            this._MA_S1_OtherIncome.KeyDown -= new KeyEventHandler(this._MA_KeyDown);
            this._MA_S1_OtherIncome.Leave -= new EventHandler(this._MA_Leave);
            this._MA_S1_OtherIncomePerc.KeyDown -= new KeyEventHandler(this._MA_KeyDown);
            this._MA_S1_OtherIncomePerc.Leave -= new EventHandler(this._MA_Leave);
            this._MA_S1_OperatingExpenses.KeyDown -= new KeyEventHandler(this._MA_OperatingExpenses_KeyDown);
            this._MA_S1_OperatingExpenses.Leave -= new EventHandler(this._MA_OperatingExpenses_Leave);
            this._MA_S1_OperatingExpensesPerc.KeyDown -= new KeyEventHandler(this._MA_OperatingExpensesPerc_KeyDown);
            this._MA_S1_OperatingExpensesPerc.Leave -= new EventHandler(this._MA_OperatingExpensesPerc_Leave);
            this._MA_TI_FederalRate.KeyDown -= new KeyEventHandler(this._MA_KeyDown);
            this._MA_TI_FederalRate.Leave -= new EventHandler(this._MA_Leave);
            this._MA_TI_StateRate.KeyDown -= new KeyEventHandler(this._MA_KeyDown);
            this._MA_TI_StateRate.Leave -= new EventHandler(this._MA_Leave);
            this._MA_TI_CapitalGainsRate.KeyDown -= new KeyEventHandler(this._MA_KeyDown);
            this._MA_TI_CapitalGainsRate.Leave -= new EventHandler(this._MA_Leave);
            this._MA_TI_BuildingValue.KeyDown -= new KeyEventHandler(this._MA_KeyDown);
            this._MA_TI_BuildingValue.Leave -= new EventHandler(this._MA_Leave);
            this._MA_SI_SaleCosts.KeyDown -= new KeyEventHandler(this._MA_KeyDown);
            this._MA_SI_SaleCosts.Leave -= new EventHandler(this._MA_Leave);
            this._MA_SI_SaleCapRate.KeyDown -= new KeyEventHandler(this._MA_KeyDown);
            this._MA_SI_SaleCapRate.Leave -= new EventHandler(this._MA_Leave);
            this._MA_S3_OpEx_Y1.KeyDown -= new KeyEventHandler(this._MA_KeyDown);
            this._MA_S3_OpEx_Y1.Leave -= new EventHandler(this._MA_Leave);
            this._MA_S3_OpEx_Y2.KeyDown -= new KeyEventHandler(this._MA_KeyDown);
            this._MA_S3_OpEx_Y2.Leave -= new EventHandler(this._MA_Leave);
            this._MA_S3_OpEx_Y3.KeyDown -= new KeyEventHandler(this._MA_KeyDown);
            this._MA_S3_OpEx_Y3.Leave -= new EventHandler(this._MA_Leave);
            this._MA_S3_OpEx_Y4.KeyDown -= new KeyEventHandler(this._MA_KeyDown);
            this._MA_S3_OpEx_Y4.Leave -= new EventHandler(this._MA_Leave);
            this._MA_S3_OpEx_Y5.KeyDown -= new KeyEventHandler(this._MA_KeyDown);
            this._MA_S3_OpEx_Y5.Leave -= new EventHandler(this._MA_Leave);
            this._MA_S3_OpEx_Y6.KeyDown -= new KeyEventHandler(this._MA_KeyDown);
            this._MA_S3_OpEx_Y6.Leave -= new EventHandler(this._MA_Leave);
            this._MA_S3_OpEx_Y7.KeyDown -= new KeyEventHandler(this._MA_KeyDown);
            this._MA_S3_OpEx_Y7.Leave -= new EventHandler(this._MA_Leave);
            this._MA_S3_OpEx_Y8.KeyDown -= new KeyEventHandler(this._MA_KeyDown);
            this._MA_S3_OpEx_Y8.Leave -= new EventHandler(this._MA_Leave);
            this._MA_S3_OpEx_Y9.KeyDown -= new KeyEventHandler(this._MA_KeyDown);
            this._MA_S3_OpEx_Y9.Leave -= new EventHandler(this._MA_Leave);
            this._MA_S3_OpEx_Y10.KeyDown -= new KeyEventHandler(this._MA_KeyDown);
            this._MA_S3_OpEx_Y10.Leave -= new EventHandler(this._MA_Leave);
            this._MA_S3_Vacancy_Y1.KeyDown -= new KeyEventHandler(this._MA_KeyDown);
            this._MA_S3_Vacancy_Y1.Leave -= new EventHandler(this._MA_Leave);
            this._MA_S3_Vacancy_Y2.KeyDown -= new KeyEventHandler(this._MA_KeyDown);
            this._MA_S3_Vacancy_Y2.Leave -= new EventHandler(this._MA_Leave);
            this._MA_S3_Vacancy_Y3.KeyDown -= new KeyEventHandler(this._MA_KeyDown);
            this._MA_S3_Vacancy_Y3.Leave -= new EventHandler(this._MA_Leave);
            this._MA_S3_Vacancy_Y4.KeyDown -= new KeyEventHandler(this._MA_KeyDown);
            this._MA_S3_Vacancy_Y4.Leave -= new EventHandler(this._MA_Leave);
            this._MA_S3_Vacancy_Y5.KeyDown -= new KeyEventHandler(this._MA_KeyDown);
            this._MA_S3_Vacancy_Y5.Leave -= new EventHandler(this._MA_Leave);
            this._MA_S3_Vacancy_Y6.KeyDown -= new KeyEventHandler(this._MA_KeyDown);
            this._MA_S3_Vacancy_Y6.Leave -= new EventHandler(this._MA_Leave);
            this._MA_S3_Vacancy_Y7.KeyDown -= new KeyEventHandler(this._MA_KeyDown);
            this._MA_S3_Vacancy_Y7.Leave -= new EventHandler(this._MA_Leave);
            this._MA_S3_Vacancy_Y8.KeyDown -= new KeyEventHandler(this._MA_KeyDown);
            this._MA_S3_Vacancy_Y8.Leave -= new EventHandler(this._MA_Leave);
            this._MA_S3_Vacancy_Y9.KeyDown -= new KeyEventHandler(this._MA_KeyDown);
            this._MA_S3_Vacancy_Y9.Leave -= new EventHandler(this._MA_Leave);
            this._MA_S3_Vacancy_Y10.KeyDown -= new KeyEventHandler(this._MA_KeyDown);
            this._MA_S3_Vacancy_Y10.Leave -= new EventHandler(this._MA_Leave);
            this._MA_S3_IncomeChange_Y1.KeyDown -= new KeyEventHandler(this._MA_KeyDown);
            this._MA_S3_IncomeChange_Y1.Leave -= new EventHandler(this._MA_Leave);
            this._MA_S3_IncomeChange_Y2.KeyDown -= new KeyEventHandler(this._MA_KeyDown);
            this._MA_S3_IncomeChange_Y2.Leave -= new EventHandler(this._MA_Leave);
            this._MA_S3_IncomeChange_Y3.KeyDown -= new KeyEventHandler(this._MA_KeyDown);
            this._MA_S3_IncomeChange_Y3.Leave -= new EventHandler(this._MA_Leave);
            this._MA_S3_IncomeChange_Y4.KeyDown -= new KeyEventHandler(this._MA_KeyDown);
            this._MA_S3_IncomeChange_Y4.Leave -= new EventHandler(this._MA_Leave);
            this._MA_S3_IncomeChange_Y5.KeyDown -= new KeyEventHandler(this._MA_KeyDown);
            this._MA_S3_IncomeChange_Y5.Leave -= new EventHandler(this._MA_Leave);
            this._MA_S3_IncomeChange_Y6.KeyDown -= new KeyEventHandler(this._MA_KeyDown);
            this._MA_S3_IncomeChange_Y6.Leave -= new EventHandler(this._MA_Leave);
            this._MA_S3_IncomeChange_Y7.KeyDown -= new KeyEventHandler(this._MA_KeyDown);
            this._MA_S3_IncomeChange_Y7.Leave -= new EventHandler(this._MA_Leave);
            this._MA_S3_IncomeChange_Y8.KeyDown -= new KeyEventHandler(this._MA_KeyDown);
            this._MA_S3_IncomeChange_Y8.Leave -= new EventHandler(this._MA_Leave);
            this._MA_S3_IncomeChange_Y9.KeyDown -= new KeyEventHandler(this._MA_KeyDown);
            this._MA_S3_IncomeChange_Y9.Leave -= new EventHandler(this._MA_Leave);
            this._MA_S3_IncomeChange_Y10.KeyDown -= new KeyEventHandler(this._MA_KeyDown);
            this._MA_S3_IncomeChange_Y10.Leave -= new EventHandler(this._MA_Leave);
            this._MA_S2_InflationPercChange.Leave -= new EventHandler(On_MA_DropDown_Leave);
            this._MA_TI_DepreciableYears.Leave -= new EventHandler(On_MA_DropDown_Leave);
            this._MA_S2_OperatingExpensesPercChange.Leave -= new EventHandler(On_MA_DropDown_Leave);
            this._MA_S2_VacancyPercChange.Leave -= new EventHandler(On_MA_DropDown_Leave);
            this._MA_S2_IncomePercChange.Leave -= new EventHandler(On_MA_DropDown_Leave);
            this._MA_TI_DepreciableYears.Leave -= new EventHandler(On_MA_DropDown_Leave);
            this._MA_RS_MARG_AnalysisPeriod.Leave -= new EventHandler(On_MA_DropDown_Leave);

            _MA_S2_OperatingExpensesPercChange.SelectedIndexChanged -= new EventHandler(On_MA_S2_OperatingExpensesPercChange_SelectedIndexChanged);
            _MA_S2_VacancyPercChange.SelectedIndexChanged -= new EventHandler(On_MA_S2_VacancyPercChange_SelectedIndexChanged);
            _MA_S2_IncomePercChange.SelectedIndexChanged -= new EventHandler(On_MA_S2_IncomePercChange_SelectedIndexChanged);

            _MA_RS_IAR_SelectAll.CheckedChanged -= new EventHandler(On_MA_RS_IAR_CheckedChanged);
            _MA_RS_IAR_BuildingInfo.CheckedChanged -= new EventHandler(On_MA_RS_IAR_CheckedChanged);
            _MA_RS_IAR_InitialIncomeSummary.CheckedChanged -= new EventHandler(On_MA_RS_IAR_CheckedChanged);
            _MA_RS_IAR_InitialExpensesSummary.CheckedChanged -= new EventHandler(On_MA_RS_IAR_CheckedChanged);
            _MA_RS_IAR_LoanSummary.CheckedChanged -= new EventHandler(On_MA_RS_IAR_CheckedChanged);
            _MA_RS_IAR_FinancialAnalisys.CheckedChanged -= new EventHandler(On_MA_RS_IAR_CheckedChanged);

            _MA_RS_MARG_SelectAll.CheckedChanged -= new EventHandler(On_MA_RS_MARG_CheckedChanged);
            _MA_RS_MARG_CashFlow.CheckedChanged -= new EventHandler(On_MA_RS_MARG_CheckedChanged);
            _MA_RS_MARG_CashFlowGraph.CheckedChanged -= new EventHandler(On_MA_RS_MARG_CheckedChanged);
            _MA_RS_MARG_NetProfitFromSale.CheckedChanged -= new EventHandler(On_MA_RS_MARG_CheckedChanged);
            _MA_RS_MARG_NetProfitGraph.CheckedChanged -= new EventHandler(On_MA_RS_MARG_CheckedChanged);
            _MA_RS_MARG_TaxesAndDeductions.CheckedChanged -= new EventHandler(On_MA_RS_MARG_CheckedChanged);

			#endregion
		}              
		#endregion

		#region HANDLE LISTVIEW Ex
		void HandleListViewExes()
		{
			//UNIT TYPE SUMMARY
            //_I_unitTypeSummary.AddSubItem = true;
            //_I_unitTypeSummary.HideComboAfterSelChange = true;

            ////Add EDITABLE cells
            //_I_unitTypeSummary.AddEditableCell(-1, 0);
            //_I_unitTypeSummary.AddEditableCell(-1, 3);
            //_I_unitTypeSummary.AddEditableCell(-1, 4);
            //_I_unitTypeSummary.AddEditableCell(-1, 5);
            //_I_unitTypeSummary.AddEditableCell(-1, 6);
            //_I_unitTypeSummary.AddEditableCell(-1, 7);
            //_I_unitTypeSummary.AddEditableCell(-1, 8);
            //_I_unitTypeSummary.AddEditableCell(-1, 10);
            //_I_unitTypeSummary.AddEditableCell(-1, 11);

			//Create data for COMBOBOX
			StringCollection bedrooms = new StringCollection();
			bedrooms.AddRange(new string[] {string.Empty, "0", "1", "2", "3", "4"});

			StringCollection baths = new StringCollection();
			baths.AddRange(new string[] {string.Empty, "1", "1.5", "2", "2.5", "3"});

			//Set COMBOBOXES
            //_I_unitTypeSummary.AddComboBoxCell( -1, 1, bedrooms ); 
            //_I_unitTypeSummary.AddComboBoxCell( -1, 2, baths ); 


            ////RENT ROLL
            //_I_rentRoll.AddSubItem = true;
            //_I_rentRoll.HideComboAfterSelChange = true;

            ////Add EDITABLE cells
            //_I_rentRoll.AddEditableCell(-1, 0);
            //_I_rentRoll.AddEditableCell(-1, 1);
            //_I_rentRoll.AddEditableCell(-1, 3);
            //_I_rentRoll.AddEditableCell(-1, 4);
            //_I_rentRoll.AddEditableCell(-1, 5);
            //_I_rentRoll.AddEditableCell(-1, 6);
            //_I_rentRoll.AddEditableCell(-1, 7);
            //_I_rentRoll.AddEditableCell(-1, 8);
            //_I_rentRoll.AddEditableCell(-1, 9);
            //_I_rentRoll.AddEditableCell(-1, 10);
            //_I_rentRoll.AddEditableCell(-1, 11);

			//Create data for COMBOBOX
			StringCollection bedBath = new StringCollection();
			bedBath.AddRange(new string[] {string.Empty, "0/1", "1/1", "1/2", "2/1", "2/2", "2/3", "3/1", "3/2", "4/1", "4/2", "4/3"});

			//Set COMBOBOXES
            //_I_rentRoll.AddComboBoxCell(-1, 2, bedBath);

			//EXPENSES
			
			_E_expenses.AddSubItem = true;
			_E_expenses.HideComboAfterSelChange = true;

			//Add EDITABLE cells
			_E_expenses.AddEditableCell(-1, 2);
			_E_expenses.AddEditableCell(-1, 3);
			_E_expenses.AddEditableCell(-1, 4);
			_E_expenses.AddEditableCell(-1, 5);
			_E_expenses.AddEditableCell(-1, 9);

			//Create data for COMBOBOX
			StringCollection paidBy = new StringCollection();
			paidBy.AddRange(new string[] {string.Empty, "Owner", "Tenant"});

			StringCollection highLow = new StringCollection();
			highLow.AddRange(new string[] {string.Empty, "High", "Low"});

			//Set COMBOBOXES
			_E_expenses.AddComboBoxCell(-1, 7, paidBy);
			_E_expenses.AddComboBoxCell(-1, 8, highLow);
			

		}

		#endregion

		#region GENERAL - on TEXT BOX Enter
		private void OnTextBoxEnter(object sender, System.EventArgs e)
		{
//			TextBox tb = (TextBox)sender;
//			tb.BackColor = Color.LightCyan;
		}

		#endregion

		#region BUILDING INFO TEXT & COMBPO BOX HANDLERS
		
		private void _BI_OnKeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Enter)
				Handle_BI_TextBoxes(sender);
		}
		
		private void OnTextBoxLeave(object sender, System.EventArgs e)
		{
			Handle_BI_TextBoxes(sender);
		}

		void Handle_BI_TextBoxes(object sender)
		{
			TextBox tb = (TextBox)sender;

			BuildingInfoObject.Instance.PropertyName = _BI_propertyName.Text;
			BuildingInfoObject.Instance.Apn = _BI_apn.Text;

			try 
			{ 
				BuildingInfoObject.Instance.PropertyValue = Convert.ToDecimal(_BI_propertyValue.Text); 
				_BI_propertyValue.Text = BuildingInfoObject.Instance.PropertyValue.ToString(CurrFormatter);
			} 
			catch {BuildingInfoObject.Instance.PropertyValue = 0; }
			
			try {BuildingInfoObject.Instance.TotalSqFt = Convert.ToInt32(_BI_totalSqFt.Text);}
			catch {BuildingInfoObject.Instance.TotalSqFt = 0;}
			
			try {BuildingInfoObject.Instance.NetRentableSqFt = Convert.ToInt32(_BI_netRentableSqFt.Text);}
			catch {BuildingInfoObject.Instance.NetRentableSqFt = 0;}

			try {BuildingInfoObject.Instance.LotAcres = Convert.ToInt32(_BI_lotAcres.Text);}
			catch {BuildingInfoObject.Instance.LotAcres = 0;}

			try { BuildingInfoObject.Instance.LotSqFt = Convert.ToInt32(_BI_lotSqft.Text);}
			catch {BuildingInfoObject.Instance.LotSqFt = 0;}

			BuildingInfoObject.Instance.RoofType = _BI_roofType.Text;
			BuildingInfoObject.Instance.WindowType = _BI_windowType.Text;

			//readonly
			//BuildingInfoObject.Instance.ParkingRatio = 

			try { BuildingInfoObject.Instance.ParkingLotSpaces = Convert.ToInt32(_BI_lotSpaces.Text);}
			catch {BuildingInfoObject.Instance.ParkingLotSpaces  = 0;}

			try { BuildingInfoObject.Instance.ParkingCoveredSpaces = Convert.ToInt32(_BI_coveredSpaces.Text);}
			catch {BuildingInfoObject.Instance.ParkingCoveredSpaces = 0;}

			try { BuildingInfoObject.Instance.ParkingGarageSpaces = Convert.ToInt32(_BI_garageSpaces.Text);}
			catch {BuildingInfoObject.Instance.ParkingGarageSpaces = 0;}

			try 
			{ 
				BuildingInfoObject.Instance.ParkingLotFee = Convert.ToDecimal(_BI_lotFee.Text);
				_BI_lotFee.Text = BuildingInfoObject.Instance.ParkingLotFee.ToString(CurrFormatter);
			}
			catch {BuildingInfoObject.Instance.ParkingLotFee  = 0;}

			try 
			{ 
				BuildingInfoObject.Instance.ParkingCoveredFee = Convert.ToDecimal(_BI_coveredFee.Text);
				_BI_coveredFee.Text = BuildingInfoObject.Instance.ParkingCoveredFee.ToString(CurrFormatter);
			}
			catch {BuildingInfoObject.Instance.ParkingCoveredFee = 0;}

			try 
			{ 
				BuildingInfoObject.Instance.ParkingGarageFee = Convert.ToDecimal(_BI_garageFee.Text);
				_BI_garageFee.Text = BuildingInfoObject.Instance.ParkingGarageFee.ToString(CurrFormatter);
			}
			catch {BuildingInfoObject.Instance.ParkingGarageFee = 0;}

			//readonly
			//BuildingInfoObject.Instance.ParkingTotalSpaces
			//BuildingInfoObject.Instance.ParkingTotalFee

			BuildingInfoObject.Instance.ParkingComments = _BI_comments.Text;

			BuildingInfoObject.Instance.Lessor = _BI_lessor.Text;
			BuildingInfoObject.Instance.StartDate = _BI_startDate.Text;
			BuildingInfoObject.Instance.EndDate = _BI_endDate.Text;
			
			try 
			{
				BuildingInfoObject.Instance.Rent = Convert.ToDecimal(_BI_rent.Text);
				_BI_rent.Text = BuildingInfoObject.Instance.Rent.ToString(CurrFormatter);
			}
			catch {BuildingInfoObject.Instance.Rent = 0;}

			BuildingInfoObject.Instance.LandLeaseComments = _BI_landLeaseComments.Text;
			BuildingInfoObject.Instance.RecentImprovements = _BI_recentImprovements.Text;
			BuildingInfoObject.Instance.ImprovementsNeeded = _BI_improvementsNeeded.Text;
			BuildingInfoObject.Instance.PropertyDescription = _BI_propertyDescription.Text;
			BuildingInfoObject.Instance.LocationDescription = _BI_locationDescription.Text;
			BuildingInfoObject.Instance.Amenities = _BI_amenities.Text;
			BuildingInfoObject.Instance.Landscaping = _BI_landscaping.Text;	
			
			if (tb.Name == _BI_lotSpaces.Name || tb.Name == _BI_coveredSpaces.Name || tb.Name == _BI_garageSpaces.Name || 
				tb.Name == _BI_lotFee.Name || tb.Name == _BI_coveredFee.Name || tb.Name == _BI_garageFee.Name)		
			{
				_BI_totalSpaces.Text = BuildingInfoObject.Instance.ParkingTotalSpaces.ToString();				
				_BI_totalFee.Text = BuildingInfoObject.Instance.ParkingTotalFee.ToString(CurrFormatter);
				
				//related to BI Total spaces - BEGIN
                //_I_OI_Parking.Text = _BI_totalFee.Text; 
                //_I_OI_Total.Text = IncomeObject.Instance.OtherIncomeObject.TotalProperty.ToString(CurrFormatter);
				IncomeObject.Instance.OtherIncomeObject.ParkingProperty = BuildingInfoObject.Instance.ParkingTotalFee.ToString(CurrFormatter);
				//END

				_BI_parkingRatio.Text = BuildingInfoObject.Instance.ParkingRatio;
			}
		}

		private void OnCheckBoxLeave(object sender, System.EventArgs e)
		{
			HandleCheckBoxes(sender);			
		}

		private void OnCheckBoxSelectedValueChanged(object sender, System.EventArgs e)
		{
			HandleCheckBoxes(sender);
		}	
	
		void HandleCheckBoxes(object sender)
		{
			ComboBox cb = (ComboBox)sender;
			
			BuildingInfoObject.Instance.PropertyType = (null != _BI_propertyType.SelectedItem) ? _BI_propertyType.SelectedItem.ToString() : string.Empty;		
			BuildingInfoObject.Instance.Class = (null != _BI_class.SelectedItem) ? _BI_class.SelectedItem.ToString() : string.Empty;
			BuildingInfoObject.Instance.YearBuilt = (null != _BI_yearBuilt.SelectedItem) ? _BI_yearBuilt.SelectedItem.ToString() : string.Empty;
			BuildingInfoObject.Instance.RehabYear = (null != _BI_rehabYear.SelectedItem) ? _BI_rehabYear.SelectedItem.ToString() : string.Empty;
			BuildingInfoObject.Instance.NrOfUnits = (null != _BI_units.SelectedItem) ? _BI_units.SelectedItem.ToString() : string.Empty;
			BuildingInfoObject.Instance.NrOfFloors = (null != _BI_floors.SelectedItem) ? _BI_floors.SelectedItem.ToString() : string.Empty;
			BuildingInfoObject.Instance.NrOfStructures = (null != _BI_structures.SelectedItem) ? _BI_structures.SelectedItem.ToString() : string.Empty;
			BuildingInfoObject.Instance.RoofAge = (null != _BI_roofAge.SelectedItem) ? _BI_roofAge.SelectedItem.ToString() : string.Empty;
			BuildingInfoObject.Instance.PlumbingType = (null != _BI_plumbingType.SelectedItem) ? _BI_plumbingType.SelectedItem.ToString() : string.Empty;
			BuildingInfoObject.Instance.PlumbingAge = (null != _BI_plumbingAge.SelectedItem) ? _BI_plumbingAge.SelectedItem.ToString() : string.Empty;
			BuildingInfoObject.Instance.ElectricalAge = (null != _BI_electricalAge.SelectedItem) ? _BI_electricalAge.SelectedItem.ToString() : string.Empty;
			BuildingInfoObject.Instance.SidingType = (null != _BI_sidingType.SelectedItem) ? _BI_sidingType.SelectedItem.ToString() : string.Empty;
			BuildingInfoObject.Instance.WindowAge = (null != _BI_windowsAge.SelectedItem) ? _BI_windowsAge.SelectedItem.ToString() : string.Empty;
			BuildingInfoObject.Instance.LandLease = (null != _BI_landLease.SelectedItem) ? _BI_landLease.SelectedItem.ToString() : string.Empty;
			BuildingInfoObject.Instance.LandLeaseOption = (null != _BI_option.SelectedItem) ? _BI_option.SelectedItem.ToString() : string.Empty;

			if (cb.Name == _BI_units.Name)
			{
				_BI_parkingRatio.Text = BuildingInfoObject.Instance.ParkingRatio;
			}
		}		

		#endregion

		#region INCOME TEXTBOX HANDLERS
		private void I_AI_GSI_OnTextBoxLeave(object sender, System.EventArgs e)
		{				
			HandleActualIncomes_GSI();
		}

		private void I_AI_GSI_OnKeyPressed(object sender, System.Windows.Forms.KeyPressEventArgs e)
		{
			if ( e.KeyChar == 13 || e.KeyChar == 27 )
				HandleActualIncomes_GSI();
		}

		void HandleActualIncomes_GSI()
		{
			//SET NEW PROPERTY (take from GUI)
            //IncomeObject.Instance.ActualIncomeObject.GrossRent = _I_AI_GrossRent.Text;	
			
            //try
            //{
            //    //TRY TO CONVERT TO CURR FORMATTER
            //    decimal dTemp = decimal.Parse(_I_AI_GrossRent.Text); 
            //    IncomeObject.Instance.ActualIncomeObject.GrossRent = dTemp.ToString(CurrFormatter);
            //    _I_AI_GrossRent.Text = IncomeObject.Instance.ActualIncomeObject.GrossRent;				
            //}
            //catch 
            //{
            //    //AS IS
            //    IncomeObject.Instance.ActualIncomeObject.GrossRent = _I_AI_GrossRent.Text;		
            //}
		
            //IncomeObject.Instance.ActualIncomeObject.VacancyOrCreditLossPerc = _I_AI_VacancyOrCreditLossPerc.Text;			
				
			
            ////CALCULATE THE OTHER and set to GUI			
            //_I_AI_VacancyOrCreditLoss.Text = IncomeObject.Instance.ActualIncomeObject.VacancyOrCreditLossProperty.ToString(CurrFormatter);			
            //_I_AI_OtherIncome.Text = IncomeObject.Instance.ActualIncomeObject.OtherIncomeProperty.ToString(CurrFormatter);
            //_I_AI_OperatingIncome.Text = IncomeObject.Instance.ActualIncomeObject.OperatingIncomeProperty.ToString(CurrFormatter);			

            ////FORCE 0.00 FORMAT
            //_I_AI_GrossRent.Text = IncomeObject.Instance.ActualIncomeObject.GrossRentProperty.ToString(CurrFormatter);
		}		

		private void I_AI_LossPerc_OnTextBoxLeave(object sender, System.EventArgs e)
		{
			HandleActualIncomes_LossPerc();
		}

		private void I_AI_LossPerc_OnKeyPressed(object sender, System.Windows.Forms.KeyPressEventArgs e)
		{
			if ( e.KeyChar == 13 || e.KeyChar == 27 )
				HandleActualIncomes_LossPerc();
		}

		void HandleActualIncomes_LossPerc()
		{
			//SET NEW PROPERTY (take from GUI)			
            //try
            //{
            //    //TRY TO CONVERT TO CURR FORMATTER
            //    decimal dTemp = decimal.Parse(_I_AI_GrossRent.Text); 
            //    IncomeObject.Instance.ActualIncomeObject.GrossRent = dTemp.ToString(CurrFormatter);
            //    _I_AI_GrossRent.Text = IncomeObject.Instance.ActualIncomeObject.GrossRent;				
            //}
            //catch 
            //{
            //    //AS IS
            //    IncomeObject.Instance.ActualIncomeObject.GrossRent = _I_AI_GrossRent.Text;		
            //}

            //IncomeObject.Instance.ActualIncomeObject.VacancyOrCreditLossPerc = _I_AI_VacancyOrCreditLossPerc.Text;
			
            ////CALCULATE THE OTHER and set to GUI
            //_I_AI_VacancyOrCreditLoss.Text = (IncomeObject.Instance.ActualIncomeObject.VacancyOrCreditLossProperty).ToString(CurrFormatter);			
            //_I_AI_OtherIncome.Text = IncomeObject.Instance.ActualIncomeObject.OtherIncomeProperty.ToString(CurrFormatter);
            //_I_AI_OperatingIncome.Text = IncomeObject.Instance.ActualIncomeObject.OperatingIncomeProperty.ToString(CurrFormatter);			

            ////FORCE 0.00 FORMAT
            //_I_AI_GrossRent.Text = IncomeObject.Instance.ActualIncomeObject.GrossRentProperty.ToString(CurrFormatter);
		}

		private void I_OI_KeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Enter)
				HandleOtherIncome();
		}

		private void I_OnTextBoxLeave(object sender, System.EventArgs e)
		{			
			HandleOtherIncome();
		}

		void HandleOtherIncome()
		{
			//SET PROPERTIES
            //try 
            //{ 
            //    //TRY TO CONVERT TO CURR FORMATTER
            //    decimal dTemp = decimal.Parse(_I_OI_Parking.Text); 
            //    IncomeObject.Instance.OtherIncomeObject.ParkingProperty = dTemp.ToString(CurrFormatter);
            //    _I_OI_Parking.Text = IncomeObject.Instance.OtherIncomeObject.ParkingProperty;
            //}
            //catch 
            //{
            //    //AS IS
            //    IncomeObject.Instance.OtherIncomeObject.ParkingProperty = _I_OI_Parking.Text;
            //}
			
            //try
            //{
            //    decimal dTemp = decimal.Parse(_I_OI_Laundry.Text); 
            //    IncomeObject.Instance.OtherIncomeObject.LaundryProperty = dTemp.ToString(CurrFormatter);
            //    _I_OI_Laundry.Text = IncomeObject.Instance.OtherIncomeObject.LaundryProperty;
            //}		
            //catch 
            //{
            //    IncomeObject.Instance.OtherIncomeObject.LaundryProperty = _I_OI_Laundry.Text;
            //}

            //try
            //{
            //    decimal dTemp = decimal.Parse(_I_OI_Vending.Text); 
            //    IncomeObject.Instance.OtherIncomeObject.VendingProperty = dTemp.ToString(CurrFormatter);
            //    _I_OI_Vending.Text = IncomeObject.Instance.OtherIncomeObject.VendingProperty;
            //}		
            //catch 
            //{
            //    IncomeObject.Instance.OtherIncomeObject.VendingProperty = _I_OI_Vending.Text;
            //}

            //try
            //{
            //    decimal dTemp = decimal.Parse(_I_OI_Other.Text); 
            //    IncomeObject.Instance.OtherIncomeObject.OtherProperty = dTemp.ToString(CurrFormatter);
            //    _I_OI_Other.Text = IncomeObject.Instance.OtherIncomeObject.OtherProperty;
            //}		
            //catch 
            //{
            //    IncomeObject.Instance.OtherIncomeObject.OtherProperty = _I_OI_Other.Text;
            //}
									
            ////SET SUM
            //_I_OI_Total.Text = IncomeObject.Instance.OtherIncomeObject.TotalProperty.ToString(CurrFormatter);
		}

		private void I_P_GSI_OnTextBoxLeave(object sender, System.EventArgs e)
		{
			HandleActualIncomes_P_GSI();			
		}

		private void I_P_GSI_OnKeyPressed(object sender, System.Windows.Forms.KeyPressEventArgs e)
		{
			if ( e.KeyChar == 13 || e.KeyChar == 27 )
				HandleActualIncomes_P_GSI();
		}

		void HandleActualIncomes_P_GSI()
		{
			//SET PROPERTIES
            //IncomeObject.Instance.ProformaIncomeObject.GrossRentProperty = _I_PI_GrossRent.Text;

            ////CALCULATE THE OTHER and set to GUI
            //_I_PI_VacancyOrCreditLoss.Text = IncomeObject.Instance.ProformaIncomeObject.VacancyOrCreditloss.ToString(CurrFormatter);
            //_I_PI_OperatingIncome.Text = IncomeObject.Instance.ProformaIncomeObject.OperatingIncome.ToString(CurrFormatter);
            //_I_PI_OtherIncome.Text = IncomeObject.Instance.ProformaIncomeObject.OtherIncome.ToString(CurrFormatter);

            ////FORCE 0.00 FORMAT
            //_I_PI_GrossRent.Text = IncomeObject.Instance.ProformaIncomeObject.GrossRent.ToString(CurrFormatter);
		}

		private void I_P_VacancyPerc_OnTextBoxLeave(object sender, System.EventArgs e)
		{			
			HandleActualIncomes_P_VacancyPerc();	
		}

		private void I_P_VacancyPerc_OnKeyPressed(object sender, System.Windows.Forms.KeyPressEventArgs e)
		{
			if ( e.KeyChar == 13 || e.KeyChar == 27 )
				HandleActualIncomes_P_VacancyPerc();
		}

		void HandleActualIncomes_P_VacancyPerc()
		{
			//SET PROPERTIES
            //IncomeObject.Instance.ProformaIncomeObject.VacancyOrCreditLossPercProperty = _I_PI_VacancyOrCreditLossPerc.Text;

            ////CALCULATE THE OTHER and set to GUI
            //_I_PI_VacancyOrCreditLoss.Text = IncomeObject.Instance.ProformaIncomeObject.VacancyOrCreditloss.ToString(CurrFormatter);
            //_I_PI_OperatingIncome.Text = IncomeObject.Instance.ProformaIncomeObject.OperatingIncome.ToString(CurrFormatter);
            //_I_PI_OtherIncome.Text = IncomeObject.Instance.ProformaIncomeObject.OtherIncome.ToString(CurrFormatter);
		}

		private void I_P_OtherIncome_OnTextBoxLeave(object sender, System.EventArgs e)
		{			
			HandleActualIncomes_P_OtherPerc();			
		}
		
		private void I_P_OtherIncome_OnKeyPressed(object sender, System.Windows.Forms.KeyPressEventArgs e)
		{
			if ( e.KeyChar == 13 || e.KeyChar == 27 )
				HandleActualIncomes_P_OtherPerc();
		}

		void HandleActualIncomes_P_OtherPerc()
		{
			//SET PROPERTIES
            //IncomeObject.Instance.ProformaIncomeObject.OtherIncomeProperty = _I_PI_OtherIncome.Text;

            ////CALCULATE THE OTHER and set to GUI
            //_I_PI_VacancyOrCreditLoss.Text = IncomeObject.Instance.ProformaIncomeObject.VacancyOrCreditloss.ToString(CurrFormatter);
            //_I_PI_OperatingIncome.Text = IncomeObject.Instance.ProformaIncomeObject.OperatingIncome.ToString(CurrFormatter);
            //_I_PI_OtherIncome.Text = IncomeObject.Instance.ProformaIncomeObject.OtherIncome.ToString(CurrFormatter);
		}
		#endregion
		
		#region INCOME = INIT LISTVIEWS
		void InitIncomeListViews()
		{			
			for (int i = 0; i < IncomeObject.UTSMaxItems; i++)
			{
				ListViewItem lvi = new ListViewItem();
				lvi.SubItems.Add(string.Empty);
				lvi.SubItems.Add(string.Empty);
				lvi.SubItems.Add(string.Empty);
				lvi.SubItems.Add(string.Empty);
				lvi.SubItems.Add(string.Empty);
				lvi.SubItems.Add(string.Empty);
				lvi.SubItems.Add(string.Empty);
				lvi.SubItems.Add(string.Empty);
				lvi.SubItems.Add(string.Empty);
				lvi.SubItems.Add(string.Empty);
				lvi.SubItems.Add(string.Empty);
				lvi.SubItems.Add(string.Empty);
				
				//Add Tag
				lvi.Tag = i;
				
				//Color readonly fields
                //_I_unitTypeSummary.Items.Add(lvi);
                //_I_unitTypeSummary.Items[i].UseItemStyleForSubItems = false;
                //_I_unitTypeSummary.Items[i].SubItems[9].BackColor = Color.LightGray;
                //_I_unitTypeSummary.Items[i].SubItems[12].BackColor = Color.LightGray;
			}	
		
			for (int i = 0; i < IncomeObject.RRMaxItems; i++)
			{
				ListViewItem lvi = new ListViewItem();
				lvi.SubItems.Add(string.Empty);
				lvi.SubItems.Add(string.Empty);
				lvi.SubItems.Add(string.Empty);
				lvi.SubItems.Add(string.Empty);
				lvi.SubItems.Add(string.Empty);
				lvi.SubItems.Add(string.Empty);
				lvi.SubItems.Add(string.Empty);
				lvi.SubItems.Add(string.Empty);
				lvi.SubItems.Add(string.Empty);
				lvi.SubItems.Add(string.Empty);
				lvi.SubItems.Add(string.Empty);				
				
				//Add Tag
				lvi.Tag = i;
				
				//Color readonly fields
                //_I_rentRoll.Items.Add(lvi);		
                //_I_rentRoll.Items[i].UseItemStyleForSubItems = false;
                //_I_rentRoll.Items[i].SubItems[7].BackColor = Color.LightGray;				
			}		
		}

		#endregion 

		#region INCOME = MAP TO OBJECT MODEL
		private void I_unitTypeSummary_OnListViewExItemChanged(int row, int col, string val)
		{			
            //if (-1 != row && -1 != col)
            //{				
            //    //MAP TO OBJECT MODEL
            //    switch (col)
            //    {
            //        case 0:
            //            IncomeObject.Instance.UnitTypeSummaries[row].NrOfUnitsProperty = val;						
            //            break;

            //        case 1:
            //            IncomeObject.Instance.UnitTypeSummaries[row].NrOfBedroomsProperty = val;
            //            break;

            //        case 2:
            //            IncomeObject.Instance.UnitTypeSummaries[row].NrOfBathsProperty = val;
            //            break;

            //        case 3:
            //            IncomeObject.Instance.UnitTypeSummaries[row].FloorPlanProperty = val;
            //            break;

            //        case 4:
            //            IncomeObject.Instance.UnitTypeSummaries[row].OccupancyProperty = val;						
            //            break;

            //        case 5:
            //            IncomeObject.Instance.UnitTypeSummaries[row].SF_LowProperty = val;
            //            _I_unitTypeSummary.Items[row].SubItems[9].Text = IncomeObject.Instance.UnitTypeSummaries[row].ActualRentPerItem.ToString(CurrFormatter);
            //            _I_unitTypeSummary.Items[row].SubItems[12].Text = IncomeObject.Instance.UnitTypeSummaries[row].MarketRentPerItem.ToString(CurrFormatter);

            //            break;

            //        case 6:
            //            IncomeObject.Instance.UnitTypeSummaries[row].SF_HighProperty= val;
            //            _I_unitTypeSummary.Items[row].SubItems[9].Text = IncomeObject.Instance.UnitTypeSummaries[row].ActualRentPerItem.ToString(CurrFormatter);
            //            _I_unitTypeSummary.Items[row].SubItems[12].Text = IncomeObject.Instance.UnitTypeSummaries[row].MarketRentPerItem.ToString(CurrFormatter);
            //            break;

            //        case 7:
            //            IncomeObject.Instance.UnitTypeSummaries[row].AR_LowProperty = val;
            //            _I_unitTypeSummary.Items[row].SubItems[9].Text = IncomeObject.Instance.UnitTypeSummaries[row].ActualRentPerItem.ToString(CurrFormatter);
            //            break;

            //        case 8:
            //            IncomeObject.Instance.UnitTypeSummaries[row].AR_HighProperty = val;
            //            _I_unitTypeSummary.Items[row].SubItems[9].Text = IncomeObject.Instance.UnitTypeSummaries[row].ActualRentPerItem.ToString(CurrFormatter);
            //            break;					

            //        case 10:
            //            IncomeObject.Instance.UnitTypeSummaries[row].MR_LowProperty = val;
            //            break;

            //        case 11:
            //            IncomeObject.Instance.UnitTypeSummaries[row].MR_HighProperty= val;
            //            _I_unitTypeSummary.Items[row].SubItems[12].Text = IncomeObject.Instance.UnitTypeSummaries[row].MarketRentPerItem.ToString(CurrFormatter);
            //            break;					
            //    }

            //    _I_UTS_TotalUnits.Text = IncomeObject.Instance.UTS_TotalNumberOfUnits.ToString();
            //    _I_UTS_TotalOccupancy.Text = IncomeObject.Instance.UTS_WeightedAverageTotalOccupancy.ToString("0.00") + "%";
            //    _I_UTS_Total_SQ_Low.Text = IncomeObject.Instance.UTS_WeightedAverageTotalSqFtLow.ToString(CurrFormatter);
            //    _I_UTS_Total_SQ_High.Text = IncomeObject.Instance.UTS_WeightedAverageTotalSqFtHigh.ToString(CurrFormatter);
            //    _I_UTS_Total_AR_Low.Text = IncomeObject.Instance.UTS_SumTotalARLow.ToString(CurrFormatter);
            //    _I_UTS_Total_AR_High.Text = IncomeObject.Instance.UTS_SumTotalARHigh.ToString(CurrFormatter);
            //    _I_UTS_Total_AR_UsdPerSqFt.Text = IncomeObject.Instance.UTS_WeightedAverageARUSDPerSqFt.ToString(CurrFormatter);
            //    _I_UTS_Total_MR_Low.Text = IncomeObject.Instance.UTS_SumTotalMRLow.ToString(CurrFormatter);
            //    _I_UTS_Total_MR_High.Text = IncomeObject.Instance.UTS_SumTotalMRHigh.ToString(CurrFormatter);
            //    _I_UTS_Total_MR_USDperSqFt.Text = IncomeObject.Instance.UTS_WeightedAverageMRUSDPerSqFt.ToString(CurrFormatter);				
            //}
		}

		private void I_rentRoll_OnListViewExItemChanged(int row, int col, string val)
		{
            //if (-1 != row && -1 != col)
            //{				
            //    //MAP TO OBJECT MODEL
            //    switch (col)
            //    {
            //        case 0:
            //            IncomeObject.Instance.RentRolls[row].UnitNrProperty = val;
            //            break;

            //        case 1:
            //            IncomeObject.Instance.RentRolls[row].TennantNameProperty = val;
            //            break;

            //        case 2:
            //            IncomeObject.Instance.RentRolls[row].BedBathProperty = val;
            //            break;

            //        case 3:
            //            IncomeObject.Instance.RentRolls[row].SqftProperty = val;
            //            _I_RR_TotalSqFt.Text = IncomeObject.Instance.RR_TotalSqft.ToString(CurrFormatter);
            //            break;

            //        case 4:
            //            IncomeObject.Instance.RentRolls[row].SecDepProperty = val;
            //            _I_RR_TotalSecDep.Text = IncomeObject.Instance.RR_TotalSecDep.ToString(CurrFormatter);
            //            break;

            //        case 5:
            //            IncomeObject.Instance.RentRolls[row].TennantRentProperty = val;
            //            _I_rentRoll.Items[row].SubItems[7].Text = IncomeObject.Instance.RentRolls[row].TotalRentProperty.ToString(CurrFormatter);						
            //            _I_RR_TotalTennantRent.Text = IncomeObject.Instance.RR_TotalTennantRent.ToString(CurrFormatter);
            //            _I_RR_TotalTotalRent.Text = IncomeObject.Instance.RR_TotalTotalRent.ToString(CurrFormatter);
            //            break;

            //        case 6:
            //            IncomeObject.Instance.RentRolls[row].Sec8RentProperty = val;
            //            _I_rentRoll.Items[row].SubItems[7].Text = IncomeObject.Instance.RentRolls[row].TotalRentProperty.ToString(CurrFormatter);
            //            _I_RR_TotalSec8Rent.Text = IncomeObject.Instance.RR_TotalSec8Rent.ToString(CurrFormatter);
            //            _I_RR_TotalTotalRent.Text = IncomeObject.Instance.RR_TotalTotalRent.ToString(CurrFormatter);
            //            break;					

            //        case 8:
            //            IncomeObject.Instance.RentRolls[row].MarketRentProperty = val;
            //            _I_RR_TotalMarketRent.Text = IncomeObject.Instance.RR_TotalMarketRent.ToString(CurrFormatter);
            //            break;

            //        case 9:
            //            IncomeObject.Instance.RentRolls[row].OccupDateProperty = val;
            //            break;

            //        case 10:
            //            IncomeObject.Instance.RentRolls[row].LeaseTermProperty = val;
            //            break;

            //        case 11:
            //            IncomeObject.Instance.RentRolls[row].CommentProperty = val;
            //            break;
            //    }
            //}
		}

		#endregion
		
		#region INCOME = CALCULATION
		private void On_CalculateFromUnitTypeSummary_Click(object sender, EventArgs e)
		{
            //decimal grossRent;
            //decimal vacancyOrCreditLoss;
            //decimal vacancyOrCreditLossPerc;
            //decimal otherIncome;
            //decimal operatingIncome;

            //IncomeObject.Instance.ActualIncomeObject.CalculateValuesFromUTS(out grossRent, out vacancyOrCreditLoss, out vacancyOrCreditLossPerc, out otherIncome, out operatingIncome);
						
            //IncomeObject.Instance.ActualIncomeObject.GrossRent = _I_AI_GrossRent.Text = grossRent.ToString(CurrFormatter);
            //IncomeObject.Instance.ActualIncomeObject.VacancyOrCreditLoss = _I_AI_VacancyOrCreditLoss.Text = vacancyOrCreditLoss.ToString(CurrFormatter);
            //IncomeObject.Instance.ActualIncomeObject.VacancyOrCreditLossPerc = _I_AI_VacancyOrCreditLossPerc.Text = vacancyOrCreditLossPerc.ToString(CurrFormatter);
            //_I_AI_OtherIncome.Text = otherIncome.ToString(CurrFormatter);
            //IncomeObject.Instance.ActualIncomeObject.OtherIncomeProperty = otherIncome;
			

            //decimal gsi;
            //IncomeObject.Instance.ProformaIncomeObject.CalculateFromUnitTypeSummary(out gsi);
            //_I_PI_GrossRent.Text = gsi.ToString(CurrFormatter);
            //IncomeObject.Instance.ProformaIncomeObject.GrossRentProperty = gsi.ToString();

            ////FORCE RECALC:
            //HandleActualIncomes_LossPerc();
		}				
		#endregion

		#region EXPENSES
		void InitExpensesListViews()
		{
			for (int i = 0; i < ExpensesObject.MaxExpensesItems; i++)
			{
				string type;
				string desc;

				ExpensesObject.Instance.ExpensesItemsProperty.GetDescriptor(i, out type, out desc);

				ListViewItem lvi = new ListViewItem(type);
				lvi.SubItems.Add(desc);
				lvi.SubItems.Add(string.Empty);
				lvi.SubItems.Add(string.Empty);
				lvi.SubItems.Add(string.Empty);
				lvi.SubItems.Add(string.Empty);
				lvi.SubItems.Add(string.Empty);
				lvi.SubItems.Add(string.Empty);
				lvi.SubItems.Add(string.Empty);
				lvi.SubItems.Add(string.Empty);

				lvi.Tag = i;

				_E_expenses.Items.Add(lvi);
				_E_expenses.Items[i].UseItemStyleForSubItems = false;
				_E_expenses.Items[i].SubItems[6].BackColor = Color.LightGray;				
			}
		}
		#endregion

		#region EXPENSES MAP TO OBJECT MODEL
		private void E_expenses_OnListViewExItemChanged(int row, int col, string val)
		{
			if (-1 != row && -1 != col)
			{
				//MAP TO OBJECT MODEL
				switch (col)
				{
					case 2:
						ExpensesObject.Instance.ExpensesItemsProperty.Items[row].UsdPerMonthProperty = val;						
						break;
					case 3:
						ExpensesObject.Instance.ExpensesItemsProperty.Items[row].UsdPerYearProperty = val;						
						break;
					case 4:
						ExpensesObject.Instance.ExpensesItemsProperty.Items[row].UsdPerSqFtProperty = val;
						break;
					case 5:
						ExpensesObject.Instance.ExpensesItemsProperty.Items[row].UsdPerUnitProperty = val;
						break;
					case 7:
						ExpensesObject.Instance.ExpensesItemsProperty.Items[row].PaidByProperty = val;
						break;
					case 8:
						ExpensesObject.Instance.ExpensesItemsProperty.Items[row].HighLowProperty = val;
						break;
					case 9:
						ExpensesObject.Instance.ExpensesItemsProperty.Items[row].CommentProperty = val;
						break;

					
				}
				
				_E_expenses.Items[row].SubItems[3].Text = ExpensesObject.Instance.ExpensesItemsProperty.Items[row].USDPerYearValue.ToString(CurrFormatter);
				_E_expenses.Items[row].SubItems[2].Text = ExpensesObject.Instance.ExpensesItemsProperty.Items[row].USDPerMonthValue.ToString(CurrFormatter);
				_E_expenses.Items[row].SubItems[4].Text = ExpensesObject.Instance.ExpensesItemsProperty.Items[row].USDPerSqFtValue.ToString(CurrFormatter);
				_E_expenses.Items[row].SubItems[5].Text = ExpensesObject.Instance.ExpensesItemsProperty.Items[row].USDPerUnitValue.ToString(CurrFormatter);
				
							
				decimal usdPerMonthValueTotal = 0;
				decimal usdPerYearValueTotal = 0;
				decimal usdPerSqFtValueTotal = 0;
				decimal usdPerUnitValueTotal = 0;
				
				
				//ADMINISTRATIVE
				ExpensesObject.Instance.ExpensesItemsProperty.GetExpensesTotal(ExpensesTypes.Administrative, out usdPerMonthValueTotal, out usdPerYearValueTotal, out usdPerSqFtValueTotal, out usdPerUnitValueTotal);

				decimal administrativeTotalUsdPerYearValue = usdPerYearValueTotal;
				_E_TotalUsdPerMonth_1.Text = usdPerMonthValueTotal.ToString(CurrFormatter);
				_E_TotalUsdPerYear_1.Text = usdPerYearValueTotal.ToString(CurrFormatter);
				_E_TotalUsdPerSqft_1.Text = usdPerSqFtValueTotal.ToString(CurrFormatter);
				_E_TotalUsdPerUnit_1.Text = usdPerUnitValueTotal.ToString(CurrFormatter);							

				//MANAGEMENT
				ExpensesObject.Instance.ExpensesItemsProperty.GetExpensesTotal(ExpensesTypes.Management, out usdPerMonthValueTotal, out usdPerYearValueTotal, out usdPerSqFtValueTotal, out usdPerUnitValueTotal);

				decimal managementTotalUsdPerYearValue = usdPerYearValueTotal;
				_E_TotalUsdPerMonth_2.Text = usdPerMonthValueTotal.ToString(CurrFormatter);
				_E_TotalUsdPerYear_2.Text = usdPerYearValueTotal.ToString(CurrFormatter);
				_E_TotalUsdPerSqft_2.Text = usdPerSqFtValueTotal.ToString(CurrFormatter);
				_E_TotalUsdPerUnit_2.Text = usdPerUnitValueTotal.ToString(CurrFormatter);				

				//UTILITIES
				ExpensesObject.Instance.ExpensesItemsProperty.GetExpensesTotal(ExpensesTypes.Utilities, out usdPerMonthValueTotal, out usdPerYearValueTotal, out usdPerSqFtValueTotal, out usdPerUnitValueTotal);

				decimal utilitiesTotalUsdPerYearValue = usdPerYearValueTotal;
				_E_TotalUsdPerMonth_3.Text = usdPerMonthValueTotal.ToString(CurrFormatter);
				_E_TotalUsdPerYear_3.Text = usdPerYearValueTotal.ToString(CurrFormatter);
				_E_TotalUsdPerSqft_3.Text = usdPerSqFtValueTotal.ToString(CurrFormatter);
				_E_TotalUsdPerUnit_3.Text = usdPerUnitValueTotal.ToString(CurrFormatter);				

				//INSURANCE
				ExpensesObject.Instance.ExpensesItemsProperty.GetExpensesTotal(ExpensesTypes.Insurance, out usdPerMonthValueTotal, out usdPerYearValueTotal, out usdPerSqFtValueTotal, out usdPerUnitValueTotal);

				decimal insuranceTotalUsdPerYearValue = usdPerYearValueTotal;
				_E_TotalUsdPerMonth_4.Text = usdPerMonthValueTotal.ToString(CurrFormatter);
				_E_TotalUsdPerYear_4.Text = usdPerYearValueTotal.ToString(CurrFormatter);
				_E_TotalUsdPerSqft_4.Text = usdPerSqFtValueTotal.ToString(CurrFormatter);
				_E_TotalUsdPerUnit_4.Text = usdPerUnitValueTotal.ToString(CurrFormatter);				

				//OPERATING
				ExpensesObject.Instance.ExpensesItemsProperty.GetExpensesTotal(ExpensesTypes.Operating, out usdPerMonthValueTotal, out usdPerYearValueTotal, out usdPerSqFtValueTotal, out usdPerUnitValueTotal);

				decimal operatingTotalUsdPerYearValue = usdPerYearValueTotal;
				_E_TotalUsdPerMonth_5.Text = usdPerMonthValueTotal.ToString(CurrFormatter);
				_E_TotalUsdPerYear_5.Text = usdPerYearValueTotal.ToString(CurrFormatter);
				_E_TotalUsdPerSqft_5.Text = usdPerSqFtValueTotal.ToString(CurrFormatter);
				_E_TotalUsdPerUnit_5.Text = usdPerUnitValueTotal.ToString(CurrFormatter);

				//TYPES TOTALs
				ExpensesObject.Instance.ExpensesItemsProperty.GetExpensesTypesTotal(out usdPerMonthValueTotal, out usdPerYearValueTotal, out usdPerSqFtValueTotal, out usdPerUnitValueTotal);
				_E_TotalUsdPerMonth_6.Text = usdPerMonthValueTotal.ToString(CurrFormatter);
				_E_TotalUsdPerYear_6.Text = usdPerYearValueTotal.ToString(CurrFormatter);
				_E_TotalUsdPerSqft_6.Text = usdPerSqFtValueTotal.ToString(CurrFormatter);
				_E_TotalUsdPerUnit_6.Text = usdPerUnitValueTotal.ToString(CurrFormatter);

				ExpensesObject.Instance.TotalExpensesItemsProperty.Items[5].UsdPerYearProperty = usdPerYearValueTotal.ToString(CurrFormatter);

				//UPDATE PERC TOTALS PER TYPES
				if (0 != usdPerYearValueTotal)
				{
					_E_TotalPercOfTotal_1.Text = (administrativeTotalUsdPerYearValue / usdPerYearValueTotal * 100).ToString(CurrFormatter);
					_E_TotalPercOfTotal_2.Text = (managementTotalUsdPerYearValue / usdPerYearValueTotal * 100).ToString(CurrFormatter);
					_E_TotalPercOfTotal_3.Text = (utilitiesTotalUsdPerYearValue / usdPerYearValueTotal * 100).ToString(CurrFormatter);
					_E_TotalPercOfTotal_4.Text = (insuranceTotalUsdPerYearValue / usdPerYearValueTotal * 100).ToString(CurrFormatter);	
					_E_TotalPercOfTotal_5.Text = (operatingTotalUsdPerYearValue / usdPerYearValueTotal * 100).ToString(CurrFormatter);
				}
				else
				{
					_E_TotalPercOfTotal_1.Text = "N/A";
					_E_TotalPercOfTotal_2.Text = "N/A";
					_E_TotalPercOfTotal_3.Text = "N/A";
					_E_TotalPercOfTotal_4.Text = "N/A";	
					_E_TotalPercOfTotal_5.Text = "N/A";
				}

				//UPDATE PERC TOTALS PER EXPENSES
				for (int i = 0; i < ExpensesObject.MaxExpensesItems; i++)
				{
					ExpensesTypes currType = ExpensesTypes.Administrative;

					if (0 <= i && i <=5)
						currType = ExpensesTypes.Administrative;
					else if (6 <= i && i <= 8)
						currType = ExpensesTypes.Management;
					else if (9 <= i && i <= 13)
						currType = ExpensesTypes.Utilities;
					else if (14 <= i && i <= 19)
						currType = ExpensesTypes.Insurance;
					else if (20 <= i && i <= 33)
						currType = ExpensesTypes.Operating;	

					ExpensesObject.Instance.ExpensesItemsProperty.GetExpensesTotal(currType, out usdPerMonthValueTotal, out usdPerYearValueTotal, out usdPerSqFtValueTotal, out usdPerUnitValueTotal);
					decimal totalYearExpensesForAllTypes = ExpensesObject.Instance.ExpensesItemsProperty.GetTotalYearExpensesForAllTypes();

					if (0 != usdPerYearValueTotal)
						_E_expenses.Items[i].SubItems[6].Text = ((ExpensesObject.Instance.ExpensesItemsProperty.Items[i].USDPerYearValue / totalYearExpensesForAllTypes/*usdPerYearValueTotal*/) * 100).ToString("0.00");
					else
						_E_expenses.Items[i].SubItems[6].Text = "N/A";
				}				
			}
		}


		private void _E_UT_ElMeter_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			ComboBox cb = (ComboBox)sender;
			ExpensesObject.Instance.UtilityTypesProperty.ElectricMeterProperty = cb.SelectedItem.ToString();
		}

		private void _E_UT_Heat_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			ComboBox cb = (ComboBox)sender;
			ExpensesObject.Instance.UtilityTypesProperty.HeatProperty = cb.SelectedItem.ToString();
		}

		private void _E_UT_HotWaterSelectedIndexChanged(object sender, System.EventArgs e)
		{
			ComboBox cb = (ComboBox)sender;
			ExpensesObject.Instance.UtilityTypesProperty.HotWaterProperty = cb.SelectedItem.ToString();
		}

		private void _E_CA_OnLeave(object sender, System.EventArgs e)
		{
			Handle_CA_Change(sender);
		}

		private void _E_CA_OnKeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
		{
			if (e.KeyCode ==Keys.Enter)
				Handle_CA_Change(sender);
		}

		void Handle_CA_Change(object sender)
		{
			TextBox tb = (TextBox)sender;
			switch (tb.Name)
			{
				case "_E_CA_Cost_Year1":
					try
					{
						decimal dTemp = Convert.ToDecimal(tb.Text);
						_E_CA_Cost_Year1.Text = ExpensesObject.Instance.CapitalAdditionsProperty.yearsProperty[0]._costProperty = dTemp.ToString(CurrFormatter);
					}
					catch 
					{
						ExpensesObject.Instance.CapitalAdditionsProperty.yearsProperty[0]._costProperty = tb.Text;
					}					
					break;

				case "_E_CA_Cost_Year2":
					try
					{
						decimal dTemp = Convert.ToDecimal(tb.Text);
						_E_CA_Cost_Year2.Text = ExpensesObject.Instance.CapitalAdditionsProperty.yearsProperty[1]._costProperty = dTemp.ToString(CurrFormatter);
					}
					catch 
					{
						ExpensesObject.Instance.CapitalAdditionsProperty.yearsProperty[1]._costProperty = tb.Text;
					}								
					break;

				case "_E_CA_Cost_Year3":
					try
					{
						decimal dTemp = Convert.ToDecimal(tb.Text);
						_E_CA_Cost_Year3.Text = ExpensesObject.Instance.CapitalAdditionsProperty.yearsProperty[2]._costProperty = dTemp.ToString(CurrFormatter);
					}
					catch 
					{
						ExpensesObject.Instance.CapitalAdditionsProperty.yearsProperty[2]._costProperty = tb.Text;
					}						
					break;

				case "_E_CA_Cost_Year4":
					try
					{
						decimal dTemp = Convert.ToDecimal(tb.Text);
						_E_CA_Cost_Year4.Text = ExpensesObject.Instance.CapitalAdditionsProperty.yearsProperty[3]._costProperty = dTemp.ToString(CurrFormatter);
					}
					catch 
					{
						ExpensesObject.Instance.CapitalAdditionsProperty.yearsProperty[3]._costProperty = tb.Text;
					}						
					break;

				case "_E_CA_Cost_Year5":
					try
					{
						decimal dTemp = Convert.ToDecimal(tb.Text);
						_E_CA_Cost_Year5.Text = ExpensesObject.Instance.CapitalAdditionsProperty.yearsProperty[4]._costProperty = dTemp.ToString(CurrFormatter);
					}
					catch 
					{
						ExpensesObject.Instance.CapitalAdditionsProperty.yearsProperty[4]._costProperty = tb.Text;
					}						
					break;

				case "_E_CA_Cost_Year6":
					try
					{
						decimal dTemp = Convert.ToDecimal(tb.Text);
						_E_CA_Cost_Year6.Text = ExpensesObject.Instance.CapitalAdditionsProperty.yearsProperty[5]._costProperty = dTemp.ToString(CurrFormatter);
					}
					catch 
					{
						ExpensesObject.Instance.CapitalAdditionsProperty.yearsProperty[5]._costProperty = tb.Text;
					}					
					break;

				case "_E_CA_Cost_Year7":
					try
					{
						decimal dTemp = Convert.ToDecimal(tb.Text);
						_E_CA_Cost_Year7.Text = ExpensesObject.Instance.CapitalAdditionsProperty.yearsProperty[6]._costProperty = dTemp.ToString(CurrFormatter);
					}
					catch 
					{
						ExpensesObject.Instance.CapitalAdditionsProperty.yearsProperty[6]._costProperty = tb.Text;
					}						
					break;

				case "_E_CA_Cost_Year8":
					try
					{
						decimal dTemp = Convert.ToDecimal(tb.Text);
						_E_CA_Cost_Year8.Text = ExpensesObject.Instance.CapitalAdditionsProperty.yearsProperty[7]._costProperty = dTemp.ToString(CurrFormatter);
					}
					catch 
					{
						ExpensesObject.Instance.CapitalAdditionsProperty.yearsProperty[7]._costProperty = tb.Text;
					}						
					break;

				case "_E_CA_Cost_Year9":
					try
					{
						decimal dTemp = Convert.ToDecimal(tb.Text);
						_E_CA_Cost_Year9.Text = ExpensesObject.Instance.CapitalAdditionsProperty.yearsProperty[8]._costProperty = dTemp.ToString(CurrFormatter);
					}
					catch 
					{
						ExpensesObject.Instance.CapitalAdditionsProperty.yearsProperty[8]._costProperty = tb.Text;
					}						
					break;

				case "_E_CA_Cost_Year10":
					try
					{
						decimal dTemp = Convert.ToDecimal(tb.Text);
						_E_CA_Cost_Year10.Text = ExpensesObject.Instance.CapitalAdditionsProperty.yearsProperty[9]._costProperty = dTemp.ToString(CurrFormatter);
					}
					catch 
					{
						ExpensesObject.Instance.CapitalAdditionsProperty.yearsProperty[9]._costProperty = tb.Text;
					}						
					break;
				
				case "_E_CA_Type_Year1":
					ExpensesObject.Instance.CapitalAdditionsProperty.yearsProperty[0]._typeProperty = tb.Text;
					break;

				case "_E_CA_Type_Year2":
					ExpensesObject.Instance.CapitalAdditionsProperty.yearsProperty[1]._typeProperty = tb.Text;
					break;

				case "_E_CA_Type_Year3":
					ExpensesObject.Instance.CapitalAdditionsProperty.yearsProperty[2]._typeProperty = tb.Text;
					break;
				
				case "_E_CA_Type_Year4":
					ExpensesObject.Instance.CapitalAdditionsProperty.yearsProperty[3]._typeProperty = tb.Text;
					break;

				case "_E_CA_Type_Year5":
					ExpensesObject.Instance.CapitalAdditionsProperty.yearsProperty[4]._typeProperty = tb.Text;
					break;

				case "_E_CA_Type_Year6":
					ExpensesObject.Instance.CapitalAdditionsProperty.yearsProperty[5]._typeProperty = tb.Text;
					break;

				case "_E_CA_Type_Year7":
					ExpensesObject.Instance.CapitalAdditionsProperty.yearsProperty[6]._typeProperty = tb.Text;
					break;

				case "_E_CA_Type_Year8":
					ExpensesObject.Instance.CapitalAdditionsProperty.yearsProperty[7]._typeProperty = tb.Text;
					break;

				case "_E_CA_Type_Year9":
					ExpensesObject.Instance.CapitalAdditionsProperty.yearsProperty[8]._typeProperty = tb.Text;
					break;

				case "_E_CA_Type_Year10":
					ExpensesObject.Instance.CapitalAdditionsProperty.yearsProperty[9]._typeProperty = tb.Text;
					break;


			}
		}

		#endregion

		#region LOANS MAP TO OBJECT MODEL
		
		private void _L_FirstAssumableSelectedIndexChanged(object sender, System.EventArgs e)
		{
			ComboBox cb = (ComboBox)sender;
			LoansObject.Instance.FirstAssumableProperty = cb.SelectedItem.ToString();
		}

		private void _L_SecondAssumableSelectedIndexChanged(object sender, System.EventArgs e)
		{
			ComboBox cb = (ComboBox)sender;
			LoansObject.Instance.SecondAssumableProperty = cb.SelectedItem.ToString();
		}

		private void _L_SellerCBSelectedIndexChanged(object sender, System.EventArgs e)
		{
			ComboBox cb = (ComboBox)sender;
			LoansObject.Instance.SellerCarrybackProperty = cb.SelectedItem.ToString();
		}

		
		
		private void On_L_1L_Type_SelectedValueChanged(object sender, EventArgs e)
		{			
			ComboBox cb = (ComboBox)sender;
			if ("Fixed" == cb.SelectedItem.ToString())
			{
				_L_1L_AmortTerm.ReadOnly = false;
				
				_L_1L_RA_InitFixedPeriod.ReadOnly = true;
				_L_1L_RA_InitialRateCap.ReadOnly = true;
				_L_1L_RA_AnnualCap.ReadOnly = true;
				_L_1L_RA_LifeTimeCap.ReadOnly = true;
				_L_1L_RA_RateYear1.ReadOnly = true;
				_L_1L_RA_RateYear2.ReadOnly = true;
				_L_1L_RA_RateYear3.ReadOnly = true;
				_L_1L_RA_RateYear4.ReadOnly = true;
				_L_1L_RA_RateYear5.ReadOnly = true;
				_L_1L_RA_RateYear6.ReadOnly = true;
				_L_1L_RA_RateYear7.ReadOnly = true;
				_L_1L_RA_RateYear8.ReadOnly = true;
				_L_1L_RA_RateYear9.ReadOnly = true;
				_L_1L_RA_RateYear10.ReadOnly = true;

				_L_1L_RA_InitFixedPeriod.Text = string.Empty;
				_L_1L_RA_InitialRateCap.Text = string.Empty;
				_L_1L_RA_AnnualCap.Text = string.Empty;
				_L_1L_RA_LifeTimeCap.Text = string.Empty;
				_L_1L_RA_RateYear1.Text = string.Empty;
				_L_1L_RA_RateYear2.Text = string.Empty;
				_L_1L_RA_RateYear3.Text = string.Empty;
				_L_1L_RA_RateYear4.Text = string.Empty;
				_L_1L_RA_RateYear5.Text = string.Empty;
				_L_1L_RA_RateYear6.Text = string.Empty;
				_L_1L_RA_RateYear7.Text = string.Empty;
				_L_1L_RA_RateYear8.Text = string.Empty;
				_L_1L_RA_RateYear9.Text = string.Empty;
				_L_1L_RA_RateYear10.Text = string.Empty;
			}
			else if ("Adjustable" == cb.SelectedItem.ToString())
			{
				_L_1L_AmortTerm.ReadOnly = false;

				_L_1L_RA_InitFixedPeriod.ReadOnly = false;
				_L_1L_RA_InitialRateCap.ReadOnly = false;
				_L_1L_RA_AnnualCap.ReadOnly = false;
				_L_1L_RA_LifeTimeCap.ReadOnly = false;
				_L_1L_RA_RateYear1.ReadOnly = false;
				_L_1L_RA_RateYear2.ReadOnly = false;
				_L_1L_RA_RateYear3.ReadOnly = false;
				_L_1L_RA_RateYear4.ReadOnly = false;
				_L_1L_RA_RateYear5.ReadOnly = false;
				_L_1L_RA_RateYear6.ReadOnly = false;
				_L_1L_RA_RateYear7.ReadOnly = false;
				_L_1L_RA_RateYear8.ReadOnly = false;
				_L_1L_RA_RateYear9.ReadOnly = false;
				_L_1L_RA_RateYear10.ReadOnly = false;
			}
			else //IO
			{
				_L_1L_AmortTerm.ReadOnly = true;

				_L_1L_RA_InitFixedPeriod.ReadOnly = true;
				_L_1L_RA_InitialRateCap.ReadOnly = true;
				_L_1L_RA_AnnualCap.ReadOnly = true;
				_L_1L_RA_LifeTimeCap.ReadOnly = true;
				_L_1L_RA_RateYear1.ReadOnly = true;
				_L_1L_RA_RateYear2.ReadOnly = true;
				_L_1L_RA_RateYear3.ReadOnly = true;
				_L_1L_RA_RateYear4.ReadOnly = true;
				_L_1L_RA_RateYear5.ReadOnly = true;
				_L_1L_RA_RateYear6.ReadOnly = true;
				_L_1L_RA_RateYear7.ReadOnly = true;
				_L_1L_RA_RateYear8.ReadOnly = true;
				_L_1L_RA_RateYear9.ReadOnly = true;
				_L_1L_RA_RateYear10.ReadOnly = true;

				_L_1L_AmortTerm.Text = string.Empty;
				_L_1L_RA_InitFixedPeriod.Text = string.Empty;
				_L_1L_RA_InitialRateCap.Text = string.Empty;
				_L_1L_RA_AnnualCap.Text = string.Empty;
				_L_1L_RA_LifeTimeCap.Text = string.Empty;
				_L_1L_RA_RateYear1.Text = string.Empty;
				_L_1L_RA_RateYear2.Text = string.Empty;
				_L_1L_RA_RateYear3.Text = string.Empty;
				_L_1L_RA_RateYear4.Text = string.Empty;
				_L_1L_RA_RateYear5.Text = string.Empty;
				_L_1L_RA_RateYear6.Text = string.Empty;
				_L_1L_RA_RateYear7.Text = string.Empty;
				_L_1L_RA_RateYear8.Text = string.Empty;
				_L_1L_RA_RateYear9.Text = string.Empty;
				_L_1L_RA_RateYear10.Text = string.Empty;
			}
		}
		
		
		private void On_L_2L_Type_SelectedValueChanged(object sender, EventArgs e)
		{
			ComboBox cb = (ComboBox)sender;
			if ("Fixed" == cb.SelectedItem.ToString())
			{
				_L_2L_AmortTerm.ReadOnly = false;
				
				_L_2L_RA_InitFixedPeriod.ReadOnly = true;
				_L_2L_RA_InitialRateCap.ReadOnly = true;
				_L_2L_RA_AnnualCap.ReadOnly = true;
				_L_2L_RA_LifeTimeCap.ReadOnly = true;
				_L_2L_RA_RateYear1.ReadOnly = true;
				_L_2L_RA_RateYear2.ReadOnly = true;
				_L_2L_RA_RateYear3.ReadOnly = true;
				_L_2L_RA_RateYear4.ReadOnly = true;
				_L_2L_RA_RateYear5.ReadOnly = true;
				_L_2L_RA_RateYear6.ReadOnly = true;
				_L_2L_RA_RateYear7.ReadOnly = true;
				_L_2L_RA_RateYear8.ReadOnly = true;
				_L_2L_RA_RateYear9.ReadOnly = true;
				_L_2L_RA_RateYear10.ReadOnly = true;

				_L_2L_RA_InitFixedPeriod.Text = string.Empty;
				_L_2L_RA_InitialRateCap.Text = string.Empty;
				_L_2L_RA_AnnualCap.Text = string.Empty;
				_L_2L_RA_LifeTimeCap.Text = string.Empty;
				_L_2L_RA_RateYear1.Text = string.Empty;
				_L_2L_RA_RateYear2.Text = string.Empty;
				_L_2L_RA_RateYear3.Text = string.Empty;
				_L_2L_RA_RateYear4.Text = string.Empty;
				_L_2L_RA_RateYear5.Text = string.Empty;
				_L_2L_RA_RateYear6.Text = string.Empty;
				_L_2L_RA_RateYear7.Text = string.Empty;
				_L_2L_RA_RateYear8.Text = string.Empty;
				_L_2L_RA_RateYear9.Text = string.Empty;
				_L_2L_RA_RateYear10.Text = string.Empty;
			}
			else if ("Adjustable" == cb.SelectedItem.ToString())
			{
				_L_2L_AmortTerm.ReadOnly = false;

				_L_2L_RA_InitFixedPeriod.ReadOnly = false;
				_L_2L_RA_InitialRateCap.ReadOnly = false;
				_L_2L_RA_AnnualCap.ReadOnly = false;
				_L_2L_RA_LifeTimeCap.ReadOnly = false;
				_L_2L_RA_RateYear1.ReadOnly = false;
				_L_2L_RA_RateYear2.ReadOnly = false;
				_L_2L_RA_RateYear3.ReadOnly = false;
				_L_2L_RA_RateYear4.ReadOnly = false;
				_L_2L_RA_RateYear5.ReadOnly = false;
				_L_2L_RA_RateYear6.ReadOnly = false;
				_L_2L_RA_RateYear7.ReadOnly = false;
				_L_2L_RA_RateYear8.ReadOnly = false;
				_L_2L_RA_RateYear9.ReadOnly = false;
				_L_2L_RA_RateYear10.ReadOnly = false;
			}
			else //IO
			{
				_L_2L_AmortTerm.ReadOnly = true;

				_L_2L_RA_InitFixedPeriod.ReadOnly = true;
				_L_2L_RA_InitialRateCap.ReadOnly = true;
				_L_2L_RA_AnnualCap.ReadOnly = true;
				_L_2L_RA_LifeTimeCap.ReadOnly = true;
				_L_2L_RA_RateYear1.ReadOnly = true;
				_L_2L_RA_RateYear2.ReadOnly = true;
				_L_2L_RA_RateYear3.ReadOnly = true;
				_L_2L_RA_RateYear4.ReadOnly = true;
				_L_2L_RA_RateYear5.ReadOnly = true;
				_L_2L_RA_RateYear6.ReadOnly = true;
				_L_2L_RA_RateYear7.ReadOnly = true;
				_L_2L_RA_RateYear8.ReadOnly = true;
				_L_2L_RA_RateYear9.ReadOnly = true;
				_L_2L_RA_RateYear10.ReadOnly = true;

				_L_2L_AmortTerm.Text = string.Empty;
				_L_2L_RA_InitFixedPeriod.Text = string.Empty;
				_L_2L_RA_InitialRateCap.Text = string.Empty;
				_L_2L_RA_AnnualCap.Text = string.Empty;
				_L_2L_RA_LifeTimeCap.Text = string.Empty;
				_L_2L_RA_RateYear1.Text = string.Empty;
				_L_2L_RA_RateYear2.Text = string.Empty;
				_L_2L_RA_RateYear3.Text = string.Empty;
				_L_2L_RA_RateYear4.Text = string.Empty;
				_L_2L_RA_RateYear5.Text = string.Empty;
				_L_2L_RA_RateYear6.Text = string.Empty;
				_L_2L_RA_RateYear7.Text = string.Empty;
				_L_2L_RA_RateYear8.Text = string.Empty;
				_L_2L_RA_RateYear9.Text = string.Empty;
				_L_2L_RA_RateYear10.Text = string.Empty;
			}
		}

		
		private void On_L_3L_Type_SelectedValueChanged(object sender, EventArgs e)
		{
			ComboBox cb = (ComboBox)sender;
			if ("Fixed" == cb.SelectedItem.ToString())
			{
				_L_3L_AmortTerm.ReadOnly = false;
				
				_L_3L_RA_InitFixedPeriod.ReadOnly = true;
				_L_3L_RA_InitialRateCap.ReadOnly = true;
				_L_3L_RA_AnnualCap.ReadOnly = true;
				_L_3L_RA_LifeTimeCap.ReadOnly = true;
				_L_3L_RA_RateYear1.ReadOnly = true;
				_L_3L_RA_RateYear2.ReadOnly = true;
				_L_3L_RA_RateYear3.ReadOnly = true;
				_L_3L_RA_RateYear4.ReadOnly = true;
				_L_3L_RA_RateYear5.ReadOnly = true;
				_L_3L_RA_RateYear6.ReadOnly = true;
				_L_3L_RA_RateYear7.ReadOnly = true;
				_L_3L_RA_RateYear8.ReadOnly = true;
				_L_3L_RA_RateYear9.ReadOnly = true;
				_L_3L_RA_RateYear10.ReadOnly = true;

				_L_3L_RA_InitFixedPeriod.Text = string.Empty;
				_L_3L_RA_InitialRateCap.Text = string.Empty;
				_L_3L_RA_AnnualCap.Text = string.Empty;
				_L_3L_RA_LifeTimeCap.Text = string.Empty;
				_L_3L_RA_RateYear1.Text = string.Empty;
				_L_3L_RA_RateYear2.Text = string.Empty;
				_L_3L_RA_RateYear3.Text = string.Empty;
				_L_3L_RA_RateYear4.Text = string.Empty;
				_L_3L_RA_RateYear5.Text = string.Empty;
				_L_3L_RA_RateYear6.Text = string.Empty;
				_L_3L_RA_RateYear7.Text = string.Empty;
				_L_3L_RA_RateYear8.Text = string.Empty;
				_L_3L_RA_RateYear9.Text = string.Empty;
				_L_3L_RA_RateYear10.Text = string.Empty;
			}
			else if ("Adjustable" == cb.SelectedItem.ToString())
			{
				_L_3L_AmortTerm.ReadOnly = false;

				_L_3L_RA_InitFixedPeriod.ReadOnly = false;
				_L_3L_RA_InitialRateCap.ReadOnly = false;
				_L_3L_RA_AnnualCap.ReadOnly = false;
				_L_3L_RA_LifeTimeCap.ReadOnly = false;
				_L_3L_RA_RateYear1.ReadOnly = false;
				_L_3L_RA_RateYear2.ReadOnly = false;
				_L_3L_RA_RateYear3.ReadOnly = false;
				_L_3L_RA_RateYear4.ReadOnly = false;
				_L_3L_RA_RateYear5.ReadOnly = false;
				_L_3L_RA_RateYear6.ReadOnly = false;
				_L_3L_RA_RateYear7.ReadOnly = false;
				_L_3L_RA_RateYear8.ReadOnly = false;
				_L_3L_RA_RateYear9.ReadOnly = false;
				_L_3L_RA_RateYear10.ReadOnly = false;
			}
			else //IO
			{
				_L_3L_AmortTerm.ReadOnly = true;

				_L_3L_RA_InitFixedPeriod.ReadOnly = true;
				_L_3L_RA_InitialRateCap.ReadOnly = true;
				_L_3L_RA_AnnualCap.ReadOnly = true;
				_L_3L_RA_LifeTimeCap.ReadOnly = true;
				_L_3L_RA_RateYear1.ReadOnly = true;
				_L_3L_RA_RateYear2.ReadOnly = true;
				_L_3L_RA_RateYear3.ReadOnly = true;
				_L_3L_RA_RateYear4.ReadOnly = true;
				_L_3L_RA_RateYear5.ReadOnly = true;
				_L_3L_RA_RateYear6.ReadOnly = true;
				_L_3L_RA_RateYear7.ReadOnly = true;
				_L_3L_RA_RateYear8.ReadOnly = true;
				_L_3L_RA_RateYear9.ReadOnly = true;
				_L_3L_RA_RateYear10.ReadOnly = true;

				_L_3L_AmortTerm.Text = string.Empty;
				_L_3L_RA_InitFixedPeriod.Text = string.Empty;
				_L_3L_RA_InitialRateCap.Text = string.Empty;
				_L_3L_RA_AnnualCap.Text = string.Empty;
				_L_3L_RA_LifeTimeCap.Text = string.Empty;
				_L_3L_RA_RateYear1.Text = string.Empty;
				_L_3L_RA_RateYear2.Text = string.Empty;
				_L_3L_RA_RateYear3.Text = string.Empty;
				_L_3L_RA_RateYear4.Text = string.Empty;
				_L_3L_RA_RateYear5.Text = string.Empty;
				_L_3L_RA_RateYear6.Text = string.Empty;
				_L_3L_RA_RateYear7.Text = string.Empty;
				_L_3L_RA_RateYear8.Text = string.Empty;
				_L_3L_RA_RateYear9.Text = string.Empty;
				_L_3L_RA_RateYear10.Text = string.Empty;
			}
		}

		
		private void On_BI_propertyValue_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyData == Keys.Enter)
				Handle_BI_propertyValueChanged();
		}

		
		private void On_BI_propertyValue_LostFocus(object sender, EventArgs e)
		{
			Handle_BI_propertyValueChanged();
		}

		
		void Handle_BI_propertyValueChanged()
		{
			_L_PurchasePrice.Text = _BI_propertyValue.Text;

			try
			{
				decimal dTemp = Convert.ToDecimal(_L_PurchasePrice.Text);
				LoansObject.Instance.PurchasePriceProperty = _L_PurchasePrice.Text = dTemp.ToString(CurrFormatter);
			}
			catch 
			{
				LoansObject.Instance.PurchasePriceProperty = _L_PurchasePrice.Text;			
			}
		}

		
		private void On_L_DownPayment_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyData == Keys.Enter)
				HandleDownPaymentChange();
		}

		
		private void On_L_DownPayment_LostFocus(object sender, EventArgs e)
		{
			HandleDownPaymentChange();
		}

		
		void HandleDownPaymentChange()
		{
			try 
			{
				decimal dTemp = Convert.ToDecimal(_L_DownPayment.Text);
				_L_DownPayment.Text = dTemp.ToString(CurrFormatter);
				LoansObject.Instance.DownPaymentProperty = dTemp.ToString(CurrFormatter);
			}
			catch 
			{
				LoansObject.Instance.DownPaymentProperty = _L_DownPayment.Text;
			}
			_L_DownPaymentPerc.Text = LoansObject.Instance.DownPaymentPercValue.ToString("0.00");
		}

		
		private void On_L_DownPaymentPerc_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyData == Keys.Enter)
				HandleDownPaymentPercChange();
		}

		
		private void On_L_DownPaymentPerc_SelectedValueChanged(object sender, EventArgs e)
		{
			HandleDownPaymentPercChange();
		}

		
		void HandleDownPaymentPercChange()
		{
			LoansObject.Instance.DownPaymentPercProperty = _L_DownPaymentPerc.Text;
			_L_DownPayment.Text = LoansObject.Instance.DownPaymentValue.ToString(CurrFormatter);
		}

		
		private void On_L_PurchasePrice_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyData == Keys.Enter)
			{
				try
				{
					decimal dTemp = Convert.ToDecimal(_L_PurchasePrice.Text);
					_L_PurchasePrice.Text = dTemp.ToString(CurrFormatter);
					LoansObject.Instance.PurchasePriceProperty = dTemp.ToString(CurrFormatter);
				}
				catch
				{
					LoansObject.Instance.PurchasePriceProperty = _L_PurchasePrice.Text;
				}
				HandleDownPaymentChange();
			}
		}

		
		private void On_L_PurchasePrice_LostFocus(object sender, EventArgs e)
		{
			try
			{
				decimal dTemp = Convert.ToDecimal(_L_PurchasePrice.Text);
				_L_PurchasePrice.Text = LoansObject.Instance.PurchasePriceProperty = dTemp.ToString(CurrFormatter);
			}
			catch 
			{
				LoansObject.Instance.PurchasePriceProperty = _L_PurchasePrice.Text;
			}
			HandleDownPaymentChange();
		}

		private void OnLoanTypeChanged(object sender, System.EventArgs e)
		{
			ComboBox cb = (ComboBox)sender;
			if (cb.Name == _L_1L_Type.Name)		
			{
				LoansObject.Instance.LoansProperty[0].LoanTypeProperty = cb.SelectedItem.ToString();
				_L_1L_MonthlyPayment.Text = LoansObject.Instance.LoansProperty[0].CalculatePeriodicPaymentAmount("Fixed" == cb.SelectedItem.ToString() ? true : false).ToString(CurrFormatter);
			}
			else if (cb.Name == _L_2L_Type.Name)			
			{
				LoansObject.Instance.LoansProperty[1].LoanTypeProperty = cb.SelectedItem.ToString();			
				_L_2L_MonthlyPayment.Text = LoansObject.Instance.LoansProperty[1].CalculatePeriodicPaymentAmount("Fixed" == cb.SelectedItem.ToString() ? true : false).ToString(CurrFormatter);
			}
			else if (cb.Name == _L_3L_Type.Name)			
			{
				LoansObject.Instance.LoansProperty[2].LoanTypeProperty = cb.SelectedItem.ToString();						
				_L_3L_MonthlyPayment.Text = LoansObject.Instance.LoansProperty[2].CalculatePeriodicPaymentAmount("Fixed" == cb.SelectedItem.ToString() ? true : false).ToString(CurrFormatter);
			}
		}
					
		
		private void _L_OnLTVKeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Enter)
				HandleLTVChange(sender);
		}

		private void _L_OnLTVLeave(object sender, System.EventArgs e)
		{
			HandleLTVChange(sender);
		}

		void HandleLTVChange(object sender)
		{
			TextBox tb = (TextBox)sender;
			if (tb.Name ==_L_1L_LTVPerc.Name)
			{
				LoansObject.Instance.LoansProperty[0].LtvPercProperty = _L_1L_LTVPerc.Text;
				_L_CLTVPerc.Text = LoansObject.Instance.CLTVPerc.ToString("0.00");

				LoansObject.Instance.LoansProperty[0].AmountProperty = ((LoansObject.Instance.LoansProperty[0].LtvPercValue / 100) * LoansObject.Instance.PurchasePriceValue).ToString(CurrFormatter);
				_L_1L_Amount.Text = LoansObject.Instance.LoansProperty[0].AmountValue.ToString(CurrFormatter);

				if (null != _L_1L_Type.SelectedItem)
					_L_1L_MonthlyPayment.Text = LoansObject.Instance.LoansProperty[0].CalculatePeriodicPaymentAmount("Fixed" == _L_1L_Type.SelectedItem.ToString() ? true : false).ToString(CurrFormatter);				

				LoansObject.Instance.LoansProperty[0].PointsUSDProperty = ((LoansObject.Instance.LoansProperty[0].PointsPercValue / 100) * LoansObject.Instance.LoansProperty[0].AmountValue).ToString(CurrFormatter);
				_L_1L_Points.Text = LoansObject.Instance.LoansProperty[0].PointsUSDValue.ToString(CurrFormatter);
			}
			else if (tb.Name ==_L_2L_LTVPerc.Name)
			{
				LoansObject.Instance.LoansProperty[1].LtvPercProperty = _L_2L_LTVPerc.Text;
				_L_CLTVPerc.Text = LoansObject.Instance.CLTVPerc.ToString(CurrFormatter);

				LoansObject.Instance.LoansProperty[1].AmountProperty = ((LoansObject.Instance.LoansProperty[1].LtvPercValue / 100) * LoansObject.Instance.PurchasePriceValue).ToString(CurrFormatter);
				_L_2L_Amount.Text = LoansObject.Instance.LoansProperty[1].AmountValue.ToString(CurrFormatter);

				if (null != _L_2L_Type.SelectedItem)
					_L_2L_MonthlyPayment.Text = LoansObject.Instance.LoansProperty[1].CalculatePeriodicPaymentAmount("Fixed" == _L_2L_Type.SelectedItem.ToString() ? true : false).ToString(CurrFormatter);

				LoansObject.Instance.LoansProperty[1].PointsUSDProperty = ((LoansObject.Instance.LoansProperty[1].PointsPercValue / 100) * LoansObject.Instance.LoansProperty[1].AmountValue).ToString(CurrFormatter);
				_L_2L_Points.Text = LoansObject.Instance.LoansProperty[1].PointsUSDValue.ToString(CurrFormatter);

			}
			else if (tb.Name ==_L_3L_LTVPerc.Name)
			{
				LoansObject.Instance.LoansProperty[2].LtvPercProperty = _L_3L_LTVPerc.Text;
				_L_CLTVPerc.Text = LoansObject.Instance.CLTVPerc.ToString(CurrFormatter);

				LoansObject.Instance.LoansProperty[2].AmountProperty = ((LoansObject.Instance.LoansProperty[2].LtvPercValue / 100) * LoansObject.Instance.PurchasePriceValue).ToString(CurrFormatter);
				_L_3L_Amount.Text = LoansObject.Instance.LoansProperty[2].AmountValue.ToString(CurrFormatter);

				if (null != _L_3L_Type.SelectedItem)
					_L_3L_MonthlyPayment.Text = LoansObject.Instance.LoansProperty[2].CalculatePeriodicPaymentAmount("Fixed" == _L_3L_Type.SelectedItem.ToString() ? true : false).ToString(CurrFormatter);

				LoansObject.Instance.LoansProperty[2].PointsUSDProperty = ((LoansObject.Instance.LoansProperty[2].PointsPercValue / 100) * LoansObject.Instance.LoansProperty[2].AmountValue).ToString(CurrFormatter);
				_L_3L_Points.Text = LoansObject.Instance.LoansProperty[2].PointsUSDValue.ToString(CurrFormatter);

			}					
		}

		private void _L_OnAmountKeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Enter)
				HandleAmountChange(sender);
		}

		private void _L_OnAmountLeave(object sender, System.EventArgs e)
		{
			HandleAmountChange(sender);
		}

		void HandleAmountChange(object sender)
		{
			TextBox tb = (TextBox)sender;
			if (tb.Name ==_L_1L_Amount.Name)
			{
				try
				{
					decimal dTemp = Convert.ToDecimal(_L_1L_Amount.Text);
					LoansObject.Instance.LoansProperty[0].AmountProperty = _L_1L_Amount.Text = dTemp.ToString(CurrFormatter);
				}
				catch 
				{
					LoansObject.Instance.LoansProperty[0].AmountProperty = _L_1L_Amount.Text;
				}
				
				_L_BlendedRate.Text = LoansObject.Instance.BlendedRatePerc.ToString("0.000");
				
				if (null != _L_1L_Type.SelectedItem)
					_L_1L_MonthlyPayment.Text = LoansObject.Instance.LoansProperty[0].CalculatePeriodicPaymentAmount("Fixed" == _L_1L_Type.SelectedItem.ToString() ? true : false).ToString(CurrFormatter);


				try { LoansObject.Instance.LoansProperty[0].LtvPercProperty = ((LoansObject.Instance.LoansProperty[0].AmountValue / LoansObject.Instance.PurchasePriceValue) * 100).ToString(CurrFormatter); }
				catch {LoansObject.Instance.LoansProperty[0].LtvPercProperty = "0"; }

				_L_1L_LTVPerc.Text = LoansObject.Instance.LoansProperty[0].LtvPercValue.ToString(CurrFormatter);

				LoansObject.Instance.LoansProperty[0].PointsUSDProperty = ((LoansObject.Instance.LoansProperty[0].PointsPercValue / 100) * LoansObject.Instance.LoansProperty[0].AmountValue).ToString(CurrFormatter);
				_L_1L_Points.Text = LoansObject.Instance.LoansProperty[0].PointsUSDValue.ToString(CurrFormatter);
			}
			else if (tb.Name ==_L_2L_Amount.Name)
			{
				try
				{
					decimal dTemp = Convert.ToDecimal(_L_2L_Amount.Text);
					LoansObject.Instance.LoansProperty[1].AmountProperty = _L_2L_Amount.Text = dTemp.ToString(CurrFormatter);
				}
				catch 
				{
					LoansObject.Instance.LoansProperty[1].AmountProperty = _L_2L_Amount.Text;
				}
				_L_BlendedRate.Text = LoansObject.Instance.BlendedRatePerc.ToString("0.000");
				
				if (null != _L_2L_Type.SelectedItem)
					_L_2L_MonthlyPayment.Text = LoansObject.Instance.LoansProperty[1].CalculatePeriodicPaymentAmount("Fixed" == _L_2L_Type.SelectedItem.ToString() ? true : false).ToString(CurrFormatter);

				try { LoansObject.Instance.LoansProperty[1].LtvPercProperty = ((LoansObject.Instance.LoansProperty[1].AmountValue / LoansObject.Instance.PurchasePriceValue) * 100).ToString(CurrFormatter); }
				catch { LoansObject.Instance.LoansProperty[1].LtvPercProperty = "0"; }
				_L_2L_LTVPerc.Text = LoansObject.Instance.LoansProperty[1].LtvPercValue.ToString(CurrFormatter);

				LoansObject.Instance.LoansProperty[1].PointsUSDProperty = ((LoansObject.Instance.LoansProperty[1].PointsPercValue / 100) * LoansObject.Instance.LoansProperty[1].AmountValue).ToString(CurrFormatter);
				_L_2L_Points.Text = LoansObject.Instance.LoansProperty[1].PointsUSDValue.ToString(CurrFormatter);

			}
			else if (tb.Name ==_L_3L_Amount.Name)
			{
				try
				{
					decimal dTemp = Convert.ToDecimal(_L_3L_Amount.Text);
					LoansObject.Instance.LoansProperty[2].AmountProperty = _L_3L_Amount.Text = dTemp.ToString(CurrFormatter);
				}
				catch 
				{
					LoansObject.Instance.LoansProperty[2].AmountProperty = _L_3L_Amount.Text;
				}

				
				_L_BlendedRate.Text = LoansObject.Instance.BlendedRatePerc.ToString("0.000");
				
				if (null != _L_3L_Type.SelectedItem)
					_L_3L_MonthlyPayment.Text = LoansObject.Instance.LoansProperty[2].CalculatePeriodicPaymentAmount("Fixed" == _L_3L_Type.SelectedItem.ToString() ? true : false).ToString(CurrFormatter);

				try { LoansObject.Instance.LoansProperty[2].LtvPercProperty = ((LoansObject.Instance.LoansProperty[2].AmountValue / LoansObject.Instance.PurchasePriceValue) * 100).ToString(CurrFormatter); }
				catch {LoansObject.Instance.LoansProperty[2].LtvPercProperty = "0";}
				_L_3L_LTVPerc.Text = LoansObject.Instance.LoansProperty[2].LtvPercValue.ToString(CurrFormatter);

				LoansObject.Instance.LoansProperty[2].PointsUSDProperty = ((LoansObject.Instance.LoansProperty[2].PointsPercValue / 100) * LoansObject.Instance.LoansProperty[2].AmountValue).ToString(CurrFormatter);
				_L_3L_Points.Text = LoansObject.Instance.LoansProperty[2].PointsUSDValue.ToString(CurrFormatter);

			}
		}

		private void L_PointsPercLeave(object sender, System.EventArgs e)
		{
			Handle_L_PointsPerc(sender);
		}

		private void L_PointsPercKeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Enter)
				Handle_L_PointsPerc(sender);
		}

		void Handle_L_PointsPerc(object sender)
		{
			TextBox tb = (TextBox)sender;

			if (tb.Name ==_L_1L_PointsPerc.Name)
			{
				LoansObject.Instance.LoansProperty[0].PointsPercProperty = _L_1L_PointsPerc.Text;
				LoansObject.Instance.LoansProperty[0].PointsUSDProperty = ((LoansObject.Instance.LoansProperty[0].PointsPercValue / 100) * LoansObject.Instance.LoansProperty[0].AmountValue).ToString(CurrFormatter);
				_L_1L_Points.Text = LoansObject.Instance.LoansProperty[0].PointsUSDValue.ToString(CurrFormatter);
			}
			else if (tb.Name ==_L_2L_PointsPerc.Name)
			{
				LoansObject.Instance.LoansProperty[1].PointsPercProperty = _L_2L_PointsPerc.Text;
				LoansObject.Instance.LoansProperty[1].PointsUSDProperty = ((LoansObject.Instance.LoansProperty[1].PointsPercValue / 100) * LoansObject.Instance.LoansProperty[1].AmountValue).ToString(CurrFormatter);
				_L_2L_Points.Text = LoansObject.Instance.LoansProperty[1].PointsUSDValue.ToString(CurrFormatter);
			}
			else if (tb.Name ==_L_3L_PointsPerc.Name)
			{
				LoansObject.Instance.LoansProperty[2].PointsPercProperty = _L_3L_PointsPerc.Text;
				LoansObject.Instance.LoansProperty[2].PointsUSDProperty = ((LoansObject.Instance.LoansProperty[2].PointsPercValue / 100) * LoansObject.Instance.LoansProperty[2].AmountValue).ToString(CurrFormatter);
				_L_3L_Points.Text = LoansObject.Instance.LoansProperty[2].PointsUSDValue.ToString(CurrFormatter);
			}
		}

		private void L_Points_KeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Enter)
				Handle_L_Points(sender);
		}

		private void L_Points_Leave(object sender, System.EventArgs e)
		{
			Handle_L_Points(sender);
		}

		void Handle_L_Points(object sender)
		{
			TextBox tb = (TextBox)sender;

			if (tb.Name ==_L_1L_Points.Name)
			{
				try 
				{
					decimal dTemp = Convert.ToDecimal(_L_1L_Points.Text);
					LoansObject.Instance.LoansProperty[0].PointsUSDProperty = _L_1L_Points.Text = dTemp.ToString(CurrFormatter);
					LoansObject.Instance.LoansProperty[0].PointsPercProperty = ((dTemp / LoansObject.Instance.LoansProperty[0].AmountValue) * 100).ToString("0.00");
					_L_1L_PointsPerc.Text = LoansObject.Instance.LoansProperty[0].PointsPercValue.ToString(CurrFormatter);
				}
				catch 
				{
					LoansObject.Instance.LoansProperty[0].PointsUSDProperty = _L_1L_Points.Text;
				}
			}
			else if (tb.Name ==_L_2L_Points.Name)
			{
				try
				{
					decimal dTemp = Convert.ToDecimal(_L_2L_Points.Text);
					LoansObject.Instance.LoansProperty[1].PointsUSDProperty = _L_2L_Points.Text = dTemp.ToString(CurrFormatter);
					LoansObject.Instance.LoansProperty[1].PointsPercProperty = ((dTemp / LoansObject.Instance.LoansProperty[1].AmountValue) * 100).ToString("0.00");;
					_L_2L_PointsPerc.Text = LoansObject.Instance.LoansProperty[1].PointsPercValue.ToString(CurrFormatter);
				}
				catch 
				{
					LoansObject.Instance.LoansProperty[1].PointsUSDProperty = _L_2L_Points.Text;
				}
			}
			else if (tb.Name ==_L_3L_Points.Name)
			{
				try
				{
					decimal dTemp = Convert.ToDecimal(_L_3L_Points.Text);
					LoansObject.Instance.LoansProperty[2].PointsUSDProperty = _L_3L_Points.Text = dTemp.ToString(CurrFormatter);
					LoansObject.Instance.LoansProperty[2].PointsPercProperty = ((dTemp / LoansObject.Instance.LoansProperty[2].AmountValue) * 100).ToString("0.00");;
					_L_3L_PointsPerc.Text = LoansObject.Instance.LoansProperty[2].PointsPercValue.ToString(CurrFormatter);
				}
				catch 
				{
					LoansObject.Instance.LoansProperty[2].PointsUSDProperty = _L_3L_Points.Text;
				}
			}
		}

		decimal CalculateDCR()
		{
			/*
			 *  DCR = NOI/(total annual payments)
				NOI = Actual Operating Income (from Income tab) - All Types_Total_$/year (from Expenses tab)
			 */
			
			decimal dcr = 0;
			decimal noi = 0;			
			decimal operatingIncome = IncomeObject.Instance.ActualIncomeObject.OperatingIncomeProperty;			

			decimal usdPerYear = 0;
			
			try { usdPerYear = Convert.ToDecimal(ExpensesObject.Instance.TotalExpensesItemsProperty.Items[5].UsdPerYearProperty); }
			catch { usdPerYear = 0; }

			noi = operatingIncome - usdPerYear;

			if (0 != LoansObject.Instance.TotalAnnualDebtService)
				dcr = noi / LoansObject.Instance.TotalAnnualDebtService;
			else
				dcr = 0;
			
			return dcr;
        }

		private void OnLoansTextChanged(object sender, System.EventArgs e)
		{
			TextBox tb = (TextBox)sender;
			
			//1st LOAN
			if (tb.Name ==_L_1L_AmortTerm.Name)
			{
				LoansObject.Instance.LoansProperty[0].AmortTermProperty = _L_1L_AmortTerm.Text;
				_L_1L_MonthlyPayment.Text = LoansObject.Instance.LoansProperty[0].CalculatePeriodicPaymentAmount("Fixed" == _L_1L_Type.SelectedItem.ToString() ? true : false).ToString(CurrFormatter);
			}
			else if (tb.Name ==_L_1L_Rate.Name)
			{
				LoansObject.Instance.LoansProperty[0].RatePercProperty = _L_1L_Rate.Text;
				_L_BlendedRate.Text = LoansObject.Instance.BlendedRatePerc.ToString("0.000");
				_L_1L_MonthlyPayment.Text = LoansObject.Instance.LoansProperty[0].CalculatePeriodicPaymentAmount("Fixed" == _L_1L_Type.SelectedItem.ToString() ? true : false).ToString(CurrFormatter);
			}
			else if (tb.Name ==_L_1L_MonthlyPayment.Name)
			{
				try
				{
					decimal dTemp = Convert.ToDecimal(_L_1L_MonthlyPayment.Text);
					LoansObject.Instance.LoansProperty[0].MonthlyPaymentProperty = _L_1L_MonthlyPayment.Text = dTemp.ToString(CurrFormatter);
				}
				catch
				{
					LoansObject.Instance.LoansProperty[0].MonthlyPaymentProperty = _L_1L_MonthlyPayment.Text;
				}

				_L_TotalPaymentsMonthly.Text = LoansObject.Instance.TotalMonthlyPayments.ToString(CurrFormatter);
				_L_TotalPaymentsAnnual.Text = LoansObject.Instance.TotalAnnualDebtService.ToString(CurrFormatter);
				_L_DCR.Text = CalculateDCR().ToString(CurrFormatter);
			}						
			else if (tb.Name ==_L_1L_FirstPaymentDate.Name)
			{
				LoansObject.Instance.LoansProperty[0].FirstPaymentDateProperty = _L_1L_FirstPaymentDate.Text;
			}
			else if (tb.Name ==_L_1L_LastPaymentDate.Name)
			{
				LoansObject.Instance.LoansProperty[0].LastPaymentDateProperty = _L_1L_LastPaymentDate.Text;
			}
			else if (tb.Name ==_L_1L_Lender.Name)
			{
				LoansObject.Instance.LoansProperty[0].LenderProperty = _L_1L_Lender.Text;
			}
			else if (tb.Name ==_L_1L_RA_InitFixedPeriod.Name)
			{
				LoansObject.Instance.LoansProperty[0].LrAdjustmentsProperty.InitialFixedPeriodYrsProperty = _L_1L_RA_InitFixedPeriod.Text;
			}
			else if (tb.Name ==_L_1L_RA_AnnualCap.Name)
			{
				LoansObject.Instance.LoansProperty[0].LrAdjustmentsProperty.AnnualCapPercProperty = _L_1L_RA_AnnualCap.Text;
			}
			else if (tb.Name ==_L_1L_RA_InitialRateCap.Name)
			{
				LoansObject.Instance.LoansProperty[0].LrAdjustmentsProperty.InitialRateCapPercProperty = _L_1L_RA_InitialRateCap.Text;
			}
			else if (tb.Name ==_L_1L_RA_LifeTimeCap.Name)
			{
				LoansObject.Instance.LoansProperty[0].LrAdjustmentsProperty.LifeTimeCapPercProperty = _L_1L_RA_LifeTimeCap.Text;
			}
			else if (tb.Name ==_L_1L_RA_RateYear1.Name)
			{
				LoansObject.Instance.LoansProperty[0].LrAdjustmentsProperty.RatesProperty[0] = _L_1L_RA_RateYear1.Text;
			}
			else if (tb.Name ==_L_1L_RA_RateYear2.Name)
			{
				LoansObject.Instance.LoansProperty[0].LrAdjustmentsProperty.RatesProperty[1] = _L_1L_RA_RateYear2.Text;
			}
			else if (tb.Name ==_L_1L_RA_RateYear3.Name)
			{
				LoansObject.Instance.LoansProperty[0].LrAdjustmentsProperty.RatesProperty[2] = _L_1L_RA_RateYear3.Text;
			}
			else if (tb.Name ==_L_1L_RA_RateYear4.Name)
			{
				LoansObject.Instance.LoansProperty[0].LrAdjustmentsProperty.RatesProperty[3] = _L_1L_RA_RateYear4.Text;
			}
			else if (tb.Name ==_L_1L_RA_RateYear5.Name)
			{
				LoansObject.Instance.LoansProperty[0].LrAdjustmentsProperty.RatesProperty[4] = _L_1L_RA_RateYear5.Text;
			}
			else if (tb.Name ==_L_1L_RA_RateYear6.Name)
			{
				LoansObject.Instance.LoansProperty[0].LrAdjustmentsProperty.RatesProperty[5] = _L_1L_RA_RateYear6.Text;
			}
			else if (tb.Name ==_L_1L_RA_RateYear7.Name)
			{
				LoansObject.Instance.LoansProperty[0].LrAdjustmentsProperty.RatesProperty[6] = _L_1L_RA_RateYear7.Text;
			}
			else if (tb.Name ==_L_1L_RA_RateYear8.Name)
			{
				LoansObject.Instance.LoansProperty[0].LrAdjustmentsProperty.RatesProperty[7] = _L_1L_RA_RateYear8.Text;
			}
			else if (tb.Name ==_L_1L_RA_RateYear9.Name)
			{
				LoansObject.Instance.LoansProperty[0].LrAdjustmentsProperty.RatesProperty[8] = _L_1L_RA_RateYear9.Text;
			}
			else if (tb.Name ==_L_1L_RA_RateYear10.Name)
			{
				LoansObject.Instance.LoansProperty[0].LrAdjustmentsProperty.RatesProperty[9] = _L_1L_RA_RateYear10.Text;
			}


			//2nd LOAN
			else if (tb.Name ==_L_2L_AmortTerm.Name)
			{
				LoansObject.Instance.LoansProperty[1].AmortTermProperty = _L_2L_AmortTerm.Text;
				_L_2L_MonthlyPayment.Text = LoansObject.Instance.LoansProperty[1].CalculatePeriodicPaymentAmount("Fixed" == _L_2L_Type.SelectedItem.ToString() ? true : false).ToString(CurrFormatter);
			}
			else if (tb.Name ==_L_2L_Rate.Name)
			{
				LoansObject.Instance.LoansProperty[1].RatePercProperty = _L_2L_Rate.Text;
				_L_BlendedRate.Text = LoansObject.Instance.BlendedRatePerc.ToString("0.000");
				_L_2L_MonthlyPayment.Text = LoansObject.Instance.LoansProperty[1].CalculatePeriodicPaymentAmount("Fixed" == _L_2L_Type.SelectedItem.ToString() ? true : false).ToString(CurrFormatter);
			}
			else if (tb.Name ==_L_2L_MonthlyPayment.Name)
			{
				try
				{
					decimal dTemp = Convert.ToDecimal(_L_2L_MonthlyPayment.Text);
					LoansObject.Instance.LoansProperty[1].MonthlyPaymentProperty = _L_2L_MonthlyPayment.Text = dTemp.ToString(CurrFormatter);
				}
				catch
				{
					LoansObject.Instance.LoansProperty[1].MonthlyPaymentProperty = _L_2L_MonthlyPayment.Text;
				}

				_L_TotalPaymentsMonthly.Text = LoansObject.Instance.TotalMonthlyPayments.ToString(CurrFormatter);
				_L_TotalPaymentsAnnual.Text = LoansObject.Instance.TotalAnnualDebtService.ToString(CurrFormatter);
				_L_DCR.Text = CalculateDCR().ToString(CurrFormatter);
			}						
			else if (tb.Name ==_L_2L_FirstPaymentDate.Name)
			{
				LoansObject.Instance.LoansProperty[1].FirstPaymentDateProperty = _L_2L_FirstPaymentDate.Text;
			}
			else if (tb.Name ==_L_2L_LastPaymentDate.Name)
			{
				LoansObject.Instance.LoansProperty[1].LastPaymentDateProperty = _L_2L_LastPaymentDate.Text;
			}
			else if (tb.Name ==_L_2L_Lender.Name)
			{
				LoansObject.Instance.LoansProperty[1].LenderProperty = _L_2L_Lender.Text;
			}
			else if (tb.Name ==_L_2L_RA_InitFixedPeriod.Name)
			{
				LoansObject.Instance.LoansProperty[1].LrAdjustmentsProperty.InitialFixedPeriodYrsProperty = _L_2L_RA_InitFixedPeriod.Text;
			}
			else if (tb.Name ==_L_2L_RA_AnnualCap.Name)
			{
				LoansObject.Instance.LoansProperty[1].LrAdjustmentsProperty.AnnualCapPercProperty = _L_2L_RA_AnnualCap.Text;
			}
			else if (tb.Name ==_L_2L_RA_InitialRateCap.Name)
			{
				LoansObject.Instance.LoansProperty[1].LrAdjustmentsProperty.InitialRateCapPercProperty = _L_2L_RA_InitialRateCap.Text;
			}
			else if (tb.Name ==_L_2L_RA_LifeTimeCap.Name)
			{
				LoansObject.Instance.LoansProperty[1].LrAdjustmentsProperty.LifeTimeCapPercProperty = _L_2L_RA_LifeTimeCap.Text;
			}
			else if (tb.Name ==_L_2L_RA_RateYear1.Name)
			{
				LoansObject.Instance.LoansProperty[1].LrAdjustmentsProperty.RatesProperty[0] = _L_2L_RA_RateYear1.Text;
			}
			else if (tb.Name ==_L_2L_RA_RateYear2.Name)
			{
				LoansObject.Instance.LoansProperty[1].LrAdjustmentsProperty.RatesProperty[1] = _L_2L_RA_RateYear2.Text;
			}
			else if (tb.Name ==_L_2L_RA_RateYear3.Name)
			{
				LoansObject.Instance.LoansProperty[1].LrAdjustmentsProperty.RatesProperty[2] = _L_2L_RA_RateYear3.Text;
			}
			else if (tb.Name ==_L_2L_RA_RateYear4.Name)
			{
				LoansObject.Instance.LoansProperty[1].LrAdjustmentsProperty.RatesProperty[3] = _L_2L_RA_RateYear4.Text;
			}
			else if (tb.Name ==_L_2L_RA_RateYear5.Name)
			{
				LoansObject.Instance.LoansProperty[1].LrAdjustmentsProperty.RatesProperty[4] = _L_2L_RA_RateYear5.Text;
			}
			else if (tb.Name ==_L_2L_RA_RateYear6.Name)
			{
				LoansObject.Instance.LoansProperty[1].LrAdjustmentsProperty.RatesProperty[5] = _L_2L_RA_RateYear6.Text;
			}
			else if (tb.Name ==_L_2L_RA_RateYear7.Name)
			{
				LoansObject.Instance.LoansProperty[1].LrAdjustmentsProperty.RatesProperty[6] = _L_2L_RA_RateYear7.Text;
			}
			else if (tb.Name ==_L_2L_RA_RateYear8.Name)
			{
				LoansObject.Instance.LoansProperty[1].LrAdjustmentsProperty.RatesProperty[7] = _L_2L_RA_RateYear8.Text;
			}
			else if (tb.Name ==_L_2L_RA_RateYear9.Name)
			{
				LoansObject.Instance.LoansProperty[1].LrAdjustmentsProperty.RatesProperty[8] = _L_2L_RA_RateYear9.Text;
			}
			else if (tb.Name ==_L_2L_RA_RateYear10.Name)
			{
				LoansObject.Instance.LoansProperty[1].LrAdjustmentsProperty.RatesProperty[9] = _L_2L_RA_RateYear10.Text;
			}

			//3rd LOAN
			else if (tb.Name ==_L_3L_AmortTerm.Name)
			{
				LoansObject.Instance.LoansProperty[2].AmortTermProperty = _L_3L_AmortTerm.Text;
				_L_3L_MonthlyPayment.Text = LoansObject.Instance.LoansProperty[2].CalculatePeriodicPaymentAmount("Fixed" == _L_3L_Type.SelectedItem.ToString() ? true : false).ToString(CurrFormatter);
			}
			else if (tb.Name ==_L_3L_Rate.Name)
			{
				LoansObject.Instance.LoansProperty[2].RatePercProperty = _L_3L_Rate.Text;
				_L_BlendedRate.Text = LoansObject.Instance.BlendedRatePerc.ToString("0.000");
				_L_3L_MonthlyPayment.Text = LoansObject.Instance.LoansProperty[2].CalculatePeriodicPaymentAmount("Fixed" == _L_3L_Type.SelectedItem.ToString() ? true : false).ToString(CurrFormatter);
			}
			else if (tb.Name ==_L_3L_MonthlyPayment.Name)
			{
				try
				{
					decimal dTemp = Convert.ToDecimal(_L_3L_MonthlyPayment.Text);
					LoansObject.Instance.LoansProperty[2].MonthlyPaymentProperty = _L_3L_MonthlyPayment.Text = dTemp.ToString(CurrFormatter);
				}
				catch 
				{
					LoansObject.Instance.LoansProperty[2].MonthlyPaymentProperty = _L_3L_MonthlyPayment.Text;
				}

				_L_TotalPaymentsMonthly.Text = LoansObject.Instance.TotalMonthlyPayments.ToString(CurrFormatter);
				_L_TotalPaymentsAnnual.Text = LoansObject.Instance.TotalAnnualDebtService.ToString(CurrFormatter);
				_L_DCR.Text = CalculateDCR().ToString(CurrFormatter);
			}						
			else if (tb.Name ==_L_3L_FirstPaymentDate.Name)
			{
				LoansObject.Instance.LoansProperty[2].FirstPaymentDateProperty = _L_3L_FirstPaymentDate.Text;
			}
			else if (tb.Name ==_L_3L_LastPaymentDate.Name)
			{
				LoansObject.Instance.LoansProperty[2].LastPaymentDateProperty = _L_3L_LastPaymentDate.Text;
			}
			else if (tb.Name ==_L_3L_Lender.Name)
			{
				LoansObject.Instance.LoansProperty[2].LenderProperty = _L_3L_Lender.Text;
			}
			else if (tb.Name ==_L_3L_RA_InitFixedPeriod.Name)
			{
				LoansObject.Instance.LoansProperty[2].LrAdjustmentsProperty.InitialFixedPeriodYrsProperty = _L_3L_RA_InitFixedPeriod.Text;
			}
			else if (tb.Name ==_L_3L_RA_AnnualCap.Name)
			{
				LoansObject.Instance.LoansProperty[2].LrAdjustmentsProperty.AnnualCapPercProperty = _L_3L_RA_AnnualCap.Text;
			}
			else if (tb.Name ==_L_3L_RA_InitialRateCap.Name)
			{
				LoansObject.Instance.LoansProperty[2].LrAdjustmentsProperty.InitialRateCapPercProperty = _L_3L_RA_InitialRateCap.Text;
			}
			else if (tb.Name ==_L_3L_RA_LifeTimeCap.Name)
			{
				LoansObject.Instance.LoansProperty[2].LrAdjustmentsProperty.LifeTimeCapPercProperty = _L_3L_RA_LifeTimeCap.Text;
			}
			else if (tb.Name ==_L_3L_RA_RateYear1.Name)
			{
				LoansObject.Instance.LoansProperty[2].LrAdjustmentsProperty.RatesProperty[0] = _L_3L_RA_RateYear1.Text;
			}
			else if (tb.Name ==_L_3L_RA_RateYear2.Name)
			{
				LoansObject.Instance.LoansProperty[2].LrAdjustmentsProperty.RatesProperty[1] = _L_3L_RA_RateYear2.Text;
			}
			else if (tb.Name ==_L_3L_RA_RateYear3.Name)
			{
				LoansObject.Instance.LoansProperty[2].LrAdjustmentsProperty.RatesProperty[2] = _L_3L_RA_RateYear3.Text;
			}
			else if (tb.Name ==_L_3L_RA_RateYear4.Name)
			{
				LoansObject.Instance.LoansProperty[2].LrAdjustmentsProperty.RatesProperty[3] = _L_3L_RA_RateYear4.Text;
			}
			else if (tb.Name ==_L_3L_RA_RateYear5.Name)
			{
				LoansObject.Instance.LoansProperty[2].LrAdjustmentsProperty.RatesProperty[4] = _L_3L_RA_RateYear5.Text;
			}
			else if (tb.Name ==_L_3L_RA_RateYear6.Name)
			{
				LoansObject.Instance.LoansProperty[2].LrAdjustmentsProperty.RatesProperty[5] = _L_3L_RA_RateYear6.Text;
			}
			else if (tb.Name ==_L_3L_RA_RateYear7.Name)
			{
				LoansObject.Instance.LoansProperty[2].LrAdjustmentsProperty.RatesProperty[6] = _L_3L_RA_RateYear7.Text;
			}
			else if (tb.Name ==_L_3L_RA_RateYear8.Name)
			{
				LoansObject.Instance.LoansProperty[2].LrAdjustmentsProperty.RatesProperty[7] = _L_3L_RA_RateYear8.Text;
			}
			else if (tb.Name ==_L_3L_RA_RateYear9.Name)
			{
				LoansObject.Instance.LoansProperty[2].LrAdjustmentsProperty.RatesProperty[8] = _L_3L_RA_RateYear9.Text;
			}
			else if (tb.Name ==_L_3L_RA_RateYear10.Name)
			{
				LoansObject.Instance.LoansProperty[2].LrAdjustmentsProperty.RatesProperty[9] = _L_3L_RA_RateYear10.Text;
			}

		}

		#endregion

		#region FINANCIAL ANALYSIS
		
		//ACTUAL
		///<summary>Copy ACTUAL INCOME from Income tab</summary>		
		private void On_FA_copyAIfromIncome_Click(object sender, EventArgs e)
		{			
			//GSI ANNUAL
			_FA_A_GSIAnnual.Text = IncomeObject.Instance.ActualIncomeObject.GrossRentProperty.ToString(CurrFormatter);
			FinancialInfoObject.Instance.Data[0].GsiAnnualProperty = IncomeObject.Instance.ActualIncomeObject.GrossRentProperty.ToString(CurrFormatter);			

			//VACANCY CREDIT LOSS PERC and VALUE
			_FA_A_VacancyCreditLossPerc.Text = (IncomeObject.Instance.ActualIncomeObject.VacancyOrCreditLossPercProperty * 100).ToString(CurrFormatter);
			FinancialInfoObject.Instance.Data[0].VacancyCreditLossPercProperty = (IncomeObject.Instance.ActualIncomeObject.VacancyOrCreditLossPercProperty * 100).ToString(CurrFormatter);

			_FA_A_VacancyCreditLoss.Text = IncomeObject.Instance.ActualIncomeObject.VacancyOrCreditLossProperty.ToString(CurrFormatter);
			FinancialInfoObject.Instance.Data[0].VacancyCreditLossProperty = IncomeObject.Instance.ActualIncomeObject.VacancyOrCreditLossProperty.ToString(CurrFormatter);

			//OTHER INCOME
			_FA_A_OtherIncome.Text = IncomeObject.Instance.ActualIncomeObject.OtherIncomeProperty.ToString(CurrFormatter);
			FinancialInfoObject.Instance.Data[0].OtherIncomeProperty = IncomeObject.Instance.ActualIncomeObject.OtherIncomeProperty.ToString(CurrFormatter);
		}

		/// <summary>Copy EXPENSES from EXPENSES TAB</summary>		
		private void On_FA_copyEfromExpenses_Click(object sender, EventArgs e)
		{
			//OPERATING EXPENSES
			decimal usdPerMonthValue;
			decimal usdPerYearValue;
			decimal usdPerSqFtValue;
			decimal usdPerUnitValue;

			ExpensesObject.Instance.ExpensesItemsProperty.GetExpensesTotal(ExpensesTypes.Operating, out usdPerMonthValue, out usdPerYearValue, out usdPerSqFtValue, out usdPerUnitValue);
			decimal operatingTotalUsdPerYearValue = usdPerYearValue;

			//TYPES TOTALs
			decimal usdPerMonthValueTotal;
			decimal usdPerYearValueTotal;
			decimal usdPerSqFtValueTotal;
			decimal usdPerUnitValueTotal;
			ExpensesObject.Instance.ExpensesItemsProperty.GetExpensesTypesTotal(out usdPerMonthValueTotal, out usdPerYearValueTotal, out usdPerSqFtValueTotal, out usdPerUnitValueTotal);
			
			_FA_A_OperatingExpenses.Text = usdPerYearValueTotal.ToString(CurrFormatter);
			FinancialInfoObject.Instance.Data[0].OperatingExpensesProperty = usdPerYearValueTotal.ToString(CurrFormatter);
			
			if (0 != FinancialInfoObject.Instance.Data[0].GsiAnnualValueProperty)
			{
				_FA_A_OperatingExpensesPerc.Text = ((usdPerYearValueTotal / FinancialInfoObject.Instance.Data[0].GsiAnnualValueProperty) * 100).ToString(CurrFormatter);			
				FinancialInfoObject.Instance.Data[0].OperatingExpensesPercProperty = ((usdPerYearValueTotal / FinancialInfoObject.Instance.Data[0].GsiAnnualValueProperty) * 100).ToString(CurrFormatter);
			}
		}

		/// <summary>Copy Data from LOAN tab</summary>		
		private void On_FA_copyPDfromLoan_Click(object sender, EventArgs e)
		{
			//PRICE / VALUE
			_FA_A_Price.Text = LoansObject.Instance.PurchasePriceValue.ToString(CurrFormatter);
			FinancialInfoObject.Instance.Data[0].PriceProperty = LoansObject.Instance.PurchasePriceValue.ToString(CurrFormatter);

			_FA_A_DebtService.Text = LoansObject.Instance.TotalAnnualDebtService.ToString(CurrFormatter);
			FinancialInfoObject.Instance.Data[0].DebtServiceProperty = LoansObject.Instance.TotalAnnualDebtService.ToString(CurrFormatter);

			PriceValueChanged(TabColumnName.Actual);
			GSIChanged(TabColumnName.Actual);
			VacancyPercChanged(TabColumnName.Actual);
			OperatingExpensesPercChanged(TabColumnName.Actual);			
			DebtServiceChanged(TabColumnName.Actual);
		}

		//PROFORMA
		private void On_FA_P_copyPIfromIncome_Click(object sender, EventArgs e)
		{
			//GSI ANNUAL
			_FA_P_GSIAnnual.Text = IncomeObject.Instance.ProformaIncomeObject.GrossRent.ToString(CurrFormatter);
			FinancialInfoObject.Instance.Data[1].GsiAnnualProperty = IncomeObject.Instance.ProformaIncomeObject.GrossRent.ToString(CurrFormatter);

			//VACANCY CREDIT LOSS PERC
			_FA_P_VacancyCreditLossPerc.Text = (IncomeObject.Instance.ProformaIncomeObject.VacancyOrCreditlossPerc * 100).ToString(CurrFormatter);
			FinancialInfoObject.Instance.Data[1].VacancyCreditLossPercProperty = (IncomeObject.Instance.ProformaIncomeObject.VacancyOrCreditlossPerc * 100).ToString(CurrFormatter);

			_FA_P_VacancyCreditLoss.Text = IncomeObject.Instance.ProformaIncomeObject.VacancyOrCreditloss.ToString(CurrFormatter);
			FinancialInfoObject.Instance.Data[1].VacancyCreditLossProperty = IncomeObject.Instance.ProformaIncomeObject.VacancyOrCreditloss.ToString(CurrFormatter);

			//OTHER INCOME
			_FA_P_OtherIncome.Text = IncomeObject.Instance.ProformaIncomeObject.OtherIncome.ToString(CurrFormatter);
			FinancialInfoObject.Instance.Data[1].OtherIncomeProperty = IncomeObject.Instance.ProformaIncomeObject.OtherIncome.ToString(CurrFormatter);
		}

		private void On_FA_P_copyEfromExpenses_Click(object sender, EventArgs e)
		{
			//OPERATING EXPENSES
			
			//TYPES TOTALs
			decimal usdPerMonthValueTotal;
			decimal usdPerYearValueTotal;
			decimal usdPerSqFtValueTotal;
			decimal usdPerUnitValueTotal;
			ExpensesObject.Instance.ExpensesItemsProperty.GetExpensesTypesTotal(out usdPerMonthValueTotal, out usdPerYearValueTotal, out usdPerSqFtValueTotal, out usdPerUnitValueTotal);
			
			_FA_P_OperatingExpenses.Text = usdPerYearValueTotal.ToString(CurrFormatter);
			FinancialInfoObject.Instance.Data[1].OperatingExpensesProperty = usdPerYearValueTotal.ToString(CurrFormatter);
			
			if (0 != FinancialInfoObject.Instance.Data[1].GsiAnnualValueProperty)
			{
				_FA_P_OperatingExpensesPerc.Text = ((usdPerYearValueTotal / FinancialInfoObject.Instance.Data[1].GsiAnnualValueProperty) * 100).ToString(CurrFormatter);			
				FinancialInfoObject.Instance.Data[1].OperatingExpensesPercProperty = ((usdPerYearValueTotal / FinancialInfoObject.Instance.Data[1].GsiAnnualValueProperty) * 100).ToString(CurrFormatter);
			}			
		}

		private void On_FA_P_copyFromActual_Click(object sender, EventArgs e)
		{
			FinancialInfoObject.Instance.Data[1] = new FA_Data(FinancialInfoObject.Instance.Data[0]);
			Update_FI_Proforma_GUI(1);
		}

		//SCENARIO
		private void On_FA_S_copyFromActual_Click(object sender, EventArgs e)
		{
			FinancialInfoObject.Instance.Data[2] = new FA_Data(FinancialInfoObject.Instance.Data[0]);
			Update_FI_Scenario_GUI(2);
		}

		private void On_FA_S_copyFromProforma_Click(object sender, EventArgs e)
		{
			FinancialInfoObject.Instance.Data[2] = new FA_Data(FinancialInfoObject.Instance.Data[1]);
			Update_FI_Scenario_GUI(2);
		}

		void Update_FI_Proforma_GUI(int idx)
		{
			_FA_P_Price.Text = FinancialInfoObject.Instance.Data[idx].PriceProperty;
			_FA_P_GSIAnnual.Text = FinancialInfoObject.Instance.Data[idx].GsiAnnualProperty;
			_FA_P_VacancyCreditLoss.Text = FinancialInfoObject.Instance.Data[idx].VacancyCreditLossProperty;
			_FA_P_VacancyCreditLossPerc.Text = FinancialInfoObject.Instance.Data[idx].VacancyCreditLossPercProperty;
			_FA_P_OtherIncome.Text = FinancialInfoObject.Instance.Data[idx].OtherIncomeProperty;
			_FA_P_GOIAnnual.Text = FinancialInfoObject.Instance.Data[idx].GoiAnnualProperty;
			_FA_P_OperatingExpenses.Text = FinancialInfoObject.Instance.Data[idx].OperatingExpensesProperty;
			_FA_P_OperatingExpensesPerc.Text = FinancialInfoObject.Instance.Data[idx].OperatingExpensesPercProperty;
			_FA_P_NetOperatingIncome.Text = FinancialInfoObject.Instance.Data[idx].NetOperatingIncomeProperty;
			_FA_P_CapRate.Text = FinancialInfoObject.Instance.Data[idx].CapRateProperty;
			_FA_P_DebtService.Text = FinancialInfoObject.Instance.Data[idx].DebtServiceProperty;
			_FA_P_DebtCoverageRatio.Text = FinancialInfoObject.Instance.Data[idx].DebtCoverageRatioProperty;
			_FA_P_CashFlow.Text = FinancialInfoObject.Instance.Data[idx].CashFlowPreTaxProperty;
			_FA_P_CashOnCashReturn.Text = FinancialInfoObject.Instance.Data[idx].CashOnCashReturnProperty;
		}

		void Update_FI_Scenario_GUI(int idx)
		{
			_FA_S_Price.Text = FinancialInfoObject.Instance.Data[idx].PriceProperty;
			_FA_S_GSIAnnual.Text = FinancialInfoObject.Instance.Data[idx].GsiAnnualProperty;
			_FA_S_VacancyCreditLoss.Text = FinancialInfoObject.Instance.Data[idx].VacancyCreditLossProperty;
			_FA_S_VacancyCreditLossPerc.Text = FinancialInfoObject.Instance.Data[idx].VacancyCreditLossPercProperty;
			_FA_S_OtherIncome.Text = FinancialInfoObject.Instance.Data[idx].OtherIncomeProperty;
			_FA_S_GOIAnnual.Text = FinancialInfoObject.Instance.Data[idx].GoiAnnualProperty;
			_FA_S_OperatingExpenses.Text = FinancialInfoObject.Instance.Data[idx].OperatingExpensesProperty;
			_FA_S_OperatingExpensesPerc.Text = FinancialInfoObject.Instance.Data[idx].OperatingExpensesPercProperty;
			_FA_S_NetOperatingIncome.Text = FinancialInfoObject.Instance.Data[idx].NetOperatingIncomeProperty;
			_FA_S_CapRate.Text = FinancialInfoObject.Instance.Data[idx].CapRateProperty;
			_FA_S_DebtService.Text = FinancialInfoObject.Instance.Data[idx].DebtServiceProperty;
			_FA_S_DebtCoverageRatio.Text = FinancialInfoObject.Instance.Data[idx].DebtCoverageRatioProperty;
			_FA_S_CashFlow.Text = FinancialInfoObject.Instance.Data[idx].CashFlowPreTaxProperty;
			_FA_S_CashOnCashReturn.Text = FinancialInfoObject.Instance.Data[idx].CashOnCashReturnProperty;
		}

		enum TabColumnName
		{
			Actual, 
			Proforma, 
			Scenario,
		}

		private void _FA_A_OnKeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Enter)
				Handle_FA(sender, TabColumnName.Actual);
		}

		private void _FA_A_OnLeave(object sender, System.EventArgs e)
		{
			Handle_FA(sender, TabColumnName.Actual);			
		}	

		private void _FA_P_OnKeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Enter)
				Handle_FA(sender, TabColumnName.Proforma);
		}

		private void _FA_P_OnLeave(object sender, System.EventArgs e)
		{
			Handle_FA(sender, TabColumnName.Proforma);
		}

		private void _FA_S_OnKeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Enter)
				Handle_FA(sender, TabColumnName.Scenario);
		}

		private void _FA_S_OnLeave(object sender, System.EventArgs e)
		{
			Handle_FA(sender, TabColumnName.Scenario);
		}
		
		void Handle_FA(object sender, TabColumnName tcn)
		{
			TextBox tb = (TextBox)sender;

			if (tb.Name == _FA_A_Price.Name || tb.Name == _FA_P_Price.Name || tb.Name == _FA_S_Price.Name)
			{
				PriceValueChanged(tcn);
			}
			else if (tb.Name == _FA_A_GSIAnnual.Name || tb.Name == _FA_P_GSIAnnual.Name || tb.Name == _FA_S_GSIAnnual.Name)
			{
				GSIChanged(tcn);
			}
			else if (tb.Name == _FA_A_VacancyCreditLoss.Name || tb.Name == _FA_P_VacancyCreditLoss.Name || tb.Name == _FA_S_VacancyCreditLoss.Name)
			{				
				//RECALC PERC
				VacancyChanged(tcn);
			}
			else if (tb.Name == _FA_A_VacancyCreditLossPerc.Name || tb.Name == _FA_P_VacancyCreditLossPerc.Name || tb.Name == _FA_S_VacancyCreditLossPerc.Name)
			{
				VacancyPercChanged(tcn);
			}
			else if (tb.Name == _FA_A_OtherIncome.Name || tb.Name == _FA_P_OtherIncome.Name || tb.Name == _FA_S_OtherIncome.Name)
			{
				OtherIncomeChanged(tcn);
			}
			else if (tb.Name == _FA_A_OperatingExpenses.Name || tb.Name == _FA_P_OperatingExpenses.Name || tb.Name == _FA_S_OperatingExpenses.Name)
			{
				// NO OP FOR NOW!?
			}
			else if (tb.Name == _FA_A_OperatingExpensesPerc.Name || tb.Name == _FA_P_OperatingExpensesPerc.Name || tb.Name == _FA_S_OperatingExpensesPerc.Name)
			{
				OperatingExpensesPercChanged(tcn);
			}
			else if (tb.Name == _FA_A_CapRate.Name || tb.Name == _FA_P_CapRate.Name || tb.Name == _FA_S_CapRate.Name)
			{
				CapRateChanged(tcn);
			}
			else if (tb.Name == _FA_A_DebtService.Name || tb.Name == _FA_P_DebtService.Name || tb.Name == _FA_S_DebtService.Name)
			{
				DebtServiceChanged(tcn);
			}
			else if (tb.Name == _FA_A_Comment.Name || tb.Name == _FA_S_Comment.Name || tb.Name == _FA_P_Comment.Name)
			{
				CommentChanged(tcn);
			}
		}

		void CommentChanged(TabColumnName tcn)
		{
			switch (tcn)
			{
				case TabColumnName.Actual:
					FinancialInfoObject.Instance.Data[0].CommentsProperty = _FA_A_Comment.Text;
					break;

				case TabColumnName.Proforma:
					FinancialInfoObject.Instance.Data[1].CommentsProperty = _FA_P_Comment.Text;
					break;

				case TabColumnName.Scenario:
					FinancialInfoObject.Instance.Data[2].CommentsProperty = _FA_S_Comment.Text;
					break;											   
			}
		}

		void PriceValueChanged(TabColumnName cn)
		{
			int dataIdx = 0;
			switch (cn)
			{
				case TabColumnName.Actual:
					try
					{
						decimal dTemp = Convert.ToDecimal(_FA_A_Price.Text);
						FinancialInfoObject.Instance.Data[0].PriceProperty = _FA_A_Price.Text = dTemp.ToString(CurrFormatter);
					}
					catch 
					{
						FinancialInfoObject.Instance.Data[0].PriceProperty = _FA_A_Price.Text;
					}
					dataIdx = 0;
					break;

				case TabColumnName.Proforma:
					try
					{
						decimal dTemp = Convert.ToDecimal(_FA_P_Price.Text);
						FinancialInfoObject.Instance.Data[1].PriceProperty = _FA_P_Price.Text = dTemp.ToString(CurrFormatter);
					}
					catch 
					{
						FinancialInfoObject.Instance.Data[1].PriceProperty = _FA_P_Price.Text;
					}
					
					dataIdx = 1;
					break;

				case TabColumnName.Scenario:
					try
					{
						decimal dTemp = Convert.ToDecimal(_FA_S_Price.Text);
						FinancialInfoObject.Instance.Data[2].PriceProperty = _FA_S_Price.Text = dTemp.ToString(CurrFormatter);
					}
					catch 
					{
						FinancialInfoObject.Instance.Data[2].PriceProperty = _FA_S_Price.Text;
					}					
					dataIdx = 2;
					break;
			}

			//CapRate (%): %=NOI/Price
			try { FinancialInfoObject.Instance.Data[dataIdx].CapRateProperty = (FinancialInfoObject.Instance.Data[dataIdx].NetOperatingIncomeValueProperty * 100/ FinancialInfoObject.Instance.Data[dataIdx].PriceValueProperty).ToString(CurrFormatter);}
			catch { FinancialInfoObject.Instance.Data[dataIdx].CapRateProperty = "NaN"; }

			switch (cn)
			{
				case TabColumnName.Actual:
					try
					{
						decimal dTemp = Convert.ToDecimal(_FA_A_CapRate.Text);
						_FA_A_CapRate.Text = FinancialInfoObject.Instance.Data[dataIdx].CapRateProperty = dTemp.ToString("0.00");
					}
					catch
					{
						_FA_A_CapRate.Text = FinancialInfoObject.Instance.Data[dataIdx].CapRateProperty;
					}
					break;

				case TabColumnName.Proforma:
					try
					{
						decimal dTemp = Convert.ToDecimal(_FA_P_CapRate.Text);
						_FA_P_CapRate.Text = FinancialInfoObject.Instance.Data[dataIdx].CapRateProperty = dTemp.ToString("0.00");
					}
					catch
					{
						_FA_P_CapRate.Text = FinancialInfoObject.Instance.Data[dataIdx].CapRateProperty;
					}					
					break;

				case TabColumnName.Scenario:
					try
					{
						decimal dTemp = Convert.ToDecimal(_FA_S_CapRate.Text);
						_FA_S_CapRate.Text = FinancialInfoObject.Instance.Data[dataIdx].CapRateProperty = dTemp.ToString("0.00");
					}
					catch
					{
						_FA_S_CapRate.Text = FinancialInfoObject.Instance.Data[dataIdx].CapRateProperty;
					}					
					break;
			}
		}
		
		/// <summary>
		/// GSI CHANGE EFFECTS: Vacancy($), GOI, Operating Expenses($), NOI, CapRate, DCR, CashFlow, CashOnCashReturn
		/// </summary>
		void GSIChanged(TabColumnName cn)
		{
			int dataIdx = 0;
			switch (cn)
			{
				case TabColumnName.Actual:
					try
					{
						decimal dTemp = Convert.ToDecimal(_FA_A_GSIAnnual.Text);
						FinancialInfoObject.Instance.Data[0].GsiAnnualProperty = _FA_A_GSIAnnual.Text = dTemp.ToString(CurrFormatter);
					}
					catch
					{
						FinancialInfoObject.Instance.Data[0].GsiAnnualProperty = _FA_A_GSIAnnual.Text;
					}
					dataIdx = 0;
					break;

				case TabColumnName.Proforma:
					try
					{
						decimal dTemp = Convert.ToDecimal(_FA_P_GSIAnnual.Text);
						FinancialInfoObject.Instance.Data[1].GsiAnnualProperty = _FA_P_GSIAnnual.Text = dTemp.ToString(CurrFormatter);
					}
					catch
					{
						FinancialInfoObject.Instance.Data[1].GsiAnnualProperty = _FA_P_GSIAnnual.Text;
					}					
					dataIdx = 1;
					break;

				case TabColumnName.Scenario:
					try
					{
						decimal dTemp = Convert.ToDecimal(_FA_S_GSIAnnual.Text);
						FinancialInfoObject.Instance.Data[2].GsiAnnualProperty = _FA_S_GSIAnnual.Text = dTemp.ToString(CurrFormatter);
					}
					catch
					{
						FinancialInfoObject.Instance.Data[2].GsiAnnualProperty = _FA_S_GSIAnnual.Text;
					}									
					dataIdx = 2;
					break;
			}

			//VACNANCY: $=%*GSI
			FinancialInfoObject.Instance.Data[dataIdx].VacancyCreditLossProperty = (FinancialInfoObject.Instance.Data[dataIdx].GsiAnnualValueProperty * FinancialInfoObject.Instance.Data[dataIdx].VacancyCreditLossPercValueProperty / 100).ToString(CurrFormatter); 
			
			//GOI: $=GSI-Vac+Other
			FinancialInfoObject.Instance.Data[dataIdx].GoiAnnualProperty = (FinancialInfoObject.Instance.Data[dataIdx].GsiAnnualValueProperty - FinancialInfoObject.Instance.Data[dataIdx].VacancyCreditLossValueProperty + FinancialInfoObject.Instance.Data[dataIdx].OtherIncomeValueProperty).ToString(CurrFormatter);

			//OPERATING EXPENSES: $=%*GSI
			FinancialInfoObject.Instance.Data[dataIdx].OperatingExpensesProperty = (FinancialInfoObject.Instance.Data[dataIdx].OperatingExpensesPercValueProperty / 100 * FinancialInfoObject.Instance.Data[dataIdx].GsiAnnualValueProperty).ToString(CurrFormatter);

			//NOI (NetOperatingIncome): $=GOI-OE
			FinancialInfoObject.Instance.Data[dataIdx].NetOperatingIncomeProperty = (FinancialInfoObject.Instance.Data[dataIdx].GoiAnnualValueProperty - FinancialInfoObject.Instance.Data[dataIdx].OperatingExpensesValueProperty).ToString(CurrFormatter);

			//CapRate: %=NOI/Price
			try { FinancialInfoObject.Instance.Data[dataIdx].CapRateProperty = (FinancialInfoObject.Instance.Data[dataIdx].NetOperatingIncomeValueProperty / FinancialInfoObject.Instance.Data[dataIdx].PriceValueProperty * 100).ToString(CurrFormatter); }
			catch { FinancialInfoObject.Instance.Data[dataIdx].CapRateProperty = "NaN"; }

			//DCR: =NOI/Debt Service
			try { FinancialInfoObject.Instance.Data[dataIdx].DebtCoverageRatioProperty = (FinancialInfoObject.Instance.Data[dataIdx].NetOperatingIncomeValueProperty / FinancialInfoObject.Instance.Data[dataIdx].DebtServiceValueProperty).ToString(CurrFormatter); }
			catch {FinancialInfoObject.Instance.Data[dataIdx].DebtCoverageRatioProperty = "NaN";}

			//CashFlow: $=NOI-Debt Service
			FinancialInfoObject.Instance.Data[dataIdx].CashFlowPreTaxProperty = (FinancialInfoObject.Instance.Data[dataIdx].NetOperatingIncomeValueProperty - FinancialInfoObject.Instance.Data[dataIdx].DebtServiceValueProperty).ToString(CurrFormatter);

			//CashOnCashReturn: =Cash Flow/Down Payment
			try { FinancialInfoObject.Instance.Data[dataIdx].CashOnCashReturnProperty = (FinancialInfoObject.Instance.Data[dataIdx].CashFlowPreTaxValueProperty / LoansObject.Instance.DownPaymentValue * 100).ToString(CurrFormatter); }
			catch {FinancialInfoObject.Instance.Data[dataIdx].CashOnCashReturnProperty = "NaN";}

			switch (cn)
			{
				case TabColumnName.Actual:
					_FA_A_VacancyCreditLoss.Text = FinancialInfoObject.Instance.Data[dataIdx].VacancyCreditLossProperty;
					_FA_A_GOIAnnual.Text = FinancialInfoObject.Instance.Data[dataIdx].GoiAnnualProperty;
					_FA_A_OperatingExpenses.Text = FinancialInfoObject.Instance.Data[dataIdx].OperatingExpensesProperty;
					_FA_A_NetOperatingIncome.Text = FinancialInfoObject.Instance.Data[dataIdx].NetOperatingIncomeProperty;
					_FA_A_CapRate.Text = FinancialInfoObject.Instance.Data[dataIdx].CapRateProperty;
					_FA_A_DebtCoverageRatio.Text = FinancialInfoObject.Instance.Data[dataIdx].DebtCoverageRatioProperty;
					_FA_A_CashFlow.Text = FinancialInfoObject.Instance.Data[dataIdx].CashFlowPreTaxProperty;
					_FA_A_CashOnCashReturn.Text = FinancialInfoObject.Instance.Data[dataIdx].CashOnCashReturnProperty;
					break;

				case TabColumnName.Proforma:
					_FA_P_VacancyCreditLoss.Text = FinancialInfoObject.Instance.Data[dataIdx].VacancyCreditLossProperty;
					_FA_P_GOIAnnual.Text = FinancialInfoObject.Instance.Data[dataIdx].GoiAnnualProperty;
					_FA_P_OperatingExpenses.Text = FinancialInfoObject.Instance.Data[dataIdx].OperatingExpensesProperty;
					_FA_P_NetOperatingIncome.Text = FinancialInfoObject.Instance.Data[dataIdx].NetOperatingIncomeProperty;
					_FA_P_CapRate.Text = FinancialInfoObject.Instance.Data[dataIdx].CapRateProperty;
					_FA_P_DebtCoverageRatio.Text = FinancialInfoObject.Instance.Data[dataIdx].DebtCoverageRatioProperty;
					_FA_P_CashFlow.Text = FinancialInfoObject.Instance.Data[dataIdx].CashFlowPreTaxProperty;
					_FA_P_CashOnCashReturn.Text = FinancialInfoObject.Instance.Data[dataIdx].CashOnCashReturnProperty;
					break;

				case TabColumnName.Scenario:
					_FA_S_VacancyCreditLoss.Text = FinancialInfoObject.Instance.Data[dataIdx].VacancyCreditLossProperty;
					_FA_S_GOIAnnual.Text = FinancialInfoObject.Instance.Data[dataIdx].GoiAnnualProperty;
					_FA_S_OperatingExpenses.Text = FinancialInfoObject.Instance.Data[dataIdx].OperatingExpensesProperty;
					_FA_S_NetOperatingIncome.Text = FinancialInfoObject.Instance.Data[dataIdx].NetOperatingIncomeProperty;
					_FA_S_CapRate.Text = FinancialInfoObject.Instance.Data[dataIdx].CapRateProperty;
					_FA_S_DebtCoverageRatio.Text = FinancialInfoObject.Instance.Data[dataIdx].DebtCoverageRatioProperty;
					_FA_S_CashFlow.Text = FinancialInfoObject.Instance.Data[dataIdx].CashFlowPreTaxProperty;
					_FA_S_CashOnCashReturn.Text = FinancialInfoObject.Instance.Data[dataIdx].CashOnCashReturnProperty;
					break;
			}

		}

		/// <summary>
		/// VACANCY ($) CHANGE EFFECTS: VACANCY (%) + TRIGGER VACANCY(%) DEPENDENCIES
		/// </summary>		
		void VacancyChanged(TabColumnName cn)
		{
			int dataIdx = 0;
			switch (cn)
			{
				case TabColumnName.Actual:
					try
					{
						decimal dTemp = Convert.ToDecimal(_FA_A_VacancyCreditLoss.Text);
						FinancialInfoObject.Instance.Data[0].VacancyCreditLossProperty = _FA_A_VacancyCreditLoss.Text = dTemp.ToString(CurrFormatter);
					}
					catch 
					{
						FinancialInfoObject.Instance.Data[0].VacancyCreditLossProperty = _FA_A_VacancyCreditLoss.Text;
					}
					dataIdx = 0;
					break;

				case TabColumnName.Proforma:
					try
					{
						decimal dTemp = Convert.ToDecimal(_FA_P_VacancyCreditLoss.Text);
						FinancialInfoObject.Instance.Data[1].VacancyCreditLossProperty = _FA_P_VacancyCreditLoss.Text = dTemp.ToString(CurrFormatter);
					}
					catch 
					{
						FinancialInfoObject.Instance.Data[1].VacancyCreditLossProperty = _FA_P_VacancyCreditLoss.Text;
					}					
					dataIdx = 1;
					break;

				case TabColumnName.Scenario:
					try
					{
						decimal dTemp = Convert.ToDecimal(_FA_S_VacancyCreditLoss.Text);
						FinancialInfoObject.Instance.Data[2].VacancyCreditLossProperty = _FA_S_VacancyCreditLoss.Text = dTemp.ToString(CurrFormatter);
					}
					catch 
					{
						FinancialInfoObject.Instance.Data[2].VacancyCreditLossProperty = _FA_S_VacancyCreditLoss.Text;
					}						
					dataIdx = 2;
					break;
			}

			//VACNANCY: %=$ / GSI
			try { FinancialInfoObject.Instance.Data[dataIdx].VacancyCreditLossPercProperty = ((FinancialInfoObject.Instance.Data[dataIdx].VacancyCreditLossValueProperty / FinancialInfoObject.Instance.Data[dataIdx].GsiAnnualValueProperty ) * 100).ToString(CurrFormatter); }
			catch {FinancialInfoObject.Instance.Data[dataIdx].VacancyCreditLossPercProperty = "0";}

			switch (cn)
			{
				case TabColumnName.Actual:
					_FA_A_VacancyCreditLossPerc.Text = FinancialInfoObject.Instance.Data[dataIdx].VacancyCreditLossPercProperty;					
					break;

				case TabColumnName.Proforma:
					_FA_P_VacancyCreditLossPerc.Text = FinancialInfoObject.Instance.Data[dataIdx].VacancyCreditLossPercProperty;					
					break;

				case TabColumnName.Scenario:
					_FA_S_VacancyCreditLossPerc.Text = FinancialInfoObject.Instance.Data[dataIdx].VacancyCreditLossPercProperty;					
					break;
			}


			VacancyPercChanged(cn);
		}
		
		/// <summary>
		/// VACANCY(%) CHANGE EFFECTS: VACANCY($), GOI, NOI, CapRate, DCR, CashFlow, CashOnCashReturn
		/// </summary>
		void VacancyPercChanged(TabColumnName cn)
		{
			int dataIdx = 0;
			switch (cn)
			{
				case TabColumnName.Actual:
					try
					{
						decimal dTemp = Convert.ToDecimal(_FA_A_VacancyCreditLossPerc.Text);
						FinancialInfoObject.Instance.Data[0].VacancyCreditLossPercProperty = _FA_A_VacancyCreditLossPerc.Text = dTemp.ToString(CurrFormatter);
					}
					catch 
					{
						FinancialInfoObject.Instance.Data[0].VacancyCreditLossPercProperty = _FA_A_VacancyCreditLossPerc.Text;
					}
					dataIdx = 0;
					break;

				case TabColumnName.Proforma:
					try
					{
						decimal dTemp = Convert.ToDecimal(_FA_P_VacancyCreditLossPerc.Text);
						FinancialInfoObject.Instance.Data[1].VacancyCreditLossPercProperty = _FA_P_VacancyCreditLossPerc.Text = dTemp.ToString(CurrFormatter);
					}
					catch 
					{
						FinancialInfoObject.Instance.Data[1].VacancyCreditLossPercProperty = _FA_P_VacancyCreditLossPerc.Text;
					}					
					dataIdx = 1;
					break;

				case TabColumnName.Scenario:
					try
					{
						decimal dTemp = Convert.ToDecimal(_FA_S_VacancyCreditLossPerc.Text);
						FinancialInfoObject.Instance.Data[2].VacancyCreditLossPercProperty = _FA_S_VacancyCreditLossPerc.Text = dTemp.ToString(CurrFormatter);
					}
					catch 
					{
						FinancialInfoObject.Instance.Data[2].VacancyCreditLossPercProperty = _FA_S_VacancyCreditLossPerc.Text;
					}						
					dataIdx = 2;
					break;
			}

			//VACNANCY: $=%*GSI
			FinancialInfoObject.Instance.Data[dataIdx].VacancyCreditLossProperty = (FinancialInfoObject.Instance.Data[dataIdx].GsiAnnualValueProperty * FinancialInfoObject.Instance.Data[dataIdx].VacancyCreditLossPercValueProperty / 100).ToString(CurrFormatter);

			//GOI: $=GSI-Vac+Other
			FinancialInfoObject.Instance.Data[dataIdx].GoiAnnualProperty = (FinancialInfoObject.Instance.Data[dataIdx].GsiAnnualValueProperty - FinancialInfoObject.Instance.Data[dataIdx].VacancyCreditLossValueProperty + FinancialInfoObject.Instance.Data[dataIdx].OtherIncomeValueProperty).ToString(CurrFormatter);			

			//NOI (NetOperatingIncome): $=GOI-OE
			FinancialInfoObject.Instance.Data[dataIdx].NetOperatingIncomeProperty = (FinancialInfoObject.Instance.Data[dataIdx].GoiAnnualValueProperty - FinancialInfoObject.Instance.Data[dataIdx].OperatingExpensesValueProperty).ToString(CurrFormatter);

			//CapRate: %=NOI/Price
			try {FinancialInfoObject.Instance.Data[dataIdx].CapRateProperty = (FinancialInfoObject.Instance.Data[dataIdx].NetOperatingIncomeValueProperty / FinancialInfoObject.Instance.Data[dataIdx].PriceValueProperty * 100).ToString(CurrFormatter);}
			catch {FinancialInfoObject.Instance.Data[dataIdx].CapRateProperty = "NaN";}

			//DCR: =NOI/Debt Service
			try {FinancialInfoObject.Instance.Data[dataIdx].DebtCoverageRatioProperty = (FinancialInfoObject.Instance.Data[dataIdx].NetOperatingIncomeValueProperty / FinancialInfoObject.Instance.Data[dataIdx].DebtServiceValueProperty).ToString(CurrFormatter);}
			catch {FinancialInfoObject.Instance.Data[dataIdx].DebtCoverageRatioProperty = "NaN";}

			//CashFlow: $=NOI-Debt Service
			FinancialInfoObject.Instance.Data[dataIdx].CashFlowPreTaxProperty = (FinancialInfoObject.Instance.Data[dataIdx].NetOperatingIncomeValueProperty - FinancialInfoObject.Instance.Data[dataIdx].DebtServiceValueProperty).ToString(CurrFormatter);

			//CashOnCashReturn: =Cash Flow/Down Payment
			try {FinancialInfoObject.Instance.Data[dataIdx].CashOnCashReturnProperty = (FinancialInfoObject.Instance.Data[dataIdx].CashFlowPreTaxValueProperty / LoansObject.Instance.DownPaymentValue * 100).ToString(CurrFormatter);}
			catch {FinancialInfoObject.Instance.Data[dataIdx].CashOnCashReturnProperty = "NaN";}

			switch (cn)
			{
				case TabColumnName.Actual:
					_FA_A_VacancyCreditLoss.Text = FinancialInfoObject.Instance.Data[dataIdx].VacancyCreditLossProperty;
					_FA_A_GOIAnnual.Text = FinancialInfoObject.Instance.Data[dataIdx].GoiAnnualProperty;					
					_FA_A_NetOperatingIncome.Text = FinancialInfoObject.Instance.Data[dataIdx].NetOperatingIncomeProperty;
					_FA_A_CapRate.Text = FinancialInfoObject.Instance.Data[dataIdx].CapRateProperty;
					_FA_A_DebtCoverageRatio.Text = FinancialInfoObject.Instance.Data[dataIdx].DebtCoverageRatioProperty;
					_FA_A_CashFlow.Text = FinancialInfoObject.Instance.Data[dataIdx].CashFlowPreTaxProperty;
					_FA_A_CashOnCashReturn.Text = FinancialInfoObject.Instance.Data[dataIdx].CashOnCashReturnProperty;
					break;

				case TabColumnName.Proforma:
					_FA_P_VacancyCreditLoss.Text = FinancialInfoObject.Instance.Data[dataIdx].VacancyCreditLossProperty;
					_FA_P_GOIAnnual.Text = FinancialInfoObject.Instance.Data[dataIdx].GoiAnnualProperty;					
					_FA_P_NetOperatingIncome.Text = FinancialInfoObject.Instance.Data[dataIdx].NetOperatingIncomeProperty;
					_FA_P_CapRate.Text = FinancialInfoObject.Instance.Data[dataIdx].CapRateProperty;
					_FA_P_DebtCoverageRatio.Text = FinancialInfoObject.Instance.Data[dataIdx].DebtCoverageRatioProperty;
					_FA_P_CashFlow.Text = FinancialInfoObject.Instance.Data[dataIdx].CashFlowPreTaxProperty;
					_FA_P_CashOnCashReturn.Text = FinancialInfoObject.Instance.Data[dataIdx].CashOnCashReturnProperty;
					break;

				case TabColumnName.Scenario:
					_FA_S_VacancyCreditLoss.Text = FinancialInfoObject.Instance.Data[dataIdx].VacancyCreditLossProperty;
					_FA_S_GOIAnnual.Text = FinancialInfoObject.Instance.Data[dataIdx].GoiAnnualProperty;					
					_FA_S_NetOperatingIncome.Text = FinancialInfoObject.Instance.Data[dataIdx].NetOperatingIncomeProperty;
					_FA_S_CapRate.Text = FinancialInfoObject.Instance.Data[dataIdx].CapRateProperty;
					_FA_S_DebtCoverageRatio.Text = FinancialInfoObject.Instance.Data[dataIdx].DebtCoverageRatioProperty;
					_FA_S_CashFlow.Text = FinancialInfoObject.Instance.Data[dataIdx].CashFlowPreTaxProperty;
					_FA_S_CashOnCashReturn.Text = FinancialInfoObject.Instance.Data[dataIdx].CashOnCashReturnProperty;
					break;
			}
		}

		
		/// <summary>
		/// OTHER INCOME CHANGE EFFECTS: GOI, NOI, CapRate, DCR, CashFlow, CashOnCashReturn
		/// </summary>
		void OtherIncomeChanged(TabColumnName cn)
		{
			int dataIdx = 0;
			switch (cn)
			{
				case TabColumnName.Actual:
					try
					{
						decimal dTemp = Convert.ToDecimal(_FA_A_OtherIncome.Text);
						FinancialInfoObject.Instance.Data[0].OtherIncomeProperty = _FA_A_OtherIncome.Text = dTemp.ToString(CurrFormatter);
					}
					catch 
					{
						FinancialInfoObject.Instance.Data[0].OtherIncomeProperty = _FA_A_OtherIncome.Text;
					}
					dataIdx = 0;
					break;

				case TabColumnName.Proforma:
					try
					{
						decimal dTemp = Convert.ToDecimal(_FA_P_OtherIncome.Text);
						FinancialInfoObject.Instance.Data[1].OtherIncomeProperty = _FA_P_OtherIncome.Text = dTemp.ToString(CurrFormatter);
					}
					catch 
					{
						FinancialInfoObject.Instance.Data[1].OtherIncomeProperty = _FA_P_OtherIncome.Text;
					}					
					dataIdx = 1;
					break;

				case TabColumnName.Scenario:
					try
					{
						decimal dTemp = Convert.ToDecimal(_FA_S_OtherIncome.Text);
						FinancialInfoObject.Instance.Data[2].OtherIncomeProperty = _FA_S_OtherIncome.Text = dTemp.ToString(CurrFormatter);
					}
					catch 
					{
						FinancialInfoObject.Instance.Data[2].OtherIncomeProperty = _FA_S_OtherIncome.Text;
					}							
					dataIdx = 2;
					break;
			}			

			//GOI: $=GSI-Vac+Other
			FinancialInfoObject.Instance.Data[dataIdx].GoiAnnualProperty = (FinancialInfoObject.Instance.Data[dataIdx].GsiAnnualValueProperty - FinancialInfoObject.Instance.Data[dataIdx].VacancyCreditLossValueProperty + FinancialInfoObject.Instance.Data[dataIdx].OtherIncomeValueProperty).ToString(CurrFormatter);			

			//NOI (NetOperatingIncome): $=GOI-OE
			FinancialInfoObject.Instance.Data[dataIdx].NetOperatingIncomeProperty = (FinancialInfoObject.Instance.Data[dataIdx].GoiAnnualValueProperty - FinancialInfoObject.Instance.Data[dataIdx].OperatingExpensesValueProperty).ToString(CurrFormatter);

			//CapRate: %=NOI/Price
			try { FinancialInfoObject.Instance.Data[dataIdx].CapRateProperty = (FinancialInfoObject.Instance.Data[dataIdx].NetOperatingIncomeValueProperty / FinancialInfoObject.Instance.Data[dataIdx].PriceValueProperty * 100).ToString(CurrFormatter); }
			catch {FinancialInfoObject.Instance.Data[dataIdx].CapRateProperty = "NaN";}

			//DCR: =NOI/Debt Service
			try { FinancialInfoObject.Instance.Data[dataIdx].DebtCoverageRatioProperty = (FinancialInfoObject.Instance.Data[dataIdx].NetOperatingIncomeValueProperty / FinancialInfoObject.Instance.Data[dataIdx].DebtServiceValueProperty).ToString(CurrFormatter);}
			catch {FinancialInfoObject.Instance.Data[dataIdx].DebtCoverageRatioProperty = "NaN";}

			//CashFlow: $=NOI-Debt Service
			FinancialInfoObject.Instance.Data[dataIdx].CashFlowPreTaxProperty = (FinancialInfoObject.Instance.Data[dataIdx].NetOperatingIncomeValueProperty - FinancialInfoObject.Instance.Data[dataIdx].DebtServiceValueProperty).ToString(CurrFormatter);

			//CashOnCashReturn: =Cash Flow/Down Payment
			try { FinancialInfoObject.Instance.Data[dataIdx].CashOnCashReturnProperty = (FinancialInfoObject.Instance.Data[dataIdx].CashFlowPreTaxValueProperty / LoansObject.Instance.DownPaymentValue * 100).ToString(CurrFormatter);}
			catch {FinancialInfoObject.Instance.Data[dataIdx].CashOnCashReturnProperty = "NaN";}
	
			switch (cn)
			{
				case TabColumnName.Actual:					
					_FA_A_GOIAnnual.Text = FinancialInfoObject.Instance.Data[dataIdx].GoiAnnualProperty;					
					_FA_A_NetOperatingIncome.Text = FinancialInfoObject.Instance.Data[dataIdx].NetOperatingIncomeProperty;
					_FA_A_CapRate.Text = FinancialInfoObject.Instance.Data[dataIdx].CapRateProperty;
					_FA_A_DebtCoverageRatio.Text = FinancialInfoObject.Instance.Data[dataIdx].DebtCoverageRatioProperty;
					_FA_A_CashFlow.Text = FinancialInfoObject.Instance.Data[dataIdx].CashFlowPreTaxProperty;
					_FA_A_CashOnCashReturn.Text = FinancialInfoObject.Instance.Data[dataIdx].CashOnCashReturnProperty;
					break;

				case TabColumnName.Proforma:					
					_FA_P_GOIAnnual.Text = FinancialInfoObject.Instance.Data[dataIdx].GoiAnnualProperty;					
					_FA_P_NetOperatingIncome.Text = FinancialInfoObject.Instance.Data[dataIdx].NetOperatingIncomeProperty;
					_FA_P_CapRate.Text = FinancialInfoObject.Instance.Data[dataIdx].CapRateProperty;
					_FA_P_DebtCoverageRatio.Text = FinancialInfoObject.Instance.Data[dataIdx].DebtCoverageRatioProperty;
					_FA_P_CashFlow.Text = FinancialInfoObject.Instance.Data[dataIdx].CashFlowPreTaxProperty;
					_FA_P_CashOnCashReturn.Text = FinancialInfoObject.Instance.Data[dataIdx].CashOnCashReturnProperty;
					break;

				case TabColumnName.Scenario:					
					_FA_S_GOIAnnual.Text = FinancialInfoObject.Instance.Data[dataIdx].GoiAnnualProperty;					
					_FA_S_NetOperatingIncome.Text = FinancialInfoObject.Instance.Data[dataIdx].NetOperatingIncomeProperty;
					_FA_S_CapRate.Text = FinancialInfoObject.Instance.Data[dataIdx].CapRateProperty;
					_FA_S_DebtCoverageRatio.Text = FinancialInfoObject.Instance.Data[dataIdx].DebtCoverageRatioProperty;
					_FA_S_CashFlow.Text = FinancialInfoObject.Instance.Data[dataIdx].CashFlowPreTaxProperty;
					_FA_S_CashOnCashReturn.Text = FinancialInfoObject.Instance.Data[dataIdx].CashOnCashReturnProperty;
					break;
			}
		}

		
		/// <summary>
		/// OPERATING EXPENSES PERC CHANGE EFFECTS: Operating Expenses($), NOI, CapRate, DCR, CashFlow, CashOnCashReturn 
		/// </summary>
		void OperatingExpensesPercChanged(TabColumnName cn)
		{
			int dataIdx = 0;
			switch (cn)
			{
				case TabColumnName.Actual:
					try
					{
						decimal dTemp = Convert.ToDecimal(_FA_A_OperatingExpensesPerc.Text);
						FinancialInfoObject.Instance.Data[0].OperatingExpensesPercProperty = _FA_A_OperatingExpensesPerc.Text = dTemp.ToString(CurrFormatter);
					}
					catch
					{
						FinancialInfoObject.Instance.Data[0].OperatingExpensesPercProperty = _FA_A_OperatingExpensesPerc.Text;
					}
					dataIdx = 0;
					break;

				case TabColumnName.Proforma:
					try
					{
						decimal dTemp = Convert.ToDecimal(_FA_P_OperatingExpensesPerc.Text);
						FinancialInfoObject.Instance.Data[1].OperatingExpensesPercProperty = _FA_P_OperatingExpensesPerc.Text = dTemp.ToString(CurrFormatter);
					}
					catch
					{
						FinancialInfoObject.Instance.Data[1].OperatingExpensesPercProperty = _FA_P_OperatingExpensesPerc.Text;
					}					
					dataIdx = 1;
					break;

				case TabColumnName.Scenario:
					try
					{
						decimal dTemp = Convert.ToDecimal(_FA_S_OperatingExpensesPerc.Text);
						FinancialInfoObject.Instance.Data[2].OperatingExpensesPercProperty = _FA_S_OperatingExpensesPerc.Text = dTemp.ToString(CurrFormatter);
					}
					catch
					{
						FinancialInfoObject.Instance.Data[2].OperatingExpensesPercProperty = _FA_S_OperatingExpensesPerc.Text;
					}								
					dataIdx = 2;
					break;
			}			

			//OPERATING EXPENSES: $=%*GSI
			FinancialInfoObject.Instance.Data[dataIdx].OperatingExpensesProperty = (FinancialInfoObject.Instance.Data[dataIdx].OperatingExpensesPercValueProperty / 100 * FinancialInfoObject.Instance.Data[dataIdx].GsiAnnualValueProperty).ToString(CurrFormatter);

			//NOI (NetOperatingIncome): $=GOI-OE
			FinancialInfoObject.Instance.Data[dataIdx].NetOperatingIncomeProperty = (FinancialInfoObject.Instance.Data[dataIdx].GoiAnnualValueProperty - FinancialInfoObject.Instance.Data[dataIdx].OperatingExpensesValueProperty).ToString(CurrFormatter);

			//CapRate: %=NOI/Price
			try { FinancialInfoObject.Instance.Data[dataIdx].CapRateProperty = (FinancialInfoObject.Instance.Data[dataIdx].NetOperatingIncomeValueProperty / FinancialInfoObject.Instance.Data[dataIdx].PriceValueProperty * 100).ToString(CurrFormatter); }
			catch {FinancialInfoObject.Instance.Data[dataIdx].CapRateProperty = "NaN";}

			//DCR: =NOI/Debt Service
			try {FinancialInfoObject.Instance.Data[dataIdx].DebtCoverageRatioProperty = (FinancialInfoObject.Instance.Data[dataIdx].NetOperatingIncomeValueProperty / FinancialInfoObject.Instance.Data[dataIdx].DebtServiceValueProperty).ToString(CurrFormatter); }
			catch {FinancialInfoObject.Instance.Data[dataIdx].DebtCoverageRatioProperty = "NaN";}

			//CashFlow: $=NOI-Debt Service
			FinancialInfoObject.Instance.Data[dataIdx].CashFlowPreTaxProperty = (FinancialInfoObject.Instance.Data[dataIdx].NetOperatingIncomeValueProperty - FinancialInfoObject.Instance.Data[dataIdx].DebtServiceValueProperty).ToString(CurrFormatter);

			//CashOnCashReturn: =Cash Flow/Down Payment
			try { FinancialInfoObject.Instance.Data[dataIdx].CashOnCashReturnProperty = (FinancialInfoObject.Instance.Data[dataIdx].CashFlowPreTaxValueProperty / LoansObject.Instance.DownPaymentValue * 100).ToString(CurrFormatter); }
			catch {FinancialInfoObject.Instance.Data[dataIdx].CashOnCashReturnProperty = "NaN";}

	
			switch (cn)
			{
				case TabColumnName.Actual:					
					_FA_A_OperatingExpenses.Text = FinancialInfoObject.Instance.Data[dataIdx].OperatingExpensesProperty;
					_FA_A_NetOperatingIncome.Text = FinancialInfoObject.Instance.Data[dataIdx].NetOperatingIncomeProperty;
					_FA_A_CapRate.Text = FinancialInfoObject.Instance.Data[dataIdx].CapRateProperty;
					_FA_A_DebtCoverageRatio.Text = FinancialInfoObject.Instance.Data[dataIdx].DebtCoverageRatioProperty;
					_FA_A_CashFlow.Text = FinancialInfoObject.Instance.Data[dataIdx].CashFlowPreTaxProperty;
					_FA_A_CashOnCashReturn.Text = FinancialInfoObject.Instance.Data[dataIdx].CashOnCashReturnProperty;
					break;

				case TabColumnName.Proforma:					
					_FA_P_OperatingExpenses.Text = FinancialInfoObject.Instance.Data[dataIdx].OperatingExpensesProperty;
					_FA_P_NetOperatingIncome.Text = FinancialInfoObject.Instance.Data[dataIdx].NetOperatingIncomeProperty;
					_FA_P_CapRate.Text = FinancialInfoObject.Instance.Data[dataIdx].CapRateProperty;
					_FA_P_DebtCoverageRatio.Text = FinancialInfoObject.Instance.Data[dataIdx].DebtCoverageRatioProperty;
					_FA_P_CashFlow.Text = FinancialInfoObject.Instance.Data[dataIdx].CashFlowPreTaxProperty;
					_FA_P_CashOnCashReturn.Text = FinancialInfoObject.Instance.Data[dataIdx].CashOnCashReturnProperty;
					break;

				case TabColumnName.Scenario:					
					_FA_S_OperatingExpenses.Text = FinancialInfoObject.Instance.Data[dataIdx].OperatingExpensesProperty;
					_FA_S_NetOperatingIncome.Text = FinancialInfoObject.Instance.Data[dataIdx].NetOperatingIncomeProperty;
					_FA_S_CapRate.Text = FinancialInfoObject.Instance.Data[dataIdx].CapRateProperty;
					_FA_S_DebtCoverageRatio.Text = FinancialInfoObject.Instance.Data[dataIdx].DebtCoverageRatioProperty;
					_FA_S_CashFlow.Text = FinancialInfoObject.Instance.Data[dataIdx].CashFlowPreTaxProperty;
					_FA_S_CashOnCashReturn.Text = FinancialInfoObject.Instance.Data[dataIdx].CashOnCashReturnProperty;
					break;
			}
		}

		
		/// <summary>
		/// CAP RATE CHANGE EFFECTS: Price/Value 
		/// </summary>
		void CapRateChanged(TabColumnName cn)
		{
			int dataIdx = 0;
			switch (cn)
			{
				case TabColumnName.Actual:
					try
					{
						decimal dTemp = Convert.ToDecimal(_FA_A_CapRate.Text);
						FinancialInfoObject.Instance.Data[0].CapRateProperty = _FA_A_CapRate.Text = dTemp.ToString(CurrFormatter);
					}
					catch 
					{
						FinancialInfoObject.Instance.Data[0].CapRateProperty = _FA_A_CapRate.Text;
					}
					dataIdx = 0;
					break;

				case TabColumnName.Proforma:
					try
					{
						decimal dTemp = Convert.ToDecimal(_FA_P_CapRate.Text);
						FinancialInfoObject.Instance.Data[1].CapRateProperty = _FA_P_CapRate.Text = dTemp.ToString(CurrFormatter);
					}
					catch 
					{
						FinancialInfoObject.Instance.Data[1].CapRateProperty = _FA_P_CapRate.Text;
					}					
					dataIdx = 1;
					break;

				case TabColumnName.Scenario:
					try
					{
						decimal dTemp = Convert.ToDecimal(_FA_S_CapRate.Text);
						FinancialInfoObject.Instance.Data[2].CapRateProperty = _FA_S_CapRate.Text = dTemp.ToString(CurrFormatter);
					}
					catch 
					{
						FinancialInfoObject.Instance.Data[2].CapRateProperty = _FA_S_CapRate.Text;
					}							
					dataIdx = 2;
					break;
			}
			
			//Price/Value: NOI/Cap Rate
			try { FinancialInfoObject.Instance.Data[dataIdx].PriceProperty = (FinancialInfoObject.Instance.Data[dataIdx].NetOperatingIncomeValueProperty  / (FinancialInfoObject.Instance.Data[dataIdx].CapRateValueProperty / 100)).ToString(CurrFormatter); }
			catch {FinancialInfoObject.Instance.Data[dataIdx].PriceProperty = "NaN";}

			switch (cn)
			{
				case TabColumnName.Actual:					
					_FA_A_Price.Text = FinancialInfoObject.Instance.Data[dataIdx].PriceValueProperty.ToString(CurrFormatter);//PriceProperty;
					break;

				case TabColumnName.Proforma:					
					_FA_P_Price.Text = FinancialInfoObject.Instance.Data[dataIdx].PriceValueProperty.ToString(CurrFormatter);//PriceProperty;
					break;

				case TabColumnName.Scenario:					
					_FA_S_Price.Text = FinancialInfoObject.Instance.Data[dataIdx].PriceValueProperty.ToString(CurrFormatter);//PriceProperty;
					break;
			}
		}

		//DEBT SERVICE CHANGE EFFECTS: DCR, CashFlow, CashOnCashReturn
		void DebtServiceChanged(TabColumnName cn)
		{
			int dataIdx = 0;
			switch (cn)
			{
				case TabColumnName.Actual:
					try
					{
						decimal dTemp = Convert.ToDecimal(_FA_A_DebtService.Text);
						FinancialInfoObject.Instance.Data[0].DebtServiceProperty = _FA_A_DebtService.Text = dTemp.ToString(CurrFormatter);
					}
					catch 
					{
						FinancialInfoObject.Instance.Data[0].DebtServiceProperty = _FA_A_DebtService.Text;
					}
					dataIdx = 0;
					break;

				case TabColumnName.Proforma:
					try
					{
						decimal dTemp = Convert.ToDecimal(_FA_P_DebtService.Text);
						FinancialInfoObject.Instance.Data[1].DebtServiceProperty = _FA_P_DebtService.Text = dTemp.ToString(CurrFormatter);
					}
					catch 
					{
						FinancialInfoObject.Instance.Data[1].DebtServiceProperty = _FA_P_DebtService.Text;
					}					
					dataIdx = 1;
					break;

				case TabColumnName.Scenario:
					try
					{
						decimal dTemp = Convert.ToDecimal(_FA_S_DebtService.Text);
						FinancialInfoObject.Instance.Data[2].DebtServiceProperty = _FA_S_DebtService.Text = dTemp.ToString(CurrFormatter);
					}
					catch 
					{
						FinancialInfoObject.Instance.Data[2].DebtServiceProperty = _FA_S_DebtService.Text;
					}					
					dataIdx = 2;
					break;
			}			

			//DCR: =NOI/Debt Service
			try { FinancialInfoObject.Instance.Data[dataIdx].DebtCoverageRatioProperty = (FinancialInfoObject.Instance.Data[dataIdx].NetOperatingIncomeValueProperty / FinancialInfoObject.Instance.Data[dataIdx].DebtServiceValueProperty).ToString(CurrFormatter); }
			catch {FinancialInfoObject.Instance.Data[dataIdx].DebtCoverageRatioProperty = "NaN";}

			//CashFlow: $=NOI-Debt Service
			FinancialInfoObject.Instance.Data[dataIdx].CashFlowPreTaxProperty = (FinancialInfoObject.Instance.Data[dataIdx].NetOperatingIncomeValueProperty - FinancialInfoObject.Instance.Data[dataIdx].DebtServiceValueProperty).ToString(CurrFormatter);

			//CashOnCashReturn: =Cash Flow/Down Payment
			try { FinancialInfoObject.Instance.Data[dataIdx].CashOnCashReturnProperty = (FinancialInfoObject.Instance.Data[dataIdx].CashFlowPreTaxValueProperty / LoansObject.Instance.DownPaymentValue * 100).ToString(CurrFormatter); }
			catch {FinancialInfoObject.Instance.Data[dataIdx].CashOnCashReturnProperty = "NaN";}
	
			switch (cn)
			{
				case TabColumnName.Actual:					
					_FA_A_DebtCoverageRatio.Text = FinancialInfoObject.Instance.Data[dataIdx].DebtCoverageRatioProperty;
					_FA_A_CashFlow.Text = FinancialInfoObject.Instance.Data[dataIdx].CashFlowPreTaxProperty;
					_FA_A_CashOnCashReturn.Text = FinancialInfoObject.Instance.Data[dataIdx].CashOnCashReturnProperty;
					break;

				case TabColumnName.Proforma:					
					_FA_P_DebtCoverageRatio.Text = FinancialInfoObject.Instance.Data[dataIdx].DebtCoverageRatioProperty;
					_FA_P_CashFlow.Text = FinancialInfoObject.Instance.Data[dataIdx].CashFlowPreTaxProperty;
					_FA_P_CashOnCashReturn.Text = FinancialInfoObject.Instance.Data[dataIdx].CashOnCashReturnProperty;
					break;

				case TabColumnName.Scenario:					
					_FA_S_DebtCoverageRatio.Text = FinancialInfoObject.Instance.Data[dataIdx].DebtCoverageRatioProperty;
					_FA_S_CashFlow.Text = FinancialInfoObject.Instance.Data[dataIdx].CashFlowPreTaxProperty;
					_FA_S_CashOnCashReturn.Text = FinancialInfoObject.Instance.Data[dataIdx].CashOnCashReturnProperty;
					break;
			}
		}
		#endregion

		#region FINANCIAL ANALYSIS - GRAPHS
		private void _FA_Graph1_Click(object sender, System.EventArgs e)
		{
			CapRateVsPriceValue graphX = new CapRateVsPriceValue();
			double delta = 0;

			try {delta = Convert.ToDouble(_FA_CR_Change_NUD.Value);}
			catch {delta = 0;}

			graphX.SetData((double)FinancialInfoObject.Instance.Data[0].CapRateValueProperty, delta, (double)FinancialInfoObject.Instance.Data[0].NetOperatingIncomeValueProperty);
			graphX.ShowDialog();
		}

		private void _FA_Graph2_Click(object sender, System.EventArgs e)
		{
			CashFlowVsCashOnCashReturn graphX = new CashFlowVsCashOnCashReturn();
			double delta = 0;

			try {delta = Convert.ToDouble(_FA_GSI_Range_NUD.Value); }
			catch {delta = 0;}

			graphX.SetDataForGSIRange((double)FinancialInfoObject.Instance.Data[0].GsiAnnualValueProperty, 
										(double)FinancialInfoObject.Instance.Data[0].VacancyCreditLossPercValueProperty, 
										(double)FinancialInfoObject.Instance.Data[0].OtherIncomeValueProperty, 
										(double)FinancialInfoObject.Instance.Data[0].OperatingExpensesPercValueProperty, 
										(double)FinancialInfoObject.Instance.Data[0].DebtServiceValueProperty, 
										(double)LoansObject.Instance.DownPaymentValue, 
										delta);

			graphX.ShowDialog();
		}

		private void _FA_Graph3_Click(object sender, System.EventArgs e)
		{
			CashFlowVsCashOnCashReturn graphX = new CashFlowVsCashOnCashReturn();
			double delta = 0;

			try {delta = Convert.ToDouble(_FA_OE_Range_NUD.Value); }
			catch {delta = 0;}

			graphX.SetDataForOERange((double)FinancialInfoObject.Instance.Data[0].GsiAnnualValueProperty, 
				(double)FinancialInfoObject.Instance.Data[0].VacancyCreditLossPercValueProperty, 
				(double)FinancialInfoObject.Instance.Data[0].OtherIncomeValueProperty,				
				(double)FinancialInfoObject.Instance.Data[0].OperatingExpensesPercValueProperty, 
				(double)FinancialInfoObject.Instance.Data[0].DebtServiceValueProperty, 
				(double)LoansObject.Instance.DownPaymentValue, 
				delta);

			graphX.ShowDialog();
		}

		private void _FA_Graph4_Click(object sender, System.EventArgs e)
		{			
			CashFlowVsCashOnCashReturn graphX = new CashFlowVsCashOnCashReturn();
			double delta = 0;

			try {delta = Convert.ToDouble(_FA_LR_Range_NUD.Value); }
			catch {delta = 0;}

			graphX.SetDataForLRRange((double)FinancialInfoObject.Instance.Data[0].GsiAnnualValueProperty, 
				(double)FinancialInfoObject.Instance.Data[0].VacancyCreditLossPercValueProperty, 
				(double)FinancialInfoObject.Instance.Data[0].OtherIncomeValueProperty,
				(double)FinancialInfoObject.Instance.Data[0].OperatingExpensesPercValueProperty, 				
				(double)FinancialInfoObject.Instance.Data[0].DebtServiceValueProperty, 
				(double)LoansObject.Instance.DownPaymentValue, 
				delta);

			graphX.ShowDialog();
		}
		#endregion		

        #region MULTIYEAR ANALYSIS MAP TO OBJECT MODEL
        void _MA_Leave(object sender, EventArgs e)
        {
            Handle_MA_TextBoxes(sender);
        }

        void _MA_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Enter)
                Handle_MA_TextBoxes(sender);
        }

        void On_MA_DropDown_Leave(object sender, EventArgs e)
        {
            Handle_MA_DropDownBoxes(sender);
        }
        
        
        void Init_MA_DropDownBoxes()
        {
            _MA_S2_InflationPercChange.Items.Add(new CustomItem(5));
            _MA_S2_InflationPercChange.Items.Add(new CustomItem(4));
            _MA_S2_InflationPercChange.Items.Add(new CustomItem(3));
            _MA_S2_InflationPercChange.Items.Add(new CustomItem(2));
            _MA_S2_InflationPercChange.Items.Add(new CustomItem(1));
            _MA_S2_InflationPercChange.Items.Add(new CustomItem(0));
            _MA_S2_InflationPercChange.Items.Add(new CustomItem(-1));
            _MA_S2_InflationPercChange.Items.Add(new CustomItem(-2));
            _MA_S2_InflationPercChange.Items.Add(new CustomItem(-3));
            _MA_S2_InflationPercChange.Items.Add(new CustomItem(-4));
            _MA_S2_InflationPercChange.Items.Add(new CustomItem(-5));

            _MA_S2_OperatingExpensesPercChange.Items.Add(new CustomItem(10));
            _MA_S2_OperatingExpensesPercChange.Items.Add(new CustomItem(8));
            _MA_S2_OperatingExpensesPercChange.Items.Add(new CustomItem(6));
            _MA_S2_OperatingExpensesPercChange.Items.Add(new CustomItem(4));
            _MA_S2_OperatingExpensesPercChange.Items.Add(new CustomItem(2));
            _MA_S2_OperatingExpensesPercChange.Items.Add(new CustomItem(1));
            _MA_S2_OperatingExpensesPercChange.Items.Add(new CustomItem(0));
            _MA_S2_OperatingExpensesPercChange.Items.Add(new CustomItem(-1));
            _MA_S2_OperatingExpensesPercChange.Items.Add(new CustomItem(-2));
            _MA_S2_OperatingExpensesPercChange.Items.Add(new CustomItem(-4));
            _MA_S2_OperatingExpensesPercChange.Items.Add(new CustomItem(-6));
            _MA_S2_OperatingExpensesPercChange.Items.Add(new CustomItem(-8));
            _MA_S2_OperatingExpensesPercChange.Items.Add(new CustomItem(-10));

            _MA_S2_VacancyPercChange.Items.Add(new CustomItem(10));
            _MA_S2_VacancyPercChange.Items.Add(new CustomItem(8));
            _MA_S2_VacancyPercChange.Items.Add(new CustomItem(6));
            _MA_S2_VacancyPercChange.Items.Add(new CustomItem(4));
            _MA_S2_VacancyPercChange.Items.Add(new CustomItem(2));
            _MA_S2_VacancyPercChange.Items.Add(new CustomItem(0));
            _MA_S2_VacancyPercChange.Items.Add(new CustomItem(-2));
            _MA_S2_VacancyPercChange.Items.Add(new CustomItem(-4));
            _MA_S2_VacancyPercChange.Items.Add(new CustomItem(-6));
            _MA_S2_VacancyPercChange.Items.Add(new CustomItem(-8));
            _MA_S2_VacancyPercChange.Items.Add(new CustomItem(-10));

            _MA_S2_IncomePercChange.Items.Add(new CustomItem(6));
            _MA_S2_IncomePercChange.Items.Add(new CustomItem(5));
            _MA_S2_IncomePercChange.Items.Add(new CustomItem(4));
            _MA_S2_IncomePercChange.Items.Add(new CustomItem(3));
            _MA_S2_IncomePercChange.Items.Add(new CustomItem(2));
            _MA_S2_IncomePercChange.Items.Add(new CustomItem(1));
            _MA_S2_IncomePercChange.Items.Add(new CustomItem(0));
            _MA_S2_IncomePercChange.Items.Add(new CustomItem(1));
            _MA_S2_IncomePercChange.Items.Add(new CustomItem(2));
            _MA_S2_IncomePercChange.Items.Add(new CustomItem(3));
            _MA_S2_IncomePercChange.Items.Add(new CustomItem(4));
            _MA_S2_IncomePercChange.Items.Add(new CustomItem(5));

            _MA_RS_MARG_AnalysisPeriod.Items.Add(new CustomYearItem(10));
            _MA_RS_MARG_AnalysisPeriod.Items.Add(new CustomYearItem(9));
            _MA_RS_MARG_AnalysisPeriod.Items.Add(new CustomYearItem(8));
            _MA_RS_MARG_AnalysisPeriod.Items.Add(new CustomYearItem(7));
            _MA_RS_MARG_AnalysisPeriod.Items.Add(new CustomYearItem(6));
            _MA_RS_MARG_AnalysisPeriod.Items.Add(new CustomYearItem(5));
            _MA_RS_MARG_AnalysisPeriod.Items.Add(new CustomYearItem(4));
            _MA_RS_MARG_AnalysisPeriod.Items.Add(new CustomYearItem(3));
            _MA_RS_MARG_AnalysisPeriod.Items.Add(new CustomYearItem(2));
            _MA_RS_MARG_AnalysisPeriod.Items.Add(new CustomYearItem(1));
        }

        #region DOWNPAYMENT
        void _MA_DownPayment_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Enter)
                Handle_MA_DownPayment();
        }

        void _MA_DownPayment_Leave(object sender, EventArgs e)
        {
            Handle_MA_DownPayment();
        }

        void Handle_MA_DownPayment()
        {            
            try
            {
                decimal dTemp = Convert.ToDecimal(_MA_DownPayment.Text);
                MultiyearAnalysisObject.Instance.DownPayement = _MA_DownPayment.Text = dTemp.ToString(CurrFormatter);
            }
            catch { MultiyearAnalysisObject.Instance.DownPayement = _MA_DownPayment.Text; }
            Update_MA_InitialInvestment();
        }
        #endregion

        #region POINTS
        void _MA_Points_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Enter)
                Handle_MA_Points();
        }

        void _MA_Points_Leave(object sender, EventArgs e)
        {            
            Handle_MA_Points();
        }

        void Handle_MA_Points()
        {             
            try
            {
                decimal dTemp = Convert.ToDecimal(_MA_Points.Text);
                MultiyearAnalysisObject.Instance.Points = _MA_Points.Text = dTemp.ToString(CurrFormatter);
            }
            catch { MultiyearAnalysisObject.Instance.Points = _MA_Points.Text; }
            Update_MA_InitialInvestment();
        }
        #endregion

        #region OTHER ACQUISITION COSTS
        void _MA_OtherAcquisitionCosts_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Enter)
                Handle_MA_OtherAcquisitionCosts();
        }

        void _MA_OtherAcquisitionCosts_Leave(object sender, EventArgs e)
        {
            Handle_MA_OtherAcquisitionCosts();
        }

        void Handle_MA_OtherAcquisitionCosts()
        {            
            try
            {
                decimal dTemp = Convert.ToDecimal(_MA_OtherAcquisitionCosts.Text);
                MultiyearAnalysisObject.Instance.OtherAcquisitionCosts = _MA_OtherAcquisitionCosts.Text = dTemp.ToString(CurrFormatter);
            }
            catch { MultiyearAnalysisObject.Instance.OtherAcquisitionCosts = _MA_OtherAcquisitionCosts.Text; }
            Update_MA_InitialInvestment();
        }
        #endregion

        void Update_MA_InitialInvestment()
        {
            //=down payment+points+other acquisition costs

            decimal value = 0;
            value += GetDecimalFromString(MultiyearAnalysisObject.Instance.DownPayement);
            value += GetDecimalFromString(MultiyearAnalysisObject.Instance.Points);
            value += GetDecimalFromString(MultiyearAnalysisObject.Instance.OtherAcquisitionCosts);

            _MA_InitialInvestment.Text = MultiyearAnalysisObject.Instance.InitialInvestments = value.ToString(CurrFormatter);
        }

        #region GSI
        void _MA_GSI_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                Handle_MA_GSI();
        }
        void _MA_GSI_Leave(object sender, EventArgs e)
        {
            Handle_MA_GSI();
        }

        void Handle_MA_GSI()
        {            
            try
            {
                decimal dTemp = Convert.ToDecimal(_MA_S1_GSI.Text);
                MultiyearAnalysisObject.Instance.GsiAnnual = _MA_S1_GSI.Text = dTemp.ToString(CurrFormatter);
            }
            catch { MultiyearAnalysisObject.Instance.GsiAnnual = _MA_S1_GSI.Text; }

            UpdateVacancyCreditLossUponChange();
            UpdateOperatingExpensesUponChange();
        }        
        #endregion

        #region VACANCY
        void _MA_Vacancy_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                Handle_MA_Vacancy();
        }
        void _MA_Vacancy_Leave(object sender, EventArgs e)
        {
            Handle_MA_Vacancy();
        }

        void Handle_MA_Vacancy()
        {            
            try
            {
                decimal dTemp = Convert.ToDecimal(_MA_S1_Vacancy.Text);
                MultiyearAnalysisObject.Instance.VacancyCreditLoss = _MA_S1_Vacancy.Text = dTemp.ToString(CurrFormatter);
            }
            catch { MultiyearAnalysisObject.Instance.VacancyCreditLoss = _MA_S1_Vacancy.Text; }
            UpdateVacancyCreditLossPercUponChange();
            Calc_MA_VacancyPercPerYears();
        }
        #endregion

        #region VACANCY PERC
        void _MA_VacancyPerc_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                Handle_MA_VacancyPerc();
        }
        void _MA_VacancyPerc_Leave(object sender, EventArgs e)
        {
            Handle_MA_VacancyPerc();
        }

        void Handle_MA_VacancyPerc()
        {
            try
            {
                decimal dTemp = Convert.ToDecimal(_MA_S1_VacancyPerc.Text);
                MultiyearAnalysisObject.Instance.VacancyCreditLossPerc = _MA_S1_VacancyPerc.Text = dTemp.ToString(CurrFormatter);
            }
            catch { MultiyearAnalysisObject.Instance.VacancyCreditLossPerc = _MA_S1_VacancyPerc.Text; }
            UpdateVacancyCreditLossUponChange();
        }
        #endregion

        #region OPERATING EXPENSES
        void _MA_OperatingExpenses_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                Handle_MA_OperatingExpenses();
        }
        void _MA_OperatingExpenses_Leave(object sender, EventArgs e)
        {
            Handle_MA_OperatingExpenses();
        }

        void Handle_MA_OperatingExpenses()
        {            
            try
            {
                decimal dTemp = Convert.ToDecimal(_MA_S1_OperatingExpenses.Text);
                MultiyearAnalysisObject.Instance.OperatingExpenses = _MA_S1_OperatingExpenses.Text = dTemp.ToString(CurrFormatter);
            }
            catch { MultiyearAnalysisObject.Instance.OperatingExpenses = _MA_S1_OperatingExpenses.Text; }
            UpdateOperatingExpensesPercUponChange();                                    
        }
        #endregion

        #region OPERATING EXPENSES PERC
        void _MA_OperatingExpensesPerc_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                Handle_MA_OperatingExpensesPerc();
        }
        void _MA_OperatingExpensesPerc_Leave(object sender, EventArgs e)
        {
            Handle_MA_OperatingExpensesPerc();
        }

        void Handle_MA_OperatingExpensesPerc()
        {
            try
            {
                decimal dTemp = Convert.ToDecimal(_MA_S1_OperatingExpensesPerc.Text);
                MultiyearAnalysisObject.Instance.OperatingExpensesPerc = _MA_S1_OperatingExpensesPerc.Text = dTemp.ToString(CurrFormatter);
            }
            catch { MultiyearAnalysisObject.Instance.OperatingExpensesPerc = _MA_S1_OperatingExpensesPerc.Text; }
            UpdateOperatingExpensesUponChange();
        }
        #endregion

        void UpdateVacancyCreditLossUponChange()
        {
            //=%Vacancy*GSI
            decimal val = GetDecimalFromString(MultiyearAnalysisObject.Instance.VacancyCreditLossPerc) / 100 * GetDecimalFromString(MultiyearAnalysisObject.Instance.GsiAnnual);
            _MA_S1_Vacancy.Text = MultiyearAnalysisObject.Instance.VacancyCreditLoss = val.ToString(CurrFormatter);

            Calc_MA_VacancyPercPerYears();
        }

        void UpdateVacancyCreditLossPercUponChange()
        {
            //=Vacancy/GSI
            if (0 != GetDecimalFromString(MultiyearAnalysisObject.Instance.GsiAnnual))
            {
                decimal val = GetDecimalFromString(MultiyearAnalysisObject.Instance.VacancyCreditLoss) / GetDecimalFromString(MultiyearAnalysisObject.Instance.GsiAnnual) * 100;
                _MA_S1_VacancyPerc.Text = MultiyearAnalysisObject.Instance.VacancyCreditLossPerc = val.ToString(CurrFormatter);
            }
            else
                _MA_S1_VacancyPerc.Text = MultiyearAnalysisObject.Instance.VacancyCreditLossPerc = "NaN";
        }
        
        void UpdateOperatingExpensesUponChange()
        {
            //=%OE*GSI
            decimal val = GetDecimalFromString(MultiyearAnalysisObject.Instance.OperatingExpensesPerc) / 100 * GetDecimalFromString(MultiyearAnalysisObject.Instance.GsiAnnual);
            _MA_S1_OperatingExpenses.Text = MultiyearAnalysisObject.Instance.OperatingExpenses = val.ToString(CurrFormatter);

            Calc_MA_OperatingExpensesPercPerYears();            
        }

        void UpdateOperatingExpensesPercUponChange()
        {
            //=OperatingExpenses/GSI
            if (0 != GetDecimalFromString(MultiyearAnalysisObject.Instance.GsiAnnual))
            {
                decimal val = GetDecimalFromString(MultiyearAnalysisObject.Instance.OperatingExpenses) / GetDecimalFromString(MultiyearAnalysisObject.Instance.GsiAnnual) * 100;
                _MA_S1_OperatingExpensesPerc.Text = MultiyearAnalysisObject.Instance.OperatingExpensesPerc = val.ToString(CurrFormatter);
            }
            else
                _MA_S1_OperatingExpensesPerc.Text = MultiyearAnalysisObject.Instance.OperatingExpensesPerc = "NaN";

            Calc_MA_OperatingExpensesPercPerYears();
        }
        
        void Handle_MA_TextBoxes(object sender)
        {
            TextBox tb = (TextBox)sender;
            decimal dTemp = 0;
            if (tb.Name == _MA_PurchasePrice.Name)
            {
                try
                {
                    dTemp = Convert.ToDecimal(_MA_PurchasePrice.Text);
                    MultiyearAnalysisObject.Instance.PurchasePrice = _MA_PurchasePrice.Text = dTemp.ToString(CurrFormatter);
                }
                catch { MultiyearAnalysisObject.Instance.PurchasePrice = _MA_PurchasePrice.Text; }
            }
            else if (tb.Name == _MA_InitialInvestment.Name)
            {
                try
                {
                    dTemp = Convert.ToDecimal(_MA_InitialInvestment.Text);
                    MultiyearAnalysisObject.Instance.InitialInvestments = _MA_InitialInvestment.Text = dTemp.ToString(CurrFormatter);
                }
                catch { MultiyearAnalysisObject.Instance.InitialInvestments = _MA_InitialInvestment.Text; }
            }                                    
            
            else if (tb.Name == _MA_S1_GSI_Perc.Name)
            {
                try
                {
                    dTemp = Convert.ToDecimal(_MA_S1_GSI_Perc.Text);
                    MultiyearAnalysisObject.Instance.GsiAnnualPerc = _MA_S1_GSI_Perc.Text = dTemp.ToString(CurrFormatter);
                }
                catch { MultiyearAnalysisObject.Instance.GsiAnnualPerc = _MA_S1_GSI_Perc.Text; }
            }
            
            else if (tb.Name == _MA_S1_OtherIncome.Name)
            {
                try
                {
                    dTemp = Convert.ToDecimal(_MA_S1_OtherIncome.Text);
                    MultiyearAnalysisObject.Instance.OtherIncome = _MA_S1_OtherIncome.Text = dTemp.ToString(CurrFormatter);
                }
                catch { MultiyearAnalysisObject.Instance.OtherIncome = _MA_S1_OtherIncome.Text; }
            }
            else if (tb.Name == _MA_S1_OtherIncomePerc.Name)
            {
                try
                {
                    dTemp = Convert.ToDecimal(_MA_S1_OtherIncomePerc.Text);
                    MultiyearAnalysisObject.Instance.OtherIncomePerc = _MA_S1_OtherIncomePerc.Text = dTemp.ToString(CurrFormatter);
                }
                catch { MultiyearAnalysisObject.Instance.OtherIncomePerc = _MA_S1_OtherIncomePerc.Text; }
            }                
            else if (tb.Name == _MA_TI_FederalRate.Name)
            {
                try
                {
                    dTemp = Convert.ToDecimal(_MA_TI_FederalRate.Text);
                    MultiyearAnalysisObject.Instance.FederalRate = _MA_TI_FederalRate.Text = dTemp.ToString(CurrFormatter);
                }
                catch { MultiyearAnalysisObject.Instance.FederalRate = _MA_TI_FederalRate.Text; }
            }
            else if (tb.Name == _MA_TI_StateRate.Name)
            {
                try
                {
                    dTemp = Convert.ToDecimal(_MA_TI_StateRate.Text);
                    MultiyearAnalysisObject.Instance.StateRate = _MA_TI_StateRate.Text = dTemp.ToString(CurrFormatter);
                }
                catch { MultiyearAnalysisObject.Instance.StateRate = _MA_TI_StateRate.Text; }
            }
            else if (tb.Name == _MA_TI_CapitalGainsRate.Name)
            {
                try
                {
                    dTemp = Convert.ToDecimal(_MA_TI_CapitalGainsRate.Text);
                    MultiyearAnalysisObject.Instance.CapitalGainsRate = _MA_TI_CapitalGainsRate.Text = dTemp.ToString(CurrFormatter);
                }
                catch { MultiyearAnalysisObject.Instance.CapitalGainsRate = _MA_TI_CapitalGainsRate.Text; }
            }
            else if (tb.Name == _MA_TI_BuildingValue.Name)
            {
                try
                {
                    dTemp = Convert.ToDecimal(_MA_TI_BuildingValue.Text);
                    MultiyearAnalysisObject.Instance.BuildingValue = _MA_TI_BuildingValue.Text = dTemp.ToString(CurrFormatter);
                }
                catch { MultiyearAnalysisObject.Instance.BuildingValue = _MA_TI_BuildingValue.Text; }
            }
            else if (tb.Name == _MA_SI_SaleCosts.Name)
            {
                try
                {
                    dTemp = Convert.ToDecimal(_MA_SI_SaleCosts.Text);
                    MultiyearAnalysisObject.Instance.SaleCosts = _MA_SI_SaleCosts.Text = dTemp.ToString(CurrFormatter);
                }
                catch { MultiyearAnalysisObject.Instance.SaleCosts = _MA_SI_SaleCosts.Text; }
            }
            else if (tb.Name == _MA_SI_SaleCapRate.Name)
            {
                try
                {
                    dTemp = Convert.ToDecimal(_MA_SI_SaleCapRate.Text);
                    MultiyearAnalysisObject.Instance.SaleCapRate = _MA_SI_SaleCapRate.Text = dTemp.ToString(CurrFormatter);
                }
                catch { MultiyearAnalysisObject.Instance.SaleCapRate = _MA_SI_SaleCapRate.Text; }
            }
            else if (tb.Name == _MA_S3_OpEx_Y1.Name)
            {
                try
                {
                    dTemp = Convert.ToDecimal(_MA_S3_OpEx_Y1.Text);
                    MultiyearAnalysisObject.Instance.OperatingExpensesPercs[0] = _MA_S3_OpEx_Y1.Text = dTemp.ToString(CurrFormatter);
                }
                catch { MultiyearAnalysisObject.Instance.OperatingExpensesPercs[0] = _MA_S3_OpEx_Y1.Text; }
            }
            else if (tb.Name == _MA_S3_OpEx_Y2.Name)
            {
                try
                {
                    dTemp = Convert.ToDecimal(_MA_S3_OpEx_Y2.Text);
                    MultiyearAnalysisObject.Instance.OperatingExpensesPercs[1] = _MA_S3_OpEx_Y2.Text = dTemp.ToString(CurrFormatter);
                }
                catch { MultiyearAnalysisObject.Instance.OperatingExpensesPercs[1] = _MA_S3_OpEx_Y2.Text; }
            }
            else if (tb.Name == _MA_S3_OpEx_Y3.Name)
            {
                try
                {
                    dTemp = Convert.ToDecimal(_MA_S3_OpEx_Y3.Text);
                    MultiyearAnalysisObject.Instance.OperatingExpensesPercs[2] = _MA_S3_OpEx_Y3.Text = dTemp.ToString(CurrFormatter);
                }
                catch { MultiyearAnalysisObject.Instance.OperatingExpensesPercs[2] = _MA_S3_OpEx_Y3.Text; }
            }
            else if (tb.Name == _MA_S3_OpEx_Y4.Name)
            {
                try
                {
                    dTemp = Convert.ToDecimal(_MA_S3_OpEx_Y4.Text);
                    MultiyearAnalysisObject.Instance.OperatingExpensesPercs[3] = _MA_S3_OpEx_Y4.Text = dTemp.ToString(CurrFormatter);
                }
                catch { MultiyearAnalysisObject.Instance.OperatingExpensesPercs[3] = _MA_S3_OpEx_Y4.Text; }
            }
            else if (tb.Name == _MA_S3_OpEx_Y5.Name)
            {
                try
                {
                    dTemp = Convert.ToDecimal(_MA_S3_OpEx_Y5.Text);
                    MultiyearAnalysisObject.Instance.OperatingExpensesPercs[4] = _MA_S3_OpEx_Y5.Text = dTemp.ToString(CurrFormatter);
                }
                catch { MultiyearAnalysisObject.Instance.OperatingExpensesPercs[4] = _MA_S3_OpEx_Y5.Text; }
            }
            else if (tb.Name == _MA_S3_OpEx_Y6.Name)
            {
                try
                {
                    dTemp = Convert.ToDecimal(_MA_S3_OpEx_Y6.Text);
                    MultiyearAnalysisObject.Instance.OperatingExpensesPercs[5] = _MA_S3_OpEx_Y6.Text = dTemp.ToString(CurrFormatter);
                }
                catch { MultiyearAnalysisObject.Instance.OperatingExpensesPercs[5] = _MA_S3_OpEx_Y6.Text; }
            }
            else if (tb.Name == _MA_S3_OpEx_Y7.Name)
            {
                try
                {
                    dTemp = Convert.ToDecimal(_MA_S3_OpEx_Y7.Text);
                    MultiyearAnalysisObject.Instance.OperatingExpensesPercs[6] = _MA_S3_OpEx_Y7.Text = dTemp.ToString(CurrFormatter);
                }
                catch { MultiyearAnalysisObject.Instance.OperatingExpensesPercs[6] = _MA_S3_OpEx_Y7.Text; }
            }
            else if (tb.Name == _MA_S3_OpEx_Y8.Name)
            {
                try
                {
                    dTemp = Convert.ToDecimal(_MA_S3_OpEx_Y8.Text);
                    MultiyearAnalysisObject.Instance.OperatingExpensesPercs[7] = _MA_S3_OpEx_Y8.Text = dTemp.ToString(CurrFormatter);
                }
                catch { MultiyearAnalysisObject.Instance.OperatingExpensesPercs[7] = _MA_S3_OpEx_Y8.Text; }
            }
            else if (tb.Name == _MA_S3_OpEx_Y9.Name)
            {
                try
                {
                    dTemp = Convert.ToDecimal(_MA_S3_OpEx_Y9.Text);
                    MultiyearAnalysisObject.Instance.OperatingExpensesPercs[8] = _MA_S3_OpEx_Y9.Text = dTemp.ToString(CurrFormatter);
                }
                catch { MultiyearAnalysisObject.Instance.OperatingExpensesPercs[8] = _MA_S3_OpEx_Y9.Text; }
            }
            else if (tb.Name == _MA_S3_OpEx_Y10.Name)
            {
                try
                {
                    dTemp = Convert.ToDecimal(_MA_S3_OpEx_Y10.Text);
                    MultiyearAnalysisObject.Instance.OperatingExpensesPercs[9] = _MA_S3_OpEx_Y10.Text = dTemp.ToString(CurrFormatter);
                }
                catch { MultiyearAnalysisObject.Instance.OperatingExpensesPercs[9] = _MA_S3_OpEx_Y10.Text; }
            }
            else if (tb.Name == _MA_S3_Vacancy_Y1.Name)
            {
                try
                {
                    dTemp = Convert.ToDecimal(_MA_S3_Vacancy_Y1.Text);
                    MultiyearAnalysisObject.Instance.VacancyPercs[0] = _MA_S3_Vacancy_Y1.Text = dTemp.ToString(CurrFormatter);
                }
                catch { MultiyearAnalysisObject.Instance.VacancyPercs[0] = _MA_S3_Vacancy_Y1.Text; }
            }
            else if (tb.Name == _MA_S3_Vacancy_Y2.Name)
            {
                try
                {
                    dTemp = Convert.ToDecimal(_MA_S3_Vacancy_Y2.Text);
                    MultiyearAnalysisObject.Instance.VacancyPercs[1] = _MA_S3_Vacancy_Y2.Text = dTemp.ToString(CurrFormatter);
                }
                catch { MultiyearAnalysisObject.Instance.VacancyPercs[1] = _MA_S3_Vacancy_Y2.Text; }
            }
            else if (tb.Name == _MA_S3_Vacancy_Y3.Name)
            {
                try
                {
                    dTemp = Convert.ToDecimal(_MA_S3_Vacancy_Y3.Text);
                    MultiyearAnalysisObject.Instance.VacancyPercs[2] = _MA_S3_Vacancy_Y3.Text = dTemp.ToString(CurrFormatter);
                }
                catch { MultiyearAnalysisObject.Instance.VacancyPercs[2] = _MA_S3_Vacancy_Y3.Text; }
            }
            else if (tb.Name == _MA_S3_Vacancy_Y4.Name)
            {
                try
                {
                    dTemp = Convert.ToDecimal(_MA_S3_Vacancy_Y4.Text);
                    MultiyearAnalysisObject.Instance.VacancyPercs[3] = _MA_S3_Vacancy_Y4.Text = dTemp.ToString(CurrFormatter);
                }
                catch { MultiyearAnalysisObject.Instance.VacancyPercs[3] = _MA_S3_Vacancy_Y4.Text; }
            }
            else if (tb.Name == _MA_S3_Vacancy_Y5.Name)
            {
                try
                {
                    dTemp = Convert.ToDecimal(_MA_S3_Vacancy_Y5.Text);
                    MultiyearAnalysisObject.Instance.VacancyPercs[4] = _MA_S3_Vacancy_Y5.Text = dTemp.ToString(CurrFormatter);
                }
                catch { MultiyearAnalysisObject.Instance.VacancyPercs[4] = _MA_S3_Vacancy_Y5.Text; }
            }
            else if (tb.Name == _MA_S3_Vacancy_Y6.Name)
            {
                try
                {
                    dTemp = Convert.ToDecimal(_MA_S3_Vacancy_Y6.Text);
                    MultiyearAnalysisObject.Instance.VacancyPercs[5] = _MA_S3_Vacancy_Y6.Text = dTemp.ToString(CurrFormatter);
                }
                catch { MultiyearAnalysisObject.Instance.VacancyPercs[5] = _MA_S3_Vacancy_Y6.Text; }
            }
            else if (tb.Name == _MA_S3_Vacancy_Y7.Name)
            {
                try
                {
                    dTemp = Convert.ToDecimal(_MA_S3_Vacancy_Y7.Text);
                    MultiyearAnalysisObject.Instance.VacancyPercs[6] = _MA_S3_Vacancy_Y7.Text = dTemp.ToString(CurrFormatter);
                }
                catch { MultiyearAnalysisObject.Instance.VacancyPercs[6] = _MA_S3_Vacancy_Y7.Text; }
            }
            else if (tb.Name == _MA_S3_Vacancy_Y8.Name)
            {
                try
                {
                    dTemp = Convert.ToDecimal(_MA_S3_Vacancy_Y8.Text);
                    MultiyearAnalysisObject.Instance.VacancyPercs[7] = _MA_S3_Vacancy_Y8.Text = dTemp.ToString(CurrFormatter);
                }
                catch { MultiyearAnalysisObject.Instance.VacancyPercs[7] = _MA_S3_Vacancy_Y8.Text; }
            }
            else if (tb.Name == _MA_S3_Vacancy_Y9.Name)
            {
                try
                {
                    dTemp = Convert.ToDecimal(_MA_S3_Vacancy_Y9.Text);
                    MultiyearAnalysisObject.Instance.VacancyPercs[8] = _MA_S3_Vacancy_Y9.Text = dTemp.ToString(CurrFormatter);
                }
                catch { MultiyearAnalysisObject.Instance.VacancyPercs[8] = _MA_S3_Vacancy_Y9.Text; }
            }
            else if (tb.Name == _MA_S3_Vacancy_Y10.Name)
            {
                try
                {
                    dTemp = Convert.ToDecimal(_MA_S3_Vacancy_Y10.Text);
                    MultiyearAnalysisObject.Instance.VacancyPercs[9] = _MA_S3_Vacancy_Y10.Text = dTemp.ToString(CurrFormatter);
                }
                catch { MultiyearAnalysisObject.Instance.VacancyPercs[9] = _MA_S3_Vacancy_Y10.Text; }
            }
            else if (tb.Name == _MA_S3_IncomeChange_Y1.Name)
            {
                try
                {
                    dTemp = Convert.ToDecimal(_MA_S3_IncomeChange_Y1.Text);
                    MultiyearAnalysisObject.Instance.IncomeChangePercs[0] = _MA_S3_IncomeChange_Y1.Text = dTemp.ToString(CurrFormatter);
                }
                catch { MultiyearAnalysisObject.Instance.IncomeChangePercs[0] = _MA_S3_IncomeChange_Y1.Text; }
            }
            else if (tb.Name == _MA_S3_IncomeChange_Y2.Name)
            {
                try
                {
                    dTemp = Convert.ToDecimal(_MA_S3_IncomeChange_Y2.Text);
                    MultiyearAnalysisObject.Instance.IncomeChangePercs[1] = _MA_S3_IncomeChange_Y2.Text = dTemp.ToString(CurrFormatter);
                }
                catch { MultiyearAnalysisObject.Instance.IncomeChangePercs[1] = _MA_S3_IncomeChange_Y2.Text; }
            }
            else if (tb.Name == _MA_S3_IncomeChange_Y3.Name)
            {
                try
                {
                    dTemp = Convert.ToDecimal(_MA_S3_IncomeChange_Y3.Text);
                    MultiyearAnalysisObject.Instance.IncomeChangePercs[2] = _MA_S3_IncomeChange_Y3.Text = dTemp.ToString(CurrFormatter);
                }
                catch { MultiyearAnalysisObject.Instance.IncomeChangePercs[2] = _MA_S3_IncomeChange_Y3.Text; }
            }
            else if (tb.Name == _MA_S3_IncomeChange_Y4.Name)
            {
                try
                {
                    dTemp = Convert.ToDecimal(_MA_S3_IncomeChange_Y4.Text);
                    MultiyearAnalysisObject.Instance.IncomeChangePercs[3] = _MA_S3_IncomeChange_Y4.Text = dTemp.ToString(CurrFormatter);
                }
                catch { MultiyearAnalysisObject.Instance.IncomeChangePercs[3] = _MA_S3_IncomeChange_Y4.Text; }
            }
            else if (tb.Name == _MA_S3_IncomeChange_Y5.Name)
            {
                try
                {
                    dTemp = Convert.ToDecimal(_MA_S3_IncomeChange_Y5.Text);
                    MultiyearAnalysisObject.Instance.IncomeChangePercs[4] = _MA_S3_IncomeChange_Y5.Text = dTemp.ToString(CurrFormatter);
                }
                catch { MultiyearAnalysisObject.Instance.IncomeChangePercs[4] = _MA_S3_IncomeChange_Y5.Text; }
            }
            else if (tb.Name == _MA_S3_IncomeChange_Y6.Name)
            {
                try
                {
                    dTemp = Convert.ToDecimal(_MA_S3_IncomeChange_Y6.Text);
                    MultiyearAnalysisObject.Instance.IncomeChangePercs[5] = _MA_S3_IncomeChange_Y6.Text = dTemp.ToString(CurrFormatter);
                }
                catch { MultiyearAnalysisObject.Instance.IncomeChangePercs[5] = _MA_S3_IncomeChange_Y6.Text; }
            }
            else if (tb.Name == _MA_S3_IncomeChange_Y7.Name)
            {
                try
                {
                    dTemp = Convert.ToDecimal(_MA_S3_IncomeChange_Y7.Text);
                    MultiyearAnalysisObject.Instance.IncomeChangePercs[6] = _MA_S3_IncomeChange_Y7.Text = dTemp.ToString(CurrFormatter);
                }
                catch { MultiyearAnalysisObject.Instance.IncomeChangePercs[6] = _MA_S3_IncomeChange_Y7.Text; }
            }
            else if (tb.Name == _MA_S3_IncomeChange_Y8.Name)
            {
                try
                {
                    dTemp = Convert.ToDecimal(_MA_S3_IncomeChange_Y8.Text);
                    MultiyearAnalysisObject.Instance.IncomeChangePercs[7] = _MA_S3_IncomeChange_Y8.Text = dTemp.ToString(CurrFormatter);
                }
                catch { MultiyearAnalysisObject.Instance.IncomeChangePercs[7] = _MA_S3_IncomeChange_Y8.Text; }
            }
            else if (tb.Name == _MA_S3_IncomeChange_Y9.Name)
            {
                try
                {
                    dTemp = Convert.ToDecimal(_MA_S3_IncomeChange_Y9.Text);
                    MultiyearAnalysisObject.Instance.IncomeChangePercs[8] = _MA_S3_IncomeChange_Y9.Text = dTemp.ToString(CurrFormatter);
                }
                catch { MultiyearAnalysisObject.Instance.IncomeChangePercs[8] = _MA_S3_IncomeChange_Y9.Text; }
            }
            else if (tb.Name == _MA_S3_IncomeChange_Y10.Name)
            {
                try
                {
                    dTemp = Convert.ToDecimal(_MA_S3_IncomeChange_Y10.Text);
                    MultiyearAnalysisObject.Instance.IncomeChangePercs[9] = _MA_S3_IncomeChange_Y10.Text = dTemp.ToString(CurrFormatter);
                }
                catch { MultiyearAnalysisObject.Instance.IncomeChangePercs[9] = _MA_S3_IncomeChange_Y10.Text; }
            }


        }

        void Handle_MA_DropDownBoxes(object sender)
        {
            ComboBox cb = (ComboBox)sender;

            if (null != cb && null != cb.SelectedItem)
            {
                if (cb.Name == _MA_S2_InflationPercChange.Name)
                {
                    MultiyearAnalysisObject.Instance.AnnualInflationChange = ((CustomItem)(cb.SelectedItem)).ChangeValue.ToString();
                }
                else if (cb.Name == _MA_S2_OperatingExpensesPercChange.Name)
                {
                    MultiyearAnalysisObject.Instance.AnnualOperatingExpensesChange = ((CustomItem)(cb.SelectedItem)).ChangeValue.ToString();                    
                }
                else if (cb.Name == _MA_S2_VacancyPercChange.Name)
                {
                    MultiyearAnalysisObject.Instance.AnnualVacancyChange = ((CustomItem)(cb.SelectedItem)).ChangeValue.ToString();
                }
                else if (cb.Name == _MA_S2_IncomePercChange.Name)
                {
                    MultiyearAnalysisObject.Instance.AnnualIncomeChange = ((CustomItem)(cb.SelectedItem)).ChangeValue.ToString();
                }
                else if (cb.Name == _MA_TI_DepreciableYears.Name)
                {
                    MultiyearAnalysisObject.Instance.DepreciableYears = cb.SelectedItem.ToString();
                }
            }       
        }

        void On_MA_S2_OperatingExpensesPercChange_SelectedIndexChanged(object sender, EventArgs e)
        {
            Calc_MA_OperatingExpensesPercPerYears();
        }

        void On_MA_S2_VacancyPercChange_SelectedIndexChanged(object sender, EventArgs e)
        {
            Calc_MA_VacancyPercPerYears();
        }

        void On_MA_S2_IncomePercChange_SelectedIndexChanged(object sender, EventArgs e)
        {
            Update_MA_PercentIncomeTable();
        } 

        void Calc_MA_OperatingExpensesPercPerYears()
        {
            List<decimal> lst = new List<decimal>();
            lst.Add(GetDecimalFromString(MultiyearAnalysisObject.Instance.OperatingExpensesPerc));
            for (int i = 1; i < 10; i++)            
            {
                lst.Add(lst[i - 1] * (1 + (decimal)((CustomItem)_MA_S2_OperatingExpensesPercChange.SelectedItem).ChangeValue / 100));
            }

            MultiyearAnalysisObject.Instance.OperatingExpensesPercs[0] = _MA_S3_OpEx_Y1.Text = lst[0].ToString(CurrFormatter);
            MultiyearAnalysisObject.Instance.OperatingExpensesPercs[1] = _MA_S3_OpEx_Y2.Text = lst[1].ToString(CurrFormatter);
            MultiyearAnalysisObject.Instance.OperatingExpensesPercs[2] = _MA_S3_OpEx_Y3.Text = lst[2].ToString(CurrFormatter);
            MultiyearAnalysisObject.Instance.OperatingExpensesPercs[3] = _MA_S3_OpEx_Y4.Text = lst[3].ToString(CurrFormatter);
            MultiyearAnalysisObject.Instance.OperatingExpensesPercs[4] = _MA_S3_OpEx_Y5.Text = lst[4].ToString(CurrFormatter);
            MultiyearAnalysisObject.Instance.OperatingExpensesPercs[5] = _MA_S3_OpEx_Y6.Text = lst[5].ToString(CurrFormatter);
            MultiyearAnalysisObject.Instance.OperatingExpensesPercs[6] = _MA_S3_OpEx_Y7.Text = lst[6].ToString(CurrFormatter);
            MultiyearAnalysisObject.Instance.OperatingExpensesPercs[7] = _MA_S3_OpEx_Y8.Text = lst[7].ToString(CurrFormatter);
            MultiyearAnalysisObject.Instance.OperatingExpensesPercs[8] = _MA_S3_OpEx_Y9.Text = lst[8].ToString(CurrFormatter);
            MultiyearAnalysisObject.Instance.OperatingExpensesPercs[9] = _MA_S3_OpEx_Y10.Text = lst[9].ToString(CurrFormatter);                        
        }

        void Calc_MA_VacancyPercPerYears()
        {
            List<decimal> lst = new List<decimal>();
            lst.Add(GetDecimalFromString(MultiyearAnalysisObject.Instance.VacancyCreditLossPerc));
            for (int i = 1; i < 10; i++)
            {
                lst.Add(lst[i - 1] * (1 + (decimal)((CustomItem)_MA_S2_VacancyPercChange.SelectedItem).ChangeValue / 100));
            }

            MultiyearAnalysisObject.Instance.VacancyPercs[0] = _MA_S3_Vacancy_Y1.Text = lst[0].ToString(CurrFormatter);
            MultiyearAnalysisObject.Instance.VacancyPercs[1] = _MA_S3_Vacancy_Y2.Text = lst[1].ToString(CurrFormatter);
            MultiyearAnalysisObject.Instance.VacancyPercs[2] = _MA_S3_Vacancy_Y3.Text = lst[2].ToString(CurrFormatter);
            MultiyearAnalysisObject.Instance.VacancyPercs[3] = _MA_S3_Vacancy_Y4.Text = lst[3].ToString(CurrFormatter);
            MultiyearAnalysisObject.Instance.VacancyPercs[4] = _MA_S3_Vacancy_Y5.Text = lst[4].ToString(CurrFormatter);
            MultiyearAnalysisObject.Instance.VacancyPercs[5] = _MA_S3_Vacancy_Y6.Text = lst[5].ToString(CurrFormatter);
            MultiyearAnalysisObject.Instance.VacancyPercs[6] = _MA_S3_Vacancy_Y7.Text = lst[6].ToString(CurrFormatter);
            MultiyearAnalysisObject.Instance.VacancyPercs[7] = _MA_S3_Vacancy_Y8.Text = lst[7].ToString(CurrFormatter);
            MultiyearAnalysisObject.Instance.VacancyPercs[8] = _MA_S3_Vacancy_Y9.Text = lst[8].ToString(CurrFormatter);
            MultiyearAnalysisObject.Instance.VacancyPercs[9] = _MA_S3_Vacancy_Y10.Text = lst[9].ToString(CurrFormatter); 
        }

        void Update_MA_PercentIncomeTable()
        {
            string val = ((CustomItem)_MA_S2_IncomePercChange.SelectedItem).ChangeValue.ToString(CurrFormatter);
            MultiyearAnalysisObject.Instance.IncomeChangePercs[0] = _MA_S3_IncomeChange_Y1.Text = val;
            MultiyearAnalysisObject.Instance.IncomeChangePercs[1] = _MA_S3_IncomeChange_Y2.Text = val;
            MultiyearAnalysisObject.Instance.IncomeChangePercs[2] = _MA_S3_IncomeChange_Y3.Text = val;
            MultiyearAnalysisObject.Instance.IncomeChangePercs[3] = _MA_S3_IncomeChange_Y4.Text = val;
            MultiyearAnalysisObject.Instance.IncomeChangePercs[4] = _MA_S3_IncomeChange_Y5.Text = val;
            MultiyearAnalysisObject.Instance.IncomeChangePercs[5] = _MA_S3_IncomeChange_Y6.Text = val;
            MultiyearAnalysisObject.Instance.IncomeChangePercs[6] = _MA_S3_IncomeChange_Y7.Text = val;
            MultiyearAnalysisObject.Instance.IncomeChangePercs[7] = _MA_S3_IncomeChange_Y8.Text = val;
            MultiyearAnalysisObject.Instance.IncomeChangePercs[8] = _MA_S3_IncomeChange_Y9.Text = val;
            MultiyearAnalysisObject.Instance.IncomeChangePercs[9] = _MA_S3_IncomeChange_Y10.Text = val;
        }
        #endregion

        #region MULTIYEAR ANALYSIS
        void On_MA_CopyFromProforma_Click(object sender, EventArgs e)
        {
            //GSI
            _MA_S1_GSI.Text = MultiyearAnalysisObject.Instance.GsiAnnual = FinancialInfoObject.Instance.Data[1].GsiAnnualProperty;
            //_MA_S1_GSI_Perc

            //VACANCY
            _MA_S1_Vacancy.Text = MultiyearAnalysisObject.Instance.VacancyCreditLoss = FinancialInfoObject.Instance.Data[1].VacancyCreditLossProperty;
            _MA_S1_VacancyPerc.Text = MultiyearAnalysisObject.Instance.VacancyCreditLossPerc = FinancialInfoObject.Instance.Data[1].VacancyCreditLossPercProperty;

            //OTHER INCOME
            _MA_S1_OtherIncome.Text = MultiyearAnalysisObject.Instance.OtherIncome = FinancialInfoObject.Instance.Data[1].OtherIncomeProperty;
            //_MA_S1_OtherIncomePerc.Text

            //OPERATING EXPENSES
            _MA_S1_OperatingExpenses.Text = MultiyearAnalysisObject.Instance.OperatingExpenses = FinancialInfoObject.Instance.Data[1].OperatingExpensesProperty;
            _MA_S1_OperatingExpensesPerc.Text = MultiyearAnalysisObject.Instance.OperatingExpensesPerc = FinancialInfoObject.Instance.Data[1].OperatingExpensesPercProperty; 
        }

        void On_MA_CopyFromActual_Click(object sender, EventArgs e)
        {            
            //GSI
            _MA_S1_GSI.Text = MultiyearAnalysisObject.Instance.GsiAnnual = FinancialInfoObject.Instance.Data[0].GsiAnnualProperty;
            //_MA_S1_GSI_Perc
            
            //VACANCY
            _MA_S1_Vacancy.Text = MultiyearAnalysisObject.Instance.VacancyCreditLoss = FinancialInfoObject.Instance.Data[0].VacancyCreditLossProperty;
            _MA_S1_VacancyPerc.Text = MultiyearAnalysisObject.Instance.VacancyCreditLossPerc = FinancialInfoObject.Instance.Data[0].VacancyCreditLossPercProperty;

            //OTHER INCOME
            _MA_S1_OtherIncome.Text = MultiyearAnalysisObject.Instance.OtherIncome = FinancialInfoObject.Instance.Data[0].OtherIncomeProperty;
            //_MA_S1_OtherIncomePerc.Text

            //OPERATING EXPENSES
            _MA_S1_OperatingExpenses.Text = MultiyearAnalysisObject.Instance.OperatingExpenses = FinancialInfoObject.Instance.Data[0].OperatingExpensesProperty;
            _MA_S1_OperatingExpensesPerc.Text = MultiyearAnalysisObject.Instance.OperatingExpensesPerc = FinancialInfoObject.Instance.Data[0].OperatingExpensesPercProperty;
        }

        void On_MA_copyFromLoanTab_Click(object sender, EventArgs e)
        {
            _MA_PurchasePrice.Text = MultiyearAnalysisObject.Instance.PurchasePrice = LoansObject.Instance.PurchasePriceProperty;
            _MA_DownPayment.Text = MultiyearAnalysisObject.Instance.DownPayement = LoansObject.Instance.DownPaymentProperty;
            
            decimal usdPoints = 0;
            for (int i = 0; i < LoansObject.Instance.LoansProperty.Length; i++)
                if (null != LoansObject.Instance.LoansProperty[i])
                    usdPoints += LoansObject.Instance.LoansProperty[i].PointsUSDValue;

            _MA_Points.Text = MultiyearAnalysisObject.Instance.Points = usdPoints.ToString(CurrFormatter);

            Update_MA_InitialInvestment();
        }                
        #endregion

        #region PRINT REPORTS
        void On_MA_PrintSelectedReports_Click(object sender, EventArgs e)
        {
            if (_MA_RS_IAR_BuildingInfo.Checked)
            {
                PrintReports(ReportOriginator.BuildingInfo, e);
            }

            if (_MA_RS_IAR_InitialIncomeSummary.Checked)
            {
                PrintReports(ReportOriginator.IncomeSummary, e);
            }

            if (_MA_RS_IAR_InitialExpensesSummary.Checked)
            {
                PrintReports(ReportOriginator.ExpensesSummary, e);
            }

            if (_MA_RS_IAR_LoanSummary.Checked)
            {
                PrintReports(ReportOriginator.LoanInfo, e);
            }

            if (_MA_RS_IAR_FinancialAnalisys.Checked)
            {
                PrintReports(ReportOriginator.FinancialAnalisys, e);
            }

            if (_MA_RS_MARG_CashFlow.Checked)
            {
                PrintReports(ReportOriginator.CashFlow, e);
            }

            if (_MA_RS_MARG_CashFlowGraph.Checked)
            {
                DrawGraph(ReportOriginator.CashFlow);
            }

            if (_MA_RS_MARG_NetProfitFromSale.Checked)
            {
                PrintReports(ReportOriginator.NetProfitFromSale, e);
            }

            if (_MA_RS_MARG_NetProfitGraph.Checked)
            {
                DrawGraph(ReportOriginator.NetProfitGraph);
            }

            if (_MA_RS_MARG_TaxesAndDeductions.Checked)
            {
                PrintReports(ReportOriginator.TaxesAndDeductions, e);
            }
        }

        void On_MA_RS_IAR_CheckedChanged(object sender, EventArgs e)
        {
            _MA_RS_IAR_SelectAll.CheckedChanged -= new EventHandler(On_MA_RS_IAR_CheckedChanged);
            _MA_RS_IAR_BuildingInfo.CheckedChanged -= new EventHandler(On_MA_RS_IAR_CheckedChanged);
            _MA_RS_IAR_InitialIncomeSummary.CheckedChanged -= new EventHandler(On_MA_RS_IAR_CheckedChanged);
            _MA_RS_IAR_InitialExpensesSummary.CheckedChanged -= new EventHandler(On_MA_RS_IAR_CheckedChanged);
            _MA_RS_IAR_LoanSummary.CheckedChanged -= new EventHandler(On_MA_RS_IAR_CheckedChanged);
            _MA_RS_IAR_FinancialAnalisys.CheckedChanged -= new EventHandler(On_MA_RS_IAR_CheckedChanged);

            if (sender == _MA_RS_IAR_SelectAll)
            {
                _MA_RS_IAR_BuildingInfo.Checked = true;
                _MA_RS_IAR_InitialIncomeSummary.Checked = true;
                _MA_RS_IAR_InitialExpensesSummary.Checked = true;
                _MA_RS_IAR_LoanSummary.Checked = true;
                _MA_RS_IAR_FinancialAnalisys.Checked = true;
            }
            else
                _MA_RS_IAR_SelectAll.Checked = false;

            _MA_RS_IAR_SelectAll.CheckedChanged += new EventHandler(On_MA_RS_IAR_CheckedChanged);
            _MA_RS_IAR_BuildingInfo.CheckedChanged += new EventHandler(On_MA_RS_IAR_CheckedChanged);
            _MA_RS_IAR_InitialIncomeSummary.CheckedChanged += new EventHandler(On_MA_RS_IAR_CheckedChanged);
            _MA_RS_IAR_InitialExpensesSummary.CheckedChanged += new EventHandler(On_MA_RS_IAR_CheckedChanged);
            _MA_RS_IAR_LoanSummary.CheckedChanged += new EventHandler(On_MA_RS_IAR_CheckedChanged);
            _MA_RS_IAR_FinancialAnalisys.CheckedChanged += new EventHandler(On_MA_RS_IAR_CheckedChanged);
        }

        void On_MA_RS_MARG_CheckedChanged(object sender, EventArgs e)
        {
            _MA_RS_MARG_SelectAll.CheckedChanged -= new EventHandler(On_MA_RS_MARG_CheckedChanged);
            _MA_RS_MARG_CashFlow.CheckedChanged -= new EventHandler(On_MA_RS_MARG_CheckedChanged);
            _MA_RS_MARG_CashFlowGraph.CheckedChanged -= new EventHandler(On_MA_RS_MARG_CheckedChanged);
            _MA_RS_MARG_NetProfitFromSale.CheckedChanged -= new EventHandler(On_MA_RS_MARG_CheckedChanged);
            _MA_RS_MARG_NetProfitGraph.CheckedChanged -= new EventHandler(On_MA_RS_MARG_CheckedChanged);
            _MA_RS_MARG_TaxesAndDeductions.CheckedChanged -= new EventHandler(On_MA_RS_MARG_CheckedChanged);            

            if (sender == _MA_RS_MARG_SelectAll)
            {
                _MA_RS_MARG_CashFlow.Checked = true;
                _MA_RS_MARG_CashFlowGraph.Checked = true;
                _MA_RS_MARG_NetProfitFromSale.Checked = true;
                _MA_RS_MARG_NetProfitGraph.Checked = true;
                _MA_RS_MARG_TaxesAndDeductions.Checked = true;
            }
            else
                _MA_RS_MARG_SelectAll.Checked = false;

            _MA_RS_MARG_SelectAll.CheckedChanged += new EventHandler(On_MA_RS_MARG_CheckedChanged);
            _MA_RS_MARG_CashFlow.CheckedChanged += new EventHandler(On_MA_RS_MARG_CheckedChanged);
            _MA_RS_MARG_CashFlowGraph.CheckedChanged += new EventHandler(On_MA_RS_MARG_CheckedChanged);
            _MA_RS_MARG_NetProfitFromSale.CheckedChanged += new EventHandler(On_MA_RS_MARG_CheckedChanged);
            _MA_RS_MARG_NetProfitGraph.CheckedChanged += new EventHandler(On_MA_RS_MARG_CheckedChanged);
            _MA_RS_MARG_TaxesAndDeductions.CheckedChanged += new EventHandler(On_MA_RS_MARG_CheckedChanged);            
        }
#endregion

        #region SET DATA
        public void SetData(string address)
        {
            _address = address;
        }
        
        public void SetData(FinancialInfoObject obj)
		{
			try
			{
				_FA_A_Price.Text = obj.Data[0].PriceProperty;
				_FA_A_GSIAnnual.Text = obj.Data[0].GsiAnnualProperty;
				_FA_A_VacancyCreditLoss.Text = obj.Data[0].VacancyCreditLossProperty;
				_FA_A_VacancyCreditLossPerc.Text = obj.Data[0].VacancyCreditLossPercProperty;
				_FA_A_OtherIncome.Text = obj.Data[0].OtherIncomeProperty;
				_FA_A_GOIAnnual.Text = obj.Data[0].GoiAnnualProperty;
				_FA_A_OperatingExpenses.Text = obj.Data[0].OperatingExpensesProperty;
				_FA_A_OperatingExpensesPerc.Text = obj.Data[0].OperatingExpensesPercProperty;
				_FA_A_NetOperatingIncome.Text = obj.Data[0].NetOperatingIncomeProperty;
				_FA_A_CapRate.Text = obj.Data[0].CapRateProperty;
				_FA_A_DebtService.Text = obj.Data[0].DebtServiceProperty;
				_FA_A_DebtCoverageRatio.Text = obj.Data[0].DebtCoverageRatioProperty;
				_FA_A_CashFlow.Text = obj.Data[0].CashFlowPreTaxProperty;
				_FA_A_CashOnCashReturn.Text = obj.Data[0].CashOnCashReturnProperty;
				_FA_A_Comment.Text = obj.Data[0].CommentsProperty;

				_FA_P_Price.Text = obj.Data[1].PriceProperty;
				_FA_P_GSIAnnual.Text = obj.Data[1].GsiAnnualProperty;
				_FA_P_VacancyCreditLoss.Text = obj.Data[1].VacancyCreditLossProperty;
				_FA_P_VacancyCreditLossPerc.Text = obj.Data[1].VacancyCreditLossPercProperty;
				_FA_P_OtherIncome.Text = obj.Data[1].OtherIncomeProperty;
				_FA_P_GOIAnnual.Text = obj.Data[1].GoiAnnualProperty;
				_FA_P_OperatingExpenses.Text = obj.Data[1].OperatingExpensesProperty;
				_FA_P_OperatingExpensesPerc.Text = obj.Data[1].OperatingExpensesPercProperty;
				_FA_P_NetOperatingIncome.Text = obj.Data[1].NetOperatingIncomeProperty;
				_FA_P_CapRate.Text = obj.Data[1].CapRateProperty;
				_FA_P_DebtService.Text = obj.Data[1].DebtServiceProperty;
				_FA_P_DebtCoverageRatio.Text = obj.Data[1].DebtCoverageRatioProperty;
				_FA_P_CashFlow.Text = obj.Data[1].CashFlowPreTaxProperty;
				_FA_P_CashOnCashReturn.Text = obj.Data[1].CashOnCashReturnProperty;
				_FA_P_Comment.Text = obj.Data[1].CommentsProperty;

				_FA_S_Price.Text = obj.Data[2].PriceProperty;
				_FA_S_GSIAnnual.Text = obj.Data[2].GsiAnnualProperty;
				_FA_S_VacancyCreditLoss.Text = obj.Data[2].VacancyCreditLossProperty;
				_FA_S_VacancyCreditLossPerc.Text = obj.Data[2].VacancyCreditLossPercProperty;
				_FA_S_OtherIncome.Text = obj.Data[2].OtherIncomeProperty;
				_FA_S_GOIAnnual.Text = obj.Data[2].GoiAnnualProperty;
				_FA_S_OperatingExpenses.Text = obj.Data[2].OperatingExpensesProperty;
				_FA_S_OperatingExpensesPerc.Text = obj.Data[2].OperatingExpensesPercProperty;
				_FA_S_NetOperatingIncome.Text = obj.Data[2].NetOperatingIncomeProperty;
				_FA_S_CapRate.Text = obj.Data[2].CapRateProperty;
				_FA_S_DebtService.Text = obj.Data[2].DebtServiceProperty;
				_FA_S_DebtCoverageRatio.Text = obj.Data[2].DebtCoverageRatioProperty;
				_FA_S_CashFlow.Text = obj.Data[2].CashFlowPreTaxProperty;
				_FA_S_CashOnCashReturn.Text = obj.Data[2].CashOnCashReturnProperty;
				_FA_S_Comment.Text = obj.Data[2].CommentsProperty;
				
			}
			catch (Exception exc)
			{
				MessageBox.Show(exc.ToString());
			}
		}

		public void SetData(LoansObject obj)
		{
			try
			{
			_L_PurchasePrice.Text = LoansObject.Instance.PurchasePriceProperty;
			_L_DownPayment.Text = LoansObject.Instance.DownPaymentProperty;
			_L_DownPaymentPerc.Text = LoansObject.Instance.DownPaymentPercValue.ToString("0.00");
			_L_BlendedRate.Text = LoansObject.Instance.BlendedRatePerc.ToString("0.000");
			_L_CLTVPerc.Text = LoansObject.Instance.CLTVPerc.ToString(CurrFormatter);

			_L_1stAssumable.SelectedItem = LoansObject.Instance.FirstAssumableProperty;
			_L_Assumable.SelectedItem = LoansObject.Instance.SecondAssumableProperty;
			_L_SellerCarryback.SelectedItem = LoansObject.Instance.SellerCarrybackProperty;


			//1st LOAN
			_L_1L_LTVPerc.Text = LoansObject.Instance.LoansProperty[0].LtvPercValue.ToString(CurrFormatter);
			_L_1L_Points.Text = LoansObject.Instance.LoansProperty[0].PointsUSDValue.ToString(CurrFormatter);
			_L_1L_PointsPerc.Text = LoansObject.Instance.LoansProperty[0].PointsPercProperty;
			_L_1L_Amount.Text = LoansObject.Instance.LoansProperty[0].AmountProperty;			

			_L_1L_Type.SelectedItem = (string.Empty != obj.LoansProperty[0].LoanTypeProperty) ? obj.LoansProperty[0].LoanTypeProperty : null;
			_L_1L_AmortTerm.Text = obj.LoansProperty[0].AmortTermProperty;
			_L_1L_MonthlyPayment.Text = obj.LoansProperty[0].CalculatePeriodicPaymentAmount("Fixed" == obj.LoansProperty[0].LoanTypeProperty ? true : false).ToString(CurrFormatter);						
			_L_1L_Rate.Text = obj.LoansProperty[0].RatePercProperty;
			_L_BlendedRate.Text = obj.BlendedRatePerc.ToString("0.000");
			_L_1L_MonthlyPayment.Text = obj.LoansProperty[0].CalculatePeriodicPaymentAmount("Fixed" == obj.LoansProperty[0].LoanTypeProperty  ? true : false).ToString(CurrFormatter);			
			_L_1L_MonthlyPayment.Text = obj.LoansProperty[0].MonthlyPaymentProperty;			
			_L_TotalPaymentsMonthly.Text = obj.TotalMonthlyPayments.ToString(CurrFormatter);
			_L_TotalPaymentsAnnual.Text = obj.TotalAnnualDebtService.ToString(CurrFormatter);
			_L_DCR.Text = CalculateDCR().ToString(CurrFormatter);
									
			_L_1L_FirstPaymentDate.Text = obj.LoansProperty[0].FirstPaymentDateProperty;
			_L_1L_LastPaymentDate.Text = obj.LoansProperty[0].LastPaymentDateProperty;
			_L_1L_Lender.Text = obj.LoansProperty[0].LenderProperty;
			_L_1L_RA_InitFixedPeriod.Text = obj.LoansProperty[0].LrAdjustmentsProperty.InitialFixedPeriodYrsProperty;
			_L_1L_RA_AnnualCap.Text = obj.LoansProperty[0].LrAdjustmentsProperty.AnnualCapPercProperty;
			_L_1L_RA_InitialRateCap.Text = obj.LoansProperty[0].LrAdjustmentsProperty.InitialRateCapPercProperty;
			_L_1L_RA_LifeTimeCap.Text = obj.LoansProperty[0].LrAdjustmentsProperty.LifeTimeCapPercProperty;
			_L_1L_RA_RateYear1.Text = obj.LoansProperty[0].LrAdjustmentsProperty.RatesProperty[0];
			_L_1L_RA_RateYear2.Text = obj.LoansProperty[0].LrAdjustmentsProperty.RatesProperty[1];
			_L_1L_RA_RateYear3.Text = obj.LoansProperty[0].LrAdjustmentsProperty.RatesProperty[2];
			_L_1L_RA_RateYear4.Text = obj.LoansProperty[0].LrAdjustmentsProperty.RatesProperty[3];
			_L_1L_RA_RateYear5.Text = obj.LoansProperty[0].LrAdjustmentsProperty.RatesProperty[4];
			_L_1L_RA_RateYear6.Text = obj.LoansProperty[0].LrAdjustmentsProperty.RatesProperty[5];
			_L_1L_RA_RateYear7.Text = obj.LoansProperty[0].LrAdjustmentsProperty.RatesProperty[6];
			_L_1L_RA_RateYear8.Text = obj.LoansProperty[0].LrAdjustmentsProperty.RatesProperty[7];
			_L_1L_RA_RateYear9.Text = obj.LoansProperty[0].LrAdjustmentsProperty.RatesProperty[8];
			_L_1L_RA_RateYear10.Text = obj.LoansProperty[0].LrAdjustmentsProperty.RatesProperty[9];
						

			//2nd LOAN
			_L_2L_LTVPerc.Text = LoansObject.Instance.LoansProperty[1].LtvPercValue.ToString(CurrFormatter);
			_L_2L_Points.Text = LoansObject.Instance.LoansProperty[1].PointsUSDValue.ToString(CurrFormatter);
			_L_2L_PointsPerc.Text = LoansObject.Instance.LoansProperty[1].PointsPercProperty;
			_L_2L_Amount.Text = LoansObject.Instance.LoansProperty[1].AmountProperty;

			_L_2L_Type.SelectedItem = (string.Empty != obj.LoansProperty[1].LoanTypeProperty) ? obj.LoansProperty[1].LoanTypeProperty : null;
			_L_2L_AmortTerm.Text = obj.LoansProperty[1].AmortTermProperty;
			_L_2L_MonthlyPayment.Text = obj.LoansProperty[1].CalculatePeriodicPaymentAmount("Fixed" == obj.LoansProperty[1].LoanTypeProperty  ? true : false).ToString(CurrFormatter);
			_L_2L_Rate.Text = obj.LoansProperty[1].RatePercProperty;
			_L_BlendedRate.Text = obj.BlendedRatePerc.ToString("0.000");
			_L_2L_MonthlyPayment.Text = obj.LoansProperty[1].CalculatePeriodicPaymentAmount("Fixed" == obj.LoansProperty[1].LoanTypeProperty ? true : false).ToString(CurrFormatter);
			_L_2L_MonthlyPayment.Text = obj.LoansProperty[1].MonthlyPaymentProperty;
			_L_TotalPaymentsMonthly.Text = obj.TotalMonthlyPayments.ToString(CurrFormatter);
			_L_TotalPaymentsAnnual.Text = obj.TotalAnnualDebtService.ToString(CurrFormatter);
			_L_DCR.Text = CalculateDCR().ToString(CurrFormatter);
			_L_2L_FirstPaymentDate.Text = obj.LoansProperty[1].FirstPaymentDateProperty;
			_L_2L_LastPaymentDate.Text = obj.LoansProperty[1].LastPaymentDateProperty;
			_L_2L_Lender.Text = obj.LoansProperty[1].LenderProperty;
			_L_2L_RA_InitFixedPeriod.Text = obj.LoansProperty[1].LrAdjustmentsProperty.InitialFixedPeriodYrsProperty;
			_L_2L_RA_AnnualCap.Text = obj.LoansProperty[1].LrAdjustmentsProperty.AnnualCapPercProperty;
			_L_2L_RA_InitialRateCap.Text = obj.LoansProperty[1].LrAdjustmentsProperty.InitialRateCapPercProperty;
			_L_2L_RA_LifeTimeCap.Text = obj.LoansProperty[1].LrAdjustmentsProperty.LifeTimeCapPercProperty;
			_L_2L_RA_RateYear1.Text = obj.LoansProperty[1].LrAdjustmentsProperty.RatesProperty[0];
			_L_2L_RA_RateYear2.Text = obj.LoansProperty[1].LrAdjustmentsProperty.RatesProperty[1];
			_L_2L_RA_RateYear3.Text = obj.LoansProperty[1].LrAdjustmentsProperty.RatesProperty[2];
			_L_2L_RA_RateYear4.Text = obj.LoansProperty[1].LrAdjustmentsProperty.RatesProperty[3];
			_L_2L_RA_RateYear5.Text = obj.LoansProperty[1].LrAdjustmentsProperty.RatesProperty[4];
			_L_2L_RA_RateYear6.Text	= obj.LoansProperty[1].LrAdjustmentsProperty.RatesProperty[5];
			_L_2L_RA_RateYear7.Text = obj.LoansProperty[1].LrAdjustmentsProperty.RatesProperty[6];
			_L_2L_RA_RateYear8.Text = obj.LoansProperty[1].LrAdjustmentsProperty.RatesProperty[7];
			_L_2L_RA_RateYear9.Text = obj.LoansProperty[1].LrAdjustmentsProperty.RatesProperty[8];
			_L_2L_RA_RateYear10.Text = obj.LoansProperty[1].LrAdjustmentsProperty.RatesProperty[9];
			
			
			//3rd LOAN
			_L_3L_LTVPerc.Text = LoansObject.Instance.LoansProperty[2].LtvPercValue.ToString(CurrFormatter);
			_L_3L_Points.Text = LoansObject.Instance.LoansProperty[2].PointsUSDValue.ToString(CurrFormatter);
			_L_3L_PointsPerc.Text = LoansObject.Instance.LoansProperty[2].PointsPercProperty;
			_L_3L_Amount.Text = LoansObject.Instance.LoansProperty[2].AmountProperty;

			_L_3L_Type.SelectedItem = (string.Empty != obj.LoansProperty[2].LoanTypeProperty) ? obj.LoansProperty[2].LoanTypeProperty : null; 
			_L_3L_AmortTerm.Text = obj.LoansProperty[2].AmortTermProperty;
			_L_3L_MonthlyPayment.Text = obj.LoansProperty[2].CalculatePeriodicPaymentAmount("Fixed" == obj.LoansProperty[2].LoanTypeProperty ? true : false).ToString(CurrFormatter);
			_L_3L_Rate.Text = obj.LoansProperty[2].RatePercProperty;
			_L_BlendedRate.Text = obj.BlendedRatePerc.ToString("0.000");
			_L_3L_MonthlyPayment.Text = obj.LoansProperty[2].CalculatePeriodicPaymentAmount("Fixed" == obj.LoansProperty[2].LoanTypeProperty ? true : false).ToString(CurrFormatter);
			_L_3L_MonthlyPayment.Text = obj.LoansProperty[2].MonthlyPaymentProperty;
			_L_TotalPaymentsMonthly.Text = obj.TotalMonthlyPayments.ToString(CurrFormatter);
			_L_TotalPaymentsAnnual.Text = obj.TotalAnnualDebtService.ToString(CurrFormatter);
			_L_DCR.Text = CalculateDCR().ToString(CurrFormatter);
			_L_3L_FirstPaymentDate.Text = obj.LoansProperty[2].FirstPaymentDateProperty;
			_L_3L_LastPaymentDate.Text = obj.LoansProperty[2].LastPaymentDateProperty;
			_L_3L_Lender.Text = obj.LoansProperty[2].LenderProperty;
			_L_3L_RA_InitFixedPeriod.Text = obj.LoansProperty[2].LrAdjustmentsProperty.InitialFixedPeriodYrsProperty;
			_L_3L_RA_AnnualCap.Text = obj.LoansProperty[2].LrAdjustmentsProperty.AnnualCapPercProperty;
			_L_3L_RA_InitialRateCap.Text = obj.LoansProperty[2].LrAdjustmentsProperty.InitialRateCapPercProperty;
			_L_3L_RA_LifeTimeCap.Text = obj.LoansProperty[2].LrAdjustmentsProperty.LifeTimeCapPercProperty;
			_L_3L_RA_RateYear1.Text = obj.LoansProperty[2].LrAdjustmentsProperty.RatesProperty[0];
			_L_3L_RA_RateYear2.Text = obj.LoansProperty[2].LrAdjustmentsProperty.RatesProperty[1];
			_L_3L_RA_RateYear3.Text = obj.LoansProperty[2].LrAdjustmentsProperty.RatesProperty[2];
			_L_3L_RA_RateYear4.Text = obj.LoansProperty[2].LrAdjustmentsProperty.RatesProperty[3];
			_L_3L_RA_RateYear5.Text = obj.LoansProperty[2].LrAdjustmentsProperty.RatesProperty[4];
			_L_3L_RA_RateYear6.Text = obj.LoansProperty[2].LrAdjustmentsProperty.RatesProperty[5];
			_L_3L_RA_RateYear7.Text = obj.LoansProperty[2].LrAdjustmentsProperty.RatesProperty[6];
			_L_3L_RA_RateYear8.Text = obj.LoansProperty[2].LrAdjustmentsProperty.RatesProperty[7];
			_L_3L_RA_RateYear9.Text = obj.LoansProperty[2].LrAdjustmentsProperty.RatesProperty[8];
			_L_3L_RA_RateYear10.Text = obj.LoansProperty[2].LrAdjustmentsProperty.RatesProperty[9];						
			}
			catch (Exception exc)
			{
				MessageBox.Show(exc.ToString());
			}
		}		

		public void SetData(BuildingInfoObject obj)
		{
			try
			{
			_BI_propertyName.Text = obj.PropertyName;
			_BI_apn.Text = obj.Apn;

			_BI_propertyValue.Text = obj.PropertyValue.ToString(CurrFormatter);
			
			_BI_totalSqFt.Text = obj.TotalSqFt.ToString();
			_BI_netRentableSqFt.Text = obj.NetRentableSqFt.ToString();			
			_BI_lotAcres.Text = obj.LotAcres.ToString();

			_BI_lotSqft.Text = obj.LotSqFt.ToString();			
			_BI_roofType.Text = obj.RoofType;
			_BI_windowType.Text = obj.WindowType;
			
			_BI_lotSpaces.Text = obj.ParkingLotSpaces.ToString();
			_BI_coveredSpaces.Text = obj.ParkingCoveredSpaces.ToString();
			_BI_garageSpaces.Text = obj.ParkingGarageSpaces.ToString();
			_BI_lotFee.Text = obj.ParkingLotFee.ToString();
			_BI_coveredFee.Text = obj.ParkingCoveredFee.ToString();			
			_BI_garageFee.Text = obj.ParkingGarageFee.ToString();
						
			_BI_comments.Text = obj.ParkingComments;			 

			_BI_lessor.Text = obj.Lessor;			 
			_BI_startDate.Text = obj.StartDate;
			_BI_endDate.Text = obj.EndDate;

			_BI_rent.Text = obj.Rent.ToString(CurrFormatter);
						
			_BI_landLeaseComments.Text = obj.LandLeaseComments;
			_BI_recentImprovements.Text = obj.RecentImprovements;
			_BI_improvementsNeeded.Text = obj.ImprovementsNeeded;
			_BI_propertyDescription.Text = obj.PropertyDescription;
			_BI_locationDescription.Text = obj.LocationDescription;
			_BI_amenities.Text = obj.Amenities;
			_BI_landscaping.Text = obj.Landscaping;
			
			_BI_totalSpaces.Text = obj.ParkingTotalSpaces.ToString();				
			_BI_totalFee.Text = obj.ParkingTotalFee.ToString(CurrFormatter);
			_BI_parkingRatio.Text = obj.ParkingRatio;
											
			//related to BI Total spaces - BEGIN
			//_I_OI_Parking.Text = _BI_totalFee.Text; 
			//_I_OI_Total.Text = IncomeObject.Instance.OtherIncomeObject.TotalProperty.ToString(CurrFormatter);			
			//END	
			

			
			_BI_propertyType.SelectedItem = string.Empty != obj.PropertyType ? obj.PropertyType : null;												
			_BI_class.SelectedItem = (string.Empty != obj.Class) ? obj.Class : null;			
			_BI_yearBuilt.SelectedItem = (string.Empty != obj.YearBuilt) ? obj.YearBuilt : null;
			_BI_rehabYear.SelectedItem = (string.Empty != obj.RehabYear) ? obj.RehabYear : null;
			_BI_units.SelectedItem = (string.Empty != obj.NrOfUnits) ? obj.NrOfUnits : null;
			_BI_floors.SelectedItem = (string.Empty != obj.NrOfFloors) ? obj.NrOfFloors : null;
			_BI_structures.SelectedItem = (string.Empty != obj.NrOfStructures) ? obj.NrOfStructures : null;
			_BI_roofAge.SelectedItem = (string.Empty != obj.RoofAge) ? obj.RoofAge : null;
			_BI_plumbingType.SelectedItem = (string.Empty != obj.PlumbingType) ? obj.PlumbingType : null;
			_BI_plumbingAge.SelectedItem = (string.Empty != obj.PlumbingAge) ? obj.PlumbingAge : null;
			_BI_electricalAge.SelectedItem = (string.Empty != obj.ElectricalAge) ? obj.ElectricalAge : null;
			_BI_sidingType.SelectedItem = (string.Empty != obj.SidingType) ? obj.SidingType : null;
			_BI_windowsAge.SelectedItem = (string.Empty != obj.WindowAge) ? obj.WindowAge : null;
			_BI_landLease.SelectedItem = (string.Empty != obj.LandLease) ? obj.LandLease : null;
			_BI_option.SelectedItem = (string.Empty != obj.LandLeaseOption) ? obj.LandLeaseOption : null;
			
			//_BI_parkingRatio.Text = BuildingInfoObject.Instance.ParkingRatio;						
			}
			catch (Exception exc)
			{
				MessageBox.Show(exc.ToString());
			}
		}						
		
		public void SetData(IncomeObject obj)
		{	
            //try
            //{
            ////UNIT TYPE SUMMARY
            //for (int i = 0; i < obj.UnitTypeSummaries.Length; i++)
            //{
            //    _I_unitTypeSummary.Items[i].SubItems[0].Text = obj.UnitTypeSummaries[i].NrOfUnitsProperty;
            //    _I_unitTypeSummary.Items[i].SubItems[1].Text = obj.UnitTypeSummaries[i].NrOfBedroomsProperty;
            //    _I_unitTypeSummary.Items[i].SubItems[2].Text = obj.UnitTypeSummaries[i].NrOfBathsProperty;
            //    _I_unitTypeSummary.Items[i].SubItems[3].Text = obj.UnitTypeSummaries[i].FloorPlanProperty;
            //    _I_unitTypeSummary.Items[i].SubItems[4].Text = obj.UnitTypeSummaries[i].OccupancyProperty;
            //    _I_unitTypeSummary.Items[i].SubItems[5].Text = obj.UnitTypeSummaries[i].SF_LowProperty;
            //    _I_unitTypeSummary.Items[i].SubItems[6].Text = obj.UnitTypeSummaries[i].SF_HighProperty;
            //    _I_unitTypeSummary.Items[i].SubItems[7].Text = obj.UnitTypeSummaries[i].AR_LowProperty;
            //    _I_unitTypeSummary.Items[i].SubItems[8].Text = obj.UnitTypeSummaries[i].AR_HighProperty;
            //    _I_unitTypeSummary.Items[i].SubItems[9].Text = obj.UnitTypeSummaries[i].ActualRentPerItem.ToString(CurrFormatter);
            //    _I_unitTypeSummary.Items[i].SubItems[10].Text = obj.UnitTypeSummaries[i].MR_LowProperty;
            //    _I_unitTypeSummary.Items[i].SubItems[11].Text = obj.UnitTypeSummaries[i].MR_HighProperty;
            //    _I_unitTypeSummary.Items[i].SubItems[12].Text = obj.UnitTypeSummaries[i].MarketRentPerItem.ToString(CurrFormatter);
            //}

            //_I_UTS_TotalUnits.Text = obj.UTS_TotalNumberOfUnits.ToString();
            //_I_UTS_TotalOccupancy.Text = obj.UTS_WeightedAverageTotalOccupancy.ToString("0.00") + "%";
            //_I_UTS_Total_SQ_Low.Text = obj.UTS_WeightedAverageTotalSqFtLow.ToString(CurrFormatter);
            //_I_UTS_Total_SQ_High.Text = obj.UTS_WeightedAverageTotalSqFtHigh.ToString(CurrFormatter);
            //_I_UTS_Total_AR_Low.Text = obj.UTS_SumTotalARLow.ToString(CurrFormatter);
            //_I_UTS_Total_AR_High.Text = obj.UTS_SumTotalARHigh.ToString(CurrFormatter);
            //_I_UTS_Total_AR_UsdPerSqFt.Text = obj.UTS_WeightedAverageARUSDPerSqFt.ToString(CurrFormatter);
            //_I_UTS_Total_MR_Low.Text = obj.UTS_SumTotalMRLow.ToString(CurrFormatter);
            //_I_UTS_Total_MR_High.Text = obj.UTS_SumTotalMRHigh.ToString(CurrFormatter);
            //_I_UTS_Total_MR_USDperSqFt.Text = obj.UTS_WeightedAverageMRUSDPerSqFt.ToString(CurrFormatter);

            ////RENT ROLL
            //for (int i = 0; i <	IncomeObject.Instance.RentRolls.Length; i++)
            //{
            //    _I_rentRoll.Items[i].SubItems[0].Text = obj.RentRolls[i].UnitNrProperty;
            //    _I_rentRoll.Items[i].SubItems[1].Text = obj.RentRolls[i].TennantNameProperty;
            //    _I_rentRoll.Items[i].SubItems[2].Text = obj.RentRolls[i].BedBathProperty;
            //    _I_rentRoll.Items[i].SubItems[3].Text = obj.RentRolls[i].SqftProperty;
            //    _I_rentRoll.Items[i].SubItems[4].Text = obj.RentRolls[i].SecDepProperty;
            //    _I_rentRoll.Items[i].SubItems[5].Text = obj.RentRolls[i].TennantRentProperty;
            //    _I_rentRoll.Items[i].SubItems[6].Text = obj.RentRolls[i].Sec8RentProperty;
            //    _I_rentRoll.Items[i].SubItems[7].Text = obj.RentRolls[i].TotalRentProperty.ToString(CurrFormatter);
            //    _I_rentRoll.Items[i].SubItems[8].Text = obj.RentRolls[i].MarketRentProperty;
            //    _I_rentRoll.Items[i].SubItems[9].Text = obj.RentRolls[i].OccupDateProperty;
            //    _I_rentRoll.Items[i].SubItems[10].Text = obj.RentRolls[i].LeaseTermProperty;
            //    _I_rentRoll.Items[i].SubItems[11].Text = obj.RentRolls[i].CommentProperty;

            //}

            //_I_RR_TotalSqFt.Text = obj.RR_TotalSqft.ToString(CurrFormatter);
            //_I_RR_TotalSecDep.Text = obj.RR_TotalSecDep.ToString(CurrFormatter);
			
            //_I_RR_TotalTennantRent.Text = obj.RR_TotalTennantRent.ToString(CurrFormatter);
            //_I_RR_TotalTotalRent.Text = obj.RR_TotalTotalRent.ToString(CurrFormatter);
			
            //_I_RR_TotalSec8Rent.Text = obj.RR_TotalSec8Rent.ToString(CurrFormatter);
            //_I_RR_TotalTotalRent.Text = obj.RR_TotalTotalRent.ToString(CurrFormatter);
            //_I_RR_TotalMarketRent.Text = obj.RR_TotalMarketRent.ToString(CurrFormatter);

            //_I_AI_OtherIncome.Text = obj.ActualIncomeObject.OtherIncomeProperty.ToString(CurrFormatter);;
            //_I_PI_GrossRent.Text = IncomeObject.Instance.ProformaIncomeObject.GrossRentProperty;

            ////OTHER INCOME
            //_I_OI_Parking.Text = obj.OtherIncomeObject.ParkingProperty;
            //_I_OI_Laundry.Text = obj.OtherIncomeObject.LaundryProperty;
            //_I_OI_Vending.Text = obj.OtherIncomeObject.VendingProperty;
            //_I_OI_Other.Text = obj.OtherIncomeObject.OtherProperty;
            //_I_OI_Total.Text = obj.OtherIncomeObject.TotalProperty.ToString(CurrFormatter);

            ////ACTUAL INCOME
            //_I_AI_VacancyOrCreditLossPerc.Text = obj.ActualIncomeObject.VacancyOrCreditLossPerc;
            //_I_AI_VacancyOrCreditLoss.Text = obj.ActualIncomeObject.VacancyOrCreditLossProperty.ToString(CurrFormatter);			
            //_I_AI_OtherIncome.Text = obj.ActualIncomeObject.OtherIncomeProperty.ToString(CurrFormatter);
            //_I_AI_OperatingIncome.Text = obj.ActualIncomeObject.OperatingIncomeProperty.ToString(CurrFormatter);						
            //_I_AI_GrossRent.Text = obj.ActualIncomeObject.GrossRentProperty.ToString(CurrFormatter);

            ////PROFORMA INCOME
            //_I_PI_VacancyOrCreditLoss.Text = obj.ProformaIncomeObject.VacancyOrCreditloss.ToString(CurrFormatter);
            //_I_PI_VacancyOrCreditLossPerc.Text = (obj.ProformaIncomeObject.VacancyOrCreditlossPerc * 100).ToString(CurrFormatter);
            //_I_PI_OperatingIncome.Text = obj.ProformaIncomeObject.OperatingIncome.ToString(CurrFormatter);
            //_I_PI_OtherIncome.Text = obj.ProformaIncomeObject.OtherIncome.ToString(CurrFormatter);			
            //_I_PI_GrossRent.Text = obj.ProformaIncomeObject.GrossRent.ToString(CurrFormatter);
            //}
            //catch (Exception exc)
            //{
            //    MessageBox.Show(exc.ToString());
            //}			
		}
		
		public void SetData(ExpensesObject obj)
		{
			try
			{
			try 
			{
				for (int i = 0; i < _E_expenses.Items.Count; i++)
				{
					_E_expenses.Items[i].SubItems[2].Text = ExpensesObject.Instance.ExpensesItemsProperty.Items[i].UsdPerMonthProperty;
					_E_expenses.Items[i].SubItems[3].Text = ExpensesObject.Instance.ExpensesItemsProperty.Items[i].UsdPerYearProperty; 
					_E_expenses.Items[i].SubItems[4].Text = ExpensesObject.Instance.ExpensesItemsProperty.Items[i].UsdPerSqFtProperty;
					_E_expenses.Items[i].SubItems[5].Text = ExpensesObject.Instance.ExpensesItemsProperty.Items[i].UsdPerUnitProperty;
					//_E_expenses.Items[i].SubItems[6].Text = 
					_E_expenses.Items[i].SubItems[7].Text = ExpensesObject.Instance.ExpensesItemsProperty.Items[i].PaidByProperty;
					_E_expenses.Items[i].SubItems[8].Text = ExpensesObject.Instance.ExpensesItemsProperty.Items[i].HighLowProperty;
					_E_expenses.Items[i].SubItems[9].Text = ExpensesObject.Instance.ExpensesItemsProperty.Items[i].CommentProperty;

					_E_expenses.Items[i].SubItems[3].Text = ExpensesObject.Instance.ExpensesItemsProperty.Items[i].USDPerYearValue.ToString(CurrFormatter);
					_E_expenses.Items[i].SubItems[2].Text = ExpensesObject.Instance.ExpensesItemsProperty.Items[i].USDPerMonthValue.ToString(CurrFormatter);
					_E_expenses.Items[i].SubItems[4].Text = ExpensesObject.Instance.ExpensesItemsProperty.Items[i].USDPerSqFtValue.ToString(CurrFormatter);
					_E_expenses.Items[i].SubItems[5].Text = ExpensesObject.Instance.ExpensesItemsProperty.Items[i].USDPerUnitValue.ToString(CurrFormatter);
				}
			}
			catch {}
			
			
				
							
			decimal usdPerMonthValueTotal = 0;
			decimal usdPerYearValueTotal = 0;
			decimal usdPerSqFtValueTotal = 0;
			decimal usdPerUnitValueTotal = 0;
				
				
			//ADMINISTRATIVE
			obj.ExpensesItemsProperty.GetExpensesTotal(ExpensesTypes.Administrative, out usdPerMonthValueTotal, out usdPerYearValueTotal, out usdPerSqFtValueTotal, out usdPerUnitValueTotal);

			decimal administrativeTotalUsdPerYearValue = usdPerYearValueTotal;
			_E_TotalUsdPerMonth_1.Text = usdPerMonthValueTotal.ToString(CurrFormatter);
			_E_TotalUsdPerYear_1.Text = usdPerYearValueTotal.ToString(CurrFormatter);
			_E_TotalUsdPerSqft_1.Text = usdPerSqFtValueTotal.ToString(CurrFormatter);
			_E_TotalUsdPerUnit_1.Text = usdPerUnitValueTotal.ToString(CurrFormatter);							

			//MANAGEMENT
			obj.ExpensesItemsProperty.GetExpensesTotal(ExpensesTypes.Management, out usdPerMonthValueTotal, out usdPerYearValueTotal, out usdPerSqFtValueTotal, out usdPerUnitValueTotal);

			decimal managementTotalUsdPerYearValue = usdPerYearValueTotal;
			_E_TotalUsdPerMonth_2.Text = usdPerMonthValueTotal.ToString(CurrFormatter);
			_E_TotalUsdPerYear_2.Text = usdPerYearValueTotal.ToString(CurrFormatter);
			_E_TotalUsdPerSqft_2.Text = usdPerSqFtValueTotal.ToString(CurrFormatter);
			_E_TotalUsdPerUnit_2.Text = usdPerUnitValueTotal.ToString(CurrFormatter);				

			//UTILITIES
			obj.ExpensesItemsProperty.GetExpensesTotal(ExpensesTypes.Utilities, out usdPerMonthValueTotal, out usdPerYearValueTotal, out usdPerSqFtValueTotal, out usdPerUnitValueTotal);

			decimal utilitiesTotalUsdPerYearValue = usdPerYearValueTotal;
			_E_TotalUsdPerMonth_3.Text = usdPerMonthValueTotal.ToString(CurrFormatter);
			_E_TotalUsdPerYear_3.Text = usdPerYearValueTotal.ToString(CurrFormatter);
			_E_TotalUsdPerSqft_3.Text = usdPerSqFtValueTotal.ToString(CurrFormatter);
			_E_TotalUsdPerUnit_3.Text = usdPerUnitValueTotal.ToString(CurrFormatter);				

			//INSURANCE
			obj.ExpensesItemsProperty.GetExpensesTotal(ExpensesTypes.Insurance, out usdPerMonthValueTotal, out usdPerYearValueTotal, out usdPerSqFtValueTotal, out usdPerUnitValueTotal);

			decimal insuranceTotalUsdPerYearValue = usdPerYearValueTotal;
			_E_TotalUsdPerMonth_4.Text = usdPerMonthValueTotal.ToString(CurrFormatter);
			_E_TotalUsdPerYear_4.Text = usdPerYearValueTotal.ToString(CurrFormatter);
			_E_TotalUsdPerSqft_4.Text = usdPerSqFtValueTotal.ToString(CurrFormatter);
			_E_TotalUsdPerUnit_4.Text = usdPerUnitValueTotal.ToString(CurrFormatter);				

			//OPERATING
			obj.ExpensesItemsProperty.GetExpensesTotal(ExpensesTypes.Operating, out usdPerMonthValueTotal, out usdPerYearValueTotal, out usdPerSqFtValueTotal, out usdPerUnitValueTotal);

			decimal operatingTotalUsdPerYearValue = usdPerYearValueTotal;
			_E_TotalUsdPerMonth_5.Text = usdPerMonthValueTotal.ToString(CurrFormatter);
			_E_TotalUsdPerYear_5.Text = usdPerYearValueTotal.ToString(CurrFormatter);
			_E_TotalUsdPerSqft_5.Text = usdPerSqFtValueTotal.ToString(CurrFormatter);
			_E_TotalUsdPerUnit_5.Text = usdPerUnitValueTotal.ToString(CurrFormatter);

			//TYPES TOTALs
			obj.ExpensesItemsProperty.GetExpensesTypesTotal(out usdPerMonthValueTotal, out usdPerYearValueTotal, out usdPerSqFtValueTotal, out usdPerUnitValueTotal);
			_E_TotalUsdPerMonth_6.Text = usdPerMonthValueTotal.ToString(CurrFormatter);
			_E_TotalUsdPerYear_6.Text = usdPerYearValueTotal.ToString(CurrFormatter);
			_E_TotalUsdPerSqft_6.Text = usdPerSqFtValueTotal.ToString(CurrFormatter);
			_E_TotalUsdPerUnit_6.Text = usdPerUnitValueTotal.ToString(CurrFormatter);

			ExpensesObject.Instance.TotalExpensesItemsProperty.Items[5].UsdPerYearProperty = usdPerYearValueTotal.ToString(CurrFormatter);

			//UPDATE PERC TOTALS PER TYPES
			if (0 != usdPerYearValueTotal)
			{
				_E_TotalPercOfTotal_1.Text = (administrativeTotalUsdPerYearValue / usdPerYearValueTotal * 100).ToString(CurrFormatter);
				_E_TotalPercOfTotal_2.Text = (managementTotalUsdPerYearValue / usdPerYearValueTotal * 100).ToString(CurrFormatter);
				_E_TotalPercOfTotal_3.Text = (utilitiesTotalUsdPerYearValue / usdPerYearValueTotal * 100).ToString(CurrFormatter);
				_E_TotalPercOfTotal_4.Text = (insuranceTotalUsdPerYearValue / usdPerYearValueTotal * 100).ToString(CurrFormatter);	
				_E_TotalPercOfTotal_5.Text = (operatingTotalUsdPerYearValue / usdPerYearValueTotal * 100).ToString(CurrFormatter);
			}
			else
			{
				_E_TotalPercOfTotal_1.Text = "N/A";
				_E_TotalPercOfTotal_2.Text = "N/A";
				_E_TotalPercOfTotal_3.Text = "N/A";
				_E_TotalPercOfTotal_4.Text = "N/A";	
				_E_TotalPercOfTotal_5.Text = "N/A";
			}

			//UPDATE PERC TOTALS PER EXPENSES
			for (int i = 0; i < ExpensesObject.MaxExpensesItems; i++)
			{
				ExpensesTypes currType = ExpensesTypes.Administrative;

				if (0 <= i && i <=5)
					currType = ExpensesTypes.Administrative;
				else if (6 <= i && i <= 8)
					currType = ExpensesTypes.Management;
				else if (9 <= i && i <= 13)
					currType = ExpensesTypes.Utilities;
				else if (14 <= i && i <= 19)
					currType = ExpensesTypes.Insurance;
				else if (20 <= i && i <= 33)
					currType = ExpensesTypes.Operating;	

				obj.ExpensesItemsProperty.GetExpensesTotal(currType, out usdPerMonthValueTotal, out usdPerYearValueTotal, out usdPerSqFtValueTotal, out usdPerUnitValueTotal);
				decimal totalYearExpensesForAllTypes = obj.ExpensesItemsProperty.GetTotalYearExpensesForAllTypes();

				if (0 != usdPerYearValueTotal)
					_E_expenses.Items[i].SubItems[6].Text = ((obj.ExpensesItemsProperty.Items[i].USDPerYearValue / totalYearExpensesForAllTypes/*usdPerYearValueTotal*/) * 100).ToString("0.00");
				else
					_E_expenses.Items[i].SubItems[6].Text = "N/A";
			}
	
			_E_UT_ElectricityMeter.SelectedItem = (string.Empty != ExpensesObject.Instance.UtilityTypesProperty.ElectricMeterProperty )? ExpensesObject.Instance.UtilityTypesProperty.ElectricMeterProperty : null;
			_E_UT_Heat.SelectedItem = (string.Empty != ExpensesObject.Instance.UtilityTypesProperty.HeatProperty) ? ExpensesObject.Instance.UtilityTypesProperty.HeatProperty : null;
			_E_UT_HotWater.SelectedItem = (string.Empty != ExpensesObject.Instance.UtilityTypesProperty.HotWaterProperty) ? ExpensesObject.Instance.UtilityTypesProperty.HotWaterProperty : null;

			//COSTS
			_E_CA_Cost_Year1.Text = ExpensesObject.Instance.CapitalAdditionsProperty.yearsProperty[0]._costProperty;									
			_E_CA_Cost_Year2.Text = ExpensesObject.Instance.CapitalAdditionsProperty.yearsProperty[1]._costProperty;									
			_E_CA_Cost_Year3.Text = ExpensesObject.Instance.CapitalAdditionsProperty.yearsProperty[2]._costProperty;									
			_E_CA_Cost_Year4.Text = ExpensesObject.Instance.CapitalAdditionsProperty.yearsProperty[3]._costProperty;								
			_E_CA_Cost_Year5.Text = ExpensesObject.Instance.CapitalAdditionsProperty.yearsProperty[4]._costProperty;								
			_E_CA_Cost_Year6.Text = ExpensesObject.Instance.CapitalAdditionsProperty.yearsProperty[5]._costProperty;								
			_E_CA_Cost_Year7.Text = ExpensesObject.Instance.CapitalAdditionsProperty.yearsProperty[6]._costProperty;								
			_E_CA_Cost_Year8.Text = ExpensesObject.Instance.CapitalAdditionsProperty.yearsProperty[7]._costProperty;								
			_E_CA_Cost_Year9.Text = ExpensesObject.Instance.CapitalAdditionsProperty.yearsProperty[8]._costProperty;									
			_E_CA_Cost_Year10.Text = ExpensesObject.Instance.CapitalAdditionsProperty.yearsProperty[9]._costProperty;

			//TYPE									
			_E_CA_Type_Year1.Text = ExpensesObject.Instance.CapitalAdditionsProperty.yearsProperty[0]._typeProperty;								
			_E_CA_Type_Year2.Text = ExpensesObject.Instance.CapitalAdditionsProperty.yearsProperty[1]._typeProperty;
			_E_CA_Type_Year3.Text = ExpensesObject.Instance.CapitalAdditionsProperty.yearsProperty[2]._typeProperty;												
			_E_CA_Type_Year4.Text = ExpensesObject.Instance.CapitalAdditionsProperty.yearsProperty[3]._typeProperty;								
			_E_CA_Type_Year5.Text = ExpensesObject.Instance.CapitalAdditionsProperty.yearsProperty[4]._typeProperty;
			_E_CA_Type_Year6.Text = ExpensesObject.Instance.CapitalAdditionsProperty.yearsProperty[5]._typeProperty;
			_E_CA_Type_Year7.Text = ExpensesObject.Instance.CapitalAdditionsProperty.yearsProperty[6]._typeProperty;								
			_E_CA_Type_Year8.Text = ExpensesObject.Instance.CapitalAdditionsProperty.yearsProperty[7]._typeProperty;								
			_E_CA_Type_Year9.Text = ExpensesObject.Instance.CapitalAdditionsProperty.yearsProperty[8]._typeProperty;								
			_E_CA_Type_Year10.Text = ExpensesObject.Instance.CapitalAdditionsProperty.yearsProperty[9]._typeProperty;				
			}
			catch (Exception exc)
			{
				MessageBox.Show(exc.ToString());
			}
		}

        public void SetData(MultiyearAnalysisObject obj)
        {
            try
            {
                _MA_PurchasePrice.Text = obj.PurchasePrice;
                _MA_InitialInvestment.Text = obj.InitialInvestments;
                _MA_DownPayment.Text = obj.DownPayement;
                _MA_Points.Text = obj.Points;
                _MA_OtherAcquisitionCosts.Text = obj.OtherAcquisitionCosts;
                _MA_S1_GSI.Text = obj.GsiAnnual;
                _MA_S1_GSI_Perc.Text = obj.GsiAnnualPerc;
                _MA_S1_Vacancy.Text = obj.VacancyCreditLoss;
                _MA_S1_VacancyPerc.Text = obj.VacancyCreditLossPerc;
                _MA_S1_OtherIncome.Text = obj.OtherIncome;
                _MA_S1_OtherIncomePerc.Text = obj.OtherIncomePerc;
                _MA_S1_OperatingExpenses.Text = obj.OperatingExpenses;
                _MA_S1_OperatingExpensesPerc.Text = obj.OperatingExpensesPerc;
                _MA_TI_FederalRate.Text = obj.FederalRate;
                _MA_TI_StateRate.Text = obj.StateRate;
                _MA_TI_CapitalGainsRate.Text = obj.CapitalGainsRate;
                _MA_TI_BuildingValue.Text = obj.BuildingValue;
                _MA_SI_SaleCosts.Text = obj.SaleCosts;
                _MA_SI_SaleCapRate.Text = obj.SaleCapRate;
                _MA_S3_OpEx_Y1.Text = obj.OperatingExpensesPercs[0];
                _MA_S3_OpEx_Y2.Text = obj.OperatingExpensesPercs[1];
                _MA_S3_OpEx_Y3.Text = obj.OperatingExpensesPercs[2];
                _MA_S3_OpEx_Y4.Text = obj.OperatingExpensesPercs[3];
                _MA_S3_OpEx_Y5.Text = obj.OperatingExpensesPercs[4];
                _MA_S3_OpEx_Y6.Text = obj.OperatingExpensesPercs[5];
                _MA_S3_OpEx_Y7.Text = obj.OperatingExpensesPercs[6];
                _MA_S3_OpEx_Y8.Text = obj.OperatingExpensesPercs[7];
                _MA_S3_OpEx_Y9.Text = obj.OperatingExpensesPercs[8];
                _MA_S3_OpEx_Y10.Text = obj.OperatingExpensesPercs[9];
                _MA_S3_Vacancy_Y1.Text = obj.VacancyPercs[0];
                _MA_S3_Vacancy_Y2.Text = obj.VacancyPercs[1];
                _MA_S3_Vacancy_Y3.Text = obj.VacancyPercs[2];
                _MA_S3_Vacancy_Y4.Text = obj.VacancyPercs[3];
                _MA_S3_Vacancy_Y5.Text = obj.VacancyPercs[4];
                _MA_S3_Vacancy_Y6.Text = obj.VacancyPercs[5];
                _MA_S3_Vacancy_Y7.Text = obj.VacancyPercs[6];
                _MA_S3_Vacancy_Y8.Text = obj.VacancyPercs[7];
                _MA_S3_Vacancy_Y9.Text = obj.VacancyPercs[8];
                _MA_S3_Vacancy_Y10.Text = obj.VacancyPercs[9];
                _MA_S3_IncomeChange_Y1.Text = obj.IncomeChangePercs[0];
                _MA_S3_IncomeChange_Y2.Text = obj.IncomeChangePercs[1];
                _MA_S3_IncomeChange_Y3.Text = obj.IncomeChangePercs[2];
                _MA_S3_IncomeChange_Y4.Text = obj.IncomeChangePercs[3];
                _MA_S3_IncomeChange_Y5.Text = obj.IncomeChangePercs[4];
                _MA_S3_IncomeChange_Y6.Text = obj.IncomeChangePercs[5];
                _MA_S3_IncomeChange_Y7.Text = obj.IncomeChangePercs[6];
                _MA_S3_IncomeChange_Y8.Text = obj.IncomeChangePercs[7];
                _MA_S3_IncomeChange_Y9.Text = obj.IncomeChangePercs[8];
                _MA_S3_IncomeChange_Y10.Text = obj.IncomeChangePercs[9];


                if (null != obj.AnnualInflationChange && string.Empty != obj.AnnualInflationChange)
                {
                    try
                    {
                        int temp = Convert.ToInt32(obj.AnnualInflationChange);
                        for (int i = 0; i < _MA_S2_InflationPercChange.Items.Count; i++)
                        {
                            if (((CustomItem)_MA_S2_InflationPercChange.Items[i]).ChangeValue == temp)
                            {
                                _MA_S2_InflationPercChange.SelectedItem = _MA_S2_InflationPercChange.Items[i];
                                break;
                            }
                        }                        
                    }
                    catch { _MA_S2_InflationPercChange.SelectedItem = null; }
                }
                else
                    _MA_S2_InflationPercChange.SelectedItem = null;

                if (null != obj.AnnualOperatingExpensesChange && string.Empty != obj.AnnualOperatingExpensesChange)
                {
                    try
                    {
                        int temp = Convert.ToInt32(obj.AnnualOperatingExpensesChange);
                        for (int i = 0; i < _MA_S2_OperatingExpensesPercChange.Items.Count; i++)
                        {
                            if (((CustomItem)_MA_S2_OperatingExpensesPercChange.Items[i]).ChangeValue == temp)
                            {
                                _MA_S2_OperatingExpensesPercChange.SelectedItem = _MA_S2_OperatingExpensesPercChange.Items[i];
                                break;
                            }
                        }
                    }
                    catch { _MA_S2_OperatingExpensesPercChange.SelectedItem = null; }
                }
                else
                    _MA_S2_OperatingExpensesPercChange.SelectedItem = null;

                if (null != obj.AnnualVacancyChange && string.Empty != obj.AnnualVacancyChange)
                {
                    try
                    {
                        int temp = Convert.ToInt32(obj.AnnualVacancyChange);
                        for (int i = 0; i < _MA_S2_VacancyPercChange.Items.Count; i++)
                        {
                            if (((CustomItem)_MA_S2_VacancyPercChange.Items[i]).ChangeValue == temp)
                            {
                                _MA_S2_VacancyPercChange.SelectedItem = _MA_S2_VacancyPercChange.Items[i];
                                break;
                            }
                        }
                    }
                    catch { _MA_S2_VacancyPercChange.SelectedItem = null; }
                }
                else
                    _MA_S2_VacancyPercChange.SelectedItem = null;

                if (null != obj.AnnualIncomeChange && string.Empty != obj.AnnualIncomeChange)
                {
                    try
                    {
                        int temp = Convert.ToInt32(obj.AnnualIncomeChange);
                        for (int i = 0; i < _MA_S2_IncomePercChange.Items.Count; i++)
                        {
                            if (((CustomItem)_MA_S2_IncomePercChange.Items[i]).ChangeValue == temp)
                            {
                                _MA_S2_IncomePercChange.SelectedItem = _MA_S2_IncomePercChange.Items[i];
                                break;
                            }
                        }
                    }
                    catch { _MA_S2_IncomePercChange.SelectedItem = null; }
                }
                else
                    _MA_S2_IncomePercChange.SelectedItem = null;

                if (null != obj.DepreciableYears && string.Empty != obj.DepreciableYears)
                {
                    try
                    {
                        decimal temp = Convert.ToDecimal(obj.DepreciableYears);
                        for (int i = 0; i < _MA_TI_DepreciableYears.Items.Count; i++)
                        {
                            if (_MA_TI_DepreciableYears.Items[i].ToString() == temp.ToString())
                                _MA_TI_DepreciableYears.SelectedItem = _MA_TI_DepreciableYears.Items[i];
                        }
                    }
                    catch { _MA_TI_DepreciableYears.SelectedItem = null; }
                }
                else
                    _MA_TI_DepreciableYears.SelectedItem = null;                                                                                
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.ToString());
            }
                        
        }
		#endregion

		#region FLOATERS
		private void On_bi_floater_MouseHover(object sender, EventArgs e)
		{
			_bi_FloaterForm = new BuildingInfoFloater();

			Point pReadMe = _bi_floater.Location;
			Size fSize = _bi_FloaterForm.Size;

			_bi_FloaterForm.Location = PointToScreen(new Point (pReadMe.X - fSize.Width, pReadMe.Y) );
			_bi_FloaterForm.Show();
		}

		private void On_bi_floater_MouseLeave(object sender, EventArgs e)
		{
			try 
			{
				_bi_FloaterForm.Hide();
			}
			catch 
			{}
		}

		private void On_i_floater_MouseHover(object sender, EventArgs e)
		{
			//_i_FloaterForm = new IncomeFloater();

            //Point pReadMe = _i_floater.Location;
			Size fSize = _i_FloaterForm.Size;

			//_i_FloaterForm.Location = PointToScreen(new Point (pReadMe.X - fSize.Width, pReadMe.Y) );
			//_i_FloaterForm.Show();
		}

		private void On_i_floater_MouseLeave(object sender, EventArgs e)
		{
			try
			{
				_i_FloaterForm.Hide();
			}
			catch{}
		}

		private void On_e_floater_MouseHover(object sender, EventArgs e)
		{
			_e_FloaterForm = new ExpensesFloater();

			Point pReadMe = _e_floater.Location;
			Size fSize = _e_FloaterForm.Size;

			_e_FloaterForm.Location = PointToScreen(new Point (pReadMe.X - fSize.Width, pReadMe.Y) );
			_e_FloaterForm.Show();
		}

		private void On_e_floater_MouseLeave(object sender, EventArgs e)
		{
			try
			{
			_e_FloaterForm.Hide();
			}
			catch{}
		}

		private void On_l_floater_MouseHover(object sender, EventArgs e)
		{
			_l_FloaterForm = new LoansFloater();

			Point pReadMe = _l_floater.Location;
			Size fSize = _l_FloaterForm.Size;

			_l_FloaterForm.Location = PointToScreen(new Point (pReadMe.X - fSize.Width, pReadMe.Y) );
			_l_FloaterForm.Show();
		}

		private void On_l_floater_MouseLeave(object sender, EventArgs e)
		{
			try
			{
			_l_FloaterForm.Hide();
			}
			catch{}
		}

		private void On_fa_floater_MouseHover(object sender, EventArgs e)
		{
			_fa_FloaterForm = new FinancialAnalisysFloater();

			Point pReadMe = _fa_floater.Location;
			Size fSize = _fa_FloaterForm.Size;

			_fa_FloaterForm.Location = PointToScreen(new Point (pReadMe.X - fSize.Width, pReadMe.Y) );
			_fa_FloaterForm.Show();
		}				

		private void On_fa_floater_MouseLeave(object sender, EventArgs e)
		{
			try
			{
			_fa_FloaterForm.Hide();
			}
			catch{}
		}
		#endregion

        #region COMMETNED OUT
        private void On_CalculateFromRentRoll_Click(object sender, EventArgs e)
		{
			//BuildingInfoObject.Instance.SerializeToStream();
			//BuildingInfoObject obj = BuildingInfoObject.Instance.DeserializeFromStream();

			//IncomeObject.Instance.SerializeToStream();
			//IncomeObject obj = IncomeObject.Instance.DeserializeFromStream();

			//ExpensesObject.Instance.SerializeToStream();
			//ExpensesObject obj = ExpensesObject.Instance.DeserializeFromStream();				

			//LoansObject.Instance.SerializeToStream();	
			//LoansObject obj = LoansObject.Instance.DeserializeFromStream();				

			//			if (DateTime.Now > DateTime.MinValue)
			//			{
			//				FinancialInfoObject.Instance.SerializeToStream();								
			//			}
			//			else
			//			{
			//				FinancialInfoObject obj = FinancialInfoObject.Instance.DeserializeFromStream();					
			//				SetData(obj);
			//			}
        }
        #endregion

        #region DRAW GRAPH
        void DrawGraph(ReportOriginator ro)
        {
            if (ReportOriginator.CashFlow == ro)
            {
                int nrOfYears = 1;
                if (null != _MA_RS_MARG_AnalysisPeriod.SelectedItem)
                    nrOfYears = ((CustomYearItem)_MA_RS_MARG_AnalysisPeriod.SelectedItem).Value;

                _cfGen.InitGenerator(MultiyearAnalysisObject.Instance, ExpensesObject.Instance, LoansObject.Instance, nrOfYears);

                CashFlow cf = new CashFlow(_cfGen, nrOfYears);
                cf.ShowDialog();
            }
            if (ReportOriginator.NetProfitGraph== ro)
            {
                int nrOfYears = 1;
                if (null != _MA_RS_MARG_AnalysisPeriod.SelectedItem)
                    nrOfYears = ((CustomYearItem)_MA_RS_MARG_AnalysisPeriod.SelectedItem).Value;

                NetProfitFromSale cf = new NetProfitFromSale(_npotdGen, nrOfYears);
                cf.ShowDialog();
            }
        }
        #endregion

        #region ON PRINT REPORTS (GENERATE REPORTS)
        void On_PrintReports_Click(object sender, EventArgs e)
        {            
            if (sender == _printBuildingInfo)
            {                                
                PrintReports(ReportOriginator.BuildingInfo, e);
            }
            //else if (sender == _printIncomeSummary)
            //{                
            //    PrintReports(ReportOriginator.IncomeSummary, e);
            //}
            //else if (sender == _printRentRoll)
            //{                
            //    PrintReports(ReportOriginator.IncomeRentRoll, e);
            //}
            else if (sender == _printExpensesSummary)
            {                
                PrintReports(ReportOriginator.ExpensesSummary, e);
            }
            else if (sender == _PrintExpensesDetails)
            {                
                PrintReports(ReportOriginator.ExpensesDetails, e);
            }
            else if (sender == _printFinancialAnalysis)
            {                
                PrintReports(ReportOriginator.FinancialAnalisys, e);
            }
            else if (sender == _printLoanInfo)
            {                
                PrintReports(ReportOriginator.LoanInfo, e);
            }
        }

        void PrintReports(ReportOriginator ro, EventArgs e)
        {
            PageSettings pageSettings = null;
            PageSetupDialog pageSetupDialog = new PageSetupDialog();
			pageSetupDialog.PageSettings = new PageSettings();
			pageSetupDialog.PrinterSettings = new PrinterSettings();
			pageSetupDialog.ShowNetwork = false;

            pageSetupDialog.PageSettings.PaperSize = new PaperSize("A4", 827, 1169);
            pageSetupDialog.PageSettings.Margins = new Margins(0, 0, 0, 0);
            			

            //if (DialogResult.OK == pageSetupDialog.ShowDialog())
            //{
            //    pageSettings = new PageSettings(pageSetupDialog.PrinterSettings);
            //    pageSettings.PaperSize = pageSetupDialog.PageSettings.PaperSize;
            //    pageSettings.Margins = pageSetupDialog.PageSettings.Margins;								
            //}

            PrintDialog pd = new PrintDialog();
            pd.UseEXDialog = true;

            PrintDocument tmpprndoc = new PrintDocument();
            
            if (ReportOriginator.BuildingInfo == ro)
            {
                tmpprndoc.PrintPage += new PrintPageEventHandler(On_PrintBuildingInfo);
                tmpprndoc.EndPrint += new PrintEventHandler(OnPrintBuildingInfo_EndPrint);

            }
            else if (ReportOriginator.IncomeSummary == ro)
            {
                tmpprndoc.DefaultPageSettings.Landscape = true;
                tmpprndoc.PrintPage += new PrintPageEventHandler(On_PrintIncomeSummary);
                tmpprndoc.EndPrint += new PrintEventHandler(On_PrintIncome_EndPrint);
            }
            else if (ReportOriginator.IncomeRentRoll == ro)
            {
                tmpprndoc.DefaultPageSettings.Landscape = true;
                tmpprndoc.PrintPage += new PrintPageEventHandler(On_PrintRentRoll);
                tmpprndoc.EndPrint += new PrintEventHandler(On_PrintIncome_EndPrint);
            }
            else if (ReportOriginator.ExpensesSummary == ro)
            {
                tmpprndoc.DefaultPageSettings.Landscape = true;
                tmpprndoc.PrintPage += new PrintPageEventHandler(On_PrintExpensesSummary);
                tmpprndoc.EndPrint += new PrintEventHandler(On_PrintExpenses_EndPrint);
            }
            else if (ReportOriginator.ExpensesDetails == ro)
            {
                tmpprndoc.DefaultPageSettings.Landscape = true;
                tmpprndoc.PrintPage += new PrintPageEventHandler(On_PrintExpensesDetails);
                tmpprndoc.EndPrint += new PrintEventHandler(On_PrintExpenses_EndPrint);
            }
            else if (ReportOriginator.FinancialAnalisys == ro)
            {
                tmpprndoc.DefaultPageSettings.Landscape = true;
                tmpprndoc.PrintPage += new PrintPageEventHandler(On_PrintFinancialAnalysis);
                tmpprndoc.EndPrint += new PrintEventHandler(On_FinancialAnalysis_EndPrint);
            }
            else if (ReportOriginator.LoanInfo == ro)
            {
                tmpprndoc.DefaultPageSettings.Landscape = false;
                tmpprndoc.PrintPage += new PrintPageEventHandler(On_PrintLoanInfo);
                tmpprndoc.EndPrint += new PrintEventHandler(On_LoanInfo_EndPrint);
            }

            else if (ReportOriginator.CashFlow == ro)
            {
                tmpprndoc.DefaultPageSettings.Landscape = true;
                tmpprndoc.PrintPage += new PrintPageEventHandler(On_PrintCashFlow);
                tmpprndoc.EndPrint += new PrintEventHandler(On_CashFlow_EndPrint);
            }
            else if (ReportOriginator.NetProfitFromSale == ro)
            {
                tmpprndoc.DefaultPageSettings.Landscape = true;
                tmpprndoc.PrintPage += new PrintPageEventHandler(On_PrintNetProfitFromSale);
                tmpprndoc.EndPrint += new PrintEventHandler(On_NetProfitFromSale_EndPrint);
            }
            else if (ReportOriginator.TaxesAndDeductions == ro)
            {
                tmpprndoc.DefaultPageSettings.Landscape = true;
                tmpprndoc.PrintPage += new PrintPageEventHandler(On_PrintTaxesAndDeductions);
                tmpprndoc.EndPrint += new PrintEventHandler(On_TaxesAndDeductionsEndPrint);
            }


            pd.Document = tmpprndoc;
            PrintPreviewDialogEx tmpprdiag = new PrintPreviewDialogEx();
           
            tmpprdiag.Document = tmpprndoc;
            //tmpprdiag.Document.DefaultPageSettings = pageSettings;
            tmpprdiag.ShowDialog();
        }
                
        #endregion

        #region RESET GENERATORS

        void On_TaxesAndDeductionsEndPrint(object sender, PrintEventArgs e)
        {
            _npotdGen.ResetGen();
        }

        void On_NetProfitFromSale_EndPrint(object sender, PrintEventArgs e)
        {
            _npotdGen.ResetGen();
        }

        void On_CashFlow_EndPrint(object sender, PrintEventArgs e)
        {
            _cfGen.ResetGen();
        }

        void On_LoanInfo_EndPrint(object sender, PrintEventArgs e)
        {
            _lGen.ResetGen();
        }        

        void On_FinancialAnalysis_EndPrint(object sender, PrintEventArgs e)
        {
            _fGen.ResetGen();
        }

        void On_PrintExpenses_EndPrint(object sender, PrintEventArgs e)
        {
            _eGen.ResetGen();
        }        

        void On_PrintIncome_EndPrint(object sender, PrintEventArgs e)
        {
            _iGen.ResetGen();
        }

        void OnPrintBuildingInfo_EndPrint(object sender, PrintEventArgs e)
        {
            _biGen.ResetGen();
        }
        #endregion

        #region START GENERATORS
        void On_PrintTaxesAndDeductions(object sender, PrintPageEventArgs e)
        {
            int nrOfYears = 1;
            if (null != _MA_RS_MARG_AnalysisPeriod.SelectedItem)
                nrOfYears = ((CustomYearItem)_MA_RS_MARG_AnalysisPeriod.SelectedItem).Value;

            _npotdGen.GenerateTaxesAndDeduction(e, MultiyearAnalysisObject.Instance, ExpensesObject.Instance, LoansObject.Instance, nrOfYears);
        }
        
        void On_PrintNetProfitFromSale(object sender, PrintPageEventArgs e)
        {
            int nrOfYears = 1;
            if (null != _MA_RS_MARG_AnalysisPeriod.SelectedItem)
                nrOfYears = ((CustomYearItem)_MA_RS_MARG_AnalysisPeriod.SelectedItem).Value;

            _npotdGen.Generate(e, MultiyearAnalysisObject.Instance, ExpensesObject.Instance, LoansObject.Instance, nrOfYears);            
        }

        void On_PrintCashFlow(object sender, PrintPageEventArgs e)
        {
            int nrOfYears = 1;
            if (null != _MA_RS_MARG_AnalysisPeriod.SelectedItem)
                nrOfYears = ((CustomYearItem)_MA_RS_MARG_AnalysisPeriod.SelectedItem).Value;

            _cfGen.Generate(e, MultiyearAnalysisObject.Instance, ExpensesObject.Instance, LoansObject.Instance, nrOfYears);            
        }

        void On_PrintLoanInfo(object sender, PrintPageEventArgs e)
        {
            _lGen.GenerateDetails(e, LoansObject.Instance, _address);            
        }

        private void On_PrintBuildingInfo(object sender, PrintPageEventArgs e)
        {
            _biGen.Generate(e, BuildingInfoObject.Instance, _address);            
        }

        private void On_PrintIncomeSummary(object sender, PrintPageEventArgs e)
        {
            _iGen.GenerateSummary(e, IncomeObject.Instance, _address);
        }

        private void On_PrintRentRoll(object sender, PrintPageEventArgs e)
        {
            _iGen.GenerateRentRoll(e, IncomeObject.Instance, _address);
        }

        private void On_PrintExpensesSummary(object sender, PrintPageEventArgs e)
        {
            _eGen.GenerateSummary(e, ExpensesObject.Instance, _address);
        }

        private void On_PrintExpensesDetails(object sender, PrintPageEventArgs e)
        {
            _eGen.GenerateDetails(e, ExpensesObject.Instance, _address);
        }

        private void On_PrintFinancialAnalysis(object sender, PrintPageEventArgs e)
        {
            _fGen.GenerateReport(e, FinancialInfoObject.Instance, _address);
        }
        #endregion

        #region GET DECIMAL FROM STRING
        decimal GetDecimalFromString(string text)
        {
            decimal retVal = 0;
            try
            {
                if (string.Empty != text)
                {
                    retVal = Convert.ToDecimal(text);
                }
                else
                {
                    retVal = 0;
                }
            }
            catch 
            { 
                retVal = 0; 
            }
            return retVal;
        }
        #endregion


        void On_BI_propertyType_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox cb = (ComboBox)sender;
            SwitchIncomeTab(cb.SelectedIndex);                       
        }

        Income_Multifamily i_mf = null;
        Income_RetailOrOffice i_retail = null;
        Income_RetailOrOffice i_office = null;
        Income_SelfStorage i_storage = null;
        Income_Dummy i_dummy = null;

        public void SwitchIncomeTab(int selectedTabIdx)
        {
            _generalIncome.Controls.Clear();

            switch (selectedTabIdx)
            {
                case 0:                    
                    i_retail = null;
                    i_office = null;
                    i_storage = null;
                    i_dummy = null;

                    i_mf = new Income_Multifamily();
                    i_mf.Dock = DockStyle.Fill;
                    i_mf.SetAutoSaveInterface(_autosaveInterface);
                    _generalIncome.Controls.Add(i_mf);                    
                    break;

                case 1:
                    i_mf = null;                    
                    i_office = null;
                    i_storage = null;
                    i_dummy = null;

                    i_retail = new Income_RetailOrOffice();
                    i_retail.Dock = DockStyle.Fill;
                    i_retail.SetAutoSaveInterface(_autosaveInterface);
                    _generalIncome.Controls.Add(i_retail);                    
                    break;

                case 2:
                    i_mf = null;
                    i_retail = null;                    
                    i_storage = null;
                    i_dummy = null;

                    i_office = new Income_RetailOrOffice();
                    i_office.Dock = DockStyle.Fill;
                    i_office.SetAutoSaveInterface(_autosaveInterface);
                    _generalIncome.Controls.Add(i_office);                    
                    break;

                case 3:
                    i_mf = null;
                    i_retail = null;
                    i_office = null;                    
                    i_dummy = null;

                    i_storage = new Income_SelfStorage();
                    i_storage.Dock = DockStyle.Fill;
                    i_storage.SetAutoSaveInterface(_autosaveInterface);
                    _generalIncome.Controls.Add(i_storage);
                    break;

                default:
                    i_mf = null;
                    i_retail = null;
                    i_office = null;
                    i_storage = null;                    

                    i_dummy = new Income_Dummy();
                    i_dummy.Dock = DockStyle.Fill;
                    _generalIncome.Controls.Add(i_dummy);
                    break;

            }
        }

        public void SetIncomeData(int selectedTabIdx, IncomeObject incomeObject)
        {
            switch (selectedTabIdx)
            {
                case 0:
                    i_mf.SetAutoSaveInterface(_autosaveInterface);
                    i_mf.SetData(incomeObject);
                    break;

                case 1:
                    i_retail.SetAutoSaveInterface(_autosaveInterface);
                    i_retail.SetData(incomeObject);
                    break;

                case 2:
                    i_office.SetAutoSaveInterface(_autosaveInterface);
                    i_office.SetData(incomeObject);
                    break;

                case 3:
                    i_storage.SetAutoSaveInterface(_autosaveInterface);
                    i_storage.SetData(incomeObject);
                    break;

                default:                    
                    break;

            }
        }

        public void DisableControl()
        {
            this.Controls.Remove(_masterTabControl);
            Label dummyLabel = new Label();
            dummyLabel.Text = "In order to use new Commercials features, please activate Commercials Module";
            dummyLabel.Location = new Point(10, 10);
            dummyLabel.AutoSize = true;
            this.Controls.Add(dummyLabel);
        }         
    }
}
