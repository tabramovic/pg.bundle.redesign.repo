using System.Reflection;
using System.Runtime.CompilerServices;

//
// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
//
[assembly: AssemblyTitle("")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("")]
[assembly: AssemblyCopyright("")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]		

//
// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Revision and Build Numbers 
// by using the '*' as shown below:

[assembly: AssemblyVersion("4.6.0.0")]

//4.6.0.0 - Disable Control
//4.5.0.0 - SelfStorage
//4.4.0.0 - BugFixing
//4.3.0.0 - BugFixing
//4.2.1.0 - Additinal Impl
//4.2.0.0 - Phase#3
//4.1.0.0 - Graph adjustments
//4.0.0.0 - Formula adjustments + change specs
//3.9.0.0 - Formula adjustments
//3.8.0.0 - NetProfitFromSale (graph cosmetics + formulas corrections)
//3.7.0.0 - NetProfitFromSale graph + report, Taxes and Deductions graph
//3.6.0.0 - CashFlow report + graph adjustments
//3.5.0.0 - CashFlow report + graph
//3.4.0.0 - Phase#1 reports fixed as Trevor noted
//3.3.0.0 - Phase#2 dev
//3.2.0.0 - Phase#2 calculations added
//3.1.0.0 - Phase#1 REPORTS ADDED
//3.0.0.0 - multiyear analisys - GUI, autosave, reload, currency formatter, DB + DALC expansion
//2.9.0.0 - bugfixes
//2.8.0.0 - bugfixes
//2.7.0.0 - bugfixes
//2.6.0.0 - bugfixes
//2.5.0.0 - integrated floaters
//2.4.0.0 - integrated into PG (DB Layer auto create, auto save, auto populate)
//2.3.0.0 - persist all commercials objects, SetDataToGUI for (BI, I , E) + cosmetics
//2.2.0.0 - Labels, graphs cosmetics
//2.1.0.0 - DCR, additionaly triggered calculations
//2.0.0.0 - Graph changes, added currency formatting.
//1.9.0.0 - Graph Improvements
//1.8.0.0 - Improvements
//1.7.0.0 - Financial Analysis + Graphs
//1.6.0.0 - Loans
//1.5.0.0 - Expenses 
//1.4.0.0 - Expenses Part1
//1.3.0.0 - Income bugfixes
//1.2.0.0 - BuildingInfo & Income GUI + Objects + Formulas
//1.1.0.0 - Building Info GUI mapped to object
//1.0.0.0 - GUI

//
// In order to sign your assembly you must specify a key to use. Refer to the 
// Microsoft .NET Framework documentation for more information on assembly signing.
//
// Use the attributes below to control which key is used for signing. 
//
// Notes: 
//   (*) If no key is specified, the assembly is not signed.
//   (*) KeyName refers to a key that has been installed in the Crypto Service
//       Provider (CSP) on your machine. KeyFile refers to a file which contains
//       a key.
//   (*) If the KeyFile and the KeyName values are both specified, the 
//       following processing occurs:
//       (1) If the KeyName can be found in the CSP, that key is used.
//       (2) If the KeyName does not exist and the KeyFile does exist, the key 
//           in the KeyFile is installed into the CSP and used.
//   (*) In order to create a KeyFile, you can use the sn.exe (Strong Name) utility.
//       When specifying the KeyFile, the location of the KeyFile should be
//       relative to the project output directory which is
//       %Project Directory%\obj\<configuration>. For example, if your KeyFile is
//       located in the project directory, you would specify the AssemblyKeyFile 
//       attribute as [assembly: AssemblyKeyFile("..\\..\\mykey.snk")]
//   (*) Delay Signing is an advanced option - see the Microsoft .NET Framework
//       documentation for more information on this.
//
[assembly: AssemblyDelaySign(false)]
[assembly: AssemblyKeyFile("")]
[assembly: AssemblyKeyName("")]
