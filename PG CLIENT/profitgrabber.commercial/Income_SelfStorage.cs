﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Printing;
using System.Data;
using System.Text;
using System.Windows.Forms;

//Added
using ProfitGrabber.Common;
using ProfitGrabber.Commercial.Objects;
using ProfitGrabber.Commercial.Generators;

namespace ProfitGrabber.Commercial
{
    public partial class Income_SelfStorage : UserControl
    {
        IAutoSave _autoSave = null;
        const string CurrFormatter = "#,##0.00";
        string _address = string.Empty;

        IncomeGenerator _iGen = new IncomeGenerator();

        public Income_SelfStorage()
        {
            InitializeComponent();
            InitIncomeListViews();
            AddEventHandlers();
            HandleListViewExes();
        }

        protected override void OnLoad(EventArgs e)
        {
            //base.OnLoad(e);
            HandleListViewExes();
        }

        public void SetAutoSaveInterface(IAutoSave iAutoSaveIfc)
        {
            _autoSave = iAutoSaveIfc;
            AddAutoSaveEventHandlers();
        }


        void AddEventHandlers()
        {
            this._I_PI_VacancyOrCreditLossPerc.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.I_P_VacancyPerc_OnKeyPressed);
            this._I_PI_VacancyOrCreditLossPerc.Leave += new System.EventHandler(this.I_P_VacancyPerc_OnTextBoxLeave);
            this._I_PI_OtherIncome.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.I_P_OtherIncome_OnKeyPressed);
            this._I_PI_OtherIncome.Leave += new System.EventHandler(this.I_P_OtherIncome_OnTextBoxLeave);
            this._I_PI_GrossRent.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.I_P_GSI_OnKeyPressed);
            this._I_AI_VacancyOrCreditLossPerc.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.I_AI_LossPerc_OnKeyPressed);
            this._I_AI_VacancyOrCreditLossPerc.Leave += new System.EventHandler(this.I_AI_LossPerc_OnTextBoxLeave);
            this._I_AI_VacancyOrCreditLossPerc.Enter += new System.EventHandler(this.OnTextBoxEnter);
            this._I_AI_VacancyOrCreditLoss.Enter += new System.EventHandler(this.OnTextBoxEnter);
            this._I_AI_GrossRent.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.I_AI_GSI_OnKeyPressed);
            this._I_AI_GrossRent.Leave += new System.EventHandler(this.I_AI_GSI_OnTextBoxLeave);
            this._I_AI_GrossRent.Enter += new System.EventHandler(this.OnTextBoxEnter);

            //List View            
            _I_Summary.OnListViewExItemChanged += new ListViewEx.ListViewExItemChanged(On_I_Summary_OnListViewExItemChanged);
            _printIncomeReport.Click += new EventHandler(On_PrintIncomeReport_Click);

        }        

        void RemoveEventHandlers()
        {
            this._I_PI_VacancyOrCreditLossPerc.KeyPress -= new System.Windows.Forms.KeyPressEventHandler(this.I_P_VacancyPerc_OnKeyPressed);
            this._I_PI_VacancyOrCreditLossPerc.Leave -= new System.EventHandler(this.I_P_VacancyPerc_OnTextBoxLeave);
            this._I_PI_OtherIncome.KeyPress -= new System.Windows.Forms.KeyPressEventHandler(this.I_P_OtherIncome_OnKeyPressed);
            this._I_PI_OtherIncome.Leave -= new System.EventHandler(this.I_P_OtherIncome_OnTextBoxLeave);
            this._I_PI_GrossRent.KeyPress -= new System.Windows.Forms.KeyPressEventHandler(this.I_P_GSI_OnKeyPressed);
            this._I_AI_VacancyOrCreditLossPerc.KeyPress -= new System.Windows.Forms.KeyPressEventHandler(this.I_AI_LossPerc_OnKeyPressed);
            this._I_AI_VacancyOrCreditLossPerc.Leave -= new System.EventHandler(this.I_AI_LossPerc_OnTextBoxLeave);
            this._I_AI_VacancyOrCreditLossPerc.Enter -= new System.EventHandler(this.OnTextBoxEnter);
            this._I_AI_VacancyOrCreditLoss.Enter -= new System.EventHandler(this.OnTextBoxEnter);
            this._I_AI_GrossRent.KeyPress -= new System.Windows.Forms.KeyPressEventHandler(this.I_AI_GSI_OnKeyPressed);
            this._I_AI_GrossRent.Leave -= new System.EventHandler(this.I_AI_GSI_OnTextBoxLeave);
            this._I_AI_GrossRent.Enter -= new System.EventHandler(this.OnTextBoxEnter);

        }

        #region INIT LISTVIEW
        void InitIncomeListViews()
        {
            for (int i = 0; i < IncomeObject.UTSMaxItems; i++)
            {
                ListViewItem lvi = new ListViewItem();
                lvi.SubItems.Add(string.Empty);
                lvi.SubItems.Add(string.Empty);
                lvi.SubItems.Add(string.Empty);
                lvi.SubItems.Add(string.Empty);
                lvi.SubItems.Add(string.Empty);
                lvi.SubItems.Add(string.Empty);
                lvi.SubItems.Add(string.Empty);
                lvi.SubItems.Add(string.Empty);
                lvi.SubItems.Add(string.Empty);
                lvi.SubItems.Add(string.Empty);
                lvi.SubItems.Add(string.Empty);
                lvi.SubItems.Add(string.Empty);

                //Add Tag
                lvi.Tag = i;

                //Color readonly fields
                _I_Summary.Items.Add(lvi);
                _I_Summary.Items[i].UseItemStyleForSubItems = false;
                _I_Summary.Items[i].SubItems[7].BackColor = Color.LightGray;
                _I_Summary.Items[i].SubItems[10].BackColor = Color.LightGray;                
            }
        }
        #endregion

        #region HANDLE LIST VIEWS
        void HandleListViewExes()
        {
            //UNIT TYPE SUMMARY
            _I_Summary.AddSubItem = true;
            _I_Summary.HideComboAfterSelChange = true;

            //Add EDITABLE cells
            _I_Summary.AddEditableCell(-1, 0);
            _I_Summary.AddEditableCell(-1, 3);
            _I_Summary.AddEditableCell(-1, 4);
            _I_Summary.AddEditableCell(-1, 5);
            _I_Summary.AddEditableCell(-1, 6);
            //_I_Summary.AddEditableCell(-1, 7);
            _I_Summary.AddEditableCell(-1, 8);
            _I_Summary.AddEditableCell(-1, 9);
            //_I_Summary.AddEditableCell(-1, 10);
            _I_Summary.AddEditableCell(-1, 11);

            //Create data for COMBOBOX
            StringCollection size = new StringCollection();
            size.AddRange(new string[] { string.Empty, "5x5", "5x10", "5x15", "8x24", "10x10", "10x12", "10x15", "10x20", "10x25", "10x30", "10x35", "12x35", "14x30", "Parking", });

            StringCollection floor = new StringCollection();
            floor.AddRange(new string[] { string.Empty, "First", "Upper" });

            //Set COMBOBOXES
            _I_Summary.AddComboBoxCell(-1, 1, size);
            _I_Summary.AddComboBoxCell(-1, 2, floor);
        }
        #endregion


        #region INCOME TEXTBOX HANDLERS
        private void I_AI_GSI_OnTextBoxLeave(object sender, System.EventArgs e)
        {
            HandleActualIncomes_GSI();
        }

        private void I_AI_GSI_OnKeyPressed(object sender, System.Windows.Forms.KeyPressEventArgs e)
        {
            if (e.KeyChar == 13 || e.KeyChar == 27)
                HandleActualIncomes_GSI();
        }

        void HandleActualIncomes_GSI()
        {
            //SET NEW PROPERTY (take from GUI)
            IncomeObject.Instance.ActualIncomeObject.GrossRent = _I_AI_GrossRent.Text;

            try
            {
                //TRY TO CONVERT TO CURR FORMATTER
                decimal dTemp = decimal.Parse(_I_AI_GrossRent.Text);
                IncomeObject.Instance.ActualIncomeObject.GrossRent = dTemp.ToString(CurrFormatter);
                _I_AI_GrossRent.Text = IncomeObject.Instance.ActualIncomeObject.GrossRent;
            }
            catch
            {
                //AS IS
                IncomeObject.Instance.ActualIncomeObject.GrossRent = _I_AI_GrossRent.Text;
            }

            IncomeObject.Instance.ActualIncomeObject.VacancyOrCreditLossPerc = _I_AI_VacancyOrCreditLossPerc.Text;


            //CALCULATE THE OTHER and set to GUI			
            _I_AI_VacancyOrCreditLoss.Text = IncomeObject.Instance.ActualIncomeObject.VacancyOrCreditLossProperty.ToString(CurrFormatter);
            _I_AI_OtherIncome.Text = IncomeObject.Instance.ActualIncomeObject.OtherIncomeProperty.ToString(CurrFormatter);
            _I_AI_OperatingIncome.Text = IncomeObject.Instance.ActualIncomeObject.OperatingIncomeProperty.ToString(CurrFormatter);

            //FORCE 0.00 FORMAT
            _I_AI_GrossRent.Text = IncomeObject.Instance.ActualIncomeObject.GrossRentProperty.ToString(CurrFormatter);
        }

        private void I_AI_LossPerc_OnTextBoxLeave(object sender, System.EventArgs e)
        {
            HandleActualIncomes_LossPerc();
        }

        private void I_AI_LossPerc_OnKeyPressed(object sender, System.Windows.Forms.KeyPressEventArgs e)
        {
            if (e.KeyChar == 13 || e.KeyChar == 27)
                HandleActualIncomes_LossPerc();
        }

        void HandleActualIncomes_LossPerc()
        {
            //SET NEW PROPERTY (take from GUI)			
            try
            {
                //TRY TO CONVERT TO CURR FORMATTER
                decimal dTemp = decimal.Parse(_I_AI_GrossRent.Text);
                IncomeObject.Instance.ActualIncomeObject.GrossRent = dTemp.ToString(CurrFormatter);
                _I_AI_GrossRent.Text = IncomeObject.Instance.ActualIncomeObject.GrossRent;
            }
            catch
            {
                //AS IS
                IncomeObject.Instance.ActualIncomeObject.GrossRent = _I_AI_GrossRent.Text;
            }

            IncomeObject.Instance.ActualIncomeObject.VacancyOrCreditLossPerc = _I_AI_VacancyOrCreditLossPerc.Text;

            //CALCULATE THE OTHER and set to GUI
            _I_AI_VacancyOrCreditLoss.Text = (IncomeObject.Instance.ActualIncomeObject.VacancyOrCreditLossProperty).ToString(CurrFormatter);
            _I_AI_OtherIncome.Text = IncomeObject.Instance.ActualIncomeObject.OtherIncomeProperty.ToString(CurrFormatter);
            _I_AI_OperatingIncome.Text = IncomeObject.Instance.ActualIncomeObject.OperatingIncomeProperty.ToString(CurrFormatter);

            //FORCE 0.00 FORMAT
            _I_AI_GrossRent.Text = IncomeObject.Instance.ActualIncomeObject.GrossRentProperty.ToString(CurrFormatter);
        }

        private void I_P_GSI_OnTextBoxLeave(object sender, System.EventArgs e)
        {
            HandleActualIncomes_P_GSI();
        }

        private void I_P_GSI_OnKeyPressed(object sender, System.Windows.Forms.KeyPressEventArgs e)
        {
            if (e.KeyChar == 13 || e.KeyChar == 27)
                HandleActualIncomes_P_GSI();
        }

        void HandleActualIncomes_P_GSI()
        {
            //SET PROPERTIES
            IncomeObject.Instance.ProformaIncomeObject.GrossRentProperty = _I_PI_GrossRent.Text;

            //CALCULATE THE OTHER and set to GUI
            _I_PI_VacancyOrCreditLoss.Text = IncomeObject.Instance.ProformaIncomeObject.VacancyOrCreditloss.ToString(CurrFormatter);
            _I_PI_OperatingIncome.Text = IncomeObject.Instance.ProformaIncomeObject.OperatingIncome.ToString(CurrFormatter);
            _I_PI_OtherIncome.Text = IncomeObject.Instance.ProformaIncomeObject.OtherIncome.ToString(CurrFormatter);

            //FORCE 0.00 FORMAT
            _I_PI_GrossRent.Text = IncomeObject.Instance.ProformaIncomeObject.GrossRent.ToString(CurrFormatter);
        }

        private void I_P_VacancyPerc_OnTextBoxLeave(object sender, System.EventArgs e)
        {
            HandleActualIncomes_P_VacancyPerc();
        }

        private void I_P_VacancyPerc_OnKeyPressed(object sender, System.Windows.Forms.KeyPressEventArgs e)
        {
            if (e.KeyChar == 13 || e.KeyChar == 27)
                HandleActualIncomes_P_VacancyPerc();
        }

        void HandleActualIncomes_P_VacancyPerc()
        {
            //SET PROPERTIES
            IncomeObject.Instance.ProformaIncomeObject.VacancyOrCreditLossPercProperty = _I_PI_VacancyOrCreditLossPerc.Text;

            //CALCULATE THE OTHER and set to GUI
            _I_PI_VacancyOrCreditLoss.Text = IncomeObject.Instance.ProformaIncomeObject.VacancyOrCreditloss.ToString(CurrFormatter);
            _I_PI_OperatingIncome.Text = IncomeObject.Instance.ProformaIncomeObject.OperatingIncome.ToString(CurrFormatter);
            _I_PI_OtherIncome.Text = IncomeObject.Instance.ProformaIncomeObject.OtherIncome.ToString(CurrFormatter);
        }

        private void I_P_OtherIncome_OnTextBoxLeave(object sender, System.EventArgs e)
        {
            HandleActualIncomes_P_OtherPerc();
        }

        private void I_P_OtherIncome_OnKeyPressed(object sender, System.Windows.Forms.KeyPressEventArgs e)
        {
            if (e.KeyChar == 13 || e.KeyChar == 27)
                HandleActualIncomes_P_OtherPerc();
        }

        void HandleActualIncomes_P_OtherPerc()
        {
            //SET PROPERTIES
            IncomeObject.Instance.ProformaIncomeObject.OtherIncomeProperty = _I_PI_OtherIncome.Text;

            //CALCULATE THE OTHER and set to GUI
            _I_PI_VacancyOrCreditLoss.Text = IncomeObject.Instance.ProformaIncomeObject.VacancyOrCreditloss.ToString(CurrFormatter);
            _I_PI_OperatingIncome.Text = IncomeObject.Instance.ProformaIncomeObject.OperatingIncome.ToString(CurrFormatter);
            _I_PI_OtherIncome.Text = IncomeObject.Instance.ProformaIncomeObject.OtherIncome.ToString(CurrFormatter);
        }
        #endregion

        private void OnTextBoxEnter(object sender, System.EventArgs e)
        {
            //			TextBox tb = (TextBox)sender;
            //			tb.BackColor = Color.LightCyan;
        }

        #region SET DATA
        public void SetData(IncomeObject obj)
        {
            for (int i = 0; i < IncomeObject.SSMaxItems; i++)
            {
                _I_Summary.Items[i].SubItems[0].Text = IncomeObject.Instance.SelfStorageSummaryObject[i].Units;
                _I_Summary.Items[i].SubItems[1].Text = IncomeObject.Instance.SelfStorageSummaryObject[i].Size;
                _I_Summary.Items[i].SubItems[2].Text = IncomeObject.Instance.SelfStorageSummaryObject[i].Floor;
                _I_Summary.Items[i].SubItems[3].Text = IncomeObject.Instance.SelfStorageSummaryObject[i].Occupancy;
                _I_Summary.Items[i].SubItems[4].Text = IncomeObject.Instance.SelfStorageSummaryObject[i].SqFt;
                _I_Summary.Items[i].SubItems[5].Text = IncomeObject.Instance.SelfStorageSummaryObject[i].AR_Low;
                _I_Summary.Items[i].SubItems[6].Text = IncomeObject.Instance.SelfStorageSummaryObject[i].AR_High;
                _I_Summary.Items[i].SubItems[7].Text = IncomeObject.Instance.SelfStorageSummaryObject[i].ARPerItem.ToString(CurrFormatter);
                _I_Summary.Items[i].SubItems[8].Text = IncomeObject.Instance.SelfStorageSummaryObject[i].MR_Low;
                _I_Summary.Items[i].SubItems[9].Text = IncomeObject.Instance.SelfStorageSummaryObject[i].MR_High;
                _I_Summary.Items[i].SubItems[10].Text = IncomeObject.Instance.SelfStorageSummaryObject[i].MRPerItem.ToString(CurrFormatter);
                _I_Summary.Items[i].SubItems[11].Text = IncomeObject.Instance.SelfStorageSummaryObject[i].Comment;
            }
            
            _I_S_TotalUnits.Text = IncomeObject.Instance.GetTotal_SS_Units().ToString(CurrFormatter);
            _I_S_TotalOccupancy.Text = IncomeObject.Instance.GetTotal_SS_Occupancy().ToString(CurrFormatter);
            _I_S_TotalSqFt.Text = IncomeObject.Instance.GetTotal_SS_SqFt().ToString(CurrFormatter);
            _I_S_AR_Low.Text = IncomeObject.Instance.GetTotal_SS_AR_Low().ToString(CurrFormatter);
            _I_S_AR_High.Text = IncomeObject.Instance.GetTotal_SS_AR_High().ToString(CurrFormatter);
            _I_S_AR_UsdPerSqFt.Text = IncomeObject.Instance.GetTotal_SS_AR_UsdPerSqFt().ToString(CurrFormatter);
            _I_S_MR_Low.Text = IncomeObject.Instance.GetTotal_SS_MR_Low().ToString(CurrFormatter);
            _I_S_MR_High.Text = IncomeObject.Instance.GetTotal_SS_MR_High().ToString(CurrFormatter);
            _I_S_MR_UsdPerSqFt.Text = IncomeObject.Instance.GetTotal_SS_MR_UsdPerSqFt().ToString(CurrFormatter);                

            try
            {                               
                //ACTUAL INCOME
                _I_AI_VacancyOrCreditLossPerc.Text = obj.ActualIncomeObject.VacancyOrCreditLossPerc;
                _I_AI_VacancyOrCreditLoss.Text = obj.ActualIncomeObject.VacancyOrCreditLossProperty.ToString(CurrFormatter);
                _I_AI_OtherIncome.Text = obj.ActualIncomeObject.OtherIncomeProperty.ToString(CurrFormatter);
                _I_AI_OperatingIncome.Text = obj.ActualIncomeObject.OperatingIncomeProperty.ToString(CurrFormatter);
                _I_AI_GrossRent.Text = obj.ActualIncomeObject.GrossRentProperty.ToString(CurrFormatter);

                //PROFORMA INCOME
                _I_PI_VacancyOrCreditLoss.Text = obj.ProformaIncomeObject.VacancyOrCreditloss.ToString(CurrFormatter);
                _I_PI_VacancyOrCreditLossPerc.Text = (obj.ProformaIncomeObject.VacancyOrCreditlossPerc * 100).ToString(CurrFormatter);
                _I_PI_OperatingIncome.Text = obj.ProformaIncomeObject.OperatingIncome.ToString(CurrFormatter);
                _I_PI_OtherIncome.Text = obj.ProformaIncomeObject.OtherIncome.ToString(CurrFormatter);
                _I_PI_GrossRent.Text = obj.ProformaIncomeObject.GrossRent.ToString(CurrFormatter);
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.ToString());
            }
        }
        #endregion

        void On_I_Summary_OnListViewExItemChanged(int row, int col, string val)
        {
            if (-1 != row && -1 != col)
            {
                //MAP TO OBJECT MODEL
                switch (col)
                {
                    case 0:
                        IncomeObject.Instance.SelfStorageSummaryObject[row].Units = val;
                        break;

                    case 1:
                        IncomeObject.Instance.SelfStorageSummaryObject[row].Size = val;
                        
                        switch (val)
                        {
                            case "5x5":
                                _I_Summary.Items[row].SubItems[4].Text = "25";
                                IncomeObject.Instance.SelfStorageSummaryObject[row].SqFt = "25";
                                break;

                            case "5x10":
                                _I_Summary.Items[row].SubItems[4].Text = "50";
                                IncomeObject.Instance.SelfStorageSummaryObject[row].SqFt = "50";
                                break;

                            case "5x15":
                                _I_Summary.Items[row].SubItems[4].Text = "75";
                                IncomeObject.Instance.SelfStorageSummaryObject[row].SqFt = "75";
                                break;

                            case "8x24":
                                _I_Summary.Items[row].SubItems[4].Text = "192";
                                IncomeObject.Instance.SelfStorageSummaryObject[row].SqFt = "192";
                                break;

                            case "10x10":
                                _I_Summary.Items[row].SubItems[4].Text = "100";
                                IncomeObject.Instance.SelfStorageSummaryObject[row].SqFt = "100";
                                break;

                            case "10x12":
                                _I_Summary.Items[row].SubItems[4].Text = "120";
                                IncomeObject.Instance.SelfStorageSummaryObject[row].SqFt = "120";
                                break;

                            case "10x15":
                                _I_Summary.Items[row].SubItems[4].Text = "150";
                                IncomeObject.Instance.SelfStorageSummaryObject[row].SqFt = "150";
                                break;

                            case "10x20":
                                _I_Summary.Items[row].SubItems[4].Text = "200";
                                IncomeObject.Instance.SelfStorageSummaryObject[row].SqFt = "200";
                                break;

                            case "10x25":
                                _I_Summary.Items[row].SubItems[4].Text = "250";
                                IncomeObject.Instance.SelfStorageSummaryObject[row].SqFt = "250";
                                break;

                            case "10x30":
                                _I_Summary.Items[row].SubItems[4].Text = "300";
                                IncomeObject.Instance.SelfStorageSummaryObject[row].SqFt = "300";
                                break;

                            case "10x35":
                                _I_Summary.Items[row].SubItems[4].Text = "350";
                                IncomeObject.Instance.SelfStorageSummaryObject[row].SqFt = "350";
                                break;

                            case "12x35":
                                _I_Summary.Items[row].SubItems[4].Text = "420";
                                IncomeObject.Instance.SelfStorageSummaryObject[row].SqFt = "420";
                                break;

                            case "14x30":
                                _I_Summary.Items[row].SubItems[4].Text = "420";
                                IncomeObject.Instance.SelfStorageSummaryObject[row].SqFt = "420";
                                break;                            
                        }

                        break;

                    case 2:
                        IncomeObject.Instance.SelfStorageSummaryObject[row].Floor = val;
                        break;

                    case 3:
                        IncomeObject.Instance.SelfStorageSummaryObject[row].Occupancy = val;
                        break;

                    case 4:
                        IncomeObject.Instance.SelfStorageSummaryObject[row].SqFt = val;
                        break;

                    case 5:
                        IncomeObject.Instance.SelfStorageSummaryObject[row].AR_Low = val;
                        _I_Summary.Items[row].SubItems[7].Text = IncomeObject.Instance.SelfStorageSummaryObject[row].ARPerItem.ToString(CurrFormatter);
                        break;

                    case 6:
                        IncomeObject.Instance.SelfStorageSummaryObject[row].AR_High = val;
                        _I_Summary.Items[row].SubItems[7].Text = IncomeObject.Instance.SelfStorageSummaryObject[row].ARPerItem.ToString(CurrFormatter);
                        break;
                    
                    case 8:
                        IncomeObject.Instance.SelfStorageSummaryObject[row].MR_Low = val;
                        _I_Summary.Items[row].SubItems[10].Text = IncomeObject.Instance.SelfStorageSummaryObject[row].MRPerItem.ToString(CurrFormatter);
                        break;

                    case 9:
                        IncomeObject.Instance.SelfStorageSummaryObject[row].MR_High = val;
                        _I_Summary.Items[row].SubItems[10].Text = IncomeObject.Instance.SelfStorageSummaryObject[row].MRPerItem.ToString(CurrFormatter);
                        break;

                    case 11:
                        IncomeObject.Instance.SelfStorageSummaryObject[row].Comment = val;                        
                        break;                    

                }

                _I_S_TotalUnits.Text = IncomeObject.Instance.GetTotal_SS_Units().ToString(CurrFormatter);
                _I_S_TotalOccupancy.Text = IncomeObject.Instance.GetTotal_SS_Occupancy().ToString(CurrFormatter);
                _I_S_TotalSqFt.Text = IncomeObject.Instance.GetTotal_SS_SqFt().ToString(CurrFormatter);
                _I_S_AR_Low.Text = IncomeObject.Instance.GetTotal_SS_AR_Low().ToString(CurrFormatter);
                _I_S_AR_High.Text = IncomeObject.Instance.GetTotal_SS_AR_High().ToString(CurrFormatter);
                _I_S_AR_UsdPerSqFt.Text = IncomeObject.Instance.GetTotal_SS_AR_UsdPerSqFt().ToString(CurrFormatter);
                _I_S_MR_Low.Text = IncomeObject.Instance.GetTotal_SS_MR_Low().ToString(CurrFormatter);
                _I_S_MR_High.Text = IncomeObject.Instance.GetTotal_SS_MR_High().ToString(CurrFormatter);
                _I_S_MR_UsdPerSqFt.Text = IncomeObject.Instance.GetTotal_SS_MR_UsdPerSqFt().ToString(CurrFormatter);                
            }
        }

        #region AUTOSAVE
        void AddAutoSaveEventHandlers()
        {
            ArrayList objectsFound = new ArrayList();
            FindAutoSaveObjects(this.Controls, ref objectsFound);

            foreach (object o in objectsFound)
            {
                if (o is TextBox)
                {
                    ((TextBox)o).Leave += new EventHandler(On_TextBox_Leave);

                    if (!((TextBox)o).ReadOnly)
                        ((TextBox)o).TextChanged += new EventHandler(On_TextChanged);
                }

                if (o is ComboBox)
                    ((ComboBox)o).SelectedIndexChanged += new EventHandler(On_SelectedIndexChanged);

                if (o is DateTimePicker)
                    ((DateTimePicker)o).ValueChanged += new EventHandler(On_SelectedIndexChanged);// we use combobox event handler
            }
        }

        void On_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (null != _autoSave)
                _autoSave.TriggerSelectedIndexChanged();
        }

        void On_TextChanged(object sender, EventArgs e)
        {
            if (null != _autoSave)
                _autoSave.TriggerTextBoxChanged();
        }

        void On_TextBox_Leave(object sender, EventArgs e)
        {
            if (null != _autoSave)
                _autoSave.TriggerTextBoxLeave();
        }

        void FindAutoSaveObjects(System.Windows.Forms.Control.ControlCollection ctrls, ref ArrayList triggerObjects)
        {
            foreach (object o in ctrls)
            {
                if (o is GroupBox)
                    FindAutoSaveObjects(((GroupBox)o).Controls, ref triggerObjects);

                if (o is Panel)
                    FindAutoSaveObjects(((Panel)o).Controls, ref triggerObjects);

                if (o is TabControl)
                    FindAutoSaveObjects(((TabControl)o).Controls, ref triggerObjects);

                if (o is TextBox)
                    triggerObjects.Add(o);

                if (o is ComboBox)
                    triggerObjects.Add(o);

                if (o is DateTimePicker)
                    triggerObjects.Add(o);

                if (o is CommercialControl)
                    FindAutoSaveObjects(((CommercialControl)o).Controls, ref triggerObjects);
            }
        }
        #endregion

        void On_PrintIncomeReport_Click(object sender, EventArgs e)
        {
            PageSettings pageSettings = null;
            PageSetupDialog pageSetupDialog = new PageSetupDialog();
            pageSetupDialog.PageSettings = new PageSettings();
            pageSetupDialog.PrinterSettings = new PrinterSettings();
            pageSetupDialog.ShowNetwork = false;

            pageSetupDialog.PageSettings.PaperSize = new PaperSize("A4", 827, 1169);
            pageSetupDialog.PageSettings.Margins = new Margins(0, 0, 0, 0);


            //if (DialogResult.OK == pageSetupDialog.ShowDialog())
            //{
            //    pageSettings = new PageSettings(pageSetupDialog.PrinterSettings);
            //    pageSettings.PaperSize = pageSetupDialog.PageSettings.PaperSize;
            //    pageSettings.Margins = pageSetupDialog.PageSettings.Margins;								
            //}

            PrintDialog pd = new PrintDialog();
            pd.UseEXDialog = true;

            PrintDocument tmpprndoc = new PrintDocument();


            tmpprndoc.DefaultPageSettings.Landscape = true;
            tmpprndoc.PrintPage += new PrintPageEventHandler(On_PrintIncomeSummary);
            tmpprndoc.EndPrint += new PrintEventHandler(On_PrintIncome_EndPrint);


            pd.Document = tmpprndoc;
            PrintPreviewDialogEx tmpprdiag = new PrintPreviewDialogEx();

            tmpprdiag.Document = tmpprndoc;
            //tmpprdiag.Document.DefaultPageSettings = pageSettings;
            tmpprdiag.ShowDialog();
        }

        private void On_PrintIncomeSummary(object sender, PrintPageEventArgs e)
        {
            _iGen.GenerateSelfStorageSummary(e, IncomeObject.Instance, _address);
        }

        void On_PrintIncome_EndPrint(object sender, PrintEventArgs e)
        {
            _iGen.ResetGen();
        }  
    }
}
