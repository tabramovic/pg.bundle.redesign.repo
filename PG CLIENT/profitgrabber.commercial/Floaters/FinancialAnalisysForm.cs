using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace ProfitGrabber.Commercial.Floaters
{
	/// <summary>
	/// Summary description for FinancialAnalisysForm.
	/// </summary>
	public class FinancialAnalisysFloater : FloaterBaseForm
	{
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.Label label13;
		private System.Windows.Forms.Label label14;
		private System.Windows.Forms.Label label15;
		private System.Windows.Forms.Label label16;
		private System.Windows.Forms.Label label17;
		private System.Windows.Forms.Label label18;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public FinancialAnalisysFloater()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			this.Opacity = 0;
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		protected override void OnPaint(PaintEventArgs e)
		{
			GfxDrawer.DrawCurveFromOffsets(3, 130, 3, 3, 5, 3, "INTERACTIVE HELP", 2, 5, this, e.Graphics, Brushes.LightYellow, Pens.Black);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.label7 = new System.Windows.Forms.Label();
			this.label8 = new System.Windows.Forms.Label();
			this.label9 = new System.Windows.Forms.Label();
			this.label10 = new System.Windows.Forms.Label();
			this.label11 = new System.Windows.Forms.Label();
			this.label12 = new System.Windows.Forms.Label();
			this.label13 = new System.Windows.Forms.Label();
			this.label14 = new System.Windows.Forms.Label();
			this.label15 = new System.Windows.Forms.Label();
			this.label16 = new System.Windows.Forms.Label();
			this.label17 = new System.Windows.Forms.Label();
			this.label18 = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// label2
			// 
			this.label2.ForeColor = System.Drawing.Color.Green;
			this.label2.Location = new System.Drawing.Point(8, 568);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(872, 32);
			this.label2.TabIndex = 1;
			this.label2.Text = "If a minimum cash flow is desired for the property, the graph can be used to find" +
				" the MAXIMUM PERCENTAGE RATE acceptable for the first mortgage when writing the " +
				"loan contingency in the offer to purchase.";
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(8, 16);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(840, 32);
			this.label3.TabIndex = 2;
			this.label3.Text = "This is where you combine the income, expense and loan information to do a financ" +
				"ial analysis resulting in NOI (Net Operating Income), Cap Rate, DCR, pretax Cash" +
				" Flow and Cash on Cash Return.";
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(8, 56);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(608, 23);
			this.label4.TabIndex = 3;
			this.label4.Text = "There are three columns for comparison, ACTUAL, PROFORMA and SCENARIO.";
			// 
			// label5
			// 
			this.label5.ForeColor = System.Drawing.Color.Green;
			this.label5.Location = new System.Drawing.Point(8, 80);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(696, 24);
			this.label5.TabIndex = 4;
			this.label5.Text = "Results from the Income, Expense and Loans tabs can be copied directly into the f" +
				"irst two columns using the Copy buttons.  ";
			// 
			// label6
			// 
			this.label6.Location = new System.Drawing.Point(8, 104);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(856, 40);
			this.label6.TabIndex = 5;
			this.label6.Text = "Results from the Actual Column can be copied to the Proforma or Scenario columns," +
				" and results from either the Actual or Proforma column can be automatically copi" +
				"ed to the Scenario column by clicking on the appropriate Copy button.";
			// 
			// label7
			// 
			this.label7.ForeColor = System.Drawing.Color.Red;
			this.label7.Location = new System.Drawing.Point(8, 144);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(744, 16);
			this.label7.TabIndex = 6;
			this.label7.Text = "Each column can be used to adjust an INCOME, EXPENSE, VALUE or CAP RATE to analyz" +
				"e the effect on the other quantities.  ";
			// 
			// label8
			// 
			this.label8.Location = new System.Drawing.Point(8, 168);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(856, 32);
			this.label8.TabIndex = 7;
			this.label8.Text = "For example, entering a new Price, GSI, % Vacancy, % Operating Expenses, Cap Rate" +
				" or Debt Service will change the calculated DCR, Cash Flow, Cash on Cash Return " +
				"and other affected results.";
			// 
			// label9
			// 
			this.label9.ForeColor = System.Drawing.Color.Red;
			this.label9.Location = new System.Drawing.Point(8, 208);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(880, 16);
			this.label9.TabIndex = 8;
			this.label9.Text = "If calculations in a column are not being updated as you make changes, clicking i" +
				"n the Percent Vacancy or Other Income fields should automatically update all the" +
				" calculations.";
			// 
			// label10
			// 
			this.label10.Location = new System.Drawing.Point(8, 232);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(872, 40);
			this.label10.TabIndex = 9;
			this.label10.Text = @"The COMMENTS box at the bottom of each column can be used to note the changes and reasons for the changes to each column.  For example, at the bottom of the Actual column you may note the high operating expenses, and copying the results to the third Scenario column you may change the % Operating Expenses and note the change and the effect in the comments box.";
			// 
			// label11
			// 
			this.label11.ForeColor = System.Drawing.Color.Red;
			this.label11.Location = new System.Drawing.Point(8, 280);
			this.label11.Name = "label11";
			this.label11.Size = new System.Drawing.Size(872, 32);
			this.label11.TabIndex = 10;
			this.label11.Text = "The Financial Analysis GRAPHS are unique and exceptional analysis tools.  The gra" +
				"phs show how variations in one quantity will affect other important quantities l" +
				"ike price, cash flow and cash on cash return.  ";
			// 
			// label12
			// 
			this.label12.Location = new System.Drawing.Point(8, 320);
			this.label12.Name = "label12";
			this.label12.Size = new System.Drawing.Size(872, 32);
			this.label12.TabIndex = 11;
			this.label12.Text = "For each graph, any percent variation can be selected.  Default variations are au" +
				"tomatically entered to simplify graphing.  Each graph uses the results in the Ac" +
				"tual column for the calculations.";
			// 
			// label13
			// 
			this.label13.Location = new System.Drawing.Point(8, 360);
			this.label13.Name = "label13";
			this.label13.Size = new System.Drawing.Size(872, 40);
			this.label13.TabIndex = 12;
			this.label13.Text = @"The CAP RATE versus PRICE/VALUE GRAPH shows how the price varies with flexibility in the Cap Rate.  For example, if the list price makes the cap rate too low for the property, the graph will show how low the offer price should be for a more appropriate cap rate.";
			// 
			// label14
			// 
			this.label14.Location = new System.Drawing.Point(8, 400);
			this.label14.Name = "label14";
			this.label14.Size = new System.Drawing.Size(872, 32);
			this.label14.TabIndex = 13;
			this.label14.Text = "The GSI versus CASH FLOW and CASH ON CASH RETURN GRAPH shows how an increase or d" +
				"ecrease in the rent will affect the all important Cash Flow and Cash on Cash Ret" +
				"urn.  ";
			// 
			// label15
			// 
			this.label15.ForeColor = System.Drawing.Color.Green;
			this.label15.Location = new System.Drawing.Point(8, 440);
			this.label15.Name = "label15";
			this.label15.Size = new System.Drawing.Size(872, 16);
			this.label15.TabIndex = 14;
			this.label15.Text = "If the actual cash flow is slightly negative, the graph will show the BREAK EVEN " +
				"POINT, the minimum GSI needed to have a positive cash flow.";
			// 
			// label16
			// 
			this.label16.Location = new System.Drawing.Point(8, 464);
			this.label16.Name = "label16";
			this.label16.Size = new System.Drawing.Size(872, 32);
			this.label16.TabIndex = 15;
			this.label16.Text = "The Percent Operating Expenses versus CASH FLOW and CASH ON CASH RETURN GRAPH sho" +
				"ws how an increase or decrease in Operating Expenses will affect the all importa" +
				"nt Cash Flow and Cash on Cash Return.  ";
			// 
			// label17
			// 
			this.label17.ForeColor = System.Drawing.Color.Green;
			this.label17.Location = new System.Drawing.Point(8, 504);
			this.label17.Name = "label17";
			this.label17.Size = new System.Drawing.Size(856, 16);
			this.label17.TabIndex = 16;
			this.label17.Text = "If the actual cash flow is slightly negative, the graph will show the BREAK EVEN " +
				"POINT, the maximum percent operating expenses needed to have a positive cash flo" +
				"w.";
			// 
			// label18
			// 
			this.label18.Location = new System.Drawing.Point(8, 528);
			this.label18.Name = "label18";
			this.label18.Size = new System.Drawing.Size(864, 32);
			this.label18.TabIndex = 17;
			this.label18.Text = "The First Loan Rate versus CASH FLOW and CASH ON CASH RETURN GRAPH shows how an i" +
				"ncrease or decrease in the First Mortgage will affect the all important Cash Flo" +
				"w and Cash on Cash Return.  ";
			// 
			// FinancialAnalisysFloater
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.BackColor = System.Drawing.Color.LightYellow;
			this.ClientSize = new System.Drawing.Size(888, 608);
			this.Controls.Add(this.label18);
			this.Controls.Add(this.label17);
			this.Controls.Add(this.label16);
			this.Controls.Add(this.label15);
			this.Controls.Add(this.label14);
			this.Controls.Add(this.label13);
			this.Controls.Add(this.label12);
			this.Controls.Add(this.label11);
			this.Controls.Add(this.label10);
			this.Controls.Add(this.label9);
			this.Controls.Add(this.label8);
			this.Controls.Add(this.label7);
			this.Controls.Add(this.label6);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Name = "FinancialAnalisysFloater";
			this.Opacity = 1;
			this.Text = "FinancialAnalisysForm";
			this.ResumeLayout(false);

		}
		#endregion
	}
}
