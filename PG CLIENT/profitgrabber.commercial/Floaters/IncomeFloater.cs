using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace ProfitGrabber.Commercial.Floaters
{
	/// <summary>
	/// Summary description for IncomeFloater.
	/// </summary>
	public class IncomeFloater : FloaterBaseForm
	{
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.Label label11;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public IncomeFloater()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			this.Opacity = 0;
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.label7 = new System.Windows.Forms.Label();
			this.label8 = new System.Windows.Forms.Label();
			this.label9 = new System.Windows.Forms.Label();
			this.label10 = new System.Windows.Forms.Label();
			this.label11 = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(8, 16);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(400, 23);
			this.label2.TabIndex = 1;
			this.label2.Text = "This is where you enter the income information for the multifamily property.  ";
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(8, 40);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(576, 32);
			this.label3.TabIndex = 2;
			this.label3.Text = "The tab includes a UNIT TYPE SUMMARY which gives an overview of each unit type as" +
				" well as a RENT ROLL for every individual unit in the complex.  Both the SUMMARY" +
				" and RENT ROLL are printable.";
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(8, 80);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(472, 23);
			this.label4.TabIndex = 3;
			this.label4.Text = "The UNIT TYPE SUMMARY can accept information for up to 20 unit types.  ";
			// 
			// label5
			// 
			this.label5.ForeColor = System.Drawing.Color.Red;
			this.label5.Location = new System.Drawing.Point(8, 104);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(552, 48);
			this.label5.TabIndex = 4;
			this.label5.Text = "# UNITS, % OCCUPANCY, LOW and HIGH SqFt, and LOW and HIGH ACTUAL and MARKET RENTS" +
				" are all used in calculations (All of the TOTALS, and $/SqFt) so it is best to E" +
				"NTER as much of this information as possible.  ";
			// 
			// label6
			// 
			this.label6.Location = new System.Drawing.Point(8, 152);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(560, 32);
			this.label6.TabIndex = 5;
			this.label6.Text = "The Actual Rent total and Proforma Rent totals can be copied to the Income Summar" +
				"y at the bottom of the page by clicking �Calculate from UNIT TYPE SUMMARY.�  ";
			// 
			// label7
			// 
			this.label7.Location = new System.Drawing.Point(8, 192);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(544, 40);
			this.label7.TabIndex = 6;
			this.label7.Text = "The RENT ROLL can accept information for up to 50 individual units.  Tenant Rent " +
				"is the rent the tenant pays and Section 8 Rent is the rent the government pays. " +
				" The two rents are automatically added together to give the Total Rent.  ";
			// 
			// label8
			// 
			this.label8.Location = new System.Drawing.Point(8, 240);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(536, 23);
			this.label8.TabIndex = 7;
			this.label8.Text = "The annual OTHER INCOME is entered in the OTHER INCOME section.";
			// 
			// label9
			// 
			this.label9.ForeColor = System.Drawing.Color.Red;
			this.label9.Location = new System.Drawing.Point(8, 264);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(560, 48);
			this.label9.TabIndex = 8;
			this.label9.Text = "The annual ACTUAL INCOME and PROFORMA INCOME can be automatically calculated from" +
				" the UNIT TYPE SUMMARY monthly totals.  Clicking on the �Calculate from UNIT TYP" +
				"E SUMMARY� will automatically calculate the annual GSI and % vacancy.";
			// 
			// label10
			// 
			this.label10.Location = new System.Drawing.Point(8, 320);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(544, 23);
			this.label10.TabIndex = 9;
			this.label10.Text = "Changing the % Vacancy will automatically recalculate the Vacancy Loss and the Op" +
				"erating Income (GOI).";
			// 
			// label11
			// 
			this.label11.ForeColor = System.Drawing.Color.Green;
			this.label11.Location = new System.Drawing.Point(8, 352);
			this.label11.Name = "label11";
			this.label11.Size = new System.Drawing.Size(560, 32);
			this.label11.TabIndex = 10;
			this.label11.Text = "The annual ACTUAL INCOME and PROFORMA INCOME can be automatically transferred to " +
				"the FINANCIAL ANALYSIS TAB using the copy buttons on that tab.";
			// 
			// IncomeFloater
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.BackColor = System.Drawing.Color.LightYellow;
			this.ClientSize = new System.Drawing.Size(592, 392);
			this.Controls.Add(this.label11);
			this.Controls.Add(this.label10);
			this.Controls.Add(this.label9);
			this.Controls.Add(this.label8);
			this.Controls.Add(this.label7);
			this.Controls.Add(this.label6);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Name = "IncomeFloater";
			this.Opacity = 1;
			this.Text = "IncomeFloater";
			this.ResumeLayout(false);

		}
		#endregion

		protected override void OnPaint(PaintEventArgs e)
		{
			GfxDrawer.DrawCurveFromOffsets(3, 130, 3, 3, 5, 3, "INTERACTIVE HELP", 2, 5, this, e.Graphics, Brushes.LightYellow, Pens.Black);
		}
	}
}
