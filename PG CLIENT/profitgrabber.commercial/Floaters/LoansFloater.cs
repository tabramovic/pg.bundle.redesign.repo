using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace ProfitGrabber.Commercial.Floaters
{
	/// <summary>
	/// Summary description for LoansFloater.
	/// </summary>
	public class LoansFloater : FloaterBaseForm
	{
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.Label label13;
		private System.Windows.Forms.Label label15;
		private System.Windows.Forms.Label label16;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.Label label12;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public LoansFloater()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			this.Opacity = 0;
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.label7 = new System.Windows.Forms.Label();
			this.label8 = new System.Windows.Forms.Label();
			this.label10 = new System.Windows.Forms.Label();
			this.label11 = new System.Windows.Forms.Label();
			this.label13 = new System.Windows.Forms.Label();
			this.label15 = new System.Windows.Forms.Label();
			this.label16 = new System.Windows.Forms.Label();
			this.label9 = new System.Windows.Forms.Label();
			this.label12 = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(8, 16);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(536, 23);
			this.label2.TabIndex = 1;
			this.label2.Text = "This is where you enter the loan information for the property.";
			// 
			// label3
			// 
			this.label3.ForeColor = System.Drawing.Color.Red;
			this.label3.Location = new System.Drawing.Point(8, 40);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(544, 23);
			this.label3.TabIndex = 2;
			this.label3.Text = "It is useful to fill in as much information as possible on this tab.  ";
			// 
			// label4
			// 
			this.label4.ForeColor = System.Drawing.Color.Green;
			this.label4.Location = new System.Drawing.Point(8, 72);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(552, 32);
			this.label4.TabIndex = 3;
			this.label4.Text = "The FINANCIAL ANALYSIS TAB uses the PURCHASE PRICE, DOWN PAYMENT amount, and the " +
				"TOTAL ANNUAL PAYMENTS.";
			// 
			// label5
			// 
			this.label5.ForeColor = System.Drawing.Color.Red;
			this.label5.Location = new System.Drawing.Point(8, 112);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(480, 23);
			this.label5.TabIndex = 4;
			this.label5.Text = "Always start by entering the PURCHASE PRICE (List Price/Asking Price/Offer Price)" +
				".";
			// 
			// label6
			// 
			this.label6.Location = new System.Drawing.Point(8, 136);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(544, 23);
			this.label6.TabIndex = 5;
			this.label6.Text = "Then, selecting a PERCENT DOWN PAYMENT will automatically calculate the amount of" +
				" down payment.";
			// 
			// label7
			// 
			this.label7.Location = new System.Drawing.Point(8, 160);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(512, 23);
			this.label7.TabIndex = 6;
			this.label7.Text = "There are identical sections for entering information for a first, second and thi" +
				"rd loan.";
			// 
			// label8
			// 
			this.label8.ForeColor = System.Drawing.Color.Red;
			this.label8.Location = new System.Drawing.Point(8, 184);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(368, 23);
			this.label8.TabIndex = 7;
			this.label8.Text = "Always start this section by selecting the loan TYPE.";
			// 
			// label10
			// 
			this.label10.ForeColor = System.Drawing.Color.Green;
			this.label10.Location = new System.Drawing.Point(8, 208);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(552, 32);
			this.label10.TabIndex = 8;
			this.label10.Text = "Notice when different TYPES are selected, different fields become grey (blocked)." +
				"  The blocked fields are not applicable to the selected loan type.";
			// 
			// label11
			// 
			this.label11.Location = new System.Drawing.Point(8, 240);
			this.label11.Name = "label11";
			this.label11.Size = new System.Drawing.Size(552, 23);
			this.label11.TabIndex = 9;
			this.label11.Text = "Entering a %LTV (Loan to Value) will automatically calculate the loan AMOUNT by u" +
				"sing the Purchase Price.";
			// 
			// label13
			// 
			this.label13.Location = new System.Drawing.Point(8, 264);
			this.label13.Name = "label13";
			this.label13.Size = new System.Drawing.Size(512, 32);
			this.label13.TabIndex = 10;
			this.label13.Text = "For the FIXED rate loan type, also entering an AMORTIZATION TERM in years and the" +
				" RATE as a percent will automatically calculate the amortized MONTHLY PAYMENT.";
			// 
			// label15
			// 
			this.label15.Location = new System.Drawing.Point(8, 304);
			this.label15.Name = "label15";
			this.label15.Size = new System.Drawing.Size(544, 32);
			this.label15.TabIndex = 11;
			this.label15.Text = "For the IO (Interest Only) or ADJUSTABLE rate loan types, entering the RATE as a " +
				"percent with the LTV will automatically calculate the MONTHLY INTEREST ONLY PAYM" +
				"ENT.  ";
			// 
			// label16
			// 
			this.label16.Location = new System.Drawing.Point(8, 344);
			this.label16.Name = "label16";
			this.label16.Size = new System.Drawing.Size(552, 32);
			this.label16.TabIndex = 12;
			this.label16.Text = "The calculated monthly payments are automatically included in the TOTAL DEBT SERV" +
				"ICE SUMMARY in the Monthly and Annual Total Payments";
			// 
			// label9
			// 
			this.label9.Location = new System.Drawing.Point(8, 384);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(544, 16);
			this.label9.TabIndex = 13;
			this.label9.Text = "Entering PERCENT POINTS will automatically calculate the POINTS DOLLAR AMOUNT.";
			// 
			// label12
			// 
			this.label12.Location = new System.Drawing.Point(8, 408);
			this.label12.Name = "label12";
			this.label12.Size = new System.Drawing.Size(552, 51);
			this.label12.TabIndex = 14;
			this.label12.Text = @"The TOTAL DEBT SERVICE SUMMARY calculates a BLENDED Interest RATE (the weighted average Rate for all the loans combined), the CLTV (Combined Loan to Value for all the loans), the DCR (Debt Coverage Ratio) using the Actual Income and Expense information from the previous two tabs, and the TOTAL combined MONTHLY and ANNUAL loan PAYMENTS.";
			// 
			// LoansFloater
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.BackColor = System.Drawing.Color.LightYellow;
			this.ClientSize = new System.Drawing.Size(568, 472);
			this.Controls.Add(this.label12);
			this.Controls.Add(this.label9);
			this.Controls.Add(this.label16);
			this.Controls.Add(this.label11);
			this.Controls.Add(this.label8);
			this.Controls.Add(this.label7);
			this.Controls.Add(this.label6);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label10);
			this.Controls.Add(this.label13);
			this.Controls.Add(this.label15);
			this.Name = "LoansFloater";
			this.Opacity = 1;
			this.Text = "LoansFloater";
			this.ResumeLayout(false);

		}
		#endregion

		protected override void OnPaint(PaintEventArgs e)
		{
			GfxDrawer.DrawCurveFromOffsets(3, 130, 3, 3, 5, 3, "INTERACTIVE HELP", 2, 5, this, e.Graphics, Brushes.LightYellow, Pens.Black);
		}
	}
}
