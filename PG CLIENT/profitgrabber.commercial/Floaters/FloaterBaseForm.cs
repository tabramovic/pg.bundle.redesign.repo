using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace ProfitGrabber.Commercial.Floaters
{
	/// <summary>
	/// Summary description for FloaterBaseForm.
	/// </summary>
	public class FloaterBaseForm : System.Windows.Forms.Form
	{
		Timer timer;
		private float opacity = 0;
		private bool fadeIn = true;
		private System.ComponentModel.IContainer components;

		public FloaterBaseForm()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			timer = new Timer();
			timer.Interval = 100;
			timer.Start();
			timer.Tick += new EventHandler(TimerTick);


			SetStyle(ControlStyles.AllPaintingInWmPaint |
				ControlStyles.UserPaint |
				ControlStyles.DoubleBuffer |
				ControlStyles.CacheText, 
				true);

			//this.HideAll();
			this.Opacity = 0;
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			// 
			// FloaterBaseForm
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(192, 160);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.Name = "FloaterBaseForm";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
			this.Text = "FloaterBaseForm";

		}
		#endregion

		private void TimerTick(object sender, System.EventArgs e)
		{
			if (true == fadeIn)
				opacity += 0.1f;
			
			this.Opacity = opacity;
			if (opacity >= 1.0)
			{
				this.fadeIn = false;						
			}			
		}
	}
}
