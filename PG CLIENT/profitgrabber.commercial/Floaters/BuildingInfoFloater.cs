using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace ProfitGrabber.Commercial.Floaters
{
	/// <summary>
	/// Summary description for BuildingInfoFloater.
	/// </summary>
	public class BuildingInfoFloater : FloaterBaseForm
	{
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label6;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public BuildingInfoFloater()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			this.Opacity = 0;
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		protected override void OnPaint(PaintEventArgs e)
		{
			GfxDrawer.DrawCurveFromOffsets(3, 130, 3, 3, 5, 3, "INTERACTIVE HELP", 2, 5, this, e.Graphics, Brushes.LightYellow, Pens.Black);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(8, 16);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(472, 23);
			this.label2.TabIndex = 1;
			this.label2.Text = "This is where you enter most of the nonfinancial information about the commercial" +
				" property.";
			// 
			// label3
			// 
			this.label3.ForeColor = System.Drawing.Color.Red;
			this.label3.Location = new System.Drawing.Point(8, 40);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(632, 40);
			this.label3.TabIndex = 2;
			this.label3.Text = "There are three ESSENTIAL pieces of information (PROPERTY TYPE, # UNITS and TOTAL" +
				" SqFt) which MUST BE INPUT in this tab because it affects what happens in other " +
				"tabs.";
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(8, 80);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(624, 23);
			this.label4.TabIndex = 3;
			this.label4.Text = "First, choosing the PROPERTY TYPE will change the INCOME TAB for the different pr" +
				"operty types.";
			// 
			// label5
			// 
			this.label5.Location = new System.Drawing.Point(8, 104);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(624, 32);
			this.label5.TabIndex = 4;
			this.label5.Text = "Second and third, choosing the # UNITS and TOTAL SqFt will allow per Unit and per" +
				" SqFt calculations to be done on the INCOME and EXPENSES TABS.";
			// 
			// label6
			// 
			this.label6.Location = new System.Drawing.Point(8, 144);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(616, 31);
			this.label6.TabIndex = 5;
			this.label6.Text = "The PARKING section does automatic calculations of Total # Spaces, Total Annual F" +
				"ee and the Parking Ratio (# spaces per unit).  The Total Annual Fee is automatic" +
				"ally transferred to the Income tab for some property Types.  ";
			// 
			// BuildingInfoFloater
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.BackColor = System.Drawing.Color.LightYellow;
			this.ClientSize = new System.Drawing.Size(656, 184);
			this.Controls.Add(this.label6);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Name = "BuildingInfoFloater";
			this.Opacity = 1;
			this.Text = "BuildingInfoFloater";
			this.ResumeLayout(false);

		}
		#endregion
	}
}
