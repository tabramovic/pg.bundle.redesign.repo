using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace ProfitGrabber.Commercial.Floaters
{
	/// <summary>
	/// Summary description for ExpensesFloater.
	/// </summary>
	public class ExpensesFloater : FloaterBaseForm
	{
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.Label label10;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public ExpensesFloater()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			this.Opacity = 0;
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		protected override void OnPaint(PaintEventArgs e)
		{
			GfxDrawer.DrawCurveFromOffsets(3, 130, 3, 3, 5, 3, "INTERACTIVE HELP", 2, 5, this, e.Graphics, Brushes.LightYellow, Pens.Black);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.label7 = new System.Windows.Forms.Label();
			this.label8 = new System.Windows.Forms.Label();
			this.label9 = new System.Windows.Forms.Label();
			this.label10 = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(8, 16);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(408, 23);
			this.label2.TabIndex = 1;
			this.label2.Text = "This is where you enter the expenses information for the property.";
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(8, 40);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(528, 23);
			this.label3.TabIndex = 2;
			this.label3.Text = "CAPITAL ADDITIONS are recorded separately and for ten years to be utilized in the" +
				" Multiyear Analysis.  ";
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(8, 64);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(528, 32);
			this.label4.TabIndex = 3;
			this.label4.Text = "Expenses are separated into FIVE DIFFERENT TYPES (Administrative, Management, Uti" +
				"lities, Insurance and Operating Expenses) to assist in the cost analysis.  ";
			// 
			// label5
			// 
			this.label5.Location = new System.Drawing.Point(8, 104);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(528, 32);
			this.label5.TabIndex = 4;
			this.label5.Text = "Many types of expenses are listed so the user will be sure to include all relevan" +
				"t expenses for the property�s analysis.";
			// 
			// label6
			// 
			this.label6.Location = new System.Drawing.Point(8, 144);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(488, 23);
			this.label6.TabIndex = 5;
			this.label6.Text = "The TOTAL COST for each category type is listed at the bottom of the page. ";
			// 
			// label7
			// 
			this.label7.ForeColor = System.Drawing.Color.Red;
			this.label7.Location = new System.Drawing.Point(8, 168);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(528, 32);
			this.label7.TabIndex = 6;
			this.label7.Text = "Each expense can be entered as a $/month, $/year, $/SqFt or $/Unit and the other " +
				"three types will be automatically calculated.  ";
			// 
			// label8
			// 
			this.label8.ForeColor = System.Drawing.Color.Red;
			this.label8.Location = new System.Drawing.Point(8, 200);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(528, 40);
			this.label8.TabIndex = 7;
			this.label8.Text = "The $/SqFt calculation uses the Total SqFt entered in the Building Info tab and t" +
				"he $/Unit uses the # Units entered in the Building Info tab.  ";
			// 
			// label9
			// 
			this.label9.Location = new System.Drawing.Point(8, 248);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(528, 32);
			this.label9.TabIndex = 8;
			this.label9.Text = "For each expense you can also note who pays the expense (Tenant or Owner), and if" +
				" the expense is too high or too low.";
			// 
			// label10
			// 
			this.label10.ForeColor = System.Drawing.Color.Green;
			this.label10.Location = new System.Drawing.Point(8, 288);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(528, 27);
			this.label10.TabIndex = 9;
			this.label10.Text = "The TOTAL ANNUAL EXPENSES (All Types: Total: $/Year) can be copied directly to th" +
				"e FINANCIAL ANALYSIS TAB using the copy expenses button on that tab.";
			// 
			// ExpensesFloater
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.BackColor = System.Drawing.Color.LightYellow;
			this.ClientSize = new System.Drawing.Size(544, 328);
			this.Controls.Add(this.label10);
			this.Controls.Add(this.label9);
			this.Controls.Add(this.label8);
			this.Controls.Add(this.label7);
			this.Controls.Add(this.label6);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Name = "ExpensesFloater";
			this.Opacity = 1;
			this.Text = "ExpensesFloater";
			this.ResumeLayout(false);

		}
		#endregion
	}
}
