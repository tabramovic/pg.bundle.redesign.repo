using System;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

namespace ProfitGrabber.Commercial.Floaters
{
	public class GfxDrawer
	{
		/// <summary>
		/// Draw Curve (opened or closed)        
		///     LRU         ---Optional Text---         RLU
		/// LLU                                             RRU
		/// 
		/// 
		/// LLL                                             RRL
		///     LRL                                     RLL     
		/// </summary>
		/// <param name="llu"></param>
		/// <param name="lru"></param>
		/// <param name="rlu"></param>
		/// <param name="rru"></param>
		/// <param name="lll"></param>
		/// <param name="lrl"></param>
		/// <param name="rll"></param>
		/// <param name="rrl"></param>
		/// <param name="lineDistance"></param>
		/// <param name="topHorizontalLineEmptySpace"></param>
		/// <param name="text"></param>
		/// <param name="horizontalTextOffset"></param>
		/// <param name="gfx"></param>
		/// <param name="brush"></param>
		/// <param name="pen"></param>
		public static void DrawRectCurve(Point llu, Point lru, Point rlu, Point rru, Point lll, Point lrl, Point rll, Point rrl, int lineDistance, int topHorizontalLineEmptySpace, string text, int horizontalTextOffset, int verticalTextOffset, Graphics gfx, Brush brush, Pen pen)
		{
			gfx.FillRectangle(brush, new Rectangle(lru.X, lru.Y, rlu.X - lru.X, llu.Y - lru.Y));
			gfx.FillRectangle(brush, new Rectangle(lrl.X, lll.Y, rlu.X - lru.X, llu.Y - lru.Y));
			gfx.FillRectangle(brush, new Rectangle(llu.X, llu.Y, lru.X - llu.X, lll.Y - llu.Y));
			gfx.FillRectangle(brush, new Rectangle(rlu.X, rru.Y, rru.X - rlu.X, rrl.Y - rru.Y));
			gfx.FillRectangle(brush, new Rectangle(lru.X, llu.Y, rlu.X - lru.X, rrl.Y - rru.Y));

			if (0 == topHorizontalLineEmptySpace)
			{
				gfx.DrawLine(pen, lru, rlu);
			}
			else
			{
				Point voidBegin, voidEnd;
				int length = rlu.X - lru.X;
				if (length > 0 && length > topHorizontalLineEmptySpace)
				{
					int leftOver = length - topHorizontalLineEmptySpace;
					int span = leftOver / 2;

					voidBegin = new Point(lru.X + span, lru.Y);
					voidEnd = new Point(rlu.X - span, rlu.Y);

					gfx.DrawLine(pen, lru, voidBegin);
					gfx.DrawLine(pen, voidEnd, rlu);

					Font textFont = new Font("Verdana", 8, FontStyle.Bold);

					PointF textPos = new PointF(voidBegin.X + horizontalTextOffset, voidBegin.Y - verticalTextOffset);
					gfx.DrawString(text, textFont, Brushes.Black, textPos);
				}
			}

			gfx.DrawLine(pen, rru, rrl);
			gfx.DrawLine(pen, llu, lll);
			gfx.DrawLine(pen, lrl, rll);

			Rectangle luRect = new Rectangle(
				llu.X,
				lru.Y,
				lru.X - llu.X + lineDistance,
				llu.Y - lru.Y + lineDistance
				);
#if DEBUG
			gfx.DrawRectangle(Pens.Red, luRect);
#endif
			gfx.FillPie(brush, luRect, 180, 90);
			gfx.DrawArc(pen, luRect, 180, 90);

			Rectangle llRect = new Rectangle(
				lll.X,
				lll.Y - lineDistance,
				lrl.X - lll.X + lineDistance,
				lrl.Y - lll.Y + lineDistance
				);
#if DEBUG
			gfx.DrawRectangle(Pens.Red, llRect);
#endif
			gfx.FillPie(brush, llRect, 90, 90);
			gfx.DrawArc(pen, llRect, 90, 90);

			Rectangle rlRect = new Rectangle(
				rll.X - lineDistance,
				rrl.Y - lineDistance,
				rrl.X - rll.X + lineDistance,
				rll.Y - rrl.Y + lineDistance
				);
#if DEBUG
			gfx.DrawRectangle(Pens.Red, rlRect);
#endif
			gfx.FillPie(brush, rlRect, 0, 90);
			gfx.DrawArc(pen, rlRect, 0, 90);

			Rectangle ruRect = new Rectangle(
				rlu.X - lineDistance,
				rlu.Y,
				rru.X - rlu.X + lineDistance,
				rru.Y - rlu.Y + lineDistance
				);
#if DEBUG
			gfx.DrawRectangle(Pens.Red, ruRect);
#endif
			gfx.FillPie(brush, ruRect, 270, 90);
			gfx.DrawArc(pen, ruRect, 270, 90);

		}

        
		/// <summary>
		/// 
		/// </summary>
		/// <param name="lineDistance"></param>
		/// <param name="topHorizontalLineEmptySpace"></param>
		/// <param name="leftHorizontalOffset"></param>
		/// <param name="topVerticalOffset"></param>
		/// <param name="rectWidth"></param>
		/// <param name="rectHeight"></param>
		/// <param name="text"></param>
		/// <param name="horizontalTextOffset"></param>
		/// <param name="gfx"></param>
		/// <param name="brush"></param>
		/// <param name="pen"></param>
		public static void DrawCurveFromRect
			(
			int lineDistance,
			int topHorizontalLineEmptySpace,
			int leftHorizontalOffset,
			int topVerticalOffset,
			int rectWidth,
			int rectHeight,
			string text,
			int horizontalTextOffset,
			int verticalTextOffset,
			Graphics gfx,
			Brush brush,
			Pen pen
			)
		{
			Point llu, lru, rlu, rru, lll, lrl, rll, rrl;

			llu = new Point(leftHorizontalOffset, topVerticalOffset + lineDistance);
			lru = new Point(leftHorizontalOffset + lineDistance, topVerticalOffset);
			rlu = new Point(lru.X + rectWidth, topVerticalOffset);
			rru = new Point(llu.X + rectWidth + 2 * lineDistance, topVerticalOffset + lineDistance);

			lll = new Point(leftHorizontalOffset, llu.Y + rectHeight);
			lrl = new Point(lru.X, lru.Y + rectHeight + 2 * lineDistance);
			rll = new Point(rlu.X, rlu.Y + rectHeight + 2 * lineDistance);
			rrl = new Point(rru.X, rru.Y + rectHeight);

			GfxDrawer.DrawRectCurve(llu, lru, rlu, rru, lll, lrl, rll, rrl, lineDistance, topHorizontalLineEmptySpace, text, horizontalTextOffset, verticalTextOffset, gfx, brush, pen);

		}

        
        
		/// <summary>
		/// Draw Curve (opened or closed)        
		///     LRU         ---Optional Text---         RLU
		/// LLU                                             RRU
		/// 
		/// 
		/// LLL                                             RRL
		///     LRL                                     RLL        
		/// </summary>
		/// <param name="lineDistance">Distance between lines</param>
		/// <param name="topHorizontalLineEmptySpace">Top horizontal line empty spaces length</param>
		/// <param name="leftHorizontalOffset">Left Horizontal offset</param>
		/// <param name="rightHorizontalOffset">Right Horizontal offset</param>
		/// <param name="topVerticalOffset">Top Vertical offset</param>
		/// <param name="bottomVerticalOffset">Bottom Vertical offset</param>
		/// <param name="text">Text</param>
		/// <param name="horizontalTextOffset">Horizontal Text Offset</param>
		/// <param name="gfx">GraphX</param>
		/// <param name="brush">Brush</param>
		/// <param name="pen">Pen</param>
		public static void DrawCurveFromOffsets
			(  
			int lineDistance,
			int topHorizontalLineEmptySpace,
			int leftHorizontalOffset, 
			int rightHorizontalOffset,
			int topVerticalOffset, 
			int bottomVerticalOffset, 
			string text,
			int horizontalTextOffset,
			int verticalTextOffset,
			Control uc,                
			Graphics gfx, 
			Brush brush, 
			Pen pen
			)
		{
			Point llu, lru, rlu, rru, lll, lrl, rll, rrl;

			llu = new Point(leftHorizontalOffset, topVerticalOffset + lineDistance);
			lru = new Point(leftHorizontalOffset + lineDistance, topVerticalOffset);
			rlu = new Point(uc.Size.Width - (rightHorizontalOffset + lineDistance), topVerticalOffset);
			rru = new Point(uc.Size.Width - rightHorizontalOffset, topVerticalOffset + lineDistance);

			lll = new Point(leftHorizontalOffset, uc.Size.Height - (bottomVerticalOffset + lineDistance));
			lrl = new Point(leftHorizontalOffset + lineDistance, uc.Size.Height - bottomVerticalOffset);
			rll = new Point(uc.Size.Width - (rightHorizontalOffset + lineDistance), uc.Size.Height - bottomVerticalOffset);
			rrl = new Point(uc.Size.Width - rightHorizontalOffset, uc.Size.Height - (bottomVerticalOffset + lineDistance));

			GfxDrawer.DrawRectCurve(llu, lru, rlu, rru, lll, lrl, rll, rrl, lineDistance, topHorizontalLineEmptySpace, text, horizontalTextOffset, verticalTextOffset, gfx, brush, pen);
		}

		public static void DrawTriangle(Point p1, Point p2, Point p3, Graphics gfx, Brush brush, Pen pen)
		{
			Point[] points = new Point[3] { p1, p2, p3 };

			gfx.FillPolygon(brush, points);
			gfx.DrawPolygon(pen, points);
		}

		public static void DrawLine(Point p1, Point p2, Graphics gfx, Pen pen)
		{
			gfx.DrawLine(pen, p1, p2);
		}      
	}
}
