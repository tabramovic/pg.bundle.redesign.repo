﻿using System;
using System.Collections;
using System.Drawing;
using System.Drawing.Printing;

using ProfitGrabber.Commercial.Objects;

namespace ProfitGrabber.Commercial.Generators
{
    public class FinancialAnalisysGenerator : BaseCommercialsGenerator
    {

        FinancialInfoObject _obj = null;
        const string ReportName = "FINANCIAL ANALYSIS SHEET";        
       

        public FinancialAnalisysGenerator()
        {
            
        }

        public void GenerateReport(PrintPageEventArgs e, FinancialInfoObject obj, string address)
        {
            _obj = obj;
            _e = e;
            _gfx = e.Graphics;
            _paperWidth = e.PageSettings.PaperSize.Height;

            _gfx.DrawString(ReportName, base._arial22, base._blackBrush, base.GetOrigin(_gfx, ePosition.Center, ReportName, base._arial22, _paperWidth, 20));

            int currY = 60;
            //1st ROW
            _gfx.DrawString("Property Site Address:", _arial12, _blackBrush, new PointF(20, currY));
            _gfx.DrawString(address, _arial12, _blackBrush, GetOriginUponAlreadySetText(_gfx, ePosition.Right, "Property Site Address:", new Point(20, currY), _arial12));

            //2nd ROW
            currY += 25;
            _gfx.DrawString("ACTUAL", _arial12, _blackBrush, new PointF(20, currY));
            _gfx.DrawString("PROFORMA", _arial12, _blackBrush, new PointF(350, currY));
            _gfx.DrawString("SCENARIO", _arial12, _blackBrush, new PointF(700, currY));

            currY += 25;
            _gfx.DrawString("Price/Value:", _arial10, _blackBrush, new PointF(20, currY));
            _gfx.DrawString("Price/Value:", _arial10, _blackBrush, new PointF(350, currY));
            _gfx.DrawString("Price/Value:", _arial10, _blackBrush, new PointF(700, currY));

            _gfx.DrawString("$" + _obj.Data[0].PriceProperty, _arial10, _blackBrush, new PointF(170, currY));
            _gfx.DrawString("$" + _obj.Data[1].PriceProperty, _arial10, _blackBrush, new PointF(500, currY));
            _gfx.DrawString("$" + _obj.Data[2].PriceProperty, _arial10, _blackBrush, new PointF(850, currY));

            currY += 25;
            _gfx.DrawString("GSI (annual):", _arial10, _blackBrush, new PointF(20, currY));
            _gfx.DrawString("GSI (annual):", _arial10, _blackBrush, new PointF(350, currY));
            _gfx.DrawString("GSI (annual):", _arial10, _blackBrush, new PointF(700, currY));

            _gfx.DrawString("$" + _obj.Data[0].GsiAnnualProperty, _arial10, _blackBrush, new PointF(170, currY));
            _gfx.DrawString("$" + _obj.Data[1].GsiAnnualProperty, _arial10, _blackBrush, new PointF(500, currY));
            _gfx.DrawString("$" + _obj.Data[2].GsiAnnualProperty, _arial10, _blackBrush, new PointF(850, currY));

            currY += 25;
            _gfx.DrawString("Vacancy/Credit Loss:", _arial10, _blackBrush, new PointF(20, currY));
            _gfx.DrawString("Vacancy/Credit Loss:", _arial10, _blackBrush, new PointF(350, currY));
            _gfx.DrawString("Vacancy/Credit Loss:", _arial10, _blackBrush, new PointF(700, currY));

            _gfx.DrawString("$" + _obj.Data[0].VacancyCreditLossProperty, _arial10, _blackBrush, new PointF(170, currY));
            _gfx.DrawString("$" + _obj.Data[1].VacancyCreditLossProperty, _arial10, _blackBrush, new PointF(500, currY));
            _gfx.DrawString("$" + _obj.Data[2].VacancyCreditLossProperty, _arial10, _blackBrush, new PointF(850, currY));

            _gfx.DrawString(_obj.Data[0].VacancyCreditLossPercProperty + " %", _arial10, _blackBrush, new PointF(270, currY));
            _gfx.DrawString(_obj.Data[1].VacancyCreditLossPercProperty + " %", _arial10, _blackBrush, new PointF(600, currY));
            _gfx.DrawString(_obj.Data[2].VacancyCreditLossPercProperty + " %", _arial10, _blackBrush, new PointF(950, currY));

            currY += 25;
            _gfx.DrawString("Other Income:", _arial10, _blackBrush, new PointF(20, currY));
            _gfx.DrawString("Other Income:", _arial10, _blackBrush, new PointF(350, currY));
            _gfx.DrawString("Other Income:", _arial10, _blackBrush, new PointF(700, currY));

            _gfx.DrawString("$" + _obj.Data[0].OtherIncomeProperty, _arial10, _blackBrush, new PointF(170, currY));
            _gfx.DrawString("$" + _obj.Data[1].OtherIncomeProperty, _arial10, _blackBrush, new PointF(500, currY));
            _gfx.DrawString("$" + _obj.Data[2].OtherIncomeProperty, _arial10, _blackBrush, new PointF(850, currY));

            currY += 25;
            _gfx.DrawString("GOI (annual):", _arial10, _blackBrush, new PointF(20, currY));
            _gfx.DrawString("GOI (annual):", _arial10, _blackBrush, new PointF(350, currY));
            _gfx.DrawString("GOI (annual):", _arial10, _blackBrush, new PointF(700, currY));

            _gfx.DrawString("$" + _obj.Data[0].GoiAnnualProperty, _arial10, _blackBrush, new PointF(170, currY));
            _gfx.DrawString("$" + _obj.Data[1].GoiAnnualProperty, _arial10, _blackBrush, new PointF(500, currY));
            _gfx.DrawString("$" + _obj.Data[2].GoiAnnualProperty, _arial10, _blackBrush, new PointF(850, currY));

            currY += 25;
            _gfx.DrawString("Operating Expenses:", _arial10, _blackBrush, new PointF(20, currY));
            _gfx.DrawString("Operating Expenses:", _arial10, _blackBrush, new PointF(350, currY));
            _gfx.DrawString("Operating Expenses:", _arial10, _blackBrush, new PointF(700, currY));

            _gfx.DrawString("$" + _obj.Data[0].OperatingExpensesProperty, _arial10, _blackBrush, new PointF(170, currY));
            _gfx.DrawString("$" + _obj.Data[1].OperatingExpensesProperty, _arial10, _blackBrush, new PointF(500, currY));
            _gfx.DrawString("$" + _obj.Data[2].OperatingExpensesProperty, _arial10, _blackBrush, new PointF(850, currY));

            _gfx.DrawString(_obj.Data[0].OperatingExpensesPercProperty + " %", _arial10, _blackBrush, new PointF(270, currY));
            _gfx.DrawString(_obj.Data[1].OperatingExpensesPercProperty + " %", _arial10, _blackBrush, new PointF(600, currY));
            _gfx.DrawString(_obj.Data[2].OperatingExpensesPercProperty + " %", _arial10, _blackBrush, new PointF(950, currY));

            currY += 25;
            _gfx.DrawString("NOI:", _arial10, _blackBrush, new PointF(20, currY));
            _gfx.DrawString("NOI:", _arial10, _blackBrush, new PointF(350, currY));
            _gfx.DrawString("NOI:", _arial10, _blackBrush, new PointF(700, currY));

            _gfx.DrawString("$" + _obj.Data[0].NetOperatingIncomeProperty, _arial10, _blackBrush, new PointF(170, currY));
            _gfx.DrawString("$" + _obj.Data[1].NetOperatingIncomeProperty, _arial10, _blackBrush, new PointF(500, currY));
            _gfx.DrawString("$" + _obj.Data[2].NetOperatingIncomeProperty, _arial10, _blackBrush, new PointF(850, currY));

            currY += 25;
            _gfx.DrawString("Cap Rate:", _arial10, _blackBrush, new PointF(20, currY));
            _gfx.DrawString("Cap Rate:", _arial10, _blackBrush, new PointF(350, currY));
            _gfx.DrawString("Cap Rate:", _arial10, _blackBrush, new PointF(700, currY));

            _gfx.DrawString(_obj.Data[0].CapRateProperty + " %", _arial10, _blackBrush, new PointF(170, currY));
            _gfx.DrawString(_obj.Data[1].CapRateProperty + " %", _arial10, _blackBrush, new PointF(500, currY));
            _gfx.DrawString(_obj.Data[2].CapRateProperty + " %", _arial10, _blackBrush, new PointF(850, currY));

            currY += 25;
            _gfx.DrawString("Debt Service:", _arial10, _blackBrush, new PointF(20, currY));
            _gfx.DrawString("Debt Service:", _arial10, _blackBrush, new PointF(350, currY));
            _gfx.DrawString("Debt Service:", _arial10, _blackBrush, new PointF(700, currY));

            _gfx.DrawString("$" + _obj.Data[0].DebtServiceProperty, _arial10, _blackBrush, new PointF(170, currY));
            _gfx.DrawString("$" + _obj.Data[1].DebtServiceProperty, _arial10, _blackBrush, new PointF(500, currY));
            _gfx.DrawString("$" + _obj.Data[2].DebtServiceProperty, _arial10, _blackBrush, new PointF(850, currY));

            currY += 25;
            _gfx.DrawString("Debt Coverage Ratio:", _arial10, _blackBrush, new PointF(20, currY));
            _gfx.DrawString("Debt Coverage Ratio:", _arial10, _blackBrush, new PointF(350, currY));
            _gfx.DrawString("Debt Coverage Ratio:", _arial10, _blackBrush, new PointF(700, currY));

            _gfx.DrawString(_obj.Data[0].DebtCoverageRatioProperty, _arial10, _blackBrush, new PointF(170, currY));
            _gfx.DrawString(_obj.Data[1].DebtCoverageRatioProperty, _arial10, _blackBrush, new PointF(500, currY));
            _gfx.DrawString(_obj.Data[2].DebtCoverageRatioProperty, _arial10, _blackBrush, new PointF(850, currY));

            currY += 25;
            _gfx.DrawString("Cash Flow (pretax):", _arial10, _blackBrush, new PointF(20, currY));
            _gfx.DrawString("Cash Flow (pretax):", _arial10, _blackBrush, new PointF(350, currY));
            _gfx.DrawString("Cash Flow (pretax):", _arial10, _blackBrush, new PointF(700, currY));

            _gfx.DrawString("$" + _obj.Data[0].CashFlowPreTaxProperty, _arial10, _blackBrush, new PointF(170, currY));
            _gfx.DrawString("$" + _obj.Data[1].CashFlowPreTaxProperty, _arial10, _blackBrush, new PointF(500, currY));
            _gfx.DrawString("$" + _obj.Data[2].CashFlowPreTaxProperty, _arial10, _blackBrush, new PointF(850, currY));

            currY += 25;
            _gfx.DrawString("Cash on Cash Return:", _arial10, _blackBrush, new PointF(20, currY));
            _gfx.DrawString("Cash on Cash Return:", _arial10, _blackBrush, new PointF(350, currY));
            _gfx.DrawString("Cash on Cash Return:", _arial10, _blackBrush, new PointF(700, currY));

            _gfx.DrawString(_obj.Data[0].CashOnCashReturnProperty + " %", _arial10, _blackBrush, new PointF(170, currY));
            _gfx.DrawString(_obj.Data[1].CashOnCashReturnProperty + " %", _arial10, _blackBrush, new PointF(500, currY));
            _gfx.DrawString(_obj.Data[2].CashOnCashReturnProperty + " %", _arial10, _blackBrush, new PointF(850, currY));

            currY += 15;

            string[] lines = null;
            if (null != _obj.Data[0].CommentsProperty && string.Empty != _obj.Data[0].CommentsProperty)
            {
                currY += 25;

                _gfx.DrawString("Actual Comments: ", _arial10, _blackBrush, new PointF(20, currY));
                lines = GetPartialLinesFromString(_gfx, _arial10, 875, _obj.Data[0].CommentsProperty);
                if (null != lines && lines.Length > 0)
                {
                    int xOffset = GetOriginUponAlreadySetText(_gfx, ePosition.Right, "Actual Comments: ", new Point(20, currY), _arial10).X;
                    foreach (string line in lines)
                    {
                        _gfx.DrawString(line, _arial10, _blackBrush, new PointF(xOffset, currY));
                        currY += 15;
                    }
                }
            }

            if (null != _obj.Data[1].CommentsProperty && string.Empty != _obj.Data[1].CommentsProperty)
            {
                currY += 25;
                _gfx.DrawString("Proforma Comments: ", _arial10, _blackBrush, new PointF(20, currY));
                lines = GetPartialLinesFromString(_gfx, _arial10, 875, _obj.Data[1].CommentsProperty);
                if (null != lines && lines.Length > 0)
                {
                    int xOffset = GetOriginUponAlreadySetText(_gfx, ePosition.Right, "Proforma Comments: ", new Point(20, currY), _arial10).X;
                    foreach (string line in lines)
                    {
                        _gfx.DrawString(line, _arial10, _blackBrush, new PointF(xOffset, currY));
                        currY += 15;
                    }
                }
            }

            if (null != _obj.Data[2].CommentsProperty && string.Empty != _obj.Data[2].CommentsProperty)
            {
                currY += 25;
                _gfx.DrawString("Scenario Comments: ", _arial10, _blackBrush, new PointF(20, currY));
                lines = GetPartialLinesFromString(_gfx, _arial10, 875, _obj.Data[2].CommentsProperty);
                if (null != lines && lines.Length > 0)
                {
                    int xOffset = GetOriginUponAlreadySetText(_gfx, ePosition.Right, "Scenario Comments: ", new Point(20, currY), _arial10).X;
                    foreach (string line in lines)
                    {
                        _gfx.DrawString(line, _arial10, _blackBrush, new PointF(xOffset, currY));
                        currY += 15;
                    }
                }
            }
        }
    }
}
