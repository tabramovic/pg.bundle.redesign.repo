﻿using System;
using System.Collections;
using System.Drawing;
using System.Drawing.Printing;

using ProfitGrabber.Commercial.Objects;

namespace ProfitGrabber.Commercial.Generators
{
    public class BuildingInfoGenerator : BaseCommercialsGenerator
    {
        BuildingInfoObject _obj = null;
        const string ReportName = "COMMERCIAL BUILDING INFO SHEET";

        public BuildingInfoGenerator()
        {            
            
        }

        public void Generate(PrintPageEventArgs e, BuildingInfoObject obj, string address)
        {
            _obj = obj;            
            _e = e;
            _gfx = e.Graphics;
            _paperWidth = e.PageSettings.PaperSize.Width;
            int currY = 60;
            string[] lines = new string[1] { string.Empty };

            if (0 == _pageCnt)
            {
                _gfx.DrawString(ReportName, base._arial22, base._blackBrush, base.GetOrigin(_gfx, ePosition.Center, ReportName, base._arial22, _paperWidth, 20));

                currY = 60;
                //1st ROW
                _gfx.DrawString("Property Site Address:", _arial12, _blackBrush, new PointF(20, currY));
                _gfx.DrawString(address, _arial12, _blackBrush, GetOriginUponAlreadySetText(_gfx, ePosition.Right, "Property Site Address:", new Point(20, currY), _arial12));

                currY += 25;
                //2nd ROW
                _gfx.DrawString("Property Name:", _arial12, _blackBrush, new PointF(20, currY));
                _gfx.DrawString(_obj.PropertyName, _arial12, _blackBrush, GetOriginUponAlreadySetText(_gfx, ePosition.Right, "Property Name:", new Point(20, currY), _arial12));

                _gfx.DrawString("Property Type:", _arial12, _blackBrush, new PointF(300, currY));
                _gfx.DrawString(_obj.PropertyType, _arial12, _blackBrush, GetOriginUponAlreadySetText(_gfx, ePosition.Right, "Property Type:", new Point(300, currY), _arial12));

                _gfx.DrawString("Property APN:", _arial12, _blackBrush, new PointF(550, currY));
                _gfx.DrawString(_obj.Apn, _arial12, _blackBrush, GetOriginUponAlreadySetText(_gfx, ePosition.Right, "Property APN:", new Point(550, currY), _arial12));


                //2nd ROW
                currY += 25;
                _gfx.DrawString("Property Value (List Price): $", _arial12, _blackBrush, new PointF(20, currY));
                _gfx.DrawString(_obj.PropertyValue.ToString(CurrFormatter), _arial12, _blackBrush, GetOriginUponAlreadySetText(_gfx, ePosition.Right, "Property Value (List Price): $", new Point(20, currY), _arial12));

                _gfx.DrawString("Property Class:", _arial12, _blackBrush, new PointF(550, currY));
                _gfx.DrawString(_obj.Class, _arial12, _blackBrush, GetOriginUponAlreadySetText(_gfx, ePosition.Right, "Property Class:", new Point(550, currY), _arial12));


                //3rd ROW
                currY += 40;
                _gfx.DrawString("CONSTRUCTION:", _arial16, _blackBrush, new PointF(20, currY));

                //4th ROW
                currY += 25;
                _gfx.DrawString("Year Built:", _arial12, _blackBrush, new PointF(20, currY));
                _gfx.DrawString(_obj.YearBuilt, _arial12, _blackBrush, GetOriginUponAlreadySetText(_gfx, ePosition.Right, "Year Built:", new Point(20, currY), _arial12));

                _gfx.DrawString("Rehab Year:", _arial12, _blackBrush, new PointF(200, currY));
                _gfx.DrawString(_obj.RehabYear, _arial12, _blackBrush, GetOriginUponAlreadySetText(_gfx, ePosition.Right, "Rehab Year:", new Point(200, currY), _arial12));

                _gfx.DrawString("#Units:", _arial12, _blackBrush, new PointF(380, currY));
                _gfx.DrawString(_obj.NrOfUnits, _arial12, _blackBrush, GetOriginUponAlreadySetText(_gfx, ePosition.Right, "#Units:", new Point(380, currY), _arial12));

                _gfx.DrawString("#Floors:", _arial12, _blackBrush, new PointF(510, currY));
                _gfx.DrawString(_obj.NrOfFloors, _arial12, _blackBrush, GetOriginUponAlreadySetText(_gfx, ePosition.Right, "#Floors:", new Point(510, currY), _arial12));

                _gfx.DrawString("#Structures:", _arial12, _blackBrush, new PointF(640, currY));
                _gfx.DrawString(_obj.NrOfStructures, _arial12, _blackBrush, GetOriginUponAlreadySetText(_gfx, ePosition.Right, "#Structures:", new Point(640, currY), _arial12));


                //5th ROW 
                currY += 25;
                _gfx.DrawString("Total Rentable SqFt:", _arial12, _blackBrush, new PointF(20, currY));
                _gfx.DrawString(_obj.TotalSqFt.ToString(), _arial12, _blackBrush, GetOriginUponAlreadySetText(_gfx, ePosition.Right, "Total Rentable SqFt:", new Point(20, currY), _arial12));

                _gfx.DrawString("Net Rentable SqFt:", _arial12, _blackBrush, new PointF(250, currY));
                _gfx.DrawString(_obj.NetRentableSqFt.ToString(), _arial12, _blackBrush, GetOriginUponAlreadySetText(_gfx, ePosition.Right, "Net Rentable SqFt:", new Point(250, currY), _arial12));

                _gfx.DrawString("Lot Acres:", _arial12, _blackBrush, new PointF(480, currY));
                _gfx.DrawString(_obj.LotAcres.ToString(), _arial12, _blackBrush, GetOriginUponAlreadySetText(_gfx, ePosition.Right, "Lot Acres:", new Point(480, currY), _arial12));

                _gfx.DrawString("Lot SqFt:", _arial12, _blackBrush, new PointF(650, currY));
                _gfx.DrawString(_obj.LotSqFt.ToString(), _arial12, _blackBrush, GetOriginUponAlreadySetText(_gfx, ePosition.Right, "Lot SqFt:", new Point(650, currY), _arial12));


                //6th ROW 
                currY += 25;
                _gfx.DrawString("Roof Age:", _arial12, _blackBrush, new PointF(20, currY));
                _gfx.DrawString(_obj.RoofAge, _arial12, _blackBrush, GetOriginUponAlreadySetText(_gfx, ePosition.Right, "Roof Age:", new Point(20, currY), _arial12));

                _gfx.DrawString("Roof Type:", _arial12, _blackBrush, new PointF(250, currY));
                _gfx.DrawString(_obj.RoofType, _arial12, _blackBrush, GetOriginUponAlreadySetText(_gfx, ePosition.Right, "Roof Type:", new Point(250, currY), _arial12));

                _gfx.DrawString("Plumbing Age:", _arial12, _blackBrush, new PointF(480, currY));
                _gfx.DrawString(_obj.PlumbingAge, _arial12, _blackBrush, GetOriginUponAlreadySetText(_gfx, ePosition.Right, "Plumbing Age:", new Point(480, currY), _arial12));

                _gfx.DrawString("Electrical Age:", _arial12, _blackBrush, new PointF(650, currY));
                _gfx.DrawString(_obj.ElectricalAge, _arial12, _blackBrush, GetOriginUponAlreadySetText(_gfx, ePosition.Right, "Electrical Age:", new Point(650, currY), _arial12));


                //7th ROW
                currY += 25;
                _gfx.DrawString("Siding Type:", _arial12, _blackBrush, new PointF(20, currY));
                _gfx.DrawString(_obj.SidingType, _arial12, _blackBrush, GetOriginUponAlreadySetText(_gfx, ePosition.Right, "Siding Type:", new Point(20, currY), _arial12));

                _gfx.DrawString("Window Type:", _arial12, _blackBrush, new PointF(300, currY));
                _gfx.DrawString(_obj.WindowType, _arial12, _blackBrush, GetOriginUponAlreadySetText(_gfx, ePosition.Right, "Window Type:", new Point(300, currY), _arial12));

                _gfx.DrawString("Window Age:", _arial12, _blackBrush, new PointF(550, currY));
                _gfx.DrawString(_obj.WindowAge, _arial12, _blackBrush, GetOriginUponAlreadySetText(_gfx, ePosition.Right, "Window Age:", new Point(550, currY), _arial12));


                //8th ROW
                currY += 40;
                _gfx.DrawString("PARKING:", _arial16, _blackBrush, new PointF(20, currY));
                _gfx.DrawString("LANDLEASE:", _arial16, _blackBrush, new PointF(420, currY));

                //9th ROW
                currY += 25;
                _gfx.DrawString("Parking Ratio:", _arial12, _blackBrush, new PointF(20, currY));
                _gfx.DrawString(_obj.ParkingRatio, _arial12, _blackBrush, GetOriginUponAlreadySetText(_gfx, ePosition.Right, "Parking Ratio:", new Point(20, currY), _arial12));

                _gfx.DrawString("Land Lease:", _arial12, _blackBrush, new PointF(420, currY));
                _gfx.DrawString(_obj.LandLease, _arial12, _blackBrush, GetOriginUponAlreadySetText(_gfx, ePosition.Right, "Land Lease:", new Point(420, currY), _arial12));

                _gfx.DrawString("Option:", _arial12, _blackBrush, new PointF(620, currY));
                _gfx.DrawString(_obj.LandLeaseOption, _arial12, _blackBrush, GetOriginUponAlreadySetText(_gfx, ePosition.Right, "Option:", new Point(620, currY), _arial12));


                //10th ROW
                currY += 25;
                _gfx.DrawString("Lot", _arial12, _blackBrush, new PointF(20, currY));

                _gfx.DrawString("#Spaces:", _arial12, _blackBrush, new PointF(100, currY));
                _gfx.DrawString(_obj.ParkingLotSpaces.ToString(), _arial12, _blackBrush, GetOriginUponAlreadySetText(_gfx, ePosition.Right, "#Spaces:", new Point(100, currY), _arial12));

                _gfx.DrawString("Fee: $", _arial12, _blackBrush, new PointF(220, currY));
                _gfx.DrawString(_obj.ParkingLotFee.ToString(), _arial12, _blackBrush, GetOriginUponAlreadySetText(_gfx, ePosition.Right, "Fee: $", new Point(220, currY), _arial12));

                _gfx.DrawString("Lessor", _arial12, _blackBrush, new PointF(420, currY));
                _gfx.DrawString(_obj.Lessor, _arial12, _blackBrush, GetOriginUponAlreadySetText(_gfx, ePosition.Right, "Lessor:", new Point(420, currY), _arial12));


                //11th ROW
                currY += 25;
                _gfx.DrawString("Covered", _arial12, _blackBrush, new PointF(20, currY));

                _gfx.DrawString("#Spaces:", _arial12, _blackBrush, new PointF(100, currY));
                _gfx.DrawString(_obj.ParkingCoveredSpaces.ToString(), _arial12, _blackBrush, GetOriginUponAlreadySetText(_gfx, ePosition.Right, "#Spaces:", new Point(100, currY), _arial12));

                _gfx.DrawString("Fee: $", _arial12, _blackBrush, new PointF(220, currY));
                _gfx.DrawString(_obj.ParkingCoveredFee.ToString(), _arial12, _blackBrush, GetOriginUponAlreadySetText(_gfx, ePosition.Right, "Fee: $", new Point(220, currY), _arial12));

                _gfx.DrawString("Start Date:", _arial12, _blackBrush, new PointF(420, currY));
                _gfx.DrawString(_obj.StartDate, _arial12, _blackBrush, GetOriginUponAlreadySetText(_gfx, ePosition.Right, "Start Date:", new Point(420, currY), _arial12));

                _gfx.DrawString("End Date:", _arial12, _blackBrush, new PointF(620, currY));
                _gfx.DrawString(_obj.EndDate, _arial12, _blackBrush, GetOriginUponAlreadySetText(_gfx, ePosition.Right, "End Date:", new Point(620, currY), _arial12));


                //12th ROW
                currY += 25;
                _gfx.DrawString("Garage", _arial12, _blackBrush, new PointF(20, currY));

                _gfx.DrawString("#Spaces:", _arial12, _blackBrush, new PointF(100, currY));
                _gfx.DrawString(_obj.ParkingCoveredSpaces.ToString(), _arial12, _blackBrush, GetOriginUponAlreadySetText(_gfx, ePosition.Right, "#Spaces:", new Point(100, currY), _arial12));

                _gfx.DrawString("Fee: $", _arial12, _blackBrush, new PointF(220, currY));
                _gfx.DrawString(_obj.ParkingCoveredFee.ToString(), _arial12, _blackBrush, GetOriginUponAlreadySetText(_gfx, ePosition.Right, "Fee: $", new Point(220, currY), _arial12));

                _gfx.DrawString("Rent: $", _arial12, _blackBrush, new PointF(420, currY));
                _gfx.DrawString(_obj.Rent.ToString(CurrFormatter), _arial12, _blackBrush, GetOriginUponAlreadySetText(_gfx, ePosition.Right, "Rent: $", new Point(420, currY), _arial12));


                //13th ROW
                currY += 25;
                _gfx.DrawString("Total", _arial12, _blackBrush, new PointF(20, currY));

                _gfx.DrawString("#Spaces:", _arial12, _blackBrush, new PointF(100, currY));
                _gfx.DrawString(_obj.ParkingTotalSpaces.ToString(), _arial12, _blackBrush, GetOriginUponAlreadySetText(_gfx, ePosition.Right, "#Spaces:", new Point(100, currY), _arial12));

                _gfx.DrawString("Fee: $", _arial12, _blackBrush, new PointF(220, currY));
                _gfx.DrawString(_obj.ParkingTotalFee.ToString(), _arial12, _blackBrush, GetOriginUponAlreadySetText(_gfx, ePosition.Right, "Fee: $", new Point(220, currY), _arial12));

                _gfx.DrawString("Comments:", _arial12, _blackBrush, new PointF(420, currY));
                lines = GetPartialLinesFromString(_gfx, _arial12, 300, _obj.LandLeaseComments);
                if (null != lines && lines.Length > 0)
                {
                    int xOffset = GetOriginUponAlreadySetText(_gfx, ePosition.Right, "Comments:", new Point(420, currY), _arial12).X;
                    foreach (string line in lines)
                    {
                        _gfx.DrawString(line, _arial12, _blackBrush, new PointF(xOffset, currY));
                        currY += 15;
                    }
                }
            }
            

            //14th ROW                                    
            currY += 25;
            _gfx.DrawString("COMMENTS:", _arial16, _blackBrush, new PointF(20, currY));


            //15th ROW
            currY += 25;

            _gfx.DrawString("Property Description:", _arial12, _blackBrush, new PointF(20, currY));
            lines = GetPartialLinesFromString(_gfx, _arial12, 675, _obj.PropertyDescription);
            if (null != lines && lines.Length > 0)
            {
                int xOffset = GetOriginUponAlreadySetText(_gfx, ePosition.Right, "Property Description:", new Point(20, currY), _arial12).X;
                foreach (string line in lines)
                {
                    _gfx.DrawString(line, _arial12, _blackBrush, new PointF(xOffset, currY));
                    currY += 15;
                }
            }


            //16th ROW
            currY += 15;

            _gfx.DrawString("Location Description:", _arial12, _blackBrush, new PointF(20, currY));
            lines = GetPartialLinesFromString(_gfx, _arial12, 675, _obj.LocationDescription);
            if (null != lines && lines.Length > 0)
            {
                int xOffset = GetOriginUponAlreadySetText(_gfx, ePosition.Right, "Location Description:", new Point(20, currY), _arial12).X;
                foreach (string line in lines)
                {
                    _gfx.DrawString(line, _arial12, _blackBrush, new PointF(xOffset, currY));
                    currY += 15;
                }
            }


            //17th ROW
            currY += 15;

            _gfx.DrawString("Recent Improvements:", _arial12, _blackBrush, new PointF(20, currY));
            lines = GetPartialLinesFromString(_gfx, _arial12, 675, _obj.RecentImprovements);
            if (null != lines && lines.Length > 0)
            {
                int xOffset = GetOriginUponAlreadySetText(_gfx, ePosition.Right, "Recent Improvements:", new Point(20, currY), _arial12).X;
                foreach (string line in lines)
                {
                    _gfx.DrawString(line, _arial12, _blackBrush, new PointF(xOffset, currY));
                    currY += 15;
                }
            }


            //18th ROW
            currY += 15;

            _gfx.DrawString("Improvements Needed:", _arial12, _blackBrush, new PointF(20, currY));
            lines = GetPartialLinesFromString(_gfx, _arial12, 675, _obj.ImprovementsNeeded);
            if (null != lines && lines.Length > 0)
            {
                int xOffset = GetOriginUponAlreadySetText(_gfx, ePosition.Right, "Improvements Needed", new Point(20, currY), _arial12).X;
                foreach (string line in lines)
                {
                    _gfx.DrawString(line, _arial12, _blackBrush, new PointF(xOffset, currY));
                    currY += 15;
                }
            }


            //19th ROW
            currY += 15;

            _gfx.DrawString("Amenities:", _arial12, _blackBrush, new PointF(20, currY));
            lines = GetPartialLinesFromString(_gfx, _arial12, 675, _obj.Amenities);
            if (null != lines && lines.Length > 0)
            {
                int xOffset = GetOriginUponAlreadySetText(_gfx, ePosition.Right, "Amenities:", new Point(20, currY), _arial12).X;
                foreach (string line in lines)
                {
                    _gfx.DrawString(line, _arial12, _blackBrush, new PointF(xOffset, currY));
                    currY += 15;
                }
            }


            //20th ROW
            currY += 15;

            _gfx.DrawString("Landscaping:", _arial12, _blackBrush, new PointF(20, currY));
            lines = GetPartialLinesFromString(_gfx, _arial12, 675, _obj.Landscaping);
            if (null != lines && lines.Length > 0)
            {
                int xOffset = GetOriginUponAlreadySetText(_gfx, ePosition.Right, "Landscaping:", new Point(20, currY), _arial12).X;
                foreach (string line in lines)
                {
                    _gfx.DrawString(line, _arial12, _blackBrush, new PointF(xOffset, currY));
                    currY += 15;
                }
            }
            
        }
    }
}
