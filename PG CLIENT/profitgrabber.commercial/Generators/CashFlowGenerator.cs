﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Printing;

using ProfitGrabber.Commercial.Objects;

namespace ProfitGrabber.Commercial.Generators
{
    public class CashFlowGenerator : BaseCommercialsGenerator
    {
        MultiyearAnalysisObject _maObj = null;
        ExpensesObject _expObj = null;
        LoansObject _lObj = null;
        int _nrOfYears = 1;

        const string ReportName = "CASH FLOW ANALYSIS";

        public List<decimal> gsiValuesPerYears = null;
        List<decimal> vacancyCreditLossPerYears = null;
        List<decimal> otherIncomePerYears = null;
        List<decimal> grossOperatingIncomePerYears = null;
        List<decimal> operatingExpensesPerYears = null;
        List<decimal> netOperatingIncomePerYears = null;

        List<decimal> valuesPerYears = null;

        List<decimal> debtServicePerYears = null;
        List<decimal> cumulativeDebtServicePerYears = null;
        
        List<double> principalPaymentsPerYears;
        List<double> interestPaymentsPerYears;

        List<double> cumulativePriciplePaymentsPerYears = null;
        List<double> cumulativeInterestPaymentsPerYears = null;

        List<double> capitalImprovementsPerYears = null;
        List<double> cumulativeCapitalImprovementsPerYears = null;

        List<double> cashFlowBeforeTaxesPerYears = null;
        List<double> cumulativeCashFlowBeforeTaxesPerYears = null;

        List<double> cashOnCashReturnPerYears = null;
        List<double> annualDepreciationPerYears = null;

        public List<double> taxableIncomePerYears = null;
        public List<double> totalTaxCostPerYears = null;
        List<double> cashFlowAfterTaxesPerYears = null;
        List<double> cumulativeCashFlowAfterTaxesPerYears = null;


        public CashFlowGenerator()
        {
 
        }

        public void InitGenerator(MultiyearAnalysisObject maObj, ExpensesObject expObj, LoansObject lObj, int nrOfYears)
        {
            _maObj = maObj;
            _expObj = expObj;
            _lObj = lObj;
            _nrOfYears = 1;
        }

        public void InitCurvesForGraph( int nrOfYears,
                                        out List<decimal> gsiValuesPerYears, 
                                        out List<decimal> grossOperatingIncomePerYears, 
                                        out List<decimal> netOperatingIncomePerYears, 
                                        out List<double> cashFlowBeforeTaxesPerYears,
                                        out List<double> cashFlowAfterTaxesPerYears)
        {
            _nrOfYears = nrOfYears;
            //List<decimal> valuesPerYears = InitValues(_nrOfYears);
            gsiValuesPerYears = InitGsiValuesPerYears(_nrOfYears);
            List<decimal> vacancyCreditLossPerYears = InitVacancyValuesPerYears(_nrOfYears, gsiValuesPerYears);
            List<decimal> otherIncomePerYears = InitOtherIncomePerYears(_nrOfYears);
            grossOperatingIncomePerYears = InitGrossOperatingIncomePerYears(_nrOfYears, gsiValuesPerYears, vacancyCreditLossPerYears, otherIncomePerYears);
            List<decimal> operatingExpensesPerYears = InitOperatingExpensesPerYears(_nrOfYears, gsiValuesPerYears);
            netOperatingIncomePerYears = InitNetOperatingIncomePerYears(_nrOfYears, grossOperatingIncomePerYears, operatingExpensesPerYears);
            List<decimal> valuesPerYears = InitValues(_nrOfYears, netOperatingIncomePerYears);
            List<decimal> debtServicePerYears = InitDebtServicePerYears(_nrOfYears);
            List<decimal> cumulativeDebtServicePerYears = InitCumulativeDebtServicePerYears(_nrOfYears, debtServicePerYears);

            List<double> principalPaymentsPerYears;
            List<double> interestPaymentsPerYears;

            InitPrincipalAndInterestPaymentsPerYears(_nrOfYears, out principalPaymentsPerYears, out interestPaymentsPerYears);

            List<double> cumulativePriciplePaymentsPerYears = InitCumulativePrinciplePaymentsPerYears(_nrOfYears, principalPaymentsPerYears);
            List<double> cumulativeInterestPaymentsPerYears = InitCumulativeInterestPaymentsPerYears(_nrOfYears, interestPaymentsPerYears);

            List<double> capitalImprovementsPerYears = InitCapitalImprovementsPerYears(_nrOfYears, interestPaymentsPerYears);
            List<double> cumulativeCapitalImprovementsPerYears = InitCumulativeCapitalImprovementsPerYears(_nrOfYears, capitalImprovementsPerYears);

            cashFlowBeforeTaxesPerYears = InitCashFlowBeforeTaxesPerYears(_nrOfYears, netOperatingIncomePerYears, debtServicePerYears, capitalImprovementsPerYears);
            List<double> cumulativeCashFlowBeforeTaxesPerYears = InitCumulativeCashFlowBeoreTaxesPerYears(_nrOfYears, cashFlowBeforeTaxesPerYears);

            List<double> cashOnCashReturnPerYears = InitCashOnCashReturnPerYears(_nrOfYears, cumulativeCashFlowBeforeTaxesPerYears, Util.GetDecimalFromString(_maObj.InitialInvestments), cumulativeCapitalImprovementsPerYears);
            List<double> annualDepreciationPerYears = InitDepreciableValuePerYears(_nrOfYears, Util.GetDecimalFromString(_maObj.BuildingValue), cumulativeCapitalImprovementsPerYears, Util.GetDecimalFromString(_maObj.DepreciableYears));

            List<double> taxableIncomePerYears = InitTaxableIncomePerYears(_nrOfYears, netOperatingIncomePerYears, interestPaymentsPerYears, annualDepreciationPerYears);
            List<double> totalTaxCostPerYears = InitTotalTaxCostPerYears(_nrOfYears, taxableIncomePerYears, (double)Util.GetDecimalFromString(_maObj.FederalRate), (double)Util.GetDecimalFromString(_maObj.StateRate));
            cashFlowAfterTaxesPerYears = InitCashFlowAfterTaxesPerYears(_nrOfYears, cashFlowBeforeTaxesPerYears, totalTaxCostPerYears);            
        }

        public void GenerateValues(int nrOfYears)
        {
            gsiValuesPerYears = InitGsiValuesPerYears(nrOfYears);
            vacancyCreditLossPerYears = InitVacancyValuesPerYears(nrOfYears, gsiValuesPerYears);
            otherIncomePerYears = InitOtherIncomePerYears(nrOfYears);
            grossOperatingIncomePerYears = InitGrossOperatingIncomePerYears(nrOfYears, gsiValuesPerYears, vacancyCreditLossPerYears, otherIncomePerYears);
            operatingExpensesPerYears = InitOperatingExpensesPerYears(nrOfYears, gsiValuesPerYears);
            netOperatingIncomePerYears = InitNetOperatingIncomePerYears(nrOfYears, grossOperatingIncomePerYears, operatingExpensesPerYears);

            valuesPerYears = InitValues(nrOfYears, netOperatingIncomePerYears);

            debtServicePerYears = InitDebtServicePerYears(nrOfYears);
            cumulativeDebtServicePerYears = InitCumulativeDebtServicePerYears(nrOfYears, debtServicePerYears);

            List<double> principalPaymentsPerYears;
            List<double> interestPaymentsPerYears;

            InitPrincipalAndInterestPaymentsPerYears(nrOfYears, out principalPaymentsPerYears, out interestPaymentsPerYears);

            cumulativePriciplePaymentsPerYears = InitCumulativePrinciplePaymentsPerYears(nrOfYears, principalPaymentsPerYears);
            cumulativeInterestPaymentsPerYears = InitCumulativeInterestPaymentsPerYears(nrOfYears, interestPaymentsPerYears);

            capitalImprovementsPerYears = InitCapitalImprovementsPerYears(nrOfYears, interestPaymentsPerYears);
            cumulativeCapitalImprovementsPerYears = InitCumulativeCapitalImprovementsPerYears(nrOfYears, capitalImprovementsPerYears);

            cashFlowBeforeTaxesPerYears = InitCashFlowBeforeTaxesPerYears(nrOfYears, netOperatingIncomePerYears, debtServicePerYears, capitalImprovementsPerYears);
            cumulativeCashFlowBeforeTaxesPerYears = InitCumulativeCashFlowBeoreTaxesPerYears(nrOfYears, cashFlowBeforeTaxesPerYears);

            cashOnCashReturnPerYears = InitCashOnCashReturnPerYears(nrOfYears, cumulativeCashFlowBeforeTaxesPerYears, Util.GetDecimalFromString(_maObj.InitialInvestments), cumulativeCapitalImprovementsPerYears);
            annualDepreciationPerYears = InitDepreciableValuePerYears(nrOfYears, Util.GetDecimalFromString(_maObj.BuildingValue), cumulativeCapitalImprovementsPerYears, Util.GetDecimalFromString(_maObj.DepreciableYears));

            taxableIncomePerYears = InitTaxableIncomePerYears(nrOfYears, netOperatingIncomePerYears, interestPaymentsPerYears, annualDepreciationPerYears);
            totalTaxCostPerYears = InitTotalTaxCostPerYears(nrOfYears, taxableIncomePerYears, (double)Util.GetDecimalFromString(_maObj.FederalRate), (double)Util.GetDecimalFromString(_maObj.StateRate));
            cashFlowAfterTaxesPerYears = InitCashFlowAfterTaxesPerYears(nrOfYears, cashFlowBeforeTaxesPerYears, totalTaxCostPerYears);
            cumulativeCashFlowAfterTaxesPerYears = InitCumulativeCashFlowAfterTaxesPerYears(nrOfYears, cashFlowAfterTaxesPerYears);
        }

        public void Generate(PrintPageEventArgs e, MultiyearAnalysisObject maObj, ExpensesObject expObj, LoansObject lObj, int nrOfYears)
        {
            _maObj = maObj;
            _expObj = expObj;
            _lObj = lObj;
            _e = e;
            _gfx = e.Graphics;
            _paperWidth = e.PageSettings.PaperSize.Height;

            int currY = 60;
            string[] lines = new string[1] { string.Empty };

            //List<decimal> valuesPerYears = InitValues(nrOfYears);
            gsiValuesPerYears = InitGsiValuesPerYears(nrOfYears);
            vacancyCreditLossPerYears = InitVacancyValuesPerYears(nrOfYears, gsiValuesPerYears);
            otherIncomePerYears = InitOtherIncomePerYears(nrOfYears);
            grossOperatingIncomePerYears = InitGrossOperatingIncomePerYears(nrOfYears, gsiValuesPerYears, vacancyCreditLossPerYears, otherIncomePerYears);
            operatingExpensesPerYears = InitOperatingExpensesPerYears(nrOfYears, gsiValuesPerYears);
            netOperatingIncomePerYears = InitNetOperatingIncomePerYears(nrOfYears, grossOperatingIncomePerYears, operatingExpensesPerYears);

            valuesPerYears = InitValues(nrOfYears, netOperatingIncomePerYears);

            debtServicePerYears = InitDebtServicePerYears(nrOfYears);
            cumulativeDebtServicePerYears = InitCumulativeDebtServicePerYears(nrOfYears, debtServicePerYears);
            
            List<double> principalPaymentsPerYears;
            List<double> interestPaymentsPerYears;

            InitPrincipalAndInterestPaymentsPerYears(nrOfYears, out principalPaymentsPerYears, out interestPaymentsPerYears);

            cumulativePriciplePaymentsPerYears = InitCumulativePrinciplePaymentsPerYears(nrOfYears, principalPaymentsPerYears);
            cumulativeInterestPaymentsPerYears = InitCumulativeInterestPaymentsPerYears(nrOfYears, interestPaymentsPerYears);

            capitalImprovementsPerYears = InitCapitalImprovementsPerYears(nrOfYears, interestPaymentsPerYears);
            cumulativeCapitalImprovementsPerYears = InitCumulativeCapitalImprovementsPerYears(nrOfYears, capitalImprovementsPerYears);

            cashFlowBeforeTaxesPerYears = InitCashFlowBeforeTaxesPerYears(nrOfYears, netOperatingIncomePerYears, debtServicePerYears, capitalImprovementsPerYears);
            cumulativeCashFlowBeforeTaxesPerYears = InitCumulativeCashFlowBeoreTaxesPerYears(nrOfYears, cashFlowBeforeTaxesPerYears);

            cashOnCashReturnPerYears = InitCashOnCashReturnPerYears(nrOfYears, cumulativeCashFlowBeforeTaxesPerYears, Util.GetDecimalFromString(_maObj.InitialInvestments), cumulativeCapitalImprovementsPerYears);
            annualDepreciationPerYears = InitDepreciableValuePerYears(nrOfYears, Util.GetDecimalFromString(_maObj.BuildingValue), cumulativeCapitalImprovementsPerYears, Util.GetDecimalFromString(_maObj.DepreciableYears));

            taxableIncomePerYears = InitTaxableIncomePerYears(nrOfYears, netOperatingIncomePerYears, interestPaymentsPerYears, annualDepreciationPerYears);
            totalTaxCostPerYears = InitTotalTaxCostPerYears(nrOfYears, taxableIncomePerYears, (double)Util.GetDecimalFromString(_maObj.FederalRate), (double)Util.GetDecimalFromString(_maObj.StateRate));
            cashFlowAfterTaxesPerYears = InitCashFlowAfterTaxesPerYears(nrOfYears, cashFlowBeforeTaxesPerYears, totalTaxCostPerYears);
            cumulativeCashFlowAfterTaxesPerYears = InitCumulativeCashFlowAfterTaxesPerYears(nrOfYears, cashFlowAfterTaxesPerYears);

            #region HEADER
            _gfx.DrawString(ReportName, base._arial22, base._blackBrush, base.GetOrigin(_gfx, ePosition.Center, ReportName, base._arial22, _paperWidth, 20));

            _gfx.DrawString("Purchase Information", _arial10B, _blackBrush, new PointF(40, currY));
            _gfx.DrawString("Initial Income and", _arial10B, _blackBrush, new PointF(220, currY));
            _gfx.DrawString("Expense Factors", _arial10B, _blackBrush, new PointF(220, currY + 15));
            _gfx.DrawString("Annual Changes", _arial10B, _blackBrush, new PointF(420, currY));
            _gfx.DrawString("Loan Information", _arial10B, _blackBrush, new PointF(580, currY));
            _gfx.DrawString("Tax Information", _arial10B, _blackBrush, new PointF(790, currY));
            _gfx.DrawString("Sale Information", _arial10B, _blackBrush, new PointF(950, currY));

            _gfx.DrawLine(_blackPen, new Point(40, currY + 30), new Point(1050, currY + 30));

            currY += 40;
            _gfx.DrawString("Down Payment: ", _arial10, _blackBrush, new PointF(40, currY));
            _gfx.DrawString("Initial GSI: " + _maObj.GsiAnnual, _arial10, _blackBrush, new PointF(220, currY));            
            _gfx.DrawString("%Income: " + _maObj.AnnualIncomeChange, _arial10, _blackBrush, new PointF(420, currY));
            if (null != _lObj && null != _lObj.LoansProperty && null != _lObj.LoansProperty[0])
                _gfx.DrawString("1st: " + _lObj.LoansProperty[0].ToString(), _arial10, _blackBrush, new PointF(580, currY));
            _gfx.DrawString("Fed. Income Rate: " + _maObj.FederalRate, _arial10, _blackBrush, new PointF(790, currY));
            _gfx.DrawString("Sale Cap Rate: " + _maObj.SaleCapRate, _arial10, _blackBrush, new PointF(950, currY));

            currY += 20;
            _gfx.DrawString("$" + _maObj.DownPayement, _arial10, _blackBrush, new PointF(80, currY));
            _gfx.DrawString("Init. %Vacancy: " + _maObj.VacancyCreditLossPerc, _arial10, _blackBrush, new PointF(220, currY));
            _gfx.DrawString("%Vacancy: " + _maObj.AnnualVacancyChange, _arial10, _blackBrush, new PointF(420, currY));
            if (null != _lObj && null != _lObj.LoansProperty && null != _lObj.LoansProperty[1])
                _gfx.DrawString("2nd: " + _lObj.LoansProperty[1].ToString(), _arial10, _blackBrush, new PointF(580, currY));
            _gfx.DrawString("State Income Rate: " + _maObj.StateRate, _arial10, _blackBrush, new PointF(790, currY));

            currY += 20;
            _gfx.DrawString("Purchase Costs: ", _arial10, _blackBrush, new PointF(40, currY));
            _gfx.DrawString("Init. Other Income: " + _maObj.OtherIncome, _arial10, _blackBrush, new PointF(220, currY));
            _gfx.DrawString("%Inflation: " + _maObj.AnnualInflationChange, _arial10, _blackBrush, new PointF(420, currY));
            if (null != _lObj && null != _lObj.LoansProperty && null != _lObj.LoansProperty[2])
                _gfx.DrawString("3rd: " + _lObj.LoansProperty[2].ToString(), _arial10, _blackBrush, new PointF(580, currY));
            _gfx.DrawString("Depreciable Value: " + _maObj.BuildingValue, _arial10, _blackBrush, new PointF(790, currY));

            currY += 20;
            _gfx.DrawString("$" + _maObj.PurchasePrice, _arial10, _blackBrush, new PointF(80, currY));
            _gfx.DrawString("Init. Oper. Expenses: " + _maObj.OperatingExpenses, _arial10, _blackBrush, new PointF(220, currY));
            _gfx.DrawString("%Oper. Expenses: " + _maObj.AnnualOperatingExpensesChange, _arial10, _blackBrush, new PointF(420, currY));            
            _gfx.DrawString("Depreciable Years: " + _maObj.DepreciableYears, _arial10, _blackBrush, new PointF(790, currY));


            currY += 25;
            _gfx.DrawString("CAPITAL ADDITIONS (IMPROVEMENTS)", _arial12, _blackBrush, new PointF(20, currY));

            //TABLE
            currY += 25;
            int startY = currY;
            _gfx.DrawLine(_blackPen, new Point(40, currY), new Point(940, currY));

            _gfx.DrawString(string.Empty, _arial10, _blackBrush, new Point(40, currY));
            _gfx.DrawString("Year 1", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "Year 1", _arial10, new Rectangle(140, currY, 80, 20)));
            _gfx.DrawString("Year 2", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "Year 2", _arial10, new Rectangle(220, currY, 80, 20)));
            _gfx.DrawString("Year 3", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "Year 3", _arial10, new Rectangle(300, currY, 80, 20)));
            _gfx.DrawString("Year 4", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "Year 4", _arial10, new Rectangle(380, currY, 80, 20)));
            _gfx.DrawString("Year 5", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "Year 5", _arial10, new Rectangle(460, currY, 80, 20)));
            _gfx.DrawString("Year 6", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "Year 6", _arial10, new Rectangle(540, currY, 80, 20)));
            _gfx.DrawString("Year 7", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "Year 7", _arial10, new Rectangle(620, currY, 80, 20)));
            _gfx.DrawString("Year 8", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "Year 8", _arial10, new Rectangle(700, currY, 80, 20)));
            _gfx.DrawString("Year 9", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "Year 9", _arial10, new Rectangle(780, currY, 80, 20)));
            _gfx.DrawString("Year 10", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "Year 10", _arial10, new Rectangle(860, currY, 80, 20)));

            currY += 20;
            _gfx.DrawLine(_blackPen, new Point(40, currY), new Point(940, currY));
            _gfx.DrawString("Cost:", _arial10, _blackBrush, new Point(90, currY));

            _gfx.DrawString("$" + expObj.CapitalAdditionsProperty.yearsProperty[0]._costProperty, _arial10, _blackBrush, new Point(220, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
            _gfx.DrawString("$" + expObj.CapitalAdditionsProperty.yearsProperty[1]._costProperty, _arial10, _blackBrush, new Point(300, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
            _gfx.DrawString("$" + expObj.CapitalAdditionsProperty.yearsProperty[2]._costProperty, _arial10, _blackBrush, new Point(380, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
            _gfx.DrawString("$" + expObj.CapitalAdditionsProperty.yearsProperty[3]._costProperty, _arial10, _blackBrush, new Point(460, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
            _gfx.DrawString("$" + expObj.CapitalAdditionsProperty.yearsProperty[4]._costProperty, _arial10, _blackBrush, new Point(540, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
            _gfx.DrawString("$" + expObj.CapitalAdditionsProperty.yearsProperty[5]._costProperty, _arial10, _blackBrush, new Point(620, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
            _gfx.DrawString("$" + expObj.CapitalAdditionsProperty.yearsProperty[6]._costProperty, _arial10, _blackBrush, new Point(700, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
            _gfx.DrawString("$" + expObj.CapitalAdditionsProperty.yearsProperty[7]._costProperty, _arial10, _blackBrush, new Point(780, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
            _gfx.DrawString("$" + expObj.CapitalAdditionsProperty.yearsProperty[8]._costProperty, _arial10, _blackBrush, new Point(860, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
            _gfx.DrawString("$" + expObj.CapitalAdditionsProperty.yearsProperty[9]._costProperty, _arial10, _blackBrush, new Point(940, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));

            currY += 20;
            _gfx.DrawLine(_blackPen, new Point(40, currY), new Point(940, currY));
            _gfx.DrawString("Type:", _arial10, _blackBrush, new Point(90, currY));

            _gfx.DrawString(expObj.CapitalAdditionsProperty.yearsProperty[0]._typeProperty, _arial10, _blackBrush, new Point(220, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
            _gfx.DrawString(expObj.CapitalAdditionsProperty.yearsProperty[1]._typeProperty, _arial10, _blackBrush, new Point(300, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
            _gfx.DrawString(expObj.CapitalAdditionsProperty.yearsProperty[2]._typeProperty, _arial10, _blackBrush, new Point(380, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
            _gfx.DrawString(expObj.CapitalAdditionsProperty.yearsProperty[3]._typeProperty, _arial10, _blackBrush, new Point(460, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
            _gfx.DrawString(expObj.CapitalAdditionsProperty.yearsProperty[4]._typeProperty, _arial10, _blackBrush, new Point(540, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
            _gfx.DrawString(expObj.CapitalAdditionsProperty.yearsProperty[5]._typeProperty, _arial10, _blackBrush, new Point(620, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
            _gfx.DrawString(expObj.CapitalAdditionsProperty.yearsProperty[6]._typeProperty, _arial10, _blackBrush, new Point(700, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
            _gfx.DrawString(expObj.CapitalAdditionsProperty.yearsProperty[7]._typeProperty, _arial10, _blackBrush, new Point(780, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
            _gfx.DrawString(expObj.CapitalAdditionsProperty.yearsProperty[8]._typeProperty, _arial10, _blackBrush, new Point(860, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
            _gfx.DrawString(expObj.CapitalAdditionsProperty.yearsProperty[9]._typeProperty, _arial10, _blackBrush, new Point(940, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));

            currY += 20;
            _gfx.DrawLine(_blackPen, new Point(40, currY), new Point(940, currY));

            //DRAW VERTICAL LINES
            _gfx.DrawLine(_blackPen, new Point(40, startY), new Point(40, currY));
            _gfx.DrawLine(_blackPen, new Point(140, startY), new Point(140, currY));
            _gfx.DrawLine(_blackPen, new Point(220, startY), new Point(220, currY));
            _gfx.DrawLine(_blackPen, new Point(300, startY), new Point(300, currY));
            _gfx.DrawLine(_blackPen, new Point(380, startY), new Point(380, currY));
            _gfx.DrawLine(_blackPen, new Point(460, startY), new Point(460, currY));
            _gfx.DrawLine(_blackPen, new Point(540, startY), new Point(540, currY));
            _gfx.DrawLine(_blackPen, new Point(620, startY), new Point(620, currY));
            _gfx.DrawLine(_blackPen, new Point(700, startY), new Point(700, currY));
            _gfx.DrawLine(_blackPen, new Point(780, startY), new Point(780, currY));
            _gfx.DrawLine(_blackPen, new Point(860, startY), new Point(860, currY));
            _gfx.DrawLine(_blackPen, new Point(940, startY), new Point(940, currY));

            currY += 25;
            #endregion

            #region BODY
            for (int iRow = 0; iRow <= 24; iRow++)
            {                
                _gfx.DrawLine(_blackPen, new Point(40, currY), new Point(1050, currY));

                if (iRow >= 1)
                    _gfx.DrawString(GetLabel(iRow), _arial10, _blackBrush, new Point(42, currY));

                if (iRow < 24)
                {
                    _gfx.DrawLine(_blackPen, new Point(40, currY), new Point(40, currY + 20));
                    _gfx.DrawLine(_blackPen, new Point(250, currY), new Point(250, currY + 20));
                    _gfx.DrawLine(_blackPen, new Point(1050, currY), new Point(1050, currY + 20));
                    
                    for (int jCol = 0; jCol < nrOfYears; jCol++)
                    {
                        _gfx.DrawLine(_blackPen, new Point(250 + (jCol + 1) * 80, currY), new Point(250 + (jCol + 1) * 80, currY + 20));
                        
                        if (0 == iRow)
                            _gfx.DrawString("Year " + (jCol + 1).ToString(), _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "Year " + (jCol + 1).ToString(), _arial10, new Rectangle(250 + (jCol) * 80, currY, 80, 20)));
                        else if (1 == iRow)
                            _gfx.DrawString(valuesPerYears[jCol].ToString(Math.Abs(valuesPerYears[jCol]) < FormatterLimit ? CurrFormatter : CurrFormatterEx), _arial10, _blackBrush, new Point(250 + (jCol + 1) * 80, currY + 2), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                        else if (2 == iRow)
                            _gfx.DrawString(gsiValuesPerYears[jCol].ToString(Math.Abs(gsiValuesPerYears[jCol]) < FormatterLimit ? CurrFormatter : CurrFormatterEx), _arial10, _blackBrush, new Point(250 + (jCol + 1) * 80, currY + 2), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                        else if (3 == iRow)
                            _gfx.DrawString(vacancyCreditLossPerYears[jCol].ToString(Math.Abs(vacancyCreditLossPerYears[jCol]) < FormatterLimit ? CurrFormatter : CurrFormatterEx), _arial10, _blackBrush, new Point(250 + (jCol + 1) * 80, currY + 2), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                        else if (4 == iRow)
                            _gfx.DrawString(otherIncomePerYears[jCol].ToString(Math.Abs(otherIncomePerYears[jCol]) < FormatterLimit ? CurrFormatter : CurrFormatterEx), _arial10, _blackBrush, new Point(250 + (jCol + 1) * 80, currY + 2), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                        else if (5 == iRow)
                            _gfx.DrawString(grossOperatingIncomePerYears[jCol].ToString(Math.Abs(grossOperatingIncomePerYears[jCol]) < FormatterLimit ? CurrFormatter : CurrFormatterEx), _arial10, _blackBrush, new Point(250 + (jCol + 1) * 80, currY + 2), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                        else if (6 == iRow)
                            _gfx.DrawString(operatingExpensesPerYears[jCol].ToString(Math.Abs(operatingExpensesPerYears[jCol]) < FormatterLimit ? CurrFormatter : CurrFormatterEx), _arial10, _blackBrush, new Point(250 + (jCol + 1) * 80, currY + 2), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                        else if (7 == iRow)
                            _gfx.DrawString(netOperatingIncomePerYears[jCol].ToString(Math.Abs(netOperatingIncomePerYears[jCol]) < FormatterLimit ? CurrFormatter : CurrFormatterEx), _arial10, _blackBrush, new Point(250 + (jCol + 1) * 80, currY + 2), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                        else if (8 == iRow)
                            _gfx.DrawString(debtServicePerYears[jCol].ToString(Math.Abs(debtServicePerYears[jCol]) < FormatterLimit ? CurrFormatter : CurrFormatterEx), _arial10, _blackBrush, new Point(250 + (jCol + 1) * 80, currY + 2), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                        else if (9 == iRow)
                            _gfx.DrawString(cumulativeDebtServicePerYears[jCol].ToString(Math.Abs(cumulativeDebtServicePerYears[jCol]) < FormatterLimit ? CurrFormatter : CurrFormatterEx), _arial10, _blackBrush, new Point(250 + (jCol + 1) * 80, currY + 2), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                        else if (10 == iRow)
                            _gfx.DrawString(principalPaymentsPerYears[jCol].ToString(Math.Abs(principalPaymentsPerYears[jCol]) < FormatterLimit ? CurrFormatter : CurrFormatterEx), _arial10, _blackBrush, new Point(250 + (jCol + 1) * 80, currY + 2), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                        else if (11 == iRow)
                            _gfx.DrawString(cumulativePriciplePaymentsPerYears[jCol].ToString(Math.Abs(cumulativePriciplePaymentsPerYears[jCol]) < FormatterLimit ? CurrFormatter : CurrFormatterEx), _arial10, _blackBrush, new Point(250 + (jCol + 1) * 80, currY + 2), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                        else if (12 == iRow)
                            _gfx.DrawString(interestPaymentsPerYears[jCol].ToString(Math.Abs(interestPaymentsPerYears[jCol]) < FormatterLimit ? CurrFormatter : CurrFormatterEx), _arial10, _blackBrush, new Point(250 + (jCol + 1) * 80, currY + 2), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                        else if (13 == iRow)
                            _gfx.DrawString(cumulativeInterestPaymentsPerYears[jCol].ToString(Math.Abs(cumulativeInterestPaymentsPerYears[jCol]) < FormatterLimit ? CurrFormatter : CurrFormatterEx), _arial10, _blackBrush, new Point(250 + (jCol + 1) * 80, currY + 2), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                        else if (14 == iRow)
                            _gfx.DrawString(capitalImprovementsPerYears[jCol].ToString(Math.Abs(capitalImprovementsPerYears[jCol]) < FormatterLimit ? CurrFormatter : CurrFormatterEx), _arial10, _blackBrush, new Point(250 + (jCol + 1) * 80, currY + 2), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                        else if (15 == iRow)
                            _gfx.DrawString(cumulativeCapitalImprovementsPerYears[jCol].ToString(Math.Abs(cumulativeCapitalImprovementsPerYears[jCol]) < FormatterLimit ? CurrFormatter : CurrFormatterEx), _arial10, _blackBrush, new Point(250 + (jCol + 1) * 80, currY + 2), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                        else if (16 == iRow)
                            _gfx.DrawString(cashFlowBeforeTaxesPerYears[jCol].ToString(Math.Abs(cashFlowBeforeTaxesPerYears[jCol]) < FormatterLimit ? CurrFormatter : CurrFormatterEx), _arial10, _blackBrush, new Point(250 + (jCol + 1) * 80, currY + 2), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                        else if (17 == iRow)
                            _gfx.DrawString(cumulativeCashFlowBeforeTaxesPerYears[jCol].ToString(Math.Abs(cumulativeCashFlowBeforeTaxesPerYears[jCol]) < FormatterLimit ? CurrFormatter : CurrFormatterEx), _arial10, _blackBrush, new Point(250 + (jCol + 1) * 80, currY + 2), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                        else if (18 == iRow)
                            _gfx.DrawString((cashOnCashReturnPerYears[jCol] * 100).ToString(Math.Abs((cashOnCashReturnPerYears[jCol] * 100)) < FormatterLimit ? CurrFormatter : CurrFormatterEx) + "%", _arial10, _blackBrush, new Point(250 + (jCol + 1) * 80, currY + 2), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                        else if (19 == iRow)
                            _gfx.DrawString(annualDepreciationPerYears[jCol].ToString(Math.Abs(annualDepreciationPerYears[jCol]) < FormatterLimit ? CurrFormatter : CurrFormatterEx), _arial10, _blackBrush, new Point(250 + (jCol + 1) * 80, currY + 2), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                        else if (20 == iRow)
                            _gfx.DrawString(taxableIncomePerYears[jCol].ToString(Math.Abs(taxableIncomePerYears[jCol]) < FormatterLimit ? CurrFormatter : CurrFormatterEx), _arial10, _blackBrush, new Point(250 + (jCol + 1) * 80, currY + 2), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                        else if (21 == iRow)
                            _gfx.DrawString(totalTaxCostPerYears[jCol].ToString(Math.Abs(totalTaxCostPerYears[jCol]) < FormatterLimit ? CurrFormatter : CurrFormatterEx), _arial10, _blackBrush, new Point(250 + (jCol + 1) * 80, currY + 2), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                        else if (22 == iRow)
                            _gfx.DrawString(cashFlowAfterTaxesPerYears[jCol].ToString(Math.Abs(cashFlowAfterTaxesPerYears[jCol]) < FormatterLimit ? CurrFormatter : CurrFormatterEx), _arial10, _blackBrush, new Point(250 + (jCol + 1) * 80, currY + 2), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                        else if (23 == iRow)
                            _gfx.DrawString(cumulativeCashFlowAfterTaxesPerYears[jCol].ToString(Math.Abs(cumulativeCashFlowAfterTaxesPerYears[jCol]) < FormatterLimit ? CurrFormatter : CurrFormatterEx), _arial10, _blackBrush, new Point(250 + (jCol + 1) * 80, currY + 2), new StringFormat(StringFormatFlags.DirectionRightToLeft));                       
                    }
                }
                currY += 20;
            }
             #endregion
        }
       

        #region VALUE
        /*List<decimal> InitValues(int years)
        {
            List<decimal> vals = new List<decimal>();

            decimal saleCapRate = Util.GetDecimalFromString(_maObj.SaleCapRate) / 100;
            
            decimal gsi = Util.GetDecimalFromString(_maObj.GsiAnnual);
            decimal vac = Util.GetDecimalFromString(_maObj.VacancyCreditLoss);
            decimal other = Util.GetDecimalFromString(_maObj.OtherIncome);

            for (int i = 0; i < years; i++)
            {
                decimal noi = (gsi - vac + other) - (Util.GetDecimalFromString(_maObj.OperatingExpensesPercs[i]) / 100 * gsi);
                if (0 != saleCapRate)
                    vals.Add(noi / saleCapRate);
                else
                    vals.Add(0);
            }

            return vals;
        }*/

        List<decimal>  InitValues(int years, List<decimal> noi)
        {
            List<decimal> vals = new List<decimal>();
            decimal saleCapRate = Util.GetDecimalFromString(_maObj.SaleCapRate) / 100;

            for (int i = 0; i < years; i++)
            {                
                if (0 != saleCapRate)
                    vals.Add(noi[i] / saleCapRate);
                else
                    vals.Add(0);
            }

            return vals;
        }
        #endregion

        #region RENTAL INCOME (GSI)
        List<decimal> InitGsiValuesPerYears(int years)
        {
            List<decimal> gsiVals = new List<decimal>();            
            for (int i = 0; i < years; i++)
            {
                if (0 == i)
                {
                    gsiVals.Add(Util.GetDecimalFromString(_maObj.GsiAnnual));
                }
                else
                {
                    gsiVals.Add(gsiVals[i - 1] * (1 + Util.GetDecimalFromString(_maObj.IncomeChangePercs[i]) / 100));
                }
            }
            return gsiVals;
        }
        #endregion

        #region VACANCY / CREDIT LOSS
        List<decimal> InitVacancyValuesPerYears(int years, List<decimal> gsis)
        {
            List<decimal> vals = new List<decimal>();
            for (int i = 0; i < years; i++)
            {
                decimal yVacPerc = Util.GetDecimalFromString(_maObj.VacancyPercs[i]) / 100;
                decimal yGSI = gsis[i];
                vals.Add(yVacPerc * yGSI);
            }
            return vals;
        }
        #endregion

        #region OTHER INCOME
        List<decimal> InitOtherIncomePerYears(int years)
        {
            List<decimal> vals = new List<decimal>();

            decimal inflationRate = Util.GetDecimalFromString(_maObj.AnnualInflationChange) / 100;
            decimal initialOtherIncome = Util.GetDecimalFromString(_maObj.OtherIncome);

            for (int i = 0; i < years; i++)
            {
                if (0 == i)
                {                    
                    vals.Add(initialOtherIncome);
                }
                else
                {
                    vals.Add(vals[i - 1] * (1 + inflationRate));
                }
            }
            return vals;
        }
        #endregion

        #region GROSS OPERATING INCOME
        List<decimal> InitGrossOperatingIncomePerYears(int years, List<decimal> gsis, List<decimal> vacs, List<decimal> otherIncomes)
        {
            List<decimal> vals = new List<decimal>();
            
            for (int i = 0; i < years; i++)
            {                
                vals.Add(gsis[i] - vacs[i] + otherIncomes[i]);                
            }
            return vals;
        }
        #endregion

        #region OPERATING EXPENSES
        List<decimal> InitOperatingExpensesPerYears(int years, List<decimal> gsis)
        {
            List<decimal> vals = new List<decimal>();

            for (int i = 0; i < years; i++)
            {
                if (0 == i)
                {
                    vals.Add(Util.GetDecimalFromString(_maObj.OperatingExpenses));
                }
                else
                {
                    vals.Add(Util.GetDecimalFromString(_maObj.OperatingExpensesPercs[i]) / 100 * gsis[i]);
                }
            }
            return vals;
        }
        #endregion

        #region NET OPERATING INCOME
        List<decimal> InitNetOperatingIncomePerYears(int years, List<decimal> gois, List<decimal> oes)
        {
            List<decimal> vals = new List<decimal>();

            for (int i = 0; i < years; i++)
            {
                vals.Add(gois[i] - oes[i]);
            }
            return vals;
        }
        #endregion

        #region DEBT SERVICE
        List<decimal> InitDebtServicePerYears(int years)
        {
            List<decimal> vals = new List<decimal>();

            for (int i = 0; i < years; i++)
            {
                vals.Add(_lObj.TotalAnnualDebtService);
            }
            return vals;
        }
        #endregion

        #region CUMULATIVE DEBT SERVICE
        List<decimal> InitCumulativeDebtServicePerYears(int years, List<decimal> debtServicePerYears)
        {
            List<decimal> vals = new List<decimal>();

            for (int i = 0; i < years; i++)
            {
                if (0 == i)
                {
                    vals.Add(_lObj.TotalAnnualDebtService);
                }
                else
                {
                    vals.Add(vals[i - 1] + debtServicePerYears[i]);
                }
            }
            return vals;
        }
        #endregion

        #region PRINCIPAL PAYMENTS
        void InitPrincipalAndInterestPaymentsPerYears(int years, out List<double> pjs, out List<double> ijs)
        {
            pjs = new List<double>();
            ijs = new List<double>();

            //INIT VALUES
            for (int iYrCnt = 0; iYrCnt < years; iYrCnt++)
            {
                pjs.Add(0);
                ijs.Add(0);
            }

            if (null != _lObj && null != _lObj.LoansProperty && 0 < _lObj.LoansProperty.Length)
            {
                for (int iLoanCnt = 0; iLoanCnt < _lObj.LoansProperty.Length; iLoanCnt++)
                {
                    double P = (double)_lObj.LoansProperty[iLoanCnt].AmountValue;
                    double i = (double)((_lObj.LoansProperty[iLoanCnt].RatePercValue / 100) / 12);
                    double N = (double)_lObj.LoansProperty[iLoanCnt].AmortTermValue * 12;                    

                    double pj = 0;
                    double ipmt = 0;

                    if (null != _lObj.LoansProperty[iLoanCnt].LoanTypeProperty && string.Empty != _lObj.LoansProperty[iLoanCnt].LoanTypeProperty)
                    {
                        if ("Fixed" == _lObj.LoansProperty[iLoanCnt].LoanTypeProperty)
                        {
                            double cpj = 0;
                            double cipmt = 0;

                            for (int j = 1; j <= years * 12; j++)
                            {
                                try { pj = Microsoft.VisualBasic.Financial.PPmt(i, j, N, P, 0, Microsoft.VisualBasic.DueDate.EndOfPeriod); }
                                catch { pj = 0; }

                                try { ipmt = Microsoft.VisualBasic.Financial.IPmt(i, j, N, P, 0, Microsoft.VisualBasic.DueDate.EndOfPeriod); }
                                catch { ipmt = 0; }

                                cpj += Math.Abs(pj);
                                cipmt += Math.Abs(ipmt);

                                if (0 == ((j /*+ 1*/) % 12))
                                {
                                    pjs[((j + 1) / 12) - 1] += cpj;   
                                    ijs[((j + 1) / 12) - 1] += cipmt; 

                                    cpj = 0;
                                    cipmt = 0;
                                }
                            }
                        }
                        else
                        {
                            for (int j = 1; j <= years * 12; j++)
                            {
                                if (0 == ((j + 1) % 12))
                                {
                                    pjs[((j + 1) / 12) - 1] += 0; //P;
                                    ijs[((j + 1) / 12) - 1] += P * i * 12;
                                }
                            }
                        }
                    }
                }
            }
        }
        #endregion

        #region PRINCIPAL PAYMENTS CUMULATIVE
        List<double> InitCumulativePrinciplePaymentsPerYears(int years, List<double> principalPaymentsPerYears)
        {
            List<double> vals = new List<double>();

            for (int i = 0; i < years; i++)
            {
                if (0 == i)
                {
                    vals.Add(principalPaymentsPerYears[0]);
                }
                else
                {
                    vals.Add(vals[i - 1] + principalPaymentsPerYears[i]);
                }
            }
            return vals;
        }
        #endregion

        #region INTEREST PAYMENTS CUMULATIVE
        List<double> InitCumulativeInterestPaymentsPerYears(int years, List<double> interestPaymentsPerYears)
        {
            List<double> vals = new List<double>();

            for (int i = 0; i < years; i++)
            {
                if (0 == i)
                {
                    vals.Add(interestPaymentsPerYears[0]);
                }
                else
                {
                    vals.Add(vals[i - 1] + interestPaymentsPerYears[i]);
                }
            }
            return vals;
        }
        #endregion

        #region CAPITAL IMPROVEMENTS
        List<double> InitCapitalImprovementsPerYears(int years, List<double> interestPaymentsPerYears)
        {
            List<double> vals = new List<double>();

            for (int i = 0; i < years; i++)
            {                
                vals.Add((double)Util.GetDecimalFromString(_expObj.CapitalAdditionsProperty.yearsProperty[i]._costProperty));
            }

            return vals;
        }
        #endregion

        #region CAPITAL IMPROVEMENTS CUMULATIVE
        List<double> InitCumulativeCapitalImprovementsPerYears(int years, List<double> capitalImprovementsPerYears)
        {
            List<double> vals = new List<double>();

            for (int i = 0; i < years; i++)
            {
                if (0 == i)
                {
                    vals.Add(capitalImprovementsPerYears[0]);
                }
                else
                {
                    vals.Add(vals[i - 1] + capitalImprovementsPerYears[i]);
                }
            }
            return vals;
        }
        #endregion

        #region CASH FLOW BEFORE TAXES
        List<double> InitCashFlowBeforeTaxesPerYears(int years, List<decimal> nois, List<decimal> debtServices, List<double> capitalImps)
        {
            List<double> vals = new List<double>();

            for (int i = 0; i < years; i++)
            {                
                vals.Add((double)(nois[i] - debtServices[i]) - capitalImps[i]);                
            }
            return vals;
        }
        #endregion

        #region CASH FLOW BEFORE TAXSES CUMULATIVE
        List<double> InitCumulativeCashFlowBeoreTaxesPerYears(int years, List<double> cashFlowBeforeTaxesPerYears)
        {
            List<double> vals = new List<double>();

            for (int i = 0; i < years; i++)
            {
                if (0 == i)
                {
                    vals.Add(cashFlowBeforeTaxesPerYears[0]);
                }
                else
                {
                    vals.Add(vals[i - 1] + cashFlowBeforeTaxesPerYears[i]);
                }
            }
            return vals;
        }
        #endregion

        #region CASH ON CASH RETURN
        List<double> InitCashOnCashReturnPerYears(int years, List<double> cumulativeCashFlowPerYears, decimal initialInvestment, List<double> cumulativeCapitalImprovements)
        {
            List<double> vals = new List<double>();

            for (int i = 0; i < years; i++)
            {
                vals.Add(cumulativeCashFlowPerYears[i] / ((double)initialInvestment + cumulativeCapitalImprovements[i]));
            }
            return vals;
        }
        #endregion

        #region ANNUAL DEPRECIATION
        List<double> InitDepreciableValuePerYears(int years, decimal depValue, List<double> cumulativeCapitalImprovements, decimal nrOfDepYears)
        {
            List<double> vals = new List<double>();

            for (int i = 0; i < years; i++)
            {
                vals.Add(((double)depValue + cumulativeCapitalImprovements[i]) / (double)nrOfDepYears);
            }
            return vals;
        }
        #endregion

        #region TAXABLE INCOME
        List<double> InitTaxableIncomePerYears(int years, List<decimal> nois, List<double> interestPayments, List<double> depreciations)
        {
            List<double> vals = new List<double>();

            for (int i = 0; i < years; i++)
            {
                vals.Add((double)nois[i] - interestPayments[i] - depreciations[i]);
            }
            return vals;
        }
        #endregion

        #region TOTAL TAX COST
        List<double> InitTotalTaxCostPerYears(int years, List<double> taxableIncome, double fedRate, double stateRate)
        {
            List<double> vals = new List<double>();

            for (int i = 0; i < years; i++)
            {
                vals.Add(taxableIncome[i] * ((fedRate / 100) + (stateRate / 100)));
            }
            return vals;
        }
        #endregion

        #region CASH FLOW AFTER TAXES
        List<double> InitCashFlowAfterTaxesPerYears(int years, List<double> cashFlowBeforeTaxes, List<double> totalTaxCosts)
        {
            List<double> vals = new List<double>();

            for (int i = 0; i < years; i++)
            {
                vals.Add(cashFlowBeforeTaxes[i] - totalTaxCosts[i]);
            }
            return vals;
        }
        #endregion

        #region CASH FLOW AFTER TAXES CUMMULATIVE
        List<double> InitCumulativeCashFlowAfterTaxesPerYears(int years, List<double> cashFlowAfterTaxesPerYears)
        {
            List<double> vals = new List<double>();

            for (int i = 0; i < years; i++)
            {
                if (0 == i)
                {
                    vals.Add(cashFlowAfterTaxesPerYears[0]);
                }
                else
                {
                    vals.Add(vals[i - 1] + cashFlowAfterTaxesPerYears[i]);
                }
            }
            return vals;
        }
        #endregion

        #region ROW LABELS
        string GetLabel(int iRow)
        {
            switch (iRow)
            {
                case 1:
                    return "Value";
                case 2:
                    return "Rental Income (GSI)";
                case 3:
                    return "Vacancy/Credit Loss";
                case 4:
                    return "Other Income";
                case 5:
                    return "Gross Operating Income";
                case 6:
                    return "Operating Expenses";
                case 7:
                    return "Net Operating Income";
                case 8:
                    return "Debt Service";
                case 9:
                    return "Debt Service (Cumulative)";
                case 10:
                    return "Principal Payments";
                case 11:
                    return "Principal Payments (Cumulative)";
                case 12:
                    return "Interests Payments";
                case 13:
                    return "Interests Payments (Cumul.)";
                case 14:
                    return "Capital Improvements";
                case 15:
                    return "Capital Improvements (Cumul.)";
                case 16:
                    return "Cash Flow Before Taxes";
                case 17:
                    return "Cash Flow Before Taxes (Cum.)";
                case 18:
                    return "Cash on Cash Return";
                case 19:
                    return "Annual Depreciation";
                case 20:
                    return "Taxable Income";
                case 21:
                    return "Total Tax Cost";
                case 22:
                    return "Cash Flow after taxes";
                case 23:
                    return "Cash Flow after taxes (Cumul.)";   
                
                default:
                    return string.Empty;
            }
        }
        #endregion
    }
}
