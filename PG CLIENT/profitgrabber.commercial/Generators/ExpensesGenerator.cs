﻿using System;
using System.Collections;
using System.Drawing;
using System.Drawing.Printing;

using ProfitGrabber.Commercial.Objects;

namespace ProfitGrabber.Commercial.Generators
{
    public class ExpensesGenerator : BaseCommercialsGenerator
    {
        ExpensesObject _obj = null;
        const string ReportName = "EXPENSES DETAIL SHEET";
        const string ReportNameSummary = "EXPENSES SUMMARY SHEET";
        
        public ExpensesGenerator()
        {
            
        }

        public void GenerateDetails(PrintPageEventArgs e, ExpensesObject obj, string address)
        {
            _obj = obj;
            _e = e;
            _gfx = e.Graphics;
            _paperWidth = e.PageSettings.PaperSize.Height;

            _gfx.DrawString(ReportName, base._arial22, base._blackBrush, base.GetOrigin(_gfx, ePosition.Center, ReportName, base._arial22, _paperWidth, 20));

            int currY = 60;
            //1st ROW
            _gfx.DrawString("Property Site Address:", _arial12, _blackBrush, new PointF(20, currY));
            _gfx.DrawString(address, _arial12, _blackBrush, GetOriginUponAlreadySetText(_gfx, ePosition.Right, "Property Site Address:", new Point(20, currY), _arial12));

            //2nd ROW
            currY += 25;
            _gfx.DrawString("EXPENSES TOTALS", _arial12, _blackBrush, new PointF(20, currY));

            //TABLE 
            //HEADER
            currY += 25;

            int startY = currY;
            _gfx.DrawLine(_blackPen, new Point(40, currY), new Point(1000, currY));

            //currY += 25;

            _gfx.DrawString("Type", _arial10, _blackBrush, new Point(40, currY));
            _gfx.DrawString("Description", _arial10, _blackBrush, new Point(140, currY));
            _gfx.DrawString("$/month", _arial10, _blackBrush, new Point(310, currY));
            _gfx.DrawString("$/year", _arial10, _blackBrush, new Point(390, currY));
            _gfx.DrawString("$/SqFt ann.", _arial10, _blackBrush, new Point(470, currY));
            _gfx.DrawString("$/Unit ann.", _arial10, _blackBrush, new Point(550, currY));
            _gfx.DrawString("% of Total", _arial10, _blackBrush, new Point(630, currY));
            _gfx.DrawString("Paid by", _arial10, _blackBrush, new Point(710, currY));
            _gfx.DrawString("High/Low", _arial10, _blackBrush, new Point(790, currY));
            _gfx.DrawString("Comment", _arial10, _blackBrush, new Point(870, currY));

            for (int i = _pageBreakIterator; i < ExpensesObject.MaxExpensesItems; i++)
            {                                
                string type = string.Empty;
                string desc = string.Empty;

                ExpensesObject.Instance.ExpensesItemsProperty.GetDescriptor(i, out type, out desc);

                currY += 20;

                string expensesPercOfTotal = string.Empty;
                decimal usdPerMonthValueTotal = 0;
                decimal usdPerYearValueTotal = 0;
                decimal usdPerSqFtValueTotal = 0;
                decimal usdPerUnitValueTotal = 0;
                ExpensesTypes currType = ExpensesTypes.Administrative;

                if (0 <= i && i <= 5)
                    currType = ExpensesTypes.Administrative;
                else if (6 <= i && i <= 8)
                    currType = ExpensesTypes.Management;
                else if (9 <= i && i <= 13)
                    currType = ExpensesTypes.Utilities;
                else if (14 <= i && i <= 19)
                    currType = ExpensesTypes.Insurance;
                else if (20 <= i && i <= 33)
                    currType = ExpensesTypes.Operating;

                _obj.ExpensesItemsProperty.GetExpensesTotal(currType, out usdPerMonthValueTotal, out usdPerYearValueTotal, out usdPerSqFtValueTotal, out usdPerUnitValueTotal);
                decimal totalYearExpensesForAllTypes = _obj.ExpensesItemsProperty.GetTotalYearExpensesForAllTypes();

                if (0 != usdPerYearValueTotal)
                    expensesPercOfTotal = ((_obj.ExpensesItemsProperty.Items[i].USDPerYearValue / totalYearExpensesForAllTypes) * 100).ToString("0.00");
                else
                    expensesPercOfTotal = "N/A";

                _gfx.DrawString(type, _arial10, _blackBrush, new Point(40, currY));
                _gfx.DrawString(desc, _arial10, _blackBrush, new Point(140, currY));
                _gfx.DrawString(_obj.ExpensesItemsProperty.Items[i].USDPerMonthValue.ToString(CurrFormatter), _arial10, _blackBrush, new Point(310, currY));
                _gfx.DrawString(_obj.ExpensesItemsProperty.Items[i].USDPerYearValue.ToString(CurrFormatter), _arial10, _blackBrush, new Point(390, currY));
                _gfx.DrawString(_obj.ExpensesItemsProperty.Items[i].USDPerSqFtValue.ToString(CurrFormatter), _arial10, _blackBrush, new Point(470, currY));
                _gfx.DrawString(_obj.ExpensesItemsProperty.Items[i].USDPerUnitValue.ToString(CurrFormatter), _arial10, _blackBrush, new Point(550, currY));
                _gfx.DrawString(expensesPercOfTotal, _arial10, _blackBrush, new Point(630, currY));
                _gfx.DrawString(_obj.ExpensesItemsProperty.Items[i].PaidByProperty, _arial10, _blackBrush, new Point(710, currY));
                _gfx.DrawString(_obj.ExpensesItemsProperty.Items[i].HighLowProperty, _arial10, _blackBrush, new Point(790, currY));
                _gfx.DrawString(_obj.ExpensesItemsProperty.Items[i].CommentProperty, _arial10, _blackBrush, new Point(870, currY));

                
                _gfx.DrawLine(_blackPen, new Point(40, currY), new Point(1000, currY));
            }

            currY += 20;

            //DRAW VERTICAL LINES
            _gfx.DrawLine(_blackPen, new Point(40, startY), new Point(40, currY));  //type
            _gfx.DrawLine(_blackPen, new Point(140, startY), new Point(140, currY));//description
            _gfx.DrawLine(_blackPen, new Point(310, startY), new Point(310, currY));//$/month
            _gfx.DrawLine(_blackPen, new Point(390, startY), new Point(390, currY));//$/year
            _gfx.DrawLine(_blackPen, new Point(470, startY), new Point(470, currY));//$/sqft
            _gfx.DrawLine(_blackPen, new Point(550, startY), new Point(550, currY));//$/unit
            _gfx.DrawLine(_blackPen, new Point(630, startY), new Point(630, currY));//% of Total                    
            _gfx.DrawLine(_blackPen, new Point(710, startY), new Point(710, currY));//paid by
            _gfx.DrawLine(_blackPen, new Point(790, startY), new Point(790, currY));//high / low                    
            _gfx.DrawLine(_blackPen, new Point(870, startY), new Point(870, currY));//comments
            _gfx.DrawLine(_blackPen, new Point(1000, startY), new Point(1000, currY));//
            _gfx.DrawLine(_blackPen, new Point(40, currY), new Point(1000, currY));

        }

        public void GenerateSummary(PrintPageEventArgs e, ExpensesObject obj, string address)
        {
            _obj = obj;
            _e = e;
            _gfx = e.Graphics;
            _paperWidth = e.PageSettings.PaperSize.Height;

            _gfx.DrawString(ReportNameSummary, base._arial22, base._blackBrush, base.GetOrigin(_gfx, ePosition.Center, ReportName, base._arial22, _paperWidth, 20));

            int currY = 60;
            //1st ROW
            _gfx.DrawString("Property Site Address:", _arial12, _blackBrush, new PointF(20, currY));
            _gfx.DrawString(address, _arial12, _blackBrush, GetOriginUponAlreadySetText(_gfx, ePosition.Right, "Property Site Address:", new Point(20, currY), _arial12));

            //2nd ROW
            currY += 25;
            _gfx.DrawString("UTILITY TYPES", _arial12, _blackBrush, new PointF(20, currY));

            //3rd ROW
            currY += 25;
            _gfx.DrawString("Electricity Meter: " + obj.UtilityTypesProperty.ElectricMeterProperty, _arial10, _blackBrush, new PointF(20, currY));
            _gfx.DrawString("Heat: " + obj.UtilityTypesProperty.HeatProperty, _arial10, _blackBrush, new PointF(250, currY));
            _gfx.DrawString("Hot Water: " + obj.UtilityTypesProperty.HotWaterProperty, _arial10, _blackBrush, new PointF(400, currY));

            //2nd ROW
            currY += 25;
            _gfx.DrawString("CAPITAL ADDITIONS (IMPROVEMENTS)", _arial12, _blackBrush, new PointF(20, currY));

            //TABLE
            currY += 25;
            int startY = currY;
            _gfx.DrawLine(_blackPen, new Point(40, currY), new Point(940, currY));

            _gfx.DrawString(string.Empty, _arial10, _blackBrush, new Point(40, currY));
            _gfx.DrawString("Year 1", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "Year 1", _arial10, new Rectangle(140, currY, 80, 20)));
            _gfx.DrawString("Year 2", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "Year 2", _arial10, new Rectangle(220, currY, 80, 20)));
            _gfx.DrawString("Year 3", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "Year 3", _arial10, new Rectangle(300, currY, 80, 20)));
            _gfx.DrawString("Year 4", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "Year 4", _arial10, new Rectangle(380, currY, 80, 20)));
            _gfx.DrawString("Year 5", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "Year 5", _arial10, new Rectangle(460, currY, 80, 20)));
            _gfx.DrawString("Year 6", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "Year 6", _arial10, new Rectangle(540, currY, 80, 20)));
            _gfx.DrawString("Year 7", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "Year 7", _arial10, new Rectangle(620, currY, 80, 20)));
            _gfx.DrawString("Year 8", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "Year 8", _arial10, new Rectangle(700, currY, 80, 20)));
            _gfx.DrawString("Year 9", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "Year 9", _arial10, new Rectangle(780, currY, 80, 20)));
            _gfx.DrawString("Year 10", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "Year 10", _arial10, new Rectangle(860, currY, 80, 20)));

            currY += 25;
            _gfx.DrawLine(_blackPen, new Point(40, currY), new Point(940, currY));
            _gfx.DrawString("Cost:", _arial10, _blackBrush, new Point(90, currY));

            _gfx.DrawString("$" + _obj.CapitalAdditionsProperty.yearsProperty[0]._costProperty, _arial10, _blackBrush, new Point(220, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
            _gfx.DrawString("$" + _obj.CapitalAdditionsProperty.yearsProperty[1]._costProperty, _arial10, _blackBrush, new Point(300, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
            _gfx.DrawString("$" + _obj.CapitalAdditionsProperty.yearsProperty[2]._costProperty, _arial10, _blackBrush, new Point(380, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
            _gfx.DrawString("$" + _obj.CapitalAdditionsProperty.yearsProperty[3]._costProperty, _arial10, _blackBrush, new Point(460, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
            _gfx.DrawString("$" + _obj.CapitalAdditionsProperty.yearsProperty[4]._costProperty, _arial10, _blackBrush, new Point(540, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
            _gfx.DrawString("$" + _obj.CapitalAdditionsProperty.yearsProperty[5]._costProperty, _arial10, _blackBrush, new Point(620, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
            _gfx.DrawString("$" + _obj.CapitalAdditionsProperty.yearsProperty[6]._costProperty, _arial10, _blackBrush, new Point(700, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
            _gfx.DrawString("$" + _obj.CapitalAdditionsProperty.yearsProperty[7]._costProperty, _arial10, _blackBrush, new Point(780, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
            _gfx.DrawString("$" + _obj.CapitalAdditionsProperty.yearsProperty[8]._costProperty, _arial10, _blackBrush, new Point(860, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
            _gfx.DrawString("$" + _obj.CapitalAdditionsProperty.yearsProperty[9]._costProperty, _arial10, _blackBrush, new Point(940, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));

            currY += 25;
            _gfx.DrawLine(_blackPen, new Point(40, currY), new Point(940, currY));
            _gfx.DrawString("Type:", _arial10, _blackBrush, new Point(90, currY));

            _gfx.DrawString(_obj.CapitalAdditionsProperty.yearsProperty[0]._typeProperty, _arial10, _blackBrush, new Point(220, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
            _gfx.DrawString(_obj.CapitalAdditionsProperty.yearsProperty[1]._typeProperty, _arial10, _blackBrush, new Point(300, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
            _gfx.DrawString(_obj.CapitalAdditionsProperty.yearsProperty[2]._typeProperty, _arial10, _blackBrush, new Point(380, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
            _gfx.DrawString(_obj.CapitalAdditionsProperty.yearsProperty[3]._typeProperty, _arial10, _blackBrush, new Point(460, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
            _gfx.DrawString(_obj.CapitalAdditionsProperty.yearsProperty[4]._typeProperty, _arial10, _blackBrush, new Point(540, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
            _gfx.DrawString(_obj.CapitalAdditionsProperty.yearsProperty[5]._typeProperty, _arial10, _blackBrush, new Point(620, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
            _gfx.DrawString(_obj.CapitalAdditionsProperty.yearsProperty[6]._typeProperty, _arial10, _blackBrush, new Point(700, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
            _gfx.DrawString(_obj.CapitalAdditionsProperty.yearsProperty[7]._typeProperty, _arial10, _blackBrush, new Point(780, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
            _gfx.DrawString(_obj.CapitalAdditionsProperty.yearsProperty[8]._typeProperty, _arial10, _blackBrush, new Point(860, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
            _gfx.DrawString(_obj.CapitalAdditionsProperty.yearsProperty[9]._typeProperty, _arial10, _blackBrush, new Point(940, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));

            currY += 25;
            _gfx.DrawLine(_blackPen, new Point(40, currY), new Point(940, currY));

            //DRAW VERTICAL LINES
            _gfx.DrawLine(_blackPen, new Point(40, startY), new Point(40, currY));
            _gfx.DrawLine(_blackPen, new Point(140, startY), new Point(140, currY));
            _gfx.DrawLine(_blackPen, new Point(220, startY), new Point(220, currY));
            _gfx.DrawLine(_blackPen, new Point(300, startY), new Point(300, currY));
            _gfx.DrawLine(_blackPen, new Point(380, startY), new Point(380, currY));
            _gfx.DrawLine(_blackPen, new Point(460, startY), new Point(460, currY));
            _gfx.DrawLine(_blackPen, new Point(540, startY), new Point(540, currY));
            _gfx.DrawLine(_blackPen, new Point(620, startY), new Point(620, currY));
            _gfx.DrawLine(_blackPen, new Point(700, startY), new Point(700, currY));
            _gfx.DrawLine(_blackPen, new Point(780, startY), new Point(780, currY));
            _gfx.DrawLine(_blackPen, new Point(860, startY), new Point(860, currY));
            _gfx.DrawLine(_blackPen, new Point(940, startY), new Point(940, currY));


            //2nd ROW
            currY += 25;
            _gfx.DrawString("CAPITAL ADDITIONS (IMPROVEMENTS)", _arial12, _blackBrush, new PointF(20, currY));


            decimal usdPerMonthValueTotal = 0;
            decimal usdPerYearValueTotal = 0;
            decimal usdPerSqFtValueTotal = 0;
            decimal usdPerUnitValueTotal = 0;

            //TABLE
            currY += 25;
            startY = currY;

            _gfx.DrawLine(_blackPen, new Point(40, currY), new Point(740, currY));
            _gfx.DrawString("Type:", _arial10, _blackBrush, new Point(40, currY));
            _gfx.DrawString("Description:", _arial10, _blackBrush, new Point(140, currY));
            _gfx.DrawString("$/month:", _arial10, _blackBrush, new Point(240, currY));
            _gfx.DrawString("$/year:", _arial10, _blackBrush, new Point(340, currY));
            _gfx.DrawString("$/SqFt (annual)", _arial10, _blackBrush, new Point(440, currY));
            _gfx.DrawString("$/Unit (annual):", _arial10, _blackBrush, new Point(540, currY));
            _gfx.DrawString("% of Total", _arial10, _blackBrush, new Point(640, currY));

            currY += 25;
            



            //ADMINISTRATIVE
            obj.ExpensesItemsProperty.GetExpensesTotal(ExpensesTypes.Administrative, out usdPerMonthValueTotal, out usdPerYearValueTotal, out usdPerSqFtValueTotal, out usdPerUnitValueTotal);
            decimal administrativeTotalUsdPerYearValue = usdPerYearValueTotal;

            _gfx.DrawLine(_blackPen, new Point(40, currY), new Point(740, currY));
            _gfx.DrawString("Administrative", _arial10, _blackBrush, new Point(40, currY));
            _gfx.DrawString("Total", _arial10, _blackBrush, new Point(140, currY));
            _gfx.DrawString("$" + usdPerMonthValueTotal.ToString(CurrFormatter), _arial10, _blackBrush, new Point(240, currY));
            _gfx.DrawString("$" + usdPerYearValueTotal.ToString(CurrFormatter), _arial10, _blackBrush, new Point(340, currY));
            _gfx.DrawString("$" + usdPerSqFtValueTotal.ToString(CurrFormatter), _arial10, _blackBrush, new Point(440, currY));
            _gfx.DrawString("$" + usdPerUnitValueTotal.ToString(CurrFormatter), _arial10, _blackBrush, new Point(540, currY));            
            int administrativeY = currY;

            currY += 25;
            //MANAGEMENT
            obj.ExpensesItemsProperty.GetExpensesTotal(ExpensesTypes.Management, out usdPerMonthValueTotal, out usdPerYearValueTotal, out usdPerSqFtValueTotal, out usdPerUnitValueTotal);
            decimal managementTotalUsdPerYearValue = usdPerYearValueTotal;
            _gfx.DrawLine(_blackPen, new Point(40, currY), new Point(740, currY));
            _gfx.DrawString("Management", _arial10, _blackBrush, new Point(40, currY));
            _gfx.DrawString("Total", _arial10, _blackBrush, new Point(140, currY));
            _gfx.DrawString("$" + usdPerMonthValueTotal.ToString(CurrFormatter), _arial10, _blackBrush, new Point(240, currY));
            _gfx.DrawString("$" + usdPerYearValueTotal.ToString(CurrFormatter), _arial10, _blackBrush, new Point(340, currY));
            _gfx.DrawString("$" + usdPerSqFtValueTotal.ToString(CurrFormatter), _arial10, _blackBrush, new Point(440, currY));
            _gfx.DrawString("$" + usdPerUnitValueTotal.ToString(CurrFormatter), _arial10, _blackBrush, new Point(540, currY));
            int managementY = currY;
            

            currY += 25;
            //UTILITIES
            obj.ExpensesItemsProperty.GetExpensesTotal(ExpensesTypes.Utilities, out usdPerMonthValueTotal, out usdPerYearValueTotal, out usdPerSqFtValueTotal, out usdPerUnitValueTotal);
            decimal utilitiesTotalUsdPerYearValue = usdPerYearValueTotal;
            _gfx.DrawLine(_blackPen, new Point(40, currY), new Point(740, currY));
            _gfx.DrawString("Utilities", _arial10, _blackBrush, new Point(40, currY));
            _gfx.DrawString("Total", _arial10, _blackBrush, new Point(140, currY));
            _gfx.DrawString("$" + usdPerMonthValueTotal.ToString(CurrFormatter), _arial10, _blackBrush, new Point(240, currY));
            _gfx.DrawString("$" + usdPerYearValueTotal.ToString(CurrFormatter), _arial10, _blackBrush, new Point(340, currY));
            _gfx.DrawString("$" + usdPerSqFtValueTotal.ToString(CurrFormatter), _arial10, _blackBrush, new Point(440, currY));
            _gfx.DrawString("$" + usdPerUnitValueTotal.ToString(CurrFormatter), _arial10, _blackBrush, new Point(540, currY));            
            int utilitiesY = currY;

            currY += 25;
            //INSURANCE
            obj.ExpensesItemsProperty.GetExpensesTotal(ExpensesTypes.Insurance, out usdPerMonthValueTotal, out usdPerYearValueTotal, out usdPerSqFtValueTotal, out usdPerUnitValueTotal);
            decimal insuranceTotalUsdPerYearValue = usdPerYearValueTotal;
            _gfx.DrawLine(_blackPen, new Point(40, currY), new Point(740, currY));
            _gfx.DrawString("Insurance", _arial10, _blackBrush, new Point(40, currY));
            _gfx.DrawString("Total", _arial10, _blackBrush, new Point(140, currY));
            _gfx.DrawString("$" + usdPerMonthValueTotal.ToString(CurrFormatter), _arial10, _blackBrush, new Point(240, currY));
            _gfx.DrawString("$" + usdPerYearValueTotal.ToString(CurrFormatter), _arial10, _blackBrush, new Point(340, currY));
            _gfx.DrawString("$" + usdPerSqFtValueTotal.ToString(CurrFormatter), _arial10, _blackBrush, new Point(440, currY));
            _gfx.DrawString("$" + usdPerUnitValueTotal.ToString(CurrFormatter), _arial10, _blackBrush, new Point(540, currY));            
            int insuranceY = currY;

            currY += 25;
            //OPERATING
            obj.ExpensesItemsProperty.GetExpensesTotal(ExpensesTypes.Operating, out usdPerMonthValueTotal, out usdPerYearValueTotal, out usdPerSqFtValueTotal, out usdPerUnitValueTotal);
            decimal operatingTotalUsdPerYearValue = usdPerYearValueTotal;
            _gfx.DrawLine(_blackPen, new Point(40, currY), new Point(740, currY));
            _gfx.DrawString("Operating", _arial10, _blackBrush, new Point(40, currY));
            _gfx.DrawString("Total", _arial10, _blackBrush, new Point(140, currY));
            _gfx.DrawString("$" + usdPerMonthValueTotal.ToString(CurrFormatter), _arial10, _blackBrush, new Point(240, currY));
            _gfx.DrawString("$" + usdPerYearValueTotal.ToString(CurrFormatter), _arial10, _blackBrush, new Point(340, currY));
            _gfx.DrawString("$" + usdPerSqFtValueTotal.ToString(CurrFormatter), _arial10, _blackBrush, new Point(440, currY));
            _gfx.DrawString("$" + usdPerUnitValueTotal.ToString(CurrFormatter), _arial10, _blackBrush, new Point(540, currY));            
            int operatingY = currY;

            currY += 25;
            //TYPES TOTALs
            obj.ExpensesItemsProperty.GetExpensesTypesTotal(out usdPerMonthValueTotal, out usdPerYearValueTotal, out usdPerSqFtValueTotal, out usdPerUnitValueTotal);
            _gfx.DrawLine(_blackPen, new Point(40, currY), new Point(740, currY));
            _gfx.DrawString("All Types", _arial10, _blackBrush, new Point(40, currY));
            _gfx.DrawString("Total", _arial10, _blackBrush, new Point(140, currY));
            _gfx.DrawString("$" + usdPerMonthValueTotal.ToString(CurrFormatter), _arial10, _blackBrush, new Point(240, currY));
            _gfx.DrawString("$" + usdPerYearValueTotal.ToString(CurrFormatter), _arial10, _blackBrush, new Point(340, currY));
            _gfx.DrawString("$" + usdPerSqFtValueTotal.ToString(CurrFormatter), _arial10, _blackBrush, new Point(440, currY));
            _gfx.DrawString("$" + usdPerUnitValueTotal.ToString(CurrFormatter), _arial10, _blackBrush, new Point(540, currY));            
            int typesTotalY = currY;

            //UPDATE PERC TOTALS PER TYPES
            if (0 != usdPerYearValueTotal)
            {                
                _gfx.DrawString((administrativeTotalUsdPerYearValue / usdPerYearValueTotal * 100).ToString(CurrFormatter), _arial10, _blackBrush, new Point(640, administrativeY));
                _gfx.DrawString((managementTotalUsdPerYearValue / usdPerYearValueTotal * 100).ToString(CurrFormatter), _arial10, _blackBrush, new Point(640, managementY));
                _gfx.DrawString((utilitiesTotalUsdPerYearValue / usdPerYearValueTotal * 100).ToString(CurrFormatter), _arial10, _blackBrush, new Point(640, utilitiesY));
                _gfx.DrawString((insuranceTotalUsdPerYearValue / usdPerYearValueTotal * 100).ToString(CurrFormatter), _arial10, _blackBrush, new Point(640, insuranceY));
                _gfx.DrawString((operatingTotalUsdPerYearValue / usdPerYearValueTotal * 100).ToString(CurrFormatter), _arial10, _blackBrush, new Point(640, operatingY));
            }


            currY += 25;
            _gfx.DrawLine(_blackPen, new Point(40, currY), new Point(740, currY));


            //DRAW VERTICAL LINES
            _gfx.DrawLine(_blackPen, new Point(40, startY), new Point(40, currY));
            _gfx.DrawLine(_blackPen, new Point(140, startY), new Point(140, currY));
            _gfx.DrawLine(_blackPen, new Point(240, startY), new Point(240, currY));
            _gfx.DrawLine(_blackPen, new Point(340, startY), new Point(340, currY));
            _gfx.DrawLine(_blackPen, new Point(440, startY), new Point(440, currY));
            _gfx.DrawLine(_blackPen, new Point(540, startY), new Point(540, currY));
            _gfx.DrawLine(_blackPen, new Point(640, startY), new Point(640, currY));
            _gfx.DrawLine(_blackPen, new Point(740, startY), new Point(740, currY));
            
            
        }
    }
}
