﻿using System;
using System.Collections;
using System.Drawing;
using System.Drawing.Printing;

using ProfitGrabber.Commercial.Objects;


namespace ProfitGrabber.Commercial.Generators
{
    public class IncomeGenerator : BaseCommercialsGenerator
    {        
        IncomeObject _obj = null;
        const string ReportName = "INCOME SUMMARY SHEET";
        const string RentRollReportName = "RENT ROLL SUMMARY SHEET";
        

        public IncomeGenerator()
        {
            
        }

        public void GenerateSummary(PrintPageEventArgs e, IncomeObject obj, string address)
        {
            _obj = obj;
            _e = e;
            _gfx = e.Graphics;
            _paperWidth = e.PageSettings.PaperSize.Height;

            _gfx.DrawString(ReportName, base._arial22, base._blackBrush, base.GetOrigin(_gfx, ePosition.Center, ReportName, base._arial22, _paperWidth, 20));

            int currY = 60;
            //1st ROW
            _gfx.DrawString("Property Site Address:", _arial12, _blackBrush, new PointF(20, currY));
            _gfx.DrawString(address, _arial12, _blackBrush, GetOriginUponAlreadySetText(_gfx, ePosition.Right, "Property Site Address:", new Point(20, currY), _arial12));

            //2nd ROW
            currY += 25;
            _gfx.DrawString("UNIT TYPE SUMMARY", _arial12, _blackBrush, new PointF(20, currY));

            //TABLE (max 20 rows + 2 header ROWs)
            //HEADER
            currY += 25;

            int startY = currY;
            _gfx.DrawLine(_blackPen, new Point(100, currY), new Point(1040, currY));
            _gfx.DrawString("Square Feet", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "Square Feet", _arial10, new Rectangle(440, currY, 160, 20)));
            _gfx.DrawString("Actual Rent", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "Actual Rent", _arial10, new Rectangle(600, currY, 220, 20)));
            _gfx.DrawString("Market Rent", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "Market Rent", _arial10, new Rectangle(820, currY, 220, 20)));
            currY += 20;

            //DRAW HEADER TEXT
            _gfx.DrawString("# Units", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "# Units", _arial10, new Rectangle(100, currY, 60, 20)));
            _gfx.DrawString("Brdms", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "Bdrms", _arial10, new Rectangle(160, currY, 60, 20)));
            _gfx.DrawString("Baths", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "Baths", _arial10, new Rectangle(220, currY, 60, 20)));            
            _gfx.DrawString("Floor Plan", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "Floor Plan", _arial10, new Rectangle(280, currY, 100, 20)));
            _gfx.DrawString("Occup.", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "Occup.", _arial10, new Rectangle(380, currY, 60, 20)));
            _gfx.DrawString("Low", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "Low", _arial10, new Rectangle(440, currY, 80, 20)));
            _gfx.DrawString("High", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "High", _arial10, new Rectangle(520, currY, 80, 20)));
            _gfx.DrawString("Low", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "Low", _arial10, new Rectangle(600, currY, 80, 20)));
            _gfx.DrawString("High", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "High", _arial10, new Rectangle(680, currY, 80, 20)));
            _gfx.DrawString("$/SqFt", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "$/SqFt", _arial10, new Rectangle(760, currY, 60, 20)));
            _gfx.DrawString("Low", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "Low", _arial10, new Rectangle(820, currY, 80, 20)));
            _gfx.DrawString("High", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "High", _arial10, new Rectangle(900, currY, 80, 20)));
            _gfx.DrawString("$/SqFt", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "$/SqFt", _arial10, new Rectangle(980, currY, 60, 20)));
            

            _gfx.DrawLine(_blackPen, new Point(100, currY), new Point(1040, currY));
            currY += 20;
            _gfx.DrawLine(_blackPen, new Point(100, currY), new Point(1040, currY));
            //currY += 20;


            for (int i = 0; i < _obj.UnitTypeSummaries.Length; i++)
            {
                if (_obj.UnitTypeSummaries[i].IsValid)
                {
                    _gfx.DrawString(_obj.UnitTypeSummaries[i].NrOfUnitsProperty, _arial10, _blackBrush, new Point(160, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                    _gfx.DrawString(_obj.UnitTypeSummaries[i].NrOfBedroomsProperty, _arial10, _blackBrush, new Point(220, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                    _gfx.DrawString(_obj.UnitTypeSummaries[i].NrOfBathsProperty, _arial10, _blackBrush, new Point(280, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                    _gfx.DrawString(_obj.UnitTypeSummaries[i].FloorPlanProperty, _arial10, _blackBrush, new Point(380, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                    _gfx.DrawString(_obj.UnitTypeSummaries[i].OccupancyProperty, _arial10, _blackBrush, new Point(440, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                    _gfx.DrawString(_obj.UnitTypeSummaries[i].SqFtLowPerItem.ToString(), _arial10, _blackBrush, new Point(520, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                    _gfx.DrawString(_obj.UnitTypeSummaries[i].SqFtHighPerItem.ToString(), _arial10, _blackBrush, new Point(600, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                    _gfx.DrawString(_obj.UnitTypeSummaries[i].AR_LowProperty, _arial10, _blackBrush, new Point(680, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                    _gfx.DrawString(_obj.UnitTypeSummaries[i].AR_HighProperty, _arial10, _blackBrush, new Point(760, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                    _gfx.DrawString(_obj.UnitTypeSummaries[i].ActualRentPerItem.ToString(CurrFormatter), _arial10, _blackBrush, new Point(820, currY) , new StringFormat(StringFormatFlags.DirectionRightToLeft));
                    _gfx.DrawString(_obj.UnitTypeSummaries[i].MR_LowProperty, _arial10, _blackBrush, new Point(900, currY) , new StringFormat(StringFormatFlags.DirectionRightToLeft));
                    _gfx.DrawString(_obj.UnitTypeSummaries[i].MR_HighProperty, _arial10, _blackBrush, new Point(980, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                    _gfx.DrawString(_obj.UnitTypeSummaries[i].MarketRentPerItem.ToString(CurrFormatter), _arial10, _blackBrush, new Point(1040, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));                    

                    _gfx.DrawLine(_blackPen, new Point(100, currY), new Point(1040, currY));
                    currY += 20;
                }
            }

            _gfx.DrawLine(_blackPen, new Point(100, currY), new Point(1040, currY));

            //set totals            
            _gfx.DrawString("Total", _arial10, _blackBrush, new Point(100, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
            _gfx.DrawString(_obj.UTS_TotalNumberOfUnits.ToString(), _arial10, _blackBrush, new Point(160, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));

            _gfx.DrawString(_obj.UTS_WeightedAverageTotalOccupancy.ToString("0.00") + "%", _arial10, _blackBrush, new Point(440, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
            _gfx.DrawString(_obj.UTS_WeightedAverageTotalSqFtLow.ToString(CurrFormatter), _arial10, _blackBrush, new Point(520, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
            _gfx.DrawString(_obj.UTS_WeightedAverageTotalSqFtHigh.ToString(CurrFormatter), _arial10, _blackBrush, new Point(600, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
            _gfx.DrawString(_obj.UTS_SumTotalARLow.ToString(CurrFormatter), _arial10, _blackBrush, new Point(680, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
            _gfx.DrawString(_obj.UTS_SumTotalARHigh.ToString(CurrFormatter), _arial10, _blackBrush, new Point(760, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
            _gfx.DrawString(_obj.UTS_WeightedAverageARUSDPerSqFt.ToString(CurrFormatter), _arial10, _blackBrush, new Point(820, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
            _gfx.DrawString(_obj.UTS_SumTotalMRLow.ToString(CurrFormatter), _arial10, _blackBrush, new Point(900, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
            _gfx.DrawString(_obj.UTS_SumTotalMRHigh.ToString(CurrFormatter), _arial10, _blackBrush, new Point(980, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
            _gfx.DrawString(_obj.UTS_WeightedAverageMRUSDPerSqFt.ToString(CurrFormatter), _arial10, _blackBrush, new Point(1040, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));


            currY += 20;
            _gfx.DrawLine(_blackPen, new Point(100, currY), new Point(1040, currY));

            //DRAW VERTICAL LINES            
            _gfx.DrawLine(_blackPen, new Point(100, startY), new Point(100, currY));//units
            _gfx.DrawLine(_blackPen, new Point(160, startY), new Point(160, currY));//bdms
            _gfx.DrawLine(_blackPen, new Point(220, startY), new Point(220, currY));//baths            
            _gfx.DrawLine(_blackPen, new Point(280, startY), new Point(280, currY));//f.p.
            _gfx.DrawLine(_blackPen, new Point(380, startY), new Point(380, currY));//occupancy
            _gfx.DrawLine(_blackPen, new Point(440, startY), new Point(440, currY));//low
            _gfx.DrawLine(_blackPen, new Point(520, startY + 20), new Point(520, currY));//high
            _gfx.DrawLine(_blackPen, new Point(600, startY), new Point(600, currY));//low
            _gfx.DrawLine(_blackPen, new Point(680, startY + 20), new Point(680, currY));//high
            _gfx.DrawLine(_blackPen, new Point(760, startY + 20), new Point(760, currY));//$/sqft
            _gfx.DrawLine(_blackPen, new Point(820, startY), new Point(820, currY));//low
            _gfx.DrawLine(_blackPen, new Point(900, startY + 20), new Point(900, currY));//high
            _gfx.DrawLine(_blackPen, new Point(980, startY + 20), new Point(980, currY));//$/sqft
            _gfx.DrawLine(_blackPen, new Point(1040, startY), new Point(1040, currY));//


            //2nd ROW
            currY += 40;
            _gfx.DrawString("OTHER INCOME (annual):", _arial10, _blackBrush, new PointF(20, currY));
            _gfx.DrawString("ACTUAL INCOME (annual):", _arial10, _blackBrush, new PointF(320, currY));
            _gfx.DrawString("PROFORMA INCOME (annual):", _arial10, _blackBrush, new PointF(650, currY));
            
            currY += 25;
            _gfx.DrawString("Parking:", _arial10, _blackBrush, new PointF(20, currY));
            _gfx.DrawString("Gross Rent (GSI):", _arial10, _blackBrush, new PointF(320, currY));
            _gfx.DrawString("Gross Rent (GSI):", _arial10, _blackBrush, new PointF(650, currY));

            _gfx.DrawString("$ " + _obj.OtherIncomeObject.ParkingProperty, _arial10, _blackBrush, new PointF(120, currY));
            _gfx.DrawString("$ " + _obj.ActualIncomeObject.GrossRent, _arial10, _blackBrush, new PointF(520, currY));
            _gfx.DrawString("$ " + _obj.ProformaIncomeObject.GrossRentProperty, _arial10, _blackBrush, new PointF(850, currY));

            currY += 25;
            _gfx.DrawString("Laundry:", _arial10, _blackBrush, new PointF(20, currY));
            _gfx.DrawString("Vacancy/Credit Loss (%):", _arial10, _blackBrush, new PointF(320, currY));
            _gfx.DrawString("Vacancy/Credit Loss (%):", _arial10, _blackBrush, new PointF(650, currY));

            _gfx.DrawString("$ " + _obj.OtherIncomeObject.LaundryProperty, _arial10, _blackBrush, new PointF(120, currY));
            _gfx.DrawString(_obj.ActualIncomeObject.VacancyOrCreditLossPerc + " %", _arial10, _blackBrush, new PointF(520, currY));
            _gfx.DrawString( _obj.ProformaIncomeObject.VacancyOrCreditLossPercProperty + " %", _arial10, _blackBrush, new PointF(850, currY));

            currY += 25;
            _gfx.DrawString("Vending:", _arial10, _blackBrush, new PointF(20, currY));
            _gfx.DrawString("Vacancy/Credit Loss ($):", _arial10, _blackBrush, new PointF(320, currY));
            _gfx.DrawString("Vacancy/Credit Loss ($):", _arial10, _blackBrush, new PointF(650, currY));

            _gfx.DrawString("$ " + _obj.OtherIncomeObject.VendingProperty, _arial10, _blackBrush, new PointF(120, currY));
            _gfx.DrawString("$ " + _obj.ActualIncomeObject.VacancyOrCreditLossProperty.ToString(CurrFormatter), _arial10, _blackBrush, new PointF(520, currY));
            _gfx.DrawString("$ " + _obj.ProformaIncomeObject.VacancyOrCreditloss.ToString(CurrFormatter), _arial10, _blackBrush, new PointF(850, currY));

            currY += 25;
            _gfx.DrawString("Other:", _arial10, _blackBrush, new PointF(20, currY));
            _gfx.DrawString("Other Income:", _arial10, _blackBrush, new PointF(320, currY));
            _gfx.DrawString("Other Income:", _arial10, _blackBrush, new PointF(650, currY));

            _gfx.DrawString("$ " + _obj.OtherIncomeObject.OtherProperty, _arial10, _blackBrush, new PointF(120, currY));
            _gfx.DrawString("$ " + _obj.ActualIncomeObject.OtherIncomeProperty.ToString(CurrFormatter), _arial10, _blackBrush, new PointF(520, currY));
            _gfx.DrawString("$ " + _obj.ProformaIncomeObject.OtherIncome.ToString(CurrFormatter), _arial10, _blackBrush, new PointF(850, currY));

            currY += 25;
            _gfx.DrawString("Total:", _arial10, _blackBrush, new PointF(20, currY));
            _gfx.DrawString("Operating Income (GOI):", _arial10, _blackBrush, new PointF(320, currY));
            _gfx.DrawString("Operating Income (GOI):", _arial10, _blackBrush, new PointF(650, currY));

            _gfx.DrawString("$ " + _obj.OtherIncomeObject.TotalProperty.ToString(CurrFormatter), _arial10, _blackBrush, new PointF(120, currY));
            _gfx.DrawString("$ " + _obj.ActualIncomeObject.OperatingIncomeProperty.ToString(CurrFormatter), _arial10, _blackBrush, new PointF(520, currY));
            _gfx.DrawString("$ " + _obj.ProformaIncomeObject.OperatingIncome.ToString(CurrFormatter), _arial10, _blackBrush, new PointF(850, currY));
           
            
        }

        public void GenerateRentRoll(PrintPageEventArgs e, IncomeObject obj, string address)
        {
            _obj = obj;
            _e = e;
            _gfx = e.Graphics;
            _paperWidth = e.PageSettings.PaperSize.Height;
            int startY = 0;

            _gfx.DrawString(RentRollReportName, base._arial22, base._blackBrush, base.GetOrigin(_gfx, ePosition.Center, RentRollReportName, base._arial22, _paperWidth, 20));

            int currY = 60;
            if (0 == _pageCnt)
            {                
                //1st ROW
                _gfx.DrawString("Property Site Address:", _arial12, _blackBrush, new PointF(20, currY));
                _gfx.DrawString(address, _arial12, _blackBrush, GetOriginUponAlreadySetText(_gfx, ePosition.Right, "Property Site Address:", new Point(20, currY), _arial12));

                //TABLE (max 20 rows + 2 header ROWs)
                //HEADER

                currY += 25;

                startY = currY;
                _gfx.DrawLine(_blackPen, new Point(40, currY), new Point(1000, currY));

                //DRAW HEADER TEXT
                _gfx.DrawString("Unit#", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "Unit#", _arial10, new Rectangle(40, currY + 5, 60, 20)));
                _gfx.DrawString("Tenant", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "Tenant", _arial10, new Rectangle(100, currY + 5, 200, 20)));
                _gfx.DrawString("Bed/Bath", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "Bed/Bath", _arial10, new Rectangle(300, currY + 5, 60, 20)));
                _gfx.DrawString("SqFt.", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "SqFt.", _arial10, new Rectangle(360, currY + 5, 60, 20)));
                _gfx.DrawString("Sec.", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "Sec.", _arial10, new Rectangle(420, currY + 5, 60, 20)));
                _gfx.DrawString("Tenant", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "Tenant", _arial10, new Rectangle(480, currY + 5, 60, 20)));
                _gfx.DrawString("Sec 8", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "Sec 8", _arial10, new Rectangle(540, currY + 5, 60, 20)));
                _gfx.DrawString("Total", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "Total", _arial10, new Rectangle(600, currY + 5, 60, 20)));
                _gfx.DrawString("Market", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "Occup.", _arial10, new Rectangle(660, currY + 5, 60, 20)));
                _gfx.DrawString("Occup.", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "$/SqFt", _arial10, new Rectangle(720, currY + 5, 60, 20)));
                _gfx.DrawString("Lease", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "Lease", _arial10, new Rectangle(780, currY + 5, 60, 20)));
                _gfx.DrawString("Comment", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "Comment", _arial10, new Rectangle(840, currY + 5, 160, 20)));


                currY += 25;
                _gfx.DrawString("Name", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "Tenant", _arial10, new Rectangle(100, currY - 5, 200, 20)));
                _gfx.DrawString("Dep.", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "Dep.", _arial10, new Rectangle(420, currY - 5, 60, 20)));
                _gfx.DrawString("Rent", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "Rent", _arial10, new Rectangle(480, currY - 5, 60, 20)));
                _gfx.DrawString("Rent", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "Rent", _arial10, new Rectangle(540, currY - 5, 60, 20)));
                _gfx.DrawString("Rent", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "Rent", _arial10, new Rectangle(600, currY - 5, 60, 20)));
                _gfx.DrawString("Rent", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "Rent", _arial10, new Rectangle(660, currY - 5, 60, 20)));
                _gfx.DrawString("Date", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "Date", _arial10, new Rectangle(720, currY - 5, 60, 20)));
                _gfx.DrawString("Term", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "Term", _arial10, new Rectangle(780, currY - 5, 60, 20)));

                
                currY += 20;
            }

            if (0 < _pageCnt)
                startY = currY;

            _gfx.DrawLine(_blackPen, new Point(40, currY), new Point(1000, currY));

            for (int i = _pageBreakIterator; i < _obj.RentRolls.Length; i++)
            {
                if (currY > e.PageSettings.PaperSize.Width - 40)
                {
                    _pageBreakIterator = i;
                    _pageCnt++;
                    e.HasMorePages = true;
                    return;
                }
                else
                {
                    if (_obj.RentRolls[i].IsValid)
                    {
                        _gfx.DrawString(_obj.RentRolls[i].UnitNrProperty, _arial10, _blackBrush, new Point(40, currY));
                        _gfx.DrawString(_obj.RentRolls[i].TennantNameProperty, _arial10, _blackBrush, new Point(100, currY));
                        _gfx.DrawString(_obj.RentRolls[i].BedBathProperty, _arial10, _blackBrush, new Point(360, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                        _gfx.DrawString(_obj.RentRolls[i].SqftProperty, _arial10, _blackBrush, new Point(420, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                        _gfx.DrawString(_obj.RentRolls[i].SecDepPerItem.ToString(CurrFormatter), _arial10, _blackBrush, new Point(480, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                        _gfx.DrawString(_obj.RentRolls[i].TennantRentPerItem.ToString(CurrFormatter), _arial10, _blackBrush, new Point(540, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                        _gfx.DrawString(_obj.RentRolls[i].Sec8PerItem.ToString(CurrFormatter), _arial10, _blackBrush, new Point(600, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                        _gfx.DrawString(_obj.RentRolls[i].TotalRentProperty.ToString(CurrFormatter), _arial10, _blackBrush, new Point(660, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                        _gfx.DrawString(_obj.RentRolls[i].MarketRentPerItem.ToString(CurrFormatter), _arial10, _blackBrush, new Point(720, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                        _gfx.DrawString(_obj.RentRolls[i].OccupDateProperty, _arial10, _blackBrush, new Point(780, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                        _gfx.DrawString(_obj.RentRolls[i].LeaseTermProperty, _arial10, _blackBrush, new Point(840, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                        _gfx.DrawString(_obj.RentRolls[i].CommentProperty, _arial10, _blackBrush, new Point(1000, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));

                        currY += 20;
                        //DRAW VERTICAL LINES
                        _gfx.DrawLine(_blackPen, new Point(40, startY), new Point(40, currY));  //unit NR
                        _gfx.DrawLine(_blackPen, new Point(100, startY), new Point(100, currY));//tennant name
                        _gfx.DrawLine(_blackPen, new Point(300, startY), new Point(300, currY));//bed/bath
                        _gfx.DrawLine(_blackPen, new Point(360, startY), new Point(360, currY));//sq ft         
                        _gfx.DrawLine(_blackPen, new Point(420, startY), new Point(420, currY));//sec dept
                        _gfx.DrawLine(_blackPen, new Point(480, startY), new Point(480, currY));//tennant rent
                        _gfx.DrawLine(_blackPen, new Point(540, startY), new Point(540, currY));//sec 8 rent
                        _gfx.DrawLine(_blackPen, new Point(600, startY), new Point(600, currY));//total rent
                        _gfx.DrawLine(_blackPen, new Point(660, startY), new Point(660, currY));//market rent
                        _gfx.DrawLine(_blackPen, new Point(720, startY), new Point(720, currY));//occup date
                        _gfx.DrawLine(_blackPen, new Point(780, startY), new Point(780, currY));//lease term
                        _gfx.DrawLine(_blackPen, new Point(840, startY), new Point(840, currY));//comment                        
                        _gfx.DrawLine(_blackPen, new Point(1000, startY), new Point(1000, currY));//


                        _gfx.DrawLine(_blackPen, new Point(40, currY), new Point(1000, currY));

                        startY = currY;                        
                    }
                }
            }

            _gfx.DrawLine(_blackPen, new Point(40, currY), new Point(1000, currY));

            if (currY > e.PageSettings.PaperSize.Width - 40)
            {                
                _pageCnt++;
                e.HasMorePages = true;
                return;
            }

            startY = currY;
            _gfx.DrawLine(_blackPen, new Point(40, currY), new Point(1000, currY));

            _gfx.DrawString("Total", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "Total", _arial10, new Rectangle(300, currY, 60, 20)));
            _gfx.DrawString(_obj.RR_TotalSqft.ToString(CurrFormatter), _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, _obj.RR_TotalSqft.ToString(CurrFormatter), _arial10, new Rectangle(360, currY, 60, 20)));
            _gfx.DrawString(_obj.RR_TotalSecDep.ToString(CurrFormatter), _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, _obj.RR_TotalSec8Rent.ToString(CurrFormatter), _arial10, new Rectangle(420, currY, 60, 20)));
            _gfx.DrawString(_obj.RR_TotalTennantRent.ToString(CurrFormatter), _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, _obj.RR_TotalTennantRent.ToString(CurrFormatter), _arial10, new Rectangle(480, currY, 60, 20)));
            _gfx.DrawString(_obj.RR_TotalSec8Rent.ToString(CurrFormatter), _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, _obj.RR_TotalSec8Rent.ToString(CurrFormatter), _arial10, new Rectangle(540, currY, 60, 20)));
            _gfx.DrawString(_obj.RR_TotalTotalRent.ToString(CurrFormatter), _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, _obj.RR_TotalTotalRent.ToString(CurrFormatter), _arial10, new Rectangle(600, currY, 60, 20)));
            _gfx.DrawString(_obj.RR_TotalMarketRent.ToString(CurrFormatter), _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, _obj.RR_TotalMarketRent.ToString(CurrFormatter), _arial10, new Rectangle(660, currY, 60, 20)));
            
            currY += 20;
            _gfx.DrawLine(_blackPen, new Point(40, currY), new Point(1000, currY));
                        
            _gfx.DrawLine(_blackPen, new Point(40, startY), new Point(40, currY));  //unit NR            
            _gfx.DrawLine(_blackPen, new Point(360, startY), new Point(360, currY));//sq ft         
            _gfx.DrawLine(_blackPen, new Point(420, startY), new Point(420, currY));//sec dept
            _gfx.DrawLine(_blackPen, new Point(480, startY), new Point(480, currY));//tennant rent
            _gfx.DrawLine(_blackPen, new Point(540, startY), new Point(540, currY));//sec 8 rent
            _gfx.DrawLine(_blackPen, new Point(600, startY), new Point(600, currY));//total rent
            _gfx.DrawLine(_blackPen, new Point(660, startY), new Point(660, currY));//market rent
            _gfx.DrawLine(_blackPen, new Point(720, startY), new Point(720, currY));//occup date            
            _gfx.DrawLine(_blackPen, new Point(1000, startY), new Point(1000, currY));//            
        }

        public void GenerateRetailOrOfficeSummary(PrintPageEventArgs e, IncomeObject obj, string address)
        {
            _obj = obj;
            _e = e;
            _gfx = e.Graphics;
            _paperWidth = e.PageSettings.PaperSize.Height;

            _gfx.DrawString(ReportName, base._arial22, base._blackBrush, base.GetOrigin(_gfx, ePosition.Center, ReportName, base._arial22, _paperWidth, 20));

            int currY = 60;
            //1st ROW
            _gfx.DrawString("Property Site Address:", _arial12, _blackBrush, new PointF(20, currY));
            _gfx.DrawString(address, _arial12, _blackBrush, GetOriginUponAlreadySetText(_gfx, ePosition.Right, "Property Site Address:", new Point(20, currY), _arial12));

            //2nd ROW
            currY += 25;
            _gfx.DrawString("UNIT TYPE SUMMARY", _arial12, _blackBrush, new PointF(20, currY));

            //TABLE (max 20 rows + 2 header ROWs)
            //HEADER
            currY += 25;

            int startY = currY;
            _gfx.DrawLine(_blackPen, new Point(100, currY), new Point(1080, currY));            
            _gfx.DrawString("Actual Rate", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "Actual Rate", _arial10, new Rectangle(440, currY, 320, 20)));
            _gfx.DrawString("Proforma Rate", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "Proforma Rate", _arial10, new Rectangle(760, currY, 320, 20)));
            currY += 20;

            //DRAW HEADER TEXT
            _gfx.DrawString("Space", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "Space", _arial10, new Rectangle(100, currY, 60, 20)));
            _gfx.DrawString("Occup.", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "Occup.", _arial10, new Rectangle(160, currY, 60, 20)));
            _gfx.DrawString("Tenant", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "Tenant", _arial10, new Rectangle(220, currY, 60, 20)));
            _gfx.DrawString("Floor", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "Floor", _arial10, new Rectangle(280, currY, 60, 20)));
            _gfx.DrawString("SqFt.", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "SqFt.", _arial10, new Rectangle(380, currY, 60, 20)));
            _gfx.DrawString("$/Month", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "$/Month", _arial10, new Rectangle(440, currY, 80, 20)));
            _gfx.DrawString("$/Year", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "$/Year", _arial10, new Rectangle(520, currY, 80, 20)));
            _gfx.DrawString("$/SqFt/Mth", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "$/SqFt/Mth", _arial10, new Rectangle(600, currY, 80, 20)));
            _gfx.DrawString("$/SqFt/Yr", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "$/SqFt/Yr", _arial10, new Rectangle(680, currY, 80, 20)));
            _gfx.DrawString("$/Month", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "$/Month", _arial10, new Rectangle(760, currY, 80, 20)));
            _gfx.DrawString("$/Year", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "$/Year", _arial10, new Rectangle(840, currY, 80, 20)));
            _gfx.DrawString("$/SqFt/Mth", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "$/SqFt/Mth", _arial10, new Rectangle(920, currY, 80, 20)));
            _gfx.DrawString("$/SqFt/Yr", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "$/SqFt/Yr", _arial10, new Rectangle(1000, currY, 80, 20)));


            _gfx.DrawLine(_blackPen, new Point(100, currY), new Point(1080, currY));
            currY += 20;
            _gfx.DrawLine(_blackPen, new Point(100, currY), new Point(1080, currY));
            //currY += 20;


            for (int i = 0; i < _obj.RetailOfficeSummaryObject.Length; i++)
            {
                if (_obj.RetailOfficeSummaryObject[i].IsValid)
                {
                    _gfx.DrawString(_obj.RetailOfficeSummaryObject[i].Space, _arial10, _blackBrush, new Point(160, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                    _gfx.DrawString(_obj.RetailOfficeSummaryObject[i].Occupied, _arial10, _blackBrush, new Point(220, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                    _gfx.DrawString(_obj.RetailOfficeSummaryObject[i].Tennant, _arial10, _blackBrush, new Point(280, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                    _gfx.DrawString(_obj.RetailOfficeSummaryObject[i].Floor, _arial10, _blackBrush, new Point(380, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                    _gfx.DrawString(_obj.RetailOfficeSummaryObject[i].SqFt, _arial10, _blackBrush, new Point(440, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                    _gfx.DrawString(_obj.RetailOfficeSummaryObject[i].AR_usdPerMonth.ToString(), _arial10, _blackBrush, new Point(520, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                    _gfx.DrawString(_obj.RetailOfficeSummaryObject[i].AR_usdPerYear.ToString(), _arial10, _blackBrush, new Point(600, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                    _gfx.DrawString(_obj.RetailOfficeSummaryObject[i].AR_usdPerSqFtPerMonth, _arial10, _blackBrush, new Point(680, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                    _gfx.DrawString(_obj.RetailOfficeSummaryObject[i].AR_usdPerSqFtPerYear, _arial10, _blackBrush, new Point(760, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                    _gfx.DrawString(_obj.RetailOfficeSummaryObject[i].PR_usdPerMonth, _arial10, _blackBrush, new Point(840, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                    _gfx.DrawString(_obj.RetailOfficeSummaryObject[i].PR_usdPerYear, _arial10, _blackBrush, new Point(920, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                    _gfx.DrawString(_obj.RetailOfficeSummaryObject[i].PR_usdPerSqFtPerMonth, _arial10, _blackBrush, new Point(1000, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                    _gfx.DrawString(_obj.RetailOfficeSummaryObject[i].PR_usdPerSqFtPerYear, _arial10, _blackBrush, new Point(1080, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));

                    _gfx.DrawLine(_blackPen, new Point(100, currY), new Point(1080, currY));
                    currY += 20;
                }
            }

            _gfx.DrawLine(_blackPen, new Point(100, currY), new Point(1080, currY));

            //set totals            
            _gfx.DrawString("Total", _arial10, _blackBrush, new Point(100, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
            _gfx.DrawString(_obj.GetTotalOccupied().ToString(CurrFormatter), _arial10, _blackBrush, new Point(160, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));

            _gfx.DrawString(_obj.GetTotalSqFt().ToString(CurrFormatter), _arial10, _blackBrush, new Point(440, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
            _gfx.DrawString(_obj.GetTotal_AR_UsdPerMonth().ToString(CurrFormatter), _arial10, _blackBrush, new Point(520, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
            _gfx.DrawString(_obj.GetTotal_AR_UsdPerYear().ToString(CurrFormatter), _arial10, _blackBrush, new Point(600, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
            _gfx.DrawString(_obj.GetTotal_AR_UsdPerMonthPerSqFt().ToString(CurrFormatter), _arial10, _blackBrush, new Point(680, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
            _gfx.DrawString(_obj.GetTotal_AR_UsdPerYearPerSqFt().ToString(CurrFormatter), _arial10, _blackBrush, new Point(760, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
            _gfx.DrawString(_obj.GetTotal_PR_UsdPerMonth().ToString(CurrFormatter), _arial10, _blackBrush, new Point(840, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
            _gfx.DrawString(_obj.GetTotal_PR_UsdPerYear().ToString(CurrFormatter), _arial10, _blackBrush, new Point(920, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
            _gfx.DrawString(_obj.GetTotal_PR_UsdPerMonthPerSqFt().ToString(CurrFormatter), _arial10, _blackBrush, new Point(1000, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
            _gfx.DrawString(_obj.GetTotal_PR_UsdPerYearPerSqFt().ToString(CurrFormatter), _arial10, _blackBrush, new Point(1080, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));


            currY += 20;
            _gfx.DrawLine(_blackPen, new Point(100, currY), new Point(1080, currY));

            //DRAW VERTICAL LINES            
            _gfx.DrawLine(_blackPen, new Point(100, startY), new Point(100, currY));
            _gfx.DrawLine(_blackPen, new Point(160, startY), new Point(160, currY));
            _gfx.DrawLine(_blackPen, new Point(220, startY), new Point(220, currY));            
            _gfx.DrawLine(_blackPen, new Point(280, startY), new Point(280, currY));
            _gfx.DrawLine(_blackPen, new Point(380, startY), new Point(380, currY));
            _gfx.DrawLine(_blackPen, new Point(440, startY), new Point(440, currY));
            _gfx.DrawLine(_blackPen, new Point(520, startY + 20), new Point(520, currY));
            _gfx.DrawLine(_blackPen, new Point(600, startY + 20), new Point(600, currY));
            _gfx.DrawLine(_blackPen, new Point(680, startY + 20), new Point(680, currY));
            _gfx.DrawLine(_blackPen, new Point(760, startY ), new Point(760, currY));
            _gfx.DrawLine(_blackPen, new Point(840, startY + 20), new Point(840, currY));
            _gfx.DrawLine(_blackPen, new Point(920, startY + 20), new Point(920, currY));
            _gfx.DrawLine(_blackPen, new Point(1000, startY + 20), new Point(1000, currY));
            _gfx.DrawLine(_blackPen, new Point(1080, startY), new Point(1080, currY));


            //2nd ROW
            currY += 40;
            
            _gfx.DrawString("ACTUAL INCOME (annual):", _arial10, _blackBrush, new PointF(320, currY));
            _gfx.DrawString("PROFORMA INCOME (annual):", _arial10, _blackBrush, new PointF(650, currY));

            currY += 25;
            
            _gfx.DrawString("Gross Rent (GSI):", _arial10, _blackBrush, new PointF(320, currY));
            _gfx.DrawString("Gross Rent (GSI):", _arial10, _blackBrush, new PointF(650, currY));

            
            _gfx.DrawString("$ " + _obj.ActualIncomeObject.GrossRent, _arial10, _blackBrush, new PointF(520, currY));
            _gfx.DrawString("$ " + _obj.ProformaIncomeObject.GrossRentProperty, _arial10, _blackBrush, new PointF(850, currY));

            currY += 25;
            
            _gfx.DrawString("Vacancy/Credit Loss (%):", _arial10, _blackBrush, new PointF(320, currY));
            _gfx.DrawString("Vacancy/Credit Loss (%):", _arial10, _blackBrush, new PointF(650, currY));

            
            _gfx.DrawString(_obj.ActualIncomeObject.VacancyOrCreditLossPerc + " %", _arial10, _blackBrush, new PointF(520, currY));
            _gfx.DrawString(_obj.ProformaIncomeObject.VacancyOrCreditLossPercProperty + " %", _arial10, _blackBrush, new PointF(850, currY));

            currY += 25;
            
            _gfx.DrawString("Vacancy/Credit Loss ($):", _arial10, _blackBrush, new PointF(320, currY));
            _gfx.DrawString("Vacancy/Credit Loss ($):", _arial10, _blackBrush, new PointF(650, currY));

            
            _gfx.DrawString("$ " + _obj.ActualIncomeObject.VacancyOrCreditLossProperty.ToString(CurrFormatter), _arial10, _blackBrush, new PointF(520, currY));
            _gfx.DrawString("$ " + _obj.ProformaIncomeObject.VacancyOrCreditloss.ToString(CurrFormatter), _arial10, _blackBrush, new PointF(850, currY));

            currY += 25;
            
            _gfx.DrawString("Other Income:", _arial10, _blackBrush, new PointF(320, currY));
            _gfx.DrawString("Other Income:", _arial10, _blackBrush, new PointF(650, currY));

            
            _gfx.DrawString("$ " + _obj.ActualIncomeObject.OtherIncomeProperty.ToString(CurrFormatter), _arial10, _blackBrush, new PointF(520, currY));
            _gfx.DrawString("$ " + _obj.ProformaIncomeObject.OtherIncome.ToString(CurrFormatter), _arial10, _blackBrush, new PointF(850, currY));

            currY += 25;
            
            _gfx.DrawString("Operating Income (GOI):", _arial10, _blackBrush, new PointF(320, currY));
            _gfx.DrawString("Operating Income (GOI):", _arial10, _blackBrush, new PointF(650, currY));

            
            _gfx.DrawString("$ " + _obj.ActualIncomeObject.OperatingIncomeProperty.ToString(CurrFormatter), _arial10, _blackBrush, new PointF(520, currY));
            _gfx.DrawString("$ " + _obj.ProformaIncomeObject.OperatingIncome.ToString(CurrFormatter), _arial10, _blackBrush, new PointF(850, currY));
        }

        public void GenerateSelfStorageSummary(PrintPageEventArgs e, IncomeObject obj, string address)
        {
            _obj = obj;
            _e = e;
            _gfx = e.Graphics;
            _paperWidth = e.PageSettings.PaperSize.Height;

            _gfx.DrawString(ReportName, base._arial22, base._blackBrush, base.GetOrigin(_gfx, ePosition.Center, ReportName, base._arial22, _paperWidth, 20));

            int currY = 60;
            //1st ROW
            _gfx.DrawString("Property Site Address:", _arial12, _blackBrush, new PointF(20, currY));
            _gfx.DrawString(address, _arial12, _blackBrush, GetOriginUponAlreadySetText(_gfx, ePosition.Right, "Property Site Address:", new Point(20, currY), _arial12));

            //2nd ROW
            currY += 25;
            _gfx.DrawString("UNIT SUMMARY", _arial12, _blackBrush, new PointF(20, currY));

            //TABLE (max 20 rows + 2 header ROWs)
            //HEADER
            currY += 25;

            int startY = currY;
            _gfx.DrawLine(_blackPen, new Point(100, currY), new Point(920, currY));
            _gfx.DrawString("Actual Rent", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "Actual Rate", _arial10, new Rectangle(440, currY, 240, 20)));
            _gfx.DrawString("Proforma Rent", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "Proforma Rate", _arial10, new Rectangle(680, currY, 240, 20)));
            currY += 20;

            //DRAW HEADER TEXT
            _gfx.DrawString("#Units", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "Space", _arial10, new Rectangle(100, currY, 60, 20)));
            _gfx.DrawString("Size", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "Occup.", _arial10, new Rectangle(160, currY, 60, 20)));
            _gfx.DrawString("Floor", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "Tenant", _arial10, new Rectangle(220, currY, 60, 20)));
            _gfx.DrawString("%Occup.", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "Floor", _arial10, new Rectangle(280, currY, 60, 20)));
            _gfx.DrawString("SqFt.", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "SqFt.", _arial10, new Rectangle(380, currY, 60, 20)));
            _gfx.DrawString("Low", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "$/Month", _arial10, new Rectangle(440, currY, 80, 20)));
            _gfx.DrawString("High", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "$/Year", _arial10, new Rectangle(520, currY, 80, 20)));
            _gfx.DrawString("$/SqFt", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "$/SqFt/Mth", _arial10, new Rectangle(600, currY, 80, 20)));
            _gfx.DrawString("Low", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "$/SqFt/Yr", _arial10, new Rectangle(680, currY, 80, 20)));
            _gfx.DrawString("High", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "$/Month", _arial10, new Rectangle(760, currY, 80, 20)));
            _gfx.DrawString("$/SqFt", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "$/Year", _arial10, new Rectangle(840, currY, 80, 20)));            


            _gfx.DrawLine(_blackPen, new Point(100, currY), new Point(920, currY));
            currY += 20;
            _gfx.DrawLine(_blackPen, new Point(100, currY), new Point(920, currY));
            //currY += 20;


            for (int i = 0; i < _obj.RetailOfficeSummaryObject.Length; i++)
            {
                if (_obj.SelfStorageSummaryObject[i].IsValid)
                {
                    _gfx.DrawString(_obj.SelfStorageSummaryObject[i].NumberOfUnits.ToString(), _arial10, _blackBrush, new Point(160, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                    _gfx.DrawString(_obj.SelfStorageSummaryObject[i].Size, _arial10, _blackBrush, new Point(220, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                    _gfx.DrawString(_obj.SelfStorageSummaryObject[i].Floor, _arial10, _blackBrush, new Point(280, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                    _gfx.DrawString(_obj.SelfStorageSummaryObject[i].Occupancy, _arial10, _blackBrush, new Point(380, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                    _gfx.DrawString(_obj.SelfStorageSummaryObject[i].SqFt, _arial10, _blackBrush, new Point(440, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                    _gfx.DrawString(_obj.SelfStorageSummaryObject[i].AR_Low, _arial10, _blackBrush, new Point(520, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                    _gfx.DrawString(_obj.SelfStorageSummaryObject[i].AR_High, _arial10, _blackBrush, new Point(600, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                    _gfx.DrawString(_obj.SelfStorageSummaryObject[i].ARPerItem.ToString(CurrFormatter), _arial10, _blackBrush, new Point(680, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                    _gfx.DrawString(_obj.SelfStorageSummaryObject[i].MR_Low, _arial10, _blackBrush, new Point(760, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                    _gfx.DrawString(_obj.SelfStorageSummaryObject[i].MR_High, _arial10, _blackBrush, new Point(840, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                    _gfx.DrawString(_obj.SelfStorageSummaryObject[i].MRPerItem.ToString(CurrFormatter), _arial10, _blackBrush, new Point(920, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));

                    _gfx.DrawLine(_blackPen, new Point(100, currY), new Point(920, currY));
                    currY += 20;
                }
            }

            _gfx.DrawLine(_blackPen, new Point(100, currY), new Point(920, currY));

            //set totals            
            _gfx.DrawString("Total", _arial10, _blackBrush, new Point(100, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
            _gfx.DrawString(_obj.GetTotal_SS_Units().ToString(CurrFormatter), _arial10, _blackBrush, new Point(160, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
            _gfx.DrawString(_obj.GetTotal_SS_Occupancy().ToString(CurrFormatter), _arial10, _blackBrush, new Point(380, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
            _gfx.DrawString(_obj.GetTotal_SS_SqFt().ToString(CurrFormatter), _arial10, _blackBrush, new Point(440, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
            _gfx.DrawString(_obj.GetTotal_SS_AR_Low().ToString(CurrFormatter), _arial10, _blackBrush, new Point(520, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
            _gfx.DrawString(_obj.GetTotal_SS_AR_High().ToString(CurrFormatter), _arial10, _blackBrush, new Point(600, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
            _gfx.DrawString(_obj.GetTotal_SS_AR_UsdPerSqFt().ToString(CurrFormatter), _arial10, _blackBrush, new Point(680, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
            _gfx.DrawString(_obj.GetTotal_SS_MR_Low().ToString(CurrFormatter), _arial10, _blackBrush, new Point(760, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
            _gfx.DrawString(_obj.GetTotal_SS_MR_High().ToString(CurrFormatter), _arial10, _blackBrush, new Point(840, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
            _gfx.DrawString(_obj.GetTotal_SS_MR_UsdPerSqFt().ToString(CurrFormatter), _arial10, _blackBrush, new Point(920, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));            

            currY += 20;
            _gfx.DrawLine(_blackPen, new Point(100, currY), new Point(920, currY));

            //DRAW VERTICAL LINES            
            _gfx.DrawLine(_blackPen, new Point(100, startY), new Point(100, currY));
            _gfx.DrawLine(_blackPen, new Point(160, startY), new Point(160, currY));
            _gfx.DrawLine(_blackPen, new Point(220, startY), new Point(220, currY));
            _gfx.DrawLine(_blackPen, new Point(280, startY), new Point(280, currY));
            _gfx.DrawLine(_blackPen, new Point(380, startY), new Point(380, currY));
            _gfx.DrawLine(_blackPen, new Point(440, startY), new Point(440, currY));
            _gfx.DrawLine(_blackPen, new Point(520, startY + 20), new Point(520, currY));
            _gfx.DrawLine(_blackPen, new Point(600, startY + 20), new Point(600, currY));
            _gfx.DrawLine(_blackPen, new Point(680, startY), new Point(680, currY));
            _gfx.DrawLine(_blackPen, new Point(760, startY + 20), new Point(760, currY));
            _gfx.DrawLine(_blackPen, new Point(840, startY + 20), new Point(840, currY));
            _gfx.DrawLine(_blackPen, new Point(920, startY), new Point(920, currY));            


            //2nd ROW
            currY += 40;

            _gfx.DrawString("ACTUAL INCOME (annual):", _arial10, _blackBrush, new PointF(320, currY));
            _gfx.DrawString("PROFORMA INCOME (annual):", _arial10, _blackBrush, new PointF(650, currY));

            currY += 25;

            _gfx.DrawString("Gross Rent (GSI):", _arial10, _blackBrush, new PointF(320, currY));
            _gfx.DrawString("Gross Rent (GSI):", _arial10, _blackBrush, new PointF(650, currY));


            _gfx.DrawString("$ " + _obj.ActualIncomeObject.GrossRent, _arial10, _blackBrush, new PointF(520, currY));
            _gfx.DrawString("$ " + _obj.ProformaIncomeObject.GrossRentProperty, _arial10, _blackBrush, new PointF(850, currY));

            currY += 25;

            _gfx.DrawString("Vacancy/Credit Loss (%):", _arial10, _blackBrush, new PointF(320, currY));
            _gfx.DrawString("Vacancy/Credit Loss (%):", _arial10, _blackBrush, new PointF(650, currY));


            _gfx.DrawString(_obj.ActualIncomeObject.VacancyOrCreditLossPerc + " %", _arial10, _blackBrush, new PointF(520, currY));
            _gfx.DrawString(_obj.ProformaIncomeObject.VacancyOrCreditLossPercProperty + " %", _arial10, _blackBrush, new PointF(850, currY));

            currY += 25;

            _gfx.DrawString("Vacancy/Credit Loss ($):", _arial10, _blackBrush, new PointF(320, currY));
            _gfx.DrawString("Vacancy/Credit Loss ($):", _arial10, _blackBrush, new PointF(650, currY));


            _gfx.DrawString("$ " + _obj.ActualIncomeObject.VacancyOrCreditLossProperty.ToString(CurrFormatter), _arial10, _blackBrush, new PointF(520, currY));
            _gfx.DrawString("$ " + _obj.ProformaIncomeObject.VacancyOrCreditloss.ToString(CurrFormatter), _arial10, _blackBrush, new PointF(850, currY));

            currY += 25;

            _gfx.DrawString("Other Income:", _arial10, _blackBrush, new PointF(320, currY));
            _gfx.DrawString("Other Income:", _arial10, _blackBrush, new PointF(650, currY));


            _gfx.DrawString("$ " + _obj.ActualIncomeObject.OtherIncomeProperty.ToString(CurrFormatter), _arial10, _blackBrush, new PointF(520, currY));
            _gfx.DrawString("$ " + _obj.ProformaIncomeObject.OtherIncome.ToString(CurrFormatter), _arial10, _blackBrush, new PointF(850, currY));

            currY += 25;

            _gfx.DrawString("Operating Income (GOI):", _arial10, _blackBrush, new PointF(320, currY));
            _gfx.DrawString("Operating Income (GOI):", _arial10, _blackBrush, new PointF(650, currY));


            _gfx.DrawString("$ " + _obj.ActualIncomeObject.OperatingIncomeProperty.ToString(CurrFormatter), _arial10, _blackBrush, new PointF(520, currY));
            _gfx.DrawString("$ " + _obj.ProformaIncomeObject.OperatingIncome.ToString(CurrFormatter), _arial10, _blackBrush, new PointF(850, currY));
            
        }
    }
}
