﻿using System;
using System.Collections;
using System.Drawing;
using System.Drawing.Printing;

using ProfitGrabber.Commercial.Objects;

namespace ProfitGrabber.Commercial.Generators
{
    public class LoansInfoGenerator : BaseCommercialsGenerator
    {
        LoansObject _obj = null;
        const string ReportName = "LOANS INFO SHEET";


        public LoansInfoGenerator()
        {
            
        }

        public void GenerateDetails(PrintPageEventArgs e, LoansObject obj, string address)
        {
            _obj = obj;
            _e = e;
            _gfx = e.Graphics;
            _paperWidth = e.PageSettings.PaperSize.Width;

            _gfx.DrawString(ReportName, base._arial22, base._blackBrush, base.GetOrigin(_gfx, ePosition.Center, ReportName, base._arial22, _paperWidth, 20));

            int currY = 60;
            //1st ROW
            _gfx.DrawString("Property Site Address:", _arial12, _blackBrush, new PointF(20, currY));
            _gfx.DrawString(address, _arial12, _blackBrush, GetOriginUponAlreadySetText(_gfx, ePosition.Right, "Property Site Address:", new Point(20, currY), _arial12));

            //2nd ROW
            currY += 40;
            _gfx.DrawString("Purchase Price: $" + obj.PurchasePriceProperty, _arial10, _blackBrush, new PointF(20, currY));
            _gfx.DrawString("Down Payment: $" + obj.DownPaymentProperty, _arial10, _blackBrush, new PointF(250, currY));
            _gfx.DrawString("Down Payment: " + obj.DownPaymentPercValue.ToString("0.00") + " %", _arial10, _blackBrush, new PointF(500, currY));

            currY += 25;
            _gfx.DrawString("1st Loan Assumable: " + obj.FirstAssumableProperty, _arial10, _blackBrush, new PointF(20, currY));
            _gfx.DrawString("2nd Loan Assumable: " + obj.SecondAssumableProperty, _arial10, _blackBrush, new PointF(250, currY));
            _gfx.DrawString("Seller Carryback: " + obj.SellerCarrybackProperty, _arial10, _blackBrush, new PointF(500, currY));

            currY += 40;
            _gfx.DrawString("TOTAL DEBT SERVICE SUMMARY", _arial12, _blackBrush, new PointF(20, currY));

            currY += 25;
            _gfx.DrawString("Blended Rate: " + obj.BlendedRatePerc.ToString("0.000"), _arial10, _blackBrush, new PointF(20, currY));
            _gfx.DrawString("CLTV: " + obj.CLTVPerc.ToString(CurrFormatter), _arial10, _blackBrush, new PointF(250, currY));
            _gfx.DrawString("DCR: " + CalculateDCR().ToString(CurrFormatter), _arial10, _blackBrush, new PointF(500, currY));

            currY += 25;
            _gfx.DrawString("Total Payments (monthly): $" + obj.TotalMonthlyPayments.ToString(CurrFormatter), _arial10, _blackBrush, new PointF(20, currY));
            _gfx.DrawString("Total Payments (annual): $" + obj.TotalAnnualDebtService.ToString(CurrFormatter), _arial10, _blackBrush, new PointF(500, currY));

            //FIRST LOAN
            currY += 40;
            _gfx.DrawString("FIRST LOAN", _arial12, _blackBrush, new PointF(20, currY));

            currY += 25;
            _gfx.DrawString("Type: " + obj.LoansProperty[0].LoanTypeProperty, _arial10, _blackBrush, new PointF(20, currY));
            _gfx.DrawString("%LTV: " + obj.LoansProperty[0].LtvPercValue.ToString(CurrFormatter), _arial10, _blackBrush, new PointF(250, currY));
            _gfx.DrawString("Amount: $ " + obj.LoansProperty[0].AmountProperty, _arial10, _blackBrush, new PointF(500, currY));

            currY += 25;
            _gfx.DrawString("Amortization Term: " + obj.LoansProperty[0].AmortTermProperty, _arial10, _blackBrush, new PointF(20, currY));
            _gfx.DrawString("Interest Rate: " + obj.LoansProperty[0].RatePercProperty + " %", _arial10, _blackBrush, new PointF(250, currY));
            _gfx.DrawString("Monthly Payment: $ " + obj.LoansProperty[0].MonthlyPaymentProperty, _arial10, _blackBrush, new PointF(500, currY));

            currY += 25;
            _gfx.DrawString("Points: " + obj.LoansProperty[0].PointsPercProperty + " %", _arial10, _blackBrush, new PointF(20, currY));
            _gfx.DrawString("Points: $" + obj.LoansProperty[0].PointsUSDValue.ToString(CurrFormatter), _arial10, _blackBrush, new PointF(250, currY));
            _gfx.DrawString("Lender: " + obj.LoansProperty[0].LenderProperty, _arial10, _blackBrush, new PointF(500, currY));

            currY += 25;
            _gfx.DrawString("First Payment Date: " + obj.LoansProperty[0].FirstPaymentDateProperty, _arial10, _blackBrush, new PointF(20, currY));
            _gfx.DrawString("Last Payment Date: " + obj.LoansProperty[0].LastPaymentDateProperty, _arial10, _blackBrush, new PointF(500, currY));

            currY += 25;
            _gfx.DrawString("Adjustment Parameters (if adjustable)", _arial10, _blackBrush, new PointF(20, currY));

            currY += 25;
            _gfx.DrawString("Initial Fixed Period: " + obj.LoansProperty[0].LrAdjustmentsProperty.InitialFixedPeriodYrsProperty + " Yrs", _arial10, _blackBrush, new PointF(20, currY));
            _gfx.DrawString("Initial Rate Cap: " + obj.LoansProperty[0].LrAdjustmentsProperty.InitialRateCapPercProperty + " %", _arial10, _blackBrush, new PointF(250, currY));

            currY += 25;
            _gfx.DrawString("Annual Rate Cap: " + obj.LoansProperty[0].LrAdjustmentsProperty.AnnualCapPercProperty + " %", _arial10, _blackBrush, new PointF(20, currY));
            _gfx.DrawString("Lifetime Rate Cap: " + obj.LoansProperty[0].LrAdjustmentsProperty.LifeTimeCapPercProperty + " %", _arial10, _blackBrush, new PointF(250, currY));

            currY += 25;
            int startY = currY;
            _gfx.DrawLine(_blackPen, new PointF(50, currY), new Point(820, currY));
            _gfx.DrawString("Year 1", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "Year 1", _arial10, new Rectangle(120, currY, 70, 20)));
            _gfx.DrawString("Year 2", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "Year 2", _arial10, new Rectangle(190, currY, 70, 20)));
            _gfx.DrawString("Year 3", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "Year 3", _arial10, new Rectangle(260, currY, 70, 20)));
            _gfx.DrawString("Year 4", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "Year 4", _arial10, new Rectangle(330, currY, 70, 20)));
            _gfx.DrawString("Year 5", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "Year 5", _arial10, new Rectangle(400, currY, 70, 20)));
            _gfx.DrawString("Year 6", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "Year 6", _arial10, new Rectangle(470, currY, 70, 20)));
            _gfx.DrawString("Year 7", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "Year 7", _arial10, new Rectangle(540, currY, 70, 20)));
            _gfx.DrawString("Year 8", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "Year 8", _arial10, new Rectangle(610, currY, 70, 20)));
            _gfx.DrawString("Year 9", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "Year 9", _arial10, new Rectangle(680, currY, 70, 20)));
            _gfx.DrawString("Year 10", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "Year 10", _arial10, new Rectangle(750, currY, 70, 20)));

            currY += 20;
            _gfx.DrawLine(_blackPen, new PointF(50, currY), new Point(820, currY));
            _gfx.DrawString("Rate (%)", _arial10, _blackBrush, new PointF(50, currY));
            _gfx.DrawString(obj.LoansProperty[0].LrAdjustmentsProperty.RatesProperty[0] + "%", _arial10, _blackBrush, new PointF(190, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
            _gfx.DrawString(obj.LoansProperty[0].LrAdjustmentsProperty.RatesProperty[1] + "%", _arial10, _blackBrush, new PointF(260, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
            _gfx.DrawString(obj.LoansProperty[0].LrAdjustmentsProperty.RatesProperty[2] + "%", _arial10, _blackBrush, new PointF(330, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
            _gfx.DrawString(obj.LoansProperty[0].LrAdjustmentsProperty.RatesProperty[3] + "%", _arial10, _blackBrush, new PointF(400, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
            _gfx.DrawString(obj.LoansProperty[0].LrAdjustmentsProperty.RatesProperty[4] + "%", _arial10, _blackBrush, new PointF(470, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
            _gfx.DrawString(obj.LoansProperty[0].LrAdjustmentsProperty.RatesProperty[5] + "%", _arial10, _blackBrush, new PointF(540, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
            _gfx.DrawString(obj.LoansProperty[0].LrAdjustmentsProperty.RatesProperty[6] + "%", _arial10, _blackBrush, new PointF(610, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
            _gfx.DrawString(obj.LoansProperty[0].LrAdjustmentsProperty.RatesProperty[7] + "%", _arial10, _blackBrush, new PointF(680, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
            _gfx.DrawString(obj.LoansProperty[0].LrAdjustmentsProperty.RatesProperty[8] + "%", _arial10, _blackBrush, new PointF(750, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
            _gfx.DrawString(obj.LoansProperty[0].LrAdjustmentsProperty.RatesProperty[9] + "%", _arial10, _blackBrush, new PointF(820, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));

            currY += 20;
            _gfx.DrawLine(_blackPen, new PointF(50, currY), new Point(820, currY));

            //draw vert lines
            _gfx.DrawLine(_blackPen, new PointF(50, startY), new Point(50, currY));
            _gfx.DrawLine(_blackPen, new PointF(120, startY), new Point(120, currY));
            _gfx.DrawLine(_blackPen, new PointF(190, startY), new Point(190, currY));
            _gfx.DrawLine(_blackPen, new PointF(260, startY), new Point(260, currY));
            _gfx.DrawLine(_blackPen, new PointF(330, startY), new Point(330, currY));
            _gfx.DrawLine(_blackPen, new PointF(400, startY), new Point(400, currY));
            _gfx.DrawLine(_blackPen, new PointF(470, startY), new Point(470, currY));
            _gfx.DrawLine(_blackPen, new PointF(540, startY), new Point(540, currY));
            _gfx.DrawLine(_blackPen, new PointF(610, startY), new Point(610, currY));
            _gfx.DrawLine(_blackPen, new PointF(680, startY), new Point(680, currY));
            _gfx.DrawLine(_blackPen, new PointF(750, startY), new Point(750, currY));
            _gfx.DrawLine(_blackPen, new PointF(820, startY), new Point(820, currY));

            //SECOND LOAN
            if (null != obj.LoansProperty[2].LoanTypeProperty && string.Empty != obj.LoansProperty[2].LoanTypeProperty)
            {
                currY += 40;
                _gfx.DrawString("SECOND LOAN", _arial12, _blackBrush, new PointF(20, currY));

                currY += 25;
                _gfx.DrawString("Type: " + obj.LoansProperty[2].LoanTypeProperty, _arial10, _blackBrush, new PointF(20, currY));
                _gfx.DrawString("%LTV: " + obj.LoansProperty[2].LtvPercValue.ToString(CurrFormatter), _arial10, _blackBrush, new PointF(250, currY));
                _gfx.DrawString("Amount: $ " + obj.LoansProperty[2].AmountProperty, _arial10, _blackBrush, new PointF(500, currY));

                currY += 25;
                _gfx.DrawString("Amortization Term: " + obj.LoansProperty[2].AmortTermProperty, _arial10, _blackBrush, new PointF(20, currY));
                _gfx.DrawString("Interest Rate: " + obj.LoansProperty[2].RatePercProperty + " %", _arial10, _blackBrush, new PointF(250, currY));
                _gfx.DrawString("Monthly Payment: $ " + obj.LoansProperty[2].MonthlyPaymentProperty, _arial10, _blackBrush, new PointF(500, currY));

                currY += 25;
                _gfx.DrawString("Points: " + obj.LoansProperty[2].PointsPercProperty + " %", _arial10, _blackBrush, new PointF(20, currY));
                _gfx.DrawString("Points: $" + obj.LoansProperty[2].PointsUSDValue.ToString(CurrFormatter), _arial10, _blackBrush, new PointF(250, currY));
                _gfx.DrawString("Lender: " + obj.LoansProperty[2].LenderProperty, _arial10, _blackBrush, new PointF(500, currY));

                currY += 25;
                _gfx.DrawString("First Payment Date: " + obj.LoansProperty[2].FirstPaymentDateProperty, _arial10, _blackBrush, new PointF(20, currY));
                _gfx.DrawString("Last Payment Date: " + obj.LoansProperty[2].LastPaymentDateProperty, _arial10, _blackBrush, new PointF(500, currY));

                currY += 25;
                _gfx.DrawString("Adjustment Parameters (if adjustable)", _arial10, _blackBrush, new PointF(20, currY));

                currY += 25;
                _gfx.DrawString("Initial Fixed Period: " + obj.LoansProperty[2].LrAdjustmentsProperty.InitialFixedPeriodYrsProperty + " Yrs", _arial10, _blackBrush, new PointF(20, currY));
                _gfx.DrawString("Initial Rate Cap: " + obj.LoansProperty[2].LrAdjustmentsProperty.InitialRateCapPercProperty + " %", _arial10, _blackBrush, new PointF(250, currY));

                currY += 25;
                _gfx.DrawString("Annual Rate Cap: " + obj.LoansProperty[2].LrAdjustmentsProperty.AnnualCapPercProperty + " %", _arial10, _blackBrush, new PointF(20, currY));
                _gfx.DrawString("Lifetime Rate Cap: " + obj.LoansProperty[2].LrAdjustmentsProperty.LifeTimeCapPercProperty + " %", _arial10, _blackBrush, new PointF(250, currY));

                currY += 25;
                startY = currY;
                _gfx.DrawLine(_blackPen, new PointF(50, currY), new Point(820, currY));
                _gfx.DrawString("Year 1", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "Year 1", _arial10, new Rectangle(120, currY, 70, 20)));
                _gfx.DrawString("Year 2", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "Year 2", _arial10, new Rectangle(190, currY, 70, 20)));
                _gfx.DrawString("Year 3", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "Year 3", _arial10, new Rectangle(260, currY, 70, 20)));
                _gfx.DrawString("Year 4", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "Year 4", _arial10, new Rectangle(330, currY, 70, 20)));
                _gfx.DrawString("Year 5", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "Year 5", _arial10, new Rectangle(400, currY, 70, 20)));
                _gfx.DrawString("Year 6", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "Year 6", _arial10, new Rectangle(470, currY, 70, 20)));
                _gfx.DrawString("Year 7", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "Year 7", _arial10, new Rectangle(540, currY, 70, 20)));
                _gfx.DrawString("Year 8", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "Year 8", _arial10, new Rectangle(610, currY, 70, 20)));
                _gfx.DrawString("Year 9", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "Year 9", _arial10, new Rectangle(680, currY, 70, 20)));
                _gfx.DrawString("Year 10", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "Year 10", _arial10, new Rectangle(750, currY, 70, 20)));

                currY += 20;
                _gfx.DrawLine(_blackPen, new PointF(50, currY), new Point(820, currY));
                _gfx.DrawString("Rate (%)", _arial10, _blackBrush, new PointF(50, currY));
                _gfx.DrawString(obj.LoansProperty[2].LrAdjustmentsProperty.RatesProperty[0] + "%", _arial10, _blackBrush, new PointF(190, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                _gfx.DrawString(obj.LoansProperty[2].LrAdjustmentsProperty.RatesProperty[1] + "%", _arial10, _blackBrush, new PointF(260, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                _gfx.DrawString(obj.LoansProperty[2].LrAdjustmentsProperty.RatesProperty[2] + "%", _arial10, _blackBrush, new PointF(330, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                _gfx.DrawString(obj.LoansProperty[2].LrAdjustmentsProperty.RatesProperty[3] + "%", _arial10, _blackBrush, new PointF(400, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                _gfx.DrawString(obj.LoansProperty[2].LrAdjustmentsProperty.RatesProperty[4] + "%", _arial10, _blackBrush, new PointF(470, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                _gfx.DrawString(obj.LoansProperty[2].LrAdjustmentsProperty.RatesProperty[5] + "%", _arial10, _blackBrush, new PointF(540, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                _gfx.DrawString(obj.LoansProperty[2].LrAdjustmentsProperty.RatesProperty[6] + "%", _arial10, _blackBrush, new PointF(610, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                _gfx.DrawString(obj.LoansProperty[2].LrAdjustmentsProperty.RatesProperty[7] + "%", _arial10, _blackBrush, new PointF(680, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                _gfx.DrawString(obj.LoansProperty[2].LrAdjustmentsProperty.RatesProperty[8] + "%", _arial10, _blackBrush, new PointF(750, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                _gfx.DrawString(obj.LoansProperty[2].LrAdjustmentsProperty.RatesProperty[9] + "%", _arial10, _blackBrush, new PointF(820, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));

                currY += 20;
                _gfx.DrawLine(_blackPen, new PointF(50, currY), new Point(820, currY));

                //draw vert lines
                _gfx.DrawLine(_blackPen, new PointF(50, startY), new Point(50, currY));
                _gfx.DrawLine(_blackPen, new PointF(120, startY), new Point(120, currY));
                _gfx.DrawLine(_blackPen, new PointF(190, startY), new Point(190, currY));
                _gfx.DrawLine(_blackPen, new PointF(260, startY), new Point(260, currY));
                _gfx.DrawLine(_blackPen, new PointF(330, startY), new Point(330, currY));
                _gfx.DrawLine(_blackPen, new PointF(400, startY), new Point(400, currY));
                _gfx.DrawLine(_blackPen, new PointF(470, startY), new Point(470, currY));
                _gfx.DrawLine(_blackPen, new PointF(540, startY), new Point(540, currY));
                _gfx.DrawLine(_blackPen, new PointF(610, startY), new Point(610, currY));
                _gfx.DrawLine(_blackPen, new PointF(680, startY), new Point(680, currY));
                _gfx.DrawLine(_blackPen, new PointF(750, startY), new Point(750, currY));
                _gfx.DrawLine(_blackPen, new PointF(820, startY), new Point(820, currY));
            }

            if (null != obj.LoansProperty[1].LoanTypeProperty && string.Empty != obj.LoansProperty[1].LoanTypeProperty)
            {
                //THIRD LOAN
                currY += 40;
                _gfx.DrawString("THIRD LOAN", _arial12, _blackBrush, new PointF(20, currY));

                currY += 25;
                _gfx.DrawString("Type: " + obj.LoansProperty[1].LoanTypeProperty, _arial10, _blackBrush, new PointF(20, currY));
                _gfx.DrawString("%LTV: " + obj.LoansProperty[1].LtvPercValue.ToString(CurrFormatter), _arial10, _blackBrush, new PointF(250, currY));
                _gfx.DrawString("Amount: $ " + obj.LoansProperty[1].AmountProperty, _arial10, _blackBrush, new PointF(500, currY));

                currY += 25;
                _gfx.DrawString("Amortization Term: " + obj.LoansProperty[1].AmortTermProperty, _arial10, _blackBrush, new PointF(20, currY));
                _gfx.DrawString("Interest Rate: " + obj.LoansProperty[1].RatePercProperty + " %", _arial10, _blackBrush, new PointF(250, currY));
                _gfx.DrawString("Monthly Payment: $ " + obj.LoansProperty[1].MonthlyPaymentProperty, _arial10, _blackBrush, new PointF(500, currY));

                currY += 25;
                _gfx.DrawString("Points: " + obj.LoansProperty[1].PointsPercProperty + " %", _arial10, _blackBrush, new PointF(20, currY));
                _gfx.DrawString("Points: $" + obj.LoansProperty[1].PointsUSDValue.ToString(CurrFormatter), _arial10, _blackBrush, new PointF(250, currY));
                _gfx.DrawString("Lender: " + obj.LoansProperty[1].LenderProperty, _arial10, _blackBrush, new PointF(500, currY));

                currY += 25;
                _gfx.DrawString("First Payment Date: " + obj.LoansProperty[1].FirstPaymentDateProperty, _arial10, _blackBrush, new PointF(20, currY));
                _gfx.DrawString("Last Payment Date: " + obj.LoansProperty[1].LastPaymentDateProperty, _arial10, _blackBrush, new PointF(500, currY));

                currY += 25;
                _gfx.DrawString("Adjustment Parameters (if adjustable)", _arial10, _blackBrush, new PointF(20, currY));

                currY += 25;
                _gfx.DrawString("Initial Fixed Period: " + obj.LoansProperty[1].LrAdjustmentsProperty.InitialFixedPeriodYrsProperty + " Yrs", _arial10, _blackBrush, new PointF(20, currY));
                _gfx.DrawString("Initial Rate Cap: " + obj.LoansProperty[1].LrAdjustmentsProperty.InitialRateCapPercProperty + " %", _arial10, _blackBrush, new PointF(250, currY));

                currY += 25;
                _gfx.DrawString("Annual Rate Cap: " + obj.LoansProperty[1].LrAdjustmentsProperty.AnnualCapPercProperty + " %", _arial10, _blackBrush, new PointF(20, currY));
                _gfx.DrawString("Lifetime Rate Cap: " + obj.LoansProperty[1].LrAdjustmentsProperty.LifeTimeCapPercProperty + " %", _arial10, _blackBrush, new PointF(250, currY));

                currY += 25;
                startY = currY;
                _gfx.DrawLine(_blackPen, new PointF(50, currY), new Point(820, currY));
                _gfx.DrawString("Year 1", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "Year 1", _arial10, new Rectangle(120, currY, 70, 20)));
                _gfx.DrawString("Year 2", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "Year 2", _arial10, new Rectangle(190, currY, 70, 20)));
                _gfx.DrawString("Year 3", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "Year 3", _arial10, new Rectangle(260, currY, 70, 20)));
                _gfx.DrawString("Year 4", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "Year 4", _arial10, new Rectangle(330, currY, 70, 20)));
                _gfx.DrawString("Year 5", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "Year 5", _arial10, new Rectangle(400, currY, 70, 20)));
                _gfx.DrawString("Year 6", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "Year 6", _arial10, new Rectangle(470, currY, 70, 20)));
                _gfx.DrawString("Year 7", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "Year 7", _arial10, new Rectangle(540, currY, 70, 20)));
                _gfx.DrawString("Year 8", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "Year 8", _arial10, new Rectangle(610, currY, 70, 20)));
                _gfx.DrawString("Year 9", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "Year 9", _arial10, new Rectangle(680, currY, 70, 20)));
                _gfx.DrawString("Year 10", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "Year 10", _arial10, new Rectangle(750, currY, 70, 20)));

                currY += 20;
                _gfx.DrawLine(_blackPen, new PointF(50, currY), new Point(820, currY));
                _gfx.DrawString("Rate (%)", _arial10, _blackBrush, new PointF(50, currY));
                _gfx.DrawString(obj.LoansProperty[1].LrAdjustmentsProperty.RatesProperty[0] + "%", _arial10, _blackBrush, new PointF(190, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                _gfx.DrawString(obj.LoansProperty[1].LrAdjustmentsProperty.RatesProperty[1] + "%", _arial10, _blackBrush, new PointF(260, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                _gfx.DrawString(obj.LoansProperty[1].LrAdjustmentsProperty.RatesProperty[2] + "%", _arial10, _blackBrush, new PointF(330, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                _gfx.DrawString(obj.LoansProperty[1].LrAdjustmentsProperty.RatesProperty[3] + "%", _arial10, _blackBrush, new PointF(400, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                _gfx.DrawString(obj.LoansProperty[1].LrAdjustmentsProperty.RatesProperty[4] + "%", _arial10, _blackBrush, new PointF(470, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                _gfx.DrawString(obj.LoansProperty[1].LrAdjustmentsProperty.RatesProperty[5] + "%", _arial10, _blackBrush, new PointF(540, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                _gfx.DrawString(obj.LoansProperty[1].LrAdjustmentsProperty.RatesProperty[6] + "%", _arial10, _blackBrush, new PointF(610, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                _gfx.DrawString(obj.LoansProperty[1].LrAdjustmentsProperty.RatesProperty[7] + "%", _arial10, _blackBrush, new PointF(680, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                _gfx.DrawString(obj.LoansProperty[1].LrAdjustmentsProperty.RatesProperty[8] + "%", _arial10, _blackBrush, new PointF(750, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                _gfx.DrawString(obj.LoansProperty[1].LrAdjustmentsProperty.RatesProperty[9] + "%", _arial10, _blackBrush, new PointF(820, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));

                currY += 20;
                _gfx.DrawLine(_blackPen, new PointF(50, currY), new Point(820, currY));

                //draw vert lines
                _gfx.DrawLine(_blackPen, new PointF(50, startY), new Point(50, currY));
                _gfx.DrawLine(_blackPen, new PointF(120, startY), new Point(120, currY));
                _gfx.DrawLine(_blackPen, new PointF(190, startY), new Point(190, currY));
                _gfx.DrawLine(_blackPen, new PointF(260, startY), new Point(260, currY));
                _gfx.DrawLine(_blackPen, new PointF(330, startY), new Point(330, currY));
                _gfx.DrawLine(_blackPen, new PointF(400, startY), new Point(400, currY));
                _gfx.DrawLine(_blackPen, new PointF(470, startY), new Point(470, currY));
                _gfx.DrawLine(_blackPen, new PointF(540, startY), new Point(540, currY));
                _gfx.DrawLine(_blackPen, new PointF(610, startY), new Point(610, currY));
                _gfx.DrawLine(_blackPen, new PointF(680, startY), new Point(680, currY));
                _gfx.DrawLine(_blackPen, new PointF(750, startY), new Point(750, currY));
                _gfx.DrawLine(_blackPen, new PointF(820, startY), new Point(820, currY));
            }
            
        }

        decimal CalculateDCR()
        {
            /*
             *  DCR = NOI/(total annual payments)
                NOI = Actual Operating Income (from Income tab) - All Types_Total_$/year (from Expenses tab)
             */

            decimal dcr = 0;
            decimal noi = 0;
            decimal operatingIncome = IncomeObject.Instance.ActualIncomeObject.OperatingIncomeProperty;

            decimal usdPerYear = 0;

            try { usdPerYear = Convert.ToDecimal(ExpensesObject.Instance.TotalExpensesItemsProperty.Items[5].UsdPerYearProperty); }
            catch { usdPerYear = 0; }

            noi = operatingIncome - usdPerYear;

            if (0 != LoansObject.Instance.TotalAnnualDebtService)
                dcr = noi / LoansObject.Instance.TotalAnnualDebtService;
            else
                dcr = 0;

            return dcr;
        }
    }
}
