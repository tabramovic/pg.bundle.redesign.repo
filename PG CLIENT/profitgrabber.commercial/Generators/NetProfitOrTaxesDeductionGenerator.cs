﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Printing;

using ProfitGrabber.Commercial.Objects;

namespace ProfitGrabber.Commercial.Generators
{
    public class NetProfitOrTaxesDeductionGenerator : BaseCommercialsGenerator
    {
        MultiyearAnalysisObject _maObj = null;
        ExpensesObject _expObj = null;
        LoansObject _lObj = null;
        int _nrOfYears = 1;

        const string ReportName = "NET PROFIT FROM SALE ANALYSIS";
        const string ReportNameForTaxesAndDeductions = "TAXES AND DEDUCTIONS";

        CashFlowGenerator _cfg = null;

        //REGION REPORT DATA
        List<decimal> _gsiValuesPerYears = null;
        List<decimal> _vacancyCreditLossPerYears = null;
        List<decimal> _otherIncomePerYears = null;
        List<decimal> _grossOperatingIncomePerYears = null;
        List<decimal> _operatingExpensesPerYears = null;
        List<decimal> _netOperatingIncomePerYears = null;
        List<decimal> _debtService = null;
        List<decimal> _gsiAndOtherIncome = null;

        //SUMMARY
        List<decimal> _salePrices = null;

        //DETAILS
        List<decimal> _capitalImprovementsAnnual = null;
        List<decimal> _capitalImprovementsCumulative = null;
        List<decimal> _depreciationAnnual = null;
        List<decimal> _depreciationCumulative = null;
        List<decimal> _adjustedTaxBasis = null;

        //EXPENSES
        List<decimal> _downPayment = null;
        List<decimal> _purchaseClosingCosts = null;

        List<double> _pjsForFirstLoan = null;
        List<double> _ijsForFirstLoan = null;
        List<double> _lbforFirstLoan = null;

        List<double> _pjsForSecondLoan = null;
        List<double> _ijsForSecondLoan = null;
        List<double> _lbforSecondLoan = null;

        List<double> _pjsForThirdLoan = null;
        List<double> _ijsForThirdLoan = null;
        List<double> _lbforThirdLoan = null;

        List<decimal> _propertySaleCosts = null;
        List<double> _totalExpenses = null;
        List<decimal> _totalExchangeExpenses = null;

        //TAXES
        List<decimal> _taxableGains =  null;
        List<decimal> _depreciationRecoveryTaxes = null;
        List<decimal> _capitalGainsTaxes = null;
        List<decimal> _totalTaxLiabilities = null;

        //ANNUAL INCOME AND EXPENSES
        List<double> _incomeTax = null;
        List<double> _netIncomeProfitLoss = null;
        List<double> _netIncomeProfitLossCumulative = null;

        //SUMMARY
        List<double> _netSaleFromProfitLoss = null;
        List<double> _netProfitLoss =  null;
        List<double> _cashFlowPreTax = null;
        List<double> _cashFlowAfterTax =  null;
         

        //--- TAXES AND DEDUCTIONS ---
        
        //INCOME DETAILS
        List<decimal> _rentalIncome = null;
        List<double> _totalDeductions = null;

        //LOANS
        List<double> _totalInterestDeduction = null;

        //DEPRECIATION
        List<decimal> _buildingDepreciation = null;
        List<decimal> _capitalImprovementsDepreciationCumulative = null;
        List<decimal> _totalDepreciation = null;

        //TAXABLE INCOME
        List<double> _taxableIncome = null;

        //INCOME TAX
        List<double> _stateTax = null;
        List<double> _federalTax = null;
        List<double> _totalTax = null;

        public List<double> NetSaleFromProfitLoss
        {
            get { return _netSaleFromProfitLoss; }
        }

        public List<double> NetIncomeProfitLoss
        {
            get { return _netIncomeProfitLoss; }
        }

        public List<double> NetIncomeProfitLossCumulative
        {
            get { return _netIncomeProfitLossCumulative; }
        }

        public List<double> NetProfitLoss
        {
            get { return _netProfitLoss; }
        }

        public NetProfitOrTaxesDeductionGenerator()
        { }

        public void InitGenerator(MultiyearAnalysisObject maObj, ExpensesObject expObj, LoansObject lObj, int nrOfYears)
        {
            _maObj = maObj;
            _expObj = expObj;
            _lObj = lObj;
            _nrOfYears = 1;

            _cfg = new CashFlowGenerator();
            _cfg.InitGenerator(maObj, expObj, lObj, nrOfYears);
            _cfg.GenerateValues(nrOfYears);
        }

        public void GenerateValues(int nrOfYears)
        {
            _gsiValuesPerYears = InitGsiValuesPerYears(nrOfYears);
            _vacancyCreditLossPerYears = InitVacancyValuesPerYears(nrOfYears, _gsiValuesPerYears);
            _otherIncomePerYears = InitOtherIncomePerYears(nrOfYears);
            _grossOperatingIncomePerYears = InitGrossOperatingIncomePerYears(nrOfYears, _gsiValuesPerYears, _vacancyCreditLossPerYears, _otherIncomePerYears);
            _operatingExpensesPerYears = InitOperatingExpensesPerYears(nrOfYears, _gsiValuesPerYears);
            _netOperatingIncomePerYears = InitNetOperatingIncomePerYears(nrOfYears, _grossOperatingIncomePerYears, _operatingExpensesPerYears);
            _debtService = InitDebtServicePerYears(nrOfYears);
            _gsiAndOtherIncome = InitGsiAndOtherIncome(nrOfYears, _gsiValuesPerYears, _otherIncomePerYears);

            //SUMMARY
            _salePrices = InitSalePrice(nrOfYears, _netOperatingIncomePerYears);

            //DETAILS
            _capitalImprovementsAnnual = InitCapitalImprovementsAnnual(nrOfYears);
            _capitalImprovementsCumulative = InitCapitalImprovementsCumuative(nrOfYears, _capitalImprovementsAnnual);
            _depreciationAnnual = InitDepreciationAnnual(nrOfYears, _capitalImprovementsCumulative);
            _depreciationCumulative = InitDepreciationCumulative(nrOfYears, _depreciationAnnual);
            _adjustedTaxBasis = InitAdjustedTaxBasis(nrOfYears, _capitalImprovementsCumulative, _depreciationCumulative);

            //EXPENSES
            _downPayment = InitDownPayment(nrOfYears);
            _purchaseClosingCosts = InitPurchaseClosingCosts(nrOfYears);

            _pjsForFirstLoan = new List<double>();
            _ijsForFirstLoan = new List<double>();
            InitPrincipalAndInterestPaymentsPerYears(nrOfYears, 0, out _pjsForFirstLoan, out _ijsForFirstLoan);
            _lbforFirstLoan = InitLoanBalance(nrOfYears, 0, _pjsForFirstLoan);

            _pjsForSecondLoan = new List<double>();
            _ijsForSecondLoan = new List<double>();
            InitPrincipalAndInterestPaymentsPerYears(nrOfYears, 1, out _pjsForSecondLoan, out _ijsForSecondLoan);
            _lbforSecondLoan = InitLoanBalance(nrOfYears, 1, _pjsForSecondLoan);

            _pjsForThirdLoan = new List<double>();
            _ijsForThirdLoan = new List<double>();
            InitPrincipalAndInterestPaymentsPerYears(nrOfYears, 2, out _pjsForThirdLoan, out _ijsForThirdLoan);
            _lbforThirdLoan = InitLoanBalance(nrOfYears, 2, _pjsForThirdLoan);

            _propertySaleCosts = InitPropertySaleCosts(nrOfYears, _salePrices);
            _totalExpenses = InitTotalExpenses(nrOfYears, _downPayment, _purchaseClosingCosts, _capitalImprovementsCumulative, _lbforFirstLoan, _lbforSecondLoan, _lbforThirdLoan, _propertySaleCosts);
            _totalExchangeExpenses = InitTotalExchangeExpenses(nrOfYears, _purchaseClosingCosts, _propertySaleCosts);

            //TAXES
            _taxableGains = InitTaxableGain(nrOfYears, _salePrices, _adjustedTaxBasis, _totalExchangeExpenses);
            _depreciationRecoveryTaxes = InitDepreciationRecoveryTaxes(nrOfYears, _depreciationCumulative);
            _capitalGainsTaxes = InitCapitalGainsTaxes(nrOfYears, _taxableGains);
            _totalTaxLiabilities = InitTotalTaxLiabilities(nrOfYears, _depreciationRecoveryTaxes, _capitalGainsTaxes);

            //ANNUAL INCOME AND EXPENSES
            _incomeTax = InitIncomeTaxes(nrOfYears, _cfg.totalTaxCostPerYears);
            _netIncomeProfitLoss = InitNetIncomeProfitLoss(nrOfYears, _netOperatingIncomePerYears, _debtService, _incomeTax);
            _netIncomeProfitLossCumulative = InitNetIncomeProfitLossCumulative(nrOfYears, _netIncomeProfitLoss);

            //SUMMARY
            _netSaleFromProfitLoss = InitNetSaleProfitLoss(nrOfYears, _salePrices, _totalExpenses, _totalTaxLiabilities);
            _netProfitLoss = InitNetProfitLoss(nrOfYears, _netSaleFromProfitLoss, _netIncomeProfitLossCumulative);

            //--- TAXES AND DEDUCTIONS ---            
            
            //LOANS
            _totalInterestDeduction = InitTotalInterestDeduction(nrOfYears, _ijsForFirstLoan, _ijsForSecondLoan, _ijsForThirdLoan);

            //DEPRECIATION
            _buildingDepreciation = CalculateBuildingDepreciation(nrOfYears, Util.GetDecimalFromString(_maObj.BuildingValue), Util.GetDecimalFromString(_maObj.DepreciableYears));
            _capitalImprovementsDepreciationCumulative = CalculateCumulativaCapitalImprovementsDepreciation(nrOfYears, _capitalImprovementsCumulative, Util.GetDecimalFromString(_maObj.DepreciableYears));
            _totalDepreciation = CalculateTotalDepreciation(nrOfYears, _buildingDepreciation, _capitalImprovementsDepreciationCumulative);

            //TAXABLE INCOME
            _taxableIncome = _cfg.taxableIncomePerYears; //CalculateTaxableIncome(nrOfYears, _netOperatingIncomePerYears, _buildingDepreciation, _totalInterestDeduction);

            //INCOME TAX
            _stateTax = CalculateStateTax(nrOfYears, _taxableIncome, Util.GetDecimalFromString(_maObj.StateRate));
            _federalTax = CalculateFederalTax(nrOfYears, _taxableIncome, Util.GetDecimalFromString(_maObj.FederalRate));
            _totalTax = CalculateTotalTax(nrOfYears, _stateTax, _federalTax);

            //INCOME DETAILS
            _rentalIncome = _cfg.gsiValuesPerYears; //InitRentalIncome(nrOfYears, _grossOperatingIncomePerYears, _maObj.IncomeChangePercs);
            _totalDeductions = InitTotalDeductions(nrOfYears, _totalDepreciation, _totalInterestDeduction);
            _cashFlowPreTax = InitCashFlowBeforeTaxesPerYears(nrOfYears, _netOperatingIncomePerYears, _debtService, _capitalImprovementsAnnual);
            _cashFlowAfterTax = InitCashFlowAfterTax(nrOfYears, _cashFlowPreTax, _totalTax);
        }

        public void Generate(PrintPageEventArgs e, MultiyearAnalysisObject maObj, ExpensesObject expObj, LoansObject lObj, int nrOfYears)
        {
            _maObj = maObj;
            _expObj = expObj;
            _lObj = lObj;
            _e = e;
            _gfx = e.Graphics;
            _paperWidth = e.PageSettings.PaperSize.Height;

            _cfg = new CashFlowGenerator();
            _cfg.InitGenerator(maObj, expObj, lObj, nrOfYears);
            _cfg.GenerateValues(nrOfYears);
            

            GenerateValues(nrOfYears);
            
            int currY = 60;

            if (0 == _pageCnt)
            {

                #region HEADER
                _gfx.DrawString(ReportName, base._arial22, base._blackBrush, base.GetOrigin(_gfx, ePosition.Center, ReportName, base._arial22, _paperWidth, 20));

                _gfx.DrawString("Purchase Information", _arial10B, _blackBrush, new PointF(40, currY));
                _gfx.DrawString("Initial Income and", _arial10B, _blackBrush, new PointF(220, currY));
                _gfx.DrawString("Expense Factors", _arial10B, _blackBrush, new PointF(220, currY + 15));
                _gfx.DrawString("Annual Changes", _arial10B, _blackBrush, new PointF(420, currY));
                _gfx.DrawString("Loan Information", _arial10B, _blackBrush, new PointF(580, currY));
                _gfx.DrawString("Tax Information", _arial10B, _blackBrush, new PointF(790, currY));
                _gfx.DrawString("Sale Information", _arial10B, _blackBrush, new PointF(950, currY));

                _gfx.DrawLine(_blackPen, new Point(40, currY + 30), new Point(1050, currY + 30));

                currY += 40;
                _gfx.DrawString("Pur. Price: " + _maObj.PurchasePrice, _arial10, _blackBrush, new PointF(40, currY));
                _gfx.DrawString("Initial GSI: " + _maObj.GsiAnnual, _arial10, _blackBrush, new PointF(220, currY));
                _gfx.DrawString("%Income: " + _maObj.AnnualIncomeChange, _arial10, _blackBrush, new PointF(420, currY));
                if (null != _lObj && null != _lObj.LoansProperty && null != _lObj.LoansProperty[0])
                    _gfx.DrawString("1st: " + _lObj.LoansProperty[0].ToString(), _arial10, _blackBrush, new PointF(580, currY));
                _gfx.DrawString("Fed. Income Rate: " + _maObj.FederalRate, _arial10, _blackBrush, new PointF(790, currY));
                _gfx.DrawString("Sale Costs(%): " + _maObj.SaleCosts, _arial10, _blackBrush, new PointF(950, currY));

                currY += 20;
                _gfx.DrawString("Down Pmnt:" + _maObj.DownPayement, _arial10, _blackBrush, new PointF(40, currY));
                _gfx.DrawString("Init. %Vacancy: " + _maObj.VacancyCreditLossPerc, _arial10, _blackBrush, new PointF(220, currY));
                _gfx.DrawString("%Vacancy: " + _maObj.AnnualVacancyChange, _arial10, _blackBrush, new PointF(420, currY));
                if (null != _lObj && null != _lObj.LoansProperty && null != _lObj.LoansProperty[1])
                    _gfx.DrawString("2nd: " + _lObj.LoansProperty[1].ToString(), _arial10, _blackBrush, new PointF(580, currY));
                _gfx.DrawString("State Income Rate: " + _maObj.StateRate, _arial10, _blackBrush, new PointF(790, currY));
                _gfx.DrawString("Sale Cap Rate: " + _maObj.SaleCapRate, _arial10, _blackBrush, new PointF(950, currY));

                currY += 20;
                _gfx.DrawString("Points: " + _maObj.Points, _arial10, _blackBrush, new PointF(40, currY));
                _gfx.DrawString("Init. Other Income: " + _maObj.OtherIncome, _arial10, _blackBrush, new PointF(220, currY));
                _gfx.DrawString("%Inflation: " + _maObj.AnnualInflationChange, _arial10, _blackBrush, new PointF(420, currY));
                if (null != _lObj && null != _lObj.LoansProperty && null != _lObj.LoansProperty[2])
                    _gfx.DrawString("3rd: " + _lObj.LoansProperty[2].ToString(), _arial10, _blackBrush, new PointF(580, currY));
                _gfx.DrawString("Depreciable Value: " + _maObj.BuildingValue, _arial10, _blackBrush, new PointF(790, currY));

                currY += 20;
                _gfx.DrawString("Oth. Acq. Costs: " + _maObj.OtherAcquisitionCosts, _arial10, _blackBrush, new PointF(40, currY));
                _gfx.DrawString("Init. Oper. Expenses: " + _maObj.OperatingExpenses, _arial10, _blackBrush, new PointF(220, currY));
                _gfx.DrawString("%Oper. Expenses: " + _maObj.AnnualOperatingExpensesChange, _arial10, _blackBrush, new PointF(420, currY));
                _gfx.DrawString("Depreciable Years: " + _maObj.DepreciableYears, _arial10, _blackBrush, new PointF(790, currY));

                currY += 20;
                _gfx.DrawString("Capital Gains Rate: " + _maObj.CapitalGainsRate, _arial10, _blackBrush, new PointF(790, currY));

                currY += 5;
                _gfx.DrawString("CAPITAL ADDITIONS (IMPROVEMENTS)", _arial12, _blackBrush, new PointF(20, currY));

                //TABLE
                currY += 25;
                int startY = currY;
                _gfx.DrawLine(_blackPen, new Point(40, currY), new Point(940, currY));

                _gfx.DrawString(string.Empty, _arial10, _blackBrush, new Point(40, currY));
                _gfx.DrawString("Year 1", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "Year 1", _arial10, new Rectangle(140, currY, 80, 20)));
                _gfx.DrawString("Year 2", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "Year 2", _arial10, new Rectangle(220, currY, 80, 20)));
                _gfx.DrawString("Year 3", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "Year 3", _arial10, new Rectangle(300, currY, 80, 20)));
                _gfx.DrawString("Year 4", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "Year 4", _arial10, new Rectangle(380, currY, 80, 20)));
                _gfx.DrawString("Year 5", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "Year 5", _arial10, new Rectangle(460, currY, 80, 20)));
                _gfx.DrawString("Year 6", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "Year 6", _arial10, new Rectangle(540, currY, 80, 20)));
                _gfx.DrawString("Year 7", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "Year 7", _arial10, new Rectangle(620, currY, 80, 20)));
                _gfx.DrawString("Year 8", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "Year 8", _arial10, new Rectangle(700, currY, 80, 20)));
                _gfx.DrawString("Year 9", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "Year 9", _arial10, new Rectangle(780, currY, 80, 20)));
                _gfx.DrawString("Year 10", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "Year 10", _arial10, new Rectangle(860, currY, 80, 20)));

                currY += 20;
                _gfx.DrawLine(_blackPen, new Point(40, currY), new Point(940, currY));
                _gfx.DrawString("Cost:", _arial10, _blackBrush, new Point(90, currY));

                _gfx.DrawString("$" + expObj.CapitalAdditionsProperty.yearsProperty[0]._costProperty, _arial10, _blackBrush, new Point(220, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                _gfx.DrawString("$" + expObj.CapitalAdditionsProperty.yearsProperty[1]._costProperty, _arial10, _blackBrush, new Point(300, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                _gfx.DrawString("$" + expObj.CapitalAdditionsProperty.yearsProperty[2]._costProperty, _arial10, _blackBrush, new Point(380, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                _gfx.DrawString("$" + expObj.CapitalAdditionsProperty.yearsProperty[3]._costProperty, _arial10, _blackBrush, new Point(460, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                _gfx.DrawString("$" + expObj.CapitalAdditionsProperty.yearsProperty[4]._costProperty, _arial10, _blackBrush, new Point(540, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                _gfx.DrawString("$" + expObj.CapitalAdditionsProperty.yearsProperty[5]._costProperty, _arial10, _blackBrush, new Point(620, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                _gfx.DrawString("$" + expObj.CapitalAdditionsProperty.yearsProperty[6]._costProperty, _arial10, _blackBrush, new Point(700, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                _gfx.DrawString("$" + expObj.CapitalAdditionsProperty.yearsProperty[7]._costProperty, _arial10, _blackBrush, new Point(780, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                _gfx.DrawString("$" + expObj.CapitalAdditionsProperty.yearsProperty[8]._costProperty, _arial10, _blackBrush, new Point(860, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                _gfx.DrawString("$" + expObj.CapitalAdditionsProperty.yearsProperty[9]._costProperty, _arial10, _blackBrush, new Point(940, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));

                currY += 20;
                _gfx.DrawLine(_blackPen, new Point(40, currY), new Point(940, currY));
                _gfx.DrawString("Type:", _arial10, _blackBrush, new Point(90, currY));

                _gfx.DrawString(expObj.CapitalAdditionsProperty.yearsProperty[0]._typeProperty, _arial10, _blackBrush, new Point(220, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                _gfx.DrawString(expObj.CapitalAdditionsProperty.yearsProperty[1]._typeProperty, _arial10, _blackBrush, new Point(300, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                _gfx.DrawString(expObj.CapitalAdditionsProperty.yearsProperty[2]._typeProperty, _arial10, _blackBrush, new Point(380, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                _gfx.DrawString(expObj.CapitalAdditionsProperty.yearsProperty[3]._typeProperty, _arial10, _blackBrush, new Point(460, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                _gfx.DrawString(expObj.CapitalAdditionsProperty.yearsProperty[4]._typeProperty, _arial10, _blackBrush, new Point(540, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                _gfx.DrawString(expObj.CapitalAdditionsProperty.yearsProperty[5]._typeProperty, _arial10, _blackBrush, new Point(620, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                _gfx.DrawString(expObj.CapitalAdditionsProperty.yearsProperty[6]._typeProperty, _arial10, _blackBrush, new Point(700, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                _gfx.DrawString(expObj.CapitalAdditionsProperty.yearsProperty[7]._typeProperty, _arial10, _blackBrush, new Point(780, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                _gfx.DrawString(expObj.CapitalAdditionsProperty.yearsProperty[8]._typeProperty, _arial10, _blackBrush, new Point(860, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                _gfx.DrawString(expObj.CapitalAdditionsProperty.yearsProperty[9]._typeProperty, _arial10, _blackBrush, new Point(940, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));

                currY += 20;
                _gfx.DrawLine(_blackPen, new Point(40, currY), new Point(940, currY));

                //DRAW VERTICAL LINES
                _gfx.DrawLine(_blackPen, new Point(40, startY), new Point(40, currY));
                _gfx.DrawLine(_blackPen, new Point(140, startY), new Point(140, currY));
                _gfx.DrawLine(_blackPen, new Point(220, startY), new Point(220, currY));
                _gfx.DrawLine(_blackPen, new Point(300, startY), new Point(300, currY));
                _gfx.DrawLine(_blackPen, new Point(380, startY), new Point(380, currY));
                _gfx.DrawLine(_blackPen, new Point(460, startY), new Point(460, currY));
                _gfx.DrawLine(_blackPen, new Point(540, startY), new Point(540, currY));
                _gfx.DrawLine(_blackPen, new Point(620, startY), new Point(620, currY));
                _gfx.DrawLine(_blackPen, new Point(700, startY), new Point(700, currY));
                _gfx.DrawLine(_blackPen, new Point(780, startY), new Point(780, currY));
                _gfx.DrawLine(_blackPen, new Point(860, startY), new Point(860, currY));
                _gfx.DrawLine(_blackPen, new Point(940, startY), new Point(940, currY));

                currY += 25;
                #endregion

                #region BODY - SUMMARY
                for (int iRow = 0; iRow <= 9; iRow++)
                {
                    _gfx.DrawLine(_blackPen, new Point(40, currY), new Point(1050, currY));

                    if (iRow == 0)
                        _gfx.DrawString(GetLabel(iRow, 0), _arial10B, _blackBrush, new Point(42, currY));
                    else
                        _gfx.DrawString(GetLabel(iRow, 0), _arial10, _blackBrush, new Point(42, currY));

                    if (iRow < 9)
                    {
                        _gfx.DrawLine(_blackPen, new Point(40, currY), new Point(40, currY + 20));
                        _gfx.DrawLine(_blackPen, new Point(250, currY), new Point(250, currY + 20));
                        _gfx.DrawLine(_blackPen, new Point(1050, currY), new Point(1050, currY + 20));

                        for (int jCol = 0; jCol < nrOfYears; jCol++)
                        {
                            _gfx.DrawLine(_blackPen, new Point(250 + (jCol + 1) * 80, currY), new Point(250 + (jCol + 1) * 80, currY + 20));

                            if (0 == iRow)
                                _gfx.DrawString("Year " + (jCol + 1).ToString(), _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "Year " + (jCol + 1).ToString(), _arial10, new Rectangle(250 + (jCol) * 80, currY, 80, 20)));
                            else if (1 == iRow)
                                _gfx.DrawString(FormatStringForOutput(_salePrices[jCol]), _arial10, _blackBrush, new Point(250 + (jCol + 1) * 80, currY + 2), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                            else if (2 == iRow)
                                _gfx.DrawString(FormatStringForOutput(_adjustedTaxBasis[jCol]), _arial10, _blackBrush, new Point(250 + (jCol + 1) * 80, currY + 2), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                            else if (3 == iRow)
                                _gfx.DrawString(FormatStringForOutput(_taxableGains[jCol]), _arial10, _blackBrush, new Point(250 + (jCol + 1) * 80, currY + 2), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                            else if (4 == iRow)
                                _gfx.DrawString(FormatStringForOutput(_totalTaxLiabilities[jCol]), _arial10, _blackBrush, new Point(250 + (jCol + 1) * 80, currY + 2), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                            else if (5 == iRow)
                                _gfx.DrawString(FormatStringForOutput(_totalExpenses[jCol]), _arial10, _blackBrush, new Point(250 + (jCol + 1) * 80, currY + 2), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                            else if (6 == iRow)
                                _gfx.DrawString(FormatStringForOutput(_netSaleFromProfitLoss[jCol]), _arial10, _blackBrush, new Point(250 + (jCol + 1) * 80, currY + 2), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                            else if (7 == iRow)
                                _gfx.DrawString(FormatStringForOutput(_netIncomeProfitLossCumulative[jCol]), _arial10, _blackBrush, new Point(250 + (jCol + 1) * 80, currY + 2), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                            else if (8 == iRow)
                                _gfx.DrawString(FormatStringForOutput(_netProfitLoss[jCol]), _arial10, _blackBrush, new Point(250 + (jCol + 1) * 80, currY + 2), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                        }
                    }
                    currY += 20;
                }
                #endregion

                #region BODY - TAX BASIS DETAILS
                for (int iRow = 0; iRow <= 7; iRow++)
                {
                    _gfx.DrawLine(_blackPen, new Point(40, currY), new Point(1050, currY));

                    if (iRow == 0)
                        _gfx.DrawString(GetLabel(iRow, 1), _arial10B, _blackBrush, new Point(42, currY));
                    else
                        _gfx.DrawString(GetLabel(iRow, 1), _arial10, _blackBrush, new Point(42, currY));

                    if (iRow < 7)
                    {
                        _gfx.DrawLine(_blackPen, new Point(40, currY), new Point(40, currY + 20));
                        _gfx.DrawLine(_blackPen, new Point(250, currY), new Point(250, currY + 20));
                        _gfx.DrawLine(_blackPen, new Point(1050, currY), new Point(1050, currY + 20));

                        for (int jCol = 0; jCol < nrOfYears; jCol++)
                        {
                            _gfx.DrawLine(_blackPen, new Point(250 + (jCol + 1) * 80, currY), new Point(250 + (jCol + 1) * 80, currY + 20));

                            if (0 == iRow)
                                _gfx.DrawString("Year " + (jCol + 1).ToString(), _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "Year " + (jCol + 1).ToString(), _arial10, new Rectangle(250 + (jCol) * 80, currY, 80, 20)));
                            else if (1 == iRow)
                                _gfx.DrawString(FormatStringForOutput(_lObj.PurchasePriceValue), _arial10, _blackBrush, new Point(250 + (jCol + 1) * 80, currY + 2), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                            else if (2 == iRow)
                                _gfx.DrawString(FormatStringForOutput(_capitalImprovementsAnnual[jCol]), _arial10, _blackBrush, new Point(250 + (jCol + 1) * 80, currY + 2), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                            else if (3 == iRow)
                                _gfx.DrawString(FormatStringForOutput(_capitalImprovementsCumulative[jCol]), _arial10, _blackBrush, new Point(250 + (jCol + 1) * 80, currY + 2), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                            else if (4 == iRow)
                                _gfx.DrawString(FormatStringForOutput(_depreciationAnnual[jCol]), _arial10, _blackBrush, new Point(250 + (jCol + 1) * 80, currY + 2), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                            else if (5 == iRow)
                                _gfx.DrawString(FormatStringForOutput(_depreciationCumulative[jCol]), _arial10, _blackBrush, new Point(250 + (jCol + 1) * 80, currY + 2), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                            else if (6 == iRow)
                                _gfx.DrawString(FormatStringForOutput(_adjustedTaxBasis[jCol]), _arial10, _blackBrush, new Point(250 + (jCol + 1) * 80, currY + 2), new StringFormat(StringFormatFlags.DirectionRightToLeft));                            
                        }
                    }
                    currY += 20;
                }
                #endregion

                _gfx.DrawString("Page 1/2", _arial8I, _blackBrush, new PointF(1000, 800));
                _pageCnt++;
                e.HasMorePages = true;
                return;
            }
            else
            {
                #region BODY - EXPENSES
                for (int iRow = 0; iRow <= 10; iRow++)
                {
                    _gfx.DrawLine(_blackPen, new Point(40, currY), new Point(1050, currY));

                    if (iRow == 0)
                        _gfx.DrawString(GetLabel(iRow, 2), _arial10B, _blackBrush, new Point(42, currY));
                    else
                        _gfx.DrawString(GetLabel(iRow, 2), _arial10, _blackBrush, new Point(42, currY));

                    if (iRow < 10)
                    {
                        _gfx.DrawLine(_blackPen, new Point(40, currY), new Point(40, currY + 20));
                        _gfx.DrawLine(_blackPen, new Point(250, currY), new Point(250, currY + 20));
                        _gfx.DrawLine(_blackPen, new Point(1050, currY), new Point(1050, currY + 20));

                        for (int jCol = 0; jCol < nrOfYears; jCol++)
                        {
                            _gfx.DrawLine(_blackPen, new Point(250 + (jCol + 1) * 80, currY), new Point(250 + (jCol + 1) * 80, currY + 20));

                            if (0 == iRow)
                                _gfx.DrawString("Year " + (jCol + 1).ToString(), _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "Year " + (jCol + 1).ToString(), _arial10, new Rectangle(250 + (jCol) * 80, currY, 80, 20)));
                            else if (1 == iRow)
                                _gfx.DrawString(FormatStringForOutput(_downPayment[jCol]), _arial10, _blackBrush, new Point(250 + (jCol + 1) * 80, currY + 2), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                            else if (2 == iRow)
                                _gfx.DrawString(FormatStringForOutput(_purchaseClosingCosts[jCol]), _arial10, _blackBrush, new Point(250 + (jCol + 1) * 80, currY + 2), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                            else if (3 == iRow)
                                _gfx.DrawString(FormatStringForOutput(_capitalImprovementsCumulative[jCol]), _arial10, _blackBrush, new Point(250 + (jCol + 1) * 80, currY + 2), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                            else if (4 == iRow)
                                _gfx.DrawString(FormatStringForOutput(_lbforFirstLoan[jCol]), _arial10, _blackBrush, new Point(250 + (jCol + 1) * 80, currY + 2), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                            else if (5 == iRow)
                                _gfx.DrawString(FormatStringForOutput(_lbforSecondLoan[jCol]), _arial10, _blackBrush, new Point(250 + (jCol + 1) * 80, currY + 2), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                            else if (6 == iRow)
                                _gfx.DrawString(FormatStringForOutput(_lbforThirdLoan[jCol]), _arial10, _blackBrush, new Point(250 + (jCol + 1) * 80, currY + 2), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                            else if (7 == iRow)
                                _gfx.DrawString(FormatStringForOutput(_propertySaleCosts[jCol]), _arial10, _blackBrush, new Point(250 + (jCol + 1) * 80, currY + 2), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                            else if (8 == iRow)
                                _gfx.DrawString(FormatStringForOutput(_totalExpenses[jCol]), _arial10, _blackBrush, new Point(250 + (jCol + 1) * 80, currY + 2), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                            else if (9 == iRow)
                                _gfx.DrawString(FormatStringForOutput(_totalExchangeExpenses[jCol]), _arial10, _blackBrush, new Point(250 + (jCol + 1) * 80, currY + 2), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                        }
                    }
                    currY += 20;
                }
                #endregion

                #region BODY - TAXES
                for (int iRow = 0; iRow <= 5; iRow++)
                {
                    _gfx.DrawLine(_blackPen, new Point(40, currY), new Point(1050, currY));

                    if (iRow == 0)
                        _gfx.DrawString(GetLabel(iRow, 3), _arial10B, _blackBrush, new Point(42, currY));
                    else
                        _gfx.DrawString(GetLabel(iRow, 3), _arial10, _blackBrush, new Point(42, currY));

                    if (iRow < 5)
                    {
                        _gfx.DrawLine(_blackPen, new Point(40, currY), new Point(40, currY + 20));
                        _gfx.DrawLine(_blackPen, new Point(250, currY), new Point(250, currY + 20));
                        _gfx.DrawLine(_blackPen, new Point(1050, currY), new Point(1050, currY + 20));

                        for (int jCol = 0; jCol < nrOfYears; jCol++)
                        {
                            _gfx.DrawLine(_blackPen, new Point(250 + (jCol + 1) * 80, currY), new Point(250 + (jCol + 1) * 80, currY + 20));

                            if (0 == iRow)
                                _gfx.DrawString("Year " + (jCol + 1).ToString(), _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "Year " + (jCol + 1).ToString(), _arial10, new Rectangle(250 + (jCol) * 80, currY, 80, 20)));
                            else if (1 == iRow)
                                _gfx.DrawString(FormatStringForOutput(_taxableGains[jCol]), _arial10, _blackBrush, new Point(250 + (jCol + 1) * 80, currY + 2), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                            else if (2 == iRow)
                                _gfx.DrawString(FormatStringForOutput(_depreciationRecoveryTaxes[jCol]), _arial10, _blackBrush, new Point(250 + (jCol + 1) * 80, currY + 2), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                            else if (3 == iRow)
                                _gfx.DrawString(FormatStringForOutput(_capitalGainsTaxes[jCol]), _arial10, _blackBrush, new Point(250 + (jCol + 1) * 80, currY + 2), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                            else if (4 == iRow)
                                _gfx.DrawString(FormatStringForOutput(_totalTaxLiabilities[jCol]), _arial10, _blackBrush, new Point(250 + (jCol + 1) * 80, currY + 2), new StringFormat(StringFormatFlags.DirectionRightToLeft));                            
                        }
                    }
                    currY += 20;
                }
                #endregion

                #region BODY - ANNUAL INCOME AND EXPENSES
                for (int iRow = 0; iRow <= 8; iRow++)
                {
                    _gfx.DrawLine(_blackPen, new Point(40, currY), new Point(1050, currY));

                    if (iRow == 0)
                        _gfx.DrawString(GetLabel(iRow, 4), _arial10B, _blackBrush, new Point(42, currY));
                    else
                        _gfx.DrawString(GetLabel(iRow, 4), _arial10, _blackBrush, new Point(42, currY));

                    if (iRow < 8)
                    {
                        _gfx.DrawLine(_blackPen, new Point(40, currY), new Point(40, currY + 20));
                        _gfx.DrawLine(_blackPen, new Point(250, currY), new Point(250, currY + 20));
                        _gfx.DrawLine(_blackPen, new Point(1050, currY), new Point(1050, currY + 20));

                        for (int jCol = 0; jCol < nrOfYears; jCol++)
                        {
                            _gfx.DrawLine(_blackPen, new Point(250 + (jCol + 1) * 80, currY), new Point(250 + (jCol + 1) * 80, currY + 20));

                            if (0 == iRow)
                                _gfx.DrawString("Year " + (jCol + 1).ToString(), _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "Year " + (jCol + 1).ToString(), _arial10, new Rectangle(250 + (jCol) * 80, currY, 80, 20)));
                            else if (1 == iRow)
                                _gfx.DrawString(FormatStringForOutput(_gsiAndOtherIncome[jCol]), _arial10, _blackBrush, new Point(250 + (jCol + 1) * 80, currY + 2), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                            else if (2 == iRow)
                                _gfx.DrawString(FormatStringForOutput(_vacancyCreditLossPerYears[jCol]), _arial10, _blackBrush, new Point(250 + (jCol + 1) * 80, currY + 2), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                            else if (3 == iRow)
                                _gfx.DrawString(FormatStringForOutput(_operatingExpensesPerYears[jCol]), _arial10, _blackBrush, new Point(250 + (jCol + 1) * 80, currY + 2), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                            else if (4 == iRow)
                                _gfx.DrawString(FormatStringForOutput(_debtService[jCol]), _arial10, _blackBrush, new Point(250 + (jCol + 1) * 80, currY + 2), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                            else if (5 == iRow)
                                _gfx.DrawString(FormatStringForOutput(_incomeTax[jCol]), _arial10, _blackBrush, new Point(250 + (jCol + 1) * 80, currY + 2), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                            else if (6 == iRow)
                                _gfx.DrawString(FormatStringForOutput(_netIncomeProfitLoss[jCol]), _arial10, _blackBrush, new Point(250 + (jCol + 1) * 80, currY + 2), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                            else if (7 == iRow)
                                _gfx.DrawString(FormatStringForOutput(_netIncomeProfitLossCumulative[jCol]), _arial10, _blackBrush, new Point(250 + (jCol + 1) * 80, currY + 2), new StringFormat(StringFormatFlags.DirectionRightToLeft));                           
                        }
                    }
                    currY += 20;
                }
                #endregion

                _gfx.DrawString("Page 2/2", _arial8I, _blackBrush, new PointF(1000, 800));
            }
        }

        public void GenerateTaxesAndDeduction(PrintPageEventArgs e, MultiyearAnalysisObject maObj, ExpensesObject expObj, LoansObject lObj, int nrOfYears)
        {
            _maObj = maObj;
            _expObj = expObj;
            _lObj = lObj;
            _e = e;
            _gfx = e.Graphics;
            _paperWidth = e.PageSettings.PaperSize.Height;

            _cfg = new CashFlowGenerator();
            _cfg.InitGenerator(maObj, expObj, lObj, nrOfYears);
            _cfg.GenerateValues(nrOfYears);

            GenerateValues(nrOfYears);

            int currY = 60;

            if (0 == _pageCnt)
            {

                #region HEADER
                _gfx.DrawString(ReportNameForTaxesAndDeductions, base._arial22, base._blackBrush, base.GetOrigin(_gfx, ePosition.Center, ReportName, base._arial22, _paperWidth, 20));

                _gfx.DrawString("Initial Income and", _arial10B, _blackBrush, new PointF(40, currY));
                _gfx.DrawString("Expense Factors", _arial10B, _blackBrush, new PointF(40, currY + 15));
                _gfx.DrawString("Annual Changes", _arial10B, _blackBrush, new PointF(280, currY));                
                _gfx.DrawString("Loan Information", _arial10B, _blackBrush, new PointF(480, currY));
                _gfx.DrawString("Tax Information", _arial10B, _blackBrush, new PointF(700, currY));                               

                _gfx.DrawLine(_blackPen, new Point(40, currY + 30), new Point(1050, currY + 30));

                currY += 40;
                _gfx.DrawString("Initial GSI: " + _maObj.GsiAnnual, _arial10, _blackBrush, new PointF(40, currY));
                _gfx.DrawString("%Income: " + _maObj.AnnualIncomeChange, _arial10, _blackBrush, new PointF(280, currY));                
                if (null != _lObj && null != _lObj.LoansProperty && null != _lObj.LoansProperty[0])
                    _gfx.DrawString("1st: " + _lObj.LoansProperty[0].ToString(), _arial10, _blackBrush, new PointF(480, currY));
                _gfx.DrawString("Fed. Income Rate: " + _maObj.FederalRate, _arial10, _blackBrush, new PointF(700, currY));                

                currY += 20;                
                _gfx.DrawString("Init. %Vacancy: " + _maObj.VacancyCreditLossPerc, _arial10, _blackBrush, new PointF(40, currY));
                _gfx.DrawString("%Vacancy: " + _maObj.AnnualVacancyChange, _arial10, _blackBrush, new PointF(280, currY));
                if (null != _lObj && null != _lObj.LoansProperty && null != _lObj.LoansProperty[1])
                    _gfx.DrawString("2nd: " + _lObj.LoansProperty[1].ToString(), _arial10, _blackBrush, new PointF(480, currY));
                _gfx.DrawString("State Income Rate: " + _maObj.StateRate, _arial10, _blackBrush, new PointF(700, currY));
                

                currY += 20;                
                _gfx.DrawString("Init. Other Income: " + _maObj.OtherIncome, _arial10, _blackBrush, new PointF(40, currY));
                _gfx.DrawString("%Inflation: " + _maObj.AnnualInflationChange, _arial10, _blackBrush, new PointF(280, currY));
                if (null != _lObj && null != _lObj.LoansProperty && null != _lObj.LoansProperty[2])
                    _gfx.DrawString("3rd: " + _lObj.LoansProperty[2].ToString(), _arial10, _blackBrush, new PointF(480, currY));
                _gfx.DrawString("Depreciable Value: " + _maObj.BuildingValue, _arial10, _blackBrush, new PointF(700, currY));

                currY += 20;                
                _gfx.DrawString("Init. Oper. Expenses: " + _maObj.OperatingExpenses, _arial10, _blackBrush, new PointF(40, currY));
                _gfx.DrawString("%Oper. Expenses: " + _maObj.AnnualOperatingExpensesChange, _arial10, _blackBrush, new PointF(280, currY));
                _gfx.DrawString("Depreciable Years: " + _maObj.DepreciableYears, _arial10, _blackBrush, new PointF(700, currY));
                

                currY += 45;
                _gfx.DrawString("CAPITAL ADDITIONS (IMPROVEMENTS)", _arial12, _blackBrush, new PointF(20, currY));

                //TABLE
                currY += 25;
                int startY = currY;
                _gfx.DrawLine(_blackPen, new Point(40, currY), new Point(940, currY));

                _gfx.DrawString(string.Empty, _arial10, _blackBrush, new Point(40, currY));
                _gfx.DrawString("Year 1", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "Year 1", _arial10, new Rectangle(140, currY, 80, 20)));
                _gfx.DrawString("Year 2", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "Year 2", _arial10, new Rectangle(220, currY, 80, 20)));
                _gfx.DrawString("Year 3", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "Year 3", _arial10, new Rectangle(300, currY, 80, 20)));
                _gfx.DrawString("Year 4", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "Year 4", _arial10, new Rectangle(380, currY, 80, 20)));
                _gfx.DrawString("Year 5", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "Year 5", _arial10, new Rectangle(460, currY, 80, 20)));
                _gfx.DrawString("Year 6", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "Year 6", _arial10, new Rectangle(540, currY, 80, 20)));
                _gfx.DrawString("Year 7", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "Year 7", _arial10, new Rectangle(620, currY, 80, 20)));
                _gfx.DrawString("Year 8", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "Year 8", _arial10, new Rectangle(700, currY, 80, 20)));
                _gfx.DrawString("Year 9", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "Year 9", _arial10, new Rectangle(780, currY, 80, 20)));
                _gfx.DrawString("Year 10", _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "Year 10", _arial10, new Rectangle(860, currY, 80, 20)));

                currY += 20;
                _gfx.DrawLine(_blackPen, new Point(40, currY), new Point(940, currY));
                _gfx.DrawString("Cost:", _arial10, _blackBrush, new Point(90, currY));

                _gfx.DrawString("$" + expObj.CapitalAdditionsProperty.yearsProperty[0]._costProperty, _arial10, _blackBrush, new Point(220, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                _gfx.DrawString("$" + expObj.CapitalAdditionsProperty.yearsProperty[1]._costProperty, _arial10, _blackBrush, new Point(300, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                _gfx.DrawString("$" + expObj.CapitalAdditionsProperty.yearsProperty[2]._costProperty, _arial10, _blackBrush, new Point(380, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                _gfx.DrawString("$" + expObj.CapitalAdditionsProperty.yearsProperty[3]._costProperty, _arial10, _blackBrush, new Point(460, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                _gfx.DrawString("$" + expObj.CapitalAdditionsProperty.yearsProperty[4]._costProperty, _arial10, _blackBrush, new Point(540, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                _gfx.DrawString("$" + expObj.CapitalAdditionsProperty.yearsProperty[5]._costProperty, _arial10, _blackBrush, new Point(620, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                _gfx.DrawString("$" + expObj.CapitalAdditionsProperty.yearsProperty[6]._costProperty, _arial10, _blackBrush, new Point(700, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                _gfx.DrawString("$" + expObj.CapitalAdditionsProperty.yearsProperty[7]._costProperty, _arial10, _blackBrush, new Point(780, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                _gfx.DrawString("$" + expObj.CapitalAdditionsProperty.yearsProperty[8]._costProperty, _arial10, _blackBrush, new Point(860, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                _gfx.DrawString("$" + expObj.CapitalAdditionsProperty.yearsProperty[9]._costProperty, _arial10, _blackBrush, new Point(940, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));

                currY += 20;
                _gfx.DrawLine(_blackPen, new Point(40, currY), new Point(940, currY));
                _gfx.DrawString("Type:", _arial10, _blackBrush, new Point(90, currY));

                _gfx.DrawString(expObj.CapitalAdditionsProperty.yearsProperty[0]._typeProperty, _arial10, _blackBrush, new Point(220, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                _gfx.DrawString(expObj.CapitalAdditionsProperty.yearsProperty[1]._typeProperty, _arial10, _blackBrush, new Point(300, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                _gfx.DrawString(expObj.CapitalAdditionsProperty.yearsProperty[2]._typeProperty, _arial10, _blackBrush, new Point(380, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                _gfx.DrawString(expObj.CapitalAdditionsProperty.yearsProperty[3]._typeProperty, _arial10, _blackBrush, new Point(460, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                _gfx.DrawString(expObj.CapitalAdditionsProperty.yearsProperty[4]._typeProperty, _arial10, _blackBrush, new Point(540, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                _gfx.DrawString(expObj.CapitalAdditionsProperty.yearsProperty[5]._typeProperty, _arial10, _blackBrush, new Point(620, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                _gfx.DrawString(expObj.CapitalAdditionsProperty.yearsProperty[6]._typeProperty, _arial10, _blackBrush, new Point(700, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                _gfx.DrawString(expObj.CapitalAdditionsProperty.yearsProperty[7]._typeProperty, _arial10, _blackBrush, new Point(780, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                _gfx.DrawString(expObj.CapitalAdditionsProperty.yearsProperty[8]._typeProperty, _arial10, _blackBrush, new Point(860, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                _gfx.DrawString(expObj.CapitalAdditionsProperty.yearsProperty[9]._typeProperty, _arial10, _blackBrush, new Point(940, currY), new StringFormat(StringFormatFlags.DirectionRightToLeft));

                currY += 20;
                _gfx.DrawLine(_blackPen, new Point(40, currY), new Point(940, currY));

                //DRAW VERTICAL LINES
                _gfx.DrawLine(_blackPen, new Point(40, startY), new Point(40, currY));
                _gfx.DrawLine(_blackPen, new Point(140, startY), new Point(140, currY));
                _gfx.DrawLine(_blackPen, new Point(220, startY), new Point(220, currY));
                _gfx.DrawLine(_blackPen, new Point(300, startY), new Point(300, currY));
                _gfx.DrawLine(_blackPen, new Point(380, startY), new Point(380, currY));
                _gfx.DrawLine(_blackPen, new Point(460, startY), new Point(460, currY));
                _gfx.DrawLine(_blackPen, new Point(540, startY), new Point(540, currY));
                _gfx.DrawLine(_blackPen, new Point(620, startY), new Point(620, currY));
                _gfx.DrawLine(_blackPen, new Point(700, startY), new Point(700, currY));
                _gfx.DrawLine(_blackPen, new Point(780, startY), new Point(780, currY));
                _gfx.DrawLine(_blackPen, new Point(860, startY), new Point(860, currY));
                _gfx.DrawLine(_blackPen, new Point(940, startY), new Point(940, currY));

                currY += 25;
                #endregion

                #region BODY - SUMMARY
                for (int iRow = 0; iRow <= 7; iRow++)
                {
                    _gfx.DrawLine(_blackPen, new Point(40, currY), new Point(1050, currY));

                    if (iRow == 0)
                        _gfx.DrawString(GetLabelForTaxesAndDeductions(iRow, 0), _arial10B, _blackBrush, new Point(42, currY));
                    else
                        _gfx.DrawString(GetLabelForTaxesAndDeductions(iRow, 0), _arial10, _blackBrush, new Point(42, currY));

                    if (iRow < 7)
                    {
                        _gfx.DrawLine(_blackPen, new Point(40, currY), new Point(40, currY + 20));
                        _gfx.DrawLine(_blackPen, new Point(250, currY), new Point(250, currY + 20));
                        _gfx.DrawLine(_blackPen, new Point(1050, currY), new Point(1050, currY + 20));

                        for (int jCol = 0; jCol < nrOfYears; jCol++)
                        {
                            _gfx.DrawLine(_blackPen, new Point(250 + (jCol + 1) * 80, currY), new Point(250 + (jCol + 1) * 80, currY + 20));

                            if (0 == iRow)
                                _gfx.DrawString("Year " + (jCol + 1).ToString(), _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "Year " + (jCol + 1).ToString(), _arial10, new Rectangle(250 + (jCol) * 80, currY, 80, 20)));
                            else if (1 == iRow)
                                _gfx.DrawString(FormatStringForOutput(_netOperatingIncomePerYears[jCol]), _arial10, _blackBrush, new Point(250 + (jCol + 1) * 80, currY + 2), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                            else if (2 == iRow)
                                _gfx.DrawString(FormatStringForOutput(_totalDeductions[jCol]), _arial10, _blackBrush, new Point(250 + (jCol + 1) * 80, currY + 2), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                            else if (3 == iRow)
                                _gfx.DrawString(FormatStringForOutput(_taxableIncome[jCol]), _arial10, _blackBrush, new Point(250 + (jCol + 1) * 80, currY + 2), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                            else if (4 == iRow)
                                _gfx.DrawString(FormatStringForOutput(_totalTax[jCol]), _arial10, _blackBrush, new Point(250 + (jCol + 1) * 80, currY + 2), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                            else if (5 == iRow)
                                _gfx.DrawString(FormatStringForOutput(_cashFlowPreTax[jCol]), _arial10, _blackBrush, new Point(250 + (jCol + 1) * 80, currY + 2), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                            else if (6 == iRow)
                                _gfx.DrawString(FormatStringForOutput(_cashFlowAfterTax[jCol]), _arial10, _blackBrush, new Point(250 + (jCol + 1) * 80, currY + 2), new StringFormat(StringFormatFlags.DirectionRightToLeft));                            
                        }
                    }
                    currY += 20;
                }
                #endregion

                #region BODY - INCOME DETAILS
                for (int iRow = 0; iRow <= 7; iRow++)
                {
                    _gfx.DrawLine(_blackPen, new Point(40, currY), new Point(1050, currY));

                    if (iRow == 0)
                        _gfx.DrawString(GetLabelForTaxesAndDeductions(iRow, 1), _arial10B, _blackBrush, new Point(42, currY));
                    else
                        _gfx.DrawString(GetLabelForTaxesAndDeductions(iRow, 1), _arial10, _blackBrush, new Point(42, currY));

                    if (iRow < 7)
                    {
                        _gfx.DrawLine(_blackPen, new Point(40, currY), new Point(40, currY + 20));
                        _gfx.DrawLine(_blackPen, new Point(250, currY), new Point(250, currY + 20));
                        _gfx.DrawLine(_blackPen, new Point(1050, currY), new Point(1050, currY + 20));

                        for (int jCol = 0; jCol < nrOfYears; jCol++)
                        {
                            _gfx.DrawLine(_blackPen, new Point(250 + (jCol + 1) * 80, currY), new Point(250 + (jCol + 1) * 80, currY + 20));

                            if (0 == iRow)
                                _gfx.DrawString("Year " + (jCol + 1).ToString(), _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "Year " + (jCol + 1).ToString(), _arial10, new Rectangle(250 + (jCol) * 80, currY, 80, 20)));
                            else if (1 == iRow)
                                _gfx.DrawString(FormatStringForOutput(_rentalIncome[jCol]), _arial10, _blackBrush, new Point(250 + (jCol + 1) * 80, currY + 2), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                            else if (2 == iRow)
                                _gfx.DrawString(FormatStringForOutput(_vacancyCreditLossPerYears[jCol]), _arial10, _blackBrush, new Point(250 + (jCol + 1) * 80, currY + 2), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                            else if (3 == iRow)
                                _gfx.DrawString(FormatStringForOutput(_otherIncomePerYears[jCol]), _arial10, _blackBrush, new Point(250 + (jCol + 1) * 80, currY + 2), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                            else if (4 == iRow)
                                _gfx.DrawString(FormatStringForOutput(_grossOperatingIncomePerYears[jCol]), _arial10, _blackBrush, new Point(250 + (jCol + 1) * 80, currY + 2), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                            else if (5 == iRow)
                                _gfx.DrawString(FormatStringForOutput(_operatingExpensesPerYears[jCol]), _arial10, _blackBrush, new Point(250 + (jCol + 1) * 80, currY + 2), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                            else if (6 == iRow)
                                _gfx.DrawString(FormatStringForOutput(_netOperatingIncomePerYears[jCol]), _arial10, _blackBrush, new Point(250 + (jCol + 1) * 80, currY + 2), new StringFormat(StringFormatFlags.DirectionRightToLeft));                           
                        }
                    }
                    currY += 20;
                }
                #endregion


                _gfx.DrawString("Disclaimer: Income, Tax and Sale related calculations and projections are only approximations based on the user's input and a possible interpretation of the tax code.  ", _arial8I, _blackBrush, new PointF(40, 760));
                _gfx.DrawString("Consult your CPA and attorney for actual taxation related to your properties.  ", _arial8I, _blackBrush, new PointF(40, 780));                
                _gfx.DrawString("Page 1/2", _arial8I, _blackBrush, new PointF(1000, 800));
                
                _pageCnt++;
                e.HasMorePages = true;
                return;
            }
            else
            {
                #region BODY - OPERATING EXPENSES
                for (int iRow = 0; iRow <= 2; iRow++)
                {
                    _gfx.DrawLine(_blackPen, new Point(40, currY), new Point(1050, currY));

                    if (iRow == 0)
                        _gfx.DrawString(GetLabelForTaxesAndDeductions(iRow, 2), _arial10B, _blackBrush, new Point(42, currY));
                    else
                        _gfx.DrawString(GetLabelForTaxesAndDeductions(iRow, 2), _arial10, _blackBrush, new Point(42, currY));

                    if (iRow < 2)
                    {
                        _gfx.DrawLine(_blackPen, new Point(40, currY), new Point(40, currY + 20));
                        _gfx.DrawLine(_blackPen, new Point(250, currY), new Point(250, currY + 20));
                        _gfx.DrawLine(_blackPen, new Point(1050, currY), new Point(1050, currY + 20));

                        for (int jCol = 0; jCol < nrOfYears; jCol++)
                        {
                            _gfx.DrawLine(_blackPen, new Point(250 + (jCol + 1) * 80, currY), new Point(250 + (jCol + 1) * 80, currY + 20));

                            if (0 == iRow)
                                _gfx.DrawString("Year " + (jCol + 1).ToString(), _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "Year " + (jCol + 1).ToString(), _arial10, new Rectangle(250 + (jCol) * 80, currY, 80, 20)));
                            else if (1 == iRow)
                                _gfx.DrawString(FormatStringForOutput(_operatingExpensesPerYears[jCol]), _arial10, _blackBrush, new Point(250 + (jCol + 1) * 80, currY + 2), new StringFormat(StringFormatFlags.DirectionRightToLeft));                           
                        }
                    }
                    currY += 20;
                }
                #endregion

                #region BODY - DEPRECIATION
                for (int iRow = 0; iRow <= 4; iRow++)
                {
                    _gfx.DrawLine(_blackPen, new Point(40, currY), new Point(1050, currY));

                    if (iRow == 0)
                        _gfx.DrawString(GetLabelForTaxesAndDeductions(iRow, 3), _arial10B, _blackBrush, new Point(42, currY));
                    else
                        _gfx.DrawString(GetLabelForTaxesAndDeductions(iRow, 3), _arial10, _blackBrush, new Point(42, currY));

                    if (iRow < 4)
                    {
                        _gfx.DrawLine(_blackPen, new Point(40, currY), new Point(40, currY + 20));
                        _gfx.DrawLine(_blackPen, new Point(250, currY), new Point(250, currY + 20));
                        _gfx.DrawLine(_blackPen, new Point(1050, currY), new Point(1050, currY + 20));

                        for (int jCol = 0; jCol < nrOfYears; jCol++)
                        {
                            _gfx.DrawLine(_blackPen, new Point(250 + (jCol + 1) * 80, currY), new Point(250 + (jCol + 1) * 80, currY + 20));

                            if (0 == iRow)
                                _gfx.DrawString("Year " + (jCol + 1).ToString(), _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "Year " + (jCol + 1).ToString(), _arial10, new Rectangle(250 + (jCol) * 80, currY, 80, 20)));
                            else if (1 == iRow)
                                _gfx.DrawString(FormatStringForOutput(_buildingDepreciation[jCol]), _arial10, _blackBrush, new Point(250 + (jCol + 1) * 80, currY + 2), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                            else if (2 == iRow)
                                _gfx.DrawString(FormatStringForOutput(_capitalImprovementsDepreciationCumulative[jCol]), _arial10, _blackBrush, new Point(250 + (jCol + 1) * 80, currY + 2), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                            else if (3 == iRow)
                                _gfx.DrawString(FormatStringForOutput(_totalDepreciation[jCol]), _arial10, _blackBrush, new Point(250 + (jCol + 1) * 80, currY + 2), new StringFormat(StringFormatFlags.DirectionRightToLeft));                            
                        }
                    }
                    currY += 20;
                }
                #endregion

                #region BODY - LOANS
                for (int iRow = 0; iRow <= 5; iRow++)
                {
                    _gfx.DrawLine(_blackPen, new Point(40, currY), new Point(1050, currY));

                    if (iRow == 0)
                        _gfx.DrawString(GetLabelForTaxesAndDeductions(iRow, 4), _arial10B, _blackBrush, new Point(42, currY));
                    else
                        _gfx.DrawString(GetLabelForTaxesAndDeductions(iRow, 4), _arial10, _blackBrush, new Point(42, currY));

                    if (iRow < 5)
                    {
                        _gfx.DrawLine(_blackPen, new Point(40, currY), new Point(40, currY + 20));
                        _gfx.DrawLine(_blackPen, new Point(250, currY), new Point(250, currY + 20));
                        _gfx.DrawLine(_blackPen, new Point(1050, currY), new Point(1050, currY + 20));

                        for (int jCol = 0; jCol < nrOfYears; jCol++)
                        {
                            _gfx.DrawLine(_blackPen, new Point(250 + (jCol + 1) * 80, currY), new Point(250 + (jCol + 1) * 80, currY + 20));

                            if (0 == iRow)
                                _gfx.DrawString("Year " + (jCol + 1).ToString(), _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "Year " + (jCol + 1).ToString(), _arial10, new Rectangle(250 + (jCol) * 80, currY, 80, 20)));
                            else if (1 == iRow)
                                _gfx.DrawString(FormatStringForOutput(_ijsForFirstLoan[jCol]), _arial10, _blackBrush, new Point(250 + (jCol + 1) * 80, currY + 2), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                            else if (2 == iRow)
                                _gfx.DrawString(FormatStringForOutput(_ijsForSecondLoan[jCol]), _arial10, _blackBrush, new Point(250 + (jCol + 1) * 80, currY + 2), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                            else if (3 == iRow)
                                _gfx.DrawString(FormatStringForOutput(_ijsForThirdLoan[jCol]), _arial10, _blackBrush, new Point(250 + (jCol + 1) * 80, currY + 2), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                            else if (4 == iRow)
                                _gfx.DrawString(FormatStringForOutput(_totalInterestDeduction[jCol]), _arial10, _blackBrush, new Point(250 + (jCol + 1) * 80, currY + 2), new StringFormat(StringFormatFlags.DirectionRightToLeft));                            
                        }
                    }
                    currY += 20;
                }
                #endregion

                #region BODY - TAXABLE INCOME
                for (int iRow = 0; iRow <= 2; iRow++)
                {
                    _gfx.DrawLine(_blackPen, new Point(40, currY), new Point(1050, currY));

                    if (iRow == 0)
                        _gfx.DrawString(GetLabelForTaxesAndDeductions(iRow, 5), _arial10B, _blackBrush, new Point(42, currY));
                    else
                        _gfx.DrawString(GetLabelForTaxesAndDeductions(iRow, 5), _arial10, _blackBrush, new Point(42, currY));

                    if (iRow < 2)
                    {
                        _gfx.DrawLine(_blackPen, new Point(40, currY), new Point(40, currY + 20));
                        _gfx.DrawLine(_blackPen, new Point(250, currY), new Point(250, currY + 20));
                        _gfx.DrawLine(_blackPen, new Point(1050, currY), new Point(1050, currY + 20));

                        for (int jCol = 0; jCol < nrOfYears; jCol++)
                        {
                            _gfx.DrawLine(_blackPen, new Point(250 + (jCol + 1) * 80, currY), new Point(250 + (jCol + 1) * 80, currY + 20));

                            if (0 == iRow)
                                _gfx.DrawString("Year " + (jCol + 1).ToString(), _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "Year " + (jCol + 1).ToString(), _arial10, new Rectangle(250 + (jCol) * 80, currY, 80, 20)));
                            else if (1 == iRow)
                                _gfx.DrawString(FormatStringForOutput(_taxableIncome[jCol]), _arial10, _blackBrush, new Point(250 + (jCol + 1) * 80, currY + 2), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                        }
                    }
                    currY += 20;
                }
                #endregion

                #region BODY - INCOME TAX
                for (int iRow = 0; iRow <= 4; iRow++)
                {
                    _gfx.DrawLine(_blackPen, new Point(40, currY), new Point(1050, currY));

                    if (iRow == 0)
                        _gfx.DrawString(GetLabelForTaxesAndDeductions(iRow, 6), _arial10B, _blackBrush, new Point(42, currY));
                    else
                        _gfx.DrawString(GetLabelForTaxesAndDeductions(iRow, 6), _arial10, _blackBrush, new Point(42, currY));

                    if (iRow < 4)
                    {
                        _gfx.DrawLine(_blackPen, new Point(40, currY), new Point(40, currY + 20));
                        _gfx.DrawLine(_blackPen, new Point(250, currY), new Point(250, currY + 20));
                        _gfx.DrawLine(_blackPen, new Point(1050, currY), new Point(1050, currY + 20));

                        for (int jCol = 0; jCol < nrOfYears; jCol++)
                        {
                            _gfx.DrawLine(_blackPen, new Point(250 + (jCol + 1) * 80, currY), new Point(250 + (jCol + 1) * 80, currY + 20));

                            if (0 == iRow)
                                _gfx.DrawString("Year " + (jCol + 1).ToString(), _arial10, _blackBrush, GetStringPosInACell(_gfx, ePosition.Center, "Year " + (jCol + 1).ToString(), _arial10, new Rectangle(250 + (jCol) * 80, currY, 80, 20)));
                            else if (1 == iRow)
                                _gfx.DrawString(FormatStringForOutput(_stateTax[jCol]), _arial10, _blackBrush, new Point(250 + (jCol + 1) * 80, currY + 2), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                            else if (2 == iRow)
                                _gfx.DrawString(FormatStringForOutput(_federalTax[jCol]), _arial10, _blackBrush, new Point(250 + (jCol + 1) * 80, currY + 2), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                            else if (3 == iRow)
                                _gfx.DrawString(FormatStringForOutput(_totalTax[jCol]), _arial10, _blackBrush, new Point(250 + (jCol + 1) * 80, currY + 2), new StringFormat(StringFormatFlags.DirectionRightToLeft));                            
                        }
                    }
                    currY += 20;
                }
                #endregion

                _gfx.DrawString("Disclaimer: Income, Tax and Sale related calculations and projections are only approximations based on the user's input and a possible interpretation of the tax code.  ", _arial8I, _blackBrush, new PointF(40, 760));
                _gfx.DrawString("Consult your CPA and attorney for actual taxation related to your properties.  ", _arial8I, _blackBrush, new PointF(40, 780));
                _gfx.DrawString("Page 2/2", _arial8I, _blackBrush, new PointF(1000, 800));
            }

        }

        #region INIT VALUES
        List<decimal> InitGsiValuesPerYears(int years)
        {
            List<decimal> gsiVals = new List<decimal>();
            for (int i = 0; i < years; i++)
            {
                if (0 == i)
                {
                    gsiVals.Add(Util.GetDecimalFromString(_maObj.GsiAnnual));
                }
                else
                {
                    gsiVals.Add(gsiVals[i - 1] * (1 + Util.GetDecimalFromString(_maObj.IncomeChangePercs[i]) / 100));
                }
            }
            return gsiVals;
        }

        List<decimal> InitVacancyValuesPerYears(int years, List<decimal> gsis)
        {
            List<decimal> vals = new List<decimal>();
            for (int i = 0; i < years; i++)
            {
                decimal yVacPerc = Util.GetDecimalFromString(_maObj.VacancyPercs[i]) / 100;
                decimal yGSI = gsis[i];
                vals.Add(yVacPerc * yGSI);
            }
            return vals;
        }

        List<decimal> InitOtherIncomePerYears(int years)
        {
            List<decimal> vals = new List<decimal>();

            decimal inflationRate = Util.GetDecimalFromString(_maObj.AnnualInflationChange) / 100;
            decimal initialOtherIncome = Util.GetDecimalFromString(_maObj.OtherIncome);

            for (int i = 0; i < years; i++)
            {
                if (0 == i)
                {
                    vals.Add(initialOtherIncome);
                }
                else
                {
                    vals.Add(vals[i - 1] * (1 + inflationRate));
                }
            }
            return vals;
        }

        List<decimal> InitOperatingExpensesPerYears(int years, List<decimal> gsis)
        {
            List<decimal> vals = new List<decimal>();

            for (int i = 0; i < years; i++)
            {
                if (0 == i)
                {
                    vals.Add(Util.GetDecimalFromString(_maObj.OperatingExpenses));
                }
                else
                {
                    vals.Add(Util.GetDecimalFromString(_maObj.OperatingExpensesPercs[i]) / 100 * gsis[i]);
                }
            }
            return vals;
        }

        List<decimal> InitGrossOperatingIncomePerYears(int years, List<decimal> gsis, List<decimal> vacs, List<decimal> otherIncomes)
        {
            List<decimal> vals = new List<decimal>();

            for (int i = 0; i < years; i++)
            {
                vals.Add(gsis[i] - vacs[i] + otherIncomes[i]);
            }
            return vals;
        }

        List<decimal> InitNetOperatingIncomePerYears(int years, List<decimal> gois, List<decimal> oes)
        {
            List<decimal> vals = new List<decimal>();

            for (int i = 0; i < years; i++)
            {
                vals.Add(gois[i] - oes[i]);
            }
            return vals;
        }

        List<decimal> InitDebtServicePerYears(int years)
        {
            List<decimal> vals = new List<decimal>();

            for (int i = 0; i < years; i++)
            {
                vals.Add(_lObj.TotalAnnualDebtService);
            }
            return vals;
        }        

        List<double> InitCashFlowBeforeTaxesPerYears(int years, List<decimal> nois, List<decimal> debtServices, List<decimal> capitalImps)
        {
            List<double> vals = new List<double>();

            for (int i = 0; i < years; i++)
            {
                vals.Add((double)(nois[i] - debtServices[i]) - (double)capitalImps[i]);
            }
            return vals;
        }

        //SUMMARY
        List<decimal> InitSalePrice(int nrOfYears, List<decimal> noi)
        {
            List<decimal> res = new List<decimal>();

            for (int i = 0; i < nrOfYears; i++)
            {
                res.Add(noi[i] / (Util.GetDecimalFromString(_maObj.SaleCapRate) / 100));
            }

            return res;
        }

        List<double> InitNetSaleProfitLoss(int nrOfYears, List<decimal> salePrice, List<double> totalExpenses, List<decimal> totalTaxLiability)
        {
            List<double> res = new List<double>();

            for (int i = 0; i < nrOfYears; i++)
            {
                res.Add((double)salePrice[i] - totalExpenses[i] - (double)totalTaxLiability[i]);
            }

            return res;
        }

        List<double> InitCashFlowAfterTax(int nrOfYears, List<double> cashFlowPreTax, List<double> totalTax)
        {
            List<double> res = new List<double>();

            for (int i = 0; i < nrOfYears; i++)
            {
                res.Add(cashFlowPreTax[i] - totalTax[i]);
            }

            return res;
        }

        //DETAILS
        List<decimal> InitCapitalImprovementsAnnual(int nrOfYears)
        {
            List<decimal> res = new List<decimal>();

            for (int i = 0; i < nrOfYears; i++)
            {
                res.Add(Util.GetDecimalFromString(_expObj.CapitalAdditionsProperty.yearsProperty[i]._costProperty));
            }

            return res;
        }

        List<decimal> InitCapitalImprovementsCumuative(int nrOfYears, List<decimal> capitalImprovementsAnnual)
        {
            List<decimal> res = new List<decimal>();
            decimal sum = 0;

            for (int i = 0; i < nrOfYears; i++)
            {
                sum += capitalImprovementsAnnual[i];
                res.Add(sum);
            }

            return res;
        }

        List<decimal> InitDepreciationAnnual(int nrOfYears, List<decimal> capitalImprovementsCumulative)
        {
            List<decimal> res = new List<decimal>();

            for (int i = 0; i < nrOfYears; i++)
            {
                res.Add((Util.GetDecimalFromString(_maObj.BuildingValue) + capitalImprovementsCumulative[i]) / Util.GetDecimalFromString(_maObj.DepreciableYears));
            }

            return res;
        }

        List<decimal> InitDepreciationCumulative(int nrOfYears, List<decimal> depreciationAnnual)
        {
            List<decimal> res = new List<decimal>();
            decimal sum = 0;

            for (int i = 0; i < nrOfYears; i++)
            {
                sum += depreciationAnnual[i];
                res.Add(sum);
            }

            return res;
        }

        List<decimal> InitAdjustedTaxBasis(int nrOfYears, List<decimal> capitalImprovementsCumulative, List<decimal> depreciationCumulative)
        {
            List<decimal> res = new List<decimal>();

            for (int i = 0; i < nrOfYears; i++)
            {
                res.Add(_lObj.PurchasePriceValue + capitalImprovementsCumulative[i] - depreciationCumulative[i]);
            }

            return res;
        }

        //EXPENSES
        List<decimal> InitDownPayment(int nrOfYears)
        {
            List<decimal> res = new List<decimal>();

            for (int i = 0; i < nrOfYears; i++)
            {
                res.Add(Util.GetDecimalFromString(_maObj.DownPayement));
            }

            return res;
        }

        List<decimal> InitPurchaseClosingCosts(int nrOfYears)
        {
            List<decimal> res = new List<decimal>();

            for (int i = 0; i < nrOfYears; i++)
            {
                res.Add(Util.GetDecimalFromString(_maObj.Points) + Util.GetDecimalFromString(_maObj.OtherAcquisitionCosts));
            }

            return res;
        }
        
        void InitPrincipalAndInterestPaymentsPerYears(int years, int iLoanCnt, out List<double> pjs, out List<double> ijs)
        {
            pjs = new List<double>();
            ijs = new List<double>();

            //INIT VALUES
            for (int iYrCnt = 0; iYrCnt < years; iYrCnt++)
            {
                pjs.Add(0);
                ijs.Add(0);
            }

            if (null != _lObj && null != _lObj.LoansProperty && 0 < _lObj.LoansProperty.Length && _lObj.LoansProperty.Length >= iLoanCnt)
            {
                //for (int iLoanCnt = 0; iLoanCnt < _lObj.LoansProperty.Length; iLoanCnt++)
                {
                    double P = (double)_lObj.LoansProperty[iLoanCnt].AmountValue;
                    double i = (double)((_lObj.LoansProperty[iLoanCnt].RatePercValue / 100) / 12);
                    double N = (double)_lObj.LoansProperty[iLoanCnt].AmortTermValue * 12;

                    double pj = 0;
                    double ipmt = 0;

                    if (null != _lObj.LoansProperty[iLoanCnt].LoanTypeProperty && string.Empty != _lObj.LoansProperty[iLoanCnt].LoanTypeProperty)
                    {
                        if ("Fixed" == _lObj.LoansProperty[iLoanCnt].LoanTypeProperty)
                        {
                            double cpj = 0;
                            double cipmt = 0;

                            for (int j = 1; j <= years * 12; j++)
                            {
                                try { pj = Microsoft.VisualBasic.Financial.PPmt(i, j, N, P, 0, Microsoft.VisualBasic.DueDate.EndOfPeriod); }
                                catch { pj = 0; }

                                try { ipmt = Microsoft.VisualBasic.Financial.IPmt(i, j, N, P, 0, Microsoft.VisualBasic.DueDate.EndOfPeriod); }
                                catch { ipmt = 0; }

                                cpj += Math.Abs(pj);
                                cipmt += Math.Abs(ipmt);

                                if (0 == ((j /*+ 1*/) % 12))
                                {
                                    pjs[((j + 1) / 12) - 1] += cpj;
                                    ijs[((j + 1) / 12) - 1] += cipmt;

                                    cpj = 0;
                                    cipmt = 0;
                                }
                            }
                        }
                        else
                        {
                            for (int j = 1; j <= years * 12; j++)
                            {
                                if (0 == ((j + 1) % 12))
                                {
                                    pjs[((j + 1) / 12) - 1] += 0; //P;
                                    ijs[((j + 1) / 12) - 1] += P * i * 12;
                                }
                            }
                        }
                    }
                }
            }
        }

        List<double> InitLoanBalance(int years, int iLoanCnt, List<double> pjs)
        {
            List<double> res = new List<double>();

            double P = 0;
            
            for (int i = 0; i < years; i++)
            {
                if (0 == i)
                {
                    try { P = (double)_lObj.LoansProperty[iLoanCnt].AmountValue; }
                    catch { P = 0; }
                }
                else
                    P = res[i - 1];

                res.Add(P - pjs[i]);
            }

            return res;  
        }

        List<decimal> InitPropertySaleCosts(int nrOfYears, List<decimal> salePrice)
        {
            List<decimal> res = new List<decimal>();
            
            for (int i = 0; i < nrOfYears; i++)
            {
                res.Add(salePrice[i] * Util.GetDecimalFromString(_maObj.SaleCosts) / 100);
            }

            return res;
        }

        List<double> InitTotalExpenses(int nrOfYears, List<decimal> downPaymnets, List<decimal> purchaseClosingCosts, List<decimal> capitalImprovements, List<double> firstLoanBalance, List<double> secondLoanBalance, List<double> thirdLoanBalance, List<decimal> propertySaleCosts)
        {
            List<double> res = new List<double>();

            for (int i = 0; i < nrOfYears; i++)
            {
                double tempRes = (double)downPaymnets[i] + (double)purchaseClosingCosts[i] + (double)capitalImprovements[i] + (double)firstLoanBalance[i] + secondLoanBalance[i] + thirdLoanBalance[i] + (double)propertySaleCosts[i];
                res.Add(tempRes);
            }

            return res;
        }

        List<decimal> InitTotalExchangeExpenses(int nrOfYears, List<decimal> purchaseClosingCosts, List<decimal> propertySaleCosts)
        {
            List<decimal> res = new List<decimal>();

            for (int i = 0; i < nrOfYears; i++)
            {
                res.Add(purchaseClosingCosts[i] + propertySaleCosts[i]);
            }

            return res;
        }

        //TAXES
        List<decimal> InitTaxableGain(int nrOfYears, List<decimal> salePrice, List<decimal> adjustableTaxBasis, List<decimal> totalExchangeExpenses)
        {
            List<decimal> res = new List<decimal>();

            for (int i = 0; i < nrOfYears; i++)
            {
                res.Add(salePrice[i] - adjustableTaxBasis[i] - totalExchangeExpenses[i]);
            }

            return res;
        }

        List<decimal> InitDepreciationRecoveryTaxes(int nrOfYears, List<decimal> depreciation) 
        {
            List<decimal> res = new List<decimal>();

            for (int i = 0; i < nrOfYears; i++)
            {
                res.Add(depreciation[i] * Util.GetDecimalFromString(_maObj.FederalRate) / 100);
            }

            return res;
        }

        List<decimal> InitCapitalGainsTaxes(int nrOfYears, List<decimal> taxableGain)
        {
            List<decimal> res = new List<decimal>();

            for (int i = 0; i < nrOfYears; i++)
            {
                res.Add(taxableGain[i] * Util.GetDecimalFromString(_maObj.CapitalGainsRate) / 100);
            }

            return res;
        }

        List<decimal> InitTotalTaxLiabilities(int nrOfYears, List<decimal> depreciationRecoveryTax, List<decimal> capitalGainsTax)
        {
            List<decimal> res = new List<decimal>();

            for (int i = 0; i < nrOfYears; i++)
            {
                res.Add(depreciationRecoveryTax[i] + capitalGainsTax[i]);
            }

            return res;
        }

        //ANNUAL INCOME AND EXPENSES
        List<decimal> InitGsiAndOtherIncome(int nrOfYears, List<decimal> gsi, List<decimal> otherIncome)
        {
            List<decimal> res = new List<decimal>();

            for (int i = 0; i < nrOfYears; i++)
            {
                res.Add(gsi[i] + otherIncome[i]);
            }

            return res;
        }

        List<double> InitIncomeTaxes(int nrOfYears, List<double> totalTaxCostPerYears)
        {
            List<double> res = new List<double>();

            for (int i = 0; i < nrOfYears; i++)
            {
                res.Add(totalTaxCostPerYears[i]);
            }

            return res;
        }

        List<double> InitNetIncomeProfitLoss(int nrOfYears, List<decimal> noi, List<decimal> debtService, List<double> incomeTax)
        {
            List<double> res = new List<double>();

            for (int i = 0; i < nrOfYears; i++)
            {
                res.Add((double)noi[i] - (double)debtService[i] - incomeTax[i]);
            }

            return res;
        }

        List<double> InitNetIncomeProfitLossCumulative(int nrOfYears, List<double> netIncomeProfitLoss)
        {
            List<double> res = new List<double>();
            double sum = 0;

            for (int i = 0; i < nrOfYears; i++)
            {
                sum += netIncomeProfitLoss[i];
                res.Add(sum);
            }

            return res;
        }

        List<double> InitNetProfitLoss(int nrOfYears, List<double> netSaleProfitLoss, List<double> netIncomeProfitLoss)
        {
            List<double> res = new List<double>();

            for (int i = 0; i < nrOfYears; i++)
            {
                res.Add(netSaleProfitLoss[i] + (double)netIncomeProfitLoss[i]);
            }

            return res;
        }

        //--- TAXES AND DEDUCTIONS ---
        
        //SUMMARY
        List<double> InitTotalDeductions(int nrOfYears, List<decimal> depreciation, List<double> interests)
        {
            List<double> res = new List<double>();

            for (int i = 0; i < nrOfYears; i++)
            {
                res.Add((double)depreciation[i] + interests[i]);
            }

            return res;
        }

        

        //INCOME DETAILS
        List<decimal> InitRentalIncome(int nrOfYears, List<decimal> gsi, string[] incomeChange)
        {
            List<decimal> res = new List<decimal>();                        

            for (int i = 0; i < nrOfYears; i++)
            {
                if (0 == i)
                    res.Add(gsi[0]);
                else
                {
                    decimal incomeChg = 1;
                    if (null != incomeChange && incomeChange.Length > i)
                        incomeChg = Util.GetDecimalFromString(incomeChange[i]);

                    res.Add(res[i - 1] * (1 + (incomeChg / 100)));
                }                
            }

            return res;
        }

        //LOANS
        List<double> InitTotalInterestDeduction(int nrOfYears, List<double> firstLoanInterest, List<double> secondLoanInterest, List<double> thirdLoanInterest)
        {
            List<double> res = new List<double>();

            for (int i = 0; i < nrOfYears; i++)
            {
                double tempRes = 0;
                if (null != firstLoanInterest && firstLoanInterest.Count > i)
                    tempRes += firstLoanInterest[i];

                if (null != secondLoanInterest && secondLoanInterest.Count > i)
                    tempRes += secondLoanInterest[i];

                if (null != thirdLoanInterest && thirdLoanInterest.Count > i)
                    tempRes += thirdLoanInterest[i];
                    
                res.Add(tempRes);
            }

            return res;
        }
        
        //DEPRECIATION
        List<decimal> CalculateBuildingDepreciation(int nrOfYears, decimal depreciableValue, decimal nrOfDepreciableYears)
        {
            List<decimal> res = new List<decimal>();

            for (int i = 0; i < nrOfYears; i++)
            {
                res.Add(depreciableValue / nrOfDepreciableYears);
            }

            return res;
        }

        //CAPITAL IMPROVEMENTS DEPRECIATION
        List<decimal> CalculateCumulativaCapitalImprovementsDepreciation(int nrOfYears, List<decimal> cumCapitalImprovements, decimal nrOfDepreciableYears)
        {
            List<decimal> res = new List<decimal>();

            for (int i = 0; i < nrOfYears; i++)
            {
                res.Add(cumCapitalImprovements[i] / nrOfDepreciableYears);
            }

            return res;
        }

        //TOTAL DEPRECIATION
        List<decimal> CalculateTotalDepreciation(int nrOfYears, List<decimal> buildingDepreciation, List<decimal> cumulativeCapitalImprovementsDepreciation)
        {
            List<decimal> res = new List<decimal>();

            for (int i = 0; i < nrOfYears; i++)
            {
                res.Add(buildingDepreciation[i] + cumulativeCapitalImprovementsDepreciation[i]);
            }

            return res;
        }

        //TAXABLE INCOME
        List<double> CalculateTaxableIncome(int nrOfYears, List<decimal> nois, List<decimal> depreciationAnnual, List<double> totalInterestDeduction)
        {
            List<double> res = new List<double>();

            for (int i = 0; i < nrOfYears; i++)
            {
                res.Add((double)nois[i] - (double)depreciationAnnual[i] - totalInterestDeduction[i]);
            }

            return res;
        }

        //INCOME TAX
        List<double> CalculateStateTax(int nrOfYears, List<double> taxableIncome, decimal stateRate)
        {
            List<double> res = new List<double>();

            for (int i = 0; i < nrOfYears; i++)
            {
                res.Add(taxableIncome[i] * (double)stateRate / 100);
            }

            return res;
        }

        List<double> CalculateFederalTax(int nrOfYears, List<double> taxableIncome, decimal federalRate)
        {
            List<double> res = new List<double>();

            for (int i = 0; i < nrOfYears; i++)
            {
                res.Add(taxableIncome[i] * (double)federalRate / 100);
            }

            return res;
        }

        List<double> CalculateTotalTax(int nrOfYears, List<double> stateTax, List<double> federalTax)
        {
            List<double> res = new List<double>();

            for (int i = 0; i < nrOfYears; i++)
            {
                res.Add(stateTax[i] + federalTax[i]);
            }

            return res;
        }
        
        #endregion

        #region ROW LABELS
        string GetLabel(int iRow, int region)
        {
            switch (region)
            {
                case 0:
                    switch (iRow)
                    {
                        case 0:
                            return "SUMMARY";
                        case 1:
                            return "Sale Price";
                        case 2:
                            return "Adjusted Tax Basis";
                        case 3:
                            return "Taxable Gain";
                        case 4:
                            return "Total Tax Liability";
                        case 5:
                            return "Total Expenses";
                        case 6:
                            return "Net Sale Profit/Loss";
                        case 7:
                            return "Net Income Profit/Loss (Cum.)";
                        case 8:
                            return "Net Profit/Loss";                

                        default:
                            return string.Empty;
                    }                    

                case 1:
                    switch (iRow)
                    {
                        case 0:
                            return "TAX BASIS DETAILS";
                        case 1:
                            return "Purchase Price";
                        case 2:
                            return "Capital Improvements (Annual)";
                        case 3:
                            return "Capital Improvements (Cum.)";
                        case 4:
                            return "Depreciation";
                        case 5:
                            return "Depreciation (Cumulative)";
                        case 6:
                            return "Adjusted Tax Basis";                        

                        default:
                            return string.Empty;
                    }

                case 2:
                    switch (iRow)
                    {
                        case 0:
                            return "EXPENSES DETAILS";
                        case 1:
                            return "Down Payment";
                        case 2:
                            return "Purchase Closing Costs";
                        case 3:
                            return "Capital Improvements (Cum.)";
                        case 4:
                            return "1st Loan Balance";
                        case 5:
                            return "2nd Loan Balance";
                        case 6:
                            return "3rd Loan Balance";
                        case 7:
                            return "Property Sale Cost";
                        case 8:
                            return "Total Expenses";
                        case 9:
                            return "Total Exchange Expenses";

                        default:
                            return string.Empty;
                    }

                case 3:
                    switch (iRow)
                    {
                        case 0:
                            return "TAXES DETAILS";
                        case 1:
                            return "Taxable Gain";
                        case 2:
                            return "Depreciation Recovery Tax";
                        case 3:
                            return "Capital Gains Tax";
                        case 4:
                            return "Total Tax Liability";                        

                        default:
                            return string.Empty;
                    }

                case 4:
                    switch (iRow)
                    {
                        case 0:
                            return "ANNUAL INCOME&EXPENSES";
                        case 1:
                            return "GSI and Other Income";
                        case 2:
                            return "Vacancy Loss";
                        case 3:
                            return "Operating Expenses";
                        case 4:
                            return "Debt Service";
                        case 5:
                            return "Income Tax";
                        case 6:
                            return "Net Income Profit/Loss";
                        case 7:
                            return "Net Income Profit/Loss (Cum.)";

                        default:
                            return string.Empty;
                    }

                default:
                    return string.Empty;
            }
        }
        #endregion

        #region ROW LABELS FOR TAXES AND DEDUCTIONS
        string GetLabelForTaxesAndDeductions(int iRow, int region)
        {
            switch (region)
            {
                case 0:
                    switch (iRow)
                    {
                        case 0:
                            return "SUMMARY";
                        case 1:
                            return "Net Operating Income";
                        case 2:
                            return "Total Deductions";
                        case 3:
                            return "Taxable Income";
                        case 4:
                            return "Total Tax";
                        case 5:
                            return "Cash Flow (pre-tax)";
                        case 6:
                            return "Cash Flow (after-tax)";                       
                        default:
                            return string.Empty;
                    }

                case 1:
                    switch (iRow)
                    {
                        case 0:
                            return "INCOME DETAILS";
                        case 1:
                            return "Rental Income";
                        case 2:
                            return "Vacancy/Credit Loss";
                        case 3:
                            return "Other Income";
                        case 4:
                            return "Gross Operating Income";
                        case 5:
                            return "Operating Expenses";
                        case 6:
                            return "Net Operating Income";

                        default:
                            return string.Empty;
                    }

                case 2:
                    switch (iRow)
                    {
                        case 0:
                            return "OPERATING EXPENSES";
                        case 1:
                            return "Annual";
                        
                        default:
                            return string.Empty;
                    }

                case 3:
                    switch (iRow)
                    {
                        case 0:
                            return "DEPRECIATION";
                        case 1:
                            return "Building";
                        case 2:
                            return "Capial Improvements";
                        case 3:
                            return "Total Depreciation";
                        
                        default:
                            return string.Empty;
                    }

                case 4:
                    switch (iRow)
                    {
                        case 0:
                            return "LOANS";
                        case 1:
                            return "1st Loan Interest";
                        case 2:
                            return "2nd Loan Interest";
                        case 3:
                            return "3rd Loan Interest";
                        case 4:
                            return "Total Interest Deduction";
                        
                        default:
                            return string.Empty;
                    }

                case 5:
                    switch (iRow)
                    {
                        case 0:
                            return "TAXABLE INCOME";
                        case 1:
                            return "Value";                        

                        default:
                            return string.Empty;
                    }

                case 6:
                    switch (iRow)
                    {
                        case 0:
                            return "INCOME TAX";
                        case 1:
                            return "State Tax";
                        case 2:
                            return "Federal Tax";
                        case 3:
                            return "Total Tax";                        

                        default:
                            return string.Empty;
                    }


                default:
                    return string.Empty;
            }
        }
        #endregion

        #region FORMAT STRING FOR OUTPUT
        string FormatStringForOutput(int val)
        {
            if (val >= 0)
                return val.ToString(Math.Abs(val) < FormatterLimit ? CurrFormatter : CurrFormatterEx);
            else
                return Math.Abs(val).ToString(Math.Abs(val) < FormatterLimit ? CurrFormatter : CurrFormatterEx) + "-";
        }

        string FormatStringForOutput(decimal val)
        {
            if (val >= 0)
                return val.ToString(Math.Abs(val) < FormatterLimit ? CurrFormatter : CurrFormatterEx);
            else
                return Math.Abs(val).ToString(Math.Abs(val) < FormatterLimit ? CurrFormatter : CurrFormatterEx) + "-";
        }

        string FormatStringForOutput(double val)
        {
            if (val >= 0)
                return val.ToString(Math.Abs(val) < FormatterLimit ? CurrFormatter : CurrFormatterEx);
            else
                return Math.Abs(val).ToString(Math.Abs(val) < FormatterLimit ? CurrFormatter : CurrFormatterEx) + "-";
        }
        #endregion
    }
}
