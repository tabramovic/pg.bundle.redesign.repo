using System;
using System.Threading;
using System.Text;

namespace Core
{
	/// <summary>
	/// Summary description for Random.
	/// 
	/// Pseudo-random numbers are chosen with equal probability from a 
	/// finite set of numbers. The chosen numbers are not completely random 
	/// because a definite mathematical algorithm is used to select them, 
	/// but they are sufficiently random for practical purposes.
	/// 
	/// The random number generation starts from a seed value. If the same seed
	///  is used repeatedly, the same series of numbers is generated. One way to 
	///  produce different sequences is to make the seed value time-dependent, 
	///  thereby producing a different series with each new instance of Random.
	/// To improve performance, create one Random to generate many random numbers 
	/// over time, instead of repeatedly creating a new Random to generate one 
	/// random number.
	/// </summary>
	public 
		class 
		Random
		: System.Random
	{
		public 
			Random
			(
			)
			: base()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		public
			char
			NextAlpha
			(
			)
		{
			Thread.Sleep(10);
			int int_rnd = this.Next(26);

			char chr = (char) (65 + int_rnd);

			return chr;
		}

		public
			int
			NextNumeric
			(
			)
		{
			Thread.Sleep(10);
			int int_rnd = this.Next(10);

			return int_rnd;
		}

		public
			char
			NextAlphaNumeric
			(
			)
		{
			Thread.Sleep(10);
			int int_rnd = this.Next(2);
			char ch;

			if (0 == int_rnd)
			{
				ch = NextAlpha();
			}
			else
			{
				ch = (char) (48 + (char) NextNumeric());
			}

			return ch;
		}

		public
			string
			NextString
			(
			int length
			)
		{
			StringBuilder strbldr = new StringBuilder(null, length);

			for (int i = 1; i <= length; i++)
			{
				strbldr.Append(NextAlpha());
			}

			return strbldr.ToString();
		}

	} // class
} // namespace
