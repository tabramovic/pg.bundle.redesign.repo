using System;
using System.IO;
using System.Text;


namespace ClassLibrary.Core
{
	/// <summary>
	/// Summary description for UnhandledExceptionHandlers.
	/// </summary>
	public 
		class 
		UnhandledExceptionHandlers
	{
		private
			string
			str_filename
			;

		int
			tabs
			= 0
			;

		/// <summary>
		/// CTR default
		/// </summary>
		public
			UnhandledExceptionHandlers
			(
			)
		{
		}

		/// <summary>
		/// CTR
		/// </summary>
		/// <param name="filename">Name of the File where exceptions will be dumped</param>
		public
			UnhandledExceptionHandlers
			(
			  string filename
			)
			: this()
		{
			str_filename = filename;
		}

		/// <summary>
		/// Name of the File where exceptions will be dumped
		/// </summary>
		public
			string
			Filename
		{
			set
			{
				str_filename = value;
			}
			get
			{
				return str_filename;
			}
		}

		#region Methods for unhandled exceptions
		/// <summary>
		/// to catch exceptions on all threads, then subscribe to the
		/// UnhandledException event on the current App Domain, as it will fire 
		/// when an unhandled exception occurs on any thread.
		/// 
		/// </summary>
		/// <param name="sender">
		/// </param>
		/// <param name="event_args">
		/// </param>
		public
			void 
			Function2HandleUnhandledAppDomainException
			(
			  object sender 
			, System.UnhandledExceptionEventArgs event_args
			)
		{
			Exception ex = event_args.ExceptionObject as Exception;

			try
			{
				TextWriter txtwrtr = 
					new StreamWriter(str_filename, true, Encoding.UTF8);
				txtwrtr.WriteLine
					(
					DateTime.Now.ToString("yyyy.MM.dd HH:mm:ss")
					+ " " +
					"UnhandledAppDomainException occured"
					);

				Recurse(ex,txtwrtr);

				txtwrtr.Flush();
				txtwrtr.Close();
			}
			catch 
			{
			}
		}
 
		///////////////////////////////////////////////////////////////////////
		/// <summary>
		/// Subscription to the Application.ThreadException event to catch 
		/// exceptions that are not caught lower down in the chain eventually 
		/// filter up to there.
		/// 
		/// only going to catch exceptions that occur on the UI thread though.  
		/// Exceptions that occur on the other thread will not be caught by
		/// </summary>
		/// <param name="sender">
		/// </param>
		/// <param name="event_args">
		/// </param>
		public 
			void 
			Function2HandleUnhandledApplicationThreadException
			(
			  object sender
			, System.Threading.ThreadExceptionEventArgs event_args
			)
		{
			Exception ex = event_args.Exception as Exception;

			try
			{
				TextWriter txtwrtr = 
					new StreamWriter(str_filename, true, Encoding.UTF8);
				txtwrtr.WriteLine
					(
					DateTime.Now.ToString("yyyy.MM.dd HH:mm:ss")
					+ " " +
					"UnhandledThreadException occured"
					);

				Recurse(ex,txtwrtr);

				txtwrtr.Flush();
				txtwrtr.Close();
			}
			catch 
			{
			}
		}
		#endregion Methodes for unhandled exceptions

		public
			void
			Recurse
			(
			  Exception ex
			, TextWriter txtwrtr
			)
		{
			string prefix = "";

			txtwrtr.WriteLine
				(
				prefix + "Message		= " + ex.Message
				+ Environment.NewLine 
				+ prefix + "Source		= " + ex.Source
				+ Environment.NewLine 
				+ prefix + "StackTrace	= " + ex.StackTrace
				+ Environment.NewLine 
				+ prefix + "StackTrace	= "	+ ex.TargetSite
				+ Environment.NewLine 
				);

			if (null != ex.InnerException)
			{
				tabs += 1;
				for (int i = 1; i <= tabs; i++)
				{
					prefix += @"\t";
				}

				Recurse(ex.InnerException,txtwrtr);
			}

			return;
		}

	}
}
