using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;
using System.Reflection;
using System.Xml;

namespace ObjectPresentation
{
	/// <summary>
	/// Summary description for GridEditObjects.
	/// </summary>
	public class PanelSelectObjects : ObjectPresentation.GridEditObjects
	{
		private System.Windows.Forms.Button select;
		private System.Windows.Forms.Button delete;
		private IList selectionList;

		public PanelSelectObjects()
		{
			InitializeComponent();
		}

		public PanelSelectObjects(IList objs, IList selectionList, Type type, string xmlMapping, int depth)
		{
			InitializeComponent();

			this.selectionList = selectionList;
			this.Editable = false;

			_GridEditObjects(objs, type, xmlMapping, xmlMapping, depth);
		}

		public PanelSelectObjects(IList objs, IList selectionList, Type type, string xmlMapping)
		{
			InitializeComponent();

			this.selectionList = selectionList;
			this.Editable = false;

			_GridEditObjects(objs, type, xmlMapping, xmlMapping, 1);
		}

		public PanelSelectObjects(IList objs, IList selectionList, Type type, string xmlMapping, string objectXmlMapping)
		{
			InitializeComponent();

			this.selectionList = selectionList;
			this.Editable = false;

			_GridEditObjects(objs, type, xmlMapping, objectXmlMapping, 1);
		}

		public PanelSelectObjects(IList objs, IList selectionList, Type type, string xmlMapping, string objectXmlMapping, int depth)
		{
			InitializeComponent();

			this.selectionList = selectionList;
			this.Editable = false;

			_GridEditObjects(objs, type, xmlMapping, objectXmlMapping, depth);
		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.select = new System.Windows.Forms.Button();
			this.delete = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// grid
			// 
			this.grid.Name = "grid";
			this.grid.Size = new System.Drawing.Size(688, 400);
			// 
			// label
			// 
			this.label.Name = "label";
			// 
			// select
			// 
			this.select.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.select.Location = new System.Drawing.Point(520, 440);
			this.select.Name = "select";
			this.select.Size = new System.Drawing.Size(72, 23);
			this.select.TabIndex = 1;
			this.select.Text = "&Select";
			this.select.Click += new System.EventHandler(this.select_Click);
			// 
			// delete
			// 
			this.delete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.delete.Location = new System.Drawing.Point(600, 440);
			this.delete.Name = "delete";
			this.delete.TabIndex = 5;
			this.delete.Text = "&Delete";
			this.delete.Click += new System.EventHandler(this.delete_Click);
			// 
			// PanelSelectObjects
			// 
			this.Controls.Add(this.delete);
			this.Controls.Add(this.select);
			this.Name = "PanelSelectObjects";
			this.Size = new System.Drawing.Size(688, 472);
			this.Controls.SetChildIndex(this.grid, 0);
			this.Controls.SetChildIndex(this.select, 0);
			this.Controls.SetChildIndex(this.delete, 0);
			this.Controls.SetChildIndex(this.label, 0);
			this.ResumeLayout(false);

		}
		#endregion

		private void select_Click(object sender, System.EventArgs e)
		{
			this.Editable = false;
			selectCell(selectionList);
		}

		private void delete_Click(object sender, System.EventArgs e)
		{
			deleteCell();
		}
	}
}
