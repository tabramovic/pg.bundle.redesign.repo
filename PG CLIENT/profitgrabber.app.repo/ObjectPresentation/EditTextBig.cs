using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;

namespace ObjectPresentation
{
	/// <summary>
	/// Summary description for EditTextBig.
	/// </summary>
	public class EditTextBig : System.Windows.Forms.UserControl, ObjectPresentation.IPropertyEdit
	{
		protected System.Windows.Forms.TextBox TextBox;
		protected System.Windows.Forms.Label Label;
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected object currentObject;
		protected string currentProperty;

		public EditTextBig()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

			// TODO: Add any initialization after the InitializeComponent call

		}

		public EditTextBig(string description, object obj, string property)
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

			Label.Text = description;

			PropertySet(obj, property);
		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region IPropertyEdit Members


		public bool Editable
		{
			set
			{
				TextBox.Enabled = value;
			}
		}

		virtual public void PropertySet(object obj, string property)
		{		
			currentObject = obj;
			TextBox.Text = (string)obj.GetType().GetProperty(property).GetValue(obj, null);
			currentProperty = property;
		}

		virtual public void PropertySave()
		{
			currentObject.GetType().GetProperty(currentProperty).SetValue(currentObject, TextBox.Text, null);
		}

		virtual public void PropertyRevert()
		{
			TextBox.Text = (string)currentObject.GetType().GetProperty(currentProperty).GetValue(currentObject, null);
		}

		public int DescriptionWidth
		{
			set
			{
//				Label.Width = value;
//				TextBox.Left = Label.Width + 10;
//				this.Width = Label.Width + TextBox.Width + 10;
			}
		}

		public int EditWidth
		{
			set
			{
//				TextBox.Width = value;
//				this.Width = Label.Width + TextBox.Width + 10;
			}
		}

		#endregion


		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.TextBox = new System.Windows.Forms.TextBox();
			this.Label = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// TextBox
			// 
			this.TextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.TextBox.Location = new System.Drawing.Point(24, 32);
			this.TextBox.Multiline = true;
			this.TextBox.Name = "TextBox";
			this.TextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.TextBox.Size = new System.Drawing.Size(520, 352);
			this.TextBox.TabIndex = 0;
			this.TextBox.Text = "";
			// 
			// Label
			// 
			this.Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.Label.Location = new System.Drawing.Point(8, 8);
			this.Label.Name = "Label";
			this.Label.Size = new System.Drawing.Size(168, 21);
			this.Label.TabIndex = 1;
			// 
			// EditTextBig
			// 
			this.Controls.Add(this.Label);
			this.Controls.Add(this.TextBox);
			this.Name = "EditTextBig";
			this.Size = new System.Drawing.Size(544, 408);
			this.ResumeLayout(false);

		}
		#endregion
	}
}
