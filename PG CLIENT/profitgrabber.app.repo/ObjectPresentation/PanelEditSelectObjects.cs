using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;
using System.Reflection;
using System.Xml;

namespace ObjectPresentation
{
	/// <summary>
	/// Summary description for GridEditObjects.
	/// </summary>
	public class PanelEditSelectObjects : ObjectPresentation.GridEditObjects
	{
		private System.Windows.Forms.Button delete;
		private System.Windows.Forms.Button add;
		private System.Windows.Forms.Button select;
		private System.Windows.Forms.Button edit;

		private IList selectionList;

		public PanelEditSelectObjects()
		{
			InitializeComponent();
		}

		//nhibernate constructor, accepts session, and criteria expression
		public PanelEditSelectObjects(NHibernate.ISessionFactory sessionFactory, NHibernate.ISession session, IList selectionList, Type type, NHibernate.Expression.Expression expression, string xmlMapping)
		{
			InitializeComponent();

			this.selectionList = selectionList;
			this.Editable = false;

			_GridEditObjects(sessionFactory, session, type, expression, xmlMapping, xmlMapping, 1);
		}

		//nhibernate constructor, accepts session
		public PanelEditSelectObjects(NHibernate.ISessionFactory sessionFactory, NHibernate.ISession session, IList selectionList, Type type, string xmlMapping)
		{
			InitializeComponent();

			this.selectionList = selectionList;
			this.Editable = false;

			_GridEditObjects(sessionFactory, session, type, null, xmlMapping, xmlMapping, 1);
		}

		public PanelEditSelectObjects(IList objs, IList selectionList, Type type, string xmlMapping, int depth)
		{
			InitializeComponent();

			this.selectionList = selectionList;
			this.Editable = false;

			_GridEditObjects(objs, type, xmlMapping, xmlMapping, depth);
		}

		public PanelEditSelectObjects(IList objs, IList selectionList, Type type, string xmlMapping)
		{
			InitializeComponent();

			this.selectionList = selectionList;
			this.Editable = false;

			_GridEditObjects(objs, type, xmlMapping, xmlMapping, 1);
		}

		public PanelEditSelectObjects(IList objs, IList selectionList, Type type, string xmlMapping, string objectXmlMapping)
		{
			InitializeComponent();

			this.selectionList = selectionList;
			this.Editable = false;

			_GridEditObjects(objs, type, xmlMapping, objectXmlMapping, 1);
		}

		public PanelEditSelectObjects(IList objs, IList selectionList, Type type, string xmlMapping, string objectXmlMapping, int depth)
		{
			InitializeComponent();

			this.selectionList = selectionList;
			this.Editable = false;

			_GridEditObjects(objs, type, xmlMapping, objectXmlMapping, depth);
		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.delete = new System.Windows.Forms.Button();
			this.add = new System.Windows.Forms.Button();
			this.edit = new System.Windows.Forms.Button();
			this.select = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// grid
			// 
			this.grid.Name = "grid";
			this.grid.Size = new System.Drawing.Size(688, 400);
			// 
			// label
			// 
			this.label.Name = "label";
			// 
			// delete
			// 
			this.delete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.delete.Location = new System.Drawing.Point(600, 440);
			this.delete.Name = "delete";
			this.delete.TabIndex = 1;
			this.delete.Text = "&Delete";
			this.delete.Click += new System.EventHandler(this.delete_Click);
			// 
			// add
			// 
			this.add.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.add.Location = new System.Drawing.Point(515, 440);
			this.add.Name = "add";
			this.add.TabIndex = 2;
			this.add.Text = "&Add";
			this.add.Click += new System.EventHandler(this.add_Click);
			// 
			// edit
			// 
			this.edit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.edit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.edit.Location = new System.Drawing.Point(429, 440);
			this.edit.Name = "edit";
			this.edit.TabIndex = 3;
			this.edit.Text = "&Edit";
			this.edit.Click += new System.EventHandler(this.edit_Click);
			// 
			// select
			// 
			this.select.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.select.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.select.Location = new System.Drawing.Point(344, 440);
			this.select.Name = "select";
			this.select.TabIndex = 5;
			this.select.Text = "&Select";
			this.select.Click += new System.EventHandler(this.select_Click);
			// 
			// PanelEditSelectObjects
			// 
			this.Controls.Add(this.select);
			this.Controls.Add(this.edit);
			this.Controls.Add(this.add);
			this.Controls.Add(this.delete);
			this.Name = "PanelEditSelectObjects";
			this.Size = new System.Drawing.Size(688, 472);
			this.Controls.SetChildIndex(this.grid, 0);
			this.Controls.SetChildIndex(this.delete, 0);
			this.Controls.SetChildIndex(this.add, 0);
			this.Controls.SetChildIndex(this.edit, 0);
			this.Controls.SetChildIndex(this.label, 0);
			this.Controls.SetChildIndex(this.select, 0);
			this.ResumeLayout(false);

		}
		#endregion

		private void edit_Click(object sender, System.EventArgs e)
		{
			editCell();
		}

		private void delete_Click(object sender, System.EventArgs e)
		{
			deleteCell();
		}

		private void add_Click(object sender, System.EventArgs e)
		{
			addCell();
		}

		private void select_Click(object sender, System.EventArgs e)
		{
			selectCell(selectionList);
		}
	}
}
