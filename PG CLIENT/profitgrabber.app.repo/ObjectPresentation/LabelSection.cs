using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;

namespace ObjectPresentation
{
	/// <summary>
	/// Summary description for Section.
	/// </summary>
	public class LabelSection : System.Windows.Forms.UserControl, ObjectPresentation.IPropertyEdit
	{
		private System.Windows.Forms.Label Label;
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public LabelSection()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

			// TODO: Add any initialization after the InitializeComponent call

		}

		public LabelSection(string text)
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

			// TODO: Add any initialization after the InitializeComponent call

			Label.Text = text;
		}



		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Label = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// Label
			// 
			this.Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.Label.Location = new System.Drawing.Point(16, 8);
			this.Label.Name = "Label";
			this.Label.Size = new System.Drawing.Size(320, 24);
			this.Label.TabIndex = 0;
			// 
			// LabelSection
			// 
			this.Controls.Add(this.Label);
			this.Name = "LabelSection";
			this.Size = new System.Drawing.Size(304, 40);
			this.ResumeLayout(false);

		}
		#endregion

		public bool Editable
		{
			set
			{
			}
		}

		public int DescriptionWidth
		{
			set
			{
				Label.Width = value;
				this.Width = Label.Width + 10;
			}
		}


		public int EditWidth
		{
			set
			{
			}
		}


		public void PropertySet(object obj, string property)
		{
		}

		public void PropertySave()
		{
		}

		public void PropertyRevert()
		{
		}
	}
}
