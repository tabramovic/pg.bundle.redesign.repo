using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;
using System.Reflection;

namespace ObjectPresentation
{
	/// <summary>
	/// Summary description for ComboObject.
	/// </summary>
	public class ComboObject : System.Windows.Forms.UserControl, ObjectPresentation.IPropertyEdit
	{

		private System.Windows.Forms.Label label;
		private System.Windows.Forms.ComboBox comboBox;
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected object currentObject;
		protected string currentProperty;
		protected string currentPropertyEnlist;
		private ComboObjectItem currentComboObjectItem;
		protected object currentObjectList;

		public ComboObject(string description, object obj, string property, string propertyEnlist, ArrayList objectList)
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

			//if ComboObjectItem passed, deencapsulate it

			if(obj is ComboObjectItem)
			{
				obj = ((ComboObjectItem)obj).Object;
			}

			label.Text = description;

			currentPropertyEnlist = propertyEnlist;			
			currentObject = obj;
			currentProperty = property;
			currentPropertyEnlist = propertyEnlist;
			currentObjectList = objectList;

			object containedObject = obj.GetType().GetProperty(property).GetValue(obj, null);

			comboBox.Items.Clear();

			bool selectObject = false;
			int selectObjectIndex = 0;
			int cnt = 0;
			PropertyInfo propertyInfo = null;


/*			//in no list add contained object in
			if(objectList == null)
			{
				//!!!! za sad stavio ovdje, mozda treba maknut gore
//				if(containedObject == null)
//				{
//					containedObject = "";
//				}
				
				objectList = new ArrayList();
				objectList.Add(containedObject);
			}
*/
			//check if object exists in list
			//this is separated so we could put non existant object first
			foreach(object o in objectList)
			{			
				if(o == containedObject && o != null)
				{
					selectObject = true;
					selectObjectIndex = cnt;
				}
				cnt++;
			}

			cnt=0;

			if(!selectObject)
			{
				ComboObjectItem item = new ComboObjectItem();

				if(containedObject!=null)
				{
					item.Object = containedObject;

					if(propertyEnlist!=null)
						propertyInfo = containedObject.GetType().GetProperty(propertyEnlist);

					if(propertyInfo != null)
						item.Display = (string)propertyInfo.GetValue(containedObject, null);
					else
						item.Display = containedObject.ToString();

					comboBox.Items.Add(item);

					comboBox.SelectedIndex = cnt;
					//				cnt++;
					//				selectObjectIndex++;
				}
			}

			foreach(object o in objectList)
			{			
				/*				if(o == containedObject)
								{
									selectObject = true;
									selectObjectIndex = cnt;
								}
				*/
				if(propertyEnlist != null)
					propertyInfo = o.GetType().GetProperty(propertyEnlist);

				ComboObjectItem item = new ComboObjectItem();
	
				item.Object = o;

				if(propertyInfo != null)
					item.Display = (string)propertyInfo.GetValue(o, null);
				else
					item.Display = o.ToString();
				
				currentComboObjectItem = item;

				comboBox.Items.Add(item);

//				cnt++;

			}

			if(selectObject)
			{
				comboBox.SelectedIndex = selectObjectIndex;
			}
		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label = new System.Windows.Forms.Label();
			this.comboBox = new System.Windows.Forms.ComboBox();
			this.SuspendLayout();
			// 
			// label
			// 
			this.label.Location = new System.Drawing.Point(8, 3);
			this.label.Name = "label";
			this.label.Size = new System.Drawing.Size(168, 32);
			this.label.TabIndex = 0;
			// 
			// comboBox
			// 
			this.comboBox.Location = new System.Drawing.Point(176, 3);
			this.comboBox.Name = "comboBox";
			this.comboBox.Size = new System.Drawing.Size(128, 21);
			this.comboBox.TabIndex = 1;
			this.comboBox.Validating += new System.ComponentModel.CancelEventHandler(this.comboBox_Validating);
			// 
			// ComboObject
			// 
			this.Controls.Add(this.comboBox);
			this.Controls.Add(this.label);
			this.Name = "ComboObject";
			this.Size = new System.Drawing.Size(312, 32);
			this.ResumeLayout(false);

		}
		#endregion


		#region IPropertyEdit Members

		public bool Editable
		{
			set
			{
				comboBox.Enabled = value;
			}
		}

		public void PropertySet(object obj, string property)
		{		
			//not used here
		}

		public void PropertySave()
		{
			if(comboBox.SelectedItem != null)
				currentObject.GetType().GetProperty(currentProperty).SetValue(currentObject, ((ComboObjectItem)comboBox.SelectedItem).Object, null);
		}

		public void PropertyRevert()
		{
			comboBox.SelectedItem = (bool)currentObject.GetType().GetProperty(currentProperty).GetValue(currentComboObjectItem, null);
		}

		public int DescriptionWidth
		{
			set
			{
				label.Width = value;
				comboBox.Left = label.Width + 10;
				this.Width = label.Width + comboBox.Width + 10;
			}
		}

		public int EditWidth
		{
			set
			{
				comboBox.Width = value;
				this.Width = label.Width + comboBox.Width + 10;
			}
		}

		#endregion

		private void comboBox_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if(comboBox.SelectedItem == null && comboBox.Text.Length > 0) e.Cancel = true;
		}
	}

	public class ComboObjectItem : IComparable
	{
		public object Object;
		public string Display;
		public string PropertyName;

		/*			public ComboObjectItem(object obj, string disp)
					{
						Object = obj;
						disp = Display;
					}
		*/
		public override string ToString()
		{
			return Display;
		}

		#region IComparable Members

		public int CompareTo(object obj)
		{
			try
			{
				IComparable o = (IComparable)this.Object;

				return o.CompareTo(((ComboObjectItem)obj).Object);
			}
			catch
			{
				return 0;
			}
		}

		#endregion
	}
}
