using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace ObjectPresentation
{
	/// <summary>
	/// Summary description for FormSelectObject.
	/// </summary>
	public class FormSelectObject : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Button ok;
		private System.Windows.Forms.Button cancel;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
		private ObjectPresentation.GridEditObjects gridEditObjects;

		private object selectedObject;
		private string selectedPropertyName = null;


		public object SelectedObject
		{
			get
			{
				return selectedObject;
			}
		}

		public FormSelectObject()
		{
			InitializeComponent();
		}

		public FormSelectObject(IList objs, Type type, string xmlMapping, int depth)
		{
			InitializeComponent();

			_FormSelectObject(objs, type, xmlMapping, depth);
		}

		public FormSelectObject(IList objs, Type type, string xmlMapping, int depth, string propertyName)
		{
			InitializeComponent();

			selectedPropertyName = propertyName;
			_FormSelectObject(objs, type, xmlMapping, depth);
		}

		public FormSelectObject(IList objs, Type type, string xmlMapping)
		{
			InitializeComponent();

			_FormSelectObject(objs, type, xmlMapping, 1);
		}

		public void _FormSelectObject(IList objs, Type type, string xmlMapping, int depth)
		{

			gridEditObjects = new GridEditObjects(objs, type, xmlMapping, depth, selectedPropertyName);

			gridEditObjects.Editable = false;
			gridEditObjects.doubleClickEventHandler = new EventHandler(ok_Click);

			gridEditObjects.ShowGrid();

			this.Controls.Add(gridEditObjects);

			InitializeComponent();

			gridEditObjects.Location = new Point(0,0);

			gridEditObjects.AutoSizeBounds();

			if(gridEditObjects.Height > (Screen.GetWorkingArea(this).Height*0.8))
			{
				gridEditObjects.Height = (int)(Screen.GetWorkingArea(this).Height*0.8);
			}

			if(gridEditObjects.Width > (Screen.GetWorkingArea(this).Width*0.8))
			{
				gridEditObjects.Width = (int)(Screen.GetWorkingArea(this).Width*0.8);
			}
			
			this.ClientSize = gridEditObjects.Size;
			this.Height+=ok.Height + 10;

			this.CenterToScreen();

			gridEditObjects.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top;

		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.ok = new System.Windows.Forms.Button();
			this.cancel = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// ok
			// 
			this.ok.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.ok.Location = new System.Drawing.Point(532, 392);
			this.ok.Name = "ok";
			this.ok.Size = new System.Drawing.Size(72, 23);
			this.ok.TabIndex = 0;
			this.ok.Text = "&OK";
			this.ok.Click += new System.EventHandler(this.ok_Click);
			// 
			// cancel
			// 
			this.cancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.cancel.Location = new System.Drawing.Point(616, 392);
			this.cancel.Name = "cancel";
			this.cancel.TabIndex = 1;
			this.cancel.Text = "&Cancel";
			this.cancel.Click += new System.EventHandler(this.cancel_Click);
			// 
			// FormSelectObject
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(704, 421);
			this.Controls.Add(this.cancel);
			this.Controls.Add(this.ok);
			this.Name = "FormSelectObject";
			this.ResumeLayout(false);

		}
		#endregion

		private void ok_Click(object sender, System.EventArgs e)
		{
			if(gridEditObjects.SelectedObject != null)
			{
				this.DialogResult = DialogResult.OK;
				this.selectedObject = gridEditObjects.SelectedObject;
				this.Close();
			}
			else
			{
				MessageBox.Show("Row not selected!");
			}
		}

		private void cancel_Click(object sender, System.EventArgs e)
		{
			this.DialogResult = DialogResult.Cancel;
			this.Close();
		}
/*
		public void ShowGrid()
		{
			gridEditObjects.ShowGrid();
		}
*/	}
}
