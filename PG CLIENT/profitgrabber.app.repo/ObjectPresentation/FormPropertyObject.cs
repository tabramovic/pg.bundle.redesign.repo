using System;
using System.Reflection;
using System.Windows.Forms;

namespace ObjectPresentation
{
	/// <summary>
	/// Summary description for FormPropertyObject.
	/// </summary>
	public class FormPropertyObject
	{
		public FormPropertyObject()
		{
		}

		public static void FillForm(Form form, object obj)
		{
			foreach(PropertyInfo propertyInfo in obj.GetType().GetProperties())
			{
				foreach(Control control in form.Controls)
				{
					if(	control.Name.Length >= propertyInfo.Name.Length &&
						propertyInfo.Name == control.Name.Substring(0, propertyInfo.Name.Length))
					{
						if(control is TextBox)
						{
							control.Text = Convert.ToString(propertyInfo.GetValue(obj, null));
						}

						if(control is ComboBox)
						{
							if(propertyInfo.PropertyType == typeof(string))
							{
								((ComboBox)control).Text = Convert.ToString(propertyInfo.GetValue(obj, null));
							}
						}

						if(control is DateTimePicker)
						{
							((DateTimePicker)control).Value = Convert.ToDateTime(propertyInfo.GetValue(obj, null));
						}


						if(control is CheckBox)
						{
							((CheckBox)control).Checked = Convert.ToBoolean(propertyInfo.GetValue(obj, null));
						}

						if(control is RadioButton)
						{
							if(	control.Name.Length > propertyInfo.Name.Length &&
								Convert.ToInt32(control.Name.Substring(propertyInfo.Name.Length, control.Name.Length - propertyInfo.Name.Length)) == Convert.ToInt32(propertyInfo.GetValue(obj, null)))
							{
								((RadioButton)control).Checked = true;
							}
						}
					}
				}
			}
		}

		public static void FillObject(Form form, object obj)
		{
            if (null == obj)
                return;

			foreach(PropertyInfo propertyInfo in obj.GetType().GetProperties())
			{
				foreach(Control control in form.Controls)
				{
					if(	control.Name.Length >= propertyInfo.Name.Length &&
						propertyInfo.Name == control.Name.Substring(0, propertyInfo.Name.Length))
					{
						if(control is TextBox)
						{
							if(propertyInfo.PropertyType == typeof(int))
							{
								propertyInfo.SetValue(obj, Convert.ToInt32(control.Text), null);
							}

							if(propertyInfo.PropertyType == typeof(string))
							{
								propertyInfo.SetValue(obj, control.Text, null);
							}
						}

						if(control is ComboBox)
						{
							if(propertyInfo.PropertyType == typeof(string))
							{
								propertyInfo.SetValue(obj, control.Text, null);
							}
						}

						if(control is DateTimePicker)
						{
							propertyInfo.SetValue(obj, ((DateTimePicker)control).Value, null);
						}

						if(control is CheckBox)
						{
							propertyInfo.SetValue(obj, ((CheckBox)control).Checked, null);
						}

						if(control is RadioButton)
						{
							if(control.Name.Length > propertyInfo.Name.Length)
							{
								if(((RadioButton)control).Checked == true)
								{
									propertyInfo.SetValue(obj, 
										Convert.ToInt32(control.Name.Substring(propertyInfo.Name.Length)),
										null);
								}
							}
						}
					}
				}
			}
		}
	}
}
