using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;

namespace ObjectPresentation
{
	/// <summary>
	/// Summary description for EditBool.
	/// </summary>
	public class EditBool : System.Windows.Forms.UserControl, ObjectPresentation.IPropertyEdit
	{
		private System.Windows.Forms.CheckBox CheckBox;
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected object currentObject;
		protected string currentProperty;

		public EditBool()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

			// TODO: Add any initialization after the InitializeComponent call

		}
		
		public EditBool(string description, object obj, string property)
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

			CheckBox.Text = description;

			PropertySet(obj, property);
		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.CheckBox = new System.Windows.Forms.CheckBox();
			this.SuspendLayout();
			// 
			// CheckBox
			// 
			this.CheckBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.CheckBox.Location = new System.Drawing.Point(8, 0);
			this.CheckBox.Name = "CheckBox";
			this.CheckBox.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
			this.CheckBox.Size = new System.Drawing.Size(184, 32);
			this.CheckBox.TabIndex = 1;
			this.CheckBox.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// EditBool
			// 
			this.Controls.Add(this.CheckBox);
			this.Name = "EditBool";
			this.Size = new System.Drawing.Size(200, 32);
			this.ResumeLayout(false);

		}
		#endregion

		#region IPropertyEdit Members

		public bool Editable
		{
			set
			{
				CheckBox.Enabled = value;
			}
		}

		public void PropertySet(object obj, string property)
		{		
			currentObject = obj;
			CheckBox.Checked = (bool)obj.GetType().GetProperty(property).GetValue(obj, null);
			currentProperty = property;
		}

		public void PropertySave()
		{
			currentObject.GetType().GetProperty(currentProperty).SetValue(currentObject, CheckBox.Checked, null);
		}

		public void PropertyRevert()
		{
			CheckBox.Checked = (bool)currentObject.GetType().GetProperty(currentProperty).GetValue(currentObject, null);
		}

		public int DescriptionWidth
		{
			set
			{
				this.Width = CheckBox.Width;
			}
		}

		public int EditWidth
		{
			set
			{
			}
		}

		#endregion

	}
}
