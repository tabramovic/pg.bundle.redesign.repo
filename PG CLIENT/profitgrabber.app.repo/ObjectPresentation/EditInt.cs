using System;

namespace ObjectPresentation
{
	/// <summary>
	/// Summary description for EditInt.
	/// </summary>
	public class EditInt : ObjectPresentation.EditText
	{

		private void InitializeComponent()
		{
			this.SuspendLayout();
			// 
			// TextBox
			// 
			this.TextBox.Name = "TextBox";
			// 
			// Label
			// 
			this.Label.Name = "Label";
			// 
			// EditInt
			// 
			this.Name = "EditInt";
			this.Load += new System.EventHandler(this.EditInt_Load);
			this.ResumeLayout(false);

		}

		public EditInt(string description, object obj, string property)
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();


			TextBox.Validating += new System.ComponentModel.CancelEventHandler(this.TextBox_Validating);

			Label.Text = description;

			PropertySet(obj, property);
		}

		#region IPropertyEdit Members


		public override void PropertySet(object obj, string property)
		{		
			currentObject = obj;
			TextBox.Text = (string)obj.GetType().GetProperty(property).GetValue(obj, null).ToString();
			currentProperty = property;
		}

		public override  void PropertySave()
		{
			currentObject.GetType().GetProperty(currentProperty).SetValue(currentObject, Convert.ToInt32(TextBox.Text), null);
		}

		public override  void PropertyRevert()
		{
			TextBox.Text = currentObject.GetType().GetProperty(currentProperty).GetValue(currentObject, null).ToString();
		}

		#endregion

		private void TextBox_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		//private void TextBox_Validating(object sender, System.EventArgs e)
		{
			try
			{
				int i = Convert.ToInt32(TextBox.Text);
			}
			catch
			{
				// Cancel the event and select the text to be corrected by the user.
				e.Cancel = true;
				TextBox.Select(0, TextBox.Text.Length);

				// Set the ErrorProvider error with the text to display. 
				//this.errorProvider1.SetError(textBox1, errorMsg);
//				this.errorProvider1.SetError(textBox1, errorMsg);
			}
		}

		private void EditInt_Load(object sender, System.EventArgs e)
		{
		
		}
	}
}
