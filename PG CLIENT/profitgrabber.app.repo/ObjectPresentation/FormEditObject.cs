using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace ObjectPresentation
{
	/// <summary>
	/// Summary description for FormEditObject.
	/// </summary>
	public class FormEditObject : System.Windows.Forms.Form
	{
		private ObjectPresentation.PanelEditObject editObject;
		private string xmlMappingFile;
		private object loadedObject;
		private int initialDepth;
		private System.Windows.Forms.Panel panel;

		public bool EditArray
		{
			set
			{
				editObject.EditArray = value;
			}
			get
			{
				return editObject.EditArray;
			}
		}

		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		/*
		 * 		public EditObject(object obj, string xmlMapping, int depth)
		{
			_EditObject(obj, xmlMapping, depth);
		}

		public EditObject(object obj, string xmlMapping)
		{
			_EditObject(obj, xmlMapping, 1);
		}

		 */

		public void AddArrayList(string name, ArrayList list)
		{
			editObject.AddArrayList(name, list);
		}

		public void SetArrayList(CollectionKeyObject list)
		{
			editObject.SetArrayList(list);
		}

		public FormEditObject(object obj, string xmlMapping, int depth)
		{
			InitializeComponent();

			_FormEditObject(obj, xmlMapping, depth);
		}

		public FormEditObject(object obj, string xmlMapping, bool fullEdit, int depth)
		{
			InitializeComponent();

			_FormEditObject(obj, xmlMapping, fullEdit, depth);
		}

		public FormEditObject(object obj, string xmlMapping)
		{
			InitializeComponent();

			_FormEditObject(obj, xmlMapping, 1);
		}

		public void _FormEditObject(object obj, string xmlMapping, int depth)
		{
			editObject = new PanelEditObject(obj, xmlMapping, depth);

			loadedObject = obj;
			xmlMappingFile = xmlMapping;
			initialDepth = depth;
		}

		public void _FormEditObject(object obj, string xmlMapping, bool fullEdit, int depth)
		{
			editObject = new PanelEditObject(obj, xmlMapping, depth);

			editObject.FullEdit = fullEdit;
			loadedObject = obj;
			xmlMappingFile = xmlMapping;
			initialDepth = depth;
		}

		public void ShowEdit()
		{
//			editObject = new ObjectPresentation.PanelEditObject(loadedObject,xmlMappingFile);
			
//			editObject.Editable = false;
			editObject.ShowEdit();

			this.Text = editObject.ObjectName;
			panel.Location = new Point(5,5);

			if(editObject.Height > (Screen.GetWorkingArea(this).Height*0.8))
			{
				editObject.Height = (int)(Screen.GetWorkingArea(this).Height*0.8);
			}

			panel.Size = editObject.Size;
			panel.Width+=5;
			panel.Height+=5;

			//todo: do this right
			this.Size = new Size(panel.Width + 20,  panel.Height + 80);
			this.CenterToParent();

			panel.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top;
			panel.Controls.Add(editObject);
			editObject.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top;

			Button okButton = new Button();
			okButton.Location = new Point(this.Width - 175, panel.Height + 15);
			okButton.Size = new Size(75, 23);
			okButton.Text = "&OK";
			okButton.Click += new System.EventHandler(this.okButton_Click);
			okButton.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
			this.Controls.Add(okButton);

			Button cancelButton = new Button();
			cancelButton.Location = new Point(this.Width - 90, panel.Height + 15);
			cancelButton.Size = new Size(75, 23);
			cancelButton.Text = "&Cancel";
			cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
			cancelButton.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
			this.Controls.Add(cancelButton);
		}

		private void okButton_Click(object sender, System.EventArgs e)
		{
			editObject.Save();
			this.DialogResult =	DialogResult.OK;
			this.Close();
		}

		private void cancelButton_Click(object sender, System.EventArgs e)
		{
			this.DialogResult =	DialogResult.Cancel;
			this.Close();
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		public void SetPropertiesToValues(CollectionKeyObject objectValues)
		{
			editObject.SetPropertiesToValues(objectValues);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.panel = new System.Windows.Forms.Panel();
			this.SuspendLayout();
			// 
			// panel
			// 
			this.panel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.panel.Location = new System.Drawing.Point(8, 8);
			this.panel.Name = "panel";
			this.panel.Size = new System.Drawing.Size(376, 296);
			this.panel.TabIndex = 0;
			// 
			// FormEditObject
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(392, 341);
			this.Controls.Add(this.panel);
			this.Name = "FormEditObject";
			this.ResumeLayout(false);

		}
		#endregion
	}
}
