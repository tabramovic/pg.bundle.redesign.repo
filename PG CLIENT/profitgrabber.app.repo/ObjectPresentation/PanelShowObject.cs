using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;
using System.Reflection;

namespace ObjectPresentation
{
	/// <summary>
	/// Summary description for PanelShowObject.
	/// </summary>
	public class PanelShowObject : ObjectPresentation.PanelEditObject
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		private ArrayList objectArray = null;

		private object currentObject;
		private string currentProperty;

		private object selectedObject; //object extracted from property
		//private object loadedObject;

		private string xmlMapping = null;

		private int initialDepth;

		private bool editAdd;

/*		public bool EditAdd
		{
			set
			{
				editAdd = value;
			}
			get
			{
				return editAdd;
			}
		}
*/
		public PanelShowObject()
		{
			InitializeComponent();
		}


		public PanelShowObject(object obj, string property, ArrayList array, string xmlMapping, int depth, string propertyName, bool EditAdd)
		{
			this.selectedPropertyName = propertyName;
			InitializeComponent();
			_PanelShowObject(obj, property, array, xmlMapping, depth, EditAdd);
		}

		public PanelShowObject(object obj, string property, ArrayList array, string xmlMapping, int depth, bool EditAdd)
		{
			InitializeComponent();
			_PanelShowObject(obj, property, array, xmlMapping, depth, EditAdd);
		}

		public PanelShowObject(object obj, string property, ArrayList array, string xmlMapping, bool EditAdd)
		{
			InitializeComponent();
			_PanelShowObject(obj, property, array, xmlMapping, 1, EditAdd);
		}

		public PanelShowObject(object obj, string property, string xmlMapping, int depth, bool EditAdd)
		{
			InitializeComponent();
			_PanelShowObject(obj, property, null, xmlMapping, depth, EditAdd);
		}

		public PanelShowObject(object obj, string property, string xmlMapping, bool EditAdd)
		{
			InitializeComponent();
			_PanelShowObject(obj, property, null, xmlMapping, 1, EditAdd);
		}



		public PanelShowObject(object obj, string property, ArrayList array, string xmlMapping, int depth, string propertyName)
		{
			this.selectedPropertyName = propertyName;
			InitializeComponent();
			_PanelShowObject(obj, property, array, xmlMapping, depth, false);
		}

		public PanelShowObject(object obj, string property, ArrayList array, string xmlMapping, int depth)
		{
			InitializeComponent();
			_PanelShowObject(obj, property, array, xmlMapping, depth, false);
		}

		public PanelShowObject(object obj, string property, ArrayList array, string xmlMapping)
		{
			InitializeComponent();
			_PanelShowObject(obj, property, array, xmlMapping, 1, false);
		}

		public PanelShowObject(object obj, string property, string xmlMapping, int depth)
		{
			InitializeComponent();
			_PanelShowObject(obj, property, null, xmlMapping, depth, false);
		}

		public PanelShowObject(object obj, string property, string xmlMapping)
		{
			InitializeComponent();
			_PanelShowObject(obj, property, null, xmlMapping, 1, false);
		}

/*remove
 * 		public PanelShowObject(object obj, string property, bool fullEdit, string xmlMapping)
		{
			this.fullEdit = fullEdit;

			InitializeComponent();
			_PanelShowObject(obj, property, null, xmlMapping, 1, false);
		}
*/
		/*		public PanelShowObject(object obj, Type type, string property, ArrayList array, string xmlMapping, int depth)
		{
			InitializeComponent();
			_PanelShowObject(obj, type, property, array, xmlMapping, depth);
		}

		public PanelShowObject(object obj, Type type, string property, ArrayList array, string xmlMapping)
		{
			InitializeComponent();
			_PanelShowObject(obj, type, property, array, xmlMapping, 1);
		}

		public PanelShowObject(object obj, Type type, string property, string xmlMapping, int depth)
		{
			InitializeComponent();
			_PanelShowObject(obj, type, property, null, xmlMapping, depth);
		}

		public PanelShowObject(object obj, Type type, string property, string xmlMapping)
		{
			InitializeComponent();
			_PanelShowObject(obj, type, property, null, xmlMapping, 1);
		}
*/
		public void _PanelShowObject(object obj, string property, ArrayList array, string xmlMapping, int depth, bool EditAdd)
		{
			currentObject = obj;
			currentProperty = property;
			objectArray = array;
			this.editAdd = EditAdd;

			selectedObject = loadedObject = currentObject.GetType().GetProperty(currentProperty).GetValue(currentObject, null);
		
			initialDepth = depth;
			this.xmlMapping = xmlMapping;

			//_EditObject(obj, xmlMapping, depth);
			
//			_EditObject(selectedObject, type, xmlMapping, depth);
			_EditObject(selectedObject, obj.GetType().GetProperty(property).PropertyType, xmlMapping, depth);

			this.Editable = false;

			drawButton();
		}

		private void drawButton()
		{
			if(objectArray != null)
			{						
				this.SuspendLayout();

				Button selectObject = new Button();

				selectObject.SuspendLayout();

				selectObject.Text = "Select";
				selectObject.Size = new Size(75, 23);
				this.expandHeigth = selectObject.Height;
				selectObject.Left = this.Width - selectObject.Width - 10;
				selectObject.Top = this.Height - selectObject.Height - 10;

				selectObject.Anchor = AnchorStyles.Right | AnchorStyles.Bottom;

				selectObject.Click +=new EventHandler(selectObject_Click);

				this.Controls.Add(selectObject);
				selectObject.ResumeLayout();

				if(editAdd)
				{
					Button addObject = new Button();

					addObject.SuspendLayout();
					addObject.Text = "Add";
					addObject.Size = new Size(75, 23);
					this.expandHeigth = addObject.Height;
					addObject.Left = this.Width - addObject.Width*2 - 10*2;
					addObject.Top = this.Height - addObject.Height - 10;

					addObject.Anchor = AnchorStyles.Right | AnchorStyles.Bottom;

					addObject.Click +=new EventHandler(addObject_Click);
					this.Controls.Add(addObject);
					addObject.ResumeLayout();

					Button editObject = new Button();
					editObject.Text = "Edit";
					editObject.Size = new Size(75, 23);
					this.expandHeigth = editObject.Height;
					editObject.Left = this.Width - editObject.Width*3 - 10*3;
					editObject.Top = this.Height - editObject.Height - 10;

					editObject.Anchor = AnchorStyles.Right | AnchorStyles.Bottom;

					editObject.Click +=new EventHandler(editObject_Click);
					this.Controls.Add(editObject);
					editObject.ResumeLayout();
				}

				this.ResumeLayout();
			}
		}

		protected void selectObject_Click(object sender, System.EventArgs e)
		{
			FormSelectObject formSelectObject = new FormSelectObject(objectArray, loadedObjectType, xmlMapping, initialDepth, selectedPropertyName);
			formSelectObject.ShowDialog();

			if(formSelectObject.SelectedObject != null)
			{
//				this.loadedObject = formSelectObject.SelectedObject;
				this.loadedObject = this.selectedObject = formSelectObject.SelectedObject;
				this.Controls.Clear();
//				_PanelShowObject(cur, objectArray, xmlMapping, initialDepth);
				_EditObject(selectedObject, selectedObject.GetType(), xmlMapping, initialDepth);
				this.Editable = false;
				this.ShowEdit();
				drawButton();
			}
		}

		protected void editObject_Click(object sender, System.EventArgs e)
		{
			ObjectPresentation.FormEditObject formEditObject = new FormEditObject(this.selectedObject, this.xmlMapping, true, this.initialDepth);

			//not actually required here
			formEditObject.SetPropertiesToValues(listSetPropertiesToValues);

			formEditObject.SetArrayList(arrayList);

			formEditObject.ShowEdit();
			formEditObject.ShowDialog();

			this.ShowEdit();
		}

		protected void addObject_Click(object sender, System.EventArgs e)
		{
			object newObject = Activator.CreateInstance(loadedObjectType);

			//postavi sve IListove u objektu na novi ArrayList
			foreach(PropertyInfo pi in newObject.GetType().GetProperties())
			{
				if(pi.PropertyType == typeof(IList))
				{
					pi.SetValue(newObject, new ArrayList(), null);
				}
			}

			ObjectPresentation.FormEditObject formEditObject = new FormEditObject(newObject, this.xmlMapping, true, this.initialDepth);

			formEditObject.SetPropertiesToValues(listSetPropertiesToValues);

			formEditObject.SetArrayList(arrayList);

			formEditObject.ShowEdit();
			formEditObject.ShowDialog();

			if(formEditObject.DialogResult == DialogResult.OK)
			{
//				this.loadedObject = newObject;
				this.loadedObject = this.selectedObject = newObject;
				this.ShowEdit();
			}
		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		override public void PropertySet(object obj, string property)
		{		
			Console.WriteLine("Not implemented");
/*			currentObject = obj;
			loadedObject = obj.GetType().GetProperty(property).GetValue(obj, null);
			currentProperty = property;
*/		}

		override public void PropertySave()
		{
			//selectedObject
			currentObject.GetType().GetProperty(currentProperty).SetValue(currentObject, selectedObject, null);
		}

		override public void PropertyRevert()
		{
			Console.WriteLine("Not implemented");
			//			TextBox.Text = (string)currentObject.GetType().GetProperty(currentProperty).GetValue(currentObject, null);
		}

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			components = new System.ComponentModel.Container();
		}
		#endregion
	}
}
