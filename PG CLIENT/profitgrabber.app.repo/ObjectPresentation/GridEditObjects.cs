using System;

using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;
using System.Reflection;
using System.Xml;

namespace ObjectPresentation
{
	/// <summary>
	/// Summary description for GridEditObjects.
	/// </summary>
	public class GridEditObjects : System.Windows.Forms.UserControl, IPropertyEdit
	{
		public class StoredObject: IComparable
		{
			public object Object;
			public string Property;
			public string SubProperty;

			#region IComparable Members

			public int CompareTo(object obj)
			{
				IComparable o = (IComparable)Object;

				return o.CompareTo(obj);
			}

			#endregion
		}

		private bool editArray;

		public bool EditArray
		{
			set
			{
				editArray = value;
			}
			get
			{
				return editArray;
			}
		}

		private bool readXmlFromResource = true;

		public bool ReadXmlFromResource
		{
			set
			{
				readXmlFromResource = value;
			}
			get
			{
				return readXmlFromResource;
			}
		}

		/// <summary> 
		/// Required designer variable.
		/// </summary>
		protected System.ComponentModel.Container components = null;

		protected XmlTextReader reader;

		protected string xmlMappingFile;
		protected string objectXmlMappingFile;
		protected IList loadedObjects;
		public SourceGrid.Grid grid;
		protected string showObjectName;
		protected int initialDepth;

		protected string selectedPropertyName;

		Type loadedType;


		//todo: skuzi duljinu iz xml-a
		private const int addReflectionListDepth = 4;

		private CollectionKeyObject arrayList = new CollectionKeyObject();

//		protected ArrayList comboArrayList = new ArrayList();
		protected System.Windows.Forms.Label label;
		protected int arrayListCount = 0;
		private bool autoExtendCols = true;

		private bool editable = true;

		private object selectedObject;

		private bool autoSizeColumnAfterEdit = false;

		public System.EventHandler doubleClickEventHandler;

		protected NHibernate.ISessionFactory nhibernateSessionFactory = null;
		protected NHibernate.ISession nhibernateSession = null;

		private CollectionKeyObject intList = new CollectionKeyObject();

		public delegate void SelectionChangedEventHandler(object sender, EventArgs e);
		public event SelectionChangedEventHandler SelectionChanged;

		public void AddIntList(string name, ArrayList list)
		{            
			intList.Add(name, list);
		}

		public void AddArrayList(string name, ArrayList list)
		{
			arrayList.Add(name, list);
		}

		public void SetArrayList(CollectionKeyObject list)
		{
			arrayList = list;
		}

		public object SelectedObject
		{
			get
			{
				return selectedObject;
			}
		}

		public bool Editable
		{
			set
			{
				editable = value;
			}
			get
			{
				return editable;
			}
		}

		public void ShowGrid() 
		{

			if(readXmlFromResource)
			{
				string name = null;
				IList i = System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceNames();
				foreach(object o in i)
				{
					if(o.ToString().EndsWith(xmlMappingFile))
					{
						name = o.ToString();
						break;
					}
				}

				System.IO.Stream stream = System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream(name);
				reader = new XmlTextReader(stream);
			}
			else
			{
				reader = new XmlTextReader(xmlMappingFile);
			}

			while(reader.Read())
			{
				if((reader.Depth == initialDepth && reader.Name == "array" && loadedType.ToString() == reader.GetAttribute("type") && selectedPropertyName == null) || (reader.Depth == initialDepth && reader.Name == "object" && selectedPropertyName != null && selectedPropertyName == reader.GetAttribute("name")))
				{
					parseObject(loadedObjects, reader.Depth);
				}
			}

			grid.AutoSizeAll(15,15);

			AutoExtendCols = autoExtendCols;
			AutoExtendColsDo();

			this.PerformLayout();
		}

		void parseObject(IList in_objs, int depth)
		{
			string objectName = reader.GetAttribute("type");
			string objectDisplay = reader.GetAttribute("display");

			int grid_pos_x = 0;
			int grid_pos_y = 0;

			if(objectDisplay == null) objectDisplay = objectName;

			if(depth == initialDepth) showObjectName = objectDisplay;

			this.label.Text = objectDisplay;

			grid.RemoveRow(0, grid.Rows);
			grid.RemoveColumn(0, grid.Cols);

			grid.Rows = in_objs.Count + 1;
			grid.Cols = 1;
			grid.FixedRows = 1;
			grid.FixedCols = 1;

			grid[0,0] = new SourceGrid.CellHeader();

			int i = 0;

            //sasa quick fix - grid now uses IComparable for default sort 19.11.09
            ArrayList objs = new ArrayList(in_objs);
            try
            {
                objs.Sort();
            }
            catch (Exception exc)
            {
                bool stopHere = true;
            }

            foreach(object obj in objs)
            {
				StoredObject soHeader = new StoredObject();
				soHeader.Object = obj;

				i++;

				grid[i,0] = new SourceGrid.CellHeader();
				grid[i,0].Tag = soHeader;

				if(editable)
					grid[i,0].DoubleClick += new System.EventHandler(this.grid_DoubleClick);

				if(doubleClickEventHandler != null)
					grid[i,0].DoubleClick += new System.EventHandler(doubleClickEventHandler);

				grid[i,0].SelectionChange += new EventHandler(gridCell_SelectionChange);
			}

//			arrayListCount = 0;

			while(reader.Read() && reader.Depth > depth)
			{
				if(reader.Depth == depth + 1 && reader.HasAttributes)
				{
//					bool arrayListCount_do = false;

					//edit in array
					bool show = true;
					string editInArray = reader.GetAttribute("array");
					if(editInArray != null && editInArray == "false") show = false;
//&& editInArray == null || editInArray != "false")

					if((reader.Name == "edit" || reader.Name == "combo" || reader.Name == "intlist") && show)
					{
						string propertyName = reader.GetAttribute("name");
						string propertyDisplay = reader.GetAttribute("display");

						string propertyNoGrid = reader.GetAttribute("nogrid");

						string subProperty = reader.GetAttribute("subproperty");

						if(propertyNoGrid != null && propertyNoGrid == "true") continue;

						//int editWidth;
						//string editWidth_s = reader.GetAttribute("editwidth");
						//if(editWidth_s != null) editWidth = Convert.ToInt32(editWidth_s);

						if(propertyDisplay == null) propertyDisplay = propertyName;

						Type propertyType = null;

						if(propertyName != null)
							propertyType = loadedType.GetProperty(propertyName).PropertyType;

						grid.Cols = ++grid_pos_x + 1;

						grid_pos_y = 0;

						grid[grid_pos_y,grid_pos_x] = new SourceGrid.ColHeader(propertyDisplay);

						foreach(object obj in objs)
						{
							grid_pos_y++;

							StoredObject so = new StoredObject();
						
							if(subProperty == null)
							{
								so.Object = obj;
								so.Property = propertyName;
							}
							else
							{
								//								so.Object = obj.GetType().GetProperty(propertyName).GetValue(obj,null);
								propertyType = obj.GetType().GetProperty(propertyName).PropertyType.GetProperty(subProperty).PropertyType;
								//								so.Property = subProperty;
								so.Object = obj;
								so.Property = propertyName;
								so.SubProperty = subProperty;


/*								if(so.Object == null)
								{
//									so.Object = new Dummy();
//									so.Property = "Dumm";

									//Activator.CreateInstance(obj.GetType());
								}
*/							}

							SourceGrid.Cell sg = null;

							if(reader.Name == "edit")
							{
								if(propertyType == typeof(string))
								{
									sg = new SourceGrid.CellTyped(typeof(string));
								}

								if(propertyType == typeof(int))
								{
									sg = new SourceGrid.CellTyped(typeof(int));
								}

								if(propertyType == typeof(System.Decimal))
								{
									sg = new SourceGrid.CellTyped(typeof(System.Decimal));
								}

								if(propertyType == typeof(bool))
								{
									sg = new SourceGrid.CellCheckBox(false);
									((SourceGrid.CellCheckBox)sg).CheckEnable = editable;
								}

								if(propertyType == typeof(DateTime))
								{
									sg = new SourceGrid.CellTyped(typeof(DateTime));
								}

								if(so.Object != null)
								{
									if(so.SubProperty == null)
									{
										sg.Value = so.Object.GetType().GetProperty(so.Property).GetValue(so.Object, null);
									}
									else
									{
										object o = so.Object.GetType().GetProperty(propertyName).GetValue(so.Object,null);

										if(o != null)
											sg.Value = o.GetType().GetProperty(so.SubProperty).GetValue(o, null);
									}

									sg.Tag = so;
								}

								if(editable)
								{
									sg.EditableMode = SourceGrid.EditableMode.AnyKey | SourceGrid.EditableMode.DoubleClick;
								}
								else
								{
									sg.EditableMode = SourceGrid.EditableMode.None;
								}

								sg.ValueChanged += new EventHandler(gridCell_ValueChanged);
								sg.SelectionChange += new EventHandler(gridCell_SelectionChange);

								if(doubleClickEventHandler != null)
									sg.DoubleClick += new EventHandler(doubleClickEventHandler);

								grid[grid_pos_y,grid_pos_x] = sg;
							}

							if(reader.Name == "intlist")
							{
								sg = new SourceGrid.CellTyped(typeof(string));
								sg.EditableMode = SourceGrid.EditableMode.None;
								sg.Value = ((ArrayList)intList[propertyName])[(int)(so.Object.GetType().GetProperty(so.Property).GetValue(so.Object, null))];
								sg.Tag = so;
								sg.SelectionChange += new EventHandler(gridCell_SelectionChange);

								if(doubleClickEventHandler != null)
									sg.DoubleClick += new EventHandler(doubleClickEventHandler);
								
								grid[grid_pos_y,grid_pos_x] = sg;
							}

							if(reader.Name == "combo")
							{
								string propertyEnlist = reader.GetAttribute("enlist");

								if(editable)
								{
									ArrayList comboList = new ArrayList();

									object containedObject = obj.GetType().GetProperty(propertyName).GetValue(obj, null);

									ObjectPresentation.ComboObjectItem currentComboItem = null;

									foreach(object o in (ArrayList)arrayList[propertyName])
									{
										ObjectPresentation.ComboObjectItem comboItem = new ComboObjectItem();

										comboItem.PropertyName = propertyEnlist;
										comboItem.Object = o;
										comboItem.Display = o.GetType().GetProperty(propertyEnlist).GetValue(o, null).ToString();

										if(o == containedObject) currentComboItem = comboItem;

										comboList.Add(comboItem);
									}

									//add object that don't exist in array
									if(currentComboItem == null && containedObject != null)
									{
										currentComboItem = new ComboObjectItem();
										currentComboItem.PropertyName = propertyEnlist;
										currentComboItem.Object = containedObject;
										currentComboItem.Display = containedObject.GetType().GetProperty(propertyEnlist).GetValue(containedObject, null).ToString();
									}

									if(currentComboItem == null)
									{
										currentComboItem = new ComboObjectItem();
									}

									currentComboItem.PropertyName = propertyEnlist;

									so.Object = obj;
									so.Property = propertyName;

									SourceGrid.CellFormatter.CellFormatterTypeConverter cellFormatter = 
										new SourceGrid.CellFormatter.CellFormatterTypeConverter(typeof(ComboObjectItem),comboList);
								
									SourceGrid.CellTyped sgct =
										new SourceGrid.CellTyped(
										typeof(ComboObjectItem),
										cellFormatter,
										currentComboItem,
										null
										);


									sgct.Tag = so;

									sgct.EditableMode = SourceGrid.EditableMode.AnyKey | SourceGrid.EditableMode.DoubleClick;

									sgct.ValueChanged += new EventHandler(gridCell_ValueChanged);
									sgct.SelectionChange += new EventHandler(gridCell_SelectionChange);

									if(doubleClickEventHandler != null)
										sgct.DoubleClick += new EventHandler(doubleClickEventHandler);
									
									grid[grid_pos_y,grid_pos_x] = sgct;
								}
								else
								{
									//string cellDisplay = so.Object.GetType().GetProperty(propertyEnlist).GetValue(so.Object, null).ToString();
									object cellObject = so.Object.GetType().GetProperty(propertyName).GetValue(so.Object, null);
									
									string cellDisplay;

									if(cellObject != null)
									{
										cellDisplay = cellObject.GetType().GetProperty(propertyEnlist).GetValue(cellObject, null).ToString();
									}
									else
									{
										cellDisplay = "";
									}

									sg = new SourceGrid.CellTyped(typeof(string));

									sg.Value = cellDisplay;
									sg.EditableMode = SourceGrid.EditableMode.None;
									sg.SelectionChange += new EventHandler(gridCell_SelectionChange);
									sg.Tag = so;

									if(doubleClickEventHandler != null)
										sg.DoubleClick += new EventHandler(doubleClickEventHandler);

									grid[grid_pos_y,grid_pos_x] = sg;

								}
							}
						}
					}

/*					if(arrayListCount_do)
					{
						arrayListCount++;
						arrayListCount_do = false;
					}*/
				}
			}
		}

		protected void gridCell_ValueChanged(object sender, System.EventArgs e)
		{
			SourceGrid.Cell cell = (SourceGrid.Cell) sender;
			StoredObject storedObject = (StoredObject)cell.Tag;

			object setValue = null;
		
			if(cell.Value is ComboObjectItem)
			{
				setValue = ((ComboObjectItem)(cell.Value)).Object;
			}
			else
			{
				setValue = cell.Value;
			}

//			storedObject.Object.GetType().GetProperty(storedObject.Property).SetValue(storedObject.Object, setValue, null);
			if(storedObject.SubProperty == null)
			{
//				sg.Value = so.Object.GetType().GetProperty(so.Property).GetValue(so.Object, null);
				storedObject.Object.GetType().GetProperty(storedObject.Property).SetValue(storedObject.Object, setValue, null);
			}
			else
			{
				object o = storedObject.Object.GetType().GetProperty(storedObject.Property).GetValue(storedObject.Object,null);

				if(o != null)
//					sg.Value = o.GetType().GetProperty(so.SubProperty).GetValue(o, null);
					o.GetType().GetProperty(storedObject.SubProperty).SetValue(o, setValue, null);
			}

			if(nhibernateSession != null)
			{
				object obj = storedObject.Object;

				//NHibernate.ISession session = nhibernateSessionFactory.OpenSession();
				NHibernate.ISession session = nhibernateSession;

				NHibernate.ITransaction nhibernateTransaction = session.BeginTransaction();
				//			NHibernate.ITransaction nhibernateTransaction = nhibernateSession.Transaction;

				try
				{
					session.Update(obj);

					nhibernateTransaction.Commit();
					//nhibernateSession.Flush();
				}
				catch(Exception ex)
				{
					MessageBox.Show(ex.ToString());
					nhibernateTransaction.Rollback();
					nhibernateSession.Refresh(obj);
//					cell.Value = storedObject.Object.GetType().GetProperty(storedObject.Property).GetValue(storedObject.Object, null);
				}
				finally
				{
//					session.Close();
				}
				//				nhibernateEdit(storedObject.Object);

				//cell.Value = storedObject.Object.GetType().GetProperty(storedObject.Property).GetValue(storedObject.Object, null);
				//todo: maybe refresh cells
			}

			if(autoSizeColumnAfterEdit)
				grid.AutoSizeColumn(cell.Col, 10);
		}

		public bool AutoSizeColumnAfterEdit
		{
			set
			{
				autoSizeColumnAfterEdit = value;
			}
			get
			{
				return autoSizeColumnAfterEdit;
			}
		}

/*		public void AddComboList(ArrayList list)
		{
			comboArrayList.Add(list);
		}
*/
		public string ObjectName
		{
			get
			{
				return showObjectName;
			}
		}

		//nhibernate constructor, accepts session and expression
		public GridEditObjects(NHibernate.ISessionFactory sessionFactory, NHibernate.ISession session, Type type, NHibernate.Expression.Expression expression, string xmlMapping)
		{
			InitializeComponent();

			_GridEditObjects(sessionFactory, session, type, expression, xmlMapping, xmlMapping, 1);
		}

		//nhibernate constructor, accepts session
		public GridEditObjects(NHibernate.ISessionFactory sessionFactory, NHibernate.ISession session, Type type, string xmlMapping)
		{
			InitializeComponent();

			_GridEditObjects(sessionFactory, session, type, null, xmlMapping, xmlMapping, 1);
		}

		//nhibernate constructor, accepts session and expression
		public GridEditObjects(NHibernate.ISessionFactory sessionFactory, Type type, NHibernate.Expression.Expression expression, string xmlMapping)
		{
			InitializeComponent();

			_GridEditObjects(sessionFactory, type, expression, xmlMapping, xmlMapping, 1);
		}

		//nhibernate constructor, accepts session
		public GridEditObjects(NHibernate.ISessionFactory sessionFactory, Type type, string xmlMapping)
		{
			InitializeComponent();

			_GridEditObjects(sessionFactory, type, null, xmlMapping, xmlMapping, 1);
		}

		public GridEditObjects()
		{
			InitializeComponent();
		}

		public GridEditObjects(IList objs, Type type, string xmlMapping, int depth, string selectedPropertyName)
		{
			InitializeComponent();

			this.selectedPropertyName = selectedPropertyName;
			_GridEditObjects(objs, type, xmlMapping, xmlMapping, depth);
		}

		public GridEditObjects(IList objs, Type type, string xmlMapping)
		{
			InitializeComponent();

			_GridEditObjects(objs, type, xmlMapping, xmlMapping, 1);
		}

		public GridEditObjects(IList objs, Type type, string xmlMapping, string objectXmlMapping)
		{
			InitializeComponent();

			_GridEditObjects(objs, type, xmlMapping, objectXmlMapping, 1);
		}

		public GridEditObjects(IList objs, Type type, string xmlMapping, string objectXmlMapping, int depth)
		{
			InitializeComponent();

			_GridEditObjects(objs, type, xmlMapping, objectXmlMapping, depth);
		}

		public void _GridEditObjects(IList objs, Type type, string gridXmlMapping, string objectXmlMapping, int depth)
		{
			loadedObjects = objs;
			xmlMappingFile = gridXmlMapping;
			this.objectXmlMappingFile = objectXmlMapping;
			loadedType = type;
			initialDepth = depth;
			grid.Selection.SelectionMode = SourceGrid.GridSelectionMode.Row;
		}

		public void _GridEditObjects(NHibernate.ISessionFactory sessionFactory, NHibernate.ISession session, Type type, NHibernate.Expression.Expression expression, string gridXmlMapping, string objectXmlMapping, int depth)
		{
			//			nhibernateSessionFactory = sessionFactory;
			nhibernateSession = session;//nhibernateSessionFactory.OpenSession();
			nhibernateSessionFactory = sessionFactory;

			if(expression == null)
			{
				loadedObjects = (IList)nhibernateSession.CreateCriteria(type).List();
			}
			else
			{
				loadedObjects = (IList)nhibernateSession.CreateCriteria(type).Add(expression).List();
			}

			xmlMappingFile = gridXmlMapping;
			this.objectXmlMappingFile = objectXmlMapping;
			loadedType = type;
			initialDepth = depth;

			grid.Selection.SelectionMode = SourceGrid.GridSelectionMode.Row;
			//			nhibernateAddArrayLists(type);

			//			nhibernateSession.Close();
		}

		public void _GridEditObjects(NHibernate.ISessionFactory sessionFactory, Type type, NHibernate.Expression.Expression expression, string gridXmlMapping, string objectXmlMapping, int depth)
		{
			nhibernateSessionFactory = sessionFactory;
			nhibernateSession = sessionFactory.OpenSession();

			if(expression == null)
			{
				loadedObjects = (IList)nhibernateSession.CreateCriteria(type).List();
			}
			else
			{
				loadedObjects = (IList)nhibernateSession.CreateCriteria(type).Add(expression).List();
			}

			xmlMappingFile = gridXmlMapping;
			this.objectXmlMappingFile = objectXmlMapping;
			loadedType = type;
			initialDepth = depth;
			grid.Selection.SelectionMode = SourceGrid.GridSelectionMode.Row;
		}

		public void AddReflectionArrayLists()
		{
			nhibernateAddArrayLists(loadedType, null);
		}

		public void AddReflectionArrayLists(NHibernate.Expression.Expression expression)
		{
			nhibernateAddArrayLists(loadedType, expression);
		}

		void nhibernateAddArrayLists(Type type, NHibernate.Expression.Expression expression)
		{
			nhibernateAddArrayLists(type, expression, 0);
		}

		void nhibernateAddArrayLists(Type type, NHibernate.Expression.Expression expression, int depth)
		{
			string filteredProperty = null;
			bool executeExpression = false;
			
			
			//checks if filtered field exists, if not all objects are loaded
			if(expression != null)
			{
				string expressionString = expression.ToString();

				filteredProperty = expressionString.Substring(0,expressionString.IndexOf(' '));
			}

			foreach(PropertyInfo propertyInfo in type.GetProperties())
			{
				//check if we already have this property entered
				if(arrayList[propertyInfo.Name] == null)
				{

					//				Console.WriteLine(propertyInfo.ToString());
					if(!propertyInfo.PropertyType.IsPrimitive &&
						propertyInfo.PropertyType != typeof(String) &&
						propertyInfo.PropertyType != typeof(DateTime) &&
						propertyInfo.PropertyType != typeof(IList) &&
						propertyInfo.PropertyType != typeof(Decimal) &&
						propertyInfo.PropertyType != typeof(Guid) &&
						propertyInfo.PropertyType != typeof(Object))
					{
						//					Console.WriteLine(propertyInfo.Name);

						if(expression != null)
						{
							//						expressionPropertyInfo = ;

							executeExpression = type.GetProperty(propertyInfo.Name).PropertyType.GetProperty(filteredProperty) != null;
						}

						try
						{
							//						this.AddArrayList(propertyInfo.Name,(ArrayList)nhibernateSession.CreateCriteria(propertyInfo.PropertyType).List());
							if(executeExpression)
							{
								this.AddArrayList(propertyInfo.Name,(ArrayList)nhibernateSession.CreateCriteria(propertyInfo.PropertyType).Add(expression).List());
							}
							else
							{
								this.AddArrayList(propertyInfo.Name,(ArrayList)nhibernateSession.CreateCriteria(propertyInfo.PropertyType).List());
							}

							if(depth < addReflectionListDepth)
								nhibernateAddArrayLists(propertyInfo.PropertyType, expression, depth + 1);
							//						Console.WriteLine("auto list: added: " + e.ToString());
						}
						catch(Exception e)
						{
							Console.WriteLine("auto list: add failed: " + e.ToString());
						}
					}

					//hack!!!! smisli pametnije
					//sad podrazumjeva tip da je tip spremljenih objekata u IListu ime propertya
					if(propertyInfo.PropertyType == typeof(IList))
					{
						//Type type1 = Type.GetType("EventManager.DataClasses."+propertyInfo.Name);

						String containedType = type.ToString();
							
						containedType = containedType.Substring(0,containedType.LastIndexOf('.'));
						containedType += "." + propertyInfo.Name;

						if(expression != null)
						{
							//expressionPropertyInfo = ;

							Type t = Type.GetType(containedType);

							if(t != null)
							{
								executeExpression = t.GetProperty(filteredProperty) != null;
							}
							else
							{
								executeExpression = false;
							}
						}

						try
						{
							if(executeExpression)
							{
								this.AddArrayList(propertyInfo.Name,(ArrayList)nhibernateSession.CreateCriteria(Type.GetType(containedType)).Add(expression).List());
							}
							else
							{
								this.AddArrayList(propertyInfo.Name,(ArrayList)nhibernateSession.CreateCriteria(Type.GetType(containedType)).List());
							}

							if(depth < addReflectionListDepth)
								nhibernateAddArrayLists(Type.GetType(containedType), expression, depth + 1);
						}
						catch(Exception e)
						{
							Console.WriteLine("auto add list failed: " + e.ToString());
						}

					}
				}
			}
		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
/*			vjeroajtno se sam disposea			
			if(nhibernateSessionFactory != null)
				nhibernateSession.Close();
*/
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		public void refresh()
		{
			int x;
			int y;

			//todo add row tracking (to prevent refreshing all rows)
			for(x=1;x<grid.Cols;x++)
				for(y=1;y<grid.Rows;y++)
				{
					StoredObject storedObject = (StoredObject)(grid[y,x]).Tag;

					if(grid[y,x].Value is ComboObjectItem)
					{
						ComboObjectItem oldComboObjectItem = (ComboObjectItem)grid[y,x].Value;

						ComboObjectItem newComboItem = new ComboObjectItem();

						//						newComboItem.Display = storedObject.Object.GetType().GetProperty(oldComboObjectItem.PropertyName).GetValue(storedObject.Object, null).ToString();
						object selectedObject =	storedObject.Object.GetType().GetProperty(storedObject.Property).GetValue(storedObject.Object, null);

						if(selectedObject != null)
						{
							newComboItem.Object = selectedObject;
							newComboItem.Display = selectedObject.GetType().GetProperty(oldComboObjectItem.PropertyName).GetValue(selectedObject, null).ToString();							
						}
					
						newComboItem.PropertyName = oldComboObjectItem.PropertyName;
					
						grid[y,x].Value = newComboItem;
					}
					else
					{
						if(storedObject != null)
						{
							if(storedObject.SubProperty == null)
							{
								if(grid[y,x].Value != null && grid[y,x].Value.GetType() == typeof(string) && storedObject.Object.GetType().GetProperty(storedObject.Property).PropertyType == typeof(int))
								{
									//for intlist mode, not tested yet
									grid[y,x].Value = ((ArrayList)intList[storedObject.Property])[(int)(storedObject.Object.GetType().GetProperty(storedObject.Property).GetValue(storedObject.Object, null))];
								}
								else
								{																					
									grid[y,x].Value = storedObject.Object.GetType().GetProperty(storedObject.Property).GetValue(storedObject.Object, null);
								}
							}
							else
							{
								object o = storedObject.Object.GetType().GetProperty(storedObject.Property).GetValue(storedObject.Object,null);

								if(o != null)
									grid[y,x].Value = o.GetType().GetProperty(storedObject.SubProperty).GetValue(o, null);

								//sg.Value = o.GetType().GetProperty(so.SubProperty).GetValue(o, null);
							}
						
						}

						grid.ShowCell(grid[y,x]);
					}
				}

			this.selectedObject = null;
		}

		protected void selectCell(IList selectFrom)
		{
			FormSelectObject formSelectObject = new FormSelectObject(selectFrom, loadedType, xmlMappingFile, initialDepth);

			formSelectObject.ShowDialog();

			if(formSelectObject.DialogResult == DialogResult.OK)
			{
				loadedObjects.Add(formSelectObject.SelectedObject);

				//todo: add only this row to grid, don't redraw whole grid
				this.ShowGrid();
			}
		}

		public void addCell()
		{
			object newObject = Activator.CreateInstance(loadedType);

			//postavi sve IListove u objektu na novi ArrayList
			foreach(PropertyInfo pi in newObject.GetType().GetProperties())
			{
				if(pi.PropertyType == typeof(IList))
				{
					pi.SetValue(newObject, new ArrayList(), null);
				}
			}

			ObjectPresentation.FormEditObject formEditObject = new FormEditObject(newObject, objectXmlMappingFile, initialDepth);

			formEditObject.SetPropertiesToValues(listSetPropertiesToValues);

			formEditObject.SetArrayList(arrayList);

			formEditObject.EditArray = editArray;

			formEditObject.ShowEdit();
			formEditObject.ShowDialog();

			if(formEditObject.DialogResult == DialogResult.OK)
			{
				if(nhibernateSession != null)
				{
					nhibernateAdd(newObject);
				}
				else
				{
					loadedObjects.Add(newObject);

					//todo: add only this row to grid, don't redraw whole grid
					this.ShowGrid();
				}
			}
		}

		void nhibernateAdd(object newObject)
		{
//			NHibernate.ISession session = nhibernateSessionFactory.OpenSession();

			NHibernate.ISession session = nhibernateSession;

			NHibernate.ITransaction nhibernateTransaction = session.BeginTransaction();
			try
			{
				//				NHibernate.ITransaction nhibernateTransaction = nhibernateSession.Transaction;

				session.Save(newObject);

				nhibernateTransaction.Commit();
				//nhibernateSession.Flush();

				loadedObjects.Add(newObject);
				//todo: add only this row to grid, don't redraw whole grid
				this.ShowGrid();
			}
			catch(Exception e)
			{
				MessageBox.Show(e.ToString());
				nhibernateTransaction.Rollback();
			}
			finally
			{
//				session.Close();
			}
		}

		public int CurrentWidth
		{
			get
			{
				int width = 0;

				for(int i = 0;i < grid.Cols; i++)
				{
					width+=grid.GetColWidth(i);
				}

				return width+20;
			}
		}

		public int CurrentHeigth
		{
			get
			{
				int heigth = 0;

				for(int i = 0;i < grid.Rows; i++)
				{
					heigth+=grid.GetRowHeight(i);
				}

				return heigth+10;
			}
		}

		public bool AutoExtendCols
		{
			set
			{
				autoExtendCols = value;

				if(autoExtendCols)
				{
					this.SizeChanged += new EventHandler(grid_AutoExtendCols);
				}
				else
				{
					this.SizeChanged -= new EventHandler(grid_AutoExtendCols);
				}
			}
			get
			{
				return autoExtendCols;
			}
		}

		protected void grid_AutoExtendCols(object sender, System.EventArgs e)
		{
			AutoExtendColsDo();
		}

		public void AutoExtendColsDo()
		{
//			grid.SuspendLayout();
			this.SuspendLayout();

			grid.AutoSizeAll();

			double extendRatio = (double)grid.Width / (double)CurrentWidth;

			if(extendRatio > 1)
			{
				for(int i = 0;i < grid.Cols; i++)
				{
					grid.SetColWidth(i, (int)(grid.GetColWidth(i) * (double)extendRatio));
				}
			}

			this.ResumeLayout();
//			grid.ResumeLayout();
		}

		public void AutoSizeBounds()
		{
			int anchor = (int)grid.Anchor;

			grid.SuspendLayout();
			this.SuspendLayout();

			grid.AutoSizeAll();

			grid.Anchor = AnchorStyles.Top | AnchorStyles.Left;
			grid.Size = new Size(this.CurrentWidth,this.CurrentHeigth);
			this.Height = grid.Height + label.Height + 10;
			this.Width = grid.Width;
			grid.Anchor = (AnchorStyles)anchor;

			this.ResumeLayout();
			grid.ResumeLayout();
		}

		public void editCell()
		{
			object obj;

			try
			{
				obj = ((StoredObject)(grid.Selection[0].Tag)).Object;
			}
			catch
			{
				MessageBox.Show("No row selected!");
				return;
			}
		
			ObjectPresentation.FormEditObject formEditObject = new FormEditObject(obj, objectXmlMappingFile, initialDepth);

			//not actually required here
			formEditObject.SetPropertiesToValues(listSetPropertiesToValues);

			formEditObject.SetArrayList(arrayList);

			formEditObject.EditArray = editArray;

/*			foreach(ArrayList list in comboArrayList)
			{
				formEditObject.AddComboList(list);
			}
*/

//			System.Collections.Specialized.NameObjectCollectionBase o = new System.Collections.Specialized.NameObjectCollectionBase();

/*			NHibernate.ITransaction nhibernateTransaction;

			if(nhibernateSession != null)
			{
				nhibernateTransaction = nhibernateSession.Transaction;
			}
*/
			formEditObject.ShowEdit();
			formEditObject.ShowDialog();

			if(nhibernateSession != null)
			{
				nhibernateEdit(obj);
			}
	
			this.refresh();
		}

		void nhibernateEdit(object obj)
		{
//			NHibernate.ISession session = nhibernateSessionFactory.OpenSession();
			NHibernate.ISession session = nhibernateSession;

			NHibernate.ITransaction nhibernateTransaction = session.BeginTransaction();
//			NHibernate.ITransaction nhibernateTransaction = nhibernateSession.Transaction;

			try
			{
				session.Update(obj);

				nhibernateTransaction.Commit();
				//nhibernateSession.Flush();
			}
			catch(Exception e)
			{
				MessageBox.Show(e.ToString());
				nhibernateTransaction.Rollback();
				nhibernateSession.Refresh(obj);
			}
			finally
			{
//				session.Close();
			}
		}

		public void deleteCell()
		{
			object obj;

			try
			{
				obj = ((StoredObject)(grid.Selection[0].Tag)).Object;
			}
			catch
			{
				MessageBox.Show("No row selected!");
				return;
			}
		
			ArrayList pendingRows = new ArrayList();


			if(nhibernateSession != null)
			{
				NHibernate.ISession session = nhibernateSessionFactory.OpenSession();
				//NHibernate.ISession session = nhibernateSession;
			
				//				NHibernate.ISession session = EventManager.Globals.nhibernateSessionFactory.OpenSession();

				NHibernate.ITransaction nhibernateTransaction = session.BeginTransaction();

				//			NHibernate.ITransaction nhibernateTransaction = nhibernateSession.BeginTransaction();
				//			NHibernate.ITransaction nhibernateTransaction = nhibernateSession.Transaction;

//				bool deleteError = false;

				try
				{
					nhibernateSession.Evict(obj);
					session.Delete(obj);

					nhibernateTransaction.Commit();
					loadedObjects.Remove(obj);
					grid.RemoveRow(grid.Selection[0].Row);
					this.refresh();
				}
				catch
				{
					nhibernateTransaction.Rollback();

//					nhibernateTransaction.
					//recreates new session (or else we have delete errors)
/*					session.Close();

					if(nhibernateSessionFactory == null)
					{
						EventManager.Globals.nhibernateSession = 
							session = nhibernateSession = 
							EventManager.Globals.nhibernateSessionFactory.OpenSession();
					}
					else
					{
						session = nhibernateSession = nhibernateSessionFactory.OpenSession();
					}
*/
					nhibernateSession.Refresh(obj);
					MessageBox.Show("Unable to delete object!");
				}
				finally
				{
				}

			}
			else
			{
				grid.RemoveRow(grid.Selection[0].Row);
				loadedObjects.Remove(obj);
				this.refresh();
			}

		}

		protected void gridCell_SelectionChange(object sender, System.EventArgs e)
		{
			if(((SourceGrid.Cell)sender).Tag != null)
				selectedObject = ((StoredObject)(((SourceGrid.Cell)sender).Tag)).Object;

			if (SelectionChanged != null)
				SelectionChanged(this, e);
		}

		protected void grid_DoubleClick(object sender, System.EventArgs e)
		{
			editCell();
		}

		CollectionKeyObject	listSetPropertiesToValues = null;

		public void SetPropertiesToValues(CollectionKeyObject objectValues)
		{
			listSetPropertiesToValues = objectValues;
		}

		public IList Selection
		{
			get
			{
				ArrayList selection = new ArrayList();

				foreach(object obj in grid.Selection)
				{
					if(obj is SourceGrid.CellTyped)
					{
						SourceGrid.CellTyped cell = (SourceGrid.CellTyped)obj;

						if(cell.Col == 1)
						{
							selection.Add(((StoredObject)(cell.Tag)).Object);
						}
					}
				}

				return selection;
			}
		}

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.grid = new SourceGrid.Grid();
			this.label = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// grid
			// 
			this.grid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.grid.AutoSizeMinHeight = 15;
			this.grid.AutoSizeMinWidth = 15;
			this.grid.CellsContainerCursor = System.Windows.Forms.Cursors.Default;
			this.grid.CellsContainerToolTipText = "";
			this.grid.Cols = 0;
			this.grid.CustomScrollArea = new System.Drawing.Size(0, 0);
			this.grid.CustomScrollPosition = new System.Drawing.Point(0, 0);
			this.grid.EnableMultiSelection = true;
			this.grid.EnableRowColSpan = true;
			this.grid.FixedCols = 0;
			this.grid.FixedRows = 0;
			this.grid.GridContextMenu = ((SourceGrid.GridContextMenu)((((((SourceGrid.GridContextMenu.ColumnResize | SourceGrid.GridContextMenu.RowResize) 
				| SourceGrid.GridContextMenu.AutoSize) 
				| SourceGrid.GridContextMenu.ClearSelection) 
				| SourceGrid.GridContextMenu.CopyPasteSelection) 
				| SourceGrid.GridContextMenu.PropertySelection)));
			this.grid.GridScrollPosition = new System.Drawing.Point(0, 0);
			this.grid.Location = new System.Drawing.Point(0, 32);
			this.grid.Name = "grid";
			this.grid.Redraw = true;
			this.grid.Rows = 0;
			this.grid.Size = new System.Drawing.Size(584, 416);
			this.grid.TabIndex = 0;
			this.grid.ToolTipActive = true;
			// 
			// label
			// 
			this.label.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label.Location = new System.Drawing.Point(8, 8);
			this.label.Name = "label";
			this.label.Size = new System.Drawing.Size(544, 24);
			this.label.TabIndex = 4;
			// 
			// GridEditObjects
			// 
			this.Controls.Add(this.label);
			this.Controls.Add(this.grid);
			this.Name = "GridEditObjects";
			this.Size = new System.Drawing.Size(584, 448);
			this.ResumeLayout(false);

		}
		#endregion

		#region IPropertyEdit Members

		public object CurrentObject
		{
			set
			{
			}
		}

		public void PropertySet(object obj, string property)
		{
			// TODO:  Add PanelEditObjects.PropertySet implementation
		}

		public void PropertySave()
		{
			// TODO:  Add PanelEditObjects.PropertySave implementation
		}

		public void PropertyRevert()
		{
			// TODO:  Add PanelEditObjects.PropertyRevert implementation
		}

		public int DescriptionWidth
		{
			set
			{
				// TODO:  Add PanelEditObjects.DescriptionWidth setter implementation
			}
		}

		public int EditWidth
		{
			set
			{
				// TODO:  Add PanelEditObjects.EditWidth setter implementation
			}
		}

		#endregion
	}
}
