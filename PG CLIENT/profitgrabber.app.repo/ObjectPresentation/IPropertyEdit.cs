using System;

namespace ObjectPresentation
{
	/// <summary>
	/// Summary description for IPropertyEdit.
	/// </summary>
	public interface IPropertyEdit
	{
		void PropertySet(object obj, string property);
		void PropertySave();
		void PropertyRevert();

		bool Editable
		{
			set;
		}

		int DescriptionWidth
		{
			set;
		}

		int EditWidth
		{
			set;
		}
	}
}
