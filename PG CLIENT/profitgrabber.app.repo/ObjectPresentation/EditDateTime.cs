using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;

namespace ObjectPresentation
{
	/// <summary>
	/// Summary description for EditDateTime.
	/// </summary>
	public class EditDateTime : System.Windows.Forms.UserControl, ObjectPresentation.IPropertyEdit
	{
		private System.Windows.Forms.Label label;
		private System.Windows.Forms.DateTimePicker dateTime;
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected object currentObject;
		protected string currentProperty;

		public EditDateTime()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

			// TODO: Add any initialization after the InitializeComponent call

		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		public EditDateTime(string description, object obj, string property)
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

			label.Text = description;

			PropertySet(obj, property);
		}


		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label = new System.Windows.Forms.Label();
			this.dateTime = new System.Windows.Forms.DateTimePicker();
			this.SuspendLayout();
			// 
			// label
			// 
			this.label.Location = new System.Drawing.Point(8, 3);
			this.label.Name = "label";
			this.label.Size = new System.Drawing.Size(192, 24);
			this.label.TabIndex = 0;
			// 
			// dateTime
			// 
			this.dateTime.CustomFormat = "dd.MM.yyyy - HH:mm";
			this.dateTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dateTime.Location = new System.Drawing.Point(208, 3);
			this.dateTime.Name = "dateTime";
			this.dateTime.Size = new System.Drawing.Size(128, 20);
			this.dateTime.TabIndex = 1;
			// 
			// EditDateTime
			// 
			this.Controls.Add(this.dateTime);
			this.Controls.Add(this.label);
			this.Name = "EditDateTime";
			this.Size = new System.Drawing.Size(352, 35);
			this.ResumeLayout(false);

		}
		#endregion

		#region IPropertyEdit Members

		public bool Editable
		{
			set
			{
				dateTime.Enabled = value;
			}
		}

		virtual public void PropertySet(object obj, string property)
		{		
			currentObject = obj;
			//			TextBox.Text = (string)obj.GetType().GetProperty(property).GetValue(obj, null);

			try
			{
				dateTime.Value = (DateTime)obj.GetType().GetProperty(property).GetValue(obj, null);
			}
			catch
			{
				dateTime.Value = DateTime.Now;
			}

			currentProperty = property;
		}

		virtual public void PropertySave()
		{
			currentObject.GetType().GetProperty(currentProperty).SetValue(currentObject, dateTime.Value, null);
		}

		virtual public void PropertyRevert()
		{
			dateTime.Value = (DateTime)currentObject.GetType().GetProperty(currentProperty).GetValue(currentObject, null);
		}

		public int DescriptionWidth
		{
			set
			{
				label.Width = value;
				dateTime.Left = dateTime.Width + 32;
				this.Width = label.Width + dateTime.Width + 10;
			}
		}

		public int EditWidth
		{
			set
			{
				dateTime.Width = value;
				this.Width = label.Width + dateTime.Width + 10;
			}
		}

		#endregion
	}
}
