using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;

namespace ObjectPresentation
{
	/// <summary>
	/// Summary description for ClassLabel.
	/// </summary>
	public class LabelClass : System.Windows.Forms.UserControl
	{
		private System.Windows.Forms.Label Label;
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public LabelClass()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

			// TODO: Add any initialization after the InitializeComponent call

		}

		public LabelClass(string label)
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();
			Label.Text = label;

			// TODO: Add any initialization after the InitializeComponent call

		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Label = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// Label
			// 
			this.Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.Label.Location = new System.Drawing.Point(8, 12);
			this.Label.Name = "Label";
			this.Label.Size = new System.Drawing.Size(248, 24);
			this.Label.TabIndex = 0;
			// 
			// LabelClass
			// 
			this.Controls.Add(this.Label);
			this.Name = "LabelClass";
			this.Size = new System.Drawing.Size(264, 41);
			this.ResumeLayout(false);

		}
		#endregion

	}
}
