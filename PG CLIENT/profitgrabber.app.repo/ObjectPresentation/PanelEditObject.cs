using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;
using System.Xml;
using System.Reflection;

namespace ObjectPresentation
{
	/// <summary>
	/// Summary description for ObjectPresentation.
	/// </summary>
	public class PanelEditObject : System.Windows.Forms.UserControl, IPropertyEdit
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
		private XmlTextReader reader;

		protected int current_x;
		protected int current_y;
		private int max_x;
		private int max_y;
		private string showObjectName;
		protected bool editArray = false;

		protected object loadedObject;
		protected Type loadedObjectType;
		private int initialDepth;
		private string xmlMappingFile;
		protected CollectionKeyObject arrayList = new CollectionKeyObject();

		//		private ArrayList comboArrayList = new ArrayList();
//		protected System.Collections.Specialized.NameValueCollection arrayList = new System.Collections.Specialized.NameValueCollection();
//		private int arrayListCount = 0;

		private bool editable;

		//if false only display properties without fulledit="true" atribute
		private bool fullEdit = false;

		protected string selectedPropertyName;

		protected int expandHeigth = 0;

		protected ArrayList propertyEditList = new ArrayList();

		private bool readXmlFromResource = true;

		public bool ReadXmlFromResource
		{
			set
			{
				readXmlFromResource = value;
			}
			get
			{
				return readXmlFromResource;
			}
		}

		public bool FullEdit
		{
			set
			{
				fullEdit = value;
			}
			get
			{
				return fullEdit;
			}
		}

		public bool EditArray
		{
			set
			{
				editArray = value;
			}
			get
			{
				return editArray;
			}
		}

		public bool Editable
		{
			get
			{
				return editable;
			}
			set
			{
				editable = value;
			}
		}

		public string ObjectName
		{
			get
			{
				return showObjectName;
			}
		}

		public void _EditObject(object obj, Type type, string xmlMapping, int depth)
		{
			loadedObject = obj;
			loadedObjectType = type;
			editable = true;
			initialDepth = depth;
			xmlMappingFile = xmlMapping;
		}
		
		public void ShowEdit()
		{
			//clear controls in case of redraw
			
			ShowEdit_continueForeach:
			foreach(System.Windows.Forms.Control control in this.Controls)
			{
				if(control is ObjectPresentation.IPropertyEdit)
				//if(!(control is Button))
				{
					this.Controls.Remove(control);
					goto ShowEdit_continueForeach;
				}
			}

			if(readXmlFromResource)
			{
				string name = null;
				IList i = System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceNames();
				foreach(object o in i)
				{
					if(o.ToString().EndsWith(xmlMappingFile))
					{
						name = o.ToString();
						break;
					}
				}

				System.IO.Stream stream = System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream(name);
				reader = new XmlTextReader(stream);
			}
			else
			{
				reader = new XmlTextReader(xmlMappingFile);
			}
//			reader = new XmlTextReader(xmlMappingFile);

			while(reader.Read())
			{
				//reader.GetAttribute("type");

//				if(reader.Depth == initialDepth && reader.Name == "object" && loadedObject.GetType().ToString() == reader.GetAttribute("type"))
				if(reader.Depth == initialDepth && loadedObjectType.ToString() == reader.GetAttribute("type"))
				{
					//if((reader.Name == "object" && !editArray) || (reader.Name == "array" && editArray))
					if(
						(
							(reader.Name == "object" && !editArray) ||
							(reader.Name == "array" && editArray)
						)
						&&
						(
							selectedPropertyName == null ||
							selectedPropertyName == reader.GetAttribute("name")
						)
					)
					//selectedPropertyName
					{

						if(loadedObject == null)
						{
							loadedObject = Activator.CreateInstance(loadedObjectType);
						}

						parseObject(loadedObject, reader.Depth);
					}
				}
			}

			//reset to allow redraw after
			this.current_x = 0;
			this.current_y = 0;

			//set predefined values
			doSetPropertiesToValues();

			this.PerformLayout();
		}

		public PanelEditObject()
		{
			InitializeComponent();
		}

		public PanelEditObject(object obj, string xmlMapping, int depth)
		{
			InitializeComponent();
			_EditObject(obj, obj.GetType(), xmlMapping, depth);
		}

		public PanelEditObject(object obj, string xmlMapping)
		{
			InitializeComponent();
			_EditObject(obj, obj.GetType(), xmlMapping, 1);
		}

		public PanelEditObject(object obj, Type type, string xmlMapping, int depth)
		{
			InitializeComponent();
			_EditObject(obj, type, xmlMapping, depth);
		}

		public PanelEditObject(object obj, Type type, string xmlMapping, int depth, string propertyName)
		{
			InitializeComponent();

			selectedPropertyName = propertyName;
			_EditObject(obj, type, xmlMapping, depth);
		}

		public PanelEditObject(object obj, Type type, string xmlMapping)
		{
			InitializeComponent();
			_EditObject(obj, type, xmlMapping, 1);
		}

		/*
		public void AddComboList(ArrayList list)
		{
			comboArrayList.Add(list);
		}
		*/

		public void AddArrayList(string name, ArrayList list)
		{
			arrayList.Add(name, list);
		}

		public void SetArrayList(CollectionKeyObject list)
		{
			arrayList = list;
		}

		void parseObject(object obj, int depth)
		{
			string objectName = reader.GetAttribute("type");
			string objectDisplay = reader.GetAttribute("display");
			int move_y = 0;
			int move_max_y = 0;

			if(objectDisplay == null) objectDisplay = objectName;

			if(depth == initialDepth) showObjectName = objectDisplay;

			ObjectPresentation.LabelClass labelClass = new ObjectPresentation.LabelClass(objectDisplay);
			labelClass.Location = new Point(0,0);

			current_y+=labelClass.Height;

			labelClass.TabStop = false;
			this.Controls.Add(labelClass);

			while(reader.Read() && reader.Depth > depth)
			{
				//if(reader.Depth == depth && reader.HasAttributes == false) break;
				if(reader.Depth == depth + 1 && reader.HasAttributes)
				{
					//discard if subProperty
					if(reader.Name == "object" || reader.Name == "array" || (reader.Name == "edit" && reader.GetAttribute("subproperty") == null) || reader.Name == "combo" || reader.Name == "section")
					{
						if(reader.GetAttribute("fulledit") == "true" && !fullEdit) continue;

						int offset_x = 0;
						int offset_y = 0;

						bool tabStop = true;

						string propertyName = reader.GetAttribute("name");
						string propertyDisplay = reader.GetAttribute("display");
						string propertyHorizontal_s = reader.GetAttribute("horizontal");

						string arrayPropertyType = reader.GetAttribute("type");

						string labelWidth_s = reader.GetAttribute("descwidth");
						string editWidth_s = reader.GetAttribute("editwidth");
						
						int labelWidth = 150;
						int editWidth = 150;

						if(labelWidth_s != null) labelWidth = Convert.ToInt32(labelWidth_s);
						if(editWidth_s != null) editWidth = Convert.ToInt32(editWidth_s);

						bool propertyHorizontal = false;

						if(propertyHorizontal_s == null) propertyHorizontal = false;
						if(propertyHorizontal_s != null) propertyHorizontal = true;

						if(propertyDisplay == null) propertyDisplay = propertyName;

						//if(!propertyHorizontal) current_y+=move_y;
//						if(!propertyHorizontal) current_y+=move_max_y;

						if(propertyHorizontal)
						{
							if(move_max_y < move_y) move_max_y = move_y;
							current_x = Convert.ToInt32(propertyHorizontal_s);
						}
						else
						{
							if(move_max_y > 0)
								current_y+=move_max_y;
							else
								current_y+=move_y;

							move_max_y = 0;
							current_x = 0;
						}

						Type propertyType = null;
						if(propertyName != null)
							propertyType = obj.GetType().GetProperty(propertyName).PropertyType;

						Control control=null;

						if(reader.Name == "array")
						{
							if(reader.GetAttribute("edit") == "true" && (reader.GetAttribute("select") == "false" || reader.GetAttribute("select") == null))
							{
								offset_x = 15;

								
								//object o1 = obj.GetType().GetProperty(propertyName).GetValue(obj, null);
								//ArrayList objs = (ArrayList)(obj.GetType().GetProperty(propertyName).GetValue(obj, null));
								IList objs = (IList)(obj.GetType().GetProperty(propertyName).GetValue(obj, null));

								//if null assign one
								if(objs == null)
								{
									objs = new ArrayList();
									obj.GetType().GetProperty(propertyName).SetValue(obj,objs,null);
								}

								ObjectPresentation.PanelEditObjects panelEditObjects = new ObjectPresentation.PanelEditObjects(objs, Type.GetType(arrayPropertyType), xmlMappingFile, initialDepth + 1);

								panelEditObjects.EditArray = true;

								panelEditObjects.SetArrayList(arrayList);

								panelEditObjects.Editable = editable;

								panelEditObjects.ShowGrid();

								control = panelEditObjects;
								control.Height = Convert.ToInt32(reader.GetAttribute("heigth"));
							}
							else
							{
								offset_x = 15;
								IList objs = (IList)(obj.GetType().GetProperty(propertyName).GetValue(obj, null));

								//if null assign one
								if(objs == null)
								{
									objs = new ArrayList();
									obj.GetType().GetProperty(propertyName).SetValue(obj,objs,null);
								}

								ObjectPresentation.PanelSelectObjects panelSelectObjects = new ObjectPresentation.PanelSelectObjects(objs, (ArrayList)arrayList[propertyName], Type.GetType(arrayPropertyType), xmlMappingFile, initialDepth + 1);

								panelSelectObjects.EditArray = true;

								panelSelectObjects.ShowGrid();

								control = panelSelectObjects;
								control.Height = Convert.ToInt32(reader.GetAttribute("heigth"));
							}

							if(reader.GetAttribute("edit") == "true" && reader.GetAttribute("select") == "true")
							{
								offset_x = 15;
								IList objs = (IList)(obj.GetType().GetProperty(propertyName).GetValue(obj, null));

								//if null assign one
								if(objs == null)
								{
									objs = new ArrayList();
									obj.GetType().GetProperty(propertyName).SetValue(obj,objs,null);
								}

								ObjectPresentation.PanelEditSelectObjects panelSelectEditObjects = new ObjectPresentation.PanelEditSelectObjects(objs, (ArrayList)arrayList[propertyName], Type.GetType(arrayPropertyType), xmlMappingFile, initialDepth + 1);

								panelSelectEditObjects.EditArray = true;

								panelSelectEditObjects.SetArrayList(arrayList);

								panelSelectEditObjects.Editable = editable;

								panelSelectEditObjects.ShowGrid();

								control = panelSelectEditObjects;
								control.Height = Convert.ToInt32(reader.GetAttribute("heigth"));
							}
						}

						if(reader.Name == "object")
						{
							if(reader.GetAttribute("edit") == "true" && (reader.GetAttribute("select") == "false" || reader.GetAttribute("select") == null))
							{
								offset_x = 15;

								//get encapsulated object
								object editObject = obj.GetType().GetProperty(propertyName).GetValue(obj,null);
								Type editType = obj.GetType().GetProperty(propertyName).PropertyType;

								//if null assign one
								if(editObject == null)
								{
									editObject = Activator.CreateInstance(editType);

									obj.GetType().GetProperty(propertyName).SetValue(obj,editObject,null);
								}

								//ObjectPresentation.PanelEditObject panelEditObject = new ObjectPresentation.PanelEditObject(editObject, editType, xmlMappingFile, initialDepth + 1);
								ObjectPresentation.PanelEditObject panelEditObject = new ObjectPresentation.PanelEditObject(editObject, editType, xmlMappingFile, initialDepth + 1, propertyName);

								panelEditObject.SetArrayList(arrayList);

								panelEditObject.ShowEdit();
								control = panelEditObject;
							}
							else
							{
								offset_x = 15;

								ObjectPresentation.PanelShowObject panelShowObjects = new ObjectPresentation.PanelShowObject(obj, propertyName,(ArrayList)arrayList[propertyName], xmlMappingFile, initialDepth + 1, propertyName);

								panelShowObjects.SetArrayList(arrayList);

								panelShowObjects.ShowEdit();
								control = panelShowObjects;
							}
///////////////////
							if(reader.GetAttribute("edit") == "true" && reader.GetAttribute("select") == "true")
							{
								offset_x = 15;

								ObjectPresentation.PanelShowObject panelShowObjects = new ObjectPresentation.PanelShowObject(obj, propertyName,(ArrayList)arrayList[propertyName], xmlMappingFile, initialDepth + 1, propertyName, true);

//								panelShowObjects.EditAdd = true;

								panelShowObjects.SetArrayList(arrayList);

								panelShowObjects.ShowEdit();
								control = panelShowObjects;
							}
						}

						if(reader.Name == "section")
						{
							control = new ObjectPresentation.LabelSection(propertyDisplay);
							tabStop = false;
						}

						if(reader.Name == "edit")
						{
							if(propertyType == typeof(string))
							{
								string multiline = reader.GetAttribute("multiline");

								if(multiline != null && multiline == "true")
								{
									control = new EditTextBig(propertyDisplay, obj, propertyName);
								}
								else
								{
									control = new EditText(propertyDisplay, obj, propertyName);
								}
							}

							if(propertyType == typeof(int))
							{
								control = new EditInt(propertyDisplay, obj, propertyName);
							}

							if(propertyType == typeof(System.Decimal))
							{
								control = new EditDouble(propertyDisplay, obj, propertyName);
							}

							if(propertyType == typeof(bool))
							{
								control = new EditBool(propertyDisplay, obj, propertyName);
							}

							if(propertyType == typeof(DateTime))
							{
								control = new EditDateTime(propertyDisplay, obj, propertyName);
							}
						}

						if(reader.Name == "combo")
						{
							string propertyEnlist = reader.GetAttribute("enlist");

//							int count = 0;

//							ArrayList properList = null;

							//removed array list automatic select
							//now add ordered, in case of object or object array
							//encapsulate list of arrays inside combo list
/*							foreach(ArrayList list in comboArrayList)
							{
								if(list.Count > 0)
								{
									if(list[0].GetType() == loadedObject.GetType().GetProperty(propertyName).PropertyType)
									{
										if(arrayListCount <= count)
										{
											properList = list;
											arrayListCount++;
											break;
										}
										count++;
									}
								}
							}
*/
/*
							properList =  (ArrayList)comboArrayList[arrayListCount++];
*/
//							if(arrayList[propertyName] != null)						
								control = new ComboObject(propertyDisplay, obj, propertyName, propertyEnlist, (ArrayList)arrayList[propertyName]);
						}

						if(control!=null)
						{
							move_y = control.Height;

							control.Location = new Point(current_x + offset_x, current_y + offset_y);
							
							IPropertyEdit edit = (IPropertyEdit)control;

							edit.Editable = editable;
							edit.DescriptionWidth = labelWidth;
							edit.EditWidth = editWidth;

							if(max_x < control.Width + current_x) max_x = control.Width + current_x;
							if(max_y < control.Height + current_y) max_y = control.Height + current_y;

							this.Controls.Add(control);
							this.Size = new Size(max_x + 35, max_y + 25 + expandHeigth);
						
							control.TabStop = tabStop;

							propertyEditList.Add(control);
						}
					}
				}
			}

			//poravnaj sve PanelEditObjects na sirinu
			foreach(object o in this.Controls)
			{
				if(o is PanelEditObjects)
				{
					((PanelEditObjects)o).Width  = this.Width - 20;
					((PanelEditObjects)o).Anchor = AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top;
				}
			}
		}

		public void Save()
		{
			foreach(ObjectPresentation.IPropertyEdit propertyEdit in propertyEditList)
			{
				propertyEdit.PropertySave();
			}
		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		protected CollectionKeyObject	listSetPropertiesToValues = null;

		public void SetPropertiesToValues(CollectionKeyObject objectValues)
		{
			listSetPropertiesToValues = objectValues;
		}

		public void doSetPropertiesToValues(CollectionKeyObject listSetPropertiesToValues, object obj)
		{
			if(listSetPropertiesToValues != null)
				foreach(String key in listSetPropertiesToValues.Keys)
				{
					obj.GetType().GetProperty(key).SetValue(obj, listSetPropertiesToValues[key], null);
				}
		}

		public void doSetPropertiesToValues()
		{
			doSetPropertiesToValues(listSetPropertiesToValues, loadedObject);
		}

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			// 
			// PanelEditObject
			// 
			this.AutoScroll = true;
			this.Name = "PanelEditObject";
			this.Size = new System.Drawing.Size(240, 128);

		}
		#endregion

		#region IPropertyEdit Members


		virtual public void PropertySet(object obj, string property)
		{
			// TODO:  Add PanelEditObject.PropertySet implementation
		}

		virtual public void PropertySave()
		{
			this.Save();
		}

		virtual public void PropertyRevert()
		{
			// TODO:  Add PanelEditObject.PropertyRevert implementation
		}

		bool ObjectPresentation.IPropertyEdit.Editable
		{
			set
			{
				// TODO:  Add PanelEditObject.ObjectPresentation.IPropertyEdit.Editable setter implementation
			}
		}

		public int DescriptionWidth
		{
			set
			{
				// TODO:  Add PanelEditObject.DescriptionWidth setter implementation
			}
		}

		public int EditWidth
		{
			set
			{
				// TODO:  Add PanelEditObject.EditWidth setter implementation
			}
		}

		#endregion
	}
}
