#define NEW_INSTALLER
//#define PROMO_VERSION
//#define EXPORT_TEST
//#define NW_TEST_ENV

#region USING
using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.Data.SqlClient; 
using System.Diagnostics;
using System.IO;
using System.Collections.Specialized;
using System.ServiceProcess;
using System.Configuration;
using System.Threading;

using System.Runtime.InteropServices;


using Microsoft.Win32;

using NHibernate;
using NHibernate.Cfg;
using DealMaker.DALC;
using DealMaker.Classes;
using DealMaker.PlainClasses;
using DealMaker.Export;
using DealMaker.Forms.Wizard;
using DealMaker.Forms.Export;
using WindowsApplication.AcquisitionCalculator;
using DealMaker.Registration;
using DealMaker.DataConsumption;
using DealMaker.UserControls.Duplicates;
using ProfitGrabber.Commercial.Objects;
using ProfitGrabber.Common;

using DealMaker.ShortSale;
using DealMaker.ShortSale.Forms;
using DealMaker.ShortSale.Utils;
using DealMaker.PlainClasses.ShortSale.Adapters;
using DealMaker.PlainClasses.Utils;
using DealMaker.Forms.EmailSupport;
using DealMaker.Forms.Task2Email;
using DealMaker.PlainClasses.ContactTypes;
using ProfitGrabber.RVM.Stratics.UOW;
using ProfitGrabber.RVM.Stratics.DTO.Request.Token;
using System.Net.Mail;
using google.oauth.lib;
using DealMaker.Forms.Import.RealEFlow;
#endregion


namespace DealMaker
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class MainForm : System.Windows.Forms.Form
	{

		public delegate void RefreshLockedFeatures();
		public static event RefreshLockedFeatures REFRESH_LOCKED_FEATURES;

		protected void OnRefreshLockedFeatures()
		{
			if (null != REFRESH_LOCKED_FEATURES)
			{
				REFRESH_LOCKED_FEATURES();
			}
		}

		#region � Data �
		private System.Windows.Forms.MainMenu appMenu;
		public  System.Windows.Forms.Panel appBodyPanel;
		private System.Windows.Forms.MenuItem menuItem1;
		private System.Windows.Forms.MenuItem miExit;
		private System.Windows.Forms.Panel appLeftPanel;
		private System.Windows.Forms.Button bGroups;
		private System.Windows.Forms.Button bContacts;
		private System.Windows.Forms.MenuItem menuItem7;
		private System.Windows.Forms.MenuItem menuItem3;
		private System.Windows.Forms.MenuItem menuItem13;
		private System.Windows.Forms.MenuItem menuItem19;
		private System.Windows.Forms.MenuItem menuItem20;
		private System.Windows.Forms.MenuItem menuItem33;
		private System.Windows.Forms.MenuItem menuItem35;
		private System.Windows.Forms.MenuItem menuItem38;
		private System.Windows.Forms.MenuItem menuItem52;
		private System.Windows.Forms.MenuItem menuItem66;
		private System.Windows.Forms.MenuItem menuItem67;
		private System.Windows.Forms.MenuItem menuItem70;
		private System.Windows.Forms.MenuItem menuItem71;
		private System.Windows.Forms.MenuItem menuItem72;
		private System.Windows.Forms.MenuItem menuItem73;
		private System.Windows.Forms.MenuItem menuItem74;
		private System.Windows.Forms.MenuItem menuItem75;

		// ihuk, 04.12.2004
#if EXPORT_TEST
		private System.Windows.Forms.MenuItem testMenuItem;
#endif
		private System.ComponentModel.IContainer components;		
		private System.Windows.Forms.MenuItem menuItem76;
		private System.Windows.Forms.MenuItem menuItem77;
		private System.Windows.Forms.MenuItem menuItem78;
		private System.Windows.Forms.MenuItem menuItem79;
		private System.Windows.Forms.MenuItem menuItem5;
		private System.Windows.Forms.MenuItem menuItem6;
		private System.Windows.Forms.MenuItem menuItem8;
		private System.Windows.Forms.MenuItem menuItem9;
		private System.Windows.Forms.MenuItem menuItem81;
		private System.Windows.Forms.MenuItem menuItem82;
		private System.Windows.Forms.MenuItem menuItem84;
		private System.Windows.Forms.MenuItem menuItem85;
		private System.Windows.Forms.MenuItem menuItem86;
		private System.Windows.Forms.MenuItem menuItem15;
		private System.Windows.Forms.MenuItem menuItem23;
		private System.Windows.Forms.MenuItem menuItem24;
		private System.Windows.Forms.MenuItem menuItem25;
		private System.Windows.Forms.MenuItem menuItem26;
		private System.Windows.Forms.MenuItem menuItem27;
		private System.Windows.Forms.MenuItem importContacts;		
		private System.Windows.Forms.MenuItem mi_CreateNewActivity;
		private System.Windows.Forms.MenuItem mi_TaskManager;
		private System.Windows.Forms.ToolTip toolTip1;
		private System.Windows.Forms.Button bFindContact;
		private System.Windows.Forms.Button bGetFOrmsReady;
		private System.Windows.Forms.Button bReturnMailProcessing;
		private System.Windows.Forms.Button bMailFaxWizard;
		private System.Windows.Forms.Button bDealIQ;
		private System.Windows.Forms.Button bTaskManager;
		private System.Windows.Forms.ImageList imgList;
		private System.Windows.Forms.Button bAddNewContact;
		private System.Windows.Forms.ImageList newImgList;
		private System.Windows.Forms.MenuItem miSubscribeToData;
		private System.Windows.Forms.MenuItem miMyAccount;
		private System.Windows.Forms.MenuItem menuItem2;
		private System.Windows.Forms.MenuItem menuItem4;
		private System.Windows.Forms.MenuItem menuItem11;
		private System.Windows.Forms.MenuItem miGetFormsReady;
		private System.Windows.Forms.MenuItem miDealIQ;
		private System.Windows.Forms.MenuItem miEditMailFaxTemplate;
		private System.Windows.Forms.MenuItem miAbout;
		private System.Windows.Forms.MenuItem miHowTo;
		private System.Windows.Forms.MenuItem miVideoLessons;
		private System.Windows.Forms.MenuItem mi_EmailSS;
		private System.Windows.Forms.MenuItem miLabelOffsetSettings;
		private System.Windows.Forms.MenuItem miSelectDatabase;
		private System.Windows.Forms.MenuItem menuItem10;
		private System.Windows.Forms.MenuItem miGetAdditionalModules;
		private System.Windows.Forms.MenuItem miGetNewLicences;
		private System.Windows.Forms.MenuItem miSelectDB;
		private System.Windows.Forms.MenuItem miCheckForNewLeads;
		private System.Windows.Forms.MenuItem miCOMPS;
		private System.Windows.Forms.MenuItem miTestamonials;
		private System.Windows.Forms.MenuItem miLiveAnswers;
		private System.Windows.Forms.MenuItem miSpecialModules;
		private System.Windows.Forms.MenuItem miAboutSystem;
		private System.Windows.Forms.MenuItem menuItem14;
		private System.Windows.Forms.MenuItem miActivate;
		private System.Windows.Forms.MenuItem miMyActiveModules;
		private System.Windows.Forms.MenuItem menuItem16;
		private System.Windows.Forms.MenuItem mi_purgeOldTaks;
		private System.Windows.Forms.MenuItem miBoosters;
		private System.Windows.Forms.MenuItem mi_MatchingModule;
		private System.Windows.Forms.MenuItem _miInterfaxAccountSetup;
		private System.Windows.Forms.MenuItem _shortSaleModule;
		private System.Windows.Forms.MenuItem _miServerInfo;
		private System.Windows.Forms.MenuItem _scanForDuplicates;
		private System.Windows.Forms.MenuItem _miAccountUsage;
        private MenuItem menuItem17;
        private MenuItem _setShortSaleFileFolder;
        private MenuItem _setWorkingFileFolder;
        private MenuItem _selectLogFile;
        private MenuItem _repairDatabase;
        private MenuItem _setCustomSettings;
        private MenuItem _updateKit;
        private MenuItem _smtpSetup;
        private MenuItem _sendTasksViaEmail;
        private MenuItem _task2EmailMenu;
        private MenuItem menuItem21;
        private MenuItem _viewSentTasksLog;
        private MenuItem menuItem22;
		private System.Windows.Forms.Button bListView;

		//CustomersOverview ctrlCustOverview = null;
		//Customer customer = null; 
        //CustomizedTreeViewUserControl groups = null;
        #endregion � Data �

        Thread _task2MailThread = null;
        private MenuItem _readSSXManual;
        private MenuItem _viewEmailProcessorConsole;
        private MenuItem _newBuyerContact;
        private MenuItem _newLenderContact;
        private MenuItem menuItem12;
        private MenuItem _routing;
        private MenuItem _viewSentEmails;
        private MenuItem miDataAppend;
        private MenuItem _skipTrace;
        private MenuItem _emailMarketingFooterSetup;
        private MenuItem _importRealEFlow;
        private MenuItem _dealFinder360GetLeads;
        bool _runTask2MailThread = true;

#if NW_TEST_ENV
		// SASA REMOVE AFTER TEST
		//		private void NetworkUDP_ChangedNode(object sender, EventArgs e)
		private void NetworkUDP_ChangedNode(Network.NetworkUDP.Packet p)
		{
			MessageBox.Show("Node changed "+p.NodeID.ToString());
		}

		//private void NetworkUDP_ChangedPropertyItem(object sender, EventArgs e)
		private void NetworkUDP_ChangedPropertyItem(Network.NetworkUDP.Packet p)
		{
			MessageBox.Show("PI changed"+p.PropertyItemID.ToString());
		}

		private void NetworkUDP_ChangedTask(Network.NetworkUDP.Packet p)
		{
			MessageBox.Show("atsk changed"+p.TaskID.ToString());
		}
#endif


        public MainForm()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

            //var getAuthToken = new GetAuthenticationToken();
            //var resp = getAuthToken.Execute(new Request("mr@markorubel.com", "lorenzo34"));

            ApplicationHelper.LoadEmailMarketingSignatures();

            //BEGIN - support for UpdateManager - downloading non binary files
            var execPath = Path.GetDirectoryName(Application.ExecutablePath);
            var emailDirectory = execPath + @"\Document Templates\Marketing Templates\Email Templates\";
            if (!Directory.Exists(emailDirectory))
                Directory.CreateDirectory(emailDirectory);

            if (!File.Exists($"{emailDirectory}LOI-ForEmail.tx"))
            {
                var dc = new DownloadClient();
                System.Threading.Tasks.Task.Factory.StartNew(() => dc.DownloadFile(new Uri("http://www.profitgrabber.com/RY16/updates/LOI-ForEmail.tx"), "LOI-ForEmail.tx", emailDirectory));
            }
            //END

#if PROMO_VERSION
			this.miBoosters.Visible = false;
#endif		
	
			//Globals.Load();

#if NW_TEST_ENV
			//sasa test: COMMENT AFTER!!!
            //BEGIN TEST
            Network.NetworkLock.Start();
            Form f = new Network.TestNW2();
            f.ShowDialog();
            Network.NetworkLock.Stop();
			
			// Get the hostname
			string myHost = System.Net.Dns.GetHostName();

			// Show the hostname
			MessageBox.Show(myHost);

            
			TestForm tf = new TestForm();
            //

			//Network.DatabaseSelect tf = new DealMaker.Network.DatabaseSelect();
            //END TEST
#endif

			Globals.MainForm = this;

			Globals.NetworkUdpThreadListener = new System.Threading.Thread(new System.Threading.ThreadStart(Network.NetworkUDP.ThreadListener));
			Globals.NetworkUdpThreadListener.Start();

#if NW_TEST_ENV
            // - BEGIN TEST
            Network.NetworkUDP.ChangedNode += new DealMaker.Network.NetworkChangedEventHandler(NetworkUDP_ChangedNode);
            Network.NetworkUDP.ChangedPropertyItem += new DealMaker.Network.NetworkChangedEventHandler(NetworkUDP_ChangedPropertyItem);
            Network.NetworkUDP.ChangedTask += new DealMaker.Network.NetworkChangedEventHandler(NetworkUDP_ChangedTask);

            tf.ShowDialog();
            // - END TEST
#endif

            _dealFinder360GetLeads.Click += _dealFinder360GetLeads_Click;

            //TA++:16.05.2005.
            //Bug Nr: RQ_0002
            //NEW ++

            _newBuyerContact.Click += new EventHandler(On_NewBuyerContact_Click);
            _newLenderContact.Click += new EventHandler(On_NewLenderContact_Click);

            //repairDatabase
            _repairDatabase.Click += new EventHandler(On_RepairDatabase_Click);
			
			//HUD
			_shortSaleModule.Click += new EventHandler(On_ShortSaleModule_Click);
			_scanForDuplicates.Click += new EventHandler(OnScanForDuplicates_Click);
			
			//NEW WS
			_miAccountUsage.Click += new EventHandler(On_AccountUsage_Click);

            //REAL EFLOW IMPORT
            _importRealEFlow.Click += _importRealEFlow_Click;
			
			Globals.StartUpPath = Application.StartupPath;
			string registeredUser = string.Empty;
			if (null != Globals.UserRegistration)
			{
				if (string.Empty != Globals.UserRegistration.FirstName)
					registeredUser += Globals.UserRegistration.FirstName;
				if (
					string.Empty != Globals.UserRegistration.MiddleName &&
					"%" != Globals.UserRegistration.MiddleName					
					)
					registeredUser += " " + Globals.UserRegistration.MiddleName;
				if (string.Empty != Globals.UserRegistration.LastName)
					registeredUser += " " + Globals.UserRegistration.LastName;				
			}
			

            string error = string.Empty;
            CustomSettingsManager csm = new CustomSettingsManager();
            CustomSettings cs = csm.LoadCustomSettings(out error);
            if (null != cs && (1== cs.QuickStart))
            {
                //NOP
            }
            else
            {
                SplashScreenForm ssf = new SplashScreenForm();
                ssf.FullName = registeredUser;
                ssf.ShowDialog();
            }
			//System.Threading.Thread.Sleep(500);
					
			

            //ssf.Update();
			//System.Threading.Thread.Sleep(3500);
			//ssf.Close();
			//NEW --
			//TA--:16.05.2005.

			if (null == Globals.activeModules)
			{
				Globals.activeModules = ApplicationHelper.LoadActiveModules();
			}

            /*BEGIN: if PGDatabaseSelectService is present start it and change to automatic*/            
            {
                //PGDatabaseSelectService
                ServiceControllerEx sceMSDE = null;
                try
                {
                    sceMSDE = new ServiceControllerEx(Globals.PGDatabaseSelectService);
                    //Start PGDatabaseSelectService if not running
                    if (!sceMSDE.Status.Equals(ServiceControllerStatus.Running))
                        sceMSDE.Start();
                    //Change startup type to automatic
                    if (sceMSDE.StartupType != "Automatic")
                        sceMSDE.StartupType = "Automatic";
                }
                catch (Exception exc)
                {
                    bool stopHere = true;
                }
                finally
                {
                    sceMSDE.Dispose();
                }
 
            }
            /*END*/

            bool isSrvVersion = true;
			//***Network Version Client - BEGIN***
            if (null != Globals.activeModules && true == Globals.activeModules.MOD_6)
            {
                //MSSQL$PG_DB2
                ServiceControllerEx sceMSDE = null;
                try
                {
                    sceMSDE = new ServiceControllerEx(Globals.MSDE);
                    ////Stop MSSQL$PG_DB2 if running
                    if (sceMSDE.Status.Equals(ServiceControllerStatus.Running))
                        sceMSDE.Stop();
                    //Change startup type to disabled
                    if (sceMSDE.StartupType != "Disabled")
                        sceMSDE.StartupType = "Disabled";
                }
                catch
                { }
                finally
                {
                    sceMSDE.Dispose();
                }

                _miServerInfo.Visible = true;

                //***SWAP KEYCODES TO SRV
                string srvKeyCode = string.Empty;
                srvKeyCode = ApplicationHelper.LoadServerKeyCode();

                if (string.Empty != srvKeyCode)
                {
                    if (null != Globals.DataConsumptionReg)
                        Globals.DataConsumptionReg.KeyCode = srvKeyCode.ToUpper();

                    ServiceDataConsumption sdc = new ServiceDataConsumption();

                    if (string.Empty != Globals.ProfitGrabberWebSupportURLDataConsumption)
                    {
                        sdc.Url = Globals.ProfitGrabberWebSupportURLDataConsumption;
                    }

                    bool queryGuidRes = sdc.QueryGuidFromKey(Globals.DataConsumptionReg.KeyCode, out var userId, out var msg);
                    Globals.UserRegistration.IDUser = userId;
                }
                //***SWAP KEYCODES TO SRV

                //*** DISABLE miSubscribeToData AND	miMyAccount
                this.miSubscribeToData.Visible = false;
                this.miMyAccount.Visible = false;
                //*** DISABLE miSubscribeToData AND	miMyAccount

                //***DISABLE Task2Mail Feature
                _task2EmailMenu.Enabled = false;
                isSrvVersion = false;
                //***DISABLE Task2Mail Feature
            }            
			//***Network Version Client - END***
			
			Globals.ReadURLForWebSupport();
			ApplicationHelper.TestConnection();
			Globals.InitNHibernate();

			//scan for modules...
			this.ScanForUpdateManager();

            //Update Contact data layer
            ContactViewDataLayer.ExpandDataLayer(Connection.getInstance().HatchConn);

            //Update DB Layer for Task2Email Feature
            Task2EmailDataManager.Instance.TestCreateDBLayer(Connection.getInstance().HatchConn);

			//Update Insert Property Item SPL
			ApplicationHelper.TestDatabase_AlterSPL_InsertPropertyItem();

            //create data layer if needed            
            LeadSourceDataManager.Instance.TestCreateDBLayer(Connection.getInstance().HatchConn);
            OfficeStaffDataManager.Instance.TestCreateDBLayer(Connection.getInstance().HatchConn);
            
            //alter task table, add task priority
            ApplicationHelper.AlterTable_ExpandTaskTable();

            //add ShortSale DALC    5.1.1.0
            ApplicationHelper.TestDatabase_CreateShortSaleTable();

            //Expand Users Table if Needed
            UsersTableManager.Instance.TestAlterColumn(Connection.getInstance().HatchConn);

            //Repairs Manager
            Repairs.RepairsManager.Instance.TestCreateDBLayer(Connection.getInstance().HatchConn);

            //Email Blast Support
            ApplicationHelper.TestDatabase_AlterSPL_GetMFWPropertyItemDetails();

            //EMail Blast for AssignTask support
            ApplicationHelper.Database_AlterTable_ExpandMailFaxWizardTable();

            //Expand Users table with new properties used in email blast templates
            ApplicationHelper.Database_AlterTable_Users();

            //CREATE PROCEDURE GetPropertyItemsDescInNodeExt
            ApplicationHelper.TestDatabase_CreateSPL_GetPropertyItemsDescInNodeExt();

            //LockingForm lf = new LockingForm();
            //lf.ShowDialog();

            //Globals.lockObj = new LockingObject(lf.EnableCOMPS, lf.EnableGFR, lf.EnableMergeModule);

            #region � Sasa �
            //sasa, 17.09.05: starts task manager blinking icon thread
            //			Forms.SchedulingActicities.Static.CheckPendingTasksThreadDelegate;
            Forms.SchedulingActicities.Static.StartCheckPendingTasksThread();
			//System.Threading.Thread t = new System.Threading.Thread(new System.Threading.ThreadStart(blinkTaskManagerIcon));
			//t.Start();
			Globals.blinkTaskManager = new System.Threading.Thread(new System.Threading.ThreadStart(blinkTaskManagerIcon));
			Globals.blinkTaskManager.Start();
			#endregion � Sasa �


			this.Closed += new EventHandler(MainForm_Closed);
			this.mi_EmailSS.Click += new EventHandler(EmailSS_EH);

			this.miLabelOffsetSettings.Click += new EventHandler(miLabelOffsetSettings_EH);
			
			if (false == this.TestForLocking())
			{					
				return;
			}

			RegistrationChecker rc = new RegistrationChecker();

			this.miCheckForNewLeads.Enabled = false;
			OneTwoThreeSold webEntries = null;
            AAHSync aahWebLeads = null;
			if (null != Globals.activeModules && Globals.activeModules.MOD_7)		//123Sold
			{
				this.miCheckForNewLeads.Enabled = true;
                //webEntries = new OneTwoThreeSold(OSFolderManager.Instance.GetPlainLocalUserAppDataPath() + @"\AAHIntegration");

                aahWebLeads = new AAHSync(OSFolderManager.Instance.GetPlainLocalUserAppDataPath() + @"\AAHIntegration");
			}

			if (null != Globals.activeModules && Globals.activeModules.MOD_5)	// Matching module !
			{
				this.mi_MatchingModule.Enabled = true;
			}

            _setShortSaleFileFolder.Click += new EventHandler(On_SetShortSaleFileFolder_Click);
            DealMaker.PlainClasses.Utils.ContentCopier.Instance.StartUpPath = Application.StartupPath + @"\SSX";

            _setWorkingFileFolder.Click += new EventHandler(On_SetWorkingFileFolder_Click);
            _selectLogFile.Click += new EventHandler(On_SelectLogFile_Click);
            Util.StoreStartupPath(Application.StartupPath);

            _setCustomSettings.Click += new EventHandler(On_SetCustomSettings_Click);
            _readSSXManual.Click += new EventHandler(On_ReadSSXManual_Click);

            //set user app folder to SSX module
            DataManager.Instance.UserAppFolder = OSFolderManager.Instance.GetPlainLocalUserAppDataPath();

            //Task2Email Event Handlers PROCESSING            
            if (isSrvVersion)  //IF SRV FEATURES, THEN ENABLE Task2Email PROCESSING
            {
                _smtpSetup.Click += new EventHandler(On_SmtpSetup_Click);
                _sendTasksViaEmail.Click += new EventHandler(On_SendTasksViaEmail_Click);
                _viewSentTasksLog.Click += new EventHandler(On_ViewSentTasksLog_Click);
                _viewEmailProcessorConsole.Click += new EventHandler(On_ViewEmailProcessorConsole_Click);

                //***ENABLE Task2Email Processing                
                _task2MailThread = new Thread(new ThreadStart(SendOutEmailsThread));
                _task2MailThread.Start();
                //***ENABLE Task2Email Processing
            }
            
            

// ihuk, 04.12.2004, test menu item
#if EXPORT_TEST
			testMenuItem = new MenuItem("Test");

			 //testMenuItem
			testMenuItem.Click += new EventHandler(TestMenuItemClick);
			menuItem33.MenuItems.Add(new MenuItem("-"));
			menuItem33.MenuItems.Add(testMenuItem);
#endif
		}
        
        // ihuk, 04.12.2004
#if EXPORT_TEST
		private void TestMenuItemClick(object sender, EventArgs e)
		{
			Group group = GroupFactory.GetInstance().GetGroup(30);
			SimpleFilter testFilter = new SimpleFilter("Bedrooms", Globals.IsGreaterThanOrEqualTo, 4);
#if SIMPLE_TEST
			foreach (PropertyItem item in group.PropertyItems)
			{
				PropertyItemAttribute attribute;

				if (null == (attribute = item["Bedrooms"]))
					continue;

				MessageBox.Show(String.Format("{0}: {1}, passed: {2}",
					attribute.AttributeDescription,
					attribute.AttributeValue,
					testFilter.EvaluateObject(item)));
			}
#endif

#if SIMPLE_EXPORT
			PropertyItemsExport export = new PropertyItemsExport(group.PropertyItems);
			export.ExportFileName = "C:\\test_export.csv";
			export.AddFilter(testFilter);
			export.AddMapping(new ExportAttributeMapping("Listing Price", "Listing Price"));
			export.AddMapping(new ExportAttributeMapping("Bedrooms", "Number of bedrooms"));
			export.AddMapping(new ExportAttributeMapping("Bathrooms", "Number of bathrooms"));

			export.Execute();
#endif
			//PropertyItemsExportWizard testWizard = new PropertyItemsExportWizard();
			//testWizard.Execute();

			PropertyItemsExportWizard testWizard = new PropertyItemsExportWizard(group.PropertyItems);
			testWizard.Execute();
		}
#endif

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
                
                Task2EmailWorker.Instance.StopSync = true;
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.appMenu = new System.Windows.Forms.MainMenu(this.components);
            this.menuItem1 = new System.Windows.Forms.MenuItem();
            this.menuItem70 = new System.Windows.Forms.MenuItem();
            this.menuItem76 = new System.Windows.Forms.MenuItem();
            this.menuItem71 = new System.Windows.Forms.MenuItem();
            this.miLabelOffsetSettings = new System.Windows.Forms.MenuItem();
            this.menuItem77 = new System.Windows.Forms.MenuItem();
            this.menuItem17 = new System.Windows.Forms.MenuItem();
            this._setShortSaleFileFolder = new System.Windows.Forms.MenuItem();
            this._setWorkingFileFolder = new System.Windows.Forms.MenuItem();
            this._selectLogFile = new System.Windows.Forms.MenuItem();
            this.menuItem67 = new System.Windows.Forms.MenuItem();
            this._task2EmailMenu = new System.Windows.Forms.MenuItem();
            this._smtpSetup = new System.Windows.Forms.MenuItem();
            this._emailMarketingFooterSetup = new System.Windows.Forms.MenuItem();
            this.menuItem21 = new System.Windows.Forms.MenuItem();
            this._sendTasksViaEmail = new System.Windows.Forms.MenuItem();
            this._viewSentTasksLog = new System.Windows.Forms.MenuItem();
            this._viewEmailProcessorConsole = new System.Windows.Forms.MenuItem();
            this._viewSentEmails = new System.Windows.Forms.MenuItem();
            this.menuItem22 = new System.Windows.Forms.MenuItem();
            this.menuItem72 = new System.Windows.Forms.MenuItem();
            this.menuItem73 = new System.Windows.Forms.MenuItem();
            this.menuItem74 = new System.Windows.Forms.MenuItem();
            this.menuItem78 = new System.Windows.Forms.MenuItem();
            this.menuItem79 = new System.Windows.Forms.MenuItem();
            this.menuItem7 = new System.Windows.Forms.MenuItem();
            this.menuItem66 = new System.Windows.Forms.MenuItem();
            this.menuItem75 = new System.Windows.Forms.MenuItem();
            this.miExit = new System.Windows.Forms.MenuItem();
            this.menuItem5 = new System.Windows.Forms.MenuItem();
            this.menuItem2 = new System.Windows.Forms.MenuItem();
            this.menuItem6 = new System.Windows.Forms.MenuItem();
            this.menuItem4 = new System.Windows.Forms.MenuItem();
            this.menuItem3 = new System.Windows.Forms.MenuItem();
            this.menuItem9 = new System.Windows.Forms.MenuItem();
            this._newBuyerContact = new System.Windows.Forms.MenuItem();
            this._newLenderContact = new System.Windows.Forms.MenuItem();
            this.menuItem12 = new System.Windows.Forms.MenuItem();
            this.menuItem81 = new System.Windows.Forms.MenuItem();
            this.importContacts = new System.Windows.Forms.MenuItem();
            this.menuItem84 = new System.Windows.Forms.MenuItem();
            this.menuItem85 = new System.Windows.Forms.MenuItem();
            this.menuItem86 = new System.Windows.Forms.MenuItem();
            this.menuItem8 = new System.Windows.Forms.MenuItem();
            this.menuItem82 = new System.Windows.Forms.MenuItem();
            this._dealFinder360GetLeads = new System.Windows.Forms.MenuItem();
            this._importRealEFlow = new System.Windows.Forms.MenuItem();
            this.menuItem13 = new System.Windows.Forms.MenuItem();
            this.mi_CreateNewActivity = new System.Windows.Forms.MenuItem();
            this.mi_purgeOldTaks = new System.Windows.Forms.MenuItem();
            this.menuItem19 = new System.Windows.Forms.MenuItem();
            this.menuItem20 = new System.Windows.Forms.MenuItem();
            this.menuItem11 = new System.Windows.Forms.MenuItem();
            this.mi_TaskManager = new System.Windows.Forms.MenuItem();
            this.miCheckForNewLeads = new System.Windows.Forms.MenuItem();
            this.menuItem15 = new System.Windows.Forms.MenuItem();
            this.menuItem23 = new System.Windows.Forms.MenuItem();
            this.menuItem24 = new System.Windows.Forms.MenuItem();
            this.miSubscribeToData = new System.Windows.Forms.MenuItem();
            this.miMyAccount = new System.Windows.Forms.MenuItem();
            this._miAccountUsage = new System.Windows.Forms.MenuItem();
            this.miMyActiveModules = new System.Windows.Forms.MenuItem();
            this._miServerInfo = new System.Windows.Forms.MenuItem();
            this.menuItem33 = new System.Windows.Forms.MenuItem();
            this.menuItem35 = new System.Windows.Forms.MenuItem();
            this.menuItem25 = new System.Windows.Forms.MenuItem();
            this.menuItem26 = new System.Windows.Forms.MenuItem();
            this.menuItem27 = new System.Windows.Forms.MenuItem();
            this.menuItem38 = new System.Windows.Forms.MenuItem();
            this.miDealIQ = new System.Windows.Forms.MenuItem();
            this.miGetFormsReady = new System.Windows.Forms.MenuItem();
            this.miEditMailFaxTemplate = new System.Windows.Forms.MenuItem();
            this._miInterfaxAccountSetup = new System.Windows.Forms.MenuItem();
            this.menuItem10 = new System.Windows.Forms.MenuItem();
            this.miGetAdditionalModules = new System.Windows.Forms.MenuItem();
            this.miGetNewLicences = new System.Windows.Forms.MenuItem();
            this.mi_MatchingModule = new System.Windows.Forms.MenuItem();
            this._shortSaleModule = new System.Windows.Forms.MenuItem();
            this._scanForDuplicates = new System.Windows.Forms.MenuItem();
            this._repairDatabase = new System.Windows.Forms.MenuItem();
            this._setCustomSettings = new System.Windows.Forms.MenuItem();
            this._routing = new System.Windows.Forms.MenuItem();
            this._skipTrace = new System.Windows.Forms.MenuItem();
            this.miBoosters = new System.Windows.Forms.MenuItem();
            this.miCOMPS = new System.Windows.Forms.MenuItem();
            this.miDataAppend = new System.Windows.Forms.MenuItem();
            this.miTestamonials = new System.Windows.Forms.MenuItem();
            this.miLiveAnswers = new System.Windows.Forms.MenuItem();
            this.miSpecialModules = new System.Windows.Forms.MenuItem();
            this.miAboutSystem = new System.Windows.Forms.MenuItem();
            this.menuItem14 = new System.Windows.Forms.MenuItem();
            this.miActivate = new System.Windows.Forms.MenuItem();
            this.menuItem52 = new System.Windows.Forms.MenuItem();
            this.miHowTo = new System.Windows.Forms.MenuItem();
            this._readSSXManual = new System.Windows.Forms.MenuItem();
            this.mi_EmailSS = new System.Windows.Forms.MenuItem();
            this.miVideoLessons = new System.Windows.Forms.MenuItem();
            this.menuItem16 = new System.Windows.Forms.MenuItem();
            this.miAbout = new System.Windows.Forms.MenuItem();
            this._updateKit = new System.Windows.Forms.MenuItem();
            this.miSelectDB = new System.Windows.Forms.MenuItem();
            this.appLeftPanel = new System.Windows.Forms.Panel();
            this.bTaskManager = new System.Windows.Forms.Button();
            this.imgList = new System.Windows.Forms.ImageList(this.components);
            this.bGetFOrmsReady = new System.Windows.Forms.Button();
            this.bReturnMailProcessing = new System.Windows.Forms.Button();
            this.bMailFaxWizard = new System.Windows.Forms.Button();
            this.bFindContact = new System.Windows.Forms.Button();
            this.bAddNewContact = new System.Windows.Forms.Button();
            this.bListView = new System.Windows.Forms.Button();
            this.bGroups = new System.Windows.Forms.Button();
            this.bContacts = new System.Windows.Forms.Button();
            this.bDealIQ = new System.Windows.Forms.Button();
            this.appBodyPanel = new System.Windows.Forms.Panel();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.newImgList = new System.Windows.Forms.ImageList(this.components);
            this.miSelectDatabase = new System.Windows.Forms.MenuItem();
            this.appLeftPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // appMenu
            // 
            this.appMenu.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.menuItem1,
            this.menuItem5,
            this.menuItem3,
            this.menuItem13,
            this.menuItem19,
            this.menuItem15,
            this.menuItem33,
            this.miBoosters,
            this.menuItem52});
            // 
            // menuItem1
            // 
            this.menuItem1.Index = 0;
            this.menuItem1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.menuItem70,
            this.menuItem76,
            this.menuItem71,
            this.miLabelOffsetSettings,
            this.menuItem77,
            this.menuItem17,
            this._setShortSaleFileFolder,
            this._setWorkingFileFolder,
            this._selectLogFile,
            this.menuItem67,
            this._task2EmailMenu,
            this.menuItem22,
            this.menuItem72,
            this.menuItem78,
            this.menuItem79,
            this.menuItem7,
            this.menuItem66,
            this.menuItem75,
            this.miExit});
            this.menuItem1.Text = "File";
            // 
            // menuItem70
            // 
            this.menuItem70.Index = 0;
            this.menuItem70.Text = "User Information";
            this.menuItem70.Click += new System.EventHandler(this.menuItem70_Click);
            // 
            // menuItem76
            // 
            this.menuItem76.Index = 1;
            this.menuItem76.Text = "Printer Setup";
            this.menuItem76.Visible = false;
            this.menuItem76.Click += new System.EventHandler(this.menuItem76_Click);
            // 
            // menuItem71
            // 
            this.menuItem71.Index = 2;
            this.menuItem71.Text = "Fax Setup";
            this.menuItem71.Visible = false;
            // 
            // miLabelOffsetSettings
            // 
            this.miLabelOffsetSettings.Index = 3;
            this.miLabelOffsetSettings.Text = "Label Offset Settings";
            // 
            // menuItem77
            // 
            this.menuItem77.Index = 4;
            this.menuItem77.Text = "BC Scanner Setup";
            this.menuItem77.Click += new System.EventHandler(this.menuItem77_Click);
            // 
            // menuItem17
            // 
            this.menuItem17.Index = 5;
            this.menuItem17.Text = "-";
            // 
            // _setShortSaleFileFolder
            // 
            this._setShortSaleFileFolder.Index = 6;
            this._setShortSaleFileFolder.Text = "Set Short Sale File Folder";
            this._setShortSaleFileFolder.Visible = false;
            // 
            // _setWorkingFileFolder
            // 
            this._setWorkingFileFolder.Index = 7;
            this._setWorkingFileFolder.Text = "Set Working File Folder";
            // 
            // _selectLogFile
            // 
            this._selectLogFile.Index = 8;
            this._selectLogFile.Text = "Select Log File";
            this._selectLogFile.Visible = false;
            // 
            // menuItem67
            // 
            this.menuItem67.Index = 9;
            this.menuItem67.Text = "-";
            this.menuItem67.Visible = false;
            // 
            // _task2EmailMenu
            // 
            this._task2EmailMenu.Index = 10;
            this._task2EmailMenu.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this._smtpSetup,
            this._emailMarketingFooterSetup,
            this.menuItem21,
            this._sendTasksViaEmail,
            this._viewSentTasksLog,
            this._viewEmailProcessorConsole,
            this._viewSentEmails});
            this._task2EmailMenu.Text = "Email Setup";
            // 
            // _smtpSetup
            // 
            this._smtpSetup.Index = 0;
            this._smtpSetup.Text = "Gmail Setup";
            // 
            // _emailMarketingFooterSetup
            // 
            this._emailMarketingFooterSetup.Index = 1;
            this._emailMarketingFooterSetup.Text = "Email Marketing - Setup Email Signatures";
            this._emailMarketingFooterSetup.Click += new System.EventHandler(this._emailMarketingFooterSetup_Click);
            // 
            // menuItem21
            // 
            this.menuItem21.Index = 2;
            this.menuItem21.Text = "-";
            // 
            // _sendTasksViaEmail
            // 
            this._sendTasksViaEmail.Index = 3;
            this._sendTasksViaEmail.Text = "Send Tasks via Email / Text Now";
            // 
            // _viewSentTasksLog
            // 
            this._viewSentTasksLog.Index = 4;
            this._viewSentTasksLog.Text = "View Sent Tasks Log";
            this._viewSentTasksLog.Visible = false;
            // 
            // _viewEmailProcessorConsole
            // 
            this._viewEmailProcessorConsole.Index = 5;
            this._viewEmailProcessorConsole.Text = "View Email Processor Console";
            // 
            // _viewSentEmails
            // 
            this._viewSentEmails.Index = 6;
            this._viewSentEmails.Text = "View Sent Emails";
            this._viewSentEmails.Click += new System.EventHandler(this._viewSentEmails_Click);
            // 
            // menuItem22
            // 
            this.menuItem22.Index = 11;
            this.menuItem22.Text = "-";
            this.menuItem22.Visible = false;
            // 
            // menuItem72
            // 
            this.menuItem72.Index = 12;
            this.menuItem72.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.menuItem73,
            this.menuItem74});
            this.menuItem72.Text = "Maintenance";
            this.menuItem72.Visible = false;
            // 
            // menuItem73
            // 
            this.menuItem73.Index = 0;
            this.menuItem73.Text = "ReIndex";
            // 
            // menuItem74
            // 
            this.menuItem74.Index = 1;
            this.menuItem74.Text = "Purge";
            // 
            // menuItem78
            // 
            this.menuItem78.Index = 13;
            this.menuItem78.Text = "Print";
            this.menuItem78.Visible = false;
            // 
            // menuItem79
            // 
            this.menuItem79.Index = 14;
            this.menuItem79.Text = "Backup";
            this.menuItem79.Visible = false;
            // 
            // menuItem7
            // 
            this.menuItem7.Index = 15;
            this.menuItem7.Text = "-";
            this.menuItem7.Visible = false;
            // 
            // menuItem66
            // 
            this.menuItem66.Index = 16;
            this.menuItem66.Text = "Register";
            this.menuItem66.Visible = false;
            // 
            // menuItem75
            // 
            this.menuItem75.Index = 17;
            this.menuItem75.Text = "-";
            // 
            // miExit
            // 
            this.miExit.Index = 18;
            this.miExit.Text = "Exit";
            this.miExit.Click += new System.EventHandler(this.miExit_Click);
            // 
            // menuItem5
            // 
            this.menuItem5.Index = 1;
            this.menuItem5.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.menuItem2,
            this.menuItem6,
            this.menuItem4});
            this.menuItem5.Text = "LookUp";
            // 
            // menuItem2
            // 
            this.menuItem2.Index = 0;
            this.menuItem2.Text = "Select Group";
            this.menuItem2.Click += new System.EventHandler(this.bGroups_Click);
            // 
            // menuItem6
            // 
            this.menuItem6.Index = 1;
            this.menuItem6.Text = "Contact View";
            this.menuItem6.Click += new System.EventHandler(this.bContacts_Click);
            // 
            // menuItem4
            // 
            this.menuItem4.Index = 2;
            this.menuItem4.Text = "List View";
            this.menuItem4.Click += new System.EventHandler(this.bListView_Click);
            // 
            // menuItem3
            // 
            this.menuItem3.Index = 2;
            this.menuItem3.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.menuItem9,
            this._newBuyerContact,
            this._newLenderContact,
            this.menuItem12,
            this.menuItem81,
            this.importContacts,
            this.menuItem84,
            this.menuItem85,
            this.menuItem86,
            this.menuItem8,
            this.menuItem82,
            this._dealFinder360GetLeads,
            this._importRealEFlow});
            this.menuItem3.Text = "Contacts";
            // 
            // menuItem9
            // 
            this.menuItem9.Index = 0;
            this.menuItem9.Text = "New Seller Contact ";
            this.menuItem9.Click += new System.EventHandler(this.bAddNewContact_Click);
            // 
            // _newBuyerContact
            // 
            this._newBuyerContact.Index = 1;
            this._newBuyerContact.Text = "New Buyer Contact";
            // 
            // _newLenderContact
            // 
            this._newLenderContact.Index = 2;
            this._newLenderContact.Text = "New Lender Contact";
            // 
            // menuItem12
            // 
            this.menuItem12.Index = 3;
            this.menuItem12.Text = "-";
            // 
            // menuItem81
            // 
            this.menuItem81.Index = 4;
            this.menuItem81.Text = "Find Contacts";
            this.menuItem81.Click += new System.EventHandler(this.button1_Click);
            // 
            // importContacts
            // 
            this.importContacts.Index = 5;
            this.importContacts.Text = "Import Contacts";
            this.importContacts.Click += new System.EventHandler(this.importContacts_Click);
            // 
            // menuItem84
            // 
            this.menuItem84.Index = 6;
            this.menuItem84.Text = "Export Contacts";
            this.menuItem84.Click += new System.EventHandler(this.menuItem84_Click);
            // 
            // menuItem85
            // 
            this.menuItem85.Index = 7;
            this.menuItem85.Text = "Merge Groups";
            this.menuItem85.Visible = false;
            // 
            // menuItem86
            // 
            this.menuItem86.Index = 8;
            this.menuItem86.Text = "Append Groups";
            this.menuItem86.Visible = false;
            // 
            // menuItem8
            // 
            this.menuItem8.Index = 9;
            this.menuItem8.Text = "Create New Group";
            this.menuItem8.Visible = false;
            // 
            // menuItem82
            // 
            this.menuItem82.Index = 10;
            this.menuItem82.Text = "Delete Contact";
            this.menuItem82.Visible = false;
            // 
            // _dealFinder360GetLeads
            // 
            this._dealFinder360GetLeads.Index = 11;
            this._dealFinder360GetLeads.Text = "DealFinder360 - Get Leads";
            // 
            // _importRealEFlow
            // 
            this._importRealEFlow.Index = 12;
            this._importRealEFlow.Text = "DealFinder360 - Import Leads";
            // 
            // menuItem13
            // 
            this.menuItem13.Index = 3;
            this.menuItem13.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mi_CreateNewActivity,
            this.mi_purgeOldTaks});
            this.menuItem13.Text = "Scheduling";
            // 
            // mi_CreateNewActivity
            // 
            this.mi_CreateNewActivity.Index = 0;
            this.mi_CreateNewActivity.Text = "Create New Activity";
            this.mi_CreateNewActivity.Click += new System.EventHandler(this.mi_CreateNewActivity_Click);
            // 
            // mi_purgeOldTaks
            // 
            this.mi_purgeOldTaks.Index = 1;
            this.mi_purgeOldTaks.Text = "Purge Old Tasks";
            this.mi_purgeOldTaks.Click += new System.EventHandler(this.mi_purgeOldTaks_Click);
            // 
            // menuItem19
            // 
            this.menuItem19.Index = 4;
            this.menuItem19.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.menuItem20,
            this.menuItem11,
            this.mi_TaskManager,
            this.miCheckForNewLeads});
            this.menuItem19.Text = "Manager";
            // 
            // menuItem20
            // 
            this.menuItem20.Index = 0;
            this.menuItem20.Text = "Mail / Email / Fax Wizard";
            this.menuItem20.Click += new System.EventHandler(this.menuItem20_Click);
            // 
            // menuItem11
            // 
            this.menuItem11.Index = 1;
            this.menuItem11.Text = "Return Mail Wizard";
            this.menuItem11.Click += new System.EventHandler(this.button3_Click);
            // 
            // mi_TaskManager
            // 
            this.mi_TaskManager.Index = 2;
            this.mi_TaskManager.Text = "Task Manager";
            this.mi_TaskManager.Click += new System.EventHandler(this.mi_TaskManager_Click);
            // 
            // miCheckForNewLeads
            // 
            this.miCheckForNewLeads.Enabled = false;
            this.miCheckForNewLeads.Index = 3;
            this.miCheckForNewLeads.Text = "Check For New Leads";
            this.miCheckForNewLeads.Click += new System.EventHandler(this.miCheckForNewLeads_Click);
            // 
            // menuItem15
            // 
            this.menuItem15.Index = 5;
            this.menuItem15.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.menuItem23,
            this.menuItem24,
            this.miSubscribeToData,
            this.miMyAccount,
            this._miAccountUsage,
            this.miMyActiveModules,
            this._miServerInfo});
            this.menuItem15.Text = "Property Data";
            // 
            // menuItem23
            // 
            this.menuItem23.Index = 0;
            this.menuItem23.Text = "Get Property Profile";
            this.menuItem23.Visible = false;
            // 
            // menuItem24
            // 
            this.menuItem24.Index = 1;
            this.menuItem24.Text = "Get COMPS";
            this.menuItem24.Visible = false;
            // 
            // miSubscribeToData
            // 
            this.miSubscribeToData.Index = 2;
            this.miSubscribeToData.Text = "Subscribe to Data";
            this.miSubscribeToData.Click += new System.EventHandler(this.miSubscribeToData_Click);
            // 
            // miMyAccount
            // 
            this.miMyAccount.Index = 3;
            this.miMyAccount.Text = "My Account";
            this.miMyAccount.Click += new System.EventHandler(this.miMyAccount_Click);
            // 
            // _miAccountUsage
            // 
            this._miAccountUsage.Index = 4;
            this._miAccountUsage.Text = "My Account Details ";
            // 
            // miMyActiveModules
            // 
            this.miMyActiveModules.Index = 5;
            this.miMyActiveModules.Text = "My Active Modules";
            this.miMyActiveModules.Click += new System.EventHandler(this.miMyActiveModules_Click);
            // 
            // _miServerInfo
            // 
            this._miServerInfo.Index = 6;
            this._miServerInfo.Text = "Server Info";
            this._miServerInfo.Visible = false;
            this._miServerInfo.Click += new System.EventHandler(this._miServerInfo_Click);
            // 
            // menuItem33
            // 
            this.menuItem33.Index = 6;
            this.menuItem33.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.menuItem35,
            this.menuItem38,
            this.miDealIQ,
            this.miGetFormsReady,
            this.miEditMailFaxTemplate,
            this._miInterfaxAccountSetup,
            this.menuItem10,
            this.miGetAdditionalModules,
            this.miGetNewLicences,
            this.mi_MatchingModule,
            this._shortSaleModule,
            this._scanForDuplicates,
            this._repairDatabase,
            this._setCustomSettings,
            this._routing,
            this._skipTrace});
            this.menuItem33.Text = "Tools";
            // 
            // menuItem35
            // 
            this.menuItem35.Index = 0;
            this.menuItem35.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.menuItem25,
            this.menuItem26,
            this.menuItem27});
            this.menuItem35.Text = "Return Mail Processing";
            this.menuItem35.Visible = false;
            // 
            // menuItem25
            // 
            this.menuItem25.Index = 0;
            this.menuItem25.Text = "Scan In Return Mail";
            // 
            // menuItem26
            // 
            this.menuItem26.Index = 1;
            this.menuItem26.Text = "Export To Skiptracer";
            // 
            // menuItem27
            // 
            this.menuItem27.Index = 2;
            this.menuItem27.Text = "Import From SkipTracer";
            // 
            // menuItem38
            // 
            this.menuItem38.Index = 1;
            this.menuItem38.Text = "Loan Calculator";
            this.menuItem38.Visible = false;
            // 
            // miDealIQ
            // 
            this.miDealIQ.Index = 2;
            this.miDealIQ.Text = "Deal IQ";
            this.miDealIQ.Click += new System.EventHandler(this.AcqCalcEnter);
            // 
            // miGetFormsReady
            // 
            this.miGetFormsReady.Index = 3;
            this.miGetFormsReady.Text = "Get Forms Ready";
            this.miGetFormsReady.Click += new System.EventHandler(this.button5_Click);
            // 
            // miEditMailFaxTemplate
            // 
            this.miEditMailFaxTemplate.Index = 4;
            this.miEditMailFaxTemplate.Text = "Edit Mail / Email / Fax Template";
            this.miEditMailFaxTemplate.Click += new System.EventHandler(this.menuItem10_Click);
            // 
            // _miInterfaxAccountSetup
            // 
            this._miInterfaxAccountSetup.Index = 5;
            this._miInterfaxAccountSetup.Text = "Interfax Account Setup";
            this._miInterfaxAccountSetup.Click += new System.EventHandler(this._miInterfaxAccountSetup_Click);
            // 
            // menuItem10
            // 
            this.menuItem10.Index = 6;
            this.menuItem10.Text = "-";
            this.menuItem10.Visible = false;
            // 
            // miGetAdditionalModules
            // 
            this.miGetAdditionalModules.Index = 7;
            this.miGetAdditionalModules.Text = "Get Additional Modules";
            this.miGetAdditionalModules.Visible = false;
            this.miGetAdditionalModules.Click += new System.EventHandler(this.miGetAdditionalModules_Click);
            // 
            // miGetNewLicences
            // 
            this.miGetNewLicences.Index = 8;
            this.miGetNewLicences.Text = "Get New Licences";
            this.miGetNewLicences.Visible = false;
            this.miGetNewLicences.Click += new System.EventHandler(this.miGetNewLicences_Click);
            // 
            // mi_MatchingModule
            // 
            this.mi_MatchingModule.Enabled = false;
            this.mi_MatchingModule.Index = 9;
            this.mi_MatchingModule.Text = "Matching Module";
            this.mi_MatchingModule.Click += new System.EventHandler(this.mi_MatchingModule_Click);
            // 
            // _shortSaleModule
            // 
            this._shortSaleModule.Index = 10;
            this._shortSaleModule.Text = "Short Sale Module";
            this._shortSaleModule.Visible = false;
            // 
            // _scanForDuplicates
            // 
            this._scanForDuplicates.Index = 11;
            this._scanForDuplicates.Text = "Scan For Duplicates";
            // 
            // _repairDatabase
            // 
            this._repairDatabase.Index = 12;
            this._repairDatabase.Text = "Repair Database";
            // 
            // _setCustomSettings
            // 
            this._setCustomSettings.Index = 13;
            this._setCustomSettings.Text = "Set Custom Settings";
            // 
            // _routing
            // 
            this._routing.Index = 14;
            this._routing.Text = "Door-Knocking Module";
            this._routing.Click += new System.EventHandler(this._routing_Click);
            // 
            // _skipTrace
            // 
            this._skipTrace.Index = 15;
            this._skipTrace.Text = "Get Phone && Email - Skip Trace";
            this._skipTrace.Click += new System.EventHandler(this._skipTrace_Click);
            // 
            // miBoosters
            // 
            this.miBoosters.Index = 7;
            this.miBoosters.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.miCOMPS,
            this.miDataAppend,
            this.miTestamonials,
            this.miLiveAnswers,
            this.miSpecialModules,
            this.miAboutSystem,
            this.menuItem14,
            this.miActivate});
            this.miBoosters.Text = "Profit Boosters";
            // 
            // miCOMPS
            // 
            this.miCOMPS.Index = 0;
            this.miCOMPS.Text = "Get Instant COMPS - Subscribe to Property Data";
            this.miCOMPS.Click += new System.EventHandler(this.miCOMPS_Click);
            // 
            // miDataAppend
            // 
            this.miDataAppend.Index = 1;
            this.miDataAppend.Text = "Get Phone && Email - Subscribe to Skip Tracing";
            this.miDataAppend.Click += new System.EventHandler(this.miDataAppend_Click);
            // 
            // miTestamonials
            // 
            this.miTestamonials.Index = 2;
            this.miTestamonials.Text = "Create Credibility With Sellers\' Voice Testimonials";
            this.miTestamonials.Visible = false;
            this.miTestamonials.Click += new System.EventHandler(this.miTestamonials_Click);
            // 
            // miLiveAnswers
            // 
            this.miLiveAnswers.Index = 3;
            this.miLiveAnswers.Text = "Get Live Answer - Subscribe to Phone Service";
            this.miLiveAnswers.Click += new System.EventHandler(this.miLiveAnswers_Click);
            // 
            // miSpecialModules
            // 
            this.miSpecialModules.Index = 4;
            this.miSpecialModules.Text = "Money Making Specialty Modules";
            this.miSpecialModules.Visible = false;
            this.miSpecialModules.Click += new System.EventHandler(this.miSpecialModules_Click);
            // 
            // miAboutSystem
            // 
            this.miAboutSystem.Index = 5;
            this.miAboutSystem.Text = "About Niche2Wealth System";
            this.miAboutSystem.Visible = false;
            this.miAboutSystem.Click += new System.EventHandler(this.miAboutSystem_Click);
            // 
            // menuItem14
            // 
            this.menuItem14.Index = 6;
            this.menuItem14.Text = "-";
            // 
            // miActivate
            // 
            this.miActivate.Index = 7;
            this.miActivate.Text = "Activate Specialty Module";
            this.miActivate.Click += new System.EventHandler(this.miActivate_Click);
            // 
            // menuItem52
            // 
            this.menuItem52.Index = 8;
            this.menuItem52.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.miHowTo,
            this._readSSXManual,
            this.mi_EmailSS,
            this.miVideoLessons,
            this.menuItem16,
            this.miAbout,
            this._updateKit});
            this.menuItem52.Text = "Help";
            // 
            // miHowTo
            // 
            this.miHowTo.Index = 0;
            this.miHowTo.Text = "Read ProfitGrabber Pro User Guide Online";
            this.miHowTo.Click += new System.EventHandler(this.miHowTo_Click);
            // 
            // _readSSXManual
            // 
            this._readSSXManual.Index = 1;
            this._readSSXManual.Text = "Read Short Sale Excelerator User Guide Online";
            // 
            // mi_EmailSS
            // 
            this.mi_EmailSS.Index = 2;
            this.mi_EmailSS.Text = "Contact Support";
            // 
            // miVideoLessons
            // 
            this.miVideoLessons.Index = 3;
            this.miVideoLessons.Text = "Video Training";
            this.miVideoLessons.Click += new System.EventHandler(this.miVideoLessons_Click);
            // 
            // menuItem16
            // 
            this.menuItem16.Index = 4;
            this.menuItem16.Text = "-";
            // 
            // miAbout
            // 
            this.miAbout.Index = 5;
            this.miAbout.Text = "About";
            this.miAbout.Click += new System.EventHandler(this.miAbout_Click);
            // 
            // _updateKit
            // 
            this._updateKit.Index = 6;
            this._updateKit.Text = "Update Kit\'s Objects";
            this._updateKit.Visible = false;
            this._updateKit.Click += new System.EventHandler(this._updateKit_Click);
            // 
            // miSelectDB
            // 
            this.miSelectDB.Index = -1;
            this.miSelectDB.Text = "Select Database";
            this.miSelectDB.Click += new System.EventHandler(this.miSelectDatabase_Click);
            // 
            // appLeftPanel
            // 
            this.appLeftPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.appLeftPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.appLeftPanel.Controls.Add(this.bTaskManager);
            this.appLeftPanel.Controls.Add(this.bGetFOrmsReady);
            this.appLeftPanel.Controls.Add(this.bReturnMailProcessing);
            this.appLeftPanel.Controls.Add(this.bMailFaxWizard);
            this.appLeftPanel.Controls.Add(this.bFindContact);
            this.appLeftPanel.Controls.Add(this.bAddNewContact);
            this.appLeftPanel.Controls.Add(this.bListView);
            this.appLeftPanel.Controls.Add(this.bGroups);
            this.appLeftPanel.Controls.Add(this.bContacts);
            this.appLeftPanel.Controls.Add(this.bDealIQ);
            this.appLeftPanel.Dock = System.Windows.Forms.DockStyle.Left;
            this.appLeftPanel.Location = new System.Drawing.Point(0, 0);
            this.appLeftPanel.Name = "appLeftPanel";
            this.appLeftPanel.Size = new System.Drawing.Size(104, 840);
            this.appLeftPanel.TabIndex = 1;
            this.appLeftPanel.Paint += new System.Windows.Forms.PaintEventHandler(this.appLeftPanel_Paint);
            // 
            // bTaskManager
            // 
            this.bTaskManager.BackColor = System.Drawing.Color.Transparent;
            this.bTaskManager.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bTaskManager.ImageIndex = 18;
            this.bTaskManager.ImageList = this.imgList;
            this.bTaskManager.Location = new System.Drawing.Point(7, 524);
            this.bTaskManager.Name = "bTaskManager";
            this.bTaskManager.Size = new System.Drawing.Size(55, 48);
            this.bTaskManager.TabIndex = 8;
            this.bTaskManager.TabStop = false;
            this.toolTip1.SetToolTip(this.bTaskManager, "Task Manager");
            this.bTaskManager.UseVisualStyleBackColor = false;
            this.bTaskManager.Click += new System.EventHandler(this.bTaskManager_Click);
            this.bTaskManager.MouseEnter += new System.EventHandler(this.bTaskManager_MouseEnter);
            this.bTaskManager.MouseLeave += new System.EventHandler(this.bTaskManager_MouseLeave);
            // 
            // imgList
            // 
            this.imgList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imgList.ImageStream")));
            this.imgList.TransparentColor = System.Drawing.Color.Transparent;
            this.imgList.Images.SetKeyName(0, "");
            this.imgList.Images.SetKeyName(1, "");
            this.imgList.Images.SetKeyName(2, "");
            this.imgList.Images.SetKeyName(3, "");
            this.imgList.Images.SetKeyName(4, "");
            this.imgList.Images.SetKeyName(5, "");
            this.imgList.Images.SetKeyName(6, "");
            this.imgList.Images.SetKeyName(7, "");
            this.imgList.Images.SetKeyName(8, "");
            this.imgList.Images.SetKeyName(9, "");
            this.imgList.Images.SetKeyName(10, "");
            this.imgList.Images.SetKeyName(11, "");
            this.imgList.Images.SetKeyName(12, "");
            this.imgList.Images.SetKeyName(13, "");
            this.imgList.Images.SetKeyName(14, "");
            this.imgList.Images.SetKeyName(15, "");
            this.imgList.Images.SetKeyName(16, "");
            this.imgList.Images.SetKeyName(17, "");
            this.imgList.Images.SetKeyName(18, "");
            this.imgList.Images.SetKeyName(19, "");
            this.imgList.Images.SetKeyName(20, "");
            this.imgList.Images.SetKeyName(21, "");
            this.imgList.Images.SetKeyName(22, "");
            this.imgList.Images.SetKeyName(23, "");
            // 
            // bGetFOrmsReady
            // 
            this.bGetFOrmsReady.BackColor = System.Drawing.Color.Transparent;
            this.bGetFOrmsReady.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bGetFOrmsReady.ImageIndex = 6;
            this.bGetFOrmsReady.ImageList = this.imgList;
            this.bGetFOrmsReady.Location = new System.Drawing.Point(7, 467);
            this.bGetFOrmsReady.Name = "bGetFOrmsReady";
            this.bGetFOrmsReady.Size = new System.Drawing.Size(55, 48);
            this.bGetFOrmsReady.TabIndex = 7;
            this.bGetFOrmsReady.TabStop = false;
            this.toolTip1.SetToolTip(this.bGetFOrmsReady, "Get Forms Ready");
            this.bGetFOrmsReady.UseVisualStyleBackColor = false;
            this.bGetFOrmsReady.Click += new System.EventHandler(this.button5_Click);
            this.bGetFOrmsReady.MouseEnter += new System.EventHandler(this.bGetFOrmsReady_MouseEnter);
            this.bGetFOrmsReady.MouseLeave += new System.EventHandler(this.bGetFOrmsReady_MouseLeave);
            // 
            // bReturnMailProcessing
            // 
            this.bReturnMailProcessing.BackColor = System.Drawing.Color.Transparent;
            this.bReturnMailProcessing.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bReturnMailProcessing.ImageIndex = 14;
            this.bReturnMailProcessing.ImageList = this.imgList;
            this.bReturnMailProcessing.Location = new System.Drawing.Point(7, 352);
            this.bReturnMailProcessing.Name = "bReturnMailProcessing";
            this.bReturnMailProcessing.Size = new System.Drawing.Size(55, 48);
            this.bReturnMailProcessing.TabIndex = 6;
            this.bReturnMailProcessing.TabStop = false;
            this.toolTip1.SetToolTip(this.bReturnMailProcessing, "Return Mail Processing");
            this.bReturnMailProcessing.UseVisualStyleBackColor = false;
            this.bReturnMailProcessing.Click += new System.EventHandler(this.button3_Click);
            this.bReturnMailProcessing.MouseEnter += new System.EventHandler(this.bReturnMailProcessing_MouseEnter);
            this.bReturnMailProcessing.MouseLeave += new System.EventHandler(this.bReturnMailProcessing_MouseLeave);
            // 
            // bMailFaxWizard
            // 
            this.bMailFaxWizard.BackColor = System.Drawing.Color.Transparent;
            this.bMailFaxWizard.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bMailFaxWizard.ImageIndex = 10;
            this.bMailFaxWizard.ImageList = this.imgList;
            this.bMailFaxWizard.Location = new System.Drawing.Point(7, 295);
            this.bMailFaxWizard.Name = "bMailFaxWizard";
            this.bMailFaxWizard.Size = new System.Drawing.Size(55, 47);
            this.bMailFaxWizard.TabIndex = 5;
            this.bMailFaxWizard.TabStop = false;
            this.toolTip1.SetToolTip(this.bMailFaxWizard, "Mail / Email / Fax Wizard");
            this.bMailFaxWizard.UseVisualStyleBackColor = false;
            this.bMailFaxWizard.Click += new System.EventHandler(this.button2_Click);
            this.bMailFaxWizard.MouseEnter += new System.EventHandler(this.bMailFaxWizard_MouseEnter);
            this.bMailFaxWizard.MouseLeave += new System.EventHandler(this.bMailFaxWizard_MouseLeave);
            // 
            // bFindContact
            // 
            this.bFindContact.BackColor = System.Drawing.Color.Transparent;
            this.bFindContact.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bFindContact.ImageIndex = 4;
            this.bFindContact.ImageList = this.imgList;
            this.bFindContact.Location = new System.Drawing.Point(7, 237);
            this.bFindContact.Name = "bFindContact";
            this.bFindContact.Size = new System.Drawing.Size(55, 48);
            this.bFindContact.TabIndex = 4;
            this.bFindContact.TabStop = false;
            this.toolTip1.SetToolTip(this.bFindContact, "Find Contact");
            this.bFindContact.UseVisualStyleBackColor = false;
            this.bFindContact.Click += new System.EventHandler(this.button1_Click);
            this.bFindContact.MouseEnter += new System.EventHandler(this.bFindContact_MouseEnter);
            this.bFindContact.MouseLeave += new System.EventHandler(this.bFindContact_MouseLeave);
            // 
            // bAddNewContact
            // 
            this.bAddNewContact.BackColor = System.Drawing.Color.Transparent;
            this.bAddNewContact.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bAddNewContact.ImageIndex = 12;
            this.bAddNewContact.ImageList = this.imgList;
            this.bAddNewContact.Location = new System.Drawing.Point(7, 179);
            this.bAddNewContact.Name = "bAddNewContact";
            this.bAddNewContact.Size = new System.Drawing.Size(55, 49);
            this.bAddNewContact.TabIndex = 3;
            this.bAddNewContact.TabStop = false;
            this.toolTip1.SetToolTip(this.bAddNewContact, "New Contact");
            this.bAddNewContact.UseVisualStyleBackColor = false;
            this.bAddNewContact.Click += new System.EventHandler(this.bAddNewContact_Click);
            this.bAddNewContact.MouseEnter += new System.EventHandler(this.bImport_MouseEnter);
            this.bAddNewContact.MouseLeave += new System.EventHandler(this.bImport_MouseLeave);
            // 
            // bListView
            // 
            this.bListView.BackColor = System.Drawing.Color.Transparent;
            this.bListView.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bListView.ImageIndex = 8;
            this.bListView.ImageList = this.imgList;
            this.bListView.Location = new System.Drawing.Point(7, 122);
            this.bListView.Name = "bListView";
            this.bListView.Size = new System.Drawing.Size(55, 49);
            this.bListView.TabIndex = 2;
            this.bListView.TabStop = false;
            this.toolTip1.SetToolTip(this.bListView, "List View");
            this.bListView.UseVisualStyleBackColor = false;
            this.bListView.Click += new System.EventHandler(this.bListView_Click);
            this.bListView.MouseEnter += new System.EventHandler(this.bListView_MouseEnter);
            this.bListView.MouseLeave += new System.EventHandler(this.bListView_MouseLeave);
            // 
            // bGroups
            // 
            this.bGroups.BackColor = System.Drawing.Color.Transparent;
            this.bGroups.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bGroups.ImageIndex = 16;
            this.bGroups.ImageList = this.imgList;
            this.bGroups.Location = new System.Drawing.Point(7, 7);
            this.bGroups.Name = "bGroups";
            this.bGroups.Size = new System.Drawing.Size(55, 48);
            this.bGroups.TabIndex = 1;
            this.bGroups.TabStop = false;
            this.toolTip1.SetToolTip(this.bGroups, "Groups");
            this.bGroups.UseVisualStyleBackColor = false;
            this.bGroups.Click += new System.EventHandler(this.bGroups_Click);
            this.bGroups.MouseEnter += new System.EventHandler(this.bGroups_MouseEnter);
            this.bGroups.MouseLeave += new System.EventHandler(this.bGroups_MouseLeave);
            // 
            // bContacts
            // 
            this.bContacts.BackColor = System.Drawing.Color.Transparent;
            this.bContacts.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bContacts.ImageIndex = 2;
            this.bContacts.ImageList = this.imgList;
            this.bContacts.Location = new System.Drawing.Point(7, 65);
            this.bContacts.Name = "bContacts";
            this.bContacts.Size = new System.Drawing.Size(55, 48);
            this.bContacts.TabIndex = 0;
            this.bContacts.TabStop = false;
            this.toolTip1.SetToolTip(this.bContacts, "Contact View");
            this.bContacts.UseVisualStyleBackColor = false;
            this.bContacts.Click += new System.EventHandler(this.bContacts_Click);
            this.bContacts.MouseEnter += new System.EventHandler(this.bContacts_MouseEnter);
            this.bContacts.MouseLeave += new System.EventHandler(this.bContacts_MouseLeave);
            // 
            // bDealIQ
            // 
            this.bDealIQ.BackColor = System.Drawing.Color.Transparent;
            this.bDealIQ.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bDealIQ.ImageIndex = 0;
            this.bDealIQ.ImageList = this.imgList;
            this.bDealIQ.Location = new System.Drawing.Point(7, 409);
            this.bDealIQ.Name = "bDealIQ";
            this.bDealIQ.Size = new System.Drawing.Size(55, 49);
            this.bDealIQ.TabIndex = 4;
            this.bDealIQ.TabStop = false;
            this.toolTip1.SetToolTip(this.bDealIQ, "Deal IQ");
            this.bDealIQ.UseVisualStyleBackColor = false;
            this.bDealIQ.Click += new System.EventHandler(this.AcqCalcEnter);
            this.bDealIQ.MouseEnter += new System.EventHandler(this.bDealIQ_MouseEnter);
            this.bDealIQ.MouseLeave += new System.EventHandler(this.bDealIQ_MouseLeave);
            // 
            // appBodyPanel
            // 
            this.appBodyPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.appBodyPanel.Location = new System.Drawing.Point(104, 0);
            this.appBodyPanel.Name = "appBodyPanel";
            this.appBodyPanel.Size = new System.Drawing.Size(1320, 840);
            this.appBodyPanel.TabIndex = 3;
            // 
            // toolTip1
            // 
            this.toolTip1.AutomaticDelay = 1;
            this.toolTip1.AutoPopDelay = 10000;
            this.toolTip1.InitialDelay = 1;
            this.toolTip1.ReshowDelay = 0;
            // 
            // newImgList
            // 
            this.newImgList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("newImgList.ImageStream")));
            this.newImgList.TransparentColor = System.Drawing.Color.Transparent;
            this.newImgList.Images.SetKeyName(0, "");
            this.newImgList.Images.SetKeyName(1, "");
            this.newImgList.Images.SetKeyName(2, "");
            this.newImgList.Images.SetKeyName(3, "");
            this.newImgList.Images.SetKeyName(4, "");
            this.newImgList.Images.SetKeyName(5, "");
            this.newImgList.Images.SetKeyName(6, "");
            this.newImgList.Images.SetKeyName(7, "");
            this.newImgList.Images.SetKeyName(8, "");
            this.newImgList.Images.SetKeyName(9, "");
            this.newImgList.Images.SetKeyName(10, "");
            this.newImgList.Images.SetKeyName(11, "");
            this.newImgList.Images.SetKeyName(12, "");
            this.newImgList.Images.SetKeyName(13, "");
            this.newImgList.Images.SetKeyName(14, "");
            this.newImgList.Images.SetKeyName(15, "");
            // 
            // miSelectDatabase
            // 
            this.miSelectDatabase.Index = -1;
            this.miSelectDatabase.Text = "Select Database";
            this.miSelectDatabase.Click += new System.EventHandler(this.miSelectDatabase_Click);
            // 
            // MainForm
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(1424, 840);
            this.Controls.Add(this.appBodyPanel);
            this.Controls.Add(this.appLeftPanel);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximumSize = new System.Drawing.Size(1440, 900);
            this.Menu = this.appMenu;
            this.MinimumSize = new System.Drawing.Size(738, 439);
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ProfitGrabber Pro";
            this.Closing += new System.ComponentModel.CancelEventHandler(this.MainForm_Closing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.appLeftPanel.ResumeLayout(false);
            this.ResumeLayout(false);

		}
		#endregion


        [DllImport("user32.dll")]
        private static extern
            bool SetForegroundWindow(IntPtr hWnd);
        [DllImport("user32.dll")]
        private static extern
            bool ShowWindowAsync(IntPtr hWnd, int nCmdShow);
        [DllImport("user32.dll")]
        private static extern
            bool IsIconic(IntPtr hWnd);

        private const int SW_HIDE = 0;
        private const int SW_SHOWNORMAL = 1;
        private const int SW_SHOWMINIMIZED = 2;
        private const int SW_SHOWMAXIMIZED = 3;
        private const int SW_SHOWNOACTIVATE = 4;
        private const int SW_RESTORE = 9;
        private const int SW_SHOWDEFAULT = 10;


		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
            // get the name of our process
            string proc = Process.GetCurrentProcess().ProcessName;
            // get the list of all processes by that name
            Process[] processes = Process.GetProcessesByName(proc);

            bool allowMultipleInstances = false;
            try
            {
                string temp = ConfigurationManager.AppSettings["AllowMultipleInstances"];
                allowMultipleInstances = Convert.ToBoolean(temp);
            }
            catch
            {
                allowMultipleInstances = false;
            }
            

            // if there is more than one process...
            if (processes.Length > 1 && !allowMultipleInstances)
            {
                // Assume there is our process, which we will terminate, 
                // and the other process, which we want to bring to the 
                // foreground. This assumes there are only two processes 
                // in the processes array, and we need to find out which 
                // one is NOT us.

                // get our process
                Process p = Process.GetCurrentProcess();
                int n = 0;        // assume the other process is at index 0
                // if this process id is OUR process ID...
                if (processes[0].Id == p.Id)
                {
                    // then the other process is at index 1
                    n = 1;
                }
                // get the window handle
                IntPtr hWnd = processes[n].MainWindowHandle;
                // if iconic, we need to restore the window
                if (IsIconic(hWnd))
                {
                    ShowWindowAsync(hWnd, SW_RESTORE);
                }
                // bring it to the foreground
                SetForegroundWindow(hWnd);
                // exit our process
                return;
            }

            ClassLibrary.Core.UnhandledExceptionHandlers ueh = new ClassLibrary.Core.UnhandledExceptionHandlers(OSFolderManager.Instance.GetPlainLocalUserAppDataPath() + @"\Error.Exceptions.txt");
			Application.ThreadException += new System.Threading.ThreadExceptionEventHandler(ueh.Function2HandleUnhandledApplicationThreadException);
			AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(ueh.Function2HandleUnhandledAppDomainException);

			Application.EnableVisualStyles();
			Application.DoEvents();            
            
#if !NEW_INSTALLER
            UpgradeToNwManager.Instance.UpgradeToNwVersion(Application.ProductName, Path.GetDirectoryName(Application.ExecutablePath));
#endif
			
			RegistrationForm rf = new RegistrationForm();
			if (false == Globals.Registered)
			{
				Application.Run(rf);
				if (true == rf.SuccessFullRegistration)		
						Application.Run(new MainForm());				
			}			
			else
			{
				Application.Run(new MainForm());								
			}															
		}

		private void Form1_Load(object sender, System.EventArgs e)
		{
			if(Globals.ExitApplicationAfterLoad)
			{
				this.Close();
				Application.Exit();
				Application.DoEvents();
				return;
			}

			Size maxSize = ApplicationHelper.GetParentSize(this);
			maxSize.Width = maxSize.Width - 100;
			maxSize.Height = maxSize.Height - 50;
			Globals.applicationSize = maxSize;

			StartUserControl suc = new StartUserControl();
			suc.Dock = DockStyle.Fill;

			//CustomersOverview ctrlCustOverview = new CustomersOverview();
			//Customer customer = new Customer();
			//customer.Size = maxSize;
			//rtf control = new rtf();
			//ctrlCustOverview.Dock = DockStyle.Fill;
			
			this.appBodyPanel.Controls.Add(suc);
			//this.appBodyPanel.Controls.Add(ctrlCustOverview);


			//sasa 07.08.07 - NwVerEnabled
			if(Globals.NwVerEnabled)
				this.menuItem33.MenuItems.Add(this.miSelectDB);

            //sasa: ShortSale module init - todo
            //ShortSaleModule.Common.Adapters.DataManager.Instance.UserAppFolder = OSFolderManager.Instance.GetPlainLocalUserAppDataPath() + @"\ShortSaleExpeditor";
            //ShortSaleModule.Common.Adapters.GroupFilesManager.Instance.UserAppFolder = OSFolderManager.Instance.GetPlainLocalUserAppDataPath() + @"\ShortSaleExpeditor";
            //ShortSaleModule.Common.OffsetData.Instance.UserAppFolder = OSFolderManager.Instance.GetPlainLocalUserAppDataPath() + @"\ShortSaleExpeditor";
            //ShortSaleModule.Common.OffsetData.Instance.LoadOffsets(); 

            appBodyPanel.ControlRemoved += AppBodyPanel_ControlRemoved;           
		}

        private void AppBodyPanel_ControlRemoved(object sender, ControlEventArgs e)
        {
            this.Text = Globals.ProductName;
        }

        private void miExit_Click(object sender, System.EventArgs e)
		{
			//Application.Exit();
			this.Close();
		}

		/// <summary>
		/// Groups
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void bGroups_Click(object sender, System.EventArgs e)
		{	
			//sasa: 12.12.05, network bugfix, contact screen loaded variable
			Globals.ContactScreenOnMainForm = false;
			//sasa: 27.01.06, network lock, stop query
			Network.NetworkLock.Stop();

			//sasa: 20.11.05: 'bugfix', actualy easy way to handle unwanted multiple event handlers in nw, we don't need new group every time
			//TA, check if this is ok?
			if(Globals.SelectGroup == null)
			{
				Globals.SelectGroup = new SelectGroup();
			}

			SelectGroup sg = Globals.SelectGroup;

			//30.01.06, sasa: bugfix, task list was not refreshed on page reload
			sg.RefreshTaskList();

			sg.customizedTreeViewUserControl1.UpdateGlobals = true;

			
			//SelectGroup sg = new SelectGroup();
			sg.customizedTreeViewUserControl1.UpdateGlobals = true;

			sg.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top; 
			sg.Dock = DockStyle.Fill;

            SwapUserControls(sg);
            //this.appBodyPanel.Controls.RemoveAt(0);
			//this.appBodyPanel.Controls.Add(sg);
			
		}

		private void importContacts_Click(object sender, System.EventArgs e)
		{
			//sasa: 12.12.05, network bugfix, contact screen loaded variable
			bool tmpContactScreenOnMainForm = Globals.ContactScreenOnMainForm;
			Globals.ContactScreenOnMainForm = false;

			ImportMask im = new ImportMask();
			im.Location = new Point(15, 15);
			im.ShowDialog();

			//sasa: 12.12.05, network bugfix, contact screen loaded variable
			Globals.ContactScreenOnMainForm = tmpContactScreenOnMainForm;

			/*if (true != Globals.importWizardStarted)
			{
				Globals.importWindowPos = new Point(Globals.importWizardStartXPos, Globals.importWizardStartYPos);
				ImportMask1 im1 = new ImportMask1();
				im1.Show();
			}*/
			
/*			if (true == im1.Valid)
			{
				ImportMask2 im2 = new ImportMask2();
				im2.ShowDialog();
			}
*/
		}		



		/// <summary>
		/// Returm Mail Wizard
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void button3_Click(object sender, System.EventArgs e)
		{
			//sasa: 12.12.05, network bugfix, contact screen loaded variable
			bool tmpContactScreenOnMainForm = Globals.ContactScreenOnMainForm;
			Globals.ContactScreenOnMainForm = false;
			//sasa: 27.01.06, network lock, no need for stop query, object stays locked

			ReturnMailWizardForm rmwf = new ReturnMailWizardForm();
			rmwf.ShowDialog();

			//sasa: 12.12.05, network bugfix, contact screen loaded variable
			Globals.ContactScreenOnMainForm = tmpContactScreenOnMainForm;

		}

		/// <summary>
		/// GetFormsReady
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void button5_Click(object sender, System.EventArgs e)
		{ 			
			
			GetFormsReady.MainForm gfrMf = new GetFormsReady.MainForm();

			//TA:06.04.2005: New RQ: 
			if (null == Globals.CurrentPropertyItem)
			{
				MessageBox.Show(this, "You need to select the contact you want to create forms for!\nGo to Contact View first, or click on New Contact!", "Select Contact", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
				return;
			}

			if (null != Globals.CurrentPropertyItem)
			{
                if (null != Globals.CurrentPropertyItem?.Owner)
                {
                    gfrMf.GFR_FullSellerName = Globals.CurrentPropertyItem.Owner.FullName;

                    //Added
                    gfrMf.UserFirstName = Globals.CurrentPropertyItem.Owner.FirstName;
                    gfrMf.UserLastName = Globals.CurrentPropertyItem.Owner.LastName;
                    gfrMf.UserStreetAddress = Globals.CurrentPropertyItem.Owner.SiteAddress?.FullSiteStreetAddress;
                    gfrMf.UserCity = Globals.CurrentPropertyItem.Owner.SiteAddress?.SiteCity;
                    gfrMf.UserState = Globals.CurrentPropertyItem.Owner.SiteAddress?.SiteState;
                    gfrMf.UserZIP = Globals.CurrentPropertyItem.Owner.SiteAddress?.SiteZIP;
                    gfrMf.UserPhone = Globals.CurrentPropertyItem.Owner.HomePhone?.PhoneNr;
                    gfrMf.UserFax = Globals.CurrentPropertyItem.Owner.FaxNumber;
                }

				if (null != Globals.CurrentPropertyItem?.Owner?.MailAddress)
				{
					gfrMf.GFR_FullSellerMailingAddress = Globals.CurrentPropertyItem.Owner.MailAddress.FullSiteStreetAddress;
					gfrMf.GFR_MailingCity  = Globals.CurrentPropertyItem.Owner.MailAddress.SiteCity;
					gfrMf.GFR_MailingState = Globals.CurrentPropertyItem.Owner.MailAddress.SiteState;
					gfrMf.GFR_MailingZIP   = Globals.CurrentPropertyItem.Owner.MailAddress.SiteZIP;
				}

				//TA++ 12.07.2005., Change Request
				//GFR will now take parcel number (APN) from DealProperty instead of imported data
				//BEGIN
				//OLD++
				//if (null != Globals.CurrentPropertyItem.DealInfo && null != Globals.CurrentPropertyItem.DealInfo.FinancialProperty)
				//OLD--
				//NEW++
				if (null != Globals.CurrentPropertyItem?.DealProperty)
				//NEW--
				{										
					//OLD++
						//gfrMf.GFR_ParcelNumber = Globals.CurrentPropertyItem.DealInfo.FinancialProperty.ParcelNumber;
					//OLD--
					//NEW++
						gfrMf.GFR_ParcelNumber = Globals.CurrentPropertyItem.DealProperty.Apn;
					//NEW--					
				}
				//END

				if (null != Globals.CurrentPropertyItem?.Owner?.SiteAddress)
				{
					gfrMf.GFR_County = Globals.CurrentPropertyItem.Owner.SiteAddress.SiteCountry;
					gfrMf.GFR_FullSiteStreetAddress = Globals.CurrentPropertyItem.Owner.SiteAddress.FullSiteStreetAddress;
					gfrMf.GFR_SiteCity = Globals.CurrentPropertyItem.Owner.SiteAddress.SiteCity;
					gfrMf.GFR_SiteState = Globals.CurrentPropertyItem.Owner.SiteAddress.SiteState;
					gfrMf.GFR_SiteZIP = Globals.CurrentPropertyItem.Owner.SiteAddress.SiteZIP;
				}
			}
			if (null != Globals.CurrentPropertyItem)
			{
				gfrMf.GFR_ContactId = Globals.CurrentPropertyItem.IdPropertyItem.ToString();
				gfrMf.LoadSavedData();
			}
			gfrMf.Show();			
		}

		/// <summary>
		/// Contacts
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void bContacts_Click(object sender, System.EventArgs e)
		{
            Hashtable htNodePosPropertyItemId = ApplicationHelper.GetPropertyItemsInNode(Globals.selectedNodeId);
            //BEGIN - support for ContactView on Group with nonexisting records
            if (null == htNodePosPropertyItemId || 0 == htNodePosPropertyItemId.Count)
            {
                MessageBox.Show(this, "Selected Group doesn't contain any records!", "Contact View", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            //END

			//sasa: 12.12.05, network bugfix, contact screen loaded variable
			Globals.ContactScreenOnMainForm = true;

			ContactScreen cs = null;
			int currentOffset = 1;
			bool bViewOldData = true;
			if (null != Globals.oldContactScreen)
			{
				//sasa, 20.11.05: hack, aaaghr, we must remove event handler from former contact screen
				Network.NetworkUDP.ChangedPropertyItem -= Globals.oldContactScreen.networkEventHandler;
				//sasa, 24.01.06: new nw lock
				Network.NetworkLock.StatusChanged -=Globals.oldContactScreen.networkEventHandler2;;

				currentOffset = Globals.oldContactScreen.CurrentOffset;

				if (1 == Globals.selectedNodeId) // dirty hack due to time reasons, lack of documentation, new user requirements, TODO docs, ... you name it
				{
					Globals.selectedNodeId = Globals.oldContactScreen.CurrentNodeId;
				}
			}


		
			if (-1 == Globals.selectedNodeId || 1 == Globals.selectedNodeId)
			{
				SelectGroupForm sgf = new SelectGroupForm();
				sgf.customizedTreeViewUserControl1.UpdateGlobals = true;
				sgf.ShowDialog();
				if (true == sgf.Valid)
				{
					Globals.selectedNodeId = sgf.customizedTreeViewUserControl1.SelectedNodeId;
					bViewOldData = false;
					if (1 == Globals.selectedNodeId)
					{
						MessageBox.Show(this, "Please select a group you want to display in the Contact View!", "Select Group", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
						return;
					}
				}
				else 
					return;
			}
			
			//Hashtable htNodePosPropertyItemId = ApplicationHelper.GetPropertyItemsInNode(Globals.selectedNodeId);
			if (0 != htNodePosPropertyItemId.Count)
			{
				//sasa, 14.11.05: bug/bugfix? Contact Screen twice? makes no sense? and disturbes udp notification
				//cs = new ContactScreen(htNodePosPropertyItemId);	
				if (true == bViewOldData && currentOffset <= htNodePosPropertyItemId.Count)
				{
					cs = new ContactScreen(htNodePosPropertyItemId, currentOffset);
				}
				else
				{
					cs = new ContactScreen(htNodePosPropertyItemId);						
				}
			}
			else 
			{
                //this can't happen anymore
				//cs = new ContactScreen();
				//cs.LoadMedia();
			}
			
			cs.Size = Globals.applicationSize; 			
			cs.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top; 
			cs.Dock = DockStyle.Fill;

            SwapUserControls(cs);
            //this.appBodyPanel.Controls.RemoveAt(0);
			//this.appBodyPanel.Controls.Add(cs);

			//27.01.06, sasa: nw lock, reload contact and acquire new lock if cs already on screen
			cs.ReloadContact();
		}

		/// <summary>
		/// ListView
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void bListView_Click(object sender, System.EventArgs e)
		{
			//sasa: 12.12.05, network bugfix, contact screen loaded variable
			Globals.ContactScreenOnMainForm = false;
			//sasa: 27.01.06, network lock, stop query
			Network.NetworkLock.Stop();
			
			//TA: listview should always show content of currently selected group

			PropertyItemListViewControl pilwc = new PropertyItemListViewControl();			
			
			//if (null == Globals.oldListView)
			{
				if (-1 != Globals.selectedNodeId)
				{
					Hashtable htNodePosPropertyItemId = ApplicationHelper.GetPropertyItemsInNode(Globals.selectedNodeId);
					if (0 != htNodePosPropertyItemId.Count)
						pilwc = new PropertyItemListViewControl(Globals.selectedNodeId, Globals.selectedNodeName);	
					else 
						pilwc = new PropertyItemListViewControl();
				}
				else
				{
					SelectGroupForm sgf = new SelectGroupForm();
					sgf.customizedTreeViewUserControl1.UpdateGlobals = true;
					sgf.ShowDialog();
					if (true == sgf.Valid)
					{
						Globals.selectedNodeId = sgf.customizedTreeViewUserControl1.SelectedNodeId;						
					}
					else 
						return;
					
					if (ApplicationHelper.GetPropertyCountInNode(Globals.selectedNodeId) >= 5000)
					{
						MessageBox.Show(this, "Due to performance reasons List View can operate only on groups which contain less than 5000 contacts!", "Large Group!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
						return;
					}
					pilwc = new PropertyItemListViewControl(Globals.selectedNodeId, Globals.selectedNodeName);	
				}
			}
			//else
			//{				
			//	pilwc = Globals.oldListView;
			//}
			
			pilwc.Size = Globals.applicationSize; 			
			pilwc.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top;
			pilwc.Dock = DockStyle.Fill;

            SwapUserControls(pilwc);
            //this.appBodyPanel.Controls.RemoveAt(0);
			//this.appBodyPanel.Controls.Add(pilwc);
		}

		/// <summary>
		/// Find group
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void button1_Click(object sender, System.EventArgs e)
		{
			//sasa: 12.12.05, network bugfix, contact screen loaded variable
			Globals.ContactScreenOnMainForm = false;
			//sasa: 27.01.06, network lock, stop query
			Network.NetworkLock.Stop();
			
			FindPropertyItem fpi = null;
			if (null == Globals.oldFindPropertyItem)
			{
				fpi = new FindPropertyItem();
			}
			else
			{
				fpi = Globals.oldFindPropertyItem;
			}
			fpi.Size = Globals.applicationSize; 
			fpi.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top;
			fpi.Dock = DockStyle.Fill;

            SwapUserControls(fpi);
            //this.appBodyPanel.Controls.RemoveAt(0);
			//this.appBodyPanel.Controls.Add(fpi);
		}

		/// <summary>
		/// Mail Fax Wizard
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void button2_Click(object sender, System.EventArgs e)
		{
			//sasa: 12.12.05, network bugfix, contact screen loaded variable
			bool tmpContactScreenOnMainForm = Globals.ContactScreenOnMainForm;
			Globals.ContactScreenOnMainForm = false;
			//sasa: 27.01.06, network lock, no need for stop query, object stays locked


			MailFaxWizard mfwObject = new MailFaxWizard();

			MailFaxWizardForm mfwf = new MailFaxWizardForm(mfwObject);
			mfwf.ShowDialog();

			//sasa: 12.12.05, network bugfix, contact screen loaded variable
			Globals.ContactScreenOnMainForm = tmpContactScreenOnMainForm;
		}

		private void menuItem70_Click(object sender, System.EventArgs e)
		{
			//sasa: 12.12.05, network bugfix, contact screen loaded variable
			bool tmpContactScreenOnMainForm = Globals.ContactScreenOnMainForm;
			Globals.ContactScreenOnMainForm = false;

			UserInformationForm uif = new UserInformationForm();
			uif.ShowDialog();

			//sasa: 12.12.05, network bugfix, contact screen loaded variable
			Globals.ContactScreenOnMainForm = tmpContactScreenOnMainForm;
		}

        

        private void menuItem10_Click(object sender, System.EventArgs e)
		{
			EditDocumentTemplateUserControl edtuc = null;
												
			if (null == Globals.oldEditDocumentTemplateUserControl)
			{
				edtuc = new EditDocumentTemplateUserControl();
                edtuc.OnDocumentCreated += Edtuc_OnDocumentCreated;
                edtuc.OnDocumentLoaded += Edtuc_OnDocumentLoaded;
                edtuc.OnDocumentModified += Edtuc_OnDocumentModified;
                edtuc.OnDocumentSaved += Edtuc_OnDocumentSaved;
                edtuc.OnDocumentClosed += Edtuc_OnDocumentClosed;

                edtuc.LostFocus += Edtuc_LostFocus;                                
			}
			else
			{
				edtuc = Globals.oldEditDocumentTemplateUserControl;
			}
			
			
			edtuc.Size = Globals.applicationSize;

			edtuc.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top;
			edtuc.Dock = DockStyle.Fill;

            SwapUserControls(edtuc);
            //this.appBodyPanel.Controls.RemoveAt(0);
			//this.appBodyPanel.Controls.Add(edtuc);            
		}

        string edtucDocumentName = string.Empty;
        bool edtucIsDocumentTemplete = false;
        bool edtucHasDocumentChanged = false;

        private void Edtuc_LostFocus(object sender, EventArgs e)
        {
            this.Text = Globals.ProductName;
        }

        private void Edtuc_OnDocumentClosed()
        {
            edtucHasDocumentChanged = false;
            this.Text = Globals.ProductName;
        }

        private void Edtuc_OnDocumentSaved(string documentName)
        {
            edtucHasDocumentChanged = false;
            edtucDocumentName = documentName;
            this.Text = $"{Globals.ProductName} - {documentName}";
        }

        private void Edtuc_OnDocumentModified(string documentName)
        {
            edtucHasDocumentChanged = true;
            edtucDocumentName = documentName;
            this.Text = $"{Globals.ProductName} - {documentName}*";
        }

        private void Edtuc_OnDocumentLoaded(string documentName, bool template)
        {
            edtucHasDocumentChanged = false;
            edtucDocumentName = documentName;
            edtucIsDocumentTemplete = template;

            this.Text = $"{Globals.ProductName} - {documentName}";
        }

        private void Edtuc_OnDocumentCreated(string documentName)
        {
            edtucHasDocumentChanged = true;
            edtucDocumentName = documentName;
            this.Text = $"{Globals.ProductName} - {documentName}";
        }

        private void menuItem20_Click(object sender, System.EventArgs e)
		{
			//sasa: 12.12.05, network bugfix, contact screen loaded variable
			bool tmpContactScreenOnMainForm = Globals.ContactScreenOnMainForm;
			Globals.ContactScreenOnMainForm = false;

			MailFaxWizard mfwObject = new MailFaxWizard();

			MailFaxWizardForm mfwf = new MailFaxWizardForm(mfwObject);
			mfwf.ShowDialog();

			//sasa: 12.12.05, network bugfix, contact screen loaded variable
			Globals.ContactScreenOnMainForm = tmpContactScreenOnMainForm;			
		}

		private void menuItem77_Click(object sender, System.EventArgs e)
		{
			BarCodeScannerSetup bcss = new BarCodeScannerSetup();
			bcss.ShowDialog();
		}

		private void menuItem76_Click(object sender, System.EventArgs e)
		{
		
		}

		private void menuItem84_Click(object sender, System.EventArgs e)
		{	
			//sasa: 12.12.05, network bugfix, contact screen loaded variable
			bool tmpContactScreenOnMainForm = Globals.ContactScreenOnMainForm;
			Globals.ContactScreenOnMainForm = false;
		
			PropertyItemsExportWizard exportWizard = new PropertyItemsExportWizard();
			exportWizard.Execute();
			//GC.Collect();			

			//sasa: 12.12.05, network bugfix, contact screen loaded variable
			Globals.ContactScreenOnMainForm = tmpContactScreenOnMainForm;
		}

		private void mi_CreateNewActivity_Click(object sender, System.EventArgs e)
		{
			//sasa: 12.12.05, network bugfix, contact screen loaded variable
			bool tmpContactScreenOnMainForm = Globals.ContactScreenOnMainForm;
			Globals.ContactScreenOnMainForm = false;

			DealMaker.Forms.SchedulingActicities.AssignActivity form = new DealMaker.Forms.SchedulingActicities.AssignActivity();
			form.ShowDialog();

			//sasa: 12.12.05, network bugfix, contact screen loaded variable
			Globals.ContactScreenOnMainForm = tmpContactScreenOnMainForm;
		}

		private void mi_TaskManager_Click(object sender, System.EventArgs e)
		{
			//sasa: 12.12.05, network bugfix, contact screen loaded variable
			bool tmpContactScreenOnMainForm = Globals.ContactScreenOnMainForm;
			Globals.ContactScreenOnMainForm = false;

			DealMaker.Forms.SchedulingActicities.TaskManager tm = new DealMaker.Forms.SchedulingActicities.TaskManager();

			tm.Size = Globals.applicationSize; 
			tm.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top; 
			tm.Dock = DockStyle.Fill;

            SwapUserControls(tm);
            //this.appBodyPanel.Controls.RemoveAt(0);
			//this.appBodyPanel.Controls.Add(tm);

			//sasa: 12.12.05, network bugfix, contact screen loaded variable
			Globals.ContactScreenOnMainForm = tmpContactScreenOnMainForm;
		}

		#region � Mouse Enter && Mouse Leave �
		#region � Groups �
		private void bGroups_MouseEnter(object sender, System.EventArgs e)
		{
			Button b = (Button)sender;
			b.ImageIndex = 17;	
		}		

		private void bGroups_MouseLeave(object sender, System.EventArgs e)
		{
			Button b = (Button)sender;
			b.ImageIndex = 16;					
		}
		#endregion � Groups �	
		#region � Contacts �
		private void bContacts_MouseEnter(object sender, System.EventArgs e)
		{
			Button b = (Button)sender;
			b.ImageIndex = 3;
		}

		private void bContacts_MouseLeave(object sender, System.EventArgs e)
		{	
			Button b = (Button)sender;
			b.ImageIndex = 2;
		}
		#endregion � Contacts �
		#region � ListView �
		private void bListView_MouseEnter(object sender, System.EventArgs e)
		{
			Button b = (Button)sender;
			b.ImageIndex = 9;
		}

		private void bListView_MouseLeave(object sender, System.EventArgs e)
		{
			Button b = (Button)sender;
			b.ImageIndex = 8;
		}
		#endregion � ListView �
		#region � Import �
		private void bImport_MouseEnter(object sender, System.EventArgs e)
		{
			Button b = (Button)sender;
			b.ImageIndex = 13;
		}

		private void bImport_MouseLeave(object sender, System.EventArgs e)
		{
			Button b = (Button)sender;
			b.ImageIndex = 12;
		}
		#endregion � Import �
		#region � Find Contact �
		private void bFindContact_MouseEnter(object sender, System.EventArgs e)
		{
			Button b = (Button)sender;
			b.ImageIndex = 5;
		}

		private void bFindContact_MouseLeave(object sender, System.EventArgs e)
		{
			Button b = (Button)sender;
			b.ImageIndex = 4;
		}
		#endregion � Find Contact �
		#region � Mail Fax Wizard �
		private void bMailFaxWizard_MouseEnter(object sender, System.EventArgs e)
		{
			Button b = (Button)sender;
			b.ImageIndex = 11;
		}

		private void bMailFaxWizard_MouseLeave(object sender, System.EventArgs e)
		{
			Button b = (Button)sender;
			b.ImageIndex = 10;
		}
		#endregion � Mail Fax Wizard �
		#region � Return Mail Processing �
		private void bReturnMailProcessing_MouseEnter(object sender, System.EventArgs e)
		{
			Button b = (Button)sender;
			b.ImageIndex = 15;
		}

		private void bReturnMailProcessing_MouseLeave(object sender, System.EventArgs e)
		{
			Button b = (Button)sender;
			b.ImageIndex = 14;
		}
		#endregion � Return Mail Processing �
		#region � DealIQ �
		private void bDealIQ_MouseEnter(object sender, System.EventArgs e)
		{
			Button b = (Button)sender;
			b.ImageIndex = 22;
		}

		private void bDealIQ_MouseLeave(object sender, System.EventArgs e)
		{
			Button b = (Button)sender;
			b.ImageIndex = 0;
		}
		#endregion � DealIQ �
		#region � GetFormsReady �
		private void bGetFOrmsReady_MouseEnter(object sender, System.EventArgs e)
		{
			Button b = (Button)sender;
			b.ImageIndex = 7;
		}

		private void bGetFOrmsReady_MouseLeave(object sender, System.EventArgs e)
		{
			Button b = (Button)sender;
			b.ImageIndex = 6;
		}
		#endregion � GetFormsReady �
		#region � Task Manager �
		private void bTaskManager_MouseEnter(object sender, System.EventArgs e)
		{
			Button b = (Button)sender;
			b.ImageIndex = 19;
		}

		private void bTaskManager_MouseLeave(object sender, System.EventArgs e)
		{
			Button b = (Button)sender;
			b.ImageIndex = 18;
		}
		#endregion � Task Manager �

		/// <summary>
		/// Task Manager
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void bTaskManager_Click(object sender, System.EventArgs e)
		{
			//sasa: 12.12.05, network bugfix, contact screen loaded variable
			Globals.ContactScreenOnMainForm = false;
			//sasa: 27.01.06, network lock, stop query
			Network.NetworkLock.Stop();
			
			DealMaker.Forms.SchedulingActicities.TaskManager tm;

			//sasa: 20.11.05: 'bugfix', actualy easy way to handle unwanted multiple event handlers in nw, we don't need new task manager every time
			if(Globals.TaskManager == null)
			{
				Globals.TaskManager = new DealMaker.Forms.SchedulingActicities.TaskManager();
			}
			else
			{
				//sasa, 06.01.2006: reload tasks if clicked again on task manager (if something changes)
				Globals.TaskManager.loadTasks();
                Globals.TaskManager.ReloadAssignees();
                //Globals.TaskManager
			}

			tm = Globals.TaskManager;

			tm.Size = Globals.applicationSize; 
			tm.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top; 
			tm.Dock = DockStyle.Fill;


            SwapUserControls(tm);
            //this.appBodyPanel.Controls.RemoveAt(0);
			//this.appBodyPanel.Controls.Add(tm);
		}
		#endregion � Mouse Enter && Mouse Leave �

		/// <summary>
		/// Add new contact
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void bAddNewContact_Click(object sender, System.EventArgs e)
		{
            CreateContact(ContactTypes.Undefined);
		}

        void On_NewLenderContact_Click(object sender, EventArgs e)
        {
            CreateContact(ContactTypes.Lender);
        }

        void On_NewBuyerContact_Click(object sender, EventArgs e)
        {
            CreateContact(ContactTypes.Buyer);
        }

        void CreateContact(ContactTypes contactType)
        {
            SelectGroupForm sgf = new SelectGroupForm();
            sgf.ContactType = contactType;
            DialogResult dr = sgf.ShowDialog();

            if (DialogResult.OK != dr)
                return;

            int selectedNodeId = sgf.SelectedNodeId;
            string selectedNodeName = sgf.SelectedNodeName;
            if (selectedNodeId == ContactTypesManager.Instance.CashBuyersGroupId)
                contactType = ContactTypes.Buyer;
            else if (selectedNodeId == ContactTypesManager.Instance.PrivateLenderGroupId)
                contactType = ContactTypes.Lender;
            else
                contactType = ContactTypes.Seller;
            
            
            //COMMERCIALS - BEGIN
            BuildingInfoObject.Instance.ResetObject();
            IncomeObject.Instance.ResetObject();
            ExpensesObject.Instance.ResetObject();
            LoansObject.Instance.ResetObject();
            FinancialInfoObject.Instance.ResetObject();
            //COMMERCIALS - END

            //BUYER INFO DATA - BEGIN
            BuyerInfoObject.Instance.ResetObject();
            //BUYER INFO DATA - END

            //SHORT SALE MODULE - BEGIN
            DealMaker.PlainClasses.ShortSale.Adapters.DataManager.Instance.ResetObject();
            DealMaker.PlainClasses.CalculateOffer.CalculateOfferManager.Instance.ResetObject();
            //SHORT SALE MODULE - END

            //ONLINE DATA - BEGIN
            DealMaker.PlainClasses.OnLineData.COMPS.CompsManager.Instance.ResetObject();
            DealMaker.PlainClasses.OnLineData.PropertyProfile.PPManager.Instance.ResetObject();
            //ONLINE DATA - END

            //REPAIRS - BEGIN
            DealMaker.Repairs.RepairsManager.Instance.ResetObject();
            //REPAIRS - END

            //sasa: 12.12.05, network bugfix, contact screen loaded variable
            Globals.ContactScreenOnMainForm = true;
            //sasa: 27.01.06, network lock, stop query
            Network.NetworkLock.Stop();

            //sasa: 24.3.06, nw2 bugfix no lock for new contact

            ContactScreen cs = null;
            if (null != Globals.oldContactScreen)
            {
                //sasa, 20.11.05: hack, aaaghr, we must remove event handler from former contact screen
                Network.NetworkUDP.ChangedPropertyItem -= Globals.oldContactScreen.networkEventHandler;
                //sasa, 24.01.06: new nw lock
                Network.NetworkLock.StatusChanged -= Globals.oldContactScreen.networkEventHandler2;
            }

            cs = new ContactScreen(selectedNodeId, selectedNodeName);

            cs.LoadMedia();
            cs.AdjustGUI(contactType);

            cs.Size = Globals.applicationSize;
            cs.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top;
            cs.Dock = DockStyle.Fill;

            SwapUserControls(cs);
            //this.appBodyPanel.Controls.RemoveAt(0);
            //this.appBodyPanel.Controls.Add(cs);
        }

		private void pbGroups_Click(object sender, System.EventArgs e)
		{
			SelectGroup sg = new SelectGroup();
			sg.customizedTreeViewUserControl1.UpdateGlobals = true;

			sg.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top; 
			sg.Dock = DockStyle.Fill;

            SwapUserControls(sg);
            //this.appBodyPanel.Controls.RemoveAt(0);
			//this.appBodyPanel.Controls.Add(sg);
		}

		private void pbContacts_Click(object sender, System.EventArgs e)
		{
			ContactScreen cs = null;
			if (-1 == Globals.selectedNodeId)
			{
				SelectGroupForm sgf = new SelectGroupForm();
				sgf.customizedTreeViewUserControl1.UpdateGlobals = true;
				sgf.ShowDialog();
				if (true == sgf.Valid)
					Globals.selectedNodeId = sgf.customizedTreeViewUserControl1.SelectedNodeId;
				else 
					return;
			}
			
			Hashtable htNodePosPropertyItemId = ApplicationHelper.GetPropertyItemsInNode(Globals.selectedNodeId);
			if (0 != htNodePosPropertyItemId.Count)
				cs = new ContactScreen(htNodePosPropertyItemId);	
			else 
				cs = new ContactScreen();
			
			cs.Size = Globals.applicationSize; 			
			cs.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top; 
			cs.Dock = DockStyle.Fill;

            SwapUserControls(cs);
            //this.appBodyPanel.Controls.RemoveAt(0);
			//this.appBodyPanel.Controls.Add(cs);
		}

		private void pbListView_Click(object sender, System.EventArgs e)
		{
			PropertyItemListViewControl pilwc = new PropertyItemListViewControl();			
			if (null == Globals.oldListView)
			{
				if (-1 != Globals.selectedNodeId)
				{
					Hashtable htNodePosPropertyItemId = ApplicationHelper.GetPropertyItemsInNode(Globals.selectedNodeId);
					if (0 != htNodePosPropertyItemId.Count)
						pilwc = new PropertyItemListViewControl(Globals.selectedNodeId, Globals.selectedNodeName);	
					else 
						pilwc = new PropertyItemListViewControl();
				}
				else
				{
					pilwc = new PropertyItemListViewControl();
				}
			}
			else
			{
				pilwc = Globals.oldListView;
			}
			
			pilwc.Size = Globals.applicationSize; 			
			pilwc.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top;
			pilwc.Dock = DockStyle.Fill;

            SwapUserControls(pilwc);
            //this.appBodyPanel.Controls.RemoveAt(0);
			//this.appBodyPanel.Controls.Add(pilwc);
		}

		private void pbAddNewContact_Click(object sender, System.EventArgs e)
		{
		
		}

		private void pbFindContact_Click(object sender, System.EventArgs e)
		{
			FindPropertyItem fpi = null;
			if (null == Globals.oldFindPropertyItem)
			{
				fpi = new FindPropertyItem();
			}
			else
			{
				fpi = Globals.oldFindPropertyItem;
			}
			fpi.Size = Globals.applicationSize; 
			fpi.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top;
			fpi.Dock = DockStyle.Fill;

            SwapUserControls(fpi);
            //this.appBodyPanel.Controls.RemoveAt(0);
			//this.appBodyPanel.Controls.Add(fpi);
		}

		private void pbMailFaxWizard_Click(object sender, System.EventArgs e)
		{
			MailFaxWizard mfwObject = new MailFaxWizard();

			MailFaxWizardForm mfwf = new MailFaxWizardForm(mfwObject);
			mfwf.ShowDialog();
		}

		private void pbReturnMailProcessing_Click(object sender, System.EventArgs e)
		{
			ReturnMailWizardForm rmwf = new ReturnMailWizardForm();
			rmwf.ShowDialog();
		}

		private void pbDealIQ_Click(object sender, System.EventArgs e)
		{
		
		}

		private void pbGetFormsReady_Click(object sender, System.EventArgs e)
		{
			object o = sender;
			GetFormsReady.MainForm gfrMf = new GetFormsReady.MainForm();
			gfrMf.Show();
		}

		private void appLeftPanel_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
		
		}

		private void pbTaskManager_Click(object sender, System.EventArgs e)
		{
			DealMaker.Forms.SchedulingActicities.TaskManager tm = new DealMaker.Forms.SchedulingActicities.TaskManager();

			tm.Size = Globals.applicationSize; 
			tm.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top; 
			tm.Dock = DockStyle.Fill;

            SwapUserControls(tm);
            //this.appBodyPanel.Controls.RemoveAt(0);
			//this.appBodyPanel.Controls.Add(tm);
		}

		private void AcqCalcEnter(object sender, System.EventArgs e)
		{
			//sasa: 12.12.05, network bugfix, contact screen loaded variable
			Globals.ContactScreenOnMainForm = false;
			//sasa: 27.01.06, network lock, stop query
			Network.NetworkLock.Stop();
			
			UserControl_AcquisitionCalculator ac = null;
			//if (null == Globals.oldAcquisitionCalculator)
			//{
				ac = new UserControl_AcquisitionCalculator();				
			//}
			//else
			//{
			//	ac = Globals.oldAcquisitionCalculator;
			//}
			
			Globals.oldAcquisitionCalculator = ac;
			
			//Set data to AC
			#region PropertyAddress 
			if (null != Globals.CurrentPropertyItem && null != Globals.CurrentPropertyItem.Owner && null != Globals.CurrentPropertyItem.Owner.SiteAddress)
			{				
				ac.PropertyAddress = Globals.CurrentPropertyItem.Owner.SiteAddress.FullSiteStreetAddress;				
			}
			else
			{
				ac.PropertyAddress = string.Empty;		
			}
			#endregion PropertyAddress 

			#region Worth By COMPS 
			//WorthByCOMPS			
			if (null != Globals.CurrentPropertyItem && null != Globals.CurrentPropertyItem.DealProperty)				
			{
				System.IFormatProvider format =	new System.Globalization.CultureInfo("en-US", true);
				try
				{
					string temp = Globals.CurrentPropertyItem.DealProperty.Arv.ToString();
					temp = temp.Replace("$", "");
					temp = temp.Trim();
					ac.WorthByCOMPS = Convert.ToDecimal(temp, format);
				}
				catch 
				{
					ac.WorthByCOMPS = 0;
				}
			}
			#endregion Worth By COMPS 

			#region WorthBySeller 
			if (null != Globals.CurrentPropertyItem && null != Globals.CurrentPropertyItem.DealProperty)
			{
				decimal dConvVal = 0;
				try
				{
					dConvVal = Convert.ToDecimal(Globals.CurrentPropertyItem.DealProperty.WorthBySeller);
					
				}
				catch (Exception)
				{
					dConvVal = 0;
				}
				ac.WorthBySeller = dConvVal;
			}
			else
			{
				ac.WorthBySeller = 0;
			}
			#endregion WorthBySeller 
			
			#region Asking Price 
			//AskingPrice --> what to bind?	iz dealinfo tab - lowest price acceptable
			if (null != Globals.CurrentPropertyItem && null != Globals.CurrentPropertyItem.DealProperty)
			{
				try
				{
					ac.AskingPrice = Convert.ToDecimal(Globals.CurrentPropertyItem.DealProperty.LowestPriceAcceptable);
				}
				catch 
				{
					ac.AskingPrice = 0;
				}
			}
			#endregion Asking Price 

			
			//AskingCash --> what to bind? iz dealinfo tab - minimum cash needed (text polje) - minimum cash needed - why - miljenko
			if (null != Globals.CurrentPropertyItem && null != Globals.CurrentPropertyItem.DealProperty)
			{
				ac.AskingCash = Globals.CurrentPropertyItem.DealProperty.MinCashNeededWhy;
			}
			else
			{
				ac.AskingCash = string.Empty;
			}

			//TotalOwed		--> ContactScreen -->DealInfo = total owed !!! PAZI !!!!!!!!!!
			if (null != Globals.CurrentPropertyItem)
				ac.TotalOwed = ApplicationHelper.GetTotalOwed(Globals.CurrentPropertyItem);
			
			#region TotalMonthly 
			if (null != Globals.CurrentPropertyItem)
				ac.TotalMonthly = ApplicationHelper.GetTotalMonthlyPayment(Globals.CurrentPropertyItem);
			#endregion TotalMonthly 
			
			#region Foreclosing 
			//ForeClosing   --> what to bind? - Deal info tab - In Forecl.?- "TEXT"
			if (null != Globals.CurrentPropertyItem && null != Globals.CurrentPropertyItem.DealProperty && null != Globals.CurrentPropertyItem.DealProperty.InForecl)
			{
				ac.ForeClosing = Globals.CurrentPropertyItem.DealProperty.InForecl;
			}
			else
			{
				ac.ForeClosing = string.Empty;
			}
			#endregion Foreclosing 			
			
			#region ReinstateAmount 
			if (null != Globals.CurrentPropertyItem && null != Globals.CurrentPropertyItem.DealProperty)
			{
				decimal dConvVal = 0;
				try
				{
					dConvVal = Convert.ToDecimal(Globals.CurrentPropertyItem.DealProperty.ReInsAmnt);					
				}
				catch (Exception)
				{
					dConvVal = 0;
				}
				ac.ReinstateAmount = dConvVal;
			}
			else
			{
				ac.ReinstateAmount = 0;
			}
			#endregion ReinstateAmount
	
			// after setting data from PropertyInfo load persisted XML
			ac.Load();

			
			ac.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top; 
			ac.Dock = DockStyle.Fill;

            SwapUserControls(ac);
            //this.appBodyPanel.Controls.RemoveAt(0);
			//this.appBodyPanel.Controls.Add(ac);

		}

		private void miSubscribeToData_Click(object sender, System.EventArgs e)
		{
            if (null != Globals.UserRegistration)
            {
                Process.Start("http://www.profitgrabber.com/account/subscribe_renew.cfm?IDUser={" + Globals.UserRegistration.IDUser.ToString() + "}");                
            }
		}

		private void miMyAccount_Click(object sender, System.EventArgs e)
		{
            if (null != Globals.UserRegistration)
            {
                Process.Start("http://www.ProfitGrabber.com/Account/Home.cfm?GUID={" + Globals.UserRegistration.IDUser.ToString() + "}");                
            }
		}

		private void miAbout_Click(object sender, System.EventArgs e)
		{
			AboutForm af = new AboutForm();
			af.ShowDialog();
		}

		private void miHowTo_Click(object sender, System.EventArgs e)
		{
			Process.Start("http://www.profitgrabber.com/OnlineUserGuide.html");            
		}

		private void miVideoLessons_Click(object sender, System.EventArgs e)
		{
			Process.Start("http://www.profitgrabber.com/training");            
		}

		private void MainForm_Closed(object sender, EventArgs e)
		{
		/*
			if (
				null != Globals.registrationChecker 
				//System.Threading.ThreadState.Stopped != Globals.registrationChecker.ThreadState ||
				//System.Threading.ThreadState.Aborted != Globals.registrationChecker.ThreadState
				)
			{
				Globals.registrationChecker.Abort();
				if (null != Globals.registrationReminder)
				{
					Globals.registrationReminder.Abort();
				}
				Globals.registrationChecker = null;
				Globals.registrationReminder = null;
			}

			if (null != Globals.taskManagerChecker)
			{
				Globals.taskManagerChecker.Abort();
			}
			Globals.taskManagerChecker = null;

			if (null != Globals.blinkTaskManager)
			{
				Globals.blinkTaskManager.Abort();
			}
			Globals.blinkTaskManager = null;
		*/
			//sasa 4.12.05: kill threads moved to funct, must call from splash screen too
			ApplicationHelper.KillThreads();

			Globals.AbortAllThreads = true;
			
		}

		private void EmailSS_EH(object sender, EventArgs e)
		{
			Process.Start($"http://support.profitgrabber.com/{Globals.UserRegistration.IDUser}");            
		}

		private void MainForm_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			//sasa 05.12.05: don't ask for exit if exiting after database select
			if(!Globals.ExitApplicationAfterLoad)
			{
                CheckSaveDocument();

                if (DialogResult.No == MessageBox.Show(this, "Are you sure that you want to exit ProfitGrabber Pro?", "ProfitGrabber Pro", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2))
				{
					e.Cancel = true;
				}
				else
				{
					e.Cancel = false;
				}
			}
		}

		private void miLabelOffsetSettings_EH(object sender, EventArgs e)
		{
			offsetSettingsForm osf = new offsetSettingsForm(null);
			osf.ShowDialog();
		}

		//sasa, 18.09.05: 
		private void blinkTaskManagerIcon()
		{
			while(true)
			{
				//MessageBox.Show(Forms.SchedulingActicities.Static.PendingTasks.ToString());
				if(Forms.SchedulingActicities.Static.PendingTasks)
				{
					if(bTaskManager.ImageIndex != 19)
					{
						if(bTaskManager.ImageIndex == 23)
						{
							bTaskManager.ImageIndex = 18;
						}
						else
						{
							bTaskManager.ImageIndex = 23;
						}
					}

					//					if(bTaskManager.BackColor == Color.Transparent)
					//						bTaskManager.BackColor = Color.LightCoral;
					//					else
					//						bTaskManager.BackColor = Color.Transparent;
				}
				else
				{
					//					if(bTaskManager.BackColor != Color.Transparent)
					//						bTaskManager.BackColor = Color.Transparent;
					if(!(bTaskManager.ImageIndex == 18 && bTaskManager.ImageIndex == 19))
					{
						bTaskManager.ImageIndex = 18;
					}
				}
				System.Threading.Thread.Sleep(Globals.CheckPendingTasksThread_BlinkTime);
			}
		}

		private void ScanForUpdateManager()
		{
			string appExecPath = Application.StartupPath;
			try
			{
				string[] um = Directory.GetFiles(appExecPath, "UpdateManager.exe");
				if (um.Length > 0)
				{
					MenuItem mi = new MenuItem("Check For Updates");
					mi.Click += new EventHandler(Start_UpdateManager_EH);
					this.menuItem52.MenuItems.Add(2, mi);
					//TODO_INT - ovog nema u novom, provjeri
					
					MenuItem mi2 = new MenuItem("Backup - Restore Manager");
					mi2.Click += new EventHandler(OnBackupRestoreManagerClick);

					this.menuItem1.MenuItems.Add(5, mi2);
					
				}
			}
			catch(Exception
#if DEBUG
				exc
#endif
				)
			{
#if DEBUG
				MessageBox.Show(exc.Message);
#endif
			}
		}

		private void Start_UpdateManager_EH(object sender, EventArgs e)
		{
			Process.Start(Application.StartupPath + @"\UpdateManager.exe");
		}

		private void miGetAdditionalModules_Click(object sender, System.EventArgs e)
		{
			//if (null != Globals.UserRegistration)
				//Process.Start("IExplore.exe", "http://www.profitgrabber.com/XXX_BUY_ADDITIONAL_MODULES_XXX/buy_modules.cfm?IDUser={" + Globals.UserRegistration.IDUser.ToString() + "}");		
		}

		private void miGetNewLicences_Click(object sender, System.EventArgs e)
		{
			//reregister again - and save the data!
			DealMaker.Registration.Service1 service = null;
			ActiveModules am = null;
			try
			{
				service = new DealMaker.Registration.Service1();
				string msg = string.Empty;
				service.Timeout = 10000;
				am = service.GetActiveModules(Globals.UserRegistration.IDUser, out msg);
				
			}
			catch (Exception 
#if DEBUG
				exc
#endif
				)
			{
#if DEBUG
				MessageBox.Show("Message: " + exc.ToString() + "\nInner Exception: " + exc.InnerException);
#endif				
			}
			if (null != am)
			{
				Globals.activeModules = am;
				OnRefreshLockedFeatures();
				ApplicationHelper.SaveActiveModules(am);
			}

		}	
		private bool TestForLocking()
		{
			//MainForm Locking

			if (null != Globals.activeModules)
			{
				this.bDealIQ.Enabled = Globals.activeModules.MOD_4;
				this.miDealIQ.Enabled = Globals.activeModules.MOD_4;
				this.bGetFOrmsReady.Enabled = Globals.activeModules.MOD_3;
				this.miGetFormsReady.Enabled = Globals.activeModules.MOD_3;		
				if (false == Globals.activeModules.MOD_0)
				{
					ApplicationHelper.SelfDeactivate();					
					return false;
				}
			}
			else
			{
				this.bGetFOrmsReady.Enabled = true;
				this.miGetFormsReady.Enabled = true;
			}
			return true;			
		}

		private void miCheckForNewLeads_Click(object sender, System.EventArgs e)
		{
			Globals.breakWaitingPeriod = true;
		}

		private void miActivate_Click(object sender, System.EventArgs e)
		{
			DealMaker.Registration.Service1 service = null;
			try
			{
				service = new DealMaker.Registration.Service1();
			}
			catch (Exception exc)
			{
#if DEBUG
				MessageBox.Show("Message: " + exc.ToString() + "\nInner Exception: " + exc.InnerException);
#endif
				throw new Exception("service = new DealMaker.Registration.Service1()",  exc);	//TA:11.04.2005 - For saving exceptions that may come...
			}
			service.Timeout = 90000;
			
			//read from app.config
			try
			{
				NameValueCollection nvc = (NameValueCollection)ConfigurationSettings.GetConfig("ProfitGrabberWebSupport");
				if (null != nvc && nvc.Count != 0)
				{
					Globals.ProfitGrabberWebSupportURL = (string)nvc["URL"];
					service.Url = (string)nvc["URL"];				
				}
			}
			catch (Exception exc)
			{
				throw new Exception("Reading from app.Config",  exc);	//TA:11.04.2005 - For saving exceptions that may come...
			}

			string msg = string.Empty;
			DealMaker.Registration.ActiveModules am = ApplicationHelper.GetActiveModulesFromWS(Globals.UserRegistration.IDUser, service, out msg);
			//SET THEM MEMORY RESIDENT
			Globals.activeModules = am;
			//SAVE TO FILE
			ApplicationHelper.SaveActiveModules(am);
			MessageBox.Show("ProfitGrabber Pro has successfully saved new modules settings!\nPlease restart ProfitGrabber Pro!", "ProfitGrabber Pro Web Services", MessageBoxButtons.OK, MessageBoxIcon.Information);
		}

		private void miAboutSystem_Click(object sender, System.EventArgs e)
		{
            if (null != Globals.UserRegistration)
            {
                Process.Start("http://www.ProfitGrabber.com/Account/Niche2WeatlhSystem.cfm?GUID={" + Globals.UserRegistration.IDUser.ToString() + "}");                
            }
		}

		private void miSpecialModules_Click(object sender, System.EventArgs e)
		{
            if (null != Globals.UserRegistration)
            {
                Process.Start("http://www.ProfitGrabber.com/Account/SpecialtyModules.cfm?GUID={" + Globals.UserRegistration.IDUser.ToString() + "}");                
            }
		}

		private void miLiveAnswers_Click(object sender, System.EventArgs e)
		{
            if (null != Globals.UserRegistration)
            {
                Process.Start("http://www.ProfitGrabber.com/Account/GetLiveAnswer.cfm?GUID={" + Globals.UserRegistration.IDUser.ToString() + "}");                
            }
		}

		private void miTestamonials_Click(object sender, System.EventArgs e)
		{
            if (null != Globals.UserRegistration)
            {
                Process.Start("http://www.ProfitGrabber.com/Account/GetCredibility.cfm?GUID={" + Globals.UserRegistration.IDUser.ToString() + "}");                
            }
		}

		private void miCOMPS_Click(object sender, System.EventArgs e)
		{
            if (null != Globals.UserRegistration)
            {
                Process.Start("http://www.ProfitGrabber.com/Account/PropertyData.cfm?GUID={" + Globals.UserRegistration.IDUser.ToString() + "}");                
            }
		}        

		private void miMyActiveModules_Click(object sender, System.EventArgs e)
		{
			ActiveModulesForm amf = new ActiveModulesForm();
			amf.Show();
		}
		
		private void miSelectDatabase_Click(object sender, System.EventArgs e)
		{
			//sasa: 12.12.05, network bugfix, contact screen loaded variable
			bool tmpContactScreenOnMainForm = Globals.ContactScreenOnMainForm;
			Globals.ContactScreenOnMainForm = false;
			
			Network.DatabaseSelect databaseSelectForm = new Network.DatabaseSelect();
			databaseSelectForm.RestartRequired = true;
			//databaseSelectForm.CancelEnabled = false;
			
			databaseSelectForm.ShowDialog();

			//sasa: 12.12.05, network bugfix, contact screen loaded variable
			Globals.ContactScreenOnMainForm = tmpContactScreenOnMainForm;
		}
		
		private void mi_purgeOldTaks_Click(object sender, System.EventArgs e)
		{
			DealMaker.Forms.SchedulingActicities.PurgeOldTasks purgeOldTasks = new DealMaker.Forms.SchedulingActicities.PurgeOldTasks();
			purgeOldTasks.ShowDialog();
		}

		private void mi_MatchingModule_Click(object sender, System.EventArgs e)
		{
			MM_Matching_Module mm = new MM_Matching_Module();
			mm.Dock = DockStyle.Fill;

            SwapUserControls(mm);
            //this.appBodyPanel.Controls.RemoveAt(0);
			//this.appBodyPanel.Controls.Add(mm);
		}

		private void _miInterfaxAccountSetup_Click(object sender, System.EventArgs e)
		{
			SetupInterfaxAccountForm form = new SetupInterfaxAccountForm();
			form.ShowDialog();
		}

		private void On_ShortSaleModule_Click(object sender, EventArgs e)
		{
			HUD_MODULE.HUD hud = new HUD_MODULE.HUD(System.Environment.CurrentDirectory);
			hud.Show();
		}

		private void _miServerInfo_Click(object sender, System.EventArgs e)
		{
			ServerInfoForm sif = new ServerInfoForm();
			sif.ShowDialog();

			if (sif.ValidClose)
			{
				ApplicationHelper.SaveServerKeyCode(sif.Serial);
			}
		}
		
		//TODO_INT - ovog nema u novom provjeri		
		private void OnBackupRestoreManagerClick(object sender, EventArgs e)
		{
			//TODO: implement Custom message !
			Process.Start(Application.StartupPath + @"\UpdateManager.exe");
		}

		private void OnScanForDuplicates_Click(object sender, EventArgs e)
		{
			PurgeDuplicatesControl pdc = new PurgeDuplicatesControl();			
			pdc.Dock = DockStyle.Fill;

            SwapUserControls(pdc);
            //this.appBodyPanel.Controls.RemoveAt(0);
			//this.appBodyPanel.Controls.Add(pdc);
		}

		private void On_AccountUsage_Click(object sender, EventArgs e)
		{						
			int month;
			int topUp;
			Guid userId = Guid.Empty;
			string msg = string.Empty;
			bool res = false;

			try
			{
				ServiceDataConsumption sdc  = new ServiceDataConsumption();

                if (string.Empty != Globals.ProfitGrabberWebSupportURLDataConsumption)
                {
                    sdc.Url = Globals.ProfitGrabberWebSupportURLDataConsumption;
                }

                bool queryGuidRes = sdc.QueryGuidFromKey(Globals.DataConsumptionReg.KeyCode, out userId, out msg);
				res = sdc.QueryAcc(userId, out month, out topUp);
				if (res)			
					MessageBox.Show("Available credits for current month: " + month.ToString() + System.Environment.NewLine + "Available TopUp Credits: " + topUp.ToString(), "ProfitGrabber Pro - Account Details", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
			catch (Exception exc)
			{
				MessageBox.Show("Can not access web service resources!" + System.Environment.NewLine + exc.Message, "ProfitGrabber Pro - Account Details", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}			
		}

        void On_SetShortSaleFileFolder_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            fbd.SelectedPath = DealMaker.PlainClasses.Utils.ContentCopier.Instance.GetOriginPath();
            fbd.ShowNewFolderButton = true;
            fbd.Description = "This folder will be copied with 'Create Standard Folder' in the Short Sale tab. This folder should contain all subfolders and standard files you'd like to copy for each new short sale property you have in process.";

            if (DialogResult.OK == fbd.ShowDialog())
            {
                DealMaker.PlainClasses.Utils.ContentCopier.Instance.SaveOriginPath(fbd.SelectedPath);
            }
        }
        
        void On_SetWorkingFileFolder_Click(object sender, EventArgs e)
        {
            DefaultInfoValues div = null;
            string error = string.Empty;
            DataManager.Instance.LoadDefaultInfoValues(out div, out error);

            if (null == div)
                div = new DefaultInfoValues();

            FolderBrowserDialog fbd = new FolderBrowserDialog();
            fbd.ShowNewFolderButton = true;

            fbd.Description += "STEP 1: Select location or a folder in which you will keep all the documents for each property.";
            fbd.Description += "Then click �Make New Folder� button to create a new folder and call it �My Short Sale Deals�.";

            if (string.Empty != div.WorkingFileFolder)
                if (Directory.Exists(div.WorkingFileFolder))
                {
                    fbd.SelectedPath = div.WorkingFileFolder;
                }

            if (DialogResult.OK == fbd.ShowDialog())
            {
                div.WorkingFileFolder = fbd.SelectedPath;
                DataManager.Instance.UpdateDefaultInfoValues(div, out error);


                //if (!Directory.Exists(div.WorkingFileFolder + @"\" + Path.GetFileNameWithoutExtension(DataManager.Instance.DocumentName)))
                //    Directory.CreateDirectory(div.WorkingFileFolder + @"\" + Path.GetFileNameWithoutExtension(DataManager.Instance.DocumentName));

                //try
                //{
                //    if (!File.Exists(div.WorkingFileFolder + @"\" + Path.GetFileNameWithoutExtension(DataManager.Instance.DocumentName) + @"\" + "PhoneLog_" + Path.GetFileNameWithoutExtension(DataManager.Instance.DocumentName) + "_.xls"))
                //        File.Copy(Application.StartupPath + @"\AllSheets.xls", div.WorkingFileFolder + @"\" + Path.GetFileNameWithoutExtension(DataManager.Instance.DocumentName) + @"\" + "PhoneLog_" + Path.GetFileNameWithoutExtension(DataManager.Instance.DocumentName) + "_.xls");
                //}
                //catch { }


                SubfolderStructure subStruct = new SubfolderStructure();
                if (DialogResult.OK == subStruct.ShowDialog())
                {
                    div = subStruct.div; 
                    //foreach (string subFolder in StringStore.PredefinedFolders)
                    //{
                    //    Directory.CreateDirectory(div.WorkingFileFolder + @"\" + Path.GetFileNameWithoutExtension(DataManager.Instance.DocumentName) + @"\" + subFolder);
                    //}

                    //DataManager.Instance.LoadDefaultInfoValues(out div, out error);
                    //if (null != div.UserSubFolderStructField && 0 != div.UserSubFolderStructField.Length)
                    //{
                    //    foreach (string subFolder in div.UserSubFolderStructField)
                    //    {
                    //        Directory.CreateDirectory(div.WorkingFileFolder + @"\" + Path.GetFileNameWithoutExtension(DataManager.Instance.DocumentName) + @"\" + subFolder);
                    //    }
                    //}
                }
            }
        }

        void On_SelectLogFile_Click(object sender, EventArgs e)
        {
            DefaultInfoValues div = null;
            string error = string.Empty;
            DataManager.Instance.LoadDefaultInfoValues(out div, out error);

            if (null == div)
            {
                MessageBox.Show("Before you select your Log File for your shortcut button, you must define location of your Working Folders." + System.Environment.NewLine + System.Environment.NewLine + "Click OK and set your folders by going to Set Your Working Folder command under File menu (it takes only a few clicks to complete this setup).", StringStore.AppName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            else
            {
                //MessageBox.Show("Select the file you want �Open Log File� button to open." + Environment.NewLine + Environment.NewLine + "This is a convenient shortcut from a Lien tab that is under Get Approval tab. Most investors select their XL document as an additional log.", StringStore.AppName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                CustomPopUpForm cpf = new CustomPopUpForm();
                cpf.ShowDialog();
                OpenFileDialog ofd = new OpenFileDialog();

                ofd.InitialDirectory = div.WorkingFileFolder + @"\" + Path.GetFileNameWithoutExtension(DataManager.Instance.DocumentName);
                ofd.Filter = "Excel Files (*.xls; *.xlsx)|*.xls;*.xlsx|Word Files (*.doc; *.docx)|*.doc;*.docx|Text Files (*.txt)|*.txt";

                if (DialogResult.OK == ofd.ShowDialog())
                    DataManager.Instance.Log = ofd.FileName;
            }
        }

        void On_RepairDatabase_Click(object sender, EventArgs e)
        {
            //string txt;
            //ScriptedFixes fixes = new ScriptedFixes();
            //fixes.ApplyFixes(out txt);

            ApplyFixes afForm = new ApplyFixes();
            afForm.ShowDialog();
        }

        void On_SetCustomSettings_Click(object sender, EventArgs e)
        {
            CustomSettingsForm csf = new CustomSettingsForm();
            csf.ShowDialog();
        }

        private void _updateKit_Click(object sender, EventArgs e)
        {
            DealMaker.PlainClasses.ShortSale.Adapters.DataManager.Instance.SQLConnection = Connection.getInstance().HatchConn;
            DealMaker.PlainClasses.ShortSale.Conversion.Converter converter = new DealMaker.PlainClasses.ShortSale.Conversion.Converter();
        }

        void On_SmtpSetup_Click(object sender, EventArgs e)
        {            
            SetupEmailV2 setupEmailForm = new SetupEmailV2();
            var dialogResult = setupEmailForm.ShowDialog();
            
            //SMTPObject smtpSettings = new SMTPObject();
            //smtpSettings = smtpSettings.LoadSettigns();

            //SetupEmail setupEmailForm = new SetupEmail(smtpSettings);
            //DialogResult dRes = setupEmailForm.ShowDialog();

            //if (DialogResult.OK == dRes)
            //    setupEmailForm.SmtpSettings.PersistSettings();


            //SMTPObject smtpObject = new SMTPObject("smtp.gmail.com", "tomislav.abramovic@gmail.com", "tomislav.abramovic@gmail.com", "myPwd");
            //string errMsg = string.Empty;
            //bool retVal = smtpObject.TestSettings("tomislav.abramovic@gmail.com", out errMsg);
            //bool stopHere = false;
        }

        async void On_SendTasksViaEmail_Click(object sender, EventArgs e)
        {                                                                                                    
            try
            {
                var smtpObject = new SMTPObject().LoadSettigns();

                if (string.IsNullOrEmpty(smtpObject?.EmailAddress) || string.IsNullOrEmpty(smtpObject?.DisplayName))
                {
                    MessageBox.Show($"Please setup account data!{System.Environment.NewLine}Go to File >> Email Setup >> Sending Email - SMTP Setup", "ProfitGrabber Pro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                Dictionary<Guid, List<string>> officeStaffTasks = Task2EmailWorker.Instance.GetOfficeStaffTasks();
                var mailMsgs = Task2EmailWorker.Instance.PrepareMailMsgs(officeStaffTasks, smtpObject.EmailAddress, smtpObject.DisplayName);

                foreach (var mailMsg in mailMsgs)
                {
                    using (var emailService = new EmailService())
                    {
                        try
                        {
                            await emailService.InitializeGmailAccountService(Globals.ProductName, Globals.ClientId, Globals.ClientSecret, Globals.GetUserToAuthorize(smtpObject?.EmailAddress));                        
                            var response = emailService.SendEmail(mailMsg);
                        }
                        catch (Exception exc)
                        {
                            //build list of msgs that didn't send successfully

                            MessageBox.Show($"Please click OK and try to send email again and Profit Grabber will try to authenticate the Email again.{System.Environment.NewLine}{System.Environment.NewLine}{exc.Message}");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Could not sent emails.{System.Environment.NewLine}{System.Environment.NewLine}{ex.Message}", "ProfitGrabber Pro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }                                                                        
        }        

        void On_ViewSentTasksLog_Click(object sender, EventArgs e)
        {
            string task2EmailLog = string.Empty;
            try 
            {
                task2EmailLog = DealMaker.Properties.Settings.Default["Task2EmailLastSent"].ToString();

                if (!String.IsNullOrEmpty(task2EmailLog))
                    MessageBox.Show("Last email sent to: " + task2EmailLog, "ProfitGrabberPro", MessageBoxButtons.OK, MessageBoxIcon.Information);            
                else
                    MessageBox.Show("Service has not yet sent any emails out", "ProfitGrabberPro", MessageBoxButtons.OK, MessageBoxIcon.Information);            
            }
            catch (Exception exc)
            {
                MessageBox.Show("Error in Log" + System.Environment.NewLine + exc.Message, "ProfitGrabberPro", MessageBoxButtons.OK, MessageBoxIcon.Error);            
            }
        }

        void SendOutEmailsThread()
        {
            Task2EmailWorker.Instance.SendOutEmails();            
        }

        void On_ViewEmailProcessorConsole_Click(object sender, EventArgs e)
        {
            EmailProcessorConsole epcForm = new EmailProcessorConsole();
            epcForm.Show();
        }


        void On_ReadSSXManual_Click(object sender, EventArgs e)
        {
            Process.Start("http://www.profitgrabber.com/short-sale-accelerator-manual.html");            
        }

        private void _routing_Click(object sender, EventArgs e)
        {
            //sasa: 12.12.05, network bugfix, contact screen loaded variable
            Globals.ContactScreenOnMainForm = false;
            //sasa: 27.01.06, network lock, stop query
            Network.NetworkLock.Stop();

            //TA: listview should always show content of currently selected group

            PropertyItemListViewControlRouting pilwc = new PropertyItemListViewControlRouting();

            //if (null == Globals.oldListView)
            {
                if (-1 != Globals.selectedNodeId)
                {
                    Hashtable htNodePosPropertyItemId = ApplicationHelper.GetPropertyItemsInNode(Globals.selectedNodeId);
                    if (0 != htNodePosPropertyItemId.Count)
                        pilwc = new PropertyItemListViewControlRouting(Globals.selectedNodeId, Globals.selectedNodeName);
                    else
                        pilwc = new PropertyItemListViewControlRouting();
                }
                else
                {
                    SelectGroupForm sgf = new SelectGroupForm();
                    sgf.customizedTreeViewUserControl1.UpdateGlobals = true;
                    sgf.ShowDialog();
                    if (true == sgf.Valid)
                    {
                        Globals.selectedNodeId = sgf.customizedTreeViewUserControl1.SelectedNodeId;
                    }
                    else
                        return;

                    if (ApplicationHelper.GetPropertyCountInNode(Globals.selectedNodeId) >= 5000)
                    {
                        MessageBox.Show(this, "Due to performance reasons List View can operate only on groups which contain less than 5000 contacts!", "Large Group!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        return;
                    }
                    pilwc = new PropertyItemListViewControlRouting(Globals.selectedNodeId, Globals.selectedNodeName);
                }
            }
            //else
            //{				
            //	pilwc = Globals.oldListView;
            //}

            pilwc.Size = Globals.applicationSize;
            pilwc.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top;
            pilwc.Dock = DockStyle.Fill;

            SwapUserControls(pilwc);
            //this.appBodyPanel.Controls.RemoveAt(0);
            //this.appBodyPanel.Controls.Add(pilwc);
        }

        public void SwapUserControls(UserControl uc)
        {
            CheckSaveDocument();

            this.appBodyPanel.Controls.RemoveAt(0);
            this.appBodyPanel.Controls.Add(uc);

            if (uc is EditDocumentTemplateUserControl)
            {
                EditDocumentTemplateUserControl edtuc = uc as EditDocumentTemplateUserControl;
                this.Text = $"{Globals.ProductName} - {edtuc.documentName}";
            }
        }

        void CheckSaveDocument()
        {
            if (appBodyPanel.Controls[0] is EditDocumentTemplateUserControl)
            {
                if (edtucHasDocumentChanged)
                {
                    DialogResult dr = MessageBox.Show("Save changes?", "Unsaved Changes", MessageBoxButtons.YesNo, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                    if (DialogResult.Yes == dr)
                    {
                        EditDocumentTemplateUserControl edtuc = appBodyPanel.Controls[0] as EditDocumentTemplateUserControl;

                        if (edtucIsDocumentTemplete)
                            edtuc.SaveDocumentTemplate(null, null);
                        else
                            edtuc.SaveDocument(null, null);

                        return;
                    }
                }
            }
        }

        private void _viewSentEmails_Click(object sender, EventArgs e)
        {
            Email_Overview eo = new Email_Overview(Globals.UserRegistration.IDUser);
            eo.ShowDialog();
        }

        private void miDataAppend_Click(object sender, EventArgs e)
        {
            if (null != Globals.UserRegistration)
            {
                Process.Start("https://www.profitgrabber.com/SkipTracing/plans.php?PGID=" + Globals.UserRegistration.IDUser.ToString());                
            }           
        }

        private void _skipTrace_Click(object sender, EventArgs e)
        {
            Forms.AppendDataForm.SkipTraceForm sf = new Forms.AppendDataForm.SkipTraceForm();
            sf.ShowDialog();
        }

        private void _emailMarketingFooterSetup_Click(object sender, EventArgs e)
        {
            SetupFooter sf = new SetupFooter();
            sf.ShowDialog();
        }
        private void _importRealEFlow_Click(object sender, EventArgs e)
        {
            var realEFlowImportMask = new RealEFlowImportMask(Globals.SelectGroup?.customizedTreeViewUserControl1);
            realEFlowImportMask.ShowDialog();
        }

        private void _dealFinder360GetLeads_Click(object sender, EventArgs e)
        {
            if (null != Globals.UserRegistration)
            {
                Process.Start($"http://www.profitgrabber.com/df360?PGID={Globals.UserRegistration.IDUser}");                
            }
        }
    }
}
