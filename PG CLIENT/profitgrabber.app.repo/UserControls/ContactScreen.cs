#region � Using �
using System;
using System.Collections;
using System.Diagnostics;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Data;
using System.Windows.Forms;
using System.Data.SqlClient; 
using System.Data.SqlTypes;
using System.Reflection;

using NHibernate;
using NHibernate.Cfg;

using DealMaker.PlainClasses.Utils;
using DealMaker.PlainClasses.ContactTypes;

//Floaters
using DealMaker.Forms.Floaters;
using ProfitGrabber.Commercial;
using ProfitGrabber.Commercial.DALC;
using ProfitGrabber.Commercial.Objects;

using System.Drawing.Printing;
using ProfitGrabber.Common;

using DealMaker.UserControls.ShortSale;
using DealMaker.Forms.AppendDataForm;
using Newtonsoft.Json;
using System.Globalization;

#endregion � Using �
#region � Namespace DealMaker �
namespace DealMaker
{
	#region � Class ContactScreen �
	/// <summary>
	/// Summary description for ContactScreen.
	/// </summary>
	public partial class ContactScreen : System.Windows.Forms.UserControl, IAutoSave, IAppendDataForm
    {
        #region DATA
        private System.Windows.Forms.TabPage tpCommercial;
        private ProfitGrabber.Commercial.CommercialControl _commercialControl;
        private TabPage tpContactInfo;
        private TextBox tbMailOldCityState;
        private TextBox tbMailPreviousCityState;
        private TextBox tbMailOldState;
        private TextBox tbMailPreviousState;
        private TextBox tbMailLastKnownState;
        private Label label125;
        private ReadMe readMe3;
        private PictureBox pictureBox1;
        private Label label63;
        private Label label62;
        private Label label60;
        private TextBox tbMailOldCity;
        private TextBox tbMailPreviousCity;
        private TextBox tbMailLastKnownCity;
        private Label label10;
        private TextBox tbCellBestTime;
        private TextBox tbWorkBestTime;
        private TextBox tbHomeBestTime;
        private TextBox tbEmail;
        private TextBox tbFax;
        private TextBox tbCellPh;
        private TextBox tbWorkPh;
        private TextBox tbHomePh;
        private Label label17;
        private Label label16;
        private Label label15;
        private TextBox tbMailOldZIP;
        private TextBox tbMailOldAdd;
        private TextBox tbMailPreviousZIP;
        private TextBox tbMailPreviousAdd;
        private TextBox tbMailLastKnownZIP;
        private TextBox tbMailLastKnownAdd;
        private Label label14;
        private Label label13;
        private Label label12;
        private Label label19;
        private Label label18;
        private Label label20;
        private Label label163;
        private TextBox tbMailLastKnownCityState;
        private TabPage _buyersInfoTab;
        private BuyerInfoOverView _buyerInfoOverview;
        private TabPage _shortSaleTab;
        
        private Button _unLockPI;
        private TabPage _evaluate;
        private TabControl _evaluateSubTabControl;
        private TabPage _propertyProfileSubTabPage;
        private TabPage _compsSubTabPage;
        private TabPage _estimateRepairsSubTabPage;
        private DealMaker.OnlineData.UserControlPropertyProfileComps ucppc;
        private DealMaker.OnlineData.UserControlPropertyProfileCompsListView ucppclv;
        private DealMaker.UserControls.Repairs.EstimateRepairsControl _repairs;
        private DealMaker.UserControls.ShortSale.ShortSaleControl _shortSaleControl;
        private PictureBox _pbHouse;

        ContactTypes _contactType = ContactTypes.Seller;
        #endregion

        Network.NetworkLock.LockStatus lastLockstatus = Network.NetworkLock.LockStatus.Pending;
        private TextBox _ci_NotesField;
        private Label _ci_NotesLabel;
        private ContextMenuStrip ctxPbHouseOptions;
        private ToolStripMenuItem getStreetView;
        private ToolStripMenuItem uploadPhoto;
        CustomSettings _customSettings = null;
        private TextBox _stLandline;
        private Label label236;
        private TextBox _stCellPhone;
        private Label label201;
        private TextBox _stEmail;
        private Label label192;
        private Label _stResultsLabel;
        private Label _stLastUpdated;
        private TabPage tpSkipTracedInfo;
        private TabControl skipTracedInfoTabControl;
        private TabPage skipTracedContactInfo;
        private Label label92;
        private TabPage skipTracedOtherProperty;
        private Label label260;
        private Label label259;
        private TextBox st_ci_fullName;
        private TextBox st_ci_lastName;
        private Label label256;
        private Label label257;
        private Label label258;
        private TextBox st_ci_aliasName;
        private TextBox st_ci_age;
        private TextBox st_ci_firstName;
        private TextBox st_ci_middleName;
        private TextBox st_ci_email2;
        private Label label255;
        private TextBox st_ci_email1;
        private Label label248;
        private TextBox st_ci_rp4;
        private Label label254;
        private TextBox st_ci_rp3;
        private Label label253;
        private TextBox st_ci_mp2;
        private Label label252;
        private TextBox st_ci_mp1;
        private Label label251;
        private TextBox st_ci_mp0;
        private Label label249;
        private TextBox st_ci_email0;
        private Label label250;
        private TextBox st_ci_a2_lastSeen;
        private TextBox st_ci_a1_lastSeen;
        private TextBox st_ci_a0_lastSeen;
        private Label label247;
        private TextBox st_ci_a2_firstSeen;
        private TextBox st_ci_a1_firstSeen;
        private TextBox st_ci_a0_firstSeen;
        private Label label246;
        private TextBox st_ci_a2_state;
        private TextBox st_ci_a1_state;
        private TextBox st_ci_a0_state;
        private Label label239;
        private Label label240;
        private Label label241;
        private Label label242;
        private TextBox st_ci_a2_city;
        private TextBox st_ci_a1_city;
        private TextBox st_ci_a0_city;
        private TextBox st_ci_a2_zip;
        private TextBox st_ci_a2_address;
        private TextBox st_ci_a1_zip;
        private TextBox st_ci_a1_address;
        private TextBox st_ci_a0_zip;
        private TextBox st_ci_a0_address;
        private Label label243;
        private Label label244;
        private Label label245;
        private Label label238;
        private Label label237;
        private WindowsApplication.AcquisitionCalculator.TextBox st_op_bedrooms3;
        private WindowsApplication.AcquisitionCalculator.TextBox st_op_bathrooms3;
        private WindowsApplication.AcquisitionCalculator.TextBox st_op_yearBuilt3;
        private WindowsApplication.AcquisitionCalculator.TextBox st_op_buildingSqFt3;
        private WindowsApplication.AcquisitionCalculator.TextBox st_op_lotSqFt3;
        private WindowsApplication.AcquisitionCalculator.TextBox st_op_parcelNumber3;
        private WindowsApplication.AcquisitionCalculator.TextBox st_op_useCode3;
        private WindowsApplication.AcquisitionCalculator.TextBox st_op_county3;
        private WindowsApplication.AcquisitionCalculator.TextBox st_op_address3;
        private Label label279;
        private Label label280;
        private Label label281;
        private Label label282;
        private Label label283;
        private Label label284;
        private Label label285;
        private Label label286;
        private System.Windows.Forms.ListView st_op_lv3;
        private ColumnHeader columnHeader9;
        private ColumnHeader columnHeader10;
        private ColumnHeader columnHeader11;
        private ColumnHeader columnHeader12;
        private ColumnHeader columnHeader13;
        private ColumnHeader columnHeader15;
        private ColumnHeader columnHeader16;
        private Label label287;
        private WindowsApplication.AcquisitionCalculator.TextBox st_op_bedrooms2;
        private WindowsApplication.AcquisitionCalculator.TextBox st_op_bathrooms2;
        private WindowsApplication.AcquisitionCalculator.TextBox st_op_yearBuilt2;
        private WindowsApplication.AcquisitionCalculator.TextBox st_op_buildingSqFt2;
        private WindowsApplication.AcquisitionCalculator.TextBox st_op_lotSqFt2;
        private WindowsApplication.AcquisitionCalculator.TextBox st_op_parcelNumber2;
        private WindowsApplication.AcquisitionCalculator.TextBox st_op_useCode2;
        private WindowsApplication.AcquisitionCalculator.TextBox st_op_county2;
        private WindowsApplication.AcquisitionCalculator.TextBox st_op_address2;
        private Label label270;
        private Label label271;
        private Label label272;
        private Label label273;
        private Label label274;
        private Label label275;
        private Label label276;
        private Label label277;
        private System.Windows.Forms.ListView st_op_lv2;
        private ColumnHeader columnHeader1;
        private ColumnHeader columnHeader2;
        private ColumnHeader columnHeader3;
        private ColumnHeader columnHeader4;
        private ColumnHeader columnHeader5;
        private ColumnHeader columnHeader7;
        private ColumnHeader columnHeader8;
        private Label label278;
        private WindowsApplication.AcquisitionCalculator.TextBox st_op_bedrooms1;
        private WindowsApplication.AcquisitionCalculator.TextBox st_op_bathrooms1;
        private WindowsApplication.AcquisitionCalculator.TextBox st_op_yearBuilt1;
        private WindowsApplication.AcquisitionCalculator.TextBox st_op_buildingSqFt1;
        private WindowsApplication.AcquisitionCalculator.TextBox st_op_lotSqFt1;
        private WindowsApplication.AcquisitionCalculator.TextBox st_op_parcelNumber1;
        private WindowsApplication.AcquisitionCalculator.TextBox st_op_useCode1;
        private WindowsApplication.AcquisitionCalculator.TextBox st_op_county1;
        private WindowsApplication.AcquisitionCalculator.TextBox st_op_address1;
        private Label label269;
        private Label label268;
        private Label label267;
        private Label label266;
        private Label label265;
        private Label label264;
        private Label label263;
        private Label label262;
        private System.Windows.Forms.ListView st_op_lv1;
        private ColumnHeader transferDate;
        private ColumnHeader seller;
        private ColumnHeader buyer;
        private ColumnHeader transferType;
        private ColumnHeader loanValue;
        private ColumnHeader lenderName;
        private ColumnHeader interesetType;
        private Label label261;
        private TabPage skipTracedRelativesInfo;
        private System.Windows.Forms.ListView st_ri_relatives;
        private ColumnHeader columnHeader6;
        private ColumnHeader columnHeader14;
        private ColumnHeader columnHeader17;
        private ColumnHeader columnHeader18;
        private ColumnHeader columnHeader19;
        private ColumnHeader columnHeader20;
        private ColumnHeader columnHeader21;
        private WindowsApplication.AcquisitionCalculator.TextBox st_op_bedrooms4;
        private WindowsApplication.AcquisitionCalculator.TextBox st_op_bathrooms4;
        private WindowsApplication.AcquisitionCalculator.TextBox st_op_yearBuilt4;
        private WindowsApplication.AcquisitionCalculator.TextBox st_op_buildingSqFt4;
        private WindowsApplication.AcquisitionCalculator.TextBox st_op_lotSqFt4;
        private WindowsApplication.AcquisitionCalculator.TextBox st_op_parcelNumber4;
        private WindowsApplication.AcquisitionCalculator.TextBox st_op_useCode4;
        private WindowsApplication.AcquisitionCalculator.TextBox st_op_county4;
        private WindowsApplication.AcquisitionCalculator.TextBox st_op_address4;
        private Label label288;
        private Label label289;
        private Label label290;
        private Label label291;
        private Label label292;
        private Label label293;
        private Label label294;
        private Label label295;
        private System.Windows.Forms.ListView st_op_lv4;
        private ColumnHeader columnHeader22;
        private ColumnHeader columnHeader23;
        private ColumnHeader columnHeader24;
        private ColumnHeader columnHeader25;
        private ColumnHeader columnHeader26;
        private ColumnHeader columnHeader27;
        private ColumnHeader columnHeader28;
        private Label label296;
        private Button _skipTraceRecord;
        private Label label297;
        private Button _copySTRank0InfoToContactInfoTab;
        private Panel _ci_old_panel;
        private Panel _ci_previous_panel;
        private Panel _ci_lk_panel;
        private Panel panel3;
        private Panel _ci_email_panel;
        private Panel _ci_fax_panel;
        private Panel _ci_cellphone_panel;
        private Panel _ci_workphone_panel;
        private Panel _ci_homephone_panel;
        private Panel panel2;
        private Panel panel1;
        private Panel _ci_st_landline_panel;
        private Panel _ci_st_cell_panel;
        private Panel _ci_st_email_panel;
        private Panel panel14;
        private Panel panel15;
        private Panel _up_names_panel;
        private Panel _up_address_panel;
        private Panel panel4;
        private Panel _ci_notes;

        #region � Network Lock �
        //sasa, 25.01.06: new nw version, lock

        Object messageBoxHelperLock = new Object();

        private delegate void LockUpdateDelegate();
        private void NetworkLock_StatusChanged()
        {
            lock (messageBoxHelperLock)
            {
                if (Globals.ContactScreenOnMainForm && this.pi != null)
                {
                    this.BeginInvoke(new LockUpdateDelegate(UpdateGuiLockStatus));
                }
            }
        }

		public void UpdateGuiLockStatus()
		{
			if(Network.NetworkLock.GetStatus() == Network.NetworkLock.LockStatus.Pending)
			{
				lockStatus.Text = "Pending";
				SetReadWrite();
			}

			if(Network.NetworkLock.GetStatus() == Network.NetworkLock.LockStatus.Locked)
			{
/*				bool showBox = false;
				lock(messageBoxHelperLock)
				{
					if(lastLockstatus != Network.NetworkLock.LockStatus.Locked) 
					{
						showBox = true;
						lastLockstatus = Network.NetworkLock.GetStatus();
					}
				}
				if(showBox) MessageBox.Show("FREE is locked by another user, please wait until it's freed!","Attention!",MessageBoxButtons.OK,MessageBoxIcon.Warning);
*/
				lockStatus.Text = "Locked";
				SetReadWrite();
			}

			if(Network.NetworkLock.GetStatus() == Network.NetworkLock.LockStatus.ReadOnly)
			{
				bool showBox = false;
				lock(messageBoxHelperLock)
				{
					if(lastLockstatus != Network.NetworkLock.LockStatus.ReadOnly) 
					{
						showBox = true;
						lastLockstatus = Network.NetworkLock.GetStatus();
					}
				}
				if(showBox) MessageBox.Show("Contact is locked by another user, please wait until it's freed!","Attention!",MessageBoxButtons.OK,MessageBoxIcon.Warning);

				lockStatus.Text = "ReadOnly";
				SetReadOnly();
			}

			lock(messageBoxHelperLock)
			{
				lastLockstatus = Network.NetworkLock.GetStatus();
			}
		}

		public void ReloadContact()
		{
			if(this.pi != null && this.pi.IdPropertyItem != Guid.Empty)
			{
//				Network.NetworkLock.RequestLock(this.pi.IdPropertyItem);

                //TA: BEGIN CustomContactTypes - Commented out 2013.June.04
                //this.LoadDisplayData(this.pi.IdPropertyItem);
                //TA: END
			}
		}


		//sasa, 06.02.2006: new nw screen locking, comps and some other things should not
		//be disabled on read only
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Button button2;

		bool readOnly = false;

		void SetEditable(Control control, bool editable)
		{
			foreach(Control c in control.Controls)
			{
				if(c.Tag != null ? c.Tag.ToString() != "RW" : true)
				{
					if(c is TextBox ||
						c is DateTimePicker ||
						c is ComboBox ||
						c is Button ||
						c is System.Windows.Forms.ListView ||
						c is DealMaker.Forms.SchedulingActicities.TaskAndHistoryList //|| 
                        //c is UserControlPropertyProfileComps ||
                        //c is UserControlPropertyProfileCompsListView
						) c.Enabled = editable;

					if(c.Controls.Count > 0) SetEditable(c, editable);
				}
			}
		}

		public void SetReadOnly()
		{
			if(!readOnly)
			{
				bSave.Enabled = false;

				tbFirstname.Enabled = false;
				tbMiddleName.Enabled = false;
				tbLastName.Enabled = false;
				tbSiteAdd.Enabled = false;
				tbSiteCity.Enabled = false;
				tbSiteState.Enabled = false;
				tbSiteZIP.Enabled = false;
				tbCounty.Enabled = false;
				tbSubDivision.Enabled = false;

				SetEditable(_contactTab, false);
			}
			readOnly = true;
		}

		public void SetReadWrite()
		{
			if(readOnly)
			{
				bSave.Enabled = true;

				tbFirstname.Enabled = true;
				tbMiddleName.Enabled = true;
				tbLastName.Enabled = true;
				tbSiteAdd.Enabled = true;
				tbSiteCity.Enabled = true;
				tbSiteState.Enabled = true;
				tbSiteZIP.Enabled = true;
				tbCounty.Enabled = true;
				tbSubDivision.Enabled = true;
			}

			SetEditable(_contactTab, true);

			readOnly = false;
		}

		#endregion � Network Lock �

		#region � Network UDP �
		//		//sasa, 13.11.05: nw version, invoke required (refresh thread in different task)
		private delegate void UpdateDelegate();
		
		public Network.NetworkChangedEventHandler networkEventHandler;
		public Network.NetworkLock.StatusChangedEventHandler networkEventHandler2;
		
		private System.Windows.Forms.Label lockStatus;

		Guid UpdateDelegateFunc_pi;

		private void UpdateDelegateFunc()
		{
			this.LoadDisplayData(UpdateDelegateFunc_pi);
		}
	
		//26.05.07: sasa, new user bug correction
		bool needRecount = false;
		private void Recount()
		{
			needRecount = false; //pazi, sam zove sebe iz LoadDisplayData
			Hashtable htNodePosPropertyItemId = ApplicationHelper.GetPropertyItemsInNode(this.currentPropertyItemGroup.nodeId);
			this.ContactScreenStarter(htNodePosPropertyItemId);
			this.tbCurrPos.Text = this.currentOffset.ToString() + " / " + htNodePosPropertyItemId.Count.ToString();
		}

		//we must recount pi-s if new added
		private void UpdateDelegateFunc_Recount()
		{
			//26.05.07: new user bug correction
			needRecount = true;
		}

		private void NetworkUDP_ChangedPI(Network.NetworkUDP.Packet p)
		{
			if(Globals.ContactScreenOnMainForm && this.pi != null && p.PropertyItemID == this.pi.IdPropertyItem)
			{
                if (null != _customSettings && 1 == _customSettings.SpecialSSX)
                {
                    UpdateDelegateFunc_pi = this.pi.IdPropertyItem;
                    Globals.MainForm.BeginInvoke(new UpdateDelegate(UpdateDelegateFunc));
                    Globals.MainForm.BeginInvoke(new UpdateDelegate(MarkAsExternallyChanged));
                }
                else   //DEAFULT BEHAVIOR                
                {
                    if (MessageBox.Show(this, "Other application has changed contact that you are currently working on." + System.Environment.NewLine + "Would you like to reload contact?", "Attention", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        UpdateDelegateFunc_pi = this.pi.IdPropertyItem;
                        Globals.MainForm.BeginInvoke(new UpdateDelegate(UpdateDelegateFunc));
                    }
                }
			}
			else
			{
				Globals.MainForm.BeginInvoke(new UpdateDelegate(UpdateDelegateFunc_Recount));
			}
		}

        void MarkAsExternallyChanged()
        {
            this.tbLastModified.BackColor = Color.Red;
        }
		#endregion � Network UDP �

		#region � Data �		
		private DealMaker.Forms.Floaters.GroupMembershipTab groupMembershipFloater = null;
		private DealMaker.Forms.Floaters.NewContact_ContactView contactFloater = null;
		private DealMaker.Forms.Floaters.DealInfo dealInfoFloater = null;
        private DealMaker.Forms.Floaters.ImportedData importedFloater = null;	//TA:03.04.2005.: Dirty Hack to get the estimated value (BY COMPS) due to another meaningful request which was additionaly supplied.
		
		private PropertyItem pi = null;
		private Node currentPropertyItemStatusGroup = null;
		private Node currentPropertyItemGroup		= null;
		private bool bUpdate = false;
		
		private int currentOffset = 1;
		private Hashtable htNodePosPropertyItemId = null;

        private System.Windows.Forms.PictureBox pbMap;
        private System.Windows.Forms.ToolTip ttMap;
        private System.Windows.Forms.Button _copyDataToDealInfo;
        private System.ComponentModel.IContainer components;

		private System.Windows.Forms.Panel lowerPanel;
		private System.Windows.Forms.Panel upperPanel;
		private System.Windows.Forms.TextBox tbCurrPos;
		private System.Windows.Forms.Button bPrev;
		private System.Windows.Forms.Button bFirst;
		private System.Windows.Forms.Button bNext;
		private System.Windows.Forms.Button bLast;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox tbStatusGroup;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.TextBox tbFirstEntered;
		private System.Windows.Forms.TextBox tbLastModified;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.TextBox tbFirstname;
		private System.Windows.Forms.TextBox tbMiddleName;
		private System.Windows.Forms.TextBox tbLastName;
		private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox tbFullName;
		private System.Windows.Forms.TextBox tbSiteZIP;
		private System.Windows.Forms.TextBox tbSiteAdd;
		private System.Windows.Forms.TabPage tpDealInfo;
		private System.Windows.Forms.TabPage tpGroupMembership;
        private System.Windows.Forms.TabPage tpActivitiesHistory;
		private System.Windows.Forms.Label label55;
		private System.Windows.Forms.Label label58;
		private System.Windows.Forms.Label label59;
		private System.Windows.Forms.Button bLeadsNotProcessedYet;
		private System.Windows.Forms.Button bDealsInProgress;
		private System.Windows.Forms.Button bDeadLeads;
		private System.Windows.Forms.Button bPurchased;
		private System.Windows.Forms.Button bDoNotMailGroup;
		private System.Windows.Forms.TextBox tbGroupStatusGroup;
		private System.Windows.Forms.Label label61;
		private System.Windows.Forms.ListView lvOtherGroups;
		private System.Windows.Forms.ColumnHeader groupName;
		private System.Windows.Forms.ColumnHeader groupDesc;
		private System.Windows.Forms.Button bRemoveFromGroup;
		private System.Windows.Forms.Button bAssignToGroup;
		private System.Windows.Forms.ColumnHeader nodeId;
		private System.Windows.Forms.Button bNew;
		private System.Windows.Forms.Button bSave;
        private System.Windows.Forms.TabControl _contactTab;

        DealMaker.Forms.SchedulingActicities.TaskAndHistoryList taskAndHistoryList;
		private DealMaker.ReadMe readMe1;
		private System.Windows.Forms.Label label21;
		private System.Windows.Forms.ComboBox cbType;
		private System.Windows.Forms.Label label22;
		private System.Windows.Forms.Label label23;
		private System.Windows.Forms.Label label24;
		private System.Windows.Forms.TextBox tbSqFt;
		private System.Windows.Forms.Label label25;
		private System.Windows.Forms.Label label26;
		private System.Windows.Forms.Label label27;
		private System.Windows.Forms.ComboBox cbBedrooms;
		private System.Windows.Forms.ComboBox cbBaths;
		private System.Windows.Forms.ComboBox cbYearBuilt;
		private System.Windows.Forms.ComboBox cbGarage;
		private System.Windows.Forms.ComboBox cbPool;
		private System.Windows.Forms.Label label28;
		private System.Windows.Forms.TextBox tbCondition;
		private System.Windows.Forms.Label label29;
		private System.Windows.Forms.Label label30;
		private NumericTextBox tbWorthBySeller;
		private System.Windows.Forms.ComboBox cbSource;
		private System.Windows.Forms.Label label31;
		private System.Windows.Forms.Label label33;
		private System.Windows.Forms.Label label34;
		private NumericTextBox tb1Bal;
		private System.Windows.Forms.Label label35;
		private System.Windows.Forms.Label label36;
		private System.Windows.Forms.Label label37;
		private System.Windows.Forms.Label label38;
		private System.Windows.Forms.Label label39;
		private System.Windows.Forms.Label label40;
		private System.Windows.Forms.Label label42;
		private System.Windows.Forms.Label label43;
		private System.Windows.Forms.Label label44;
		private System.Windows.Forms.Label label46;
		private System.Windows.Forms.Label label47;
		private System.Windows.Forms.Label label48;
		private System.Windows.Forms.Label label49;
		private System.Windows.Forms.Label label50;
		private System.Windows.Forms.Label label51;
		private System.Windows.Forms.Label label52;
		private System.Windows.Forms.Label label53;
		private System.Windows.Forms.Label label56;
		private System.Windows.Forms.Label label57;
		private System.Windows.Forms.Label label64;
		private System.Windows.Forms.Label label65;
		private System.Windows.Forms.TextBox tbTotalOwed;
		private NumericTextBox tbOtherLiens;
		private NumericTextBox tb3MoPayment;
		private System.Windows.Forms.TextBox tb3IntRate;
		private NumericTextBox tbHOA;
		private NumericTextBox tb2MoPayment;
		private System.Windows.Forms.TextBox tb2IntRate;
		private NumericTextBox tb2Bal;
		private System.Windows.Forms.ComboBox cbTaxIncl;
		private NumericTextBox tb1MoPayment;
		private System.Windows.Forms.TextBox tb1IntRate;
		private System.Windows.Forms.TextBox tbWhySelling;
		private System.Windows.Forms.DateTimePicker dtpListingExpDate;
		private System.Windows.Forms.DateTimePicker dtpForSaleDate;
		private System.Windows.Forms.TextBox tbTotalMonthly;
		private System.Windows.Forms.TabPage tpImportedInfo;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.Label label32;
		private System.Windows.Forms.Label label68;
		private System.Windows.Forms.Label label69;
		private System.Windows.Forms.Label label70;
		private System.Windows.Forms.Label label71;
		private System.Windows.Forms.Label label72;
		private System.Windows.Forms.Label label73;
		private System.Windows.Forms.Label label54;
		private System.Windows.Forms.Label label74;
		private System.Windows.Forms.Label label75;
		private System.Windows.Forms.Label label76;
		private System.Windows.Forms.Label label77;
		private System.Windows.Forms.Label label66;
		private System.Windows.Forms.Label label67;
		private System.Windows.Forms.Label label78;
		private System.Windows.Forms.Label label79;
		private System.Windows.Forms.TextBox tbARV;
		private System.Windows.Forms.TextBox tbNextStep;
		private System.Windows.Forms.Label label80;
		private System.Windows.Forms.ComboBox cbOccupiedBy;
		private System.Windows.Forms.TextBox tbRepairs;
		private System.Windows.Forms.ComboBox cbLeadSource;
		private System.Windows.Forms.ComboBox cbMotivation;
		private System.Windows.Forms.TextBox tbMinCashNeeded;
		private System.Windows.Forms.ComboBox cbSell4What;
		private System.Windows.Forms.ComboBox cbMoBehind;
		private DealMaker.NumericTextBox tbTaxAmount;
		private System.Windows.Forms.ComboBox cb3Type;
		private System.Windows.Forms.ComboBox cb2Type;
		private DealMaker.NumericTextBox tbLowestPriceAcceptable;
		private System.Windows.Forms.ComboBox cbCanCancel;
		private System.Windows.Forms.ComboBox cbListed;
		private System.Windows.Forms.ComboBox cbPmntsCurrnet;
		private System.Windows.Forms.ComboBox cbInForecl;
		private DealMaker.NumericTextBox tbReinsAmnt;
		private DealMaker.NumericTextBox tb3Bal;
		private System.Windows.Forms.ComboBox cb1Type;
		private System.Windows.Forms.Label label41;
		private System.Windows.Forms.Label label45;
		private System.Windows.Forms.Label label81;
		private System.Windows.Forms.Label label82;
		private System.Windows.Forms.Label label83;
		private System.Windows.Forms.Label label84;
		private System.Windows.Forms.Label label85;
		private System.Windows.Forms.Label label86;
		private System.Windows.Forms.Label label87;
		private System.Windows.Forms.Label label88;
		private System.Windows.Forms.Label label89;
		private System.Windows.Forms.Label label90;
		private System.Windows.Forms.Label label91;
		private System.Windows.Forms.Label label93;
		private System.Windows.Forms.Label label94;
		private System.Windows.Forms.Label label95;
		private System.Windows.Forms.Label label96;
		private System.Windows.Forms.Label label97;
		private System.Windows.Forms.Label label98;
		private System.Windows.Forms.TabControl tcSubImportedInfo;
		private System.Windows.Forms.TabPage tpForeClosureInfo;
		private System.Windows.Forms.TabPage tpListingInfo;
		private System.Windows.Forms.TabPage tpPropertyCharacteristics;
		private System.Windows.Forms.TabPage tpFinancials;
		private System.Windows.Forms.TabPage tpTaxInfo;
		private System.Windows.Forms.TabPage tpOtherInfo;
		private System.Windows.Forms.Label label99;
		private System.Windows.Forms.Label label100;
		private System.Windows.Forms.Label label101;
		private System.Windows.Forms.Label label102;
		private System.Windows.Forms.Label label103;
		private System.Windows.Forms.Label label104;
		private System.Windows.Forms.Label label105;
		private System.Windows.Forms.Label label106;
		private System.Windows.Forms.Label label107;
		private System.Windows.Forms.Label label108;
		private System.Windows.Forms.Label label109;
		private System.Windows.Forms.Label label110;
		private System.Windows.Forms.Label label111;
		private System.Windows.Forms.Label label112;
		private System.Windows.Forms.Label label113;
		private System.Windows.Forms.Label label114;
		private System.Windows.Forms.Label label115;
		private System.Windows.Forms.Label label116;
		private System.Windows.Forms.Label label117;
		private System.Windows.Forms.Label label118;
		private System.Windows.Forms.Label label119;
		private System.Windows.Forms.Label label120;
		private System.Windows.Forms.Label label121;
		private System.Windows.Forms.Label label122;
		private System.Windows.Forms.Label label123;
		private System.Windows.Forms.Label label126;
		private System.Windows.Forms.Label label127;
		private System.Windows.Forms.Label label128;
		private System.Windows.Forms.Label label129;
		private System.Windows.Forms.Label label130;
		private System.Windows.Forms.Label label131;
		private System.Windows.Forms.Label label132;
		private System.Windows.Forms.Label label134;
		private System.Windows.Forms.Label label135;
		private System.Windows.Forms.Label label136;
		private System.Windows.Forms.Label label137;
		private System.Windows.Forms.Label label138;
		private System.Windows.Forms.Label label139;
		private System.Windows.Forms.Label label140;
		private System.Windows.Forms.Label label141;
		private System.Windows.Forms.Label label142;
		private System.Windows.Forms.Label label143;
		private System.Windows.Forms.Label label144;
		private System.Windows.Forms.Label label145;
		private System.Windows.Forms.Label label146;
		private System.Windows.Forms.Label label147;
		private System.Windows.Forms.Label label148;
		private System.Windows.Forms.Label label149;
		private System.Windows.Forms.Label label150;
		private System.Windows.Forms.Label label151;
		private System.Windows.Forms.Label label152;
		private System.Windows.Forms.Label label153;
		private System.Windows.Forms.Label label154;
		private System.Windows.Forms.Label label155;
		private System.Windows.Forms.Label label156;
		private System.Windows.Forms.Label label157;
		private System.Windows.Forms.Label label158;
		private System.Windows.Forms.Label label159;
		private System.Windows.Forms.Label label160;
		private System.Windows.Forms.Label label161;
		private System.Windows.Forms.Label label162;
		private System.Windows.Forms.Label label164;
		private System.Windows.Forms.Label label165;
		private System.Windows.Forms.Label label166;
		private System.Windows.Forms.Label label167;
		private System.Windows.Forms.Label label168;
		private System.Windows.Forms.Label label169;
		private System.Windows.Forms.Label label170;
		private System.Windows.Forms.Label label171;
		private System.Windows.Forms.Label label172;
		private System.Windows.Forms.Label label173;
		private System.Windows.Forms.Label label174;
		private System.Windows.Forms.Label label175;
		private System.Windows.Forms.Label label176;
		private System.Windows.Forms.Label label177;
		private System.Windows.Forms.Label label178;
		private System.Windows.Forms.Label label179;
		private System.Windows.Forms.Label label180;
		private System.Windows.Forms.Label label181;
		private System.Windows.Forms.Label label182;
		private System.Windows.Forms.Label label183;
		private System.Windows.Forms.Label label184;
		private System.Windows.Forms.Label label185;
		private System.Windows.Forms.Label label186;
		private System.Windows.Forms.Label label187;
		private System.Windows.Forms.Label label188;
		private System.Windows.Forms.Label label189;
		private System.Windows.Forms.Label label190;
		private System.Windows.Forms.Label label191;
		private System.Windows.Forms.Label label193;
		private System.Windows.Forms.Label label194;
		private System.Windows.Forms.Label label195;
		private System.Windows.Forms.Label label196;
		private System.Windows.Forms.Label label197;
		private System.Windows.Forms.Label label198;
		private System.Windows.Forms.Label label199;
		private System.Windows.Forms.Label label200;
		private System.Windows.Forms.Label label202;
		private System.Windows.Forms.Label label203;
		private System.Windows.Forms.Label label204;
		private System.Windows.Forms.Label label205;
		private System.Windows.Forms.Label label206;
		private System.Windows.Forms.Label label207;
		private System.Windows.Forms.Label label208;
		private System.Windows.Forms.Label label209;
		private System.Windows.Forms.Label label210;
		private System.Windows.Forms.Label label211;
		private System.Windows.Forms.Label label212;
		private System.Windows.Forms.Label label213;
		private System.Windows.Forms.Label label214;
		private System.Windows.Forms.Label label215;
		private System.Windows.Forms.Label label216;
		private System.Windows.Forms.Label label217;
		private System.Windows.Forms.Label label218;
		private System.Windows.Forms.Label label219;
		private System.Windows.Forms.Label label220;
		private System.Windows.Forms.Label label221;
		private System.Windows.Forms.Label label222;
		private System.Windows.Forms.Label label223;
		private System.Windows.Forms.Label label224;
		private System.Windows.Forms.Label label225;
		private System.Windows.Forms.Label label226;
		private System.Windows.Forms.Label label227;
		private System.Windows.Forms.Label label228;
		private System.Windows.Forms.Label label229;
		private System.Windows.Forms.Label label230;
		private System.Windows.Forms.Label label231;
		private System.Windows.Forms.Label label232;
		private System.Windows.Forms.Label label233;
		private System.Windows.Forms.Label label234;
		private System.Windows.Forms.TextBox tb_II_LI_Comments;
		private System.Windows.Forms.TextBox tb_II_LI_LA_Email;
		private System.Windows.Forms.TextBox tb_II_LI_LA_FaxNumber;
		private System.Windows.Forms.TextBox tb_II_LI_LA_PhoneNumber;
		private System.Windows.Forms.TextBox tb_II_LI_LA_AgentCode;
		private System.Windows.Forms.TextBox tb_II_LI_LA_FullName;
		private System.Windows.Forms.TextBox tb_II_LI_LA_LastName;
		private System.Windows.Forms.TextBox tb_II_LI_LA_FirstName;
		private System.Windows.Forms.TextBox tb_II_LI_ListingOffice;
		private System.Windows.Forms.TextBox tb_II_LI_ListingMLSNumber;
		private System.Windows.Forms.TextBox tb_II_LI_Status;
		private System.Windows.Forms.TextBox tb_II_LI_ListingPrice;
		private System.Windows.Forms.TextBox tb_II_FI_MortgageBeneLast;
		private System.Windows.Forms.TextBox tb_II_FI_MortgageBenefFirst;
		private System.Windows.Forms.TextBox tb_II_FI_OpeningBidAmount;
		private System.Windows.Forms.TextBox tb_II_FI_UnpaidBalance;
		private System.Windows.Forms.TextBox tb_II_FI_OrigMorgageBalance;
		private System.Windows.Forms.TextBox tb_II_FI_RecordingDate;
		private System.Windows.Forms.TextBox tb_II_FI_LoanRecNumber;
		private System.Windows.Forms.TextBox tb_II_FI_ZIP;
		private System.Windows.Forms.TextBox tb_II_FI_State;
		private System.Windows.Forms.TextBox tb_II_FI_City;
		private System.Windows.Forms.TextBox tb_II_FI_TrusteeFullStreetAddress;
		private System.Windows.Forms.TextBox tb_II_FI_TrusteeSaleNr;
		private System.Windows.Forms.TextBox tb_II_FI_TrusteePhoneNr;
		private System.Windows.Forms.TextBox tb_II_FI_TrusteeName;
		private System.Windows.Forms.TextBox tb_II_FI_SalePrice;
		private System.Windows.Forms.TextBox tb_II_FI_SaleTime;
		private System.Windows.Forms.TextBox tb_II_FI_Foreclosure_Sale;
		private System.Windows.Forms.TextBox tb_II_FI_FileNumber;
		private System.Windows.Forms.TextBox tb_II_FI_ForeclosureFileDate;
		private System.Windows.Forms.TextBox tb_II_FI_Bathrooms;
		private System.Windows.Forms.TextBox tb_II_FI_Bedrooms;
		private System.Windows.Forms.TextBox tb_II_FI_HomeSqFt;
		private System.Windows.Forms.TextBox tb_II_FI_YearBuilt;
		private System.Windows.Forms.TextBox tb_II_FI_DeedDate;
		private System.Windows.Forms.TextBox tb_II_FI_SaleDocNo;
		private System.Windows.Forms.TextBox tb_II_FI_LastSaleValue;
		private System.Windows.Forms.TextBox tb_II_FI_EstimatedEquity;
		private System.Windows.Forms.TextBox tb_II_FI_EstimatedHome;
		private System.Windows.Forms.TextBox tb_II_FI_OriginalTrustor;
		private System.Windows.Forms.TextBox tb_II_FI_ForeclosureStatus;
		private System.Windows.Forms.TextBox tb_II_FI_PropertyType;
		private System.Windows.Forms.TextBox tb_II_FI_UseCode;
		private System.Windows.Forms.TextBox tb_II_FI_TaxAssedValue;
		private System.Windows.Forms.TextBox tb_II_FI_APN;
		private System.Windows.Forms.TextBox tb_II_FI_Subdivision;
		private System.Windows.Forms.TextBox tb_II_FI_County;
		private System.Windows.Forms.TextBox tb_II_PC_Pool;
		private System.Windows.Forms.TextBox tb_II_PC_LotSqFt;
		private System.Windows.Forms.TextBox tb_II_PC_Garage;
		private System.Windows.Forms.TextBox tb_II_PC_Bathrooms;
		private System.Windows.Forms.TextBox tb_II_PC_Bedrooms;
		private System.Windows.Forms.TextBox tb_II_PC_YearBuilt;
		private System.Windows.Forms.TextBox tb_II_PC_LegalDesc;
		private System.Windows.Forms.TextBox tb_II_PC_PropertyType;
		private System.Windows.Forms.TextBox tb_II_PC_UseCode;
		private System.Windows.Forms.TextBox tb_II_PC_Subdivision;
		private System.Windows.Forms.TextBox tb_II_PC_County;
		private System.Windows.Forms.TextBox tb_II_F_EstimatedEquity;
		private System.Windows.Forms.TextBox tb_II_F_EstimatedHome;
		private System.Windows.Forms.TextBox tb_II_F_CashDownPayment;
		private System.Windows.Forms.TextBox tb_II_F_TotalLoans;
		private System.Windows.Forms.TextBox tb_II_F_Lender;
		private System.Windows.Forms.TextBox tb_II_F_LoanInterestRate;
		private System.Windows.Forms.TextBox tb_II_F_LoanType;
		private System.Windows.Forms.TextBox tb_II_F_LoanAmount;
		private System.Windows.Forms.TextBox tb_II_F_CostPerSqFt;
		private System.Windows.Forms.TextBox tb_II_F_DeedDate;
		private System.Windows.Forms.TextBox tb_II_F_SaleDocumentNumber;
		private System.Windows.Forms.TextBox tb_II_F_SaleDate;
		private System.Windows.Forms.TextBox tb_II_F_SaleValue;
		private System.Windows.Forms.TextBox tb_II_F_OwnerAbsentOccupied;
		private System.Windows.Forms.TextBox tb_II_F_PercentImprovement;
		private System.Windows.Forms.TextBox tb_II_F_LandValue;
		private System.Windows.Forms.TextBox tb_II_F_TaxAmount;
		private System.Windows.Forms.TextBox tb_II_F_FullCashValue;
		private System.Windows.Forms.TextBox tb_II_F_TaxAssessedValue;
		private System.Windows.Forms.TextBox tb_II_F_TaxDeliquentYear;
		private System.Windows.Forms.TextBox tb_II_F_TaxStatus;
		private System.Windows.Forms.TextBox tb_II_F_TaxExemption;
		private System.Windows.Forms.TextBox tb_II_F_TaxRateArea;
		private System.Windows.Forms.TextBox tb_II_F_APN;
		private System.Windows.Forms.TextBox tb_II_F_MailZIP;
		private System.Windows.Forms.TextBox tb_II_F_MailCityState;
		private System.Windows.Forms.TextBox tb_II_TI_MailFullStreetAddress;
		private System.Windows.Forms.TextBox tb_II_OU_UF5;
		private System.Windows.Forms.TextBox tb_II_OU_UF4;
		private System.Windows.Forms.TextBox tb_II_OU_UF3;
		private System.Windows.Forms.TextBox tb_II_OU_UF2;
		private System.Windows.Forms.TextBox tb_II_OU_UF1;
		private System.Windows.Forms.TextBox tb_II_OU_DoNotCallFlag;
		private System.Windows.Forms.TextBox tb_II_OU_PrivacyFlag;
		private System.Windows.Forms.TextBox tb_II_OU_MailScore;
		private System.Windows.Forms.TextBox tb_II_OU_MailCRRT;
		private System.Windows.Forms.TextBox tb_II_OU_DivorceDate;
		private System.Windows.Forms.TextBox tb_II_OU_DivorceStatus;
		private System.Windows.Forms.TextBox tb_II_OU_MapArea;
		private System.Windows.Forms.TextBox tb_II_OU_MapGrid;
		private System.Windows.Forms.TextBox tb_II_OU_Vesting;
		private System.Windows.Forms.TextBox tb_II_OU_LegalDescription;
		private System.Windows.Forms.TextBox tb_II_OU_Subdividion;
		private System.Windows.Forms.TextBox tb_II_OI_County;
		private System.Windows.Forms.TextBox tb_II_PC_HomeSqFt;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox tbDesiredClosingDate;
		private System.Windows.Forms.Label label235;
		private System.Windows.Forms.PictureBox pictureBox2;
        private DealMaker.ReadMe readMe2;
		private DealMaker.ReadMe readMe4;
		private DealMaker.ReadMe readMe5;
		private DealMaker.ReadMe readMe6;
		private DealMaker.ReadMe readMe7;
		private DealMaker.ReadMe readMe8;
		private DealMaker.ReadMe readMe9;
		private System.Windows.Forms.TextBox tbSubDivision;
        private System.Windows.Forms.TextBox tbCounty;
		private System.Windows.Forms.Button bPending;
		private System.Windows.Forms.TextBox tbAPN;
        private System.Windows.Forms.Label label124;
		private System.Windows.Forms.TextBox tbSiteState;
		private System.Windows.Forms.TextBox tbSiteCity;
		private System.Windows.Forms.TextBox tbSiteCityState;
        private System.Windows.Forms.Label label133;
		private System.Windows.Forms.TextBox tb_II_F_LoanAmount2;
		private System.Windows.Forms.Button bSaveImportedInfo;
		private System.Windows.Forms.Button bPrintDealInfo;
		private System.Windows.Forms.TextBox tbForSaleDate;
		private System.Windows.Forms.TextBox tbListingExpDate;
		#endregion � Data �        
		
		#region � CTR && Dispose �
		
		//sasa, 13.11.05: bugfix? for some strange reason, AddEventHandlers seems to be called twice
		//so we get 2 udp packets
		bool eventHandlersAddes = false;

		private void AddEventHandlers()
		{
			if(!eventHandlersAddes)
			{
				eventHandlersAddes = true;

                _pbHouse.Click += new EventHandler(On_PbHouse_Click);
                _pbHouse.ContextMenuStrip = this.ctxPbHouseOptions;

                getStreetView.Click += new EventHandler(On_getStreetView_Click);
                uploadPhoto.Click += new EventHandler(On_UploadPhoto_Click);

                //UnLockPropertyItem
                _unLockPI.Click += new EventHandler(On_UnLockPI_Click);

				//AutoSave
				AddAutoSaveEventHandlers();
				
				//Copy Import Data to Deal Info data
				_copyDataToDealInfo.Click += new EventHandler(On_CopyDataToDealInfo_Click);
	
				OneTwoThreeSold.NEW_CONTACT_IN_GROUP_BY_123_SOLD += new DealMaker.OneTwoThreeSold.NewContactInGroupFrom123Sold(NewContactInGroup_EH);
							
				this.tb3MoPayment.TextChanged += new EventHandler(UpdateTotalOwed_and_Monthly_EH);
				this.tb2MoPayment.TextChanged += new EventHandler(UpdateTotalOwed_and_Monthly_EH);
				this.tb1MoPayment.TextChanged += new EventHandler(UpdateTotalOwed_and_Monthly_EH);
				this.tb3Bal.TextChanged += new EventHandler(UpdateTotalOwed_and_Monthly_EH);
				this.tb2Bal.TextChanged += new EventHandler(UpdateTotalOwed_and_Monthly_EH);
				this.tb1Bal.TextChanged += new EventHandler(UpdateTotalOwed_and_Monthly_EH);
				this.tbOtherLiens.TextChanged += new EventHandler(UpdateTotalOwed_and_Monthly_EH);
				this.tbTaxAmount.TextChanged += new EventHandler(UpdateTotalOwed_and_Monthly_EH);
				this.tbHOA.TextChanged += new EventHandler(UpdateTotalOwed_and_Monthly_EH);
	
				this.tbFirstname.Leave += new EventHandler(UpdateFullName_EH);
				this.tbMiddleName.Leave += new EventHandler(UpdateFullName_EH);
				this.tbLastName.Leave += new EventHandler(UpdateFullName_EH);
	
				this.tbMailLastKnownCity.TextChanged += new EventHandler(updateLastKnownSS_EH);
				this.tbMailLastKnownState.TextChanged += new EventHandler(updateLastKnownSS_EH);
	
				this.tbMailPreviousCity.TextChanged += new EventHandler(updatePreviousSS_EH);
				this.tbMailPreviousState.TextChanged += new EventHandler(updatePreviousSS_EH);
	
				this.tbMailOldCity.TextChanged += new EventHandler(updateOldSS_EH);
				this.tbMailOldState.TextChanged += new EventHandler(updateOldSS_EH);
	
				this.tbSiteCity.TextChanged += new EventHandler(updateSiteSS_EH);
				this.tbSiteState.TextChanged += new EventHandler(updateSiteSS_EH);
	
				this.bPrintDealInfo.Click += new EventHandler(bPrintDealInfo_Click);
				
				//Floaters
				this.readMe2.MouseEnter += new EventHandler(GroupMembership_MouseEnter);
				this.readMe2.MouseLeave += new EventHandler(GroupMembership_MouseLeave);
				
				this.readMe3.MouseEnter += new EventHandler(Contact_MouseEnter);
				this.readMe3.MouseLeave += new EventHandler(Contact_MouseLeave);
	
				this.readMe1.MouseEnter += new EventHandler(DealInfo_MouseEnter);
				this.readMe1.MouseLeave += new EventHandler(DealInfo_MouseLeave);
	
				this.readMe6.MouseEnter += new EventHandler(ImportedData_MouseEnter);
				this.readMe6.MouseLeave += new EventHandler(ImportedData_MouseLeave);
				
				this.readMe5.MouseEnter += new EventHandler(ImportedData_MouseEnter);
				this.readMe5.MouseLeave += new EventHandler(ImportedData_MouseLeave);
	
				this.readMe4.MouseEnter += new EventHandler(ImportedData_MouseEnter);
				this.readMe4.MouseLeave += new EventHandler(ImportedData_MouseLeave);
	
				this.readMe7.MouseEnter += new EventHandler(ImportedData_MouseEnter);
				this.readMe7.MouseLeave += new EventHandler(ImportedData_MouseLeave);
	
				this.readMe8.MouseEnter += new EventHandler(ImportedData_MouseEnter);
				this.readMe8.MouseLeave += new EventHandler(ImportedData_MouseLeave);
	
				this.readMe9.MouseEnter += new EventHandler(ImportedData_MouseEnter);
				this.readMe9.MouseLeave += new EventHandler(ImportedData_MouseLeave);
	
				this.MainForm_REFRESH_LOCKED_FEATURES();
	
				//MainForm.REFRESH_LOCKED_FEATURES += new DealMaker.MainForm.RefreshLockedFeatures(MainForm_REFRESH_LOCKED_FEATURES);
			
				//sasa, 13.11.05: nw broadcast udp packet changed node
				Network.NetworkUDP.ChangedPropertyItem += networkEventHandler = new DealMaker.Network.NetworkChangedEventHandler(NetworkUDP_ChangedPI);
				//sasa, 24.01.06: new nw lock
				Network.NetworkLock.StatusChanged += networkEventHandler2 = new DealMaker.Network.NetworkLock.StatusChangedEventHandler(NetworkLock_StatusChanged);                

                string customSettingsError = string.Empty;
                _customSettings = new CustomSettingsManager().LoadCustomSettings(out customSettingsError);

                this._copySTRank0InfoToContactInfoTab.Click += new System.EventHandler(this._copySTRank0InfoToContactInfoTab_Click);
            }
		}

        

                

        void On_UnLockPI_Click(object sender, EventArgs e)
        {
            DealMaker.Network.NetworkLock.LockStatus currLockStatus = Network.NetworkLock.GetStatus();
            Network.NetworkLock.RequestLock(pi.IdPropertyItem);
            Network.NetworkUDP.Broadcast(pi);
            currLockStatus = Network.NetworkLock.GetStatus();
            Network.NetworkLock.RequestUnlock();            
        }

        public ContactScreen()
        {
            new ContactScreen(-1, string.Empty);
        }


        public ContactScreen(int preSelectedNodeId, string preSelectedNodeName)
		{
            try
            {
                // This call is required by the Windows.Forms Form Designer.
                InitializeComponent();

                //sasa, 26.01.06: moved to load
                //			this.AddEventHandlers();

                if (taskAndHistoryList == null) putTaskList();

                this.currentPropertyItemGroup = new Node();

                if (-1 != preSelectedNodeId && string.Empty != preSelectedNodeName)
                {
                    this.currentPropertyItemGroup.nodeId = preSelectedNodeId;
                    this.currentPropertyItemGroup.nodeName = preSelectedNodeName;
                }
                else
                {                    
                    if (-1 == Globals.selectedNodeId)
                    {
                        this.currentPropertyItemGroup.nodeId = 1;
                    }
                    else
                    {
                        this.currentPropertyItemGroup.nodeId = Globals.selectedNodeId;
                        this.currentPropertyItemGroup.nodeName = Globals.selectedNodeName;
                    }
                }


                //CommercialInfo
                if (null != Globals.activeModules && Globals.activeModules.MOD_9)
                {
                    DataManager.Instance.SetConnection(Connection.getInstance().HatchConn);
                    _commercialControl.AddEventHandlers();
                    _commercialControl.SetAutoSaveInterface(this as IAutoSave);
                }

                //BuyersInfo
                BuyerInfoManager.Instance.SetConnection(Connection.getInstance().HatchConn);

                //ShortSale
                if (null != Globals.activeModules && Globals.activeModules.MOD_8)
                    DealMaker.PlainClasses.ShortSale.Adapters.DataManager.Instance.SQLConnection = Connection.getInstance().HatchConn;

                //PictureManager
                PictureManager.Instance.SetConnection(Connection.getInstance().HatchConn);

                if (-1 != preSelectedNodeId)
                    AssignToGroup(preSelectedNodeId, false);
            }
            catch (Exception exc)
            {
                MessageBox.Show("1:" + exc.Message + System.Environment.NewLine + exc.StackTrace + System.Environment.NewLine + exc.InnerException);
            }
            
		}

		public ContactScreen(Hashtable htNodePosPropertyItemId)
		{
			try
            {
                // This call is required by the Windows.Forms Form Designer.
			    InitializeComponent();	

			    //CommercialInfo
			    DataManager.Instance.SetConnection(Connection.getInstance().HatchConn);

                //BuyersInfo
                BuyerInfoManager.Instance.SetConnection(Connection.getInstance().HatchConn);

                //ShortSale
                if (null != Globals.activeModules && Globals.activeModules.MOD_8)
                    DealMaker.PlainClasses.ShortSale.Adapters.DataManager.Instance.SQLConnection = Connection.getInstance().HatchConn;            

                //PictureManager
                PictureManager.Instance.SetConnection(Connection.getInstance().HatchConn);

			    //sasa		
    //			this.AddEventHandlers();
			    this.ContactScreenStarter(htNodePosPropertyItemId);
			    /*this.htNodePosPropertyItemId = htNodePosPropertyItemId;			
			    Guid firstPropertyItem = (Guid)htNodePosPropertyItemId[1];
			    this.LoadDisplayData(firstPropertyItem);
			    this.tbGroup.Text = Globals.selectedNodeName;
			    this.DisplayGroupMembershipData(firstPropertyItem);

			    this.currentPropertyItemGroup = new Node();
			    this.currentPropertyItemGroup.nodeId = Globals.selectedNodeId;
			    this.currentPropertyItemGroup.nodeName = Globals.selectedNodeName;*/
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message + System.Environment.NewLine + exc.StackTrace + System.Environment.NewLine + exc.InnerException);
            }
		}

		public ContactScreen(Hashtable htNodePosPropertyItemId, int currentOffset)
		{
            try
            {
			    // This call is required by the Windows.Forms Form Designer.
			    InitializeComponent();

			    //CommercialInfo
			    DataManager.Instance.SetConnection(Connection.getInstance().HatchConn);

                //BuyersInfo
                BuyerInfoManager.Instance.SetConnection(Connection.getInstance().HatchConn);

                //ShortSale
                if (null != Globals.activeModules && Globals.activeModules.MOD_8)
                    DealMaker.PlainClasses.ShortSale.Adapters.DataManager.Instance.SQLConnection = Connection.getInstance().HatchConn;            

                //PictureManager
                PictureManager.Instance.SetConnection(Connection.getInstance().HatchConn);

			    //sasa, 29.01.06: bugfix, nw lock, now addEventHandlers is called from ContactScreen_Load
    //			this.AddEventHandlers();
			    this.CurrentOffset = currentOffset;
			    this.ContactScreenStarter(htNodePosPropertyItemId);
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message + System.Environment.NewLine + exc.StackTrace + System.Environment.NewLine + exc.InnerException);
            }
		}

		private void putTaskList()
		{
			taskAndHistoryList = new DealMaker.Forms.SchedulingActicities.TaskAndHistoryList();

			taskAndHistoryList.Location = new Point(0,0);
			taskAndHistoryList.Size = tpActivitiesHistory.Size;

			taskAndHistoryList.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top;

			taskAndHistoryList.Show();

			tpActivitiesHistory.Controls.Add(taskAndHistoryList);
		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}
		#endregion � CTR && Dispose �

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ContactScreen));
            this.lowerPanel = new System.Windows.Forms.Panel();
            this._contactTab = new System.Windows.Forms.TabControl();
            this.tpContactInfo = new System.Windows.Forms.TabPage();
            this._ci_NotesField = new System.Windows.Forms.TextBox();
            this._ci_NotesLabel = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this._ci_email_panel = new System.Windows.Forms.Panel();
            this.label19 = new System.Windows.Forms.Label();
            this.tbEmail = new System.Windows.Forms.TextBox();
            this._ci_fax_panel = new System.Windows.Forms.Panel();
            this.label18 = new System.Windows.Forms.Label();
            this.tbFax = new System.Windows.Forms.TextBox();
            this._ci_cellphone_panel = new System.Windows.Forms.Panel();
            this.label17 = new System.Windows.Forms.Label();
            this.tbCellPh = new System.Windows.Forms.TextBox();
            this.tbCellBestTime = new System.Windows.Forms.TextBox();
            this._ci_workphone_panel = new System.Windows.Forms.Panel();
            this.label16 = new System.Windows.Forms.Label();
            this.tbWorkPh = new System.Windows.Forms.TextBox();
            this.tbWorkBestTime = new System.Windows.Forms.TextBox();
            this._ci_homephone_panel = new System.Windows.Forms.Panel();
            this.label15 = new System.Windows.Forms.Label();
            this.tbHomePh = new System.Windows.Forms.TextBox();
            this.tbHomeBestTime = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label10 = new System.Windows.Forms.Label();
            this._ci_old_panel = new System.Windows.Forms.Panel();
            this.label14 = new System.Windows.Forms.Label();
            this.tbMailOldAdd = new System.Windows.Forms.TextBox();
            this.tbMailOldZIP = new System.Windows.Forms.TextBox();
            this.tbMailOldCity = new System.Windows.Forms.TextBox();
            this.tbMailOldState = new System.Windows.Forms.TextBox();
            this.tbMailOldCityState = new System.Windows.Forms.TextBox();
            this.label60 = new System.Windows.Forms.Label();
            this._ci_previous_panel = new System.Windows.Forms.Panel();
            this.label13 = new System.Windows.Forms.Label();
            this.tbMailPreviousAdd = new System.Windows.Forms.TextBox();
            this.tbMailPreviousZIP = new System.Windows.Forms.TextBox();
            this.tbMailPreviousCity = new System.Windows.Forms.TextBox();
            this.tbMailPreviousState = new System.Windows.Forms.TextBox();
            this.tbMailPreviousCityState = new System.Windows.Forms.TextBox();
            this.label163 = new System.Windows.Forms.Label();
            this._ci_lk_panel = new System.Windows.Forms.Panel();
            this.tbMailLastKnownAdd = new System.Windows.Forms.TextBox();
            this.tbMailLastKnownCityState = new System.Windows.Forms.TextBox();
            this.tbMailLastKnownZIP = new System.Windows.Forms.TextBox();
            this.tbMailLastKnownCity = new System.Windows.Forms.TextBox();
            this.tbMailLastKnownState = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label62 = new System.Windows.Forms.Label();
            this.label63 = new System.Windows.Forms.Label();
            this.label125 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this._ci_st_landline_panel = new System.Windows.Forms.Panel();
            this._stLandline = new System.Windows.Forms.TextBox();
            this.label236 = new System.Windows.Forms.Label();
            this._ci_st_cell_panel = new System.Windows.Forms.Panel();
            this.label201 = new System.Windows.Forms.Label();
            this._stCellPhone = new System.Windows.Forms.TextBox();
            this._ci_st_email_panel = new System.Windows.Forms.Panel();
            this.label192 = new System.Windows.Forms.Label();
            this._stEmail = new System.Windows.Forms.TextBox();
            this._stResultsLabel = new System.Windows.Forms.Label();
            this.label297 = new System.Windows.Forms.Label();
            this._stLastUpdated = new System.Windows.Forms.Label();
            this.readMe3 = new DealMaker.ReadMe();
            this.tpDealInfo = new System.Windows.Forms.TabPage();
            this.tbReinsAmnt = new DealMaker.NumericTextBox();
            this.tb3MoPayment = new DealMaker.NumericTextBox();
            this.tb2MoPayment = new DealMaker.NumericTextBox();
            this.tbOtherLiens = new DealMaker.NumericTextBox();
            this.tbHOA = new DealMaker.NumericTextBox();
            this.tb3Bal = new DealMaker.NumericTextBox();
            this.tb2Bal = new DealMaker.NumericTextBox();
            this.tb1Bal = new DealMaker.NumericTextBox();
            this.tbWorthBySeller = new DealMaker.NumericTextBox();
            this.tbTaxAmount = new DealMaker.NumericTextBox();
            this.tb1MoPayment = new DealMaker.NumericTextBox();
            this.cbTaxIncl = new System.Windows.Forms.ComboBox();
            this.tbTotalOwed = new System.Windows.Forms.TextBox();
            this.tbTotalMonthly = new System.Windows.Forms.TextBox();
            this.label83 = new System.Windows.Forms.Label();
            this.tbARV = new System.Windows.Forms.TextBox();
            this.cbLeadSource = new System.Windows.Forms.ComboBox();
            this.tbListingExpDate = new System.Windows.Forms.TextBox();
            this.tbForSaleDate = new System.Windows.Forms.TextBox();
            this.cbType = new System.Windows.Forms.ComboBox();
            this.tbSqFt = new System.Windows.Forms.TextBox();
            this.tbAPN = new System.Windows.Forms.TextBox();
            this.cbOccupiedBy = new System.Windows.Forms.ComboBox();
            this.cbPool = new System.Windows.Forms.ComboBox();
            this.cbGarage = new System.Windows.Forms.ComboBox();
            this.cbYearBuilt = new System.Windows.Forms.ComboBox();
            this.cbBaths = new System.Windows.Forms.ComboBox();
            this.cbBedrooms = new System.Windows.Forms.ComboBox();
            this.bPrintDealInfo = new System.Windows.Forms.Button();
            this.tbNextStep = new System.Windows.Forms.TextBox();
            this.cbMotivation = new System.Windows.Forms.ComboBox();
            this.label235 = new System.Windows.Forms.Label();
            this.tbDesiredClosingDate = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label98 = new System.Windows.Forms.Label();
            this.label91 = new System.Windows.Forms.Label();
            this.label90 = new System.Windows.Forms.Label();
            this.label89 = new System.Windows.Forms.Label();
            this.label88 = new System.Windows.Forms.Label();
            this.label87 = new System.Windows.Forms.Label();
            this.label86 = new System.Windows.Forms.Label();
            this.label85 = new System.Windows.Forms.Label();
            this.label84 = new System.Windows.Forms.Label();
            this.label82 = new System.Windows.Forms.Label();
            this.label81 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label80 = new System.Windows.Forms.Label();
            this.tbRepairs = new System.Windows.Forms.TextBox();
            this.label79 = new System.Windows.Forms.Label();
            this.label78 = new System.Windows.Forms.Label();
            this.label67 = new System.Windows.Forms.Label();
            this.label66 = new System.Windows.Forms.Label();
            this.tbMinCashNeeded = new System.Windows.Forms.TextBox();
            this.label77 = new System.Windows.Forms.Label();
            this.label76 = new System.Windows.Forms.Label();
            this.cbSell4What = new System.Windows.Forms.ComboBox();
            this.label75 = new System.Windows.Forms.Label();
            this.label74 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.label73 = new System.Windows.Forms.Label();
            this.label72 = new System.Windows.Forms.Label();
            this.cbMoBehind = new System.Windows.Forms.ComboBox();
            this.label71 = new System.Windows.Forms.Label();
            this.label70 = new System.Windows.Forms.Label();
            this.cb3Type = new System.Windows.Forms.ComboBox();
            this.label69 = new System.Windows.Forms.Label();
            this.cb2Type = new System.Windows.Forms.ComboBox();
            this.label68 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label65 = new System.Windows.Forms.Label();
            this.label64 = new System.Windows.Forms.Label();
            this.cbCanCancel = new System.Windows.Forms.ComboBox();
            this.label57 = new System.Windows.Forms.Label();
            this.dtpListingExpDate = new System.Windows.Forms.DateTimePicker();
            this.label56 = new System.Windows.Forms.Label();
            this.cbListed = new System.Windows.Forms.ComboBox();
            this.cbPmntsCurrnet = new System.Windows.Forms.ComboBox();
            this.cbInForecl = new System.Windows.Forms.ComboBox();
            this.dtpForSaleDate = new System.Windows.Forms.DateTimePicker();
            this.label53 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.tb3IntRate = new System.Windows.Forms.TextBox();
            this.label46 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.tb2IntRate = new System.Windows.Forms.TextBox();
            this.label42 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.cb1Type = new System.Windows.Forms.ComboBox();
            this.label38 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.tb1IntRate = new System.Windows.Forms.TextBox();
            this.label35 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.cbSource = new System.Windows.Forms.ComboBox();
            this.label31 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.tbWhySelling = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.tbCondition = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.tbLowestPriceAcceptable = new DealMaker.NumericTextBox();
            this.readMe1 = new DealMaker.ReadMe();
            this.tpSkipTracedInfo = new System.Windows.Forms.TabPage();
            this.skipTracedInfoTabControl = new System.Windows.Forms.TabControl();
            this.skipTracedContactInfo = new System.Windows.Forms.TabPage();
            this._copySTRank0InfoToContactInfoTab = new System.Windows.Forms.Button();
            this._skipTraceRecord = new System.Windows.Forms.Button();
            this.label260 = new System.Windows.Forms.Label();
            this.label259 = new System.Windows.Forms.Label();
            this.st_ci_fullName = new System.Windows.Forms.TextBox();
            this.st_ci_lastName = new System.Windows.Forms.TextBox();
            this.label256 = new System.Windows.Forms.Label();
            this.label257 = new System.Windows.Forms.Label();
            this.label258 = new System.Windows.Forms.Label();
            this.st_ci_aliasName = new System.Windows.Forms.TextBox();
            this.st_ci_age = new System.Windows.Forms.TextBox();
            this.st_ci_firstName = new System.Windows.Forms.TextBox();
            this.st_ci_middleName = new System.Windows.Forms.TextBox();
            this.st_ci_email2 = new System.Windows.Forms.TextBox();
            this.label255 = new System.Windows.Forms.Label();
            this.st_ci_email1 = new System.Windows.Forms.TextBox();
            this.label248 = new System.Windows.Forms.Label();
            this.st_ci_rp4 = new System.Windows.Forms.TextBox();
            this.label254 = new System.Windows.Forms.Label();
            this.st_ci_rp3 = new System.Windows.Forms.TextBox();
            this.label253 = new System.Windows.Forms.Label();
            this.st_ci_mp2 = new System.Windows.Forms.TextBox();
            this.label252 = new System.Windows.Forms.Label();
            this.st_ci_mp1 = new System.Windows.Forms.TextBox();
            this.label251 = new System.Windows.Forms.Label();
            this.st_ci_mp0 = new System.Windows.Forms.TextBox();
            this.label249 = new System.Windows.Forms.Label();
            this.st_ci_email0 = new System.Windows.Forms.TextBox();
            this.label250 = new System.Windows.Forms.Label();
            this.st_ci_a2_lastSeen = new System.Windows.Forms.TextBox();
            this.st_ci_a1_lastSeen = new System.Windows.Forms.TextBox();
            this.st_ci_a0_lastSeen = new System.Windows.Forms.TextBox();
            this.label247 = new System.Windows.Forms.Label();
            this.st_ci_a2_firstSeen = new System.Windows.Forms.TextBox();
            this.st_ci_a1_firstSeen = new System.Windows.Forms.TextBox();
            this.st_ci_a0_firstSeen = new System.Windows.Forms.TextBox();
            this.label246 = new System.Windows.Forms.Label();
            this.st_ci_a2_state = new System.Windows.Forms.TextBox();
            this.st_ci_a1_state = new System.Windows.Forms.TextBox();
            this.st_ci_a0_state = new System.Windows.Forms.TextBox();
            this.label239 = new System.Windows.Forms.Label();
            this.label240 = new System.Windows.Forms.Label();
            this.label241 = new System.Windows.Forms.Label();
            this.label242 = new System.Windows.Forms.Label();
            this.st_ci_a2_city = new System.Windows.Forms.TextBox();
            this.st_ci_a1_city = new System.Windows.Forms.TextBox();
            this.st_ci_a0_city = new System.Windows.Forms.TextBox();
            this.st_ci_a2_zip = new System.Windows.Forms.TextBox();
            this.st_ci_a2_address = new System.Windows.Forms.TextBox();
            this.st_ci_a1_zip = new System.Windows.Forms.TextBox();
            this.st_ci_a1_address = new System.Windows.Forms.TextBox();
            this.st_ci_a0_zip = new System.Windows.Forms.TextBox();
            this.st_ci_a0_address = new System.Windows.Forms.TextBox();
            this.label243 = new System.Windows.Forms.Label();
            this.label244 = new System.Windows.Forms.Label();
            this.label245 = new System.Windows.Forms.Label();
            this.label238 = new System.Windows.Forms.Label();
            this.label237 = new System.Windows.Forms.Label();
            this.label92 = new System.Windows.Forms.Label();
            this.skipTracedOtherProperty = new System.Windows.Forms.TabPage();
            this.st_op_bedrooms4 = new WindowsApplication.AcquisitionCalculator.TextBox();
            this.st_op_bathrooms4 = new WindowsApplication.AcquisitionCalculator.TextBox();
            this.st_op_yearBuilt4 = new WindowsApplication.AcquisitionCalculator.TextBox();
            this.st_op_buildingSqFt4 = new WindowsApplication.AcquisitionCalculator.TextBox();
            this.st_op_lotSqFt4 = new WindowsApplication.AcquisitionCalculator.TextBox();
            this.st_op_parcelNumber4 = new WindowsApplication.AcquisitionCalculator.TextBox();
            this.st_op_useCode4 = new WindowsApplication.AcquisitionCalculator.TextBox();
            this.st_op_county4 = new WindowsApplication.AcquisitionCalculator.TextBox();
            this.st_op_address4 = new WindowsApplication.AcquisitionCalculator.TextBox();
            this.label288 = new System.Windows.Forms.Label();
            this.label289 = new System.Windows.Forms.Label();
            this.label290 = new System.Windows.Forms.Label();
            this.label291 = new System.Windows.Forms.Label();
            this.label292 = new System.Windows.Forms.Label();
            this.label293 = new System.Windows.Forms.Label();
            this.label294 = new System.Windows.Forms.Label();
            this.label295 = new System.Windows.Forms.Label();
            this.st_op_lv4 = new System.Windows.Forms.ListView();
            this.columnHeader22 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader23 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader24 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader25 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader26 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader27 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader28 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.label296 = new System.Windows.Forms.Label();
            this.st_op_bedrooms3 = new WindowsApplication.AcquisitionCalculator.TextBox();
            this.st_op_bathrooms3 = new WindowsApplication.AcquisitionCalculator.TextBox();
            this.st_op_yearBuilt3 = new WindowsApplication.AcquisitionCalculator.TextBox();
            this.st_op_buildingSqFt3 = new WindowsApplication.AcquisitionCalculator.TextBox();
            this.st_op_lotSqFt3 = new WindowsApplication.AcquisitionCalculator.TextBox();
            this.st_op_parcelNumber3 = new WindowsApplication.AcquisitionCalculator.TextBox();
            this.st_op_useCode3 = new WindowsApplication.AcquisitionCalculator.TextBox();
            this.st_op_county3 = new WindowsApplication.AcquisitionCalculator.TextBox();
            this.st_op_address3 = new WindowsApplication.AcquisitionCalculator.TextBox();
            this.label279 = new System.Windows.Forms.Label();
            this.label280 = new System.Windows.Forms.Label();
            this.label281 = new System.Windows.Forms.Label();
            this.label282 = new System.Windows.Forms.Label();
            this.label283 = new System.Windows.Forms.Label();
            this.label284 = new System.Windows.Forms.Label();
            this.label285 = new System.Windows.Forms.Label();
            this.label286 = new System.Windows.Forms.Label();
            this.st_op_lv3 = new System.Windows.Forms.ListView();
            this.columnHeader9 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader10 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader11 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader12 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader13 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader15 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader16 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.label287 = new System.Windows.Forms.Label();
            this.st_op_bedrooms2 = new WindowsApplication.AcquisitionCalculator.TextBox();
            this.st_op_bathrooms2 = new WindowsApplication.AcquisitionCalculator.TextBox();
            this.st_op_yearBuilt2 = new WindowsApplication.AcquisitionCalculator.TextBox();
            this.st_op_buildingSqFt2 = new WindowsApplication.AcquisitionCalculator.TextBox();
            this.st_op_lotSqFt2 = new WindowsApplication.AcquisitionCalculator.TextBox();
            this.st_op_parcelNumber2 = new WindowsApplication.AcquisitionCalculator.TextBox();
            this.st_op_useCode2 = new WindowsApplication.AcquisitionCalculator.TextBox();
            this.st_op_county2 = new WindowsApplication.AcquisitionCalculator.TextBox();
            this.st_op_address2 = new WindowsApplication.AcquisitionCalculator.TextBox();
            this.label270 = new System.Windows.Forms.Label();
            this.label271 = new System.Windows.Forms.Label();
            this.label272 = new System.Windows.Forms.Label();
            this.label273 = new System.Windows.Forms.Label();
            this.label274 = new System.Windows.Forms.Label();
            this.label275 = new System.Windows.Forms.Label();
            this.label276 = new System.Windows.Forms.Label();
            this.label277 = new System.Windows.Forms.Label();
            this.st_op_lv2 = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader8 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.label278 = new System.Windows.Forms.Label();
            this.st_op_bedrooms1 = new WindowsApplication.AcquisitionCalculator.TextBox();
            this.st_op_bathrooms1 = new WindowsApplication.AcquisitionCalculator.TextBox();
            this.st_op_yearBuilt1 = new WindowsApplication.AcquisitionCalculator.TextBox();
            this.st_op_buildingSqFt1 = new WindowsApplication.AcquisitionCalculator.TextBox();
            this.st_op_lotSqFt1 = new WindowsApplication.AcquisitionCalculator.TextBox();
            this.st_op_parcelNumber1 = new WindowsApplication.AcquisitionCalculator.TextBox();
            this.st_op_useCode1 = new WindowsApplication.AcquisitionCalculator.TextBox();
            this.st_op_county1 = new WindowsApplication.AcquisitionCalculator.TextBox();
            this.st_op_address1 = new WindowsApplication.AcquisitionCalculator.TextBox();
            this.label269 = new System.Windows.Forms.Label();
            this.label268 = new System.Windows.Forms.Label();
            this.label267 = new System.Windows.Forms.Label();
            this.label266 = new System.Windows.Forms.Label();
            this.label265 = new System.Windows.Forms.Label();
            this.label264 = new System.Windows.Forms.Label();
            this.label263 = new System.Windows.Forms.Label();
            this.label262 = new System.Windows.Forms.Label();
            this.st_op_lv1 = new System.Windows.Forms.ListView();
            this.transferDate = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.seller = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.buyer = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.transferType = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.loanValue = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lenderName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.interesetType = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.label261 = new System.Windows.Forms.Label();
            this.skipTracedRelativesInfo = new System.Windows.Forms.TabPage();
            this.st_ri_relatives = new System.Windows.Forms.ListView();
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader14 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader17 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader18 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader19 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader20 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader21 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.tpGroupMembership = new System.Windows.Forms.TabPage();
            this.bPending = new System.Windows.Forms.Button();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.bAssignToGroup = new System.Windows.Forms.Button();
            this.bRemoveFromGroup = new System.Windows.Forms.Button();
            this.readMe2 = new DealMaker.ReadMe();
            this.lvOtherGroups = new System.Windows.Forms.ListView();
            this.nodeId = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.groupName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.groupDesc = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.label61 = new System.Windows.Forms.Label();
            this.tbGroupStatusGroup = new System.Windows.Forms.TextBox();
            this.bDoNotMailGroup = new System.Windows.Forms.Button();
            this.bPurchased = new System.Windows.Forms.Button();
            this.bDeadLeads = new System.Windows.Forms.Button();
            this.bDealsInProgress = new System.Windows.Forms.Button();
            this.bLeadsNotProcessedYet = new System.Windows.Forms.Button();
            this.label59 = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this.label55 = new System.Windows.Forms.Label();
            this.tpActivitiesHistory = new System.Windows.Forms.TabPage();
            this._evaluate = new System.Windows.Forms.TabPage();
            this._evaluateSubTabControl = new System.Windows.Forms.TabControl();
            this._propertyProfileSubTabPage = new System.Windows.Forms.TabPage();
            this.ucppc = new DealMaker.OnlineData.UserControlPropertyProfileComps();
            this._compsSubTabPage = new System.Windows.Forms.TabPage();
            this.ucppclv = new DealMaker.OnlineData.UserControlPropertyProfileCompsListView();
            this._estimateRepairsSubTabPage = new System.Windows.Forms.TabPage();
            this._repairs = new DealMaker.UserControls.Repairs.EstimateRepairsControl();
            this._shortSaleTab = new System.Windows.Forms.TabPage();
            this._shortSaleControl = new DealMaker.UserControls.ShortSale.ShortSaleControl();
            this._buyersInfoTab = new System.Windows.Forms.TabPage();
            this._buyerInfoOverview = new DealMaker.BuyerInfoOverView();
            this.tpImportedInfo = new System.Windows.Forms.TabPage();
            this.tcSubImportedInfo = new System.Windows.Forms.TabControl();
            this.tpPropertyCharacteristics = new System.Windows.Forms.TabPage();
            this._copyDataToDealInfo = new System.Windows.Forms.Button();
            this.readMe6 = new DealMaker.ReadMe();
            this.tb_II_PC_Pool = new System.Windows.Forms.TextBox();
            this.label170 = new System.Windows.Forms.Label();
            this.tb_II_PC_LotSqFt = new System.Windows.Forms.TextBox();
            this.label169 = new System.Windows.Forms.Label();
            this.tb_II_PC_HomeSqFt = new System.Windows.Forms.TextBox();
            this.label168 = new System.Windows.Forms.Label();
            this.tb_II_PC_Garage = new System.Windows.Forms.TextBox();
            this.label167 = new System.Windows.Forms.Label();
            this.tb_II_PC_Bathrooms = new System.Windows.Forms.TextBox();
            this.label164 = new System.Windows.Forms.Label();
            this.tb_II_PC_Bedrooms = new System.Windows.Forms.TextBox();
            this.label165 = new System.Windows.Forms.Label();
            this.tb_II_PC_YearBuilt = new System.Windows.Forms.TextBox();
            this.label166 = new System.Windows.Forms.Label();
            this.tb_II_PC_LegalDesc = new System.Windows.Forms.TextBox();
            this.label162 = new System.Windows.Forms.Label();
            this.tb_II_PC_PropertyType = new System.Windows.Forms.TextBox();
            this.label161 = new System.Windows.Forms.Label();
            this.tb_II_PC_UseCode = new System.Windows.Forms.TextBox();
            this.label160 = new System.Windows.Forms.Label();
            this.tb_II_PC_Subdivision = new System.Windows.Forms.TextBox();
            this.label159 = new System.Windows.Forms.Label();
            this.tb_II_PC_County = new System.Windows.Forms.TextBox();
            this.label158 = new System.Windows.Forms.Label();
            this.tpListingInfo = new System.Windows.Forms.TabPage();
            this.tb_II_LI_ListingPrice = new System.Windows.Forms.TextBox();
            this.bSaveImportedInfo = new System.Windows.Forms.Button();
            this.tb_II_LI_Comments = new System.Windows.Forms.TextBox();
            this.label157 = new System.Windows.Forms.Label();
            this.tb_II_LI_LA_Email = new System.Windows.Forms.TextBox();
            this.label156 = new System.Windows.Forms.Label();
            this.tb_II_LI_LA_FaxNumber = new System.Windows.Forms.TextBox();
            this.label155 = new System.Windows.Forms.Label();
            this.label154 = new System.Windows.Forms.Label();
            this.tb_II_LI_LA_PhoneNumber = new System.Windows.Forms.TextBox();
            this.tb_II_LI_LA_AgentCode = new System.Windows.Forms.TextBox();
            this.label153 = new System.Windows.Forms.Label();
            this.tb_II_LI_LA_FullName = new System.Windows.Forms.TextBox();
            this.label152 = new System.Windows.Forms.Label();
            this.tb_II_LI_LA_LastName = new System.Windows.Forms.TextBox();
            this.label151 = new System.Windows.Forms.Label();
            this.tb_II_LI_LA_FirstName = new System.Windows.Forms.TextBox();
            this.label150 = new System.Windows.Forms.Label();
            this.label149 = new System.Windows.Forms.Label();
            this.tb_II_LI_ListingOffice = new System.Windows.Forms.TextBox();
            this.label148 = new System.Windows.Forms.Label();
            this.tb_II_LI_ListingMLSNumber = new System.Windows.Forms.TextBox();
            this.label147 = new System.Windows.Forms.Label();
            this.tb_II_LI_Status = new System.Windows.Forms.TextBox();
            this.label146 = new System.Windows.Forms.Label();
            this.label145 = new System.Windows.Forms.Label();
            this.label144 = new System.Windows.Forms.Label();
            this.readMe5 = new DealMaker.ReadMe();
            this.tpForeClosureInfo = new System.Windows.Forms.TabPage();
            this.tb_II_FI_OpeningBidAmount = new System.Windows.Forms.TextBox();
            this.tb_II_FI_UnpaidBalance = new System.Windows.Forms.TextBox();
            this.tb_II_FI_OrigMorgageBalance = new System.Windows.Forms.TextBox();
            this.tb_II_FI_EstimatedEquity = new System.Windows.Forms.TextBox();
            this.tb_II_FI_EstimatedHome = new System.Windows.Forms.TextBox();
            this.tb_II_FI_TaxAssedValue = new System.Windows.Forms.TextBox();
            this.label143 = new System.Windows.Forms.Label();
            this.label142 = new System.Windows.Forms.Label();
            this.label141 = new System.Windows.Forms.Label();
            this.tb_II_FI_MortgageBeneLast = new System.Windows.Forms.TextBox();
            this.label140 = new System.Windows.Forms.Label();
            this.tb_II_FI_MortgageBenefFirst = new System.Windows.Forms.TextBox();
            this.label139 = new System.Windows.Forms.Label();
            this.label138 = new System.Windows.Forms.Label();
            this.label137 = new System.Windows.Forms.Label();
            this.tb_II_FI_RecordingDate = new System.Windows.Forms.TextBox();
            this.label136 = new System.Windows.Forms.Label();
            this.label135 = new System.Windows.Forms.Label();
            this.tb_II_FI_LoanRecNumber = new System.Windows.Forms.TextBox();
            this.label134 = new System.Windows.Forms.Label();
            this.tb_II_FI_ZIP = new System.Windows.Forms.TextBox();
            this.label132 = new System.Windows.Forms.Label();
            this.tb_II_FI_State = new System.Windows.Forms.TextBox();
            this.label131 = new System.Windows.Forms.Label();
            this.tb_II_FI_City = new System.Windows.Forms.TextBox();
            this.label130 = new System.Windows.Forms.Label();
            this.tb_II_FI_TrusteeFullStreetAddress = new System.Windows.Forms.TextBox();
            this.label129 = new System.Windows.Forms.Label();
            this.tb_II_FI_TrusteeSaleNr = new System.Windows.Forms.TextBox();
            this.label128 = new System.Windows.Forms.Label();
            this.tb_II_FI_TrusteePhoneNr = new System.Windows.Forms.TextBox();
            this.label127 = new System.Windows.Forms.Label();
            this.tb_II_FI_TrusteeName = new System.Windows.Forms.TextBox();
            this.label126 = new System.Windows.Forms.Label();
            this.tb_II_FI_SalePrice = new System.Windows.Forms.TextBox();
            this.label123 = new System.Windows.Forms.Label();
            this.tb_II_FI_SaleTime = new System.Windows.Forms.TextBox();
            this.label122 = new System.Windows.Forms.Label();
            this.tb_II_FI_Foreclosure_Sale = new System.Windows.Forms.TextBox();
            this.label121 = new System.Windows.Forms.Label();
            this.tb_II_FI_FileNumber = new System.Windows.Forms.TextBox();
            this.label120 = new System.Windows.Forms.Label();
            this.tb_II_FI_ForeclosureFileDate = new System.Windows.Forms.TextBox();
            this.label119 = new System.Windows.Forms.Label();
            this.tb_II_FI_Bathrooms = new System.Windows.Forms.TextBox();
            this.label118 = new System.Windows.Forms.Label();
            this.tb_II_FI_Bedrooms = new System.Windows.Forms.TextBox();
            this.label117 = new System.Windows.Forms.Label();
            this.tb_II_FI_HomeSqFt = new System.Windows.Forms.TextBox();
            this.label116 = new System.Windows.Forms.Label();
            this.tb_II_FI_YearBuilt = new System.Windows.Forms.TextBox();
            this.label115 = new System.Windows.Forms.Label();
            this.tb_II_FI_DeedDate = new System.Windows.Forms.TextBox();
            this.label114 = new System.Windows.Forms.Label();
            this.tb_II_FI_SaleDocNo = new System.Windows.Forms.TextBox();
            this.label113 = new System.Windows.Forms.Label();
            this.tb_II_FI_LastSaleValue = new System.Windows.Forms.TextBox();
            this.label112 = new System.Windows.Forms.Label();
            this.label111 = new System.Windows.Forms.Label();
            this.label110 = new System.Windows.Forms.Label();
            this.label109 = new System.Windows.Forms.Label();
            this.label107 = new System.Windows.Forms.Label();
            this.label108 = new System.Windows.Forms.Label();
            this.tb_II_FI_OriginalTrustor = new System.Windows.Forms.TextBox();
            this.tb_II_FI_ForeclosureStatus = new System.Windows.Forms.TextBox();
            this.label105 = new System.Windows.Forms.Label();
            this.label106 = new System.Windows.Forms.Label();
            this.tb_II_FI_PropertyType = new System.Windows.Forms.TextBox();
            this.tb_II_FI_UseCode = new System.Windows.Forms.TextBox();
            this.tb_II_FI_APN = new System.Windows.Forms.TextBox();
            this.tb_II_FI_Subdivision = new System.Windows.Forms.TextBox();
            this.tb_II_FI_County = new System.Windows.Forms.TextBox();
            this.label104 = new System.Windows.Forms.Label();
            this.label103 = new System.Windows.Forms.Label();
            this.label102 = new System.Windows.Forms.Label();
            this.label101 = new System.Windows.Forms.Label();
            this.label100 = new System.Windows.Forms.Label();
            this.label99 = new System.Windows.Forms.Label();
            this.readMe4 = new DealMaker.ReadMe();
            this.tpFinancials = new System.Windows.Forms.TabPage();
            this.tb_II_F_LoanAmount = new System.Windows.Forms.TextBox();
            this.tb_II_F_CostPerSqFt = new System.Windows.Forms.TextBox();
            this.tb_II_F_EstimatedEquity = new System.Windows.Forms.TextBox();
            this.label195 = new System.Windows.Forms.Label();
            this.label196 = new System.Windows.Forms.Label();
            this.tb_II_F_EstimatedHome = new System.Windows.Forms.TextBox();
            this.label193 = new System.Windows.Forms.Label();
            this.label194 = new System.Windows.Forms.Label();
            this.tb_II_F_CashDownPayment = new System.Windows.Forms.TextBox();
            this.label190 = new System.Windows.Forms.Label();
            this.label191 = new System.Windows.Forms.Label();
            this.tb_II_F_TotalLoans = new System.Windows.Forms.TextBox();
            this.label188 = new System.Windows.Forms.Label();
            this.label189 = new System.Windows.Forms.Label();
            this.tb_II_F_LoanAmount2 = new System.Windows.Forms.TextBox();
            this.label186 = new System.Windows.Forms.Label();
            this.label187 = new System.Windows.Forms.Label();
            this.label185 = new System.Windows.Forms.Label();
            this.tb_II_F_Lender = new System.Windows.Forms.TextBox();
            this.label184 = new System.Windows.Forms.Label();
            this.label183 = new System.Windows.Forms.Label();
            this.tb_II_F_LoanInterestRate = new System.Windows.Forms.TextBox();
            this.label182 = new System.Windows.Forms.Label();
            this.tb_II_F_LoanType = new System.Windows.Forms.TextBox();
            this.label181 = new System.Windows.Forms.Label();
            this.label179 = new System.Windows.Forms.Label();
            this.label180 = new System.Windows.Forms.Label();
            this.label178 = new System.Windows.Forms.Label();
            this.label176 = new System.Windows.Forms.Label();
            this.label177 = new System.Windows.Forms.Label();
            this.tb_II_F_DeedDate = new System.Windows.Forms.TextBox();
            this.label175 = new System.Windows.Forms.Label();
            this.tb_II_F_SaleDocumentNumber = new System.Windows.Forms.TextBox();
            this.label174 = new System.Windows.Forms.Label();
            this.tb_II_F_SaleDate = new System.Windows.Forms.TextBox();
            this.label173 = new System.Windows.Forms.Label();
            this.tb_II_F_SaleValue = new System.Windows.Forms.TextBox();
            this.label172 = new System.Windows.Forms.Label();
            this.label171 = new System.Windows.Forms.Label();
            this.readMe7 = new DealMaker.ReadMe();
            this.tpTaxInfo = new System.Windows.Forms.TabPage();
            this.tb_II_F_TaxAssessedValue = new System.Windows.Forms.TextBox();
            this.tb_II_F_LandValue = new System.Windows.Forms.TextBox();
            this.tb_II_F_TaxAmount = new System.Windows.Forms.TextBox();
            this.tb_II_F_FullCashValue = new System.Windows.Forms.TextBox();
            this.tb_II_F_OwnerAbsentOccupied = new System.Windows.Forms.TextBox();
            this.label217 = new System.Windows.Forms.Label();
            this.label216 = new System.Windows.Forms.Label();
            this.label215 = new System.Windows.Forms.Label();
            this.label214 = new System.Windows.Forms.Label();
            this.label213 = new System.Windows.Forms.Label();
            this.label212 = new System.Windows.Forms.Label();
            this.tb_II_F_PercentImprovement = new System.Windows.Forms.TextBox();
            this.label207 = new System.Windows.Forms.Label();
            this.label208 = new System.Windows.Forms.Label();
            this.label209 = new System.Windows.Forms.Label();
            this.label210 = new System.Windows.Forms.Label();
            this.label211 = new System.Windows.Forms.Label();
            this.tb_II_F_TaxDeliquentYear = new System.Windows.Forms.TextBox();
            this.tb_II_F_TaxStatus = new System.Windows.Forms.TextBox();
            this.tb_II_F_TaxExemption = new System.Windows.Forms.TextBox();
            this.tb_II_F_TaxRateArea = new System.Windows.Forms.TextBox();
            this.tb_II_F_APN = new System.Windows.Forms.TextBox();
            this.label206 = new System.Windows.Forms.Label();
            this.label205 = new System.Windows.Forms.Label();
            this.label204 = new System.Windows.Forms.Label();
            this.label203 = new System.Windows.Forms.Label();
            this.label202 = new System.Windows.Forms.Label();
            this.tb_II_F_MailZIP = new System.Windows.Forms.TextBox();
            this.tb_II_F_MailCityState = new System.Windows.Forms.TextBox();
            this.tb_II_TI_MailFullStreetAddress = new System.Windows.Forms.TextBox();
            this.label200 = new System.Windows.Forms.Label();
            this.label199 = new System.Windows.Forms.Label();
            this.label198 = new System.Windows.Forms.Label();
            this.label197 = new System.Windows.Forms.Label();
            this.readMe8 = new DealMaker.ReadMe();
            this.tpOtherInfo = new System.Windows.Forms.TabPage();
            this.readMe9 = new DealMaker.ReadMe();
            this.tb_II_OU_UF5 = new System.Windows.Forms.TextBox();
            this.tb_II_OU_UF4 = new System.Windows.Forms.TextBox();
            this.tb_II_OU_UF3 = new System.Windows.Forms.TextBox();
            this.tb_II_OU_UF2 = new System.Windows.Forms.TextBox();
            this.tb_II_OU_UF1 = new System.Windows.Forms.TextBox();
            this.tb_II_OU_DoNotCallFlag = new System.Windows.Forms.TextBox();
            this.tb_II_OU_PrivacyFlag = new System.Windows.Forms.TextBox();
            this.tb_II_OU_MailScore = new System.Windows.Forms.TextBox();
            this.tb_II_OU_MailCRRT = new System.Windows.Forms.TextBox();
            this.tb_II_OU_DivorceDate = new System.Windows.Forms.TextBox();
            this.tb_II_OU_DivorceStatus = new System.Windows.Forms.TextBox();
            this.tb_II_OU_MapArea = new System.Windows.Forms.TextBox();
            this.tb_II_OU_MapGrid = new System.Windows.Forms.TextBox();
            this.tb_II_OU_Vesting = new System.Windows.Forms.TextBox();
            this.tb_II_OU_LegalDescription = new System.Windows.Forms.TextBox();
            this.tb_II_OU_Subdividion = new System.Windows.Forms.TextBox();
            this.tb_II_OI_County = new System.Windows.Forms.TextBox();
            this.label234 = new System.Windows.Forms.Label();
            this.label233 = new System.Windows.Forms.Label();
            this.label232 = new System.Windows.Forms.Label();
            this.label231 = new System.Windows.Forms.Label();
            this.label230 = new System.Windows.Forms.Label();
            this.label229 = new System.Windows.Forms.Label();
            this.label228 = new System.Windows.Forms.Label();
            this.label227 = new System.Windows.Forms.Label();
            this.label226 = new System.Windows.Forms.Label();
            this.label225 = new System.Windows.Forms.Label();
            this.label224 = new System.Windows.Forms.Label();
            this.label223 = new System.Windows.Forms.Label();
            this.label222 = new System.Windows.Forms.Label();
            this.label221 = new System.Windows.Forms.Label();
            this.label220 = new System.Windows.Forms.Label();
            this.label219 = new System.Windows.Forms.Label();
            this.label218 = new System.Windows.Forms.Label();
            this.tpCommercial = new System.Windows.Forms.TabPage();
            this._commercialControl = new ProfitGrabber.Commercial.CommercialControl();
            this.upperPanel = new System.Windows.Forms.Panel();
            this._pbHouse = new System.Windows.Forms.PictureBox();
            this.tbStatusGroup = new System.Windows.Forms.TextBox();
            this._unLockPI = new System.Windows.Forms.Button();
            this.pbMap = new System.Windows.Forms.PictureBox();
            this.label133 = new System.Windows.Forms.Label();
            this.tbSiteCityState = new System.Windows.Forms.TextBox();
            this.tbSiteState = new System.Windows.Forms.TextBox();
            this.label124 = new System.Windows.Forms.Label();
            this.label97 = new System.Windows.Forms.Label();
            this.label96 = new System.Windows.Forms.Label();
            this.label95 = new System.Windows.Forms.Label();
            this.label94 = new System.Windows.Forms.Label();
            this.label93 = new System.Windows.Forms.Label();
            this.tbSubDivision = new System.Windows.Forms.TextBox();
            this.tbCounty = new System.Windows.Forms.TextBox();
            this.tbSiteCity = new System.Windows.Forms.TextBox();
            this.bSave = new System.Windows.Forms.Button();
            this.bNew = new System.Windows.Forms.Button();
            this.tbSiteZIP = new System.Windows.Forms.TextBox();
            this.tbSiteAdd = new System.Windows.Forms.TextBox();
            this.tbFullName = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.tbLastName = new System.Windows.Forms.TextBox();
            this.tbMiddleName = new System.Windows.Forms.TextBox();
            this.tbFirstname = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.tbLastModified = new System.Windows.Forms.TextBox();
            this.tbFirstEntered = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.bLast = new System.Windows.Forms.Button();
            this.bNext = new System.Windows.Forms.Button();
            this.bFirst = new System.Windows.Forms.Button();
            this.bPrev = new System.Windows.Forms.Button();
            this.tbCurrPos = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.lockStatus = new System.Windows.Forms.Label();
            this.ttMap = new System.Windows.Forms.ToolTip(this.components);
            this.ctxPbHouseOptions = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.getStreetView = new System.Windows.Forms.ToolStripMenuItem();
            this.uploadPhoto = new System.Windows.Forms.ToolStripMenuItem();
            this.panel14 = new System.Windows.Forms.Panel();
            this.panel15 = new System.Windows.Forms.Panel();
            this._up_address_panel = new System.Windows.Forms.Panel();
            this._up_names_panel = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this._ci_notes = new System.Windows.Forms.Panel();
            this.lowerPanel.SuspendLayout();
            this._contactTab.SuspendLayout();
            this.tpContactInfo.SuspendLayout();
            this.panel3.SuspendLayout();
            this._ci_email_panel.SuspendLayout();
            this._ci_fax_panel.SuspendLayout();
            this._ci_cellphone_panel.SuspendLayout();
            this._ci_workphone_panel.SuspendLayout();
            this._ci_homephone_panel.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this._ci_old_panel.SuspendLayout();
            this._ci_previous_panel.SuspendLayout();
            this._ci_lk_panel.SuspendLayout();
            this.panel1.SuspendLayout();
            this._ci_st_landline_panel.SuspendLayout();
            this._ci_st_cell_panel.SuspendLayout();
            this._ci_st_email_panel.SuspendLayout();
            this.tpDealInfo.SuspendLayout();
            this.tpSkipTracedInfo.SuspendLayout();
            this.skipTracedInfoTabControl.SuspendLayout();
            this.skipTracedContactInfo.SuspendLayout();
            this.skipTracedOtherProperty.SuspendLayout();
            this.skipTracedRelativesInfo.SuspendLayout();
            this.tpGroupMembership.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this._evaluate.SuspendLayout();
            this._evaluateSubTabControl.SuspendLayout();
            this._propertyProfileSubTabPage.SuspendLayout();
            this._compsSubTabPage.SuspendLayout();
            this._estimateRepairsSubTabPage.SuspendLayout();
            this._shortSaleTab.SuspendLayout();
            this._buyersInfoTab.SuspendLayout();
            this.tpImportedInfo.SuspendLayout();
            this.tcSubImportedInfo.SuspendLayout();
            this.tpPropertyCharacteristics.SuspendLayout();
            this.tpListingInfo.SuspendLayout();
            this.tpForeClosureInfo.SuspendLayout();
            this.tpFinancials.SuspendLayout();
            this.tpTaxInfo.SuspendLayout();
            this.tpOtherInfo.SuspendLayout();
            this.tpCommercial.SuspendLayout();
            this.upperPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._pbHouse)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbMap)).BeginInit();
            this.ctxPbHouseOptions.SuspendLayout();
            this.panel14.SuspendLayout();
            this.panel15.SuspendLayout();
            this._up_address_panel.SuspendLayout();
            this._up_names_panel.SuspendLayout();
            this.panel4.SuspendLayout();
            this._ci_notes.SuspendLayout();
            this.SuspendLayout();
            // 
            // lowerPanel
            // 
            this.lowerPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.lowerPanel.Controls.Add(this._contactTab);
            this.lowerPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lowerPanel.Location = new System.Drawing.Point(0, 157);
            this.lowerPanel.Name = "lowerPanel";
            this.lowerPanel.Size = new System.Drawing.Size(1320, 683);
            this.lowerPanel.TabIndex = 0;
            // 
            // _contactTab
            // 
            this._contactTab.Controls.Add(this.tpContactInfo);
            this._contactTab.Controls.Add(this.tpDealInfo);
            this._contactTab.Controls.Add(this.tpSkipTracedInfo);
            this._contactTab.Controls.Add(this.tpGroupMembership);
            this._contactTab.Controls.Add(this.tpActivitiesHistory);
            this._contactTab.Controls.Add(this._evaluate);
            this._contactTab.Controls.Add(this._shortSaleTab);
            this._contactTab.Controls.Add(this._buyersInfoTab);
            this._contactTab.Controls.Add(this.tpImportedInfo);
            this._contactTab.Controls.Add(this.tpCommercial);
            this._contactTab.Dock = System.Windows.Forms.DockStyle.Fill;
            this._contactTab.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._contactTab.Location = new System.Drawing.Point(0, 0);
            this._contactTab.Name = "_contactTab";
            this._contactTab.SelectedIndex = 0;
            this._contactTab.Size = new System.Drawing.Size(1320, 683);
            this._contactTab.TabIndex = 0;
            this._contactTab.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            // 
            // tpContactInfo
            // 
            this.tpContactInfo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.tpContactInfo.Controls.Add(this.panel4);
            this.tpContactInfo.Controls.Add(this.panel3);
            this.tpContactInfo.Controls.Add(this.panel2);
            this.tpContactInfo.Controls.Add(this.panel1);
            this.tpContactInfo.Controls.Add(this.readMe3);
            this.tpContactInfo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.tpContactInfo.Location = new System.Drawing.Point(4, 28);
            this.tpContactInfo.Name = "tpContactInfo";
            this.tpContactInfo.Size = new System.Drawing.Size(1312, 651);
            this.tpContactInfo.TabIndex = 7;
            this.tpContactInfo.Text = "Contact Info";
            this.tpContactInfo.Click += new System.EventHandler(this.tpContactInfo_Click);
            // 
            // _ci_NotesField
            // 
            this._ci_NotesField.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._ci_NotesField.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this._ci_NotesField.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._ci_NotesField.Location = new System.Drawing.Point(7, 2);
            this._ci_NotesField.Multiline = true;
            this._ci_NotesField.Name = "_ci_NotesField";
            this._ci_NotesField.Size = new System.Drawing.Size(670, 109);
            this._ci_NotesField.TabIndex = 0;
            // 
            // _ci_NotesLabel
            // 
            this._ci_NotesLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._ci_NotesLabel.AutoSize = true;
            this._ci_NotesLabel.Font = new System.Drawing.Font("Century Gothic", 12F);
            this._ci_NotesLabel.Location = new System.Drawing.Point(6, 14);
            this._ci_NotesLabel.Name = "_ci_NotesLabel";
            this._ci_NotesLabel.Size = new System.Drawing.Size(59, 21);
            this._ci_NotesLabel.TabIndex = 80;
            this._ci_NotesLabel.Text = "Notes:";
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.panel3.Controls.Add(this._ci_email_panel);
            this.panel3.Controls.Add(this._ci_fax_panel);
            this.panel3.Controls.Add(this._ci_cellphone_panel);
            this.panel3.Controls.Add(this._ci_workphone_panel);
            this.panel3.Controls.Add(this._ci_homephone_panel);
            this.panel3.Controls.Add(this.label20);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel3.Location = new System.Drawing.Point(0, 200);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(601, 241);
            this.panel3.TabIndex = 5;
            // 
            // _ci_email_panel
            // 
            this._ci_email_panel.Controls.Add(this.label19);
            this._ci_email_panel.Controls.Add(this.tbEmail);
            this._ci_email_panel.Location = new System.Drawing.Point(7, 206);
            this._ci_email_panel.Name = "_ci_email_panel";
            this._ci_email_panel.Size = new System.Drawing.Size(591, 32);
            this._ci_email_panel.TabIndex = 4;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.label19.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label19.Location = new System.Drawing.Point(3, 5);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(55, 21);
            this.label19.TabIndex = 40;
            this.label19.Text = "Email:";
            // 
            // tbEmail
            // 
            this.tbEmail.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.tbEmail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbEmail.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.tbEmail.Location = new System.Drawing.Point(135, 2);
            this.tbEmail.Name = "tbEmail";
            this.tbEmail.Size = new System.Drawing.Size(407, 27);
            this.tbEmail.TabIndex = 0;
            // 
            // _ci_fax_panel
            // 
            this._ci_fax_panel.Controls.Add(this.label18);
            this._ci_fax_panel.Controls.Add(this.tbFax);
            this._ci_fax_panel.Location = new System.Drawing.Point(7, 164);
            this._ci_fax_panel.Name = "_ci_fax_panel";
            this._ci_fax_panel.Size = new System.Drawing.Size(591, 32);
            this._ci_fax_panel.TabIndex = 3;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.label18.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label18.Location = new System.Drawing.Point(3, 5);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(41, 21);
            this.label18.TabIndex = 39;
            this.label18.Text = "Fax:";
            // 
            // tbFax
            // 
            this.tbFax.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.tbFax.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbFax.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.tbFax.Location = new System.Drawing.Point(135, 3);
            this.tbFax.Name = "tbFax";
            this.tbFax.Size = new System.Drawing.Size(282, 27);
            this.tbFax.TabIndex = 0;
            // 
            // _ci_cellphone_panel
            // 
            this._ci_cellphone_panel.Controls.Add(this.label17);
            this._ci_cellphone_panel.Controls.Add(this.tbCellPh);
            this._ci_cellphone_panel.Controls.Add(this.tbCellBestTime);
            this._ci_cellphone_panel.Location = new System.Drawing.Point(7, 122);
            this._ci_cellphone_panel.Name = "_ci_cellphone_panel";
            this._ci_cellphone_panel.Size = new System.Drawing.Size(591, 32);
            this._ci_cellphone_panel.TabIndex = 2;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.label17.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label17.Location = new System.Drawing.Point(3, 5);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(70, 21);
            this.label17.TabIndex = 61;
            this.label17.Text = "Cell Ph.:";
            // 
            // tbCellPh
            // 
            this.tbCellPh.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.tbCellPh.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbCellPh.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.tbCellPh.Location = new System.Drawing.Point(135, 3);
            this.tbCellPh.Name = "tbCellPh";
            this.tbCellPh.Size = new System.Drawing.Size(282, 27);
            this.tbCellPh.TabIndex = 0;
            // 
            // tbCellBestTime
            // 
            this.tbCellBestTime.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.tbCellBestTime.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbCellBestTime.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.tbCellBestTime.Location = new System.Drawing.Point(420, 3);
            this.tbCellBestTime.Name = "tbCellBestTime";
            this.tbCellBestTime.Size = new System.Drawing.Size(122, 27);
            this.tbCellBestTime.TabIndex = 1;
            // 
            // _ci_workphone_panel
            // 
            this._ci_workphone_panel.Controls.Add(this.label16);
            this._ci_workphone_panel.Controls.Add(this.tbWorkPh);
            this._ci_workphone_panel.Controls.Add(this.tbWorkBestTime);
            this._ci_workphone_panel.Location = new System.Drawing.Point(7, 80);
            this._ci_workphone_panel.Name = "_ci_workphone_panel";
            this._ci_workphone_panel.Size = new System.Drawing.Size(591, 32);
            this._ci_workphone_panel.TabIndex = 1;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.label16.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label16.Location = new System.Drawing.Point(3, 5);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(83, 21);
            this.label16.TabIndex = 60;
            this.label16.Text = "Work  Ph.:";
            // 
            // tbWorkPh
            // 
            this.tbWorkPh.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.tbWorkPh.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbWorkPh.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.tbWorkPh.Location = new System.Drawing.Point(135, 3);
            this.tbWorkPh.Name = "tbWorkPh";
            this.tbWorkPh.Size = new System.Drawing.Size(282, 27);
            this.tbWorkPh.TabIndex = 0;
            // 
            // tbWorkBestTime
            // 
            this.tbWorkBestTime.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.tbWorkBestTime.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbWorkBestTime.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.tbWorkBestTime.Location = new System.Drawing.Point(420, 3);
            this.tbWorkBestTime.Name = "tbWorkBestTime";
            this.tbWorkBestTime.Size = new System.Drawing.Size(122, 27);
            this.tbWorkBestTime.TabIndex = 1;
            // 
            // _ci_homephone_panel
            // 
            this._ci_homephone_panel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._ci_homephone_panel.Controls.Add(this.label15);
            this._ci_homephone_panel.Controls.Add(this.tbHomePh);
            this._ci_homephone_panel.Controls.Add(this.tbHomeBestTime);
            this._ci_homephone_panel.Location = new System.Drawing.Point(7, 38);
            this._ci_homephone_panel.Name = "_ci_homephone_panel";
            this._ci_homephone_panel.Size = new System.Drawing.Size(591, 32);
            this._ci_homephone_panel.TabIndex = 0;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.label15.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label15.Location = new System.Drawing.Point(3, 5);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(87, 21);
            this.label15.TabIndex = 59;
            this.label15.Text = "Home Ph.:";
            // 
            // tbHomePh
            // 
            this.tbHomePh.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.tbHomePh.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbHomePh.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.tbHomePh.Location = new System.Drawing.Point(135, 2);
            this.tbHomePh.Name = "tbHomePh";
            this.tbHomePh.Size = new System.Drawing.Size(282, 27);
            this.tbHomePh.TabIndex = 0;
            // 
            // tbHomeBestTime
            // 
            this.tbHomeBestTime.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.tbHomeBestTime.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbHomeBestTime.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.tbHomeBestTime.Location = new System.Drawing.Point(420, 2);
            this.tbHomeBestTime.Name = "tbHomeBestTime";
            this.tbHomeBestTime.Size = new System.Drawing.Size(122, 27);
            this.tbHomeBestTime.TabIndex = 1;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.label20.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label20.Location = new System.Drawing.Point(423, 14);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(86, 21);
            this.label20.TabIndex = 46;
            this.label20.Text = "Best Time:";
            this.label20.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.pictureBox1);
            this.panel2.Controls.Add(this.label10);
            this.panel2.Controls.Add(this._ci_old_panel);
            this.panel2.Controls.Add(this.label60);
            this.panel2.Controls.Add(this._ci_previous_panel);
            this.panel2.Controls.Add(this.label163);
            this.panel2.Controls.Add(this._ci_lk_panel);
            this.panel2.Controls.Add(this.label62);
            this.panel2.Controls.Add(this.label63);
            this.panel2.Controls.Add(this.label125);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1312, 200);
            this.panel2.TabIndex = 0;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(1232, 5);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(72, 72);
            this.pictureBox1.TabIndex = 77;
            this.pictureBox1.TabStop = false;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label10.Location = new System.Drawing.Point(7, 18);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(181, 25);
            this.label10.TabIndex = 70;
            this.label10.Text = "Mailing Address:";
            // 
            // _ci_old_panel
            // 
            this._ci_old_panel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._ci_old_panel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this._ci_old_panel.Controls.Add(this.label14);
            this._ci_old_panel.Controls.Add(this.tbMailOldAdd);
            this._ci_old_panel.Controls.Add(this.tbMailOldZIP);
            this._ci_old_panel.Controls.Add(this.tbMailOldCity);
            this._ci_old_panel.Controls.Add(this.tbMailOldState);
            this._ci_old_panel.Controls.Add(this.tbMailOldCityState);
            this._ci_old_panel.Location = new System.Drawing.Point(7, 154);
            this._ci_old_panel.Name = "_ci_old_panel";
            this._ci_old_panel.Size = new System.Drawing.Size(1313, 32);
            this._ci_old_panel.TabIndex = 4;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label14.Location = new System.Drawing.Point(1, 4);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(42, 21);
            this.label14.TabIndex = 52;
            this.label14.Text = "Old:";
            // 
            // tbMailOldAdd
            // 
            this.tbMailOldAdd.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.tbMailOldAdd.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbMailOldAdd.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbMailOldAdd.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(128)))), ((int)(((byte)(185)))));
            this.tbMailOldAdd.Location = new System.Drawing.Point(131, 2);
            this.tbMailOldAdd.Name = "tbMailOldAdd";
            this.tbMailOldAdd.Size = new System.Drawing.Size(279, 27);
            this.tbMailOldAdd.TabIndex = 0;
            // 
            // tbMailOldZIP
            // 
            this.tbMailOldZIP.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.tbMailOldZIP.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbMailOldZIP.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbMailOldZIP.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(128)))), ((int)(((byte)(185)))));
            this.tbMailOldZIP.Location = new System.Drawing.Point(816, 1);
            this.tbMailOldZIP.Name = "tbMailOldZIP";
            this.tbMailOldZIP.Size = new System.Drawing.Size(91, 27);
            this.tbMailOldZIP.TabIndex = 4;
            // 
            // tbMailOldCity
            // 
            this.tbMailOldCity.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.tbMailOldCity.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbMailOldCity.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbMailOldCity.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(128)))), ((int)(((byte)(185)))));
            this.tbMailOldCity.Location = new System.Drawing.Point(413, 1);
            this.tbMailOldCity.Name = "tbMailOldCity";
            this.tbMailOldCity.Size = new System.Drawing.Size(174, 27);
            this.tbMailOldCity.TabIndex = 1;
            // 
            // tbMailOldState
            // 
            this.tbMailOldState.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.tbMailOldState.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbMailOldState.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbMailOldState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(128)))), ((int)(((byte)(185)))));
            this.tbMailOldState.Location = new System.Drawing.Point(590, 1);
            this.tbMailOldState.Name = "tbMailOldState";
            this.tbMailOldState.Size = new System.Drawing.Size(56, 27);
            this.tbMailOldState.TabIndex = 2;
            // 
            // tbMailOldCityState
            // 
            this.tbMailOldCityState.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.tbMailOldCityState.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbMailOldCityState.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbMailOldCityState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(128)))), ((int)(((byte)(185)))));
            this.tbMailOldCityState.Location = new System.Drawing.Point(648, 1);
            this.tbMailOldCityState.Name = "tbMailOldCityState";
            this.tbMailOldCityState.ReadOnly = true;
            this.tbMailOldCityState.Size = new System.Drawing.Size(166, 27);
            this.tbMailOldCityState.TabIndex = 3;
            this.tbMailOldCityState.TabStop = false;
            // 
            // label60
            // 
            this.label60.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label60.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label60.Location = new System.Drawing.Point(134, 56);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(78, 21);
            this.label60.TabIndex = 74;
            this.label60.Text = "Address:";
            this.label60.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // _ci_previous_panel
            // 
            this._ci_previous_panel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._ci_previous_panel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this._ci_previous_panel.Controls.Add(this.label13);
            this._ci_previous_panel.Controls.Add(this.tbMailPreviousAdd);
            this._ci_previous_panel.Controls.Add(this.tbMailPreviousZIP);
            this._ci_previous_panel.Controls.Add(this.tbMailPreviousCity);
            this._ci_previous_panel.Controls.Add(this.tbMailPreviousState);
            this._ci_previous_panel.Controls.Add(this.tbMailPreviousCityState);
            this._ci_previous_panel.Location = new System.Drawing.Point(7, 118);
            this._ci_previous_panel.Name = "_ci_previous_panel";
            this._ci_previous_panel.Size = new System.Drawing.Size(1313, 32);
            this._ci_previous_panel.TabIndex = 3;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label13.Location = new System.Drawing.Point(3, 5);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(77, 21);
            this.label13.TabIndex = 51;
            this.label13.Text = "Previous:";
            // 
            // tbMailPreviousAdd
            // 
            this.tbMailPreviousAdd.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.tbMailPreviousAdd.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbMailPreviousAdd.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbMailPreviousAdd.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(128)))), ((int)(((byte)(185)))));
            this.tbMailPreviousAdd.Location = new System.Drawing.Point(131, 3);
            this.tbMailPreviousAdd.Name = "tbMailPreviousAdd";
            this.tbMailPreviousAdd.Size = new System.Drawing.Size(279, 27);
            this.tbMailPreviousAdd.TabIndex = 0;
            // 
            // tbMailPreviousZIP
            // 
            this.tbMailPreviousZIP.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.tbMailPreviousZIP.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbMailPreviousZIP.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbMailPreviousZIP.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(128)))), ((int)(((byte)(185)))));
            this.tbMailPreviousZIP.Location = new System.Drawing.Point(817, 3);
            this.tbMailPreviousZIP.Name = "tbMailPreviousZIP";
            this.tbMailPreviousZIP.Size = new System.Drawing.Size(91, 27);
            this.tbMailPreviousZIP.TabIndex = 4;
            // 
            // tbMailPreviousCity
            // 
            this.tbMailPreviousCity.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.tbMailPreviousCity.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbMailPreviousCity.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbMailPreviousCity.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(128)))), ((int)(((byte)(185)))));
            this.tbMailPreviousCity.Location = new System.Drawing.Point(414, 3);
            this.tbMailPreviousCity.Name = "tbMailPreviousCity";
            this.tbMailPreviousCity.Size = new System.Drawing.Size(173, 27);
            this.tbMailPreviousCity.TabIndex = 1;
            // 
            // tbMailPreviousState
            // 
            this.tbMailPreviousState.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.tbMailPreviousState.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbMailPreviousState.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbMailPreviousState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(128)))), ((int)(((byte)(185)))));
            this.tbMailPreviousState.Location = new System.Drawing.Point(590, 3);
            this.tbMailPreviousState.Name = "tbMailPreviousState";
            this.tbMailPreviousState.Size = new System.Drawing.Size(56, 27);
            this.tbMailPreviousState.TabIndex = 2;
            // 
            // tbMailPreviousCityState
            // 
            this.tbMailPreviousCityState.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.tbMailPreviousCityState.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbMailPreviousCityState.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbMailPreviousCityState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(128)))), ((int)(((byte)(185)))));
            this.tbMailPreviousCityState.Location = new System.Drawing.Point(648, 3);
            this.tbMailPreviousCityState.Name = "tbMailPreviousCityState";
            this.tbMailPreviousCityState.ReadOnly = true;
            this.tbMailPreviousCityState.Size = new System.Drawing.Size(166, 27);
            this.tbMailPreviousCityState.TabIndex = 3;
            this.tbMailPreviousCityState.TabStop = false;
            // 
            // label163
            // 
            this.label163.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label163.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label163.Location = new System.Drawing.Point(651, 56);
            this.label163.Name = "label163";
            this.label163.Size = new System.Drawing.Size(118, 21);
            this.label163.TabIndex = 70;
            this.label163.Text = "City, State:";
            this.label163.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // _ci_lk_panel
            // 
            this._ci_lk_panel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._ci_lk_panel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this._ci_lk_panel.Controls.Add(this.tbMailLastKnownAdd);
            this._ci_lk_panel.Controls.Add(this.tbMailLastKnownCityState);
            this._ci_lk_panel.Controls.Add(this.tbMailLastKnownZIP);
            this._ci_lk_panel.Controls.Add(this.tbMailLastKnownCity);
            this._ci_lk_panel.Controls.Add(this.tbMailLastKnownState);
            this._ci_lk_panel.Controls.Add(this.label12);
            this._ci_lk_panel.Location = new System.Drawing.Point(7, 80);
            this._ci_lk_panel.Name = "_ci_lk_panel";
            this._ci_lk_panel.Size = new System.Drawing.Size(1313, 32);
            this._ci_lk_panel.TabIndex = 2;
            // 
            // tbMailLastKnownAdd
            // 
            this.tbMailLastKnownAdd.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.tbMailLastKnownAdd.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbMailLastKnownAdd.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbMailLastKnownAdd.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(128)))), ((int)(((byte)(185)))));
            this.tbMailLastKnownAdd.Location = new System.Drawing.Point(131, 3);
            this.tbMailLastKnownAdd.Name = "tbMailLastKnownAdd";
            this.tbMailLastKnownAdd.Size = new System.Drawing.Size(279, 20);
            this.tbMailLastKnownAdd.TabIndex = 0;
            // 
            // tbMailLastKnownCityState
            // 
            this.tbMailLastKnownCityState.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.tbMailLastKnownCityState.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbMailLastKnownCityState.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbMailLastKnownCityState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(128)))), ((int)(((byte)(185)))));
            this.tbMailLastKnownCityState.Location = new System.Drawing.Point(648, 3);
            this.tbMailLastKnownCityState.Name = "tbMailLastKnownCityState";
            this.tbMailLastKnownCityState.ReadOnly = true;
            this.tbMailLastKnownCityState.Size = new System.Drawing.Size(166, 20);
            this.tbMailLastKnownCityState.TabIndex = 3;
            this.tbMailLastKnownCityState.TabStop = false;
            // 
            // tbMailLastKnownZIP
            // 
            this.tbMailLastKnownZIP.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.tbMailLastKnownZIP.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbMailLastKnownZIP.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbMailLastKnownZIP.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(128)))), ((int)(((byte)(185)))));
            this.tbMailLastKnownZIP.Location = new System.Drawing.Point(816, 3);
            this.tbMailLastKnownZIP.Name = "tbMailLastKnownZIP";
            this.tbMailLastKnownZIP.Size = new System.Drawing.Size(91, 20);
            this.tbMailLastKnownZIP.TabIndex = 4;
            // 
            // tbMailLastKnownCity
            // 
            this.tbMailLastKnownCity.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.tbMailLastKnownCity.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbMailLastKnownCity.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbMailLastKnownCity.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(128)))), ((int)(((byte)(185)))));
            this.tbMailLastKnownCity.Location = new System.Drawing.Point(413, 3);
            this.tbMailLastKnownCity.Name = "tbMailLastKnownCity";
            this.tbMailLastKnownCity.Size = new System.Drawing.Size(174, 20);
            this.tbMailLastKnownCity.TabIndex = 1;
            // 
            // tbMailLastKnownState
            // 
            this.tbMailLastKnownState.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.tbMailLastKnownState.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbMailLastKnownState.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbMailLastKnownState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(128)))), ((int)(((byte)(185)))));
            this.tbMailLastKnownState.Location = new System.Drawing.Point(590, 3);
            this.tbMailLastKnownState.Name = "tbMailLastKnownState";
            this.tbMailLastKnownState.Size = new System.Drawing.Size(56, 20);
            this.tbMailLastKnownState.TabIndex = 2;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label12.Location = new System.Drawing.Point(3, 5);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(102, 21);
            this.label12.TabIndex = 50;
            this.label12.Text = "Last Known:";
            // 
            // label62
            // 
            this.label62.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label62.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label62.Location = new System.Drawing.Point(417, 56);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(78, 21);
            this.label62.TabIndex = 75;
            this.label62.Text = "City:";
            this.label62.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // label63
            // 
            this.label63.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label63.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label63.Location = new System.Drawing.Point(820, 56);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(46, 21);
            this.label63.TabIndex = 76;
            this.label63.Text = "ZIP:";
            this.label63.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // label125
            // 
            this.label125.AutoSize = true;
            this.label125.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label125.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label125.Location = new System.Drawing.Point(593, 56);
            this.label125.Name = "label125";
            this.label125.Size = new System.Drawing.Size(57, 21);
            this.label125.TabIndex = 79;
            this.label125.Text = "State:";
            this.label125.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this._ci_st_landline_panel);
            this.panel1.Controls.Add(this._ci_st_cell_panel);
            this.panel1.Controls.Add(this._ci_st_email_panel);
            this.panel1.Controls.Add(this._stResultsLabel);
            this.panel1.Controls.Add(this.label297);
            this.panel1.Controls.Add(this._stLastUpdated);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 441);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1312, 210);
            this.panel1.TabIndex = 94;
            // 
            // _ci_st_landline_panel
            // 
            this._ci_st_landline_panel.Controls.Add(this._stLandline);
            this._ci_st_landline_panel.Controls.Add(this.label236);
            this._ci_st_landline_panel.Location = new System.Drawing.Point(3, 132);
            this._ci_st_landline_panel.Name = "_ci_st_landline_panel";
            this._ci_st_landline_panel.Size = new System.Drawing.Size(1292, 32);
            this._ci_st_landline_panel.TabIndex = 12;
            // 
            // _stLandline
            // 
            this._stLandline.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this._stLandline.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._stLandline.Font = new System.Drawing.Font("Century Gothic", 12F);
            this._stLandline.Location = new System.Drawing.Point(213, 3);
            this._stLandline.Name = "_stLandline";
            this._stLandline.Size = new System.Drawing.Size(282, 27);
            this._stLandline.TabIndex = 0;
            // 
            // label236
            // 
            this.label236.AutoSize = true;
            this.label236.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.label236.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label236.Location = new System.Drawing.Point(7, 4);
            this.label236.Name = "label236";
            this.label236.Size = new System.Drawing.Size(172, 21);
            this.label236.TabIndex = 87;
            this.label236.Text = "Skip Traced Landline:";
            // 
            // _ci_st_cell_panel
            // 
            this._ci_st_cell_panel.Controls.Add(this.label201);
            this._ci_st_cell_panel.Controls.Add(this._stCellPhone);
            this._ci_st_cell_panel.Location = new System.Drawing.Point(3, 89);
            this._ci_st_cell_panel.Name = "_ci_st_cell_panel";
            this._ci_st_cell_panel.Size = new System.Drawing.Size(1292, 32);
            this._ci_st_cell_panel.TabIndex = 11;
            // 
            // label201
            // 
            this.label201.AutoSize = true;
            this.label201.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.label201.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label201.Location = new System.Drawing.Point(7, 5);
            this.label201.Name = "label201";
            this.label201.Size = new System.Drawing.Size(187, 21);
            this.label201.TabIndex = 85;
            this.label201.Text = "Skip Traced Cellphone:";
            // 
            // _stCellPhone
            // 
            this._stCellPhone.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this._stCellPhone.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._stCellPhone.Font = new System.Drawing.Font("Century Gothic", 12F);
            this._stCellPhone.Location = new System.Drawing.Point(213, 3);
            this._stCellPhone.Name = "_stCellPhone";
            this._stCellPhone.Size = new System.Drawing.Size(282, 27);
            this._stCellPhone.TabIndex = 0;
            // 
            // _ci_st_email_panel
            // 
            this._ci_st_email_panel.Controls.Add(this.label192);
            this._ci_st_email_panel.Controls.Add(this._stEmail);
            this._ci_st_email_panel.Location = new System.Drawing.Point(3, 46);
            this._ci_st_email_panel.Name = "_ci_st_email_panel";
            this._ci_st_email_panel.Size = new System.Drawing.Size(1292, 32);
            this._ci_st_email_panel.TabIndex = 10;
            // 
            // label192
            // 
            this.label192.AutoSize = true;
            this.label192.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.label192.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label192.Location = new System.Drawing.Point(7, 5);
            this.label192.Name = "label192";
            this.label192.Size = new System.Drawing.Size(148, 21);
            this.label192.TabIndex = 83;
            this.label192.Text = "Skip Traced Email:";
            // 
            // _stEmail
            // 
            this._stEmail.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this._stEmail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._stEmail.Font = new System.Drawing.Font("Century Gothic", 12F);
            this._stEmail.Location = new System.Drawing.Point(213, 3);
            this._stEmail.Name = "_stEmail";
            this._stEmail.Size = new System.Drawing.Size(407, 27);
            this._stEmail.TabIndex = 0;
            // 
            // _stResultsLabel
            // 
            this._stResultsLabel.AutoSize = true;
            this._stResultsLabel.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._stResultsLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this._stResultsLabel.Location = new System.Drawing.Point(7, 10);
            this._stResultsLabel.Name = "_stResultsLabel";
            this._stResultsLabel.Size = new System.Drawing.Size(209, 25);
            this._stResultsLabel.TabIndex = 82;
            this._stResultsLabel.Text = "Skip Traced Results:";
            // 
            // label297
            // 
            this.label297.AutoSize = true;
            this.label297.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.label297.ForeColor = System.Drawing.Color.Red;
            this.label297.Location = new System.Drawing.Point(9, 175);
            this.label297.Name = "label297";
            this.label297.Size = new System.Drawing.Size(652, 21);
            this.label297.TabIndex = 90;
            this.label297.Text = "This is just a partial list, to see the full skip traced info, go to the Skip Tra" +
    "ced Info Tab.";
            // 
            // _stLastUpdated
            // 
            this._stLastUpdated.AutoSize = true;
            this._stLastUpdated.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._stLastUpdated.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this._stLastUpdated.Location = new System.Drawing.Point(144, 35);
            this._stLastUpdated.Name = "_stLastUpdated";
            this._stLastUpdated.Size = new System.Drawing.Size(0, 15);
            this._stLastUpdated.TabIndex = 89;
            // 
            // readMe3
            // 
            this.readMe3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.readMe3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.readMe3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("readMe3.BackgroundImage")));
            this.readMe3.Location = new System.Drawing.Point(1288, 8);
            this.readMe3.Name = "readMe3";
            this.readMe3.Size = new System.Drawing.Size(16, 16);
            this.readMe3.TabIndex = 78;
            // 
            // tpDealInfo
            // 
            this.tpDealInfo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.tpDealInfo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tpDealInfo.Controls.Add(this.tbReinsAmnt);
            this.tpDealInfo.Controls.Add(this.tb3MoPayment);
            this.tpDealInfo.Controls.Add(this.tb2MoPayment);
            this.tpDealInfo.Controls.Add(this.tbOtherLiens);
            this.tpDealInfo.Controls.Add(this.tbHOA);
            this.tpDealInfo.Controls.Add(this.tb3Bal);
            this.tpDealInfo.Controls.Add(this.tb2Bal);
            this.tpDealInfo.Controls.Add(this.tb1Bal);
            this.tpDealInfo.Controls.Add(this.tbWorthBySeller);
            this.tpDealInfo.Controls.Add(this.tbTaxAmount);
            this.tpDealInfo.Controls.Add(this.tb1MoPayment);
            this.tpDealInfo.Controls.Add(this.cbTaxIncl);
            this.tpDealInfo.Controls.Add(this.tbTotalOwed);
            this.tpDealInfo.Controls.Add(this.tbTotalMonthly);
            this.tpDealInfo.Controls.Add(this.label83);
            this.tpDealInfo.Controls.Add(this.tbARV);
            this.tpDealInfo.Controls.Add(this.cbLeadSource);
            this.tpDealInfo.Controls.Add(this.tbListingExpDate);
            this.tpDealInfo.Controls.Add(this.tbForSaleDate);
            this.tpDealInfo.Controls.Add(this.cbType);
            this.tpDealInfo.Controls.Add(this.tbSqFt);
            this.tpDealInfo.Controls.Add(this.tbAPN);
            this.tpDealInfo.Controls.Add(this.cbOccupiedBy);
            this.tpDealInfo.Controls.Add(this.cbPool);
            this.tpDealInfo.Controls.Add(this.cbGarage);
            this.tpDealInfo.Controls.Add(this.cbYearBuilt);
            this.tpDealInfo.Controls.Add(this.cbBaths);
            this.tpDealInfo.Controls.Add(this.cbBedrooms);
            this.tpDealInfo.Controls.Add(this.bPrintDealInfo);
            this.tpDealInfo.Controls.Add(this.tbNextStep);
            this.tpDealInfo.Controls.Add(this.cbMotivation);
            this.tpDealInfo.Controls.Add(this.label235);
            this.tpDealInfo.Controls.Add(this.tbDesiredClosingDate);
            this.tpDealInfo.Controls.Add(this.label1);
            this.tpDealInfo.Controls.Add(this.label98);
            this.tpDealInfo.Controls.Add(this.label91);
            this.tpDealInfo.Controls.Add(this.label90);
            this.tpDealInfo.Controls.Add(this.label89);
            this.tpDealInfo.Controls.Add(this.label88);
            this.tpDealInfo.Controls.Add(this.label87);
            this.tpDealInfo.Controls.Add(this.label86);
            this.tpDealInfo.Controls.Add(this.label85);
            this.tpDealInfo.Controls.Add(this.label84);
            this.tpDealInfo.Controls.Add(this.label82);
            this.tpDealInfo.Controls.Add(this.label81);
            this.tpDealInfo.Controls.Add(this.label45);
            this.tpDealInfo.Controls.Add(this.label41);
            this.tpDealInfo.Controls.Add(this.label80);
            this.tpDealInfo.Controls.Add(this.tbRepairs);
            this.tpDealInfo.Controls.Add(this.label79);
            this.tpDealInfo.Controls.Add(this.label78);
            this.tpDealInfo.Controls.Add(this.label67);
            this.tpDealInfo.Controls.Add(this.label66);
            this.tpDealInfo.Controls.Add(this.tbMinCashNeeded);
            this.tpDealInfo.Controls.Add(this.label77);
            this.tpDealInfo.Controls.Add(this.label76);
            this.tpDealInfo.Controls.Add(this.cbSell4What);
            this.tpDealInfo.Controls.Add(this.label75);
            this.tpDealInfo.Controls.Add(this.label74);
            this.tpDealInfo.Controls.Add(this.label54);
            this.tpDealInfo.Controls.Add(this.label73);
            this.tpDealInfo.Controls.Add(this.label72);
            this.tpDealInfo.Controls.Add(this.cbMoBehind);
            this.tpDealInfo.Controls.Add(this.label71);
            this.tpDealInfo.Controls.Add(this.label70);
            this.tpDealInfo.Controls.Add(this.cb3Type);
            this.tpDealInfo.Controls.Add(this.label69);
            this.tpDealInfo.Controls.Add(this.cb2Type);
            this.tpDealInfo.Controls.Add(this.label68);
            this.tpDealInfo.Controls.Add(this.label32);
            this.tpDealInfo.Controls.Add(this.label11);
            this.tpDealInfo.Controls.Add(this.label8);
            this.tpDealInfo.Controls.Add(this.label65);
            this.tpDealInfo.Controls.Add(this.label64);
            this.tpDealInfo.Controls.Add(this.cbCanCancel);
            this.tpDealInfo.Controls.Add(this.label57);
            this.tpDealInfo.Controls.Add(this.dtpListingExpDate);
            this.tpDealInfo.Controls.Add(this.label56);
            this.tpDealInfo.Controls.Add(this.cbListed);
            this.tpDealInfo.Controls.Add(this.cbPmntsCurrnet);
            this.tpDealInfo.Controls.Add(this.cbInForecl);
            this.tpDealInfo.Controls.Add(this.dtpForSaleDate);
            this.tpDealInfo.Controls.Add(this.label53);
            this.tpDealInfo.Controls.Add(this.label52);
            this.tpDealInfo.Controls.Add(this.label51);
            this.tpDealInfo.Controls.Add(this.label50);
            this.tpDealInfo.Controls.Add(this.label49);
            this.tpDealInfo.Controls.Add(this.label48);
            this.tpDealInfo.Controls.Add(this.label47);
            this.tpDealInfo.Controls.Add(this.label44);
            this.tpDealInfo.Controls.Add(this.tb3IntRate);
            this.tpDealInfo.Controls.Add(this.label46);
            this.tpDealInfo.Controls.Add(this.label43);
            this.tpDealInfo.Controls.Add(this.label40);
            this.tpDealInfo.Controls.Add(this.tb2IntRate);
            this.tpDealInfo.Controls.Add(this.label42);
            this.tpDealInfo.Controls.Add(this.label39);
            this.tpDealInfo.Controls.Add(this.cb1Type);
            this.tpDealInfo.Controls.Add(this.label38);
            this.tpDealInfo.Controls.Add(this.label37);
            this.tpDealInfo.Controls.Add(this.label36);
            this.tpDealInfo.Controls.Add(this.tb1IntRate);
            this.tpDealInfo.Controls.Add(this.label35);
            this.tpDealInfo.Controls.Add(this.label34);
            this.tpDealInfo.Controls.Add(this.label33);
            this.tpDealInfo.Controls.Add(this.cbSource);
            this.tpDealInfo.Controls.Add(this.label31);
            this.tpDealInfo.Controls.Add(this.label30);
            this.tpDealInfo.Controls.Add(this.tbWhySelling);
            this.tpDealInfo.Controls.Add(this.label29);
            this.tpDealInfo.Controls.Add(this.tbCondition);
            this.tpDealInfo.Controls.Add(this.label28);
            this.tpDealInfo.Controls.Add(this.label27);
            this.tpDealInfo.Controls.Add(this.label26);
            this.tpDealInfo.Controls.Add(this.label25);
            this.tpDealInfo.Controls.Add(this.label24);
            this.tpDealInfo.Controls.Add(this.label23);
            this.tpDealInfo.Controls.Add(this.label22);
            this.tpDealInfo.Controls.Add(this.label21);
            this.tpDealInfo.Controls.Add(this.tbLowestPriceAcceptable);
            this.tpDealInfo.Controls.Add(this.readMe1);
            this.tpDealInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tpDealInfo.Location = new System.Drawing.Point(4, 28);
            this.tpDealInfo.Name = "tpDealInfo";
            this.tpDealInfo.Size = new System.Drawing.Size(1312, 651);
            this.tpDealInfo.TabIndex = 0;
            this.tpDealInfo.Text = "Deal Info";
            // 
            // tbReinsAmnt
            // 
            this.tbReinsAmnt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbReinsAmnt.Location = new System.Drawing.Point(296, 328);
            this.tbReinsAmnt.Name = "tbReinsAmnt";
            this.tbReinsAmnt.RegexErrorMessage = "Not numeric value !";
            this.tbReinsAmnt.RegularExpressionMask = "^([1-9]{1}[\\d]{0,2}(\\,[\\d]{3})*(\\.[\\d]{0,2})?|[1-9]{1}[\\d]{0,}(\\.[\\d]{0,2})?|0(\\." +
    "[\\d]{0,2})?|(\\.[\\d]{1,2})?)$";
            this.tbReinsAmnt.Size = new System.Drawing.Size(88, 20);
            this.tbReinsAmnt.TabIndex = 36;
            // 
            // tb3MoPayment
            // 
            this.tb3MoPayment.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb3MoPayment.Location = new System.Drawing.Point(328, 272);
            this.tb3MoPayment.Name = "tb3MoPayment";
            this.tb3MoPayment.RegexErrorMessage = "Not numeric value !";
            this.tb3MoPayment.RegularExpressionMask = "^([1-9]{1}[\\d]{0,2}(\\,[\\d]{3})*(\\.[\\d]{0,2})?|[1-9]{1}[\\d]{0,}(\\.[\\d]{0,2})?|0(\\." +
    "[\\d]{0,2})?|(\\.[\\d]{1,2})?)$";
            this.tb3MoPayment.Size = new System.Drawing.Size(88, 20);
            this.tb3MoPayment.TabIndex = 32;
            // 
            // tb2MoPayment
            // 
            this.tb2MoPayment.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb2MoPayment.Location = new System.Drawing.Point(328, 224);
            this.tb2MoPayment.Name = "tb2MoPayment";
            this.tb2MoPayment.RegexErrorMessage = "Not numeric value !";
            this.tb2MoPayment.RegularExpressionMask = "^([1-9]{1}[\\d]{0,2}(\\,[\\d]{3})*(\\.[\\d]{0,2})?|[1-9]{1}[\\d]{0,}(\\.[\\d]{0,2})?|0(\\." +
    "[\\d]{0,2})?|(\\.[\\d]{1,2})?)$";
            this.tb2MoPayment.Size = new System.Drawing.Size(88, 20);
            this.tb2MoPayment.TabIndex = 27;
            // 
            // tbOtherLiens
            // 
            this.tbOtherLiens.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbOtherLiens.Location = new System.Drawing.Point(432, 272);
            this.tbOtherLiens.Name = "tbOtherLiens";
            this.tbOtherLiens.RegexErrorMessage = "Not numeric value !";
            this.tbOtherLiens.RegularExpressionMask = "^([1-9]{1}[\\d]{0,2}(\\,[\\d]{3})*(\\.[\\d]{0,2})?|[1-9]{1}[\\d]{0,}(\\.[\\d]{0,2})?|0(\\." +
    "[\\d]{0,2})?|(\\.[\\d]{1,2})?)$";
            this.tbOtherLiens.Size = new System.Drawing.Size(104, 20);
            this.tbOtherLiens.TabIndex = 33;
            // 
            // tbHOA
            // 
            this.tbHOA.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbHOA.Location = new System.Drawing.Point(432, 224);
            this.tbHOA.Name = "tbHOA";
            this.tbHOA.RegexErrorMessage = "Not numeric value !";
            this.tbHOA.RegularExpressionMask = "^([1-9]{1}[\\d]{0,2}(\\,[\\d]{3})*(\\.[\\d]{0,2})?|[1-9]{1}[\\d]{0,}(\\.[\\d]{0,2})?|0(\\." +
    "[\\d]{0,2})?|(\\.[\\d]{1,2})?)$";
            this.tbHOA.Size = new System.Drawing.Size(104, 20);
            this.tbHOA.TabIndex = 28;
            // 
            // tb3Bal
            // 
            this.tb3Bal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb3Bal.Location = new System.Drawing.Point(88, 272);
            this.tb3Bal.Name = "tb3Bal";
            this.tb3Bal.RegexErrorMessage = "Not numeric value !";
            this.tb3Bal.RegularExpressionMask = "^([1-9]{1}[\\d]{0,2}(\\,[\\d]{3})*(\\.[\\d]{0,2})?|[1-9]{1}[\\d]{0,}(\\.[\\d]{0,2})?|0(\\." +
    "[\\d]{0,2})?|(\\.[\\d]{1,2})?)$";
            this.tb3Bal.Size = new System.Drawing.Size(72, 20);
            this.tb3Bal.TabIndex = 29;
            // 
            // tb2Bal
            // 
            this.tb2Bal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb2Bal.Location = new System.Drawing.Point(88, 224);
            this.tb2Bal.Name = "tb2Bal";
            this.tb2Bal.RegexErrorMessage = "Not numeric value !";
            this.tb2Bal.RegularExpressionMask = "^([1-9]{1}[\\d]{0,2}(\\,[\\d]{3})*(\\.[\\d]{0,2})?|[1-9]{1}[\\d]{0,}(\\.[\\d]{0,2})?|0(\\." +
    "[\\d]{0,2})?|(\\.[\\d]{1,2})?)$";
            this.tb2Bal.Size = new System.Drawing.Size(72, 20);
            this.tb2Bal.TabIndex = 24;
            // 
            // tb1Bal
            // 
            this.tb1Bal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb1Bal.Location = new System.Drawing.Point(88, 176);
            this.tb1Bal.Name = "tb1Bal";
            this.tb1Bal.RegexErrorMessage = "Not numeric value !";
            this.tb1Bal.RegularExpressionMask = "^([1-9]{1}[\\d]{0,2}(\\,[\\d]{3})*(\\.[\\d]{0,2})?|[1-9]{1}[\\d]{0,}(\\.[\\d]{0,2})?|0(\\." +
    "[\\d]{0,2})?|(\\.[\\d]{1,2})?)$";
            this.tb1Bal.Size = new System.Drawing.Size(72, 20);
            this.tb1Bal.TabIndex = 18;
            // 
            // tbWorthBySeller
            // 
            this.tbWorthBySeller.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbWorthBySeller.Location = new System.Drawing.Point(520, 80);
            this.tbWorthBySeller.Name = "tbWorthBySeller";
            this.tbWorthBySeller.RegexErrorMessage = "Not numeric value !";
            this.tbWorthBySeller.RegularExpressionMask = "^([1-9]{1}[\\d]{0,2}(\\,[\\d]{3})*(\\.[\\d]{0,2})?|[1-9]{1}[\\d]{0,}(\\.[\\d]{0,2})?|0(\\." +
    "[\\d]{0,2})?|(\\.[\\d]{1,2})?)$";
            this.tbWorthBySeller.Size = new System.Drawing.Size(104, 20);
            this.tbWorthBySeller.TabIndex = 16;
            // 
            // tbTaxAmount
            // 
            this.tbTaxAmount.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbTaxAmount.Location = new System.Drawing.Point(560, 176);
            this.tbTaxAmount.Name = "tbTaxAmount";
            this.tbTaxAmount.RegexErrorMessage = "Not numeric value !";
            this.tbTaxAmount.RegularExpressionMask = "^([1-9]{1}[\\d]{0,2}(\\,[\\d]{3})*(\\.[\\d]{0,2})?|[1-9]{1}[\\d]{0,}(\\.[\\d]{0,2})?|0(\\." +
    "[\\d]{0,2})?|(\\.[\\d]{1,2})?)$";
            this.tbTaxAmount.Size = new System.Drawing.Size(112, 20);
            this.tbTaxAmount.TabIndex = 23;
            // 
            // tb1MoPayment
            // 
            this.tb1MoPayment.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb1MoPayment.Location = new System.Drawing.Point(328, 176);
            this.tb1MoPayment.Name = "tb1MoPayment";
            this.tb1MoPayment.RegexErrorMessage = "Not numeric value !";
            this.tb1MoPayment.RegularExpressionMask = "^([1-9]{1}[\\d]{0,2}(\\,[\\d]{3})*(\\.[\\d]{0,2})?|[1-9]{1}[\\d]{0,}(\\.[\\d]{0,2})?|0(\\." +
    "[\\d]{0,2})?|(\\.[\\d]{1,2})?)$";
            this.tb1MoPayment.Size = new System.Drawing.Size(88, 20);
            this.tb1MoPayment.TabIndex = 21;
            // 
            // cbTaxIncl
            // 
            this.cbTaxIncl.Items.AddRange(new object[] {
            "?",
            "Yes",
            "No"});
            this.cbTaxIncl.Location = new System.Drawing.Point(432, 176);
            this.cbTaxIncl.Name = "cbTaxIncl";
            this.cbTaxIncl.Size = new System.Drawing.Size(104, 21);
            this.cbTaxIncl.TabIndex = 22;
            // 
            // tbTotalOwed
            // 
            this.tbTotalOwed.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbTotalOwed.Location = new System.Drawing.Point(560, 224);
            this.tbTotalOwed.Name = "tbTotalOwed";
            this.tbTotalOwed.ReadOnly = true;
            this.tbTotalOwed.Size = new System.Drawing.Size(136, 20);
            this.tbTotalOwed.TabIndex = 56;
            this.tbTotalOwed.TabStop = false;
            // 
            // tbTotalMonthly
            // 
            this.tbTotalMonthly.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbTotalMonthly.Location = new System.Drawing.Point(560, 272);
            this.tbTotalMonthly.Name = "tbTotalMonthly";
            this.tbTotalMonthly.ReadOnly = true;
            this.tbTotalMonthly.Size = new System.Drawing.Size(136, 20);
            this.tbTotalMonthly.TabIndex = 58;
            this.tbTotalMonthly.TabStop = false;
            // 
            // label83
            // 
            this.label83.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label83.Location = new System.Drawing.Point(76, 272);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(16, 23);
            this.label83.TabIndex = 117;
            this.label83.Text = "$";
            this.label83.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tbARV
            // 
            this.tbARV.BackColor = System.Drawing.Color.LightGray;
            this.tbARV.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbARV.Location = new System.Drawing.Point(16, 520);
            this.tbARV.Name = "tbARV";
            this.tbARV.Size = new System.Drawing.Size(176, 20);
            this.tbARV.TabIndex = 108;
            this.tbARV.TabStop = false;
            // 
            // cbLeadSource
            // 
            this.cbLeadSource.Location = new System.Drawing.Point(216, 464);
            this.cbLeadSource.Name = "cbLeadSource";
            this.cbLeadSource.Size = new System.Drawing.Size(176, 21);
            this.cbLeadSource.TabIndex = 47;
            // 
            // tbListingExpDate
            // 
            this.tbListingExpDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbListingExpDate.Location = new System.Drawing.Point(640, 328);
            this.tbListingExpDate.Name = "tbListingExpDate";
            this.tbListingExpDate.Size = new System.Drawing.Size(104, 20);
            this.tbListingExpDate.TabIndex = 134;
            this.tbListingExpDate.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbListingExpDate_KeyPress);
            this.tbListingExpDate.MouseDown += new System.Windows.Forms.MouseEventHandler(this.tbListingExpDate_MouseDown);
            // 
            // tbForSaleDate
            // 
            this.tbForSaleDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbForSaleDate.Location = new System.Drawing.Point(472, 328);
            this.tbForSaleDate.Name = "tbForSaleDate";
            this.tbForSaleDate.Size = new System.Drawing.Size(96, 20);
            this.tbForSaleDate.TabIndex = 133;
            this.tbForSaleDate.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbForSaleDate_KeyPress);
            this.tbForSaleDate.MouseDown += new System.Windows.Forms.MouseEventHandler(this.tbForSaleDate_MouseDown);
            // 
            // cbType
            // 
            this.cbType.Items.AddRange(new object[] {
            "?",
            "SFH",
            "SFH - Two Levels",
            "SFH - Multi-Level",
            "LuxHome",
            "LuxHome - Two Levels",
            "LuxHome - Multi-Level",
            "Condo",
            "Condo - Two Levels",
            "Condo - Multi-Level",
            "Townhouse",
            "Townhouse - Two Levels",
            "Townhouse - Multi-Level",
            "Office Bld.",
            "Apartment",
            "Apartment Bld.",
            "Undeveloped",
            "Vacant Land"});
            this.cbType.Location = new System.Drawing.Point(8, 24);
            this.cbType.Name = "cbType";
            this.cbType.Size = new System.Drawing.Size(144, 21);
            this.cbType.TabIndex = 2;
            // 
            // tbSqFt
            // 
            this.tbSqFt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbSqFt.Location = new System.Drawing.Point(321, 24);
            this.tbSqFt.Name = "tbSqFt";
            this.tbSqFt.Size = new System.Drawing.Size(56, 20);
            this.tbSqFt.TabIndex = 8;
            // 
            // tbAPN
            // 
            this.tbAPN.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbAPN.Location = new System.Drawing.Point(781, 24);
            this.tbAPN.Name = "tbAPN";
            this.tbAPN.Size = new System.Drawing.Size(121, 20);
            this.tbAPN.TabIndex = 13;
            // 
            // cbOccupiedBy
            // 
            this.cbOccupiedBy.Items.AddRange(new object[] {
            "?",
            "Seller",
            "Tenant",
            "Vacant",
            "Family"});
            this.cbOccupiedBy.Location = new System.Drawing.Point(685, 24);
            this.cbOccupiedBy.Name = "cbOccupiedBy";
            this.cbOccupiedBy.Size = new System.Drawing.Size(91, 21);
            this.cbOccupiedBy.TabIndex = 12;
            // 
            // cbPool
            // 
            this.cbPool.Items.AddRange(new object[] {
            "?",
            "No",
            "Pool",
            "Heated Pool",
            "Diving Pool",
            "Community Pool "});
            this.cbPool.Location = new System.Drawing.Point(576, 24);
            this.cbPool.Name = "cbPool";
            this.cbPool.Size = new System.Drawing.Size(104, 21);
            this.cbPool.TabIndex = 11;
            // 
            // cbGarage
            // 
            this.cbGarage.Items.AddRange(new object[] {
            "?",
            "1 Car Garage",
            "2 Car Garage",
            "2.5 Car Garage",
            "3 Car Garage",
            "4 Car Garage",
            "4 + Garage",
            "1 Car Carport",
            "2 Car Carport",
            "2+ Car Carport",
            "Unassigned Parking",
            "Assigned Parking"});
            this.cbGarage.Location = new System.Drawing.Point(454, 24);
            this.cbGarage.Name = "cbGarage";
            this.cbGarage.Size = new System.Drawing.Size(117, 21);
            this.cbGarage.TabIndex = 10;
            // 
            // cbYearBuilt
            // 
            this.cbYearBuilt.Items.AddRange(new object[] {
            "1950",
            "1951",
            "1952",
            "1953",
            "1954",
            "1955",
            "1956",
            "1957",
            "1958",
            "1959",
            "1960",
            "1961",
            "1962",
            "1963",
            "1964",
            "1965",
            "1966",
            "1967",
            "1968",
            "1969",
            "1970",
            "1971",
            "1972",
            "1973",
            "1974",
            "1975",
            "1976",
            "1977",
            "1978",
            "1979",
            "1980",
            "1981",
            "1982",
            "1983",
            "1984",
            "1985",
            "1986",
            "1987",
            "1988",
            "1989",
            "1990",
            "1991",
            "1992",
            "1993",
            "1994",
            "1995",
            "1996",
            "1997",
            "1998",
            "1999",
            "2000",
            "2001",
            "2002",
            "2003",
            "2004",
            "2005",
            "2006",
            "2007",
            "2008",
            "2009",
            "2010",
            "2011",
            "2012",
            "2013",
            "2014",
            "2015",
            "2016",
            "2017",
            "2018",
            "2019"});
            this.cbYearBuilt.Location = new System.Drawing.Point(385, 24);
            this.cbYearBuilt.Name = "cbYearBuilt";
            this.cbYearBuilt.Size = new System.Drawing.Size(62, 21);
            this.cbYearBuilt.TabIndex = 9;
            // 
            // cbBaths
            // 
            this.cbBaths.Items.AddRange(new object[] {
            "1",
            "1.5",
            "1.75",
            "2",
            "2.5",
            "2.75",
            "3",
            "3.5",
            "3.75",
            "4",
            "4.5",
            "5",
            "5.5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15"});
            this.cbBaths.Location = new System.Drawing.Point(252, 24);
            this.cbBaths.Name = "cbBaths";
            this.cbBaths.Size = new System.Drawing.Size(62, 21);
            this.cbBaths.TabIndex = 6;
            // 
            // cbBedrooms
            // 
            this.cbBedrooms.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20"});
            this.cbBedrooms.Location = new System.Drawing.Point(159, 24);
            this.cbBedrooms.Name = "cbBedrooms";
            this.cbBedrooms.Size = new System.Drawing.Size(87, 21);
            this.cbBedrooms.TabIndex = 4;
            // 
            // bPrintDealInfo
            // 
            this.bPrintDealInfo.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.bPrintDealInfo.Location = new System.Drawing.Point(720, 176);
            this.bPrintDealInfo.Name = "bPrintDealInfo";
            this.bPrintDealInfo.Size = new System.Drawing.Size(112, 48);
            this.bPrintDealInfo.TabIndex = 132;
            this.bPrintDealInfo.Text = "&Print Deal Info";
            // 
            // tbNextStep
            // 
            this.tbNextStep.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbNextStep.Location = new System.Drawing.Point(400, 464);
            this.tbNextStep.MaxLength = 65535;
            this.tbNextStep.Multiline = true;
            this.tbNextStep.Name = "tbNextStep";
            this.tbNextStep.Size = new System.Drawing.Size(504, 80);
            this.tbNextStep.TabIndex = 48;
            // 
            // cbMotivation
            // 
            this.cbMotivation.Items.AddRange(new object[] {
            "?",
            "Extremely",
            "Must-Sell",
            "OK",
            "Little",
            "Not at All"});
            this.cbMotivation.Location = new System.Drawing.Point(16, 464);
            this.cbMotivation.Name = "cbMotivation";
            this.cbMotivation.Size = new System.Drawing.Size(176, 21);
            this.cbMotivation.TabIndex = 46;
            // 
            // label235
            // 
            this.label235.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label235.Location = new System.Drawing.Point(781, 8);
            this.label235.Name = "label235";
            this.label235.Size = new System.Drawing.Size(41, 16);
            this.label235.TabIndex = 131;
            this.label235.Text = "APN:";
            // 
            // tbDesiredClosingDate
            // 
            this.tbDesiredClosingDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbDesiredClosingDate.Location = new System.Drawing.Point(736, 392);
            this.tbDesiredClosingDate.Name = "tbDesiredClosingDate";
            this.tbDesiredClosingDate.Size = new System.Drawing.Size(168, 20);
            this.tbDesiredClosingDate.TabIndex = 45;
            // 
            // label1
            // 
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label1.Location = new System.Drawing.Point(421, 176);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(16, 23);
            this.label1.TabIndex = 128;
            this.label1.Text = "$";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label98
            // 
            this.label98.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label98.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label98.Location = new System.Drawing.Point(400, 440);
            this.label98.Name = "label98";
            this.label98.Size = new System.Drawing.Size(120, 16);
            this.label98.TabIndex = 127;
            this.label98.Text = "NOTES:";
            // 
            // label91
            // 
            this.label91.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label91.Location = new System.Drawing.Point(547, 176);
            this.label91.Name = "label91";
            this.label91.Size = new System.Drawing.Size(16, 23);
            this.label91.TabIndex = 125;
            this.label91.Text = "$";
            this.label91.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label90
            // 
            this.label90.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label90.Location = new System.Drawing.Point(547, 272);
            this.label90.Name = "label90";
            this.label90.Size = new System.Drawing.Size(16, 23);
            this.label90.TabIndex = 124;
            this.label90.Text = "$";
            this.label90.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label89
            // 
            this.label89.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label89.Location = new System.Drawing.Point(547, 224);
            this.label89.Name = "label89";
            this.label89.Size = new System.Drawing.Size(16, 23);
            this.label89.TabIndex = 123;
            this.label89.Text = "$";
            this.label89.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label88
            // 
            this.label88.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label88.Location = new System.Drawing.Point(421, 224);
            this.label88.Name = "label88";
            this.label88.Size = new System.Drawing.Size(16, 23);
            this.label88.TabIndex = 122;
            this.label88.Text = "$";
            this.label88.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label87
            // 
            this.label87.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label87.Location = new System.Drawing.Point(421, 272);
            this.label87.Name = "label87";
            this.label87.Size = new System.Drawing.Size(16, 23);
            this.label87.TabIndex = 121;
            this.label87.Text = "$";
            this.label87.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label86
            // 
            this.label86.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label86.Location = new System.Drawing.Point(317, 272);
            this.label86.Name = "label86";
            this.label86.Size = new System.Drawing.Size(16, 23);
            this.label86.TabIndex = 120;
            this.label86.Text = "$";
            this.label86.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label85
            // 
            this.label85.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label85.Location = new System.Drawing.Point(317, 224);
            this.label85.Name = "label85";
            this.label85.Size = new System.Drawing.Size(16, 23);
            this.label85.TabIndex = 119;
            this.label85.Text = "$";
            this.label85.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label84
            // 
            this.label84.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label84.Location = new System.Drawing.Point(317, 176);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(16, 23);
            this.label84.TabIndex = 118;
            this.label84.Text = "$";
            this.label84.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label82
            // 
            this.label82.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label82.Location = new System.Drawing.Point(76, 224);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(16, 23);
            this.label82.TabIndex = 116;
            this.label82.Text = "$";
            this.label82.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label81
            // 
            this.label81.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label81.Location = new System.Drawing.Point(76, 176);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(16, 23);
            this.label81.TabIndex = 115;
            this.label81.Text = "$";
            this.label81.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label45
            // 
            this.label45.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label45.Location = new System.Drawing.Point(168, 256);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(24, 16);
            this.label45.TabIndex = 114;
            this.label45.Text = "I.R.:";
            // 
            // label41
            // 
            this.label41.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label41.Location = new System.Drawing.Point(168, 208);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(32, 16);
            this.label41.TabIndex = 113;
            this.label41.Text = "I.R.:";
            // 
            // label80
            // 
            this.label80.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label80.Location = new System.Drawing.Point(685, 8);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(90, 23);
            this.label80.TabIndex = 111;
            this.label80.Text = "Occupied by:";
            // 
            // tbRepairs
            // 
            this.tbRepairs.BackColor = System.Drawing.Color.LightGray;
            this.tbRepairs.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbRepairs.Location = new System.Drawing.Point(216, 520);
            this.tbRepairs.Name = "tbRepairs";
            this.tbRepairs.Size = new System.Drawing.Size(176, 20);
            this.tbRepairs.TabIndex = 109;
            this.tbRepairs.TabStop = false;
            // 
            // label79
            // 
            this.label79.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label79.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label79.Location = new System.Drawing.Point(216, 496);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(120, 23);
            this.label79.TabIndex = 107;
            this.label79.Text = "Repairs:  ";
            // 
            // label78
            // 
            this.label78.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label78.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label78.Location = new System.Drawing.Point(16, 496);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(100, 23);
            this.label78.TabIndex = 106;
            this.label78.Text = "ARV:";
            // 
            // label67
            // 
            this.label67.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label67.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label67.Location = new System.Drawing.Point(216, 440);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(100, 24);
            this.label67.TabIndex = 104;
            this.label67.Text = "Lead Source:";
            // 
            // label66
            // 
            this.label66.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label66.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label66.Location = new System.Drawing.Point(16, 441);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(100, 23);
            this.label66.TabIndex = 102;
            this.label66.Text = "MOTIVATION:";
            // 
            // tbMinCashNeeded
            // 
            this.tbMinCashNeeded.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbMinCashNeeded.Location = new System.Drawing.Point(536, 392);
            this.tbMinCashNeeded.Name = "tbMinCashNeeded";
            this.tbMinCashNeeded.Size = new System.Drawing.Size(168, 20);
            this.tbMinCashNeeded.TabIndex = 44;
            // 
            // label77
            // 
            this.label77.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label77.Location = new System.Drawing.Point(520, 392);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(12, 23);
            this.label77.TabIndex = 100;
            this.label77.Text = "$";
            this.label77.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label76
            // 
            this.label76.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label76.Location = new System.Drawing.Point(536, 376);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(160, 16);
            this.label76.TabIndex = 99;
            this.label76.Text = "Min. Cash Needed-why:";
            this.label76.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // cbSell4What
            // 
            this.cbSell4What.Items.AddRange(new object[] {
            "?",
            "Yes",
            "No",
            "Maybe"});
            this.cbSell4What.Location = new System.Drawing.Point(280, 392);
            this.cbSell4What.Name = "cbSell4What";
            this.cbSell4What.Size = new System.Drawing.Size(208, 21);
            this.cbSell4What.TabIndex = 43;
            // 
            // label75
            // 
            this.label75.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label75.Location = new System.Drawing.Point(280, 376);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(160, 16);
            this.label75.TabIndex = 97;
            this.label75.Text = "Sell 4 what Owes?";
            this.label75.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // label74
            // 
            this.label74.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label74.Location = new System.Drawing.Point(67, 392);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(16, 23);
            this.label74.TabIndex = 96;
            this.label74.Text = "$";
            this.label74.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label54
            // 
            this.label54.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label54.Location = new System.Drawing.Point(88, 376);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(160, 16);
            this.label54.TabIndex = 95;
            this.label54.Text = "Lowest Price Acceptable:";
            this.label54.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // label73
            // 
            this.label73.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label73.Location = new System.Drawing.Point(576, 312);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(56, 16);
            this.label73.TabIndex = 94;
            this.label73.Text = "Listed?";
            // 
            // label72
            // 
            this.label72.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label72.Location = new System.Drawing.Point(285, 328);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(15, 23);
            this.label72.TabIndex = 93;
            this.label72.Text = "$";
            this.label72.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cbMoBehind
            // 
            this.cbMoBehind.Items.AddRange(new object[] {
            "?",
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20",
            "21",
            "22",
            "23",
            "24",
            "25",
            "26",
            "27",
            "28",
            "29",
            "30",
            "31",
            "32",
            "33",
            "34",
            "35",
            "36",
            "37",
            "38",
            "39",
            "40",
            "41",
            "42",
            "43",
            "44",
            "45",
            "46",
            "47",
            "48",
            "49",
            "50"});
            this.cbMoBehind.Location = new System.Drawing.Point(200, 328);
            this.cbMoBehind.Name = "cbMoBehind";
            this.cbMoBehind.Size = new System.Drawing.Size(80, 21);
            this.cbMoBehind.TabIndex = 35;
            // 
            // label71
            // 
            this.label71.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label71.Location = new System.Drawing.Point(200, 312);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(80, 16);
            this.label71.TabIndex = 91;
            this.label71.Text = "Mo. Behind:";
            // 
            // label70
            // 
            this.label70.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label70.Location = new System.Drawing.Point(560, 160);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(112, 23);
            this.label70.TabIndex = 89;
            this.label70.Text = "Tax && Ins/mo.:";
            // 
            // cb3Type
            // 
            this.cb3Type.Items.AddRange(new object[] {
            "?",
            "Credit Line",
            "Mortgage",
            "Private"});
            this.cb3Type.Location = new System.Drawing.Point(224, 272);
            this.cb3Type.Name = "cb3Type";
            this.cb3Type.Size = new System.Drawing.Size(88, 21);
            this.cb3Type.TabIndex = 31;
            // 
            // label69
            // 
            this.label69.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label69.Location = new System.Drawing.Point(224, 256);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(40, 23);
            this.label69.TabIndex = 87;
            this.label69.Text = "Type:";
            // 
            // cb2Type
            // 
            this.cb2Type.Items.AddRange(new object[] {
            "?",
            "Credit Line",
            "Mortgage",
            "Private"});
            this.cb2Type.Location = new System.Drawing.Point(224, 224);
            this.cb2Type.Name = "cb2Type";
            this.cb2Type.Size = new System.Drawing.Size(88, 21);
            this.cb2Type.TabIndex = 26;
            // 
            // label68
            // 
            this.label68.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label68.Location = new System.Drawing.Point(224, 208);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(48, 16);
            this.label68.TabIndex = 85;
            this.label68.Text = "Type:";
            // 
            // label32
            // 
            this.label32.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label32.Location = new System.Drawing.Point(200, 272);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(16, 23);
            this.label32.TabIndex = 84;
            this.label32.Text = "%";
            this.label32.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label11
            // 
            this.label11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label11.Location = new System.Drawing.Point(200, 224);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(16, 23);
            this.label11.TabIndex = 83;
            this.label11.Text = "%";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label8
            // 
            this.label8.ForeColor = System.Drawing.SystemColors.Highlight;
            this.label8.Location = new System.Drawing.Point(508, 80);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(16, 23);
            this.label8.TabIndex = 82;
            this.label8.Text = "$";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label65
            // 
            this.label65.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label65.Location = new System.Drawing.Point(736, 376);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(144, 16);
            this.label65.TabIndex = 78;
            this.label65.Text = "Desired Closing Date:";
            this.label65.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // label64
            // 
            this.label64.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label64.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label64.Location = new System.Drawing.Point(8, 360);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(64, 23);
            this.label64.TabIndex = 73;
            this.label64.Text = "WANTS:";
            // 
            // cbCanCancel
            // 
            this.cbCanCancel.Items.AddRange(new object[] {
            "?",
            "Yes",
            "No",
            "Maybe"});
            this.cbCanCancel.Location = new System.Drawing.Point(752, 328);
            this.cbCanCancel.Name = "cbCanCancel";
            this.cbCanCancel.Size = new System.Drawing.Size(104, 21);
            this.cbCanCancel.TabIndex = 41;
            // 
            // label57
            // 
            this.label57.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label57.Location = new System.Drawing.Point(752, 312);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(112, 16);
            this.label57.TabIndex = 71;
            this.label57.Text = "Can Cancel List:";
            // 
            // dtpListingExpDate
            // 
            this.dtpListingExpDate.CustomFormat = "";
            this.dtpListingExpDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpListingExpDate.Location = new System.Drawing.Point(800, 280);
            this.dtpListingExpDate.Name = "dtpListingExpDate";
            this.dtpListingExpDate.Size = new System.Drawing.Size(85, 20);
            this.dtpListingExpDate.TabIndex = 40;
            this.dtpListingExpDate.Visible = false;
            // 
            // label56
            // 
            this.label56.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label56.Location = new System.Drawing.Point(640, 312);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(104, 16);
            this.label56.TabIndex = 69;
            this.label56.Text = "List. Exp. Date:";
            // 
            // cbListed
            // 
            this.cbListed.Items.AddRange(new object[] {
            "Yes",
            "No",
            "?"});
            this.cbListed.Location = new System.Drawing.Point(576, 328);
            this.cbListed.Name = "cbListed";
            this.cbListed.Size = new System.Drawing.Size(56, 21);
            this.cbListed.TabIndex = 39;
            this.cbListed.SelectedIndexChanged += new System.EventHandler(this.cbListed_SelectedIndexChanged);
            // 
            // cbPmntsCurrnet
            // 
            this.cbPmntsCurrnet.Items.AddRange(new object[] {
            "?",
            "Yes",
            "No"});
            this.cbPmntsCurrnet.Location = new System.Drawing.Point(88, 328);
            this.cbPmntsCurrnet.Name = "cbPmntsCurrnet";
            this.cbPmntsCurrnet.Size = new System.Drawing.Size(104, 21);
            this.cbPmntsCurrnet.TabIndex = 34;
            // 
            // cbInForecl
            // 
            this.cbInForecl.Items.AddRange(new object[] {
            "?",
            "Yes",
            "No"});
            this.cbInForecl.Location = new System.Drawing.Point(392, 328);
            this.cbInForecl.Name = "cbInForecl";
            this.cbInForecl.Size = new System.Drawing.Size(72, 21);
            this.cbInForecl.TabIndex = 37;
            this.cbInForecl.SelectedIndexChanged += new System.EventHandler(this.cbInForecl_SelectedIndexChanged);
            // 
            // dtpForSaleDate
            // 
            this.dtpForSaleDate.CustomFormat = "MMMM ,DD YYYY";
            this.dtpForSaleDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpForSaleDate.Location = new System.Drawing.Point(800, 256);
            this.dtpForSaleDate.Name = "dtpForSaleDate";
            this.dtpForSaleDate.Size = new System.Drawing.Size(85, 20);
            this.dtpForSaleDate.TabIndex = 38;
            this.dtpForSaleDate.Visible = false;
            // 
            // label53
            // 
            this.label53.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label53.Location = new System.Drawing.Point(472, 312);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(104, 16);
            this.label53.TabIndex = 62;
            this.label53.Text = "For. Sale Date:";
            // 
            // label52
            // 
            this.label52.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label52.Location = new System.Drawing.Point(296, 312);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(96, 16);
            this.label52.TabIndex = 61;
            this.label52.Text = "Reins. Amnt:";
            // 
            // label51
            // 
            this.label51.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label51.Location = new System.Drawing.Point(392, 312);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(72, 16);
            this.label51.TabIndex = 60;
            this.label51.Text = "In Forecl?";
            // 
            // label50
            // 
            this.label50.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label50.Location = new System.Drawing.Point(88, 312);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(112, 16);
            this.label50.TabIndex = 59;
            this.label50.Text = "Pmnts Current?";
            // 
            // label49
            // 
            this.label49.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label49.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label49.Location = new System.Drawing.Point(560, 256);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(152, 23);
            this.label49.TabIndex = 57;
            this.label49.Text = "TOTAL MONTHLY:";
            // 
            // label48
            // 
            this.label48.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label48.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label48.Location = new System.Drawing.Point(560, 208);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(112, 23);
            this.label48.TabIndex = 55;
            this.label48.Text = "TOTAL OWED:";
            // 
            // label47
            // 
            this.label47.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label47.Location = new System.Drawing.Point(432, 256);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(80, 23);
            this.label47.TabIndex = 53;
            this.label47.Text = "Other Liens:";
            // 
            // label44
            // 
            this.label44.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label44.Location = new System.Drawing.Point(328, 256);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(96, 23);
            this.label44.TabIndex = 51;
            this.label44.Text = "Mo. Payment:";
            // 
            // tb3IntRate
            // 
            this.tb3IntRate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb3IntRate.Location = new System.Drawing.Point(168, 272);
            this.tb3IntRate.Name = "tb3IntRate";
            this.tb3IntRate.Size = new System.Drawing.Size(32, 20);
            this.tb3IntRate.TabIndex = 30;
            // 
            // label46
            // 
            this.label46.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label46.Location = new System.Drawing.Point(88, 256);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(48, 23);
            this.label46.TabIndex = 47;
            this.label46.Text = "3rd Bal.:";
            // 
            // label43
            // 
            this.label43.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label43.Location = new System.Drawing.Point(432, 208);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(56, 23);
            this.label43.TabIndex = 45;
            this.label43.Text = "HOA/mo.:";
            // 
            // label40
            // 
            this.label40.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label40.Location = new System.Drawing.Point(328, 208);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(96, 16);
            this.label40.TabIndex = 43;
            this.label40.Text = "Mo. Payment:";
            // 
            // tb2IntRate
            // 
            this.tb2IntRate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb2IntRate.Location = new System.Drawing.Point(168, 224);
            this.tb2IntRate.Name = "tb2IntRate";
            this.tb2IntRate.Size = new System.Drawing.Size(32, 20);
            this.tb2IntRate.TabIndex = 25;
            // 
            // label42
            // 
            this.label42.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label42.Location = new System.Drawing.Point(88, 208);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(56, 23);
            this.label42.TabIndex = 39;
            this.label42.Text = "2nd Bal.:";
            // 
            // label39
            // 
            this.label39.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label39.Location = new System.Drawing.Point(432, 160);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(96, 23);
            this.label39.TabIndex = 37;
            this.label39.Text = "Tax&&Ins Incl.:";
            // 
            // cb1Type
            // 
            this.cb1Type.Items.AddRange(new object[] {
            "?",
            "Con-Fixed",
            "Con-ARM",
            "Con-3/1ARM",
            "Con-5/1ARM",
            "FHA Fix",
            "FHA ARM",
            "FHA-3/1ARM",
            "FHA-5/1ARM",
            "VA Fix",
            "VA ARM",
            "VA-3/1ARM",
            "VA-5/1ARM"});
            this.cb1Type.Location = new System.Drawing.Point(224, 176);
            this.cb1Type.Name = "cb1Type";
            this.cb1Type.Size = new System.Drawing.Size(88, 21);
            this.cb1Type.TabIndex = 20;
            // 
            // label38
            // 
            this.label38.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label38.Location = new System.Drawing.Point(224, 160);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(48, 23);
            this.label38.TabIndex = 35;
            this.label38.Text = "Type:";
            // 
            // label37
            // 
            this.label37.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label37.Location = new System.Drawing.Point(328, 160);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(96, 23);
            this.label37.TabIndex = 33;
            this.label37.Text = "Mo. Payment:";
            // 
            // label36
            // 
            this.label36.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label36.Location = new System.Drawing.Point(200, 176);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(16, 23);
            this.label36.TabIndex = 32;
            this.label36.Text = "%";
            this.label36.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tb1IntRate
            // 
            this.tb1IntRate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb1IntRate.Location = new System.Drawing.Point(168, 176);
            this.tb1IntRate.Name = "tb1IntRate";
            this.tb1IntRate.Size = new System.Drawing.Size(32, 20);
            this.tb1IntRate.TabIndex = 19;
            // 
            // label35
            // 
            this.label35.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label35.Location = new System.Drawing.Point(168, 160);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(32, 23);
            this.label35.TabIndex = 30;
            this.label35.Text = "I.R.:";
            // 
            // label34
            // 
            this.label34.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label34.Location = new System.Drawing.Point(88, 160);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(48, 23);
            this.label34.TabIndex = 28;
            this.label34.Text = "1st Bal.:";
            // 
            // label33
            // 
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label33.Location = new System.Drawing.Point(8, 144);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(59, 23);
            this.label33.TabIndex = 27;
            this.label33.Text = "OWE:";
            // 
            // cbSource
            // 
            this.cbSource.Items.AddRange(new object[] {
            "?",
            "RefiAppraisal",
            "Appraisal",
            "RE Agent",
            "Neighbors",
            "Guess"});
            this.cbSource.Location = new System.Drawing.Point(685, 80);
            this.cbSource.Name = "cbSource";
            this.cbSource.Size = new System.Drawing.Size(96, 21);
            this.cbSource.TabIndex = 17;
            // 
            // label31
            // 
            this.label31.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label31.Location = new System.Drawing.Point(685, 64);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(64, 23);
            this.label31.TabIndex = 23;
            this.label31.Text = "Source:";
            // 
            // label30
            // 
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label30.Location = new System.Drawing.Point(512, 64);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(112, 23);
            this.label30.TabIndex = 21;
            this.label30.Text = "Worth by seller:";
            // 
            // tbWhySelling
            // 
            this.tbWhySelling.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbWhySelling.Location = new System.Drawing.Point(256, 80);
            this.tbWhySelling.MaxLength = 2047;
            this.tbWhySelling.Multiline = true;
            this.tbWhySelling.Name = "tbWhySelling";
            this.tbWhySelling.Size = new System.Drawing.Size(232, 48);
            this.tbWhySelling.TabIndex = 15;
            // 
            // label29
            // 
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label29.Location = new System.Drawing.Point(256, 64);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(112, 23);
            this.label29.TabIndex = 19;
            this.label29.Text = "Why Selling:";
            // 
            // tbCondition
            // 
            this.tbCondition.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbCondition.Location = new System.Drawing.Point(8, 80);
            this.tbCondition.MaxLength = 2047;
            this.tbCondition.Multiline = true;
            this.tbCondition.Name = "tbCondition";
            this.tbCondition.Size = new System.Drawing.Size(232, 48);
            this.tbCondition.TabIndex = 14;
            // 
            // label28
            // 
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label28.Location = new System.Drawing.Point(8, 64);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(148, 23);
            this.label28.TabIndex = 17;
            this.label28.Text = "Feature / Condition:";
            // 
            // label27
            // 
            this.label27.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label27.Location = new System.Drawing.Point(576, 8);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(51, 23);
            this.label27.TabIndex = 13;
            this.label27.Text = "Pool:";
            // 
            // label26
            // 
            this.label26.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label26.Location = new System.Drawing.Point(454, 8);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(68, 23);
            this.label26.TabIndex = 11;
            this.label26.Text = "Garage:";
            // 
            // label25
            // 
            this.label25.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label25.Location = new System.Drawing.Point(385, 8);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(71, 23);
            this.label25.TabIndex = 9;
            this.label25.Text = "Year Built:";
            // 
            // label24
            // 
            this.label24.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label24.Location = new System.Drawing.Point(321, 8);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(48, 23);
            this.label24.TabIndex = 7;
            this.label24.Text = "SqFt:";
            // 
            // label23
            // 
            this.label23.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label23.Location = new System.Drawing.Point(252, 8);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(47, 23);
            this.label23.TabIndex = 5;
            this.label23.Text = "Baths:";
            // 
            // label22
            // 
            this.label22.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label22.Location = new System.Drawing.Point(159, 8);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(83, 23);
            this.label22.TabIndex = 3;
            this.label22.Text = "Bedrooms:";
            // 
            // label21
            // 
            this.label21.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label21.Location = new System.Drawing.Point(8, 8);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(40, 23);
            this.label21.TabIndex = 1;
            this.label21.Text = "Type:";
            // 
            // tbLowestPriceAcceptable
            // 
            this.tbLowestPriceAcceptable.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbLowestPriceAcceptable.Location = new System.Drawing.Point(88, 392);
            this.tbLowestPriceAcceptable.Name = "tbLowestPriceAcceptable";
            this.tbLowestPriceAcceptable.RegexErrorMessage = "Not numeric value !";
            this.tbLowestPriceAcceptable.RegularExpressionMask = "^([1-9]{1}[\\d]{0,2}(\\,[\\d]{3})*(\\.[\\d]{0,2})?|[1-9]{1}[\\d]{0,}(\\.[\\d]{0,2})?|0(\\." +
    "[\\d]{0,2})?|(\\.[\\d]{1,2})?)$";
            this.tbLowestPriceAcceptable.Size = new System.Drawing.Size(160, 20);
            this.tbLowestPriceAcceptable.TabIndex = 42;
            // 
            // readMe1
            // 
            this.readMe1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.readMe1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.readMe1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("readMe1.BackgroundImage")));
            this.readMe1.Location = new System.Drawing.Point(1286, 8);
            this.readMe1.Name = "readMe1";
            this.readMe1.Size = new System.Drawing.Size(16, 16);
            this.readMe1.TabIndex = 0;
            // 
            // tpSkipTracedInfo
            // 
            this.tpSkipTracedInfo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.tpSkipTracedInfo.Controls.Add(this.skipTracedInfoTabControl);
            this.tpSkipTracedInfo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.tpSkipTracedInfo.Location = new System.Drawing.Point(4, 28);
            this.tpSkipTracedInfo.Name = "tpSkipTracedInfo";
            this.tpSkipTracedInfo.Size = new System.Drawing.Size(1312, 651);
            this.tpSkipTracedInfo.TabIndex = 14;
            this.tpSkipTracedInfo.Text = "SkipTraced Info";
            // 
            // skipTracedInfoTabControl
            // 
            this.skipTracedInfoTabControl.Controls.Add(this.skipTracedContactInfo);
            this.skipTracedInfoTabControl.Controls.Add(this.skipTracedOtherProperty);
            this.skipTracedInfoTabControl.Controls.Add(this.skipTracedRelativesInfo);
            this.skipTracedInfoTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.skipTracedInfoTabControl.Location = new System.Drawing.Point(0, 0);
            this.skipTracedInfoTabControl.Name = "skipTracedInfoTabControl";
            this.skipTracedInfoTabControl.SelectedIndex = 0;
            this.skipTracedInfoTabControl.Size = new System.Drawing.Size(1312, 651);
            this.skipTracedInfoTabControl.TabIndex = 0;
            // 
            // skipTracedContactInfo
            // 
            this.skipTracedContactInfo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.skipTracedContactInfo.Controls.Add(this._copySTRank0InfoToContactInfoTab);
            this.skipTracedContactInfo.Controls.Add(this._skipTraceRecord);
            this.skipTracedContactInfo.Controls.Add(this.label260);
            this.skipTracedContactInfo.Controls.Add(this.label259);
            this.skipTracedContactInfo.Controls.Add(this.st_ci_fullName);
            this.skipTracedContactInfo.Controls.Add(this.st_ci_lastName);
            this.skipTracedContactInfo.Controls.Add(this.label256);
            this.skipTracedContactInfo.Controls.Add(this.label257);
            this.skipTracedContactInfo.Controls.Add(this.label258);
            this.skipTracedContactInfo.Controls.Add(this.st_ci_aliasName);
            this.skipTracedContactInfo.Controls.Add(this.st_ci_age);
            this.skipTracedContactInfo.Controls.Add(this.st_ci_firstName);
            this.skipTracedContactInfo.Controls.Add(this.st_ci_middleName);
            this.skipTracedContactInfo.Controls.Add(this.st_ci_email2);
            this.skipTracedContactInfo.Controls.Add(this.label255);
            this.skipTracedContactInfo.Controls.Add(this.st_ci_email1);
            this.skipTracedContactInfo.Controls.Add(this.label248);
            this.skipTracedContactInfo.Controls.Add(this.st_ci_rp4);
            this.skipTracedContactInfo.Controls.Add(this.label254);
            this.skipTracedContactInfo.Controls.Add(this.st_ci_rp3);
            this.skipTracedContactInfo.Controls.Add(this.label253);
            this.skipTracedContactInfo.Controls.Add(this.st_ci_mp2);
            this.skipTracedContactInfo.Controls.Add(this.label252);
            this.skipTracedContactInfo.Controls.Add(this.st_ci_mp1);
            this.skipTracedContactInfo.Controls.Add(this.label251);
            this.skipTracedContactInfo.Controls.Add(this.st_ci_mp0);
            this.skipTracedContactInfo.Controls.Add(this.label249);
            this.skipTracedContactInfo.Controls.Add(this.st_ci_email0);
            this.skipTracedContactInfo.Controls.Add(this.label250);
            this.skipTracedContactInfo.Controls.Add(this.st_ci_a2_lastSeen);
            this.skipTracedContactInfo.Controls.Add(this.st_ci_a1_lastSeen);
            this.skipTracedContactInfo.Controls.Add(this.st_ci_a0_lastSeen);
            this.skipTracedContactInfo.Controls.Add(this.label247);
            this.skipTracedContactInfo.Controls.Add(this.st_ci_a2_firstSeen);
            this.skipTracedContactInfo.Controls.Add(this.st_ci_a1_firstSeen);
            this.skipTracedContactInfo.Controls.Add(this.st_ci_a0_firstSeen);
            this.skipTracedContactInfo.Controls.Add(this.label246);
            this.skipTracedContactInfo.Controls.Add(this.st_ci_a2_state);
            this.skipTracedContactInfo.Controls.Add(this.st_ci_a1_state);
            this.skipTracedContactInfo.Controls.Add(this.st_ci_a0_state);
            this.skipTracedContactInfo.Controls.Add(this.label239);
            this.skipTracedContactInfo.Controls.Add(this.label240);
            this.skipTracedContactInfo.Controls.Add(this.label241);
            this.skipTracedContactInfo.Controls.Add(this.label242);
            this.skipTracedContactInfo.Controls.Add(this.st_ci_a2_city);
            this.skipTracedContactInfo.Controls.Add(this.st_ci_a1_city);
            this.skipTracedContactInfo.Controls.Add(this.st_ci_a0_city);
            this.skipTracedContactInfo.Controls.Add(this.st_ci_a2_zip);
            this.skipTracedContactInfo.Controls.Add(this.st_ci_a2_address);
            this.skipTracedContactInfo.Controls.Add(this.st_ci_a1_zip);
            this.skipTracedContactInfo.Controls.Add(this.st_ci_a1_address);
            this.skipTracedContactInfo.Controls.Add(this.st_ci_a0_zip);
            this.skipTracedContactInfo.Controls.Add(this.st_ci_a0_address);
            this.skipTracedContactInfo.Controls.Add(this.label243);
            this.skipTracedContactInfo.Controls.Add(this.label244);
            this.skipTracedContactInfo.Controls.Add(this.label245);
            this.skipTracedContactInfo.Controls.Add(this.label238);
            this.skipTracedContactInfo.Controls.Add(this.label237);
            this.skipTracedContactInfo.Controls.Add(this.label92);
            this.skipTracedContactInfo.Location = new System.Drawing.Point(4, 28);
            this.skipTracedContactInfo.Name = "skipTracedContactInfo";
            this.skipTracedContactInfo.Padding = new System.Windows.Forms.Padding(3);
            this.skipTracedContactInfo.Size = new System.Drawing.Size(1304, 619);
            this.skipTracedContactInfo.TabIndex = 0;
            this.skipTracedContactInfo.Text = "Contact Info";
            // 
            // _copySTRank0InfoToContactInfoTab
            // 
            this._copySTRank0InfoToContactInfoTab.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._copySTRank0InfoToContactInfoTab.Location = new System.Drawing.Point(763, 195);
            this._copySTRank0InfoToContactInfoTab.Name = "_copySTRank0InfoToContactInfoTab";
            this._copySTRank0InfoToContactInfoTab.Size = new System.Drawing.Size(133, 34);
            this._copySTRank0InfoToContactInfoTab.TabIndex = 147;
            this._copySTRank0InfoToContactInfoTab.Text = "Copy Address to Last Known Address";
            // 
            // _skipTraceRecord
            // 
            this._skipTraceRecord.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._skipTraceRecord.Location = new System.Drawing.Point(763, 49);
            this._skipTraceRecord.Name = "_skipTraceRecord";
            this._skipTraceRecord.Size = new System.Drawing.Size(133, 23);
            this._skipTraceRecord.TabIndex = 146;
            this._skipTraceRecord.Text = "Skip Trace Contact";
            this._skipTraceRecord.Click += new System.EventHandler(this._skipTraceRecord_Click);
            // 
            // label260
            // 
            this.label260.AutoSize = true;
            this.label260.ForeColor = System.Drawing.Color.Red;
            this.label260.Location = new System.Drawing.Point(82, 132);
            this.label260.Name = "label260";
            this.label260.Size = new System.Drawing.Size(500, 19);
            this.label260.TabIndex = 145;
            this.label260.Text = "*ST stands for SkipTraced; Rank 0 is the most likely to be accurate";
            this.label260.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // label259
            // 
            this.label259.AutoSize = true;
            this.label259.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label259.Location = new System.Drawing.Point(475, 32);
            this.label259.Name = "label259";
            this.label259.Size = new System.Drawing.Size(39, 19);
            this.label259.TabIndex = 144;
            this.label259.Text = "Full:";
            this.label259.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // st_ci_fullName
            // 
            this.st_ci_fullName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.st_ci_fullName.Location = new System.Drawing.Point(475, 51);
            this.st_ci_fullName.Name = "st_ci_fullName";
            this.st_ci_fullName.Size = new System.Drawing.Size(285, 27);
            this.st_ci_fullName.TabIndex = 141;
            // 
            // st_ci_lastName
            // 
            this.st_ci_lastName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.st_ci_lastName.Location = new System.Drawing.Point(250, 51);
            this.st_ci_lastName.Name = "st_ci_lastName";
            this.st_ci_lastName.Size = new System.Drawing.Size(139, 27);
            this.st_ci_lastName.TabIndex = 130;
            // 
            // label256
            // 
            this.label256.AutoSize = true;
            this.label256.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label256.Location = new System.Drawing.Point(250, 32);
            this.label256.Name = "label256";
            this.label256.Size = new System.Drawing.Size(41, 19);
            this.label256.TabIndex = 140;
            this.label256.Text = "Last:";
            this.label256.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // label257
            // 
            this.label257.AutoSize = true;
            this.label257.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label257.Location = new System.Drawing.Point(386, 32);
            this.label257.Name = "label257";
            this.label257.Size = new System.Drawing.Size(67, 19);
            this.label257.TabIndex = 139;
            this.label257.Text = "Middle:";
            this.label257.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // label258
            // 
            this.label258.AutoSize = true;
            this.label258.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label258.Location = new System.Drawing.Point(163, 32);
            this.label258.Name = "label258";
            this.label258.Size = new System.Drawing.Size(40, 19);
            this.label258.TabIndex = 138;
            this.label258.Text = "First:";
            this.label258.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // st_ci_aliasName
            // 
            this.st_ci_aliasName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.st_ci_aliasName.Location = new System.Drawing.Point(163, 99);
            this.st_ci_aliasName.Name = "st_ci_aliasName";
            this.st_ci_aliasName.Size = new System.Drawing.Size(273, 27);
            this.st_ci_aliasName.TabIndex = 135;
            // 
            // st_ci_age
            // 
            this.st_ci_age.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.st_ci_age.Location = new System.Drawing.Point(163, 75);
            this.st_ci_age.Name = "st_ci_age";
            this.st_ci_age.Size = new System.Drawing.Size(40, 27);
            this.st_ci_age.TabIndex = 132;
            // 
            // st_ci_firstName
            // 
            this.st_ci_firstName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.st_ci_firstName.Location = new System.Drawing.Point(163, 51);
            this.st_ci_firstName.Name = "st_ci_firstName";
            this.st_ci_firstName.Size = new System.Drawing.Size(88, 27);
            this.st_ci_firstName.TabIndex = 129;
            // 
            // st_ci_middleName
            // 
            this.st_ci_middleName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.st_ci_middleName.Location = new System.Drawing.Point(388, 51);
            this.st_ci_middleName.Name = "st_ci_middleName";
            this.st_ci_middleName.Size = new System.Drawing.Size(88, 27);
            this.st_ci_middleName.TabIndex = 131;
            // 
            // st_ci_email2
            // 
            this.st_ci_email2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.st_ci_email2.Location = new System.Drawing.Point(482, 349);
            this.st_ci_email2.Name = "st_ci_email2";
            this.st_ci_email2.Size = new System.Drawing.Size(267, 27);
            this.st_ci_email2.TabIndex = 128;
            // 
            // label255
            // 
            this.label255.AutoSize = true;
            this.label255.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label255.Location = new System.Drawing.Point(380, 351);
            this.label255.Name = "label255";
            this.label255.Size = new System.Drawing.Size(142, 19);
            this.label255.TabIndex = 127;
            this.label255.Text = "ST Email - Rank 2:";
            // 
            // st_ci_email1
            // 
            this.st_ci_email1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.st_ci_email1.Location = new System.Drawing.Point(482, 325);
            this.st_ci_email1.Name = "st_ci_email1";
            this.st_ci_email1.Size = new System.Drawing.Size(267, 27);
            this.st_ci_email1.TabIndex = 126;
            // 
            // label248
            // 
            this.label248.AutoSize = true;
            this.label248.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label248.Location = new System.Drawing.Point(380, 327);
            this.label248.Name = "label248";
            this.label248.Size = new System.Drawing.Size(142, 19);
            this.label248.TabIndex = 125;
            this.label248.Text = "ST Email - Rank 1:";
            // 
            // st_ci_rp4
            // 
            this.st_ci_rp4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.st_ci_rp4.Location = new System.Drawing.Point(163, 397);
            this.st_ci_rp4.Name = "st_ci_rp4";
            this.st_ci_rp4.Size = new System.Drawing.Size(145, 27);
            this.st_ci_rp4.TabIndex = 124;
            // 
            // label254
            // 
            this.label254.AutoSize = true;
            this.label254.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label254.Location = new System.Drawing.Point(2, 399);
            this.label254.Name = "label254";
            this.label254.Size = new System.Drawing.Size(235, 19);
            this.label254.TabIndex = 123;
            this.label254.Text = "ST Residential Phone - Rank 1:";
            // 
            // st_ci_rp3
            // 
            this.st_ci_rp3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.st_ci_rp3.Location = new System.Drawing.Point(163, 373);
            this.st_ci_rp3.Name = "st_ci_rp3";
            this.st_ci_rp3.Size = new System.Drawing.Size(145, 27);
            this.st_ci_rp3.TabIndex = 122;
            // 
            // label253
            // 
            this.label253.AutoSize = true;
            this.label253.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label253.Location = new System.Drawing.Point(2, 375);
            this.label253.Name = "label253";
            this.label253.Size = new System.Drawing.Size(235, 19);
            this.label253.TabIndex = 121;
            this.label253.Text = "ST Residential Phone - Rank 0:";
            // 
            // st_ci_mp2
            // 
            this.st_ci_mp2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.st_ci_mp2.Location = new System.Drawing.Point(163, 349);
            this.st_ci_mp2.Name = "st_ci_mp2";
            this.st_ci_mp2.Size = new System.Drawing.Size(145, 27);
            this.st_ci_mp2.TabIndex = 120;
            // 
            // label252
            // 
            this.label252.AutoSize = true;
            this.label252.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label252.Location = new System.Drawing.Point(23, 351);
            this.label252.Name = "label252";
            this.label252.Size = new System.Drawing.Size(205, 19);
            this.label252.TabIndex = 119;
            this.label252.Text = "ST Mobile Phone - Rank 2:";
            // 
            // st_ci_mp1
            // 
            this.st_ci_mp1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.st_ci_mp1.Location = new System.Drawing.Point(163, 325);
            this.st_ci_mp1.Name = "st_ci_mp1";
            this.st_ci_mp1.Size = new System.Drawing.Size(145, 27);
            this.st_ci_mp1.TabIndex = 118;
            // 
            // label251
            // 
            this.label251.AutoSize = true;
            this.label251.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label251.Location = new System.Drawing.Point(23, 327);
            this.label251.Name = "label251";
            this.label251.Size = new System.Drawing.Size(205, 19);
            this.label251.TabIndex = 117;
            this.label251.Text = "ST Mobile Phone - Rank 1:";
            // 
            // st_ci_mp0
            // 
            this.st_ci_mp0.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.st_ci_mp0.Location = new System.Drawing.Point(163, 301);
            this.st_ci_mp0.Name = "st_ci_mp0";
            this.st_ci_mp0.Size = new System.Drawing.Size(145, 27);
            this.st_ci_mp0.TabIndex = 114;
            // 
            // label249
            // 
            this.label249.AutoSize = true;
            this.label249.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label249.Location = new System.Drawing.Point(23, 303);
            this.label249.Name = "label249";
            this.label249.Size = new System.Drawing.Size(205, 19);
            this.label249.TabIndex = 113;
            this.label249.Text = "ST Mobile Phone - Rank 0:";
            // 
            // st_ci_email0
            // 
            this.st_ci_email0.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.st_ci_email0.Location = new System.Drawing.Point(482, 301);
            this.st_ci_email0.Name = "st_ci_email0";
            this.st_ci_email0.Size = new System.Drawing.Size(267, 27);
            this.st_ci_email0.TabIndex = 112;
            // 
            // label250
            // 
            this.label250.AutoSize = true;
            this.label250.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label250.Location = new System.Drawing.Point(380, 303);
            this.label250.Name = "label250";
            this.label250.Size = new System.Drawing.Size(142, 19);
            this.label250.TabIndex = 111;
            this.label250.Text = "ST Email - Rank 0:";
            // 
            // st_ci_a2_lastSeen
            // 
            this.st_ci_a2_lastSeen.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.st_ci_a2_lastSeen.Location = new System.Drawing.Point(680, 250);
            this.st_ci_a2_lastSeen.Name = "st_ci_a2_lastSeen";
            this.st_ci_a2_lastSeen.Size = new System.Drawing.Size(80, 27);
            this.st_ci_a2_lastSeen.TabIndex = 109;
            // 
            // st_ci_a1_lastSeen
            // 
            this.st_ci_a1_lastSeen.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.st_ci_a1_lastSeen.Location = new System.Drawing.Point(680, 226);
            this.st_ci_a1_lastSeen.Name = "st_ci_a1_lastSeen";
            this.st_ci_a1_lastSeen.Size = new System.Drawing.Size(80, 27);
            this.st_ci_a1_lastSeen.TabIndex = 108;
            // 
            // st_ci_a0_lastSeen
            // 
            this.st_ci_a0_lastSeen.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.st_ci_a0_lastSeen.Location = new System.Drawing.Point(680, 202);
            this.st_ci_a0_lastSeen.Name = "st_ci_a0_lastSeen";
            this.st_ci_a0_lastSeen.Size = new System.Drawing.Size(80, 27);
            this.st_ci_a0_lastSeen.TabIndex = 107;
            // 
            // label247
            // 
            this.label247.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label247.Location = new System.Drawing.Point(680, 180);
            this.label247.Name = "label247";
            this.label247.Size = new System.Drawing.Size(64, 16);
            this.label247.TabIndex = 110;
            this.label247.Text = "Last Seen:";
            this.label247.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // st_ci_a2_firstSeen
            // 
            this.st_ci_a2_firstSeen.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.st_ci_a2_firstSeen.Location = new System.Drawing.Point(601, 250);
            this.st_ci_a2_firstSeen.Name = "st_ci_a2_firstSeen";
            this.st_ci_a2_firstSeen.Size = new System.Drawing.Size(80, 27);
            this.st_ci_a2_firstSeen.TabIndex = 105;
            // 
            // st_ci_a1_firstSeen
            // 
            this.st_ci_a1_firstSeen.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.st_ci_a1_firstSeen.Location = new System.Drawing.Point(601, 226);
            this.st_ci_a1_firstSeen.Name = "st_ci_a1_firstSeen";
            this.st_ci_a1_firstSeen.Size = new System.Drawing.Size(80, 27);
            this.st_ci_a1_firstSeen.TabIndex = 104;
            // 
            // st_ci_a0_firstSeen
            // 
            this.st_ci_a0_firstSeen.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.st_ci_a0_firstSeen.Location = new System.Drawing.Point(601, 202);
            this.st_ci_a0_firstSeen.Name = "st_ci_a0_firstSeen";
            this.st_ci_a0_firstSeen.Size = new System.Drawing.Size(80, 27);
            this.st_ci_a0_firstSeen.TabIndex = 103;
            // 
            // label246
            // 
            this.label246.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label246.Location = new System.Drawing.Point(601, 180);
            this.label246.Name = "label246";
            this.label246.Size = new System.Drawing.Size(64, 16);
            this.label246.TabIndex = 106;
            this.label246.Text = "First Seen:";
            this.label246.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // st_ci_a2_state
            // 
            this.st_ci_a2_state.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.st_ci_a2_state.Location = new System.Drawing.Point(475, 250);
            this.st_ci_a2_state.Name = "st_ci_a2_state";
            this.st_ci_a2_state.Size = new System.Drawing.Size(80, 27);
            this.st_ci_a2_state.TabIndex = 95;
            // 
            // st_ci_a1_state
            // 
            this.st_ci_a1_state.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.st_ci_a1_state.Location = new System.Drawing.Point(475, 226);
            this.st_ci_a1_state.Name = "st_ci_a1_state";
            this.st_ci_a1_state.Size = new System.Drawing.Size(80, 27);
            this.st_ci_a1_state.TabIndex = 90;
            // 
            // st_ci_a0_state
            // 
            this.st_ci_a0_state.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.st_ci_a0_state.Location = new System.Drawing.Point(475, 202);
            this.st_ci_a0_state.Name = "st_ci_a0_state";
            this.st_ci_a0_state.Size = new System.Drawing.Size(80, 27);
            this.st_ci_a0_state.TabIndex = 85;
            // 
            // label239
            // 
            this.label239.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label239.Location = new System.Drawing.Point(475, 180);
            this.label239.Name = "label239";
            this.label239.Size = new System.Drawing.Size(64, 16);
            this.label239.TabIndex = 102;
            this.label239.Text = "State:";
            this.label239.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // label240
            // 
            this.label240.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label240.Location = new System.Drawing.Point(554, 180);
            this.label240.Name = "label240";
            this.label240.Size = new System.Drawing.Size(32, 16);
            this.label240.TabIndex = 101;
            this.label240.Text = "ZIP:";
            this.label240.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // label241
            // 
            this.label241.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label241.Location = new System.Drawing.Point(388, 180);
            this.label241.Name = "label241";
            this.label241.Size = new System.Drawing.Size(64, 16);
            this.label241.TabIndex = 100;
            this.label241.Text = "City:";
            this.label241.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // label242
            // 
            this.label242.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label242.Location = new System.Drawing.Point(171, 180);
            this.label242.Name = "label242";
            this.label242.Size = new System.Drawing.Size(64, 16);
            this.label242.TabIndex = 99;
            this.label242.Text = "Address:";
            this.label242.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // st_ci_a2_city
            // 
            this.st_ci_a2_city.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.st_ci_a2_city.Location = new System.Drawing.Point(388, 250);
            this.st_ci_a2_city.Name = "st_ci_a2_city";
            this.st_ci_a2_city.Size = new System.Drawing.Size(88, 27);
            this.st_ci_a2_city.TabIndex = 94;
            // 
            // st_ci_a1_city
            // 
            this.st_ci_a1_city.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.st_ci_a1_city.Location = new System.Drawing.Point(388, 226);
            this.st_ci_a1_city.Name = "st_ci_a1_city";
            this.st_ci_a1_city.Size = new System.Drawing.Size(88, 27);
            this.st_ci_a1_city.TabIndex = 89;
            // 
            // st_ci_a0_city
            // 
            this.st_ci_a0_city.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.st_ci_a0_city.Location = new System.Drawing.Point(388, 202);
            this.st_ci_a0_city.Name = "st_ci_a0_city";
            this.st_ci_a0_city.Size = new System.Drawing.Size(88, 27);
            this.st_ci_a0_city.TabIndex = 84;
            // 
            // st_ci_a2_zip
            // 
            this.st_ci_a2_zip.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.st_ci_a2_zip.Location = new System.Drawing.Point(554, 250);
            this.st_ci_a2_zip.Name = "st_ci_a2_zip";
            this.st_ci_a2_zip.Size = new System.Drawing.Size(48, 27);
            this.st_ci_a2_zip.TabIndex = 97;
            // 
            // st_ci_a2_address
            // 
            this.st_ci_a2_address.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.st_ci_a2_address.Location = new System.Drawing.Point(163, 250);
            this.st_ci_a2_address.Name = "st_ci_a2_address";
            this.st_ci_a2_address.Size = new System.Drawing.Size(226, 27);
            this.st_ci_a2_address.TabIndex = 93;
            // 
            // st_ci_a1_zip
            // 
            this.st_ci_a1_zip.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.st_ci_a1_zip.Location = new System.Drawing.Point(554, 226);
            this.st_ci_a1_zip.Name = "st_ci_a1_zip";
            this.st_ci_a1_zip.Size = new System.Drawing.Size(48, 27);
            this.st_ci_a1_zip.TabIndex = 92;
            // 
            // st_ci_a1_address
            // 
            this.st_ci_a1_address.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.st_ci_a1_address.Location = new System.Drawing.Point(163, 226);
            this.st_ci_a1_address.Name = "st_ci_a1_address";
            this.st_ci_a1_address.Size = new System.Drawing.Size(226, 27);
            this.st_ci_a1_address.TabIndex = 88;
            // 
            // st_ci_a0_zip
            // 
            this.st_ci_a0_zip.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.st_ci_a0_zip.Location = new System.Drawing.Point(554, 202);
            this.st_ci_a0_zip.Name = "st_ci_a0_zip";
            this.st_ci_a0_zip.Size = new System.Drawing.Size(48, 27);
            this.st_ci_a0_zip.TabIndex = 87;
            // 
            // st_ci_a0_address
            // 
            this.st_ci_a0_address.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.st_ci_a0_address.Location = new System.Drawing.Point(163, 202);
            this.st_ci_a0_address.Name = "st_ci_a0_address";
            this.st_ci_a0_address.Size = new System.Drawing.Size(226, 27);
            this.st_ci_a0_address.TabIndex = 83;
            // 
            // label243
            // 
            this.label243.AutoSize = true;
            this.label243.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label243.Location = new System.Drawing.Point(53, 252);
            this.label243.Name = "label243";
            this.label243.Size = new System.Drawing.Size(160, 19);
            this.label243.TabIndex = 82;
            this.label243.Text = "ST Address - Rank 2:";
            // 
            // label244
            // 
            this.label244.AutoSize = true;
            this.label244.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label244.Location = new System.Drawing.Point(53, 228);
            this.label244.Name = "label244";
            this.label244.Size = new System.Drawing.Size(160, 19);
            this.label244.TabIndex = 81;
            this.label244.Text = "ST Address - Rank 1:";
            // 
            // label245
            // 
            this.label245.AutoSize = true;
            this.label245.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label245.Location = new System.Drawing.Point(53, 204);
            this.label245.Name = "label245";
            this.label245.Size = new System.Drawing.Size(160, 19);
            this.label245.TabIndex = 80;
            this.label245.Text = "ST Address - Rank 0:";
            // 
            // label238
            // 
            this.label238.AutoSize = true;
            this.label238.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label238.Location = new System.Drawing.Point(82, 101);
            this.label238.Name = "label238";
            this.label238.Size = new System.Drawing.Size(80, 13);
            this.label238.TabIndex = 2;
            this.label238.Text = "ST Alias Name:";
            // 
            // label237
            // 
            this.label237.AutoSize = true;
            this.label237.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label237.Location = new System.Drawing.Point(116, 77);
            this.label237.Name = "label237";
            this.label237.Size = new System.Drawing.Size(46, 13);
            this.label237.TabIndex = 1;
            this.label237.Text = "ST Age:";
            // 
            // label92
            // 
            this.label92.AutoSize = true;
            this.label92.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label92.Location = new System.Drawing.Point(88, 53);
            this.label92.Name = "label92";
            this.label92.Size = new System.Drawing.Size(74, 13);
            this.label92.TabIndex = 0;
            this.label92.Text = "ST Full Name:";
            // 
            // skipTracedOtherProperty
            // 
            this.skipTracedOtherProperty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.skipTracedOtherProperty.Controls.Add(this.st_op_bedrooms4);
            this.skipTracedOtherProperty.Controls.Add(this.st_op_bathrooms4);
            this.skipTracedOtherProperty.Controls.Add(this.st_op_yearBuilt4);
            this.skipTracedOtherProperty.Controls.Add(this.st_op_buildingSqFt4);
            this.skipTracedOtherProperty.Controls.Add(this.st_op_lotSqFt4);
            this.skipTracedOtherProperty.Controls.Add(this.st_op_parcelNumber4);
            this.skipTracedOtherProperty.Controls.Add(this.st_op_useCode4);
            this.skipTracedOtherProperty.Controls.Add(this.st_op_county4);
            this.skipTracedOtherProperty.Controls.Add(this.st_op_address4);
            this.skipTracedOtherProperty.Controls.Add(this.label288);
            this.skipTracedOtherProperty.Controls.Add(this.label289);
            this.skipTracedOtherProperty.Controls.Add(this.label290);
            this.skipTracedOtherProperty.Controls.Add(this.label291);
            this.skipTracedOtherProperty.Controls.Add(this.label292);
            this.skipTracedOtherProperty.Controls.Add(this.label293);
            this.skipTracedOtherProperty.Controls.Add(this.label294);
            this.skipTracedOtherProperty.Controls.Add(this.label295);
            this.skipTracedOtherProperty.Controls.Add(this.st_op_lv4);
            this.skipTracedOtherProperty.Controls.Add(this.label296);
            this.skipTracedOtherProperty.Controls.Add(this.st_op_bedrooms3);
            this.skipTracedOtherProperty.Controls.Add(this.st_op_bathrooms3);
            this.skipTracedOtherProperty.Controls.Add(this.st_op_yearBuilt3);
            this.skipTracedOtherProperty.Controls.Add(this.st_op_buildingSqFt3);
            this.skipTracedOtherProperty.Controls.Add(this.st_op_lotSqFt3);
            this.skipTracedOtherProperty.Controls.Add(this.st_op_parcelNumber3);
            this.skipTracedOtherProperty.Controls.Add(this.st_op_useCode3);
            this.skipTracedOtherProperty.Controls.Add(this.st_op_county3);
            this.skipTracedOtherProperty.Controls.Add(this.st_op_address3);
            this.skipTracedOtherProperty.Controls.Add(this.label279);
            this.skipTracedOtherProperty.Controls.Add(this.label280);
            this.skipTracedOtherProperty.Controls.Add(this.label281);
            this.skipTracedOtherProperty.Controls.Add(this.label282);
            this.skipTracedOtherProperty.Controls.Add(this.label283);
            this.skipTracedOtherProperty.Controls.Add(this.label284);
            this.skipTracedOtherProperty.Controls.Add(this.label285);
            this.skipTracedOtherProperty.Controls.Add(this.label286);
            this.skipTracedOtherProperty.Controls.Add(this.st_op_lv3);
            this.skipTracedOtherProperty.Controls.Add(this.label287);
            this.skipTracedOtherProperty.Controls.Add(this.st_op_bedrooms2);
            this.skipTracedOtherProperty.Controls.Add(this.st_op_bathrooms2);
            this.skipTracedOtherProperty.Controls.Add(this.st_op_yearBuilt2);
            this.skipTracedOtherProperty.Controls.Add(this.st_op_buildingSqFt2);
            this.skipTracedOtherProperty.Controls.Add(this.st_op_lotSqFt2);
            this.skipTracedOtherProperty.Controls.Add(this.st_op_parcelNumber2);
            this.skipTracedOtherProperty.Controls.Add(this.st_op_useCode2);
            this.skipTracedOtherProperty.Controls.Add(this.st_op_county2);
            this.skipTracedOtherProperty.Controls.Add(this.st_op_address2);
            this.skipTracedOtherProperty.Controls.Add(this.label270);
            this.skipTracedOtherProperty.Controls.Add(this.label271);
            this.skipTracedOtherProperty.Controls.Add(this.label272);
            this.skipTracedOtherProperty.Controls.Add(this.label273);
            this.skipTracedOtherProperty.Controls.Add(this.label274);
            this.skipTracedOtherProperty.Controls.Add(this.label275);
            this.skipTracedOtherProperty.Controls.Add(this.label276);
            this.skipTracedOtherProperty.Controls.Add(this.label277);
            this.skipTracedOtherProperty.Controls.Add(this.st_op_lv2);
            this.skipTracedOtherProperty.Controls.Add(this.label278);
            this.skipTracedOtherProperty.Controls.Add(this.st_op_bedrooms1);
            this.skipTracedOtherProperty.Controls.Add(this.st_op_bathrooms1);
            this.skipTracedOtherProperty.Controls.Add(this.st_op_yearBuilt1);
            this.skipTracedOtherProperty.Controls.Add(this.st_op_buildingSqFt1);
            this.skipTracedOtherProperty.Controls.Add(this.st_op_lotSqFt1);
            this.skipTracedOtherProperty.Controls.Add(this.st_op_parcelNumber1);
            this.skipTracedOtherProperty.Controls.Add(this.st_op_useCode1);
            this.skipTracedOtherProperty.Controls.Add(this.st_op_county1);
            this.skipTracedOtherProperty.Controls.Add(this.st_op_address1);
            this.skipTracedOtherProperty.Controls.Add(this.label269);
            this.skipTracedOtherProperty.Controls.Add(this.label268);
            this.skipTracedOtherProperty.Controls.Add(this.label267);
            this.skipTracedOtherProperty.Controls.Add(this.label266);
            this.skipTracedOtherProperty.Controls.Add(this.label265);
            this.skipTracedOtherProperty.Controls.Add(this.label264);
            this.skipTracedOtherProperty.Controls.Add(this.label263);
            this.skipTracedOtherProperty.Controls.Add(this.label262);
            this.skipTracedOtherProperty.Controls.Add(this.st_op_lv1);
            this.skipTracedOtherProperty.Controls.Add(this.label261);
            this.skipTracedOtherProperty.Location = new System.Drawing.Point(4, 22);
            this.skipTracedOtherProperty.Name = "skipTracedOtherProperty";
            this.skipTracedOtherProperty.Padding = new System.Windows.Forms.Padding(3);
            this.skipTracedOtherProperty.Size = new System.Drawing.Size(1304, 625);
            this.skipTracedOtherProperty.TabIndex = 1;
            this.skipTracedOtherProperty.Text = "Other Property";
            // 
            // st_op_bedrooms4
            // 
            this.st_op_bedrooms4.Location = new System.Drawing.Point(748, 429);
            this.st_op_bedrooms4.Name = "st_op_bedrooms4";
            this.st_op_bedrooms4.Size = new System.Drawing.Size(45, 27);
            this.st_op_bedrooms4.TabIndex = 76;
            // 
            // st_op_bathrooms4
            // 
            this.st_op_bathrooms4.Location = new System.Drawing.Point(639, 429);
            this.st_op_bathrooms4.Name = "st_op_bathrooms4";
            this.st_op_bathrooms4.Size = new System.Drawing.Size(45, 27);
            this.st_op_bathrooms4.TabIndex = 75;
            // 
            // st_op_yearBuilt4
            // 
            this.st_op_yearBuilt4.Location = new System.Drawing.Point(525, 429);
            this.st_op_yearBuilt4.Name = "st_op_yearBuilt4";
            this.st_op_yearBuilt4.Size = new System.Drawing.Size(45, 27);
            this.st_op_yearBuilt4.TabIndex = 74;
            // 
            // st_op_buildingSqFt4
            // 
            this.st_op_buildingSqFt4.Location = new System.Drawing.Point(384, 429);
            this.st_op_buildingSqFt4.Name = "st_op_buildingSqFt4";
            this.st_op_buildingSqFt4.Size = new System.Drawing.Size(80, 27);
            this.st_op_buildingSqFt4.TabIndex = 73;
            // 
            // st_op_lotSqFt4
            // 
            this.st_op_lotSqFt4.Location = new System.Drawing.Point(229, 429);
            this.st_op_lotSqFt4.Name = "st_op_lotSqFt4";
            this.st_op_lotSqFt4.Size = new System.Drawing.Size(80, 27);
            this.st_op_lotSqFt4.TabIndex = 72;
            // 
            // st_op_parcelNumber4
            // 
            this.st_op_parcelNumber4.Location = new System.Drawing.Point(77, 429);
            this.st_op_parcelNumber4.Name = "st_op_parcelNumber4";
            this.st_op_parcelNumber4.Size = new System.Drawing.Size(96, 27);
            this.st_op_parcelNumber4.TabIndex = 71;
            // 
            // st_op_useCode4
            // 
            this.st_op_useCode4.Location = new System.Drawing.Point(683, 408);
            this.st_op_useCode4.Name = "st_op_useCode4";
            this.st_op_useCode4.Size = new System.Drawing.Size(210, 27);
            this.st_op_useCode4.TabIndex = 70;
            // 
            // st_op_county4
            // 
            this.st_op_county4.Location = new System.Drawing.Point(525, 407);
            this.st_op_county4.Name = "st_op_county4";
            this.st_op_county4.Size = new System.Drawing.Size(96, 27);
            this.st_op_county4.TabIndex = 69;
            // 
            // st_op_address4
            // 
            this.st_op_address4.Location = new System.Drawing.Point(57, 407);
            this.st_op_address4.Name = "st_op_address4";
            this.st_op_address4.Size = new System.Drawing.Size(419, 27);
            this.st_op_address4.TabIndex = 68;
            // 
            // label288
            // 
            this.label288.AutoSize = true;
            this.label288.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label288.Location = new System.Drawing.Point(627, 411);
            this.label288.Name = "label288";
            this.label288.Size = new System.Drawing.Size(54, 13);
            this.label288.TabIndex = 67;
            this.label288.Text = "UseCode:";
            // 
            // label289
            // 
            this.label289.AutoSize = true;
            this.label289.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label289.Location = new System.Drawing.Point(690, 432);
            this.label289.Name = "label289";
            this.label289.Size = new System.Drawing.Size(60, 13);
            this.label289.TabIndex = 66;
            this.label289.Text = "Bathrooms:";
            // 
            // label290
            // 
            this.label290.AutoSize = true;
            this.label290.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label290.Location = new System.Drawing.Point(576, 432);
            this.label290.Name = "label290";
            this.label290.Size = new System.Drawing.Size(57, 13);
            this.label290.TabIndex = 65;
            this.label290.Text = "Bedrooms:";
            // 
            // label291
            // 
            this.label291.AutoSize = true;
            this.label291.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label291.Location = new System.Drawing.Point(473, 432);
            this.label291.Name = "label291";
            this.label291.Size = new System.Drawing.Size(52, 13);
            this.label291.TabIndex = 64;
            this.label291.Text = "YearBuilt:";
            // 
            // label292
            // 
            this.label292.AutoSize = true;
            this.label292.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label292.Location = new System.Drawing.Point(315, 432);
            this.label292.Name = "label292";
            this.label292.Size = new System.Drawing.Size(66, 13);
            this.label292.TabIndex = 63;
            this.label292.Text = "BuildingSqft:";
            // 
            // label293
            // 
            this.label293.AutoSize = true;
            this.label293.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label293.Location = new System.Drawing.Point(179, 432);
            this.label293.Name = "label293";
            this.label293.Size = new System.Drawing.Size(44, 13);
            this.label293.TabIndex = 62;
            this.label293.Text = "LotSqft:";
            // 
            // label294
            // 
            this.label294.AutoSize = true;
            this.label294.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label294.Location = new System.Drawing.Point(3, 432);
            this.label294.Name = "label294";
            this.label294.Size = new System.Drawing.Size(77, 13);
            this.label294.TabIndex = 61;
            this.label294.Text = "ParcelNumber:";
            // 
            // label295
            // 
            this.label295.AutoSize = true;
            this.label295.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label295.Location = new System.Drawing.Point(482, 411);
            this.label295.Name = "label295";
            this.label295.Size = new System.Drawing.Size(43, 13);
            this.label295.TabIndex = 60;
            this.label295.Text = "County:";
            // 
            // st_op_lv4
            // 
            this.st_op_lv4.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader22,
            this.columnHeader23,
            this.columnHeader24,
            this.columnHeader25,
            this.columnHeader26,
            this.columnHeader27,
            this.columnHeader28});
            this.st_op_lv4.FullRowSelect = true;
            this.st_op_lv4.GridLines = true;
            this.st_op_lv4.HideSelection = false;
            this.st_op_lv4.Location = new System.Drawing.Point(6, 452);
            this.st_op_lv4.Name = "st_op_lv4";
            this.st_op_lv4.Size = new System.Drawing.Size(887, 79);
            this.st_op_lv4.TabIndex = 59;
            this.st_op_lv4.UseCompatibleStateImageBehavior = false;
            this.st_op_lv4.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader22
            // 
            this.columnHeader22.Text = "Transfer Date";
            this.columnHeader22.Width = 102;
            // 
            // columnHeader23
            // 
            this.columnHeader23.Text = "Seller";
            this.columnHeader23.Width = 123;
            // 
            // columnHeader24
            // 
            this.columnHeader24.Text = "Buyer";
            this.columnHeader24.Width = 117;
            // 
            // columnHeader25
            // 
            this.columnHeader25.Text = "Transfer Type";
            this.columnHeader25.Width = 103;
            // 
            // columnHeader26
            // 
            this.columnHeader26.Text = "Loan Value";
            this.columnHeader26.Width = 94;
            // 
            // columnHeader27
            // 
            this.columnHeader27.Text = "Lender Name";
            this.columnHeader27.Width = 225;
            // 
            // columnHeader28
            // 
            this.columnHeader28.Text = "Interest Type";
            this.columnHeader28.Width = 92;
            // 
            // label296
            // 
            this.label296.AutoSize = true;
            this.label296.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label296.Location = new System.Drawing.Point(3, 411);
            this.label296.Name = "label296";
            this.label296.Size = new System.Drawing.Size(48, 13);
            this.label296.TabIndex = 58;
            this.label296.Text = "Address:";
            // 
            // st_op_bedrooms3
            // 
            this.st_op_bedrooms3.Location = new System.Drawing.Point(748, 296);
            this.st_op_bedrooms3.Name = "st_op_bedrooms3";
            this.st_op_bedrooms3.Size = new System.Drawing.Size(45, 27);
            this.st_op_bedrooms3.TabIndex = 57;
            // 
            // st_op_bathrooms3
            // 
            this.st_op_bathrooms3.Location = new System.Drawing.Point(639, 296);
            this.st_op_bathrooms3.Name = "st_op_bathrooms3";
            this.st_op_bathrooms3.Size = new System.Drawing.Size(45, 27);
            this.st_op_bathrooms3.TabIndex = 56;
            // 
            // st_op_yearBuilt3
            // 
            this.st_op_yearBuilt3.Location = new System.Drawing.Point(525, 296);
            this.st_op_yearBuilt3.Name = "st_op_yearBuilt3";
            this.st_op_yearBuilt3.Size = new System.Drawing.Size(45, 27);
            this.st_op_yearBuilt3.TabIndex = 55;
            // 
            // st_op_buildingSqFt3
            // 
            this.st_op_buildingSqFt3.Location = new System.Drawing.Point(384, 296);
            this.st_op_buildingSqFt3.Name = "st_op_buildingSqFt3";
            this.st_op_buildingSqFt3.Size = new System.Drawing.Size(80, 27);
            this.st_op_buildingSqFt3.TabIndex = 54;
            // 
            // st_op_lotSqFt3
            // 
            this.st_op_lotSqFt3.Location = new System.Drawing.Point(229, 296);
            this.st_op_lotSqFt3.Name = "st_op_lotSqFt3";
            this.st_op_lotSqFt3.Size = new System.Drawing.Size(80, 27);
            this.st_op_lotSqFt3.TabIndex = 53;
            // 
            // st_op_parcelNumber3
            // 
            this.st_op_parcelNumber3.Location = new System.Drawing.Point(77, 296);
            this.st_op_parcelNumber3.Name = "st_op_parcelNumber3";
            this.st_op_parcelNumber3.Size = new System.Drawing.Size(96, 27);
            this.st_op_parcelNumber3.TabIndex = 52;
            // 
            // st_op_useCode3
            // 
            this.st_op_useCode3.Location = new System.Drawing.Point(683, 275);
            this.st_op_useCode3.Name = "st_op_useCode3";
            this.st_op_useCode3.Size = new System.Drawing.Size(210, 27);
            this.st_op_useCode3.TabIndex = 51;
            // 
            // st_op_county3
            // 
            this.st_op_county3.Location = new System.Drawing.Point(525, 274);
            this.st_op_county3.Name = "st_op_county3";
            this.st_op_county3.Size = new System.Drawing.Size(96, 27);
            this.st_op_county3.TabIndex = 50;
            // 
            // st_op_address3
            // 
            this.st_op_address3.Location = new System.Drawing.Point(57, 274);
            this.st_op_address3.Name = "st_op_address3";
            this.st_op_address3.Size = new System.Drawing.Size(419, 27);
            this.st_op_address3.TabIndex = 49;
            // 
            // label279
            // 
            this.label279.AutoSize = true;
            this.label279.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label279.Location = new System.Drawing.Point(627, 278);
            this.label279.Name = "label279";
            this.label279.Size = new System.Drawing.Size(54, 13);
            this.label279.TabIndex = 48;
            this.label279.Text = "UseCode:";
            // 
            // label280
            // 
            this.label280.AutoSize = true;
            this.label280.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label280.Location = new System.Drawing.Point(690, 299);
            this.label280.Name = "label280";
            this.label280.Size = new System.Drawing.Size(60, 13);
            this.label280.TabIndex = 47;
            this.label280.Text = "Bathrooms:";
            // 
            // label281
            // 
            this.label281.AutoSize = true;
            this.label281.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label281.Location = new System.Drawing.Point(576, 299);
            this.label281.Name = "label281";
            this.label281.Size = new System.Drawing.Size(57, 13);
            this.label281.TabIndex = 46;
            this.label281.Text = "Bedrooms:";
            // 
            // label282
            // 
            this.label282.AutoSize = true;
            this.label282.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label282.Location = new System.Drawing.Point(473, 299);
            this.label282.Name = "label282";
            this.label282.Size = new System.Drawing.Size(52, 13);
            this.label282.TabIndex = 45;
            this.label282.Text = "YearBuilt:";
            // 
            // label283
            // 
            this.label283.AutoSize = true;
            this.label283.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label283.Location = new System.Drawing.Point(315, 299);
            this.label283.Name = "label283";
            this.label283.Size = new System.Drawing.Size(66, 13);
            this.label283.TabIndex = 44;
            this.label283.Text = "BuildingSqft:";
            // 
            // label284
            // 
            this.label284.AutoSize = true;
            this.label284.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label284.Location = new System.Drawing.Point(179, 299);
            this.label284.Name = "label284";
            this.label284.Size = new System.Drawing.Size(44, 13);
            this.label284.TabIndex = 43;
            this.label284.Text = "LotSqft:";
            // 
            // label285
            // 
            this.label285.AutoSize = true;
            this.label285.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label285.Location = new System.Drawing.Point(3, 299);
            this.label285.Name = "label285";
            this.label285.Size = new System.Drawing.Size(77, 13);
            this.label285.TabIndex = 42;
            this.label285.Text = "ParcelNumber:";
            // 
            // label286
            // 
            this.label286.AutoSize = true;
            this.label286.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label286.Location = new System.Drawing.Point(482, 278);
            this.label286.Name = "label286";
            this.label286.Size = new System.Drawing.Size(43, 13);
            this.label286.TabIndex = 41;
            this.label286.Text = "County:";
            // 
            // st_op_lv3
            // 
            this.st_op_lv3.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader9,
            this.columnHeader10,
            this.columnHeader11,
            this.columnHeader12,
            this.columnHeader13,
            this.columnHeader15,
            this.columnHeader16});
            this.st_op_lv3.FullRowSelect = true;
            this.st_op_lv3.GridLines = true;
            this.st_op_lv3.HideSelection = false;
            this.st_op_lv3.Location = new System.Drawing.Point(6, 319);
            this.st_op_lv3.Name = "st_op_lv3";
            this.st_op_lv3.Size = new System.Drawing.Size(887, 79);
            this.st_op_lv3.TabIndex = 40;
            this.st_op_lv3.UseCompatibleStateImageBehavior = false;
            this.st_op_lv3.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader9
            // 
            this.columnHeader9.Text = "Transfer Date";
            this.columnHeader9.Width = 102;
            // 
            // columnHeader10
            // 
            this.columnHeader10.Text = "Seller";
            this.columnHeader10.Width = 123;
            // 
            // columnHeader11
            // 
            this.columnHeader11.Text = "Buyer";
            this.columnHeader11.Width = 117;
            // 
            // columnHeader12
            // 
            this.columnHeader12.Text = "Transfer Type";
            this.columnHeader12.Width = 103;
            // 
            // columnHeader13
            // 
            this.columnHeader13.Text = "Loan Value";
            this.columnHeader13.Width = 94;
            // 
            // columnHeader15
            // 
            this.columnHeader15.Text = "Lender Name";
            this.columnHeader15.Width = 225;
            // 
            // columnHeader16
            // 
            this.columnHeader16.Text = "Interest Type";
            this.columnHeader16.Width = 92;
            // 
            // label287
            // 
            this.label287.AutoSize = true;
            this.label287.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label287.Location = new System.Drawing.Point(3, 278);
            this.label287.Name = "label287";
            this.label287.Size = new System.Drawing.Size(48, 13);
            this.label287.TabIndex = 39;
            this.label287.Text = "Address:";
            // 
            // st_op_bedrooms2
            // 
            this.st_op_bedrooms2.Location = new System.Drawing.Point(748, 163);
            this.st_op_bedrooms2.Name = "st_op_bedrooms2";
            this.st_op_bedrooms2.Size = new System.Drawing.Size(45, 27);
            this.st_op_bedrooms2.TabIndex = 38;
            // 
            // st_op_bathrooms2
            // 
            this.st_op_bathrooms2.Location = new System.Drawing.Point(639, 163);
            this.st_op_bathrooms2.Name = "st_op_bathrooms2";
            this.st_op_bathrooms2.Size = new System.Drawing.Size(45, 27);
            this.st_op_bathrooms2.TabIndex = 37;
            // 
            // st_op_yearBuilt2
            // 
            this.st_op_yearBuilt2.Location = new System.Drawing.Point(525, 163);
            this.st_op_yearBuilt2.Name = "st_op_yearBuilt2";
            this.st_op_yearBuilt2.Size = new System.Drawing.Size(45, 27);
            this.st_op_yearBuilt2.TabIndex = 36;
            // 
            // st_op_buildingSqFt2
            // 
            this.st_op_buildingSqFt2.Location = new System.Drawing.Point(384, 163);
            this.st_op_buildingSqFt2.Name = "st_op_buildingSqFt2";
            this.st_op_buildingSqFt2.Size = new System.Drawing.Size(80, 27);
            this.st_op_buildingSqFt2.TabIndex = 35;
            // 
            // st_op_lotSqFt2
            // 
            this.st_op_lotSqFt2.Location = new System.Drawing.Point(229, 163);
            this.st_op_lotSqFt2.Name = "st_op_lotSqFt2";
            this.st_op_lotSqFt2.Size = new System.Drawing.Size(80, 27);
            this.st_op_lotSqFt2.TabIndex = 34;
            // 
            // st_op_parcelNumber2
            // 
            this.st_op_parcelNumber2.Location = new System.Drawing.Point(77, 163);
            this.st_op_parcelNumber2.Name = "st_op_parcelNumber2";
            this.st_op_parcelNumber2.Size = new System.Drawing.Size(96, 27);
            this.st_op_parcelNumber2.TabIndex = 33;
            // 
            // st_op_useCode2
            // 
            this.st_op_useCode2.Location = new System.Drawing.Point(683, 142);
            this.st_op_useCode2.Name = "st_op_useCode2";
            this.st_op_useCode2.Size = new System.Drawing.Size(210, 27);
            this.st_op_useCode2.TabIndex = 32;
            // 
            // st_op_county2
            // 
            this.st_op_county2.Location = new System.Drawing.Point(525, 141);
            this.st_op_county2.Name = "st_op_county2";
            this.st_op_county2.Size = new System.Drawing.Size(96, 27);
            this.st_op_county2.TabIndex = 31;
            // 
            // st_op_address2
            // 
            this.st_op_address2.Location = new System.Drawing.Point(57, 141);
            this.st_op_address2.Name = "st_op_address2";
            this.st_op_address2.Size = new System.Drawing.Size(419, 27);
            this.st_op_address2.TabIndex = 30;
            // 
            // label270
            // 
            this.label270.AutoSize = true;
            this.label270.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label270.Location = new System.Drawing.Point(627, 145);
            this.label270.Name = "label270";
            this.label270.Size = new System.Drawing.Size(54, 13);
            this.label270.TabIndex = 29;
            this.label270.Text = "UseCode:";
            // 
            // label271
            // 
            this.label271.AutoSize = true;
            this.label271.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label271.Location = new System.Drawing.Point(690, 166);
            this.label271.Name = "label271";
            this.label271.Size = new System.Drawing.Size(60, 13);
            this.label271.TabIndex = 28;
            this.label271.Text = "Bathrooms:";
            // 
            // label272
            // 
            this.label272.AutoSize = true;
            this.label272.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label272.Location = new System.Drawing.Point(576, 166);
            this.label272.Name = "label272";
            this.label272.Size = new System.Drawing.Size(57, 13);
            this.label272.TabIndex = 27;
            this.label272.Text = "Bedrooms:";
            // 
            // label273
            // 
            this.label273.AutoSize = true;
            this.label273.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label273.Location = new System.Drawing.Point(473, 166);
            this.label273.Name = "label273";
            this.label273.Size = new System.Drawing.Size(52, 13);
            this.label273.TabIndex = 26;
            this.label273.Text = "YearBuilt:";
            // 
            // label274
            // 
            this.label274.AutoSize = true;
            this.label274.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label274.Location = new System.Drawing.Point(315, 166);
            this.label274.Name = "label274";
            this.label274.Size = new System.Drawing.Size(66, 13);
            this.label274.TabIndex = 25;
            this.label274.Text = "BuildingSqft:";
            // 
            // label275
            // 
            this.label275.AutoSize = true;
            this.label275.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label275.Location = new System.Drawing.Point(179, 166);
            this.label275.Name = "label275";
            this.label275.Size = new System.Drawing.Size(44, 13);
            this.label275.TabIndex = 24;
            this.label275.Text = "LotSqft:";
            // 
            // label276
            // 
            this.label276.AutoSize = true;
            this.label276.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label276.Location = new System.Drawing.Point(3, 166);
            this.label276.Name = "label276";
            this.label276.Size = new System.Drawing.Size(77, 13);
            this.label276.TabIndex = 23;
            this.label276.Text = "ParcelNumber:";
            // 
            // label277
            // 
            this.label277.AutoSize = true;
            this.label277.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label277.Location = new System.Drawing.Point(482, 145);
            this.label277.Name = "label277";
            this.label277.Size = new System.Drawing.Size(43, 13);
            this.label277.TabIndex = 22;
            this.label277.Text = "County:";
            // 
            // st_op_lv2
            // 
            this.st_op_lv2.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader5,
            this.columnHeader7,
            this.columnHeader8});
            this.st_op_lv2.FullRowSelect = true;
            this.st_op_lv2.GridLines = true;
            this.st_op_lv2.HideSelection = false;
            this.st_op_lv2.Location = new System.Drawing.Point(6, 186);
            this.st_op_lv2.Name = "st_op_lv2";
            this.st_op_lv2.Size = new System.Drawing.Size(887, 79);
            this.st_op_lv2.TabIndex = 21;
            this.st_op_lv2.UseCompatibleStateImageBehavior = false;
            this.st_op_lv2.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Transfer Date";
            this.columnHeader1.Width = 102;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Seller";
            this.columnHeader2.Width = 123;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Buyer";
            this.columnHeader3.Width = 117;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Transfer Type";
            this.columnHeader4.Width = 103;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Loan Value";
            this.columnHeader5.Width = 94;
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "Lender Name";
            this.columnHeader7.Width = 225;
            // 
            // columnHeader8
            // 
            this.columnHeader8.Text = "Interest Type";
            this.columnHeader8.Width = 92;
            // 
            // label278
            // 
            this.label278.AutoSize = true;
            this.label278.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label278.Location = new System.Drawing.Point(3, 145);
            this.label278.Name = "label278";
            this.label278.Size = new System.Drawing.Size(48, 13);
            this.label278.TabIndex = 20;
            this.label278.Text = "Address:";
            // 
            // st_op_bedrooms1
            // 
            this.st_op_bedrooms1.Location = new System.Drawing.Point(748, 31);
            this.st_op_bedrooms1.Name = "st_op_bedrooms1";
            this.st_op_bedrooms1.Size = new System.Drawing.Size(45, 27);
            this.st_op_bedrooms1.TabIndex = 19;
            // 
            // st_op_bathrooms1
            // 
            this.st_op_bathrooms1.Location = new System.Drawing.Point(639, 31);
            this.st_op_bathrooms1.Name = "st_op_bathrooms1";
            this.st_op_bathrooms1.Size = new System.Drawing.Size(45, 27);
            this.st_op_bathrooms1.TabIndex = 18;
            // 
            // st_op_yearBuilt1
            // 
            this.st_op_yearBuilt1.Location = new System.Drawing.Point(525, 31);
            this.st_op_yearBuilt1.Name = "st_op_yearBuilt1";
            this.st_op_yearBuilt1.Size = new System.Drawing.Size(45, 27);
            this.st_op_yearBuilt1.TabIndex = 17;
            // 
            // st_op_buildingSqFt1
            // 
            this.st_op_buildingSqFt1.Location = new System.Drawing.Point(384, 31);
            this.st_op_buildingSqFt1.Name = "st_op_buildingSqFt1";
            this.st_op_buildingSqFt1.Size = new System.Drawing.Size(80, 27);
            this.st_op_buildingSqFt1.TabIndex = 16;
            // 
            // st_op_lotSqFt1
            // 
            this.st_op_lotSqFt1.Location = new System.Drawing.Point(229, 31);
            this.st_op_lotSqFt1.Name = "st_op_lotSqFt1";
            this.st_op_lotSqFt1.Size = new System.Drawing.Size(80, 27);
            this.st_op_lotSqFt1.TabIndex = 15;
            // 
            // st_op_parcelNumber1
            // 
            this.st_op_parcelNumber1.Location = new System.Drawing.Point(77, 31);
            this.st_op_parcelNumber1.Name = "st_op_parcelNumber1";
            this.st_op_parcelNumber1.Size = new System.Drawing.Size(96, 27);
            this.st_op_parcelNumber1.TabIndex = 14;
            // 
            // st_op_useCode1
            // 
            this.st_op_useCode1.Location = new System.Drawing.Point(683, 10);
            this.st_op_useCode1.Name = "st_op_useCode1";
            this.st_op_useCode1.Size = new System.Drawing.Size(210, 27);
            this.st_op_useCode1.TabIndex = 13;
            // 
            // st_op_county1
            // 
            this.st_op_county1.Location = new System.Drawing.Point(525, 9);
            this.st_op_county1.Name = "st_op_county1";
            this.st_op_county1.Size = new System.Drawing.Size(96, 27);
            this.st_op_county1.TabIndex = 12;
            // 
            // st_op_address1
            // 
            this.st_op_address1.Location = new System.Drawing.Point(57, 9);
            this.st_op_address1.Name = "st_op_address1";
            this.st_op_address1.Size = new System.Drawing.Size(419, 27);
            this.st_op_address1.TabIndex = 11;
            // 
            // label269
            // 
            this.label269.AutoSize = true;
            this.label269.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label269.Location = new System.Drawing.Point(627, 13);
            this.label269.Name = "label269";
            this.label269.Size = new System.Drawing.Size(54, 13);
            this.label269.TabIndex = 10;
            this.label269.Text = "UseCode:";
            // 
            // label268
            // 
            this.label268.AutoSize = true;
            this.label268.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label268.Location = new System.Drawing.Point(690, 34);
            this.label268.Name = "label268";
            this.label268.Size = new System.Drawing.Size(60, 13);
            this.label268.TabIndex = 9;
            this.label268.Text = "Bathrooms:";
            // 
            // label267
            // 
            this.label267.AutoSize = true;
            this.label267.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label267.Location = new System.Drawing.Point(576, 34);
            this.label267.Name = "label267";
            this.label267.Size = new System.Drawing.Size(57, 13);
            this.label267.TabIndex = 8;
            this.label267.Text = "Bedrooms:";
            // 
            // label266
            // 
            this.label266.AutoSize = true;
            this.label266.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label266.Location = new System.Drawing.Point(473, 34);
            this.label266.Name = "label266";
            this.label266.Size = new System.Drawing.Size(52, 13);
            this.label266.TabIndex = 7;
            this.label266.Text = "YearBuilt:";
            // 
            // label265
            // 
            this.label265.AutoSize = true;
            this.label265.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label265.Location = new System.Drawing.Point(315, 34);
            this.label265.Name = "label265";
            this.label265.Size = new System.Drawing.Size(66, 13);
            this.label265.TabIndex = 6;
            this.label265.Text = "BuildingSqft:";
            // 
            // label264
            // 
            this.label264.AutoSize = true;
            this.label264.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label264.Location = new System.Drawing.Point(179, 34);
            this.label264.Name = "label264";
            this.label264.Size = new System.Drawing.Size(44, 13);
            this.label264.TabIndex = 5;
            this.label264.Text = "LotSqft:";
            // 
            // label263
            // 
            this.label263.AutoSize = true;
            this.label263.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label263.Location = new System.Drawing.Point(3, 34);
            this.label263.Name = "label263";
            this.label263.Size = new System.Drawing.Size(77, 13);
            this.label263.TabIndex = 4;
            this.label263.Text = "ParcelNumber:";
            // 
            // label262
            // 
            this.label262.AutoSize = true;
            this.label262.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label262.Location = new System.Drawing.Point(482, 13);
            this.label262.Name = "label262";
            this.label262.Size = new System.Drawing.Size(43, 13);
            this.label262.TabIndex = 3;
            this.label262.Text = "County:";
            // 
            // st_op_lv1
            // 
            this.st_op_lv1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.transferDate,
            this.seller,
            this.buyer,
            this.transferType,
            this.loanValue,
            this.lenderName,
            this.interesetType});
            this.st_op_lv1.FullRowSelect = true;
            this.st_op_lv1.GridLines = true;
            this.st_op_lv1.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.st_op_lv1.HideSelection = false;
            this.st_op_lv1.Location = new System.Drawing.Point(6, 54);
            this.st_op_lv1.MultiSelect = false;
            this.st_op_lv1.Name = "st_op_lv1";
            this.st_op_lv1.ShowGroups = false;
            this.st_op_lv1.Size = new System.Drawing.Size(887, 79);
            this.st_op_lv1.TabIndex = 2;
            this.st_op_lv1.UseCompatibleStateImageBehavior = false;
            this.st_op_lv1.View = System.Windows.Forms.View.Details;
            // 
            // transferDate
            // 
            this.transferDate.Text = "Transfer Date";
            this.transferDate.Width = 102;
            // 
            // seller
            // 
            this.seller.Text = "Seller";
            this.seller.Width = 123;
            // 
            // buyer
            // 
            this.buyer.Text = "Buyer";
            this.buyer.Width = 117;
            // 
            // transferType
            // 
            this.transferType.Text = "Transfer Type";
            this.transferType.Width = 106;
            // 
            // loanValue
            // 
            this.loanValue.Text = "Loan Value";
            this.loanValue.Width = 94;
            // 
            // lenderName
            // 
            this.lenderName.Text = "Lender Name";
            this.lenderName.Width = 223;
            // 
            // interesetType
            // 
            this.interesetType.Text = "Interest Type";
            this.interesetType.Width = 92;
            // 
            // label261
            // 
            this.label261.AutoSize = true;
            this.label261.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label261.Location = new System.Drawing.Point(3, 13);
            this.label261.Name = "label261";
            this.label261.Size = new System.Drawing.Size(48, 13);
            this.label261.TabIndex = 1;
            this.label261.Text = "Address:";
            // 
            // skipTracedRelativesInfo
            // 
            this.skipTracedRelativesInfo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.skipTracedRelativesInfo.Controls.Add(this.st_ri_relatives);
            this.skipTracedRelativesInfo.Location = new System.Drawing.Point(4, 22);
            this.skipTracedRelativesInfo.Name = "skipTracedRelativesInfo";
            this.skipTracedRelativesInfo.Size = new System.Drawing.Size(1304, 625);
            this.skipTracedRelativesInfo.TabIndex = 2;
            this.skipTracedRelativesInfo.Text = "Relatives Info";
            // 
            // st_ri_relatives
            // 
            this.st_ri_relatives.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.st_ri_relatives.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader6,
            this.columnHeader14,
            this.columnHeader17,
            this.columnHeader18,
            this.columnHeader19,
            this.columnHeader20,
            this.columnHeader21});
            this.st_ri_relatives.FullRowSelect = true;
            this.st_ri_relatives.GridLines = true;
            this.st_ri_relatives.HideSelection = false;
            this.st_ri_relatives.Location = new System.Drawing.Point(13, 18);
            this.st_ri_relatives.MultiSelect = false;
            this.st_ri_relatives.Name = "st_ri_relatives";
            this.st_ri_relatives.Size = new System.Drawing.Size(875, 215);
            this.st_ri_relatives.TabIndex = 0;
            this.st_ri_relatives.UseCompatibleStateImageBehavior = false;
            this.st_ri_relatives.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "Score";
            // 
            // columnHeader14
            // 
            this.columnHeader14.Text = "First Name";
            this.columnHeader14.Width = 132;
            // 
            // columnHeader17
            // 
            this.columnHeader17.Text = "Last Name";
            this.columnHeader17.Width = 151;
            // 
            // columnHeader18
            // 
            this.columnHeader18.Text = "Age";
            // 
            // columnHeader19
            // 
            this.columnHeader19.Text = "Phone Rank 0";
            this.columnHeader19.Width = 140;
            // 
            // columnHeader20
            // 
            this.columnHeader20.Text = "Phone Rank 1";
            this.columnHeader20.Width = 149;
            // 
            // columnHeader21
            // 
            this.columnHeader21.Text = "Phone Rank 2";
            this.columnHeader21.Width = 153;
            // 
            // tpGroupMembership
            // 
            this.tpGroupMembership.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.tpGroupMembership.Controls.Add(this.bPending);
            this.tpGroupMembership.Controls.Add(this.pictureBox2);
            this.tpGroupMembership.Controls.Add(this.bAssignToGroup);
            this.tpGroupMembership.Controls.Add(this.bRemoveFromGroup);
            this.tpGroupMembership.Controls.Add(this.readMe2);
            this.tpGroupMembership.Controls.Add(this.lvOtherGroups);
            this.tpGroupMembership.Controls.Add(this.label61);
            this.tpGroupMembership.Controls.Add(this.tbGroupStatusGroup);
            this.tpGroupMembership.Controls.Add(this.bDoNotMailGroup);
            this.tpGroupMembership.Controls.Add(this.bPurchased);
            this.tpGroupMembership.Controls.Add(this.bDeadLeads);
            this.tpGroupMembership.Controls.Add(this.bDealsInProgress);
            this.tpGroupMembership.Controls.Add(this.bLeadsNotProcessedYet);
            this.tpGroupMembership.Controls.Add(this.label59);
            this.tpGroupMembership.Controls.Add(this.label58);
            this.tpGroupMembership.Controls.Add(this.label55);
            this.tpGroupMembership.Location = new System.Drawing.Point(4, 28);
            this.tpGroupMembership.Name = "tpGroupMembership";
            this.tpGroupMembership.Size = new System.Drawing.Size(192, 68);
            this.tpGroupMembership.TabIndex = 1;
            this.tpGroupMembership.Text = "Group Membership";
            // 
            // bPending
            // 
            this.bPending.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.bPending.Location = new System.Drawing.Point(296, 88);
            this.bPending.Name = "bPending";
            this.bPending.Size = new System.Drawing.Size(120, 40);
            this.bPending.TabIndex = 17;
            this.bPending.Text = "Pending";
            this.bPending.Click += new System.EventHandler(this.bPending_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(88, 8);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(72, 72);
            this.pictureBox2.TabIndex = 15;
            this.pictureBox2.TabStop = false;
            // 
            // bAssignToGroup
            // 
            this.bAssignToGroup.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.bAssignToGroup.Location = new System.Drawing.Point(424, 168);
            this.bAssignToGroup.Name = "bAssignToGroup";
            this.bAssignToGroup.Size = new System.Drawing.Size(144, 40);
            this.bAssignToGroup.TabIndex = 14;
            this.bAssignToGroup.Text = "Assign to a Group";
            this.bAssignToGroup.Click += new System.EventHandler(this.bAssignToGroup_Click);
            // 
            // bRemoveFromGroup
            // 
            this.bRemoveFromGroup.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.bRemoveFromGroup.Location = new System.Drawing.Point(424, 216);
            this.bRemoveFromGroup.Name = "bRemoveFromGroup";
            this.bRemoveFromGroup.Size = new System.Drawing.Size(144, 40);
            this.bRemoveFromGroup.TabIndex = 13;
            this.bRemoveFromGroup.Text = "Remove from Group";
            this.bRemoveFromGroup.Click += new System.EventHandler(this.bRemoveFromGroup_Click);
            // 
            // readMe2
            // 
            this.readMe2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.readMe2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.readMe2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("readMe2.BackgroundImage")));
            this.readMe2.Location = new System.Drawing.Point(168, 8);
            this.readMe2.Name = "readMe2";
            this.readMe2.Size = new System.Drawing.Size(16, 16);
            this.readMe2.TabIndex = 16;
            // 
            // lvOtherGroups
            // 
            this.lvOtherGroups.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.lvOtherGroups.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.nodeId,
            this.groupName,
            this.groupDesc});
            this.lvOtherGroups.FullRowSelect = true;
            this.lvOtherGroups.GridLines = true;
            this.lvOtherGroups.HideSelection = false;
            this.lvOtherGroups.Location = new System.Drawing.Point(16, 168);
            this.lvOtherGroups.MultiSelect = false;
            this.lvOtherGroups.Name = "lvOtherGroups";
            this.lvOtherGroups.Size = new System.Drawing.Size(392, 0);
            this.lvOtherGroups.TabIndex = 11;
            this.lvOtherGroups.UseCompatibleStateImageBehavior = false;
            this.lvOtherGroups.View = System.Windows.Forms.View.Details;
            // 
            // nodeId
            // 
            this.nodeId.Width = 0;
            // 
            // groupName
            // 
            this.groupName.Text = "Group Name";
            this.groupName.Width = 158;
            // 
            // groupDesc
            // 
            this.groupDesc.Text = "Description";
            this.groupDesc.Width = 230;
            // 
            // label61
            // 
            this.label61.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label61.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label61.Location = new System.Drawing.Point(8, 144);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(200, 23);
            this.label61.TabIndex = 10;
            this.label61.Text = "Other Group Membership";
            // 
            // tbGroupStatusGroup
            // 
            this.tbGroupStatusGroup.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbGroupStatusGroup.Location = new System.Drawing.Point(120, 32);
            this.tbGroupStatusGroup.Name = "tbGroupStatusGroup";
            this.tbGroupStatusGroup.ReadOnly = true;
            this.tbGroupStatusGroup.Size = new System.Drawing.Size(0, 27);
            this.tbGroupStatusGroup.TabIndex = 8;
            // 
            // bDoNotMailGroup
            // 
            this.bDoNotMailGroup.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.bDoNotMailGroup.Location = new System.Drawing.Point(680, 88);
            this.bDoNotMailGroup.Name = "bDoNotMailGroup";
            this.bDoNotMailGroup.Size = new System.Drawing.Size(120, 40);
            this.bDoNotMailGroup.TabIndex = 7;
            this.bDoNotMailGroup.Text = "Do Not Mail Group";
            this.bDoNotMailGroup.Click += new System.EventHandler(this.bDoNotMailGroup_Click);
            // 
            // bPurchased
            // 
            this.bPurchased.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.bPurchased.Location = new System.Drawing.Point(424, 88);
            this.bPurchased.Name = "bPurchased";
            this.bPurchased.Size = new System.Drawing.Size(120, 40);
            this.bPurchased.TabIndex = 6;
            this.bPurchased.Text = "Purchased";
            this.bPurchased.Click += new System.EventHandler(this.bPurchased_Click);
            // 
            // bDeadLeads
            // 
            this.bDeadLeads.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.bDeadLeads.Location = new System.Drawing.Point(552, 88);
            this.bDeadLeads.Name = "bDeadLeads";
            this.bDeadLeads.Size = new System.Drawing.Size(120, 40);
            this.bDeadLeads.TabIndex = 5;
            this.bDeadLeads.Text = "Dead Leads";
            this.bDeadLeads.Click += new System.EventHandler(this.bDeadLeads_Click);
            // 
            // bDealsInProgress
            // 
            this.bDealsInProgress.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.bDealsInProgress.Location = new System.Drawing.Point(168, 88);
            this.bDealsInProgress.Name = "bDealsInProgress";
            this.bDealsInProgress.Size = new System.Drawing.Size(120, 40);
            this.bDealsInProgress.TabIndex = 4;
            this.bDealsInProgress.Text = "Deals In Progress";
            this.bDealsInProgress.Click += new System.EventHandler(this.bDealsInProgress_Click);
            // 
            // bLeadsNotProcessedYet
            // 
            this.bLeadsNotProcessedYet.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.bLeadsNotProcessedYet.Location = new System.Drawing.Point(24, 88);
            this.bLeadsNotProcessedYet.Name = "bLeadsNotProcessedYet";
            this.bLeadsNotProcessedYet.Size = new System.Drawing.Size(136, 40);
            this.bLeadsNotProcessedYet.TabIndex = 3;
            this.bLeadsNotProcessedYet.Text = "Leads Not Processed Yet";
            this.bLeadsNotProcessedYet.Click += new System.EventHandler(this.bLeadsNotProcessedYet_Click);
            // 
            // label59
            // 
            this.label59.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label59.Location = new System.Drawing.Point(24, 64);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(320, 23);
            this.label59.TabIndex = 2;
            this.label59.Text = "Update Status Group using these Shortcut Buttons:";
            // 
            // label58
            // 
            this.label58.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label58.Location = new System.Drawing.Point(24, 35);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(96, 23);
            this.label58.TabIndex = 1;
            this.label58.Text = "Status Group:";
            // 
            // label55
            // 
            this.label55.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label55.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label55.Location = new System.Drawing.Point(8, 8);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(224, 23);
            this.label55.TabIndex = 0;
            this.label55.Text = "Status Group Membership";
            // 
            // tpActivitiesHistory
            // 
            this.tpActivitiesHistory.Location = new System.Drawing.Point(4, 28);
            this.tpActivitiesHistory.Name = "tpActivitiesHistory";
            this.tpActivitiesHistory.Size = new System.Drawing.Size(192, 68);
            this.tpActivitiesHistory.TabIndex = 2;
            this.tpActivitiesHistory.Text = "Activities History";
            this.tpActivitiesHistory.UseVisualStyleBackColor = true;
            // 
            // _evaluate
            // 
            this._evaluate.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this._evaluate.Controls.Add(this._evaluateSubTabControl);
            this._evaluate.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this._evaluate.Location = new System.Drawing.Point(4, 28);
            this._evaluate.Name = "_evaluate";
            this._evaluate.Size = new System.Drawing.Size(192, 68);
            this._evaluate.TabIndex = 12;
            this._evaluate.Text = "Evaluate";
            this._evaluate.UseVisualStyleBackColor = true;
            // 
            // _evaluateSubTabControl
            // 
            this._evaluateSubTabControl.Controls.Add(this._propertyProfileSubTabPage);
            this._evaluateSubTabControl.Controls.Add(this._compsSubTabPage);
            this._evaluateSubTabControl.Controls.Add(this._estimateRepairsSubTabPage);
            this._evaluateSubTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this._evaluateSubTabControl.Location = new System.Drawing.Point(0, 0);
            this._evaluateSubTabControl.Name = "_evaluateSubTabControl";
            this._evaluateSubTabControl.SelectedIndex = 0;
            this._evaluateSubTabControl.Size = new System.Drawing.Size(192, 68);
            this._evaluateSubTabControl.TabIndex = 0;
            // 
            // _propertyProfileSubTabPage
            // 
            this._propertyProfileSubTabPage.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this._propertyProfileSubTabPage.Controls.Add(this.ucppc);
            this._propertyProfileSubTabPage.Location = new System.Drawing.Point(4, 28);
            this._propertyProfileSubTabPage.Name = "_propertyProfileSubTabPage";
            this._propertyProfileSubTabPage.Padding = new System.Windows.Forms.Padding(3);
            this._propertyProfileSubTabPage.Size = new System.Drawing.Size(184, 36);
            this._propertyProfileSubTabPage.TabIndex = 0;
            this._propertyProfileSubTabPage.Text = "Property Profile";
            // 
            // ucppc
            // 
            this.ucppc.AllowAutoSaveTriggerEvents = false;
            this.ucppc.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.ucppc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucppc.Location = new System.Drawing.Point(3, 3);
            this.ucppc.Name = "ucppc";
            this.ucppc.OverridedSqFt = false;
            this.ucppc.OverridedSqFtAmt = 0;
            this.ucppc.Size = new System.Drawing.Size(178, 30);
            this.ucppc.TabIndex = 0;
            // 
            // _compsSubTabPage
            // 
            this._compsSubTabPage.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this._compsSubTabPage.Controls.Add(this.ucppclv);
            this._compsSubTabPage.Location = new System.Drawing.Point(4, 22);
            this._compsSubTabPage.Name = "_compsSubTabPage";
            this._compsSubTabPage.Padding = new System.Windows.Forms.Padding(3);
            this._compsSubTabPage.Size = new System.Drawing.Size(1304, 625);
            this._compsSubTabPage.TabIndex = 1;
            this._compsSubTabPage.Text = "COMPS";
            // 
            // ucppclv
            // 
            this.ucppclv.AllowAutoSaveTriggerEvents = false;
            this.ucppclv.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.ucppclv.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucppclv.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.ucppclv.Location = new System.Drawing.Point(3, 3);
            this.ucppclv.Name = "ucppclv";
            this.ucppclv.Size = new System.Drawing.Size(1298, 619);
            this.ucppclv.TabIndex = 0;
            // 
            // _estimateRepairsSubTabPage
            // 
            this._estimateRepairsSubTabPage.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this._estimateRepairsSubTabPage.Controls.Add(this._repairs);
            this._estimateRepairsSubTabPage.Location = new System.Drawing.Point(4, 22);
            this._estimateRepairsSubTabPage.Name = "_estimateRepairsSubTabPage";
            this._estimateRepairsSubTabPage.Size = new System.Drawing.Size(1304, 625);
            this._estimateRepairsSubTabPage.TabIndex = 2;
            this._estimateRepairsSubTabPage.Text = "Estimate Repairs";
            // 
            // _repairs
            // 
            this._repairs.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this._repairs.Dock = System.Windows.Forms.DockStyle.Fill;
            this._repairs.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this._repairs.Location = new System.Drawing.Point(0, 0);
            this._repairs.Name = "_repairs";
            this._repairs.PropertyItemId = new System.Guid("00000000-0000-0000-0000-000000000000");
            this._repairs.Size = new System.Drawing.Size(1304, 625);
            this._repairs.TabIndex = 0;
            // 
            // _shortSaleTab
            // 
            this._shortSaleTab.Controls.Add(this._shortSaleControl);
            this._shortSaleTab.Location = new System.Drawing.Point(4, 28);
            this._shortSaleTab.Name = "_shortSaleTab";
            this._shortSaleTab.Size = new System.Drawing.Size(192, 68);
            this._shortSaleTab.TabIndex = 13;
            this._shortSaleTab.Text = "Short Sale";
            this._shortSaleTab.UseVisualStyleBackColor = true;
            // 
            // _shortSaleControl
            // 
            this._shortSaleControl.AutoSize = true;
            this._shortSaleControl.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this._shortSaleControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this._shortSaleControl.foreclosureDate = "";
            this._shortSaleControl.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this._shortSaleControl.Location = new System.Drawing.Point(0, 0);
            this._shortSaleControl.Name = "_shortSaleControl";
            this._shortSaleControl.overAllMaxToPay = "";
            this._shortSaleControl.overAllShortSaleStrategy = "";
            this._shortSaleControl.primaryPhoneNr = "";
            this._shortSaleControl.primarySSNr = "";
            this._shortSaleControl.secondaryPhoneNr = "";
            this._shortSaleControl.secondarySSNr = "";
            this._shortSaleControl.Size = new System.Drawing.Size(192, 68);
            this._shortSaleControl.TabIndex = 0;
            this._shortSaleControl.UserAppFolder = "";
            // 
            // _buyersInfoTab
            // 
            this._buyersInfoTab.Controls.Add(this._buyerInfoOverview);
            this._buyersInfoTab.Location = new System.Drawing.Point(4, 28);
            this._buyersInfoTab.Name = "_buyersInfoTab";
            this._buyersInfoTab.Size = new System.Drawing.Size(192, 68);
            this._buyersInfoTab.TabIndex = 10;
            this._buyersInfoTab.Text = "  Sell";
            this._buyersInfoTab.UseVisualStyleBackColor = true;
            // 
            // _buyerInfoOverview
            // 
            this._buyerInfoOverview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this._buyerInfoOverview.Dock = System.Windows.Forms.DockStyle.Fill;
            this._buyerInfoOverview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this._buyerInfoOverview.Location = new System.Drawing.Point(0, 0);
            this._buyerInfoOverview.Name = "_buyerInfoOverview";
            this._buyerInfoOverview.Size = new System.Drawing.Size(192, 68);
            this._buyerInfoOverview.TabIndex = 0;
            // 
            // tpImportedInfo
            // 
            this.tpImportedInfo.Controls.Add(this.tcSubImportedInfo);
            this.tpImportedInfo.Location = new System.Drawing.Point(4, 28);
            this.tpImportedInfo.Name = "tpImportedInfo";
            this.tpImportedInfo.Size = new System.Drawing.Size(192, 68);
            this.tpImportedInfo.TabIndex = 5;
            this.tpImportedInfo.Text = "Imported Info";
            this.tpImportedInfo.UseVisualStyleBackColor = true;
            // 
            // tcSubImportedInfo
            // 
            this.tcSubImportedInfo.Controls.Add(this.tpPropertyCharacteristics);
            this.tcSubImportedInfo.Controls.Add(this.tpListingInfo);
            this.tcSubImportedInfo.Controls.Add(this.tpForeClosureInfo);
            this.tcSubImportedInfo.Controls.Add(this.tpFinancials);
            this.tcSubImportedInfo.Controls.Add(this.tpTaxInfo);
            this.tcSubImportedInfo.Controls.Add(this.tpOtherInfo);
            this.tcSubImportedInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tcSubImportedInfo.Location = new System.Drawing.Point(0, 0);
            this.tcSubImportedInfo.Name = "tcSubImportedInfo";
            this.tcSubImportedInfo.SelectedIndex = 0;
            this.tcSubImportedInfo.Size = new System.Drawing.Size(192, 68);
            this.tcSubImportedInfo.TabIndex = 0;
            // 
            // tpPropertyCharacteristics
            // 
            this.tpPropertyCharacteristics.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.tpPropertyCharacteristics.Controls.Add(this._copyDataToDealInfo);
            this.tpPropertyCharacteristics.Controls.Add(this.readMe6);
            this.tpPropertyCharacteristics.Controls.Add(this.tb_II_PC_Pool);
            this.tpPropertyCharacteristics.Controls.Add(this.label170);
            this.tpPropertyCharacteristics.Controls.Add(this.tb_II_PC_LotSqFt);
            this.tpPropertyCharacteristics.Controls.Add(this.label169);
            this.tpPropertyCharacteristics.Controls.Add(this.tb_II_PC_HomeSqFt);
            this.tpPropertyCharacteristics.Controls.Add(this.label168);
            this.tpPropertyCharacteristics.Controls.Add(this.tb_II_PC_Garage);
            this.tpPropertyCharacteristics.Controls.Add(this.label167);
            this.tpPropertyCharacteristics.Controls.Add(this.tb_II_PC_Bathrooms);
            this.tpPropertyCharacteristics.Controls.Add(this.label164);
            this.tpPropertyCharacteristics.Controls.Add(this.tb_II_PC_Bedrooms);
            this.tpPropertyCharacteristics.Controls.Add(this.label165);
            this.tpPropertyCharacteristics.Controls.Add(this.tb_II_PC_YearBuilt);
            this.tpPropertyCharacteristics.Controls.Add(this.label166);
            this.tpPropertyCharacteristics.Controls.Add(this.tb_II_PC_LegalDesc);
            this.tpPropertyCharacteristics.Controls.Add(this.label162);
            this.tpPropertyCharacteristics.Controls.Add(this.tb_II_PC_PropertyType);
            this.tpPropertyCharacteristics.Controls.Add(this.label161);
            this.tpPropertyCharacteristics.Controls.Add(this.tb_II_PC_UseCode);
            this.tpPropertyCharacteristics.Controls.Add(this.label160);
            this.tpPropertyCharacteristics.Controls.Add(this.tb_II_PC_Subdivision);
            this.tpPropertyCharacteristics.Controls.Add(this.label159);
            this.tpPropertyCharacteristics.Controls.Add(this.tb_II_PC_County);
            this.tpPropertyCharacteristics.Controls.Add(this.label158);
            this.tpPropertyCharacteristics.Location = new System.Drawing.Point(4, 28);
            this.tpPropertyCharacteristics.Name = "tpPropertyCharacteristics";
            this.tpPropertyCharacteristics.Size = new System.Drawing.Size(184, 36);
            this.tpPropertyCharacteristics.TabIndex = 2;
            this.tpPropertyCharacteristics.Text = "Property Characteristics";
            // 
            // _copyDataToDealInfo
            // 
            this._copyDataToDealInfo.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._copyDataToDealInfo.Location = new System.Drawing.Point(672, 24);
            this._copyDataToDealInfo.Name = "_copyDataToDealInfo";
            this._copyDataToDealInfo.Size = new System.Drawing.Size(176, 56);
            this._copyDataToDealInfo.TabIndex = 64;
            this._copyDataToDealInfo.Text = "Copy data to Deal Info tab";
            // 
            // readMe6
            // 
            this.readMe6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.readMe6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.readMe6.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("readMe6.BackgroundImage")));
            this.readMe6.Location = new System.Drawing.Point(160, 8);
            this.readMe6.Name = "readMe6";
            this.readMe6.Size = new System.Drawing.Size(16, 16);
            this.readMe6.TabIndex = 63;
            // 
            // tb_II_PC_Pool
            // 
            this.tb_II_PC_Pool.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_II_PC_Pool.Location = new System.Drawing.Point(344, 80);
            this.tb_II_PC_Pool.Name = "tb_II_PC_Pool";
            this.tb_II_PC_Pool.ReadOnly = true;
            this.tb_II_PC_Pool.Size = new System.Drawing.Size(104, 27);
            this.tb_II_PC_Pool.TabIndex = 62;
            // 
            // label170
            // 
            this.label170.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label170.Location = new System.Drawing.Point(344, 64);
            this.label170.Name = "label170";
            this.label170.Size = new System.Drawing.Size(72, 16);
            this.label170.TabIndex = 61;
            this.label170.Text = "Pool";
            // 
            // tb_II_PC_LotSqFt
            // 
            this.tb_II_PC_LotSqFt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_II_PC_LotSqFt.Location = new System.Drawing.Point(544, 80);
            this.tb_II_PC_LotSqFt.Name = "tb_II_PC_LotSqFt";
            this.tb_II_PC_LotSqFt.ReadOnly = true;
            this.tb_II_PC_LotSqFt.Size = new System.Drawing.Size(72, 27);
            this.tb_II_PC_LotSqFt.TabIndex = 60;
            // 
            // label169
            // 
            this.label169.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label169.Location = new System.Drawing.Point(544, 64);
            this.label169.Name = "label169";
            this.label169.Size = new System.Drawing.Size(72, 16);
            this.label169.TabIndex = 59;
            this.label169.Text = "Lot Sq. Ft.";
            // 
            // tb_II_PC_HomeSqFt
            // 
            this.tb_II_PC_HomeSqFt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_II_PC_HomeSqFt.Location = new System.Drawing.Point(456, 80);
            this.tb_II_PC_HomeSqFt.Name = "tb_II_PC_HomeSqFt";
            this.tb_II_PC_HomeSqFt.ReadOnly = true;
            this.tb_II_PC_HomeSqFt.Size = new System.Drawing.Size(80, 27);
            this.tb_II_PC_HomeSqFt.TabIndex = 58;
            // 
            // label168
            // 
            this.label168.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label168.Location = new System.Drawing.Point(456, 64);
            this.label168.Name = "label168";
            this.label168.Size = new System.Drawing.Size(80, 16);
            this.label168.TabIndex = 57;
            this.label168.Text = "Home Sq. Ft.";
            // 
            // tb_II_PC_Garage
            // 
            this.tb_II_PC_Garage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_II_PC_Garage.Location = new System.Drawing.Point(240, 80);
            this.tb_II_PC_Garage.Name = "tb_II_PC_Garage";
            this.tb_II_PC_Garage.ReadOnly = true;
            this.tb_II_PC_Garage.Size = new System.Drawing.Size(96, 27);
            this.tb_II_PC_Garage.TabIndex = 56;
            // 
            // label167
            // 
            this.label167.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label167.Location = new System.Drawing.Point(240, 64);
            this.label167.Name = "label167";
            this.label167.Size = new System.Drawing.Size(64, 16);
            this.label167.TabIndex = 55;
            this.label167.Text = "Garage";
            // 
            // tb_II_PC_Bathrooms
            // 
            this.tb_II_PC_Bathrooms.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_II_PC_Bathrooms.Location = new System.Drawing.Point(168, 80);
            this.tb_II_PC_Bathrooms.Name = "tb_II_PC_Bathrooms";
            this.tb_II_PC_Bathrooms.ReadOnly = true;
            this.tb_II_PC_Bathrooms.Size = new System.Drawing.Size(64, 27);
            this.tb_II_PC_Bathrooms.TabIndex = 54;
            // 
            // label164
            // 
            this.label164.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label164.Location = new System.Drawing.Point(168, 64);
            this.label164.Name = "label164";
            this.label164.Size = new System.Drawing.Size(64, 16);
            this.label164.TabIndex = 53;
            this.label164.Text = "Bathrooms";
            // 
            // tb_II_PC_Bedrooms
            // 
            this.tb_II_PC_Bedrooms.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_II_PC_Bedrooms.Location = new System.Drawing.Point(88, 80);
            this.tb_II_PC_Bedrooms.Name = "tb_II_PC_Bedrooms";
            this.tb_II_PC_Bedrooms.ReadOnly = true;
            this.tb_II_PC_Bedrooms.Size = new System.Drawing.Size(72, 27);
            this.tb_II_PC_Bedrooms.TabIndex = 52;
            // 
            // label165
            // 
            this.label165.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label165.Location = new System.Drawing.Point(88, 64);
            this.label165.Name = "label165";
            this.label165.Size = new System.Drawing.Size(72, 16);
            this.label165.TabIndex = 51;
            this.label165.Text = "Bedrooms";
            // 
            // tb_II_PC_YearBuilt
            // 
            this.tb_II_PC_YearBuilt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_II_PC_YearBuilt.Location = new System.Drawing.Point(8, 80);
            this.tb_II_PC_YearBuilt.Name = "tb_II_PC_YearBuilt";
            this.tb_II_PC_YearBuilt.ReadOnly = true;
            this.tb_II_PC_YearBuilt.Size = new System.Drawing.Size(72, 27);
            this.tb_II_PC_YearBuilt.TabIndex = 50;
            // 
            // label166
            // 
            this.label166.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label166.Location = new System.Drawing.Point(8, 64);
            this.label166.Name = "label166";
            this.label166.Size = new System.Drawing.Size(72, 16);
            this.label166.TabIndex = 49;
            this.label166.Text = "Year Built";
            // 
            // tb_II_PC_LegalDesc
            // 
            this.tb_II_PC_LegalDesc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_II_PC_LegalDesc.Location = new System.Drawing.Point(8, 136);
            this.tb_II_PC_LegalDesc.Multiline = true;
            this.tb_II_PC_LegalDesc.Name = "tb_II_PC_LegalDesc";
            this.tb_II_PC_LegalDesc.ReadOnly = true;
            this.tb_II_PC_LegalDesc.Size = new System.Drawing.Size(664, 88);
            this.tb_II_PC_LegalDesc.TabIndex = 15;
            // 
            // label162
            // 
            this.label162.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label162.Location = new System.Drawing.Point(8, 120);
            this.label162.Name = "label162";
            this.label162.Size = new System.Drawing.Size(120, 16);
            this.label162.TabIndex = 14;
            this.label162.Text = "Legal Description";
            // 
            // tb_II_PC_PropertyType
            // 
            this.tb_II_PC_PropertyType.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_II_PC_PropertyType.Location = new System.Drawing.Point(320, 24);
            this.tb_II_PC_PropertyType.Name = "tb_II_PC_PropertyType";
            this.tb_II_PC_PropertyType.ReadOnly = true;
            this.tb_II_PC_PropertyType.Size = new System.Drawing.Size(96, 27);
            this.tb_II_PC_PropertyType.TabIndex = 13;
            // 
            // label161
            // 
            this.label161.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label161.Location = new System.Drawing.Point(320, 8);
            this.label161.Name = "label161";
            this.label161.Size = new System.Drawing.Size(96, 16);
            this.label161.TabIndex = 12;
            this.label161.Text = "Property Type";
            // 
            // tb_II_PC_UseCode
            // 
            this.tb_II_PC_UseCode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_II_PC_UseCode.Location = new System.Drawing.Point(240, 24);
            this.tb_II_PC_UseCode.Name = "tb_II_PC_UseCode";
            this.tb_II_PC_UseCode.ReadOnly = true;
            this.tb_II_PC_UseCode.Size = new System.Drawing.Size(72, 27);
            this.tb_II_PC_UseCode.TabIndex = 11;
            // 
            // label160
            // 
            this.label160.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label160.Location = new System.Drawing.Point(240, 8);
            this.label160.Name = "label160";
            this.label160.Size = new System.Drawing.Size(64, 16);
            this.label160.TabIndex = 10;
            this.label160.Text = "Use Code";
            // 
            // tb_II_PC_Subdivision
            // 
            this.tb_II_PC_Subdivision.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_II_PC_Subdivision.Location = new System.Drawing.Point(112, 24);
            this.tb_II_PC_Subdivision.Name = "tb_II_PC_Subdivision";
            this.tb_II_PC_Subdivision.ReadOnly = true;
            this.tb_II_PC_Subdivision.Size = new System.Drawing.Size(120, 27);
            this.tb_II_PC_Subdivision.TabIndex = 9;
            // 
            // label159
            // 
            this.label159.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label159.Location = new System.Drawing.Point(112, 8);
            this.label159.Name = "label159";
            this.label159.Size = new System.Drawing.Size(88, 16);
            this.label159.TabIndex = 8;
            this.label159.Text = "Subdivision";
            // 
            // tb_II_PC_County
            // 
            this.tb_II_PC_County.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_II_PC_County.Location = new System.Drawing.Point(8, 24);
            this.tb_II_PC_County.Name = "tb_II_PC_County";
            this.tb_II_PC_County.ReadOnly = true;
            this.tb_II_PC_County.Size = new System.Drawing.Size(96, 27);
            this.tb_II_PC_County.TabIndex = 7;
            // 
            // label158
            // 
            this.label158.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label158.Location = new System.Drawing.Point(8, 8);
            this.label158.Name = "label158";
            this.label158.Size = new System.Drawing.Size(56, 16);
            this.label158.TabIndex = 0;
            this.label158.Text = "County";
            // 
            // tpListingInfo
            // 
            this.tpListingInfo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.tpListingInfo.Controls.Add(this.tb_II_LI_ListingPrice);
            this.tpListingInfo.Controls.Add(this.bSaveImportedInfo);
            this.tpListingInfo.Controls.Add(this.tb_II_LI_Comments);
            this.tpListingInfo.Controls.Add(this.label157);
            this.tpListingInfo.Controls.Add(this.tb_II_LI_LA_Email);
            this.tpListingInfo.Controls.Add(this.label156);
            this.tpListingInfo.Controls.Add(this.tb_II_LI_LA_FaxNumber);
            this.tpListingInfo.Controls.Add(this.label155);
            this.tpListingInfo.Controls.Add(this.label154);
            this.tpListingInfo.Controls.Add(this.tb_II_LI_LA_PhoneNumber);
            this.tpListingInfo.Controls.Add(this.tb_II_LI_LA_AgentCode);
            this.tpListingInfo.Controls.Add(this.label153);
            this.tpListingInfo.Controls.Add(this.tb_II_LI_LA_FullName);
            this.tpListingInfo.Controls.Add(this.label152);
            this.tpListingInfo.Controls.Add(this.tb_II_LI_LA_LastName);
            this.tpListingInfo.Controls.Add(this.label151);
            this.tpListingInfo.Controls.Add(this.tb_II_LI_LA_FirstName);
            this.tpListingInfo.Controls.Add(this.label150);
            this.tpListingInfo.Controls.Add(this.label149);
            this.tpListingInfo.Controls.Add(this.tb_II_LI_ListingOffice);
            this.tpListingInfo.Controls.Add(this.label148);
            this.tpListingInfo.Controls.Add(this.tb_II_LI_ListingMLSNumber);
            this.tpListingInfo.Controls.Add(this.label147);
            this.tpListingInfo.Controls.Add(this.tb_II_LI_Status);
            this.tpListingInfo.Controls.Add(this.label146);
            this.tpListingInfo.Controls.Add(this.label145);
            this.tpListingInfo.Controls.Add(this.label144);
            this.tpListingInfo.Controls.Add(this.readMe5);
            this.tpListingInfo.Location = new System.Drawing.Point(4, 22);
            this.tpListingInfo.Name = "tpListingInfo";
            this.tpListingInfo.Size = new System.Drawing.Size(1304, 665);
            this.tpListingInfo.TabIndex = 1;
            this.tpListingInfo.Text = "Listing Info";
            // 
            // tb_II_LI_ListingPrice
            // 
            this.tb_II_LI_ListingPrice.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_II_LI_ListingPrice.Location = new System.Drawing.Point(16, 24);
            this.tb_II_LI_ListingPrice.Name = "tb_II_LI_ListingPrice";
            this.tb_II_LI_ListingPrice.Size = new System.Drawing.Size(96, 27);
            this.tb_II_LI_ListingPrice.TabIndex = 7;
            // 
            // bSaveImportedInfo
            // 
            this.bSaveImportedInfo.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.bSaveImportedInfo.Location = new System.Drawing.Point(312, 144);
            this.bSaveImportedInfo.Name = "bSaveImportedInfo";
            this.bSaveImportedInfo.Size = new System.Drawing.Size(40, 23);
            this.bSaveImportedInfo.TabIndex = 32;
            this.bSaveImportedInfo.Text = "&Save";
            this.bSaveImportedInfo.Visible = false;
            // 
            // tb_II_LI_Comments
            // 
            this.tb_II_LI_Comments.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_II_LI_Comments.Location = new System.Drawing.Point(8, 208);
            this.tb_II_LI_Comments.Multiline = true;
            this.tb_II_LI_Comments.Name = "tb_II_LI_Comments";
            this.tb_II_LI_Comments.ReadOnly = true;
            this.tb_II_LI_Comments.Size = new System.Drawing.Size(608, 96);
            this.tb_II_LI_Comments.TabIndex = 31;
            // 
            // label157
            // 
            this.label157.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label157.Location = new System.Drawing.Point(8, 184);
            this.label157.Name = "label157";
            this.label157.Size = new System.Drawing.Size(100, 23);
            this.label157.TabIndex = 30;
            this.label157.Text = "COMMENTS";
            // 
            // tb_II_LI_LA_Email
            // 
            this.tb_II_LI_LA_Email.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_II_LI_LA_Email.Location = new System.Drawing.Point(384, 144);
            this.tb_II_LI_LA_Email.Name = "tb_II_LI_LA_Email";
            this.tb_II_LI_LA_Email.Size = new System.Drawing.Size(232, 27);
            this.tb_II_LI_LA_Email.TabIndex = 29;
            // 
            // label156
            // 
            this.label156.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label156.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label156.Location = new System.Drawing.Point(384, 128);
            this.label156.Name = "label156";
            this.label156.Size = new System.Drawing.Size(136, 16);
            this.label156.TabIndex = 28;
            this.label156.Text = "Listing Agent Email";
            // 
            // tb_II_LI_LA_FaxNumber
            // 
            this.tb_II_LI_LA_FaxNumber.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_II_LI_LA_FaxNumber.Location = new System.Drawing.Point(208, 144);
            this.tb_II_LI_LA_FaxNumber.Name = "tb_II_LI_LA_FaxNumber";
            this.tb_II_LI_LA_FaxNumber.Size = new System.Drawing.Size(96, 27);
            this.tb_II_LI_LA_FaxNumber.TabIndex = 27;
            // 
            // label155
            // 
            this.label155.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label155.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label155.Location = new System.Drawing.Point(208, 128);
            this.label155.Name = "label155";
            this.label155.Size = new System.Drawing.Size(96, 16);
            this.label155.TabIndex = 26;
            this.label155.Text = "Fax Number ";
            // 
            // label154
            // 
            this.label154.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label154.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label154.Location = new System.Drawing.Point(8, 128);
            this.label154.Name = "label154";
            this.label154.Size = new System.Drawing.Size(192, 16);
            this.label154.TabIndex = 25;
            this.label154.Text = "Listing Agent Phone Number";
            // 
            // tb_II_LI_LA_PhoneNumber
            // 
            this.tb_II_LI_LA_PhoneNumber.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_II_LI_LA_PhoneNumber.Location = new System.Drawing.Point(8, 144);
            this.tb_II_LI_LA_PhoneNumber.Name = "tb_II_LI_LA_PhoneNumber";
            this.tb_II_LI_LA_PhoneNumber.Size = new System.Drawing.Size(192, 27);
            this.tb_II_LI_LA_PhoneNumber.TabIndex = 24;
            // 
            // tb_II_LI_LA_AgentCode
            // 
            this.tb_II_LI_LA_AgentCode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_II_LI_LA_AgentCode.Location = new System.Drawing.Point(496, 96);
            this.tb_II_LI_LA_AgentCode.Name = "tb_II_LI_LA_AgentCode";
            this.tb_II_LI_LA_AgentCode.Size = new System.Drawing.Size(120, 27);
            this.tb_II_LI_LA_AgentCode.TabIndex = 23;
            // 
            // label153
            // 
            this.label153.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label153.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label153.Location = new System.Drawing.Point(496, 80);
            this.label153.Name = "label153";
            this.label153.Size = new System.Drawing.Size(112, 16);
            this.label153.TabIndex = 22;
            this.label153.Text = "Agent Code";
            // 
            // tb_II_LI_LA_FullName
            // 
            this.tb_II_LI_LA_FullName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_II_LI_LA_FullName.Location = new System.Drawing.Point(280, 96);
            this.tb_II_LI_LA_FullName.Name = "tb_II_LI_LA_FullName";
            this.tb_II_LI_LA_FullName.ReadOnly = true;
            this.tb_II_LI_LA_FullName.Size = new System.Drawing.Size(208, 27);
            this.tb_II_LI_LA_FullName.TabIndex = 21;
            // 
            // label152
            // 
            this.label152.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label152.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label152.Location = new System.Drawing.Point(280, 80);
            this.label152.Name = "label152";
            this.label152.Size = new System.Drawing.Size(112, 16);
            this.label152.TabIndex = 20;
            this.label152.Text = "Full Name";
            // 
            // tb_II_LI_LA_LastName
            // 
            this.tb_II_LI_LA_LastName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_II_LI_LA_LastName.Location = new System.Drawing.Point(152, 96);
            this.tb_II_LI_LA_LastName.Name = "tb_II_LI_LA_LastName";
            this.tb_II_LI_LA_LastName.Size = new System.Drawing.Size(120, 27);
            this.tb_II_LI_LA_LastName.TabIndex = 19;
            // 
            // label151
            // 
            this.label151.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label151.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label151.Location = new System.Drawing.Point(152, 80);
            this.label151.Name = "label151";
            this.label151.Size = new System.Drawing.Size(112, 16);
            this.label151.TabIndex = 18;
            this.label151.Text = "Last Name";
            // 
            // tb_II_LI_LA_FirstName
            // 
            this.tb_II_LI_LA_FirstName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_II_LI_LA_FirstName.Location = new System.Drawing.Point(8, 96);
            this.tb_II_LI_LA_FirstName.Name = "tb_II_LI_LA_FirstName";
            this.tb_II_LI_LA_FirstName.Size = new System.Drawing.Size(136, 27);
            this.tb_II_LI_LA_FirstName.TabIndex = 17;
            // 
            // label150
            // 
            this.label150.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label150.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label150.Location = new System.Drawing.Point(8, 80);
            this.label150.Name = "label150";
            this.label150.Size = new System.Drawing.Size(112, 16);
            this.label150.TabIndex = 16;
            this.label150.Text = "First Name";
            // 
            // label149
            // 
            this.label149.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label149.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label149.Location = new System.Drawing.Point(8, 64);
            this.label149.Name = "label149";
            this.label149.Size = new System.Drawing.Size(136, 16);
            this.label149.TabIndex = 15;
            this.label149.Text = "Listing Agent";
            // 
            // tb_II_LI_ListingOffice
            // 
            this.tb_II_LI_ListingOffice.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_II_LI_ListingOffice.Location = new System.Drawing.Point(352, 24);
            this.tb_II_LI_ListingOffice.Name = "tb_II_LI_ListingOffice";
            this.tb_II_LI_ListingOffice.Size = new System.Drawing.Size(176, 27);
            this.tb_II_LI_ListingOffice.TabIndex = 14;
            // 
            // label148
            // 
            this.label148.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label148.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label148.Location = new System.Drawing.Point(352, 8);
            this.label148.Name = "label148";
            this.label148.Size = new System.Drawing.Size(112, 16);
            this.label148.TabIndex = 13;
            this.label148.Text = "Listing Office";
            // 
            // tb_II_LI_ListingMLSNumber
            // 
            this.tb_II_LI_ListingMLSNumber.AcceptsReturn = true;
            this.tb_II_LI_ListingMLSNumber.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_II_LI_ListingMLSNumber.Location = new System.Drawing.Point(200, 24);
            this.tb_II_LI_ListingMLSNumber.Name = "tb_II_LI_ListingMLSNumber";
            this.tb_II_LI_ListingMLSNumber.Size = new System.Drawing.Size(144, 27);
            this.tb_II_LI_ListingMLSNumber.TabIndex = 12;
            // 
            // label147
            // 
            this.label147.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label147.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label147.Location = new System.Drawing.Point(200, 8);
            this.label147.Name = "label147";
            this.label147.Size = new System.Drawing.Size(144, 16);
            this.label147.TabIndex = 11;
            this.label147.Text = "Listing MLS Number:";
            // 
            // tb_II_LI_Status
            // 
            this.tb_II_LI_Status.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_II_LI_Status.Location = new System.Drawing.Point(120, 24);
            this.tb_II_LI_Status.Name = "tb_II_LI_Status";
            this.tb_II_LI_Status.ReadOnly = true;
            this.tb_II_LI_Status.Size = new System.Drawing.Size(72, 27);
            this.tb_II_LI_Status.TabIndex = 10;
            // 
            // label146
            // 
            this.label146.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label146.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label146.Location = new System.Drawing.Point(120, 8);
            this.label146.Name = "label146";
            this.label146.Size = new System.Drawing.Size(72, 16);
            this.label146.TabIndex = 9;
            this.label146.Text = "Status";
            // 
            // label145
            // 
            this.label145.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label145.Location = new System.Drawing.Point(5, 26);
            this.label145.Name = "label145";
            this.label145.Size = new System.Drawing.Size(16, 23);
            this.label145.TabIndex = 8;
            this.label145.Text = "$";
            // 
            // label144
            // 
            this.label144.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label144.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label144.Location = new System.Drawing.Point(8, 8);
            this.label144.Name = "label144";
            this.label144.Size = new System.Drawing.Size(96, 16);
            this.label144.TabIndex = 0;
            this.label144.Text = "Listing Price";
            // 
            // readMe5
            // 
            this.readMe5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.readMe5.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("readMe5.BackgroundImage")));
            this.readMe5.Location = new System.Drawing.Point(880, 8);
            this.readMe5.Name = "readMe5";
            this.readMe5.Size = new System.Drawing.Size(16, 16);
            this.readMe5.TabIndex = 33;
            // 
            // tpForeClosureInfo
            // 
            this.tpForeClosureInfo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.tpForeClosureInfo.Controls.Add(this.tb_II_FI_OpeningBidAmount);
            this.tpForeClosureInfo.Controls.Add(this.tb_II_FI_UnpaidBalance);
            this.tpForeClosureInfo.Controls.Add(this.tb_II_FI_OrigMorgageBalance);
            this.tpForeClosureInfo.Controls.Add(this.tb_II_FI_EstimatedEquity);
            this.tpForeClosureInfo.Controls.Add(this.tb_II_FI_EstimatedHome);
            this.tpForeClosureInfo.Controls.Add(this.tb_II_FI_TaxAssedValue);
            this.tpForeClosureInfo.Controls.Add(this.label143);
            this.tpForeClosureInfo.Controls.Add(this.label142);
            this.tpForeClosureInfo.Controls.Add(this.label141);
            this.tpForeClosureInfo.Controls.Add(this.tb_II_FI_MortgageBeneLast);
            this.tpForeClosureInfo.Controls.Add(this.label140);
            this.tpForeClosureInfo.Controls.Add(this.tb_II_FI_MortgageBenefFirst);
            this.tpForeClosureInfo.Controls.Add(this.label139);
            this.tpForeClosureInfo.Controls.Add(this.label138);
            this.tpForeClosureInfo.Controls.Add(this.label137);
            this.tpForeClosureInfo.Controls.Add(this.tb_II_FI_RecordingDate);
            this.tpForeClosureInfo.Controls.Add(this.label136);
            this.tpForeClosureInfo.Controls.Add(this.label135);
            this.tpForeClosureInfo.Controls.Add(this.tb_II_FI_LoanRecNumber);
            this.tpForeClosureInfo.Controls.Add(this.label134);
            this.tpForeClosureInfo.Controls.Add(this.tb_II_FI_ZIP);
            this.tpForeClosureInfo.Controls.Add(this.label132);
            this.tpForeClosureInfo.Controls.Add(this.tb_II_FI_State);
            this.tpForeClosureInfo.Controls.Add(this.label131);
            this.tpForeClosureInfo.Controls.Add(this.tb_II_FI_City);
            this.tpForeClosureInfo.Controls.Add(this.label130);
            this.tpForeClosureInfo.Controls.Add(this.tb_II_FI_TrusteeFullStreetAddress);
            this.tpForeClosureInfo.Controls.Add(this.label129);
            this.tpForeClosureInfo.Controls.Add(this.tb_II_FI_TrusteeSaleNr);
            this.tpForeClosureInfo.Controls.Add(this.label128);
            this.tpForeClosureInfo.Controls.Add(this.tb_II_FI_TrusteePhoneNr);
            this.tpForeClosureInfo.Controls.Add(this.label127);
            this.tpForeClosureInfo.Controls.Add(this.tb_II_FI_TrusteeName);
            this.tpForeClosureInfo.Controls.Add(this.label126);
            this.tpForeClosureInfo.Controls.Add(this.tb_II_FI_SalePrice);
            this.tpForeClosureInfo.Controls.Add(this.label123);
            this.tpForeClosureInfo.Controls.Add(this.tb_II_FI_SaleTime);
            this.tpForeClosureInfo.Controls.Add(this.label122);
            this.tpForeClosureInfo.Controls.Add(this.tb_II_FI_Foreclosure_Sale);
            this.tpForeClosureInfo.Controls.Add(this.label121);
            this.tpForeClosureInfo.Controls.Add(this.tb_II_FI_FileNumber);
            this.tpForeClosureInfo.Controls.Add(this.label120);
            this.tpForeClosureInfo.Controls.Add(this.tb_II_FI_ForeclosureFileDate);
            this.tpForeClosureInfo.Controls.Add(this.label119);
            this.tpForeClosureInfo.Controls.Add(this.tb_II_FI_Bathrooms);
            this.tpForeClosureInfo.Controls.Add(this.label118);
            this.tpForeClosureInfo.Controls.Add(this.tb_II_FI_Bedrooms);
            this.tpForeClosureInfo.Controls.Add(this.label117);
            this.tpForeClosureInfo.Controls.Add(this.tb_II_FI_HomeSqFt);
            this.tpForeClosureInfo.Controls.Add(this.label116);
            this.tpForeClosureInfo.Controls.Add(this.tb_II_FI_YearBuilt);
            this.tpForeClosureInfo.Controls.Add(this.label115);
            this.tpForeClosureInfo.Controls.Add(this.tb_II_FI_DeedDate);
            this.tpForeClosureInfo.Controls.Add(this.label114);
            this.tpForeClosureInfo.Controls.Add(this.tb_II_FI_SaleDocNo);
            this.tpForeClosureInfo.Controls.Add(this.label113);
            this.tpForeClosureInfo.Controls.Add(this.tb_II_FI_LastSaleValue);
            this.tpForeClosureInfo.Controls.Add(this.label112);
            this.tpForeClosureInfo.Controls.Add(this.label111);
            this.tpForeClosureInfo.Controls.Add(this.label110);
            this.tpForeClosureInfo.Controls.Add(this.label109);
            this.tpForeClosureInfo.Controls.Add(this.label107);
            this.tpForeClosureInfo.Controls.Add(this.label108);
            this.tpForeClosureInfo.Controls.Add(this.tb_II_FI_OriginalTrustor);
            this.tpForeClosureInfo.Controls.Add(this.tb_II_FI_ForeclosureStatus);
            this.tpForeClosureInfo.Controls.Add(this.label105);
            this.tpForeClosureInfo.Controls.Add(this.label106);
            this.tpForeClosureInfo.Controls.Add(this.tb_II_FI_PropertyType);
            this.tpForeClosureInfo.Controls.Add(this.tb_II_FI_UseCode);
            this.tpForeClosureInfo.Controls.Add(this.tb_II_FI_APN);
            this.tpForeClosureInfo.Controls.Add(this.tb_II_FI_Subdivision);
            this.tpForeClosureInfo.Controls.Add(this.tb_II_FI_County);
            this.tpForeClosureInfo.Controls.Add(this.label104);
            this.tpForeClosureInfo.Controls.Add(this.label103);
            this.tpForeClosureInfo.Controls.Add(this.label102);
            this.tpForeClosureInfo.Controls.Add(this.label101);
            this.tpForeClosureInfo.Controls.Add(this.label100);
            this.tpForeClosureInfo.Controls.Add(this.label99);
            this.tpForeClosureInfo.Controls.Add(this.readMe4);
            this.tpForeClosureInfo.Location = new System.Drawing.Point(4, 22);
            this.tpForeClosureInfo.Name = "tpForeClosureInfo";
            this.tpForeClosureInfo.Size = new System.Drawing.Size(1304, 665);
            this.tpForeClosureInfo.TabIndex = 0;
            this.tpForeClosureInfo.Text = "Foreclosure Info";
            // 
            // tb_II_FI_OpeningBidAmount
            // 
            this.tb_II_FI_OpeningBidAmount.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_II_FI_OpeningBidAmount.Location = new System.Drawing.Point(556, 328);
            this.tb_II_FI_OpeningBidAmount.Name = "tb_II_FI_OpeningBidAmount";
            this.tb_II_FI_OpeningBidAmount.ReadOnly = true;
            this.tb_II_FI_OpeningBidAmount.Size = new System.Drawing.Size(120, 27);
            this.tb_II_FI_OpeningBidAmount.TabIndex = 75;
            // 
            // tb_II_FI_UnpaidBalance
            // 
            this.tb_II_FI_UnpaidBalance.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_II_FI_UnpaidBalance.Location = new System.Drawing.Point(424, 328);
            this.tb_II_FI_UnpaidBalance.Name = "tb_II_FI_UnpaidBalance";
            this.tb_II_FI_UnpaidBalance.ReadOnly = true;
            this.tb_II_FI_UnpaidBalance.Size = new System.Drawing.Size(112, 27);
            this.tb_II_FI_UnpaidBalance.TabIndex = 73;
            // 
            // tb_II_FI_OrigMorgageBalance
            // 
            this.tb_II_FI_OrigMorgageBalance.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_II_FI_OrigMorgageBalance.Location = new System.Drawing.Point(264, 328);
            this.tb_II_FI_OrigMorgageBalance.Name = "tb_II_FI_OrigMorgageBalance";
            this.tb_II_FI_OrigMorgageBalance.ReadOnly = true;
            this.tb_II_FI_OrigMorgageBalance.Size = new System.Drawing.Size(144, 27);
            this.tb_II_FI_OrigMorgageBalance.TabIndex = 71;
            // 
            // tb_II_FI_EstimatedEquity
            // 
            this.tb_II_FI_EstimatedEquity.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_II_FI_EstimatedEquity.Location = new System.Drawing.Point(648, 72);
            this.tb_II_FI_EstimatedEquity.Name = "tb_II_FI_EstimatedEquity";
            this.tb_II_FI_EstimatedEquity.ReadOnly = true;
            this.tb_II_FI_EstimatedEquity.Size = new System.Drawing.Size(112, 27);
            this.tb_II_FI_EstimatedEquity.TabIndex = 19;
            // 
            // tb_II_FI_EstimatedHome
            // 
            this.tb_II_FI_EstimatedHome.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_II_FI_EstimatedHome.Location = new System.Drawing.Point(488, 72);
            this.tb_II_FI_EstimatedHome.Name = "tb_II_FI_EstimatedHome";
            this.tb_II_FI_EstimatedHome.ReadOnly = true;
            this.tb_II_FI_EstimatedHome.Size = new System.Drawing.Size(136, 27);
            this.tb_II_FI_EstimatedHome.TabIndex = 18;
            // 
            // tb_II_FI_TaxAssedValue
            // 
            this.tb_II_FI_TaxAssedValue.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_II_FI_TaxAssedValue.Location = new System.Drawing.Point(488, 24);
            this.tb_II_FI_TaxAssedValue.Name = "tb_II_FI_TaxAssedValue";
            this.tb_II_FI_TaxAssedValue.ReadOnly = true;
            this.tb_II_FI_TaxAssedValue.Size = new System.Drawing.Size(136, 27);
            this.tb_II_FI_TaxAssedValue.TabIndex = 9;
            // 
            // label143
            // 
            this.label143.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label143.Location = new System.Drawing.Point(253, 328);
            this.label143.Name = "label143";
            this.label143.Size = new System.Drawing.Size(16, 24);
            this.label143.TabIndex = 81;
            this.label143.Text = "$";
            // 
            // label142
            // 
            this.label142.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label142.Location = new System.Drawing.Point(413, 330);
            this.label142.Name = "label142";
            this.label142.Size = new System.Drawing.Size(16, 24);
            this.label142.TabIndex = 80;
            this.label142.Text = "$";
            // 
            // label141
            // 
            this.label141.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label141.Location = new System.Drawing.Point(545, 330);
            this.label141.Name = "label141";
            this.label141.Size = new System.Drawing.Size(16, 24);
            this.label141.TabIndex = 79;
            this.label141.Text = "$";
            // 
            // tb_II_FI_MortgageBeneLast
            // 
            this.tb_II_FI_MortgageBeneLast.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_II_FI_MortgageBeneLast.Location = new System.Drawing.Point(264, 376);
            this.tb_II_FI_MortgageBeneLast.Name = "tb_II_FI_MortgageBeneLast";
            this.tb_II_FI_MortgageBeneLast.ReadOnly = true;
            this.tb_II_FI_MortgageBeneLast.Size = new System.Drawing.Size(168, 27);
            this.tb_II_FI_MortgageBeneLast.TabIndex = 78;
            // 
            // label140
            // 
            this.label140.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label140.Location = new System.Drawing.Point(264, 360);
            this.label140.Name = "label140";
            this.label140.Size = new System.Drawing.Size(168, 16);
            this.label140.TabIndex = 77;
            this.label140.Text = "Mortgage Beneficiary Last";
            // 
            // tb_II_FI_MortgageBenefFirst
            // 
            this.tb_II_FI_MortgageBenefFirst.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_II_FI_MortgageBenefFirst.Location = new System.Drawing.Point(8, 376);
            this.tb_II_FI_MortgageBenefFirst.Name = "tb_II_FI_MortgageBenefFirst";
            this.tb_II_FI_MortgageBenefFirst.ReadOnly = true;
            this.tb_II_FI_MortgageBenefFirst.Size = new System.Drawing.Size(168, 27);
            this.tb_II_FI_MortgageBenefFirst.TabIndex = 76;
            // 
            // label139
            // 
            this.label139.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label139.Location = new System.Drawing.Point(556, 312);
            this.label139.Name = "label139";
            this.label139.Size = new System.Drawing.Size(120, 16);
            this.label139.TabIndex = 74;
            this.label139.Text = "Opening Bid Amnt";
            // 
            // label138
            // 
            this.label138.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label138.Location = new System.Drawing.Point(424, 312);
            this.label138.Name = "label138";
            this.label138.Size = new System.Drawing.Size(112, 16);
            this.label138.TabIndex = 72;
            this.label138.Text = "Unpaid Balance";
            // 
            // label137
            // 
            this.label137.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label137.Location = new System.Drawing.Point(264, 312);
            this.label137.Name = "label137";
            this.label137.Size = new System.Drawing.Size(144, 16);
            this.label137.TabIndex = 70;
            this.label137.Text = "Orig Mortgage Balance";
            // 
            // tb_II_FI_RecordingDate
            // 
            this.tb_II_FI_RecordingDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_II_FI_RecordingDate.Location = new System.Drawing.Point(136, 328);
            this.tb_II_FI_RecordingDate.Name = "tb_II_FI_RecordingDate";
            this.tb_II_FI_RecordingDate.ReadOnly = true;
            this.tb_II_FI_RecordingDate.Size = new System.Drawing.Size(112, 27);
            this.tb_II_FI_RecordingDate.TabIndex = 69;
            // 
            // label136
            // 
            this.label136.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label136.Location = new System.Drawing.Point(136, 312);
            this.label136.Name = "label136";
            this.label136.Size = new System.Drawing.Size(112, 16);
            this.label136.TabIndex = 68;
            this.label136.Text = "Recording Date";
            // 
            // label135
            // 
            this.label135.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label135.Location = new System.Drawing.Point(8, 360);
            this.label135.Name = "label135";
            this.label135.Size = new System.Drawing.Size(168, 16);
            this.label135.TabIndex = 66;
            this.label135.Text = "Mortgage Beneficiary First";
            // 
            // tb_II_FI_LoanRecNumber
            // 
            this.tb_II_FI_LoanRecNumber.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_II_FI_LoanRecNumber.Location = new System.Drawing.Point(8, 328);
            this.tb_II_FI_LoanRecNumber.Name = "tb_II_FI_LoanRecNumber";
            this.tb_II_FI_LoanRecNumber.ReadOnly = true;
            this.tb_II_FI_LoanRecNumber.Size = new System.Drawing.Size(120, 27);
            this.tb_II_FI_LoanRecNumber.TabIndex = 65;
            // 
            // label134
            // 
            this.label134.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label134.Location = new System.Drawing.Point(8, 312);
            this.label134.Name = "label134";
            this.label134.Size = new System.Drawing.Size(120, 16);
            this.label134.TabIndex = 64;
            this.label134.Text = "Loan Rec Number";
            // 
            // tb_II_FI_ZIP
            // 
            this.tb_II_FI_ZIP.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_II_FI_ZIP.Location = new System.Drawing.Point(384, 264);
            this.tb_II_FI_ZIP.Name = "tb_II_FI_ZIP";
            this.tb_II_FI_ZIP.ReadOnly = true;
            this.tb_II_FI_ZIP.Size = new System.Drawing.Size(64, 27);
            this.tb_II_FI_ZIP.TabIndex = 62;
            // 
            // label132
            // 
            this.label132.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label132.Location = new System.Drawing.Point(384, 248);
            this.label132.Name = "label132";
            this.label132.Size = new System.Drawing.Size(48, 16);
            this.label132.TabIndex = 61;
            this.label132.Text = "ZIP";
            // 
            // tb_II_FI_State
            // 
            this.tb_II_FI_State.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_II_FI_State.Location = new System.Drawing.Point(288, 264);
            this.tb_II_FI_State.Name = "tb_II_FI_State";
            this.tb_II_FI_State.ReadOnly = true;
            this.tb_II_FI_State.Size = new System.Drawing.Size(88, 27);
            this.tb_II_FI_State.TabIndex = 60;
            // 
            // label131
            // 
            this.label131.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label131.Location = new System.Drawing.Point(288, 248);
            this.label131.Name = "label131";
            this.label131.Size = new System.Drawing.Size(48, 16);
            this.label131.TabIndex = 59;
            this.label131.Text = "State";
            // 
            // tb_II_FI_City
            // 
            this.tb_II_FI_City.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_II_FI_City.Location = new System.Drawing.Point(192, 264);
            this.tb_II_FI_City.Name = "tb_II_FI_City";
            this.tb_II_FI_City.ReadOnly = true;
            this.tb_II_FI_City.Size = new System.Drawing.Size(88, 27);
            this.tb_II_FI_City.TabIndex = 58;
            // 
            // label130
            // 
            this.label130.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label130.Location = new System.Drawing.Point(192, 248);
            this.label130.Name = "label130";
            this.label130.Size = new System.Drawing.Size(48, 16);
            this.label130.TabIndex = 57;
            this.label130.Text = "City";
            // 
            // tb_II_FI_TrusteeFullStreetAddress
            // 
            this.tb_II_FI_TrusteeFullStreetAddress.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_II_FI_TrusteeFullStreetAddress.Location = new System.Drawing.Point(8, 264);
            this.tb_II_FI_TrusteeFullStreetAddress.Name = "tb_II_FI_TrusteeFullStreetAddress";
            this.tb_II_FI_TrusteeFullStreetAddress.ReadOnly = true;
            this.tb_II_FI_TrusteeFullStreetAddress.Size = new System.Drawing.Size(176, 27);
            this.tb_II_FI_TrusteeFullStreetAddress.TabIndex = 56;
            // 
            // label129
            // 
            this.label129.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label129.Location = new System.Drawing.Point(8, 248);
            this.label129.Name = "label129";
            this.label129.Size = new System.Drawing.Size(176, 16);
            this.label129.TabIndex = 55;
            this.label129.Text = "Trustee Full Street Address";
            // 
            // tb_II_FI_TrusteeSaleNr
            // 
            this.tb_II_FI_TrusteeSaleNr.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_II_FI_TrusteeSaleNr.Location = new System.Drawing.Point(304, 216);
            this.tb_II_FI_TrusteeSaleNr.Name = "tb_II_FI_TrusteeSaleNr";
            this.tb_II_FI_TrusteeSaleNr.ReadOnly = true;
            this.tb_II_FI_TrusteeSaleNr.Size = new System.Drawing.Size(112, 27);
            this.tb_II_FI_TrusteeSaleNr.TabIndex = 54;
            // 
            // label128
            // 
            this.label128.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label128.Location = new System.Drawing.Point(304, 200);
            this.label128.Name = "label128";
            this.label128.Size = new System.Drawing.Size(112, 16);
            this.label128.TabIndex = 53;
            this.label128.Text = "Trustee Sale Nr.";
            // 
            // tb_II_FI_TrusteePhoneNr
            // 
            this.tb_II_FI_TrusteePhoneNr.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_II_FI_TrusteePhoneNr.Location = new System.Drawing.Point(176, 216);
            this.tb_II_FI_TrusteePhoneNr.Name = "tb_II_FI_TrusteePhoneNr";
            this.tb_II_FI_TrusteePhoneNr.ReadOnly = true;
            this.tb_II_FI_TrusteePhoneNr.Size = new System.Drawing.Size(120, 27);
            this.tb_II_FI_TrusteePhoneNr.TabIndex = 52;
            // 
            // label127
            // 
            this.label127.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label127.Location = new System.Drawing.Point(176, 200);
            this.label127.Name = "label127";
            this.label127.Size = new System.Drawing.Size(120, 16);
            this.label127.TabIndex = 51;
            this.label127.Text = "Trustee Phone Nr.";
            // 
            // tb_II_FI_TrusteeName
            // 
            this.tb_II_FI_TrusteeName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_II_FI_TrusteeName.Location = new System.Drawing.Point(8, 216);
            this.tb_II_FI_TrusteeName.Name = "tb_II_FI_TrusteeName";
            this.tb_II_FI_TrusteeName.ReadOnly = true;
            this.tb_II_FI_TrusteeName.Size = new System.Drawing.Size(160, 27);
            this.tb_II_FI_TrusteeName.TabIndex = 50;
            // 
            // label126
            // 
            this.label126.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label126.Location = new System.Drawing.Point(8, 200);
            this.label126.Name = "label126";
            this.label126.Size = new System.Drawing.Size(112, 16);
            this.label126.TabIndex = 49;
            this.label126.Text = "Trustee Name";
            // 
            // tb_II_FI_SalePrice
            // 
            this.tb_II_FI_SalePrice.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_II_FI_SalePrice.Location = new System.Drawing.Point(464, 168);
            this.tb_II_FI_SalePrice.Name = "tb_II_FI_SalePrice";
            this.tb_II_FI_SalePrice.ReadOnly = true;
            this.tb_II_FI_SalePrice.Size = new System.Drawing.Size(160, 27);
            this.tb_II_FI_SalePrice.TabIndex = 46;
            // 
            // label123
            // 
            this.label123.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label123.Location = new System.Drawing.Point(464, 152);
            this.label123.Name = "label123";
            this.label123.Size = new System.Drawing.Size(72, 16);
            this.label123.TabIndex = 45;
            this.label123.Text = "Sale Place";
            // 
            // tb_II_FI_SaleTime
            // 
            this.tb_II_FI_SaleTime.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_II_FI_SaleTime.Location = new System.Drawing.Point(384, 168);
            this.tb_II_FI_SaleTime.Name = "tb_II_FI_SaleTime";
            this.tb_II_FI_SaleTime.ReadOnly = true;
            this.tb_II_FI_SaleTime.Size = new System.Drawing.Size(72, 27);
            this.tb_II_FI_SaleTime.TabIndex = 44;
            // 
            // label122
            // 
            this.label122.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label122.Location = new System.Drawing.Point(384, 152);
            this.label122.Name = "label122";
            this.label122.Size = new System.Drawing.Size(72, 16);
            this.label122.TabIndex = 43;
            this.label122.Text = "Sale Time";
            // 
            // tb_II_FI_Foreclosure_Sale
            // 
            this.tb_II_FI_Foreclosure_Sale.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_II_FI_Foreclosure_Sale.Location = new System.Drawing.Point(232, 168);
            this.tb_II_FI_Foreclosure_Sale.Name = "tb_II_FI_Foreclosure_Sale";
            this.tb_II_FI_Foreclosure_Sale.ReadOnly = true;
            this.tb_II_FI_Foreclosure_Sale.Size = new System.Drawing.Size(144, 27);
            this.tb_II_FI_Foreclosure_Sale.TabIndex = 42;
            // 
            // label121
            // 
            this.label121.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label121.Location = new System.Drawing.Point(232, 152);
            this.label121.Name = "label121";
            this.label121.Size = new System.Drawing.Size(144, 16);
            this.label121.TabIndex = 41;
            this.label121.Text = "Foreclosure Sale Date";
            // 
            // tb_II_FI_FileNumber
            // 
            this.tb_II_FI_FileNumber.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_II_FI_FileNumber.Location = new System.Drawing.Point(144, 168);
            this.tb_II_FI_FileNumber.Name = "tb_II_FI_FileNumber";
            this.tb_II_FI_FileNumber.ReadOnly = true;
            this.tb_II_FI_FileNumber.Size = new System.Drawing.Size(80, 27);
            this.tb_II_FI_FileNumber.TabIndex = 40;
            // 
            // label120
            // 
            this.label120.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label120.Location = new System.Drawing.Point(144, 152);
            this.label120.Name = "label120";
            this.label120.Size = new System.Drawing.Size(80, 16);
            this.label120.TabIndex = 39;
            this.label120.Text = "File Number";
            // 
            // tb_II_FI_ForeclosureFileDate
            // 
            this.tb_II_FI_ForeclosureFileDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_II_FI_ForeclosureFileDate.Location = new System.Drawing.Point(8, 168);
            this.tb_II_FI_ForeclosureFileDate.Name = "tb_II_FI_ForeclosureFileDate";
            this.tb_II_FI_ForeclosureFileDate.ReadOnly = true;
            this.tb_II_FI_ForeclosureFileDate.Size = new System.Drawing.Size(104, 27);
            this.tb_II_FI_ForeclosureFileDate.TabIndex = 38;
            // 
            // label119
            // 
            this.label119.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label119.Location = new System.Drawing.Point(8, 152);
            this.label119.Name = "label119";
            this.label119.Size = new System.Drawing.Size(112, 16);
            this.label119.TabIndex = 37;
            this.label119.Text = "Foreclosure File Date";
            // 
            // tb_II_FI_Bathrooms
            // 
            this.tb_II_FI_Bathrooms.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_II_FI_Bathrooms.Location = new System.Drawing.Point(736, 120);
            this.tb_II_FI_Bathrooms.Name = "tb_II_FI_Bathrooms";
            this.tb_II_FI_Bathrooms.ReadOnly = true;
            this.tb_II_FI_Bathrooms.Size = new System.Drawing.Size(72, 27);
            this.tb_II_FI_Bathrooms.TabIndex = 36;
            // 
            // label118
            // 
            this.label118.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label118.Location = new System.Drawing.Point(736, 104);
            this.label118.Name = "label118";
            this.label118.Size = new System.Drawing.Size(72, 16);
            this.label118.TabIndex = 35;
            this.label118.Text = "Bathrooms";
            // 
            // tb_II_FI_Bedrooms
            // 
            this.tb_II_FI_Bedrooms.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_II_FI_Bedrooms.Location = new System.Drawing.Point(656, 120);
            this.tb_II_FI_Bedrooms.Name = "tb_II_FI_Bedrooms";
            this.tb_II_FI_Bedrooms.ReadOnly = true;
            this.tb_II_FI_Bedrooms.Size = new System.Drawing.Size(72, 27);
            this.tb_II_FI_Bedrooms.TabIndex = 34;
            // 
            // label117
            // 
            this.label117.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label117.Location = new System.Drawing.Point(656, 104);
            this.label117.Name = "label117";
            this.label117.Size = new System.Drawing.Size(72, 16);
            this.label117.TabIndex = 33;
            this.label117.Text = "Bedrooms";
            // 
            // tb_II_FI_HomeSqFt
            // 
            this.tb_II_FI_HomeSqFt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_II_FI_HomeSqFt.Location = new System.Drawing.Point(560, 120);
            this.tb_II_FI_HomeSqFt.Name = "tb_II_FI_HomeSqFt";
            this.tb_II_FI_HomeSqFt.ReadOnly = true;
            this.tb_II_FI_HomeSqFt.Size = new System.Drawing.Size(88, 27);
            this.tb_II_FI_HomeSqFt.TabIndex = 32;
            // 
            // label116
            // 
            this.label116.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label116.Location = new System.Drawing.Point(560, 104);
            this.label116.Name = "label116";
            this.label116.Size = new System.Drawing.Size(88, 16);
            this.label116.TabIndex = 31;
            this.label116.Text = "Home Sq. Ft.";
            // 
            // tb_II_FI_YearBuilt
            // 
            this.tb_II_FI_YearBuilt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_II_FI_YearBuilt.Location = new System.Drawing.Point(488, 120);
            this.tb_II_FI_YearBuilt.Name = "tb_II_FI_YearBuilt";
            this.tb_II_FI_YearBuilt.ReadOnly = true;
            this.tb_II_FI_YearBuilt.Size = new System.Drawing.Size(64, 27);
            this.tb_II_FI_YearBuilt.TabIndex = 30;
            // 
            // label115
            // 
            this.label115.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label115.Location = new System.Drawing.Point(488, 104);
            this.label115.Name = "label115";
            this.label115.Size = new System.Drawing.Size(64, 16);
            this.label115.TabIndex = 29;
            this.label115.Text = "Year Built";
            // 
            // tb_II_FI_DeedDate
            // 
            this.tb_II_FI_DeedDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_II_FI_DeedDate.Location = new System.Drawing.Point(232, 120);
            this.tb_II_FI_DeedDate.Name = "tb_II_FI_DeedDate";
            this.tb_II_FI_DeedDate.ReadOnly = true;
            this.tb_II_FI_DeedDate.Size = new System.Drawing.Size(104, 27);
            this.tb_II_FI_DeedDate.TabIndex = 28;
            // 
            // label114
            // 
            this.label114.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label114.Location = new System.Drawing.Point(232, 104);
            this.label114.Name = "label114";
            this.label114.Size = new System.Drawing.Size(104, 16);
            this.label114.TabIndex = 27;
            this.label114.Text = "Deed Date  ";
            // 
            // tb_II_FI_SaleDocNo
            // 
            this.tb_II_FI_SaleDocNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_II_FI_SaleDocNo.Location = new System.Drawing.Point(120, 120);
            this.tb_II_FI_SaleDocNo.Name = "tb_II_FI_SaleDocNo";
            this.tb_II_FI_SaleDocNo.ReadOnly = true;
            this.tb_II_FI_SaleDocNo.Size = new System.Drawing.Size(104, 27);
            this.tb_II_FI_SaleDocNo.TabIndex = 26;
            // 
            // label113
            // 
            this.label113.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label113.Location = new System.Drawing.Point(120, 104);
            this.label113.Name = "label113";
            this.label113.Size = new System.Drawing.Size(104, 16);
            this.label113.TabIndex = 25;
            this.label113.Text = "Sale Doc. No.";
            // 
            // tb_II_FI_LastSaleValue
            // 
            this.tb_II_FI_LastSaleValue.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_II_FI_LastSaleValue.Location = new System.Drawing.Point(8, 120);
            this.tb_II_FI_LastSaleValue.Name = "tb_II_FI_LastSaleValue";
            this.tb_II_FI_LastSaleValue.ReadOnly = true;
            this.tb_II_FI_LastSaleValue.Size = new System.Drawing.Size(104, 27);
            this.tb_II_FI_LastSaleValue.TabIndex = 24;
            // 
            // label112
            // 
            this.label112.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label112.Location = new System.Drawing.Point(8, 104);
            this.label112.Name = "label112";
            this.label112.Size = new System.Drawing.Size(104, 16);
            this.label112.TabIndex = 23;
            this.label112.Text = "Last Sale Value";
            // 
            // label111
            // 
            this.label111.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label111.Location = new System.Drawing.Point(632, 72);
            this.label111.Name = "label111";
            this.label111.Size = new System.Drawing.Size(16, 23);
            this.label111.TabIndex = 22;
            this.label111.Text = "$";
            // 
            // label110
            // 
            this.label110.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label110.Location = new System.Drawing.Point(475, 72);
            this.label110.Name = "label110";
            this.label110.Size = new System.Drawing.Size(16, 23);
            this.label110.TabIndex = 21;
            this.label110.Text = "$";
            // 
            // label109
            // 
            this.label109.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label109.Location = new System.Drawing.Point(475, 27);
            this.label109.Name = "label109";
            this.label109.Size = new System.Drawing.Size(16, 23);
            this.label109.TabIndex = 20;
            this.label109.Text = "$";
            // 
            // label107
            // 
            this.label107.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label107.Location = new System.Drawing.Point(648, 56);
            this.label107.Name = "label107";
            this.label107.Size = new System.Drawing.Size(112, 16);
            this.label107.TabIndex = 17;
            this.label107.Text = "Estimated Equity";
            // 
            // label108
            // 
            this.label108.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label108.Location = new System.Drawing.Point(488, 56);
            this.label108.Name = "label108";
            this.label108.Size = new System.Drawing.Size(152, 16);
            this.label108.TabIndex = 16;
            this.label108.Text = "Estimated Home Value";
            // 
            // tb_II_FI_OriginalTrustor
            // 
            this.tb_II_FI_OriginalTrustor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_II_FI_OriginalTrustor.Location = new System.Drawing.Point(160, 72);
            this.tb_II_FI_OriginalTrustor.Name = "tb_II_FI_OriginalTrustor";
            this.tb_II_FI_OriginalTrustor.ReadOnly = true;
            this.tb_II_FI_OriginalTrustor.Size = new System.Drawing.Size(216, 27);
            this.tb_II_FI_OriginalTrustor.TabIndex = 15;
            // 
            // tb_II_FI_ForeclosureStatus
            // 
            this.tb_II_FI_ForeclosureStatus.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_II_FI_ForeclosureStatus.Location = new System.Drawing.Point(8, 72);
            this.tb_II_FI_ForeclosureStatus.Name = "tb_II_FI_ForeclosureStatus";
            this.tb_II_FI_ForeclosureStatus.ReadOnly = true;
            this.tb_II_FI_ForeclosureStatus.Size = new System.Drawing.Size(144, 27);
            this.tb_II_FI_ForeclosureStatus.TabIndex = 14;
            // 
            // label105
            // 
            this.label105.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label105.Location = new System.Drawing.Point(168, 56);
            this.label105.Name = "label105";
            this.label105.Size = new System.Drawing.Size(120, 16);
            this.label105.TabIndex = 13;
            this.label105.Text = "Original Trustor:";
            // 
            // label106
            // 
            this.label106.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label106.Location = new System.Drawing.Point(8, 56);
            this.label106.Name = "label106";
            this.label106.Size = new System.Drawing.Size(144, 16);
            this.label106.TabIndex = 12;
            this.label106.Text = "Foreclosure Status:";
            // 
            // tb_II_FI_PropertyType
            // 
            this.tb_II_FI_PropertyType.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_II_FI_PropertyType.Location = new System.Drawing.Point(720, 24);
            this.tb_II_FI_PropertyType.Name = "tb_II_FI_PropertyType";
            this.tb_II_FI_PropertyType.ReadOnly = true;
            this.tb_II_FI_PropertyType.Size = new System.Drawing.Size(93, 27);
            this.tb_II_FI_PropertyType.TabIndex = 11;
            // 
            // tb_II_FI_UseCode
            // 
            this.tb_II_FI_UseCode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_II_FI_UseCode.Location = new System.Drawing.Point(632, 24);
            this.tb_II_FI_UseCode.Name = "tb_II_FI_UseCode";
            this.tb_II_FI_UseCode.ReadOnly = true;
            this.tb_II_FI_UseCode.Size = new System.Drawing.Size(80, 27);
            this.tb_II_FI_UseCode.TabIndex = 10;
            // 
            // tb_II_FI_APN
            // 
            this.tb_II_FI_APN.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_II_FI_APN.Location = new System.Drawing.Point(240, 24);
            this.tb_II_FI_APN.Name = "tb_II_FI_APN";
            this.tb_II_FI_APN.ReadOnly = true;
            this.tb_II_FI_APN.Size = new System.Drawing.Size(208, 27);
            this.tb_II_FI_APN.TabIndex = 8;
            // 
            // tb_II_FI_Subdivision
            // 
            this.tb_II_FI_Subdivision.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_II_FI_Subdivision.Location = new System.Drawing.Point(112, 24);
            this.tb_II_FI_Subdivision.Name = "tb_II_FI_Subdivision";
            this.tb_II_FI_Subdivision.ReadOnly = true;
            this.tb_II_FI_Subdivision.Size = new System.Drawing.Size(120, 27);
            this.tb_II_FI_Subdivision.TabIndex = 7;
            // 
            // tb_II_FI_County
            // 
            this.tb_II_FI_County.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_II_FI_County.Location = new System.Drawing.Point(8, 24);
            this.tb_II_FI_County.Name = "tb_II_FI_County";
            this.tb_II_FI_County.ReadOnly = true;
            this.tb_II_FI_County.Size = new System.Drawing.Size(96, 27);
            this.tb_II_FI_County.TabIndex = 6;
            // 
            // label104
            // 
            this.label104.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label104.Location = new System.Drawing.Point(632, 8);
            this.label104.Name = "label104";
            this.label104.Size = new System.Drawing.Size(64, 16);
            this.label104.TabIndex = 5;
            this.label104.Text = "Use Code";
            // 
            // label103
            // 
            this.label103.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label103.Location = new System.Drawing.Point(720, 8);
            this.label103.Name = "label103";
            this.label103.Size = new System.Drawing.Size(80, 16);
            this.label103.TabIndex = 4;
            this.label103.Text = "Prop. Type:";
            // 
            // label102
            // 
            this.label102.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label102.Location = new System.Drawing.Point(488, 8);
            this.label102.Name = "label102";
            this.label102.Size = new System.Drawing.Size(136, 16);
            this.label102.TabIndex = 3;
            this.label102.Text = "Tax Assessed Value";
            // 
            // label101
            // 
            this.label101.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label101.Location = new System.Drawing.Point(240, 8);
            this.label101.Name = "label101";
            this.label101.Size = new System.Drawing.Size(200, 16);
            this.label101.TabIndex = 2;
            this.label101.Text = "Assessor Parcel Number (APN)";
            // 
            // label100
            // 
            this.label100.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label100.Location = new System.Drawing.Point(112, 8);
            this.label100.Name = "label100";
            this.label100.Size = new System.Drawing.Size(88, 16);
            this.label100.TabIndex = 1;
            this.label100.Text = "Subdivision";
            // 
            // label99
            // 
            this.label99.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label99.Location = new System.Drawing.Point(8, 8);
            this.label99.Name = "label99";
            this.label99.Size = new System.Drawing.Size(64, 16);
            this.label99.TabIndex = 0;
            this.label99.Text = "County";
            // 
            // readMe4
            // 
            this.readMe4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.readMe4.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("readMe4.BackgroundImage")));
            this.readMe4.Location = new System.Drawing.Point(880, 8);
            this.readMe4.Name = "readMe4";
            this.readMe4.Size = new System.Drawing.Size(16, 16);
            this.readMe4.TabIndex = 82;
            // 
            // tpFinancials
            // 
            this.tpFinancials.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.tpFinancials.Controls.Add(this.tb_II_F_LoanAmount);
            this.tpFinancials.Controls.Add(this.tb_II_F_CostPerSqFt);
            this.tpFinancials.Controls.Add(this.tb_II_F_EstimatedEquity);
            this.tpFinancials.Controls.Add(this.label195);
            this.tpFinancials.Controls.Add(this.label196);
            this.tpFinancials.Controls.Add(this.tb_II_F_EstimatedHome);
            this.tpFinancials.Controls.Add(this.label193);
            this.tpFinancials.Controls.Add(this.label194);
            this.tpFinancials.Controls.Add(this.tb_II_F_CashDownPayment);
            this.tpFinancials.Controls.Add(this.label190);
            this.tpFinancials.Controls.Add(this.label191);
            this.tpFinancials.Controls.Add(this.tb_II_F_TotalLoans);
            this.tpFinancials.Controls.Add(this.label188);
            this.tpFinancials.Controls.Add(this.label189);
            this.tpFinancials.Controls.Add(this.tb_II_F_LoanAmount2);
            this.tpFinancials.Controls.Add(this.label186);
            this.tpFinancials.Controls.Add(this.label187);
            this.tpFinancials.Controls.Add(this.label185);
            this.tpFinancials.Controls.Add(this.tb_II_F_Lender);
            this.tpFinancials.Controls.Add(this.label184);
            this.tpFinancials.Controls.Add(this.label183);
            this.tpFinancials.Controls.Add(this.tb_II_F_LoanInterestRate);
            this.tpFinancials.Controls.Add(this.label182);
            this.tpFinancials.Controls.Add(this.tb_II_F_LoanType);
            this.tpFinancials.Controls.Add(this.label181);
            this.tpFinancials.Controls.Add(this.label179);
            this.tpFinancials.Controls.Add(this.label180);
            this.tpFinancials.Controls.Add(this.label178);
            this.tpFinancials.Controls.Add(this.label176);
            this.tpFinancials.Controls.Add(this.label177);
            this.tpFinancials.Controls.Add(this.tb_II_F_DeedDate);
            this.tpFinancials.Controls.Add(this.label175);
            this.tpFinancials.Controls.Add(this.tb_II_F_SaleDocumentNumber);
            this.tpFinancials.Controls.Add(this.label174);
            this.tpFinancials.Controls.Add(this.tb_II_F_SaleDate);
            this.tpFinancials.Controls.Add(this.label173);
            this.tpFinancials.Controls.Add(this.tb_II_F_SaleValue);
            this.tpFinancials.Controls.Add(this.label172);
            this.tpFinancials.Controls.Add(this.label171);
            this.tpFinancials.Controls.Add(this.readMe7);
            this.tpFinancials.Location = new System.Drawing.Point(4, 22);
            this.tpFinancials.Name = "tpFinancials";
            this.tpFinancials.Size = new System.Drawing.Size(1304, 665);
            this.tpFinancials.TabIndex = 3;
            this.tpFinancials.Text = "Financials";
            // 
            // tb_II_F_LoanAmount
            // 
            this.tb_II_F_LoanAmount.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_II_F_LoanAmount.Location = new System.Drawing.Point(24, 120);
            this.tb_II_F_LoanAmount.Name = "tb_II_F_LoanAmount";
            this.tb_II_F_LoanAmount.ReadOnly = true;
            this.tb_II_F_LoanAmount.Size = new System.Drawing.Size(104, 27);
            this.tb_II_F_LoanAmount.TabIndex = 21;
            // 
            // tb_II_F_CostPerSqFt
            // 
            this.tb_II_F_CostPerSqFt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_II_F_CostPerSqFt.Location = new System.Drawing.Point(544, 24);
            this.tb_II_F_CostPerSqFt.Name = "tb_II_F_CostPerSqFt";
            this.tb_II_F_CostPerSqFt.ReadOnly = true;
            this.tb_II_F_CostPerSqFt.Size = new System.Drawing.Size(112, 27);
            this.tb_II_F_CostPerSqFt.TabIndex = 17;
            // 
            // tb_II_F_EstimatedEquity
            // 
            this.tb_II_F_EstimatedEquity.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_II_F_EstimatedEquity.Location = new System.Drawing.Point(304, 280);
            this.tb_II_F_EstimatedEquity.Name = "tb_II_F_EstimatedEquity";
            this.tb_II_F_EstimatedEquity.ReadOnly = true;
            this.tb_II_F_EstimatedEquity.Size = new System.Drawing.Size(112, 27);
            this.tb_II_F_EstimatedEquity.TabIndex = 55;
            // 
            // label195
            // 
            this.label195.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label195.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label195.Location = new System.Drawing.Point(290, 280);
            this.label195.Name = "label195";
            this.label195.Size = new System.Drawing.Size(14, 16);
            this.label195.TabIndex = 54;
            this.label195.Text = "$";
            // 
            // label196
            // 
            this.label196.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label196.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label196.Location = new System.Drawing.Point(304, 264);
            this.label196.Name = "label196";
            this.label196.Size = new System.Drawing.Size(120, 16);
            this.label196.TabIndex = 53;
            this.label196.Text = "Estimated Equity";
            // 
            // tb_II_F_EstimatedHome
            // 
            this.tb_II_F_EstimatedHome.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_II_F_EstimatedHome.Location = new System.Drawing.Point(24, 280);
            this.tb_II_F_EstimatedHome.Name = "tb_II_F_EstimatedHome";
            this.tb_II_F_EstimatedHome.ReadOnly = true;
            this.tb_II_F_EstimatedHome.Size = new System.Drawing.Size(152, 27);
            this.tb_II_F_EstimatedHome.TabIndex = 52;
            // 
            // label193
            // 
            this.label193.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label193.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label193.Location = new System.Drawing.Point(10, 280);
            this.label193.Name = "label193";
            this.label193.Size = new System.Drawing.Size(14, 16);
            this.label193.TabIndex = 51;
            this.label193.Text = "$";
            // 
            // label194
            // 
            this.label194.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label194.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label194.Location = new System.Drawing.Point(24, 264);
            this.label194.Name = "label194";
            this.label194.Size = new System.Drawing.Size(152, 16);
            this.label194.TabIndex = 50;
            this.label194.Text = "Estimated Home Value";
            // 
            // tb_II_F_CashDownPayment
            // 
            this.tb_II_F_CashDownPayment.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_II_F_CashDownPayment.Location = new System.Drawing.Point(488, 200);
            this.tb_II_F_CashDownPayment.Name = "tb_II_F_CashDownPayment";
            this.tb_II_F_CashDownPayment.ReadOnly = true;
            this.tb_II_F_CashDownPayment.Size = new System.Drawing.Size(136, 27);
            this.tb_II_F_CashDownPayment.TabIndex = 38;
            // 
            // label190
            // 
            this.label190.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label190.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label190.Location = new System.Drawing.Point(474, 200);
            this.label190.Name = "label190";
            this.label190.Size = new System.Drawing.Size(14, 16);
            this.label190.TabIndex = 37;
            this.label190.Text = "$";
            // 
            // label191
            // 
            this.label191.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label191.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label191.Location = new System.Drawing.Point(480, 184);
            this.label191.Name = "label191";
            this.label191.Size = new System.Drawing.Size(144, 16);
            this.label191.TabIndex = 36;
            this.label191.Text = "Cash Down Payment";
            // 
            // tb_II_F_TotalLoans
            // 
            this.tb_II_F_TotalLoans.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_II_F_TotalLoans.Location = new System.Drawing.Point(304, 200);
            this.tb_II_F_TotalLoans.Name = "tb_II_F_TotalLoans";
            this.tb_II_F_TotalLoans.ReadOnly = true;
            this.tb_II_F_TotalLoans.Size = new System.Drawing.Size(112, 27);
            this.tb_II_F_TotalLoans.TabIndex = 35;
            // 
            // label188
            // 
            this.label188.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label188.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label188.Location = new System.Drawing.Point(290, 200);
            this.label188.Name = "label188";
            this.label188.Size = new System.Drawing.Size(14, 16);
            this.label188.TabIndex = 34;
            this.label188.Text = "$";
            // 
            // label189
            // 
            this.label189.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label189.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label189.Location = new System.Drawing.Point(304, 184);
            this.label189.Name = "label189";
            this.label189.Size = new System.Drawing.Size(100, 16);
            this.label189.TabIndex = 33;
            this.label189.Text = "Total Loans";
            // 
            // tb_II_F_LoanAmount2
            // 
            this.tb_II_F_LoanAmount2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_II_F_LoanAmount2.Location = new System.Drawing.Point(24, 200);
            this.tb_II_F_LoanAmount2.Name = "tb_II_F_LoanAmount2";
            this.tb_II_F_LoanAmount2.ReadOnly = true;
            this.tb_II_F_LoanAmount2.Size = new System.Drawing.Size(104, 27);
            this.tb_II_F_LoanAmount2.TabIndex = 32;
            // 
            // label186
            // 
            this.label186.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label186.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label186.Location = new System.Drawing.Point(10, 200);
            this.label186.Name = "label186";
            this.label186.Size = new System.Drawing.Size(14, 16);
            this.label186.TabIndex = 31;
            this.label186.Text = "$";
            // 
            // label187
            // 
            this.label187.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label187.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label187.Location = new System.Drawing.Point(24, 184);
            this.label187.Name = "label187";
            this.label187.Size = new System.Drawing.Size(100, 16);
            this.label187.TabIndex = 30;
            this.label187.Text = "Loan Amount";
            // 
            // label185
            // 
            this.label185.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label185.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label185.Location = new System.Drawing.Point(8, 160);
            this.label185.Name = "label185";
            this.label185.Size = new System.Drawing.Size(100, 16);
            this.label185.TabIndex = 29;
            this.label185.Text = "Second Loan";
            // 
            // tb_II_F_Lender
            // 
            this.tb_II_F_Lender.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_II_F_Lender.Location = new System.Drawing.Point(384, 120);
            this.tb_II_F_Lender.Name = "tb_II_F_Lender";
            this.tb_II_F_Lender.ReadOnly = true;
            this.tb_II_F_Lender.Size = new System.Drawing.Size(192, 27);
            this.tb_II_F_Lender.TabIndex = 28;
            // 
            // label184
            // 
            this.label184.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label184.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label184.Location = new System.Drawing.Point(384, 104);
            this.label184.Name = "label184";
            this.label184.Size = new System.Drawing.Size(64, 16);
            this.label184.TabIndex = 27;
            this.label184.Text = "Lender";
            // 
            // label183
            // 
            this.label183.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label183.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label183.Location = new System.Drawing.Point(352, 120);
            this.label183.Name = "label183";
            this.label183.Size = new System.Drawing.Size(14, 16);
            this.label183.TabIndex = 26;
            this.label183.Text = "%";
            // 
            // tb_II_F_LoanInterestRate
            // 
            this.tb_II_F_LoanInterestRate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_II_F_LoanInterestRate.Location = new System.Drawing.Point(240, 120);
            this.tb_II_F_LoanInterestRate.Name = "tb_II_F_LoanInterestRate";
            this.tb_II_F_LoanInterestRate.ReadOnly = true;
            this.tb_II_F_LoanInterestRate.Size = new System.Drawing.Size(112, 27);
            this.tb_II_F_LoanInterestRate.TabIndex = 25;
            // 
            // label182
            // 
            this.label182.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label182.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label182.Location = new System.Drawing.Point(240, 104);
            this.label182.Name = "label182";
            this.label182.Size = new System.Drawing.Size(128, 16);
            this.label182.TabIndex = 24;
            this.label182.Text = "Loan Interest Rate";
            // 
            // tb_II_F_LoanType
            // 
            this.tb_II_F_LoanType.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_II_F_LoanType.Location = new System.Drawing.Point(136, 120);
            this.tb_II_F_LoanType.Name = "tb_II_F_LoanType";
            this.tb_II_F_LoanType.ReadOnly = true;
            this.tb_II_F_LoanType.Size = new System.Drawing.Size(96, 27);
            this.tb_II_F_LoanType.TabIndex = 23;
            // 
            // label181
            // 
            this.label181.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label181.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label181.Location = new System.Drawing.Point(136, 104);
            this.label181.Name = "label181";
            this.label181.Size = new System.Drawing.Size(96, 16);
            this.label181.TabIndex = 22;
            this.label181.Text = "Loan Type";
            // 
            // label179
            // 
            this.label179.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label179.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label179.Location = new System.Drawing.Point(10, 120);
            this.label179.Name = "label179";
            this.label179.Size = new System.Drawing.Size(14, 16);
            this.label179.TabIndex = 20;
            this.label179.Text = "$";
            // 
            // label180
            // 
            this.label180.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label180.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label180.Location = new System.Drawing.Point(24, 104);
            this.label180.Name = "label180";
            this.label180.Size = new System.Drawing.Size(100, 16);
            this.label180.TabIndex = 19;
            this.label180.Text = "Loan Amount";
            // 
            // label178
            // 
            this.label178.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label178.ForeColor = System.Drawing.SystemColors.Highlight;
            this.label178.Location = new System.Drawing.Point(8, 80);
            this.label178.Name = "label178";
            this.label178.Size = new System.Drawing.Size(100, 16);
            this.label178.TabIndex = 18;
            this.label178.Text = "First Loan";
            // 
            // label176
            // 
            this.label176.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label176.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label176.Location = new System.Drawing.Point(532, 24);
            this.label176.Name = "label176";
            this.label176.Size = new System.Drawing.Size(14, 16);
            this.label176.TabIndex = 16;
            this.label176.Text = "$";
            // 
            // label177
            // 
            this.label177.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label177.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label177.Location = new System.Drawing.Point(544, 8);
            this.label177.Name = "label177";
            this.label177.Size = new System.Drawing.Size(100, 16);
            this.label177.TabIndex = 15;
            this.label177.Text = "Cost Per SqFt.";
            // 
            // tb_II_F_DeedDate
            // 
            this.tb_II_F_DeedDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_II_F_DeedDate.Location = new System.Drawing.Point(424, 24);
            this.tb_II_F_DeedDate.Name = "tb_II_F_DeedDate";
            this.tb_II_F_DeedDate.ReadOnly = true;
            this.tb_II_F_DeedDate.Size = new System.Drawing.Size(96, 27);
            this.tb_II_F_DeedDate.TabIndex = 14;
            // 
            // label175
            // 
            this.label175.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label175.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label175.Location = new System.Drawing.Point(424, 8);
            this.label175.Name = "label175";
            this.label175.Size = new System.Drawing.Size(100, 16);
            this.label175.TabIndex = 13;
            this.label175.Text = "Deed Date";
            // 
            // tb_II_F_SaleDocumentNumber
            // 
            this.tb_II_F_SaleDocumentNumber.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_II_F_SaleDocumentNumber.Location = new System.Drawing.Point(240, 24);
            this.tb_II_F_SaleDocumentNumber.Name = "tb_II_F_SaleDocumentNumber";
            this.tb_II_F_SaleDocumentNumber.ReadOnly = true;
            this.tb_II_F_SaleDocumentNumber.Size = new System.Drawing.Size(160, 27);
            this.tb_II_F_SaleDocumentNumber.TabIndex = 12;
            // 
            // label174
            // 
            this.label174.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label174.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label174.Location = new System.Drawing.Point(240, 8);
            this.label174.Name = "label174";
            this.label174.Size = new System.Drawing.Size(160, 16);
            this.label174.TabIndex = 11;
            this.label174.Text = "Sale Document Number";
            // 
            // tb_II_F_SaleDate
            // 
            this.tb_II_F_SaleDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_II_F_SaleDate.Location = new System.Drawing.Point(136, 24);
            this.tb_II_F_SaleDate.Name = "tb_II_F_SaleDate";
            this.tb_II_F_SaleDate.ReadOnly = true;
            this.tb_II_F_SaleDate.Size = new System.Drawing.Size(96, 27);
            this.tb_II_F_SaleDate.TabIndex = 10;
            // 
            // label173
            // 
            this.label173.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label173.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label173.Location = new System.Drawing.Point(136, 8);
            this.label173.Name = "label173";
            this.label173.Size = new System.Drawing.Size(100, 16);
            this.label173.TabIndex = 9;
            this.label173.Text = "Sale Date";
            // 
            // tb_II_F_SaleValue
            // 
            this.tb_II_F_SaleValue.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_II_F_SaleValue.Location = new System.Drawing.Point(22, 24);
            this.tb_II_F_SaleValue.Name = "tb_II_F_SaleValue";
            this.tb_II_F_SaleValue.ReadOnly = true;
            this.tb_II_F_SaleValue.Size = new System.Drawing.Size(106, 27);
            this.tb_II_F_SaleValue.TabIndex = 8;
            // 
            // label172
            // 
            this.label172.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label172.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label172.Location = new System.Drawing.Point(8, 24);
            this.label172.Name = "label172";
            this.label172.Size = new System.Drawing.Size(14, 16);
            this.label172.TabIndex = 1;
            this.label172.Text = "$";
            // 
            // label171
            // 
            this.label171.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label171.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label171.Location = new System.Drawing.Point(24, 8);
            this.label171.Name = "label171";
            this.label171.Size = new System.Drawing.Size(100, 16);
            this.label171.TabIndex = 0;
            this.label171.Text = "Sale Value";
            // 
            // readMe7
            // 
            this.readMe7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.readMe7.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("readMe7.BackgroundImage")));
            this.readMe7.Location = new System.Drawing.Point(880, 8);
            this.readMe7.Name = "readMe7";
            this.readMe7.Size = new System.Drawing.Size(16, 16);
            this.readMe7.TabIndex = 56;
            // 
            // tpTaxInfo
            // 
            this.tpTaxInfo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.tpTaxInfo.Controls.Add(this.tb_II_F_TaxAssessedValue);
            this.tpTaxInfo.Controls.Add(this.tb_II_F_LandValue);
            this.tpTaxInfo.Controls.Add(this.tb_II_F_TaxAmount);
            this.tpTaxInfo.Controls.Add(this.tb_II_F_FullCashValue);
            this.tpTaxInfo.Controls.Add(this.tb_II_F_OwnerAbsentOccupied);
            this.tpTaxInfo.Controls.Add(this.label217);
            this.tpTaxInfo.Controls.Add(this.label216);
            this.tpTaxInfo.Controls.Add(this.label215);
            this.tpTaxInfo.Controls.Add(this.label214);
            this.tpTaxInfo.Controls.Add(this.label213);
            this.tpTaxInfo.Controls.Add(this.label212);
            this.tpTaxInfo.Controls.Add(this.tb_II_F_PercentImprovement);
            this.tpTaxInfo.Controls.Add(this.label207);
            this.tpTaxInfo.Controls.Add(this.label208);
            this.tpTaxInfo.Controls.Add(this.label209);
            this.tpTaxInfo.Controls.Add(this.label210);
            this.tpTaxInfo.Controls.Add(this.label211);
            this.tpTaxInfo.Controls.Add(this.tb_II_F_TaxDeliquentYear);
            this.tpTaxInfo.Controls.Add(this.tb_II_F_TaxStatus);
            this.tpTaxInfo.Controls.Add(this.tb_II_F_TaxExemption);
            this.tpTaxInfo.Controls.Add(this.tb_II_F_TaxRateArea);
            this.tpTaxInfo.Controls.Add(this.tb_II_F_APN);
            this.tpTaxInfo.Controls.Add(this.label206);
            this.tpTaxInfo.Controls.Add(this.label205);
            this.tpTaxInfo.Controls.Add(this.label204);
            this.tpTaxInfo.Controls.Add(this.label203);
            this.tpTaxInfo.Controls.Add(this.label202);
            this.tpTaxInfo.Controls.Add(this.tb_II_F_MailZIP);
            this.tpTaxInfo.Controls.Add(this.tb_II_F_MailCityState);
            this.tpTaxInfo.Controls.Add(this.tb_II_TI_MailFullStreetAddress);
            this.tpTaxInfo.Controls.Add(this.label200);
            this.tpTaxInfo.Controls.Add(this.label199);
            this.tpTaxInfo.Controls.Add(this.label198);
            this.tpTaxInfo.Controls.Add(this.label197);
            this.tpTaxInfo.Controls.Add(this.readMe8);
            this.tpTaxInfo.Location = new System.Drawing.Point(4, 22);
            this.tpTaxInfo.Name = "tpTaxInfo";
            this.tpTaxInfo.Size = new System.Drawing.Size(1304, 665);
            this.tpTaxInfo.TabIndex = 4;
            this.tpTaxInfo.Text = "Tax Info";
            // 
            // tb_II_F_TaxAssessedValue
            // 
            this.tb_II_F_TaxAssessedValue.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_II_F_TaxAssessedValue.Location = new System.Drawing.Point(16, 184);
            this.tb_II_F_TaxAssessedValue.Name = "tb_II_F_TaxAssessedValue";
            this.tb_II_F_TaxAssessedValue.ReadOnly = true;
            this.tb_II_F_TaxAssessedValue.Size = new System.Drawing.Size(136, 27);
            this.tb_II_F_TaxAssessedValue.TabIndex = 66;
            // 
            // tb_II_F_LandValue
            // 
            this.tb_II_F_LandValue.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_II_F_LandValue.Location = new System.Drawing.Point(424, 184);
            this.tb_II_F_LandValue.Name = "tb_II_F_LandValue";
            this.tb_II_F_LandValue.ReadOnly = true;
            this.tb_II_F_LandValue.Size = new System.Drawing.Size(84, 27);
            this.tb_II_F_LandValue.TabIndex = 69;
            // 
            // tb_II_F_TaxAmount
            // 
            this.tb_II_F_TaxAmount.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_II_F_TaxAmount.Location = new System.Drawing.Point(304, 184);
            this.tb_II_F_TaxAmount.Name = "tb_II_F_TaxAmount";
            this.tb_II_F_TaxAmount.ReadOnly = true;
            this.tb_II_F_TaxAmount.Size = new System.Drawing.Size(96, 27);
            this.tb_II_F_TaxAmount.TabIndex = 68;
            // 
            // tb_II_F_FullCashValue
            // 
            this.tb_II_F_FullCashValue.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_II_F_FullCashValue.Location = new System.Drawing.Point(176, 184);
            this.tb_II_F_FullCashValue.Name = "tb_II_F_FullCashValue";
            this.tb_II_F_FullCashValue.ReadOnly = true;
            this.tb_II_F_FullCashValue.Size = new System.Drawing.Size(104, 27);
            this.tb_II_F_FullCashValue.TabIndex = 67;
            // 
            // tb_II_F_OwnerAbsentOccupied
            // 
            this.tb_II_F_OwnerAbsentOccupied.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_II_F_OwnerAbsentOccupied.Location = new System.Drawing.Point(16, 240);
            this.tb_II_F_OwnerAbsentOccupied.Name = "tb_II_F_OwnerAbsentOccupied";
            this.tb_II_F_OwnerAbsentOccupied.ReadOnly = true;
            this.tb_II_F_OwnerAbsentOccupied.Size = new System.Drawing.Size(160, 27);
            this.tb_II_F_OwnerAbsentOccupied.TabIndex = 77;
            // 
            // label217
            // 
            this.label217.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label217.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label217.Location = new System.Drawing.Point(16, 224);
            this.label217.Name = "label217";
            this.label217.Size = new System.Drawing.Size(160, 16);
            this.label217.TabIndex = 76;
            this.label217.Text = "Owner Absent/Occupied";
            // 
            // label216
            // 
            this.label216.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label216.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label216.Location = new System.Drawing.Point(665, 184);
            this.label216.Name = "label216";
            this.label216.Size = new System.Drawing.Size(16, 16);
            this.label216.TabIndex = 75;
            this.label216.Text = "%";
            // 
            // label215
            // 
            this.label215.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label215.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label215.Location = new System.Drawing.Point(5, 184);
            this.label215.Name = "label215";
            this.label215.Size = new System.Drawing.Size(10, 16);
            this.label215.TabIndex = 74;
            this.label215.Text = "$";
            // 
            // label214
            // 
            this.label214.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label214.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label214.Location = new System.Drawing.Point(164, 184);
            this.label214.Name = "label214";
            this.label214.Size = new System.Drawing.Size(10, 16);
            this.label214.TabIndex = 73;
            this.label214.Text = "$";
            // 
            // label213
            // 
            this.label213.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label213.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label213.Location = new System.Drawing.Point(291, 184);
            this.label213.Name = "label213";
            this.label213.Size = new System.Drawing.Size(16, 16);
            this.label213.TabIndex = 72;
            this.label213.Text = "$";
            // 
            // label212
            // 
            this.label212.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label212.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label212.Location = new System.Drawing.Point(411, 184);
            this.label212.Name = "label212";
            this.label212.Size = new System.Drawing.Size(16, 16);
            this.label212.TabIndex = 71;
            this.label212.Text = "$";
            // 
            // tb_II_F_PercentImprovement
            // 
            this.tb_II_F_PercentImprovement.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_II_F_PercentImprovement.Location = new System.Drawing.Point(520, 184);
            this.tb_II_F_PercentImprovement.Name = "tb_II_F_PercentImprovement";
            this.tb_II_F_PercentImprovement.ReadOnly = true;
            this.tb_II_F_PercentImprovement.Size = new System.Drawing.Size(144, 27);
            this.tb_II_F_PercentImprovement.TabIndex = 70;
            this.tb_II_F_PercentImprovement.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label207
            // 
            this.label207.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label207.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label207.Location = new System.Drawing.Point(520, 168);
            this.label207.Name = "label207";
            this.label207.Size = new System.Drawing.Size(152, 16);
            this.label207.TabIndex = 65;
            this.label207.Text = "Percent Improvement";
            // 
            // label208
            // 
            this.label208.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label208.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label208.Location = new System.Drawing.Point(304, 168);
            this.label208.Name = "label208";
            this.label208.Size = new System.Drawing.Size(88, 16);
            this.label208.TabIndex = 64;
            this.label208.Text = "Tax Amount";
            // 
            // label209
            // 
            this.label209.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label209.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label209.Location = new System.Drawing.Point(176, 168);
            this.label209.Name = "label209";
            this.label209.Size = new System.Drawing.Size(112, 16);
            this.label209.TabIndex = 63;
            this.label209.Text = "Full Cash Value";
            // 
            // label210
            // 
            this.label210.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label210.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label210.Location = new System.Drawing.Point(424, 168);
            this.label210.Name = "label210";
            this.label210.Size = new System.Drawing.Size(88, 16);
            this.label210.TabIndex = 62;
            this.label210.Text = "Land Value";
            // 
            // label211
            // 
            this.label211.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label211.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label211.Location = new System.Drawing.Point(16, 168);
            this.label211.Name = "label211";
            this.label211.Size = new System.Drawing.Size(136, 16);
            this.label211.TabIndex = 61;
            this.label211.Text = "Tax Assessed Value";
            // 
            // tb_II_F_TaxDeliquentYear
            // 
            this.tb_II_F_TaxDeliquentYear.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_II_F_TaxDeliquentYear.Location = new System.Drawing.Point(592, 136);
            this.tb_II_F_TaxDeliquentYear.Name = "tb_II_F_TaxDeliquentYear";
            this.tb_II_F_TaxDeliquentYear.ReadOnly = true;
            this.tb_II_F_TaxDeliquentYear.Size = new System.Drawing.Size(128, 27);
            this.tb_II_F_TaxDeliquentYear.TabIndex = 60;
            // 
            // tb_II_F_TaxStatus
            // 
            this.tb_II_F_TaxStatus.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_II_F_TaxStatus.Location = new System.Drawing.Point(504, 136);
            this.tb_II_F_TaxStatus.Name = "tb_II_F_TaxStatus";
            this.tb_II_F_TaxStatus.ReadOnly = true;
            this.tb_II_F_TaxStatus.Size = new System.Drawing.Size(84, 27);
            this.tb_II_F_TaxStatus.TabIndex = 59;
            // 
            // tb_II_F_TaxExemption
            // 
            this.tb_II_F_TaxExemption.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_II_F_TaxExemption.Location = new System.Drawing.Point(368, 136);
            this.tb_II_F_TaxExemption.Name = "tb_II_F_TaxExemption";
            this.tb_II_F_TaxExemption.ReadOnly = true;
            this.tb_II_F_TaxExemption.Size = new System.Drawing.Size(128, 27);
            this.tb_II_F_TaxExemption.TabIndex = 58;
            // 
            // tb_II_F_TaxRateArea
            // 
            this.tb_II_F_TaxRateArea.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_II_F_TaxRateArea.Location = new System.Drawing.Point(232, 136);
            this.tb_II_F_TaxRateArea.Name = "tb_II_F_TaxRateArea";
            this.tb_II_F_TaxRateArea.ReadOnly = true;
            this.tb_II_F_TaxRateArea.Size = new System.Drawing.Size(128, 27);
            this.tb_II_F_TaxRateArea.TabIndex = 57;
            // 
            // tb_II_F_APN
            // 
            this.tb_II_F_APN.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_II_F_APN.Location = new System.Drawing.Point(16, 136);
            this.tb_II_F_APN.Name = "tb_II_F_APN";
            this.tb_II_F_APN.ReadOnly = true;
            this.tb_II_F_APN.Size = new System.Drawing.Size(208, 27);
            this.tb_II_F_APN.TabIndex = 56;
            // 
            // label206
            // 
            this.label206.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label206.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label206.Location = new System.Drawing.Point(592, 120);
            this.label206.Name = "label206";
            this.label206.Size = new System.Drawing.Size(136, 16);
            this.label206.TabIndex = 55;
            this.label206.Text = "Tax Deliquent Year";
            // 
            // label205
            // 
            this.label205.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label205.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label205.Location = new System.Drawing.Point(368, 120);
            this.label205.Name = "label205";
            this.label205.Size = new System.Drawing.Size(112, 16);
            this.label205.TabIndex = 54;
            this.label205.Text = "Tax Exemption";
            // 
            // label204
            // 
            this.label204.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label204.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label204.Location = new System.Drawing.Point(232, 120);
            this.label204.Name = "label204";
            this.label204.Size = new System.Drawing.Size(104, 16);
            this.label204.TabIndex = 53;
            this.label204.Text = "Tax Rate Area";
            // 
            // label203
            // 
            this.label203.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label203.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label203.Location = new System.Drawing.Point(504, 120);
            this.label203.Name = "label203";
            this.label203.Size = new System.Drawing.Size(80, 16);
            this.label203.TabIndex = 52;
            this.label203.Text = "Tax Status";
            // 
            // label202
            // 
            this.label202.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label202.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label202.Location = new System.Drawing.Point(16, 120);
            this.label202.Name = "label202";
            this.label202.Size = new System.Drawing.Size(208, 16);
            this.label202.TabIndex = 51;
            this.label202.Text = "Assessor Parcel Number (APN)";
            // 
            // tb_II_F_MailZIP
            // 
            this.tb_II_F_MailZIP.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_II_F_MailZIP.Location = new System.Drawing.Point(392, 56);
            this.tb_II_F_MailZIP.Name = "tb_II_F_MailZIP";
            this.tb_II_F_MailZIP.ReadOnly = true;
            this.tb_II_F_MailZIP.Size = new System.Drawing.Size(120, 27);
            this.tb_II_F_MailZIP.TabIndex = 11;
            // 
            // tb_II_F_MailCityState
            // 
            this.tb_II_F_MailCityState.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_II_F_MailCityState.Location = new System.Drawing.Point(224, 56);
            this.tb_II_F_MailCityState.Name = "tb_II_F_MailCityState";
            this.tb_II_F_MailCityState.ReadOnly = true;
            this.tb_II_F_MailCityState.Size = new System.Drawing.Size(160, 27);
            this.tb_II_F_MailCityState.TabIndex = 10;
            // 
            // tb_II_TI_MailFullStreetAddress
            // 
            this.tb_II_TI_MailFullStreetAddress.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_II_TI_MailFullStreetAddress.Location = new System.Drawing.Point(8, 56);
            this.tb_II_TI_MailFullStreetAddress.Name = "tb_II_TI_MailFullStreetAddress";
            this.tb_II_TI_MailFullStreetAddress.ReadOnly = true;
            this.tb_II_TI_MailFullStreetAddress.Size = new System.Drawing.Size(208, 27);
            this.tb_II_TI_MailFullStreetAddress.TabIndex = 9;
            // 
            // label200
            // 
            this.label200.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label200.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label200.Location = new System.Drawing.Point(392, 40);
            this.label200.Name = "label200";
            this.label200.Size = new System.Drawing.Size(100, 16);
            this.label200.TabIndex = 4;
            this.label200.Text = "Mail ZIP";
            // 
            // label199
            // 
            this.label199.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label199.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label199.Location = new System.Drawing.Point(224, 40);
            this.label199.Name = "label199";
            this.label199.Size = new System.Drawing.Size(104, 16);
            this.label199.TabIndex = 3;
            this.label199.Text = "Mail City/St";
            // 
            // label198
            // 
            this.label198.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label198.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label198.Location = new System.Drawing.Point(8, 40);
            this.label198.Name = "label198";
            this.label198.Size = new System.Drawing.Size(176, 16);
            this.label198.TabIndex = 2;
            this.label198.Text = "Mail Full Street Address*";
            // 
            // label197
            // 
            this.label197.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label197.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label197.Location = new System.Drawing.Point(8, 8);
            this.label197.Name = "label197";
            this.label197.Size = new System.Drawing.Size(112, 16);
            this.label197.TabIndex = 1;
            this.label197.Text = "Mailing Address:";
            // 
            // readMe8
            // 
            this.readMe8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.readMe8.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("readMe8.BackgroundImage")));
            this.readMe8.Location = new System.Drawing.Point(880, 8);
            this.readMe8.Name = "readMe8";
            this.readMe8.Size = new System.Drawing.Size(16, 16);
            this.readMe8.TabIndex = 78;
            // 
            // tpOtherInfo
            // 
            this.tpOtherInfo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.tpOtherInfo.Controls.Add(this.readMe9);
            this.tpOtherInfo.Controls.Add(this.tb_II_OU_UF5);
            this.tpOtherInfo.Controls.Add(this.tb_II_OU_UF4);
            this.tpOtherInfo.Controls.Add(this.tb_II_OU_UF3);
            this.tpOtherInfo.Controls.Add(this.tb_II_OU_UF2);
            this.tpOtherInfo.Controls.Add(this.tb_II_OU_UF1);
            this.tpOtherInfo.Controls.Add(this.tb_II_OU_DoNotCallFlag);
            this.tpOtherInfo.Controls.Add(this.tb_II_OU_PrivacyFlag);
            this.tpOtherInfo.Controls.Add(this.tb_II_OU_MailScore);
            this.tpOtherInfo.Controls.Add(this.tb_II_OU_MailCRRT);
            this.tpOtherInfo.Controls.Add(this.tb_II_OU_DivorceDate);
            this.tpOtherInfo.Controls.Add(this.tb_II_OU_DivorceStatus);
            this.tpOtherInfo.Controls.Add(this.tb_II_OU_MapArea);
            this.tpOtherInfo.Controls.Add(this.tb_II_OU_MapGrid);
            this.tpOtherInfo.Controls.Add(this.tb_II_OU_Vesting);
            this.tpOtherInfo.Controls.Add(this.tb_II_OU_LegalDescription);
            this.tpOtherInfo.Controls.Add(this.tb_II_OU_Subdividion);
            this.tpOtherInfo.Controls.Add(this.tb_II_OI_County);
            this.tpOtherInfo.Controls.Add(this.label234);
            this.tpOtherInfo.Controls.Add(this.label233);
            this.tpOtherInfo.Controls.Add(this.label232);
            this.tpOtherInfo.Controls.Add(this.label231);
            this.tpOtherInfo.Controls.Add(this.label230);
            this.tpOtherInfo.Controls.Add(this.label229);
            this.tpOtherInfo.Controls.Add(this.label228);
            this.tpOtherInfo.Controls.Add(this.label227);
            this.tpOtherInfo.Controls.Add(this.label226);
            this.tpOtherInfo.Controls.Add(this.label225);
            this.tpOtherInfo.Controls.Add(this.label224);
            this.tpOtherInfo.Controls.Add(this.label223);
            this.tpOtherInfo.Controls.Add(this.label222);
            this.tpOtherInfo.Controls.Add(this.label221);
            this.tpOtherInfo.Controls.Add(this.label220);
            this.tpOtherInfo.Controls.Add(this.label219);
            this.tpOtherInfo.Controls.Add(this.label218);
            this.tpOtherInfo.Location = new System.Drawing.Point(4, 22);
            this.tpOtherInfo.Name = "tpOtherInfo";
            this.tpOtherInfo.Size = new System.Drawing.Size(1304, 665);
            this.tpOtherInfo.TabIndex = 5;
            this.tpOtherInfo.Text = "Other Info";
            // 
            // readMe9
            // 
            this.readMe9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.readMe9.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("readMe9.BackgroundImage")));
            this.readMe9.Location = new System.Drawing.Point(880, 8);
            this.readMe9.Name = "readMe9";
            this.readMe9.Size = new System.Drawing.Size(16, 16);
            this.readMe9.TabIndex = 36;
            // 
            // tb_II_OU_UF5
            // 
            this.tb_II_OU_UF5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_II_OU_UF5.Location = new System.Drawing.Point(344, 336);
            this.tb_II_OU_UF5.Name = "tb_II_OU_UF5";
            this.tb_II_OU_UF5.ReadOnly = true;
            this.tb_II_OU_UF5.Size = new System.Drawing.Size(184, 27);
            this.tb_II_OU_UF5.TabIndex = 35;
            this.tb_II_OU_UF5.Visible = false;
            // 
            // tb_II_OU_UF4
            // 
            this.tb_II_OU_UF4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_II_OU_UF4.Location = new System.Drawing.Point(344, 312);
            this.tb_II_OU_UF4.Name = "tb_II_OU_UF4";
            this.tb_II_OU_UF4.ReadOnly = true;
            this.tb_II_OU_UF4.Size = new System.Drawing.Size(184, 27);
            this.tb_II_OU_UF4.TabIndex = 34;
            this.tb_II_OU_UF4.Visible = false;
            // 
            // tb_II_OU_UF3
            // 
            this.tb_II_OU_UF3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_II_OU_UF3.Location = new System.Drawing.Point(80, 360);
            this.tb_II_OU_UF3.Name = "tb_II_OU_UF3";
            this.tb_II_OU_UF3.ReadOnly = true;
            this.tb_II_OU_UF3.Size = new System.Drawing.Size(184, 27);
            this.tb_II_OU_UF3.TabIndex = 33;
            this.tb_II_OU_UF3.Visible = false;
            // 
            // tb_II_OU_UF2
            // 
            this.tb_II_OU_UF2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_II_OU_UF2.Location = new System.Drawing.Point(80, 336);
            this.tb_II_OU_UF2.Name = "tb_II_OU_UF2";
            this.tb_II_OU_UF2.ReadOnly = true;
            this.tb_II_OU_UF2.Size = new System.Drawing.Size(184, 27);
            this.tb_II_OU_UF2.TabIndex = 32;
            this.tb_II_OU_UF2.Visible = false;
            // 
            // tb_II_OU_UF1
            // 
            this.tb_II_OU_UF1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_II_OU_UF1.Location = new System.Drawing.Point(80, 312);
            this.tb_II_OU_UF1.Name = "tb_II_OU_UF1";
            this.tb_II_OU_UF1.ReadOnly = true;
            this.tb_II_OU_UF1.Size = new System.Drawing.Size(184, 27);
            this.tb_II_OU_UF1.TabIndex = 31;
            this.tb_II_OU_UF1.Visible = false;
            // 
            // tb_II_OU_DoNotCallFlag
            // 
            this.tb_II_OU_DoNotCallFlag.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_II_OU_DoNotCallFlag.Location = new System.Drawing.Point(392, 264);
            this.tb_II_OU_DoNotCallFlag.Name = "tb_II_OU_DoNotCallFlag";
            this.tb_II_OU_DoNotCallFlag.ReadOnly = true;
            this.tb_II_OU_DoNotCallFlag.Size = new System.Drawing.Size(136, 27);
            this.tb_II_OU_DoNotCallFlag.TabIndex = 30;
            // 
            // tb_II_OU_PrivacyFlag
            // 
            this.tb_II_OU_PrivacyFlag.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_II_OU_PrivacyFlag.Location = new System.Drawing.Point(264, 264);
            this.tb_II_OU_PrivacyFlag.Name = "tb_II_OU_PrivacyFlag";
            this.tb_II_OU_PrivacyFlag.ReadOnly = true;
            this.tb_II_OU_PrivacyFlag.Size = new System.Drawing.Size(120, 27);
            this.tb_II_OU_PrivacyFlag.TabIndex = 29;
            // 
            // tb_II_OU_MailScore
            // 
            this.tb_II_OU_MailScore.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_II_OU_MailScore.Location = new System.Drawing.Point(136, 264);
            this.tb_II_OU_MailScore.Name = "tb_II_OU_MailScore";
            this.tb_II_OU_MailScore.ReadOnly = true;
            this.tb_II_OU_MailScore.Size = new System.Drawing.Size(120, 27);
            this.tb_II_OU_MailScore.TabIndex = 28;
            // 
            // tb_II_OU_MailCRRT
            // 
            this.tb_II_OU_MailCRRT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_II_OU_MailCRRT.Location = new System.Drawing.Point(8, 264);
            this.tb_II_OU_MailCRRT.Name = "tb_II_OU_MailCRRT";
            this.tb_II_OU_MailCRRT.ReadOnly = true;
            this.tb_II_OU_MailCRRT.Size = new System.Drawing.Size(120, 27);
            this.tb_II_OU_MailCRRT.TabIndex = 27;
            // 
            // tb_II_OU_DivorceDate
            // 
            this.tb_II_OU_DivorceDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_II_OU_DivorceDate.Location = new System.Drawing.Point(136, 216);
            this.tb_II_OU_DivorceDate.Name = "tb_II_OU_DivorceDate";
            this.tb_II_OU_DivorceDate.ReadOnly = true;
            this.tb_II_OU_DivorceDate.Size = new System.Drawing.Size(120, 27);
            this.tb_II_OU_DivorceDate.TabIndex = 26;
            // 
            // tb_II_OU_DivorceStatus
            // 
            this.tb_II_OU_DivorceStatus.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_II_OU_DivorceStatus.Location = new System.Drawing.Point(8, 216);
            this.tb_II_OU_DivorceStatus.Name = "tb_II_OU_DivorceStatus";
            this.tb_II_OU_DivorceStatus.ReadOnly = true;
            this.tb_II_OU_DivorceStatus.Size = new System.Drawing.Size(120, 27);
            this.tb_II_OU_DivorceStatus.TabIndex = 25;
            // 
            // tb_II_OU_MapArea
            // 
            this.tb_II_OU_MapArea.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_II_OU_MapArea.Location = new System.Drawing.Point(136, 168);
            this.tb_II_OU_MapArea.Name = "tb_II_OU_MapArea";
            this.tb_II_OU_MapArea.ReadOnly = true;
            this.tb_II_OU_MapArea.Size = new System.Drawing.Size(120, 27);
            this.tb_II_OU_MapArea.TabIndex = 24;
            // 
            // tb_II_OU_MapGrid
            // 
            this.tb_II_OU_MapGrid.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_II_OU_MapGrid.Location = new System.Drawing.Point(8, 168);
            this.tb_II_OU_MapGrid.Name = "tb_II_OU_MapGrid";
            this.tb_II_OU_MapGrid.ReadOnly = true;
            this.tb_II_OU_MapGrid.Size = new System.Drawing.Size(120, 27);
            this.tb_II_OU_MapGrid.TabIndex = 23;
            // 
            // tb_II_OU_Vesting
            // 
            this.tb_II_OU_Vesting.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_II_OU_Vesting.Location = new System.Drawing.Point(8, 120);
            this.tb_II_OU_Vesting.Name = "tb_II_OU_Vesting";
            this.tb_II_OU_Vesting.ReadOnly = true;
            this.tb_II_OU_Vesting.Size = new System.Drawing.Size(120, 27);
            this.tb_II_OU_Vesting.TabIndex = 22;
            // 
            // tb_II_OU_LegalDescription
            // 
            this.tb_II_OU_LegalDescription.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_II_OU_LegalDescription.Location = new System.Drawing.Point(8, 72);
            this.tb_II_OU_LegalDescription.Name = "tb_II_OU_LegalDescription";
            this.tb_II_OU_LegalDescription.ReadOnly = true;
            this.tb_II_OU_LegalDescription.Size = new System.Drawing.Size(880, 27);
            this.tb_II_OU_LegalDescription.TabIndex = 21;
            // 
            // tb_II_OU_Subdividion
            // 
            this.tb_II_OU_Subdividion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_II_OU_Subdividion.Location = new System.Drawing.Point(280, 24);
            this.tb_II_OU_Subdividion.Name = "tb_II_OU_Subdividion";
            this.tb_II_OU_Subdividion.ReadOnly = true;
            this.tb_II_OU_Subdividion.Size = new System.Drawing.Size(120, 27);
            this.tb_II_OU_Subdividion.TabIndex = 20;
            // 
            // tb_II_OI_County
            // 
            this.tb_II_OI_County.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_II_OI_County.Location = new System.Drawing.Point(8, 24);
            this.tb_II_OI_County.Name = "tb_II_OI_County";
            this.tb_II_OI_County.ReadOnly = true;
            this.tb_II_OI_County.Size = new System.Drawing.Size(120, 27);
            this.tb_II_OI_County.TabIndex = 19;
            // 
            // label234
            // 
            this.label234.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label234.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label234.Location = new System.Drawing.Point(272, 336);
            this.label234.Name = "label234";
            this.label234.Size = new System.Drawing.Size(72, 16);
            this.label234.TabIndex = 18;
            this.label234.Text = "User Field 5";
            this.label234.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label234.Visible = false;
            // 
            // label233
            // 
            this.label233.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label233.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label233.Location = new System.Drawing.Point(272, 312);
            this.label233.Name = "label233";
            this.label233.Size = new System.Drawing.Size(72, 16);
            this.label233.TabIndex = 17;
            this.label233.Text = "User Field 4";
            this.label233.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label233.Visible = false;
            // 
            // label232
            // 
            this.label232.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label232.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label232.Location = new System.Drawing.Point(8, 360);
            this.label232.Name = "label232";
            this.label232.Size = new System.Drawing.Size(72, 16);
            this.label232.TabIndex = 16;
            this.label232.Text = "User Field 3";
            this.label232.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label232.Visible = false;
            // 
            // label231
            // 
            this.label231.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label231.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label231.Location = new System.Drawing.Point(8, 336);
            this.label231.Name = "label231";
            this.label231.Size = new System.Drawing.Size(72, 16);
            this.label231.TabIndex = 15;
            this.label231.Text = "User Field 2";
            this.label231.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label231.Visible = false;
            // 
            // label230
            // 
            this.label230.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label230.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label230.Location = new System.Drawing.Point(8, 312);
            this.label230.Name = "label230";
            this.label230.Size = new System.Drawing.Size(72, 16);
            this.label230.TabIndex = 14;
            this.label230.Text = "User Field 1";
            this.label230.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label230.Visible = false;
            // 
            // label229
            // 
            this.label229.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label229.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label229.Location = new System.Drawing.Point(392, 248);
            this.label229.Name = "label229";
            this.label229.Size = new System.Drawing.Size(128, 16);
            this.label229.TabIndex = 13;
            this.label229.Text = "Do Not Call Flag";
            this.label229.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label228
            // 
            this.label228.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label228.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label228.Location = new System.Drawing.Point(272, 248);
            this.label228.Name = "label228";
            this.label228.Size = new System.Drawing.Size(96, 16);
            this.label228.TabIndex = 12;
            this.label228.Text = "Privacy Flag";
            this.label228.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label227
            // 
            this.label227.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label227.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label227.Location = new System.Drawing.Point(136, 248);
            this.label227.Name = "label227";
            this.label227.Size = new System.Drawing.Size(80, 16);
            this.label227.TabIndex = 11;
            this.label227.Text = "Mail Score";
            this.label227.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label226
            // 
            this.label226.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label226.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label226.Location = new System.Drawing.Point(8, 248);
            this.label226.Name = "label226";
            this.label226.Size = new System.Drawing.Size(80, 16);
            this.label226.TabIndex = 10;
            this.label226.Text = "Mail CRRT";
            this.label226.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label225
            // 
            this.label225.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label225.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label225.Location = new System.Drawing.Point(136, 200);
            this.label225.Name = "label225";
            this.label225.Size = new System.Drawing.Size(104, 16);
            this.label225.TabIndex = 9;
            this.label225.Text = "Divorce Date";
            this.label225.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label224
            // 
            this.label224.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label224.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label224.Location = new System.Drawing.Point(8, 200);
            this.label224.Name = "label224";
            this.label224.Size = new System.Drawing.Size(112, 16);
            this.label224.TabIndex = 8;
            this.label224.Text = "Divorce Status";
            this.label224.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label223
            // 
            this.label223.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label223.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label223.Location = new System.Drawing.Point(136, 152);
            this.label223.Name = "label223";
            this.label223.Size = new System.Drawing.Size(72, 16);
            this.label223.TabIndex = 7;
            this.label223.Text = "Map Area";
            this.label223.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label222
            // 
            this.label222.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label222.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label222.Location = new System.Drawing.Point(8, 152);
            this.label222.Name = "label222";
            this.label222.Size = new System.Drawing.Size(72, 16);
            this.label222.TabIndex = 6;
            this.label222.Text = "Map Grid";
            this.label222.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label221
            // 
            this.label221.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label221.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label221.Location = new System.Drawing.Point(8, 104);
            this.label221.Name = "label221";
            this.label221.Size = new System.Drawing.Size(72, 16);
            this.label221.TabIndex = 5;
            this.label221.Text = "Vesting";
            // 
            // label220
            // 
            this.label220.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label220.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label220.Location = new System.Drawing.Point(8, 56);
            this.label220.Name = "label220";
            this.label220.Size = new System.Drawing.Size(120, 16);
            this.label220.TabIndex = 4;
            this.label220.Text = "Legal Description";
            // 
            // label219
            // 
            this.label219.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label219.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label219.Location = new System.Drawing.Point(280, 8);
            this.label219.Name = "label219";
            this.label219.Size = new System.Drawing.Size(88, 16);
            this.label219.TabIndex = 3;
            this.label219.Text = "Subdivision";
            // 
            // label218
            // 
            this.label218.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label218.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label218.Location = new System.Drawing.Point(8, 8);
            this.label218.Name = "label218";
            this.label218.Size = new System.Drawing.Size(64, 16);
            this.label218.TabIndex = 2;
            this.label218.Text = "County";
            // 
            // tpCommercial
            // 
            this.tpCommercial.Controls.Add(this._commercialControl);
            this.tpCommercial.Location = new System.Drawing.Point(4, 28);
            this.tpCommercial.Name = "tpCommercial";
            this.tpCommercial.Size = new System.Drawing.Size(192, 68);
            this.tpCommercial.TabIndex = 8;
            this.tpCommercial.Text = "Commercial Info";
            this.tpCommercial.UseVisualStyleBackColor = true;
            // 
            // _commercialControl
            // 
            this._commercialControl.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this._commercialControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this._commercialControl.Location = new System.Drawing.Point(0, 0);
            this._commercialControl.Name = "_commercialControl";
            this._commercialControl.Size = new System.Drawing.Size(192, 68);
            this._commercialControl.TabIndex = 0;
            // 
            // upperPanel
            // 
            this.upperPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.upperPanel.Controls.Add(this.panel15);
            this.upperPanel.Controls.Add(this.panel14);
            this.upperPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.upperPanel.Location = new System.Drawing.Point(0, 0);
            this.upperPanel.Name = "upperPanel";
            this.upperPanel.Size = new System.Drawing.Size(1320, 157);
            this.upperPanel.TabIndex = 1;
            // 
            // _pbHouse
            // 
            this._pbHouse.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this._pbHouse.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._pbHouse.Image = ((System.Drawing.Image)(resources.GetObject("_pbHouse.Image")));
            this._pbHouse.InitialImage = ((System.Drawing.Image)(resources.GetObject("_pbHouse.InitialImage")));
            this._pbHouse.Location = new System.Drawing.Point(5, 11);
            this._pbHouse.Name = "_pbHouse";
            this._pbHouse.Size = new System.Drawing.Size(152, 114);
            this._pbHouse.TabIndex = 72;
            this._pbHouse.TabStop = false;
            // 
            // tbStatusGroup
            // 
            this.tbStatusGroup.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbStatusGroup.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbStatusGroup.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.tbStatusGroup.Location = new System.Drawing.Point(566, 3);
            this.tbStatusGroup.Name = "tbStatusGroup";
            this.tbStatusGroup.ReadOnly = true;
            this.tbStatusGroup.Size = new System.Drawing.Size(235, 27);
            this.tbStatusGroup.TabIndex = 7;
            this.tbStatusGroup.TabStop = false;
            // 
            // _unLockPI
            // 
            this._unLockPI.Enabled = false;
            this._unLockPI.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._unLockPI.Location = new System.Drawing.Point(807, 31);
            this._unLockPI.Name = "_unLockPI";
            this._unLockPI.Size = new System.Drawing.Size(48, 23);
            this._unLockPI.TabIndex = 71;
            this._unLockPI.Text = "Unlock";
            this._unLockPI.Visible = false;
            // 
            // pbMap
            // 
            this.pbMap.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pbMap.Image = ((System.Drawing.Image)(resources.GetObject("pbMap.Image")));
            this.pbMap.Location = new System.Drawing.Point(1105, 5);
            this.pbMap.Name = "pbMap";
            this.pbMap.Size = new System.Drawing.Size(46, 49);
            this.pbMap.TabIndex = 70;
            this.pbMap.TabStop = false;
            this.ttMap.SetToolTip(this.pbMap, "Click here to get map");
            this.pbMap.Click += new System.EventHandler(this.ShowMap);
            // 
            // label133
            // 
            this.label133.AutoSize = true;
            this.label133.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.label133.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label133.Location = new System.Drawing.Point(535, 97);
            this.label133.Name = "label133";
            this.label133.Size = new System.Drawing.Size(96, 21);
            this.label133.TabIndex = 69;
            this.label133.Text = "City, State:";
            // 
            // tbSiteCityState
            // 
            this.tbSiteCityState.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.tbSiteCityState.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbSiteCityState.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.tbSiteCityState.Location = new System.Drawing.Point(536, 3);
            this.tbSiteCityState.Name = "tbSiteCityState";
            this.tbSiteCityState.ReadOnly = true;
            this.tbSiteCityState.Size = new System.Drawing.Size(140, 27);
            this.tbSiteCityState.TabIndex = 3;
            this.tbSiteCityState.TabStop = false;
            // 
            // tbSiteState
            // 
            this.tbSiteState.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.tbSiteState.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbSiteState.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.tbSiteState.Location = new System.Drawing.Point(470, 3);
            this.tbSiteState.Name = "tbSiteState";
            this.tbSiteState.Size = new System.Drawing.Size(60, 27);
            this.tbSiteState.TabIndex = 2;
            // 
            // label124
            // 
            this.label124.AutoSize = true;
            this.label124.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.label124.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label124.Location = new System.Drawing.Point(469, 97);
            this.label124.Name = "label124";
            this.label124.Size = new System.Drawing.Size(57, 21);
            this.label124.TabIndex = 66;
            this.label124.Text = "State:";
            // 
            // label97
            // 
            this.label97.AutoSize = true;
            this.label97.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.label97.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label97.Location = new System.Drawing.Point(920, 97);
            this.label97.Name = "label97";
            this.label97.Size = new System.Drawing.Size(99, 21);
            this.label97.TabIndex = 65;
            this.label97.Text = "Subdivision:";
            // 
            // label96
            // 
            this.label96.AutoSize = true;
            this.label96.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.label96.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label96.Location = new System.Drawing.Point(783, 97);
            this.label96.Name = "label96";
            this.label96.Size = new System.Drawing.Size(72, 21);
            this.label96.TabIndex = 64;
            this.label96.Text = "County:";
            // 
            // label95
            // 
            this.label95.AutoSize = true;
            this.label95.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.label95.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label95.Location = new System.Drawing.Point(681, 97);
            this.label95.Name = "label95";
            this.label95.Size = new System.Drawing.Size(36, 21);
            this.label95.TabIndex = 63;
            this.label95.Text = "ZIP:";
            // 
            // label94
            // 
            this.label94.AutoSize = true;
            this.label94.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.label94.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label94.Location = new System.Drawing.Point(325, 97);
            this.label94.Name = "label94";
            this.label94.Size = new System.Drawing.Size(45, 21);
            this.label94.TabIndex = 62;
            this.label94.Text = "City:";
            // 
            // label93
            // 
            this.label93.AutoSize = true;
            this.label93.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.label93.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label93.Location = new System.Drawing.Point(4, 97);
            this.label93.Name = "label93";
            this.label93.Size = new System.Drawing.Size(159, 21);
            this.label93.TabIndex = 61;
            this.label93.Text = "Site Street Address:";
            // 
            // tbSubDivision
            // 
            this.tbSubDivision.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbSubDivision.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.tbSubDivision.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbSubDivision.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.tbSubDivision.Location = new System.Drawing.Point(921, 3);
            this.tbSubDivision.Name = "tbSubDivision";
            this.tbSubDivision.Size = new System.Drawing.Size(227, 27);
            this.tbSubDivision.TabIndex = 6;
            // 
            // tbCounty
            // 
            this.tbCounty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.tbCounty.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbCounty.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.tbCounty.Location = new System.Drawing.Point(784, 3);
            this.tbCounty.Name = "tbCounty";
            this.tbCounty.Size = new System.Drawing.Size(131, 27);
            this.tbCounty.TabIndex = 5;
            // 
            // tbSiteCity
            // 
            this.tbSiteCity.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.tbSiteCity.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbSiteCity.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.tbSiteCity.Location = new System.Drawing.Point(328, 3);
            this.tbSiteCity.Name = "tbSiteCity";
            this.tbSiteCity.Size = new System.Drawing.Size(134, 27);
            this.tbSiteCity.TabIndex = 1;
            // 
            // bSave
            // 
            this.bSave.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.bSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bSave.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.bSave.Location = new System.Drawing.Point(319, 5);
            this.bSave.Name = "bSave";
            this.bSave.Size = new System.Drawing.Size(68, 27);
            this.bSave.TabIndex = 5;
            this.bSave.TabStop = false;
            this.bSave.Text = "&Save";
            this.bSave.UseVisualStyleBackColor = false;
            this.bSave.Click += new System.EventHandler(this.bSave_Click);
            // 
            // bNew
            // 
            this.bNew.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.bNew.Location = new System.Drawing.Point(823, 7);
            this.bNew.Name = "bNew";
            this.bNew.Size = new System.Drawing.Size(32, 23);
            this.bNew.TabIndex = 8;
            this.bNew.Text = "&New";
            this.bNew.Visible = false;
            // 
            // tbSiteZIP
            // 
            this.tbSiteZIP.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.tbSiteZIP.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbSiteZIP.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.tbSiteZIP.Location = new System.Drawing.Point(682, 3);
            this.tbSiteZIP.Name = "tbSiteZIP";
            this.tbSiteZIP.Size = new System.Drawing.Size(96, 27);
            this.tbSiteZIP.TabIndex = 4;
            this.tbSiteZIP.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tbTextBox_KeyDown);
            // 
            // tbSiteAdd
            // 
            this.tbSiteAdd.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.tbSiteAdd.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbSiteAdd.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.tbSiteAdd.Location = new System.Drawing.Point(5, 3);
            this.tbSiteAdd.Name = "tbSiteAdd";
            this.tbSiteAdd.Size = new System.Drawing.Size(317, 27);
            this.tbSiteAdd.TabIndex = 0;
            this.tbSiteAdd.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tbTextBox_KeyDown);
            // 
            // tbFullName
            // 
            this.tbFullName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbFullName.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.tbFullName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbFullName.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.tbFullName.Location = new System.Drawing.Point(563, 3);
            this.tbFullName.Name = "tbFullName";
            this.tbFullName.ReadOnly = true;
            this.tbFullName.Size = new System.Drawing.Size(585, 27);
            this.tbFullName.TabIndex = 3;
            this.tbFullName.TabStop = false;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label9.Location = new System.Drawing.Point(496, -14);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(57, 13);
            this.label9.TabIndex = 21;
            this.label9.Text = "Full Name:";
            // 
            // tbLastName
            // 
            this.tbLastName.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.tbLastName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbLastName.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.tbLastName.Location = new System.Drawing.Point(326, 3);
            this.tbLastName.Name = "tbLastName";
            this.tbLastName.Size = new System.Drawing.Size(232, 27);
            this.tbLastName.TabIndex = 2;
            this.tbLastName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tbTextBox_KeyDown);
            // 
            // tbMiddleName
            // 
            this.tbMiddleName.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.tbMiddleName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbMiddleName.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.tbMiddleName.Location = new System.Drawing.Point(149, 3);
            this.tbMiddleName.Name = "tbMiddleName";
            this.tbMiddleName.Size = new System.Drawing.Size(173, 27);
            this.tbMiddleName.TabIndex = 1;
            this.tbMiddleName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tbTextBox_KeyDown);
            // 
            // tbFirstname
            // 
            this.tbFirstname.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.tbFirstname.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbFirstname.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.tbFirstname.Location = new System.Drawing.Point(6, 3);
            this.tbFirstname.Name = "tbFirstname";
            this.tbFirstname.Size = new System.Drawing.Size(137, 27);
            this.tbFirstname.TabIndex = 0;
            this.tbFirstname.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tbTextBox_KeyDown);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.label7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label7.Location = new System.Drawing.Point(327, 41);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(97, 21);
            this.label7.TabIndex = 15;
            this.label7.Text = "Last Name:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label6.Location = new System.Drawing.Point(148, 41);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(119, 21);
            this.label6.TabIndex = 14;
            this.label6.Text = "Middle Name:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label5.Location = new System.Drawing.Point(6, 41);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(95, 21);
            this.label5.TabIndex = 13;
            this.label5.Text = "First Name:";
            // 
            // tbLastModified
            // 
            this.tbLastModified.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.tbLastModified.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbLastModified.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.tbLastModified.Location = new System.Drawing.Point(991, 35);
            this.tbLastModified.Name = "tbLastModified";
            this.tbLastModified.ReadOnly = true;
            this.tbLastModified.Size = new System.Drawing.Size(104, 27);
            this.tbLastModified.TabIndex = 12;
            this.tbLastModified.TabStop = false;
            // 
            // tbFirstEntered
            // 
            this.tbFirstEntered.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.tbFirstEntered.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbFirstEntered.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.tbFirstEntered.Location = new System.Drawing.Point(991, 3);
            this.tbFirstEntered.Name = "tbFirstEntered";
            this.tbFirstEntered.ReadOnly = true;
            this.tbFirstEntered.Size = new System.Drawing.Size(104, 27);
            this.tbFirstEntered.TabIndex = 10;
            this.tbFirstEntered.TabStop = false;
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label4.Location = new System.Drawing.Point(868, 37);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(117, 21);
            this.label4.TabIndex = 10;
            this.label4.Text = "Last Modified:";
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label3.Location = new System.Drawing.Point(876, 5);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(109, 21);
            this.label3.TabIndex = 9;
            this.label3.Text = "First Entered:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label2.Location = new System.Drawing.Point(497, 5);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 21);
            this.label2.TabIndex = 6;
            this.label2.Text = "Status:";
            // 
            // bLast
            // 
            this.bLast.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.bLast.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bLast.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.bLast.Location = new System.Drawing.Point(265, 5);
            this.bLast.Name = "bLast";
            this.bLast.Size = new System.Drawing.Size(48, 27);
            this.bLast.TabIndex = 4;
            this.bLast.TabStop = false;
            this.bLast.Text = ">|";
            this.bLast.UseVisualStyleBackColor = false;
            this.bLast.Click += new System.EventHandler(this.bLast_Click);
            // 
            // bNext
            // 
            this.bNext.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.bNext.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bNext.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.bNext.Location = new System.Drawing.Point(211, 5);
            this.bNext.Name = "bNext";
            this.bNext.Size = new System.Drawing.Size(48, 27);
            this.bNext.TabIndex = 3;
            this.bNext.TabStop = false;
            this.bNext.Text = ">>";
            this.bNext.UseVisualStyleBackColor = false;
            this.bNext.Click += new System.EventHandler(this.bNext_Click);
            // 
            // bFirst
            // 
            this.bFirst.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.bFirst.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bFirst.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.bFirst.Location = new System.Drawing.Point(6, 5);
            this.bFirst.Name = "bFirst";
            this.bFirst.Size = new System.Drawing.Size(48, 27);
            this.bFirst.TabIndex = 0;
            this.bFirst.TabStop = false;
            this.bFirst.Text = "|<";
            this.bFirst.UseVisualStyleBackColor = false;
            this.bFirst.Click += new System.EventHandler(this.bFirst_Click);
            // 
            // bPrev
            // 
            this.bPrev.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.bPrev.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bPrev.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.bPrev.Location = new System.Drawing.Point(60, 5);
            this.bPrev.Name = "bPrev";
            this.bPrev.Size = new System.Drawing.Size(48, 27);
            this.bPrev.TabIndex = 1;
            this.bPrev.TabStop = false;
            this.bPrev.Text = "<<";
            this.bPrev.UseVisualStyleBackColor = false;
            this.bPrev.Click += new System.EventHandler(this.bPrev_Click);
            // 
            // tbCurrPos
            // 
            this.tbCurrPos.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.tbCurrPos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbCurrPos.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.tbCurrPos.Location = new System.Drawing.Point(114, 5);
            this.tbCurrPos.Name = "tbCurrPos";
            this.tbCurrPos.ReadOnly = true;
            this.tbCurrPos.Size = new System.Drawing.Size(92, 27);
            this.tbCurrPos.TabIndex = 2;
            this.tbCurrPos.TabStop = false;
            this.tbCurrPos.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(544, 16);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(16, 16);
            this.button2.TabIndex = 72;
            this.button2.Text = "button2";
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(520, 16);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(16, 16);
            this.button1.TabIndex = 71;
            this.button1.Text = "button1";
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // lockStatus
            // 
            this.lockStatus.Location = new System.Drawing.Point(344, 32);
            this.lockStatus.Name = "lockStatus";
            this.lockStatus.Size = new System.Drawing.Size(72, 16);
            this.lockStatus.TabIndex = 70;
            this.lockStatus.Text = "Lock Status";
            // 
            // ctxPbHouseOptions
            // 
            this.ctxPbHouseOptions.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.getStreetView,
            this.uploadPhoto});
            this.ctxPbHouseOptions.Name = "ctxPbHouseOptions";
            this.ctxPbHouseOptions.Size = new System.Drawing.Size(154, 48);
            // 
            // getStreetView
            // 
            this.getStreetView.Name = "getStreetView";
            this.getStreetView.Size = new System.Drawing.Size(153, 22);
            this.getStreetView.Text = "Get Street View";
            // 
            // uploadPhoto
            // 
            this.uploadPhoto.Name = "uploadPhoto";
            this.uploadPhoto.Size = new System.Drawing.Size(153, 22);
            this.uploadPhoto.Text = "Upload Photo";
            // 
            // panel14
            // 
            this.panel14.Controls.Add(this._pbHouse);
            this.panel14.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel14.Location = new System.Drawing.Point(0, 0);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(162, 157);
            this.panel14.TabIndex = 0;
            // 
            // panel15
            // 
            this.panel15.Controls.Add(this._up_names_panel);
            this.panel15.Controls.Add(this._up_address_panel);
            this.panel15.Controls.Add(this.tbStatusGroup);
            this.panel15.Controls.Add(this.bSave);
            this.panel15.Controls.Add(this._unLockPI);
            this.panel15.Controls.Add(this.tbFirstEntered);
            this.panel15.Controls.Add(this.tbLastModified);
            this.panel15.Controls.Add(this.label3);
            this.panel15.Controls.Add(this.bNew);
            this.panel15.Controls.Add(this.label2);
            this.panel15.Controls.Add(this.bLast);
            this.panel15.Controls.Add(this.label4);
            this.panel15.Controls.Add(this.bNext);
            this.panel15.Controls.Add(this.pbMap);
            this.panel15.Controls.Add(this.bFirst);
            this.panel15.Controls.Add(this.bPrev);
            this.panel15.Controls.Add(this.tbCurrPos);
            this.panel15.Controls.Add(this.label133);
            this.panel15.Controls.Add(this.label9);
            this.panel15.Controls.Add(this.label93);
            this.panel15.Controls.Add(this.label94);
            this.panel15.Controls.Add(this.label7);
            this.panel15.Controls.Add(this.label6);
            this.panel15.Controls.Add(this.label95);
            this.panel15.Controls.Add(this.label5);
            this.panel15.Controls.Add(this.label124);
            this.panel15.Controls.Add(this.label96);
            this.panel15.Controls.Add(this.label97);
            this.panel15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel15.Location = new System.Drawing.Point(162, 0);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(1158, 157);
            this.panel15.TabIndex = 1;
            // 
            // _up_address_panel
            // 
            this._up_address_panel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._up_address_panel.Controls.Add(this.tbSiteAdd);
            this._up_address_panel.Controls.Add(this.tbSiteState);
            this._up_address_panel.Controls.Add(this.tbSiteZIP);
            this._up_address_panel.Controls.Add(this.tbSiteCityState);
            this._up_address_panel.Controls.Add(this.tbSubDivision);
            this._up_address_panel.Controls.Add(this.tbSiteCity);
            this._up_address_panel.Controls.Add(this.tbCounty);
            this._up_address_panel.Location = new System.Drawing.Point(3, 119);
            this._up_address_panel.Name = "_up_address_panel";
            this._up_address_panel.Size = new System.Drawing.Size(1152, 32);
            this._up_address_panel.TabIndex = 1;
            // 
            // _up_names_panel
            // 
            this._up_names_panel.Controls.Add(this.tbFirstname);
            this._up_names_panel.Controls.Add(this.tbMiddleName);
            this._up_names_panel.Controls.Add(this.tbLastName);
            this._up_names_panel.Controls.Add(this.tbFullName);
            this._up_names_panel.Location = new System.Drawing.Point(3, 65);
            this._up_names_panel.Name = "_up_names_panel";
            this._up_names_panel.Size = new System.Drawing.Size(1152, 32);
            this._up_names_panel.TabIndex = 0;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.panel4.Controls.Add(this._ci_notes);
            this.panel4.Controls.Add(this._ci_NotesLabel);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(601, 200);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(711, 241);
            this.panel4.TabIndex = 6;
            // 
            // _ci_notes
            // 
            this._ci_notes.Controls.Add(this._ci_NotesField);
            this._ci_notes.Location = new System.Drawing.Point(6, 38);
            this._ci_notes.Name = "_ci_notes";
            this._ci_notes.Size = new System.Drawing.Size(702, 116);
            this._ci_notes.TabIndex = 81;
            // 
            // ContactScreen
            // 
            this.Controls.Add(this.upperPanel);
            this.Controls.Add(this.lowerPanel);
            this.Name = "ContactScreen";
            this.Size = new System.Drawing.Size(1320, 840);
            this.Load += new System.EventHandler(this.ContactScreen_Load);
            this.lowerPanel.ResumeLayout(false);
            this._contactTab.ResumeLayout(false);
            this.tpContactInfo.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this._ci_email_panel.ResumeLayout(false);
            this._ci_email_panel.PerformLayout();
            this._ci_fax_panel.ResumeLayout(false);
            this._ci_fax_panel.PerformLayout();
            this._ci_cellphone_panel.ResumeLayout(false);
            this._ci_cellphone_panel.PerformLayout();
            this._ci_workphone_panel.ResumeLayout(false);
            this._ci_workphone_panel.PerformLayout();
            this._ci_homephone_panel.ResumeLayout(false);
            this._ci_homephone_panel.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this._ci_old_panel.ResumeLayout(false);
            this._ci_old_panel.PerformLayout();
            this._ci_previous_panel.ResumeLayout(false);
            this._ci_previous_panel.PerformLayout();
            this._ci_lk_panel.ResumeLayout(false);
            this._ci_lk_panel.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this._ci_st_landline_panel.ResumeLayout(false);
            this._ci_st_landline_panel.PerformLayout();
            this._ci_st_cell_panel.ResumeLayout(false);
            this._ci_st_cell_panel.PerformLayout();
            this._ci_st_email_panel.ResumeLayout(false);
            this._ci_st_email_panel.PerformLayout();
            this.tpDealInfo.ResumeLayout(false);
            this.tpDealInfo.PerformLayout();
            this.tpSkipTracedInfo.ResumeLayout(false);
            this.skipTracedInfoTabControl.ResumeLayout(false);
            this.skipTracedContactInfo.ResumeLayout(false);
            this.skipTracedContactInfo.PerformLayout();
            this.skipTracedOtherProperty.ResumeLayout(false);
            this.skipTracedOtherProperty.PerformLayout();
            this.skipTracedRelativesInfo.ResumeLayout(false);
            this.tpGroupMembership.ResumeLayout(false);
            this.tpGroupMembership.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this._evaluate.ResumeLayout(false);
            this._evaluateSubTabControl.ResumeLayout(false);
            this._propertyProfileSubTabPage.ResumeLayout(false);
            this._compsSubTabPage.ResumeLayout(false);
            this._estimateRepairsSubTabPage.ResumeLayout(false);
            this._shortSaleTab.ResumeLayout(false);
            this._shortSaleTab.PerformLayout();
            this._buyersInfoTab.ResumeLayout(false);
            this.tpImportedInfo.ResumeLayout(false);
            this.tcSubImportedInfo.ResumeLayout(false);
            this.tpPropertyCharacteristics.ResumeLayout(false);
            this.tpPropertyCharacteristics.PerformLayout();
            this.tpListingInfo.ResumeLayout(false);
            this.tpListingInfo.PerformLayout();
            this.tpForeClosureInfo.ResumeLayout(false);
            this.tpForeClosureInfo.PerformLayout();
            this.tpFinancials.ResumeLayout(false);
            this.tpFinancials.PerformLayout();
            this.tpTaxInfo.ResumeLayout(false);
            this.tpTaxInfo.PerformLayout();
            this.tpOtherInfo.ResumeLayout(false);
            this.tpOtherInfo.PerformLayout();
            this.tpCommercial.ResumeLayout(false);
            this.upperPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._pbHouse)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbMap)).EndInit();
            this.ctxPbHouseOptions.ResumeLayout(false);
            this.panel14.ResumeLayout(false);
            this.panel15.ResumeLayout(false);
            this.panel15.PerformLayout();
            this._up_address_panel.ResumeLayout(false);
            this._up_address_panel.PerformLayout();
            this._up_names_panel.ResumeLayout(false);
            this._up_names_panel.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this._ci_notes.ResumeLayout(false);
            this._ci_notes.PerformLayout();
            this.ResumeLayout(false);

		}
		#endregion

		#region � Properties �
		#region � CurrentPropertyItemId �
		public Guid CurrentPropertyItemId
		{
			get 
			{
				if (null != this.htNodePosPropertyItemId)
					return (Guid)this.htNodePosPropertyItemId[this.currentOffset];
				else 
					return this.pi.IdPropertyItem;
			}
		}
		#endregion � CurrentPropertyItemId �

		public string ARV
		{
			get {return this.tbARV.Text;}
			set {this.tbARV.Text = value;}
		}

		public int CurrentOffset
		{
			get { return this.currentOffset; }
			set { this.currentOffset = value; }
		}

		public int CurrentNodeId
		{
			get {return this.currentPropertyItemGroup.nodeId;}
		}
		#endregion � Properties �

		#region � Methods �
		#region � ContactScreenStarter �
		private void ContactScreenStarter (Hashtable htNodePosPropertyItemId)
		{
			if (null != htNodePosPropertyItemId && htNodePosPropertyItemId.Count > 0)
			{
				try
				{
					Cursor.Current = Cursors.WaitCursor;
					this.htNodePosPropertyItemId = htNodePosPropertyItemId;			
					
					if (this.CurrentOffset > htNodePosPropertyItemId.Count)
						this.CurrentOffset = htNodePosPropertyItemId.Count;
					
					Guid firstPropertyItem = (Guid)htNodePosPropertyItemId[this.CurrentOffset];
					
					this.LoadMedia();
					
					this.LoadDisplayData(firstPropertyItem);
					//this.tbGroup.Text = Globals.selectedNodeName;
					this.DisplayGroupMembershipData(firstPropertyItem);

					this.currentPropertyItemGroup = new Node();
					this.currentPropertyItemGroup.nodeId = Globals.selectedNodeId;
					this.currentPropertyItemGroup.nodeName = Globals.selectedNodeName;

					//DealMaker.DataConsumption.User() = new DealMaker.DataConsumption.User();
					//Globals.UserDataConsumption = 
					//Globals.DataConsumptionReg

					DataConsumption.Registration dataConsumptionReg = new DealMaker.DataConsumption.Registration();
					dataConsumptionReg.DateActivation = DateTime.Today;
					//dataConsumptionReg.KeyCode
				
					//Licence type
					//Registration.LicenceType licType = new DealMaker.Registration.LicenceType();
					//licType.Name = 
					//licType.Price = 
					//dataConsumptionReg.LicenceType = 

					//dataConsumptionReg.Product
					//Registration.Product product = new DealMaker.Registration.Product();
					//product.Type = 
					//product.Version = 

					//dataConsumptionReg.VolumeNumber = 

					//dataConsumptionReg.User = 
					Registration.User user = new DealMaker.Registration.User();
					//user.FirstName = 
					//user.LastName = 
					//user.MiddleName = 

				

					Registration.Address address = new DealMaker.Registration.Address();
					//address.Company = 
					//address.EMail =
					//address.FullSiteStreetNumber = 
					//address.PhoneBusiness =
					//address.PhoneCell
					//address.SiteCity
					//address.SiteCitySiteState = 
					//address.SiteCitySiteStateSiteZIP
					//address.SiteCountry				
					//address.SiteState
					//address.SiteStreetName
					//address.SiteStreetPostDirectional
					//address.SiteStreetPreDirectional
					//address.SiteStreetSuffix
					//address.SiteStreetUnitNumber
					//address.SiteZIP				
				}
				catch(Exception 
#if DEBUG
					exc
#endif
					)
				{
#if DEBUG
					MessageBox.Show(exc.Message + "\n\n" + exc.InnerException);
#endif
				}
				finally
				{
					Cursor.Current = Cursors.Arrow;
				}
			}
			else
			{
				InitializeComponent();
			}
		}
		#endregion � ContactScreenStarter �
		#region � ContactScreen_Load �
		private void ContactScreen_Load(object sender, System.EventArgs e)
		{			
			//sasa, 26.01.06: moved to load
			AddEventHandlers();
            AddRedesignEventHandlers();
            //Size lpSize = new Size(this.Size.Width - 9, this.Size.Height - 300);
            //this.lowerPanel.Size = lpSize;			
        }
		#endregion � ContactScreen_Load �
		#region � Update Decisions �
		#region � cbComboBox_SelectedIndexChanged �
		private void cbComboBox_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			this.bUpdate = true;
		}
		#endregion � cbComboBox_SelectedIndexChanged �
		#region � tbTextBox_KeyDown �
		private void tbTextBox_KeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
		{
			this.bUpdate = true;
		}
		#endregion � tbTextBox_KeyDown �
		#region � dateControl_ValueChanged �
		private void dateControl_ValueChanged(object sender, System.EventArgs e)
		{
			this.bUpdate = true;
		}
		#endregion � dateControl_ValueChanged �
		#endregion � Update Decisions �
		#region � UpdatePropertyItem �
		private void UpdatePropertyItem()
		{			
			ISession session = null;
			ITransaction transaction = null;
			
			try
			{
			
				pi.DateLastModified = DateTime.Now;
				Connection conn = Connection.getInstance();     
				SqlConnection sqlConn = conn.HatchConn;
				IDbConnection iSqlConn = sqlConn as IDbConnection;

				
				Configuration cfg = Globals.cfg;
				ISessionFactory factory = Globals.factory;
				
				
				if (null != iSqlConn) 
				{
					session = factory.OpenSession(iSqlConn);
					if (ConnectionState.Closed == iSqlConn.State) 
					{
						iSqlConn.Open();
					}
				}
				else 
				{
					session = factory.OpenSession();
				}

				transaction = session.BeginTransaction();
			
				//session.SaveOrUpdate(pi);
				pi.NhSaveOrUpdate(session);

				transaction.Commit();				
			}
			catch (Exception exc)
			{
				MessageBox.Show(exc.ToString());
				transaction.Rollback();
			}				
			finally
			{	
				session.Flush();
				session.Close();
			}			
		}
		#endregion � UpdatePropertyItem �
		#region � Navigation �
		#region � bNext_Click �
		private void bNext_Click(object sender, System.EventArgs e)
		{
			if (true == bUpdate)
			{
				//this.UpdatePropertyItem();
			}

			if (null != this.htNodePosPropertyItemId && this.currentOffset < this.htNodePosPropertyItemId.Count)
			{
				try
				{
					Cursor.Current = Cursors.WaitCursor;
					this.currentOffset++;					
					Guid currentlySelectedPropertyItem = (Guid)this.htNodePosPropertyItemId[this.currentOffset];

					//Commercials
					//DataManager.Instance.GetData(currentlySelectedPropertyItem);

					this.LoadDisplayData(currentlySelectedPropertyItem);
					this.DisplayGroupMembershipData(currentlySelectedPropertyItem);				
				}
				finally
				{
					Cursor.Current = Cursors.Arrow;
				}
			}
		}
		#endregion � bNext_Click �
		#region � bLast_Click �
		private void bLast_Click(object sender, System.EventArgs e)
		{					
			if (null != this.htNodePosPropertyItemId)
			{
				try
				{
					Cursor.Current = Cursors.WaitCursor;
					this.currentOffset = this.htNodePosPropertyItemId.Count;
					Guid currentlySelectedPropertyItem = (Guid)this.htNodePosPropertyItemId[this.currentOffset];

					//Commercials
					//DataManager.Instance.GetData(currentlySelectedPropertyItem);

					this.LoadDisplayData(currentlySelectedPropertyItem);
					this.DisplayGroupMembershipData(currentlySelectedPropertyItem);
				}
				finally
				{
					Cursor.Current = Cursors.Arrow;
				}

			}
		}
		#endregion � bLast_Click �
		#region � bPrev_Click �
		private void bPrev_Click(object sender, System.EventArgs e)
		{
			if (null != this.htNodePosPropertyItemId)
			{
				try
				{
					Cursor.Current = Cursors.WaitCursor;
					if (this.currentOffset != 1)
						this.currentOffset--;
					Guid currentlySelectedPropertyItem = (Guid)this.htNodePosPropertyItemId[this.currentOffset];

					//Commercials
					//DataManager.Instance.GetData(currentlySelectedPropertyItem);

					this.LoadDisplayData(currentlySelectedPropertyItem);
					this.DisplayGroupMembershipData(currentlySelectedPropertyItem);
				}
				finally
				{
					Cursor.Current = Cursors.Arrow;
				}
			}
		}
		#endregion � bPrev_Click �
		#region � bFirst_Click �
		private void bFirst_Click(object sender, System.EventArgs e)
		{
			if (null != this.htNodePosPropertyItemId)
			{
				try
				{
					Cursor.Current = Cursors.WaitCursor;
					this.currentOffset = 1;
					Guid currentlySelectedPropertyItem = (Guid)this.htNodePosPropertyItemId[this.currentOffset];

					//Commercials
					//DataManager.Instance.GetData(currentlySelectedPropertyItem);

					this.LoadDisplayData(currentlySelectedPropertyItem);
					this.DisplayGroupMembershipData(currentlySelectedPropertyItem);
				}
				finally
				{
					Cursor.Current = Cursors.Arrow;
				}
			}
		}
		#endregion � bFirst_Click �
		#endregion � Navigation �
		#region � LoadPropertyInfo �
		private PropertyItem LoadPropertyInfo (Guid propertyItemId)
		{
			ISession session = null;
			ITransaction transaction = null;
			PropertyItem pi = null;
			try
			{
			
				Connection conn = Connection.getInstance();     
				SqlConnection sqlConn = conn.HatchConn;
				IDbConnection iSqlConn = sqlConn as IDbConnection;

				//NHibernate update
				/*Configuration cfg = new Configuration();								

				cfg.AddAssembly("DealMaker");                  

				ISessionFactory factory = cfg.BuildSessionFactory();*/
				Configuration cfg = Globals.cfg;
				ISessionFactory factory = Globals.factory;
				
				
				if (null != iSqlConn) 
				{
					session = factory.OpenSession(iSqlConn);
					if (ConnectionState.Closed == iSqlConn.State) 
					{
						iSqlConn.Open();
					}
				}
				else 
				{
					session = factory.OpenSession();
				}
				transaction = session.BeginTransaction();							
				pi = (PropertyItem)session.Load(typeof(PropertyItem), propertyItemId);
				transaction.Commit();				
			}
			catch (Exception exc)
			{
				MessageBox.Show(exc.ToString());
				transaction.Rollback();
			}				
			finally
			{
				
				session.Close();
			}
			return pi;
		}
		#endregion � LoadPropertyInfo �
		#region � Group Membership �
		#region � GetGroupMembershipData �
		public ArrayList GetGroupMembershipData(Guid propertyItemId, out Node statusGroup)
		{
			ArrayList alGroupNames = new ArrayList();
			Node currentStatusGroup = null;
			SqlDataReader groupReader = null;
			SqlDataReader statusGroupReader = null;
			try
			{							
				SqlCommand sp_GetGroupMembership = new SqlCommand("GetGroupMembership", Connection.getInstance().HatchConn);

				sp_GetGroupMembership.CommandType = CommandType.StoredProcedure;					
				sp_GetGroupMembership.Parameters.Add("@propertyItemId", propertyItemId);
													
									
				if (ConnectionState.Open != Connection.getInstance().HatchConn.State)
					Connection.getInstance().HatchConn.Open();
				
				groupReader = sp_GetGroupMembership.ExecuteReader();
				while (groupReader.Read())
				{
					Node n = new Node();
					n.nodeId = groupReader.GetInt32(0);
					n.nodeName = groupReader.GetString(1);
					n.nodeDesc = groupReader.GetString(2);
					alGroupNames.Add(n);
				}
				groupReader.Close();

				SqlCommand sp_GetStatusGroupMembership = new SqlCommand("GetStatusGroupMembership", Connection.getInstance().HatchConn);

				sp_GetStatusGroupMembership.CommandType = CommandType.StoredProcedure;					
				sp_GetStatusGroupMembership.Parameters.Add("@propertyItemId", propertyItemId);
													
									
				if (ConnectionState.Open != Connection.getInstance().HatchConn.State)
					Connection.getInstance().HatchConn.Open();
				
				statusGroupReader = sp_GetStatusGroupMembership.ExecuteReader();
				while (statusGroupReader.Read())
				{					
					currentStatusGroup = new Node();
					currentStatusGroup.nodeId = statusGroupReader.GetInt32(0);
					currentStatusGroup.nodeName = statusGroupReader.GetString(1).TrimEnd();	
					currentStatusGroup.nodeDesc = statusGroupReader.GetString(2).TrimEnd();
				}
				statusGroupReader.Close();
			}
			catch( Exception ex)
			{
				groupReader.Close();
				statusGroupReader.Close();
				MessageBox.Show(ex.ToString());				
			}
			finally 
			{
				statusGroup = currentStatusGroup;				
			}
			return alGroupNames;
		}
		#endregion � GetGroupMembershipData �
		#region � DisplayGroupMembershipData �
		private void DisplayGroupMembershipData(Guid propertyItemId)
		{
			this.lvOtherGroups.Items.Clear();
			Node statusGroup = new Node();
			ArrayList alGroups = this.GetGroupMembershipData(propertyItemId, out statusGroup);
			if (null != statusGroup)
			{
				this.tbGroupStatusGroup.Text = statusGroup.nodeName;
				this.tbStatusGroup.Text = statusGroup.nodeName;
			}
			else 
			{
				this.tbGroupStatusGroup.Text = UserMessages.NoStatusGroupAssigned;	
				this.tbStatusGroup.Text = UserMessages.NoStatusGroupAssigned;
			}
			this.currentPropertyItemStatusGroup = statusGroup;

			for (int i = 0; i < alGroups.Count; i++)
			{
				Node n = (Node)alGroups[i];
				ListViewItem lvi = new ListViewItem(n.nodeId.ToString().Trim());				
				lvi.SubItems.Add(n.nodeName.Trim());
				lvi.SubItems.Add(n.nodeDesc.Trim());
				this.lvOtherGroups.Items.Add(lvi);
			}
		}
		#endregion � DisplayGroupMembershipData �
		#region � bAssignToGroup_Click �
		private void bAssignToGroup_Click(object sender, System.EventArgs e)
		{
			// ++ save new entry
			this.SaveData();
			// -- save new entry

			SelectGroupForm sgf = new SelectGroupForm(this.CurrentPropertyItemId);
            sgf.ContactType = _contactType;
			sgf.ShowDialog();
			this.DisplayGroupMembershipData(this.CurrentPropertyItemId);
		}
		#endregion � bAssignToGroup_Click �
		#region � bRemoveFromGroup_Click �
		private void bRemoveFromGroup_Click(object sender, System.EventArgs e)
		{	
			int removeFromNodeId = 0;
			if (null != this.lvOtherGroups.SelectedIndices && this.lvOtherGroups.SelectedIndices.Count > 0)
			{
                Node statusGroup = null;
                ArrayList alGroups = this.GetGroupMembershipData(this.CurrentPropertyItemId, out statusGroup);

                if (1 == this.lvOtherGroups.Items.Count && null == statusGroup)
				{
					MessageBox.Show(this, UserMessages.CanNotRemoveContactFromGroup, UserMessages.CanNotRemoveContactFromGroupHeader,  MessageBoxButtons.OK, MessageBoxIcon.Hand);
					return;
				}

				try 
				{
					removeFromNodeId = Convert.ToInt32(this.lvOtherGroups.SelectedItems[0].SubItems[0].Text);
				}
				catch (Exception exc)
				{
					MessageBox.Show("Convert error !" + exc.Message);
					return;
				}

				if (removeFromNodeId == this.currentPropertyItemGroup.nodeId)
				{					
					MessageBox.Show(UserMessages.BaseGroupChange);
				}

				ApplicationHelper.DeletePropertyItemFromGroup(this.CurrentPropertyItemId, removeFromNodeId);

				if (removeFromNodeId == this.currentPropertyItemGroup.nodeId)
				{										
					//this.UpdatePropertyItem();
					Hashtable htNodePosPropertyItemId = ApplicationHelper.GetPropertyItemsInNode(this.currentPropertyItemGroup.nodeId);
					this.ContactScreenStarter(htNodePosPropertyItemId);
				}
				else
				{
					this.DisplayGroupMembershipData(this.CurrentPropertyItemId);
				}
			}
			else
			{
				MessageBox.Show(this, UserMessages.GroupNotSelectedHeader, UserMessages.GroupNotSelected, MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}
		#endregion � bRemoveFromGroup_Click �
		#region � RemovePropertyItemFromStatusGroup �
		private void RemovePropertyItemFromStatusGroup()
		{
			int currentStatusGroupId = this.currentPropertyItemStatusGroup.nodeId;
			//delete it
			try
			{							
				SqlCommand sp_DeleteGroupMembership = new SqlCommand("DeleteGroupMembership", Connection.getInstance().HatchConn);

				sp_DeleteGroupMembership.CommandType = CommandType.StoredProcedure;					
				sp_DeleteGroupMembership.Parameters.Add("@propertyItemId", this.CurrentPropertyItemId);
				sp_DeleteGroupMembership.Parameters.Add("@nodeId", currentStatusGroupId);
													
									
				if (ConnectionState.Open != Connection.getInstance().HatchConn.State)
					Connection.getInstance().HatchConn.Open();
				
				sp_DeleteGroupMembership.ExecuteNonQuery();
					
			}
			catch( Exception ex)
			{					
				MessageBox.Show(ex.ToString());				
			}
		}
		#endregion � RemovePropertyItemFromStatusGroup �
		
		#region Assign To Group
		public void AssignToGroup(int group, bool removeFromStatusGroup)
		{
			
			this.SaveData();

			this.bUpdate = true;
			if (true == removeFromStatusGroup)
			{
				if (null != this.currentPropertyItemStatusGroup)
				{
					this.RemovePropertyItemFromStatusGroup();							
				}
			}
			//insert new one
			try
			{							
				SqlCommand sp_InsertGroupMembership = new SqlCommand("InsertGroupMembership", Connection.getInstance().HatchConn);

				sp_InsertGroupMembership.CommandType = CommandType.StoredProcedure;					
				sp_InsertGroupMembership.Parameters.Add("@propertyItemId", this.CurrentPropertyItemId);
				sp_InsertGroupMembership.Parameters.Add("@nodeId", group);
													
									
				if (ConnectionState.Open != Connection.getInstance().HatchConn.State)
					Connection.getInstance().HatchConn.Open();
				
				sp_InsertGroupMembership.ExecuteNonQuery();				
					
			}
			catch( Exception ex)
			{					
				MessageBox.Show(ex.ToString());				
			}
			finally
			{
				this.DisplayGroupMembershipData(this.CurrentPropertyItemId);
			}
		}
		#endregion Assign To Group
		private void bPending_Click(object sender, System.EventArgs e)
		{
			this.AssignToGroup(Globals.Pending, true);
		}	
		#region � bLeadsNotProcessedYet_Click �
		private void bLeadsNotProcessedYet_Click(object sender, System.EventArgs e)
		{
			this.AssignToGroup(Globals.LeadsNotProcessedYetId, true);
		}
		#endregion � bLeadsNotProcessedYet_Click �
		#region � bDealsInProgress_Click �
		private void bDealsInProgress_Click(object sender, System.EventArgs e)
		{
			this.AssignToGroup(Globals.DealsInProgress, true);
		}
		#endregion � bDealsInProgress_Click �
		#region � bDeadLeads_Click �
		private void bDeadLeads_Click(object sender, System.EventArgs e)
		{
			this.AssignToGroup(Globals.DeadLeads, true);
		}
		#endregion � bDeadLeads_Click �
		#region � bDoNotMailGroup_Click �
		private void bDoNotMailGroup_Click(object sender, System.EventArgs e)
		{
			this.AssignToGroup(Globals.DoNotMailGroup, false);
		}
		#endregion � bDoNotMailGroup_Click �
		#region � bPurchased_Click �
		private void bPurchased_Click(object sender, System.EventArgs e)
		{
			this.AssignToGroup(Globals.Purchased, true);
		}
		#endregion � bPurchased_Click �
		#endregion � Group Membership �
		#region  Load Media 
		public void LoadMedia()
		{
            //ArrayList alMedia = ApplicationHelper.GetMediaList();
            //foreach (Guid mediaId in alMedia)
            //{
            //    Media m = ApplicationHelper.LoadMedia(mediaId);
            //    if (null != m)
            //        this.cbLeadSource.Items.Add(m.MediaName);
            //}

            foreach (Media media in MediaObject.Instance.MediaList)
            {
                if (null != media)
                {
                    //if (null != cbLeadSource)
                        cbLeadSource.Items.Add(media.MediaName);
                }
            }
		}
		#endregion  Load Media 				

        void EnableContactInfoNotesField(bool enabled)
        {
            _ci_NotesField.Visible = enabled;
            _ci_NotesLabel.Visible = enabled;
        }

        ContactTypes GetContactType(ArrayList groupsContactBelognsTo)
        {
            if (null != groupsContactBelognsTo && groupsContactBelognsTo.Count > 0)
            {
                foreach (Node n in groupsContactBelognsTo)
                {
                    return ContactTypesManager.Instance.GetCustomTypeGroup(n.nodeId);                                        
                }
            }

            return ContactTypes.Seller;
        }
        
        public void AdjustGUI(ContactTypes contactType)
        {
            _contactType = contactType;

            if (ContactTypes.Buyer == contactType || ContactTypes.Lender == contactType)
            {
                if (_contactTab.TabPages.Contains(tpDealInfo))
                    _contactTab.TabPages.Remove(tpDealInfo);

                if (_contactTab.TabPages.Contains(tpCommercial))
                    _contactTab.TabPages.Remove(tpCommercial);

                if (_contactTab.TabPages.Contains(_evaluate))
                    _contactTab.TabPages.Remove(_evaluate);

                if (_contactTab.TabPages.Contains(_shortSaleTab))
                    _contactTab.TabPages.Remove(_shortSaleTab);

                if (_contactTab.TabPages.Contains(_buyersInfoTab))
                    _contactTab.TabPages.Remove(_buyersInfoTab);

                EnableContactInfoNotesField(true);
            }
            else
            {
                if (!_contactTab.TabPages.Contains(tpDealInfo))
                    _contactTab.TabPages.Insert(1, tpDealInfo);

                if (!_contactTab.TabPages.Contains(tpCommercial))
                    _contactTab.TabPages.Insert(2, tpCommercial);

                if (!_contactTab.TabPages.Contains(_evaluate))
                    _contactTab.TabPages.Insert(5, _evaluate);

                if (!_contactTab.TabPages.Contains(_shortSaleTab))
                    _contactTab.TabPages.Insert(6, _shortSaleTab);

                if (!_contactTab.TabPages.Contains(_buyersInfoTab))
                    _contactTab.TabPages.Insert(7, _buyersInfoTab);

                EnableContactInfoNotesField(false);
            }
        }

		#region � LoadDisplayData �        
		private void LoadDisplayData(Guid currentlySelectedPropertyItem)
		{	
			//AUTOSAVE
			_enableAutoSaveTrigger = false;
		
			//24.01.06, sasa: nw lock, acquire lock 
			//TODO, maybe add delay

			lock(messageBoxHelperLock)
			{
				lastLockstatus = Network.NetworkLock.LockStatus.Pending;
			}

			Network.NetworkLock.RequestLock(currentlySelectedPropertyItem);

            pi = this.LoadPropertyInfo(currentlySelectedPropertyItem);

			if (null == pi)
				return;
			
			Globals.CurrentPropertyItem = pi;

            /*CustomContactTypes - BEGIN*/
            Node statusGroup = null;
            ArrayList groupsContactBelongsTo = GetGroupMembershipData(currentlySelectedPropertyItem, out statusGroup);
            ContactTypes contactType = GetContactType(groupsContactBelongsTo);
            AdjustGUI(contactType);
            /*CustomContactTypes - END*/

			//COMMERCIALs
            if (null != Globals.activeModules && Globals.activeModules.MOD_9)            
                DataManager.Instance.GetData(currentlySelectedPropertyItem);
            

            //BuyersInfo            
            BuyerInfoManager.Instance.GetData(currentlySelectedPropertyItem);

            //ShortSale
            string ssErr = string.Empty;
            if (null != Globals.activeModules && Globals.activeModules.MOD_8)
            {
                _shortSaleControl.EnableAutoSaveEventHandler(false);
                _shortSaleControl.SetInsideProfitGrabber();
                _shortSaleControl.SetOfficeStaffList(UserInformationManager.Instance.GetValues(UserEntries.OfficeStaffList));
                DealMaker.PlainClasses.ShortSale.Adapters.DataManager.Instance.PropertyItemID = currentlySelectedPropertyItem;

                if (null != pi && null != pi.Owner && null != pi.Owner.SiteAddress)
                {
                    DealMaker.PlainClasses.ShortSale.Adapters.DataManager.Instance.PropertyAddress = pi.Owner.SiteAddress.FullSiteStreetAddress + " " + pi.Owner.SiteAddress.SiteCitySiteStateSiteZIP;
                    DealMaker.PlainClasses.ShortSale.Adapters.DataManager.Instance.PropertyPlainAddress = pi.Owner.SiteAddress.FullSiteStreetAddress;
                    DealMaker.PlainClasses.ShortSale.Adapters.DataManager.Instance.PropertyCityStateZIP = pi.Owner.SiteAddress.SiteCitySiteStateSiteZIP;
                }

                if (null != pi && null != pi.Owner)
                {
                    DealMaker.PlainClasses.ShortSale.Adapters.DataManager.Instance.HomeOwnerFullName = pi.Owner.FullName;
                    DealMaker.PlainClasses.ShortSale.Adapters.DataManager.Instance.PrimaryPhone = pi.Owner.HomePhoneString;
                    DealMaker.PlainClasses.ShortSale.Adapters.DataManager.Instance.SecondaryPhone = pi.Owner.WorkPhoneString;
                    DealMaker.PlainClasses.ShortSale.Adapters.DataManager.Instance.Email = pi.Owner.EMail;
                    DealMaker.PlainClasses.ShortSale.Adapters.DataManager.Instance.DocumentName = pi.Owner.FullName;
                }
                DealMaker.PlainClasses.ShortSale.Adapters.DataManager.Instance.LoadShortSaleDocument("pg", out ssErr);
                _shortSaleControl.DisplayLoadedData();
                _shortSaleControl.EnableAutoSaveEventHandler(true);
            }
            else
                _shortSaleControl.DisableControl();

            //SSX INTEGRATION
            this.ucppclv.clearForm();
            this.ucppc.clearForm();
			this.tbCurrPos.Text = this.currentOffset.ToString() + " / " + htNodePosPropertyItemId.Count.ToString();
			
			

			#region � FirstEntered && LastModified �
			this.tbFirstEntered.Text = pi.DateCreated.ToShortDateString();
            
            if (null != _customSettings && 1 == _customSettings.SpecialSSX)
                this.tbLastModified.Text = pi.DateLastModified.ToString("dd/MM/yy hh:mm tt");//pi.DateLastModified.ToShortDateString();
            else
                this.tbLastModified.Text = pi.DateLastModified.ToShortDateString();
			#endregion � FirstEntered && LastModified �
			#region � Name (First, Middle, Last, GoBy, Full �
			if (null != pi && null != pi.Owner)
			{
				this.tbFirstname.Text = pi.Owner.FirstName;
				this.tbMiddleName.Text = pi.Owner.MiddleName;
				this.tbLastName.Text = pi.Owner.LastName;
				this.tbFullName.Text = pi.Owner.FullName;
			}
			else
			{
				this.tbFirstname.Text = string.Empty;
				this.tbMiddleName.Text = string.Empty;
				this.tbLastName.Text = string.Empty;
				this.tbFullName.Text = string.Empty;
			}
			#endregion � Name (First, Middle, Last, GoBy, Full �
			
			#region � Header SubDivision �
			if (null != pi && null != pi.DealInfo && null != pi.DealInfo.DistrictProperty)
				this.tbSubDivision.Text = pi.DealInfo.DistrictProperty.Subdivision;
			else
				this.tbSubDivision.Text = string.Empty;
			#endregion � Header SubDivision �
			
			#region � Phone && BestTime (Home, Work, Cell), Fax, Email �
			if (null != pi && null != pi.Owner && null != pi.Owner.HomePhone)
            {
                this.tbHomePh.Text = pi.Owner.HomePhone.PhoneNr.ToString();
                if (pi.Owner.HomePhone.BestTimeToCall != null)
                    this.tbHomeBestTime.Text = pi.Owner.HomePhone.BestTimeToCall.ToString();
                else
                    this.tbHomeBestTime.Text = string.Empty;
            }
			else
			{
				this.tbHomePh.Text = string.Empty;
				this.tbHomeBestTime.Text = string.Empty;
			}

			if (null != pi && null != pi.Owner && null != pi.Owner.WorkPhone)
			{
				this.tbWorkPh.Text = pi.Owner.WorkPhone.PhoneNr.ToString();
				if (null != pi.Owner.WorkPhone.BestTimeToCall)
                {
                    if (pi.Owner.WorkPhone.BestTimeToCall != null)
                        this.tbWorkBestTime.Text = pi.Owner.WorkPhone.BestTimeToCall.ToString();
                    else
                        this.tbWorkBestTime.Text = string.Empty;
                }
			}
			else
			{
				this.tbWorkPh.Text = string.Empty;
				this.tbWorkBestTime.Text = string.Empty;
			}

			if (null != pi && null!= pi.Owner && null != pi.Owner.CellPhone)
            {
                this.tbCellPh.Text = pi.Owner.CellPhone.PhoneNr.ToString();
                if (pi.Owner.CellPhone.BestTimeToCall != null)
                    this.tbCellBestTime.Text = pi.Owner.CellPhone.BestTimeToCall.ToString();
                else
                    this.tbCellBestTime.Text = string.Empty;
            }
			else
			{
				this.tbCellPh.Text = string.Empty;
				this.tbCellBestTime.Text = string.Empty;
			}

			if (null != pi && null != pi.Owner)
			{
				this.tbFax.Text = pi.Owner.FaxNumber.ToString();
				this.tbEmail.Text = pi.Owner.EMail.ToString();
                _stEmail.Text = pi.Owner.STEmail;
                _stCellPhone.Text = pi.Owner.STCellphone;
                _stLandline.Text = pi.Owner.STLandline;
                if (null != pi.Owner.STLastModified && !pi.Owner.STLastModified.Equals(DateTime.MinValue))
                    _stLastUpdated.Text = $" Updated {pi.Owner.STLastModified.ToString("MM/dd/yyyy HH:mm:ss")}";
                else
                    _stLastUpdated.Text = string.Empty;
            }
			else
			{
				this.tbFax.Text = string.Empty;
				this.tbEmail.Text = string.Empty;
                _stEmail.Text = string.Empty;
                _stCellPhone.Text = string.Empty;
                _stLandline.Text = string.Empty;
            }
			#endregion � Phone && BestTime (Home, Work, Cell), Fax, Email �			
			#region � SiteAddress �
			if (null != pi.Owner.SiteAddress)
			{
				this.tbSiteAdd.Text		= pi.Owner.SiteAddress.FullSiteStreetAddress.ToString();
				this.tbSiteCity.Text		= pi.Owner.SiteAddress.SiteCity;
				this.tbSiteState.Text		= pi.Owner.SiteAddress.SiteState;
				this.tbSiteCityState.Text		= pi.Owner.SiteAddress.SiteCitySiteState;
				this.tbSiteZIP.Text		= pi.Owner.SiteAddress.SiteZIP.ToString();
				this.tbCounty.Text		= pi.Owner.SiteAddress.SiteCountry.ToString();
				
			}
			else
			{
				this.tbSiteAdd.Text		= string.Empty;
				this.tbSiteCity.Text	= string.Empty;
				this.tbSiteState.Text	= string.Empty;
				this.tbSiteCityState.Text	= string.Empty;
				this.tbSiteZIP.Text		= string.Empty;
				this.tbCounty.Text		= string.Empty;
			}
			#endregion � SiteAddress �
			#region � Mail Address (Last Known, Previous, Old) �
			if (null != pi.Owner.MailAddress)
			{
				this.tbMailLastKnownAdd.Text		= pi.Owner.MailAddress.FullSiteStreetAddress.ToString().TrimEnd(' ');
				this.tbMailLastKnownCity.Text		= pi.Owner.MailAddress.SiteCity;
				this.tbMailLastKnownState.Text		= pi.Owner.MailAddress.SiteState;
				this.tbMailLastKnownCityState.Text	= pi.Owner.MailAddress.SiteCitySiteState;
				this.tbMailLastKnownZIP.Text		= pi.Owner.MailAddress.SiteZIP.ToString();
			}
			else
			{
				this.tbMailLastKnownAdd.Text		= string.Empty;
				this.tbMailLastKnownCity.Text		= string.Empty;
				this.tbMailLastKnownState.Text		= string.Empty;
				this.tbMailLastKnownCityState.Text	= string.Empty;
				this.tbMailLastKnownZIP.Text		= string.Empty;

			}
			if (null != pi.Owner.SecondAddress)
			{
				this.tbMailPreviousAdd.Text			= pi.Owner.SecondAddress.FullSiteStreetAddress.ToString().TrimEnd(' ');
				this.tbMailPreviousCity.Text		= pi.Owner.SecondAddress.SiteCity;
				this.tbMailPreviousState.Text		= pi.Owner.SecondAddress.SiteState;
				this.tbMailPreviousCityState.Text	= pi.Owner.SecondAddress.SiteCitySiteState;
				this.tbMailPreviousZIP.Text			= pi.Owner.SecondAddress.SiteZIP.ToString();
			}
			else
			{
				this.tbMailPreviousAdd.Text			= string.Empty;
				this.tbMailPreviousCity.Text		= string.Empty;
				this.tbMailPreviousState.Text		= string.Empty;
				this.tbMailPreviousCityState.Text	= string.Empty;
				this.tbMailPreviousZIP.Text			= string.Empty;
			}

			if (null != pi.Owner.ThirdAddress)
			{
				this.tbMailOldAdd.Text			= pi.Owner.ThirdAddress.FullSiteStreetAddress.ToString().TrimEnd(' ');
				this.tbMailOldCity.Text			= pi.Owner.ThirdAddress.SiteCity;
				this.tbMailOldState.Text		= pi.Owner.ThirdAddress.SiteState;
				this.tbMailOldCityState.Text			= pi.Owner.ThirdAddress.SiteCitySiteState;
				this.tbMailOldZIP.Text			= pi.Owner.ThirdAddress.SiteZIP.ToString();			
			}
			else
			{
				this.tbMailOldAdd.Text			= string.Empty;
				this.tbMailOldCity.Text			= string.Empty;
				this.tbMailOldState.Text		= string.Empty;
				this.tbMailOldCityState.Text	= string.Empty;
				this.tbMailOldZIP.Text			= string.Empty;
			}
			#endregion � Mail Address (Last Known, Previous, Old) �
										
			#region Listing Agent 
			if (null != pi.ListingAgent)
			{
				this.tb_II_LI_ListingPrice.Text		= pi.ListingAgent.ListingPrice.ToString();				
				this.tb_II_LI_ListingMLSNumber.Text = pi.ListingAgent.ListingAgentMLSNumber;
				this.tb_II_LI_ListingOffice.Text	= pi.ListingAgent.ListingAgentOffice;
				this.tb_II_LI_LA_FirstName.Text		= pi.ListingAgent.ListingAgentFirstName;
				this.tb_II_LI_LA_LastName.Text		= pi.ListingAgent.ListingAgentLastName;
				this.tb_II_LI_LA_FullName.Text		= pi.ListingAgent.ListingAgentFullName;
				this.tb_II_LI_LA_AgentCode.Text		= pi.ListingAgent.ListingAgentCode;
				this.tb_II_LI_LA_PhoneNumber.Text	= pi.ListingAgent.ListingAgentPhoneNumber;
				this.tb_II_LI_LA_FaxNumber.Text		= pi.ListingAgent.ListingAgentFaxNumber;
				this.tb_II_LI_LA_Email.Text			= pi.ListingAgent.ListingAgentEMail;
				//this.tb_II_LI_Comments.Text = pi.ListingAgent.
			}
			else
			{
				this.tb_II_LI_ListingPrice.Text		= string.Empty;				
				this.tb_II_LI_ListingMLSNumber.Text = string.Empty;
				this.tb_II_LI_ListingOffice.Text	= string.Empty;
				this.tb_II_LI_LA_FirstName.Text		= string.Empty;
				this.tb_II_LI_LA_LastName.Text		= string.Empty;
				this.tb_II_LI_LA_FullName.Text		= string.Empty;
				this.tb_II_LI_LA_AgentCode.Text		= string.Empty;
				this.tb_II_LI_LA_PhoneNumber.Text	= string.Empty;
				this.tb_II_LI_LA_FaxNumber.Text		= string.Empty;
				this.tb_II_LI_LA_Email.Text			= string.Empty;
			}
			#endregion Listing Agent 		
			#region � DealInfo �
			if (null != pi.DealInfo)
			{				
				#region SiteAddress 
				if (null != pi.Owner && null != pi.Owner.SiteAddress)
				{
					//Property Characteristics
					this.tb_II_PC_County.Text			= pi.Owner.SiteAddress.SiteCountry;

					//Listing Info

					//ForeclosureInfo
					this.tb_II_FI_County.Text = pi.Owner.SiteAddress.SiteCountry;

					//Financials

					//Tax Info

					//Other Info
					this.tb_II_OI_County.Text			= pi.Owner.SiteAddress.SiteCountry;
				}
				else
				{					
					this.tb_II_PC_County.Text			= string.Empty;				
					this.tb_II_FI_County.Text			= string.Empty;
					this.tb_II_OI_County.Text			= string.Empty;
				}
				#endregion SiteAddress 
				#region MailAddress 
				if (null != pi.Owner && null != pi.Owner.MailAddress)
				{
					//Property Characteristics

					//Listing Info

					//ForeclosureInfo

					//Financials

					//Tax Info
					this.tb_II_TI_MailFullStreetAddress.Text	= pi.Owner.MailAddress.FullSiteStreetAddress;
					this.tb_II_F_MailCityState.Text				= pi.Owner.MailAddress.SiteCitySiteState;
					this.tb_II_F_MailZIP.Text					= pi.Owner.MailAddress.SiteZIP;

					//Other Info
				}
				else
				{
					this.tb_II_TI_MailFullStreetAddress.Text	= string.Empty;
					this.tb_II_F_MailCityState.Text				= string.Empty;
					this.tb_II_F_MailZIP.Text					= string.Empty;
				}
				#endregion MailAddress 
				#region DistrictProperty 
				if (null != pi.DealInfo.DistrictProperty)
				{
					//Header
					this.tbSubDivision.Text = pi.DealInfo.DistrictProperty.Subdivision;
					
					//Property Characteristics
					this.tb_II_PC_Subdivision.Text		= pi.DealInfo.DistrictProperty.Subdivision;
					this.tb_II_PC_LegalDesc.Text		= pi.DealInfo.DistrictProperty.LegalDescription;
					

					//Listing Info

					//ForeclosureInfo
					this.tb_II_FI_Subdivision.Text = pi.DealInfo.DistrictProperty.Subdivision;
					

					//Financials

					//Tax Info

					//Other Info
					this.tb_II_OU_Subdividion.Text		= pi.DealInfo.DistrictProperty.Subdivision;
					this.tb_II_OU_LegalDescription.Text = pi.DealInfo.DistrictProperty.LegalDescription;
					this.tb_II_OU_Vesting.Text			= pi.DealInfo.DistrictProperty.Vesting;
					this.tb_II_OU_MapGrid.Text			= pi.DealInfo.DistrictProperty.MapGrid;
					this.tb_II_OU_MapArea.Text			= pi.DealInfo.DistrictProperty.MapArea;

				}
				else
				{					
					this.tbSubDivision.Text				= string.Empty;		
					this.tb_II_PC_Subdivision.Text		= string.Empty; 
					this.tb_II_PC_LegalDesc.Text		= string.Empty;
					this.tb_II_FI_Subdivision.Text		= string.Empty;
					this.tb_II_OU_Subdividion.Text		= string.Empty;
					this.tb_II_OU_LegalDescription.Text = string.Empty;
					this.tb_II_OU_Vesting.Text			= string.Empty;
					this.tb_II_OU_MapGrid.Text			= string.Empty;
					this.tb_II_OU_MapArea.Text			= string.Empty;
				}
				#endregion DistrictProperty 
				#region DivorceProperty 
				if (null != pi.DealInfo.DivorceProperty)
				{
					//Property Characteristics

					//Listing Info

					//ForeclosureInfo

					//Financials

					//Tax Info
					this.tb_II_OU_DivorceStatus.Text	= pi.DealInfo.DivorceProperty.DivorceStatus;
					this.tb_II_OU_DivorceDate.Text		= pi.DealInfo.DivorceProperty.DivorceDate.ToString();

					//Other Info

				}
				else
				{
					this.tb_II_OU_DivorceStatus.Text	= string.Empty;
					this.tb_II_OU_DivorceDate.Text		= string.Empty;
				}
				#endregion DivorceProperty 
				#region FinancialProperty 
				if (null != pi.DealInfo.FinancialProperty)
				{
					//Property Characteristics

					//Listing Info
					this.tb_II_LI_Status.Text = pi.DealInfo.FinancialProperty.Status;

					//ForeclosureInfo
					this.tb_II_FI_APN.Text = pi.DealInfo.FinancialProperty.ParcelNumber;
					this.tb_II_FI_EstimatedEquity.Text  = pi.DealInfo.FinancialProperty.Equity.ToString();
					this.tb_II_FI_LastSaleValue.Text	= pi.DealInfo.FinancialProperty.LastSaleValue.ToString();
					this.tb_II_FI_SaleDocNo.Text		= pi.DealInfo.FinancialProperty.SaleDocumentNumber;
					
					if (-1 == pi.DealInfo.FinancialProperty.DeedDate.IndexOf("1/1/0001"))
					{
						this.tb_II_FI_DeedDate.Text			= pi.DealInfo.FinancialProperty.DeedDate;
					}
					else
					{
						this.tb_II_FI_DeedDate.Text			= string.Empty;
					}

					//Financials					
					this.tb_II_F_SaleValue.Text				= pi.DealInfo.FinancialProperty.LastSaleValue.ToString("F", Globals.format_en_US);				
					
					if (new DateTime(1,1,1) != pi.DealInfo.FinancialProperty.LastSaleDate)
					{
						this.tb_II_F_SaleDate.Text				= pi.DealInfo.FinancialProperty.LastSaleDate.ToShortDateString();
					}
					else
					{
						this.tb_II_F_SaleDate.Text				= string.Empty;
					}

					this.tb_II_F_SaleDocumentNumber.Text	= pi.DealInfo.FinancialProperty.SaleDocumentNumber;											
					this.tb_II_F_LoanAmount.Text			= pi.DealInfo.FinancialProperty.FirstLoan.ToString();
					this.tb_II_F_LoanType.Text				= pi.DealInfo.FinancialProperty.FirstLoanType;
					this.tb_II_F_LoanInterestRate.Text		= pi.DealInfo.FinancialProperty.FirstLoanInterestRate;
					this.tb_II_F_Lender.Text				= pi.DealInfo.FinancialProperty.Lender;
					this.tb_II_F_LoanAmount2.Text				= pi.DealInfo.FinancialProperty.SecondLoan.ToString();				
					this.tb_II_F_CashDownPayment.Text		= pi.DealInfo.FinancialProperty.CashDownPayment.ToString();				
					this.tb_II_F_EstimatedEquity.Text		= pi.DealInfo.FinancialProperty.Equity.ToString();
					this.tb_II_F_EstimatedHome.Text			= pi.DealInfo.FinancialProperty.EstimatedHomeValue.ToString("c");

					this.tb_II_FI_EstimatedHome.Text = pi.DealInfo.FinancialProperty.EstimatedHomeValue.ToString("c");

					if (-1 == pi.DealInfo.FinancialProperty.DeedDate.IndexOf("1/1/0001"))
					{
						this.tb_II_F_DeedDate.Text				= pi.DealInfo.FinancialProperty.DeedDate;
					}
					else
					{
						this.tb_II_F_DeedDate.Text				= string.Empty;
					}
					
					//Tax Info
					this.tb_II_F_APN.Text						= pi.DealInfo.FinancialProperty.ParcelNumber;

					//Other Info

				}
				else
				{
					this.tb_II_LI_Status.Text				= string.Empty;
					this.tb_II_FI_APN.Text					= string.Empty;
					this.tb_II_FI_EstimatedEquity.Text		= string.Empty;
					this.tb_II_FI_LastSaleValue.Text		= string.Empty;
					this.tb_II_FI_SaleDocNo.Text			= string.Empty;
					this.tb_II_F_SaleValue.Text				= string.Empty;
					this.tb_II_F_SaleDate.Text				= string.Empty;
					this.tb_II_F_SaleDocumentNumber.Text	= string.Empty;		
					this.tb_II_F_LoanAmount.Text			= string.Empty;
					this.tb_II_F_LoanType.Text				= string.Empty;
					this.tb_II_F_LoanInterestRate.Text		= string.Empty;
					this.tb_II_F_Lender.Text				= string.Empty;
					this.tb_II_F_LoanAmount2.Text			= string.Empty;
					this.tb_II_F_CashDownPayment.Text		= string.Empty;
					this.tb_II_F_EstimatedEquity.Text		= string.Empty;
					this.tb_II_F_APN.Text					= string.Empty;

					this.tb_II_F_DeedDate.Text				= string.Empty;
					this.tb_II_FI_EstimatedHome.Text		= string.Empty;
				}
				#endregion FinancialProperty 
				#region ForeClosureProperty 
				if (null != pi.DealInfo.ForeClosureProperty)
				{
					//Property Characteristics

					//Listing Info

					//ForeclosureInfo
					this.tb_II_FI_ForeclosureStatus.Text = pi.DealInfo.ForeClosureProperty.ForeclosureStatus;
					this.tb_II_FI_OriginalTrustor.Text = pi.DealInfo.ForeClosureProperty.OriginalTrustor;
					
					if (-1 == pi.DealInfo.ForeClosureProperty.ForeclosureFileDate.IndexOf("1/1/0001"))
					{
						this.tb_II_FI_ForeclosureFileDate.Text	= pi.DealInfo.ForeClosureProperty.ForeclosureFileDate;
					}
					else
					{
						this.tb_II_FI_ForeclosureFileDate.Text	= string.Empty;
					}

					this.tb_II_FI_FileNumber.Text			= pi.DealInfo.ForeClosureProperty.ForeclosureFileNumber;
					if (-1 == pi.DealInfo.ForeClosureProperty.ForeclosureSaleDate.IndexOf("1/1/0001"))
					{
						this.tb_II_FI_Foreclosure_Sale.Text		= pi.DealInfo.ForeClosureProperty.ForeclosureSaleDate;
					}
					else
					{
						this.tb_II_FI_Foreclosure_Sale.Text		= string.Empty;
					}
					this.tb_II_FI_SaleTime.Text				= pi.DealInfo.ForeClosureProperty.ForeclosureSaleTime;
					this.tb_II_FI_TrusteeName.Text			= pi.DealInfo.ForeClosureProperty.TrusteeName;
					this.tb_II_FI_TrusteePhoneNr.Text		= pi.DealInfo.ForeClosureProperty.TrusteePhoneNumber;
					this.tb_II_FI_TrusteeSaleNr.Text		= pi.DealInfo.ForeClosureProperty.TrusteeSaleNumber;
					this.tb_II_FI_TrusteeFullStreetAddress.Text	= pi.DealInfo.ForeClosureProperty.TrusteeFullStreetAddress;
					this.tb_II_FI_City.Text					= pi.DealInfo.ForeClosureProperty.TrusteeCity;
					this.tb_II_FI_State.Text				= pi.DealInfo.ForeClosureProperty.TrusteeState;
					this.tb_II_FI_ZIP.Text					= pi.DealInfo.ForeClosureProperty.TrusteeZip;
					this.tb_II_FI_OrigMorgageBalance.Text	= pi.DealInfo.ForeClosureProperty.OriginalMortgageBalance;
					this.tb_II_FI_UnpaidBalance.Text		= pi.DealInfo.ForeClosureProperty.UnpaidBalance;
					this.tb_II_FI_OpeningBidAmount.Text		= pi.DealInfo.ForeClosureProperty.OpenBidAmount;
					this.tb_II_FI_MortgageBenefFirst.Text	= pi.DealInfo.ForeClosureProperty.MortgageBeneficiaryFirst;
					this.tb_II_FI_MortgageBeneLast.Text		= pi.DealInfo.ForeClosureProperty.MortgageBeneficiaryLast;
					this.tb_II_FI_LoanRecNumber.Text		= pi.DealInfo.ForeClosureProperty.LoanRecordingNumber.ToString();
					if (-1 == pi.DealInfo.ForeClosureProperty.LoanRecordingDate.IndexOf("1/1/0001"))
					{
						this.tb_II_FI_RecordingDate.Text		= pi.DealInfo.ForeClosureProperty.LoanRecordingDate.ToString();
					}
					else
					{
						this.tb_II_FI_RecordingDate.Text		= string.Empty;
					}
					this.tb_II_FI_OriginalTrustor.Text		= pi.DealInfo.ForeClosureProperty.OriginalTrustor.ToString();
					this.tb_II_FI_SaleTime.Text				= pi.DealInfo.ForeClosureProperty.ForeclosureSaleTime.ToString();
					this.tb_II_FI_SalePrice.Text			= pi.DealInfo.ForeClosureProperty.ForeclosureSalePlace.ToString();
					this.tb_II_FI_TrusteeSaleNr.Text		= pi.DealInfo.ForeClosureProperty.TrusteeSaleNumber.ToString();
					this.tb_II_FI_TrusteeName.Text			= pi.DealInfo.ForeClosureProperty.TrusteeName.ToString();
					this.tb_II_FI_TrusteePhoneNr.Text		= pi.DealInfo.ForeClosureProperty.TrusteePhoneNr.ToString();
					this.tb_II_FI_TrusteeFullStreetAddress.Text = pi.DealInfo.ForeClosureProperty.TrusteeFullStreetAddress.ToString();
					this.tb_II_FI_City.Text					= pi.DealInfo.ForeClosureProperty.TrusteeCity.ToString();
					this.tb_II_FI_State.Text				= pi.DealInfo.ForeClosureProperty.TrusteeState.ToString();
					this.tb_II_FI_ZIP.Text					= pi.DealInfo.ForeClosureProperty.TrusteeZip.ToString();
					this.tb_II_FI_MortgageBenefFirst.Text	= pi.DealInfo.ForeClosureProperty.MortgageBeneficiaryFirst.ToString();
					this.tb_II_FI_MortgageBeneLast.Text		= pi.DealInfo.ForeClosureProperty.MortgageBeneficiaryLast.ToString();

					//Financials
					//this.tb_II_F_SaleDate.Text			= pi.DealInfo.ForeClosureProperty.ForeclosureSaleDate;
					//this.tb_II_F_DeedDate.Text			= pi.DealInfo.ForeClosureProperty.DeedDate;

					//Tax Info

					//Other Info

				}
				else
				{
					this.tb_II_FI_ForeclosureStatus.Text		= string.Empty;
					this.tb_II_FI_OriginalTrustor.Text			= string.Empty;
					this.tb_II_FI_DeedDate.Text					= string.Empty;
					this.tb_II_FI_ForeclosureFileDate.Text		= string.Empty;
					this.tb_II_FI_FileNumber.Text				= string.Empty;
					this.tb_II_FI_Foreclosure_Sale.Text			= string.Empty;
					this.tb_II_FI_SaleTime.Text					= string.Empty;
					this.tb_II_FI_TrusteeName.Text				= string.Empty;
					this.tb_II_FI_TrusteePhoneNr.Text			= string.Empty;
					this.tb_II_FI_TrusteeSaleNr.Text			= string.Empty;
					this.tb_II_FI_TrusteeFullStreetAddress.Text	= string.Empty;
					this.tb_II_FI_City.Text						= string.Empty;
					this.tb_II_FI_State.Text					= string.Empty;
					this.tb_II_FI_ZIP.Text						= string.Empty;
					this.tb_II_FI_OrigMorgageBalance.Text		= string.Empty;
					this.tb_II_FI_UnpaidBalance.Text			= string.Empty;
					this.tb_II_FI_OpeningBidAmount.Text			= string.Empty;
					this.tb_II_FI_MortgageBenefFirst.Text		= string.Empty;
					this.tb_II_FI_MortgageBeneLast.Text			= string.Empty;

					//Financials
					//this.tb_II_F_SaleDate.Text				= string.Empty;
					//this.tb_II_F_DeedDate.Text					= string.Empty;

				}
				#endregion ForeClosureProperty 
				#region HouseProperty 
				if (null != pi.DealInfo.HouseProperty)
				{
					//Property Characteristics
					this.tb_II_PC_PropertyType.Text			= pi.DealInfo.HouseProperty.PropertyType.ToString();
					this.tb_II_FI_PropertyType.Text			= pi.DealInfo.HouseProperty.PropertyType.ToString();
					this.tb_II_PC_YearBuilt.Text		= pi.DealInfo.HouseProperty.YearBuilt.ToString();
					this.tb_II_PC_Bedrooms.Text			= pi.DealInfo.HouseProperty.Bedrooms.ToString();
					this.tb_II_PC_Bathrooms.Text		= pi.DealInfo.HouseProperty.Bathrooms.ToString();
					this.tb_II_PC_Garage.Text			= pi.DealInfo.HouseProperty.Garage.ToString();
					this.tb_II_PC_Pool.Text				= pi.DealInfo.HouseProperty.Pool.ToString();
					this.tb_II_PC_HomeSqFt.Text			= pi.DealInfo.HouseProperty.HomeSqFt.ToString();
					this.tb_II_PC_LotSqFt.Text			= pi.DealInfo.HouseProperty.LotSqFt.ToString();

					//Listing Info

					//ForeclosureInfo
					this.tb_II_FI_YearBuilt.Text			= pi.DealInfo.HouseProperty.YearBuilt.ToString();
					this.tb_II_FI_HomeSqFt.Text			= pi.DealInfo.HouseProperty.HomeSqFt.ToString();
					this.tb_II_FI_Bedrooms.Text			= pi.DealInfo.HouseProperty.Bedrooms.ToString();
					this.tb_II_FI_Bathrooms.Text			= pi.DealInfo.HouseProperty.Bathrooms.ToString();

					//Financials

					//Tax Info

					//Other Info

				}
				else
				{					
					this.tb_II_PC_YearBuilt.Text			= string.Empty;
					this.tb_II_PC_Bedrooms.Text				= string.Empty; 
					this.tb_II_PC_Bathrooms.Text			=  string.Empty;
					this.tb_II_PC_Garage.Text				=  string.Empty;
					this.tb_II_PC_Pool.Text					=  string.Empty;
					this.tb_II_PC_HomeSqFt.Text				=  string.Empty;
					this.tb_II_PC_LotSqFt.Text				=  string.Empty;					
					this.tb_II_FI_YearBuilt.Text			=  string.Empty;
					this.tb_II_FI_HomeSqFt.Text				=  string.Empty;
					this.tb_II_FI_Bedrooms.Text				=  string.Empty;
					this.tb_II_FI_Bathrooms.Text			=  string.Empty;
					this.tb_II_FI_PropertyType.Text			= string.Empty;
				}
				#endregion HouseProperty 
				#region LeadSourceProperty 
				if (null != pi.DealInfo.LeadSourceProperty)
				{
					//Property Characteristics

					//Listing Info

					//ForeclosureInfo

					//Financials

					//Tax Info

					//Other Info

				}
				#endregion LeadSourceProperty 
				#region LoanProperty 
				if (null != pi.DealInfo.LoanProperty)
				{
					this.tb_II_F_TotalLoans.Text = pi.DealInfo.LoanProperty.TotalLoans.ToString("c");
					//Property Characteristics

					//Listing Info

					//ForeclosureInfo

					//Financials

					//Tax Info

					//Other Info

				}
				else
				{
					this.tb_II_F_TotalLoans.Text = string.Empty;
				}
				#endregion LoanProperty 
				#region OtherProperty 
				if (null != pi.DealInfo.OtherProperty)
				{
					//Property Characteristics

					//Listing Info

					//ForeclosureInfo

					//Financials

					//Tax Info

					//Other Info
					this.tb_II_OU_MailCRRT.Text			= pi.DealInfo.OtherProperty.MailCRRT;
					this.tb_II_OU_MailScore.Text		= pi.DealInfo.OtherProperty.MailScore;
					this.tb_II_OU_PrivacyFlag.Text		= pi.DealInfo.OtherProperty.PrivacyFlag;
					this.tb_II_OU_DoNotCallFlag.Text	= pi.DealInfo.OtherProperty.DoNotCallFlag;

				}
				else
				{
					this.tb_II_OU_MailCRRT.Text			= string.Empty;
					this.tb_II_OU_MailScore.Text		= string.Empty; 
					this.tb_II_OU_PrivacyFlag.Text		= string.Empty;
					this.tb_II_OU_DoNotCallFlag.Text	= string.Empty; 
				}
				#endregion OtherProperty 
				#region TaxProperty 
				if (null != pi.DealInfo.TaxProperty)
				{
					//Property Characteristics
					this.tb_II_PC_UseCode.Text			= pi.DealInfo.TaxProperty.UseCode;

					//Listing Info

					//ForeclosureInfo
					this.tb_II_FI_TaxAssedValue.Text = pi.DealInfo.TaxProperty.TaxAssessedValue.ToString();
					this.tb_II_FI_UseCode.Text = pi.DealInfo.TaxProperty.UseCode;

					//Financials

					//Tax Info
					this.tb_II_F_TaxRateArea.Text				= pi.DealInfo.TaxProperty.TaxRateArea;
					this.tb_II_F_TaxExemption.Text				= pi.DealInfo.TaxProperty.TaxExemption;
					this.tb_II_F_TaxStatus.Text					= pi.DealInfo.TaxProperty.TaxStatus;
					this.tb_II_F_TaxDeliquentYear.Text			= pi.DealInfo.TaxProperty.TaxDeliquentYear;
					this.tb_II_F_TaxAssessedValue.Text			= pi.DealInfo.TaxProperty.TaxAssessedValue.ToString();
					this.tb_II_F_FullCashValue.Text				= pi.DealInfo.TaxProperty.FullCashValue;
					this.tb_II_F_TaxAmount.Text					= pi.DealInfo.TaxProperty.TaxAmount;
					this.tb_II_F_LandValue.Text					= pi.DealInfo.TaxProperty.LandValue;
					this.tb_II_F_PercentImprovement.Text		= pi.DealInfo.TaxProperty.PercentImprovement;
					this.tb_II_F_OwnerAbsentOccupied.Text		= pi.DealInfo.TaxProperty.OwnerAbsentOccupied;

					//Other Info

				}
				else
				{
					this.tb_II_PC_UseCode.Text					= string.Empty;
					this.tb_II_FI_TaxAssedValue.Text			= string.Empty; 
					this.tb_II_FI_UseCode.Text					= string.Empty; 
					this.tb_II_F_TaxRateArea.Text				= string.Empty; 
					this.tb_II_F_TaxExemption.Text				= string.Empty; 
					this.tb_II_F_TaxStatus.Text					= string.Empty; 
					this.tb_II_F_TaxDeliquentYear.Text			= string.Empty; 
					this.tb_II_F_TaxAssessedValue.Text			= string.Empty; 
					this.tb_II_F_FullCashValue.Text				= string.Empty; 
					this.tb_II_F_TaxAmount.Text					= string.Empty; 
					this.tb_II_F_LandValue.Text					= string.Empty; 
					this.tb_II_F_PercentImprovement.Text		= string.Empty; 
					this.tb_II_F_OwnerAbsentOccupied.Text		= string.Empty; 
				}
				#endregion TaxProperty 
				#region � ForeClosureInfo �
				if (null != pi.DealInfo.ForeClosureProperty)
				{
					
																				
					//tb_II_FI_PropertyType = pi.p
					
					
					//tb_II_FI_EstimatedHome = pi.DealInfo.ForeClosureProperty.																					
					//tb_II_FI_SalePrice			= pi.DealInfo.ForeClosureProperty.forecl;
					
					//tb_II_FI_LoanRecNumber				= pi.DealInfo.ForeClosureProperty.
					//tb_II_FI_RecordingDate
					
				}
				#endregion � ForeClosureInfo �				
				#region � PropertyCharacteristics �
				//this.tb_II_PC_PropertyType		
				#endregion � PropertyCharacteristics �
				#region � Financials �
				if (null != pi.DealInfo.FinancialProperty)
				{
					tb_II_F_CostPerSqFt.Text = pi.DealInfo.FinancialProperty.CostPerSquareFoot.ToString("c");
				}
				else
				{
					tb_II_F_CostPerSqFt.Text = string.Empty;
				}
				//tb_II_F_TotalLoans			= pi.DealInfo.FinancialProperty.
				//tb_II_F_EstimatedHome		= 
				#endregion � Financials �				
				#region � Other Info �
				//tb_II_OU_UF1			= pi.DealInfo.OtherProperty.u
				//tb_II_OU_UF2
				//tb_II_OU_UF3
				//tb_II_OU_UF4
				//tb_II_OU_UF5*/
				#endregion � Other Info �				
			}
			else
			{
				this.tb_II_PC_County.Text			= string.Empty;				
				this.tb_II_FI_County.Text			= string.Empty;
				this.tb_II_OI_County.Text			= string.Empty;

				this.tb_II_TI_MailFullStreetAddress.Text	= string.Empty;
				this.tb_II_F_MailCityState.Text				= string.Empty;
				this.tb_II_F_MailZIP.Text					= string.Empty;

				this.tbSubDivision.Text				= string.Empty;		
				this.tb_II_PC_Subdivision.Text		= string.Empty; 
				this.tb_II_PC_LegalDesc.Text		= string.Empty;
				this.tb_II_FI_Subdivision.Text		= string.Empty;
				this.tb_II_OU_Subdividion.Text		= string.Empty;
				this.tb_II_OU_LegalDescription.Text = string.Empty;
				this.tb_II_OU_Vesting.Text			= string.Empty;
				this.tb_II_OU_MapGrid.Text			= string.Empty;
				this.tb_II_OU_MapArea.Text			= string.Empty;

				this.tb_II_OU_DivorceStatus.Text	= string.Empty;
				this.tb_II_OU_DivorceDate.Text		= string.Empty;

				this.tb_II_LI_Status.Text				= string.Empty;
				this.tb_II_FI_APN.Text					= string.Empty;
				this.tb_II_FI_EstimatedEquity.Text		= string.Empty;
				this.tb_II_FI_LastSaleValue.Text		= string.Empty;
				this.tb_II_FI_SaleDocNo.Text			= string.Empty;
				this.tb_II_F_SaleValue.Text				= string.Empty;
				this.tb_II_F_SaleDate.Text				= string.Empty;
				this.tb_II_F_SaleDocumentNumber.Text	= string.Empty;		
				this.tb_II_F_LoanAmount.Text			= string.Empty;
				this.tb_II_F_LoanType.Text				= string.Empty;
				this.tb_II_F_LoanInterestRate.Text		= string.Empty;
				this.tb_II_F_Lender.Text				= string.Empty;
				this.tb_II_F_LoanAmount2.Text			= string.Empty;
				this.tb_II_F_CashDownPayment.Text		= string.Empty;
				this.tb_II_F_EstimatedEquity.Text		= string.Empty;
				this.tb_II_F_APN.Text					= string.Empty;

				this.tb_II_F_DeedDate.Text				= string.Empty;

				this.tb_II_FI_ForeclosureStatus.Text		= string.Empty;
				this.tb_II_FI_OriginalTrustor.Text			= string.Empty;
				this.tb_II_FI_DeedDate.Text					= string.Empty;
				this.tb_II_FI_ForeclosureFileDate.Text		= string.Empty;
				this.tb_II_FI_FileNumber.Text				= string.Empty;
				this.tb_II_FI_Foreclosure_Sale.Text			= string.Empty;
				this.tb_II_FI_SaleTime.Text					= string.Empty;
				this.tb_II_FI_TrusteeName.Text				= string.Empty;
				this.tb_II_FI_TrusteePhoneNr.Text			= string.Empty;
				this.tb_II_FI_TrusteeSaleNr.Text			= string.Empty;
				this.tb_II_FI_TrusteeFullStreetAddress.Text	= string.Empty;
				this.tb_II_FI_City.Text						= string.Empty;
				this.tb_II_FI_State.Text					= string.Empty;
				this.tb_II_FI_ZIP.Text						= string.Empty;
				this.tb_II_FI_OrigMorgageBalance.Text		= string.Empty;
				this.tb_II_FI_UnpaidBalance.Text			= string.Empty;
				this.tb_II_FI_OpeningBidAmount.Text			= string.Empty;
				this.tb_II_FI_MortgageBenefFirst.Text		= string.Empty;
				this.tb_II_FI_MortgageBeneLast.Text			= string.Empty;

				this.tb_II_PC_YearBuilt.Text			= string.Empty;
				this.tb_II_PC_Bedrooms.Text				= string.Empty; 
				this.tb_II_PC_Bathrooms.Text			=  string.Empty;
				this.tb_II_PC_Garage.Text				=  string.Empty;
				this.tb_II_PC_Pool.Text					=  string.Empty;
				this.tb_II_PC_HomeSqFt.Text				=  string.Empty;
				this.tb_II_PC_LotSqFt.Text				=  string.Empty;					
				this.tb_II_FI_YearBuilt.Text			=  string.Empty;
				this.tb_II_FI_HomeSqFt.Text				=  string.Empty;
				this.tb_II_FI_Bedrooms.Text				=  string.Empty;
				this.tb_II_FI_Bathrooms.Text			=  string.Empty;

				this.tb_II_OU_MailCRRT.Text			= string.Empty;
				this.tb_II_OU_MailScore.Text		= string.Empty; 
				this.tb_II_OU_PrivacyFlag.Text		= string.Empty;
				this.tb_II_OU_DoNotCallFlag.Text	= string.Empty; 

				this.tb_II_PC_UseCode.Text					= string.Empty;
				this.tb_II_FI_TaxAssedValue.Text			= string.Empty; 
				this.tb_II_FI_UseCode.Text					= string.Empty; 
				this.tb_II_F_TaxRateArea.Text				= string.Empty; 
				this.tb_II_F_TaxExemption.Text				= string.Empty; 
				this.tb_II_F_TaxStatus.Text					= string.Empty; 
				this.tb_II_F_TaxDeliquentYear.Text			= string.Empty; 
				this.tb_II_F_TaxAssessedValue.Text			= string.Empty; 
				this.tb_II_F_FullCashValue.Text				= string.Empty; 
				this.tb_II_F_TaxAmount.Text					= string.Empty; 
				this.tb_II_F_LandValue.Text					= string.Empty; 
				this.tb_II_F_PercentImprovement.Text		= string.Empty; 
				this.tb_II_F_OwnerAbsentOccupied.Text		= string.Empty;
				this.tb_II_F_TotalLoans.Text = string.Empty;
				this.tb_II_F_EstimatedHome.Text	= string.Empty;
				this.tb_II_F_CostPerSqFt.Text = string.Empty;
				this.tb_II_FI_PropertyType.Text			= string.Empty;
				this.tb_II_FI_RecordingDate.Text = string.Empty;
			}
			#endregion � DealInfo �			
			#region DealProperty 
			if (null != pi.DealProperty)
			{
				//1st Row				
				this.cbType.SelectedItem = pi.DealProperty.Type;				
				this.cbBedrooms.SelectedItem = pi.DealProperty.Bedrooms;
				this.cbBaths.SelectedItem = pi.DealProperty.Baths;
				this.tbSqFt.Text = pi.DealProperty.Sqft;
				this.cbYearBuilt.SelectedItem = pi.DealProperty.YearBuilt;
				this.cbGarage.SelectedItem = pi.DealProperty.Garage;
				this.cbPool.SelectedItem = pi.DealProperty.Pool;
				this.cbOccupiedBy.SelectedItem = pi.DealProperty.OccupiedBy;
				this.tbAPN.Text = pi.DealProperty.Apn;

				//2nd Row
				this.tbCondition.Text = pi.DealProperty.FeatureCondition;
				this.tbWhySelling.Text = pi.DealProperty.WhySelling;
				this.tbWorthBySeller.Text = pi.DealProperty.WorthBySeller;
				this.cbSource.SelectedItem = pi.DealProperty.Source;

				//3rd Row
				this.tb1Bal.Text = pi.DealProperty.FirstBal;
				this.tb1IntRate.Text = pi.DealProperty.Ir1;
				this.cb1Type.SelectedItem = pi.DealProperty.Type1;
				this.tb1MoPayment.Text = pi.DealProperty.MoPay1;
				this.cbTaxIncl.SelectedItem = pi.DealProperty.TaxIns;
				this.tbTaxAmount.Text = pi.DealProperty.TaxAmnt;

				//4th row
				this.tb2Bal.Text = pi.DealProperty.SecondBal;
				this.tb2IntRate.Text = pi.DealProperty.Ir2;
				this.cb2Type.SelectedItem = pi.DealProperty.Type2;
				this.tb2MoPayment.Text = pi.DealProperty.MoPay2;
				this.tbHOA.Text = pi.DealProperty.Hoa;

				//5th row
				this.tb3Bal.Text = pi.DealProperty.ThirdBal;
				this.tb3IntRate.Text = pi.DealProperty.Ir3;
				this.cb3Type.SelectedItem = pi.DealProperty.Type3;
				this.tb3MoPayment.Text = pi.DealProperty.MoPay3;
				this.tbOtherLiens.Text = pi.DealProperty.OtherLiens;
			
				//6th row
				this.cbPmntsCurrnet.SelectedItem = pi.DealProperty.PmntsCurrent;
				this.cbMoBehind.SelectedItem = pi.DealProperty.MoBehind;
				this.tbReinsAmnt.Text = pi.DealProperty.ReInsAmnt;
				this.cbInForecl.SelectedItem = pi.DealProperty.InForecl;
				if (pi.DealProperty.ForSaleDate > DateTime.MinValue && pi.DealProperty.ForSaleDate < DateTime.MaxValue)
				{
					this.dtpForSaleDate.Value = pi.DealProperty.ForSaleDate;
				}
				else
				{
					this.dtpForSaleDate.Enabled = false;
				}

				if (pi.DealProperty.ForSaleDate > new DateTime(1753, 1, 1))// DateTime.MinValue && pi.DealProperty.ForSaleDate < DateTime.MaxValue)
				{
					this.tbForSaleDate.Text = pi.DealProperty.ForSaleDate.ToShortDateString();
				}	
				else
				{
					this.tbForSaleDate.Text = string.Empty;
				}

				this.cbListed.SelectedItem = pi.DealProperty.Listed;
                if (pi.DealProperty.ListExpDate > new DateTime(1753, 1, 1))//DateTime.MinValue && pi.DealProperty.ListExpDate < DateTime.MaxValue)
				{
					this.dtpListingExpDate.Value = pi.DealProperty.ListExpDate;
				}
				else
				{
					//this.dtpListingExpDate.Enabled = false;
				}
				if (pi.DealProperty.ListExpDate > new DateTime(1753, 1, 1)) //DateTime.MinValue && pi.DealProperty.ListExpDate < DateTime.MaxValue)
				{
					this.tbListingExpDate.Text = pi.DealProperty.ListExpDate.ToShortDateString();
				}
				else
				{
					this.tbListingExpDate.Text = string.Empty;
				}

				this.cbCanCancel.SelectedItem = pi.DealProperty.CanCancelList;

				//7th row
				this.tbLowestPriceAcceptable.Text = pi.DealProperty.LowestPriceAcceptable;
				this.cbSell4What.SelectedItem = pi.DealProperty.Sell4WhatOwes;
				this.tbMinCashNeeded.Text = pi.DealProperty.MinCashNeededWhy;
				this.tbDesiredClosingDate.Text = pi.DealProperty.DesiredClosingDate;

				//8th row
				this.cbMotivation.SelectedItem = pi.DealProperty.Motivation;

                if (null != pi.DealProperty.LeadSource)
                {
                    if (false == this.cbLeadSource.Items.Contains(pi.DealProperty.LeadSource))
                    {
                        Media leadSrcMedia = new Media();
                        leadSrcMedia.MediaName = pi.DealProperty.LeadSource;
                        ApplicationHelper.SaveMedia(leadSrcMedia);
                        this.cbLeadSource.Items.Clear();
                        this.LoadMedia();
                    }

                    this.cbLeadSource.SelectedItem = pi.DealProperty.LeadSource;
                }
                else
                    this.cbLeadSource.SelectedItem = null; 
				
				if (null != pi.DealProperty)
					this.tbARV.Text = pi.DealProperty.Arv;//this.ucppclv.estimatedValue.ToString(); //

				this.tbRepairs.Text = pi.DealProperty.Repairs;

                if (ContactTypes.Buyer != _contactType && ContactTypes.Lender != _contactType)
				    this.tbNextStep.Text = pi.DealProperty.NextStep;    //seller
                else
                    _ci_NotesField.Text = pi.DealProperty.NextStep;


				//update total owed && total monthly
				this.tbTotalOwed.Text = ApplicationHelper.GetTotalOwed(pi).ToString();
				this.tbTotalMonthly.Text = ApplicationHelper.GetTotalMonthlyPayment(pi).ToString();

			}
			else
			{
				//1st Row
				this.cbType.SelectedItem = null;
				this.cbBedrooms.SelectedItem = null;
				this.cbBaths.SelectedItem = null;
				this.tbSqFt.Text = null;
				this.cbYearBuilt.SelectedItem = null;
				this.cbGarage.SelectedItem = null;
				this.cbPool.SelectedItem = null;
				this.cbOccupiedBy.SelectedItem = null;
				this.tbAPN.Text = null;

				//2nd Row
				this.tbCondition.Text = null;
				this.tbWhySelling.Text = null;
				this.tbWorthBySeller.Text = null;
				this.cbSource.SelectedItem = null;

				//3rd Row
				this.tb1Bal.Text = null;
				this.tb1IntRate.Text = null;
				this.cb1Type.SelectedItem = null;
				this.tb1MoPayment.Text = null;
				this.cbTaxIncl.SelectedItem = null;
				this.tbTaxAmount.Text = null;

				//4th row
				this.tb2Bal.Text = null;
				this.tb2IntRate.Text = null;
				this.cb2Type.SelectedItem = null;
				this.tb2MoPayment.Text = null;
				this.tbHOA.Text = null;

				//5th row
				this.tb3Bal.Text = null;
				this.tb3IntRate.Text = null;
				this.cb3Type.SelectedItem = null;
				this.tb3MoPayment.Text = null;
				this.tbOtherLiens.Text = null;
			
				//6th row
				this.cbPmntsCurrnet.SelectedItem = null;
				this.cbMoBehind.SelectedItem = null;
				this.tbReinsAmnt.Text = null;
				this.cbInForecl.SelectedItem = null;
				this.dtpForSaleDate.Value = DateTime.Now;
				this.tbForSaleDate.Text = string.Empty;
				this.cbListed.SelectedItem = null;
				this.dtpListingExpDate.Value = DateTime.Now;
				this.tbListingExpDate.Text = string.Empty;
				this.cbCanCancel.SelectedItem = null;

				//7th row
				this.tbLowestPriceAcceptable.Text = null;
				this.cbSell4What.SelectedItem = null;
				this.tbMinCashNeeded.Text = null;
				this.tbDesiredClosingDate.Text = null;

				//8th row
				this.cbMotivation.SelectedItem = null;
				this.cbLeadSource.SelectedItem = null;
				this.tbARV.Text =  null;;
				this.tbRepairs.Text =  null;
				this.tbNextStep.Text =  null;
			}
			#endregion DealProperty 
																		
			#region � ActivitiesHistory �

			if(taskAndHistoryList == null) putTaskList();			
			taskAndHistoryList.User = this.pi;

			#endregion � ActivitiesHistory �									
			
			
			Connection conn = Connection.getInstance();     
			SqlConnection sqlConn = conn.HatchConn;			
			
            //SSX INTEGRATION
            this.ucppclv.setInitialData(
                (null != pi && null != pi.Owner && null != pi.Owner.SiteAddress) ? pi.Owner.SiteAddress.SiteStreetNumber : string.Empty,
                (null != pi && null != pi.Owner && null != pi.Owner.SiteAddress) ? pi.Owner.SiteAddress.SiteStreetPreDirectional : string.Empty,
                (null != pi && null != pi.Owner && null != pi.Owner.SiteAddress) ? pi.Owner.SiteAddress.SiteStreetName : string.Empty,
                (null != pi && null != pi.Owner && null != pi.Owner.SiteAddress) ? pi.Owner.SiteAddress.SiteStreetPostDirectional : string.Empty,
                (null != pi && null != pi.Owner && null != pi.Owner.SiteAddress) ? pi.Owner.SiteAddress.SiteStreetSuffix : string.Empty,
                (null != pi && null != pi.Owner && null != pi.Owner.SiteAddress) ? pi.Owner.SiteAddress.SiteStreetUnitNumber : string.Empty,
                (null != pi && null != pi.Owner && null != pi.Owner.SiteAddress) ? pi.Owner.SiteAddress.SiteZIP : string.Empty,
                (null != pi && null != pi.Owner && null != pi.Owner.SiteAddress) ? pi.Owner.SiteAddress.SiteCountry : string.Empty,
                (null != pi && null != pi.Owner && null != pi.Owner.SiteAddress) ? pi.Owner.SiteAddress.SiteState : string.Empty,
                (null != pi && null != pi.DealInfo && null != pi.DealInfo.HouseProperty ? pi.DealInfo.HouseProperty.HomeSqFt : 0),
                pi.IdPropertyItem
                );			
            this.ucppc.setInitialData(pi.IdPropertyItem);
            
            //repairs
            this._repairs.EnableAutoSaveEventHandler(false);
            this._repairs.SetData(
                    pi.IdPropertyItem,
                    (null != pi && null != pi.Owner && null != pi.Owner.SiteAddress && null != pi.Owner.SiteAddress.FullSiteStreetAddress ? pi.Owner.SiteAddress.FullSiteStreetAddress : string.Empty), 
                    (null != pi && null != pi.Owner && null != pi.Owner.FullName ? pi.Owner.FullName : string.Empty)
                    );
            this._repairs.EnableAutoSaveEventHandler(true);

            string add = string.Empty;
            if (null != pi && null != pi.Owner && null != pi.Owner.SiteAddress)
            {
                add = pi.Owner.SiteAddress.FullSiteStreetAddress;

                if (string.Empty != add && string.Empty != pi.Owner.SiteAddress.SiteCity)
                    add += " " + pi.Owner.SiteAddress.SiteCity;

                if (string.Empty != add && string.Empty != pi.Owner.SiteAddress.SiteState)
                    add += " " + pi.Owner.SiteAddress.SiteState;

                if (string.Empty != add && string.Empty != pi.Owner.SiteAddress.SiteZIP)
                    add += ", " + pi.Owner.SiteAddress.SiteZIP;

                this._commercialControl.SetData(add);
            }

			//commercials
            if (null != Globals.activeModules && Globals.activeModules.MOD_9)
            {
                this._commercialControl.RemoveEventHandlers();
                _commercialControl.SwitchIncomeTab(BuildingInfoObject.Instance.GetIncomeControlIndex());
                                
                this._commercialControl.SetAutoSaveInterface(this as IAutoSave);

                this._commercialControl.SetData(BuildingInfoObject.Instance);
                this._commercialControl.SetData(IncomeObject.Instance);
                this._commercialControl.SetIncomeData(BuildingInfoObject.Instance.GetIncomeControlIndex(), IncomeObject.Instance);
                this._commercialControl.SetData(ExpensesObject.Instance);
                this._commercialControl.SetData(LoansObject.Instance);
                this._commercialControl.SetData(FinancialInfoObject.Instance);
                this._commercialControl.SetData(MultiyearAnalysisObject.Instance);

                this._commercialControl.AddEventHandlers();
            }
            else
            {
                _commercialControl.DisableControl();
            }


            //BuyerInfo
            this._buyerInfoOverview.EnableAutoSaveEventHandler(false);
            this._buyerInfoOverview.SetData(add);
            this._buyerInfoOverview.FillForm();
            this._buyerInfoOverview.EnableAutoSaveEventHandler(true);

            //PictureManager
            _pbHouse.Image = PictureManager.Instance.GetData(pi.IdPropertyItem);

            //SkipTracedData
            if (!string.IsNullOrEmpty(pi.Owner.ST_P1_RESPONSE))
            {
                try
                {
                    var st = JsonConvert.DeserializeObject<DataAppendService.Lib.Models.IDI.SkiptraceClientResultset>(pi.Owner.ST_P1_RESPONSE);
                    UpdateSkipTracedTab(st);
                }
                catch (Exception exc)
                {
                    InvalidateSkipTracedTab();
                    //?? just log somewhere                    
                }
            }
            else
                InvalidateSkipTracedTab();

            this._contactTab.Refresh();
			this.bUpdate = false;
			Globals.oldContactScreen = this;

			//26.05.07: new user bug correction
			if(needRecount) Recount();
			//~
			
			//AUTOSAVE
			_enableAutoSaveTrigger = true;
		}
		#endregion � LoadDisplayData �											
		#region tabControl1_SelectedIndexChanged 
		private void tabControl1_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			TabControl tc = (TabControl)sender;
			switch (tc.SelectedIndex)
			{
				case 0:		//Contact Info
					break;
				case 1:		//Deal Info
					if (null != this.htNodePosPropertyItemId)
					{
						Guid currentPropertyItemId = (Guid)this.htNodePosPropertyItemId[this.currentOffset];
						this.DisplayGroupMembershipData(currentPropertyItemId);
					}
					break;
				case 2:		//Commercials Info
                    if ((null != Globals.activeModules) && !Globals.activeModules.MOD_9)
                        _commercialControl.DisableControl();
					break;
				case 3:		//Group Membership
					break;
				case 4:		//Activities History
					break;
				case 5:		//Evaluate
					break;
                case 6:     //Short Sale
                    if ((null != Globals.activeModules) && !Globals.activeModules.MOD_8)
                        _shortSaleControl.DisableControl();
                    else
                    {
                        _shortSaleControl.SetOfficeStaffList(UserInformationManager.Instance.GetValues(UserEntries.OfficeStaffList));

                        if (null != pi)
                        {
                            DealMaker.PlainClasses.ShortSale.Adapters.DataManager.Instance.PropertyItemID = pi.IdPropertyItem;

                            if (null != pi.Owner && null != pi.Owner.SiteAddress)
                            {
                                DealMaker.PlainClasses.ShortSale.Adapters.DataManager.Instance.PropertyAddress = pi.Owner.SiteAddress.FullSiteStreetAddress + " " + pi.Owner.SiteAddress.SiteCitySiteStateSiteZIP;
                                DealMaker.PlainClasses.ShortSale.Adapters.DataManager.Instance.PropertyPlainAddress = pi.Owner.SiteAddress.FullSiteStreetAddress;
                                DealMaker.PlainClasses.ShortSale.Adapters.DataManager.Instance.PropertyCityStateZIP = pi.Owner.SiteAddress.SiteCitySiteStateSiteZIP;
                            }
                            else
                            {
                                DealMaker.PlainClasses.ShortSale.Adapters.DataManager.Instance.PropertyAddress = string.Empty;
                                DealMaker.PlainClasses.ShortSale.Adapters.DataManager.Instance.PropertyPlainAddress = string.Empty;
                                DealMaker.PlainClasses.ShortSale.Adapters.DataManager.Instance.PropertyCityStateZIP = string.Empty;
                            }

                            if (null != pi.Owner)
                            {
                                DealMaker.PlainClasses.ShortSale.Adapters.DataManager.Instance.HomeOwnerFullName = pi.Owner.FullName;
                                DealMaker.PlainClasses.ShortSale.Adapters.DataManager.Instance.PrimaryPhone = pi.Owner.HomePhoneString;
                                DealMaker.PlainClasses.ShortSale.Adapters.DataManager.Instance.SecondaryPhone = pi.Owner.WorkPhoneString;
                                DealMaker.PlainClasses.ShortSale.Adapters.DataManager.Instance.Email = pi.Owner.EMail;
                                DealMaker.PlainClasses.ShortSale.Adapters.DataManager.Instance.DocumentName = pi.Owner.FullName;
                            }
                            else
                            {
                                DealMaker.PlainClasses.ShortSale.Adapters.DataManager.Instance.HomeOwnerFullName = string.Empty;
                                DealMaker.PlainClasses.ShortSale.Adapters.DataManager.Instance.PrimaryPhone = string.Empty;
                                DealMaker.PlainClasses.ShortSale.Adapters.DataManager.Instance.SecondaryPhone = string.Empty;
                                DealMaker.PlainClasses.ShortSale.Adapters.DataManager.Instance.Email = string.Empty;
                                DealMaker.PlainClasses.ShortSale.Adapters.DataManager.Instance.DocumentName = string.Empty;
                            }

                            string ssError = string.Empty;
                            DealMaker.PlainClasses.ShortSale.Adapters.DataManager.Instance.LoadShortSaleDocument("pg", out ssError);
                            _shortSaleControl.DisplayLoadedData();
                            _shortSaleControl.EnableAutoSaveEventHandler(true);
                        }

                    }


                    break;                    
                case 7:	    //Sell
                    break;                
                case 8:     //Imported Info
                    break;
                    
			}			
		}
		#endregion tabControl1_SelectedIndexChanged 
		#region � Save �
		private void SaveData()
		{
			if (null == pi)
			{
				pi = new PropertyItem();
				pi.save = true;
			}
			else
			{
				pi.save = false;
			}

			
			
			if (null == pi.Owner)
			{
				pi.Owner = new Contact();
				pi.Owner.save = true;
			}
			else
			{
				pi.Owner.save = false;
			}

			pi.DateLastModified				= DateTime.Now;

			pi.Owner.FirstName				= this.tbFirstname.Text;
			pi.Owner.MiddleName				= this.tbMiddleName.Text;
			pi.Owner.LastName				= this.tbLastName.Text;			
			string tempFullName				= ApplicationHelper.GetFullName(pi.Owner.FirstName, pi.Owner.MiddleName, pi.Owner.LastName);

			if (tempFullName.Trim() != string.Empty)
			{
				pi.Owner.FullName				=  tempFullName;
			}
			else
			{
				pi.Owner.FullName				= tbFullName.Text;
			}
									
			if (null == pi.Owner.SiteAddress)
			{
				pi.Owner.SiteAddress = new Address();
				pi.Owner.SiteAddress.save = true;
			}
			else
			{
				pi.Owner.SiteAddress.save = false;
			}
			
			pi.Owner.SiteAddress.FullSiteStreetAddress		= this.tbSiteAdd.Text;
			pi.Owner.SiteAddress.SiteCity					= this.tbSiteCity.Text;
			pi.Owner.SiteAddress.SiteState					= this.tbSiteState.Text;
			pi.Owner.SiteAddress.SiteZIP					= this.tbSiteZIP.Text;
			pi.Owner.SiteAddress.SiteCitySiteState			= this.tbSiteCityState.Text;
			pi.Owner.SiteAddress.SiteCitySiteStateSiteZIP	= string.Empty;	//we set it to empty string so that it builds itself from valid data
			
			pi.Owner.SiteAddress.SiteCountry				= this.tbCounty.Text;

			if (null == pi.Owner.MailAddress)
			{
				pi.Owner.MailAddress = new Address();
				pi.Owner.MailAddress.save = true;
			}
			else
			{
				pi.Owner.MailAddress.save = false;
			}
			
			pi.Owner.MailAddress.FullSiteStreetAddress		= this.tbMailLastKnownAdd.Text;
			pi.Owner.MailAddress.SiteCity					= this.tbMailLastKnownCity.Text;
			pi.Owner.MailAddress.SiteState					= this.tbMailLastKnownState.Text;
			pi.Owner.MailAddress.SiteCitySiteState			= this.tbMailLastKnownCityState.Text;
			pi.Owner.MailAddress.SiteZIP   = this.tbMailLastKnownZIP.Text;;
			pi.Owner.MailAddress.SiteCitySiteStateSiteZIP	= string.Empty;	//we set it to empty string so that it builds itself from valid data

			
			if (null == pi.Owner.SecondAddress)
			{
				pi.Owner.SecondAddress = new Address();
				pi.Owner.SecondAddress.save = true;
			}
			else
			{
				pi.Owner.SecondAddress.save = false;
			}
			pi.Owner.SecondAddress.FullSiteStreetAddress	= this.tbMailPreviousAdd.Text;
			pi.Owner.SecondAddress.SiteCity					= this.tbMailPreviousCity.Text;
			pi.Owner.SecondAddress.SiteState				= this.tbMailPreviousState.Text;
			pi.Owner.SecondAddress.SiteCitySiteState		= this.tbMailPreviousCityState.Text;
			pi.Owner.SecondAddress.SiteZIP					= this.tbMailPreviousZIP.Text;
			pi.Owner.SecondAddress.SiteCitySiteStateSiteZIP	= string.Empty;	//we set it to empty string so that it builds itself from valid data


			if (null == pi.Owner.ThirdAddress)
			{
				pi.Owner.ThirdAddress = new Address();
				pi.Owner.ThirdAddress.save = true;
			}
			else
			{
				pi.Owner.ThirdAddress.save = false;
			}
			pi.Owner.ThirdAddress.FullSiteStreetAddress		= this.tbMailOldAdd.Text;
			pi.Owner.ThirdAddress.SiteCity					= this.tbMailOldCity.Text;
			pi.Owner.ThirdAddress.SiteState					= this.tbMailOldState.Text;
			pi.Owner.ThirdAddress.SiteCitySiteState			= this.tbMailOldCityState.Text;
			pi.Owner.ThirdAddress.SiteZIP					= this.tbMailOldZIP.Text;
			pi.Owner.ThirdAddress.SiteCitySiteStateSiteZIP	= string.Empty;	//we set it to empty string so that it builds itself from valid data
			
			if (null == pi.Owner.HomePhone)
			{
				pi.Owner.HomePhone = new Phone();
				pi.Owner.HomePhone.save = true;
			}
			else
			{
				pi.Owner.HomePhone.save = false;
			}
			pi.Owner.HomePhone.PhoneNr						= this.tbHomePh.Text;
			pi.Owner.HomePhone.BestTimeToCall				= this.tbHomeBestTime.Text;


			if (null == pi.Owner.WorkPhone)
			{
				pi.Owner.WorkPhone = new Phone();
				pi.Owner.WorkPhone.save = true;
			}
			else
			{
				pi.Owner.WorkPhone.save = false;
			}
			pi.Owner.WorkPhone.PhoneNr						= this.tbWorkPh.Text;
			pi.Owner.WorkPhone.BestTimeToCall				= this.tbWorkBestTime.Text;


			if (null == pi.Owner.CellPhone)
			{
				pi.Owner.CellPhone = new Phone();
				pi.Owner.CellPhone.save = true;
			}
			else
			{
				pi.Owner.CellPhone.save = false;
			}
			pi.Owner.CellPhone.PhoneNr						= this.tbCellPh.Text;
			pi.Owner.CellPhone.BestTimeToCall				= this.tbCellBestTime.Text;
			
			pi.Owner.FaxNumber								= this.tbFax.Text;
			pi.Owner.EMail									= this.tbEmail.Text;

            pi.Owner.STEmail = _stEmail.Text;
            pi.Owner.STCellphone = _stCellPhone.Text;
            pi.Owner.STLandline = _stLandline.Text;
	
			
			
			if (null == pi.ListingAgent)
			{
				pi.ListingAgent = new ListingAgent();				
				pi.ListingAgent.save = true;				
			}
			else
			{
				pi.ListingAgent.save = false;
			}
			try
			{				
				pi.ListingAgent.ListingAgentFaxNumber = this.tb_II_LI_LA_FaxNumber.Text;
			}
			catch (Exception)
			{
				pi.ListingAgent.ListingAgentFaxNumber = string.Empty;
			}

            try
            {
                double dVal = Convert.ToDouble(this.tb_II_LI_ListingPrice.Text);
                pi.ListingAgent.ListingPrice = dVal;
            }
            catch
            {
                pi.ListingAgent.ListingPrice = 0;
            }

            try
            {
                string sVal = this.tb_II_LI_LA_Email.Text;
                pi.ListingAgent.ListingAgentEMail = sVal;
            }
            catch
            {
                pi.ListingAgent.ListingAgentEMail = string.Empty;
            }

            //BEGIN
            try
            {
                string sVal = this.tb_II_LI_ListingMLSNumber.Text;
                pi.ListingAgent.ListingAgentMLSNumber = sVal;
            }
            catch
            {
                pi.ListingAgent.ListingAgentMLSNumber = string.Empty;
            }

            try
            {
                string sVal = this.tb_II_LI_ListingOffice.Text;
                pi.ListingAgent.ListingAgentOffice = sVal;
            }
            catch
            {
                pi.ListingAgent.ListingAgentOffice = string.Empty;
            }

            try
            {
                string sVal = this.tb_II_LI_LA_FirstName.Text;
                pi.ListingAgent.ListingAgentFirstName = sVal;
            }
            catch
            {
                pi.ListingAgent.ListingAgentFirstName = string.Empty;
            }

            try
            {
                string sVal = this.tb_II_LI_LA_LastName.Text;
                pi.ListingAgent.ListingAgentLastName = sVal;
            }
            catch
            {
                pi.ListingAgent.ListingAgentLastName = string.Empty;
            }

            try
            {
                string sVal = this.tb_II_LI_LA_AgentCode.Text;
                pi.ListingAgent.ListingAgentCode = sVal;
            }
            catch
            {
                pi.ListingAgent.ListingAgentCode = string.Empty;
            }

            try
            {
                string sVal = this.tb_II_LI_LA_PhoneNumber.Text;
                pi.ListingAgent.ListingAgentPhoneNumber = sVal;
            }
            catch
            {
                pi.ListingAgent.ListingAgentPhoneNumber = string.Empty;
            }
            //END

            if (null == pi.DealInfo)
			{
				pi.DealInfo = new DealInfo();
				pi.DealInfo.save = true;
				if (null == pi.DealInfo.DistrictProperty)
				{
					pi.DealInfo.DistrictProperty = new DistrictProperties();
					pi.DealInfo.DistrictProperty.save = true;
				}
				else
				{
					pi.DealInfo.DistrictProperty.save = false;
				}				
			}
			else
			{
				pi.DealInfo.save = false;
				if (null == pi.DealInfo.DistrictProperty)
				{
					pi.DealInfo.DistrictProperty = new DistrictProperties();
					pi.DealInfo.DistrictProperty.save = true;
				}
				else
				{
					pi.DealInfo.DistrictProperty.save = false;
				}		
			}
			pi.DealInfo.DistrictProperty.Subdivision = this.tbSubDivision.Text;
			
						
			if (null == pi.DealProperty)
			{
				pi.DealProperty = new DealProperty();
				pi.DealProperty.save = true;
			}
			else
			{
				pi.DealProperty.save = false;
			}
			
			#region DealInfo 
			//1st Row
			pi.DealProperty.Type				= (string)this.cbType.SelectedItem;
			pi.DealProperty.Bedrooms			= (string)this.cbBedrooms.SelectedItem;
			pi.DealProperty.Baths			= (string)this.cbBaths.SelectedItem;
			pi.DealProperty.Sqft				= this.tbSqFt.Text;
			pi.DealProperty.YearBuilt		= (string)this.cbYearBuilt.SelectedItem;
			pi.DealProperty.Garage			= (string)this.cbGarage.SelectedItem;
			pi.DealProperty.Pool				= (string)this.cbPool.SelectedItem;
			pi.DealProperty.OccupiedBy		= (string)this.cbOccupiedBy.SelectedItem;
			pi.DealProperty.Apn				= this.tbAPN.Text;

			//2nd Row
			pi.DealProperty.FeatureCondition = this.tbCondition.Text;
			pi.DealProperty.WhySelling		= this.tbWhySelling.Text;
			pi.DealProperty.WorthBySeller	= this.tbWorthBySeller.Text;
			pi.DealProperty.Source			= (string)this.cbSource.SelectedItem;

			//3rd Row
			pi.DealProperty.FirstBal			= this.tb1Bal.Text;
			pi.DealProperty.Ir1				= this.tb1IntRate.Text;
			pi.DealProperty.Type1			= (string)this.cb1Type.SelectedItem;
			pi.DealProperty.MoPay1			= this.tb1MoPayment.Text;
			pi.DealProperty.TaxIns			= (string)this.cbTaxIncl.SelectedItem;
			pi.DealProperty.TaxAmnt			= this.tbTaxAmount.Text;

			//4th row
			pi.DealProperty.SecondBal		= this.tb2Bal.Text;
			pi.DealProperty.Ir2				= this.tb2IntRate.Text;
			pi.DealProperty.Type2			= (string)this.cb2Type.SelectedItem;
			pi.DealProperty.MoPay2			= this.tb2MoPayment.Text;
			pi.DealProperty.Hoa				= this.tbHOA.Text;

			//5th row
			pi.DealProperty.ThirdBal			= this.tb3Bal.Text;
			pi.DealProperty.Ir3				= this.tb3IntRate.Text;
			pi.DealProperty.Type3			= (string)this.cb3Type.SelectedItem;
			pi.DealProperty.MoPay3			= this.tb3MoPayment.Text;
			pi.DealProperty.OtherLiens		= this.tbOtherLiens.Text;
			
			//6th row
			pi.DealProperty.PmntsCurrent		= (string)this.cbPmntsCurrnet.SelectedItem;
			pi.DealProperty.MoBehind			= (string)this.cbMoBehind.SelectedItem;
			pi.DealProperty.ReInsAmnt		= this.tbReinsAmnt.Text;
			pi.DealProperty.InForecl			= (string)this.cbInForecl.SelectedItem;
			//pi.DealProperty.ForSaleDate		= this.dtpForSaleDate.Value;
			try 
            { 
                if (string.Empty != this.tbForSaleDate.Text)
                    pi.DealProperty.ForSaleDate = Convert.ToDateTime(this.tbForSaleDate.Text);
                else
                    pi.DealProperty.ForSaleDate = DateTime.MinValue;
            } 
			catch {pi.DealProperty.ForSaleDate = DateTime.MinValue;}

			pi.DealProperty.Listed			= (string)this.cbListed.SelectedItem;
			//pi.DealProperty.ListExpDate		= this.dtpListingExpDate.Value;
			try 
            { 
                if (string.Empty != this.tbListingExpDate.Text)
                    pi.DealProperty.ListExpDate		= Convert.ToDateTime(this.tbListingExpDate.Text); 
                else
                    pi.DealProperty.ListExpDate = DateTime.MinValue;
            }
			catch {pi.DealProperty.ListExpDate = DateTime.MinValue;}
			pi.DealProperty.CanCancelList	= (string)this.cbCanCancel.SelectedItem;

			//7th row
			pi.DealProperty.LowestPriceAcceptable	= this.tbLowestPriceAcceptable.Text;
			pi.DealProperty.Sell4WhatOwes	= (string)this.cbSell4What.SelectedItem;
			pi.DealProperty.MinCashNeededWhy	= this.tbMinCashNeeded.Text;
			pi.DealProperty.DesiredClosingDate		= this.tbDesiredClosingDate.Text;

			//8th row
			pi.DealProperty.Motivation		= (string)this.cbMotivation.SelectedItem;
			pi.DealProperty.LeadSource		= (string)this.cbLeadSource.SelectedItem;
			pi.DealProperty.Arv				= this.tbARV.Text;
			pi.DealProperty.Repairs			= this.tbRepairs.Text;

            if (ContactTypes.Buyer != _contactType && ContactTypes.Lender != _contactType)
                pi.DealProperty.NextStep = this.tbNextStep.Text; //seller only
            else
                pi.DealProperty.NextStep = _ci_NotesField.Text;
			#endregion DealInfo 
		
            
		
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                this.SavePropertyInfo(pi);
                
                this.ucppclv.Save(this.pi.IdPropertyItem);
                this.ucppc.Save(this.pi.IdPropertyItem);
                
                //commercials
                if (null != Globals.activeModules && Globals.activeModules.MOD_9)
                {
                    DataManager.Instance.SaveData(this.pi.IdPropertyItem);                    
                }

                //BuyersInfo
                BuyerInfoManager.Instance.SaveData(this.pi.IdPropertyItem);
                
                //PictureManager
                PictureManager.Instance.SaveData(this.pi.IdPropertyItem, _pbHouse.Image, _pbHouse.BackColor, _pbHouse.Width, _pbHouse.Height);
                
                //ShortSale
                if (null != Globals.activeModules && Globals.activeModules.MOD_8)
                {
                    DealMaker.PlainClasses.ShortSale.Adapters.DataManager.Instance.PropertyItemID = this.pi.IdPropertyItem;                    
                }

                //REPAIRS
                this._repairs.SaveData(this.pi.IdPropertyItem);
                
            }
#if DEBUG
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message + System.Environment.NewLine + exc.InnerException + System.Environment.NewLine + exc.StackTrace);
            }
#endif
			finally
			{
				Globals.CurrentPropertyItem = pi;
				Cursor.Current = Cursors.Arrow;
			}
		}
		private void bSave_Click(object sender, System.EventArgs e)
		{	
			SaveDataEx();			
		}

		void SaveDataEx()
		{
			//if (!Globals.ExpandedFields)			
			//	ApplicationHelper.Database_AlterTable_ExpandTables();
			
			//++ assign to a node
			if (null == this.pi)			
				this.bAssignToGroup_Click(null, null);			
			else
			{
				Node statusNode = null;
				ArrayList alGroupMembership = this.GetGroupMembershipData(pi.IdPropertyItem, out statusNode);

				if (0 == alGroupMembership.Count && null == statusNode)				
					this.bAssignToGroup_Click(null, null);	
			}
			//-- assign to a node

			this.SaveData();

			//TA++ Scheduler bug
			//Pass args to Activities - Task and History List			
			//this.taskAndHistoryList.Group = 
			this.taskAndHistoryList.User = this.pi;
			//TA--
		}
		#endregion � Save �
		#region SavePropertyInfo 
		private void SavePropertyInfo(PropertyItem pi)
		{
			ISession session = null;
			ITransaction transaction = null;
			
			try
			{
			
				Connection conn = Connection.getInstance();     
				SqlConnection sqlConn = conn.HatchConn;
				IDbConnection iSqlConn = sqlConn as IDbConnection;
				
				Configuration cfg = Globals.cfg;
				ISessionFactory factory = Globals.factory;
				
				
				if (null != iSqlConn) 
				{
					session = factory.OpenSession(iSqlConn);
					if (ConnectionState.Closed == iSqlConn.State) 
					{
						iSqlConn.Open();
					}
				}
				else 
				{
					session = factory.OpenSession();
				}

				transaction = session.BeginTransaction();
			
				//PropertyItem pi1 = (PropertyItem)session.Load(typeof(PropertyItem), pi.IdPropertyItem);
				//pi1 = pi;
				//session.SaveOrUpdate(pi);				
				pi.NhSaveOrUpdate(session);
				

				transaction.Commit();				

				//sasa: 24.3.06, lock after save added bugfix
				Network.NetworkLock.RequestLock(pi.IdPropertyItem);

				//sasa, 13.11.05: nw broadcast udp packet changed pi
                try { Network.NetworkUDP.Broadcast(pi); }
                catch { }
			}
			catch (Exception exc)
			{
				MessageBox.Show(exc.ToString());
				transaction.Rollback();
			}				
			finally
			{				
				session.Close();
			}			
		}
		#endregion SavePropertyInfo 

		
		private void UpdateTotalOwed_and_Monthly_EH(object sender, System.EventArgs e)
		{
			decimal dTotal = 0;
			decimal dTemp = 0;
			
			try
			{
				dTemp = Convert.ToDecimal(this.tb1Bal.Text);
			}
			catch (Exception)
			{
				dTemp = 0;
			}
			finally
			{
				dTotal += dTemp;
			}

			try
			{
				dTemp = Convert.ToDecimal(this.tb2Bal.Text);
			}
			catch (Exception)
			{
				dTemp = 0;
			}
			finally
			{
				dTotal += dTemp;
			}

			try
			{
				dTemp = Convert.ToDecimal(this.tb3Bal.Text);
			}
			catch (Exception)
			{
				dTemp = 0;
			}
			finally
			{
				dTotal += dTemp;
			}

			try
			{
				dTemp = Convert.ToDecimal(this.tbOtherLiens.Text);
			}
			catch (Exception)
			{
				dTemp = 0;
			}
			finally
			{
				dTotal += dTemp;
			}

			//update total owed && total monthly
			this.tbTotalOwed.Text = dTotal.ToString("c");
			
			dTotal = 0;
			try
			{
				dTemp = Convert.ToDecimal(this.tb1MoPayment.Text);
			}
			catch (Exception)
			{
				dTemp = 0;
			}
			finally
			{
				dTotal += dTemp;
			}

			try
			{
				dTemp = Convert.ToDecimal(this.tb2MoPayment.Text);
			}
			catch (Exception)
			{
				dTemp = 0;
			}
			finally
			{
				dTotal += dTemp;
			}

			try
			{
				dTemp = Convert.ToDecimal(this.tb3MoPayment.Text);
			}
			catch (Exception)
			{
				dTemp = 0;
			}
			finally
			{
				dTotal += dTemp;
			}

			try
			{
				dTemp = Convert.ToDecimal(this.tbTaxAmount.Text);
			}
			catch (Exception)
			{
				dTemp = 0;
			}
			finally
			{
				dTotal += dTemp;
			}

			try
			{
				dTemp = Convert.ToDecimal(this.tbHOA.Text);
			}
			catch (Exception)
			{
				dTemp = 0;
			}
			finally
			{
				dTotal += dTemp;
			}

			this.tbTotalMonthly.Text = dTotal.ToString("c");			
		}

		private void UpdateFullName_EH (object sender, System.EventArgs e)
		{
			this.tbFullName.Text = string.Empty;
			if (string.Empty != this.tbFirstname.Text)
				this.tbFullName.Text += this.tbFirstname.Text;

			if (string.Empty != this.tbMiddleName.Text)
			{
				if (string.Empty != this.tbFullName.Text)
					this.tbFullName.Text += " ";
				this.tbFullName.Text +=  this.tbMiddleName.Text;
			}

			if (string.Empty != this.tbLastName.Text)
			{
				if (string.Empty != this.tbFullName.Text)
					this.tbFullName.Text += " ";
				this.tbFullName.Text += this.tbLastName.Text; 
			}						
		}
		
		private void updateLastKnownSS_EH(object sender, EventArgs e)
		{
			string newValue = string.Empty;
			
			if (string.Empty != this.tbMailLastKnownCity.Text)
				newValue += this.tbMailLastKnownCity.Text;
			
			if (string.Empty != this.tbMailLastKnownState.Text)
			{
				if (string.Empty != newValue)
					newValue += ", ";
				newValue += this.tbMailLastKnownState.Text;
			}
				
			this.tbMailLastKnownCityState.Text = newValue;
		}

		private void updateOldSS_EH(object sender, EventArgs e)
		{
			string newValue = string.Empty;
			
			if (string.Empty != this.tbMailOldCity.Text)
				newValue += this.tbMailOldCity.Text;
			
			if (string.Empty != this.tbMailOldState.Text)
			{
				if (string.Empty != newValue)
					newValue += ", ";
				newValue += this.tbMailOldState.Text;
			}
				
			this.tbMailOldCityState.Text = newValue;
		}

		private void updatePreviousSS_EH(object sender, EventArgs e)
		{
			string newValue = string.Empty;
			
			if (string.Empty != this.tbMailPreviousCity.Text)
				newValue += this.tbMailPreviousCity.Text;
			
			if (string.Empty != this.tbMailPreviousState.Text)
			{
				if (string.Empty != newValue)
					newValue += ", ";
				newValue += this.tbMailPreviousState.Text;
			}
				
			this.tbMailPreviousCityState.Text = newValue;
		}

		private void updateSiteSS_EH(object sender, EventArgs e)
		{
			string newValue = string.Empty;
			
			if (string.Empty != this.tbSiteCity.Text)
				newValue += this.tbSiteCity.Text;
			
			if (string.Empty != this.tbSiteState.Text)
			{
				if (string.Empty != newValue)
					newValue += ", ";
				newValue += this.tbSiteState.Text;
			}
				
			this.tbSiteCityState.Text = newValue;
		}		

		private void bPrintDealInfo_Click(object sender, EventArgs e)
		{
			DealInfoPrint.DealInfoPrint.ShowPrintForm(pi);	
		}

		private void GroupMembership_MouseEnter(object sender, EventArgs e)
		{
			this.groupMembershipFloater = new DealMaker.Forms.Floaters.GroupMembershipTab();


			Point pReadMe = this.readMe2.Location;
			Size fSize = this.groupMembershipFloater.Size;
									
			this.groupMembershipFloater.Location = PointToScreen(new Point (pReadMe.X - fSize.Width, pReadMe.Y) );
			this.groupMembershipFloater.Show();

		}

		private void GroupMembership_MouseLeave(object sender, EventArgs e)
		{
			this.groupMembershipFloater.Hide();
        }                

		private void button1_Click(object sender, System.EventArgs e)
		{
			SetReadOnly();
		}
		
		private void button2_Click(object sender, System.EventArgs e)
		{
			SetReadWrite();
		}

		private void NewContactInGroup_EH(int nodeId)
		{
			try
			{
				if (nodeId != Globals.selectedNodeId)
					return; 						

				Cursor.Current = Cursors.WaitCursor;	
				
				this.htNodePosPropertyItemId = ApplicationHelper.GetPropertyItemsInNode(nodeId);

				if (null == this.htNodePosPropertyItemId || this.htNodePosPropertyItemId.Count == 0)
				{
					return;
				}

				if (this.htNodePosPropertyItemId.Contains(this.currentOffset))
				{
					Guid currentlySelectedPropertyItem = (Guid)this.htNodePosPropertyItemId[this.currentOffset];

					this.htNodePosPropertyItemId = ApplicationHelper.GetPropertyItemsInNode(Globals.selectedNodeId);		

					this.LoadDisplayData(currentlySelectedPropertyItem);
					this.DisplayGroupMembershipData(currentlySelectedPropertyItem);				
				}
			}
			finally
			{
				Cursor.Current = Cursors.Arrow;
			}
		}
				
		private void cbListed_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (null == cbListed.SelectedItem)
			{
				this.tbListingExpDate.ReadOnly = false;
				this.tbListingExpDate.Enabled = true;
				return;
			}
			if ("Yes" == cbListed.SelectedItem.ToString())
			{
				this.tbListingExpDate.ReadOnly = false;
				this.tbListingExpDate.Enabled = true;
			}
			else if ("No" == cbListed.SelectedItem.ToString())
			{
				this.tbListingExpDate.ReadOnly = true;
				this.tbListingExpDate.Text = string.Empty;
			}
			else
			{
				this.tbListingExpDate.ReadOnly = false;
			}
		}

		private void cbInForecl_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (null == cbInForecl.SelectedItem)
			{	
				this.tbForSaleDate.ReadOnly = false;
				this.tbForSaleDate.Enabled = true;
				return;
			}	
			if ("Yes" == cbInForecl.SelectedItem.ToString())
			{
				this.tbForSaleDate.ReadOnly = false;
				this.tbForSaleDate.Enabled = true;
			}
			else if ("No" == cbInForecl.SelectedItem.ToString())
			{
				this.tbForSaleDate.ReadOnly = true;
				this.tbForSaleDate.Text = string.Empty;
			}
			else
			{
				this.tbForSaleDate.ReadOnly = false;
			}
		}

		private void tbForSaleDate_KeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
		{
			if (false == this.tbForSaleDate.ReadOnly)
			{
				this.ForSaleDateHandler();
			}
		}

		private void tbForSaleDate_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			if (false == this.tbForSaleDate.ReadOnly)
			{
				this.ForSaleDateHandler();
			}
		}

		private void ForSaleDateHandler()
		{
			DateTimePickerForm dptf = new DateTimePickerForm();
			//dptf.Location = tbForSaleDate.Location;
			dptf.ShowDialog();

			if (true == dptf.Valid)
			{
				this.tbForSaleDate.Text = dptf.SelectedDate.ToShortDateString();
			}
			else
			{
				if (true == dptf.ClearDate)
				{
					this.tbForSaleDate.Text = string.Empty;
				}
			}
		}

		private void tbListingExpDate_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			if (false == this.tbListingExpDate.ReadOnly)
			{
				this.ListExpDateHandler();
			}
		}

		private void tbListingExpDate_KeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
		{	
			if (false == this.tbListingExpDate.ReadOnly)
			{
				this.ListExpDateHandler();
			}
		}

		private void ListExpDateHandler()
		{
			DateTimePickerForm dptf = new DateTimePickerForm();
			//dptf.Location = tbListingExpDate.Location;
			dptf.ShowDialog();

			if (true == dptf.Valid)
			{
				this.tbListingExpDate.Text = dptf.SelectedDate.ToShortDateString();
			}
			else
			{
				if (true == dptf.ClearDate)
				{
					this.tbListingExpDate.Text = string.Empty;
				}
			}
		}

		private void ShowMap(object sender, System.EventArgs e)
		{			
			if (null == this.pi)
			{
				MessageBox.Show("Select or save contact first!", "Contact missing", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;			
			}

			//AUTOSAVE
			AutoSaveData();

			if (null == this.pi.Owner)
			{
				MessageBox.Show("Select or save contact first!", "Contact missing", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;			
			}

			if (null == this.pi.Owner.SiteAddress)
			{
				MessageBox.Show("Select or save contact first!", "Site Address missing", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;			
			}

			if (string.Empty == this.pi.Owner.SiteAddress.FullSiteStreetAddress.Trim())
			{
				MessageBox.Show("Please enter Site Address first!", "Site Street Address is missing", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;			
			}

			if (string.Empty == this.pi.Owner.SiteAddress.SiteState.Trim())
			{
				MessageBox.Show("Please enter Site State!", "Site State is missing", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;			
			}

			if (string.Empty == this.pi.Owner.SiteAddress.SiteCity.Trim())
			{
				MessageBox.Show("Please enter Site City!", "Site City is missing", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;			
			}

			if (string.Empty == this.pi.Owner.SiteAddress.SiteZIP.Trim())
			{
				MessageBox.Show("Please enter Site ZIP!", "Site ZIP is missing", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;			
			}

			//Process.Start("iexplore.exe", "http://maps.yahoo.com/maps_result?addr=" + this.pi.Owner.SiteAddress.FullSiteStreetAddress 
			//				+ "&csz=" + this.pi.Owner.SiteAddress.SiteCity + "+" + this.pi.Owner.SiteAddress.SiteState + "+" + this.pi.Owner.SiteAddress.SiteZIP + "&country=us;");
					//"http://maps.yahoo.com/maps_result?addr=11287 W. Magnolia St.&csz=Avondale,+AZ+85323&country=us;");

            //Process p = new Process();
            //p.StartInfo.FileName = DefaultBrowserSupport.getDefaultBrowser();
            //OLD
            //p.StartInfo.Arguments = "http://maps.yahoo.com/maps_result?addr=" + this.pi.Owner.SiteAddress.FullSiteStreetAddress + "&csz=" + this.pi.Owner.SiteAddress.SiteCity + "+" + this.pi.Owner.SiteAddress.SiteState + "+" + this.pi.Owner.SiteAddress.SiteZIP + "&country=us;";

            //NEW
            //http://maps.yahoo.com/#q1=11287 W.Magnolia St. Avondale, AZ 85323 us
            //string args = this.pi.Owner.SiteAddress.FullSiteStreetAddress + " " + this.pi.Owner.SiteAddress.SiteCity + " " + this.pi.Owner.SiteAddress.SiteState + " " + this.pi.Owner.SiteAddress.SiteZIP;// +"&country=us;";
            //p.StartInfo.Arguments = "http://maps.yahoo.com/#q1=" + System.Web.HttpUtility.UrlEncode(args);            

            //p.Start();
            
            string arg = $"{pi.Owner.SiteAddress.FullSiteStreetAddress}, {pi.Owner.SiteAddress.SiteCity}, {pi.Owner.SiteAddress.SiteState} {pi.Owner.SiteAddress.SiteZIP}";
            var escapedArg = Uri.EscapeUriString(arg);
            //p.StartInfo.Arguments = $"https://www.google.com/maps/search/?api=1&query={escapedArg}";
            //p.Start();

            Process.Start($"https://www.google.com/maps/search/?api=1&query={escapedArg}");
        }

        private void On_CopyDataToDealInfo_Click(object sender, EventArgs e)
        {
            /* MAPPINGS
            //DEAL INFO TAB			IMPORTED INFO TAB			
            //1st TAB PROPERTY CHARACTERISTICS			
			
            //*type	cbType		property type			tb_II_PC_PropertyType
            //*bedrooms	cbBedrooms		bedrooms			tb_II_PC_Bedrooms
            //*baths	cbBaths		baths			tb_II_PC_Bathrooms
            //*SqFt:	tbSqFt		Home Sq. Ft.			tb_II_PC_HomeSqFt
            //*garage	cbGarage		garage			tb_II_PC_Garage
            //*pool	cbPool		pool			tb_II_PC_Pool
            //*year built	cbYearBuilt		year built			tb_II_PC_YearBuilt

            //FORECLOSURE INFO TAB			
            //*apn	tbAPN		apn			tb_II_FI_APN

            //FINANCIALS TAB			
            //*1st Bal	tb1Bal		first loan amount			tb_II_F_LoanAmount
            //*type	cb1Type		loan type			tb_II_F_LoanType
            //*I.R.	tb1IntRate		interest rate			tb_II_F_LoanInterestRate
            //2nd Bal	tb2Bal		second loan amount			tb_II_F_LoanAmount2
            */

            RemoveAutoSaveEventHandlers();

            ArrayList problemFields = new ArrayList();
            bool overWriteExistingValues = false;

            if (DialogResult.Yes == MessageBox.Show("Should ProfitGrabber overwrite exising Deal Info Data with Imported Info Data?", "Overwrite existing Deal Info data?", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2))
                overWriteExistingValues = true;


            if (string.Empty != tb_II_PC_PropertyType.Text)
                if (overWriteExistingValues || (!overWriteExistingValues && TestComboboxForOverwrite(this.cbType)))
                    if (this.cbType.Items.Contains(tb_II_PC_PropertyType.Text))
                        this.cbType.SelectedItem = tb_II_PC_PropertyType.Text;
                    else
                        problemFields.Add("Property Type");


            if (string.Empty != tb_II_PC_Bedrooms.Text)
                if (overWriteExistingValues || (!overWriteExistingValues && TestComboboxForOverwrite(this.cbBedrooms)))
                    if (this.cbBedrooms.Items.Contains(tb_II_PC_Bedrooms.Text))
                        this.cbBedrooms.SelectedItem = tb_II_PC_Bedrooms.Text;
                    else
                        problemFields.Add("Bedrooms");



            if (string.Empty != tb_II_PC_Bathrooms.Text)
                if (overWriteExistingValues || (!overWriteExistingValues && TestComboboxForOverwrite(this.cbBaths)))
                    if (this.cbBaths.Items.Contains(tb_II_PC_Bathrooms.Text))
                        this.cbBaths.SelectedItem = tb_II_PC_Bathrooms.Text;
                    else
                        problemFields.Add("Bathrooms");


            if (string.Empty != tb_II_PC_HomeSqFt.Text)
                if (overWriteExistingValues || (!overWriteExistingValues && string.Empty == this.tbSqFt.Text))
                    this.tbSqFt.Text = tb_II_PC_HomeSqFt.Text;


            if (string.Empty != tb_II_PC_Garage.Text)
                if (overWriteExistingValues || (!overWriteExistingValues && TestComboboxForOverwrite(this.cbGarage)))
                    if (this.cbGarage.Items.Contains(tb_II_PC_Garage.Text))
                        this.cbGarage.SelectedItem = tb_II_PC_Garage.Text;
                    else
                        problemFields.Add("Garage");


            if (string.Empty != tb_II_PC_Pool.Text)
                if (overWriteExistingValues || (!overWriteExistingValues && TestComboboxForOverwrite(this.cbPool)))
                    if (this.cbPool.Items.Contains(tb_II_PC_Pool.Text))
                        this.cbPool.SelectedItem = tb_II_PC_Pool.Text;
                    else
                        problemFields.Add("Pool");


            if (string.Empty != tb_II_PC_YearBuilt.Text)
                if (overWriteExistingValues || (!overWriteExistingValues && TestComboboxForOverwrite(this.cbYearBuilt)))
                    if (this.cbYearBuilt.Items.Contains(tb_II_PC_YearBuilt.Text))
                        this.cbYearBuilt.SelectedItem = tb_II_PC_YearBuilt.Text;
                    else
                        problemFields.Add("Year built");


            if (string.Empty != tb_II_FI_APN.Text)
                if (overWriteExistingValues || (!overWriteExistingValues && string.Empty == this.tbAPN.Text))
                    this.tbAPN.Text = tb_II_FI_APN.Text;


            if (string.Empty != tb_II_F_LoanAmount.Text)
                if (overWriteExistingValues || (!overWriteExistingValues && string.Empty == this.tb1Bal.Text))
                    this.tb1Bal.Text = tb_II_F_LoanAmount.Text;

            if (string.Empty != tb_II_F_LoanType.Text)
                if (overWriteExistingValues || (!overWriteExistingValues && TestComboboxForOverwrite(this.cb1Type)))
                    if (this.cb1Type.Items.Contains(tb_II_F_LoanType.Text))
                        this.cb1Type.SelectedItem = tb_II_F_LoanType.Text;
                    else
                        problemFields.Add("1st Loan Type");

            if (string.Empty != tb_II_F_LoanInterestRate.Text)
                if (overWriteExistingValues || (!overWriteExistingValues && string.Empty == this.tb1IntRate.Text))
                    this.tb1IntRate.Text = tb_II_F_LoanInterestRate.Text;

            if (string.Empty != tb_II_F_LoanAmount2.Text)
                if (overWriteExistingValues || (!overWriteExistingValues && string.Empty == this.tb2Bal.Text))
                    this.tb2Bal.Text = tb_II_F_LoanAmount2.Text;

            AddAutoSaveEventHandlers();
            this.AutoSaveData();

            if (0 < problemFields.Count)
            {
                string outMsg = string.Empty;
                outMsg += "Some fields could't be automatically copied." + System.Environment.NewLine;
                outMsg += "Pay attention at: ";

                for (int i = 0; i < problemFields.Count; i++)
                {
                    outMsg += problemFields[i];
                    if (i < problemFields.Count - 1)
                        outMsg += ", ";
                }

                if (problemFields.Count == 1)
                    outMsg += " field.";
                else
                    outMsg += " fields.";

                MessageBox.Show(outMsg, "ProfitGrabber", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        #endregion � Methods �

        delegate void MainForm_REFRESH_LOCKED_FEATURES_Delegate();
        private void MainForm_REFRESH_LOCKED_FEATURES()
        {
            //Lock & Unlock Modules (UserControl PropertyProfile, UserControl COMPS)

            if (InvokeRequired)
                BeginInvoke(new MainForm_REFRESH_LOCKED_FEATURES_Delegate(MainForm_REFRESH_LOCKED_FEATURES));
            else
            {
                if (null != Globals.activeModules)
                {
                    //SSX INTEGRATION)
                    if (null != this.ucppc)
                        this.ucppc.Enabled = Globals.activeModules.MOD_1;

                    if (null != this.ucppclv)
                        this.ucppclv.Enabled = Globals.activeModules.MOD_2;
                }
            }
        }

        #region FLOATERS
        private void Contact_MouseEnter(object sender, EventArgs e)
        {
            //contactFloater
            this.contactFloater = new DealMaker.Forms.Floaters.NewContact_ContactView();


            Point pReadMe = this.readMe3.Location;
            Size fSize = this.contactFloater.Size;

            this.contactFloater.Location = PointToScreen(new Point(pReadMe.X - fSize.Width, pReadMe.Y));
            this.contactFloater.Show();
        }

        private void Contact_MouseLeave(object sender, EventArgs e)
        {
            this.contactFloater.Hide();
        }

        private void DealInfo_MouseEnter(object sender, EventArgs e)
        {
            //dealInfoFloater
            this.dealInfoFloater = new DealMaker.Forms.Floaters.DealInfo();


            Point pReadMe = this.readMe1.Location;
            Size fSize = this.dealInfoFloater.Size;

            this.dealInfoFloater.Location = PointToScreen(new Point(pReadMe.X - fSize.Width, pReadMe.Y));
            this.dealInfoFloater.Show();
        }

        private void DealInfo_MouseLeave(object sender, EventArgs e)
        {
            this.dealInfoFloater.Hide();
        }

        private void ImportedData_MouseEnter(object sender, EventArgs e)
        {
            this.importedFloater = new DealMaker.Forms.Floaters.ImportedData();

            Point pReadMe = this.readMe6.Location;
            Size fSize = this.importedFloater.Size;

            this.importedFloater.Location = PointToScreen(new Point(pReadMe.X - fSize.Width, pReadMe.Y));

            this.importedFloater.Show();
        }

        private void ImportedData_MouseLeave(object sender, EventArgs e)
        {
            this.importedFloater.Hide();
        }
        #endregion        

        #region IAutoSave Members

        public void TriggerTextBoxLeave()
        {
            OnAutoSave_tb_Leave(null, null);
        }

        public void TriggerTextBoxChanged()
        {
            OnAutoSave_tb_TextChanged(null, null);
        }

        public void TriggerSelectedIndexChanged()
        {
            OnAutoSave_cb_SelectedIndexChanged(null, null);
        }

        #endregion

        #region AUTOSAVE
        //AUTOSAVE - BEGIN
		bool _enableAutoSaveTrigger = true;
		bool _contactDataAltered = false;
		void AddAutoSaveEventHandlers()
		{
			ArrayList objectsFound = new ArrayList();
			FindAutoSaveObjects(this.Controls, ref objectsFound);
			
			foreach (object o in objectsFound)
			{
				if (o is TextBox)
				{
					((TextBox)o).Leave += new EventHandler(OnAutoSave_tb_Leave);
					
					if (!((TextBox)o).ReadOnly)
						((TextBox)o).TextChanged += new EventHandler(OnAutoSave_tb_TextChanged);
				}
					
				if (o is ComboBox)
					((ComboBox)o).SelectedIndexChanged += new EventHandler(OnAutoSave_cb_SelectedIndexChanged);					
			}
			
            //SSX INTEGRATION
            this.ucppc.PP_Downloaded_Successfully += new DealMaker.OnlineData.UserControlPropertyProfileComps.Success_PP_Download(On_PP_Downloaded_Successfully); //+= new DealMaker.UserControlPropertyProfileComps.Success_PP_Download(On_PP_Downloaded_Successfully);
            this.ucppc.AllowAutoSaveTriggerEvents = true;

            this.ucppclv.COMPS_Downloaded_Successfully += new DealMaker.OnlineData.UserControlPropertyProfileCompsListView.Success_COMPS_Download(On_COMPS_Downloaded_Successfully); //+= new DealMaker.UserControlPropertyProfileCompsListView.Success_COMPS_Download(On_COMPS_Downloaded_Successfully);
            this.ucppclv.AllowAutoSaveTriggerEvents = true;
            
            _shortSaleControl.SSXAutoSaveEvent += new BaseShortSaleUserControl.SSXAutoSave(On_SSXAutoSaveEvent);
            _repairs.RepairObjectAutoSaveEvent += new DealMaker.UserControls.Repairs.EstimateRepairsControl.RepairObjectAutoSave(On_RepairAutoSaveEvent);
            _buyerInfoOverview.BuyerObjectAutoSaveEvent += new BuyerInfoOverView.BuyerObjectAutoSave(On_BuyerObjectAutoSaveEvent);
                        
			this.AutoSaveStatus += new AutoSaveEventHandler(On_ContactScreen_AutoSaveStatus_Changed);
		}                
                
		void RemoveAutoSaveEventHandlers()
		{
			ArrayList objectsFound = new ArrayList();
			FindAutoSaveObjects(this.Controls, ref objectsFound);
			
			foreach (object o in objectsFound)
			{
				if (o is TextBox)
				{
					((TextBox)o).Leave -= new EventHandler(OnAutoSave_tb_Leave);
					
					if (!((TextBox)o).ReadOnly)
						((TextBox)o).TextChanged -= new EventHandler(OnAutoSave_tb_TextChanged);
				}
					
				if (o is ComboBox)
					((ComboBox)o).SelectedIndexChanged -= new EventHandler(OnAutoSave_cb_SelectedIndexChanged);					
			}
			
            //SSX INTEGRATION
            this.ucppc.PP_Downloaded_Successfully -= new DealMaker.OnlineData.UserControlPropertyProfileComps.Success_PP_Download(On_PP_Downloaded_Successfully); //-= new DealMaker.UserControlPropertyProfileComps.Success_PP_Download(On_PP_Downloaded_Successfully);
            this.ucppc.AllowAutoSaveTriggerEvents = true;

            this.ucppclv.COMPS_Downloaded_Successfully -= new DealMaker.OnlineData.UserControlPropertyProfileCompsListView.Success_COMPS_Download(On_COMPS_Downloaded_Successfully); //-= new DealMaker.UserControlPropertyProfileCompsListView.Success_COMPS_Download(On_COMPS_Downloaded_Successfully);
            this.ucppclv.AllowAutoSaveTriggerEvents = true;

            _shortSaleControl.SSXAutoSaveEvent -= new BaseShortSaleUserControl.SSXAutoSave(On_SSXAutoSaveEvent);
            _repairs.RepairObjectAutoSaveEvent -= new DealMaker.UserControls.Repairs.EstimateRepairsControl.RepairObjectAutoSave(On_RepairAutoSaveEvent);
            _buyerInfoOverview.BuyerObjectAutoSaveEvent -= new BuyerInfoOverView.BuyerObjectAutoSave(On_BuyerObjectAutoSaveEvent);

			this.AutoSaveStatus -= new AutoSaveEventHandler(On_ContactScreen_AutoSaveStatus_Changed);
		}

        void On_SSXAutoSaveEvent()
        {
            AutoSaveData();
        }

        void On_RepairAutoSaveEvent()
        {
            AutoSaveData();
        }

        void On_BuyerObjectAutoSaveEvent()
        {
            AutoSaveData();
        }


		void FindAutoSaveObjects(System.Windows.Forms.Control.ControlCollection ctrls, ref ArrayList triggerObjects)
		{
			foreach (object o in ctrls)
			{
				if (o is Panel)
					FindAutoSaveObjects(((Panel)o).Controls, ref triggerObjects);

				if (o is TabControl)
					FindAutoSaveObjects(((TabControl)o).Controls, ref triggerObjects);

				if (o is TextBox)					
					triggerObjects.Add(o);

				if (o is ComboBox)					
					triggerObjects.Add(o);

				if (o is CommercialControl)
					FindAutoSaveObjects(((CommercialControl)o).Controls, ref triggerObjects);
			}
		}

		private void OnAutoSave_tb_TextChanged(object sender, EventArgs e)
		{			
			if (_enableAutoSaveTrigger)
			{
				if (null != AutoSaveStatus & !_contactDataAltered)
					AutoSaveStatus(AutoSaveState.WaitingFor);

				_contactDataAltered = true;
			}
		}
		
		void OnAutoSave_tb_Leave(object sender, EventArgs e)
		{
			if (_enableAutoSaveTrigger)
			{
				AutoSaveData();
				_contactDataAltered = false;
			}
		}

		void OnAutoSave_cb_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (_enableAutoSaveTrigger 
				//HOTFIX FOR CRASH
				//Crash happens when new contact is created and autosave is triggered upon contact 
				//that is about to be saved for the first time upon combobox change value event
				&& null != this.pi
				//END HOTFIX
				)
			{
				AutoSaveData();
				_contactDataAltered = false;
			}
		}

		enum AutoSaveState
		{
			WaitingFor = 0,
			Started = 1,
			Ended,
		}
		delegate void AutoSaveEventHandler(AutoSaveState state);
		event AutoSaveEventHandler AutoSaveStatus;
	
		void AutoSaveData()
		{
			try
			{
				if (null != AutoSaveStatus)
					AutoSaveStatus(AutoSaveState.Started);
			
				SaveDataEx();

				if (null != AutoSaveStatus)
					AutoSaveStatus(AutoSaveState.Ended);
			}
			catch 
			{ }
		}

		private void On_ContactScreen_AutoSaveStatus_Changed(AutoSaveState state)
		{
			SignalizeAutoSaveAsync(state);
		}

		private void On_PP_Downloaded_Successfully()
		{
			AutoSaveData();
		}

		private void On_COMPS_Downloaded_Successfully()
		{
			AutoSaveData();
		}

		delegate void SignalizeAutoSaveAsyncDelegate(AutoSaveState state);
		void SignalizeAutoSaveAsync(AutoSaveState state)
		{
			if (InvokeRequired)
				BeginInvoke(new SignalizeAutoSaveAsyncDelegate(SignalizeAutoSaveAsync), new object[] {state});
			else
			{
				if (state == AutoSaveState.WaitingFor)
				{
					this.tbLastModified.BackColor = Color.Gold;			
					this.tbLastModified.ForeColor = Color.Black;
															
					try
					{
                        if (null != _customSettings && 1 == _customSettings.SpecialSSX)
                            this.tbLastModified.Text = DateTime.Now.ToString("dd/MM/yy hh:mm tt");//DateTime.Now.ToShortDateString();
                        else
                            this.tbLastModified.Text = DateTime.Now.ToShortDateString();
				
						if (null == pi)
							this.tbFirstEntered.Text = DateTime.Now.ToShortDateString();																																											
					}
					catch {}	//shouldn't ever happen

					this.tbLastModified.Text += "*";					
					
				}
				else if (state == AutoSaveState.Started)
				{
					this.tbLastModified.BackColor = Color.Gold;			
					this.tbLastModified.ForeColor = Color.Green;
				}
				else
				{
					this.tbLastModified.BackColor = Color.LightGray;			
					this.tbLastModified.ForeColor = Color.Black;

					if (null != this.tbLastModified.Text)					
						this.tbLastModified.Text = this.tbLastModified.Text.Replace("*", string.Empty);
				}				
			}
		}
        //AUTOSAVE - END
        #endregion

        bool TestComboboxForOverwrite(ComboBox cb)
		{
			if (null == cb.SelectedItem || string.Empty == (string)cb.SelectedItem)
				return true;
			else
				return false;
		}

        //sasass: short sale module
        ShortSaleControl shortSaleControl;

        Panel shortSaleControlPanel = new Panel();
        MainMenu mainMenuBakcup = Globals.MainForm.Menu;

        private void ShowShortSaleModule()
        {
            ApplicationHelper.TestDatabase_CreateShortSaleTable();

            //todo check if in database, maybe move inside SetInsideProfitGrabber
            DealMaker.PlainClasses.ShortSale.Adapters.DataManager.Instance.SQLConnection = Connection.getInstance().HatchConn;
            DealMaker.PlainClasses.ShortSale.Adapters.DataManager.Instance.PropertyItemID = pi.IdPropertyItem;
            //            ShortSaleModule.Common.Adapters.DataManager.Instance.CreateNewShortSaleDocument();
            string err;
            DealMaker.PlainClasses.ShortSale.Adapters.DataManager.Instance.LoadShortSaleDocument("pg", out err);

            shortSaleControl = new ShortSaleControl();            

            shortSaleControl.UserAppFolder = OSFolderManager.Instance.GetPlainLocalUserAppDataPath();
            shortSaleControl.SetInsideProfitGrabber();

            DealMaker.PlainClasses.ShortSale.Adapters.DataManager.Instance.GetCurrentShortSaleDocument().ContactInfo.FirstName = pi.Owner.FirstName;
            DealMaker.PlainClasses.ShortSale.Adapters.DataManager.Instance.GetCurrentShortSaleDocument().ContactInfo.MiddleName = pi.Owner.MiddleName;
            DealMaker.PlainClasses.ShortSale.Adapters.DataManager.Instance.GetCurrentShortSaleDocument().ContactInfo.LastName = pi.Owner.LastName;
            DealMaker.PlainClasses.ShortSale.Adapters.DataManager.Instance.GetCurrentShortSaleDocument().ContactInfo.FullName = pi.Owner.FullName;
            DealMaker.PlainClasses.ShortSale.Adapters.DataManager.Instance.GetCurrentShortSaleDocument().ContactInfo.FullName = pi.Owner.FullName;
            DealMaker.PlainClasses.ShortSale.Adapters.DataManager.Instance.GetCurrentShortSaleDocument().ContactInfo.SiteAddress = pi.Owner.SiteAddress.FullSiteStreetAddress;
            DealMaker.PlainClasses.ShortSale.Adapters.DataManager.Instance.GetCurrentShortSaleDocument().ContactInfo.SiteCity = pi.Owner.SiteAddress.SiteCity;
            DealMaker.PlainClasses.ShortSale.Adapters.DataManager.Instance.GetCurrentShortSaleDocument().ContactInfo.SiteState = pi.Owner.SiteAddress.SiteState;
            DealMaker.PlainClasses.ShortSale.Adapters.DataManager.Instance.GetCurrentShortSaleDocument().ContactInfo.SiteZIP = pi.Owner.SiteAddress.SiteZIP;
            DealMaker.PlainClasses.ShortSale.Adapters.DataManager.Instance.GetCurrentShortSaleDocument().ContactInfo.EmailAddress = pi.Owner.EMail;

            //add to panel
            Panel shortSaleControlPanel = new Panel();
            shortSaleControlPanel.VisibleChanged += new EventHandler(p_VisibleChanged);
            shortSaleControl.Location = new Point(0, 0);
            shortSaleControlPanel.Controls.Add(shortSaleControl);
            shortSaleControlPanel.BackColor = shortSaleControl.BackColor;
            Globals.MainForm.Controls.Add(shortSaleControlPanel);
            shortSaleControlPanel.Location = new Point(0, 0);
            shortSaleControlPanel.Size = Globals.MainForm.Size;
            shortSaleControlPanel.Visible = true;
            shortSaleControlPanel.BringToFront();
            shortSaleControlPanel.Show();

            shortSaleControl.DisplayLoadedData();

            mainMenuBakcup = Globals.MainForm.Menu.CloneMenu();
            Globals.MainForm.Menu.MenuItems.Clear();            

            //MenuItem[] menu = new MenuItem[2];
            //menu[0] = new MenuItem("Label Offset Settings", new EventHandler(On_LabelOffsetSettings_Click));
            //menu[1] = new MenuItem("Print Summary Report", new EventHandler(On_PrintGroupReport_Click));

            //Globals.MainForm.Menu.MenuItems.Add(new MenuItem("Utils", menu));
        }
        
        private void On_LabelOffsetSettings_Click(object sender, EventArgs e)
        {
            //OffsetSettingsForm osf = new OffsetSettingsForm();
            //osf.ShowDialog();
        }                

        //short sale closed
        void p_VisibleChanged(object sender, EventArgs e)
        {
            //Globals.MainForm.Menu = mainMenuBakcup;
            //if (null != shortSaleControl)
            //{
            //    shortSaleControl.AutoSaveDocument();
            //    shortSaleControl.RemoveEventHandlers();
            //}
            //tabControl1.SelectTab(0);
        }

        //Add / Change House's Picture 
        void On_PbHouse_Click(object sender, EventArgs e)
        {
            string imgName = string.Empty;
            OpenFileDialog ofd = new OpenFileDialog();
            
            ofd.InitialDirectory = @"c:\";
            ofd.Filter = "jpg files (*.jpg)|*.jpg|gif files (*.gif)|*.gif|tif files (*.tif)|*.tif|All files (*.*)|*.*";
            ofd.FilterIndex = 1;

            if (ofd.ShowDialog() == DialogResult.OK)
            {
                imgName = ofd.FileName;

                try
                {
                    Image img = ScaleToFixedSize(Image.FromFile(imgName), _pbHouse.Width, _pbHouse.Height);
                    _pbHouse.Image = img;
                    PictureManager.Instance.SaveData(this.pi.IdPropertyItem, img, _pbHouse.BackColor, _pbHouse.Width, _pbHouse.Height);                    
                }
                catch { }
            }
        }

        void On_getStreetView_Click(object sender, EventArgs e)
        {
            try
            {
                string address = string.Empty;
                if (null != pi && null != pi.Owner && null != pi.Owner.SiteAddress)
                {
                    if (null != pi.Owner.SiteAddress.FullSiteStreetAddress)
                        address += pi.Owner.SiteAddress.FullSiteStreetAddress;

                    if (null != pi.Owner.SiteAddress.SiteCity)
                    {
                        if (string.Empty != address)
                            address += " ";

                        address += pi.Owner.SiteAddress.SiteCity;
                    }

                    if (null != pi.Owner.SiteAddress.SiteState)
                    {
                        if (string.Empty != address)
                            address += " ";

                        address += pi.Owner.SiteAddress.SiteState;
                    }

                    if (null != pi.Owner.SiteAddress.SiteZIP)
                    {
                        if (string.Empty != address)
                            address += " ";

                        address += pi.Owner.SiteAddress.SiteZIP;
                    }                    
                }

                System.Collections.Generic.List<string> addresses = new System.Collections.Generic.List<string>();
                addresses.Add(address);

                ImageDownloader imgDownloader = new ImageDownloader(addresses);
                ImageDownloadTracker idt = new ImageDownloadTracker(addresses.Count + 1);
                idt.ImgDownloader = imgDownloader;
                imgDownloader.StartDownload();
                idt.ShowDialog();
                                
                Image img = ScaleToFixedSize(imgDownloader.Images[1], _pbHouse.Width, _pbHouse.Height);
                _pbHouse.Image = img;
                PictureManager.Instance.SaveData(this.pi.IdPropertyItem, img, _pbHouse.BackColor, _pbHouse.Width, _pbHouse.Height);
            }
            catch { }

        }

        void On_UploadPhoto_Click(object sender, EventArgs e)
        {
            string imgName = string.Empty;
            OpenFileDialog ofd = new OpenFileDialog();

            ofd.InitialDirectory = @"c:\";
            ofd.Filter = "jpg files (*.jpg)|*.jpg|gif files (*.gif)|*.gif|tif files (*.tif)|*.tif|All files (*.*)|*.*";
            ofd.FilterIndex = 1;

            if (ofd.ShowDialog() == DialogResult.OK)
            {
                imgName = ofd.FileName;

                try
                {
                    Image img = ScaleToFixedSize(Image.FromFile(imgName), _pbHouse.Width, _pbHouse.Height);
                    _pbHouse.Image = img;
                    PictureManager.Instance.SaveData(this.pi.IdPropertyItem, img, _pbHouse.BackColor, _pbHouse.Width, _pbHouse.Height);
                }
                catch { }
            }
        }

        Image ScaleToFixedSize(Image imgPhoto, int Width, int Height)
        {
            int sourceWidth = imgPhoto.Width;
            int sourceHeight = imgPhoto.Height;
            int sourceX = 0;
            int sourceY = 0;
            int destX = 0;
            int destY = 0;

            float nPercent = 0;
            float nPercentW = 0;
            float nPercentH = 0;

            nPercentW = ((float)Width / (float)sourceWidth);
            nPercentH = ((float)Height / (float)sourceHeight);
            if (nPercentH < nPercentW)
            {
                nPercent = nPercentH;
                destX = System.Convert.ToInt16((Width - (sourceWidth * nPercent)) / 2);
            }
            else
            {
                nPercent = nPercentW;
                destY = System.Convert.ToInt16((Height - (sourceHeight * nPercent)) / 2);
            }

            int destWidth = (int)(sourceWidth * nPercent);
            int destHeight = (int)(sourceHeight * nPercent);

            Bitmap bmPhoto = new Bitmap(Width, Height, PixelFormat.Format24bppRgb);
            bmPhoto.SetResolution(imgPhoto.HorizontalResolution, imgPhoto.VerticalResolution);

            Graphics grPhoto = Graphics.FromImage(bmPhoto);
            grPhoto.Clear(BackColor);
            grPhoto.InterpolationMode = InterpolationMode.HighQualityBicubic;

            grPhoto.DrawImage(imgPhoto,
                new Rectangle(destX, destY, destWidth, destHeight),
                new Rectangle(sourceX, sourceY, sourceWidth, sourceHeight),
                GraphicsUnit.Pixel);

            grPhoto.Dispose();
            return bmPhoto;
        }

        private void _skipTraceRecord_Click(object sender, EventArgs e)
        {
            var dr = MessageBox.Show($"Before starting, you should make sure you have a name and address for each of the contacts in the list so this service will have the best chance of finding correct information.{System.Environment.NewLine}{System.Environment.NewLine}If you do not have names, you should run the Matching Module under the Tools menu to find names before starting this search.{System.Environment.NewLine}{System.Environment.NewLine}Your account will be charged if any skip traced data is returned.", "Skip Trace", MessageBoxButtons.OKCancel, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
            if (dr != DialogResult.OK)
                return;

            using (AppendDataForm appendDataForm = new AppendDataForm(Globals.selectedNodeId, this.pi.IdPropertyItem, this as IAppendDataForm))
            {
                appendDataForm.ShowDialog();
            }
        }

        
        private delegate void UpdateSTLandlineDelegate(string phoneNr);
        private delegate void UpdateSTEmailDelegate(string email);
        private delegate void UpdateSTCellPhoneDelegate(string cellPhone);

        private void UpdateSTLandline(string phoneNr)
        {
            _stLandline.Text = phoneNr;
            _stLastUpdated.Text = $" Updated {DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss")}";
        }

        private void UpdateSTEmail(string email)
        {
            _stEmail.Text = email;
            _stLastUpdated.Text = $" Updated {DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss")}";
        }

        private void UpdateSTCell(string cellPhone)
        {
            _stCellPhone.Text = cellPhone;
            _stLastUpdated.Text = $" Updated {DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss")}";
        }

        public void AppendDataWorker_OnCellRetrieved(bool success, string fullName, string address, string zip, string phoneNr)
        {
            if (success)
            {
                if (_stCellPhone.InvokeRequired)
                {
                    BeginInvoke(new UpdateSTCellPhoneDelegate(UpdateSTCell), new object[] { phoneNr });
                }
                else
                    UpdateSTCell(phoneNr);                
            }                
        }

        public void AppendDataWorker_OnLandLineRetrieved(bool success, string fullName, string address, string zip, string phoneNr)
        {
            if (success)
            {
                if (_stLandline.InvokeRequired)
                {
                    BeginInvoke(new UpdateSTLandlineDelegate(UpdateSTLandline), new object[] { phoneNr });
                }
                else
                {
                    UpdateSTLandline(phoneNr);
                }
            }
        }

        public void AppendDataWorker_OnEmailRetrieved(bool success, string fullName, string address, string zip, string email)
        {
            if (success)
            {
                if (_stEmail.InvokeRequired)
                {
                    BeginInvoke(new UpdateSTEmailDelegate(UpdateSTEmail), new object[] { email });
                }
                else
                    UpdateSTEmail(email);
            }                
        }

        public void AppendDataWorker_OnProcessFinished(bool success, string error, int matche, int totalProcessed)
        {
            //NOP, we save before
            //if (success)
            //{
            //    if (InvokeRequired)
            //        BeginInvoke(new Action(AutoSaveData));
            //}
        }

        public void AppendDataWorker_OnSkipTraceDataRetrieved(DataAppendService.Lib.Models.IDI.SkiptraceClientResultset stcrs)
        {
            UpdateSkipTracedTab(stcrs);
        }

        delegate void UpdateSkipTracedTabDelegate (DataAppendService.Lib.Models.IDI.SkiptraceClientResultset stData);
        void UpdateSkipTracedTab(DataAppendService.Lib.Models.IDI.SkiptraceClientResultset stData)
        {
            if (null == stData)
                return;

            if (this.InvokeRequired)
            {
                BeginInvoke(new UpdateSkipTracedTabDelegate(UpdateSkipTracedTab), new object[] { stData });
                return;
            }

            InvalidateSkipTracedTab();

            //contact info
            st_ci_firstName.Text = stData.FirstName;
            st_ci_middleName.Text = stData.MiddleName;
            st_ci_lastName.Text = stData.LastName;
            st_ci_fullName.Text = stData.FullName;
            st_ci_age.Text = stData.Age;
            st_ci_aliasName.Text = stData.Alias;

            if (null != stData.Addresses && stData.Addresses.Count > 0)
            {
                st_ci_a0_address.Text = stData.Addresses[0][0];
                st_ci_a0_city.Text = stData.Addresses[0][1];
                st_ci_a0_state.Text = stData.Addresses[0][2];
                st_ci_a0_zip.Text = stData.Addresses[0][3];
                st_ci_a0_firstSeen.Text = DateTime.TryParseExact(stData.Addresses[0][4], "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None, out var dtf) ? dtf.ToString("MM/dd/yyyy") : stData.Addresses[0][4];                
                st_ci_a0_lastSeen.Text = DateTime.TryParseExact(stData.Addresses[0][5], "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None, out var dtl) ? dtl.ToString("MM/dd/yyyy") : stData.Addresses[0][5];
                
            }

            if (null != stData.Addresses && stData.Addresses.Count > 1)
            {
                st_ci_a1_address.Text = stData.Addresses[1][0];
                st_ci_a1_city.Text = stData.Addresses[1][1];
                st_ci_a1_state.Text = stData.Addresses[1][2];
                st_ci_a1_zip.Text = stData.Addresses[1][3];                
                st_ci_a1_firstSeen.Text = DateTime.TryParseExact(stData.Addresses[1][4], "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None, out var dtf) ? dtf.ToString("MM/dd/yyyy") : stData.Addresses[1][4];
                st_ci_a1_lastSeen.Text = DateTime.TryParseExact(stData.Addresses[1][5], "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None, out var dtl) ? dtl.ToString("MM/dd/yyyy") : stData.Addresses[1][5];
            }

            if (null != stData.Addresses && stData.Addresses.Count > 2)
            {
                st_ci_a2_address.Text = stData.Addresses[2][0];
                st_ci_a2_city.Text = stData.Addresses[2][1];
                st_ci_a2_state.Text = stData.Addresses[2][2];
                st_ci_a2_zip.Text = stData.Addresses[2][3];                
                st_ci_a2_firstSeen.Text = DateTime.TryParseExact(stData.Addresses[2][4], "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None, out var dtf) ? dtf.ToString("MM/dd/yyyy") : stData.Addresses[2][4];
                st_ci_a2_lastSeen.Text = DateTime.TryParseExact(stData.Addresses[2][5], "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None, out var dtl) ? dtl.ToString("MM/dd/yyyy") : stData.Addresses[2][5];
            }

            st_ci_mp0.Text = (null != stData.MobilePhones && stData.MobilePhones.Count > 0 ) ? stData.MobilePhones[0] : "N/A";
            st_ci_mp1.Text = (null != stData.MobilePhones && stData.MobilePhones.Count > 1) ? stData.MobilePhones[1] : "N/A";
            st_ci_mp2.Text = (null != stData.MobilePhones && stData.MobilePhones.Count > 2) ? stData.MobilePhones[2] : "N/A";

            st_ci_rp3.Text = (null != stData.ResidentialPhones && stData.ResidentialPhones.Count > 0) ? stData.ResidentialPhones[0] : "N/A";
            st_ci_rp4.Text = (null != stData.ResidentialPhones && stData.ResidentialPhones.Count > 1) ? stData.ResidentialPhones[1] : "N/A";

            st_ci_email0.Text = (null != stData.Emails && stData.Emails.Count > 0) ? stData.Emails[0] : "N/A";
            st_ci_email1.Text = (null != stData.Emails && stData.Emails.Count > 1) ? stData.Emails[1] : "N/A";
            st_ci_email2.Text = (null != stData.Emails && stData.Emails.Count > 2) ? stData.Emails[2] : "N/A";

            //other properties
            st_op_address1.Text = null != stData.Properties && stData.Properties.Count > 0 ? stData.Properties[0].Address : string.Empty;
            st_op_address2.Text = null != stData.Properties && stData.Properties.Count > 1 ? stData.Properties[1].Address : string.Empty;
            st_op_address3.Text = null != stData.Properties && stData.Properties.Count > 2 ? stData.Properties[2].Address : string.Empty;
            st_op_address4.Text = null != stData.Properties && stData.Properties.Count > 3 ? stData.Properties[3].Address : string.Empty;

            st_op_county1.Text = null != stData.Properties && stData.Properties.Count > 0 && null != stData.Properties[0].Detail ? stData.Properties[0].Detail[0] : string.Empty;
            st_op_county2.Text = null != stData.Properties && stData.Properties.Count > 1 && null != stData.Properties[1].Detail ? stData.Properties[1].Detail[0] : string.Empty;
            st_op_county3.Text = null != stData.Properties && stData.Properties.Count > 2 && null != stData.Properties[2].Detail ? stData.Properties[2].Detail[0] : string.Empty;
            st_op_county4.Text = null != stData.Properties && stData.Properties.Count > 3 && null != stData.Properties[3].Detail ? stData.Properties[3].Detail[0] : string.Empty;

            st_op_useCode1.Text = null != stData.Properties && stData.Properties.Count > 0 && null != stData.Properties[0].Detail ? stData.Properties[0].Detail[1] : string.Empty;
            st_op_useCode2.Text = null != stData.Properties && stData.Properties.Count > 1 && null != stData.Properties[1].Detail ? stData.Properties[1].Detail[1] : string.Empty;
            st_op_useCode3.Text = null != stData.Properties && stData.Properties.Count > 2 && null != stData.Properties[2].Detail ? stData.Properties[2].Detail[1] : string.Empty;
            st_op_useCode4.Text = null != stData.Properties && stData.Properties.Count > 3 && null != stData.Properties[3].Detail ? stData.Properties[3].Detail[1] : string.Empty;

            st_op_parcelNumber1.Text = null != stData.Properties && stData.Properties.Count > 0 && null != stData.Properties[0].Detail ? stData.Properties[0].Detail[2] : string.Empty;
            st_op_parcelNumber2.Text = null != stData.Properties && stData.Properties.Count > 1 && null != stData.Properties[1].Detail ? stData.Properties[1].Detail[2] : string.Empty;
            st_op_parcelNumber3.Text = null != stData.Properties && stData.Properties.Count > 2 && null != stData.Properties[2].Detail ? stData.Properties[2].Detail[2] : string.Empty;
            st_op_parcelNumber4.Text = null != stData.Properties && stData.Properties.Count > 3 && null != stData.Properties[3].Detail ? stData.Properties[3].Detail[2] : string.Empty;

            st_op_lotSqFt1.Text = null != stData.Properties && stData.Properties.Count > 0 && null != stData.Properties[0].Detail ? stData.Properties[0].Detail[3] : string.Empty;
            st_op_lotSqFt2.Text = null != stData.Properties && stData.Properties.Count > 1 && null != stData.Properties[1].Detail ? stData.Properties[1].Detail[3] : string.Empty;
            st_op_lotSqFt3.Text = null != stData.Properties && stData.Properties.Count > 2 && null != stData.Properties[2].Detail ? stData.Properties[2].Detail[3] : string.Empty;
            st_op_lotSqFt4.Text = null != stData.Properties && stData.Properties.Count > 3 && null != stData.Properties[3].Detail ? stData.Properties[3].Detail[3] : string.Empty;

            st_op_buildingSqFt1.Text = null != stData.Properties && stData.Properties.Count > 0 && null != stData.Properties[0].Detail ? stData.Properties[0].Detail[4] : string.Empty;
            st_op_buildingSqFt2.Text = null != stData.Properties && stData.Properties.Count > 1 && null != stData.Properties[1].Detail ? stData.Properties[1].Detail[4] : string.Empty;
            st_op_buildingSqFt3.Text = null != stData.Properties && stData.Properties.Count > 2 && null != stData.Properties[2].Detail ? stData.Properties[2].Detail[4] : string.Empty;
            st_op_buildingSqFt4.Text = null != stData.Properties && stData.Properties.Count > 3 && null != stData.Properties[3].Detail ? stData.Properties[3].Detail[4] : string.Empty;

            st_op_yearBuilt1.Text = null != stData.Properties && stData.Properties.Count > 0 && null != stData.Properties[0].Detail ? stData.Properties[0].Detail[5] : string.Empty;
            st_op_yearBuilt2.Text = null != stData.Properties && stData.Properties.Count > 1 && null != stData.Properties[1].Detail ? stData.Properties[1].Detail[5] : string.Empty;
            st_op_yearBuilt3.Text = null != stData.Properties && stData.Properties.Count > 2 && null != stData.Properties[2].Detail ? stData.Properties[2].Detail[5] : string.Empty;
            st_op_yearBuilt4.Text = null != stData.Properties && stData.Properties.Count > 3 && null != stData.Properties[3].Detail ? stData.Properties[3].Detail[5] : string.Empty;

            st_op_bedrooms1.Text = null != stData.Properties && stData.Properties.Count > 0 && null != stData.Properties[0].Detail ? stData.Properties[0].Detail[6] : string.Empty;
            st_op_bedrooms2.Text = null != stData.Properties && stData.Properties.Count > 1 && null != stData.Properties[1].Detail ? stData.Properties[1].Detail[6] : string.Empty;
            st_op_bedrooms3.Text = null != stData.Properties && stData.Properties.Count > 2 && null != stData.Properties[2].Detail ? stData.Properties[2].Detail[6] : string.Empty;
            st_op_bedrooms4.Text = null != stData.Properties && stData.Properties.Count > 3 && null != stData.Properties[3].Detail ? stData.Properties[3].Detail[6] : string.Empty;

            st_op_bathrooms1.Text = null != stData.Properties && stData.Properties.Count > 0 && null != stData.Properties[0].Detail ? stData.Properties[0].Detail[7] : string.Empty;
            st_op_bathrooms2.Text = null != stData.Properties && stData.Properties.Count > 1 && null != stData.Properties[1].Detail ? stData.Properties[1].Detail[7] : string.Empty;
            st_op_bathrooms3.Text = null != stData.Properties && stData.Properties.Count > 2 && null != stData.Properties[2].Detail ? stData.Properties[2].Detail[7] : string.Empty;
            st_op_bathrooms4.Text = null != stData.Properties && stData.Properties.Count > 3 && null != stData.Properties[3].Detail ? stData.Properties[3].Detail[7] : string.Empty;

            if (null != stData.Properties && stData.Properties.Count > 0 && null != stData.Properties[0].Transfers)
            {
                foreach (var transferItem in stData.Properties[0].Transfers)
                {
                    var lvi = new ListViewItem(transferItem[0]);
                    for (int i = 1; i <= 6; i++)
                        lvi.SubItems.Add(transferItem[i]);
                    

                    st_op_lv1.Items.Add(lvi);
                }
            }

            if (null != stData.Properties && stData.Properties.Count > 1 && null != stData.Properties[1].Transfers)
            {
                foreach (var transferItem in stData.Properties[1].Transfers)
                {
                    var lvi = new ListViewItem(transferItem[0]);
                    for (int i = 1; i <= 6; i++)
                        lvi.SubItems.Add(transferItem[i]);

                    st_op_lv2.Items.Add(lvi);
                }
            }

            if (null != stData.Properties && stData.Properties.Count > 2 && null != stData.Properties[0].Transfers)
            {
                foreach (var transferItem in stData.Properties[2].Transfers)
                {
                    var lvi = new ListViewItem(transferItem[0]);
                    for (int i = 1; i <= 6; i++)
                        lvi.SubItems.Add(transferItem[i]);

                    st_op_lv3.Items.Add(lvi);
                }
            }

            if (null != stData.Properties && stData.Properties.Count > 3 && null != stData.Properties[0].Transfers)
            {
                foreach (var transferItem in stData.Properties[3].Transfers)
                {
                    var lvi = new ListViewItem(transferItem[0]);
                    for (int i = 1; i <= 6; i++)
                        lvi.SubItems.Add(transferItem[i]);

                    st_op_lv4.Items.Add(lvi);
                }
            }

            if (null != stData.RelativesInfo)
                stData.RelativesInfo.ForEach(r => {
                    if (r.Count > 0)
                    {
                        var lvi = new ListViewItem(r[0]);
                        for (var i = 1; i < r.Count; i++)
                            lvi.SubItems.Add(r[i]);

                        st_ri_relatives.Items.Add(lvi);
                    }
                });
            

        }

        private void InvalidateSkipTracedTab()
        {
            foreach (var ctrl in skipTracedContactInfo.Controls)
            {
                if (ctrl.GetType() == typeof(TextBox))
                    ((TextBox)ctrl).Text = string.Empty;

                else if (ctrl.GetType() == typeof(WindowsApplication.AcquisitionCalculator.TextBox))
                    ((TextBox)ctrl).Text = string.Empty;
            }

            foreach (var ctrl in skipTracedOtherProperty.Controls)
            {
                if (ctrl.GetType() == typeof(WindowsApplication.AcquisitionCalculator.TextBox))
                    ((TextBox)ctrl).Text = string.Empty;

                else if (ctrl.GetType() == typeof(TextBox))
                    ((TextBox)ctrl).Text = string.Empty;

                else if (ctrl.GetType() == typeof(System.Windows.Forms.ListView))
                    ((System.Windows.Forms.ListView)ctrl).Items.Clear();
            }

            foreach (var ctrl in skipTracedRelativesInfo.Controls)
            {                
                if (ctrl.GetType() == typeof(System.Windows.Forms.ListView))
                    ((System.Windows.Forms.ListView)ctrl).Items.Clear();
            }
        }

        private void _copySTRank0InfoToContactInfoTab_Click(object sender, EventArgs e)
        {
            OverwriteTextBoxValues(
                new []
                {
                    (tbMailPreviousAdd, tbMailOldAdd), (tbMailPreviousCity, tbMailOldCity), (tbMailPreviousState, tbMailOldState), (tbMailPreviousZIP, tbMailOldZIP),
                    (tbMailLastKnownAdd, tbMailPreviousAdd), (tbMailLastKnownCity, tbMailPreviousCity), (tbMailLastKnownState, tbMailPreviousState), (tbMailLastKnownZIP, tbMailPreviousZIP),
                    (st_ci_a0_address, tbMailLastKnownAdd), (st_ci_a0_city, tbMailLastKnownCity), (st_ci_a0_state, tbMailLastKnownState), (st_ci_a0_zip, tbMailLastKnownZIP)
                });

            AutoSaveData();
        }

        private void OverwriteTextBoxValues((TextBox, TextBox)[] tbTuples)
        {
            foreach (var tuple in tbTuples)
                OverwriteTextBoxValue(tuple.Item1, tuple.Item2);
        }

        private void OverwriteTextBoxValue(TextBox from, TextBox to)
        {
            to.Text = from.Text;
        }

        private void tpContactInfo_Click(object sender, EventArgs e)
        {

        }
    }
	#endregion � Class ContactScreen �
}
#endregion � Namespace DealMaker �


