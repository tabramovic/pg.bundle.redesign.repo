#region � Using �
using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;
using NHibernate;
using NHibernate.Expression;
using System.Data.SqlClient;
using DealMaker.Export;
using System.Collections.Generic;
using System.Linq;
#endregion � Using �
#region � Namespace DealMaker �
namespace DealMaker
{
	#region � Class FindPropertyItem �
	/// <summary>
	/// FindPropertyItem Class
	/// </summary>
	public class FindPropertyItem : System.Windows.Forms.UserControl
	{
		#region � Data �		
		
		private DealMaker.Forms.Floaters.FindContact floater = null;
		
		//ISessionFactory factory = null;
		ISession session = null;
		ITransaction transaction = null;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.ListView lvGroups;
		private System.Windows.Forms.ColumnHeader groupId;
		private System.Windows.Forms.ColumnHeader groupName;
		private System.Windows.Forms.Button bAddGroup;
		private System.Windows.Forms.Button bRemoveGroup;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Button bSearch;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.Label label13;
		private System.Windows.Forms.Button bClearAll;
		private System.Windows.Forms.TextBox tbSearchedItems;
		private System.Windows.Forms.TextBox tbFoundMatching;
		private System.Windows.Forms.ListView lvResults;
		private System.Windows.Forms.CheckBox cbSearchSubgroups;
		private System.Windows.Forms.ColumnHeader name;
		private System.Windows.Forms.ColumnHeader siteAddress;
		private System.Windows.Forms.ColumnHeader siteZIP;
		private System.Windows.Forms.ColumnHeader mailingAddress;
		private System.Windows.Forms.ColumnHeader mailZIP;
		private System.Windows.Forms.ColumnHeader listingAgentName;
		private System.Windows.Forms.ColumnHeader mlsNr;
		private System.Windows.Forms.ColumnHeader subdivision;
		private System.Windows.Forms.ColumnHeader tag;
		private System.Windows.Forms.Button bExport;
		private System.Windows.Forms.Button bLookUpUnTagged;
		private System.Windows.Forms.Button bLookUpTagged;
		private System.Windows.Forms.Button bUnTagAll;
		private System.Windows.Forms.Button bTagAll;
		private System.Windows.Forms.Button bDelete;
		private System.Windows.Forms.CheckBox cbSearchAllGroups;
		private DealMaker.ReadMe readMe1;
		private System.Windows.Forms.CheckBox cbName;
		private System.Windows.Forms.CheckBox cbSSA;
		private System.Windows.Forms.CheckBox cbMailA;
		private System.Windows.Forms.CheckBox cbLAFN;
		private System.Windows.Forms.CheckBox cbSubDiv;
		private System.Windows.Forms.CheckBox cbSiteZip;
		private System.Windows.Forms.CheckBox cbMailZip;
		private System.Windows.Forms.CheckBox cbMLSNr;
		private System.Windows.Forms.CheckBox cbTelNr;
		private System.Windows.Forms.TextBox tbSearchField;
		private System.Windows.Forms.Label label15;
		private System.Windows.Forms.Button bAssignToGroup;
		private System.Windows.Forms.CheckBox cbSiteCity;
		private System.Windows.Forms.CheckBox cbMailCity;
		private System.Windows.Forms.ColumnHeader siteCity;
		private System.Windows.Forms.ColumnHeader mailCity;
		private System.Windows.Forms.ColumnHeader phoneNo;
		private System.Windows.Forms.CheckBox cbBarCode;
		private CheckBox cbEmail;

		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
		#endregion � Data �
		#region � CTR && Dispose �
		public FindPropertyItem()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();
	
			this.cbName.Tag = 1;
			this.cbSSA.Tag = 2;
			this.cbMailA.Tag = 3;
			this.cbLAFN.Tag = 4;
			this.cbSubDiv.Tag = 5;
			this.cbSiteZip.Tag = 6;
			this.cbMailZip.Tag = 7;
			this.cbMLSNr.Tag = 8;
			this.cbTelNr.Tag = 9;
			this.cbSiteCity.Tag = 10;
			this.cbMailCity.Tag = 11;
			this.cbBarCode.Tag = 12;
			this.cbEmail.Tag = 13;

			this.cbName.Enter +=new EventHandler(cb_CheckedChanged);
			this.cbSSA.Enter +=new EventHandler(cb_CheckedChanged);
			this.cbMailA.Enter +=new EventHandler(cb_CheckedChanged);
			this.cbLAFN.Enter +=new EventHandler(cb_CheckedChanged);
			this.cbSubDiv.Enter +=new EventHandler(cb_CheckedChanged);
			this.cbSiteZip.Enter +=new EventHandler(cb_CheckedChanged);
			this.cbMailZip.Enter +=new EventHandler(cb_CheckedChanged);
			this.cbMLSNr.Enter +=new EventHandler(cb_CheckedChanged);
			this.cbTelNr.Enter +=new EventHandler(cb_CheckedChanged);

			this.cbMailCity.Enter += new EventHandler(cb_CheckedChanged);
			this.cbSiteCity.Enter += new EventHandler(cb_CheckedChanged);
			this.cbBarCode.Enter += new EventHandler(cb_CheckedChanged);

			cbEmail.Enter += new EventHandler(cb_CheckedChanged);

			this.readMe1.MouseEnter += new EventHandler(Floater_MouseEnter);
			this.readMe1.MouseLeave += new EventHandler(Floater_MouseLeave);

			cbSearchAllGroups.Checked = true;
		}

		private void CbEmail_Enter(object sender, EventArgs e)
		{
			throw new NotImplementedException();
		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}
		#endregion � CTR && Dispose �
		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FindPropertyItem));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lvGroups = new System.Windows.Forms.ListView();
            this.groupId = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.groupName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.bAddGroup = new System.Windows.Forms.Button();
            this.bRemoveGroup = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.bSearch = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.tbSearchedItems = new System.Windows.Forms.TextBox();
            this.tbFoundMatching = new System.Windows.Forms.TextBox();
            this.bClearAll = new System.Windows.Forms.Button();
            this.lvResults = new System.Windows.Forms.ListView();
            this.tag = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.name = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.siteAddress = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.siteCity = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.siteZIP = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.mailingAddress = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.mailCity = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.mailZIP = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.listingAgentName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.mlsNr = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.subdivision = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.phoneNo = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.cbSearchSubgroups = new System.Windows.Forms.CheckBox();
            this.bExport = new System.Windows.Forms.Button();
            this.bLookUpUnTagged = new System.Windows.Forms.Button();
            this.bLookUpTagged = new System.Windows.Forms.Button();
            this.bUnTagAll = new System.Windows.Forms.Button();
            this.bTagAll = new System.Windows.Forms.Button();
            this.cbSearchAllGroups = new System.Windows.Forms.CheckBox();
            this.bDelete = new System.Windows.Forms.Button();
            this.readMe1 = new DealMaker.ReadMe();
            this.cbName = new System.Windows.Forms.CheckBox();
            this.cbSSA = new System.Windows.Forms.CheckBox();
            this.cbMailA = new System.Windows.Forms.CheckBox();
            this.cbLAFN = new System.Windows.Forms.CheckBox();
            this.cbSubDiv = new System.Windows.Forms.CheckBox();
            this.cbSiteZip = new System.Windows.Forms.CheckBox();
            this.cbMailZip = new System.Windows.Forms.CheckBox();
            this.cbMLSNr = new System.Windows.Forms.CheckBox();
            this.cbTelNr = new System.Windows.Forms.CheckBox();
            this.tbSearchField = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.bAssignToGroup = new System.Windows.Forms.Button();
            this.cbSiteCity = new System.Windows.Forms.CheckBox();
            this.cbMailCity = new System.Windows.Forms.CheckBox();
            this.cbBarCode = new System.Windows.Forms.CheckBox();
            this.cbEmail = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(16, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(87, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Find in groups";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(32, 40);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(87, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Groups Affected:";
            // 
            // lvGroups
            // 
            this.lvGroups.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lvGroups.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.groupId,
            this.groupName});
            this.lvGroups.FullRowSelect = true;
            this.lvGroups.GridLines = true;
            this.lvGroups.HideSelection = false;
            this.lvGroups.Location = new System.Drawing.Point(32, 64);
            this.lvGroups.Name = "lvGroups";
            this.lvGroups.Size = new System.Drawing.Size(608, 80);
            this.lvGroups.TabIndex = 2;
            this.lvGroups.UseCompatibleStateImageBehavior = false;
            this.lvGroups.View = System.Windows.Forms.View.Details;
            // 
            // groupId
            // 
            this.groupId.Text = "";
            this.groupId.Width = 0;
            // 
            // groupName
            // 
            this.groupName.Text = "Group Name";
            this.groupName.Width = 603;
            // 
            // bAddGroup
            // 
            this.bAddGroup.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.bAddGroup.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.bAddGroup.Location = new System.Drawing.Point(648, 32);
            this.bAddGroup.Name = "bAddGroup";
            this.bAddGroup.Size = new System.Drawing.Size(112, 40);
            this.bAddGroup.TabIndex = 3;
            this.bAddGroup.Text = "Add group";
            this.bAddGroup.Click += new System.EventHandler(this.bAddGroup_Click);
            // 
            // bRemoveGroup
            // 
            this.bRemoveGroup.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.bRemoveGroup.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.bRemoveGroup.Location = new System.Drawing.Point(648, 80);
            this.bRemoveGroup.Name = "bRemoveGroup";
            this.bRemoveGroup.Size = new System.Drawing.Size(112, 40);
            this.bRemoveGroup.TabIndex = 4;
            this.bRemoveGroup.Text = "Remove group";
            this.bRemoveGroup.Click += new System.EventHandler(this.bRemoveGroup_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(16, 200);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Find fields";
            // 
            // bSearch
            // 
            this.bSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.bSearch.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.bSearch.Location = new System.Drawing.Point(648, 160);
            this.bSearch.Name = "bSearch";
            this.bSearch.Size = new System.Drawing.Size(112, 23);
            this.bSearch.TabIndex = 27;
            this.bSearch.Text = "&Find";
            this.bSearch.Click += new System.EventHandler(this.bSearch_Click);
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label4.Location = new System.Drawing.Point(16, 320);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(100, 16);
            this.label4.TabIndex = 25;
            this.label4.Text = "Results";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(488, 346);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(65, 13);
            this.label12.TabIndex = 26;
            this.label12.Text = "Found Items";
            this.label12.Visible = false;
            // 
            // label13
            // 
            this.label13.Location = new System.Drawing.Point(40, 346);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(192, 16);
            this.label13.TabIndex = 27;
            this.label13.Text = "Found Matching In Selected Groups";
            // 
            // tbSearchedItems
            // 
            this.tbSearchedItems.Location = new System.Drawing.Point(574, 344);
            this.tbSearchedItems.Name = "tbSearchedItems";
            this.tbSearchedItems.ReadOnly = true;
            this.tbSearchedItems.Size = new System.Drawing.Size(72, 20);
            this.tbSearchedItems.TabIndex = 32;
            this.tbSearchedItems.Visible = false;
            // 
            // tbFoundMatching
            // 
            this.tbFoundMatching.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbFoundMatching.Location = new System.Drawing.Point(240, 344);
            this.tbFoundMatching.Name = "tbFoundMatching";
            this.tbFoundMatching.ReadOnly = true;
            this.tbFoundMatching.Size = new System.Drawing.Size(72, 20);
            this.tbFoundMatching.TabIndex = 30;
            // 
            // bClearAll
            // 
            this.bClearAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.bClearAll.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.bClearAll.Location = new System.Drawing.Point(648, 128);
            this.bClearAll.Name = "bClearAll";
            this.bClearAll.Size = new System.Drawing.Size(112, 23);
            this.bClearAll.TabIndex = 5;
            this.bClearAll.Text = "Clear All";
            this.bClearAll.Click += new System.EventHandler(this.bClearAll_Click);
            // 
            // lvResults
            // 
            this.lvResults.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lvResults.CheckBoxes = true;
            this.lvResults.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.tag,
            this.name,
            this.siteAddress,
            this.siteCity,
            this.siteZIP,
            this.mailingAddress,
            this.mailCity,
            this.mailZIP,
            this.listingAgentName,
            this.mlsNr,
            this.subdivision,
            this.phoneNo});
            this.lvResults.FullRowSelect = true;
            this.lvResults.GridLines = true;
            this.lvResults.HideSelection = false;
            this.lvResults.Location = new System.Drawing.Point(16, 376);
            this.lvResults.MultiSelect = false;
            this.lvResults.Name = "lvResults";
            this.lvResults.Size = new System.Drawing.Size(886, 308);
            this.lvResults.TabIndex = 34;
            this.lvResults.UseCompatibleStateImageBehavior = false;
            this.lvResults.View = System.Windows.Forms.View.Details;
            this.lvResults.DoubleClick += new System.EventHandler(this.lvResults_DoubleClick);
            // 
            // tag
            // 
            this.tag.Text = "Tag";
            this.tag.Width = 39;
            // 
            // name
            // 
            this.name.Text = "Name";
            this.name.Width = 163;
            // 
            // siteAddress
            // 
            this.siteAddress.Text = "Site Address";
            this.siteAddress.Width = 147;
            // 
            // siteCity
            // 
            this.siteCity.Text = "Site City";
            this.siteCity.Width = 94;
            // 
            // siteZIP
            // 
            this.siteZIP.Text = "Site ZIP";
            this.siteZIP.Width = 82;
            // 
            // mailingAddress
            // 
            this.mailingAddress.Text = "Mailing Address";
            this.mailingAddress.Width = 164;
            // 
            // mailCity
            // 
            this.mailCity.Text = "Mailing City";
            this.mailCity.Width = 100;
            // 
            // mailZIP
            // 
            this.mailZIP.Text = "Mail ZIP";
            this.mailZIP.Width = 75;
            // 
            // listingAgentName
            // 
            this.listingAgentName.Text = "Listing Agent Name";
            this.listingAgentName.Width = 116;
            // 
            // mlsNr
            // 
            this.mlsNr.Text = "MLS Nr";
            this.mlsNr.Width = 55;
            // 
            // subdivision
            // 
            this.subdivision.Text = "Subdivision";
            this.subdivision.Width = 70;
            // 
            // phoneNo
            // 
            this.phoneNo.Text = "Phone No.";
            this.phoneNo.Width = 100;
            // 
            // cbSearchSubgroups
            // 
            this.cbSearchSubgroups.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbSearchSubgroups.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.cbSearchSubgroups.Location = new System.Drawing.Point(480, 160);
            this.cbSearchSubgroups.Name = "cbSearchSubgroups";
            this.cbSearchSubgroups.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.cbSearchSubgroups.Size = new System.Drawing.Size(160, 24);
            this.cbSearchSubgroups.TabIndex = 6;
            this.cbSearchSubgroups.Text = "Search in Subgroups";
            // 
            // bExport
            // 
            this.bExport.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.bExport.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.bExport.Location = new System.Drawing.Point(784, 696);
            this.bExport.Name = "bExport";
            this.bExport.Size = new System.Drawing.Size(120, 23);
            this.bExport.TabIndex = 42;
            this.bExport.Text = "Export Tagged";
            this.bExport.Click += new System.EventHandler(this.bExport_Click);
            // 
            // bLookUpUnTagged
            // 
            this.bLookUpUnTagged.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.bLookUpUnTagged.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.bLookUpUnTagged.Location = new System.Drawing.Point(528, 696);
            this.bLookUpUnTagged.Name = "bLookUpUnTagged";
            this.bLookUpUnTagged.Size = new System.Drawing.Size(120, 23);
            this.bLookUpUnTagged.TabIndex = 41;
            this.bLookUpUnTagged.Text = "Lookup Untagged";
            this.bLookUpUnTagged.Click += new System.EventHandler(this.bLookUpUnTagged_Click);
            // 
            // bLookUpTagged
            // 
            this.bLookUpTagged.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.bLookUpTagged.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.bLookUpTagged.Location = new System.Drawing.Point(400, 696);
            this.bLookUpTagged.Name = "bLookUpTagged";
            this.bLookUpTagged.Size = new System.Drawing.Size(120, 23);
            this.bLookUpTagged.TabIndex = 40;
            this.bLookUpTagged.Text = "Lookup Tagged";
            this.bLookUpTagged.Click += new System.EventHandler(this.bLookUpTagged_Click);
            // 
            // bUnTagAll
            // 
            this.bUnTagAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.bUnTagAll.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.bUnTagAll.Location = new System.Drawing.Point(272, 696);
            this.bUnTagAll.Name = "bUnTagAll";
            this.bUnTagAll.Size = new System.Drawing.Size(120, 23);
            this.bUnTagAll.TabIndex = 39;
            this.bUnTagAll.Text = "Untag All";
            this.bUnTagAll.Click += new System.EventHandler(this.bUnTagAll_Click);
            // 
            // bTagAll
            // 
            this.bTagAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.bTagAll.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.bTagAll.Location = new System.Drawing.Point(144, 696);
            this.bTagAll.Name = "bTagAll";
            this.bTagAll.Size = new System.Drawing.Size(120, 23);
            this.bTagAll.TabIndex = 38;
            this.bTagAll.Text = "Tag All";
            this.bTagAll.Click += new System.EventHandler(this.bTagAll_Click);
            // 
            // cbSearchAllGroups
            // 
            this.cbSearchAllGroups.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbSearchAllGroups.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cbSearchAllGroups.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.cbSearchAllGroups.Location = new System.Drawing.Point(504, 32);
            this.cbSearchAllGroups.Name = "cbSearchAllGroups";
            this.cbSearchAllGroups.Size = new System.Drawing.Size(136, 24);
            this.cbSearchAllGroups.TabIndex = 36;
            this.cbSearchAllGroups.Text = "Find In All Groups";
            this.cbSearchAllGroups.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cbSearchAllGroups.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // bDelete
            // 
            this.bDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.bDelete.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.bDelete.Location = new System.Drawing.Point(16, 696);
            this.bDelete.Name = "bDelete";
            this.bDelete.Size = new System.Drawing.Size(120, 23);
            this.bDelete.TabIndex = 37;
            this.bDelete.Text = "Delete Tagged";
            this.bDelete.Click += new System.EventHandler(this.bDelete_Click);
            // 
            // readMe1
            // 
            this.readMe1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.readMe1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.readMe1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("readMe1.BackgroundImage")));
            this.readMe1.Location = new System.Drawing.Point(894, 8);
            this.readMe1.Name = "readMe1";
            this.readMe1.Size = new System.Drawing.Size(16, 16);
            this.readMe1.TabIndex = 38;
            // 
            // cbName
            // 
            this.cbName.Checked = true;
            this.cbName.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbName.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.cbName.Location = new System.Drawing.Point(40, 232);
            this.cbName.Name = "cbName";
            this.cbName.Size = new System.Drawing.Size(88, 24);
            this.cbName.TabIndex = 9;
            this.cbName.Text = "Name";
            // 
            // cbSSA
            // 
            this.cbSSA.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.cbSSA.Location = new System.Drawing.Point(176, 232);
            this.cbSSA.Name = "cbSSA";
            this.cbSSA.Size = new System.Drawing.Size(144, 24);
            this.cbSSA.TabIndex = 11;
            this.cbSSA.Text = "Site Street Address";
            // 
            // cbMailA
            // 
            this.cbMailA.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.cbMailA.Location = new System.Drawing.Point(336, 232);
            this.cbMailA.Name = "cbMailA";
            this.cbMailA.Size = new System.Drawing.Size(144, 24);
            this.cbMailA.TabIndex = 13;
            this.cbMailA.Text = "Mail Street Address";
            // 
            // cbLAFN
            // 
            this.cbLAFN.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.cbLAFN.Location = new System.Drawing.Point(504, 232);
            this.cbLAFN.Name = "cbLAFN";
            this.cbLAFN.Size = new System.Drawing.Size(168, 24);
            this.cbLAFN.TabIndex = 15;
            this.cbLAFN.Text = "Listing Agent Full Name";
            // 
            // cbSubDiv
            // 
            this.cbSubDiv.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.cbSubDiv.Location = new System.Drawing.Point(40, 256);
            this.cbSubDiv.Name = "cbSubDiv";
            this.cbSubDiv.Size = new System.Drawing.Size(96, 24);
            this.cbSubDiv.TabIndex = 17;
            this.cbSubDiv.Text = "Subdivision";
            // 
            // cbSiteZip
            // 
            this.cbSiteZip.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.cbSiteZip.Location = new System.Drawing.Point(176, 280);
            this.cbSiteZip.Name = "cbSiteZip";
            this.cbSiteZip.Size = new System.Drawing.Size(80, 24);
            this.cbSiteZip.TabIndex = 10;
            this.cbSiteZip.Text = "Site ZIP";
            // 
            // cbMailZip
            // 
            this.cbMailZip.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.cbMailZip.Location = new System.Drawing.Point(336, 280);
            this.cbMailZip.Name = "cbMailZip";
            this.cbMailZip.Size = new System.Drawing.Size(88, 24);
            this.cbMailZip.TabIndex = 12;
            this.cbMailZip.Text = "Mail ZIP";
            // 
            // cbMLSNr
            // 
            this.cbMLSNr.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.cbMLSNr.Location = new System.Drawing.Point(504, 256);
            this.cbMLSNr.Name = "cbMLSNr";
            this.cbMLSNr.Size = new System.Drawing.Size(80, 24);
            this.cbMLSNr.TabIndex = 14;
            this.cbMLSNr.Text = "MLS No.";
            // 
            // cbTelNr
            // 
            this.cbTelNr.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.cbTelNr.Location = new System.Drawing.Point(40, 280);
            this.cbTelNr.Name = "cbTelNr";
            this.cbTelNr.Size = new System.Drawing.Size(104, 24);
            this.cbTelNr.TabIndex = 16;
            this.cbTelNr.Text = "Phone No.";
            // 
            // tbSearchField
            // 
            this.tbSearchField.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbSearchField.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbSearchField.Location = new System.Drawing.Point(112, 162);
            this.tbSearchField.Name = "tbSearchField";
            this.tbSearchField.Size = new System.Drawing.Size(352, 20);
            this.tbSearchField.TabIndex = 7;
            this.tbSearchField.Enter += new System.EventHandler(this.EnterTextBox);
            this.tbSearchField.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbListingMLSNumber_KeyPress);
            this.tbSearchField.Leave += new System.EventHandler(this.LeaveTextBox);
            // 
            // label15
            // 
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(14, 164);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(80, 23);
            this.label15.TabIndex = 43;
            this.label15.Text = "Find string";
            this.label15.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // bAssignToGroup
            // 
            this.bAssignToGroup.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.bAssignToGroup.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.bAssignToGroup.Location = new System.Drawing.Point(656, 696);
            this.bAssignToGroup.Name = "bAssignToGroup";
            this.bAssignToGroup.Size = new System.Drawing.Size(120, 23);
            this.bAssignToGroup.TabIndex = 44;
            this.bAssignToGroup.Text = "Assign To Group";
            this.bAssignToGroup.Click += new System.EventHandler(this.bAssignToGroup_Click);
            // 
            // cbSiteCity
            // 
            this.cbSiteCity.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.cbSiteCity.Location = new System.Drawing.Point(176, 256);
            this.cbSiteCity.Name = "cbSiteCity";
            this.cbSiteCity.Size = new System.Drawing.Size(72, 24);
            this.cbSiteCity.TabIndex = 45;
            this.cbSiteCity.Text = "Site City";
            // 
            // cbMailCity
            // 
            this.cbMailCity.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.cbMailCity.Location = new System.Drawing.Point(336, 256);
            this.cbMailCity.Name = "cbMailCity";
            this.cbMailCity.Size = new System.Drawing.Size(104, 24);
            this.cbMailCity.TabIndex = 46;
            this.cbMailCity.Text = "Mail City";
            // 
            // cbBarCode
            // 
            this.cbBarCode.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.cbBarCode.Location = new System.Drawing.Point(504, 280);
            this.cbBarCode.Name = "cbBarCode";
            this.cbBarCode.Size = new System.Drawing.Size(88, 24);
            this.cbBarCode.TabIndex = 47;
            this.cbBarCode.Text = "Bar Code";
            // 
            // cbEmail
            // 
            this.cbEmail.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.cbEmail.Location = new System.Drawing.Point(648, 232);
            this.cbEmail.Name = "cbEmail";
            this.cbEmail.Size = new System.Drawing.Size(88, 24);
            this.cbEmail.TabIndex = 48;
            this.cbEmail.Text = "Email Address";
            // 
            // FindPropertyItem
            // 
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.Controls.Add(this.cbEmail);
            this.Controls.Add(this.cbBarCode);
            this.Controls.Add(this.tbSearchedItems);
            this.Controls.Add(this.cbMailCity);
            this.Controls.Add(this.cbSiteCity);
            this.Controls.Add(this.bAssignToGroup);
            this.Controls.Add(this.tbSearchField);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.cbTelNr);
            this.Controls.Add(this.cbMLSNr);
            this.Controls.Add(this.cbMailZip);
            this.Controls.Add(this.cbSiteZip);
            this.Controls.Add(this.cbSubDiv);
            this.Controls.Add(this.cbLAFN);
            this.Controls.Add(this.cbMailA);
            this.Controls.Add(this.cbSSA);
            this.Controls.Add(this.cbName);
            this.Controls.Add(this.readMe1);
            this.Controls.Add(this.bDelete);
            this.Controls.Add(this.cbSearchAllGroups);
            this.Controls.Add(this.bExport);
            this.Controls.Add(this.bLookUpUnTagged);
            this.Controls.Add(this.bLookUpTagged);
            this.Controls.Add(this.bUnTagAll);
            this.Controls.Add(this.bTagAll);
            this.Controls.Add(this.cbSearchSubgroups);
            this.Controls.Add(this.lvResults);
            this.Controls.Add(this.bClearAll);
            this.Controls.Add(this.tbFoundMatching);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.bSearch);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.bRemoveGroup);
            this.Controls.Add(this.bAddGroup);
            this.Controls.Add(this.lvGroups);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.Name = "FindPropertyItem";
            this.Size = new System.Drawing.Size(918, 725);
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		#endregion		
		#region � Methods �
		#region � AppendHashtables �
		private Hashtable AppendHashtables (Hashtable from, Hashtable to)
		{
			int iOffset = to.Count;
			int iCnt = 0;
			IDictionaryEnumerator ide = from.GetEnumerator();
			while (ide.MoveNext())
			{
				to.Add(iOffset + iCnt, ide.Value);
				iCnt++;
			}
			return to;
		}
		#endregion � AppendHashtables �
		
		
		
		#region � bSearch_Click �
		private void bSearch_Click(object sender, System.EventArgs e)
		{				
			if (false == this.cbSearchAllGroups.Checked && this.lvGroups.Items.Count == 0)
			{
				MessageBox.Show(this, "Select group to search!", "Select group", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
				

			this.lvResults.Items.Clear();
			Cursor.Current = Cursors.WaitCursor;

			ArrayList propertyItemList = new ArrayList();			
			
			if (this.cbName.Checked)
				propertyItemList = GetContactsViaFullName(this.tbSearchField.Text);
			else if (this.cbSSA.Checked)
				propertyItemList = GetContactsViaSiteAddress(this.tbSearchField.Text);
			else if (this.cbMailA.Checked)
				propertyItemList = GetContactsViaMailAddress(this.tbSearchField.Text);
			else if (this.cbLAFN.Checked)
				propertyItemList = GetContactsViaListingAgentFullName(this.tbSearchField.Text);
			else if (this.cbSubDiv.Checked)
				propertyItemList = GetContactsViaSubDivision(this.tbSearchField.Text);
			else if (this.cbSiteZip.Checked)
				propertyItemList = GetContactsViaSiteZIP(this.tbSearchField.Text);
			else if (this.cbMailZip.Checked)
				propertyItemList = GetContactsViaMailZIP(this.tbSearchField.Text);
			else if (this.cbMLSNr.Checked)			
				propertyItemList = GetContactsViaListingMLSNumber(this.tbSearchField.Text);
			else if (this.cbTelNr.Checked)
				propertyItemList = GetContactsViaPhoneNo(this.tbSearchField.Text);
			else if (this.cbSiteCity.Checked)
				propertyItemList = GetContactsViaSiteCity(this.tbSearchField.Text);
			else if (this.cbMailCity.Checked)
				propertyItemList = GetContactsViaMailCity(this.tbSearchField.Text);
			else if (this.cbBarCode.Checked)
				propertyItemList = GetContactsViaBarCode(this.tbSearchField.Text);
			else if (this.cbEmail.Checked)
				propertyItemList = GetContactsViaEmail(this.tbSearchField.Text);

			FastPropertyItemDisplay(propertyItemList);

			//this.tbFoundMatching.Text = propertyItemList.Count.ToString();
			this.tbFoundMatching.Text = this.lvResults.Items.Count.ToString();
			Cursor.Current = Cursors.Arrow;
			Globals.oldFindPropertyItem = this;

		}

		
		#endregion � bSearch_Click �		
		#region � bAddGroup_Click �
		private void bAddGroup_Click(object sender, System.EventArgs e)
		{
			SelectGroupForm sgf = new SelectGroupForm();
			sgf.ShowDialog();
			
			ListViewItem lvi = new ListViewItem(sgf.customizedTreeViewUserControl1.SelectedNodeName);
			lvi.Tag = sgf.customizedTreeViewUserControl1.SelectedNodeId;
			lvi.SubItems.Add(sgf.customizedTreeViewUserControl1.SelectedNodeName);

			//TA++: 24.05.2005
			//RQ Nr: RQ 0012
			//Old ++

			/*
				this.lvGroups.Items.Add(lvi);
				this.SetSearchEnabled();
			 * */

			//Old--

			//New++
			if (1 != sgf.customizedTreeViewUserControl1.SelectedNodeId)
			{			
				this.lvGroups.Items.Add(lvi);
				this.SetSearchEnabled();
			}
			else
			{
				MessageBox.Show(this, UserMessages.SearchInAllGroupsDesc, UserMessages.SearchInAllGroupsHeader, MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
			//New--
			Globals.oldFindPropertyItem = this;
		}
		#endregion � bAddGroup_Click �
		#region � bClearAll_Click �
		private void bClearAll_Click(object sender, System.EventArgs e)
		{
			this.lvGroups.Items.Clear();
			this.SetSearchEnabled();
			Globals.oldFindPropertyItem = this;
		}
		#endregion � bClearAll_Click �
		#region � bRemoveGroup_Click �
		private void bRemoveGroup_Click(object sender, System.EventArgs e)
		{
			if (null != this.lvGroups && null != this.lvGroups.SelectedItems)
			{
				if (this.lvGroups.SelectedIndices.Count != 0)
				{
					ListView.SelectedListViewItemCollection selectedItems = this.lvGroups.SelectedItems;
					foreach (ListViewItem lvi in selectedItems)
					{
						this.lvGroups.Items.Remove(lvi);
					}
				}
				else
				{					
					MessageBox.Show(this, "Please, select a group to remove!", "Select group", MessageBoxButtons	.OK, MessageBoxIcon.Information);
				}				
			}
			else
			{
				MessageBox.Show(this, "Please select a group to remove!", "Select group", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
			this.SetSearchEnabled();
			Globals.oldFindPropertyItem = this;
		}
		#endregion � bRemoveGroup_Click �
		#region � SetSearchEnabled �
		private void SetSearchEnabled ()
		{
			if (this.lvGroups.Items.Count > 0)
				this.bSearch.Enabled = true;
			else
			{
				this.bSearch.Enabled = false;
				this.ClearTextFields();
			}
		}
		#endregion � SetSearchEnabled �
		#region � ClearTextFields �
		private void ClearTextFields ()
		{
			this.tbSearchedItems.Text = string.Empty;
			this.tbFoundMatching.Text = string.Empty;
		}
		#endregion � ClearTextFields �
		#region � tbListingMLSNumber_TextChanged �
		private void tbListingMLSNumber_TextChanged(object sender, System.EventArgs e)
		{
			this.ClearTextFields();
		}
		#endregion � tbListingMLSNumber_TextChanged �
		#region � tbListingMLSNumber_KeyPress �
		private void tbListingMLSNumber_KeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
		{
			if (e.KeyChar == 13)
			{
				this.bSearch_Click(sender, null);
			}			
		}
		#endregion � tbListingMLSNumber_KeyPress �
		#region � TagListView �
		private void TagListView (bool ckhValue)
		{
			for (int i = 0; i < this.lvResults.Items.Count; i++)
			{
				ListViewItem lvi = (ListViewItem)this.lvResults.Items[i];
				lvi.Checked = ckhValue;
			}
		}
		#endregion � TagListView �
		#region � bTagAll_Click �
		private void bTagAll_Click(object sender, System.EventArgs e)
		{
			Globals.oldFindPropertyItem = this;
			this.TagListView(true);
		}
		#endregion � bTagAll_Click �
		#region � bUnTagAll_Click �
		private void bUnTagAll_Click(object sender, System.EventArgs e)
		{
			Globals.oldFindPropertyItem = this;
			this.TagListView(false);
		}
		#endregion � bUnTagAll_Click �
		#region � bLookUpTagged_Click �
		private void bLookUpTagged_Click(object sender, System.EventArgs e)
		{
			Globals.oldFindPropertyItem = this;
			int iCnt = 1;
			Hashtable htPropertyItems = new Hashtable();
			for (int i = 0; i < this.lvResults.Items.Count; i++)
			{
				ListViewItem lvi = (ListViewItem)this.lvResults.Items[i];
				if (true == lvi.Checked)
				{
					htPropertyItems.Add(iCnt, (Guid)lvi.Tag);
					iCnt++;
				}
			}
			if (htPropertyItems.Count > 0)
			{
				//sasa, 2006.08.27.: bugfix nw
				Globals.ContactScreenOnMainForm = true;

				if (null != Globals.oldContactScreen)
				{
					//sasa, 20.11.05: hack, aaaghr, we must remove event handler from former contact screen
					Network.NetworkUDP.ChangedPropertyItem -= Globals.oldContactScreen.networkEventHandler;
					//sasa, 24.01.06: new nw lock
					Network.NetworkLock.StatusChanged -=Globals.oldContactScreen.networkEventHandler2;
				}
				//~sasa

				ContactScreen cs = new ContactScreen(htPropertyItems);
				cs.Size = Globals.applicationSize;
				cs.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top; 
				cs.Dock = DockStyle.Fill;
				Panel panel = (Panel)this.Parent;
				MainForm mainForm = (MainForm)panel.Parent;

				mainForm.appBodyPanel.Controls.RemoveAt(0);
				mainForm.appBodyPanel.Controls.Add(cs);				

				//2006.08.27, sasa: nw lock, reload contact and acquire new lock if cs already on screen
				cs.ReloadContact();
			}
		}
		#endregion � bLookUpTagged_Click �
		#region � bLookUpUnTagged_Click �
		private void bLookUpUnTagged_Click(object sender, System.EventArgs e)
		{
			Globals.oldFindPropertyItem = this;
			int iCnt = 1;
			Hashtable htPropertyItems = new Hashtable();
			for (int i = 0; i < this.lvResults.Items.Count; i++)
			{
				ListViewItem lvi = (ListViewItem)this.lvResults.Items[i];
				if (false == lvi.Checked)
				{
					htPropertyItems.Add(iCnt, (Guid)lvi.Tag);
					iCnt++;
				}
			}
			if (htPropertyItems.Count > 0)
			{
				ContactScreen cs = new ContactScreen(htPropertyItems);
				cs.Size = Globals.applicationSize;
				cs.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top; 
				cs.Dock = DockStyle.Fill;
				Panel panel = (Panel)this.Parent;
				MainForm mainForm = (MainForm)panel.Parent;

				mainForm.appBodyPanel.Controls.RemoveAt(0);
				mainForm.appBodyPanel.Controls.Add(cs);				
			}
		}
		#endregion � bLookUpUnTagged_Click �
		#region � bExport_Click �
		private void bExport_Click(object sender, System.EventArgs e)
		{
			ArrayList alCheckedPropertyItems = new ArrayList();			
			
			//get count of checked ListViewItems
			int iCnt = 0;
			foreach (ListViewItem lvi in this.lvResults.Items)
			{
				if (true == lvi.Checked)
					iCnt++;
			}

			if (0 == iCnt)
			{
				MessageBox.Show(this, "Please tag a contact you want to assign to a group!", "Select contacts!", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}

			//Event trigger
			EventTrigger et = new EventTrigger();
			PBForm pbf = new PBForm(et);
			pbf.Capacity = iCnt;
			pbf.WindowName = "Loading Data...";			
			pbf.Show();
			
			foreach (ListViewItem lvi in this.lvResults.Items)
			{
				if (true == lvi.Checked)
				{
					Guid propertyItemId = (Guid)lvi.Tag;
					PropertyItem pi = PropertyItem.LoadPropertyItem(propertyItemId);
					alCheckedPropertyItems.Add(pi);

					//fire event
					et.TriggerChange();
				}
			}
			PropertyItemsExportWizard exportWizard = new PropertyItemsExportWizard((PropertyItem[])alCheckedPropertyItems.ToArray(typeof(PropertyItem)));
			exportWizard.Execute();
		}
		#endregion � bExport_Click �
		#region � EnterTextBox �
		private void EnterTextBox(object sender, System.EventArgs e)
		{			
			TextBox tb = (TextBox)sender;
			tb.BackColor = Color.LightBlue;			
		}
		#endregion � EnterTextBox �
		#region � LeaveTextBox �
		private void LeaveTextBox(object sender, System.EventArgs e)
		{
			TextBox tb = (TextBox)sender;			
			tb.BackColor = Color.White;					
		}
		#endregion � LeaveTextBox �
		#region � cbSearchAllGroups_CheckedChanged �
		private void checkBox1_CheckedChanged(object sender, System.EventArgs e)
		{
			this.EnableGroups(!this.cbSearchAllGroups.Checked);			
		}
		#endregion � cbSearchAllGroups_CheckedChanged �
		#region � EnableGroups �
		private void EnableGroups(bool enable)
		{
			this.bAddGroup.Enabled = enable;
			this.bRemoveGroup.Enabled = enable;
			this.bClearAll.Enabled = enable;
			this.cbSearchSubgroups.Enabled = enable;
			this.bSearch.Enabled = !enable;
			if (this.lvGroups.Items.Count > 0)
			{
				this.bSearch.Enabled = true;
			}
		}
		#endregion � EnableGroups �
		#region � lvResults_DoubleClick �
		private void lvResults_DoubleClick(object sender, System.EventArgs e)
		{
			Globals.oldFindPropertyItem = this;
			int iCnt = 1;
			ListView lv = (ListView)sender;
			ListView.SelectedIndexCollection selectedIndices = lv.SelectedIndices;
			ListViewItem lvi = lv.Items[selectedIndices[0]];

			Hashtable htPropertyItems = new Hashtable();
			htPropertyItems.Add(iCnt, lvi.Tag);

			if (htPropertyItems.Count > 0)
			{				
				//sasa, 2006.08.27.: bugfix nw
				Globals.ContactScreenOnMainForm = true;

				if (null != Globals.oldContactScreen)
				{
					//sasa, 20.11.05: hack, aaaghr, we must remove event handler from former contact screen
					Network.NetworkUDP.ChangedPropertyItem -= Globals.oldContactScreen.networkEventHandler;
					//sasa, 24.01.06: new nw lock
					Network.NetworkLock.StatusChanged -=Globals.oldContactScreen.networkEventHandler2;
				}
				//~sasa

				ContactScreen cs = new ContactScreen(htPropertyItems);
				cs.Size = Globals.applicationSize;
				cs.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top; 
				cs.Dock = DockStyle.Fill;

				Panel panel = (Panel)this.Parent;
				MainForm mainForm = (MainForm)panel.Parent;

				mainForm.appBodyPanel.Controls.RemoveAt(0);
				mainForm.appBodyPanel.Controls.Add(cs);				

				//2006.08.27, sasa: nw lock, reload contact and acquire new lock if cs already on screen
				cs.ReloadContact();
			}
		}
		#endregion � lvResults_DoubleClick �		
		#region � bDelete_Click �
		private void bDelete_Click(object sender, System.EventArgs e)
		{
			Globals.oldFindPropertyItem = this;			
			ArrayList alPropertyItemsId = new ArrayList();
			for (int i = 0; i < this.lvResults.Items.Count; i++)
			{
				ListViewItem lvi = (ListViewItem)this.lvResults.Items[i];
				if (true == lvi.Checked)
				{
					alPropertyItemsId.Add((Guid)lvi.Tag);					
				}
			}
			if (alPropertyItemsId.Count > 0)
			{
				//remove item
				bool updateInputData = false;
				ApplicationHelper.DeleteTaggedPropertyItems(alPropertyItemsId, out updateInputData);
				if (true == updateInputData)
				{
					this.bSearch_Click(sender, e);
				}				
			}
		}
		#endregion � bDelete_Click �

		private SortedList CreateFilter()
		{
			SortedList slPropertyItemIds = new SortedList();
			for(int i = 0; i < this.lvGroups.Items.Count; i++)
			{
				int groupId = (int)this.lvGroups.Items[i].Tag;
				SortedList alCurrentGroupResults = this.CreateGroupFilter(groupId);
				IDictionaryEnumerator ide = alCurrentGroupResults.GetEnumerator();
				while (ide.MoveNext())
				{
					if (false == slPropertyItemIds.Contains(ide.Key))
						slPropertyItemIds.Add(ide.Key, null);
				}
			}
			return slPropertyItemIds;
		}
		
		private SortedList CreateGroupFilter(int nodeId)
		{
			int searchSubgroups = 1;
			int doNotSearchSubgroups = 0;

			ApplicationHelper.TestDatabase_CreateSPL_CreatePropertyItemFilter();

			SqlCommand sp_CreatePropertyItemFilter = new SqlCommand("CreatePropertyItemFilter", Connection.getInstance().HatchConn);
			sp_CreatePropertyItemFilter.CommandType = CommandType.StoredProcedure;								
			SqlDataReader reader = null;
			SortedList piList = new SortedList();
			
			try
			{
				sp_CreatePropertyItemFilter.Parameters.Add("@NodeId", nodeId);
						
				if (true == this.cbSearchSubgroups.Checked)
					sp_CreatePropertyItemFilter.Parameters.Add("@SubGroupsSearch", (int)searchSubgroups);	
				else
					sp_CreatePropertyItemFilter.Parameters.Add("@SubGroupsSearch", (int)doNotSearchSubgroups);				
			
				reader = sp_CreatePropertyItemFilter.ExecuteReader();

				piList = new SortedList();
				while (reader.Read())
				{
					Guid propertyItemId = (Guid)reader.GetGuid(0);
					if (false == piList.Contains(propertyItemId))
						piList.Add(propertyItemId, null);
				}
			}
			catch(Exception exc)
			{
				MessageBox.Show(exc.ToString() + "\n" + exc.InnerException);
			}
			finally
			{
				reader.Close();
			}
			return piList;
		}
		#endregion � Methods �

		#region * New Implementation *
		private void cb_CheckedChanged(object sender, EventArgs e)
		{
			if (sender is CheckBox)
			{
				CheckBox cb = sender as CheckBox;
				if (null != cb)
				{									
					this.EnableAllTB((int)cb.Tag);				
				}
			}
			
		}
		private void EnableAllTB(int iTbId)
		{						
						
			if (iTbId == 1)			
			{
				//this.tbName.Enabled = true;			
			}
			else
			{
				//this.tbName.Enabled = false;
				this.cbName.Checked = false;
			}
			
			if (iTbId == 2)
			{
				//this.tbSiteStreetAddress.Enabled = true;
			}
			else
			{				
				//this.tbSiteStreetAddress.Enabled = false;
				this.cbSSA.Checked = false;
			}

			if (iTbId == 3)
			{
				//this.tbMailingAddress.Enabled = true;		
			}
			else
			{
				//this.tbMailingAddress.Enabled = false;		
				this.cbMailA.Checked = false;
			}

			if (iTbId == 4)
			{
				//this.tbListingAgentFullName.Enabled = true;
			}
			else
			{
				//this.tbListingAgentFullName.Enabled = false;
				this.cbLAFN.Checked = false;
			}

			if (iTbId == 5)
			{
				//this.tbSubDivision.Enabled = true;
			}
			else
			{
				//this.tbSubDivision.Enabled = false;
				this.cbSubDiv.Checked = false;
			}

			if (iTbId == 6)
			{
				//this.tbSiteZIP.Enabled = true;
			}
			else
			{
				//this.tbSiteZIP.Enabled = false;
				this.cbSiteZip.Checked = false;
			}

			if (iTbId == 7)
			{
				//this.tbMailZIP.Enabled = true;			
			}
			else
			{
				//this.tbMailZIP.Enabled = false;			
				this.cbMailZip.Checked = false;
			}

			if (iTbId == 8)
			{
				//this.tbListingMLSNumber.Enabled = true;
			}
			else
			{
				//this.tbListingMLSNumber.Enabled = false;
				this.cbMLSNr.Checked = false;
			}

			if (iTbId == 9)
			{
				//this.tb_Tel_Nr.Enabled = true;
			}
			else
			{
				//this.tb_Tel_Nr.Enabled = false;
				this.cbTelNr.Checked = false;
			}

			if (iTbId == 10)
			{
				//this.tb_Tel_Nr.Enabled = true;
			}
			else
			{
				//this.tb_Tel_Nr.Enabled = false;
				this.cbSiteCity.Checked = false;
			}

			if (iTbId == 11)
			{
				//this.tb_Tel_Nr.Enabled = true;
			}
			else
			{
				//this.tb_Tel_Nr.Enabled = false;
				this.cbMailCity.Checked = false;
			}

			if (iTbId == 12)
			{
				//
			}
			else
			{
				this.cbBarCode.Checked = false;
			}

			if (iTbId == 13)
			{
				//
			}
			else
			{
				this.cbEmail.Checked = false;
			}
		}
		#endregion * New Implementation *

		#region � GetContactsViaFullName �
		private ArrayList GetContactsViaFullName(string fullName)
		{
			ArrayList alPropertyItems = new ArrayList();
			try
			{																
				session = Globals.factory.OpenSession();
				IList contactList = (IList)session.CreateCriteria(typeof(Contact)).
					//Add(Expression.Eq("FullName", fullName)).
					Add(Expression.Sql("FullName LIKE '%" + (string)fullName + "%'")).
					List();
					
				if(contactList != null && contactList.Count > 0)
				{
					
					if (contactList.Count > 25)
					{
						if (DialogResult.Yes == MessageBox.Show(this, "Search found " + contactList.Count.ToString() + " records." + Environment.NewLine + "Would you like to narrow your search?", "Narrow Search" , MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2))
							return new ArrayList();
					}
					foreach (Contact contact in contactList)
					{
						IList propertyItemList = (IList)session.CreateCriteria(typeof(PropertyItem)).
							Add(Expression.Eq("Owner", contact)).
							List();

						foreach (PropertyItem pi in	propertyItemList)
						{
							alPropertyItems.Add(pi);
						}
					}
				}
			}
			catch (Exception 
#if DEBUG
				exc
#endif 
				)
			{
#if DEBUG
				MessageBox.Show(exc.ToString());
#endif 
			}
			return alPropertyItems;
		}
		#endregion � GetContactsViaFullName �

		#region � GetContactsViaSiteAddress �
		private ArrayList GetContactsViaSiteAddress(string siteStreetAddress)
		{
			ArrayList alPropertyItems = new ArrayList();			
			IList addressList = null;
			IList contactList = null;
			IList totalContactList = new ArrayList();
			NHibernate.ISession session = null;
			
			try
			{									
				session = Globals.factory.OpenSession();

				addressList = (IList)session.CreateCriteria(typeof(Address)).				
					//Add(Expression.Like("FullSiteStreetAddress",(string)siteStreetAddress)).
					Add(Expression.Sql("FullSiteStreetAddress LIKE '%" + (string)siteStreetAddress + "%'")).
					List();
				
			
				if(addressList != null && addressList.Count > 0)
				{					
					foreach (Address addr in addressList)
					{					
						contactList = (IList)session.CreateCriteria(typeof(Contact)).
							Add(Expression.Eq("SiteAddress", addr)).
							List();

						foreach (Contact contact in contactList)
						{
							totalContactList.Add(contact);
						}											
					}

					if(totalContactList != null && totalContactList.Count > 0)
					{
							
						/*if (totalContactList.Count > 25)
						{
							if (DialogResult.Yes == MessageBox.Show(this, "Search found " + totalContactList.Count.ToString() + " records.", "Would you like to narrow your search?", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2))
								return new ArrayList();
						}*/
						
						foreach (Contact contact in totalContactList)
						{
							IList propertyItemList = (IList)session.CreateCriteria(typeof(PropertyItem)).
								Add(Expression.Eq("Owner", contact)).
								List();

							foreach (PropertyItem pi in	propertyItemList)
							{								
								alPropertyItems.Add(pi);								
							}
						}
					}

				}				
			}
			catch (Exception 
#if DEBUG
				exc
#endif 
				)
			{
#if DEBUG
				MessageBox.Show(exc.ToString());
#endif 
			}
			return alPropertyItems;
		}
		#endregion � GetContactsViaSiteAddress �

		#region � GetContactsViaMailAddress �
		private ArrayList GetContactsViaMailAddress(string mailAddress)
		{			
			ArrayList alPropertyItems = new ArrayList();			
			IList addressList = null;
			IList totalContactList = new ArrayList();
			NHibernate.ISession session = null;
			
			try
			{									
				session = Globals.factory.OpenSession();
				addressList = (IList)session.CreateCriteria(typeof(Address)).				
					//Add(Expression.Like("FullSiteStreetAddress",(string)siteStreetAddress)).
					Add(Expression.Sql("FullSiteStreetAddress LIKE '%" + (string)mailAddress + "%'")).
					List();
				
				
				if(addressList != null && addressList.Count > 0)
				{					
					foreach (Address addr in addressList)
					{							
						IList contactList = (IList)session.CreateCriteria(typeof(Contact)).
							Add(Expression.Eq("MailAddress", addr)).
							List();
					
						if(contactList != null && contactList.Count > 0)
						{
							
							foreach (Contact contact in contactList)
							{
								totalContactList.Add(contact);
							}
						}
					}
						
						/*
						if (totalContactList.Count > 25)
						{
							if (DialogResult.Yes == MessageBox.Show(this, "Search found " + totalContactList.Count.ToString() + " records.", "Would you like to narrow your search?", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2))
								return new ArrayList();
						}*/
					foreach (Contact contact in totalContactList)
					{						
						IList propertyItemList = (IList)session.CreateCriteria(typeof(PropertyItem)).
							Add(Expression.Eq("Owner", contact)).
							List();						

						foreach (PropertyItem pi in	propertyItemList)
						{														
							alPropertyItems.Add(pi);							
						}
					}
																				
				}				
			}
			catch (Exception 
#if DEBUG
				exc
#endif 
				)
			{
#if DEBUG
				MessageBox.Show(exc.ToString());
#endif 
			}
			return alPropertyItems;
		}
		#endregion � GetContactsViaMailAddress �

		#region � GetContactsViaSiteZIP �
		private ArrayList GetContactsViaSiteZIP(string siteZIP)
		{
			ArrayList alPropertyItems = new ArrayList();			
			IList addressList = null;
			IList totalContactList = new ArrayList();
			NHibernate.ISession session = null;
			
			try
			{												

				session = Globals.factory.OpenSession();

				addressList = (IList)session.CreateCriteria(typeof(Address)).				
					//Add(Expression.Eq("FullSiteStreetAddress",(string)siteZIP)).
					Add(Expression.Sql("SiteZIP LIKE '" + (string)siteZIP + "%'")).
					List();
				
			
				if(addressList != null && addressList.Count > 0)
				{					
					foreach (Address addr in addressList)
					{					
						IList contactList = (IList)session.CreateCriteria(typeof(Contact)).
							Add(Expression.Eq("SiteAddress", addr)).
							List();
					
						if(contactList != null && contactList.Count > 0)
						{
							foreach (Contact contact in contactList)
							{
								totalContactList.Add(contact);
							}							
						}
					}

					if (totalContactList.Count > 25)
					{
						if (DialogResult.Yes == MessageBox.Show(this, "Search found " + totalContactList.Count.ToString() + " records." + Environment.NewLine + "Would you like to narrow your search?", "Narrow Search",  MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2))
							return new ArrayList();
					}

					foreach (Contact contact in totalContactList)
					{
						IList propertyItemList = (IList)session.CreateCriteria(typeof(PropertyItem)).
							Add(Expression.Eq("Owner", contact)).
							List();

						foreach (PropertyItem pi in	propertyItemList)
						{
							alPropertyItems.Add(pi);
						}
					}
				}				
			}
			catch (Exception 
#if DEBUG
				exc
#endif 
				)
			{
#if DEBUG
				MessageBox.Show(exc.ToString());
#endif 
			}
			return alPropertyItems;
		}
		#endregion � GetContactsViaSiteZIP �

		#region � GetContactsViaSiteCity �
		private ArrayList GetContactsViaSiteCity(string siteCity)
		{
			ArrayList alPropertyItems = new ArrayList();			
			IList addressList = null;
			IList totalContactList = new ArrayList();
			NHibernate.ISession session = null;
			
			try
			{												

				session = Globals.factory.OpenSession();

				addressList = (IList)session.CreateCriteria(typeof(Address)).				
					//Add(Expression.Eq("FullSiteStreetAddress",(string)siteZIP)).
					Add(Expression.Sql("SiteCity LIKE '" + (string)siteCity + "%'")).
					List();
				
			
				if(addressList != null && addressList.Count > 0)
				{					
					foreach (Address addr in addressList)
					{					
						IList contactList = (IList)session.CreateCriteria(typeof(Contact)).
							Add(Expression.Eq("SiteAddress", addr)).
							List();
					
						if(contactList != null && contactList.Count > 0)
						{
							foreach (Contact contact in contactList)
							{
								totalContactList.Add(contact);
							}							
						}
					}

					if (totalContactList.Count > 25)
					{
						if (DialogResult.Yes == MessageBox.Show(this, "Search found " + totalContactList.Count.ToString() + " records." + Environment.NewLine + "Would you like to narrow your search?", "Narrow Search", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2))
							return new ArrayList();
					}

					foreach (Contact contact in totalContactList)
					{
						IList propertyItemList = (IList)session.CreateCriteria(typeof(PropertyItem)).
							Add(Expression.Eq("Owner", contact)).
							List();

						foreach (PropertyItem pi in	propertyItemList)
						{
							alPropertyItems.Add(pi);
						}
					}
				}				
			}
			catch (Exception 
#if DEBUG
				exc
#endif 
				)
			{
#if DEBUG
				MessageBox.Show(exc.ToString());
#endif 
			}
			return alPropertyItems;
		}
		#endregion � GetContactsViaSiteCity �

		#region � GetContactsViaMailZIP �
		private ArrayList GetContactsViaMailZIP(string mailZIP)
		{
			ArrayList alPropertyItems = new ArrayList();			
			IList addressList = null;
			IList totalContactList = new ArrayList();
			NHibernate.ISession session = null;
			
			try
			{												
				session = Globals.factory.OpenSession();

				addressList = (IList)session.CreateCriteria(typeof(Address)).				
					//Add(Expression.Like("FullSiteStreetAddress",(string)siteStreetAddress)).
					Add(Expression.Sql("SiteZIP LIKE '" + (string)mailZIP + "%'")).
					List();
				
			
				if(addressList != null && addressList.Count > 0)
				{					
					foreach (Address addr in addressList)
					{					
						IList contactList = (IList)session.CreateCriteria(typeof(Contact)).
							Add(Expression.Eq("MailAddress", addr)).
							List();
					
						if(contactList != null && contactList.Count > 0)
						{
							
							foreach (Contact contact in contactList)
							{
								totalContactList.Add(contact);			
							}
						}
					}

					if (totalContactList.Count > 25)
					{
						if (DialogResult.Yes == MessageBox.Show(this, "Search found " + totalContactList.Count.ToString() + " records." + Environment.NewLine + "Would you like to narrow your search?", "Narrow Search", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2))
							return new ArrayList();
					}

					foreach(Contact contact in totalContactList)
					{
						IList propertyItemList = (IList)session.CreateCriteria(typeof(PropertyItem)).
							Add(Expression.Eq("Owner", contact)).
							List();

						foreach (PropertyItem pi in	propertyItemList)
						{
							alPropertyItems.Add(pi);
						}
					}
				}				
			}
			catch (Exception 
#if DEBUG
				exc
#endif 
				)
			{
#if DEBUG
				MessageBox.Show(exc.ToString());
#endif 
			}
			return alPropertyItems;
		}
		#endregion � GetContactsViaMailZIP �

		#region � GetContactsViaMailCity �
		private ArrayList GetContactsViaMailCity(string mailCity)
		{
			ArrayList alPropertyItems = new ArrayList();			
			IList addressList = null;
			IList totalContactList = new ArrayList();
			NHibernate.ISession session = null;
			
			try
			{												

				session = Globals.factory.OpenSession();

				addressList = (IList)session.CreateCriteria(typeof(Address)).				
					//Add(Expression.Eq("FullSiteStreetAddress",(string)siteZIP)).
					Add(Expression.Sql("SiteCity LIKE '" + (string)mailCity + "%'")).
					List();
				
			
				if(addressList != null && addressList.Count > 0)
				{					
					foreach (Address addr in addressList)
					{					
						IList contactList = (IList)session.CreateCriteria(typeof(Contact)).
							Add(Expression.Eq("MailAddress", addr)).
							List();
					
						if(contactList != null && contactList.Count > 0)
						{
							foreach (Contact contact in contactList)
							{
								totalContactList.Add(contact);
							}							
						}
					}

					if (totalContactList.Count > 25)
					{
						if (DialogResult.Yes == MessageBox.Show(this, "Search found " + totalContactList.Count.ToString() + " records." + Environment.NewLine + "Would you like to narrow your search?", "Narrow Search", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2))
							return new ArrayList();
					}

					foreach (Contact contact in totalContactList)
					{
						IList propertyItemList = (IList)session.CreateCriteria(typeof(PropertyItem)).
							Add(Expression.Eq("Owner", contact)).
							List();

						foreach (PropertyItem pi in	propertyItemList)
						{
							alPropertyItems.Add(pi);
						}
					}
				}				
			}
			catch (Exception 
#if DEBUG
				exc
#endif 
				)
			{
#if DEBUG
				MessageBox.Show(exc.ToString());
#endif 
			}
			return alPropertyItems;
		}
		#endregion � GetContactsViaMailCity �

		#region � GetContactsViaListingAgentFullName �
		private ArrayList GetContactsViaListingAgentFullName(string lafn)
		{
			ArrayList alPropertyItems = new ArrayList();			
			IList listingAgentList = null;
			IList totalContactList = new ArrayList();
			NHibernate.ISession session = null;
			
			try
			{												
				session = Globals.factory.OpenSession();

				listingAgentList = (IList)session.CreateCriteria(typeof(ListingAgent)).				
					//Add(Expression.Like("FullSiteStreetAddress",(string)siteStreetAddress)).
					Add(Expression.Sql("ListingAgentFullName LIKE '" + (string)lafn + "%'")).
					List();
				
			
				if(listingAgentList != null && listingAgentList.Count > 0)
				{					
					if (listingAgentList.Count > 25)
					{
						if (DialogResult.Yes == MessageBox.Show(this, "Search found " + totalContactList.Count.ToString() + " records." + Environment.NewLine +  "Would you like to narrow your search?", "Narrow Search", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2))
							return new ArrayList();
					}					
					foreach (ListingAgent la in listingAgentList)
					{					
						IList propertyItemList = (IList)session.CreateCriteria(typeof(PropertyItem)).
							Add(Expression.Eq("ListingAgent", la)).
							List();

						foreach (PropertyItem pi in	propertyItemList)
						{
							alPropertyItems.Add(pi);
						}
					}					
				}				
			}
			catch (Exception 
#if DEBUG
				exc
#endif 
				)
			{
#if DEBUG
				MessageBox.Show(exc.ToString());
#endif 
			}
			return alPropertyItems;
		}
		#endregion � GetContactsViaListingAgentFullName �	

		#region � GetContactsViaListingMLSNumber �
		private ArrayList GetContactsViaListingMLSNumber(string lamn)
		{
			ArrayList alPropertyItems = new ArrayList();			
			IList listingAgentList = null;
			IList totalContactList = new ArrayList();
			NHibernate.ISession session = null;
			
			try
			{												
				session = Globals.factory.OpenSession();

				listingAgentList = (IList)session.CreateCriteria(typeof(ListingAgent)).				
					//Add(Expression.Like("FullSiteStreetAddress",(string)siteStreetAddress)).
					Add(Expression.Sql("ListingAgentMLSNumber LIKE '" + (string)lamn + "%'")).
					List();
				
			
				if(listingAgentList != null && listingAgentList.Count > 0)
				{					
					if (listingAgentList.Count > 25)
					{
						if (DialogResult.Yes == MessageBox.Show(this, "Search found " + totalContactList.Count.ToString() + " records." + Environment.NewLine + "Would you like to narrow your search?", "Narrow Search", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2))
							return new ArrayList();
					}					
					foreach (ListingAgent la in listingAgentList)
					{					
						IList propertyItemList = (IList)session.CreateCriteria(typeof(PropertyItem)).
							Add(Expression.Eq("ListingAgent", la)).
							List();

						foreach (PropertyItem pi in	propertyItemList)
						{
							alPropertyItems.Add(pi);
						}
					}					
				}				
			}
			catch (Exception 
#if DEBUG
				exc
#endif 
				)
			{
#if DEBUG
				MessageBox.Show(exc.ToString());
#endif 
			}
			return alPropertyItems;
		}
		#endregion � GetContactsViaListingMLSNumber �	

		private ArrayList GetContactsViaEmail(string text)
		{
			var recordsFound = new ArrayList();
			
			using (var session = Globals.factory.OpenSession())
			{
				List<Contact> contactsFound = new List<Contact>();
				
				var contactsMatchingEmail = session.CreateCriteria(typeof(Contact))
					//.Add(Expression.Eq("EMail", text))
					.Add(Expression.Sql($"EMail LIKE '%{text}%'"))
					.List();

				var cmeenum = contactsMatchingEmail.GetEnumerator();
				while (cmeenum.MoveNext())
					contactsFound.Add(cmeenum.Current as Contact);
								
				var contactsMatchingSTEmail = session.CreateCriteria(typeof(Contact))
					//.Add(Expression.Eq("STEmail", text))
					.Add(Expression.Sql($"STEmail LIKE '%{text}%'"))
					.List();

				
				var cmsenum = contactsMatchingSTEmail.GetEnumerator();
				while (cmsenum.MoveNext())
					contactsFound.Add(cmsenum.Current as Contact);

				contactsFound = contactsFound.Distinct().ToList();

				foreach (var cnt in contactsFound)
				{
					IList propertyItemList = (IList)session.CreateCriteria(typeof(PropertyItem)).
						Add(Expression.Eq("Owner", cnt)).
						List();

					foreach (var pi in propertyItemList)
						recordsFound.Add(pi as PropertyItem);
				}
			}

			return recordsFound;
		}

		#region � GetContactsViaPhoneNo �

		private string GetCompactPhoneNo (string phoneNo)
		{
			if (string.IsNullOrEmpty(phoneNo))
				return string.Empty;

			string compactPhNo = string.Empty;
			char[] chPhone = phoneNo.ToCharArray(0, phoneNo.Length);
			foreach(char c in chPhone)
			{
				if (Char.IsDigit(c))
				{
					compactPhNo += c.ToString();
				}
			}
			return compactPhNo;
		}
		
		private ArrayList GetContactsViaPhoneNo(string phNo)
		{
			ArrayList alPropertyItems = new ArrayList();			
			IList phoneNoList = null;
			IList alCompactPhoneNr = new ArrayList();
			IList totalContactList = new ArrayList();
			NHibernate.ISession session = null;
			
			try
			{												
				string searchedPhNo = this.GetCompactPhoneNo(phNo);
				session = Globals.factory.OpenSession();
				phoneNoList = (IList)session.CreateCriteria(typeof(Phone)).List();				
				if(phoneNoList != null && phoneNoList.Count > 0)
				{					
					foreach (Phone ph in phoneNoList)
					{
						if (string.Empty != ph.PhoneNr)						
						{
							string compactPhoneNo = this.GetCompactPhoneNo(ph.PhoneNr); 
							//if (searchedPhNo.Equals(compactPhoneNo))
							
							if (-1 != compactPhoneNo.IndexOf(searchedPhNo))
							{						
								//Phone tempPhone = new Phone();
								//tempPhone.IdPhone = ph.IdPhone;
								//tempPhone.PhoneNr = compactPhoneNo;
								alCompactPhoneNr.Add(ph);
							}
						}											
					}
					
					//if (alCompactPhoneNr.Count > 25)
					//{
					//	if (DialogResult.Yes == MessageBox.Show(this, "Search found " + totalContactList.Count.ToString() + " records." + Environment.NewLine + "Would you like to narrow your search?", "Narrow Search", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2))
					//		return new ArrayList();
					//}
					foreach (Phone phone in alCompactPhoneNr)
					{
						//Home Phone
						IList contactList_Home = (IList)session.CreateCriteria(typeof(Contact))
							.Add(Expression.Eq("HomePhone", phone))							
							.List();

						//Work Phone
						IList contactList_Work = (IList)session.CreateCriteria(typeof(Contact))
							.Add(Expression.Eq("WorkPhone", phone))							
							.List();
						
						//Cell Phone
						IList contactList_Cell = (IList)session.CreateCriteria(typeof(Contact))
							.Add(Expression.Eq("CellPhone", phone))							
							.List();												

						IList contactList = new ArrayList();
						foreach (Contact ctn in contactList_Home)
						{
							contactList.Add(ctn);
						}

						foreach (Contact ctn in contactList_Work)
						{
							contactList.Add(ctn);
						}

						foreach (Contact ctn in contactList_Cell)
						{
							contactList.Add(ctn);
						}

						if (null != contactList && contactList.Count > 0)
						{
							foreach (Contact contact in contactList)
							{
								IList propertyItemList = (IList)session.CreateCriteria(typeof(PropertyItem)).
									Add(Expression.Eq("Owner", contact)).
									List();

								foreach (PropertyItem pi in	propertyItemList)
								{
									if (false == alPropertyItems.Contains(pi))
									{
										alPropertyItems.Add(pi);
									}
								}
							}
						}
					}
				}				
			}
			catch (Exception 
#if DEBUG
				exc
#endif 
				)
			{
#if DEBUG
				MessageBox.Show(exc.ToString());
#endif 
			}
			//Append Contacts that have searched sequence in Fax No.
			ArrayList alPropertyItems_FaxNo = this.GetContactsViaFaxNo(phNo);						

			Hashtable htId = new Hashtable();
			foreach (PropertyItem pi in alPropertyItems)
			{
				htId.Add(pi.IdPropertyItem, null);
			}

			foreach (PropertyItem pi in alPropertyItems_FaxNo)
			{
				if (false == htId.Contains(pi.IdPropertyItem))
				{
					alPropertyItems.Add(pi);
				}
			}

			var foundSkipTracedRecords = SearchContactForSkipTracedPhone(phNo);
			foreach (var pi in foundSkipTracedRecords)
			{
				if (!htId.Contains(pi.IdPropertyItem))
					alPropertyItems.Add(pi);
			}

			return alPropertyItems;
		}

		private List<PropertyItem> SearchContactForSkipTracedPhone(string phoneNr)
		{
			var recordsFound = new List<PropertyItem>();

			phoneNr = GetCompactPhoneNo(phoneNr);
			using (var session = Globals.factory.OpenSession())
			{
				IEnumerable stPhoneContacts = (IList)session.CreateCriteria(typeof(Contact))
					.Add(Expression.Sql("(STCellphone IS NOT NULL and LEN(STCellphone) > 0) OR (STLandLine IS NOT NULL and LEN(STLandLine) > 0)"))
					.List();

				//IEnumerable stLandLineContacts = (IList)session.CreateCriteria(typeof(Contact))
				//	.Add(Expression.Sql("STLandLine IS NOT NULL and LEN(STCellphone) > 0"))
				//	.List();

				//matching contacts with skiptraced cellphone
				//IEnumerable stCellPhoneContacts = (IList)session.CreateCriteria(typeof(Contact))
				//	.Add(Expression.Eq("STCellphone", phoneNr))
				//	.List();

				//matching contacts with skiptraced landline
				//IEnumerable stLandLineContacts = (IList)session.CreateCriteria(typeof(Contact))
				//	.Add(Expression.Eq("STLandline", phoneNr))
				//	.List();

				var contacts = new List<Contact>();

				var penum = stPhoneContacts.GetEnumerator();
				while (penum.MoveNext())
				{
					var contact = penum.Current as Contact;
					var stCellPhone = GetCompactPhoneNo(contact?.STCellphone);
					var stLandLine = GetCompactPhoneNo(contact?.STLandline);

					if (stCellPhone.Contains(phoneNr) || stLandLine.Contains(phoneNr))					
						contacts.Add(penum.Current as Contact);
				}
					

				//var llenum = stLandLineContacts.GetEnumerator();
				//while (llenum.MoveNext())
				//	contacts.Add(llenum.Current as Contact);

				var distinctContacts = contacts.Distinct();

				foreach (var cnt in distinctContacts)
				{
					IList propertyItemList = (IList)session.CreateCriteria(typeof(PropertyItem)).
						Add(Expression.Eq("Owner", cnt)).
						List();

					foreach (var pi in propertyItemList)
						recordsFound.Add(pi as PropertyItem);
					
				}

			}
			return recordsFound.Distinct().ToList();
		}

		private ArrayList GetContactsViaFaxNo(string phNo)
		{
			NHibernate.ISession session = null;
			ArrayList alPropertyItems = new ArrayList();
			ArrayList alContactList = new ArrayList();
			
			try
			{
				session = Globals.factory.OpenSession();
			
				string searchedPhNo = this.GetCompactPhoneNo(phNo);
				IList contactList = (IList)session.CreateCriteria(typeof(Contact))
									.Add(Expression.Not(Expression.Eq("FaxNumber", string.Empty)))
									.List();
	
				if(contactList != null && contactList.Count > 0)
				{					
					foreach (Contact ctn in contactList)
					{
						if (string.Empty != ctn.FaxNumber)						
						{
							string compactFaxNo = this.GetCompactPhoneNo(ctn.FaxNumber); 						
							
							if (-1 != compactFaxNo.IndexOf(searchedPhNo))
							{													
								alContactList.Add(ctn);
							}
						}											
					}
				}

				foreach (Contact contact in alContactList)
				{
					IList propertyItemList = (IList)session.CreateCriteria(typeof(PropertyItem)).
						Add(Expression.Eq("Owner", contact)).
						List();

					foreach (PropertyItem pi in	propertyItemList)
					{
						if (false == alPropertyItems.Contains(pi))
						{
							alPropertyItems.Add(pi);
						}
					}
				}
			}
			catch (Exception 
#if DEBUG
				exc
#endif 
				)
			{
#if DEBUG
				MessageBox.Show(exc.ToString());
#endif 
			}
			return alPropertyItems;
		}
		#endregion � GetContactsViaPhoneNo �	

		#region � GetContactsViaSubDivision �						
		private ArrayList GetContactsViaSubDivision(string subDiv)
		{
			ArrayList alPropertyItems = new ArrayList();			
			IList alSubDiv = null;
			IList totalDealInfo = new ArrayList();
			IList totalContactList = new ArrayList();
			NHibernate.ISession session = null;
			
			try
			{																
				session = Globals.factory.OpenSession();
				alSubDiv = (IList)session.CreateCriteria(typeof(DistrictProperties))
					.Add(Expression.Sql("Subdivision LIKE '" + (string)subDiv + "%'"))
					.List();							
				if(alSubDiv != null && alSubDiv.Count > 0)
				{					
					if (null != alSubDiv && alSubDiv.Count > 25)
					{
						 if (DialogResult.Yes == MessageBox.Show(this, "Search found " + totalContactList.Count.ToString() + " records." + Environment.NewLine + "Would you like to narrow your search?", "Narrow search",  MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2))
							return new ArrayList();
					}
					
					foreach (DistrictProperties dp in alSubDiv)
					{
						IList alDealInfo = (IList)session.CreateCriteria(typeof(DealInfo)).
							Add(Expression.Eq("DistrictProperty", dp)).
							List();

						foreach (DealInfo di in alDealInfo)
						{
							IList propertyItemList = (IList)session.CreateCriteria(typeof(PropertyItem)).
								Add(Expression.Eq("DealInfo", di)).
								List();

							foreach (PropertyItem pi in	propertyItemList)
							{
								alPropertyItems.Add(pi);
							}
						}						
					}										
				}				
			}
			catch (Exception 
#if DEBUG
				exc
#endif 
				)
			{
#if DEBUG
				MessageBox.Show(exc.ToString());
#endif 
			}
			return alPropertyItems;
		}
		#endregion � GetContactsViaSubDivision �	

		private ArrayList GetContactsViaBarCode(string barCode)
		{
			ArrayList alPropertyItems = new ArrayList();									
			NHibernate.ISession session = null;
			
			try
			{																
				session = Globals.factory.OpenSession();

				IList propertyItemList = (IList)session.CreateCriteria(typeof(PropertyItem)).List();
							
				foreach (PropertyItem pi in	propertyItemList)
				{
					string strippedBC = pi.BarcodeId.Replace("*", string.Empty);
					if (strippedBC.ToUpper() == barCode.ToUpper())
						alPropertyItems.Add(pi);
				}							
			}
			catch (Exception 
#if DEBUG
				exc
#endif 
				)
			{
#if DEBUG
				MessageBox.Show(exc.ToString());
#endif 
			}
			return alPropertyItems;
		}

		#region * FastPropertyItemDisplay *
		private void FastPropertyItemDisplay(ArrayList alPropertyItems)
		{			
			SortedList filteredPropertyItems = null;
			if (this.lvGroups.Items.Count > 0 && (false == this.cbSearchAllGroups.Checked))
			{				 
				filteredPropertyItems = this.CreateFilter();
			}

			foreach (PropertyItem pi in alPropertyItems)
			{
				if (null != filteredPropertyItems)
				{
					if (false == filteredPropertyItems.Contains(pi.IdPropertyItem))
					{
						continue;
					}
				}
				

				ListViewItem lvi1 = new ListViewItem(string.Empty);
				if (null != pi && null != pi.Owner)
					lvi1.SubItems.Add(pi.Owner.FullName);
				else
					lvi1.SubItems.Add(string.Empty);
				if (null != pi && null != pi.Owner && null != pi.Owner.SiteAddress)
				{
					lvi1.SubItems.Add(pi.Owner.SiteAddress.FullSiteStreetAddress);
					lvi1.SubItems.Add(pi.Owner.SiteAddress.SiteCity);
					lvi1.SubItems.Add(pi.Owner.SiteAddress.SiteZIP);
				}
				else
				{
					lvi1.SubItems.Add(string.Empty);
					lvi1.SubItems.Add(string.Empty);
					lvi1.SubItems.Add(string.Empty);
				}

				if (null != pi && null != pi.Owner && null != pi.Owner.MailAddress)
				{
					lvi1.SubItems.Add(pi.Owner.MailAddress.FullSiteStreetAddress);					
					lvi1.SubItems.Add(pi.Owner.MailAddress.SiteCity);
					lvi1.SubItems.Add(pi.Owner.MailAddress.SiteZIP);
				}
				else
				{
					lvi1.SubItems.Add(string.Empty);
					lvi1.SubItems.Add(string.Empty);
					lvi1.SubItems.Add(string.Empty);
				}
				if (null != pi && null != pi.ListingAgent)
				{
					lvi1.SubItems.Add(pi.ListingAgent.ListingAgentFullName);
					lvi1.SubItems.Add(pi.ListingAgent.ListingAgentMLSNumber);
				}
				else
				{
					lvi1.SubItems.Add(string.Empty);
					lvi1.SubItems.Add(string.Empty);					
				}
				if (null != pi && null != pi.DealInfo && null != pi.DealInfo.DistrictProperty)
					lvi1.SubItems.Add(pi.DealInfo.DistrictProperty.Subdivision);
				else
					lvi1.SubItems.Add(string.Empty);

				if (null != pi && null != pi.Owner && null != pi.Owner.HomePhone)
					lvi1.SubItems.Add(pi.Owner.HomePhone.PhoneNr);
				else
					lvi1.SubItems.Add(string.Empty);


				lvi1.Tag = (Guid)pi.IdPropertyItem;

				this.lvResults.Items.Add(lvi1);		
			}
		}
		#endregion * FastPropertyItemDisplay *

		#region * bAssignToGroup_Click *
		private void bAssignToGroup_Click(object sender, System.EventArgs e)
		{
			ArrayList alPropertyItemIds = new ArrayList();
			for (int i = 0; i < this.lvResults.Items.Count; i++)
			{
				ListViewItem lvi = (ListViewItem)this.lvResults.Items[i];
				if (true == lvi.Checked)
				{
					alPropertyItemIds.Add((Guid)lvi.Tag);
				}
			}

			if (0 == alPropertyItemIds.Count)
			{
				MessageBox.Show(this, "Please tag a contact you want to assign to a group!", "Select contacts!", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}

			SelectGroupForm sgf = new SelectGroupForm(alPropertyItemIds);
			sgf.ShowDialog();
		}
		#endregion * bAssignToGroup_Click *

		private void Floater_MouseEnter(object sender, EventArgs e)
		{
			this.floater = new DealMaker.Forms.Floaters.FindContact();


			Point pReadMe = this.readMe1.Location;
			Size fSize = this.floater.Size;
						
			//this.floater.Location = new Point(pReadMe.X - fSize.Width, pReadMe.Y + this.readMe1.Height);
			this.floater.Location = PointToScreen(new Point (pReadMe.X - fSize.Width, pReadMe.Y) );

			this.floater.Show();
		}

		private void Floater_MouseLeave(object sender, EventArgs e)
		{
			this.floater.Hide();
		}
	}
	#endregion � Class FindPropertyItem �
}
#endregion � Namespace DealMaker �