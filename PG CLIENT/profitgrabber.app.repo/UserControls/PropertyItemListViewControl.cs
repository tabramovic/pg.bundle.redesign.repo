#region � Using �
using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;
using System.Data.SqlClient; 
using System.Data.SqlTypes;
using NHibernate;
using NHibernate.Cfg;
using DealMaker.Export;

#endregion � Using �
#region � Namespace DealMaker �
namespace DealMaker
{
	#region � class PropertyItemListViewControl �
	/// <summary>
	/// Summary description for PropertyItemListViewControl.
	/// </summary>
	public class PropertyItemListViewControl : System.Windows.Forms.UserControl
	{
		#region � Data �
		private DealMaker.Forms.Floaters.ListView floater = null;
		
		private bool asc = false;
		private int currentSelectedGroup = -1;		
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox tbSelectedGroup;
		private System.Windows.Forms.GroupBox PropertyItems;
		private System.Windows.Forms.ListView lvPropertyItems;
		private System.Windows.Forms.ColumnHeader FullName;
		private System.Windows.Forms.ColumnHeader SiteAddr;
		private System.Windows.Forms.ColumnHeader FirstEntered;
		private System.Windows.Forms.Button bBrowseGroup;
		private System.Windows.Forms.ColumnHeader SqFt;
		private System.Windows.Forms.ColumnHeader siteZIP;
		private System.Windows.Forms.ColumnHeader tag;
		private System.Windows.Forms.Button bTagAll;
		private System.Windows.Forms.Button bUnTagAll;
		private System.Windows.Forms.Button bLookUpTagged;
		private System.Windows.Forms.Button bLookUpUnTagged;
		private System.Windows.Forms.Button bAssignToGroup;
		private System.Windows.Forms.Button bExport;
		private System.Windows.Forms.Button bDeleteTagged;
		private DealMaker.ReadMe readMe1;
		private System.Windows.Forms.ColumnHeader siteCity;
		private System.Windows.Forms.ColumnHeader siteState;
		private System.Windows.Forms.ColumnHeader listingAgentName;
		private System.Windows.Forms.ColumnHeader listingPrice;
		private System.Windows.Forms.ColumnHeader listingAgentFax;
		private System.Windows.Forms.ColumnHeader foreclosureFileDate;
		private System.Windows.Forms.ColumnHeader foreclosureSaleDate;
		private System.Windows.Forms.ColumnHeader mailAddress;
		private System.Windows.Forms.ColumnHeader mailCity;
		private System.Windows.Forms.ColumnHeader mailState;
		private System.Windows.Forms.ColumnHeader mailZIP;
		private System.Windows.Forms.ColumnHeader siteCityState;
		private System.Windows.Forms.ColumnHeader mailCityState;
        private Button _showRoute;

        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.Container components = null;
		#endregion � Data �
		
		public Hashtable GetSelectedPropertyItems
		{
			get 
			{
				Hashtable htPropertyItems = new Hashtable();
				for (int i = 0; i < this.lvPropertyItems.Items.Count; i++)
				{
					ListViewItem lvi = (ListViewItem)this.lvPropertyItems.Items[i];
					htPropertyItems.Add(i + 1, lvi.Tag);
				}
				return htPropertyItems;
			}
		}
		
		#region � CTR && Dispose �
		public PropertyItemListViewControl()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

			// TODO: Add any initialization after the InitializeComponent call
			//Floaters
			this.readMe1.MouseEnter += new EventHandler(Floater_MouseEnter);
			this.readMe1.MouseLeave += new EventHandler(Floater_MouseLeave);
			OneTwoThreeSold.NEW_CONTACT_IN_GROUP_BY_123_SOLD += new DealMaker.OneTwoThreeSold.NewContactInGroupFrom123Sold(NewContactInGroupBy123Sold);
		}

		public PropertyItemListViewControl(int selectedGroupId, string selectedGroupName)
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();
			this.currentSelectedGroup = selectedGroupId;
			this.ShowData_FastAccess(selectedGroupId);			
			this.tbSelectedGroup.Text = selectedGroupName;

			//Floaters
			this.readMe1.MouseEnter += new EventHandler(Floater_MouseEnter);
			this.readMe1.MouseLeave += new EventHandler(Floater_MouseLeave);	
		}

		/*public PropertyItemListViewControl(Hashtable htNodePosPropertyItemId, string selectedGroupName)
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();
			this.ShowData(htNodePosPropertyItemId);			
			this.tbSelectedGroup.Text = selectedGroupName;
		}*/

				


		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}
		#endregion � CTR && Dispose �
		#region � ShowData �
		/*private void ShowData (Hashtable htNodePosPropertyItemId)
		{
			this.lvPropertyItems.Items.Clear();
			IDictionaryEnumerator ide = htNodePosPropertyItemId.GetEnumerator();
			while (ide.MoveNext())
			{
				Guid propertyItemId = (Guid)ide.Value;				
				
				PropertyItem pi = PropertyItem.LoadPropertyItem(propertyItemId);				

				if (null != pi)
				{
					ListViewItem lvi = new ListViewItem(string.Empty);				
					if (null != pi.Owner)
						lvi.SubItems.Add(pi.Owner.FullName);
					else
						lvi.SubItems.Add(string.Empty);

					if (null != pi.Owner.SiteAddress)
					{
						lvi.SubItems.Add(pi.Owner.SiteAddress.FullSiteStreetAddress);
						lvi.SubItems.Add(pi.Owner.SiteAddress.SiteZIP);
					}
					else
					{
						lvi.SubItems.Add(string.Empty);
						lvi.SubItems.Add(string.Empty);
					}

					lvi.SubItems.Add(pi.DateCreated.ToShortDateString());

					if (null != pi.DealInfo && null != pi.DealInfo.HouseProperty)
					{
						lvi.SubItems.Add(pi.DealInfo.HouseProperty.HomeSqFt.ToString());
					}
					else
					{
						lvi.SubItems.Add(string.Empty);
					}

					lvi.Tag = propertyItemId;				
					this.lvPropertyItems.Items.Insert(0, lvi);
				}				
			}
		}*/
		#endregion � ShowData �	


		private delegate void ShowData_FastAccessDelegate(int nodeId);
		private void ShowData_FastAccess (int nodeId)
		{
			Globals.SelectedGroupInListView = nodeId;			
			
			this.lvPropertyItems.BeginUpdate();
			this.lvPropertyItems.Items.Clear();
			Cursor.Current = Cursors.WaitCursor;
			SqlDataReader dataReader = null;
			try
			{																	
				if (ConnectionState.Open != Connection.getInstance().HatchConn.State)
				{
					Connection.getInstance().HatchConn.Open();
				}

				//SqlCommand sp_GetPropertyItemsDescInNode = new SqlCommand("GetPropertyItemsDescInNode", Connection.getInstance().HatchConn);

				ApplicationHelper.TestDatabase_CreateSPL_GetPropertyItemsDescInNodeExt();
				SqlCommand sp_GetPropertyItemsDescInNode = new SqlCommand("GetPropertyItemsDescInNodeExt", Connection.getInstance().HatchConn);
				sp_GetPropertyItemsDescInNode.CommandType = CommandType.StoredProcedure;				

				sp_GetPropertyItemsDescInNode.Parameters.Add("@NodeId", nodeId);			
				dataReader = sp_GetPropertyItemsDescInNode.ExecuteReader();
				while (dataReader.Read())
				{
					
					Guid	propertyItemId		= dataReader.GetGuid(0);
					DateTime dtFirstEntered		= dataReader.GetDateTime(1);
					
					string fullName				= string.Empty;
					if (DBNull.Value != dataReader[2])
						fullName				= dataReader.GetString(2);					

					string siteAddress			= string.Empty;
					if (DBNull.Value != dataReader[3])
						siteAddress				= dataReader.GetString(3);
					
					string siteCity				= string.Empty;
					if (DBNull.Value != dataReader[4])
						//TA++:16.04.2005.
						//Bug Nr.: RQ_0003
						//PREVIOUS++
						/*
							siteAddress				= dataReader.GetString(4);
						*/
						//PREVIOUS--

						//NEW++
						siteCity				= dataReader.GetString(4);	
						//NEW--

					string siteState			= string.Empty;
					if (DBNull.Value != dataReader[5])
						siteState				= dataReader.GetString(5);

					string siteCityState		= string.Empty;
					if (DBNull.Value != dataReader[6])
						siteCityState			= dataReader.GetString(6);

					string siteZIP				= string.Empty;
					if (DBNull.Value != dataReader[7])
						siteZIP					= dataReader.GetString(7);

					string mailAddress			= string.Empty;
					if (DBNull.Value != dataReader[8])
						mailAddress				= dataReader.GetString(8);
					
					string mailCity				= string.Empty;
					if (DBNull.Value != dataReader[9])
						mailCity				= dataReader.GetString(9);

					string mailState			= string.Empty;
					if (DBNull.Value != dataReader[10])
						mailState				= dataReader.GetString(10);

					string mailCityState		= string.Empty;
					if (DBNull.Value != dataReader[11])
						mailCityState				= dataReader.GetString(11);

					string mailZIP				= string.Empty;
					if (DBNull.Value != dataReader[12])
						mailZIP					= dataReader.GetString(12);

					decimal homeSqFt			= 0;
					if (DBNull.Value != dataReader[13])
						homeSqFt				= dataReader.GetDecimal(13);

					string listingAgentName		= string.Empty;
					if (DBNull.Value != dataReader[14])
						listingAgentName		= dataReader.GetString(14);

					decimal listingPrice		= 0;
					if (DBNull.Value != dataReader[15])
						listingPrice			= dataReader.GetDecimal(15);

					string listingAgentFaxNr	= string.Empty;
					if (DBNull.Value != dataReader[16])
						listingAgentFaxNr		= dataReader.GetString(16);

					string foreClosureFileDateValue	= string.Empty;
					if (DBNull.Value != dataReader[17])
						foreClosureFileDateValue		= dataReader.GetString(17);

					string foreClosureSaleDate	= string.Empty;
					if (DBNull.Value != dataReader[18])
						foreClosureSaleDate		= dataReader.GetString(18);


					
					ListViewItem lvi = new ListViewItem(string.Empty);
					lvi.SubItems.Add(fullName);
					lvi.SubItems.Add(siteAddress);
					lvi.SubItems.Add(siteCity);
					lvi.SubItems.Add(siteState);
					lvi.SubItems.Add(siteCityState);
					lvi.SubItems.Add(siteZIP);
				
					lvi.SubItems.Add(listingAgentName);
					lvi.SubItems.Add(listingPrice.ToString());
					lvi.SubItems.Add(listingAgentFaxNr);

					lvi.SubItems.Add(foreClosureFileDateValue);
					lvi.SubItems.Add(foreClosureSaleDate);

					lvi.SubItems.Add(mailAddress);
					lvi.SubItems.Add(mailCity);
					lvi.SubItems.Add(mailState);
					lvi.SubItems.Add(mailCityState);
					lvi.SubItems.Add(mailZIP);
														
					lvi.SubItems.Add(homeSqFt.ToString());
					lvi.SubItems.Add(dtFirstEntered.ToShortDateString());


					lvi.Tag = propertyItemId;	
					if (0 == this.lvPropertyItems.Items.Count)
					{
						this.lvPropertyItems.Items.Add(lvi);
					}
					else
					{
						this.lvPropertyItems.Items.Insert(0, lvi);
					}
				}
			}
			catch (Exception exc)
			{	
				MessageBox.Show(exc.Message);
			}
			finally
			{
				dataReader.Close();
				this.lvPropertyItems.EndUpdate();				
				this.lvPropertyItems.Refresh();
				this.lvPropertyItems.Select();
				Cursor.Current = Cursors.Arrow;
			}
		}

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PropertyItemListViewControl));
            this.label1 = new System.Windows.Forms.Label();
            this.tbSelectedGroup = new System.Windows.Forms.TextBox();
            this.PropertyItems = new System.Windows.Forms.GroupBox();
            this.lvPropertyItems = new System.Windows.Forms.ListView();
            this.tag = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.FullName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.SiteAddr = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.siteCity = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.siteState = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.siteCityState = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.siteZIP = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.listingAgentName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.listingPrice = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.listingAgentFax = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.foreclosureFileDate = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.foreclosureSaleDate = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.mailAddress = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.mailCity = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.mailState = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.mailCityState = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.mailZIP = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.SqFt = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.FirstEntered = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.bBrowseGroup = new System.Windows.Forms.Button();
            this.bTagAll = new System.Windows.Forms.Button();
            this.bUnTagAll = new System.Windows.Forms.Button();
            this.bLookUpTagged = new System.Windows.Forms.Button();
            this.bLookUpUnTagged = new System.Windows.Forms.Button();
            this.bAssignToGroup = new System.Windows.Forms.Button();
            this.bExport = new System.Windows.Forms.Button();
            this.bDeleteTagged = new System.Windows.Forms.Button();
            this.readMe1 = new DealMaker.ReadMe();
            this._showRoute = new System.Windows.Forms.Button();
            this.PropertyItems.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(8, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(128, 23);
            this.label1.TabIndex = 0;
            this.label1.Text = "Selected Group:";
            // 
            // tbSelectedGroup
            // 
            this.tbSelectedGroup.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbSelectedGroup.Location = new System.Drawing.Point(136, 8);
            this.tbSelectedGroup.Name = "tbSelectedGroup";
            this.tbSelectedGroup.ReadOnly = true;
            this.tbSelectedGroup.Size = new System.Drawing.Size(478, 38);
            this.tbSelectedGroup.TabIndex = 1;
            // 
            // PropertyItems
            // 
            this.PropertyItems.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PropertyItems.Controls.Add(this.lvPropertyItems);
            this.PropertyItems.Location = new System.Drawing.Point(8, 40);
            this.PropertyItems.Name = "PropertyItems";
            this.PropertyItems.Size = new System.Drawing.Size(894, 632);
            this.PropertyItems.TabIndex = 2;
            this.PropertyItems.TabStop = false;
            this.PropertyItems.Text = "Property Items   ";
            // 
            // lvPropertyItems
            // 
            this.lvPropertyItems.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lvPropertyItems.CheckBoxes = true;
            this.lvPropertyItems.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.tag,
            this.FullName,
            this.SiteAddr,
            this.siteCity,
            this.siteState,
            this.siteCityState,
            this.siteZIP,
            this.listingAgentName,
            this.listingPrice,
            this.listingAgentFax,
            this.foreclosureFileDate,
            this.foreclosureSaleDate,
            this.mailAddress,
            this.mailCity,
            this.mailState,
            this.mailCityState,
            this.mailZIP,
            this.SqFt,
            this.FirstEntered});
            this.lvPropertyItems.FullRowSelect = true;
            this.lvPropertyItems.GridLines = true;
            this.lvPropertyItems.HideSelection = false;
            this.lvPropertyItems.HoverSelection = true;
            this.lvPropertyItems.Location = new System.Drawing.Point(16, 24);
            this.lvPropertyItems.MultiSelect = false;
            this.lvPropertyItems.Name = "lvPropertyItems";
            this.lvPropertyItems.Size = new System.Drawing.Size(862, 600);
            this.lvPropertyItems.TabIndex = 0;
            this.lvPropertyItems.UseCompatibleStateImageBehavior = false;
            this.lvPropertyItems.View = System.Windows.Forms.View.Details;
            this.lvPropertyItems.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.lvPropertyItems_ColumnClick);
            this.lvPropertyItems.DoubleClick += new System.EventHandler(this.lvPropertyItems_DoubleClick);
            // 
            // tag
            // 
            this.tag.Text = "Tag";
            this.tag.Width = 38;
            // 
            // FullName
            // 
            this.FullName.Text = "Full Name";
            this.FullName.Width = 167;
            // 
            // SiteAddr
            // 
            this.SiteAddr.Text = "Site Address";
            this.SiteAddr.Width = 120;
            // 
            // siteCity
            // 
            this.siteCity.Text = "Site City";
            this.siteCity.Width = 100;
            // 
            // siteState
            // 
            this.siteState.Text = "Site State";
            this.siteState.Width = 100;
            // 
            // siteCityState
            // 
            this.siteCityState.Text = "Site City, State";
            this.siteCityState.Width = 120;
            // 
            // siteZIP
            // 
            this.siteZIP.Text = "Site ZIP";
            // 
            // listingAgentName
            // 
            this.listingAgentName.Text = "Listing Agent Name";
            this.listingAgentName.Width = 100;
            // 
            // listingPrice
            // 
            this.listingPrice.Text = "Listing Price";
            // 
            // listingAgentFax
            // 
            this.listingAgentFax.Text = "Listing Agent Fax";
            // 
            // foreclosureFileDate
            // 
            this.foreclosureFileDate.Text = "Foreclosure File Date";
            this.foreclosureFileDate.Width = 80;
            // 
            // foreclosureSaleDate
            // 
            this.foreclosureSaleDate.Text = "Foreclosure Sale Date";
            this.foreclosureSaleDate.Width = 80;
            // 
            // mailAddress
            // 
            this.mailAddress.Text = "Mail Address";
            this.mailAddress.Width = 120;
            // 
            // mailCity
            // 
            this.mailCity.Text = "Mail City";
            this.mailCity.Width = 100;
            // 
            // mailState
            // 
            this.mailState.Text = "Mail State";
            this.mailState.Width = 100;
            // 
            // mailCityState
            // 
            this.mailCityState.Text = "Mail City, State";
            this.mailCityState.Width = 120;
            // 
            // mailZIP
            // 
            this.mailZIP.Text = "Mail ZIP";
            // 
            // SqFt
            // 
            this.SqFt.Text = "SqFt";
            this.SqFt.Width = 45;
            // 
            // FirstEntered
            // 
            this.FirstEntered.Text = "First Entered";
            this.FirstEntered.Width = 101;
            // 
            // bBrowseGroup
            // 
            this.bBrowseGroup.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.bBrowseGroup.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.bBrowseGroup.Location = new System.Drawing.Point(624, 8);
            this.bBrowseGroup.Name = "bBrowseGroup";
            this.bBrowseGroup.Size = new System.Drawing.Size(32, 23);
            this.bBrowseGroup.TabIndex = 3;
            this.bBrowseGroup.Text = "...";
            this.bBrowseGroup.Click += new System.EventHandler(this.bBrowseGroup_Click);
            // 
            // bTagAll
            // 
            this.bTagAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.bTagAll.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.bTagAll.Location = new System.Drawing.Point(142, 689);
            this.bTagAll.Name = "bTagAll";
            this.bTagAll.Size = new System.Drawing.Size(120, 23);
            this.bTagAll.TabIndex = 4;
            this.bTagAll.Text = "Tag All";
            this.bTagAll.Click += new System.EventHandler(this.bTagAll_Click);
            // 
            // bUnTagAll
            // 
            this.bUnTagAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.bUnTagAll.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.bUnTagAll.Location = new System.Drawing.Point(270, 689);
            this.bUnTagAll.Name = "bUnTagAll";
            this.bUnTagAll.Size = new System.Drawing.Size(120, 23);
            this.bUnTagAll.TabIndex = 5;
            this.bUnTagAll.Text = "Untag All";
            this.bUnTagAll.Click += new System.EventHandler(this.bUnTagAll_Click);
            // 
            // bLookUpTagged
            // 
            this.bLookUpTagged.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.bLookUpTagged.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.bLookUpTagged.Location = new System.Drawing.Point(398, 689);
            this.bLookUpTagged.Name = "bLookUpTagged";
            this.bLookUpTagged.Size = new System.Drawing.Size(120, 23);
            this.bLookUpTagged.TabIndex = 6;
            this.bLookUpTagged.Text = "Lookup Tagged";
            this.bLookUpTagged.Click += new System.EventHandler(this.bLookUpTagged_Click);
            // 
            // bLookUpUnTagged
            // 
            this.bLookUpUnTagged.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.bLookUpUnTagged.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.bLookUpUnTagged.Location = new System.Drawing.Point(526, 689);
            this.bLookUpUnTagged.Name = "bLookUpUnTagged";
            this.bLookUpUnTagged.Size = new System.Drawing.Size(120, 23);
            this.bLookUpUnTagged.TabIndex = 7;
            this.bLookUpUnTagged.Text = "Lookup Untagged";
            this.bLookUpUnTagged.Click += new System.EventHandler(this.bLookUpUnTagged_Click);
            // 
            // bAssignToGroup
            // 
            this.bAssignToGroup.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.bAssignToGroup.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.bAssignToGroup.Location = new System.Drawing.Point(654, 689);
            this.bAssignToGroup.Name = "bAssignToGroup";
            this.bAssignToGroup.Size = new System.Drawing.Size(120, 23);
            this.bAssignToGroup.TabIndex = 8;
            this.bAssignToGroup.Text = "Assign to Group";
            this.bAssignToGroup.Click += new System.EventHandler(this.bAssignToGroup_Click);
            // 
            // bExport
            // 
            this.bExport.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.bExport.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.bExport.Location = new System.Drawing.Point(782, 689);
            this.bExport.Name = "bExport";
            this.bExport.Size = new System.Drawing.Size(120, 23);
            this.bExport.TabIndex = 9;
            this.bExport.Text = "Export Tagged";
            this.bExport.Click += new System.EventHandler(this.bExport_Click);
            // 
            // bDeleteTagged
            // 
            this.bDeleteTagged.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.bDeleteTagged.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.bDeleteTagged.Location = new System.Drawing.Point(14, 689);
            this.bDeleteTagged.Name = "bDeleteTagged";
            this.bDeleteTagged.Size = new System.Drawing.Size(120, 23);
            this.bDeleteTagged.TabIndex = 10;
            this.bDeleteTagged.Text = "Delete Tagged";
            this.bDeleteTagged.Click += new System.EventHandler(this.bDeleteTagged_Click);
            // 
            // readMe1
            // 
            this.readMe1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.readMe1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.readMe1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("readMe1.BackgroundImage")));
            this.readMe1.Location = new System.Drawing.Point(894, 8);
            this.readMe1.Name = "readMe1";
            this.readMe1.Size = new System.Drawing.Size(16, 16);
            this.readMe1.TabIndex = 11;
            // 
            // _showRoute
            // 
            this._showRoute.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._showRoute.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._showRoute.Location = new System.Drawing.Point(14, 718);
            this._showRoute.Name = "_showRoute";
            this._showRoute.Size = new System.Drawing.Size(120, 23);
            this._showRoute.TabIndex = 12;
            this._showRoute.Text = "Show Route";
            this._showRoute.Click += new System.EventHandler(this._showRoute_Click);
            // 
            // PropertyItemListViewControl
            // 
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.Controls.Add(this._showRoute);
            this.Controls.Add(this.readMe1);
            this.Controls.Add(this.bDeleteTagged);
            this.Controls.Add(this.bExport);
            this.Controls.Add(this.bAssignToGroup);
            this.Controls.Add(this.bLookUpUnTagged);
            this.Controls.Add(this.bLookUpTagged);
            this.Controls.Add(this.bUnTagAll);
            this.Controls.Add(this.bTagAll);
            this.Controls.Add(this.bBrowseGroup);
            this.Controls.Add(this.PropertyItems);
            this.Controls.Add(this.tbSelectedGroup);
            this.Controls.Add(this.label1);
            this.Name = "PropertyItemListViewControl";
            this.Size = new System.Drawing.Size(918, 758);
            this.Load += new System.EventHandler(this.PropertyItemListViewControl_Load);
            this.PropertyItems.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		#endregion

		#region � Methods �		
		#region � PropertyItemListViewControl_Load �
		private void PropertyItemListViewControl_Load(object sender, System.EventArgs e)
		{
			this.tbSelectedGroup.Text = Globals.selectedNodeName.TrimEnd();
		}
		#endregion � PropertyItemListViewControl_Load �
		#region � lvPropertyItems_ColumnClick �
		private void lvPropertyItems_ColumnClick(object sender, System.Windows.Forms.ColumnClickEventArgs e)
		{
			// Set the ListViewItemSorter property to a new ListViewItemComparer 
			// object. Setting this property immediately sorts the 
			// ListView using the ListViewItemComparer object.			
			this.asc = !this.asc;
			if (true == this.asc)
				this.lvPropertyItems.ListViewItemSorter = new ListViewItemComparer(e.Column);
			else
				this.lvPropertyItems.ListViewItemSorter = new ListViewItemComparerDesc(e.Column);

		}
		#endregion � lvPropertyItems_ColumnClick �
		#region � bBrowseGroup_Click �
		private void bBrowseGroup_Click(object sender, System.EventArgs e)
		{
			SelectGroupForm sgf = new SelectGroupForm();
			sgf.ShowDialog();
			//Hashtable htNodePosPropertyItemId = ApplicationHelper.GetPropertyItemsInNode(sgf.customizedTreeViewUserControl1.SelectedNodeId);
			this.currentSelectedGroup = sgf.customizedTreeViewUserControl1.SelectedNodeId;
			this.tbSelectedGroup.Text = sgf.customizedTreeViewUserControl1.tv_Nodes.SelectedNode.Text;
			//this.ShowData(htNodePosPropertyItemId);
			this.ShowData_FastAccess(sgf.customizedTreeViewUserControl1.SelectedNodeId);
			Globals.oldListView = this;
		}
		#endregion � bBrowseGroup_Click �
		#region � bTagAll_Click �
		private void bTagAll_Click(object sender, System.EventArgs e)
		{
			this.TagListView(true);
			Globals.oldListView = this;
		}
		#endregion � bTagAll_Click �
		#region � bUnTagAll_Click �
		private void bUnTagAll_Click(object sender, System.EventArgs e)
		{
			this.TagListView(false);
			Globals.oldListView = this;
		}
		#endregion � bUnTagAll_Click �
		#region � TagListView  �
		private void TagListView (bool ckhValue)
		{
			for (int i = 0; i < this.lvPropertyItems.Items.Count; i++)
			{
				ListViewItem lvi = (ListViewItem)this.lvPropertyItems.Items[i];
				lvi.Checked = ckhValue;
			}
		}
		#endregion � TagListView  �
		#region � bAssignToGroup_Click �
		private void bAssignToGroup_Click(object sender, System.EventArgs e)
		{
			ArrayList alPropertyItemIds = new ArrayList();
			int i = 0;
			for (i = 0; i < this.lvPropertyItems.Items.Count; i++)
			{
				ListViewItem lvi = (ListViewItem)this.lvPropertyItems.Items[i];
				if (true == lvi.Checked)
				{
					alPropertyItemIds.Add((Guid)lvi.Tag);
				}
			}

			if (0 == i)
			{
				MessageBox.Show("Tag contacts to Assign to group", "Tag contacts to Assign to group", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}

			SelectGroupForm sgf = new SelectGroupForm(alPropertyItemIds);
			sgf.ShowDialog();			
		}
		#endregion � bAssignToGroup_Click �
		#region � bLookUpTagged_Click �
		private void bLookUpTagged_Click(object sender, System.EventArgs e)
		{
			Globals.oldListView = this;
			int iCnt = 1;
			Hashtable htPropertyItems = new Hashtable();
			for (int i = 0; i < this.lvPropertyItems.Items.Count; i++)
			{
				ListViewItem lvi = (ListViewItem)this.lvPropertyItems.Items[i];
				if (true == lvi.Checked)
				{
					htPropertyItems.Add(iCnt, (Guid)lvi.Tag);
					iCnt++;
				}
			}
			if (htPropertyItems.Count > 0)
			{
				//sasa, 2006.08.27.: bugfix nw
				Globals.ContactScreenOnMainForm = true;

				if (null != Globals.oldContactScreen)
				{
					//sasa, 20.11.05: hack, aaaghr, we must remove event handler from former contact screen
					Network.NetworkUDP.ChangedPropertyItem -= Globals.oldContactScreen.networkEventHandler;
					//sasa, 24.01.06: new nw lock
					Network.NetworkLock.StatusChanged -=Globals.oldContactScreen.networkEventHandler2;
				}
				//~sasa

				ContactScreen cs = new ContactScreen(htPropertyItems);
				cs.Size = Globals.applicationSize;
				cs.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top; 
				cs.Dock = DockStyle.Fill;
				Panel panel = (Panel)this.Parent;
				MainForm mainForm = (MainForm)panel.Parent;

				mainForm.appBodyPanel.Controls.RemoveAt(0);
				mainForm.appBodyPanel.Controls.Add(cs);
				
				//2006.08.27, sasa: nw lock, reload contact and acquire new lock if cs already on screen
				cs.ReloadContact();
			}
		}
		#endregion � bLookUpTagged_Click �
		#region � bLookUpUnTagged_Click �
		private void bLookUpUnTagged_Click(object sender, System.EventArgs e)
		{
			Globals.oldListView = this;
			int iCnt = 1;
			Hashtable htPropertyItems = new Hashtable();
			for (int i = 0; i < this.lvPropertyItems.Items.Count; i++)
			{
				ListViewItem lvi = (ListViewItem)this.lvPropertyItems.Items[i];
				if (false == lvi.Checked)
				{
					htPropertyItems.Add(iCnt, (Guid)lvi.Tag);
					iCnt++;
				}
			}
			if (htPropertyItems.Count > 0)
			{
				ContactScreen cs = new ContactScreen(htPropertyItems);
				cs.Size = Globals.applicationSize;
				cs.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top; 
				cs.Dock = DockStyle.Fill;
				Panel panel = (Panel)this.Parent;
				MainForm mainForm = (MainForm)panel.Parent;

				mainForm.appBodyPanel.Controls.RemoveAt(0);
				mainForm.appBodyPanel.Controls.Add(cs);
				
			}
		}
		#endregion � bLookUpUnTagged_Click �
		#region � bExport_Click �
		private void bExport_Click(object sender, System.EventArgs e)
		{
			ArrayList alCheckedPropertyItems = new ArrayList();			
			
			//get count of checked ListViewItems
			int iCnt = 0;
			foreach (ListViewItem lvi in this.lvPropertyItems.Items)
			{
				if (true == lvi.Checked)
					iCnt++;
			}

			if (0 == iCnt)
			{
				MessageBox.Show("Tag contacts to export", "Tag Contacts to export", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}

			//Event trigger
			EventTrigger et = new EventTrigger();
			PBForm pbf = new PBForm(et);
			pbf.Capacity = iCnt;
			pbf.WindowName = "Loading Data...";			
			pbf.Show();
			
			foreach (ListViewItem lvi in this.lvPropertyItems.Items)
			{
				if (true == lvi.Checked)
				{
					Guid propertyItemId = (Guid)lvi.Tag;
                    PropertyItem pi = PropertyItem.LoadPropertyItem(propertyItemId);
					alCheckedPropertyItems.Add(pi);

					//fire event
					et.TriggerChange();
				}
			}
			PropertyItemsExportWizard exportWizard = new PropertyItemsExportWizard((PropertyItem[])alCheckedPropertyItems.ToArray(typeof(PropertyItem)));
			exportWizard.Execute();
			
		}
		#endregion � bExport_Click �
		#region � lvPropertyItems_DoubleClick �
		private void lvPropertyItems_DoubleClick(object sender, System.EventArgs e)
		{
			Globals.oldListView = this;
			int iCnt = 1;
			ListView lv = (ListView)sender;
			ListView.SelectedIndexCollection selectedIndices = lv.SelectedIndices;
			ListViewItem lvi = lv.Items[selectedIndices[0]];

			Hashtable htPropertyItems = new Hashtable();
			htPropertyItems.Add(iCnt, lvi.Tag);

			if (htPropertyItems.Count > 0)
			{
				//sasa, 2006.07.01.: bugfix nw
				Globals.ContactScreenOnMainForm = true;

				if (null != Globals.oldContactScreen)
				{
					//sasa, 20.11.05: hack, aaaghr, we must remove event handler from former contact screen
					Network.NetworkUDP.ChangedPropertyItem -= Globals.oldContactScreen.networkEventHandler;
					//sasa, 24.01.06: new nw lock
					Network.NetworkLock.StatusChanged -=Globals.oldContactScreen.networkEventHandler2;
				}
				//~sasa

				ContactScreen cs = new ContactScreen(htPropertyItems);

				cs.Size = Globals.applicationSize;
				cs.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top; 
				cs.Dock = DockStyle.Fill;
				Panel panel = (Panel)this.Parent;
				MainForm mainForm = (MainForm)panel.Parent;

				mainForm.appBodyPanel.Controls.RemoveAt(0);
				mainForm.appBodyPanel.Controls.Add(cs);

				//2006.07.01, sasa: nw lock, reload contact and acquire new lock if cs already on screen
				cs.ReloadContact();
			}
		}
        #endregion � lvPropertyItems_DoubleClick �

        private void _showRoute_Click(object sender, EventArgs e)
        {
            ArrayList taggedItems = new ArrayList();
            for (int i = 0; i < this.lvPropertyItems.Items.Count; i++)
            {
                ListViewItem lvi = (ListViewItem)this.lvPropertyItems.Items[i];
                if (true == lvi.Checked)
                {
                    taggedItems.Add(lvi);
                }
            }

            if (taggedItems.Count > 0)
            {
                RoutingForm rf = new RoutingForm(taggedItems, false);                
                rf.ShowDialog();
            }
        }

        #region � bDeleteTagged_Click �
        private void bDeleteTagged_Click(object sender, System.EventArgs e)
		{
			Globals.oldListView = this;			
			ArrayList alPropertyItemsId = new ArrayList();
			for (int i = 0; i < this.lvPropertyItems.Items.Count; i++)
			{
				ListViewItem lvi = (ListViewItem)this.lvPropertyItems.Items[i];
				if (true == lvi.Checked)
				{
					alPropertyItemsId.Add((Guid)lvi.Tag);					
				}
			}
			if (alPropertyItemsId.Count > 0)
			{
				//remove item
				//bool updateInputData = false;
				
				/*
				 * OLD
				if (DialogResult.No == MessageBox.Show(null, UserMessages.PermanentRecordDeleteDesc, UserMessages.PermanentRecordDelete , MessageBoxButtons.YesNo, MessageBoxIcon.Warning ))
				{					
					return;
				}
				*/

				//TA++:20.Dec.2005
				//RQ_066
				DeleteContactsQuestionForm dgcf = new DeleteContactsQuestionForm(this.tbSelectedGroup.Text);
				dgcf.ShowDialog();
				if (false == dgcf.DeleteContacts)
				{
					return;
				}
				//TA--

				/*
				 * OLD
				ApplicationHelper.DeletePropertyItemsFromGroups(alPropertyItemsId, this.currentSelectedGroup, 1);
				*/

				//TA++:20.Dec.2005
				//RQ_066 
				if (true == dgcf.PermanentDelete)
				{
					ApplicationHelper.DeletePropertyItemsFromGroups(alPropertyItemsId, this.currentSelectedGroup, 1);
				}
				else
				{
					ApplicationHelper.DeletePropertyItemsFromGroups(alPropertyItemsId, this.currentSelectedGroup, 0);
				}
				//TA--
				
				//ApplicationHelper.DeleteTaggedPropertyItems(alPropertyItemsId, out updateInputData); 								
				//if (true == updateInputData)
				{
				Hashtable htNodePosPropertyItemId = ApplicationHelper.GetPropertyItemsInNode(currentSelectedGroup);
				this.ShowData_FastAccess(this.currentSelectedGroup);
				}
			}
		}
		#endregion � bDeleteTagged_Click �
		#endregion � Methods �

		private void Floater_MouseEnter(object sender, EventArgs e)
		{
			this.floater = new DealMaker.Forms.Floaters.ListView();


			Point pReadMe = this.readMe1.Location;
			Size fSize = this.floater.Size;
						
			//this.floater.Location = new Point(pReadMe.X - fSize.Width, pReadMe.Y + this.readMe1.Height);
			this.floater.Location = PointToScreen(new Point (pReadMe.X - fSize.Width, pReadMe.Y) );

			this.floater.Show();
		}

		private void Floater_MouseLeave(object sender, EventArgs e)
		{
			this.floater.Hide();
		}

		
		private delegate void RefreshListView(int nodeId);
		private void NewContactInGroupBy123Sold(int nodeId)
		{
			if (Globals.SelectedGroupInListView == nodeId)
			{																
				ShowData_FastAccess(nodeId);					
			}
		}

       
    }
    #endregion � class PropertyItemListViewControl �

    #region � Class ListViewItemComparer �
    // Implements the manual sorting of items by columns.
    class ListViewItemComparer : IComparer
	{
		private int col;
		public ListViewItemComparer()
		{
			col = 0;
		}
		public ListViewItemComparer(int column)
		{
			col = column;
		}
		public int Compare(object x, object y)
		{
			return String.Compare(((ListViewItem)x).SubItems[col].Text, ((ListViewItem)y).SubItems[col].Text);
		}
	}
	#endregion � Class ListViewItemComparer �
	#region � Class ListViewItemComparerDesc �
	// Implements the manual sorting of items by columns.
	class ListViewItemComparerDesc : IComparer
	{
		private int col;
		public ListViewItemComparerDesc()
		{
			col = 0;
		}
		public ListViewItemComparerDesc(int column)
		{
			col = column;
		}
		public int Compare(object x, object y)
		{
			return String.Compare(((ListViewItem)y).SubItems[col].Text, ((ListViewItem)x).SubItems[col].Text);
		}
	}
	#endregion � Class ListViewItemComparerDesc �

	class ListViewItemTagComparer : IComparer	
	{	
		public int Compare(object x, object y)
		{
			int leftPos = (int)x;
			int rightPos = (int)y;

			if (leftPos < rightPos)
				return -1;
			else if (leftPos > rightPos)
				return 1;
			else 
				return 0;
		}
	}
}
#endregion � Namespace DealMaker � 