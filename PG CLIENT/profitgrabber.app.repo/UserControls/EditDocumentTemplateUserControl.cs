#region USING
using System;
using System.IO;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Printing;
using System.Data;
using System.Windows.Forms;

//Added
using DealMaker.PlainClasses.ContactTypes;
#endregion

namespace DealMaker
{
	/// <summary>
	/// Summary description for EditDocumentTemplateUserControl.
	/// </summary>
	public class EditDocumentTemplateUserControl : System.Windows.Forms.UserControl
	{
		bool _insertFrame = false;
		Panel _panel = null;

		#region DATA
		ArrayList _images = null;
		private bool bDoNotStore = false;
		private bool bAddded = false;
		private string offeredPrice = string.Empty;
		private System.Windows.Forms.ContextMenuStrip ctxMenu;
		private TXTextControl.ButtonBar txButtonBar1;
		private TXTextControl.RulerBar txRulerBar1;
		private TXTextControl.StatusBar txStatusBar1;
		public  TXTextControl.TextControl txTextControl1;
		private ToolStripMenuItem miCopy;
		private ToolStripMenuItem miPaste;
		private ToolStripMenuItem miUndo;
		private ToolStripMenuItem miRedo;
		private ToolStripMenuItem miInsertPicture;
		private ToolStripMenuItem _mi_New;
		private ToolStripMenuItem _mi_OpenFile;
		private ToolStripMenuItem _mi_SaveFile;
		private ToolStripMenuItem _mi_Open_Template;
		private ToolStripMenuItem _mi_SaveTemplate;
		private ToolStripMenuItem _miPageSetup;
		private ToolStripMenuItem _miPrint;
		private ToolStripMenuItem _mi_PrintPreview;
		private ToolStripMenuItem _insertTextFrame;
		private ToolStripMenuItem _showTextFrameBorder;
        private ToolStripMenuItem _setForegroundColor;
        private ToolStripMenuItem _setBackgroundColor;
        private ToolStripSeparator toolStripSeparator1;
        private ToolStripSeparator toolStripSeparator2;
        private ToolStripSeparator toolStripSeparator3;
        private ToolStripSeparator toolStripSeparator4;
        private ToolStripSeparator toolStripSeparator5;
        private ToolStripSeparator toolStripSeparator6;
        private ToolStripSeparator toolStripSeparator7;
        private ToolStripSeparator toolStripSeparator8;
        private ToolStripMenuItem _miCut;
        private ToolStripMenuItem miInsertPageBreak;
        private IContainer components;
        #endregion

        #region PROPERTIES
        public bool DoNotStore
		{
			get {return this.bDoNotStore;}
			set {this.bDoNotStore = value;}
		}

		public string OfferedPrice
		{
			get {return this.offeredPrice;}
			set {this.offeredPrice = value;}
		}
		#endregion

		#region CTR
		public EditDocumentTemplateUserControl()
		{
			// This call is required by the Windows.Forms Form Designer.
			try
			{
				InitializeComponent();
				AddEventHanders();
				_images = new ArrayList();
				this.txTextControl1.Focus();
			}
			catch (Exception exc)
			{
				MessageBox.Show(exc.ToString());
			}
		}
		#endregion

		void AddEventHanders()
		{
			txTextControl1.MouseDown += new MouseEventHandler(On_TxCtrl_MouseDown);
			txTextControl1.MouseUp += new MouseEventHandler(On_TxCtrl_MouseUp);
			txTextControl1.MouseMove += new MouseEventHandler(On_TxCtrl_MouseMove);
            txTextControl1.Changed += TxTextControl1_Changed;
            

			_insertTextFrame.Click += new EventHandler(On_InsertTextFrame);
			_showTextFrameBorder.Click += new EventHandler(On_ShowTextFrameBorder);
            _setForegroundColor.Click += new EventHandler(On_SetForegroundColor_Click);
            _setBackgroundColor.Click += new EventHandler(On_SetBackgroundColor_Click);

            OnDocumentLoaded += Control_OnDocumentLoaded;
            OnDocumentCreated += Control_OnDocumentCreated;
            OnDocumentModified += Control_OnDocumentModified;
            OnDocumentSaved += Control_OnDocumentSaved;
            OnDocumentClosed += Control_OnDocumentClosed;
		}

        bool forcePrompt = false;
        bool isTemplate = false;
        private void Control_OnDocumentClosed()
        {
            forcePrompt = false;
        }

        private void Control_OnDocumentSaved(string documentName)
        {
            forcePrompt = false;
        }

        private void Control_OnDocumentModified(string documentName)
        {
            forcePrompt = true;
        }

        private void Control_OnDocumentCreated(string documentName)
        {
            forcePrompt = true;
        }

        private void Control_OnDocumentLoaded(string documentName, bool template)
        {
            forcePrompt = false;
        }

        private void TxTextControl1_Changed(object sender, EventArgs e)
        {
            if (null != OnDocumentModified)
                OnDocumentModified(documentName);
        }

        public event DocumentLoaded OnDocumentLoaded;
        public event DocumentClosed OnDocumentClosed;
        public event DocumentModified OnDocumentModified;
        public event DocumentSaved OnDocumentSaved;
        public event DocumentCreated OnDocumentCreated;

        public delegate void DocumentLoaded (string documentName, bool template);
        public delegate void DocumentClosed();
        public delegate void DocumentModified(string documentName);
        public delegate void DocumentSaved(string documentName);
        public delegate void DocumentCreated(string documentName);

        


        #region DISPOSE
        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}
		#endregion

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.ctxMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this._mi_Open_Template = new System.Windows.Forms.ToolStripMenuItem();
            this._mi_SaveTemplate = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this._mi_New = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this._miCut = new System.Windows.Forms.ToolStripMenuItem();
            this.miCopy = new System.Windows.Forms.ToolStripMenuItem();
            this.miPaste = new System.Windows.Forms.ToolStripMenuItem();
            this.miUndo = new System.Windows.Forms.ToolStripMenuItem();
            this.miRedo = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.miInsertPageBreak = new System.Windows.Forms.ToolStripMenuItem();
            this.miInsertPicture = new System.Windows.Forms.ToolStripMenuItem();
            this._insertTextFrame = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this._showTextFrameBorder = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this._miPageSetup = new System.Windows.Forms.ToolStripMenuItem();
            this._miPrint = new System.Windows.Forms.ToolStripMenuItem();
            this._mi_PrintPreview = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this._setForegroundColor = new System.Windows.Forms.ToolStripMenuItem();
            this._setBackgroundColor = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this._mi_OpenFile = new System.Windows.Forms.ToolStripMenuItem();
            this._mi_SaveFile = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.txButtonBar1 = new TXTextControl.ButtonBar();
            this.txRulerBar1 = new TXTextControl.RulerBar();
            this.txStatusBar1 = new TXTextControl.StatusBar();
            this.txTextControl1 = new TXTextControl.TextControl();
            this.ctxMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // ctxMenu
            // 
            this.ctxMenu.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.ctxMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._mi_Open_Template,
            this._mi_SaveTemplate,
            this.toolStripSeparator4,
            this._mi_New,
            this.toolStripSeparator1,
            this._miCut,
            this.miCopy,
            this.miPaste,
            this.miUndo,
            this.miRedo,
            this.toolStripSeparator3,
            this.miInsertPicture,
            this._insertTextFrame,
            this.miInsertPageBreak,
            this.toolStripSeparator5,
            this._showTextFrameBorder,
            this.toolStripSeparator6,
            this._miPageSetup,
            this._miPrint,
            this._mi_PrintPreview,
            this.toolStripSeparator7,
            this._setForegroundColor,
            this._setBackgroundColor,
            this.toolStripSeparator2,
            this._mi_OpenFile,
            this._mi_SaveFile,
            this.toolStripSeparator8});
            this.ctxMenu.Name = "ctxMenu";
            this.ctxMenu.Size = new System.Drawing.Size(262, 470);
            this.ctxMenu.Opened += new System.EventHandler(this.ctxMenu_Popup);
            // 
            // _mi_Open_Template
            // 
            this._mi_Open_Template.MergeIndex = 10;
            this._mi_Open_Template.Name = "_mi_Open_Template";
            this._mi_Open_Template.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this._mi_Open_Template.Size = new System.Drawing.Size(261, 22);
            this._mi_Open_Template.Text = "Open Mail Merge Template";
            this._mi_Open_Template.Click += new System.EventHandler(this.LoadDocumentTemplate);
            // 
            // _mi_SaveTemplate
            // 
            this._mi_SaveTemplate.MergeIndex = 11;
            this._mi_SaveTemplate.Name = "_mi_SaveTemplate";
            this._mi_SaveTemplate.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this._mi_SaveTemplate.Size = new System.Drawing.Size(261, 22);
            this._mi_SaveTemplate.Text = "Save Mail Merge Template";
            this._mi_SaveTemplate.Click += new System.EventHandler(this.SaveDocumentTemplate);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(258, 6);
            // 
            // _mi_New
            // 
            this._mi_New.MergeIndex = 0;
            this._mi_New.Name = "_mi_New";
            this._mi_New.Size = new System.Drawing.Size(261, 22);
            this._mi_New.Text = "New";
            this._mi_New.Click += new System.EventHandler(this.NewDocument);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(258, 6);
            // 
            // _miCut
            // 
            this._miCut.Name = "_miCut";
            this._miCut.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.X)));
            this._miCut.Size = new System.Drawing.Size(261, 22);
            this._miCut.Text = "Cut";
            this._miCut.Click += new System.EventHandler(this._miCut_Click);
            // 
            // miCopy
            // 
            this.miCopy.MergeIndex = 2;
            this.miCopy.Name = "miCopy";
            this.miCopy.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.C)));
            this.miCopy.Size = new System.Drawing.Size(261, 22);
            this.miCopy.Text = "Copy";
            this.miCopy.Click += new System.EventHandler(this.miCopy_Click);
            // 
            // miPaste
            // 
            this.miPaste.MergeIndex = 3;
            this.miPaste.Name = "miPaste";
            this.miPaste.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.V)));
            this.miPaste.Size = new System.Drawing.Size(261, 22);
            this.miPaste.Text = "Paste";
            this.miPaste.Click += new System.EventHandler(this.miPaste_Click);
            // 
            // miUndo
            // 
            this.miUndo.MergeIndex = 4;
            this.miUndo.Name = "miUndo";
            this.miUndo.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Z)));
            this.miUndo.Size = new System.Drawing.Size(261, 22);
            this.miUndo.Text = "Undo";
            this.miUndo.Click += new System.EventHandler(this.miUndo_Click);
            // 
            // miRedo
            // 
            this.miRedo.MergeIndex = 5;
            this.miRedo.Name = "miRedo";
            this.miRedo.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Y)));
            this.miRedo.Size = new System.Drawing.Size(261, 22);
            this.miRedo.Text = "Redo";
            this.miRedo.Click += new System.EventHandler(this.miRedo_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(258, 6);
            // 
            // miInsertPageBreak
            // 
            this.miInsertPageBreak.Name = "miInsertPageBreak";
            this.miInsertPageBreak.Size = new System.Drawing.Size(261, 22);
            this.miInsertPageBreak.Text = "Insert Page Break";
            this.miInsertPageBreak.Click += new System.EventHandler(this.miInsertPageBreak_Click);
            // 
            // miInsertPicture
            // 
            this.miInsertPicture.MergeIndex = 13;
            this.miInsertPicture.Name = "miInsertPicture";
            this.miInsertPicture.Size = new System.Drawing.Size(261, 22);
            this.miInsertPicture.Text = "Insert Picture";
            this.miInsertPicture.Click += new System.EventHandler(this.miInsertPicture_Click);
            // 
            // _insertTextFrame
            // 
            this._insertTextFrame.MergeIndex = 14;
            this._insertTextFrame.Name = "_insertTextFrame";
            this._insertTextFrame.Size = new System.Drawing.Size(261, 22);
            this._insertTextFrame.Text = "Insert Text Frame";
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(258, 6);
            // 
            // _showTextFrameBorder
            // 
            this._showTextFrameBorder.Checked = true;
            this._showTextFrameBorder.CheckState = System.Windows.Forms.CheckState.Checked;
            this._showTextFrameBorder.MergeIndex = 16;
            this._showTextFrameBorder.Name = "_showTextFrameBorder";
            this._showTextFrameBorder.Size = new System.Drawing.Size(261, 22);
            this._showTextFrameBorder.Text = "Show Text Frame Border";
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(258, 6);
            // 
            // _miPageSetup
            // 
            this._miPageSetup.MergeIndex = 18;
            this._miPageSetup.Name = "_miPageSetup";
            this._miPageSetup.Size = new System.Drawing.Size(261, 22);
            this._miPageSetup.Text = "Page Setup";
            this._miPageSetup.Click += new System.EventHandler(this.PageSetup_EH);
            // 
            // _miPrint
            // 
            this._miPrint.MergeIndex = 19;
            this._miPrint.Name = "_miPrint";
            this._miPrint.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.P)));
            this._miPrint.Size = new System.Drawing.Size(261, 22);
            this._miPrint.Text = "Print";
            this._miPrint.Click += new System.EventHandler(this.PrintDocument);
            // 
            // _mi_PrintPreview
            // 
            this._mi_PrintPreview.MergeIndex = 20;
            this._mi_PrintPreview.Name = "_mi_PrintPreview";
            this._mi_PrintPreview.Size = new System.Drawing.Size(261, 22);
            this._mi_PrintPreview.Text = "Print Preview";
            this._mi_PrintPreview.Click += new System.EventHandler(this.PrintPreview_EH);
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(258, 6);
            // 
            // _setForegroundColor
            // 
            this._setForegroundColor.MergeIndex = 22;
            this._setForegroundColor.Name = "_setForegroundColor";
            this._setForegroundColor.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.T)));
            this._setForegroundColor.Size = new System.Drawing.Size(261, 22);
            this._setForegroundColor.Text = "Set Text Color";
            // 
            // _setBackgroundColor
            // 
            this._setBackgroundColor.MergeIndex = 23;
            this._setBackgroundColor.Name = "_setBackgroundColor";
            this._setBackgroundColor.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.H)));
            this._setBackgroundColor.Size = new System.Drawing.Size(261, 22);
            this._setBackgroundColor.Text = "Highlight Text Color";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(258, 6);
            // 
            // _mi_OpenFile
            // 
            this._mi_OpenFile.MergeIndex = 7;
            this._mi_OpenFile.Name = "_mi_OpenFile";
            this._mi_OpenFile.Size = new System.Drawing.Size(261, 22);
            this._mi_OpenFile.Text = "Open Other File Format";
            this._mi_OpenFile.Click += new System.EventHandler(this.OpenDocument);
            // 
            // _mi_SaveFile
            // 
            this._mi_SaveFile.MergeIndex = 8;
            this._mi_SaveFile.Name = "_mi_SaveFile";
            this._mi_SaveFile.Size = new System.Drawing.Size(261, 22);
            this._mi_SaveFile.Text = "Save Other File Format";
            this._mi_SaveFile.Click += new System.EventHandler(this.SaveDocument);
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(258, 6);
            // 
            // txButtonBar1
            // 
            this.txButtonBar1.BackColor = System.Drawing.SystemColors.Control;
            this.txButtonBar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.txButtonBar1.Location = new System.Drawing.Point(0, 0);
            this.txButtonBar1.Name = "txButtonBar1";
            this.txButtonBar1.Size = new System.Drawing.Size(696, 28);
            this.txButtonBar1.TabIndex = 10;
            this.txButtonBar1.Text = "buttonBar1";
            // 
            // txRulerBar1
            // 
            this.txRulerBar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.txRulerBar1.Location = new System.Drawing.Point(0, 28);
            this.txRulerBar1.Name = "txRulerBar1";
            this.txRulerBar1.Size = new System.Drawing.Size(696, 25);
            this.txRulerBar1.TabIndex = 11;
            this.txRulerBar1.Text = "rulerBar1";
            // 
            // txStatusBar1
            // 
            this.txStatusBar1.BackColor = System.Drawing.SystemColors.Control;
            this.txStatusBar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.txStatusBar1.Location = new System.Drawing.Point(0, 523);
            this.txStatusBar1.Name = "txStatusBar1";
            this.txStatusBar1.Size = new System.Drawing.Size(696, 22);
            this.txStatusBar1.TabIndex = 12;
            // 
            // txTextControl1
            // 
            this.txTextControl1.ContextMenuStrip = this.ctxMenu;
            this.txTextControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txTextControl1.Font = new System.Drawing.Font("Arial", 10F);
            this.txTextControl1.Location = new System.Drawing.Point(0, 53);
            this.txTextControl1.Name = "txTextControl1";
            this.txTextControl1.PageMargins.Bottom = 79.03D;
            this.txTextControl1.PageMargins.Left = 79.03D;
            this.txTextControl1.PageMargins.Right = 79.03D;
            this.txTextControl1.PageMargins.Top = 79.03D;
            this.txTextControl1.Size = new System.Drawing.Size(696, 470);
            this.txTextControl1.TabIndex = 13;
            // 
            // EditDocumentTemplateUserControl
            // 
            this.Controls.Add(this.txTextControl1);
            this.Controls.Add(this.txStatusBar1);
            this.Controls.Add(this.txRulerBar1);
            this.Controls.Add(this.txButtonBar1);
            this.Name = "EditDocumentTemplateUserControl";
            this.Size = new System.Drawing.Size(696, 545);
            this.Load += new System.EventHandler(this.EditDocumentTemplateUserControl_Load);
            this.Leave += new System.EventHandler(this.EditDocumentTemplateUserControl_Leave);
            this.ctxMenu.ResumeLayout(false);
            this.ResumeLayout(false);

		}
		#endregion

		private void On_TxCtrl_MouseDown(object sender, MouseEventArgs e)
		{
			if (e.Button == MouseButtons.Right)			
				return;									

			if (_insertFrame)
			{
				_panel = new Panel();
				_panel.Location = new Point(e.X, e.Y);
				_panel.Size = new Size(100, 100);				
				_panel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
				txTextControl1.Controls.Add(_panel);
			}				
		}

		private void On_TxCtrl_MouseUp(object sender, MouseEventArgs e)
		{
			if (e.Button == MouseButtons.Right)			
				return;
			
			
			if (_insertFrame && null != _panel)
			{
				_insertFrame = false;
				txTextControl1.Cursor = Cursors.Arrow;

				TXTextControl.TextFrame tf = new TXTextControl.TextFrame(new Size(_panel.Width * 15, _panel.Height * 15));
				

				Point tf_pos = new Point(txTextControl1.ScrollLocation.X + _panel.Left * 15, txTextControl1.ScrollLocation.Y + _panel.Top * 15);
            
				if (txTextControl1.ViewMode == TXTextControl.ViewMode.PageView)
				{
					tf_pos.X -= (int)(txTextControl1.PageMargins.Left * 15);
					tf_pos.Y -= (int)(txTextControl1.PageMargins.Top * 15) + 567;
				}

				txTextControl1.TextFrames.Add(tf, tf_pos, 0, TXTextControl.TextFrameInsertionMode.DisplaceText);
				txTextControl1.Controls.Remove(_panel);
			}

			UpdateCursor();
		}

		private void On_TxCtrl_MouseMove(object sender, MouseEventArgs e)
		{			
			if (_insertFrame & null != _panel)
			{
				_panel.Width = e.X - _panel.Location.X;
				_panel.Height = e.Y - _panel.Location.Y;				
			}
		}
        
        void PromptForSave()
        {
            if (forcePrompt)
            {
                DialogResult dr = MessageBox.Show("Save changes?", "Unsaved Changes", MessageBoxButtons.YesNo, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                if (DialogResult.Yes == dr)
                {
                    if (this.txTextControl1.TextFields.Count > 0)
                        this.SaveDocumentTemplate(null, null);
                    else
                        this.SaveDocument(null, null);

                    return;
                }
            }
        }

		#region NEW DOCUMENT
		/// <summary>
		/// New Document
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void NewDocument(object sender, System.EventArgs e)
		{
            PromptForSave();

            if (false == txTextControl1.Focused)
				txTextControl1.Focus();

			txTextControl1.SelectAll();
			txTextControl1.Clear();	
			
			foreach (TXTextControl.Image img in _images)
			{
				try { txTextControl1.Images.Remove(img); }
				catch {}				
			}
			_images.Clear();

						
			try { txTextControl1.TextFrames.Clear(); }
			catch {}

            txTextControl1.TextFields.Clear(false);
		
            
            txTextControl1.PageSize = new TXTextControl.PageSize(850, 1100);
            txTextControl1.PageMargins = new TXTextControl.PageMargins(79, 79, 79, 79);
            txTextControl1.Landscape = false;

            documentName = NewDocumentName;
            if (null != OnDocumentCreated)
                OnDocumentCreated(documentName);

		}
		#endregion

		#region OPEN DOCUMENT
		private void OpenDocument(object sender, System.EventArgs e)
		{           
            try
			{
                PromptForSave();

                //txTextControl1.Load();
                //TA++ 2006, April 16
                //Setting initial path to open file dialog
                TXTextControl.LoadSettings loadSettings = new TXTextControl.LoadSettings();
				OpenFileDialog dlgOpenFile = new OpenFileDialog();			
				dlgOpenFile.InitialDirectory = Application.StartupPath + Globals.DocumentsPath;
				dlgOpenFile.Filter = "Text Files (*.docx, *.doc, *.rtf)|*.docx;*.doc;*.rtf | Rich Text Format Files (*.rtf)|*.rtf|MS Word Files (*.doc)|*.doc|MS Word Files (*.docx)|*.docx";			
				dlgOpenFile.ShowDialog();
				try
				{
					if (dlgOpenFile.FileName != "") 
					{
                        string ext = Path.GetExtension(dlgOpenFile.FileName);
                        ext = ext.Replace(".", string.Empty);
						//string ext = dlgOpenFile.FileName.Substring(dlgOpenFile.FileName.Length - 3, 3);
						if (ext.ToUpper() == "DOC")
						{
							txTextControl1.Load(dlgOpenFile.FileName, TXTextControl.StreamType.MSWord, loadSettings);

                            documentName = Path.GetFileNameWithoutExtension(dlgOpenFile.FileName);

                            if (null != OnDocumentLoaded)
                                OnDocumentLoaded(Path.GetFileNameWithoutExtension(dlgOpenFile.FileName), false);
                        }
                        if (ext.ToUpper() == "DOCX")
                        {
                            txTextControl1.Load(dlgOpenFile.FileName, TXTextControl.StreamType.WordprocessingML, loadSettings);

                            documentName = Path.GetFileNameWithoutExtension(dlgOpenFile.FileName);

                            if (null != OnDocumentLoaded)
                                OnDocumentLoaded(Path.GetFileNameWithoutExtension(dlgOpenFile.FileName), false);
                        }
                        else if (ext.ToUpper() == "RTF")
						{
							txTextControl1.Load(dlgOpenFile.FileName, TXTextControl.StreamType.RichTextFormat, loadSettings);

                            documentName = Path.GetFileNameWithoutExtension(dlgOpenFile.FileName);

                            if (null != OnDocumentLoaded)
                                OnDocumentLoaded(Path.GetFileNameWithoutExtension(dlgOpenFile.FileName), false);
                        }
						else
							return;

						this.txTextControl1.PageMargins = loadSettings.PageMargins;
						this.txTextControl1.PageSize = loadSettings.PageSize;
					}
				}
				catch (Exception exc)
				{
					string s = exc.Message;
				}
				this.txTextControl1.Focus();
				//TA--
			}
			catch (Exception exc)
			{
				MessageBox.Show(exc.Message);
				return;
			}
		}
		#endregion

		#region SAVE DOCUMENT
		public void SaveDocument(object sender, System.EventArgs e)
		{
			System.Windows.Forms.SaveFileDialog dlgSaveFile = new SaveFileDialog();
            dlgSaveFile.Filter = "Text Control Files (*.rtf)|*.rtf|MS Word Files (*.doc)|*.doc|Adobe PDF Files (*.pdf)|*.pdf";

            dlgSaveFile.FileName = documentName;

            dlgSaveFile.ShowDialog();

			if (dlgSaveFile.FileName != "") 
			{
				string ext = dlgSaveFile.FileName.Substring(dlgSaveFile.FileName.Length - 3, 3);
				if (ext.ToUpper() == "DOC")
				{
					txTextControl1.Save(dlgSaveFile.FileName, TXTextControl.StreamType.MSWord);

                    documentName = Path.GetFileNameWithoutExtension(dlgSaveFile.FileName);

                    if (null != OnDocumentSaved)                    
                        OnDocumentSaved(Path.GetFileNameWithoutExtension(dlgSaveFile.FileName));
                    
                }
				else if (ext.ToUpper() == "RTF")
				{
					txTextControl1.Save(dlgSaveFile.FileName, TXTextControl.StreamType.RichTextFormat);

                    documentName = Path.GetFileNameWithoutExtension(dlgSaveFile.FileName);

                    if (null != OnDocumentSaved)                     
                        OnDocumentSaved(Path.GetFileNameWithoutExtension(dlgSaveFile.FileName));
                }
                else if (ext.ToUpper() == "PDF")
                {
                    txTextControl1.Save(dlgSaveFile.FileName, TXTextControl.StreamType.AdobePDF);

                    documentName = Path.GetFileNameWithoutExtension(dlgSaveFile.FileName);

                    if (null != OnDocumentSaved)
                        OnDocumentSaved(Path.GetFileNameWithoutExtension(dlgSaveFile.FileName));
                }
				else
					return;
			}
		}
		#endregion

		#region SAVE FILE RTF
		public void SaveFile_RTF(string fileName)
		{
			string dir = Application.StartupPath + @"\DocsToFax\";
			if (!Directory.Exists(dir))
				Directory.CreateDirectory(dir);

			try
			{
				txTextControl1.Save(dir + fileName + ".rtf", TXTextControl.StreamType.RichTextFormat);
			}
			catch (Exception exc)
			{
				//just avoid crash
				string error = exc.Message;
			}
		}
        #endregion

        const string NewDocumentName = "New Document";
        public string documentName = NewDocumentName;
		#region LOAD DOCUMENT TEMPLATE
		/// <summary>
		/// Load document template
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void LoadDocumentTemplate(object sender, System.EventArgs e)
		{
			try
			{
                PromptForSave();

                TXTextControl.LoadSettings loadSettings = new TXTextControl.LoadSettings();
				OpenFileDialog dlgOpenFile = new OpenFileDialog();			
				dlgOpenFile.InitialDirectory = Application.StartupPath + Globals.DocumentTemplatesPath;
				dlgOpenFile.Filter = "Text Control Files (*.tx)|*.tx";			
				dlgOpenFile.ShowDialog();
				if (dlgOpenFile.FileName != "") 
				{
                    documentName = Path.GetFileNameWithoutExtension(dlgOpenFile.FileName);
                    txTextControl1.Load(dlgOpenFile.FileName, TXTextControl.StreamType.InternalFormat, loadSettings);
					this.txTextControl1.PageMargins = loadSettings.PageMargins;
					this.txTextControl1.PageSize = loadSettings.PageSize;

                    if (null != OnDocumentLoaded)
                        OnDocumentLoaded(Path.GetFileNameWithoutExtension(dlgOpenFile.FileName), true);
                }
			}
			catch (Exception exc)
			{
				string s = exc.Message;
			}
			this.txTextControl1.Focus();
		}
		#endregion

		#region SAVE DOCUMENT TEMPLATE
		/// <summary>
		/// Save document template
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		public void SaveDocumentTemplate(object sender, System.EventArgs e)
		{
			TXTextControl.SaveSettings saveSettings = new TXTextControl.SaveSettings();
			saveSettings.PageMargins = this.txTextControl1.PageMargins;
			saveSettings.PageSize	 = this.txTextControl1.PageSize;
			
			SaveFileDialog dlgSaveFile = new SaveFileDialog();
			dlgSaveFile.Filter = "Text Control Files (*.tx)|*.tx";

            dlgSaveFile.FileName = documentName;

            DialogResult dr = dlgSaveFile.ShowDialog();

            if (dlgSaveFile.FileName != "")
            {
                txTextControl1.Save(dlgSaveFile.FileName, TXTextControl.StreamType.InternalFormat, saveSettings);

                if (null != OnDocumentSaved)
                    OnDocumentSaved(Path.GetFileNameWithoutExtension(dlgSaveFile.FileName));
            }
		}
		#endregion

		#region PRINT & PRINT PREVIEW
		private void PrintDocument(object sender, System.EventArgs e)
		{
			this.PrintWithPrintDialog();
		}		
		
		public void PrintWithPrintDialog()
		{
			try
			{
				PrintDialog myPrintDialog = new PrintDialog();
				PrintDocument myPrintDocument = new PrintDocument();

				myPrintDialog.Document = myPrintDocument;
				myPrintDialog.AllowSomePages = false;
				myPrintDialog.AllowPrintToFile = false;
				myPrintDialog.PrinterSettings.FromPage = 1;
				myPrintDialog.PrinterSettings.ToPage = txTextControl1.Pages;

                //Landscape
                myPrintDialog.PrinterSettings.DefaultPageSettings.Landscape = txTextControl1.Landscape;
                
                //Margins
                myPrintDialog.PrinterSettings.DefaultPageSettings.Margins.Top = (int)txTextControl1.PageMargins.Top;
                myPrintDialog.PrinterSettings.DefaultPageSettings.Margins.Bottom = (int)txTextControl1.PageMargins.Bottom;
                myPrintDialog.PrinterSettings.DefaultPageSettings.Margins.Left = (int)txTextControl1.PageMargins.Left;
                myPrintDialog.PrinterSettings.DefaultPageSettings.Margins.Right = (int)txTextControl1.PageMargins.Right;

                //PageSize                                
                for (int i = 0; i < myPrintDialog.PrinterSettings.PaperSizes.Count - 1; i++)
                {
                    System.Drawing.Printing.PaperSize size = myPrintDialog.PrinterSettings.PaperSizes[i];

                    if (size.Height == txTextControl1.PageSize.Height & size.Width == txTextControl1.PageSize.Width)
                    {
                        myPrintDialog.PrinterSettings.DefaultPageSettings.PaperSize = size;
                        break;
                    }
                }
                
				if (myPrintDialog.ShowDialog() == DialogResult.OK )
				{                    
                    myPrintDocument.DefaultPageSettings = myPrintDialog.PrinterSettings.DefaultPageSettings;
					txTextControl1.Print(myPrintDocument);
				}
			}
			catch(Exception exc)
			{
				MessageBox.Show(exc.Message);
			}
		}

        PrintDialog _cachedPrintDialog = null;
        public void PrintDirect(string docName)
		{
			try
            {
                bool continueWithPrint = true;
										
				//print it
				PrintDialog myPrintDialog = new PrintDialog();
				PrintDocument myPrintDocument = new PrintDocument();

                myPrintDialog.Document = myPrintDocument;
				myPrintDocument.DocumentName = docName;

                if (null == _cachedPrintDialog)
                {
                    //Landscape
                    myPrintDialog.PrinterSettings.DefaultPageSettings.Landscape = txTextControl1.Landscape;

                    //Margins
                    myPrintDialog.PrinterSettings.DefaultPageSettings.Margins.Top = (int)txTextControl1.PageMargins.Top;
                    myPrintDialog.PrinterSettings.DefaultPageSettings.Margins.Bottom = (int)txTextControl1.PageMargins.Bottom;
                    myPrintDialog.PrinterSettings.DefaultPageSettings.Margins.Left = (int)txTextControl1.PageMargins.Left;
                    myPrintDialog.PrinterSettings.DefaultPageSettings.Margins.Right = (int)txTextControl1.PageMargins.Right;

                    //PageSize                                
                    for (int i = 0; i < myPrintDialog.PrinterSettings.PaperSizes.Count - 1; i++)
                    {
                        System.Drawing.Printing.PaperSize size = myPrintDialog.PrinterSettings.PaperSizes[i];

                        if (size.Height == txTextControl1.PageSize.Height & size.Width == txTextControl1.PageSize.Width)
                        {
                            myPrintDialog.PrinterSettings.DefaultPageSettings.PaperSize = size;
                            break;
                        }
                    }

                    myPrintDocument.PrinterSettings.FromPage = 1;
                    myPrintDocument.PrinterSettings.ToPage = this.txTextControl1.Pages;
                    myPrintDocument.PrinterSettings.Copies = 1;
                }

                
                if (null == _cachedPrintDialog && myPrintDialog.ShowDialog() == DialogResult.OK)                                    
                    _cachedPrintDialog = myPrintDialog;                
                else if (null == _cachedPrintDialog && myPrintDialog.ShowDialog() != DialogResult.OK)                
                    continueWithPrint = false;                
                else
                    myPrintDocument.PrinterSettings = _cachedPrintDialog.PrinterSettings;

                if (continueWithPrint)
                {
                    myPrintDocument.DefaultPageSettings = myPrintDialog.PrinterSettings.DefaultPageSettings;
                    this.txTextControl1.Print(myPrintDocument);                    
                }
			}
			catch(Exception exc)
			{
				MessageBox.Show(exc.Message);
			}
		}

		private void PrintPreview_EH(object sender, System.EventArgs e)
		{
			this.PrintPreviewFunction();
		}

		public void PrintPreviewFunction()
		{
			try
			{
				PrintDocument pd = new PrintDocument();

                //Landscape
                pd.PrinterSettings.DefaultPageSettings.Landscape = txTextControl1.Landscape;
                
                //Margins
                pd.PrinterSettings.DefaultPageSettings.Margins.Top = (int)txTextControl1.PageMargins.Top;
                pd.PrinterSettings.DefaultPageSettings.Margins.Bottom = (int)txTextControl1.PageMargins.Bottom;
                pd.PrinterSettings.DefaultPageSettings.Margins.Left = (int)txTextControl1.PageMargins.Left;
                pd.PrinterSettings.DefaultPageSettings.Margins.Right = (int)txTextControl1.PageMargins.Right;

                //PageSize                                
                for(int i = 0;i < pd.PrinterSettings.PaperSizes.Count - 1; i++)
                {
                    System.Drawing.Printing.PaperSize size = pd.PrinterSettings.PaperSizes[i];

                    if (size.Height == txTextControl1.PageSize.Height & size.Width == txTextControl1.PageSize.Width)
                    {
                        pd.PrinterSettings.DefaultPageSettings.PaperSize = size;                        
                        break;
                    }
                                            
                }

                pd.DefaultPageSettings = pd.PrinterSettings.DefaultPageSettings;
                txTextControl1.PrintPreview(pd);
			}
			catch(Exception exc)
			{
				MessageBox.Show(exc.Message);
			}
		}
		#endregion

		#region CTX MENU POPUP
		private void ctxMenu_Popup(object sender, System.EventArgs e)
		{
            //BEGIN: ENABLE FORE/BACK COLOR SET
            bool textColorPropertyEnabled = false;

            if ((null != txTextControl1.Selection) && (null != txTextControl1.Selection.Text) && (string.Empty != txTextControl1.Selection.Text))
                textColorPropertyEnabled = true;                                        

            _setBackgroundColor.Enabled = _setForegroundColor.Enabled = textColorPropertyEnabled;
            //END: ENABLE FORE/BACK COLOR SET

            if (false == bAddded)
			{
				ContextMenuStrip cm = (ContextMenuStrip)sender;
                ToolStripMenuItem[] items = new ToolStripMenuItem[MailMergeMapper.MailMergeMappingFields.Length];
				int i = 0;
				foreach (string s in MailMergeMapper.MailMergeMappingFields)
				{
                    ToolStripMenuItem mi = new ToolStripMenuItem(s);
					mi.Click += new System.EventHandler(this.Fill);
					items[i] = mi;
					i++;
				}
				//cm.Items.Add("Property Item fields", items);
                ToolStripMenuItem pif = new ToolStripMenuItem("Property Item fields");
                pif.DropDownItems.AddRange(items);
                cm.Items.Add(pif);

                ToolStripMenuItem[] userMapperItems = new ToolStripMenuItem[UserMapper.UserMappingFields.Length];
				i = 0;
				foreach (string s in UserMapper.UserMappingFields)
				{                    
                    ToolStripMenuItem mi = new ToolStripMenuItem(s);
					mi.Click += new System.EventHandler(this.Fill);
					userMapperItems[i] = mi;
					i++;
				}
				//cm.MenuItems.Add("User Mapping Fields", userMapperItems);
                ToolStripMenuItem umf = new ToolStripMenuItem("User Mapping Fields");
                umf.DropDownItems.AddRange(userMapperItems);
                cm.Items.Add(umf);

                ToolStripMenuItem[] privateLendersCashBuyersMapperItems = new ToolStripMenuItem[PrivateLenderCashBuyersMapper.MappingFields.Length];
                i = 0;
                foreach (string s in PrivateLenderCashBuyersMapper.MappingFields)
                {
                    ToolStripMenuItem mi = new ToolStripMenuItem(s);
                    mi.Tag = "CBPL";
                    mi.Click += new System.EventHandler(this.Fill);
                    privateLendersCashBuyersMapperItems[i] = mi;
                    i++;
                }
                //cm.MenuItems.Add("Other Merging Fields", privateLendersCashBuyersMapperItems);
                ToolStripMenuItem omf = new ToolStripMenuItem("Other Merging Fields");
                omf.DropDownItems.AddRange(privateLendersCashBuyersMapperItems);
                cm.Items.Add(omf);

                bAddded = true;
			}
		}
		#endregion

        #region UPDATE CURSOR
        void UpdateCursor()
		{
			if (_insertFrame)
				txTextControl1.Cursor = Cursors.Cross;
			else
				txTextControl1.Cursor = Cursors.Arrow;
        }
        #endregion

        #region FILL
        private void Fill(object sender, System.EventArgs e)
		{

			TXTextControl.TextField textField = new TXTextControl.TextField();

			textField.Text = "(" + ((ToolStripMenuItem)sender).Text + ")";
			textField.Name = ((ToolStripMenuItem)sender).Text;

            string tag = (string)(((ToolStripMenuItem)sender).Tag);
            if ((null != tag) && (-1 != tag.IndexOf("CBPL")))
                textField.Name = "CBPL" + textField.Name;

			textField.ShowActivated = true;
			textField.DoubledInputPosition = true;
			txTextControl1.TextFields.Add(textField);

            txTextControl1.Refresh();
        }
        #endregion

        #region ON LOAD
        private void EditDocumentTemplateUserControl_Load(object sender, System.EventArgs e)
		{
			txTextControl1.ButtonBar = txButtonBar1;
			txTextControl1.RulerBar = txRulerBar1;
			txTextControl1.StatusBar = txStatusBar1;			
		}
		#endregion

		#region PAGE SETUP
		private void PageSetup_EH(object sender, System.EventArgs e)
		{
			PageSetup ps = new PageSetup();
			ps.ShowDialog(ref txTextControl1);
		}
		#endregion

		#region ON LEAVE CONTROL
		private void EditDocumentTemplateUserControl_Leave(object sender, System.EventArgs e)
		{
			if (false == this.bDoNotStore)
			{
				Globals.oldEditDocumentTemplateUserControl = this;
			}            
		}
		#endregion

		#region LOAD DOCUMENT
		public void LoadDoc (string doc)
		{
			try
			{
				TXTextControl.LoadSettings loadSettings = new TXTextControl.LoadSettings();
				txTextControl1.Load(doc, TXTextControl.StreamType.InternalFormat, loadSettings);
				this.txTextControl1.PageMargins = loadSettings.PageMargins;
				this.txTextControl1.PageSize = loadSettings.PageSize;
			}
			catch (Exception exc)
			{
				MessageBox.Show(exc.Message + " " + exc.Source);
			}
		}
		#endregion

		#region MERGE
        ContactTypes contactType = ContactTypes.Undefined;
		public void MergeMappingFields(PropertyItem pi, bool usePercentage, double percentage)
		{                        
            contactType = ContactTypesManager.Instance.GetCustomTypeGroup(pi.IdPropertyItem);

            if (null != txTextControl1.TextFields)
            {
                foreach (TXTextControl.TextField Field in txTextControl1.TextFields)
                {
                    if (0 == Field.Name.IndexOf("CBPL"))
                    {
                        if (ContactTypes.Seller == contactType)
                        {
                            MessageBox.Show("This group does not contain Buyer/Lender " + Field.Name + "merging field" + Environment.NewLine + "Cash Buyers and Private Lenders merging fields can only be used in their respective groups!", "ProfitGrabber Pro", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            return;
                        }
                    }
                }
            }


			if (null != txTextControl1.TextFields)
			{
				foreach (TXTextControl.TextField Field in txTextControl1.TextFields) 
				{									
					{
						if (true == usePercentage)
						{
                            if (Field.Name == Globals.OfferedPrice || Field.Name == Globals.AlphaOfferedPrice)
							{
								double offeredPrice = 0;
								try
								{
									offeredPrice = pi.ListingAgent.ListingPrice * percentage / 100;

									//ROUND offered price
									offeredPrice = Math.Round(offeredPrice);
								}
								catch (Exception)
								{
									offeredPrice = 0;
								}
								string _price = offeredPrice.ToString("c");
								_price = _price.Replace("$", string.Empty);								
								//Field.Text = offeredPrice.ToString("c");

                                if (Field.Name == Globals.AlphaOfferedPrice)
                                    _price = new NumberToEnglish().changeNumericToWords(offeredPrice);

								Field.Text = _price;
							}
							else
							{
								InnerMailMerge(pi, Field);
							}
						}
						else
						{
							InnerMailMerge(pi, Field);
						}
					}			
				}
			}
		}

        User usr = null;        
		private void InnerMailMerge(PropertyItem pi, TXTextControl.TextField Field)
		{
            string textValue = string.Empty;                        
            if (0 == Field.Name.IndexOf("CBPL"))
            {
                string fieldName = Field.Name.Substring(4, Field.Name.Length - 4);
                Field.Text = PrivateLenderCashBuyersMapper.GetStringFromPropertyItem(pi, new MailMergeMapper(), fieldName);
                return;
            }

            if (null == usr)
                usr = ApplicationHelper.LoadUser();

			
			try
			{
				textValue = new UserMapper().GetStringFromUserObject(usr/*ApplicationHelper.LoadUser()*/, Field.Name);					
			}
			catch (Exception)
			{
				textValue = string.Empty;
			}

			if (string.Empty != textValue)
			{							
				Field.Text = textValue;							
			}
			else
			{
				
				textValue = MailMergeMapper.GetStringFromPropertyItem(pi, new MailMergeMapper(), Field.Name);
				if (string.Empty == textValue && "First Owner Name" == Field.Name)
					textValue = "Homeowner";

				Field.Text = textValue;
			}
		}
        #endregion

        #region CUT, COPY, PASTE, UNDO, REDO
        private void _miCut_Click(object sender, EventArgs e)
        {
            this.txTextControl1.Cut();
        }

        private void miCopy_Click(object sender, System.EventArgs e)
		{
			this.txTextControl1.Copy();
		}

		private void miPaste_Click(object sender, System.EventArgs e)
		{
			this.txTextControl1.Paste();			
		}

		private void miUndo_Click(object sender, System.EventArgs e)
		{
			this.txTextControl1.Undo();
		}

		private void miRedo_Click(object sender, System.EventArgs e)
		{
			this.txTextControl1.Redo();			
		}
		#endregion

		#region INSERT PICTURE
		private void miInsertPicture_Click(object sender, System.EventArgs e)
		{
			OpenFileDialog ofd = new OpenFileDialog();
			ofd.InitialDirectory = @"c:\" ;			

			ofd.Filter = "Images (*.bmp;*.gif;*.jpg;*.png;*.tif;*.wmf)|*.bmp;*.gif;*.jpg;*.png;*.tif;*.wmf|BMP Image (*.bmp)|*.bmp|GIF Image (*.gif)|*.gif|JPG Image (*.jpg)|*.jpg|PNG Image (*.png)|*.png|TIF Image (*.tif)|*.tif|WMF Image (*.wmf)|*.wmf";
			ofd.FilterIndex = 1 ;
			ofd.RestoreDirectory = true ;
						

			if (DialogResult.OK == ofd.ShowDialog())
			{
				TXTextControl.Image inputImage = new TXTextControl.Image();
				_images.Add(inputImage);
				Point location = this.txTextControl1.InputPosition.Location;
				int position = this.txTextControl1.InputPosition.TextPosition;

				inputImage.FileName = ofd.FileName;
				inputImage.Sizeable = true;
				inputImage.Moveable = true;
				inputImage.HorizontalScaling = 100;
				inputImage.VerticalScaling = 100;

				inputImage.SaveMode = TXTextControl.ImageSaveMode.SaveAsData;				
				inputImage.ExportFilterIndex = 1;

				

				//this.txTextControl1.Images.Add(inputImage, location, position, TXTextControl.ImageInsertionMode.DisplaceText);
				this.txTextControl1.Images.Add(inputImage, position);
			}
		}
		#endregion

        #region HANDLE TEXT FRAMEs
        private void On_InsertTextFrame(object sender, EventArgs e)
		{
			_insertFrame = true;	
			UpdateCursor();
		}

		private void On_ShowTextFrameBorder(object sender, EventArgs e)
		{
			foreach (TXTextControl.TextFrame frame in txTextControl1.TextFrames)
			{
				if (_showTextFrameBorder.Checked)
					frame.BorderWidth = 0;
				else
					frame.BorderWidth = 1;
			}

			_showTextFrameBorder.Checked = !_showTextFrameBorder.Checked;
        }
        #endregion

        #region SET TEXT COLOR
        void On_SetForegroundColor_Click(object sender, EventArgs e)
        {
            ColorDialog cd = new ColorDialog();            

            if (DialogResult.OK == cd.ShowDialog())
            {
                txTextControl1.Selection.ForeColor = cd.Color;
            }
        }

        void On_SetBackgroundColor_Click(object sender, EventArgs e)
        {
            ColorDialog cd = new ColorDialog();

            if (DialogResult.OK == cd.ShowDialog())
            {
                txTextControl1.Selection.TextBackColor = cd.Color;
            }
        }


        #endregion

        private void miInsertPageBreak_Click(object sender, EventArgs e)
        {
            txTextControl1.Selection.Text = "\f";
            System.Drawing.Point scrollPos = new Point(0, txTextControl1.InputPosition.Location.Y);
            txTextControl1.ScrollLocation = scrollPos;
            txTextControl1.Focus();
        }
    }
}
