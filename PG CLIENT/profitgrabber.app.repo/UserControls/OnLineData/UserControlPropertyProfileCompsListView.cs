#define NEW_DTD
//#define DUMP_XML_OBJECT
//#define SHOW_CHARGED

using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;
using System.IO;
using System.Xml.Serialization;
using System.Web;
using System.Text;
using System.Xml;
using System.Net;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Drawing.Printing;
using System.Collections.Generic;
//Added

using ProfitGrabber.Common;
using DealMaker.PlainClasses.OnLineData.COMPS;
using DealMaker.PlainClasses.OnLineData.PropertyProfile;

namespace DealMaker.OnlineData
{
	/// <summary>
	/// Summary description for UserControlPropertyProfileCompsListView.
	/// </summary>
	public class UserControlPropertyProfileCompsListView : System.Windows.Forms.UserControl
    {
        #region DATA
        public delegate void Success_COMPS_Download();
		public event Success_COMPS_Download COMPS_Downloaded_Successfully;
		bool _allowAutoSaveTriggerEvents = false;				
		private Hashtable htPrintMappingInfo = new Hashtable();		
		private DataConsumption.ServiceDataConsumption sdc;				
		private DTD.Response.RESPONSE_GROUP response;
		private DTD.Response._DATA_PROVIDER_COMPARABLE_SALES[] comps;
		private bool asc = false;		
        private DealMaker.PlainClasses.OnLineData.COMPS.comps[] dbComps = null;
		private SqlConnection _dbConn = null;		
		private decimal _estimatedValue = 0;
        Guid _propertyItemId = Guid.Empty;

		/// <summary> 
		/// Required designer variable.
		/// </summary>
        private System.ComponentModel.Container components = null;
		private System.Windows.Forms.Button buttonGetComps;
		private System.Windows.Forms.ListView listViewPropertyProfilesComps;
		private System.Windows.Forms.Label labelSubjectPropertySquareFootage;
		private System.Windows.Forms.ColumnHeader columnHeaderTag;
		private System.Windows.Forms.ColumnHeader columnHeaderDistnace;
		private System.Windows.Forms.ColumnHeader columnHeaderSubdivision;
		private System.Windows.Forms.ColumnHeader columnHeaderCity;
		private System.Windows.Forms.ColumnHeader columnHeaderSQFT;
		private System.Windows.Forms.ColumnHeader columnHeaderYB;
		private System.Windows.Forms.ColumnHeader columnHeaderRooms;
		private System.Windows.Forms.ColumnHeader columnHeaderBedrooms;
		private System.Windows.Forms.ColumnHeader columnHeaderBath;
		private System.Windows.Forms.ColumnHeader columnHeaderSaleDate;
		private System.Windows.Forms.ColumnHeader columnHeaderSalePrice;
		private System.Windows.Forms.ColumnHeader columnHeaderLotArea;
		private System.Windows.Forms.ColumnHeader columnHeaderAssesed;
		private System.Windows.Forms.ColumnHeader columnHeaderStreetName;
		private System.Windows.Forms.ColumnHeader columnHeaderZIP;
		private System.Windows.Forms.ColumnHeader columnHeaderPool;
		private System.Windows.Forms.Button buttonInvert;
		private System.Windows.Forms.Button buttonUntagAll;
        private System.Windows.Forms.Button buttonTagAll;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.ComboBox comboBoxSqFtRange;
		private System.Windows.Forms.ComboBox comboBoxSearchRadius;
		private System.Windows.Forms.ComboBox comboBoxSalesInLast;
		private System.Windows.Forms.TextBox textBoxConservativeValue;
		private System.Windows.Forms.TextBox textBoxRealisticValue;
		private System.Windows.Forms.TextBox textBoxDate;
		private System.Windows.Forms.Button _calculateConservative;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.ColumnHeader columnHeader1;
		private System.Windows.Forms.ColumnHeader columnHeaderStreetNum;
		private System.Windows.Forms.ColumnHeader columnHeaderSqFtPrice;
		private System.Windows.Forms.TextBox textBoxStateA;
		private System.Windows.Forms.TextBox textBoxZipA;
		private System.Windows.Forms.TextBox textBoxUnitNoA;
		private System.Windows.Forms.TextBox textBoxStreetA;
		private System.Windows.Forms.TextBox textBoxStreetNoA;
		private System.Windows.Forms.Label label76;
		private System.Windows.Forms.Label label77;
		private System.Windows.Forms.Label label80;
		private System.Windows.Forms.Label label82;
		private System.Windows.Forms.TextBox textBoxCountyA;
		private System.Windows.Forms.Label label74;
		private System.Windows.Forms.Label label75;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox textBoxPreDirA;
		private System.Windows.Forms.Label label81;
		private System.Windows.Forms.TextBox textBoxSuffixA;
		private System.Windows.Forms.TextBox textBoxPostDirA;
		private System.Windows.Forms.Label label78;
        private System.Windows.Forms.Label label79;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.Button bPrint;
		private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox tbAPN;
        private Button _calculateRealistic;
        private RadioButton _rbConservative;
        private RadioButton _rbRealistic;
        private GroupBox groupBox1;
        private Button _export;
        private Button _printWithMaps;
		private System.Windows.Forms.TextBox textBoxOrigSqFt;
        #endregion

        #region PROPERTIES
        public bool AllowAutoSaveTriggerEvents
        {
            get { return _allowAutoSaveTriggerEvents; }
            set { _allowAutoSaveTriggerEvents = value; }
        }

        bool IsRealistic
        {
            get { return _rbRealistic.Checked; }
        }
        #endregion

        #region CTR
        public UserControlPropertyProfileCompsListView()
		{
			//TODO: Define if we need this here !!!
			System.Threading.Thread.CurrentThread.CurrentCulture=System.Globalization.CultureInfo.CreateSpecificCulture("EN-US");

			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

            _printWithMaps.Click += new EventHandler(On_PrintWithMaps_Click);
			this.bPrint.Click += new EventHandler(bPrint_Click);
            _rbConservative.Click += new EventHandler(On_rbConservative_Click);
            _rbRealistic.Click += new EventHandler(On_rbRealistic_Click);
            _export.Click += new EventHandler(On_Export_Click);
			
			comboBoxSearchRadius.Items.Clear();
			comboBoxSqFtRange.Items.Clear();
			comboBoxSalesInLast.Items.Clear();

			comboBoxSearchRadius.Items.AddRange(new string[] {
																 "0.1"
																 ,"0.2"
																 ,"0.3"
																 ,"0.4"																 
																 ,"0.5"
																 ,"0.6"
																 ,"0.7"
																 ,"0.8"
																 ,"0.9"
																 ,"1.0"
																 ,"1.1"
																 ,"1.2"
																 ,"1.3"
																 ,"1.4"
																 ,"1.5"
																 ,"1.6"
																 ,"1.7"
																 ,"1.8"
																 ,"1.9"
																 ,"2.0"
																 ,"2.1"
																 ,"2.2"
																 ,"2.3"
																 ,"2.4"
																 ,"2.5"
																 ,"2.6"
																 ,"2.7"
																 ,"2.8"
																 ,"2.9"
																 ,"3.0"
                                                                 ,"5.0"
                                                                 ,"10.0"
                                                                 //,"15.0"
                                                                 //,"20.0"
                                                                 //,"25.0"
															 });
			comboBoxSqFtRange.Items.AddRange(new string[] {
															  "+/-10%"
															  ,"+/-15%"
															  ,"+/-20%"
															  ,"+/-25%"
															  ,"+/-30%"
															  ,"+/-35%"
															  ,"+/-40%"
															  ,"+/-45%"
															  ,"+/-50%"
															  ,"+/-55%"});
			comboBoxSalesInLast.Items.AddRange(new string[] {
																"24 mo."
																,"20 mo."
																,"15 mo."
																,"12 mo."
																,"10 mo."
																,"8 mo."
																,"6 mo."
															});

			comboBoxSqFtRange.SelectedIndex = 3;
			comboBoxSearchRadius.SelectedIndex = 9;			
			comboBoxSalesInLast.SelectedIndex = 3;

        }

                
        #endregion

        #region DISPOSE
        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }
        #endregion

        #region Component Designer generated code
        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listViewPropertyProfilesComps = new System.Windows.Forms.ListView();
            this.columnHeaderTag = new System.Windows.Forms.ColumnHeader();
            this.columnHeaderDistnace = new System.Windows.Forms.ColumnHeader();
            this.columnHeaderSubdivision = new System.Windows.Forms.ColumnHeader();
            this.columnHeaderSQFT = new System.Windows.Forms.ColumnHeader();
            this.columnHeaderYB = new System.Windows.Forms.ColumnHeader();
            this.columnHeaderRooms = new System.Windows.Forms.ColumnHeader();
            this.columnHeaderBedrooms = new System.Windows.Forms.ColumnHeader();
            this.columnHeaderBath = new System.Windows.Forms.ColumnHeader();
            this.columnHeaderPool = new System.Windows.Forms.ColumnHeader();
            this.columnHeaderSaleDate = new System.Windows.Forms.ColumnHeader();
            this.columnHeaderSalePrice = new System.Windows.Forms.ColumnHeader();
            this.columnHeaderSqFtPrice = new System.Windows.Forms.ColumnHeader();
            this.columnHeaderLotArea = new System.Windows.Forms.ColumnHeader();
            this.columnHeaderAssesed = new System.Windows.Forms.ColumnHeader();
            this.columnHeaderStreetNum = new System.Windows.Forms.ColumnHeader();
            this.columnHeaderStreetName = new System.Windows.Forms.ColumnHeader();
            this.columnHeaderZIP = new System.Windows.Forms.ColumnHeader();
            this.columnHeaderCity = new System.Windows.Forms.ColumnHeader();
            this.columnHeader1 = new System.Windows.Forms.ColumnHeader();
            this.buttonGetComps = new System.Windows.Forms.Button();
            this.labelSubjectPropertySquareFootage = new System.Windows.Forms.Label();
            this.buttonInvert = new System.Windows.Forms.Button();
            this.buttonUntagAll = new System.Windows.Forms.Button();
            this.buttonTagAll = new System.Windows.Forms.Button();
            this.textBoxConservativeValue = new System.Windows.Forms.TextBox();
            this.textBoxRealisticValue = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.comboBoxSqFtRange = new System.Windows.Forms.ComboBox();
            this.comboBoxSearchRadius = new System.Windows.Forms.ComboBox();
            this.comboBoxSalesInLast = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.textBoxDate = new System.Windows.Forms.TextBox();
            this._calculateConservative = new System.Windows.Forms.Button();
            this.textBoxOrigSqFt = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.textBoxStateA = new System.Windows.Forms.TextBox();
            this.textBoxCountyA = new System.Windows.Forms.TextBox();
            this.textBoxZipA = new System.Windows.Forms.TextBox();
            this.textBoxUnitNoA = new System.Windows.Forms.TextBox();
            this.textBoxStreetA = new System.Windows.Forms.TextBox();
            this.textBoxStreetNoA = new System.Windows.Forms.TextBox();
            this.label76 = new System.Windows.Forms.Label();
            this.label77 = new System.Windows.Forms.Label();
            this.label80 = new System.Windows.Forms.Label();
            this.label82 = new System.Windows.Forms.Label();
            this.label74 = new System.Windows.Forms.Label();
            this.label75 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxPreDirA = new System.Windows.Forms.TextBox();
            this.label81 = new System.Windows.Forms.Label();
            this.textBoxSuffixA = new System.Windows.Forms.TextBox();
            this.textBoxPostDirA = new System.Windows.Forms.TextBox();
            this.label78 = new System.Windows.Forms.Label();
            this.label79 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.bPrint = new System.Windows.Forms.Button();
            this.tbAPN = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this._calculateRealistic = new System.Windows.Forms.Button();
            this._rbConservative = new System.Windows.Forms.RadioButton();
            this._rbRealistic = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this._export = new System.Windows.Forms.Button();
            this._printWithMaps = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // listViewPropertyProfilesComps
            // 
            this.listViewPropertyProfilesComps.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.listViewPropertyProfilesComps.CheckBoxes = true;
            this.listViewPropertyProfilesComps.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeaderTag,
            this.columnHeaderDistnace,
            this.columnHeaderSubdivision,
            this.columnHeaderSQFT,
            this.columnHeaderYB,
            this.columnHeaderRooms,
            this.columnHeaderBedrooms,
            this.columnHeaderBath,
            this.columnHeaderPool,
            this.columnHeaderSaleDate,
            this.columnHeaderSalePrice,
            this.columnHeaderSqFtPrice,
            this.columnHeaderLotArea,
            this.columnHeaderAssesed,
            this.columnHeaderStreetNum,
            this.columnHeaderStreetName,
            this.columnHeaderZIP,
            this.columnHeaderCity,
            this.columnHeader1});
            this.listViewPropertyProfilesComps.FullRowSelect = true;
            this.listViewPropertyProfilesComps.GridLines = true;
            this.listViewPropertyProfilesComps.Location = new System.Drawing.Point(8, 120);
            this.listViewPropertyProfilesComps.MultiSelect = false;
            this.listViewPropertyProfilesComps.Name = "listViewPropertyProfilesComps";
            this.listViewPropertyProfilesComps.Size = new System.Drawing.Size(894, 340);
            this.listViewPropertyProfilesComps.TabIndex = 86;
            this.listViewPropertyProfilesComps.TabStop = false;
            this.listViewPropertyProfilesComps.UseCompatibleStateImageBehavior = false;
            this.listViewPropertyProfilesComps.View = System.Windows.Forms.View.Details;
            this.listViewPropertyProfilesComps.DoubleClick += new System.EventHandler(this.listViewPropertyProfilesComps_DoubleClick);
            this.listViewPropertyProfilesComps.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.lvPropertyItems_ColumnClick);
            // 
            // columnHeaderTag
            // 
            this.columnHeaderTag.Text = "Tag";
            this.columnHeaderTag.Width = 37;
            // 
            // columnHeaderDistnace
            // 
            this.columnHeaderDistnace.Text = "Distance";
            this.columnHeaderDistnace.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.columnHeaderDistnace.Width = 69;
            // 
            // columnHeaderSubdivision
            // 
            this.columnHeaderSubdivision.Text = "Subdivision";
            this.columnHeaderSubdivision.Width = 86;
            // 
            // columnHeaderSQFT
            // 
            this.columnHeaderSQFT.Text = "SQFT";
            this.columnHeaderSQFT.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.columnHeaderSQFT.Width = 62;
            // 
            // columnHeaderYB
            // 
            this.columnHeaderYB.Text = "YB";
            this.columnHeaderYB.Width = 40;
            // 
            // columnHeaderRooms
            // 
            this.columnHeaderRooms.Text = "Rooms";
            this.columnHeaderRooms.Width = 57;
            // 
            // columnHeaderBedrooms
            // 
            this.columnHeaderBedrooms.Text = "Bedrooms";
            this.columnHeaderBedrooms.Width = 77;
            // 
            // columnHeaderBath
            // 
            this.columnHeaderBath.Text = "Baths";
            this.columnHeaderBath.Width = 50;
            // 
            // columnHeaderPool
            // 
            this.columnHeaderPool.Text = "Pool";
            this.columnHeaderPool.Width = 40;
            // 
            // columnHeaderSaleDate
            // 
            this.columnHeaderSaleDate.Text = "Sale Date";
            this.columnHeaderSaleDate.Width = 80;
            // 
            // columnHeaderSalePrice
            // 
            this.columnHeaderSalePrice.Text = "Sale Price";
            this.columnHeaderSalePrice.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.columnHeaderSalePrice.Width = 80;
            // 
            // columnHeaderSqFtPrice
            // 
            this.columnHeaderSqFtPrice.Text = "$/sq.ft.";
            this.columnHeaderSqFtPrice.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // columnHeaderLotArea
            // 
            this.columnHeaderLotArea.Text = "Lot Area";
            this.columnHeaderLotArea.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.columnHeaderLotArea.Width = 69;
            // 
            // columnHeaderAssesed
            // 
            this.columnHeaderAssesed.Text = "Assessed Value";
            this.columnHeaderAssesed.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.columnHeaderAssesed.Width = 83;
            // 
            // columnHeaderStreetNum
            // 
            this.columnHeaderStreetNum.Text = "Street #";
            // 
            // columnHeaderStreetName
            // 
            this.columnHeaderStreetName.Text = "Street Name";
            this.columnHeaderStreetName.Width = 77;
            // 
            // columnHeaderZIP
            // 
            this.columnHeaderZIP.Text = "ZIP";
            // 
            // columnHeaderCity
            // 
            this.columnHeaderCity.Text = "City";
            // 
            // columnHeader1
            // 
            this.columnHeader1.Width = 0;
            // 
            // buttonGetComps
            // 
            this.buttonGetComps.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.buttonGetComps.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonGetComps.ForeColor = System.Drawing.SystemColors.ControlText;
            this.buttonGetComps.Location = new System.Drawing.Point(787, 80);
            this.buttonGetComps.Name = "buttonGetComps";
            this.buttonGetComps.Size = new System.Drawing.Size(88, 24);
            this.buttonGetComps.TabIndex = 85;
            this.buttonGetComps.Text = "Get Comps";
            this.buttonGetComps.Click += new System.EventHandler(this.buttonGetComps_Click);
            // 
            // labelSubjectPropertySquareFootage
            // 
            this.labelSubjectPropertySquareFootage.AutoSize = true;
            this.labelSubjectPropertySquareFootage.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSubjectPropertySquareFootage.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.labelSubjectPropertySquareFootage.Location = new System.Drawing.Point(339, 62);
            this.labelSubjectPropertySquareFootage.Name = "labelSubjectPropertySquareFootage";
            this.labelSubjectPropertySquareFootage.Size = new System.Drawing.Size(68, 13);
            this.labelSubjectPropertySquareFootage.TabIndex = 11;
            this.labelSubjectPropertySquareFootage.Text = "Sq.Ft. range:";
            this.labelSubjectPropertySquareFootage.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // buttonInvert
            // 
            this.buttonInvert.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonInvert.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.buttonInvert.ForeColor = System.Drawing.SystemColors.ControlText;
            this.buttonInvert.Location = new System.Drawing.Point(232, 468);
            this.buttonInvert.Name = "buttonInvert";
            this.buttonInvert.Size = new System.Drawing.Size(104, 24);
            this.buttonInvert.TabIndex = 95;
            this.buttonInvert.Text = "Invert Tagged";
            this.buttonInvert.Click += new System.EventHandler(this.buttonInvert_Click);
            // 
            // buttonUntagAll
            // 
            this.buttonUntagAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonUntagAll.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.buttonUntagAll.ForeColor = System.Drawing.SystemColors.ControlText;
            this.buttonUntagAll.Location = new System.Drawing.Point(120, 468);
            this.buttonUntagAll.Name = "buttonUntagAll";
            this.buttonUntagAll.Size = new System.Drawing.Size(104, 24);
            this.buttonUntagAll.TabIndex = 94;
            this.buttonUntagAll.Text = "Untag All";
            this.buttonUntagAll.Click += new System.EventHandler(this.buttonUntagAll_Click);
            // 
            // buttonTagAll
            // 
            this.buttonTagAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonTagAll.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.buttonTagAll.ForeColor = System.Drawing.SystemColors.ControlText;
            this.buttonTagAll.Location = new System.Drawing.Point(8, 468);
            this.buttonTagAll.Name = "buttonTagAll";
            this.buttonTagAll.Size = new System.Drawing.Size(104, 24);
            this.buttonTagAll.TabIndex = 93;
            this.buttonTagAll.Text = "Tag All";
            this.buttonTagAll.Click += new System.EventHandler(this.buttonTagAll_Click);
            // 
            // textBoxConservativeValue
            // 
            this.textBoxConservativeValue.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxConservativeValue.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxConservativeValue.Location = new System.Drawing.Point(697, 504);
            this.textBoxConservativeValue.Name = "textBoxConservativeValue";
            this.textBoxConservativeValue.ReadOnly = true;
            this.textBoxConservativeValue.Size = new System.Drawing.Size(96, 20);
            this.textBoxConservativeValue.TabIndex = 99;
            // 
            // textBoxRealisticValue
            // 
            this.textBoxRealisticValue.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxRealisticValue.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxRealisticValue.Enabled = false;
            this.textBoxRealisticValue.Location = new System.Drawing.Point(697, 528);
            this.textBoxRealisticValue.Name = "textBoxRealisticValue";
            this.textBoxRealisticValue.ReadOnly = true;
            this.textBoxRealisticValue.Size = new System.Drawing.Size(96, 20);
            this.textBoxRealisticValue.TabIndex = 100;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label5.Location = new System.Drawing.Point(433, 62);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(67, 13);
            this.label5.TabIndex = 21;
            this.label5.Text = "Sales in Last";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label6.Location = new System.Drawing.Point(530, 62);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(77, 13);
            this.label6.TabIndex = 23;
            this.label6.Text = "Search Radius";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // comboBoxSqFtRange
            // 
            this.comboBoxSqFtRange.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxSqFtRange.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxSqFtRange.Location = new System.Drawing.Point(342, 81);
            this.comboBoxSqFtRange.Name = "comboBoxSqFtRange";
            this.comboBoxSqFtRange.Size = new System.Drawing.Size(88, 21);
            this.comboBoxSqFtRange.TabIndex = 82;
            // 
            // comboBoxSearchRadius
            // 
            this.comboBoxSearchRadius.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxSearchRadius.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxSearchRadius.Location = new System.Drawing.Point(533, 81);
            this.comboBoxSearchRadius.Name = "comboBoxSearchRadius";
            this.comboBoxSearchRadius.Size = new System.Drawing.Size(88, 21);
            this.comboBoxSearchRadius.TabIndex = 84;
            // 
            // comboBoxSalesInLast
            // 
            this.comboBoxSalesInLast.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxSalesInLast.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxSalesInLast.Location = new System.Drawing.Point(436, 81);
            this.comboBoxSalesInLast.Name = "comboBoxSalesInLast";
            this.comboBoxSalesInLast.Size = new System.Drawing.Size(88, 21);
            this.comboBoxSalesInLast.TabIndex = 83;
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label7.Location = new System.Drawing.Point(593, 480);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(96, 24);
            this.label7.TabIndex = 28;
            this.label7.Text = "COMPS Date:";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxDate
            // 
            this.textBoxDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxDate.Location = new System.Drawing.Point(697, 480);
            this.textBoxDate.Name = "textBoxDate";
            this.textBoxDate.ReadOnly = true;
            this.textBoxDate.Size = new System.Drawing.Size(96, 20);
            this.textBoxDate.TabIndex = 98;
            // 
            // _calculateConservative
            // 
            this._calculateConservative.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._calculateConservative.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._calculateConservative.ForeColor = System.Drawing.Color.Black;
            this._calculateConservative.Location = new System.Drawing.Point(798, 501);
            this._calculateConservative.Name = "_calculateConservative";
            this._calculateConservative.Size = new System.Drawing.Size(104, 24);
            this._calculateConservative.TabIndex = 96;
            this._calculateConservative.Text = "Calculate";
            this._calculateConservative.Click += new System.EventHandler(this.buttonRecalculate_Click);
            // 
            // textBoxOrigSqFt
            // 
            this.textBoxOrigSqFt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxOrigSqFt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxOrigSqFt.Location = new System.Drawing.Point(126, 82);
            this.textBoxOrigSqFt.Name = "textBoxOrigSqFt";
            this.textBoxOrigSqFt.Size = new System.Drawing.Size(56, 20);
            this.textBoxOrigSqFt.TabIndex = 81;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label8.Location = new System.Drawing.Point(123, 66);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(35, 13);
            this.label8.TabIndex = 32;
            this.label8.Text = "Sq.Ft.";
            // 
            // textBoxStateA
            // 
            this.textBoxStateA.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxStateA.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxStateA.Location = new System.Drawing.Point(671, 35);
            this.textBoxStateA.MaxLength = 2;
            this.textBoxStateA.Name = "textBoxStateA";
            this.textBoxStateA.Size = new System.Drawing.Size(32, 20);
            this.textBoxStateA.TabIndex = 79;
            // 
            // textBoxCountyA
            // 
            this.textBoxCountyA.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxCountyA.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxCountyA.Location = new System.Drawing.Point(551, 35);
            this.textBoxCountyA.Name = "textBoxCountyA";
            this.textBoxCountyA.Size = new System.Drawing.Size(97, 20);
            this.textBoxCountyA.TabIndex = 78;
            // 
            // textBoxZipA
            // 
            this.textBoxZipA.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxZipA.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxZipA.Location = new System.Drawing.Point(447, 35);
            this.textBoxZipA.Name = "textBoxZipA";
            this.textBoxZipA.Size = new System.Drawing.Size(56, 20);
            this.textBoxZipA.TabIndex = 77;
            // 
            // textBoxUnitNoA
            // 
            this.textBoxUnitNoA.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxUnitNoA.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxUnitNoA.Location = new System.Drawing.Point(391, 35);
            this.textBoxUnitNoA.Name = "textBoxUnitNoA";
            this.textBoxUnitNoA.Size = new System.Drawing.Size(50, 20);
            this.textBoxUnitNoA.TabIndex = 76;
            // 
            // textBoxStreetA
            // 
            this.textBoxStreetA.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxStreetA.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxStreetA.Location = new System.Drawing.Point(125, 35);
            this.textBoxStreetA.Name = "textBoxStreetA";
            this.textBoxStreetA.Size = new System.Drawing.Size(163, 20);
            this.textBoxStreetA.TabIndex = 73;
            // 
            // textBoxStreetNoA
            // 
            this.textBoxStreetNoA.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxStreetNoA.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxStreetNoA.Location = new System.Drawing.Point(6, 35);
            this.textBoxStreetNoA.Name = "textBoxStreetNoA";
            this.textBoxStreetNoA.Size = new System.Drawing.Size(61, 20);
            this.textBoxStreetNoA.TabIndex = 71;
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label76.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label76.Location = new System.Drawing.Point(444, 19);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(24, 13);
            this.label76.TabIndex = 70;
            this.label76.Text = "ZIP";
            // 
            // label77
            // 
            this.label77.AutoSize = true;
            this.label77.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label77.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label77.Location = new System.Drawing.Point(388, 19);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(46, 13);
            this.label77.TabIndex = 69;
            this.label77.Text = "Unit No.";
            // 
            // label80
            // 
            this.label80.AutoSize = true;
            this.label80.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label80.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label80.Location = new System.Drawing.Point(122, 19);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(66, 13);
            this.label80.TabIndex = 68;
            this.label80.Text = "Street Name";
            // 
            // label82
            // 
            this.label82.AutoSize = true;
            this.label82.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label82.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label82.Location = new System.Drawing.Point(3, 19);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(55, 13);
            this.label82.TabIndex = 67;
            this.label82.Text = "Street No.";
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label74.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label74.Location = new System.Drawing.Point(668, 19);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(72, 13);
            this.label74.TabIndex = 78;
            this.label74.Text = "State (i.e. CA)";
            // 
            // label75
            // 
            this.label75.AutoSize = true;
            this.label75.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label75.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label75.Location = new System.Drawing.Point(553, 19);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(40, 13);
            this.label75.TabIndex = 77;
            this.label75.Text = "County";
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label1.Location = new System.Drawing.Point(519, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(19, 23);
            this.label1.TabIndex = 79;
            this.label1.Text = "or";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label2.Location = new System.Drawing.Point(654, 31);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(16, 23);
            this.label2.TabIndex = 80;
            this.label2.Text = "&&";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBoxPreDirA
            // 
            this.textBoxPreDirA.BackColor = System.Drawing.Color.LightGray;
            this.textBoxPreDirA.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxPreDirA.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxPreDirA.Location = new System.Drawing.Point(74, 35);
            this.textBoxPreDirA.Name = "textBoxPreDirA";
            this.textBoxPreDirA.Size = new System.Drawing.Size(45, 20);
            this.textBoxPreDirA.TabIndex = 72;
            // 
            // label81
            // 
            this.label81.AutoSize = true;
            this.label81.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label81.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label81.Location = new System.Drawing.Point(71, 19);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(37, 13);
            this.label81.TabIndex = 81;
            this.label81.Text = "Pre-dir";
            // 
            // textBoxSuffixA
            // 
            this.textBoxSuffixA.BackColor = System.Drawing.Color.LightGray;
            this.textBoxSuffixA.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxSuffixA.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxSuffixA.Location = new System.Drawing.Point(345, 35);
            this.textBoxSuffixA.Name = "textBoxSuffixA";
            this.textBoxSuffixA.Size = new System.Drawing.Size(40, 20);
            this.textBoxSuffixA.TabIndex = 75;
            // 
            // textBoxPostDirA
            // 
            this.textBoxPostDirA.BackColor = System.Drawing.Color.LightGray;
            this.textBoxPostDirA.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxPostDirA.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxPostDirA.Location = new System.Drawing.Point(294, 35);
            this.textBoxPostDirA.Name = "textBoxPostDirA";
            this.textBoxPostDirA.Size = new System.Drawing.Size(45, 20);
            this.textBoxPostDirA.TabIndex = 74;
            // 
            // label78
            // 
            this.label78.AutoSize = true;
            this.label78.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.label78.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label78.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label78.Location = new System.Drawing.Point(345, 19);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(33, 13);
            this.label78.TabIndex = 83;
            this.label78.Text = "Suffix";
            // 
            // label79
            // 
            this.label79.AutoSize = true;
            this.label79.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.label79.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label79.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label79.Location = new System.Drawing.Point(291, 19);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(42, 13);
            this.label79.TabIndex = 82;
            this.label79.Text = "Post-dir";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label9.Location = new System.Drawing.Point(200, 84);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(123, 13);
            this.label9.TabIndex = 101;
            this.label9.Text = "SEARCH CRITERIA:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label10.Location = new System.Drawing.Point(4, 84);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(103, 13);
            this.label10.TabIndex = 102;
            this.label10.Text = "SUBJECT AREA:";
            // 
            // bPrint
            // 
            this.bPrint.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.bPrint.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.bPrint.ForeColor = System.Drawing.SystemColors.ControlText;
            this.bPrint.Location = new System.Drawing.Point(342, 468);
            this.bPrint.Name = "bPrint";
            this.bPrint.Size = new System.Drawing.Size(104, 24);
            this.bPrint.TabIndex = 97;
            this.bPrint.Text = "Print Selected";
            // 
            // tbAPN
            // 
            this.tbAPN.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbAPN.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbAPN.Location = new System.Drawing.Point(787, 35);
            this.tbAPN.MaxLength = 128;
            this.tbAPN.Name = "tbAPN";
            this.tbAPN.Size = new System.Drawing.Size(88, 20);
            this.tbAPN.TabIndex = 80;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label11.Location = new System.Drawing.Point(784, 19);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(29, 13);
            this.label11.TabIndex = 105;
            this.label11.Text = "APN";
            // 
            // _calculateRealistic
            // 
            this._calculateRealistic.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._calculateRealistic.Enabled = false;
            this._calculateRealistic.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._calculateRealistic.ForeColor = System.Drawing.Color.Black;
            this._calculateRealistic.Location = new System.Drawing.Point(798, 525);
            this._calculateRealistic.Name = "_calculateRealistic";
            this._calculateRealistic.Size = new System.Drawing.Size(104, 24);
            this._calculateRealistic.TabIndex = 106;
            this._calculateRealistic.Text = "Calculate";
            this._calculateRealistic.Click += new System.EventHandler(this._calculateRealistic_Click);
            // 
            // _rbConservative
            // 
            this._rbConservative.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._rbConservative.AutoSize = true;
            this._rbConservative.Checked = true;
            this._rbConservative.Location = new System.Drawing.Point(344, 505);
            this._rbConservative.Name = "_rbConservative";
            this._rbConservative.Size = new System.Drawing.Size(348, 17);
            this._rbConservative.TabIndex = 107;
            this._rbConservative.TabStop = true;
            this._rbConservative.Text = "Step 1:  Calculate CONSERVATIVE VALUE by tagging low COMPS:";
            this._rbConservative.UseVisualStyleBackColor = true;
            // 
            // _rbRealistic
            // 
            this._rbRealistic.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._rbRealistic.AutoSize = true;
            this._rbRealistic.Location = new System.Drawing.Point(353, 529);
            this._rbRealistic.Name = "_rbRealistic";
            this._rbRealistic.Size = new System.Drawing.Size(339, 17);
            this._rbRealistic.TabIndex = 108;
            this._rbRealistic.Text = "Step 2:  Calculate REALISTIC VALUE by tagging realistic COMPS:";
            this._rbRealistic.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.comboBoxSqFtRange);
            this.groupBox1.Controls.Add(this.comboBoxSalesInLast);
            this.groupBox1.Controls.Add(this.comboBoxSearchRadius);
            this.groupBox1.Controls.Add(this.labelSubjectPropertySquareFootage);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.textBoxOrigSqFt);
            this.groupBox1.Controls.Add(this.tbAPN);
            this.groupBox1.Controls.Add(this.textBoxSuffixA);
            this.groupBox1.Controls.Add(this.textBoxPostDirA);
            this.groupBox1.Controls.Add(this.textBoxPreDirA);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.textBoxStateA);
            this.groupBox1.Controls.Add(this.textBoxStreetNoA);
            this.groupBox1.Controls.Add(this.textBoxCountyA);
            this.groupBox1.Controls.Add(this.textBoxStreetA);
            this.groupBox1.Controls.Add(this.textBoxUnitNoA);
            this.groupBox1.Controls.Add(this.textBoxZipA);
            this.groupBox1.Controls.Add(this.label82);
            this.groupBox1.Controls.Add(this.label81);
            this.groupBox1.Controls.Add(this.label79);
            this.groupBox1.Controls.Add(this.label78);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.buttonGetComps);
            this.groupBox1.Controls.Add(this.label80);
            this.groupBox1.Controls.Add(this.label77);
            this.groupBox1.Controls.Add(this.label76);
            this.groupBox1.Controls.Add(this.label74);
            this.groupBox1.Controls.Add(this.label75);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(11, 4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(885, 110);
            this.groupBox1.TabIndex = 109;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "REQUEST PARAMETERS:";
            // 
            // _export
            // 
            this._export.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._export.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._export.ForeColor = System.Drawing.SystemColors.ControlText;
            this._export.Location = new System.Drawing.Point(452, 468);
            this._export.Name = "_export";
            this._export.Size = new System.Drawing.Size(104, 24);
            this._export.TabIndex = 110;
            this._export.Text = "Export COMPS";
            // 
            // _printWithMaps
            // 
            this._printWithMaps.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._printWithMaps.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._printWithMaps.ForeColor = System.Drawing.SystemColors.ControlText;
            this._printWithMaps.Location = new System.Drawing.Point(8, 505);
            this._printWithMaps.Name = "_printWithMaps";
            this._printWithMaps.Size = new System.Drawing.Size(104, 24);
            this._printWithMaps.TabIndex = 111;
            this._printWithMaps.Text = "Print With Maps";
            // 
            // UserControlPropertyProfileCompsListView
            // 
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.Controls.Add(this._printWithMaps);
            this.Controls.Add(this._export);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this._rbRealistic);
            this.Controls.Add(this._rbConservative);
            this.Controls.Add(this._calculateRealistic);
            this.Controls.Add(this.bPrint);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.textBoxDate);
            this.Controls.Add(this.buttonInvert);
            this.Controls.Add(this.textBoxRealisticValue);
            this.Controls.Add(this.textBoxConservativeValue);
            this.Controls.Add(this.buttonTagAll);
            this.Controls.Add(this.buttonUntagAll);
            this.Controls.Add(this.listViewPropertyProfilesComps);
            this.Controls.Add(this._calculateConservative);
            this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.Name = "UserControlPropertyProfileCompsListView";
            this.Size = new System.Drawing.Size(910, 554);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion

        #region CLEAR FORM, SET DATA
        void On_rbRealistic_Click(object sender, EventArgs e)
        {
            textBoxConservativeValue.Enabled = false;
            _calculateConservative.Enabled = false;
            textBoxRealisticValue.Enabled = true;
            _calculateRealistic.Enabled = true;

            ShowTags(); 
        }        

        void On_rbConservative_Click(object sender, EventArgs e)
        {
            textBoxConservativeValue.Enabled = true;
            _calculateConservative.Enabled = true;
            textBoxRealisticValue.Enabled = false;
            _calculateRealistic.Enabled = false;

            ShowTags();
        }      

        void ShowTags()
        {
            try
            {                
                string[] selectedArr = null;
                if (_rbConservative.Checked)
                    selectedArr = dbComps[0].P_taggedConservativeCSV.Split(new char[] { ',' });
                else
                    selectedArr = dbComps[0].P_taggedRealisticCSV.Split(new char[] { ',' });

                foreach (ListViewItem lvi in listViewPropertyProfilesComps.Items)                                           
                    lvi.Checked = isInStringArray(selectedArr, lvi.SubItems[18].Text);                                    
            }
            catch
            {
                //intended! no need to break if error...
            }
        }
        
        public void clearForm()
		{
			listViewPropertyProfilesComps.Items.Clear();
			foreach (object obj in this.Controls)
			{
				if (obj.GetType() == typeof(TextBox))
					((TextBox)obj).Text = "";
			}
			textBoxConservativeValue.Text=getMoney("0");
			textBoxRealisticValue.Text=getMoney("0");
			dbComps=null;

		}

        public void setInitialData(bool clearFormFlag)
        {
            if (clearFormFlag)
                clearForm(); 
        }

        public void setInitialData(            
            string StreetNo,
            string PreDir,
            string StreetName,
            string PostDir,
            string StreetSuffix,
            string UnitOrAppartment,
            string ZIP,
            string County,
            string State,
            double SqFt,
            Guid PropertyGuid)
		{
            _propertyItemId = PropertyGuid;

            clearForm();
			//originalSQFT = SqFt;
			
			textBoxStreetNoA.Text =StreetNo;
			textBoxPreDirA.Text = PreDir;
			textBoxStreetA.Text = StreetName;
			textBoxPostDirA.Text = PostDir;
			textBoxSuffixA.Text = StreetSuffix;
			textBoxUnitNoA.Text = UnitOrAppartment;
			textBoxZipA.Text = ZIP;
			textBoxCountyA.Text = County;
			textBoxStateA.Text = State;
			//textBoxOrigSqFt.Text = getNumber(originalSQFT.ToString());
									
            //LOAD FROM DB
            DealMaker.PlainClasses.OnLineData.COMPS.comps[] cps = CompsManager.Instance.Select(PropertyGuid);

            if (null != cps && 0 < cps.Length)
            {
                dbComps = new DealMaker.PlainClasses.OnLineData.COMPS.comps[1] { cps[0] };
                CompsManager.Instance.P_comps = cps[0];
            }
            else
                return;

            if (null == dbComps[0].P_xmlStream)
                return;
						
			MemoryStream stream = new MemoryStream(dbComps[0].P_xmlStream);
			XmlSerializer serializer = new XmlSerializer(typeof(DTD.Response.RESPONSE_GROUP));
			response = (DTD.Response.RESPONSE_GROUP)serializer.Deserialize(stream);
			if (CheckValid())
			{
				

				//NEW dtd 4.2.2
				//BEGIN
				DTD.Response._PROPERTY_INFORMATION propInfo = null; 					
				foreach (object o in response.RESPONSE.RESPONSE_DATA[0].PROPERTY_INFORMATION_RESPONSE.Items)
				{
					if (o is DTD.Response._PROPERTY_INFORMATION)
					{
						propInfo = o as DTD.Response._PROPERTY_INFORMATION;
						break;
					}
				}

				if (null != propInfo)
					comps = propInfo._DATA_PROVIDER_COMPARABLE_SALES;
				//END

				try
				{
					textBoxDate.Text = dbComps[0].P_dateOfRetrieve.ToShortDateString();
				}
				catch{}
				try
				{
					textBoxConservativeValue.Text = getMoney(dbComps[0].P_ConservativeValue.ToString());}
				catch{}
				try
				{
					textBoxRealisticValue.Text = getMoney(dbComps[0].P_RealisticValue.ToString());}
				catch{}

                textBoxStreetNoA.Text = dbComps[0].StreetNo;
                textBoxPreDirA.Text = dbComps[0].PreDir;
                textBoxStreetA.Text = dbComps[0].StreetName;
                textBoxPostDirA.Text = dbComps[0].PostDir;
                textBoxSuffixA.Text = dbComps[0].Suffix;
                textBoxUnitNoA.Text = dbComps[0].UnitNo;
                textBoxZipA.Text = dbComps[0].Zip;
                textBoxCountyA.Text = dbComps[0].County;
                textBoxStateA.Text = dbComps[0].State;
                tbAPN.Text = dbComps[0].Apn;
                textBoxOrigSqFt.Text = dbComps[0].Sqft;
                comboBoxSqFtRange.Text = dbComps[0].SqFtRange;
                comboBoxSalesInLast.Text = dbComps[0].AllSalesInLast;
                comboBoxSearchRadius.Text = dbComps[0].SearchRadius;
			}
			FillList();
			try
			{
				//string[] selectedArr = dbComps[0].P_taggedCSV.Split(new char[]{','});
                string[] selectedArr = null;
                if (_rbConservative.Checked)
                    selectedArr = dbComps[0].P_taggedConservativeCSV.Split(new char[] { ',' });
                else
                    selectedArr = dbComps[0].P_taggedRealisticCSV.Split(new char[] { ',' });

				foreach(ListViewItem lvi in listViewPropertyProfilesComps.Items)
				{
					if(isInStringArray(selectedArr, lvi.SubItems[18].Text))
					{
						lvi.Checked=true;
					}
				}
			}
			catch
			{
				//intended! no need to break if error...
			}
        }
        #endregion

        #region SAVE - MAP TO OBJECT
        public bool Save(Guid propertyItemId)
        {
            bool success = false;
            _propertyItemId = propertyItemId;

            if (null == dbComps || 0 == dbComps.Length)
                return true;


            try
            {
                dbComps[0].StreetNo = textBoxStreetNoA.Text;
                dbComps[0].PreDir = textBoxPreDirA.Text;
                dbComps[0].StreetName = textBoxStreetA.Text;
                dbComps[0].PostDir = textBoxPostDirA.Text;
                dbComps[0].Suffix = textBoxSuffixA.Text;
                dbComps[0].UnitNo = textBoxUnitNoA.Text;
                dbComps[0].Zip = textBoxZipA.Text;
                dbComps[0].County = textBoxCountyA.Text;
                dbComps[0].State = textBoxStateA.Text;
                dbComps[0].Apn = tbAPN.Text;
                dbComps[0].Sqft = textBoxOrigSqFt.Text;
                dbComps[0].SqFtRange = comboBoxSqFtRange.Text;
                dbComps[0].AllSalesInLast = comboBoxSalesInLast.Text;
                dbComps[0].SearchRadius = comboBoxSearchRadius.Text;
                dbComps[0].P_propertyItemId = propertyItemId;
                try { dbComps[0].P_dateOfRetrieve = Convert.ToDateTime(textBoxDate.Text).Date; }
                catch { dbComps[0].P_dateOfRetrieve = DateTime.Now.Date; }
                
                if (null != dbComps && null != dbComps[0] && null != dbComps[0].P_xmlStream)//TA++: 02.04.2004. - To Prevent Runtime Crashes
                {
                    string consCSV = (null != dbComps[0] && null != dbComps[0].P_taggedConservativeCSV) ? dbComps[0].P_taggedConservativeCSV : string.Empty;
                    string realCSV = (null != dbComps[0] && null != dbComps[0].P_taggedRealisticCSV) ? dbComps[0].P_taggedRealisticCSV : string.Empty;
                    CompsManager.Instance.SaveData(propertyItemId, dbComps[0].P_dateOfRetrieve, dbComps[0].P_ConservativeValue, dbComps[0].P_RealisticValue, dbComps[0].ToByteArray() /*dbComps[0].P_xmlStream*/, "#" + consCSV + "#" + realCSV + "#");
                }
                success = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + Environment.NewLine + ex.InnerException + Environment.NewLine + ex.StackTrace, "Problem: Data not saved.");
            }
            return success;
        }
        #endregion

        private void buttonGetComps_Click(object sender, System.EventArgs e)
        {
            if (textBoxZipA.Text.Trim().Equals("") && (textBoxCountyA.Text.Trim().Equals("") || textBoxStateA.Text.Trim().Equals("")))
            {
                MessageBox.Show("Please enter ZIP or County & State info!", "Address Info Incomplete!");
                return;
            }

            DialogResult dlg = MessageBox.Show("You will incur charge or credit reduction!\nDo you want to continue?", "Warning!", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (dlg == DialogResult.No)
            {
                return;
            }

            int totalCost = 0;
            int totalAmount = 0;
            string errMsg;
            int monthlyCredits;
            int topUpCredits;

            if (!UserCredits.Instance.TestForEnough(Globals.DataConsumptionReg.KeyCode, TypeOfCharge.PP, 1, out totalCost, out totalAmount, out monthlyCredits, out topUpCredits, out errMsg))
            {
                UserCreditForm ucf = new UserCreditForm(DealMaker.Globals.UserRegistration.IDUser, monthlyCredits, topUpCredits, totalAmount, TypeOfCharge.PP, 1, totalCost);
                ucf.ShowDialog();
                return;
            }

            DTD.Request.REQUEST_GROUP request = buildRequest();

#if DUMP_XML_OBJECT
            dumpObject(request, "_REQUEST.xml");
#endif

            XmlSerializer requestSerializer = new XmlSerializer(typeof(DTD.Request.REQUEST_GROUP));
            XmlSerializer responseSerializer = new XmlSerializer(typeof(DTD.Response.RESPONSE_GROUP));

            MemoryStream requestStream = new MemoryStream();
            MemoryStream responseStream = new MemoryStream();

            // IMPORTANT  -  we do not need a namespace!
            System.Xml.Serialization.XmlSerializerNamespaces nmspace = new XmlSerializerNamespaces();
            nmspace.Add("", "");
            // IMPORTANT  -  we do not need a namespace!

            System.Xml.XmlWriter writer = new XmlTextWriter(requestStream, System.Text.UTF8Encoding.UTF8);
            //writer.Formatting = System.Xml.Formatting.Indented;
            writer.WriteStartDocument();
            //TODO: define string for request..
            writer.WriteDocType("REQUEST_GROUP", null, "C2DRequestv2.0.dtd", null);
            requestSerializer.Serialize(writer, request, nmspace);
            writer.Flush();

            //requestSerializer.Serialize(requestStream, request);
            byte[] requestArray = null;
            byte[] responseArray = null;
            string resultMessage = string.Empty;
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                Application.DoEvents();

                int count = (int)requestStream.Length - 3;
                requestArray = new byte[count];
                long newPos = requestStream.Seek(0, SeekOrigin.Begin);

                //TA++:08.04.2005
                byte[] reqStream = requestStream.ToArray();
                for (int i = 3; i < requestStream.Length; i++)
                {
                    requestArray[i - 3] = (byte)reqStream[i];
                }
                //TA--:08.04.2005
                writer.Close();

                // ++ web
                //DataConsumption.ServiceDataConsumption sdc
                sdc = new DataConsumption.ServiceDataConsumption();

                if (string.Empty != Globals.ProfitGrabberWebSupportURLDataConsumption)
                {
                    sdc.Url = Globals.ProfitGrabberWebSupportURLDataConsumption;
                }

                /*sdc.RequestDataCOMPS2(
                    Globals.DataConsumptionReg,
                    buildRequestDATA(), //requestArray,
                    out responseArray,
                    out resultMessage);*/

                sdc.RequestDataCOMPS2(
                    Globals.DataConsumptionReg,
                    buildRequestDATA(),
                    out responseArray,
                    out resultMessage);


                if (null == responseArray)
                {
                    if (string.Empty == resultMessage)
                    {
                        MessageBox.Show(this, "Web services did not answer correctly.", "Web Services", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        if (resultMessage == "1u2n3f")
                        {
                            MessageBox.Show(this, "You don't have any credits for this month!\nGo to Property Data on the menu above, and select Subscribe To Data!", "Get the data advantage!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        else
                        {
                            MessageBox.Show(this, resultMessage, "Web Services", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    return;
                }

                if (0 == responseArray.Length)
                {
                    MessageBox.Show(this, "Web services answered incorrectly.", "Web Services", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;

                }


                responseStream.Write(responseArray, 0, responseArray.Length);

                responseStream.Position = 0;
                response = (DTD.Response.RESPONSE_GROUP)responseSerializer.Deserialize(responseStream);

#if DUMP_XML_OBJECT
                dumpObject(response.RESPONSE, "XML_RESPONSE");
#endif

                if (CheckValid())
                {

                    //TODO: SAVE DATA AND STORE COMPS
                    DateTime storedDate = DateTime.Now;
                    if (null == dbComps || null == dbComps[0])
                    {
                        dbComps = new DealMaker.PlainClasses.OnLineData.COMPS.comps[1];
                        dbComps[0] = new DealMaker.PlainClasses.OnLineData.COMPS.comps(new Guid(), DateTime.Now, 0, 0, null, "");
                    }

                    dbComps[0].P_xmlStream = responseStream.ToArray();
                    dbComps[0].P_dateOfRetrieve = storedDate;
                    dbComps[0].P_taggedCSV = "";
                    dbComps[0].P_taggedRealisticCSV = "";
                    dbComps[0].P_taggedConservativeCSV = "";
                    dbComps[0].P_ConservativeValue = 0;
                    textBoxConservativeValue.Text = getMoney("0");

                    textBoxDate.Text = storedDate.ToShortDateString();
                    //comps = response.
                    //	RESPONSE.
                    //	RESPONSE_DATA[0].
                    //	PROPERTY_INFORMATION_RESPONSE.
                    //	_PROPERTY_INFORMATION[0].
                    //	_DATA_PROVIDER_COMPARABLE_SALES;

                    //dtd 4.2.2
                    //BEGIN
#if !NEW_DTD
					//OLD
					comps = response.RESPONSE.RESPONSE_DATA[0].PROPERTY_INFORMATION_RESPONSE._PROPERTY_INFORMATION[0]._DATA_PROVIDER_COMPARABLE_SALES;
#else
                    //NEW						
                    DTD.Response._PROPERTY_INFORMATION propInfo = null; //response.RESPONSE.RESPONSE_DATA[0].PROPERTY_INFORMATION_RESPONSE.Items[0] as DTD.Response._PROPERTY_INFORMATION;


                    foreach (object o in response.RESPONSE.RESPONSE_DATA[0].PROPERTY_INFORMATION_RESPONSE.Items)
                    {
                        if (o is DTD.Response._PROPERTY_INFORMATION)
                        {
                            propInfo = o as DTD.Response._PROPERTY_INFORMATION;
                            break;
                        }
                    }

                    if (null != propInfo)
                        comps = propInfo._DATA_PROVIDER_COMPARABLE_SALES;
#endif
                    //END                                        

                    FillList();

                    //AUTOSAVE
                    if (null != COMPS_Downloaded_Successfully && _allowAutoSaveTriggerEvents)
                        COMPS_Downloaded_Successfully();

                    try
                    {
                        string outMsg = string.Empty;
                        //0 = COMPS
                        //1 = PP
                        //2 = Matching Module
                        string msg;
                        Guid userId = DealMaker.Globals.UserRegistration.IDUser;

                        if (Globals.NwVerEnabled && null != Globals.activeModules && Globals.activeModules.MOD_6)
                            sdc.QueryGuidFromKey(Globals.DataConsumptionReg.KeyCode, out userId, out msg);

                        bool bRes = sdc.ConfirmDataRequestSuccess(userId, 1, 0, out outMsg);

#if DEBUG
                        if (true == bRes)
                        {
#if SHOW_CHARGED
							MessageBox.Show("Charged!");
#endif
                        }
                        else
                        {
                            MessageBox.Show("PROBLEM IN CHARGE: " + outMsg);
                        }
#endif
                    }
                    catch (Exception
#if DEBUG
 exc
#endif
)
                    {
#if DEBUG
                        MessageBox.Show(exc.ToString());
#endif
                    }
                }
            }
            catch (Exception ex)
            {
                //MessageBox.Show(this, "Too many connections on server.\nTry again later.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Information);
                //#if DEBUG
                MessageBox.Show(ex.Message + "\n" + ex.InnerException);
                //#endif
                //throw ex;
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
            sdc = null;
            return;
        }

        private byte[] buildRequestDATA()
        {
            DTD.Request.REQUEST_GROUP rqg = new DTD.Request.REQUEST_GROUP();
            DTD.Request._SEARCH_CRITERIA sc = new DTD.Request._SEARCH_CRITERIA();
            DTD.Request._COMPARABLE_SEARCH cmps = new DTD.Request._COMPARABLE_SEARCH();
            DTD.Request._PROPERTY_CRITERIA pc = new DTD.Request._PROPERTY_CRITERIA();


            rqg.REQUEST = new DTD.Request.REQUEST();
            rqg.REQUEST.REQUESTDATA = new DTD.Request.REQUESTDATA[1];
            rqg.REQUEST.REQUESTDATA[0] = new DTD.Request.REQUESTDATA();
            rqg.REQUEST.REQUESTDATA[0].PROPERTY_INFORMATION_REQUEST = new DTD.Request.PROPERTY_INFORMATION_REQUEST();
            rqg.REQUEST.REQUESTDATA[0].PROPERTY_INFORMATION_REQUEST._SEARCH_CRITERIA = new DTD.Request._SEARCH_CRITERIA();

            DTD.Request._CONNECT2DATA_PRODUCT c2dp = new DTD.Request._CONNECT2DATA_PRODUCT();
            c2dp._DetailedComparableReport = DTD.Request._CONNECT2DATA_PRODUCT_DetailedComparableReport.Y;

            //Added for APN support++
            c2dp._IncludePDFIndicator = DTD.Request._CONNECT2DATA_PRODUCT_IncludePDFIndicator.Y;
            c2dp._IncludeSearchCriteriaIndicator = DTD.Request._CONNECT2DATA_PRODUCT_IncludeSearchCriteriaIndicator.Y;
            //--


            rqg.REQUEST.REQUESTDATA[0].PROPERTY_INFORMATION_REQUEST._CONNECT2DATA_PRODUCT = c2dp;

            rqg.REQUEST.REQUESTDATA[0].PROPERTY_INFORMATION_REQUEST._ActionType = DTD.Request.PROPERTY_INFORMATION_REQUEST_ActionType.Submit;
            rqg.REQUEST.REQUESTDATA[0].PROPERTY_INFORMATION_REQUEST._ActionTypeSpecified = true;

            DTD.Request._RESPONSE_CRITERIA rescrit = new DTD.Request._RESPONSE_CRITERIA();

            rescrit._NumberComparablesType = DTD.Request._RESPONSE_CRITERIA_NumberComparablesType.Item50;
            rescrit._NumberCompFarmRecordsType = DTD.Request._RESPONSE_CRITERIA_NumberCompFarmRecordsType.Item100;
            rescrit._NumberSubjectPropertiesType= DTD.Request._RESPONSE_CRITERIA_NumberSubjectPropertiesType.Item100;

            rqg.REQUEST.REQUESTDATA[0].PROPERTY_INFORMATION_REQUEST._RESPONSE_CRITERIA = rescrit;

            pc.PARSED_STREET_ADDRESS = new DTD.Request.PARSED_STREET_ADDRESS();

            pc.PARSED_STREET_ADDRESS._StreetName = textBoxStreetA.Text;
            pc.PARSED_STREET_ADDRESS._HouseNumber = textBoxStreetNoA.Text;
            pc.PARSED_STREET_ADDRESS._ApartmentOrUnit = textBoxUnitNoA.Text;

            pc.PARSED_STREET_ADDRESS._DirectionPrefix = textBoxPreDirA.Text;
            pc.PARSED_STREET_ADDRESS._DirectionSuffix = textBoxPostDirA.Text;
            pc.PARSED_STREET_ADDRESS._StreetSuffix = textBoxSuffixA.Text;

            pc._PostalCode = textBoxZipA.Text;

            pc._County = textBoxCountyA.Text;
            pc._State = textBoxStateA.Text;

            DTD.Request._LAND_USE lu = new DTD.Request._LAND_USE();
            rqg.REQUEST.REQUESTDATA[0].PROPERTY_INFORMATION_REQUEST._PROPERTY_CRITERIA = pc;
            cmps._GeographicConstraintTypeSpecified = true;

            //OLD
            //cmps._GeographicConstraintType = DTD.Request._COMPARABLE_SEARCH_GeographicConstraintType.SameCity;
            //NEW APN
            cmps._GeographicConstraintType = DTD.Request._COMPARABLE_SEARCH_GeographicConstraintType.SameZipCode;

            cmps._MonthsBackNumber = comboBoxSalesInLast.Text.TrimEnd(new char[] { ' ', 'm', 'o', '.' });
            cmps._LivingAreaVariancePercent = comboBoxSqFtRange.Text.Trim(new char[] { '+', '-', '/', '%', ' ' });
            cmps._DistanceFromSubjectNumber = comboBoxSearchRadius.Text;

            DTD.Request._LAND_USE lu2 = new DTD.Request._LAND_USE();
            lu2._SameAsSubjectType = DTD.Request._LAND_USE_SameAsSubjectType.Yes;
            lu2._SameAsSubjectTypeSpecified = true;

            cmps._LAND_USE = new DTD.Request._LAND_USE[1];
            cmps._LAND_USE[0] = lu2;

            //NEW
            if (string.Empty != tbAPN.Text)
            {
                DTD.Request._SUBJECT_SEARCH ss = new DTD.Request._SUBJECT_SEARCH();
                ss._AssessorsParcelIdentifier = tbAPN.Text;
                rqg.REQUEST.REQUESTDATA[0].PROPERTY_INFORMATION_REQUEST._SEARCH_CRITERIA.Items = new object[2] { ss, cmps };
            }
            else
                rqg.REQUEST.REQUESTDATA[0].PROPERTY_INFORMATION_REQUEST._SEARCH_CRITERIA.Items = new object[1] { cmps };
            //END

            rqg.REQUESTING_PARTY = new DTD.Request.REQUESTING_PARTY[1];
           
            rqg.REQUEST._JobIdentifier = "TEST MASTER V2.0";
            rqg.REQUEST._RecordIdentifier = "12345A8";
            rqg.REQUEST._HVERequestTypeSpecified = true;
            rqg.REQUEST._HVERequestType = DTD.Request.REQUEST_HVERequestType.Item02;
            rqg.REQUEST._HVCustomerIdentifier = "";

#if DUMP_XML_OBJECT
            dumpObject(rqg.REQUEST, "_APN_REQ");
#endif

            MemoryStream stream = new MemoryStream();
            XmlSerializer formatter = new XmlSerializer(rqg.REQUEST.REQUESTDATA[0].GetType());
            formatter.Serialize(stream, rqg.REQUEST.REQUESTDATA[0]);
            return stream.ToArray();
        }

        private DTD.Request.REQUEST_GROUP buildRequest()
        {

            DTD.Request.REQUEST_GROUP rqg = new DTD.Request.REQUEST_GROUP();
            DTD.Request._SEARCH_CRITERIA sc = new DTD.Request._SEARCH_CRITERIA();
            DTD.Request._COMPARABLE_SEARCH cmps = new DTD.Request._COMPARABLE_SEARCH();
            DTD.Request._PROPERTY_CRITERIA pc = new DTD.Request._PROPERTY_CRITERIA();


            rqg.REQUEST = new DTD.Request.REQUEST();
            rqg.REQUEST.REQUESTDATA = new DTD.Request.REQUESTDATA[1];
            rqg.REQUEST.REQUESTDATA[0] = new DTD.Request.REQUESTDATA();
            rqg.REQUEST.REQUESTDATA[0].PROPERTY_INFORMATION_REQUEST = new DTD.Request.PROPERTY_INFORMATION_REQUEST();
            rqg.REQUEST.REQUESTDATA[0].PROPERTY_INFORMATION_REQUEST._SEARCH_CRITERIA = new DTD.Request._SEARCH_CRITERIA();

            DTD.Request._CONNECT2DATA_PRODUCT c2dp = new DTD.Request._CONNECT2DATA_PRODUCT();
            c2dp._DetailedComparableReport = DTD.Request._CONNECT2DATA_PRODUCT_DetailedComparableReport.Y;
            rqg.REQUEST.REQUESTDATA[0].PROPERTY_INFORMATION_REQUEST._CONNECT2DATA_PRODUCT = c2dp;
            rqg.REQUEST.REQUESTDATA[0].PROPERTY_INFORMATION_REQUEST._ActionType = DTD.Request.PROPERTY_INFORMATION_REQUEST_ActionType.Submit;
            rqg.REQUEST.REQUESTDATA[0].PROPERTY_INFORMATION_REQUEST._ActionTypeSpecified = true;

            DTD.Request._RESPONSE_CRITERIA rescrit = new DTD.Request._RESPONSE_CRITERIA();

            rescrit._NumberComparablesType = DTD.Request._RESPONSE_CRITERIA_NumberComparablesType.Item50;
            rescrit._NumberCompFarmRecordsType = DTD.Request._RESPONSE_CRITERIA_NumberCompFarmRecordsType.Item100;
            rescrit._NumberSubjectPropertiesType = DTD.Request._RESPONSE_CRITERIA_NumberSubjectPropertiesType.Item100;

            rqg.REQUEST.REQUESTDATA[0].PROPERTY_INFORMATION_REQUEST._RESPONSE_CRITERIA = rescrit;

            pc.PARSED_STREET_ADDRESS = new DTD.Request.PARSED_STREET_ADDRESS();
            pc.PARSED_STREET_ADDRESS._StreetName = textBoxStreetA.Text;
            pc.PARSED_STREET_ADDRESS._HouseNumber = textBoxStreetNoA.Text;
            pc.PARSED_STREET_ADDRESS._ApartmentOrUnit = textBoxUnitNoA.Text;
            pc.PARSED_STREET_ADDRESS._DirectionPrefix = textBoxPreDirA.Text;
            pc.PARSED_STREET_ADDRESS._DirectionSuffix = textBoxPostDirA.Text;
            pc.PARSED_STREET_ADDRESS._StreetSuffix = textBoxSuffixA.Text;
            pc._PostalCode = textBoxZipA.Text;
            pc._County = textBoxCountyA.Text;
            pc._State = textBoxStateA.Text;

            DTD.Request._LAND_USE lu = new DTD.Request._LAND_USE();
            rqg.REQUEST.REQUESTDATA[0].PROPERTY_INFORMATION_REQUEST._PROPERTY_CRITERIA = pc;

            //cmps._PoolOptionType = DTD.Request._COMPARABLE_SEARCH_PoolOptionType.PropertiesWithAndWithoutPools;
            cmps._GeographicConstraintTypeSpecified = true;
            cmps._GeographicConstraintType = DTD.Request._COMPARABLE_SEARCH_GeographicConstraintType.SameCity;

            cmps._MonthsBackNumber = comboBoxSalesInLast.Text.TrimEnd(new char[] { ' ', 'm', 'o', '.' });
            cmps._LivingAreaVariancePercent = comboBoxSqFtRange.Text.Trim(new char[] { '+', '-', '/', '%', ' ' });
            cmps._DistanceFromSubjectNumber = comboBoxSearchRadius.Text;

            DTD.Request._LAND_USE lu2 = new DTD.Request._LAND_USE();
            lu2._SameAsSubjectType
                = DTD.Request._LAND_USE_SameAsSubjectType.Yes;
            lu2._SameAsSubjectTypeSpecified = true;

            cmps._LAND_USE = new DTD.Request._LAND_USE[1];
            cmps._LAND_USE[0] = lu2;

            //rqg.REQUEST.REQUESTDATA[0].PROPERTY_INFORMATION_REQUEST._SEARCH_CRITERIA.Item 
            //	= cmps;
            //dtd 4.2.2
            //BEGIN
#if !NEW_DTD
			//OLD
			rqg.REQUEST.REQUESTDATA[0].PROPERTY_INFORMATION_REQUEST._SEARCH_CRITERIA.Item = cmps;
#else
            //NEW
            if (string.Empty != tbAPN.Text)
            {
                DTD.Request._SUBJECT_SEARCH ss = new DTD.Request._SUBJECT_SEARCH();
                ss._AssessorsParcelIdentifier = tbAPN.Text;
                rqg.REQUEST.REQUESTDATA[0].PROPERTY_INFORMATION_REQUEST._SEARCH_CRITERIA.Items = new object[2] { ss, cmps };
            }
            else
                rqg.REQUEST.REQUESTDATA[0].PROPERTY_INFORMATION_REQUEST._SEARCH_CRITERIA.Items = new object[1] { cmps };
#endif
            //END

            rqg.REQUESTING_PARTY
                = new DTD.Request.REQUESTING_PARTY[1];

            rqg.MISMOVersionID = "2.1";

            //rqg.REQUEST.LoginAccountIdentifier = "PR.1OFT1";
            //rqg.REQUEST.LoginAccountPassword = "TK.2S1GRABR";

            rqg.REQUEST._JobIdentifier = "TEST MASTER V2.0";
            rqg.REQUEST._RecordIdentifier = "12345A8";            

            rqg.REQUEST._HVERequestTypeSpecified = true;
            rqg.REQUEST._HVERequestType = DTD.Request.REQUEST_HVERequestType.Item02;
            rqg.REQUEST._HVCustomerIdentifier = "";

            return rqg;
        }

        #region CONTROL INTERNALS
        public bool CheckValid()
		{
			DTD.Response.STATUS status = null;
			bool error = false;

			try 
			{
				status = response.RESPONSE.STATUS?[0] ?? null;
                if (null != status)
                {
                    if (!status._Condition.Trim().StartsWith("SUCCES"))
                    {
                        MessageBox.Show(status._Description, status._Condition);
                        error = true;
                    }
                }
            }
			catch{};
			try 
			{
				status =response.RESPONSE.RESPONSE_DATA[0].PROPERTY_INFORMATION_RESPONSE.STATUS;
				if (!status._Condition.Trim().StartsWith("SUCCES"))
				{
					MessageBox.Show(status._Description, status._Condition);
					error = true;
				}
			}
			catch{};
			try 
			{				
				DTD.Response._PRODUCT prod = response.RESPONSE.RESPONSE_DATA[0].PROPERTY_INFORMATION_RESPONSE.Items[0] as DTD.Response._PRODUCT;			
				
				if (null != prod && null != prod.STATUS && prod.STATUS.Length > 0)
					status = prod.STATUS[0];

				//END
				if (!status._Condition.Trim().StartsWith("SUCCES"))
				{
					MessageBox.Show(status._Description, status._Condition);
					error = true;
				}
			}
			catch{};
			try 
			{
				//DTD.Response._MULTIPLE_RECORDS[] multipla 
				//	= response.RESPONSE.RESPONSE_DATA[0].PROPERTY_INFORMATION_RESPONSE._PROPERTY_INFORMATION[0]._MULTIPLE_RECORDS;
				//dtd 4.2.2
				//BEGIN
#if !NEW_DTD
				//OLD
				DTD.Response._MULTIPLE_RECORDS[] multipla = response.RESPONSE.RESPONSE_DATA[0].PROPERTY_INFORMATION_RESPONSE._PROPERTY_INFORMATION[0]._MULTIPLE_RECORDS;
#else
				//NEW
				DTD.Response._PROPERTY_INFORMATION propInfo = null;
				DTD.Response._MULTIPLE_RECORDS[] multipla = null;

				foreach (object o in response.RESPONSE.RESPONSE_DATA[0].PROPERTY_INFORMATION_RESPONSE.Items)
				{
					if (o is DTD.Response._PROPERTY_INFORMATION)
					{
						propInfo = response.RESPONSE.RESPONSE_DATA[0].PROPERTY_INFORMATION_RESPONSE.Items[1] as DTD.Response._PROPERTY_INFORMATION;					
						break;
					}
				}

				if (null != propInfo)
					multipla = propInfo._MULTIPLE_RECORDS;
#endif
				//END
				
				FormMultipleProperiesFound form = new FormMultipleProperiesFound();

				form.lvFill(multipla);
				form.ShowDialog();

				int iterator = form.selected;
				if (iterator>=0)
				{
				
					try
					{
						textBoxStreetNoA.Text = getSecureString(multipla[iterator].PROPERTY._PARSED_STREET_ADDRESS._HouseNumber);
					}
					catch{}
					try
					{
						textBoxPreDirA.Text = getSecureString(multipla[iterator].PROPERTY._PARSED_STREET_ADDRESS._DirectionPrefix);
					}
					catch{}
					try
					{
						textBoxStreetA.Text = getSecureString(multipla[iterator].PROPERTY._PARSED_STREET_ADDRESS._StreetName);
					}
					catch{}
					try
					{
						textBoxPostDirA.Text = getSecureString(multipla[iterator].PROPERTY._PARSED_STREET_ADDRESS._DirectionSuffix);
					}
					catch{}
					try
					{
						textBoxSuffixA.Text = getSecureString(multipla[iterator].PROPERTY._PARSED_STREET_ADDRESS._StreetSuffix);
					}
					catch{}
					try
					{
						textBoxUnitNoA.Text = getSecureString(multipla[iterator].PROPERTY._PARSED_STREET_ADDRESS._ApartmentOrUnit);
					}
					catch{}
					try
					{
						textBoxZipA.Text = getSecureString(multipla[iterator].PROPERTY._PostalCode);
					}
					catch{}
					try
					{
						textBoxCountyA.Text = getSecureString(multipla[iterator].PROPERTY._County);
					}
					catch{}
					try
					{
						textBoxStateA.Text = getSecureString(multipla[iterator].PROPERTY._State);
					}
					catch{}
					try
					{
						tbAPN.Text = getSecureString(multipla[iterator].PROPERTY._AssessorsParcelIdentifier);
					}
					catch{}
				}
				error = true;
			
			}
			catch{};


			listViewPropertyProfilesComps.Items.Clear();

			return !error;

			//FillList();
			
		}
		
		private void buttonGetComps_reenginered_Click(object sender, EventArgs e) 
		{
			return;
		}		
										
		private void FillList()
		{
			
			ListViewItem lvItem;
			this.htPrintMappingInfo.Clear();

			Guid _printId = new Guid();

            if (null == comps)
                return;
						
			for(
				int iterator = comps.GetLowerBound(0); 
				iterator <= comps.GetUpperBound(0); 
				iterator++
				)
			{
				string cel01="";
				string cel02="";
				string cel03="";
				string cel04="";
				string cel05="";
				string cel06="";
				string cel07="";
				string cel08="";
				string cel09="";
				string cel10="";
				string cel11="";
				string cel12="";
				string cel13="";
				string cel14="";
				string cel15="";
				string cel16="";
				string cel17="";
				string cel18="";

				if (null == comps[iterator].PROPERTY)
					continue;

				_printId = Guid.NewGuid();

				try
				{
					this.htPrintMappingInfo.Add(_printId, comps[iterator]);
				}
				catch {}

				try
				{
					cel01 = getSecureString(comps[iterator]._DistanceFromSubjectNumber);
				}
				catch{}
				try
				{
					cel02 = getSecureString(comps[iterator].PROPERTY._SubdivisionIdentifier);
				}
				catch{}
				try
				{
					//cel03 = getNumber(getSecureString(comps[iterator].PROPERTY._PROPERTY_CHARACTERISTICS._IMPROVEMENTS._ROOM_COUNT._TotalLivingAreaSquareFeetNumber));
                    cel03 = getSemiformattedNumber(getSecureString(comps[iterator].PROPERTY._PROPERTY_CHARACTERISTICS._IMPROVEMENTS._ROOM_COUNT._TotalLivingAreaSquareFeetNumber));
				}
				catch{}
				try
				{
					cel04 = getSecureString(comps[iterator].PROPERTY._PROPERTY_CHARACTERISTICS._IMPROVEMENTS._GENERAL_DESCRIPTION._YearBuiltDateIdentifier);
				}
				catch{}
				try
				{
					cel05 = getSecureString(comps[iterator].PROPERTY._PROPERTY_CHARACTERISTICS._IMPROVEMENTS._ROOM_COUNT._TotalRoomCount);
				}
				catch{}
				try
				{
					cel06 = getSecureString(comps[iterator].PROPERTY._PROPERTY_CHARACTERISTICS._IMPROVEMENTS._ROOM_COUNT._TotalBedroomsCount);
				}
				catch{}
				try
				{
					cel07 = getSecureString(comps[iterator].PROPERTY._PROPERTY_CHARACTERISTICS._IMPROVEMENTS._ROOM_COUNT._TotalBathsCount);
				}
				catch{}
				try
				{
					cel08 = getSecureString(comps[iterator].PROPERTY._PROPERTY_CHARACTERISTICS._IMPROVEMENTS._FEATURES._POOL._HasFeatureIndicator);
				}
				catch{}
				try
				{					
					string ss = getSecureString(comps[iterator].PROPERTY._PROPERTY_HISTORY._SALES_HISTORY[0]._LastSalesDate);
					string sd = getDate(ss);					
					try
					{
						string temp = Convert.ToDateTime(sd).ToString("yyyy/MM/dd");
						cel09 = temp;
					}
					catch
					{
						cel09 = sd;
					}
					
					//cel09 = getDate(getSecureString(comps[iterator].PROPERTY._PROPERTY_HISTORY._SALES_HISTORY[0]._LastSalesDate));					
				}
				catch{}
				try
				{
					cel10 = getMoney(getSecureString(comps[iterator].PROPERTY._PROPERTY_HISTORY._SALES_HISTORY[0]._LastSalesPriceAmount));
				}
				catch{}
				try
				{
					cel11 = getMoney(getSecureString(comps[iterator].PROPERTY._PROPERTY_HISTORY._SALES_HISTORY[0]._PricePerSquareFootAmount));
				}
				catch{}
				try
				{
					//cel12 = getNumber(getSecureString(comps[iterator].PROPERTY._PROPERTY_CHARACTERISTICS._SITE._DIMENSIONS._LotAreaSquareFeetNumber));
                    cel12 = getSemiformattedNumber(getSecureString(comps[iterator].PROPERTY._PROPERTY_CHARACTERISTICS._SITE._DIMENSIONS._LotAreaSquareFeetNumber));
				}
				catch{}
				try
				{
					cel13 = getMoney(getSecureString(comps[iterator].PROPERTY._PROPERTY_TAX._TotalAssessedValueAmount));
				}
				catch{}
				try
				{
					cel14 = getSecureString(comps[iterator].PROPERTY._PARSED_STREET_ADDRESS._HouseNumber);
				}
				catch{}
				try
				{
					cel15 = getSecureString(comps[iterator].PROPERTY._PARSED_STREET_ADDRESS._StreetName);					
				}
				catch{}
				try
				{
					cel16 = getSecureString(comps[iterator].PROPERTY._PostalCode);
				}
				catch{}
				try
				{
					cel17 = getSecureString(comps[iterator].PROPERTY._City);
				}
				catch{}
				try
				{
					cel18 = getSecureString(comps[iterator]._ComparableNumber);
				}
				catch{}

				lvItem = new ListViewItem
					(new string[] {
									  ""
									  ,cel01
									  ,cel02
									  ,cel03
									  ,cel04
									  ,cel05
									  ,cel06
									  ,cel07
									  ,cel08
									  ,cel09
									  ,cel10
									  ,cel11
									  ,cel12
									  ,cel13
									  ,cel14
									  ,cel15
									  ,cel16
									  ,cel17
									  ,cel18
				});
				lvItem.Tag = _printId;
				listViewPropertyProfilesComps.Items.Add(lvItem);
			}            
		}
		
		private void lvPropertyItems_ColumnClick(object sender, System.Windows.Forms.ColumnClickEventArgs e)
		{        
			this.asc = !this.asc;

			if (true == this.asc)

				this.listViewPropertyProfilesComps.ListViewItemSorter = new ListViewItemComparer(e.Column);

			else

				this.listViewPropertyProfilesComps.ListViewItemSorter = new ListViewItemComparerDesc(e.Column);

 

		}
		
		private void buttonTagAll_Click(object sender, System.EventArgs e)
		{
			foreach(ListViewItem lvi in listViewPropertyProfilesComps.Items)
			{
				lvi.Checked = true;
			}
		}

		private void buttonUntagAll_Click(object sender, System.EventArgs e)
		{
			foreach(ListViewItem lvi in listViewPropertyProfilesComps.Items)
			{
				lvi.Checked = false;
			}
		
		}

		private void buttonInvert_Click(object sender, System.EventArgs e)
		{
			foreach(ListViewItem lvi in listViewPropertyProfilesComps.Items)
			{
				lvi.Checked = !lvi.Checked;
			}
		}
		
		private void buttonRecalculate_Click(object sender, System.EventArgs e)
		{
            CalculateFromSelected(TypeOfCOMPSValue.Conservative);
		}

        private void _calculateRealistic_Click(object sender, EventArgs e)
        {
            CalculateFromSelected(TypeOfCOMPSValue.Realistic);
        }

        void CalculateFromSelected(TypeOfCOMPSValue type)
        {
            if (0 == listViewPropertyProfilesComps.Items.Count)
            {
                return;
            }
            string strSelected = "";
            double dorigsqft;
            try
            {
                dorigsqft = Convert.ToDouble(textBoxOrigSqFt.Text);
                if (0 == dorigsqft)
                    throw new Exception();
            }
            catch
            {
                MessageBox.Show("Please enter the Subject Property Sq. Ft. before Recalculating");
                textBoxConservativeValue.Text = getMoney("0");
                return;
            }

            double estimate = 0;
            int counter = 0;
            try
            {
                foreach (ListViewItem lvi in listViewPropertyProfilesComps.Items)
                {
                    if (lvi.Checked)
                    {
                        string price = lvi.SubItems[10].Text.Replace("$", "");

                        string sqft = lvi.SubItems[3].Text;
                        double dprice;
                        double dsqft;
                        try
                        {
                            dprice = Convert.ToDouble(price);
                            dsqft = Convert.ToDouble(sqft);

                        }
                        catch
                        {
                            MessageBox.Show("Please Tag only the properties that have the Sales Price and Sq. Ft.!");
                            textBoxConservativeValue.Text = "";
                            return;
                        }

                        estimate = estimate + (dprice / dsqft);
                        counter++;
                        strSelected = strSelected + lvi.SubItems[18].Text + ",";
                    }
                }
                if (counter == 0)
                {
                    MessageBox.Show("You must select (tag) at least one property!");
                    textBoxConservativeValue.Text = "";
                    return;
                }
                strSelected = strSelected.Substring(0, strSelected.Length - 1);

                estimate = estimate / counter;
                estimate = estimate * dorigsqft;
                _estimatedValue = (decimal)estimate;

                if (type == TypeOfCOMPSValue.Conservative)
                {
                    dbComps[0].P_taggedConservativeCSV = strSelected;
                    dbComps[0].P_ConservativeValue = (decimal)estimate;
                    textBoxConservativeValue.Text = getMoney(estimate.ToString());
                    CompsManager.Instance.P_comps.P_ConservativeValue = (decimal)estimate;
                }
                else  //REALISTIC                
                {
                    dbComps[0].P_taggedRealisticCSV = strSelected;
                    dbComps[0].P_RealisticValue = (decimal)estimate;
                    textBoxRealisticValue.Text = getMoney(estimate.ToString());
                    CompsManager.Instance.P_comps.P_RealisticValue= (decimal)estimate;
                }

                Save(_propertyItemId);

            }
            catch
            {
                MessageBox.Show("Error calculating data", "", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                textBoxConservativeValue.Text = "Error";
            }
        }
		
		public decimal estimatedValue
		{
			get {return _estimatedValue;}
		}
										
		private void listViewPropertyProfilesComps_DoubleClick(object sender, System.EventArgs e)
		{
			listViewPropertyProfilesComps.SelectedItems[0].Checked = !listViewPropertyProfilesComps.SelectedItems[0].Checked;
			int selected = int.Parse(listViewPropertyProfilesComps.SelectedItems[0].SubItems[18].Text);
			Form x = new Form();

			UserControlPropertyProfileComps pc = new UserControlPropertyProfileComps();

			x.Controls.Add(pc);

			int newIdx = 0;
			for (newIdx = 0; newIdx < comps.Length; newIdx ++)
			{
				int xmlComparableNumber;

				try	
				{ xmlComparableNumber = Convert.ToInt32(comps[newIdx]._ComparableNumber);	}
				catch 
				{ xmlComparableNumber = 0; }

				if (xmlComparableNumber == selected)
				{
					selected = newIdx;			
					break;
				}
			}

			pc.fillComps(
				comps[selected /*-1*/ ].PROPERTY
				,comps[selected /*-1*/]._ComparableNumber
				,listViewPropertyProfilesComps.SelectedItems[0].SubItems[1].Text
				);
			x.Height= pc.Height;
			x.Width = pc.Width;

			pc.prepareComparable();
			pc.Dock = DockStyle.Fill;

			x.ShowInTaskbar = false;
			x.FormBorderStyle = FormBorderStyle.FixedToolWindow;
			x.Text="Property Details";
			DialogResult dr = x.ShowDialog();

            if ((((UserControlPropertyProfileComps)x.Controls[0])).OverridedSqFt)
            {                                                
                try 
                {
                    if (null == dbComps[0].P_xmlStream)
                        return;

                    MemoryStream stream = new MemoryStream(dbComps[0].P_xmlStream);
                    XmlSerializer serializer = new XmlSerializer(typeof(DTD.Response.RESPONSE_GROUP));
                    DTD.Response.RESPONSE_GROUP tempResponse = (DTD.Response.RESPONSE_GROUP)serializer.Deserialize(stream);

                    ((DTD.Response._PROPERTY_INFORMATION)(tempResponse.RESPONSE.RESPONSE_DATA[0].PROPERTY_INFORMATION_RESPONSE.Items[1]))._DATA_PROVIDER_COMPARABLE_SALES[selected].PROPERTY._PROPERTY_CHARACTERISTICS._IMPROVEMENTS._ROOM_COUNT._TotalLivingAreaSquareFeetNumber = (((UserControlPropertyProfileComps)x.Controls[0])).OverridedSqFtAmt.ToString();

                    stream = new MemoryStream();
                    XmlSerializer formatter = new XmlSerializer(tempResponse.GetType());
                    formatter.Serialize(stream, tempResponse);
                    dbComps[0].P_xmlStream = stream.ToArray();

                    this.Save(_propertyItemId);

                    listViewPropertyProfilesComps.Items[listViewPropertyProfilesComps.SelectedItems[0].Index].SubItems[3].Text = (((UserControlPropertyProfileComps)x.Controls[0])).OverridedSqFtAmt.ToString();
                }
                catch { }                                
            }            			
		}
				
		private string getSecureString(object str)
		{
			string SecureString = "";
			try
			{
				SecureString = str.ToString();
			}
			catch
			{}
			return SecureString;
		}
		
        private string getMoney(string sMoney)
		{
			try
			{
				sMoney = Convert.ToDouble(sMoney).ToString("C");
			}
			catch
			{
			}

			return sMoney;
		}

		private string getDecimal(string sDecimal)
		{
			try
			{
				sDecimal = Convert.ToDouble(sDecimal).ToString("N");
			}
			catch
			{
			}

			return sDecimal;
		}

        string getSemiformattedNumber(string sDecimal)
        {
            try
            {
                sDecimal = Convert.ToDouble(sDecimal).ToString("#,##0");
            }
            catch
            {
            }

            return sDecimal;
        }

		
        private string getNumber(string sDecimal)
		{
			try
			{
                sDecimal = Convert.ToDouble(sDecimal).ToString("#,##0.00");
			}
			catch
			{
			}

			return sDecimal;
		}

		private string getDate(string sDate)
		{
			if (sDate.Length==8)
			{
				try
				{
					DateTime d = new DateTime
						(
						(int.Parse(sDate.Substring(0,4))),
						(int.Parse(sDate.Substring(4,2))),
						(int.Parse(sDate.Substring(6,2)))
						);

					sDate = d.ToShortDateString();
				}
				catch
				{
				}
			}

			return sDate;
		}
				
		private bool isInStringArray(string[] arr, string str)
		{
			foreach(string x in arr)
			{
				if (x.Equals(str))
					return true;
			}
			return false;
		}

        List<Image> downloadedImages = new List<Image>();
        DTD.Response.PROPERTY ppProperty = null;

        void On_PrintWithMaps_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.Empty == textBoxStreetA.Text || string.Empty == this.textBoxStreetNoA.Text)
                {
                    MessageBox.Show(this, "The address fields are empty, please re-enter!", "Missing data!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
            }
            catch { }

            if (this.listViewPropertyProfilesComps.Items.Count == 0)
            {
                MessageBox.Show(this, "Select the comparable properties by tagging them!!", "Select properties!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            int iTagged = 0;
            for (int i = 0; i < this.listViewPropertyProfilesComps.Items.Count; i++)
            {
                if (true == this.listViewPropertyProfilesComps.Items[i].Checked)
                {
                    iTagged++;
                    break;
                }
            }

            if (0 == iTagged)
            {
                MessageBox.Show(this, "Select the comparable properties by tagging them!!", "Select properties!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            //DOWNLOAD IMAGES - BEGIN
            List<string> addresses = new List<string>();

            string sFor = string.Empty;
            sFor += textBoxStreetNoA.Text;
            if (sFor != string.Empty)
                sFor += " ";

            if (textBoxPreDirA.Text != string.Empty)            
                sFor += textBoxPreDirA.Text + " ";
            
            if (textBoxStreetA.Text != string.Empty)            
                sFor += textBoxStreetA.Text + " ";
            
            if (textBoxPostDirA.Text != string.Empty)            
                sFor += textBoxPostDirA.Text + " ";
            
            if (textBoxSuffixA.Text != string.Empty)            
                sFor += textBoxSuffixA.Text + " ";
            
            if (textBoxUnitNoA.Text != string.Empty)            
                sFor += textBoxUnitNoA.Text + " ";
            
            if (string.Empty != textBoxStateA.Text || string.Empty != textBoxZipA.Text)
            {
                if (' ' == sFor[sFor.Length - 1])
                {
                    sFor = sFor.Substring(0, sFor.Length - 2);
                    sFor += ", ";
                }
            }

            if (textBoxStateA.Text != string.Empty)
                sFor += textBoxStateA.Text + " ";

            if (textBoxZipA.Text != string.Empty)
                sFor += textBoxZipA.Text;

            if (string.Empty != textBoxCountyA.Text)
                sFor += ", " + textBoxCountyA.Text + " County";

            addresses.Add(sFor);
            selectedProperties = this.GetTaggedComps();
            foreach (DTD.Response.PROPERTY property in selectedProperties)
            {
                string tempAddr = string.Empty;
                if (!string.IsNullOrEmpty(property._PARSED_STREET_ADDRESS._HouseNumber))
                    tempAddr += property._PARSED_STREET_ADDRESS._HouseNumber;

                if (!string.IsNullOrEmpty(property._PARSED_STREET_ADDRESS._StreetName))
                    tempAddr += " " + property._PARSED_STREET_ADDRESS._StreetName;

                if (!string.IsNullOrEmpty(property._City))
                    tempAddr += " " + property._City;

                if (!string.IsNullOrEmpty(property._State))
                    tempAddr += " " + property._State;

                if (!string.IsNullOrEmpty(property._PostalCode))
                    tempAddr += " " + property._PostalCode;

                tempAddr = tempAddr.Trim();

                addresses.Add(tempAddr);
            }

            if (addresses.Count > 11)
            {
                MessageBox.Show("Please Tag 10 or less properties!", Globals.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            //PP - BEGIN
            DealMaker.PlainClasses.OnLineData.PropertyProfile.PropertyProfile[] dbPropertyProfile = null;
            DealMaker.PlainClasses.OnLineData.PropertyProfile.PropertyProfile[] pps = PPManager.Instance.Select(_propertyItemId);

            if (null == pps || 0 == pps.Length)
            {
                MessageBox.Show("Subject Property Profile Data is not available" + Environment.NewLine + Environment.NewLine + "Please go to Property Profile tab and download Profile data for Subject Property", Globals.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            if (null != pps && 0 < pps.Length)
                dbPropertyProfile = new DealMaker.PlainClasses.OnLineData.PropertyProfile.PropertyProfile[1] { pps[0] };
            else
                return;

            if (null == dbPropertyProfile[0].P_xmlStream)
                return;

            MemoryStream stream = new MemoryStream(dbPropertyProfile[0].P_xmlStream);
            XmlSerializer serializer = new XmlSerializer(typeof(DTD.Response.RESPONSE_GROUP));
            response = (DTD.Response.RESPONSE_GROUP)serializer.Deserialize(stream);
            //if (CheckValid())
            {
                //NEW dtd 4.2.2.
                //BEGIN
                DTD.Response._PROPERTY_INFORMATION propInfo = null;
                try
                {
                    foreach (object o in response.RESPONSE.RESPONSE_DATA[0].PROPERTY_INFORMATION_RESPONSE.Items)
                    {
                        if (o is DTD.Response._PROPERTY_INFORMATION)
                        {
                            propInfo = o as DTD.Response._PROPERTY_INFORMATION;
                            break;
                        }
                    }
                }
                catch
                {
                    propInfo = null;
                }
                if (null != propInfo && null != propInfo.PROPERTY)
                    ppProperty = propInfo.PROPERTY;
                //END
            }
            //PP - END

            ImageDownloader imgDownloader = new ImageDownloader(addresses);
            ImageDownloadTracker idt = new ImageDownloadTracker(addresses.Count + 1);
            idt.ImgDownloader = imgDownloader;
            imgDownloader.StartDownload();
            idt.ShowDialog();
            downloadedImages = imgDownloader.Images;
            //DOWNLOAD IMAGES - END

            

            try
            {
                iPageCnt = 0; //reset for future

                PrintDocument tmpprndoc = new PrintDocument();
                tmpprndoc.PrintPage += new PrintPageEventHandler(Multi_PrintPageWithMaps);
                PrintPreviewDialogEx tmpprdiag = new PrintPreviewDialogEx();
                tmpprdiag.Document = tmpprndoc;

                tmpprdiag.ShowDialog();

            }
            catch (Exception)
            {
                //silent catch (not a mistake)
                //when no printer installed -> Print -> Cancel print -> Cancel would crash app.
            }
        }
										
		private void bPrint_Click(object sender, EventArgs e)
		{
			/*if ((this.textBoxOrigSqFt.Text == string.Empty) || (this.textBoxOrigSqFt.Text == "0"))
			{
				MessageBox.Show(this, "Enter Subject Area Square Footage!", "Missing data!", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}*/

			try
			{
				if (string.Empty == textBoxStreetA.Text || string.Empty == this.textBoxStreetNoA.Text)
				{
					MessageBox.Show(this, "The address fields are empty, please re-enter!", "Missing data!", MessageBoxButtons.OK, MessageBoxIcon.Information);
					return;
				}
			}
			catch {}

			if (this.listViewPropertyProfilesComps.Items.Count == 0)
			{
				MessageBox.Show(this, "Select the comparable properties by tagging them!!", "Select properties!", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}

			int iTagged = 0;
			for (int i = 0; i < this.listViewPropertyProfilesComps.Items.Count; i++)
			{
				if (true == this.listViewPropertyProfilesComps.Items[i].Checked)
				{
					iTagged++;
					break;
				}
			}

			if (0 == iTagged)
			{
				MessageBox.Show(this, "Select the comparable properties by tagging them!!", "Select properties!", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
			
			try 
			{				
				PrintDocument tmpprndoc = new PrintDocument();
				tmpprndoc.PrintPage += new PrintPageEventHandler(Multi_PrintPage);
				PrintPreviewDialogEx tmpprdiag = new PrintPreviewDialogEx();								
				tmpprdiag.Document = tmpprndoc;
			
				tmpprdiag.ShowDialog();

			}
			catch (Exception)
			{
				//silent catch (not a mistake)
				//when no printer installed -> Print -> Cancel print -> Cancel would crash app.
			}	
		}
		
		private DTD.Response.PROPERTY[] GetTaggedComps()
		{
			int iTaggedComps = 0;
			ArrayList alProperties = new ArrayList();
			for (int i = 0; i < this.listViewPropertyProfilesComps.Items.Count; i++)
			{
				if (true == ((ListViewItem)(this.listViewPropertyProfilesComps.Items[i])).Checked) 
				{
					iTaggedComps++;
					//alProperties.Add(comps[i].PROPERTY);
					Guid _printId = ((Guid)(this.listViewPropertyProfilesComps.Items[i]).Tag);
					if (this.htPrintMappingInfo.Contains(_printId))
					{
                        //inserting distance into unused field
                        ((DTD.Response._DATA_PROVIDER_COMPARABLE_SALES)this.htPrintMappingInfo[_printId]).PROPERTY._PlatName = ((DTD.Response._DATA_PROVIDER_COMPARABLE_SALES)this.htPrintMappingInfo[_printId])._DistanceFromSubjectNumber;
						alProperties.Add(((DTD.Response._DATA_PROVIDER_COMPARABLE_SALES)this.htPrintMappingInfo[_printId]).PROPERTY);
					}
				}
			}
			DTD.Response.PROPERTY[] property = new DTD.Response.PROPERTY[iTaggedComps];
			property = (DTD.Response.PROPERTY[]) alProperties.ToArray(typeof(DTD.Response.PROPERTY));
			return property;
		}

        int iPageCnt = 0;
		DTD.Response.PROPERTY[] selectedProperties = null;

        private void Multi_PrintPageWithMaps(object sender, PrintPageEventArgs e)
        {            
            //if (null == selectedProperties)
            selectedProperties = this.GetTaggedComps();
            

            if (null != selectedProperties)
            {                
                string sFor = string.Empty;
                sFor += textBoxStreetNoA.Text;
                if (sFor != string.Empty)
                    sFor += " ";

                if (textBoxPreDirA.Text != string.Empty)
                {
                    sFor += textBoxPreDirA.Text + " ";
                }

                if (textBoxStreetA.Text != string.Empty)
                {
                    sFor += textBoxStreetA.Text + " ";
                }

                if (textBoxPostDirA.Text != string.Empty)
                {
                    sFor += textBoxPostDirA.Text + " ";
                }

                if (textBoxSuffixA.Text != string.Empty)
                {
                    sFor += textBoxSuffixA.Text +" ";
                }

                if (textBoxUnitNoA.Text != string.Empty)
                {
                    sFor += textBoxUnitNoA.Text + " ";
                }

                if (string.Empty != textBoxStateA.Text || string.Empty != textBoxZipA.Text)
                {
                    if (' ' == sFor[sFor.Length - 1])
                    {
                        sFor = sFor.Substring(0, sFor.Length - 2);
                        sFor += ", ";
                    }
                }

                if (textBoxStateA.Text != string.Empty)
                    sFor += textBoxStateA.Text + " ";

                if (textBoxZipA.Text != string.Empty)
                    sFor += textBoxZipA.Text;

                if (string.Empty != textBoxCountyA.Text)
                    sFor += ", " + textBoxCountyA.Text + " County";

                string val = string.Empty;
                string header = string.Empty;
                if (IsRealistic)
                {
                    val = textBoxRealisticValue.Text;
                    //header = "CMA (Realistic COMPS) Analysis";
                }
                else
                {
                    val = textBoxConservativeValue.Text;
                    //header = "CMA (Conservative COMPS) Analysis";
                }

                //header = "ESTIMATED VALUE REPORT";
                header = "Comparative Market Analysis (CMA) Report";                

                COMPSReportGenerator.PrintMultipleDataReportWithMaps(e, header, selectedProperties, iPageCnt++,
                    string.Empty,
                    sFor,
                    textBoxOrigSqFt.Text,
                    string.Empty,
                    (null != comboBoxSqFtRange.SelectedItem ? comboBoxSqFtRange.SelectedItem.ToString() : string.Empty),
                    (null != comboBoxSalesInLast.SelectedItem ? comboBoxSalesInLast.SelectedItem.ToString() : string.Empty),
                    (null != comboBoxSearchRadius.SelectedItem ? comboBoxSearchRadius.SelectedItem.ToString() : string.Empty),
                    val,
                    selectedProperties.Length.ToString(),
                    downloadedImages, 
                    ppProperty
                    );
                
                //if (iPageCnt == selectedProperties.Length)
                //    iPageCnt = 0;

                if (!e.HasMorePages)
                    iPageCnt = 0;
            }
        }

		private void Multi_PrintPage(object sender, PrintPageEventArgs e)
		{			
			//if (null == selectedProperties)
			selectedProperties = this.GetTaggedComps();
			if (null != selectedProperties)
			{
				string sFor = string.Empty;
				sFor += textBoxStreetNoA.Text;
				if (sFor != string.Empty)
					sFor += " ";

				if (textBoxPreDirA.Text != string.Empty)
				{
					sFor += textBoxPreDirA.Text + " "; 
				}

				if (textBoxStreetA.Text != string.Empty)
				{
					sFor += textBoxStreetA.Text + " "; 
				}

				if (textBoxPostDirA.Text != string.Empty)
				{
					sFor += textBoxPostDirA.Text + " "; 
				}

				if (textBoxSuffixA.Text != string.Empty)
				{
					sFor += textBoxSuffixA.Text + " "; 
				}

				if (textBoxUnitNoA.Text != string.Empty)
				{
					sFor += textBoxUnitNoA.Text + " "; 
				}

                string val = string.Empty;
                string header = string.Empty;
                if (IsRealistic)
                {
                    val = textBoxRealisticValue.Text;
                    //header = "CMA (Realistic COMPS) Analysis";
                }
                else
                {
                    val = textBoxConservativeValue.Text;
                    //header = "CMA (Conservative COMPS) Analysis";
                }

                //header = "ESTIMATED VALUE REPORT";
                header = "Comparative Market Analysis (CMA) Report";

                

                COMPSReportGenerator.PrintMultipleDataReport(e, header, selectedProperties, iPageCnt++,
                    string.Empty,
                    sFor,
                    textBoxOrigSqFt.Text,
                    string.Empty,
                    (null != comboBoxSqFtRange.SelectedItem ? comboBoxSqFtRange.SelectedItem.ToString() : string.Empty),
                    (null != comboBoxSalesInLast.SelectedItem ? comboBoxSalesInLast.SelectedItem.ToString() : string.Empty),
                    (null != comboBoxSearchRadius.SelectedItem ? comboBoxSearchRadius.SelectedItem.ToString() : string.Empty),
                    val,
                    selectedProperties.Length.ToString()
                    );
                if (iPageCnt == selectedProperties.Length)
                    iPageCnt = 0;
			}
		}		

		private void On_ExportCOMPS(object sender, EventArgs e)
		{
			string csvDelimiter = ",";
			string quote = "\"";
			DTD.Response.PROPERTY[] taggedProperties = null;
			taggedProperties = GetTaggedComps();

			if (null == taggedProperties || 0 == taggedProperties.Length)
			{
				MessageBox.Show(this, "Select the comparable properties by tagging them!", "Select properties!", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}

			SaveFileDialog sfd = new SaveFileDialog();			
			sfd .Filter = "Comma Separated Values(*.csv)|*.csv";
			if (DialogResult.OK == sfd.ShowDialog())
			{
				string fileName = sfd.FileName;
				try
				{
					StreamWriter sw = new StreamWriter(fileName, false);
					string headerLine = string.Empty;
					headerLine += "Owner Name,Phone Number,Mailing Address,Total Living Area,Year Built,Total Rooms,Lot Area,Parking Type,Total Baths,Pool,";
					headerLine += "Garage,Fireplaces,Zoning,Land Use,Last Sale Date,Last Sale Price,Price Per Sq.Ft.,First Loan,Second Loan,";
					headerLine += "Cash Down,Deed Type,Rec. Date,Doc.No.,APN,Prop. Tax.,Assesm. Year,Deliq. Year,Tax Area,Tax Year,Exempt. Type,";
					headerLine += "Assessed Value,Land Use,Improvement Value,Legal Description,School District,Map Ref. One,Map Ref Two,";
					sw.WriteLine(headerLine);

					for (int i = 0; i < taggedProperties.Length; i++)
					{
						string line = string.Empty;
						DTD.Response.PROPERTY property = taggedProperties[i];
						
						//Owner Name
						line += quote;
						try {line += property.PROPERTY_OWNER[0]._OwnerName;} catch {}						
						line += quote;
						line += csvDelimiter;
						
						//Phone Number
						line += quote;
						try {line += property.PROPERTY_OWNER[0]._PhoneNumber;} catch {}
						line += quote;
						line += csvDelimiter;

						//Mailing Address
						line += quote;
						try
						{
							string subjectProperty = property._PARSED_STREET_ADDRESS._HouseNumber + " " + 
								property._PARSED_STREET_ADDRESS._StreetName + " " +
								property._City + ", " +
								property._State + " " +
								property._PostalCode + " ";
							line += subjectProperty;
						}
						catch {}
						//try {line += property.PROPERTY_OWNER[0]._MailingAddress + " " + property.PROPERTY_OWNER[0]._MailingCityAndState + " " + property.PROPERTY_OWNER[0]._MailingPostalCode;} catch {}
						line += quote;
						line += csvDelimiter;

						//Total Living Area
						line += quote;
						try 
						{
							string totalLivingArea = Convert.ToDouble(property._PROPERTY_CHARACTERISTICS._IMPROVEMENTS._ROOM_COUNT._TotalLivingAreaSquareFeetNumber).ToString("N");
							totalLivingArea = totalLivingArea.Substring(0, totalLivingArea.Length - 3);
							line += totalLivingArea;
						} 
						catch {}
						line += quote;
						line += csvDelimiter;

						//Year Built
						line += quote;
						try {line += property._PROPERTY_CHARACTERISTICS._IMPROVEMENTS._GENERAL_DESCRIPTION._YearBuiltDateIdentifier;} catch {}
						line += quote;
						line += csvDelimiter;

						//Total Rooms
						line += quote;
						try {line += property._PROPERTY_CHARACTERISTICS._IMPROVEMENTS._ROOM_COUNT._TotalRoomCount;} catch {}
						line += quote;
						line += csvDelimiter;

						//Lot Area
						line += quote;
						try 
						{				
							string lotArea = Convert.ToDouble(property._PROPERTY_CHARACTERISTICS._SITE._DIMENSIONS._LotAreaSquareFeetNumber).ToString("N");
							lotArea = lotArea.Substring(0, lotArea.Length - 3);
							line += lotArea;
						} 
						catch {}
						line += quote;
						line += csvDelimiter;

						//Parking Type
						line += quote;
						try {line += property._PROPERTY_CHARACTERISTICS._IMPROVEMENTS._PARKING._TypeDescription;} catch {}
						line += quote;
						line += csvDelimiter;

						//Total Baths
						line += quote;
						try {line += property._PROPERTY_CHARACTERISTICS._IMPROVEMENTS._ROOM_COUNT._TotalBathsCount;} catch {}
						line += quote;
						line += csvDelimiter;

						//Pool
						line += quote;
						try {line += property._PROPERTY_CHARACTERISTICS._IMPROVEMENTS._FEATURES._POOL._HasFeatureIndicator;} catch {}
						line += quote;
						line += csvDelimiter;

						//Garage
						line += quote;
						try {line += property._PROPERTY_CHARACTERISTICS._IMPROVEMENTS._PARKING._TypeDescription;} catch {}
						line += quote;
						line += csvDelimiter;

						//Fireplaces
						line += quote;
						try {line += property._PROPERTY_CHARACTERISTICS._IMPROVEMENTS._FEATURES._FIREPLACES._HasFeatureIndicator;} catch {}
						line += quote;
						line += csvDelimiter;

						//Zoning
						line += quote;
						try {line += property._PROPERTY_CHARACTERISTICS._SITE._ZONING._ClassificationIdentifier;} catch {}
						line += quote;
						line += csvDelimiter;

						//Land Use
						line += quote;
						try {line += property._PROPERTY_CHARACTERISTICS._SITE._CHARACTERISTICS._LandUseDescription;} catch {}
						line += quote;
						line += csvDelimiter;

						//Last Sale Date
						line += quote;
						try 
						{
							string lsd = property._PROPERTY_HISTORY._SALES_HISTORY[0]._LastSalesDate;
							DateTime d = new DateTime
								(
								(int.Parse(lsd.Substring(0,4))),
								(int.Parse(lsd.Substring(4,2))),
								(int.Parse(lsd.Substring(6,2)))
								);				
							line += d.ToShortDateString();
						} 
						catch {}
						line += quote;
						line += csvDelimiter;

						//Last Sale Price
						line += quote;
						try {line += Convert.ToDouble(property._PROPERTY_HISTORY._SALES_HISTORY[0]._LastSalesPriceAmount).ToString("C");} catch {}
						line += quote;
						line += csvDelimiter;

						//Price Per Sq.Ft.
						line += quote;
						try {line += Convert.ToDouble(property._PROPERTY_HISTORY._SALES_HISTORY[0]._PricePerSquareFootAmount).ToString("C");} catch {}
						line += quote;
						line += csvDelimiter;

						//First Loan
						line += quote;
						try {line += Convert.ToDouble(property._PROPERTY_HISTORY._MORTGAGE_HISTORY[0]._FirstMortgageAmount).ToString("C");} catch {}
						line += quote;
						line += csvDelimiter;

						//Second Loan
						line += quote;
						try {line += Convert.ToDouble(property._PROPERTY_HISTORY._MORTGAGE_HISTORY[0]._SecondMortgageAmount).ToString("C");} catch {}
						line += quote;
						line += csvDelimiter;

						//Cash Down
						line += quote;
						try {line += Convert.ToDouble(property._PROPERTY_HISTORY._SALES_HISTORY[0]._CashDownAmount).ToString("C");} catch {}
						line += quote;
						line += csvDelimiter;

						//Deed Type
						line += quote;
						try {line += property._PROPERTY_HISTORY._SALES_HISTORY[0]._DeedTypeDescription;} catch {}
						line += quote;
						line +=	csvDelimiter;

						//Rec. Date
						line += quote;
						try 
						{
							string rd = property._PROPERTY_HISTORY._SALES_HISTORY[0]._LastRecordingDate;
							DateTime d = new DateTime
								(
								(int.Parse(rd.Substring(0,4))),
								(int.Parse(rd.Substring(4,2))),
								(int.Parse(rd.Substring(6,2)))
								);

							line += d.ToShortDateString();
						} 
						catch {}
						line += quote;
						line +=	csvDelimiter;

						//Doc.No.
						line += quote;
						try {line += property._PROPERTY_HISTORY._SALES_HISTORY[0]._DocumentNumberIdentifier;} catch {}
						line += quote;
						line +=	csvDelimiter;

						//APN
						line += quote;
						try {line += property._AssessorsParcelIdentifier;}  catch {}
						line += quote;
						line +=	csvDelimiter;

						//Prop. Tax.
						line += quote;
						try {line += Convert.ToDouble(property._PROPERTY_TAX._RealEstateTotalTaxAmount).ToString("C");}  catch {}
						line += quote;
						line +=	csvDelimiter;

						//Assesm. Year
						line += quote;
						try {line += property._PROPERTY_TAX._AssessmentYear;}  catch {}
						line += quote;
						line +=	csvDelimiter;						

						//Deliq. Year
						line += quote;
						try {line += property._PROPERTY_TAX._DelinquentYear;}  catch {}
						line += quote;
						line +=	csvDelimiter;						

						//Tax Area
						line += quote;
						try {line += property._PROPERTY_TAX._RateAreaIdentifier;}  catch {}
						line += quote;
						line +=	csvDelimiter;

						//Tax Year
						line += quote;
						try {line += property._PROPERTY_TAX._TaxYear;}  catch {}
						line += quote;
						line +=	csvDelimiter;

						//Exempt. Type
						line += quote;
						try {line += property._PROPERTY_TAX._ExemptionTypeDescription;}  catch {}
						line += quote;
						line +=	csvDelimiter;

						//Assessed Value
						line += quote;
						try {line += Convert.ToDouble(property._PROPERTY_TAX._TotalAssessedValueAmount).ToString("C");}  catch {}
						line += quote;
						line +=	csvDelimiter;
						
						//Land Use
						line += quote;
						try {line += property._PROPERTY_TAX._LandValueAmount;}  catch {}
						line += quote;
						line +=	csvDelimiter;

						//Improvement Value
						line += quote;
						try {line += Convert.ToDouble(property._PROPERTY_TAX._ImprovementValueAmount).ToString("C");}  catch {}
						line += quote;
						line +=	csvDelimiter;

						//Legal Description
						line += quote;
						try {line += property._LEGAL_DESCRIPTION._TextDescription;}  catch {}
						line += quote;
						line +=	csvDelimiter;
						
						//School District
						line += quote;
						try {line += property._NEIGHBORHOOD_AREA_INFORMATION._SchoolDistrictName;}  catch {}
						line += quote;
						line +=	csvDelimiter;

						//Map Ref. One
						line += quote;
						try {line += property._NEIGHBORHOOD_AREA_INFORMATION._MapReferenceOneIdentifier;}  catch {}
						line += quote;
						line +=	csvDelimiter;

						//Map Ref Two
						line += quote;
						try {line += property._NEIGHBORHOOD_AREA_INFORMATION._MapReferenceTwoIdentifier;}  catch {}
						line += quote;
						line +=	csvDelimiter;

						sw.WriteLine(line);
					}

					sw.Flush();
					sw.Close();
				}
				catch (Exception exc)
				{
					MessageBox.Show(this, exc.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Information);
					return;
				}
			}
        }
        #endregion

        void On_Export_Click(object sender, EventArgs e)
        {
            string csvDelimiter = ",";
            string quote = "\"";
            DTD.Response.PROPERTY[] taggedProperties = null;
            taggedProperties = GetTaggedComps();

            if (null == taggedProperties || 0 == taggedProperties.Length)
            {
                MessageBox.Show(this, "Select the comparable properties by tagging them!", "Select properties!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "Comma Separated Values(*.csv)|*.csv";
            if (DialogResult.OK == sfd.ShowDialog())
            {
                string fileName = sfd.FileName;
                try
                {
                    StreamWriter sw = new StreamWriter(fileName, false);
                    string headerLine = string.Empty;
                    headerLine += "Owner Name,Phone Number,Mailing Address,Total Living Area,Year Built,Total Rooms,Total Bedrooms,Lot Area,Parking Type,Total Baths,Pool,";
                    headerLine += "Garage,Fireplaces,Zoning,Land Use,Last Sale Date,Last Sale Price,Price Per Sq.Ft.,First Loan,Second Loan,";
                    headerLine += "Cash Down,Deed Type,Rec. Date,Doc.No.,APN,Prop. Tax.,Assesm. Year,Deliq. Year,Tax Area,Tax Year,Exempt. Type,";
                    headerLine += "Assessed Value,Land Use,Improvement Value,Legal Description,School District,Map Ref. One,Map Ref Two,";
                    sw.WriteLine(headerLine);

                    for (int i = 0; i < taggedProperties.Length; i++)
                    {
                        string line = string.Empty;
                        DTD.Response.PROPERTY property = taggedProperties[i];

                        //Owner Name
                        line += quote;
                        try { line += property.PROPERTY_OWNER[0]._OwnerName; }
                        catch { }
                        line += quote;
                        line += csvDelimiter;

                        //Phone Number
                        line += quote;
                        try { line += property.PROPERTY_OWNER[0]._PhoneNumber; }
                        catch { }
                        line += quote;
                        line += csvDelimiter;

                        //Mailing Address
                        line += quote;
                        try
                        {
                            string subjectProperty = property._PARSED_STREET_ADDRESS._HouseNumber + " " +
                                property._PARSED_STREET_ADDRESS._StreetName + " " +
                                property._City + ", " +
                                property._State + " " +
                                property._PostalCode + " ";
                            line += subjectProperty;
                        }
                        catch { }
                        //try {line += property.PROPERTY_OWNER[0]._MailingAddress + " " + property.PROPERTY_OWNER[0]._MailingCityAndState + " " + property.PROPERTY_OWNER[0]._MailingPostalCode;} catch {}
                        line += quote;
                        line += csvDelimiter;

                        //Total Living Area
                        line += quote;
                        try
                        {
                            string totalLivingArea = Convert.ToDouble(property._PROPERTY_CHARACTERISTICS._IMPROVEMENTS._ROOM_COUNT._TotalLivingAreaSquareFeetNumber).ToString("N");
                            totalLivingArea = totalLivingArea.Substring(0, totalLivingArea.Length - 3);
                            line += totalLivingArea;
                        }
                        catch { }
                        line += quote;
                        line += csvDelimiter;

                        //Year Built
                        line += quote;
                        try { line += property._PROPERTY_CHARACTERISTICS._IMPROVEMENTS._GENERAL_DESCRIPTION._YearBuiltDateIdentifier; }
                        catch { }
                        line += quote;
                        line += csvDelimiter;

                        //Total Rooms
                        line += quote;
                        try { line += property._PROPERTY_CHARACTERISTICS._IMPROVEMENTS._ROOM_COUNT._TotalRoomCount; }
                        catch { }
                        line += quote;
                        line += csvDelimiter;

                        //Total Bedrooms _TotalBedroomsCount
                        line += quote;
                        try { line += property._PROPERTY_CHARACTERISTICS._IMPROVEMENTS._ROOM_COUNT._TotalBedroomsCount; }
                        catch { }
                        line += quote;
                        line += csvDelimiter;

                        //Lot Area
                        line += quote;
                        try
                        {
                            string lotArea = Convert.ToDouble(property._PROPERTY_CHARACTERISTICS._SITE._DIMENSIONS._LotAreaSquareFeetNumber).ToString("N");
                            lotArea = lotArea.Substring(0, lotArea.Length - 3);
                            line += lotArea;
                        }
                        catch { }
                        line += quote;
                        line += csvDelimiter;

                        //Parking Type
                        line += quote;
                        try { line += property._PROPERTY_CHARACTERISTICS._IMPROVEMENTS._PARKING._TypeDescription; }
                        catch { }
                        line += quote;
                        line += csvDelimiter;

                        //Total Baths
                        line += quote;
                        try { line += property._PROPERTY_CHARACTERISTICS._IMPROVEMENTS._ROOM_COUNT._TotalBathsCount; }
                        catch { }
                        line += quote;
                        line += csvDelimiter;

                        //Pool
                        line += quote;
                        try { line += property._PROPERTY_CHARACTERISTICS._IMPROVEMENTS._FEATURES._POOL._HasFeatureIndicator; }
                        catch { }
                        line += quote;
                        line += csvDelimiter;

                        //Garage
                        line += quote;
                        try { line += property._PROPERTY_CHARACTERISTICS._IMPROVEMENTS._PARKING._TypeDescription; }
                        catch { }
                        line += quote;
                        line += csvDelimiter;

                        //Fireplaces
                        line += quote;
                        try { line += property._PROPERTY_CHARACTERISTICS._IMPROVEMENTS._FEATURES._FIREPLACES._HasFeatureIndicator; }
                        catch { }
                        line += quote;
                        line += csvDelimiter;

                        //Zoning
                        line += quote;
                        try { line += property._PROPERTY_CHARACTERISTICS._SITE._ZONING._ClassificationIdentifier; }
                        catch { }
                        line += quote;
                        line += csvDelimiter;

                        //Land Use
                        line += quote;
                        try { line += property._PROPERTY_CHARACTERISTICS._SITE._CHARACTERISTICS._LandUseDescription; }
                        catch { }
                        line += quote;
                        line += csvDelimiter;

                        //Last Sale Date
                        line += quote;
                        try
                        {
                            string lsd = property._PROPERTY_HISTORY._SALES_HISTORY[0]._LastSalesDate;
                            DateTime d = new DateTime
                                (
                                (int.Parse(lsd.Substring(0, 4))),
                                (int.Parse(lsd.Substring(4, 2))),
                                (int.Parse(lsd.Substring(6, 2)))
                                );
                            line += d.ToShortDateString();
                        }
                        catch { }
                        line += quote;
                        line += csvDelimiter;

                        //Last Sale Price
                        line += quote;
                        try { line += Convert.ToDouble(property._PROPERTY_HISTORY._SALES_HISTORY[0]._LastSalesPriceAmount).ToString("C"); }
                        catch { }
                        line += quote;
                        line += csvDelimiter;

                        //Price Per Sq.Ft.
                        line += quote;
                        try { line += Convert.ToDouble(property._PROPERTY_HISTORY._SALES_HISTORY[0]._PricePerSquareFootAmount).ToString("C"); }
                        catch { }
                        line += quote;
                        line += csvDelimiter;

                        //First Loan
                        line += quote;
                        try { line += Convert.ToDouble(property._PROPERTY_HISTORY._MORTGAGE_HISTORY[0]._FirstMortgageAmount).ToString("C"); }
                        catch { }
                        line += quote;
                        line += csvDelimiter;

                        //Second Loan
                        line += quote;
                        try { line += Convert.ToDouble(property._PROPERTY_HISTORY._MORTGAGE_HISTORY[0]._SecondMortgageAmount).ToString("C"); }
                        catch { }
                        line += quote;
                        line += csvDelimiter;

                        //Cash Down
                        line += quote;
                        try { line += Convert.ToDouble(property._PROPERTY_HISTORY._SALES_HISTORY[0]._CashDownAmount).ToString("C"); }
                        catch { }
                        line += quote;
                        line += csvDelimiter;

                        //Deed Type
                        line += quote;
                        try { line += property._PROPERTY_HISTORY._SALES_HISTORY[0]._DeedTypeDescription; }
                        catch { }
                        line += quote;
                        line += csvDelimiter;

                        //Rec. Date
                        line += quote;
                        try
                        {
                            string rd = property._PROPERTY_HISTORY._SALES_HISTORY[0]._LastRecordingDate;
                            DateTime d = new DateTime
                                (
                                (int.Parse(rd.Substring(0, 4))),
                                (int.Parse(rd.Substring(4, 2))),
                                (int.Parse(rd.Substring(6, 2)))
                                );

                            line += d.ToShortDateString();
                        }
                        catch { }
                        line += quote;
                        line += csvDelimiter;

                        //Doc.No.
                        line += quote;
                        try { line += property._PROPERTY_HISTORY._SALES_HISTORY[0]._DocumentNumberIdentifier; }
                        catch { }
                        line += quote;
                        line += csvDelimiter;

                        //APN
                        line += quote;
                        try { line += property._AssessorsParcelIdentifier; }
                        catch { }
                        line += quote;
                        line += csvDelimiter;

                        //Prop. Tax.
                        line += quote;
                        try { line += Convert.ToDouble(property._PROPERTY_TAX._RealEstateTotalTaxAmount).ToString("C"); }
                        catch { }
                        line += quote;
                        line += csvDelimiter;

                        //Assesm. Year
                        line += quote;
                        try { line += property._PROPERTY_TAX._AssessmentYear; }
                        catch { }
                        line += quote;
                        line += csvDelimiter;

                        //Deliq. Year
                        line += quote;
                        try { line += property._PROPERTY_TAX._DelinquentYear; }
                        catch { }
                        line += quote;
                        line += csvDelimiter;

                        //Tax Area
                        line += quote;
                        try { line += property._PROPERTY_TAX._RateAreaIdentifier; }
                        catch { }
                        line += quote;
                        line += csvDelimiter;

                        //Tax Year
                        line += quote;
                        try { line += property._PROPERTY_TAX._TaxYear; }
                        catch { }
                        line += quote;
                        line += csvDelimiter;

                        //Exempt. Type
                        line += quote;
                        try { line += property._PROPERTY_TAX._ExemptionTypeDescription; }
                        catch { }
                        line += quote;
                        line += csvDelimiter;

                        //Assessed Value
                        line += quote;
                        try { line += Convert.ToDouble(property._PROPERTY_TAX._TotalAssessedValueAmount).ToString("C"); }
                        catch { }
                        line += quote;
                        line += csvDelimiter;

                        //Land Use
                        line += quote;
                        try { line += property._PROPERTY_TAX._LandValueAmount; }
                        catch { }
                        line += quote;
                        line += csvDelimiter;

                        //Improvement Value
                        line += quote;
                        try { line += Convert.ToDouble(property._PROPERTY_TAX._ImprovementValueAmount).ToString("C"); }
                        catch { }
                        line += quote;
                        line += csvDelimiter;

                        //Legal Description
                        line += quote;
                        try { line += property._LEGAL_DESCRIPTION._TextDescription; }
                        catch { }
                        line += quote;
                        line += csvDelimiter;

                        //School District
                        line += quote;
                        try { line += property._NEIGHBORHOOD_AREA_INFORMATION._SchoolDistrictName; }
                        catch { }
                        line += quote;
                        line += csvDelimiter;

                        //Map Ref. One
                        line += quote;
                        try { line += property._NEIGHBORHOOD_AREA_INFORMATION._MapReferenceOneIdentifier; }
                        catch { }
                        line += quote;
                        line += csvDelimiter;

                        //Map Ref Two
                        line += quote;
                        try { line += property._NEIGHBORHOOD_AREA_INFORMATION._MapReferenceTwoIdentifier; }
                        catch { }
                        line += quote;
                        line += csvDelimiter;

                        sw.WriteLine(line);
                    }

                    sw.Flush();
                    sw.Close();
                }
                catch (Exception exc)
                {
                    MessageBox.Show(this, exc.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
            }	
        }
#if DUMP_XML_OBJECT
        private void dumpObject(object theObject, string filename)
        {
            
            System.Xml.Serialization.XmlSerializer serializer
                = new System.Xml.Serialization.XmlSerializer(theObject.GetType());

            // IMPORTANT  -  we do not need a namespace!
            System.Xml.Serialization.XmlSerializerNamespaces nmspace = new XmlSerializerNamespaces();
            nmspace.Add("", "");
            // IMPORTANT  -  we do not need a namespace!

            System.Xml.XmlTextWriter writer = new XmlTextWriter(filename, System.Text.UTF8Encoding.UTF8);
            writer.Formatting = System.Xml.Formatting.Indented;
            writer.WriteStartDocument();
            writer.WriteDocType("REQUEST_GROUP", null, "C2DRequestv2.0.PropertyProfileComps", null);
            serializer.Serialize(writer, theObject, nmspace);
            writer.Flush();
            writer.Close();
            
        }
#endif
    }	
}
