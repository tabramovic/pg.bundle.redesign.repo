#define NEW_DTD
//#define DUMP_XML_OBJECT
//#define SHOW_CHARGED

using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;
using System.IO;
using System.Xml.Serialization;
using System.Web;
using System.Text;
using System.Xml;
using System.Net;
using System.Diagnostics;
using System.Data.SqlClient;
using System.Drawing.Printing;

//Added
using DealMaker;
using DealMaker.UserControls;
using DealMaker.PlainClasses.OnLineData.PropertyProfile;
using ProfitGrabber.Common;


namespace DealMaker.OnlineData
{
	/// <summary>
	/// Summary description for UserControl1.
	/// </summary>
	public class UserControlPropertyProfileComps : System.Windows.Forms.UserControl
    {
        #region DATA
        //AUTOSAVE
		public delegate void Success_PP_Download();
		public event Success_PP_Download PP_Downloaded_Successfully;
		bool _allowAutoSaveTriggerEvents = false;				
		DataConsumption.ServiceDataConsumption sdc = null;						
		private DTD.Response.RESPONSE_GROUP response;
		private DTD.Response.PROPERTY property;
		private string comparableNumber;
		private string distanceFromSubject;
        private DealMaker.PlainClasses.OnLineData.PropertyProfile.PropertyProfile[] dbPropertyProfile = null;
		private SqlConnection _dbConn = null;
		private bool _PPInDb = false;		
		
        /// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
		private System.Windows.Forms.TextBox textBoxOwnerName;
		private System.Windows.Forms.Label labelOwnerName;
		private System.Windows.Forms.TextBox textBoxSchoolDistrict;
		private System.Windows.Forms.TextBox textBoxLotNumber;
		private System.Windows.Forms.TextBox textBoxSubdivision;
		private System.Windows.Forms.TextBox textBoxComparableNumber;
		private System.Windows.Forms.TextBox textBoxDistanceFromSubject;
		private System.Windows.Forms.TabPage tabPagePropertyCharacteristics;
		private System.Windows.Forms.TextBox textBoxCounty;
		private System.Windows.Forms.Label labelSchoolDistrict;
		private System.Windows.Forms.Label labelLotNumber;
		private System.Windows.Forms.Label labelSubdivision;
		private System.Windows.Forms.Label labelComparableNumber;
		private System.Windows.Forms.Label labelDistanceFromSubject;
		private System.Windows.Forms.Label labelCounty;
		private System.Windows.Forms.TabPage tabPageOwnershipNLocations;
		private System.Windows.Forms.TabControl tabControlOnlinePropertyProfileNComps;
		private System.Windows.Forms.TabPage tabPageTaxesNLegal;
		private System.Windows.Forms.TabPage tabPageFinancials;
		private System.Windows.Forms.TextBox textBoxPostDirection;
		private System.Windows.Forms.Label labelPostDirection;
		private System.Windows.Forms.TextBox textBoxMailingZipCode;
		private System.Windows.Forms.Label labelMailingZipCode;
		private System.Windows.Forms.TextBox textBoxMailingCityAndState;
		private System.Windows.Forms.Label labelMailingCityAndState;
		private System.Windows.Forms.TextBox textBoxMailingAddress;
		private System.Windows.Forms.Label labelMailingAddress;
		private System.Windows.Forms.TextBox textBoxMapReferenceTwo;
		private System.Windows.Forms.Label labelMapReferenceTwo;
		private System.Windows.Forms.TextBox textBoxMapReferenceOne;
		private System.Windows.Forms.Label labelMapReferenceOne;
		private System.Windows.Forms.TextBox textBoxZipCode;
		private System.Windows.Forms.Label labelZipCode;
		private System.Windows.Forms.TextBox textBoxState;
		private System.Windows.Forms.Label labelState;
		private System.Windows.Forms.TextBox textBoxCity;
		private System.Windows.Forms.Label labelCity;
		private System.Windows.Forms.TextBox textBoxUnitNumber;
		private System.Windows.Forms.Label labelUnitNumber;
		private System.Windows.Forms.TextBox textBoxSuffix;
		private System.Windows.Forms.Label labelSuffix;
		private System.Windows.Forms.TextBox textBoxStreetnName;
		private System.Windows.Forms.Label labelStreetnName;
		private System.Windows.Forms.TextBox textBoxPreDirection;
		private System.Windows.Forms.Label labelPreDirection;
		private System.Windows.Forms.TextBox textBoxHouseNumber;
		private System.Windows.Forms.Label labelHouseNumber;
		private System.Windows.Forms.Label labelOwnerNameGroup;
		private System.Windows.Forms.Label labelSiteAddress;
		private System.Windows.Forms.TextBox textBoxTotalBaths;
		private System.Windows.Forms.Label labelTotalBaths;
		private System.Windows.Forms.TextBox textBoxTotalBedrooms;
		private System.Windows.Forms.Label labelTotalBedrooms;
		private System.Windows.Forms.TextBox textBoxTotalRooms;
		private System.Windows.Forms.Label labelTotalRooms;
		private System.Windows.Forms.TextBox textBoxZoning;
		private System.Windows.Forms.Label labelZoning;
		private System.Windows.Forms.TextBox textBoxNumberOfStories;
		private System.Windows.Forms.Label labelNumberOfStories;
		private System.Windows.Forms.TextBox textBoxAirConditioning;
		private System.Windows.Forms.Label labelAirConditioning;
		private System.Windows.Forms.TextBox textBoxPool;
		private System.Windows.Forms.Label labelPool;
		private System.Windows.Forms.TextBox textBoxRoofType;
		private System.Windows.Forms.Label labelRoofType;
		private System.Windows.Forms.TextBox textBoxParkingCapacity;
		private System.Windows.Forms.Label labelParkingCapacity;
		private System.Windows.Forms.TextBox textBoxParkingType;
		private System.Windows.Forms.Label labelParkingType;
		private System.Windows.Forms.TextBox textBoxLotSize;
		private System.Windows.Forms.Label labelLotSize;
		private System.Windows.Forms.TextBox textBoxLotArea;
		private System.Windows.Forms.Label labelLotArea;
		private System.Windows.Forms.TextBox textBoxTotalLivingArea;
		private System.Windows.Forms.Label labelTotalLivingArea;
		private System.Windows.Forms.TextBox textBoxYearBuilt;
		private System.Windows.Forms.Label labelYearBuilt;
		private System.Windows.Forms.TextBox textBoxSellerName;
		private System.Windows.Forms.Label labelSellerName;
		private System.Windows.Forms.TextBox textBox4PricePerSqFt;
		private System.Windows.Forms.Label labelPricePerSqFt;
		private System.Windows.Forms.TextBox textBoxCashDown;
		private System.Windows.Forms.Label labelCashDown;
		private System.Windows.Forms.TextBox textBoxFirstMortgageLender;
		private System.Windows.Forms.Label labelFirstMortgageLender;
		private System.Windows.Forms.TextBox textBoxType;
		private System.Windows.Forms.Label labelFirstMortgageInterestRateType;
		private System.Windows.Forms.TextBox textBoxFirstMortgageInterestRateTerm;
		private System.Windows.Forms.Label labelFirstMortgageInterestRateTerm;
		private System.Windows.Forms.TextBox textBoxFirstMortgageInterestRate;
		private System.Windows.Forms.Label labelFirstMortgageInterestRate;
		private System.Windows.Forms.TextBox textBoxFirstMortgageAmount;
		private System.Windows.Forms.Label labelFirstMortgageAmount;
		private System.Windows.Forms.TextBox textBoxRecordingDate;
		private System.Windows.Forms.Label labelRecordingDate;
		private System.Windows.Forms.TextBox textBoxDocumentNumber;
		private System.Windows.Forms.Label labelDocumentNumber;
		private System.Windows.Forms.TextBox textBoxDeedType;
		private System.Windows.Forms.Label labelDeedType;
		private System.Windows.Forms.TextBox textBoxSaleType1;
		private System.Windows.Forms.Label labelSaleType1;
		private System.Windows.Forms.TextBox textBoxSalePrice;
		private System.Windows.Forms.Label labelSalePrice;
		private System.Windows.Forms.TextBox textBoxSaleDate;
		private System.Windows.Forms.Label labelSaleDate;
		private System.Windows.Forms.TextBox textBoxTownshipName;
		private System.Windows.Forms.Label labelTownshipName;
		private System.Windows.Forms.TextBox textBoxLegalDescription;
		private System.Windows.Forms.Label labelLegalDescription;
		private System.Windows.Forms.TextBox textBoxDelinquentYear;
		private System.Windows.Forms.Label labelDelinquentYear;
		private System.Windows.Forms.TextBox textBoxImprovementValue;
		private System.Windows.Forms.Label labelImprovementValue;
		private System.Windows.Forms.TextBox textBoxLandValue;
		private System.Windows.Forms.Label labelLandValue;
		private System.Windows.Forms.TextBox textBoxLandUseType;
		private System.Windows.Forms.Label labelLandUseType;
		private System.Windows.Forms.TextBox textBoxExemptionType;
		private System.Windows.Forms.Label labelExemptionType;
		private System.Windows.Forms.TextBox textBoxTaxYear;
		private System.Windows.Forms.Label labelTaxYear;
		private System.Windows.Forms.TextBox textBoxTaxArea;
		private System.Windows.Forms.Label labelTaxArea;
		private System.Windows.Forms.TextBox textBoxPropertyTax;
		private System.Windows.Forms.Label labelPropertyTax;
		private System.Windows.Forms.TextBox textBoxAssesmentYear;
		private System.Windows.Forms.Label labelAssesmentYear;
		private System.Windows.Forms.TextBox textBoxTotalAssesedValue;
		private System.Windows.Forms.Label labelTotalAssesedValue;
		private System.Windows.Forms.TextBox textBoxAlternateAPN;
		private System.Windows.Forms.Label labelAlternateAPN;
		private System.Windows.Forms.TextBox textBoxAPN;
		private System.Windows.Forms.Label labelAPN;
		private System.Windows.Forms.TextBox textBoxPhoneNumber;
		private System.Windows.Forms.TextBox textBoxLandUse;
		private System.Windows.Forms.Label labelLandUse;
		private System.Windows.Forms.TextBox textBoxLotWidth;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox textBoxLotDepth;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox textBoxGarageArea;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TextBox textBoxFireplaces;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.TextBox textBoxRoofMaterials;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.TextBox textBoxFoundation;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.TextBox textBoxQuality;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.TextBox textBoxCondition;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.TextBox textBoxBasementArea;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.TextBox textBoxBasement;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.Label label13;
		private System.Windows.Forms.Label label14;
		private System.Windows.Forms.TextBox textBoxSaleType2;
		private System.Windows.Forms.Label label15;
		private System.Windows.Forms.Label label16;
		private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
		private System.Windows.Forms.Label label22;
		private System.Windows.Forms.Label label23;
		private System.Windows.Forms.Label label24;
		private System.Windows.Forms.Label label25;
		private System.Windows.Forms.Label label26;
		private System.Windows.Forms.Label label27;
		private System.Windows.Forms.Label label28;
		private System.Windows.Forms.Label label29;
		private System.Windows.Forms.Label label19;
		private System.Windows.Forms.Label label30;
		private System.Windows.Forms.Label label31;
		private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
		private System.Windows.Forms.Label label35;
		private System.Windows.Forms.Label label36;
		private System.Windows.Forms.Label label37;
		private System.Windows.Forms.Label label38;
		private System.Windows.Forms.Label label39;
		private System.Windows.Forms.Label label40;
		private System.Windows.Forms.Label label41;
		private System.Windows.Forms.Label label42;
		private System.Windows.Forms.Label label43;
		private System.Windows.Forms.Label label44;
		private System.Windows.Forms.Label label45;
		private System.Windows.Forms.Label label46;
		private System.Windows.Forms.Label label47;
		private System.Windows.Forms.Label label48;
		private System.Windows.Forms.Label label49;
		private System.Windows.Forms.Label label50;
		private System.Windows.Forms.Label label51;
		private System.Windows.Forms.Label label52;
		private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label label54;
		private System.Windows.Forms.Label label58;
		private System.Windows.Forms.Label label59;
		private System.Windows.Forms.Label label60;
		private System.Windows.Forms.Label label61;
		private System.Windows.Forms.Label label62;
		private System.Windows.Forms.TextBox textBoxStateC;
		private System.Windows.Forms.TextBox textBoxZipC;
		private System.Windows.Forms.TextBox textBoxLatNameC;
		private System.Windows.Forms.TextBox textBoxFirstNameC;
		private System.Windows.Forms.Label label63;
		private System.Windows.Forms.Label label64;
		private System.Windows.Forms.Label label65;
		private System.Windows.Forms.Label label66;
		private System.Windows.Forms.Label label67;
		private System.Windows.Forms.TextBox textBoxStateB;
		private System.Windows.Forms.TextBox textBoxZipB;
		private System.Windows.Forms.TextBox textBoxApnB;
		private System.Windows.Forms.Label label68;
		private System.Windows.Forms.Label label69;
		private System.Windows.Forms.Label label70;
		private System.Windows.Forms.Label label71;
		private System.Windows.Forms.Button buttonSearchC;
		private System.Windows.Forms.Label label72;
		private System.Windows.Forms.Button buttonSearchB;
		private System.Windows.Forms.Label label73;
		private System.Windows.Forms.TextBox textBoxStateA;
		private System.Windows.Forms.TextBox textBoxZipA;
		private System.Windows.Forms.TextBox textBoxUnitNoA;
		private System.Windows.Forms.TextBox textBoxSuffixA;
		private System.Windows.Forms.TextBox textBoxPostDirA;
		private System.Windows.Forms.TextBox textBoxStreetA;
		private System.Windows.Forms.TextBox textBoxPreDirA;
		private System.Windows.Forms.TextBox textBoxStreetNoA;
		private System.Windows.Forms.Label label74;
		private System.Windows.Forms.Label label75;
		private System.Windows.Forms.Label label76;
		private System.Windows.Forms.Label label77;
		private System.Windows.Forms.Label label78;
		private System.Windows.Forms.Label label79;
		private System.Windows.Forms.Label label80;
		private System.Windows.Forms.Label label81;
		private System.Windows.Forms.Label label82;
		private System.Windows.Forms.Button buttonSearchA;
        private System.Windows.Forms.Label label83;
		private System.Windows.Forms.TextBox textBoxCensusTract;
		private System.Windows.Forms.TextBox textBoxLegalBlockBldg;
		private System.Windows.Forms.TextBox textBoxLegalBookPage;
		private System.Windows.Forms.TextBox textBoxPSMInterestRateType;
		private System.Windows.Forms.TextBox textBoxPSMInterestRateTerm;
		private System.Windows.Forms.TextBox textBoxPSMInterestRate;
		private System.Windows.Forms.TextBox textBoxPSMMortgageAmount;
		private System.Windows.Forms.TextBox textBoxPFMLender;
		private System.Windows.Forms.TextBox textBoxPFMInterestRateType;
		private System.Windows.Forms.TextBox textBoxPFMInterestRateTerm;
		private System.Windows.Forms.TextBox textBoxPFMInterestRate;
		private System.Windows.Forms.TextBox textBoxPFMMortgageAmount;
		private System.Windows.Forms.TextBox textBoxPSStampAmt;
		private System.Windows.Forms.TextBox textBoxPSDocumentNumber;
		private System.Windows.Forms.TextBox textBoxPSRecordingDate;
		private System.Windows.Forms.TextBox textBoxPSDeedType;
		private System.Windows.Forms.TextBox textBoxPSSaleType1;
		private System.Windows.Forms.TextBox textBoxPSSalePrice;
		private System.Windows.Forms.TextBox textBoxPSSaleDate;
		private System.Windows.Forms.TextBox textBoxSecInterestRateType;
		private System.Windows.Forms.TextBox textBoxSecInterestRateTerm;
		private System.Windows.Forms.TextBox textBoxSecInterestRate;
		private System.Windows.Forms.TextBox textBoxSecMortgageAmount;
		private System.Windows.Forms.TextBox textBoxStampAmt;
		private System.Windows.Forms.TextBox textBoxTitleCompany;
		private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.Panel panel3;
		private System.Windows.Forms.TextBox textBoxOtherImprovements;
		private System.Windows.Forms.Label label57;
		private System.Windows.Forms.Label label86;
		private System.Windows.Forms.Label label87;
		private System.Windows.Forms.TextBox textBoxCountyC;
		private System.Windows.Forms.TextBox textBoxCountyB;
		private System.Windows.Forms.TextBox textBoxCountyA;
		private System.Windows.Forms.PictureBox pictureBoxTaxesLegal;
		private System.Windows.Forms.PictureBox pictureBoxPropCharact;
        private System.Windows.Forms.PictureBox pictureBoxFinancials;
		private System.Windows.Forms.Label label88;
		private System.Windows.Forms.Label label89;
		private System.Windows.Forms.Label label85;
		private System.Windows.Forms.Label label90;
		private System.Windows.Forms.Button bPrintReport;
        private Line line1;
        private GroupBox groupBox1;
        private GroupBox groupBox2;
        private GroupBox groupBox3;
        private GroupBox groupBox4;
        private GroupBox groupBox5;
        private GroupBox groupBox6;
        private Label _accurateSearch;
        private Button _updateSqFt;
		private System.Windows.Forms.Label labelPhoneNumber;
        #endregion

        #region PROPERTIES
        public bool AllowAutoSaveTriggerEvents
        {
            get { return _allowAutoSaveTriggerEvents; }
            set { _allowAutoSaveTriggerEvents = value; }
        }

        bool AccurateSearchLabelEnabled
        {
            get { return _accurateSearch.Visible; }
            set { _accurateSearch.Visible = value; }
        }
        #endregion

        #region CTR
        public UserControlPropertyProfileComps()
		{	
			// TODO: do we need this?
			System.Threading.Thread.CurrentThread.CurrentCulture=System.Globalization.CultureInfo.CreateSpecificCulture("EN-US");

			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();
			panel3.Visible=false;

            OverridedSqFt = false;		
		}
		#endregion

        #region DISPOSE
        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }
        #endregion

        #region Component Designer generated code
        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UserControlPropertyProfileComps));
            this.textBoxMailingZipCode = new System.Windows.Forms.TextBox();
            this.labelMailingZipCode = new System.Windows.Forms.Label();
            this.textBoxMailingCityAndState = new System.Windows.Forms.TextBox();
            this.labelMailingCityAndState = new System.Windows.Forms.Label();
            this.textBoxMailingAddress = new System.Windows.Forms.TextBox();
            this.labelMailingAddress = new System.Windows.Forms.Label();
            this.textBoxMapReferenceTwo = new System.Windows.Forms.TextBox();
            this.labelMapReferenceTwo = new System.Windows.Forms.Label();
            this.textBoxMapReferenceOne = new System.Windows.Forms.TextBox();
            this.labelMapReferenceOne = new System.Windows.Forms.Label();
            this.textBoxZipCode = new System.Windows.Forms.TextBox();
            this.labelZipCode = new System.Windows.Forms.Label();
            this.textBoxState = new System.Windows.Forms.TextBox();
            this.labelState = new System.Windows.Forms.Label();
            this.textBoxCity = new System.Windows.Forms.TextBox();
            this.labelCity = new System.Windows.Forms.Label();
            this.textBoxUnitNumber = new System.Windows.Forms.TextBox();
            this.labelUnitNumber = new System.Windows.Forms.Label();
            this.textBoxSuffix = new System.Windows.Forms.TextBox();
            this.labelSuffix = new System.Windows.Forms.Label();
            this.textBoxPostDirection = new System.Windows.Forms.TextBox();
            this.labelPostDirection = new System.Windows.Forms.Label();
            this.textBoxStreetnName = new System.Windows.Forms.TextBox();
            this.labelStreetnName = new System.Windows.Forms.Label();
            this.textBoxPreDirection = new System.Windows.Forms.TextBox();
            this.labelPreDirection = new System.Windows.Forms.Label();
            this.textBoxHouseNumber = new System.Windows.Forms.TextBox();
            this.labelHouseNumber = new System.Windows.Forms.Label();
            this.textBoxPhoneNumber = new System.Windows.Forms.TextBox();
            this.labelPhoneNumber = new System.Windows.Forms.Label();
            this.textBoxOwnerName = new System.Windows.Forms.TextBox();
            this.labelOwnerName = new System.Windows.Forms.Label();
            this.labelOwnerNameGroup = new System.Windows.Forms.Label();
            this.textBoxSchoolDistrict = new System.Windows.Forms.TextBox();
            this.textBoxLotNumber = new System.Windows.Forms.TextBox();
            this.textBoxSubdivision = new System.Windows.Forms.TextBox();
            this.textBoxComparableNumber = new System.Windows.Forms.TextBox();
            this.textBoxDistanceFromSubject = new System.Windows.Forms.TextBox();
            this.tabPagePropertyCharacteristics = new System.Windows.Forms.TabPage();
            this._updateSqFt = new System.Windows.Forms.Button();
            this.textBoxOtherImprovements = new System.Windows.Forms.TextBox();
            this.textBoxAirConditioning = new System.Windows.Forms.TextBox();
            this.textBoxBasementArea = new System.Windows.Forms.TextBox();
            this.textBoxBasement = new System.Windows.Forms.TextBox();
            this.textBoxCondition = new System.Windows.Forms.TextBox();
            this.textBoxQuality = new System.Windows.Forms.TextBox();
            this.textBoxFoundation = new System.Windows.Forms.TextBox();
            this.textBoxRoofMaterials = new System.Windows.Forms.TextBox();
            this.textBoxFireplaces = new System.Windows.Forms.TextBox();
            this.textBoxGarageArea = new System.Windows.Forms.TextBox();
            this.textBoxLotWidth = new System.Windows.Forms.TextBox();
            this.textBoxLotDepth = new System.Windows.Forms.TextBox();
            this.textBoxLandUse = new System.Windows.Forms.TextBox();
            this.textBoxTotalBaths = new System.Windows.Forms.TextBox();
            this.textBoxTotalBedrooms = new System.Windows.Forms.TextBox();
            this.textBoxTotalRooms = new System.Windows.Forms.TextBox();
            this.textBoxZoning = new System.Windows.Forms.TextBox();
            this.textBoxNumberOfStories = new System.Windows.Forms.TextBox();
            this.textBoxPool = new System.Windows.Forms.TextBox();
            this.textBoxRoofType = new System.Windows.Forms.TextBox();
            this.textBoxParkingCapacity = new System.Windows.Forms.TextBox();
            this.textBoxParkingType = new System.Windows.Forms.TextBox();
            this.textBoxLotSize = new System.Windows.Forms.TextBox();
            this.textBoxLotArea = new System.Windows.Forms.TextBox();
            this.textBoxTotalLivingArea = new System.Windows.Forms.TextBox();
            this.textBoxYearBuilt = new System.Windows.Forms.TextBox();
            this.label57 = new System.Windows.Forms.Label();
            this.labelAirConditioning = new System.Windows.Forms.Label();
            this.pictureBoxPropCharact = new System.Windows.Forms.PictureBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.labelLandUse = new System.Windows.Forms.Label();
            this.labelTotalBaths = new System.Windows.Forms.Label();
            this.labelTotalBedrooms = new System.Windows.Forms.Label();
            this.labelTotalRooms = new System.Windows.Forms.Label();
            this.labelZoning = new System.Windows.Forms.Label();
            this.labelNumberOfStories = new System.Windows.Forms.Label();
            this.labelPool = new System.Windows.Forms.Label();
            this.labelRoofType = new System.Windows.Forms.Label();
            this.labelParkingCapacity = new System.Windows.Forms.Label();
            this.labelParkingType = new System.Windows.Forms.Label();
            this.labelLotSize = new System.Windows.Forms.Label();
            this.labelLotArea = new System.Windows.Forms.Label();
            this.labelTotalLivingArea = new System.Windows.Forms.Label();
            this.labelYearBuilt = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.textBoxCounty = new System.Windows.Forms.TextBox();
            this.labelSchoolDistrict = new System.Windows.Forms.Label();
            this.labelLotNumber = new System.Windows.Forms.Label();
            this.labelSubdivision = new System.Windows.Forms.Label();
            this.labelComparableNumber = new System.Windows.Forms.Label();
            this.labelDistanceFromSubject = new System.Windows.Forms.Label();
            this.tabPageOwnershipNLocations = new System.Windows.Forms.TabPage();
            this.bPrintReport = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this._accurateSearch = new System.Windows.Forms.Label();
            this.textBoxStateC = new System.Windows.Forms.TextBox();
            this.textBoxCountyC = new System.Windows.Forms.TextBox();
            this.textBoxZipC = new System.Windows.Forms.TextBox();
            this.textBoxLatNameC = new System.Windows.Forms.TextBox();
            this.textBoxFirstNameC = new System.Windows.Forms.TextBox();
            this.buttonSearchC = new System.Windows.Forms.Button();
            this.label85 = new System.Windows.Forms.Label();
            this.label90 = new System.Windows.Forms.Label();
            this.textBoxStateB = new System.Windows.Forms.TextBox();
            this.textBoxCountyB = new System.Windows.Forms.TextBox();
            this.textBoxZipB = new System.Windows.Forms.TextBox();
            this.label88 = new System.Windows.Forms.Label();
            this.textBoxApnB = new System.Windows.Forms.TextBox();
            this.label89 = new System.Windows.Forms.Label();
            this.buttonSearchB = new System.Windows.Forms.Button();
            this.textBoxStreetA = new System.Windows.Forms.TextBox();
            this.textBoxPreDirA = new System.Windows.Forms.TextBox();
            this.textBoxStreetNoA = new System.Windows.Forms.TextBox();
            this.textBoxStateA = new System.Windows.Forms.TextBox();
            this.textBoxCountyA = new System.Windows.Forms.TextBox();
            this.textBoxZipA = new System.Windows.Forms.TextBox();
            this.textBoxUnitNoA = new System.Windows.Forms.TextBox();
            this.textBoxSuffixA = new System.Windows.Forms.TextBox();
            this.textBoxPostDirA = new System.Windows.Forms.TextBox();
            this.buttonSearchA = new System.Windows.Forms.Button();
            this.label83 = new System.Windows.Forms.Label();
            this.label63 = new System.Windows.Forms.Label();
            this.label79 = new System.Windows.Forms.Label();
            this.label81 = new System.Windows.Forms.Label();
            this.label78 = new System.Windows.Forms.Label();
            this.label82 = new System.Windows.Forms.Label();
            this.label77 = new System.Windows.Forms.Label();
            this.label80 = new System.Windows.Forms.Label();
            this.label73 = new System.Windows.Forms.Label();
            this.label72 = new System.Windows.Forms.Label();
            this.label71 = new System.Windows.Forms.Label();
            this.label70 = new System.Windows.Forms.Label();
            this.label69 = new System.Windows.Forms.Label();
            this.label68 = new System.Windows.Forms.Label();
            this.label65 = new System.Windows.Forms.Label();
            this.label67 = new System.Windows.Forms.Label();
            this.label66 = new System.Windows.Forms.Label();
            this.label64 = new System.Windows.Forms.Label();
            this.label76 = new System.Windows.Forms.Label();
            this.label75 = new System.Windows.Forms.Label();
            this.label74 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.labelSiteAddress = new System.Windows.Forms.Label();
            this.labelCounty = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.tabControlOnlinePropertyProfileNComps = new System.Windows.Forms.TabControl();
            this.tabPageFinancials = new System.Windows.Forms.TabPage();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.textBoxPSMInterestRateType = new System.Windows.Forms.TextBox();
            this.textBoxPSMInterestRateTerm = new System.Windows.Forms.TextBox();
            this.textBoxPSMInterestRate = new System.Windows.Forms.TextBox();
            this.textBoxPSMMortgageAmount = new System.Windows.Forms.TextBox();
            this.label50 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.label53 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.textBoxPFMMortgageAmount = new System.Windows.Forms.TextBox();
            this.textBoxPFMInterestRate = new System.Windows.Forms.TextBox();
            this.textBoxPFMLender = new System.Windows.Forms.TextBox();
            this.textBoxPFMInterestRateTerm = new System.Windows.Forms.TextBox();
            this.textBoxPFMInterestRateType = new System.Windows.Forms.TextBox();
            this.label49 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.textBoxPSSalePrice = new System.Windows.Forms.TextBox();
            this.textBoxPSSaleType1 = new System.Windows.Forms.TextBox();
            this.textBoxPSSaleDate = new System.Windows.Forms.TextBox();
            this.textBoxPSDeedType = new System.Windows.Forms.TextBox();
            this.textBoxPSRecordingDate = new System.Windows.Forms.TextBox();
            this.textBoxPSDocumentNumber = new System.Windows.Forms.TextBox();
            this.textBoxPSStampAmt = new System.Windows.Forms.TextBox();
            this.label42 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.textBoxDocumentNumber = new System.Windows.Forms.TextBox();
            this.labelDocumentNumber = new System.Windows.Forms.Label();
            this.textBoxSecMortgageAmount = new System.Windows.Forms.TextBox();
            this.textBoxSecInterestRate = new System.Windows.Forms.TextBox();
            this.textBoxSecInterestRateTerm = new System.Windows.Forms.TextBox();
            this.textBoxSecInterestRateType = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.textBoxFirstMortgageAmount = new System.Windows.Forms.TextBox();
            this.textBoxFirstMortgageInterestRate = new System.Windows.Forms.TextBox();
            this.textBoxFirstMortgageInterestRateTerm = new System.Windows.Forms.TextBox();
            this.textBoxType = new System.Windows.Forms.TextBox();
            this.textBoxFirstMortgageLender = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.labelFirstMortgageAmount = new System.Windows.Forms.Label();
            this.labelFirstMortgageInterestRate = new System.Windows.Forms.Label();
            this.labelFirstMortgageInterestRateTerm = new System.Windows.Forms.Label();
            this.labelFirstMortgageInterestRateType = new System.Windows.Forms.Label();
            this.labelFirstMortgageLender = new System.Windows.Forms.Label();
            this.textBoxSalePrice = new System.Windows.Forms.TextBox();
            this.textBoxSaleDate = new System.Windows.Forms.TextBox();
            this.textBoxDeedType = new System.Windows.Forms.TextBox();
            this.textBoxRecordingDate = new System.Windows.Forms.TextBox();
            this.textBoxTitleCompany = new System.Windows.Forms.TextBox();
            this.textBoxStampAmt = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.textBoxSellerName = new System.Windows.Forms.TextBox();
            this.textBoxSaleType2 = new System.Windows.Forms.TextBox();
            this.textBoxSaleType1 = new System.Windows.Forms.TextBox();
            this.textBoxCashDown = new System.Windows.Forms.TextBox();
            this.textBox4PricePerSqFt = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.labelSalePrice = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.labelDeedType = new System.Windows.Forms.Label();
            this.labelRecordingDate = new System.Windows.Forms.Label();
            this.labelSellerName = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.labelSaleType1 = new System.Windows.Forms.Label();
            this.labelSaleDate = new System.Windows.Forms.Label();
            this.labelCashDown = new System.Windows.Forms.Label();
            this.labelPricePerSqFt = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.pictureBoxFinancials = new System.Windows.Forms.PictureBox();
            this.tabPageTaxesNLegal = new System.Windows.Forms.TabPage();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.textBoxLegalDescription = new System.Windows.Forms.TextBox();
            this.textBoxCensusTract = new System.Windows.Forms.TextBox();
            this.textBoxTownshipName = new System.Windows.Forms.TextBox();
            this.textBoxLegalBlockBldg = new System.Windows.Forms.TextBox();
            this.textBoxLegalBookPage = new System.Windows.Forms.TextBox();
            this.labelLegalDescription = new System.Windows.Forms.Label();
            this.labelTownshipName = new System.Windows.Forms.Label();
            this.label62 = new System.Windows.Forms.Label();
            this.label60 = new System.Windows.Forms.Label();
            this.label61 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.textBoxLandValue = new System.Windows.Forms.TextBox();
            this.label87 = new System.Windows.Forms.Label();
            this.label86 = new System.Windows.Forms.Label();
            this.textBoxLandUseType = new System.Windows.Forms.TextBox();
            this.textBoxImprovementValue = new System.Windows.Forms.TextBox();
            this.textBoxTaxArea = new System.Windows.Forms.TextBox();
            this.textBoxTaxYear = new System.Windows.Forms.TextBox();
            this.textBoxExemptionType = new System.Windows.Forms.TextBox();
            this.textBoxDelinquentYear = new System.Windows.Forms.TextBox();
            this.textBoxPropertyTax = new System.Windows.Forms.TextBox();
            this.textBoxAPN = new System.Windows.Forms.TextBox();
            this.textBoxAlternateAPN = new System.Windows.Forms.TextBox();
            this.textBoxTotalAssesedValue = new System.Windows.Forms.TextBox();
            this.textBoxAssesmentYear = new System.Windows.Forms.TextBox();
            this.label59 = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this.labelAPN = new System.Windows.Forms.Label();
            this.labelAlternateAPN = new System.Windows.Forms.Label();
            this.labelTotalAssesedValue = new System.Windows.Forms.Label();
            this.labelAssesmentYear = new System.Windows.Forms.Label();
            this.labelPropertyTax = new System.Windows.Forms.Label();
            this.labelTaxArea = new System.Windows.Forms.Label();
            this.labelTaxYear = new System.Windows.Forms.Label();
            this.labelExemptionType = new System.Windows.Forms.Label();
            this.labelLandUseType = new System.Windows.Forms.Label();
            this.labelLandValue = new System.Windows.Forms.Label();
            this.labelImprovementValue = new System.Windows.Forms.Label();
            this.labelDelinquentYear = new System.Windows.Forms.Label();
            this.pictureBoxTaxesLegal = new System.Windows.Forms.PictureBox();
            this.line1 = new DealMaker.UserControls.Line();
            this.tabPagePropertyCharacteristics.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPropCharact)).BeginInit();
            this.tabPageOwnershipNLocations.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.tabControlOnlinePropertyProfileNComps.SuspendLayout();
            this.tabPageFinancials.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxFinancials)).BeginInit();
            this.tabPageTaxesNLegal.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxTaxesLegal)).BeginInit();
            this.SuspendLayout();
            // 
            // textBoxMailingZipCode
            // 
            this.textBoxMailingZipCode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxMailingZipCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxMailingZipCode.Location = new System.Drawing.Point(824, 52);
            this.textBoxMailingZipCode.Name = "textBoxMailingZipCode";
            this.textBoxMailingZipCode.Size = new System.Drawing.Size(60, 39);
            this.textBoxMailingZipCode.TabIndex = 95;
            // 
            // labelMailingZipCode
            // 
            this.labelMailingZipCode.AutoSize = true;
            this.labelMailingZipCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMailingZipCode.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.labelMailingZipCode.Location = new System.Drawing.Point(821, 36);
            this.labelMailingZipCode.Name = "labelMailingZipCode";
            this.labelMailingZipCode.Size = new System.Drawing.Size(58, 32);
            this.labelMailingZipCode.TabIndex = 43;
            this.labelMailingZipCode.Text = "ZIP";
            // 
            // textBoxMailingCityAndState
            // 
            this.textBoxMailingCityAndState.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxMailingCityAndState.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxMailingCityAndState.Location = new System.Drawing.Point(622, 52);
            this.textBoxMailingCityAndState.Name = "textBoxMailingCityAndState";
            this.textBoxMailingCityAndState.Size = new System.Drawing.Size(196, 39);
            this.textBoxMailingCityAndState.TabIndex = 94;
            // 
            // labelMailingCityAndState
            // 
            this.labelMailingCityAndState.AutoSize = true;
            this.labelMailingCityAndState.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMailingCityAndState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.labelMailingCityAndState.Location = new System.Drawing.Point(619, 36);
            this.labelMailingCityAndState.Name = "labelMailingCityAndState";
            this.labelMailingCityAndState.Size = new System.Drawing.Size(292, 32);
            this.labelMailingCityAndState.TabIndex = 41;
            this.labelMailingCityAndState.Text = "Mailing City and State";
            // 
            // textBoxMailingAddress
            // 
            this.textBoxMailingAddress.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxMailingAddress.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxMailingAddress.Location = new System.Drawing.Point(408, 52);
            this.textBoxMailingAddress.Name = "textBoxMailingAddress";
            this.textBoxMailingAddress.Size = new System.Drawing.Size(208, 39);
            this.textBoxMailingAddress.TabIndex = 93;
            // 
            // labelMailingAddress
            // 
            this.labelMailingAddress.AutoSize = true;
            this.labelMailingAddress.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMailingAddress.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.labelMailingAddress.Location = new System.Drawing.Point(405, 36);
            this.labelMailingAddress.Name = "labelMailingAddress";
            this.labelMailingAddress.Size = new System.Drawing.Size(218, 32);
            this.labelMailingAddress.TabIndex = 39;
            this.labelMailingAddress.Text = "Mailing Address";
            // 
            // textBoxMapReferenceTwo
            // 
            this.textBoxMapReferenceTwo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxMapReferenceTwo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxMapReferenceTwo.Location = new System.Drawing.Point(283, 155);
            this.textBoxMapReferenceTwo.Name = "textBoxMapReferenceTwo";
            this.textBoxMapReferenceTwo.Size = new System.Drawing.Size(141, 39);
            this.textBoxMapReferenceTwo.TabIndex = 140;
            // 
            // labelMapReferenceTwo
            // 
            this.labelMapReferenceTwo.AutoSize = true;
            this.labelMapReferenceTwo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMapReferenceTwo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.labelMapReferenceTwo.Location = new System.Drawing.Point(280, 139);
            this.labelMapReferenceTwo.Name = "labelMapReferenceTwo";
            this.labelMapReferenceTwo.Size = new System.Drawing.Size(268, 32);
            this.labelMapReferenceTwo.TabIndex = 37;
            this.labelMapReferenceTwo.Text = "Map Reference Two";
            // 
            // textBoxMapReferenceOne
            // 
            this.textBoxMapReferenceOne.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxMapReferenceOne.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxMapReferenceOne.Location = new System.Drawing.Point(136, 155);
            this.textBoxMapReferenceOne.Name = "textBoxMapReferenceOne";
            this.textBoxMapReferenceOne.Size = new System.Drawing.Size(141, 39);
            this.textBoxMapReferenceOne.TabIndex = 130;
            // 
            // labelMapReferenceOne
            // 
            this.labelMapReferenceOne.AutoSize = true;
            this.labelMapReferenceOne.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMapReferenceOne.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.labelMapReferenceOne.Location = new System.Drawing.Point(133, 139);
            this.labelMapReferenceOne.Name = "labelMapReferenceOne";
            this.labelMapReferenceOne.Size = new System.Drawing.Size(269, 32);
            this.labelMapReferenceOne.TabIndex = 35;
            this.labelMapReferenceOne.Text = "Map Reference One";
            // 
            // textBoxZipCode
            // 
            this.textBoxZipCode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxZipCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxZipCode.Location = new System.Drawing.Point(562, 115);
            this.textBoxZipCode.Name = "textBoxZipCode";
            this.textBoxZipCode.Size = new System.Drawing.Size(66, 39);
            this.textBoxZipCode.TabIndex = 99;
            // 
            // labelZipCode
            // 
            this.labelZipCode.AutoSize = true;
            this.labelZipCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelZipCode.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.labelZipCode.Location = new System.Drawing.Point(559, 99);
            this.labelZipCode.Name = "labelZipCode";
            this.labelZipCode.Size = new System.Drawing.Size(58, 32);
            this.labelZipCode.TabIndex = 33;
            this.labelZipCode.Text = "ZIP";
            // 
            // textBoxState
            // 
            this.textBoxState.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxState.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxState.Location = new System.Drawing.Point(526, 115);
            this.textBoxState.MaxLength = 2;
            this.textBoxState.Name = "textBoxState";
            this.textBoxState.Size = new System.Drawing.Size(30, 39);
            this.textBoxState.TabIndex = 98;
            // 
            // labelState
            // 
            this.labelState.AutoSize = true;
            this.labelState.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.labelState.Location = new System.Drawing.Point(523, 99);
            this.labelState.Name = "labelState";
            this.labelState.Size = new System.Drawing.Size(82, 32);
            this.labelState.TabIndex = 31;
            this.labelState.Text = "State";
            // 
            // textBoxCity
            // 
            this.textBoxCity.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxCity.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxCity.Location = new System.Drawing.Point(368, 115);
            this.textBoxCity.Name = "textBoxCity";
            this.textBoxCity.Size = new System.Drawing.Size(152, 39);
            this.textBoxCity.TabIndex = 97;
            // 
            // labelCity
            // 
            this.labelCity.AutoSize = true;
            this.labelCity.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCity.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.labelCity.Location = new System.Drawing.Point(365, 99);
            this.labelCity.Name = "labelCity";
            this.labelCity.Size = new System.Drawing.Size(64, 32);
            this.labelCity.TabIndex = 29;
            this.labelCity.Text = "City";
            // 
            // textBoxUnitNumber
            // 
            this.textBoxUnitNumber.Location = new System.Drawing.Point(1224, 480);
            this.textBoxUnitNumber.Name = "textBoxUnitNumber";
            this.textBoxUnitNumber.Size = new System.Drawing.Size(100, 38);
            this.textBoxUnitNumber.TabIndex = 28;
            this.textBoxUnitNumber.Text = "Unit-Number";
            this.textBoxUnitNumber.Visible = false;
            // 
            // labelUnitNumber
            // 
            this.labelUnitNumber.Location = new System.Drawing.Point(1216, 464);
            this.labelUnitNumber.Name = "labelUnitNumber";
            this.labelUnitNumber.Size = new System.Drawing.Size(72, 24);
            this.labelUnitNumber.TabIndex = 27;
            this.labelUnitNumber.Text = "Unit-Number";
            this.labelUnitNumber.Visible = false;
            // 
            // textBoxSuffix
            // 
            this.textBoxSuffix.Location = new System.Drawing.Point(1104, 480);
            this.textBoxSuffix.Name = "textBoxSuffix";
            this.textBoxSuffix.Size = new System.Drawing.Size(100, 38);
            this.textBoxSuffix.TabIndex = 26;
            this.textBoxSuffix.Text = "Suffix";
            this.textBoxSuffix.Visible = false;
            // 
            // labelSuffix
            // 
            this.labelSuffix.Location = new System.Drawing.Point(1096, 464);
            this.labelSuffix.Name = "labelSuffix";
            this.labelSuffix.Size = new System.Drawing.Size(48, 24);
            this.labelSuffix.TabIndex = 25;
            this.labelSuffix.Text = "Suffix";
            this.labelSuffix.Visible = false;
            // 
            // textBoxPostDirection
            // 
            this.textBoxPostDirection.Location = new System.Drawing.Point(363, 483);
            this.textBoxPostDirection.Name = "textBoxPostDirection";
            this.textBoxPostDirection.Size = new System.Drawing.Size(100, 38);
            this.textBoxPostDirection.TabIndex = 24;
            this.textBoxPostDirection.Text = "Post-Direction";
            this.textBoxPostDirection.Visible = false;
            // 
            // labelPostDirection
            // 
            this.labelPostDirection.Location = new System.Drawing.Point(355, 467);
            this.labelPostDirection.Name = "labelPostDirection";
            this.labelPostDirection.Size = new System.Drawing.Size(80, 24);
            this.labelPostDirection.TabIndex = 23;
            this.labelPostDirection.Text = "Post-Direction";
            this.labelPostDirection.Visible = false;
            // 
            // textBoxStreetnName
            // 
            this.textBoxStreetnName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxStreetnName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxStreetnName.Location = new System.Drawing.Point(30, 115);
            this.textBoxStreetnName.Name = "textBoxStreetnName";
            this.textBoxStreetnName.Size = new System.Drawing.Size(332, 39);
            this.textBoxStreetnName.TabIndex = 96;
            // 
            // labelStreetnName
            // 
            this.labelStreetnName.AutoSize = true;
            this.labelStreetnName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelStreetnName.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.labelStreetnName.Location = new System.Drawing.Point(28, 99);
            this.labelStreetnName.Name = "labelStreetnName";
            this.labelStreetnName.Size = new System.Drawing.Size(202, 32);
            this.labelStreetnName.TabIndex = 21;
            this.labelStreetnName.Text = "Street Address";
            // 
            // textBoxPreDirection
            // 
            this.textBoxPreDirection.Location = new System.Drawing.Point(467, 444);
            this.textBoxPreDirection.Name = "textBoxPreDirection";
            this.textBoxPreDirection.Size = new System.Drawing.Size(100, 38);
            this.textBoxPreDirection.TabIndex = 20;
            this.textBoxPreDirection.Text = "Pre-Direction";
            this.textBoxPreDirection.Visible = false;
            // 
            // labelPreDirection
            // 
            this.labelPreDirection.Location = new System.Drawing.Point(465, 428);
            this.labelPreDirection.Name = "labelPreDirection";
            this.labelPreDirection.Size = new System.Drawing.Size(104, 24);
            this.labelPreDirection.TabIndex = 19;
            this.labelPreDirection.Text = "Pre-Direction";
            this.labelPreDirection.Visible = false;
            // 
            // textBoxHouseNumber
            // 
            this.textBoxHouseNumber.Location = new System.Drawing.Point(355, 444);
            this.textBoxHouseNumber.Name = "textBoxHouseNumber";
            this.textBoxHouseNumber.Size = new System.Drawing.Size(100, 38);
            this.textBoxHouseNumber.TabIndex = 18;
            this.textBoxHouseNumber.Text = "House Number";
            this.textBoxHouseNumber.Visible = false;
            // 
            // labelHouseNumber
            // 
            this.labelHouseNumber.Location = new System.Drawing.Point(353, 428);
            this.labelHouseNumber.Name = "labelHouseNumber";
            this.labelHouseNumber.Size = new System.Drawing.Size(104, 24);
            this.labelHouseNumber.TabIndex = 17;
            this.labelHouseNumber.Text = "House Number";
            this.labelHouseNumber.Visible = false;
            // 
            // textBoxPhoneNumber
            // 
            this.textBoxPhoneNumber.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxPhoneNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxPhoneNumber.Location = new System.Drawing.Point(302, 52);
            this.textBoxPhoneNumber.Name = "textBoxPhoneNumber";
            this.textBoxPhoneNumber.Size = new System.Drawing.Size(100, 39);
            this.textBoxPhoneNumber.TabIndex = 92;
            // 
            // labelPhoneNumber
            // 
            this.labelPhoneNumber.AutoSize = true;
            this.labelPhoneNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPhoneNumber.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.labelPhoneNumber.Location = new System.Drawing.Point(299, 36);
            this.labelPhoneNumber.Name = "labelPhoneNumber";
            this.labelPhoneNumber.Size = new System.Drawing.Size(205, 32);
            this.labelPhoneNumber.TabIndex = 15;
            this.labelPhoneNumber.Text = "Phone Number";
            // 
            // textBoxOwnerName
            // 
            this.textBoxOwnerName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxOwnerName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxOwnerName.Location = new System.Drawing.Point(31, 52);
            this.textBoxOwnerName.Name = "textBoxOwnerName";
            this.textBoxOwnerName.Size = new System.Drawing.Size(265, 39);
            this.textBoxOwnerName.TabIndex = 91;
            // 
            // labelOwnerName
            // 
            this.labelOwnerName.AutoSize = true;
            this.labelOwnerName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelOwnerName.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.labelOwnerName.Location = new System.Drawing.Point(28, 36);
            this.labelOwnerName.Name = "labelOwnerName";
            this.labelOwnerName.Size = new System.Drawing.Size(180, 32);
            this.labelOwnerName.TabIndex = 13;
            this.labelOwnerName.Text = "Owner Name";
            // 
            // labelOwnerNameGroup
            // 
            this.labelOwnerNameGroup.AutoSize = true;
            this.labelOwnerNameGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelOwnerNameGroup.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.labelOwnerNameGroup.Location = new System.Drawing.Point(6, 18);
            this.labelOwnerNameGroup.Name = "labelOwnerNameGroup";
            this.labelOwnerNameGroup.Size = new System.Drawing.Size(431, 32);
            this.labelOwnerNameGroup.TabIndex = 12;
            this.labelOwnerNameGroup.Text = "OWNERSHIP && MAILING INFO";
            // 
            // textBoxSchoolDistrict
            // 
            this.textBoxSchoolDistrict.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxSchoolDistrict.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxSchoolDistrict.Location = new System.Drawing.Point(430, 155);
            this.textBoxSchoolDistrict.Name = "textBoxSchoolDistrict";
            this.textBoxSchoolDistrict.Size = new System.Drawing.Size(120, 39);
            this.textBoxSchoolDistrict.TabIndex = 150;
            // 
            // textBoxLotNumber
            // 
            this.textBoxLotNumber.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxLotNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxLotNumber.Location = new System.Drawing.Point(30, 155);
            this.textBoxLotNumber.Name = "textBoxLotNumber";
            this.textBoxLotNumber.Size = new System.Drawing.Size(100, 39);
            this.textBoxLotNumber.TabIndex = 120;
            // 
            // textBoxSubdivision
            // 
            this.textBoxSubdivision.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxSubdivision.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxSubdivision.Location = new System.Drawing.Point(764, 115);
            this.textBoxSubdivision.Name = "textBoxSubdivision";
            this.textBoxSubdivision.Size = new System.Drawing.Size(118, 39);
            this.textBoxSubdivision.TabIndex = 110;
            // 
            // textBoxComparableNumber
            // 
            this.textBoxComparableNumber.Location = new System.Drawing.Point(160, 24);
            this.textBoxComparableNumber.Name = "textBoxComparableNumber";
            this.textBoxComparableNumber.Size = new System.Drawing.Size(112, 38);
            this.textBoxComparableNumber.TabIndex = 8;
            // 
            // textBoxDistanceFromSubject
            // 
            this.textBoxDistanceFromSubject.Location = new System.Drawing.Point(16, 24);
            this.textBoxDistanceFromSubject.Name = "textBoxDistanceFromSubject";
            this.textBoxDistanceFromSubject.Size = new System.Drawing.Size(128, 38);
            this.textBoxDistanceFromSubject.TabIndex = 7;
            // 
            // tabPagePropertyCharacteristics
            // 
            this.tabPagePropertyCharacteristics.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.tabPagePropertyCharacteristics.Controls.Add(this._updateSqFt);
            this.tabPagePropertyCharacteristics.Controls.Add(this.textBoxOtherImprovements);
            this.tabPagePropertyCharacteristics.Controls.Add(this.textBoxAirConditioning);
            this.tabPagePropertyCharacteristics.Controls.Add(this.textBoxBasementArea);
            this.tabPagePropertyCharacteristics.Controls.Add(this.textBoxBasement);
            this.tabPagePropertyCharacteristics.Controls.Add(this.textBoxCondition);
            this.tabPagePropertyCharacteristics.Controls.Add(this.textBoxQuality);
            this.tabPagePropertyCharacteristics.Controls.Add(this.textBoxFoundation);
            this.tabPagePropertyCharacteristics.Controls.Add(this.textBoxRoofMaterials);
            this.tabPagePropertyCharacteristics.Controls.Add(this.textBoxFireplaces);
            this.tabPagePropertyCharacteristics.Controls.Add(this.textBoxGarageArea);
            this.tabPagePropertyCharacteristics.Controls.Add(this.textBoxLotWidth);
            this.tabPagePropertyCharacteristics.Controls.Add(this.textBoxLotDepth);
            this.tabPagePropertyCharacteristics.Controls.Add(this.textBoxLandUse);
            this.tabPagePropertyCharacteristics.Controls.Add(this.textBoxTotalBaths);
            this.tabPagePropertyCharacteristics.Controls.Add(this.textBoxTotalBedrooms);
            this.tabPagePropertyCharacteristics.Controls.Add(this.textBoxTotalRooms);
            this.tabPagePropertyCharacteristics.Controls.Add(this.textBoxZoning);
            this.tabPagePropertyCharacteristics.Controls.Add(this.textBoxNumberOfStories);
            this.tabPagePropertyCharacteristics.Controls.Add(this.textBoxPool);
            this.tabPagePropertyCharacteristics.Controls.Add(this.textBoxRoofType);
            this.tabPagePropertyCharacteristics.Controls.Add(this.textBoxParkingCapacity);
            this.tabPagePropertyCharacteristics.Controls.Add(this.textBoxParkingType);
            this.tabPagePropertyCharacteristics.Controls.Add(this.textBoxLotSize);
            this.tabPagePropertyCharacteristics.Controls.Add(this.textBoxLotArea);
            this.tabPagePropertyCharacteristics.Controls.Add(this.textBoxTotalLivingArea);
            this.tabPagePropertyCharacteristics.Controls.Add(this.textBoxYearBuilt);
            this.tabPagePropertyCharacteristics.Controls.Add(this.label57);
            this.tabPagePropertyCharacteristics.Controls.Add(this.labelAirConditioning);
            this.tabPagePropertyCharacteristics.Controls.Add(this.pictureBoxPropCharact);
            this.tabPagePropertyCharacteristics.Controls.Add(this.label9);
            this.tabPagePropertyCharacteristics.Controls.Add(this.label10);
            this.tabPagePropertyCharacteristics.Controls.Add(this.label8);
            this.tabPagePropertyCharacteristics.Controls.Add(this.label7);
            this.tabPagePropertyCharacteristics.Controls.Add(this.label6);
            this.tabPagePropertyCharacteristics.Controls.Add(this.label5);
            this.tabPagePropertyCharacteristics.Controls.Add(this.label4);
            this.tabPagePropertyCharacteristics.Controls.Add(this.label3);
            this.tabPagePropertyCharacteristics.Controls.Add(this.label1);
            this.tabPagePropertyCharacteristics.Controls.Add(this.label2);
            this.tabPagePropertyCharacteristics.Controls.Add(this.labelLandUse);
            this.tabPagePropertyCharacteristics.Controls.Add(this.labelTotalBaths);
            this.tabPagePropertyCharacteristics.Controls.Add(this.labelTotalBedrooms);
            this.tabPagePropertyCharacteristics.Controls.Add(this.labelTotalRooms);
            this.tabPagePropertyCharacteristics.Controls.Add(this.labelZoning);
            this.tabPagePropertyCharacteristics.Controls.Add(this.labelNumberOfStories);
            this.tabPagePropertyCharacteristics.Controls.Add(this.labelPool);
            this.tabPagePropertyCharacteristics.Controls.Add(this.labelRoofType);
            this.tabPagePropertyCharacteristics.Controls.Add(this.labelParkingCapacity);
            this.tabPagePropertyCharacteristics.Controls.Add(this.labelParkingType);
            this.tabPagePropertyCharacteristics.Controls.Add(this.labelLotSize);
            this.tabPagePropertyCharacteristics.Controls.Add(this.labelLotArea);
            this.tabPagePropertyCharacteristics.Controls.Add(this.labelTotalLivingArea);
            this.tabPagePropertyCharacteristics.Controls.Add(this.labelYearBuilt);
            this.tabPagePropertyCharacteristics.Controls.Add(this.label11);
            this.tabPagePropertyCharacteristics.Controls.Add(this.label13);
            this.tabPagePropertyCharacteristics.Controls.Add(this.label14);
            this.tabPagePropertyCharacteristics.Controls.Add(this.label12);
            this.tabPagePropertyCharacteristics.Location = new System.Drawing.Point(10, 28);
            this.tabPagePropertyCharacteristics.Name = "tabPagePropertyCharacteristics";
            this.tabPagePropertyCharacteristics.Size = new System.Drawing.Size(975, 516);
            this.tabPagePropertyCharacteristics.TabIndex = 1;
            this.tabPagePropertyCharacteristics.Text = "Property Characteristics";
            // 
            // _updateSqFt
            // 
            this._updateSqFt.Enabled = false;
            this._updateSqFt.Location = new System.Drawing.Point(146, 165);
            this._updateSqFt.Name = "_updateSqFt";
            this._updateSqFt.Size = new System.Drawing.Size(106, 23);
            this._updateSqFt.TabIndex = 8;
            this._updateSqFt.Text = "Update SqFt";
            this._updateSqFt.UseVisualStyleBackColor = true;
            this._updateSqFt.Click += new System.EventHandler(this._updateSqFt_Click);
            // 
            // textBoxOtherImprovements
            // 
            this.textBoxOtherImprovements.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxOtherImprovements.Location = new System.Drawing.Point(458, 333);
            this.textBoxOtherImprovements.Name = "textBoxOtherImprovements";
            this.textBoxOtherImprovements.Size = new System.Drawing.Size(342, 38);
            this.textBoxOtherImprovements.TabIndex = 27;
            // 
            // textBoxAirConditioning
            // 
            this.textBoxAirConditioning.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxAirConditioning.Location = new System.Drawing.Point(362, 333);
            this.textBoxAirConditioning.Name = "textBoxAirConditioning";
            this.textBoxAirConditioning.Size = new System.Drawing.Size(90, 38);
            this.textBoxAirConditioning.TabIndex = 26;
            // 
            // textBoxBasementArea
            // 
            this.textBoxBasementArea.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxBasementArea.Location = new System.Drawing.Point(258, 333);
            this.textBoxBasementArea.Name = "textBoxBasementArea";
            this.textBoxBasementArea.Size = new System.Drawing.Size(98, 38);
            this.textBoxBasementArea.TabIndex = 25;
            // 
            // textBoxBasement
            // 
            this.textBoxBasement.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxBasement.Location = new System.Drawing.Point(146, 333);
            this.textBoxBasement.Name = "textBoxBasement";
            this.textBoxBasement.Size = new System.Drawing.Size(106, 38);
            this.textBoxBasement.TabIndex = 24;
            // 
            // textBoxCondition
            // 
            this.textBoxCondition.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxCondition.Location = new System.Drawing.Point(458, 293);
            this.textBoxCondition.Name = "textBoxCondition";
            this.textBoxCondition.Size = new System.Drawing.Size(342, 38);
            this.textBoxCondition.TabIndex = 22;
            // 
            // textBoxQuality
            // 
            this.textBoxQuality.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxQuality.Location = new System.Drawing.Point(362, 293);
            this.textBoxQuality.Name = "textBoxQuality";
            this.textBoxQuality.Size = new System.Drawing.Size(90, 38);
            this.textBoxQuality.TabIndex = 21;
            // 
            // textBoxFoundation
            // 
            this.textBoxFoundation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxFoundation.Location = new System.Drawing.Point(258, 293);
            this.textBoxFoundation.Name = "textBoxFoundation";
            this.textBoxFoundation.Size = new System.Drawing.Size(98, 38);
            this.textBoxFoundation.TabIndex = 20;
            // 
            // textBoxRoofMaterials
            // 
            this.textBoxRoofMaterials.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxRoofMaterials.Location = new System.Drawing.Point(146, 293);
            this.textBoxRoofMaterials.Name = "textBoxRoofMaterials";
            this.textBoxRoofMaterials.Size = new System.Drawing.Size(106, 38);
            this.textBoxRoofMaterials.TabIndex = 19;
            // 
            // textBoxFireplaces
            // 
            this.textBoxFireplaces.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxFireplaces.Location = new System.Drawing.Point(458, 229);
            this.textBoxFireplaces.Name = "textBoxFireplaces";
            this.textBoxFireplaces.Size = new System.Drawing.Size(90, 38);
            this.textBoxFireplaces.TabIndex = 17;
            // 
            // textBoxGarageArea
            // 
            this.textBoxGarageArea.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxGarageArea.Location = new System.Drawing.Point(258, 229);
            this.textBoxGarageArea.Name = "textBoxGarageArea";
            this.textBoxGarageArea.Size = new System.Drawing.Size(98, 38);
            this.textBoxGarageArea.TabIndex = 15;
            // 
            // textBoxLotWidth
            // 
            this.textBoxLotWidth.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxLotWidth.Location = new System.Drawing.Point(570, 165);
            this.textBoxLotWidth.Name = "textBoxLotWidth";
            this.textBoxLotWidth.Size = new System.Drawing.Size(90, 38);
            this.textBoxLotWidth.TabIndex = 12;
            // 
            // textBoxLotDepth
            // 
            this.textBoxLotDepth.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxLotDepth.Location = new System.Drawing.Point(474, 165);
            this.textBoxLotDepth.Name = "textBoxLotDepth";
            this.textBoxLotDepth.Size = new System.Drawing.Size(90, 38);
            this.textBoxLotDepth.TabIndex = 11;
            // 
            // textBoxLandUse
            // 
            this.textBoxLandUse.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxLandUse.Location = new System.Drawing.Point(26, 24);
            this.textBoxLandUse.Name = "textBoxLandUse";
            this.textBoxLandUse.Size = new System.Drawing.Size(114, 38);
            this.textBoxLandUse.TabIndex = 1;
            // 
            // textBoxTotalBaths
            // 
            this.textBoxTotalBaths.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxTotalBaths.Location = new System.Drawing.Point(258, 101);
            this.textBoxTotalBaths.Name = "textBoxTotalBaths";
            this.textBoxTotalBaths.Size = new System.Drawing.Size(98, 38);
            this.textBoxTotalBaths.TabIndex = 6;
            // 
            // textBoxTotalBedrooms
            // 
            this.textBoxTotalBedrooms.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxTotalBedrooms.Location = new System.Drawing.Point(146, 101);
            this.textBoxTotalBedrooms.Name = "textBoxTotalBedrooms";
            this.textBoxTotalBedrooms.Size = new System.Drawing.Size(106, 38);
            this.textBoxTotalBedrooms.TabIndex = 5;
            // 
            // textBoxTotalRooms
            // 
            this.textBoxTotalRooms.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxTotalRooms.Location = new System.Drawing.Point(26, 101);
            this.textBoxTotalRooms.Name = "textBoxTotalRooms";
            this.textBoxTotalRooms.Size = new System.Drawing.Size(114, 38);
            this.textBoxTotalRooms.TabIndex = 4;
            // 
            // textBoxZoning
            // 
            this.textBoxZoning.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxZoning.Location = new System.Drawing.Point(146, 24);
            this.textBoxZoning.Name = "textBoxZoning";
            this.textBoxZoning.Size = new System.Drawing.Size(106, 38);
            this.textBoxZoning.TabIndex = 2;
            // 
            // textBoxNumberOfStories
            // 
            this.textBoxNumberOfStories.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxNumberOfStories.Location = new System.Drawing.Point(26, 333);
            this.textBoxNumberOfStories.Name = "textBoxNumberOfStories";
            this.textBoxNumberOfStories.Size = new System.Drawing.Size(114, 38);
            this.textBoxNumberOfStories.TabIndex = 23;
            // 
            // textBoxPool
            // 
            this.textBoxPool.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxPool.Location = new System.Drawing.Point(362, 229);
            this.textBoxPool.Name = "textBoxPool";
            this.textBoxPool.Size = new System.Drawing.Size(90, 38);
            this.textBoxPool.TabIndex = 16;
            // 
            // textBoxRoofType
            // 
            this.textBoxRoofType.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxRoofType.Location = new System.Drawing.Point(26, 293);
            this.textBoxRoofType.Name = "textBoxRoofType";
            this.textBoxRoofType.Size = new System.Drawing.Size(114, 38);
            this.textBoxRoofType.TabIndex = 18;
            // 
            // textBoxParkingCapacity
            // 
            this.textBoxParkingCapacity.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxParkingCapacity.Location = new System.Drawing.Point(146, 229);
            this.textBoxParkingCapacity.Name = "textBoxParkingCapacity";
            this.textBoxParkingCapacity.Size = new System.Drawing.Size(106, 38);
            this.textBoxParkingCapacity.TabIndex = 14;
            // 
            // textBoxParkingType
            // 
            this.textBoxParkingType.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxParkingType.Location = new System.Drawing.Point(26, 229);
            this.textBoxParkingType.Name = "textBoxParkingType";
            this.textBoxParkingType.Size = new System.Drawing.Size(114, 38);
            this.textBoxParkingType.TabIndex = 13;
            // 
            // textBoxLotSize
            // 
            this.textBoxLotSize.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxLotSize.Location = new System.Drawing.Point(370, 165);
            this.textBoxLotSize.Name = "textBoxLotSize";
            this.textBoxLotSize.Size = new System.Drawing.Size(98, 38);
            this.textBoxLotSize.TabIndex = 10;
            // 
            // textBoxLotArea
            // 
            this.textBoxLotArea.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxLotArea.Location = new System.Drawing.Point(258, 165);
            this.textBoxLotArea.Name = "textBoxLotArea";
            this.textBoxLotArea.Size = new System.Drawing.Size(106, 38);
            this.textBoxLotArea.TabIndex = 9;
            // 
            // textBoxTotalLivingArea
            // 
            this.textBoxTotalLivingArea.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxTotalLivingArea.Location = new System.Drawing.Point(26, 165);
            this.textBoxTotalLivingArea.Name = "textBoxTotalLivingArea";
            this.textBoxTotalLivingArea.Size = new System.Drawing.Size(114, 38);
            this.textBoxTotalLivingArea.TabIndex = 7;
            // 
            // textBoxYearBuilt
            // 
            this.textBoxYearBuilt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxYearBuilt.Location = new System.Drawing.Point(258, 24);
            this.textBoxYearBuilt.Name = "textBoxYearBuilt";
            this.textBoxYearBuilt.Size = new System.Drawing.Size(48, 38);
            this.textBoxYearBuilt.TabIndex = 3;
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label57.Location = new System.Drawing.Point(455, 317);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(271, 32);
            this.label57.TabIndex = 115;
            this.label57.Text = "Other Improvements";
            // 
            // labelAirConditioning
            // 
            this.labelAirConditioning.AutoSize = true;
            this.labelAirConditioning.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.labelAirConditioning.Location = new System.Drawing.Point(359, 317);
            this.labelAirConditioning.Name = "labelAirConditioning";
            this.labelAirConditioning.Size = new System.Drawing.Size(218, 32);
            this.labelAirConditioning.TabIndex = 57;
            this.labelAirConditioning.Text = "Air Conditioning";
            // 
            // pictureBoxPropCharact
            // 
            this.pictureBoxPropCharact.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBoxPropCharact.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxPropCharact.Image")));
            this.pictureBoxPropCharact.Location = new System.Drawing.Point(951, 8);
            this.pictureBoxPropCharact.Name = "pictureBoxPropCharact";
            this.pictureBoxPropCharact.Size = new System.Drawing.Size(16, 16);
            this.pictureBoxPropCharact.TabIndex = 117;
            this.pictureBoxPropCharact.TabStop = false;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label9.Location = new System.Drawing.Point(255, 317);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(210, 32);
            this.label9.TabIndex = 109;
            this.label9.Text = "Basement Area";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label10.Location = new System.Drawing.Point(143, 317);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(143, 32);
            this.label10.TabIndex = 107;
            this.label10.Text = "Basement";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label8.Location = new System.Drawing.Point(455, 277);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(137, 32);
            this.label8.TabIndex = 105;
            this.label8.Text = "Condition";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label7.Location = new System.Drawing.Point(359, 277);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(105, 32);
            this.label7.TabIndex = 103;
            this.label7.Text = "Quality";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label6.Location = new System.Drawing.Point(255, 277);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(159, 32);
            this.label6.TabIndex = 101;
            this.label6.Text = "Foundation";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label5.Location = new System.Drawing.Point(143, 277);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(198, 32);
            this.label5.TabIndex = 99;
            this.label5.Text = "Roof Materials";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label4.Location = new System.Drawing.Point(455, 213);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(147, 32);
            this.label4.TabIndex = 97;
            this.label4.Text = "Fireplaces";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label3.Location = new System.Drawing.Point(255, 213);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(177, 32);
            this.label3.TabIndex = 95;
            this.label3.Text = "Garage Area";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label1.Location = new System.Drawing.Point(567, 149);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(135, 32);
            this.label1.TabIndex = 93;
            this.label1.Text = "Lot Width";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label2.Location = new System.Drawing.Point(471, 149);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(138, 32);
            this.label2.TabIndex = 91;
            this.label2.Text = "Lot Depth";
            // 
            // labelLandUse
            // 
            this.labelLandUse.AutoSize = true;
            this.labelLandUse.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelLandUse.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.labelLandUse.Location = new System.Drawing.Point(23, 8);
            this.labelLandUse.Name = "labelLandUse";
            this.labelLandUse.Size = new System.Drawing.Size(163, 32);
            this.labelLandUse.TabIndex = 89;
            this.labelLandUse.Text = "LAND USE";
            // 
            // labelTotalBaths
            // 
            this.labelTotalBaths.AutoSize = true;
            this.labelTotalBaths.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.labelTotalBaths.Location = new System.Drawing.Point(255, 85);
            this.labelTotalBaths.Name = "labelTotalBaths";
            this.labelTotalBaths.Size = new System.Drawing.Size(156, 32);
            this.labelTotalBaths.TabIndex = 69;
            this.labelTotalBaths.Text = "Total baths";
            // 
            // labelTotalBedrooms
            // 
            this.labelTotalBedrooms.AutoSize = true;
            this.labelTotalBedrooms.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.labelTotalBedrooms.Location = new System.Drawing.Point(143, 85);
            this.labelTotalBedrooms.Name = "labelTotalBedrooms";
            this.labelTotalBedrooms.Size = new System.Drawing.Size(215, 32);
            this.labelTotalBedrooms.TabIndex = 67;
            this.labelTotalBedrooms.Text = "Total Bedrooms";
            // 
            // labelTotalRooms
            // 
            this.labelTotalRooms.AutoSize = true;
            this.labelTotalRooms.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.labelTotalRooms.Location = new System.Drawing.Point(23, 85);
            this.labelTotalRooms.Name = "labelTotalRooms";
            this.labelTotalRooms.Size = new System.Drawing.Size(175, 32);
            this.labelTotalRooms.TabIndex = 65;
            this.labelTotalRooms.Text = "Total Rooms";
            // 
            // labelZoning
            // 
            this.labelZoning.AutoSize = true;
            this.labelZoning.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelZoning.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.labelZoning.Location = new System.Drawing.Point(143, 8);
            this.labelZoning.Name = "labelZoning";
            this.labelZoning.Size = new System.Drawing.Size(129, 32);
            this.labelZoning.TabIndex = 63;
            this.labelZoning.Text = "ZONING";
            // 
            // labelNumberOfStories
            // 
            this.labelNumberOfStories.AutoSize = true;
            this.labelNumberOfStories.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.labelNumberOfStories.Location = new System.Drawing.Point(23, 317);
            this.labelNumberOfStories.Name = "labelNumberOfStories";
            this.labelNumberOfStories.Size = new System.Drawing.Size(242, 32);
            this.labelNumberOfStories.TabIndex = 61;
            this.labelNumberOfStories.Text = "Number of Stories";
            // 
            // labelPool
            // 
            this.labelPool.AutoSize = true;
            this.labelPool.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.labelPool.Location = new System.Drawing.Point(359, 213);
            this.labelPool.Name = "labelPool";
            this.labelPool.Size = new System.Drawing.Size(73, 32);
            this.labelPool.TabIndex = 55;
            this.labelPool.Text = "Pool";
            // 
            // labelRoofType
            // 
            this.labelRoofType.AutoSize = true;
            this.labelRoofType.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.labelRoofType.Location = new System.Drawing.Point(23, 277);
            this.labelRoofType.Name = "labelRoofType";
            this.labelRoofType.Size = new System.Drawing.Size(145, 32);
            this.labelRoofType.TabIndex = 53;
            this.labelRoofType.Text = "Roof Type";
            // 
            // labelParkingCapacity
            // 
            this.labelParkingCapacity.AutoSize = true;
            this.labelParkingCapacity.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.labelParkingCapacity.Location = new System.Drawing.Point(143, 213);
            this.labelParkingCapacity.Name = "labelParkingCapacity";
            this.labelParkingCapacity.Size = new System.Drawing.Size(230, 32);
            this.labelParkingCapacity.TabIndex = 51;
            this.labelParkingCapacity.Text = "Parking Capacity";
            // 
            // labelParkingType
            // 
            this.labelParkingType.AutoSize = true;
            this.labelParkingType.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.labelParkingType.Location = new System.Drawing.Point(23, 213);
            this.labelParkingType.Name = "labelParkingType";
            this.labelParkingType.Size = new System.Drawing.Size(182, 32);
            this.labelParkingType.TabIndex = 49;
            this.labelParkingType.Text = "Parking Type";
            // 
            // labelLotSize
            // 
            this.labelLotSize.AutoSize = true;
            this.labelLotSize.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.labelLotSize.Location = new System.Drawing.Point(367, 149);
            this.labelLotSize.Name = "labelLotSize";
            this.labelLotSize.Size = new System.Drawing.Size(118, 32);
            this.labelLotSize.TabIndex = 47;
            this.labelLotSize.Text = "Lot Size";
            // 
            // labelLotArea
            // 
            this.labelLotArea.AutoSize = true;
            this.labelLotArea.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.labelLotArea.Location = new System.Drawing.Point(255, 149);
            this.labelLotArea.Name = "labelLotArea";
            this.labelLotArea.Size = new System.Drawing.Size(122, 32);
            this.labelLotArea.TabIndex = 45;
            this.labelLotArea.Text = "Lot Area";
            // 
            // labelTotalLivingArea
            // 
            this.labelTotalLivingArea.AutoSize = true;
            this.labelTotalLivingArea.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.labelTotalLivingArea.Location = new System.Drawing.Point(23, 149);
            this.labelTotalLivingArea.Name = "labelTotalLivingArea";
            this.labelTotalLivingArea.Size = new System.Drawing.Size(296, 32);
            this.labelTotalLivingArea.TabIndex = 43;
            this.labelTotalLivingArea.Text = "Total Living Area SqFt";
            // 
            // labelYearBuilt
            // 
            this.labelYearBuilt.AutoSize = true;
            this.labelYearBuilt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelYearBuilt.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.labelYearBuilt.Location = new System.Drawing.Point(255, 8);
            this.labelYearBuilt.Name = "labelYearBuilt";
            this.labelYearBuilt.Size = new System.Drawing.Size(188, 32);
            this.labelYearBuilt.TabIndex = 41;
            this.labelYearBuilt.Text = "YEAR BUILT";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label11.Location = new System.Drawing.Point(2, 69);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(352, 32);
            this.label11.TabIndex = 111;
            this.label11.Text = "ROOMS && BATHROOMS";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label13.Location = new System.Drawing.Point(2, 133);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(198, 32);
            this.label13.TabIndex = 113;
            this.label13.Text = "SIZE && AREA";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label14.Location = new System.Drawing.Point(2, 197);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(174, 32);
            this.label14.TabIndex = 114;
            this.label14.Text = "AMENITIES";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label12.Location = new System.Drawing.Point(2, 261);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(154, 32);
            this.label12.TabIndex = 112;
            this.label12.Text = "BUILDING";
            // 
            // textBoxCounty
            // 
            this.textBoxCounty.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxCounty.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxCounty.Location = new System.Drawing.Point(634, 115);
            this.textBoxCounty.Name = "textBoxCounty";
            this.textBoxCounty.Size = new System.Drawing.Size(124, 39);
            this.textBoxCounty.TabIndex = 100;
            // 
            // labelSchoolDistrict
            // 
            this.labelSchoolDistrict.AutoSize = true;
            this.labelSchoolDistrict.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSchoolDistrict.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.labelSchoolDistrict.Location = new System.Drawing.Point(427, 139);
            this.labelSchoolDistrict.Name = "labelSchoolDistrict";
            this.labelSchoolDistrict.Size = new System.Drawing.Size(197, 32);
            this.labelSchoolDistrict.TabIndex = 5;
            this.labelSchoolDistrict.Text = "School District";
            // 
            // labelLotNumber
            // 
            this.labelLotNumber.AutoSize = true;
            this.labelLotNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelLotNumber.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.labelLotNumber.Location = new System.Drawing.Point(27, 139);
            this.labelLotNumber.Name = "labelLotNumber";
            this.labelLotNumber.Size = new System.Drawing.Size(162, 32);
            this.labelLotNumber.TabIndex = 4;
            this.labelLotNumber.Text = "Lot Number";
            // 
            // labelSubdivision
            // 
            this.labelSubdivision.AutoSize = true;
            this.labelSubdivision.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSubdivision.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.labelSubdivision.Location = new System.Drawing.Point(761, 99);
            this.labelSubdivision.Name = "labelSubdivision";
            this.labelSubdivision.Size = new System.Drawing.Size(163, 32);
            this.labelSubdivision.TabIndex = 3;
            this.labelSubdivision.Text = "Subdivision";
            // 
            // labelComparableNumber
            // 
            this.labelComparableNumber.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.labelComparableNumber.Location = new System.Drawing.Point(158, 8);
            this.labelComparableNumber.Name = "labelComparableNumber";
            this.labelComparableNumber.Size = new System.Drawing.Size(136, 24);
            this.labelComparableNumber.TabIndex = 2;
            this.labelComparableNumber.Text = "Comparable Number";
            // 
            // labelDistanceFromSubject
            // 
            this.labelDistanceFromSubject.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.labelDistanceFromSubject.Location = new System.Drawing.Point(8, 0);
            this.labelDistanceFromSubject.Name = "labelDistanceFromSubject";
            this.labelDistanceFromSubject.Size = new System.Drawing.Size(144, 24);
            this.labelDistanceFromSubject.TabIndex = 1;
            this.labelDistanceFromSubject.Text = "Distance from Subject";
            // 
            // tabPageOwnershipNLocations
            // 
            this.tabPageOwnershipNLocations.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.tabPageOwnershipNLocations.Controls.Add(this.textBoxUnitNumber);
            this.tabPageOwnershipNLocations.Controls.Add(this.bPrintReport);
            this.tabPageOwnershipNLocations.Controls.Add(this.labelUnitNumber);
            this.tabPageOwnershipNLocations.Controls.Add(this.textBoxSuffix);
            this.tabPageOwnershipNLocations.Controls.Add(this.labelSuffix);
            this.tabPageOwnershipNLocations.Controls.Add(this.textBoxPostDirection);
            this.tabPageOwnershipNLocations.Controls.Add(this.labelPostDirection);
            this.tabPageOwnershipNLocations.Controls.Add(this.textBoxPreDirection);
            this.tabPageOwnershipNLocations.Controls.Add(this.labelPreDirection);
            this.tabPageOwnershipNLocations.Controls.Add(this.textBoxHouseNumber);
            this.tabPageOwnershipNLocations.Controls.Add(this.labelHouseNumber);
            this.tabPageOwnershipNLocations.Controls.Add(this.panel1);
            this.tabPageOwnershipNLocations.Controls.Add(this.panel2);
            this.tabPageOwnershipNLocations.Controls.Add(this.panel3);
            this.tabPageOwnershipNLocations.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.tabPageOwnershipNLocations.Location = new System.Drawing.Point(10, 28);
            this.tabPageOwnershipNLocations.Name = "tabPageOwnershipNLocations";
            this.tabPageOwnershipNLocations.Size = new System.Drawing.Size(975, 516);
            this.tabPageOwnershipNLocations.TabIndex = 2;
            this.tabPageOwnershipNLocations.Text = "Ownership & Locations";
            // 
            // bPrintReport
            // 
            this.bPrintReport.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.bPrintReport.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.bPrintReport.ForeColor = System.Drawing.SystemColors.ControlText;
            this.bPrintReport.Location = new System.Drawing.Point(812, 432);
            this.bPrintReport.Name = "bPrintReport";
            this.bPrintReport.Size = new System.Drawing.Size(75, 43);
            this.bPrintReport.TabIndex = 90;
            this.bPrintReport.Text = "Print Report";
            this.bPrintReport.Click += new System.EventHandler(this.bPrintReport_Click);
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Location = new System.Drawing.Point(0, 5);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(972, 219);
            this.panel1.TabIndex = 132;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this._accurateSearch);
            this.groupBox1.Controls.Add(this.textBoxStateC);
            this.groupBox1.Controls.Add(this.textBoxCountyC);
            this.groupBox1.Controls.Add(this.textBoxZipC);
            this.groupBox1.Controls.Add(this.textBoxLatNameC);
            this.groupBox1.Controls.Add(this.textBoxFirstNameC);
            this.groupBox1.Controls.Add(this.buttonSearchC);
            this.groupBox1.Controls.Add(this.label85);
            this.groupBox1.Controls.Add(this.label90);
            this.groupBox1.Controls.Add(this.textBoxStateB);
            this.groupBox1.Controls.Add(this.textBoxCountyB);
            this.groupBox1.Controls.Add(this.textBoxZipB);
            this.groupBox1.Controls.Add(this.label88);
            this.groupBox1.Controls.Add(this.textBoxApnB);
            this.groupBox1.Controls.Add(this.label89);
            this.groupBox1.Controls.Add(this.buttonSearchB);
            this.groupBox1.Controls.Add(this.textBoxStreetA);
            this.groupBox1.Controls.Add(this.textBoxPreDirA);
            this.groupBox1.Controls.Add(this.textBoxStreetNoA);
            this.groupBox1.Controls.Add(this.textBoxStateA);
            this.groupBox1.Controls.Add(this.textBoxCountyA);
            this.groupBox1.Controls.Add(this.textBoxZipA);
            this.groupBox1.Controls.Add(this.textBoxUnitNoA);
            this.groupBox1.Controls.Add(this.textBoxSuffixA);
            this.groupBox1.Controls.Add(this.textBoxPostDirA);
            this.groupBox1.Controls.Add(this.buttonSearchA);
            this.groupBox1.Controls.Add(this.label83);
            this.groupBox1.Controls.Add(this.label63);
            this.groupBox1.Controls.Add(this.label79);
            this.groupBox1.Controls.Add(this.label81);
            this.groupBox1.Controls.Add(this.label78);
            this.groupBox1.Controls.Add(this.label82);
            this.groupBox1.Controls.Add(this.label77);
            this.groupBox1.Controls.Add(this.label80);
            this.groupBox1.Controls.Add(this.label73);
            this.groupBox1.Controls.Add(this.label72);
            this.groupBox1.Controls.Add(this.label71);
            this.groupBox1.Controls.Add(this.label70);
            this.groupBox1.Controls.Add(this.label69);
            this.groupBox1.Controls.Add(this.label68);
            this.groupBox1.Controls.Add(this.label65);
            this.groupBox1.Controls.Add(this.label67);
            this.groupBox1.Controls.Add(this.label66);
            this.groupBox1.Controls.Add(this.label64);
            this.groupBox1.Controls.Add(this.label76);
            this.groupBox1.Controls.Add(this.label75);
            this.groupBox1.Controls.Add(this.label74);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(11, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(888, 213);
            this.groupBox1.TabIndex = 134;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Enter your SEARCH PARAMETERS:";
            // 
            // _accurateSearch
            // 
            this._accurateSearch.AutoSize = true;
            this._accurateSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._accurateSearch.ForeColor = System.Drawing.Color.Red;
            this._accurateSearch.Location = new System.Drawing.Point(581, 182);
            this._accurateSearch.Name = "_accurateSearch";
            this._accurateSearch.Size = new System.Drawing.Size(559, 32);
            this._accurateSearch.TabIndex = 136;
            this._accurateSearch.Text = "For accurate results use (A) or (B) searches";
            this._accurateSearch.Visible = false;
            // 
            // textBoxStateC
            // 
            this.textBoxStateC.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxStateC.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxStateC.Location = new System.Drawing.Point(470, 180);
            this.textBoxStateC.MaxLength = 2;
            this.textBoxStateC.Name = "textBoxStateC";
            this.textBoxStateC.Size = new System.Drawing.Size(24, 39);
            this.textBoxStateC.TabIndex = 88;
            // 
            // textBoxCountyC
            // 
            this.textBoxCountyC.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxCountyC.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxCountyC.Location = new System.Drawing.Point(330, 180);
            this.textBoxCountyC.Name = "textBoxCountyC";
            this.textBoxCountyC.Size = new System.Drawing.Size(115, 39);
            this.textBoxCountyC.TabIndex = 87;
            // 
            // textBoxZipC
            // 
            this.textBoxZipC.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxZipC.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxZipC.Location = new System.Drawing.Point(210, 180);
            this.textBoxZipC.Name = "textBoxZipC";
            this.textBoxZipC.Size = new System.Drawing.Size(64, 39);
            this.textBoxZipC.TabIndex = 86;
            // 
            // textBoxLatNameC
            // 
            this.textBoxLatNameC.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxLatNameC.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxLatNameC.Location = new System.Drawing.Point(116, 180);
            this.textBoxLatNameC.Name = "textBoxLatNameC";
            this.textBoxLatNameC.Size = new System.Drawing.Size(88, 39);
            this.textBoxLatNameC.TabIndex = 85;
            // 
            // textBoxFirstNameC
            // 
            this.textBoxFirstNameC.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxFirstNameC.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxFirstNameC.Location = new System.Drawing.Point(22, 180);
            this.textBoxFirstNameC.Name = "textBoxFirstNameC";
            this.textBoxFirstNameC.Size = new System.Drawing.Size(88, 39);
            this.textBoxFirstNameC.TabIndex = 84;
            // 
            // buttonSearchC
            // 
            this.buttonSearchC.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.buttonSearchC.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonSearchC.ForeColor = System.Drawing.SystemColors.ControlText;
            this.buttonSearchC.Location = new System.Drawing.Point(500, 178);
            this.buttonSearchC.Name = "buttonSearchC";
            this.buttonSearchC.Size = new System.Drawing.Size(75, 23);
            this.buttonSearchC.TabIndex = 89;
            this.buttonSearchC.Text = "SEARCH";
            this.buttonSearchC.Click += new System.EventHandler(this.buttonSearchC_Click);
            // 
            // label85
            // 
            this.label85.AutoSize = true;
            this.label85.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label85.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label85.Location = new System.Drawing.Point(292, 180);
            this.label85.Name = "label85";
            this.label85.Size = new System.Drawing.Size(40, 32);
            this.label85.TabIndex = 134;
            this.label85.Text = "or";
            // 
            // label90
            // 
            this.label90.AutoSize = true;
            this.label90.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label90.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label90.Location = new System.Drawing.Point(451, 180);
            this.label90.Name = "label90";
            this.label90.Size = new System.Drawing.Size(34, 32);
            this.label90.TabIndex = 135;
            this.label90.Text = "&&";
            // 
            // textBoxStateB
            // 
            this.textBoxStateB.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxStateB.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxStateB.Location = new System.Drawing.Point(352, 115);
            this.textBoxStateB.MaxLength = 2;
            this.textBoxStateB.Name = "textBoxStateB";
            this.textBoxStateB.Size = new System.Drawing.Size(24, 39);
            this.textBoxStateB.TabIndex = 78;
            // 
            // textBoxCountyB
            // 
            this.textBoxCountyB.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxCountyB.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxCountyB.Location = new System.Drawing.Point(212, 115);
            this.textBoxCountyB.Name = "textBoxCountyB";
            this.textBoxCountyB.Size = new System.Drawing.Size(115, 39);
            this.textBoxCountyB.TabIndex = 77;
            // 
            // textBoxZipB
            // 
            this.textBoxZipB.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxZipB.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxZipB.Location = new System.Drawing.Point(116, 115);
            this.textBoxZipB.Name = "textBoxZipB";
            this.textBoxZipB.Size = new System.Drawing.Size(40, 39);
            this.textBoxZipB.TabIndex = 76;
            // 
            // label88
            // 
            this.label88.AutoSize = true;
            this.label88.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label88.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label88.Location = new System.Drawing.Point(175, 115);
            this.label88.Name = "label88";
            this.label88.Size = new System.Drawing.Size(40, 32);
            this.label88.TabIndex = 132;
            this.label88.Text = "or";
            // 
            // textBoxApnB
            // 
            this.textBoxApnB.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxApnB.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxApnB.Location = new System.Drawing.Point(22, 115);
            this.textBoxApnB.Name = "textBoxApnB";
            this.textBoxApnB.Size = new System.Drawing.Size(88, 39);
            this.textBoxApnB.TabIndex = 75;
            // 
            // label89
            // 
            this.label89.AutoSize = true;
            this.label89.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label89.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label89.Location = new System.Drawing.Point(333, 115);
            this.label89.Name = "label89";
            this.label89.Size = new System.Drawing.Size(34, 32);
            this.label89.TabIndex = 133;
            this.label89.Text = "&&";
            // 
            // buttonSearchB
            // 
            this.buttonSearchB.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.buttonSearchB.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonSearchB.ForeColor = System.Drawing.SystemColors.ControlText;
            this.buttonSearchB.Location = new System.Drawing.Point(382, 113);
            this.buttonSearchB.Name = "buttonSearchB";
            this.buttonSearchB.Size = new System.Drawing.Size(75, 23);
            this.buttonSearchB.TabIndex = 79;
            this.buttonSearchB.Text = "SEARCH";
            this.buttonSearchB.Click += new System.EventHandler(this.buttonSearchB_Click);
            // 
            // textBoxStreetA
            // 
            this.textBoxStreetA.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxStreetA.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxStreetA.Location = new System.Drawing.Point(146, 52);
            this.textBoxStreetA.Name = "textBoxStreetA";
            this.textBoxStreetA.Size = new System.Drawing.Size(214, 39);
            this.textBoxStreetA.TabIndex = 60;
            // 
            // textBoxPreDirA
            // 
            this.textBoxPreDirA.BackColor = System.Drawing.Color.LightGray;
            this.textBoxPreDirA.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxPreDirA.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxPreDirA.Location = new System.Drawing.Point(92, 52);
            this.textBoxPreDirA.Name = "textBoxPreDirA";
            this.textBoxPreDirA.Size = new System.Drawing.Size(48, 39);
            this.textBoxPreDirA.TabIndex = 59;
            this.textBoxPreDirA.TabStop = false;
            // 
            // textBoxStreetNoA
            // 
            this.textBoxStreetNoA.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxStreetNoA.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxStreetNoA.Location = new System.Drawing.Point(22, 52);
            this.textBoxStreetNoA.Name = "textBoxStreetNoA";
            this.textBoxStreetNoA.Size = new System.Drawing.Size(64, 39);
            this.textBoxStreetNoA.TabIndex = 58;
            // 
            // textBoxStateA
            // 
            this.textBoxStateA.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxStateA.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxStateA.Location = new System.Drawing.Point(762, 52);
            this.textBoxStateA.MaxLength = 2;
            this.textBoxStateA.Name = "textBoxStateA";
            this.textBoxStateA.Size = new System.Drawing.Size(24, 39);
            this.textBoxStateA.TabIndex = 66;
            // 
            // textBoxCountyA
            // 
            this.textBoxCountyA.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxCountyA.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxCountyA.Location = new System.Drawing.Point(638, 52);
            this.textBoxCountyA.Name = "textBoxCountyA";
            this.textBoxCountyA.Size = new System.Drawing.Size(118, 39);
            this.textBoxCountyA.TabIndex = 65;
            // 
            // textBoxZipA
            // 
            this.textBoxZipA.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxZipA.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxZipA.Location = new System.Drawing.Point(568, 52);
            this.textBoxZipA.Name = "textBoxZipA";
            this.textBoxZipA.Size = new System.Drawing.Size(64, 39);
            this.textBoxZipA.TabIndex = 64;
            // 
            // textBoxUnitNoA
            // 
            this.textBoxUnitNoA.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxUnitNoA.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxUnitNoA.Location = new System.Drawing.Point(506, 52);
            this.textBoxUnitNoA.Name = "textBoxUnitNoA";
            this.textBoxUnitNoA.Size = new System.Drawing.Size(56, 39);
            this.textBoxUnitNoA.TabIndex = 63;
            // 
            // textBoxSuffixA
            // 
            this.textBoxSuffixA.BackColor = System.Drawing.Color.LightGray;
            this.textBoxSuffixA.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxSuffixA.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxSuffixA.Location = new System.Drawing.Point(436, 52);
            this.textBoxSuffixA.Name = "textBoxSuffixA";
            this.textBoxSuffixA.Size = new System.Drawing.Size(64, 39);
            this.textBoxSuffixA.TabIndex = 62;
            this.textBoxSuffixA.TabStop = false;
            // 
            // textBoxPostDirA
            // 
            this.textBoxPostDirA.BackColor = System.Drawing.Color.LightGray;
            this.textBoxPostDirA.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxPostDirA.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxPostDirA.Location = new System.Drawing.Point(366, 52);
            this.textBoxPostDirA.Name = "textBoxPostDirA";
            this.textBoxPostDirA.Size = new System.Drawing.Size(64, 39);
            this.textBoxPostDirA.TabIndex = 61;
            this.textBoxPostDirA.TabStop = false;
            // 
            // buttonSearchA
            // 
            this.buttonSearchA.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.buttonSearchA.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonSearchA.ForeColor = System.Drawing.SystemColors.ControlText;
            this.buttonSearchA.Location = new System.Drawing.Point(792, 50);
            this.buttonSearchA.Name = "buttonSearchA";
            this.buttonSearchA.Size = new System.Drawing.Size(75, 23);
            this.buttonSearchA.TabIndex = 67;
            this.buttonSearchA.Text = "SEARCH";
            this.buttonSearchA.Click += new System.EventHandler(this.buttonSearchA_Click);
            // 
            // label83
            // 
            this.label83.AutoSize = true;
            this.label83.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label83.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label83.Location = new System.Drawing.Point(6, 18);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(503, 32);
            this.label83.TabIndex = 47;
            this.label83.Text = "A.   ADDRESS SEARCH (preferred):";
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label63.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label63.Location = new System.Drawing.Point(467, 164);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(82, 32);
            this.label63.TabIndex = 83;
            this.label63.Text = "State";
            // 
            // label79
            // 
            this.label79.AutoSize = true;
            this.label79.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label79.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label79.Location = new System.Drawing.Point(363, 36);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(113, 32);
            this.label79.TabIndex = 52;
            this.label79.Text = "Post-dir";
            // 
            // label81
            // 
            this.label81.AutoSize = true;
            this.label81.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label81.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label81.Location = new System.Drawing.Point(89, 36);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(100, 32);
            this.label81.TabIndex = 50;
            this.label81.Text = "Pre-dir";
            // 
            // label78
            // 
            this.label78.AutoSize = true;
            this.label78.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label78.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label78.Location = new System.Drawing.Point(433, 35);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(87, 32);
            this.label78.TabIndex = 53;
            this.label78.Text = "Suffix";
            // 
            // label82
            // 
            this.label82.AutoSize = true;
            this.label82.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label82.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label82.Location = new System.Drawing.Point(19, 36);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(142, 32);
            this.label82.TabIndex = 49;
            this.label82.Text = "Street No.";
            // 
            // label77
            // 
            this.label77.AutoSize = true;
            this.label77.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label77.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label77.Location = new System.Drawing.Point(503, 36);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(117, 32);
            this.label77.TabIndex = 54;
            this.label77.Text = "Unit No.";
            // 
            // label80
            // 
            this.label80.AutoSize = true;
            this.label80.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label80.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label80.Location = new System.Drawing.Point(143, 36);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(173, 32);
            this.label80.TabIndex = 51;
            this.label80.Text = "Street Name";
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label73.Location = new System.Drawing.Point(6, 80);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(462, 32);
            this.label73.TabIndex = 67;
            this.label73.Text = "B.   PARCEL NUMBER SEARCH:";
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label72.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label72.Location = new System.Drawing.Point(6, 145);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(598, 32);
            this.label72.TabIndex = 69;
            this.label72.Text = "C.   OWNER NAME SEARCH (try last only):";
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label71.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label71.Location = new System.Drawing.Point(19, 99);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(73, 32);
            this.label71.TabIndex = 71;
            this.label71.Text = "APN";
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label70.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label70.Location = new System.Drawing.Point(113, 99);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(58, 32);
            this.label70.TabIndex = 72;
            this.label70.Text = "ZIP";
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label69.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label69.Location = new System.Drawing.Point(209, 99);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(105, 32);
            this.label69.TabIndex = 73;
            this.label69.Text = "County";
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label68.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label68.Location = new System.Drawing.Point(349, 99);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(82, 32);
            this.label68.TabIndex = 74;
            this.label68.Text = "State";
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label65.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label65.Location = new System.Drawing.Point(207, 164);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(58, 32);
            this.label65.TabIndex = 81;
            this.label65.Text = "ZIP";
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label67.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label67.Location = new System.Drawing.Point(19, 164);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(152, 32);
            this.label67.TabIndex = 79;
            this.label67.Text = "First Name";
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label66.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label66.Location = new System.Drawing.Point(113, 164);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(151, 32);
            this.label66.TabIndex = 80;
            this.label66.Text = "Last Name";
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label64.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label64.Location = new System.Drawing.Point(327, 164);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(105, 32);
            this.label64.TabIndex = 82;
            this.label64.Text = "County";
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label76.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label76.Location = new System.Drawing.Point(565, 36);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(58, 32);
            this.label76.TabIndex = 55;
            this.label76.Text = "ZIP";
            // 
            // label75
            // 
            this.label75.AutoSize = true;
            this.label75.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label75.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label75.Location = new System.Drawing.Point(635, 36);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(105, 32);
            this.label75.TabIndex = 56;
            this.label75.Text = "County";
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label74.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label74.Location = new System.Drawing.Point(759, 35);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(82, 32);
            this.label74.TabIndex = 57;
            this.label74.Text = "State";
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.Controls.Add(this.groupBox2);
            this.panel2.Controls.Add(this.line1);
            this.panel2.Location = new System.Drawing.Point(0, 217);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(975, 208);
            this.panel2.TabIndex = 132;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.textBoxOwnerName);
            this.groupBox2.Controls.Add(this.textBoxMailingZipCode);
            this.groupBox2.Controls.Add(this.textBoxMailingCityAndState);
            this.groupBox2.Controls.Add(this.textBoxMailingAddress);
            this.groupBox2.Controls.Add(this.textBoxPhoneNumber);
            this.groupBox2.Controls.Add(this.textBoxCounty);
            this.groupBox2.Controls.Add(this.textBoxStreetnName);
            this.groupBox2.Controls.Add(this.textBoxZipCode);
            this.groupBox2.Controls.Add(this.textBoxState);
            this.groupBox2.Controls.Add(this.textBoxCity);
            this.groupBox2.Controls.Add(this.textBoxSubdivision);
            this.groupBox2.Controls.Add(this.textBoxMapReferenceOne);
            this.groupBox2.Controls.Add(this.textBoxMapReferenceTwo);
            this.groupBox2.Controls.Add(this.textBoxSchoolDistrict);
            this.groupBox2.Controls.Add(this.textBoxLotNumber);
            this.groupBox2.Controls.Add(this.labelOwnerNameGroup);
            this.groupBox2.Controls.Add(this.labelSubdivision);
            this.groupBox2.Controls.Add(this.labelLotNumber);
            this.groupBox2.Controls.Add(this.labelSchoolDistrict);
            this.groupBox2.Controls.Add(this.labelCity);
            this.groupBox2.Controls.Add(this.labelState);
            this.groupBox2.Controls.Add(this.labelZipCode);
            this.groupBox2.Controls.Add(this.labelMapReferenceTwo);
            this.groupBox2.Controls.Add(this.labelMailingAddress);
            this.groupBox2.Controls.Add(this.labelMailingCityAndState);
            this.groupBox2.Controls.Add(this.labelMailingZipCode);
            this.groupBox2.Controls.Add(this.labelMapReferenceOne);
            this.groupBox2.Controls.Add(this.labelSiteAddress);
            this.groupBox2.Controls.Add(this.labelCounty);
            this.groupBox2.Controls.Add(this.labelStreetnName);
            this.groupBox2.Controls.Add(this.labelPhoneNumber);
            this.groupBox2.Controls.Add(this.labelOwnerName);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(11, 16);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(888, 192);
            this.groupBox2.TabIndex = 134;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "SEARCH RESULT";
            // 
            // labelSiteAddress
            // 
            this.labelSiteAddress.AutoSize = true;
            this.labelSiteAddress.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSiteAddress.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.labelSiteAddress.Location = new System.Drawing.Point(12, 81);
            this.labelSiteAddress.Name = "labelSiteAddress";
            this.labelSiteAddress.Size = new System.Drawing.Size(159, 32);
            this.labelSiteAddress.TabIndex = 45;
            this.labelSiteAddress.Text = "SITE INFO";
            // 
            // labelCounty
            // 
            this.labelCounty.AutoSize = true;
            this.labelCounty.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCounty.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.labelCounty.Location = new System.Drawing.Point(631, 99);
            this.labelCounty.Name = "labelCounty";
            this.labelCounty.Size = new System.Drawing.Size(105, 32);
            this.labelCounty.TabIndex = 0;
            this.labelCounty.Text = "County";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.textBoxDistanceFromSubject);
            this.panel3.Controls.Add(this.textBoxComparableNumber);
            this.panel3.Controls.Add(this.labelComparableNumber);
            this.panel3.Controls.Add(this.labelDistanceFromSubject);
            this.panel3.Location = new System.Drawing.Point(11, 431);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(336, 56);
            this.panel3.TabIndex = 133;
            // 
            // tabControlOnlinePropertyProfileNComps
            // 
            this.tabControlOnlinePropertyProfileNComps.Controls.Add(this.tabPageOwnershipNLocations);
            this.tabControlOnlinePropertyProfileNComps.Controls.Add(this.tabPagePropertyCharacteristics);
            this.tabControlOnlinePropertyProfileNComps.Controls.Add(this.tabPageFinancials);
            this.tabControlOnlinePropertyProfileNComps.Controls.Add(this.tabPageTaxesNLegal);
            this.tabControlOnlinePropertyProfileNComps.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlOnlinePropertyProfileNComps.ItemSize = new System.Drawing.Size(128, 18);
            this.tabControlOnlinePropertyProfileNComps.Location = new System.Drawing.Point(0, 0);
            this.tabControlOnlinePropertyProfileNComps.Name = "tabControlOnlinePropertyProfileNComps";
            this.tabControlOnlinePropertyProfileNComps.SelectedIndex = 0;
            this.tabControlOnlinePropertyProfileNComps.Size = new System.Drawing.Size(995, 554);
            this.tabControlOnlinePropertyProfileNComps.TabIndex = 0;
            // 
            // tabPageFinancials
            // 
            this.tabPageFinancials.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.tabPageFinancials.Controls.Add(this.groupBox4);
            this.tabPageFinancials.Controls.Add(this.groupBox3);
            this.tabPageFinancials.Controls.Add(this.pictureBoxFinancials);
            this.tabPageFinancials.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.tabPageFinancials.Location = new System.Drawing.Point(10, 28);
            this.tabPageFinancials.Name = "tabPageFinancials";
            this.tabPageFinancials.Size = new System.Drawing.Size(975, 516);
            this.tabPageFinancials.TabIndex = 3;
            this.tabPageFinancials.Text = "Financials";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.textBoxPSMInterestRateType);
            this.groupBox4.Controls.Add(this.textBoxPSMInterestRateTerm);
            this.groupBox4.Controls.Add(this.textBoxPSMInterestRate);
            this.groupBox4.Controls.Add(this.textBoxPSMMortgageAmount);
            this.groupBox4.Controls.Add(this.label50);
            this.groupBox4.Controls.Add(this.label51);
            this.groupBox4.Controls.Add(this.label52);
            this.groupBox4.Controls.Add(this.label53);
            this.groupBox4.Controls.Add(this.label54);
            this.groupBox4.Controls.Add(this.textBoxPFMMortgageAmount);
            this.groupBox4.Controls.Add(this.textBoxPFMInterestRate);
            this.groupBox4.Controls.Add(this.textBoxPFMLender);
            this.groupBox4.Controls.Add(this.textBoxPFMInterestRateTerm);
            this.groupBox4.Controls.Add(this.textBoxPFMInterestRateType);
            this.groupBox4.Controls.Add(this.label49);
            this.groupBox4.Controls.Add(this.label48);
            this.groupBox4.Controls.Add(this.label47);
            this.groupBox4.Controls.Add(this.label45);
            this.groupBox4.Controls.Add(this.label46);
            this.groupBox4.Controls.Add(this.textBoxPSSalePrice);
            this.groupBox4.Controls.Add(this.textBoxPSSaleType1);
            this.groupBox4.Controls.Add(this.textBoxPSSaleDate);
            this.groupBox4.Controls.Add(this.textBoxPSDeedType);
            this.groupBox4.Controls.Add(this.textBoxPSRecordingDate);
            this.groupBox4.Controls.Add(this.textBoxPSDocumentNumber);
            this.groupBox4.Controls.Add(this.textBoxPSStampAmt);
            this.groupBox4.Controls.Add(this.label42);
            this.groupBox4.Controls.Add(this.label36);
            this.groupBox4.Controls.Add(this.label27);
            this.groupBox4.Controls.Add(this.label35);
            this.groupBox4.Controls.Add(this.label28);
            this.groupBox4.Controls.Add(this.label38);
            this.groupBox4.Controls.Add(this.label39);
            this.groupBox4.Controls.Add(this.label37);
            this.groupBox4.Controls.Add(this.label40);
            this.groupBox4.Controls.Add(this.label41);
            this.groupBox4.Controls.Add(this.label43);
            this.groupBox4.Controls.Add(this.label44);
            this.groupBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox4.Location = new System.Drawing.Point(9, 248);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(846, 197);
            this.groupBox4.TabIndex = 170;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "PRIOR SALE";
            // 
            // textBoxPSMInterestRateType
            // 
            this.textBoxPSMInterestRateType.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxPSMInterestRateType.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxPSMInterestRateType.Location = new System.Drawing.Point(362, 167);
            this.textBoxPSMInterestRateType.Name = "textBoxPSMInterestRateType";
            this.textBoxPSMInterestRateType.Size = new System.Drawing.Size(60, 39);
            this.textBoxPSMInterestRateType.TabIndex = 37;
            // 
            // textBoxPSMInterestRateTerm
            // 
            this.textBoxPSMInterestRateTerm.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxPSMInterestRateTerm.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxPSMInterestRateTerm.Location = new System.Drawing.Point(234, 167);
            this.textBoxPSMInterestRateTerm.Name = "textBoxPSMInterestRateTerm";
            this.textBoxPSMInterestRateTerm.Size = new System.Drawing.Size(60, 39);
            this.textBoxPSMInterestRateTerm.TabIndex = 36;
            // 
            // textBoxPSMInterestRate
            // 
            this.textBoxPSMInterestRate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxPSMInterestRate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxPSMInterestRate.Location = new System.Drawing.Point(138, 167);
            this.textBoxPSMInterestRate.Name = "textBoxPSMInterestRate";
            this.textBoxPSMInterestRate.Size = new System.Drawing.Size(64, 39);
            this.textBoxPSMInterestRate.TabIndex = 35;
            // 
            // textBoxPSMMortgageAmount
            // 
            this.textBoxPSMMortgageAmount.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxPSMMortgageAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxPSMMortgageAmount.Location = new System.Drawing.Point(18, 167);
            this.textBoxPSMMortgageAmount.Name = "textBoxPSMMortgageAmount";
            this.textBoxPSMMortgageAmount.Size = new System.Drawing.Size(104, 39);
            this.textBoxPSMMortgageAmount.TabIndex = 34;
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label50.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label50.Location = new System.Drawing.Point(202, 169);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(40, 32);
            this.label50.TabIndex = 167;
            this.label50.Text = "%";
            this.label50.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label51.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label51.Location = new System.Drawing.Point(359, 151);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(191, 32);
            this.label51.TabIndex = 165;
            this.label51.Text = "Int. Rate Type";
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label52.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label52.Location = new System.Drawing.Point(231, 151);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(193, 32);
            this.label52.TabIndex = 163;
            this.label52.Text = "Int. Rate Term";
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label53.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label53.Location = new System.Drawing.Point(135, 151);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(121, 32);
            this.label53.TabIndex = 161;
            this.label53.Text = "Int. Rate";
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label54.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label54.Location = new System.Drawing.Point(15, 151);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(240, 32);
            this.label54.TabIndex = 159;
            this.label54.Text = "Mortgage Amount";
            // 
            // textBoxPFMMortgageAmount
            // 
            this.textBoxPFMMortgageAmount.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxPFMMortgageAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxPFMMortgageAmount.Location = new System.Drawing.Point(18, 100);
            this.textBoxPFMMortgageAmount.Name = "textBoxPFMMortgageAmount";
            this.textBoxPFMMortgageAmount.Size = new System.Drawing.Size(104, 39);
            this.textBoxPFMMortgageAmount.TabIndex = 29;
            // 
            // textBoxPFMInterestRate
            // 
            this.textBoxPFMInterestRate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxPFMInterestRate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxPFMInterestRate.Location = new System.Drawing.Point(138, 100);
            this.textBoxPFMInterestRate.Name = "textBoxPFMInterestRate";
            this.textBoxPFMInterestRate.Size = new System.Drawing.Size(64, 39);
            this.textBoxPFMInterestRate.TabIndex = 30;
            // 
            // textBoxPFMLender
            // 
            this.textBoxPFMLender.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxPFMLender.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxPFMLender.Location = new System.Drawing.Point(482, 100);
            this.textBoxPFMLender.Name = "textBoxPFMLender";
            this.textBoxPFMLender.Size = new System.Drawing.Size(144, 39);
            this.textBoxPFMLender.TabIndex = 33;
            // 
            // textBoxPFMInterestRateTerm
            // 
            this.textBoxPFMInterestRateTerm.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxPFMInterestRateTerm.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxPFMInterestRateTerm.Location = new System.Drawing.Point(234, 100);
            this.textBoxPFMInterestRateTerm.Name = "textBoxPFMInterestRateTerm";
            this.textBoxPFMInterestRateTerm.Size = new System.Drawing.Size(60, 39);
            this.textBoxPFMInterestRateTerm.TabIndex = 31;
            // 
            // textBoxPFMInterestRateType
            // 
            this.textBoxPFMInterestRateType.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxPFMInterestRateType.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxPFMInterestRateType.Location = new System.Drawing.Point(362, 100);
            this.textBoxPFMInterestRateType.Name = "textBoxPFMInterestRateType";
            this.textBoxPFMInterestRateType.Size = new System.Drawing.Size(60, 39);
            this.textBoxPFMInterestRateType.TabIndex = 32;
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label49.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label49.Location = new System.Drawing.Point(15, 84);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(240, 32);
            this.label49.TabIndex = 147;
            this.label49.Text = "Mortgage Amount";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label48.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label48.Location = new System.Drawing.Point(135, 84);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(121, 32);
            this.label48.TabIndex = 149;
            this.label48.Text = "Int. Rate";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label47.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label47.Location = new System.Drawing.Point(231, 84);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(193, 32);
            this.label47.TabIndex = 151;
            this.label47.Text = "Int. Rate Term";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label45.Location = new System.Drawing.Point(479, 84);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(104, 32);
            this.label45.TabIndex = 155;
            this.label45.Text = "Lender";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label46.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label46.Location = new System.Drawing.Point(359, 84);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(191, 32);
            this.label46.TabIndex = 153;
            this.label46.Text = "Int. Rate Type";
            // 
            // textBoxPSSalePrice
            // 
            this.textBoxPSSalePrice.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxPSSalePrice.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxPSSalePrice.Location = new System.Drawing.Point(18, 33);
            this.textBoxPSSalePrice.Name = "textBoxPSSalePrice";
            this.textBoxPSSalePrice.Size = new System.Drawing.Size(104, 39);
            this.textBoxPSSalePrice.TabIndex = 22;
            // 
            // textBoxPSSaleType1
            // 
            this.textBoxPSSaleType1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxPSSaleType1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxPSSaleType1.Location = new System.Drawing.Point(206, 33);
            this.textBoxPSSaleType1.Name = "textBoxPSSaleType1";
            this.textBoxPSSaleType1.Size = new System.Drawing.Size(90, 39);
            this.textBoxPSSaleType1.TabIndex = 24;
            // 
            // textBoxPSSaleDate
            // 
            this.textBoxPSSaleDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxPSSaleDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxPSSaleDate.Location = new System.Drawing.Point(128, 33);
            this.textBoxPSSaleDate.Name = "textBoxPSSaleDate";
            this.textBoxPSSaleDate.Size = new System.Drawing.Size(72, 39);
            this.textBoxPSSaleDate.TabIndex = 23;
            // 
            // textBoxPSDeedType
            // 
            this.textBoxPSDeedType.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxPSDeedType.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxPSDeedType.Location = new System.Drawing.Point(302, 33);
            this.textBoxPSDeedType.Name = "textBoxPSDeedType";
            this.textBoxPSDeedType.Size = new System.Drawing.Size(152, 39);
            this.textBoxPSDeedType.TabIndex = 25;
            // 
            // textBoxPSRecordingDate
            // 
            this.textBoxPSRecordingDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxPSRecordingDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxPSRecordingDate.Location = new System.Drawing.Point(460, 33);
            this.textBoxPSRecordingDate.Name = "textBoxPSRecordingDate";
            this.textBoxPSRecordingDate.Size = new System.Drawing.Size(96, 39);
            this.textBoxPSRecordingDate.TabIndex = 26;
            // 
            // textBoxPSDocumentNumber
            // 
            this.textBoxPSDocumentNumber.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxPSDocumentNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxPSDocumentNumber.Location = new System.Drawing.Point(562, 33);
            this.textBoxPSDocumentNumber.Name = "textBoxPSDocumentNumber";
            this.textBoxPSDocumentNumber.Size = new System.Drawing.Size(112, 39);
            this.textBoxPSDocumentNumber.TabIndex = 27;
            // 
            // textBoxPSStampAmt
            // 
            this.textBoxPSStampAmt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxPSStampAmt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxPSStampAmt.Location = new System.Drawing.Point(698, 33);
            this.textBoxPSStampAmt.Name = "textBoxPSStampAmt";
            this.textBoxPSStampAmt.Size = new System.Drawing.Size(64, 39);
            this.textBoxPSStampAmt.TabIndex = 28;
            // 
            // label42
            // 
            this.label42.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label42.Location = new System.Drawing.Point(680, 29);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(16, 24);
            this.label42.TabIndex = 146;
            this.label42.Text = "$";
            this.label42.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label36.Location = new System.Drawing.Point(15, 17);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(155, 32);
            this.label36.TabIndex = 133;
            this.label36.Text = "Sale Price";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label27.Location = new System.Drawing.Point(2, 65);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(363, 32);
            this.label27.TabIndex = 118;
            this.label27.Text = "PRIOR FIRST MORGAGE";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label35.Location = new System.Drawing.Point(2, 35);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(31, 32);
            this.label35.TabIndex = 135;
            this.label35.Text = "$";
            this.label35.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label28.Location = new System.Drawing.Point(2, 132);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(422, 32);
            this.label28.TabIndex = 119;
            this.label28.Text = "PRIOR SECOND MORTGAGE";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label38.Location = new System.Drawing.Point(203, 17);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(184, 32);
            this.label38.TabIndex = 136;
            this.label38.Text = "Sale Type (1)";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label39.Location = new System.Drawing.Point(299, 17);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(153, 32);
            this.label39.TabIndex = 138;
            this.label39.Text = "Deed Type";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label37.Location = new System.Drawing.Point(125, 17);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(149, 32);
            this.label37.TabIndex = 131;
            this.label37.Text = "Sale Date";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label40.Location = new System.Drawing.Point(457, 17);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(212, 32);
            this.label40.TabIndex = 140;
            this.label40.Text = "Recording Date";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label41.Location = new System.Drawing.Point(559, 17);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(251, 32);
            this.label41.TabIndex = 142;
            this.label41.Text = "Document Number";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label43.Location = new System.Drawing.Point(695, 17);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(154, 32);
            this.label43.TabIndex = 145;
            this.label43.Text = "Stamp Amt";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label44.Location = new System.Drawing.Point(202, 102);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(40, 32);
            this.label44.TabIndex = 157;
            this.label44.Text = "%";
            this.label44.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.textBoxDocumentNumber);
            this.groupBox3.Controls.Add(this.labelDocumentNumber);
            this.groupBox3.Controls.Add(this.textBoxSecMortgageAmount);
            this.groupBox3.Controls.Add(this.textBoxSecInterestRate);
            this.groupBox3.Controls.Add(this.textBoxSecInterestRateTerm);
            this.groupBox3.Controls.Add(this.textBoxSecInterestRateType);
            this.groupBox3.Controls.Add(this.label19);
            this.groupBox3.Controls.Add(this.textBoxFirstMortgageAmount);
            this.groupBox3.Controls.Add(this.textBoxFirstMortgageInterestRate);
            this.groupBox3.Controls.Add(this.textBoxFirstMortgageInterestRateTerm);
            this.groupBox3.Controls.Add(this.textBoxType);
            this.groupBox3.Controls.Add(this.textBoxFirstMortgageLender);
            this.groupBox3.Controls.Add(this.label29);
            this.groupBox3.Controls.Add(this.label33);
            this.groupBox3.Controls.Add(this.label32);
            this.groupBox3.Controls.Add(this.label31);
            this.groupBox3.Controls.Add(this.label30);
            this.groupBox3.Controls.Add(this.labelFirstMortgageAmount);
            this.groupBox3.Controls.Add(this.labelFirstMortgageInterestRate);
            this.groupBox3.Controls.Add(this.labelFirstMortgageInterestRateTerm);
            this.groupBox3.Controls.Add(this.labelFirstMortgageInterestRateType);
            this.groupBox3.Controls.Add(this.labelFirstMortgageLender);
            this.groupBox3.Controls.Add(this.textBoxSalePrice);
            this.groupBox3.Controls.Add(this.textBoxSaleDate);
            this.groupBox3.Controls.Add(this.textBoxDeedType);
            this.groupBox3.Controls.Add(this.textBoxRecordingDate);
            this.groupBox3.Controls.Add(this.textBoxTitleCompany);
            this.groupBox3.Controls.Add(this.textBoxStampAmt);
            this.groupBox3.Controls.Add(this.label24);
            this.groupBox3.Controls.Add(this.textBoxSellerName);
            this.groupBox3.Controls.Add(this.textBoxSaleType2);
            this.groupBox3.Controls.Add(this.textBoxSaleType1);
            this.groupBox3.Controls.Add(this.textBoxCashDown);
            this.groupBox3.Controls.Add(this.textBox4PricePerSqFt);
            this.groupBox3.Controls.Add(this.label16);
            this.groupBox3.Controls.Add(this.label17);
            this.groupBox3.Controls.Add(this.labelSalePrice);
            this.groupBox3.Controls.Add(this.label25);
            this.groupBox3.Controls.Add(this.label26);
            this.groupBox3.Controls.Add(this.labelDeedType);
            this.groupBox3.Controls.Add(this.labelRecordingDate);
            this.groupBox3.Controls.Add(this.labelSellerName);
            this.groupBox3.Controls.Add(this.label15);
            this.groupBox3.Controls.Add(this.labelSaleType1);
            this.groupBox3.Controls.Add(this.labelSaleDate);
            this.groupBox3.Controls.Add(this.labelCashDown);
            this.groupBox3.Controls.Add(this.labelPricePerSqFt);
            this.groupBox3.Controls.Add(this.label18);
            this.groupBox3.Controls.Add(this.label22);
            this.groupBox3.Controls.Add(this.label23);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.Location = new System.Drawing.Point(9, 8);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(846, 238);
            this.groupBox3.TabIndex = 169;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "LAST SALE";
            // 
            // textBoxDocumentNumber
            // 
            this.textBoxDocumentNumber.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxDocumentNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxDocumentNumber.Location = new System.Drawing.Point(239, 72);
            this.textBoxDocumentNumber.Name = "textBoxDocumentNumber";
            this.textBoxDocumentNumber.Size = new System.Drawing.Size(105, 39);
            this.textBoxDocumentNumber.TabIndex = 10;
            // 
            // labelDocumentNumber
            // 
            this.labelDocumentNumber.AutoSize = true;
            this.labelDocumentNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDocumentNumber.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.labelDocumentNumber.Location = new System.Drawing.Point(236, 56);
            this.labelDocumentNumber.Name = "labelDocumentNumber";
            this.labelDocumentNumber.Size = new System.Drawing.Size(251, 32);
            this.labelDocumentNumber.TabIndex = 63;
            this.labelDocumentNumber.Text = "Document Number";
            // 
            // textBoxSecMortgageAmount
            // 
            this.textBoxSecMortgageAmount.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxSecMortgageAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxSecMortgageAmount.Location = new System.Drawing.Point(18, 205);
            this.textBoxSecMortgageAmount.Name = "textBoxSecMortgageAmount";
            this.textBoxSecMortgageAmount.Size = new System.Drawing.Size(104, 39);
            this.textBoxSecMortgageAmount.TabIndex = 18;
            // 
            // textBoxSecInterestRate
            // 
            this.textBoxSecInterestRate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxSecInterestRate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxSecInterestRate.Location = new System.Drawing.Point(138, 205);
            this.textBoxSecInterestRate.Name = "textBoxSecInterestRate";
            this.textBoxSecInterestRate.Size = new System.Drawing.Size(64, 39);
            this.textBoxSecInterestRate.TabIndex = 19;
            // 
            // textBoxSecInterestRateTerm
            // 
            this.textBoxSecInterestRateTerm.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxSecInterestRateTerm.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxSecInterestRateTerm.Location = new System.Drawing.Point(239, 205);
            this.textBoxSecInterestRateTerm.Name = "textBoxSecInterestRateTerm";
            this.textBoxSecInterestRateTerm.Size = new System.Drawing.Size(115, 39);
            this.textBoxSecInterestRateTerm.TabIndex = 20;
            // 
            // textBoxSecInterestRateType
            // 
            this.textBoxSecInterestRateType.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxSecInterestRateType.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxSecInterestRateType.Location = new System.Drawing.Point(360, 203);
            this.textBoxSecInterestRateType.Name = "textBoxSecInterestRateType";
            this.textBoxSecInterestRateType.Size = new System.Drawing.Size(60, 39);
            this.textBoxSecInterestRateType.TabIndex = 21;
            // 
            // label19
            // 
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(202, 203);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(16, 24);
            this.label19.TabIndex = 129;
            this.label19.Text = "%";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // textBoxFirstMortgageAmount
            // 
            this.textBoxFirstMortgageAmount.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxFirstMortgageAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxFirstMortgageAmount.Location = new System.Drawing.Point(18, 138);
            this.textBoxFirstMortgageAmount.Name = "textBoxFirstMortgageAmount";
            this.textBoxFirstMortgageAmount.Size = new System.Drawing.Size(104, 39);
            this.textBoxFirstMortgageAmount.TabIndex = 13;
            // 
            // textBoxFirstMortgageInterestRate
            // 
            this.textBoxFirstMortgageInterestRate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxFirstMortgageInterestRate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxFirstMortgageInterestRate.Location = new System.Drawing.Point(138, 138);
            this.textBoxFirstMortgageInterestRate.Name = "textBoxFirstMortgageInterestRate";
            this.textBoxFirstMortgageInterestRate.Size = new System.Drawing.Size(64, 39);
            this.textBoxFirstMortgageInterestRate.TabIndex = 14;
            // 
            // textBoxFirstMortgageInterestRateTerm
            // 
            this.textBoxFirstMortgageInterestRateTerm.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxFirstMortgageInterestRateTerm.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxFirstMortgageInterestRateTerm.Location = new System.Drawing.Point(239, 138);
            this.textBoxFirstMortgageInterestRateTerm.Name = "textBoxFirstMortgageInterestRateTerm";
            this.textBoxFirstMortgageInterestRateTerm.Size = new System.Drawing.Size(115, 39);
            this.textBoxFirstMortgageInterestRateTerm.TabIndex = 15;
            // 
            // textBoxType
            // 
            this.textBoxType.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxType.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxType.Location = new System.Drawing.Point(360, 138);
            this.textBoxType.Name = "textBoxType";
            this.textBoxType.Size = new System.Drawing.Size(115, 39);
            this.textBoxType.TabIndex = 16;
            // 
            // textBoxFirstMortgageLender
            // 
            this.textBoxFirstMortgageLender.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxFirstMortgageLender.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxFirstMortgageLender.Location = new System.Drawing.Point(481, 138);
            this.textBoxFirstMortgageLender.Name = "textBoxFirstMortgageLender";
            this.textBoxFirstMortgageLender.Size = new System.Drawing.Size(189, 39);
            this.textBoxFirstMortgageLender.TabIndex = 17;
            // 
            // label29
            // 
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(202, 135);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(16, 24);
            this.label29.TabIndex = 120;
            this.label29.Text = "%";
            this.label29.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label33.Location = new System.Drawing.Point(15, 189);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(240, 32);
            this.label33.TabIndex = 121;
            this.label33.Text = "Mortgage Amount";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label32.Location = new System.Drawing.Point(135, 189);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(121, 32);
            this.label32.TabIndex = 123;
            this.label32.Text = "Int. Rate";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label31.Location = new System.Drawing.Point(236, 189);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(248, 32);
            this.label31.TabIndex = 125;
            this.label31.Text = "Interest Rate Term";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label30.Location = new System.Drawing.Point(357, 187);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(246, 32);
            this.label30.TabIndex = 127;
            this.label30.Text = "Interest Rate Type";
            // 
            // labelFirstMortgageAmount
            // 
            this.labelFirstMortgageAmount.AutoSize = true;
            this.labelFirstMortgageAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelFirstMortgageAmount.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.labelFirstMortgageAmount.Location = new System.Drawing.Point(15, 122);
            this.labelFirstMortgageAmount.Name = "labelFirstMortgageAmount";
            this.labelFirstMortgageAmount.Size = new System.Drawing.Size(240, 32);
            this.labelFirstMortgageAmount.TabIndex = 67;
            this.labelFirstMortgageAmount.Text = "Mortgage Amount";
            // 
            // labelFirstMortgageInterestRate
            // 
            this.labelFirstMortgageInterestRate.AutoSize = true;
            this.labelFirstMortgageInterestRate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelFirstMortgageInterestRate.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.labelFirstMortgageInterestRate.Location = new System.Drawing.Point(135, 122);
            this.labelFirstMortgageInterestRate.Name = "labelFirstMortgageInterestRate";
            this.labelFirstMortgageInterestRate.Size = new System.Drawing.Size(121, 32);
            this.labelFirstMortgageInterestRate.TabIndex = 69;
            this.labelFirstMortgageInterestRate.Text = "Int. Rate";
            // 
            // labelFirstMortgageInterestRateTerm
            // 
            this.labelFirstMortgageInterestRateTerm.AutoSize = true;
            this.labelFirstMortgageInterestRateTerm.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelFirstMortgageInterestRateTerm.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.labelFirstMortgageInterestRateTerm.Location = new System.Drawing.Point(236, 122);
            this.labelFirstMortgageInterestRateTerm.Name = "labelFirstMortgageInterestRateTerm";
            this.labelFirstMortgageInterestRateTerm.Size = new System.Drawing.Size(248, 32);
            this.labelFirstMortgageInterestRateTerm.TabIndex = 71;
            this.labelFirstMortgageInterestRateTerm.Text = "Interest Rate Term";
            // 
            // labelFirstMortgageInterestRateType
            // 
            this.labelFirstMortgageInterestRateType.AutoSize = true;
            this.labelFirstMortgageInterestRateType.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelFirstMortgageInterestRateType.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.labelFirstMortgageInterestRateType.Location = new System.Drawing.Point(357, 122);
            this.labelFirstMortgageInterestRateType.Name = "labelFirstMortgageInterestRateType";
            this.labelFirstMortgageInterestRateType.Size = new System.Drawing.Size(246, 32);
            this.labelFirstMortgageInterestRateType.TabIndex = 73;
            this.labelFirstMortgageInterestRateType.Text = "Interest Rate Type";
            // 
            // labelFirstMortgageLender
            // 
            this.labelFirstMortgageLender.AutoSize = true;
            this.labelFirstMortgageLender.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelFirstMortgageLender.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.labelFirstMortgageLender.Location = new System.Drawing.Point(478, 122);
            this.labelFirstMortgageLender.Name = "labelFirstMortgageLender";
            this.labelFirstMortgageLender.Size = new System.Drawing.Size(104, 32);
            this.labelFirstMortgageLender.TabIndex = 75;
            this.labelFirstMortgageLender.Text = "Lender";
            // 
            // textBoxSalePrice
            // 
            this.textBoxSalePrice.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxSalePrice.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxSalePrice.Location = new System.Drawing.Point(18, 32);
            this.textBoxSalePrice.Name = "textBoxSalePrice";
            this.textBoxSalePrice.Size = new System.Drawing.Size(104, 39);
            this.textBoxSalePrice.TabIndex = 1;
            // 
            // textBoxSaleDate
            // 
            this.textBoxSaleDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxSaleDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxSaleDate.Location = new System.Drawing.Point(138, 32);
            this.textBoxSaleDate.Name = "textBoxSaleDate";
            this.textBoxSaleDate.Size = new System.Drawing.Size(80, 39);
            this.textBoxSaleDate.TabIndex = 2;
            // 
            // textBoxDeedType
            // 
            this.textBoxDeedType.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxDeedType.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxDeedType.Location = new System.Drawing.Point(18, 72);
            this.textBoxDeedType.Name = "textBoxDeedType";
            this.textBoxDeedType.Size = new System.Drawing.Size(104, 39);
            this.textBoxDeedType.TabIndex = 8;
            // 
            // textBoxRecordingDate
            // 
            this.textBoxRecordingDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxRecordingDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxRecordingDate.Location = new System.Drawing.Point(138, 72);
            this.textBoxRecordingDate.Name = "textBoxRecordingDate";
            this.textBoxRecordingDate.Size = new System.Drawing.Size(80, 39);
            this.textBoxRecordingDate.TabIndex = 9;
            // 
            // textBoxTitleCompany
            // 
            this.textBoxTitleCompany.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxTitleCompany.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxTitleCompany.Location = new System.Drawing.Point(480, 72);
            this.textBoxTitleCompany.Name = "textBoxTitleCompany";
            this.textBoxTitleCompany.Size = new System.Drawing.Size(176, 39);
            this.textBoxTitleCompany.TabIndex = 12;
            // 
            // textBoxStampAmt
            // 
            this.textBoxStampAmt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxStampAmt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxStampAmt.Location = new System.Drawing.Point(368, 72);
            this.textBoxStampAmt.Name = "textBoxStampAmt";
            this.textBoxStampAmt.Size = new System.Drawing.Size(104, 39);
            this.textBoxStampAmt.TabIndex = 11;
            // 
            // label24
            // 
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(350, 68);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(16, 24);
            this.label24.TabIndex = 115;
            this.label24.Text = "$";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxSellerName
            // 
            this.textBoxSellerName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxSellerName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxSellerName.Location = new System.Drawing.Point(676, 32);
            this.textBoxSellerName.Name = "textBoxSellerName";
            this.textBoxSellerName.Size = new System.Drawing.Size(161, 39);
            this.textBoxSellerName.TabIndex = 7;
            // 
            // textBoxSaleType2
            // 
            this.textBoxSaleType2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxSaleType2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxSaleType2.Location = new System.Drawing.Point(574, 32);
            this.textBoxSaleType2.Name = "textBoxSaleType2";
            this.textBoxSaleType2.Size = new System.Drawing.Size(96, 39);
            this.textBoxSaleType2.TabIndex = 6;
            // 
            // textBoxSaleType1
            // 
            this.textBoxSaleType1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxSaleType1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxSaleType1.Location = new System.Drawing.Point(480, 32);
            this.textBoxSaleType1.Name = "textBoxSaleType1";
            this.textBoxSaleType1.Size = new System.Drawing.Size(88, 39);
            this.textBoxSaleType1.TabIndex = 5;
            // 
            // textBoxCashDown
            // 
            this.textBoxCashDown.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxCashDown.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxCashDown.Location = new System.Drawing.Point(239, 32);
            this.textBoxCashDown.Name = "textBoxCashDown";
            this.textBoxCashDown.Size = new System.Drawing.Size(105, 39);
            this.textBoxCashDown.TabIndex = 3;
            // 
            // textBox4PricePerSqFt
            // 
            this.textBox4PricePerSqFt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox4PricePerSqFt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox4PricePerSqFt.Location = new System.Drawing.Point(368, 32);
            this.textBox4PricePerSqFt.Name = "textBox4PricePerSqFt";
            this.textBox4PricePerSqFt.Size = new System.Drawing.Size(104, 39);
            this.textBox4PricePerSqFt.TabIndex = 4;
            // 
            // label16
            // 
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(350, 28);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(16, 24);
            this.label16.TabIndex = 105;
            this.label16.Text = "$";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label17
            // 
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(221, 28);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(16, 24);
            this.label17.TabIndex = 106;
            this.label17.Text = "$";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelSalePrice
            // 
            this.labelSalePrice.AutoSize = true;
            this.labelSalePrice.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSalePrice.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.labelSalePrice.Location = new System.Drawing.Point(15, 16);
            this.labelSalePrice.Name = "labelSalePrice";
            this.labelSalePrice.Size = new System.Drawing.Size(155, 32);
            this.labelSalePrice.TabIndex = 57;
            this.labelSalePrice.Text = "Sale Price";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label25.Location = new System.Drawing.Point(2, 107);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(280, 32);
            this.label25.TabIndex = 116;
            this.label25.Text = "FIRST MORTGAGE";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label26.Location = new System.Drawing.Point(2, 173);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(321, 32);
            this.label26.TabIndex = 117;
            this.label26.Text = "SECOND MORTGAGE";
            // 
            // labelDeedType
            // 
            this.labelDeedType.AutoSize = true;
            this.labelDeedType.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDeedType.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.labelDeedType.Location = new System.Drawing.Point(15, 56);
            this.labelDeedType.Name = "labelDeedType";
            this.labelDeedType.Size = new System.Drawing.Size(153, 32);
            this.labelDeedType.TabIndex = 61;
            this.labelDeedType.Text = "Deed Type";
            // 
            // labelRecordingDate
            // 
            this.labelRecordingDate.AutoSize = true;
            this.labelRecordingDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelRecordingDate.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.labelRecordingDate.Location = new System.Drawing.Point(135, 56);
            this.labelRecordingDate.Name = "labelRecordingDate";
            this.labelRecordingDate.Size = new System.Drawing.Size(212, 32);
            this.labelRecordingDate.TabIndex = 65;
            this.labelRecordingDate.Text = "Recording Date";
            // 
            // labelSellerName
            // 
            this.labelSellerName.AutoSize = true;
            this.labelSellerName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSellerName.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.labelSellerName.Location = new System.Drawing.Point(673, 16);
            this.labelSellerName.Name = "labelSellerName";
            this.labelSellerName.Size = new System.Drawing.Size(171, 32);
            this.labelSellerName.TabIndex = 93;
            this.labelSellerName.Text = "Seller Name";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label15.Location = new System.Drawing.Point(571, 16);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(184, 32);
            this.label15.TabIndex = 103;
            this.label15.Text = "Sale Type (2)";
            // 
            // labelSaleType1
            // 
            this.labelSaleType1.AutoSize = true;
            this.labelSaleType1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSaleType1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.labelSaleType1.Location = new System.Drawing.Point(477, 16);
            this.labelSaleType1.Name = "labelSaleType1";
            this.labelSaleType1.Size = new System.Drawing.Size(184, 32);
            this.labelSaleType1.TabIndex = 59;
            this.labelSaleType1.Text = "Sale Type (1)";
            // 
            // labelSaleDate
            // 
            this.labelSaleDate.AutoSize = true;
            this.labelSaleDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSaleDate.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.labelSaleDate.Location = new System.Drawing.Point(135, 16);
            this.labelSaleDate.Name = "labelSaleDate";
            this.labelSaleDate.Size = new System.Drawing.Size(149, 32);
            this.labelSaleDate.TabIndex = 55;
            this.labelSaleDate.Text = "Sale Date";
            // 
            // labelCashDown
            // 
            this.labelCashDown.AutoSize = true;
            this.labelCashDown.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCashDown.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.labelCashDown.Location = new System.Drawing.Point(236, 16);
            this.labelCashDown.Name = "labelCashDown";
            this.labelCashDown.Size = new System.Drawing.Size(160, 32);
            this.labelCashDown.TabIndex = 85;
            this.labelCashDown.Text = "Cash Down";
            // 
            // labelPricePerSqFt
            // 
            this.labelPricePerSqFt.AutoSize = true;
            this.labelPricePerSqFt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPricePerSqFt.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.labelPricePerSqFt.Location = new System.Drawing.Point(365, 16);
            this.labelPricePerSqFt.Name = "labelPricePerSqFt";
            this.labelPricePerSqFt.Size = new System.Drawing.Size(211, 32);
            this.labelPricePerSqFt.TabIndex = 87;
            this.labelPricePerSqFt.Text = "Price per Sq.Ft.";
            // 
            // label18
            // 
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(2, 28);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(16, 24);
            this.label18.TabIndex = 107;
            this.label18.Text = "$";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label22.Location = new System.Drawing.Point(365, 56);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(154, 32);
            this.label22.TabIndex = 113;
            this.label22.Text = "Stamp Amt";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label23.Location = new System.Drawing.Point(477, 56);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(198, 32);
            this.label23.TabIndex = 114;
            this.label23.Text = "Title Company";
            // 
            // pictureBoxFinancials
            // 
            this.pictureBoxFinancials.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBoxFinancials.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxFinancials.Image")));
            this.pictureBoxFinancials.Location = new System.Drawing.Point(951, 8);
            this.pictureBoxFinancials.Name = "pictureBoxFinancials";
            this.pictureBoxFinancials.Size = new System.Drawing.Size(16, 16);
            this.pictureBoxFinancials.TabIndex = 168;
            this.pictureBoxFinancials.TabStop = false;
            // 
            // tabPageTaxesNLegal
            // 
            this.tabPageTaxesNLegal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.tabPageTaxesNLegal.Controls.Add(this.groupBox6);
            this.tabPageTaxesNLegal.Controls.Add(this.groupBox5);
            this.tabPageTaxesNLegal.Controls.Add(this.pictureBoxTaxesLegal);
            this.tabPageTaxesNLegal.Location = new System.Drawing.Point(10, 28);
            this.tabPageTaxesNLegal.Name = "tabPageTaxesNLegal";
            this.tabPageTaxesNLegal.Size = new System.Drawing.Size(975, 516);
            this.tabPageTaxesNLegal.TabIndex = 0;
            this.tabPageTaxesNLegal.Text = "Taxes & Legal";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.textBoxLegalDescription);
            this.groupBox6.Controls.Add(this.textBoxCensusTract);
            this.groupBox6.Controls.Add(this.textBoxTownshipName);
            this.groupBox6.Controls.Add(this.textBoxLegalBlockBldg);
            this.groupBox6.Controls.Add(this.textBoxLegalBookPage);
            this.groupBox6.Controls.Add(this.labelLegalDescription);
            this.groupBox6.Controls.Add(this.labelTownshipName);
            this.groupBox6.Controls.Add(this.label62);
            this.groupBox6.Controls.Add(this.label60);
            this.groupBox6.Controls.Add(this.label61);
            this.groupBox6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox6.Location = new System.Drawing.Point(9, 174);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(732, 178);
            this.groupBox6.TabIndex = 119;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "LEGAL DESCRIPTION";
            // 
            // textBoxLegalDescription
            // 
            this.textBoxLegalDescription.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxLegalDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxLegalDescription.Location = new System.Drawing.Point(10, 32);
            this.textBoxLegalDescription.Multiline = true;
            this.textBoxLegalDescription.Name = "textBoxLegalDescription";
            this.textBoxLegalDescription.Size = new System.Drawing.Size(716, 88);
            this.textBoxLegalDescription.TabIndex = 13;
            // 
            // textBoxCensusTract
            // 
            this.textBoxCensusTract.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxCensusTract.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxCensusTract.Location = new System.Drawing.Point(394, 144);
            this.textBoxCensusTract.Name = "textBoxCensusTract";
            this.textBoxCensusTract.Size = new System.Drawing.Size(104, 39);
            this.textBoxCensusTract.TabIndex = 17;
            // 
            // textBoxTownshipName
            // 
            this.textBoxTownshipName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxTownshipName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxTownshipName.Location = new System.Drawing.Point(266, 144);
            this.textBoxTownshipName.Name = "textBoxTownshipName";
            this.textBoxTownshipName.Size = new System.Drawing.Size(120, 39);
            this.textBoxTownshipName.TabIndex = 16;
            // 
            // textBoxLegalBlockBldg
            // 
            this.textBoxLegalBlockBldg.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxLegalBlockBldg.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxLegalBlockBldg.Location = new System.Drawing.Point(10, 144);
            this.textBoxLegalBlockBldg.Name = "textBoxLegalBlockBldg";
            this.textBoxLegalBlockBldg.Size = new System.Drawing.Size(120, 39);
            this.textBoxLegalBlockBldg.TabIndex = 14;
            // 
            // textBoxLegalBookPage
            // 
            this.textBoxLegalBookPage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxLegalBookPage.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxLegalBookPage.Location = new System.Drawing.Point(138, 144);
            this.textBoxLegalBookPage.Name = "textBoxLegalBookPage";
            this.textBoxLegalBookPage.Size = new System.Drawing.Size(120, 39);
            this.textBoxLegalBookPage.TabIndex = 15;
            // 
            // labelLegalDescription
            // 
            this.labelLegalDescription.AutoSize = true;
            this.labelLegalDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelLegalDescription.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.labelLegalDescription.Location = new System.Drawing.Point(7, 16);
            this.labelLegalDescription.Name = "labelLegalDescription";
            this.labelLegalDescription.Size = new System.Drawing.Size(236, 32);
            this.labelLegalDescription.TabIndex = 97;
            this.labelLegalDescription.Text = "Legal Description";
            // 
            // labelTownshipName
            // 
            this.labelTownshipName.AutoSize = true;
            this.labelTownshipName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTownshipName.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.labelTownshipName.Location = new System.Drawing.Point(263, 128);
            this.labelTownshipName.Name = "labelTownshipName";
            this.labelTownshipName.Size = new System.Drawing.Size(219, 32);
            this.labelTownshipName.TabIndex = 99;
            this.labelTownshipName.Text = "Township Name";
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label62.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label62.Location = new System.Drawing.Point(391, 128);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(182, 32);
            this.label62.TabIndex = 113;
            this.label62.Text = "Census Tract";
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label60.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label60.Location = new System.Drawing.Point(135, 128);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(233, 32);
            this.label60.TabIndex = 109;
            this.label60.Text = "Legal Book/Page";
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label61.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label61.Location = new System.Drawing.Point(7, 128);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(229, 32);
            this.label61.TabIndex = 111;
            this.label61.Text = "Legal Block/Bldg";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.textBoxLandValue);
            this.groupBox5.Controls.Add(this.label87);
            this.groupBox5.Controls.Add(this.label86);
            this.groupBox5.Controls.Add(this.textBoxLandUseType);
            this.groupBox5.Controls.Add(this.textBoxImprovementValue);
            this.groupBox5.Controls.Add(this.textBoxTaxArea);
            this.groupBox5.Controls.Add(this.textBoxTaxYear);
            this.groupBox5.Controls.Add(this.textBoxExemptionType);
            this.groupBox5.Controls.Add(this.textBoxDelinquentYear);
            this.groupBox5.Controls.Add(this.textBoxPropertyTax);
            this.groupBox5.Controls.Add(this.textBoxAPN);
            this.groupBox5.Controls.Add(this.textBoxAlternateAPN);
            this.groupBox5.Controls.Add(this.textBoxTotalAssesedValue);
            this.groupBox5.Controls.Add(this.textBoxAssesmentYear);
            this.groupBox5.Controls.Add(this.label59);
            this.groupBox5.Controls.Add(this.label58);
            this.groupBox5.Controls.Add(this.labelAPN);
            this.groupBox5.Controls.Add(this.labelAlternateAPN);
            this.groupBox5.Controls.Add(this.labelTotalAssesedValue);
            this.groupBox5.Controls.Add(this.labelAssesmentYear);
            this.groupBox5.Controls.Add(this.labelPropertyTax);
            this.groupBox5.Controls.Add(this.labelTaxArea);
            this.groupBox5.Controls.Add(this.labelTaxYear);
            this.groupBox5.Controls.Add(this.labelExemptionType);
            this.groupBox5.Controls.Add(this.labelLandUseType);
            this.groupBox5.Controls.Add(this.labelLandValue);
            this.groupBox5.Controls.Add(this.labelImprovementValue);
            this.groupBox5.Controls.Add(this.labelDelinquentYear);
            this.groupBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox5.Location = new System.Drawing.Point(9, 8);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(732, 160);
            this.groupBox5.TabIndex = 118;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "TAX INFO";
            // 
            // textBoxLandValue
            // 
            this.textBoxLandValue.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxLandValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxLandValue.Location = new System.Drawing.Point(135, 131);
            this.textBoxLandValue.Name = "textBoxLandValue";
            this.textBoxLandValue.Size = new System.Drawing.Size(88, 39);
            this.textBoxLandValue.TabIndex = 11;
            // 
            // label87
            // 
            this.label87.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label87.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label87.Location = new System.Drawing.Point(120, 127);
            this.label87.Name = "label87";
            this.label87.Size = new System.Drawing.Size(16, 24);
            this.label87.TabIndex = 116;
            this.label87.Text = "$";
            this.label87.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label86
            // 
            this.label86.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label86.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label86.Location = new System.Drawing.Point(229, 127);
            this.label86.Name = "label86";
            this.label86.Size = new System.Drawing.Size(16, 24);
            this.label86.TabIndex = 115;
            this.label86.Text = "$";
            this.label86.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxLandUseType
            // 
            this.textBoxLandUseType.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxLandUseType.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxLandUseType.Location = new System.Drawing.Point(13, 131);
            this.textBoxLandUseType.Name = "textBoxLandUseType";
            this.textBoxLandUseType.Size = new System.Drawing.Size(100, 39);
            this.textBoxLandUseType.TabIndex = 10;
            // 
            // textBoxImprovementValue
            // 
            this.textBoxImprovementValue.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxImprovementValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxImprovementValue.Location = new System.Drawing.Point(246, 131);
            this.textBoxImprovementValue.Name = "textBoxImprovementValue";
            this.textBoxImprovementValue.Size = new System.Drawing.Size(125, 39);
            this.textBoxImprovementValue.TabIndex = 12;
            // 
            // textBoxTaxArea
            // 
            this.textBoxTaxArea.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxTaxArea.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxTaxArea.Location = new System.Drawing.Point(13, 83);
            this.textBoxTaxArea.Name = "textBoxTaxArea";
            this.textBoxTaxArea.Size = new System.Drawing.Size(104, 39);
            this.textBoxTaxArea.TabIndex = 6;
            // 
            // textBoxTaxYear
            // 
            this.textBoxTaxYear.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxTaxYear.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxTaxYear.Location = new System.Drawing.Point(123, 83);
            this.textBoxTaxYear.Name = "textBoxTaxYear";
            this.textBoxTaxYear.Size = new System.Drawing.Size(100, 39);
            this.textBoxTaxYear.TabIndex = 7;
            // 
            // textBoxExemptionType
            // 
            this.textBoxExemptionType.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxExemptionType.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxExemptionType.Location = new System.Drawing.Point(232, 83);
            this.textBoxExemptionType.Name = "textBoxExemptionType";
            this.textBoxExemptionType.Size = new System.Drawing.Size(139, 39);
            this.textBoxExemptionType.TabIndex = 8;
            // 
            // textBoxDelinquentYear
            // 
            this.textBoxDelinquentYear.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxDelinquentYear.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxDelinquentYear.Location = new System.Drawing.Point(394, 83);
            this.textBoxDelinquentYear.Name = "textBoxDelinquentYear";
            this.textBoxDelinquentYear.Size = new System.Drawing.Size(100, 39);
            this.textBoxDelinquentYear.TabIndex = 9;
            // 
            // textBoxPropertyTax
            // 
            this.textBoxPropertyTax.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxPropertyTax.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxPropertyTax.Location = new System.Drawing.Point(394, 35);
            this.textBoxPropertyTax.Name = "textBoxPropertyTax";
            this.textBoxPropertyTax.Size = new System.Drawing.Size(88, 39);
            this.textBoxPropertyTax.TabIndex = 4;
            // 
            // textBoxAPN
            // 
            this.textBoxAPN.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxAPN.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxAPN.Location = new System.Drawing.Point(13, 35);
            this.textBoxAPN.Name = "textBoxAPN";
            this.textBoxAPN.Size = new System.Drawing.Size(104, 39);
            this.textBoxAPN.TabIndex = 1;
            // 
            // textBoxAlternateAPN
            // 
            this.textBoxAlternateAPN.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxAlternateAPN.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxAlternateAPN.Location = new System.Drawing.Point(123, 35);
            this.textBoxAlternateAPN.Name = "textBoxAlternateAPN";
            this.textBoxAlternateAPN.Size = new System.Drawing.Size(100, 39);
            this.textBoxAlternateAPN.TabIndex = 2;
            // 
            // textBoxTotalAssesedValue
            // 
            this.textBoxTotalAssesedValue.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxTotalAssesedValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxTotalAssesedValue.Location = new System.Drawing.Point(246, 35);
            this.textBoxTotalAssesedValue.Name = "textBoxTotalAssesedValue";
            this.textBoxTotalAssesedValue.Size = new System.Drawing.Size(125, 39);
            this.textBoxTotalAssesedValue.TabIndex = 3;
            // 
            // textBoxAssesmentYear
            // 
            this.textBoxAssesmentYear.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxAssesmentYear.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxAssesmentYear.Location = new System.Drawing.Point(488, 35);
            this.textBoxAssesmentYear.Name = "textBoxAssesmentYear";
            this.textBoxAssesmentYear.Size = new System.Drawing.Size(88, 39);
            this.textBoxAssesmentYear.TabIndex = 5;
            // 
            // label59
            // 
            this.label59.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label59.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label59.Location = new System.Drawing.Point(377, 31);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(16, 24);
            this.label59.TabIndex = 108;
            this.label59.Text = "$";
            this.label59.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label58
            // 
            this.label58.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label58.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label58.Location = new System.Drawing.Point(229, 31);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(16, 24);
            this.label58.TabIndex = 107;
            this.label58.Text = "$";
            this.label58.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelAPN
            // 
            this.labelAPN.AutoSize = true;
            this.labelAPN.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelAPN.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.labelAPN.Location = new System.Drawing.Point(10, 19);
            this.labelAPN.Name = "labelAPN";
            this.labelAPN.Size = new System.Drawing.Size(73, 32);
            this.labelAPN.TabIndex = 71;
            this.labelAPN.Text = "APN";
            // 
            // labelAlternateAPN
            // 
            this.labelAlternateAPN.AutoSize = true;
            this.labelAlternateAPN.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelAlternateAPN.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.labelAlternateAPN.Location = new System.Drawing.Point(120, 19);
            this.labelAlternateAPN.Name = "labelAlternateAPN";
            this.labelAlternateAPN.Size = new System.Drawing.Size(195, 32);
            this.labelAlternateAPN.TabIndex = 73;
            this.labelAlternateAPN.Text = "Alternate APN";
            // 
            // labelTotalAssesedValue
            // 
            this.labelTotalAssesedValue.AutoSize = true;
            this.labelTotalAssesedValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTotalAssesedValue.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.labelTotalAssesedValue.Location = new System.Drawing.Point(243, 19);
            this.labelTotalAssesedValue.Name = "labelTotalAssesedValue";
            this.labelTotalAssesedValue.Size = new System.Drawing.Size(276, 32);
            this.labelTotalAssesedValue.TabIndex = 75;
            this.labelTotalAssesedValue.Text = "Total Assesed Value";
            // 
            // labelAssesmentYear
            // 
            this.labelAssesmentYear.AutoSize = true;
            this.labelAssesmentYear.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelAssesmentYear.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.labelAssesmentYear.Location = new System.Drawing.Point(485, 19);
            this.labelAssesmentYear.Name = "labelAssesmentYear";
            this.labelAssesmentYear.Size = new System.Drawing.Size(222, 32);
            this.labelAssesmentYear.TabIndex = 77;
            this.labelAssesmentYear.Text = "Assesment Year";
            // 
            // labelPropertyTax
            // 
            this.labelPropertyTax.AutoSize = true;
            this.labelPropertyTax.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPropertyTax.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.labelPropertyTax.Location = new System.Drawing.Point(391, 18);
            this.labelPropertyTax.Name = "labelPropertyTax";
            this.labelPropertyTax.Size = new System.Drawing.Size(176, 32);
            this.labelPropertyTax.TabIndex = 79;
            this.labelPropertyTax.Text = "Property Tax";
            // 
            // labelTaxArea
            // 
            this.labelTaxArea.AutoSize = true;
            this.labelTaxArea.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTaxArea.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.labelTaxArea.Location = new System.Drawing.Point(10, 67);
            this.labelTaxArea.Name = "labelTaxArea";
            this.labelTaxArea.Size = new System.Drawing.Size(129, 32);
            this.labelTaxArea.TabIndex = 81;
            this.labelTaxArea.Text = "Tax Area";
            // 
            // labelTaxYear
            // 
            this.labelTaxYear.AutoSize = true;
            this.labelTaxYear.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTaxYear.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.labelTaxYear.Location = new System.Drawing.Point(120, 67);
            this.labelTaxYear.Name = "labelTaxYear";
            this.labelTaxYear.Size = new System.Drawing.Size(129, 32);
            this.labelTaxYear.TabIndex = 83;
            this.labelTaxYear.Text = "Tax Year";
            // 
            // labelExemptionType
            // 
            this.labelExemptionType.AutoSize = true;
            this.labelExemptionType.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelExemptionType.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.labelExemptionType.Location = new System.Drawing.Point(229, 67);
            this.labelExemptionType.Name = "labelExemptionType";
            this.labelExemptionType.Size = new System.Drawing.Size(220, 32);
            this.labelExemptionType.TabIndex = 85;
            this.labelExemptionType.Text = "Exemption Type";
            // 
            // labelLandUseType
            // 
            this.labelLandUseType.AutoSize = true;
            this.labelLandUseType.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelLandUseType.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.labelLandUseType.Location = new System.Drawing.Point(10, 115);
            this.labelLandUseType.Name = "labelLandUseType";
            this.labelLandUseType.Size = new System.Drawing.Size(136, 32);
            this.labelLandUseType.TabIndex = 89;
            this.labelLandUseType.Text = "Land Use";
            // 
            // labelLandValue
            // 
            this.labelLandValue.AutoSize = true;
            this.labelLandValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelLandValue.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.labelLandValue.Location = new System.Drawing.Point(132, 115);
            this.labelLandValue.Name = "labelLandValue";
            this.labelLandValue.Size = new System.Drawing.Size(160, 32);
            this.labelLandValue.TabIndex = 91;
            this.labelLandValue.Text = "Land Value";
            // 
            // labelImprovementValue
            // 
            this.labelImprovementValue.AutoSize = true;
            this.labelImprovementValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelImprovementValue.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.labelImprovementValue.Location = new System.Drawing.Point(243, 115);
            this.labelImprovementValue.Name = "labelImprovementValue";
            this.labelImprovementValue.Size = new System.Drawing.Size(260, 32);
            this.labelImprovementValue.TabIndex = 93;
            this.labelImprovementValue.Text = "Improvement Value";
            // 
            // labelDelinquentYear
            // 
            this.labelDelinquentYear.AutoSize = true;
            this.labelDelinquentYear.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDelinquentYear.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.labelDelinquentYear.Location = new System.Drawing.Point(391, 67);
            this.labelDelinquentYear.Name = "labelDelinquentYear";
            this.labelDelinquentYear.Size = new System.Drawing.Size(220, 32);
            this.labelDelinquentYear.TabIndex = 95;
            this.labelDelinquentYear.Text = "Delinquent Year";
            // 
            // pictureBoxTaxesLegal
            // 
            this.pictureBoxTaxesLegal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBoxTaxesLegal.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxTaxesLegal.Image")));
            this.pictureBoxTaxesLegal.Location = new System.Drawing.Point(951, 8);
            this.pictureBoxTaxesLegal.Name = "pictureBoxTaxesLegal";
            this.pictureBoxTaxesLegal.Size = new System.Drawing.Size(16, 16);
            this.pictureBoxTaxesLegal.TabIndex = 117;
            this.pictureBoxTaxesLegal.TabStop = false;
            // 
            // line1
            // 
            this.line1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.line1.Location = new System.Drawing.Point(30, -8);
            this.line1.Margin = new System.Windows.Forms.Padding(4);
            this.line1.Name = "line1";
            this.line1.Size = new System.Drawing.Size(869, 12);
            this.line1.TabIndex = 151;
            // 
            // UserControlPropertyProfileComps
            // 
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.Controls.Add(this.tabControlOnlinePropertyProfileNComps);
            this.Name = "UserControlPropertyProfileComps";
            this.Size = new System.Drawing.Size(995, 554);
            this.tabPagePropertyCharacteristics.ResumeLayout(false);
            this.tabPagePropertyCharacteristics.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPropCharact)).EndInit();
            this.tabPageOwnershipNLocations.ResumeLayout(false);
            this.tabPageOwnershipNLocations.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.tabControlOnlinePropertyProfileNComps.ResumeLayout(false);
            this.tabPageFinancials.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxFinancials)).EndInit();
            this.tabPageTaxesNLegal.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxTaxesLegal)).EndInit();
            this.ResumeLayout(false);

        }
        #endregion

        #region CTRL INTERNALS
        public void clearForm()
		{
			foreach (object obj in this.Controls)
			{
				if (obj.GetType() == typeof(TextBox))
					((TextBox)obj).Text = "";                
			}
			foreach (TabPage tbp in this.tabControlOnlinePropertyProfileNComps.TabPages)
			{
				foreach (object obj in tbp.Controls)
				{
					if (obj.GetType() == typeof(TextBox))
						((TextBox)obj).Text = "";
                    if (obj.GetType() == typeof(Panel))
                    {
                        foreach (object obj2 in ((Panel)obj).Controls)
                        {
                            if (obj2.GetType() == typeof(TextBox))
                                ((TextBox)obj2).Text = "";

                            if (obj2.GetType() == typeof(GroupBox))
                            {
                                foreach (object o in ((GroupBox)obj2).Controls)
                                {
                                    if (o.GetType() == typeof(TextBox))
                                        ((TextBox)o).Text = "";
                                }
                            }
                        }                        
                    }
                    if (obj.GetType() == typeof(GroupBox))
                    {
                        foreach (object obj2 in ((GroupBox)obj).Controls)
                        {
                            if (obj2.GetType() == typeof(TextBox))
                                ((TextBox)obj2).Text = "";                            
                        }
                    }
                }
			}
			dbPropertyProfile = null;
			_PPInDb = false;
		}
				
		public void setInitialData(Guid propertyItemId)
		{
			clearForm();

            DealMaker.PlainClasses.OnLineData.PropertyProfile.PropertyProfile[] pps = PPManager.Instance.Select(propertyItemId);
            if (null != pps && 0 < pps.Length)
                dbPropertyProfile = new DealMaker.PlainClasses.OnLineData.PropertyProfile.PropertyProfile[1] { pps[0] };
            else
                return;

            if (null == dbPropertyProfile[0].P_xmlStream)
                return;
		
			MemoryStream stream = new MemoryStream(dbPropertyProfile[0].P_xmlStream);
			XmlSerializer serializer = new XmlSerializer(typeof(DTD.Response.RESPONSE_GROUP));
			response = (DTD.Response.RESPONSE_GROUP)serializer.Deserialize(stream);

#if DUMP_XML_OBJECT
            dumpObject(response.RESPONSE, "XML_RESPONSE");
#endif

            if (CheckValid())
			{
				//property = response.RESPONSE.RESPONSE_DATA[0].PROPERTY_INFORMATION_RESPONSE._PROPERTY_INFORMATION[0].PROPERTY;

				//NEW dtd 4.2.2.
				//BEGIN
				DTD.Response._PROPERTY_INFORMATION propInfo = null;				
				try
				{
					//propInfo = response.RESPONSE.RESPONSE_DATA[0].PROPERTY_INFORMATION_RESPONSE.Items[0] as DTD.Response._PROPERTY_INFORMATION;
					foreach (object o in response.RESPONSE.RESPONSE_DATA[0].PROPERTY_INFORMATION_RESPONSE.Items)
					{
						if (o is DTD.Response._PROPERTY_INFORMATION)
						{
							propInfo = o as DTD.Response._PROPERTY_INFORMATION;
							break;
						}
					}
				}
				catch 
				{
					propInfo = null;
				}
				if (null != propInfo && null != propInfo.PROPERTY)
					property = propInfo.PROPERTY;					
				//END
			}
			fillComps(property,"0","0");
			_PPInDb=true;
			
		}	
				
		public void prepareComparable()
		{
			//120 DPI
			panel3.Top = 5;
			//120 DPI
			panel2.Top = panel3.Bottom;
			panel1.Visible=false;
			panel3.Visible = true;
			//labelLine.Visible=false;
            _updateSqFt.Enabled = true;
		}
				
		public void fillComps(DTD.Response.PROPERTY _property, string _comparableNumber, string _distanceFromSubject)
		{
			this.property = _property;
			this.comparableNumber = _comparableNumber;
			this.distanceFromSubject = _distanceFromSubject;

			fillData();
		}
				
		private void fillData()
		{
			try{textBoxOwnerName.Text = property.PROPERTY_OWNER[0]._OwnerName;}
			catch{}
			try{textBoxSchoolDistrict.Text = property._NEIGHBORHOOD_AREA_INFORMATION._SchoolDistrictName;}
			catch{}
			try{textBoxLotNumber.Text = property._LotIdentifier;}
			catch{}
			try{textBoxSubdivision.Text = property._SubdivisionIdentifier;}
			catch{}
			try{textBoxComparableNumber.Text = comparableNumber;}
			catch{}
			try{textBoxDistanceFromSubject.Text = distanceFromSubject;}
			catch{}
			try{textBoxCounty.Text = property._County;}
			catch{}
			try{textBoxPostDirection.Text = property._PARSED_STREET_ADDRESS._DirectionSuffix;}
			catch{}
			try{textBoxMailingZipCode.Text = property.PROPERTY_OWNER[0]._MailingPostalCode;}
			catch{}
			try{textBoxMailingCityAndState.Text = property.PROPERTY_OWNER[0]._MailingCityAndState;}
			catch{}
			try{textBoxMailingAddress.Text = property.PROPERTY_OWNER[0]._MailingAddress;}
			catch{}
			try{textBoxMapReferenceTwo.Text = property._NEIGHBORHOOD_AREA_INFORMATION._MapReferenceTwoIdentifier;}
			catch{}
			try{textBoxMapReferenceOne.Text = property._NEIGHBORHOOD_AREA_INFORMATION._MapReferenceOneIdentifier;}
			catch{}
			try{textBoxZipCode.Text = property._PostalCode;}
			catch{}
			try{textBoxState.Text = property._State;}
			catch{}
			try{textBoxCity.Text = property._City;}
			catch{}
			try{textBoxUnitNumber.Text = property._PARSED_STREET_ADDRESS._ApartmentOrUnit;}
			catch{}
			try{textBoxSuffix.Text = property._PARSED_STREET_ADDRESS._StreetSuffix;}
			catch{}
			try{textBoxStreetnName.Text = property._PARSED_STREET_ADDRESS._StreetName;}
			catch{}
			try{textBoxPreDirection.Text = property._PARSED_STREET_ADDRESS._DirectionPrefix;}
			catch{}
			try{textBoxHouseNumber.Text = property._PARSED_STREET_ADDRESS._HouseNumber;}
			catch{}
			try{textBoxTotalBaths.Text = property._PROPERTY_CHARACTERISTICS._IMPROVEMENTS._ROOM_COUNT._TotalBathsCount;}
			catch{}
			try{textBoxTotalBedrooms.Text = property._PROPERTY_CHARACTERISTICS._IMPROVEMENTS._ROOM_COUNT._TotalBedroomsCount;}
			catch{}
			try{textBoxTotalRooms.Text = property._PROPERTY_CHARACTERISTICS._IMPROVEMENTS._ROOM_COUNT._TotalRoomCount;}
			catch{}
			try{textBoxZoning.Text = property._PROPERTY_CHARACTERISTICS._SITE._ZONING._ClassificationIdentifier;}
			catch{}
			try{textBoxNumberOfStories.Text = property._PROPERTY_CHARACTERISTICS._IMPROVEMENTS._GENERAL_DESCRIPTION._TotalStoriesNumber;}
			catch{}
			try{textBoxFireplaces.Text = property._PROPERTY_CHARACTERISTICS._IMPROVEMENTS._FEATURES._FIREPLACES._HasFeatureIndicator;}
			catch{}
			try{textBoxAirConditioning.Text = property._PROPERTY_CHARACTERISTICS._IMPROVEMENTS._COOLING._CentralizedIndicator;}
			catch{}
			try{textBoxPool.Text = property._PROPERTY_CHARACTERISTICS._IMPROVEMENTS._FEATURES._POOL._HasFeatureIndicator;}
			catch{}
			try{textBoxRoofType.Text = property._PROPERTY_CHARACTERISTICS._IMPROVEMENTS._EXTERIOR_DESCRIPTION._RoofTypeDescription;}
			catch{}
			try{textBoxParkingCapacity.Text = property._PROPERTY_CHARACTERISTICS._IMPROVEMENTS._PARKING._GarageTotalCarCount;}
			catch{}
			try{textBoxParkingType.Text = property._PROPERTY_CHARACTERISTICS._IMPROVEMENTS._PARKING._TypeDescription;}
			catch{}
			try{textBoxLotSize.Text = getDecimal(property._PROPERTY_CHARACTERISTICS._SITE._DIMENSIONS._LotAreaAcresNumber);}
			catch{}
			try{
                //textBoxLotArea.Text = getDecimal(property._PROPERTY_CHARACTERISTICS._SITE._DIMENSIONS._LotAreaSquareFeetNumber);
                textBoxLotArea.Text = getSemiformattedNumber(getSecureString(property._PROPERTY_CHARACTERISTICS._SITE._DIMENSIONS._LotAreaSquareFeetNumber));
            }
			catch{}
			try{textBoxYearBuilt.Text = property._PROPERTY_CHARACTERISTICS._IMPROVEMENTS._GENERAL_DESCRIPTION._YearBuiltDateIdentifier;}
			catch{}
			try{textBoxSecInterestRateType.Text = property._PROPERTY_HISTORY._MORTGAGE_HISTORY[0]._SecondMortgageInterestRateTypeDescription;}
			catch{}
			try{textBoxSecInterestRateTerm.Text = property._PROPERTY_HISTORY._MORTGAGE_HISTORY[0]._SecondMortgageTermNumber;}
			catch{}
			try{textBoxSecInterestRate.Text = getDecimal(property._PROPERTY_HISTORY._MORTGAGE_HISTORY[0]._SecondMortgageInterestRatePercent);}
			catch{}
			try{textBoxSecMortgageAmount.Text = getDecimal(property._PROPERTY_HISTORY._MORTGAGE_HISTORY[0]._SecondMortgageAmount);}
			catch{}
			try{textBoxSellerName.Text = property._PROPERTY_HISTORY._SALES_HISTORY[0]._SellerName;}
			catch{}
			try{textBoxPSSalePrice.Text = getDecimal(property._PROPERTY_HISTORY._SALES_HISTORY[0]._PriorSalePriceAmount);}
			catch{}
			try{textBoxPSSaleDate.Text = getDate(property._PROPERTY_HISTORY._SALES_HISTORY[0]._PriorSaleDate/*_LastSalesDate*/);}
			catch{}
			try{textBox4PricePerSqFt.Text = getDecimal(property._PROPERTY_HISTORY._SALES_HISTORY[0]._PricePerSquareFootAmount);}
			catch{}
			try{textBoxCashDown.Text = getDecimal(property._PROPERTY_HISTORY._SALES_HISTORY[0]._CashDownAmount);}
			catch{}
			try{textBoxFirstMortgageLender.Text = property._PROPERTY_HISTORY._SALES_HISTORY[0]._LenderName;}
			catch{}
			try{textBoxType.Text = property._PROPERTY_HISTORY._MORTGAGE_HISTORY[0]._FirstMortgageInterestRateTypeDescription;}
			catch{}
			try{textBoxFirstMortgageInterestRateTerm.Text = property._PROPERTY_HISTORY._MORTGAGE_HISTORY[0]._FirstMortgageTermNumber;}
			catch{}
			try{textBoxFirstMortgageInterestRate.Text = getDecimal(property._PROPERTY_HISTORY._MORTGAGE_HISTORY[0]._FirstMortgageInterestRatePercent);}
			catch{}
			try{textBoxFirstMortgageAmount.Text = getDecimal(property._PROPERTY_HISTORY._MORTGAGE_HISTORY[0]._FirstMortgageAmount);}
			catch{}
			try{textBoxRecordingDate.Text = getDate(property._PROPERTY_HISTORY._SALES_HISTORY[0]._LastRecordingDate);}
			catch{}
			try{textBoxDocumentNumber.Text = property._PROPERTY_HISTORY._SALES_HISTORY[0]._DocumentNumberIdentifier;}
			catch{}
			try{textBoxDeedType.Text = property._PROPERTY_HISTORY._SALES_HISTORY[0]._DeedTypeDescription;}
			catch{}
			try{textBoxSaleType1.Text = property._PROPERTY_HISTORY._SALES_HISTORY[0]._OneSaleTypeDescription;}
			catch{}
			try{textBoxSalePrice.Text = getDecimal(property._PROPERTY_HISTORY._SALES_HISTORY[0]._LastSalesPriceAmount);}
			catch{}
			try{textBoxSaleDate.Text = getDate(property._PROPERTY_HISTORY._SALES_HISTORY[0]._LastSalesDate);}
			catch{}
			try{textBoxTownshipName.Text = property._Municipality;}
			catch{}
			try{textBoxLegalDescription.Text = property._LEGAL_DESCRIPTION._TextDescription;}
			catch{}
			try{textBoxDelinquentYear.Text = property._PROPERTY_TAX._DelinquentYear;}
			catch{}
			try{textBoxImprovementValue.Text = getDecimal(property._PROPERTY_TAX._ImprovementValueAmount);}
			catch{}
			try{textBoxLandValue.Text = getDecimal(property._PROPERTY_TAX._LandValueAmount);}
			catch{}
			try{textBoxLandUse.Text = property._PROPERTY_CHARACTERISTICS._SITE._CHARACTERISTICS._LandUseDescription;}
			catch{}
			try{textBoxLandUseType.Text = property._PROPERTY_CHARACTERISTICS._SITE._CHARACTERISTICS._LandUseDescription;}
			catch{}
			try{textBoxExemptionType.Text = property._PROPERTY_TAX._ExemptionTypeDescription;}
			catch{}
			try{textBoxTaxYear.Text = property._PROPERTY_TAX._TaxYear;}
			catch{}
			try{textBoxTaxArea.Text = property._PROPERTY_TAX._RateAreaIdentifier;}
			catch{}
			try{textBoxPropertyTax.Text = getDecimal(property._PROPERTY_TAX._RealEstateTotalTaxAmount);}
			catch{}
			try{textBoxAssesmentYear.Text = property._PROPERTY_TAX._AssessmentYear;}
			catch{}
			try{textBoxTotalAssesedValue.Text = getDecimal(property._PROPERTY_TAX._TotalAssessedValueAmount);}
			catch{}
			try{textBoxAlternateAPN.Text = property._AssessorsAlternateParcelIdentifier;}
			catch{}
			try{textBoxAPN.Text = property._AssessorsParcelIdentifier;}
			catch{}
			try{textBoxPhoneNumber.Text = property.PROPERTY_OWNER[0]._PhoneNumber;}
			catch{}
			try{textBoxOtherImprovements.Text = property.PROPERTY_ANALYSIS._IMPROVEMENT_ANALYSIS._OtherPropertyImprovementsDescription;}
			catch{}
			try
            {
                //textBoxTotalLivingArea.Text = getDecimal(property._PROPERTY_CHARACTERISTICS._IMPROVEMENTS._ROOM_COUNT._TotalLivingAreaSquareFeetNumber);
                textBoxTotalLivingArea.Text = getSemiformattedNumber(getSecureString(property._PROPERTY_CHARACTERISTICS._IMPROVEMENTS._ROOM_COUNT._TotalLivingAreaSquareFeetNumber));
            }
			catch{}
			//textBoxSaleType2.Text=

			try
			{
				textBoxStreetnName.Text = 
					textBoxHouseNumber.Text + " " 
					+ textBoxPreDirection.Text + " " 
					+ textBoxStreetnName.Text + " "
					+ textBoxPostDirection.Text + " "
					+ textBoxSuffix.Text + " "
					+ textBoxUnitNumber.Text + " ";}
			catch{}
		}
				
		private void buttonSearchA_Click(object sender, System.EventArgs e)
		{
            AccurateSearchLabelEnabled = false;
			
			DialogResult dlg = 
				MessageBox.Show
				(
				"You will incur charge or credit reduction!\nDo you want to continue?"
				, "Warning!"
				, MessageBoxButtons.YesNo
				, MessageBoxIcon.Warning
				);
			if (dlg == DialogResult.No)
			{
				return;
			}
			
			int totalCost = 0;
			int totalAmount = 0;
			string errMsg;
			int monthlyCredits;
			int topUpCredits;

            if (!UserCredits.Instance.TestForEnough(Globals.DataConsumptionReg.KeyCode, TypeOfCharge.COMPS, 1, out totalCost, out totalAmount, out monthlyCredits, out topUpCredits, out errMsg))
            {
                UserCreditForm ucf = new UserCreditForm(DealMaker.Globals.UserRegistration.IDUser, monthlyCredits, topUpCredits, totalAmount, TypeOfCharge.COMPS, 1, totalCost);
                ucf.ShowDialog();
                return;
            }

			/*string log_filename =
				@".\PropertyProfile_usage_trace.log";
			TextWriter txtwrtr = new StreamWriter(log_filename,
				true, Encoding.UTF8);
			txtwrtr.WriteLine
				(
				DateTime.Now.ToString("yyyy.MM.dd HH:mm:ss")
				+ " " +
				"Property Profile Requested"
				);
			txtwrtr.Flush();
			txtwrtr.Close();*/

			DTD.Request._PROPERTY_CRITERIA pc 
				= new DTD.Request._PROPERTY_CRITERIA();

			DTD.Request.PARSED_STREET_ADDRESS psa 
				= new DTD.Request.PARSED_STREET_ADDRESS();

			psa._HouseNumber = textBoxStreetNoA.Text;
			psa._DirectionPrefix = textBoxPreDirA.Text;
			psa._StreetName = textBoxStreetA.Text;
			psa._DirectionSuffix =textBoxPostDirA.Text;
			psa._StreetSuffix = textBoxSuffixA.Text;
			psa._ApartmentOrUnit = textBoxUnitNoA.Text;

			pc.PARSED_STREET_ADDRESS = psa;
			pc._PostalCode = textBoxZipA.Text;
			pc._County = textBoxCountyA.Text;
			pc._State = textBoxStateA.Text;

			getSubject(null,pc);
		}
				
		private void buttonSearchB_Click(object sender, System.EventArgs e)
		{
            AccurateSearchLabelEnabled = false;

			DialogResult dlg = 
				MessageBox.Show
				(
				"You will incur charge or credit reduction!\nDo you want to continue?"
				, "Warning!"
				, MessageBoxButtons.YesNo
				, MessageBoxIcon.Warning
				);
			if (dlg == DialogResult.No)
			{
				return;
			}

			int totalCost = 0;
			int totalAmount = 0;
			string errMsg;
			int monthlyCredits;
			int topUpCredits;

            if (!UserCredits.Instance.TestForEnough(Globals.DataConsumptionReg.KeyCode, TypeOfCharge.COMPS, 1, out totalCost, out totalAmount, out monthlyCredits, out topUpCredits, out errMsg))
            {
                UserCreditForm ucf = new UserCreditForm(DealMaker.Globals.UserRegistration.IDUser, monthlyCredits, topUpCredits, totalAmount, TypeOfCharge.COMPS, 1, totalCost);
                ucf.ShowDialog();
                return;
            }

			/*string log_filename =
				@".\PropertyProfile_usage_trace.log";
			TextWriter txtwrtr = new StreamWriter(log_filename,
				true, Encoding.UTF8);
			txtwrtr.WriteLine
				(
				DateTime.Now.ToString("yyyy.MM.dd HH:mm:ss")
				+ " " +
				"Property Profile Requested"
				);
			txtwrtr.Flush();
			txtwrtr.Close();*/

			DTD.Request._PROPERTY_CRITERIA pc 
				= new DTD.Request._PROPERTY_CRITERIA();

			DTD.Request._SUBJECT_SEARCH ss 
				= new DTD.Request._SUBJECT_SEARCH();

		
			ss._AssessorsParcelIdentifier = textBoxApnB.Text;
			pc._PostalCode = textBoxZipB.Text;
			pc._County = textBoxCountyB.Text;
			pc._State = textBoxStateB.Text;

			getSubject(ss, pc);
		}
				
		private void buttonSearchC_Click(object sender, System.EventArgs e)
		{
            AccurateSearchLabelEnabled = false;
			
			DialogResult dlg = 
				MessageBox.Show
				(
				"You will incur charge or credit reduction!\nDo you want to continue?"
				, "Warning!"
				, MessageBoxButtons.YesNo
				, MessageBoxIcon.Warning
				);
			if (dlg == DialogResult.No)
			{
				return;
			}

			int totalCost = 0;
			int totalAmount = 0;
			string errMsg;
			int monthlyCredits;
			int topUpCredits;

            if (!UserCredits.Instance.TestForEnough(Globals.DataConsumptionReg.KeyCode, TypeOfCharge.COMPS, 1, out totalCost, out totalAmount, out monthlyCredits, out topUpCredits, out errMsg))
            {
                UserCreditForm ucf = new UserCreditForm(DealMaker.Globals.UserRegistration.IDUser, monthlyCredits, topUpCredits, totalAmount, TypeOfCharge.COMPS, 1, totalCost);
                ucf.ShowDialog();
                return;
            }

			/*string log_filename =
				@".\PropertyProfile_usage_trace.log";
			TextWriter txtwrtr = new StreamWriter(log_filename,
				true, Encoding.UTF8);
			txtwrtr.WriteLine
				(
				DateTime.Now.ToString("yyyy.MM.dd HH:mm:ss")
				+ " " +
				"Property Profile Requested"
				);
			txtwrtr.Flush();
			txtwrtr.Close();*/
			
			DTD.Request._PROPERTY_CRITERIA pc 
				= new DTD.Request._PROPERTY_CRITERIA();

			DTD.Request._SUBJECT_SEARCH ss 
				= new DTD.Request._SUBJECT_SEARCH();
			
			ss._OwnerFirstName = textBoxFirstNameC.Text;
			ss._OwnerLastName = textBoxLatNameC.Text;
			pc._PostalCode = textBoxZipC.Text;
			pc._County = textBoxCountyC.Text;
			pc._State = textBoxStateC.Text;


			getSubject(ss,pc);
		}

        #region . Build Request 2 .
        private byte[] buildRequestDATA(
            DTD.Request._SUBJECT_SEARCH ss
            , DTD.Request._PROPERTY_CRITERIA pc)
        {
            DTD.Request.REQUESTDATA request = new DTD.Request.REQUESTDATA();

            request.PROPERTY_INFORMATION_REQUEST
                = new DTD.Request.PROPERTY_INFORMATION_REQUEST();
            request.PROPERTY_INFORMATION_REQUEST._SEARCH_CRITERIA
                = new DTD.Request._SEARCH_CRITERIA();

            DTD.Request._CONNECT2DATA_PRODUCT c2dp
                = new DTD.Request._CONNECT2DATA_PRODUCT();

            c2dp._DetailedSubjectReport
                = DTD.Request._CONNECT2DATA_PRODUCT_DetailedSubjectReport.Y;

            request.PROPERTY_INFORMATION_REQUEST._CONNECT2DATA_PRODUCT = c2dp;

            request.PROPERTY_INFORMATION_REQUEST._ActionType
                = DTD.Request.PROPERTY_INFORMATION_REQUEST_ActionType.Submit;
            request.PROPERTY_INFORMATION_REQUEST._ActionTypeSpecified = true;

            request.PROPERTY_INFORMATION_REQUEST._PROPERTY_CRITERIA
                = pc;

            if (null != ss)
            {
                request.PROPERTY_INFORMATION_REQUEST._SEARCH_CRITERIA.Items = new object[1] { ss };
                //= ss;
            }
            else
                request.PROPERTY_INFORMATION_REQUEST._SEARCH_CRITERIA.Items = null;

            DTD.Request._RESPONSE_CRITERIA rescrit
                = new DTD.Request._RESPONSE_CRITERIA();

            rescrit._NumberSubjectPropertiesType
                = DTD.Request._RESPONSE_CRITERIA_NumberSubjectPropertiesType.Item100;

            request.PROPERTY_INFORMATION_REQUEST._RESPONSE_CRITERIA
                = rescrit;

            MemoryStream stream = new MemoryStream();
            XmlSerializer formatter = new XmlSerializer(request.GetType());
            formatter.Serialize(stream, request);
            return stream.ToArray();
        }
        #endregion
				
		private DTD.Request.REQUEST_GROUP buildRequest(DTD.Request._SUBJECT_SEARCH ss, DTD.Request._PROPERTY_CRITERIA pc)
		{
			DTD.Request.REQUEST_GROUP rqg 
				= new DTD.Request.REQUEST_GROUP();


			rqg.REQUEST 
				= new DTD.Request.REQUEST();
			rqg.REQUEST.REQUESTDATA 
				= new DTD.Request.REQUESTDATA[1];
			rqg.REQUEST.REQUESTDATA[0] 
				= new DTD.Request.REQUESTDATA();
			rqg.REQUEST.REQUESTDATA[0].PROPERTY_INFORMATION_REQUEST 
				= new DTD.Request.PROPERTY_INFORMATION_REQUEST();
			rqg.REQUEST.REQUESTDATA[0].PROPERTY_INFORMATION_REQUEST._SEARCH_CRITERIA 
				= new DTD.Request._SEARCH_CRITERIA();

			DTD.Request._CONNECT2DATA_PRODUCT c2dp
				= new DTD.Request._CONNECT2DATA_PRODUCT();

			c2dp._DetailedSubjectReport
				= DTD.Request._CONNECT2DATA_PRODUCT_DetailedSubjectReport.Y;

			rqg.REQUEST.REQUESTDATA[0].PROPERTY_INFORMATION_REQUEST._CONNECT2DATA_PRODUCT = c2dp;
			
			rqg.REQUEST.REQUESTDATA[0].PROPERTY_INFORMATION_REQUEST._ActionType 
				= DTD.Request.PROPERTY_INFORMATION_REQUEST_ActionType.Submit;
			rqg.REQUEST.REQUESTDATA[0].PROPERTY_INFORMATION_REQUEST._ActionTypeSpecified = true;

			rqg.REQUEST.REQUESTDATA[0].PROPERTY_INFORMATION_REQUEST._PROPERTY_CRITERIA
				= pc;
			//rqg.REQUEST.REQUESTDATA[0].PROPERTY_INFORMATION_REQUEST._SEARCH_CRITERIA.Item 
			//	= ss;

			//dtd 4.2.2
			//BEGIN
			//OLD
#if !NEW_DTD
			rqg.REQUEST.REQUESTDATA[0].PROPERTY_INFORMATION_REQUEST._SEARCH_CRITERIA.Item = ss;
#else
			//NEW
			if (null != ss)
				rqg.REQUEST.REQUESTDATA[0].PROPERTY_INFORMATION_REQUEST._SEARCH_CRITERIA.Items = new object[1] { ss };			
			else 
				rqg.REQUEST.REQUESTDATA[0].PROPERTY_INFORMATION_REQUEST._SEARCH_CRITERIA.Items = null;			
#endif
			//END

			rqg.REQUESTING_PARTY 
				= new DTD.Request.REQUESTING_PARTY[1];
			
			rqg.MISMOVersionID = "2.1";

			DTD.Request._RESPONSE_CRITERIA rescrit 
				= new DTD.Request._RESPONSE_CRITERIA();

			rescrit._NumberSubjectPropertiesType 
				= DTD.Request._RESPONSE_CRITERIA_NumberSubjectPropertiesType.Item100;
			
			rqg.REQUEST.REQUESTDATA[0].PROPERTY_INFORMATION_REQUEST._RESPONSE_CRITERIA
				= rescrit;


			/*
			rqg.REQUESTING_PARTY[0] 
				= new DTD.Request.REQUESTING_PARTY();
			
			rqg.REQUESTING_PARTY[0]._Name="Blue Sky Lending"; 
			rqg.REQUESTING_PARTY[0]._StreetAddress="123 Main"; 
			rqg.REQUESTING_PARTY[0]._City="Anaheim"; 
			rqg.REQUESTING_PARTY[0]._State="Ca"; 
			rqg.REQUESTING_PARTY[0]._PostalCode="92840";

			rqg.REQUESTING_PARTY[0].PREFERRED_RESPONSE
				= new DTD.Request.PREFERRED_RESPONSE[1];

			rqg.REQUESTING_PARTY[0].PREFERRED_RESPONSE[0]
				= new DTD.Request.PREFERRED_RESPONSE();

			rqg.REQUESTING_PARTY[0].PREFERRED_RESPONSE[0]._Format="XML";
			rqg.REQUESTING_PARTY[0].PREFERRED_RESPONSE[0]._DestinationDescription="";

			rqg.RECEIVING_PARTY = new DTD.Request.RECEIVING_PARTY();


			rqg.RECEIVING_PARTY._Name="Blue Sky Lending"; 
			rqg.RECEIVING_PARTY._StreetAddress="123 Main";
			rqg.RECEIVING_PARTY._City="Anaheim";
			rqg.RECEIVING_PARTY._State="Ca";
			rqg.RECEIVING_PARTY._PostalCode="92840";
			*/

			

			rqg.REQUEST._JobIdentifier="TEST MASTER V2.0"; 
			rqg.REQUEST._RecordIdentifier="12345A8";
			rqg.REQUEST._HVERequestTypeSpecified = true;
			rqg.REQUEST._HVERequestType 
				= DTD.Request.REQUEST_HVERequestType.Item02;
			rqg.REQUEST._HVCustomerIdentifier="";

			return rqg;
		}

        

        private void getSubject(DTD.Request._SUBJECT_SEARCH ss, DTD.Request._PROPERTY_CRITERIA pc)
		{			
            byte[] request = buildRequestDATA(ss, pc);			            
			XmlSerializer responseSerializer = new XmlSerializer(typeof(DTD.Response.RESPONSE_GROUP));		
			MemoryStream responseStream = new MemoryStream();

			// IMPORTANT  -  we do not need a namespace!
			System.Xml.Serialization.XmlSerializerNamespaces nmspace = new XmlSerializerNamespaces();
			nmspace.Add("","");
			// IMPORTANT  -  we do not need a namespace!
				            
			byte[] requestArray = null;
			byte[] responseArray = null;
			string resultMessage = string.Empty;
			try
			{
				Cursor.Current = Cursors.WaitCursor;
				Application.DoEvents();
                
                //DataConsumption.ServiceDataConsumption sdc;
                sdc = new DealMaker.DataConsumption.ServiceDataConsumption();

                if (string.Empty != Globals.ProfitGrabberWebSupportURLDataConsumption)
                {
                    sdc.Url = Globals.ProfitGrabberWebSupportURLDataConsumption;
                }
                
                sdc.RequestDataPropertyProfile2(
                    Globals.DataConsumptionReg,
                    request, //requestArray,
                    out responseArray,
                    out resultMessage);			

				if (null == responseArray)
				{
					if (string.Empty == resultMessage)
					{
						MessageBox.Show(this, "Web services did not answer correctly.", "Web Services", MessageBoxButtons.OK, MessageBoxIcon.Information);
					}
					else
					{
                        if (resultMessage == "1u2n3f")
						{
							MessageBox.Show(this, "You don't have any credits for this month!\nGo to Property Data on the menu above, and select Subscribe To Data!", "Get the data advantage!", MessageBoxButtons.OK, MessageBoxIcon.Information);
						}
						else
						{
							MessageBox.Show(this, resultMessage, "Web Services", MessageBoxButtons.OK, MessageBoxIcon.Information);
						}
					}
					return;
				}
				

				if (0==responseArray.Length)
				{
					

					MessageBox.Show(this, "Web services answered incorrectly.", "Web Services", MessageBoxButtons.OK, MessageBoxIcon.Information);
					return;

				}
				else
				{
					
				}

				responseStream.Write(responseArray,0,responseArray.Length);

				responseStream.Position = 0;
				response = (DTD.Response.RESPONSE_GROUP)responseSerializer.Deserialize(responseStream);
			}
			catch (Exception ex)
			{
				MessageBox.Show(this, "Too many conections on server.\nTry again later.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Information);
				throw ex;
			}
			finally
			{
				Cursor.Current = Cursors.Default;
			}
			

			if (CheckValid())
			{				
				DateTime storedDate = DateTime.Now;
				if (null == dbPropertyProfile || null == dbPropertyProfile[0])
				{
					dbPropertyProfile = new DealMaker.PlainClasses.OnLineData.PropertyProfile.PropertyProfile[1];
                    dbPropertyProfile[0] = new DealMaker.PlainClasses.OnLineData.PropertyProfile.PropertyProfile(new Guid(), null);
				}
				
				dbPropertyProfile[0].P_xmlStream = responseStream.ToArray();

				

				//dtd 4.2.2
				//BEGIN
#if !NEW_DTD
				//OLD
				property = response.RESPONSE.RESPONSE_DATA[0].PROPERTY_INFORMATION_RESPONSE._PROPERTY_INFORMATION[0].PROPERTY;
#else
				//NEW
				DTD.Response._PROPERTY_INFORMATION propInfo = null;

				
				try
				{
					//propInfo = response.RESPONSE.RESPONSE_DATA[0].PROPERTY_INFORMATION_RESPONSE.Items[0] as DTD.Response._PROPERTY_INFORMATION;
					foreach (object o in response.RESPONSE.RESPONSE_DATA[0].PROPERTY_INFORMATION_RESPONSE.Items)
					{
						if (o is DTD.Response._PROPERTY_INFORMATION)
						{
							propInfo = o as DTD.Response._PROPERTY_INFORMATION;
							break;
						}
					}
				}
				catch 
				{
					propInfo = null;
				}
				if (null != propInfo && null != propInfo.PROPERTY)
					property = propInfo.PROPERTY;
#endif
				//END

				fillComps(property,"0","0");

				//AUTOSAVE
                if (null != PP_Downloaded_Successfully && _allowAutoSaveTriggerEvents)
                    PP_Downloaded_Successfully();                

				try
				{					
					string outMsg = string.Empty;
					//0 = COMPS
					//1 = PP
					//2 = Matching Module
					string msg;
                    Guid userId = DealMaker.Globals.UserRegistration.IDUser;

                    if (Globals.NwVerEnabled && null != Globals.activeModules && Globals.activeModules.MOD_6)
                        sdc.QueryGuidFromKey(Globals.DataConsumptionReg.KeyCode, out userId, out msg);

                    bool bRes = sdc.ConfirmDataRequestSuccess(userId, 1, 1, out outMsg);					

#if DEBUG 
					if (true == bRes)
					{
#if SHOW_CHARGED
						MessageBox.Show("Charged!");
#endif
					}
					else
					{
						MessageBox.Show("PROBLEM IN CHARGE: " + outMsg);
					}
#endif
				}
				catch(Exception 
#if DEBUG
					exc
#endif
					)
				{
#if DEBUG
					MessageBox.Show(exc.ToString());
#endif
				}
			}

			sdc = null;
			return;			
		}
				
		private bool CheckValid()
		{
			DTD.Response.STATUS status = null;
			bool error = false;

			try 
			{
				status = response.RESPONSE.STATUS[0];
				if (!status._Condition.Trim().StartsWith("SUCCES"))
				{
					MessageBox.Show(status._Description, status._Condition);
					error = true;
				}
			}
			catch{};
			try 
			{
				status =response.RESPONSE.RESPONSE_DATA[0].PROPERTY_INFORMATION_RESPONSE.STATUS;
				if (!status._Condition.Trim().StartsWith("SUCCES"))
				{
					MessageBox.Show(status._Description, status._Condition);
					error = true;
				}
			}
			catch{};
			try 
			{
				
				//NEW
				DTD.Response._PRODUCT product = response.RESPONSE.RESPONSE_DATA[0].PROPERTY_INFORMATION_RESPONSE.Items[0] as DTD.Response._PRODUCT;													
                if (null != product && null != product.STATUS && product.STATUS.Length > 0)
					status = product.STATUS[0];
				//END
				if (!status._Condition.Trim().StartsWith("SUCCES"))
				{
					MessageBox.Show(status._Description, status._Condition);
					error = true;
				}
			}
			catch{};

			try 
			{
				//DTD.Response._MULTIPLE_RECORDS[] multipla 
				//	= response.RESPONSE.RESPONSE_DATA[0].PROPERTY_INFORMATION_RESPONSE._PROPERTY_INFORMATION[0]._MULTIPLE_RECORDS;

				//dtd 4.2.2
				//BEGIN
#if !NEW_DTD
				//OLD
				DTD.Response._MULTIPLE_RECORDS[] multipla = response.RESPONSE.RESPONSE_DATA[0].PROPERTY_INFORMATION_RESPONSE._PROPERTY_INFORMATION[0]._MULTIPLE_RECORDS;
#else
				//NEW
				DTD.Response._MULTIPLE_RECORDS[] multipla = null;
				DTD.Response._PROPERTY_INFORMATION propInfo = null;
									
				//propInfo = response.RESPONSE.RESPONSE_DATA[0].PROPERTY_INFORMATION_RESPONSE.Items[0] as DTD.Response._PROPERTY_INFORMATION;
				foreach (object o in response.RESPONSE.RESPONSE_DATA[0].PROPERTY_INFORMATION_RESPONSE.Items)
				{
					if (o is DTD.Response._PROPERTY_INFORMATION)
					{
						propInfo = o as DTD.Response._PROPERTY_INFORMATION;
						break;
					}
				}

				if (null != propInfo)
					multipla = propInfo._MULTIPLE_RECORDS;
#endif
				//END
				
				FormMultipleProperiesFound form = new FormMultipleProperiesFound();

				form.lvFill(multipla);
				form.ShowDialog();

				int iterator = form.selected;

				if (iterator >= 0)
				{
				
					try
					{
						textBoxStreetNoA.Text = getSecureString(multipla[iterator].PROPERTY._PARSED_STREET_ADDRESS._HouseNumber);
					}
					catch{}
					try
					{
						textBoxPreDirA.Text = getSecureString(multipla[iterator].PROPERTY._PARSED_STREET_ADDRESS._DirectionPrefix);
					}
					catch{}
					try
					{
						textBoxStreetA.Text = getSecureString(multipla[iterator].PROPERTY._PARSED_STREET_ADDRESS._StreetName);
					}
					catch{}
					try
					{
						textBoxPostDirA.Text = getSecureString(multipla[iterator].PROPERTY._PARSED_STREET_ADDRESS._DirectionSuffix);
					}
					catch{}
					try
					{
						textBoxSuffixA.Text = getSecureString(multipla[iterator].PROPERTY._PARSED_STREET_ADDRESS._StreetSuffix);
					}
					catch{}
					try
					{
						textBoxUnitNoA.Text = getSecureString(multipla[iterator].PROPERTY._PARSED_STREET_ADDRESS._ApartmentOrUnit);
					}
					catch{}
					try
					{
						textBoxZipC.Text = textBoxZipB.Text = textBoxZipA.Text = getSecureString(multipla[iterator].PROPERTY._PostalCode);
					}
					catch{}
					try
					{
						textBoxCountyC.Text =textBoxCountyB.Text = textBoxCountyA.Text = getSecureString(multipla[iterator].PROPERTY._County);
					}
					catch{}
					try
					{
						textBoxStateC.Text =textBoxStateB.Text = textBoxStateA.Text = getSecureString(multipla[iterator].PROPERTY._State);
					}
					catch{}
					try
					{
						textBoxApnB.Text = getSecureString(multipla[iterator].PROPERTY._AssessorsParcelIdentifier);
					}
					catch{}
					try
					{
						string ownerName = getSecureString(multipla[iterator].PROPERTY.PROPERTY_OWNER[0]._OwnerName);
						ownerName = ownerName.TrimStart();
						ownerName = ownerName.TrimEnd();

						int pos = -1;
						pos = ownerName.IndexOf(" ");
						textBoxLatNameC.Text = ownerName.Substring(0, pos);
						textBoxFirstNameC.Text = ownerName.Substring(pos + 1, ownerName.Length - (pos + 1));

                        if (ownerName.Contains("/"))
                            AccurateSearchLabelEnabled = true;
					}
					catch{}
				}
				error = true;
			
			}
			catch{};
			return !error;
		}
		
		public bool Save(Guid propertyItemId)
		{
			bool success = false;		
			try
			{
				if (null != dbPropertyProfile 
					&& null != dbPropertyProfile[0] 
					&& null != dbPropertyProfile[0].P_xmlStream)//TA++: 02.04.2004. - To Prevent Runtime Crashes
				{
                    dbPropertyProfile[0].P_propertyItemId = propertyItemId;
                    success = PPManager.Instance.SaveData(propertyItemId, dbPropertyProfile[0].P_xmlStream);
					
				}				
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message + Environment.NewLine + ex.InnerException + Environment.NewLine + ex.StackTrace,"Problem: Data not saved.");
			}
			return success;
		}
				
		private DTD.Response.RESPONSE_GROUP DeserializeXmlFile(string path)
		{
			DTD.Response.RESPONSE_GROUP x ;
			FileStream fs = new FileStream(path, FileMode.Open);
			
			XmlSerializer serializer = new XmlSerializer(typeof(DTD.Response.RESPONSE_GROUP));
			x = (DTD.Response.RESPONSE_GROUP)serializer.Deserialize(fs);;
			fs.Close();
			return x;
		}
						
		private string getMoney(string sMoney)
		{
			try
			{
				sMoney = Convert.ToDouble(sMoney).ToString("C");
			}
			catch
			{
			}

			return sMoney;
		}

		private string getDecimal(string sDecimal)
		{
			try
			{
				sDecimal = Convert.ToDouble(sDecimal).ToString("N");
			}
			catch
			{
			}

			return sDecimal;
		}

		private string getDate(string sDate)
		{
			if (sDate.Length==8)
			{
				try
				{
					DateTime d = new DateTime
						(
						(int.Parse(sDate.Substring(0,4))),
						(int.Parse(sDate.Substring(4,2))),
						(int.Parse(sDate.Substring(6,2)))
						);

					sDate = d.ToShortDateString();
				}
				catch
				{
				}
			}

			return sDate;
		}

        string getSemiformattedNumber(string sDecimal)
        {
            try
            {
                sDecimal = Convert.ToDouble(sDecimal).ToString("#,##0");
            }
            catch
            {
            }

            return sDecimal;
        }
		
        private string getSecureString(object str)
		{
			string SecureString = "";
			try
			{
				SecureString = str.ToString();
			}
			catch
			{}
			return SecureString;
		}
						
		private void bPrintReport_Click(object sender, System.EventArgs e)
		{
			try 
			{
				PrintDocument tmpprndoc = new PrintDocument();
				tmpprndoc.PrintPage += new PrintPageEventHandler(toPrinter);
				PrintPreviewDialogEx tmpprdiag = new PrintPreviewDialogEx();								
				tmpprdiag.Document = tmpprndoc;
			
				tmpprdiag.ShowDialog();

			}
			catch (Exception)
			{
				//silent catch (not a mistake)
				//when no printer installed -> Print -> Cancel print -> Cancel would crash app.
			}	
			
		}

		private void toPrinter(object sender, PrintPageEventArgs e)
		{
			PPReportGenerator.PrintDataReport(e, this.property, 0, 1, 1);
        }
        #endregion

        public bool OverridedSqFt { get; set; }
        public int OverridedSqFtAmt { get; set; }
                
        private void _updateSqFt_Click(object sender, EventArgs e)
        {            
            try 
            { 
                OverridedSqFtAmt = Convert.ToInt32(textBoxTotalLivingArea.Text);
                OverridedSqFt = true;
            }
            catch 
            {
                MessageBox.Show("Please enter an integer value for SqFt.", Globals.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                OverridedSqFtAmt = 0; 
            }
            
        }

#if DUMP_XML_OBJECT
        private void dumpObject(object theObject, string filename)
        {

            System.Xml.Serialization.XmlSerializer serializer
                = new System.Xml.Serialization.XmlSerializer(theObject.GetType());

            // IMPORTANT  -  we do not need a namespace!
            System.Xml.Serialization.XmlSerializerNamespaces nmspace = new XmlSerializerNamespaces();
            nmspace.Add("", "");
            // IMPORTANT  -  we do not need a namespace!

            System.Xml.XmlTextWriter writer = new XmlTextWriter(filename, System.Text.UTF8Encoding.UTF8);
            writer.Formatting = System.Xml.Formatting.Indented;
            writer.WriteStartDocument();
            writer.WriteDocType("REQUEST_GROUP", null, "C2DRequestv2.0.PropertyProfileComps", null);
            serializer.Serialize(writer, theObject, nmspace);
            writer.Flush();
            writer.Close();

        }
#endif

    }
}
