﻿namespace DealMaker.UserControls.Repairs
{
    partial class EstimateRepairsControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this._extPaint = new System.Windows.Forms.TextBox();
            this._roof = new System.Windows.Forms.TextBox();
            this._flooring = new System.Windows.Forms.TextBox();
            this._heatCool = new System.Windows.Forms.TextBox();
            this._doors = new System.Windows.Forms.TextBox();
            this._windows = new System.Windows.Forms.TextBox();
            this._plumbing = new System.Windows.Forms.TextBox();
            this._electrical = new System.Windows.Forms.TextBox();
            this._termiteDamage = new System.Windows.Forms.TextBox();
            this._waterDamage = new System.Windows.Forms.TextBox();
            this._masterBath = new System.Windows.Forms.TextBox();
            this._kitchen = new System.Windows.Forms.TextBox();
            this._misc = new System.Windows.Forms.TextBox();
            this._generalCleanUp = new System.Windows.Forms.TextBox();
            this._pool = new System.Windows.Forms.TextBox();
            this._landscaping = new System.Windows.Forms.TextBox();
            this._miscUSD = new System.Windows.Forms.Label();
            this._generalCleanUpUSD = new System.Windows.Forms.Label();
            this._poolUSD = new System.Windows.Forms.Label();
            this._landscapingUSD = new System.Windows.Forms.Label();
            this._termiteDamageUSD = new System.Windows.Forms.Label();
            this._waterDamageUSD = new System.Windows.Forms.Label();
            this._masterBathUSD = new System.Windows.Forms.Label();
            this._kitchenUSD = new System.Windows.Forms.Label();
            this._doorsUSD = new System.Windows.Forms.Label();
            this._windowsUSD = new System.Windows.Forms.Label();
            this._plumbingUSD = new System.Windows.Forms.Label();
            this._electricalUSD = new System.Windows.Forms.Label();
            this._heatCoolUSD = new System.Windows.Forms.Label();
            this._flooringUSD = new System.Windows.Forms.Label();
            this._roofUSD = new System.Windows.Forms.Label();
            this._extPaintUSD = new System.Windows.Forms.Label();
            this._extPaintBtn = new System.Windows.Forms.Button();
            this._roofBtn = new System.Windows.Forms.Button();
            this._electricalBtn = new System.Windows.Forms.Button();
            this._heatCoolBtn = new System.Windows.Forms.Button();
            this._flooringBtn = new System.Windows.Forms.Button();
            this._masterBathBtn = new System.Windows.Forms.Button();
            this._kitchenBtn = new System.Windows.Forms.Button();
            this._doorsBtn = new System.Windows.Forms.Button();
            this._windowsBtn = new System.Windows.Forms.Button();
            this._plumbingBtn = new System.Windows.Forms.Button();
            this._generalCleanUpBtn = new System.Windows.Forms.Button();
            this._poolBtn = new System.Windows.Forms.Button();
            this._landscapingBtn = new System.Windows.Forms.Button();
            this._termiteDamageBtn = new System.Windows.Forms.Button();
            this._waterDamageBtn = new System.Windows.Forms.Button();
            this._miscBtn = new System.Windows.Forms.Button();
            this._total = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this._print = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(34, 2);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(31, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Item";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(123, 2);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(240, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "General Comments to appear in SS pckg.";
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(686, 2);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(36, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Cost:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(5, 23);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(115, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Exterior / Interior Paint:";
            this.label5.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(87, 44);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(33, 13);
            this.label7.TabIndex = 6;
            this.label7.Text = "Roof:";
            this.label7.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(73, 66);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(47, 13);
            this.label8.TabIndex = 7;
            this.label8.Text = "Flooring:";
            this.label8.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(33, 86);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(87, 13);
            this.label9.TabIndex = 8;
            this.label9.Text = "Heating/Cooling:";
            this.label9.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(67, 107);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(53, 13);
            this.label10.TabIndex = 9;
            this.label10.Text = "Electrical:";
            this.label10.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(67, 128);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(53, 13);
            this.label12.TabIndex = 11;
            this.label12.Text = "Plumbing:";
            this.label12.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(66, 149);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(54, 13);
            this.label14.TabIndex = 13;
            this.label14.Text = "Windows:";
            this.label14.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(82, 170);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(38, 13);
            this.label15.TabIndex = 14;
            this.label15.Text = "Doors:";
            this.label15.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(43, 337);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(77, 13);
            this.label17.TabIndex = 24;
            this.label17.Text = "Miscellaneous:";
            this.label17.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(31, 316);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(89, 13);
            this.label18.TabIndex = 23;
            this.label18.Text = "General Cleanup:";
            this.label18.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(89, 296);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(31, 13);
            this.label19.TabIndex = 22;
            this.label19.Text = "Pool:";
            this.label19.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(49, 275);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(71, 13);
            this.label20.TabIndex = 21;
            this.label20.Text = "Landscaping:";
            this.label20.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(32, 254);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(88, 13);
            this.label21.TabIndex = 20;
            this.label21.Text = "Termite Damage:";
            this.label21.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(38, 233);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(82, 13);
            this.label22.TabIndex = 19;
            this.label22.Text = "Water Damage:";
            this.label22.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(83, 212);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(37, 13);
            this.label24.TabIndex = 17;
            this.label24.Text = "Baths:";
            this.label24.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(74, 191);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(46, 13);
            this.label25.TabIndex = 16;
            this.label25.Text = "Kitchen:";
            this.label25.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // _extPaint
            // 
            this._extPaint.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this._extPaint.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._extPaint.Location = new System.Drawing.Point(126, 21);
            this._extPaint.Name = "_extPaint";
            this._extPaint.Size = new System.Drawing.Size(560, 20);
            this._extPaint.TabIndex = 26;
            // 
            // _roof
            // 
            this._roof.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this._roof.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._roof.Location = new System.Drawing.Point(126, 42);
            this._roof.Name = "_roof";
            this._roof.Size = new System.Drawing.Size(560, 20);
            this._roof.TabIndex = 28;
            // 
            // _flooring
            // 
            this._flooring.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this._flooring.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._flooring.Location = new System.Drawing.Point(126, 63);
            this._flooring.Name = "_flooring";
            this._flooring.Size = new System.Drawing.Size(560, 20);
            this._flooring.TabIndex = 29;
            // 
            // _heatCool
            // 
            this._heatCool.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this._heatCool.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._heatCool.Location = new System.Drawing.Point(126, 84);
            this._heatCool.Name = "_heatCool";
            this._heatCool.Size = new System.Drawing.Size(560, 20);
            this._heatCool.TabIndex = 30;
            // 
            // _doors
            // 
            this._doors.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this._doors.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._doors.Location = new System.Drawing.Point(126, 168);
            this._doors.Name = "_doors";
            this._doors.Size = new System.Drawing.Size(560, 20);
            this._doors.TabIndex = 34;
            // 
            // _windows
            // 
            this._windows.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this._windows.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._windows.Location = new System.Drawing.Point(126, 147);
            this._windows.Name = "_windows";
            this._windows.Size = new System.Drawing.Size(560, 20);
            this._windows.TabIndex = 33;
            // 
            // _plumbing
            // 
            this._plumbing.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this._plumbing.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._plumbing.Location = new System.Drawing.Point(126, 126);
            this._plumbing.Name = "_plumbing";
            this._plumbing.Size = new System.Drawing.Size(560, 20);
            this._plumbing.TabIndex = 32;
            // 
            // _electrical
            // 
            this._electrical.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this._electrical.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._electrical.Location = new System.Drawing.Point(126, 105);
            this._electrical.Name = "_electrical";
            this._electrical.Size = new System.Drawing.Size(560, 20);
            this._electrical.TabIndex = 31;
            // 
            // _termiteDamage
            // 
            this._termiteDamage.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this._termiteDamage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._termiteDamage.Location = new System.Drawing.Point(126, 252);
            this._termiteDamage.Name = "_termiteDamage";
            this._termiteDamage.Size = new System.Drawing.Size(560, 20);
            this._termiteDamage.TabIndex = 40;
            // 
            // _waterDamage
            // 
            this._waterDamage.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this._waterDamage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._waterDamage.Location = new System.Drawing.Point(126, 231);
            this._waterDamage.Name = "_waterDamage";
            this._waterDamage.Size = new System.Drawing.Size(560, 20);
            this._waterDamage.TabIndex = 39;
            // 
            // _masterBath
            // 
            this._masterBath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this._masterBath.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._masterBath.Location = new System.Drawing.Point(126, 210);
            this._masterBath.Name = "_masterBath";
            this._masterBath.Size = new System.Drawing.Size(560, 20);
            this._masterBath.TabIndex = 37;
            // 
            // _kitchen
            // 
            this._kitchen.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this._kitchen.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._kitchen.Location = new System.Drawing.Point(126, 189);
            this._kitchen.Name = "_kitchen";
            this._kitchen.Size = new System.Drawing.Size(560, 20);
            this._kitchen.TabIndex = 36;
            // 
            // _misc
            // 
            this._misc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this._misc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._misc.Location = new System.Drawing.Point(126, 336);
            this._misc.Name = "_misc";
            this._misc.Size = new System.Drawing.Size(560, 20);
            this._misc.TabIndex = 44;
            // 
            // _generalCleanUp
            // 
            this._generalCleanUp.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this._generalCleanUp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._generalCleanUp.Location = new System.Drawing.Point(126, 315);
            this._generalCleanUp.Name = "_generalCleanUp";
            this._generalCleanUp.Size = new System.Drawing.Size(560, 20);
            this._generalCleanUp.TabIndex = 43;
            // 
            // _pool
            // 
            this._pool.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this._pool.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._pool.Location = new System.Drawing.Point(126, 294);
            this._pool.Name = "_pool";
            this._pool.Size = new System.Drawing.Size(560, 20);
            this._pool.TabIndex = 42;
            // 
            // _landscaping
            // 
            this._landscaping.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this._landscaping.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._landscaping.Location = new System.Drawing.Point(126, 273);
            this._landscaping.Name = "_landscaping";
            this._landscaping.Size = new System.Drawing.Size(560, 20);
            this._landscaping.TabIndex = 41;
            // 
            // _miscUSD
            // 
            this._miscUSD.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._miscUSD.Location = new System.Drawing.Point(692, 338);
            this._miscUSD.Name = "_miscUSD";
            this._miscUSD.Size = new System.Drawing.Size(57, 13);
            this._miscUSD.TabIndex = 64;
            this._miscUSD.Text = "-$-";
            this._miscUSD.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // _generalCleanUpUSD
            // 
            this._generalCleanUpUSD.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._generalCleanUpUSD.Location = new System.Drawing.Point(692, 316);
            this._generalCleanUpUSD.Name = "_generalCleanUpUSD";
            this._generalCleanUpUSD.Size = new System.Drawing.Size(57, 13);
            this._generalCleanUpUSD.TabIndex = 63;
            this._generalCleanUpUSD.Text = "-$-";
            this._generalCleanUpUSD.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // _poolUSD
            // 
            this._poolUSD.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._poolUSD.Location = new System.Drawing.Point(692, 296);
            this._poolUSD.Name = "_poolUSD";
            this._poolUSD.Size = new System.Drawing.Size(57, 13);
            this._poolUSD.TabIndex = 62;
            this._poolUSD.Text = "-$-";
            this._poolUSD.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // _landscapingUSD
            // 
            this._landscapingUSD.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._landscapingUSD.Location = new System.Drawing.Point(692, 275);
            this._landscapingUSD.Name = "_landscapingUSD";
            this._landscapingUSD.Size = new System.Drawing.Size(57, 13);
            this._landscapingUSD.TabIndex = 61;
            this._landscapingUSD.Text = "-$-";
            this._landscapingUSD.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // _termiteDamageUSD
            // 
            this._termiteDamageUSD.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._termiteDamageUSD.Location = new System.Drawing.Point(692, 254);
            this._termiteDamageUSD.Name = "_termiteDamageUSD";
            this._termiteDamageUSD.Size = new System.Drawing.Size(57, 13);
            this._termiteDamageUSD.TabIndex = 60;
            this._termiteDamageUSD.Text = "-$-";
            this._termiteDamageUSD.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // _waterDamageUSD
            // 
            this._waterDamageUSD.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._waterDamageUSD.Location = new System.Drawing.Point(692, 233);
            this._waterDamageUSD.Name = "_waterDamageUSD";
            this._waterDamageUSD.Size = new System.Drawing.Size(57, 13);
            this._waterDamageUSD.TabIndex = 59;
            this._waterDamageUSD.Text = "-$-";
            this._waterDamageUSD.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // _masterBathUSD
            // 
            this._masterBathUSD.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._masterBathUSD.Location = new System.Drawing.Point(692, 212);
            this._masterBathUSD.Name = "_masterBathUSD";
            this._masterBathUSD.Size = new System.Drawing.Size(57, 13);
            this._masterBathUSD.TabIndex = 57;
            this._masterBathUSD.Text = "-$-";
            this._masterBathUSD.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // _kitchenUSD
            // 
            this._kitchenUSD.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._kitchenUSD.Location = new System.Drawing.Point(692, 191);
            this._kitchenUSD.Name = "_kitchenUSD";
            this._kitchenUSD.Size = new System.Drawing.Size(57, 13);
            this._kitchenUSD.TabIndex = 56;
            this._kitchenUSD.Text = "-$-";
            this._kitchenUSD.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // _doorsUSD
            // 
            this._doorsUSD.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._doorsUSD.Location = new System.Drawing.Point(692, 170);
            this._doorsUSD.Name = "_doorsUSD";
            this._doorsUSD.Size = new System.Drawing.Size(57, 13);
            this._doorsUSD.TabIndex = 54;
            this._doorsUSD.Text = "-$-";
            this._doorsUSD.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // _windowsUSD
            // 
            this._windowsUSD.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._windowsUSD.Location = new System.Drawing.Point(692, 149);
            this._windowsUSD.Name = "_windowsUSD";
            this._windowsUSD.Size = new System.Drawing.Size(57, 13);
            this._windowsUSD.TabIndex = 53;
            this._windowsUSD.Text = "-$-";
            this._windowsUSD.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // _plumbingUSD
            // 
            this._plumbingUSD.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._plumbingUSD.Location = new System.Drawing.Point(692, 128);
            this._plumbingUSD.Name = "_plumbingUSD";
            this._plumbingUSD.Size = new System.Drawing.Size(57, 13);
            this._plumbingUSD.TabIndex = 52;
            this._plumbingUSD.Text = "-$-";
            this._plumbingUSD.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // _electricalUSD
            // 
            this._electricalUSD.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._electricalUSD.Location = new System.Drawing.Point(692, 107);
            this._electricalUSD.Name = "_electricalUSD";
            this._electricalUSD.Size = new System.Drawing.Size(57, 13);
            this._electricalUSD.TabIndex = 51;
            this._electricalUSD.Text = "-$-";
            this._electricalUSD.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // _heatCoolUSD
            // 
            this._heatCoolUSD.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._heatCoolUSD.Location = new System.Drawing.Point(692, 86);
            this._heatCoolUSD.Name = "_heatCoolUSD";
            this._heatCoolUSD.Size = new System.Drawing.Size(57, 13);
            this._heatCoolUSD.TabIndex = 50;
            this._heatCoolUSD.Text = "-$-";
            this._heatCoolUSD.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // _flooringUSD
            // 
            this._flooringUSD.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._flooringUSD.Location = new System.Drawing.Point(692, 66);
            this._flooringUSD.Name = "_flooringUSD";
            this._flooringUSD.Size = new System.Drawing.Size(57, 13);
            this._flooringUSD.TabIndex = 49;
            this._flooringUSD.Text = "-$-";
            this._flooringUSD.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // _roofUSD
            // 
            this._roofUSD.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._roofUSD.Location = new System.Drawing.Point(692, 44);
            this._roofUSD.Name = "_roofUSD";
            this._roofUSD.Size = new System.Drawing.Size(57, 13);
            this._roofUSD.TabIndex = 48;
            this._roofUSD.Text = "-$-";
            this._roofUSD.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // _extPaintUSD
            // 
            this._extPaintUSD.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._extPaintUSD.Location = new System.Drawing.Point(692, 23);
            this._extPaintUSD.Name = "_extPaintUSD";
            this._extPaintUSD.Size = new System.Drawing.Size(57, 13);
            this._extPaintUSD.TabIndex = 46;
            this._extPaintUSD.Text = "-$-";
            this._extPaintUSD.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // _extPaintBtn
            // 
            this._extPaintBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._extPaintBtn.Location = new System.Drawing.Point(763, 19);
            this._extPaintBtn.Name = "_extPaintBtn";
            this._extPaintBtn.Size = new System.Drawing.Size(81, 20);
            this._extPaintBtn.TabIndex = 66;
            this._extPaintBtn.Text = "Breakdown";
            this._extPaintBtn.UseVisualStyleBackColor = true;
            // 
            // _roofBtn
            // 
            this._roofBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._roofBtn.Location = new System.Drawing.Point(763, 40);
            this._roofBtn.Name = "_roofBtn";
            this._roofBtn.Size = new System.Drawing.Size(81, 20);
            this._roofBtn.TabIndex = 68;
            this._roofBtn.Text = "Breakdown";
            this._roofBtn.UseVisualStyleBackColor = true;
            // 
            // _electricalBtn
            // 
            this._electricalBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._electricalBtn.Location = new System.Drawing.Point(763, 103);
            this._electricalBtn.Name = "_electricalBtn";
            this._electricalBtn.Size = new System.Drawing.Size(81, 20);
            this._electricalBtn.TabIndex = 71;
            this._electricalBtn.Text = "Breakdown";
            this._electricalBtn.UseVisualStyleBackColor = true;
            // 
            // _heatCoolBtn
            // 
            this._heatCoolBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._heatCoolBtn.Location = new System.Drawing.Point(763, 82);
            this._heatCoolBtn.Name = "_heatCoolBtn";
            this._heatCoolBtn.Size = new System.Drawing.Size(81, 20);
            this._heatCoolBtn.TabIndex = 70;
            this._heatCoolBtn.Text = "Breakdown";
            this._heatCoolBtn.UseVisualStyleBackColor = true;
            // 
            // _flooringBtn
            // 
            this._flooringBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._flooringBtn.Location = new System.Drawing.Point(763, 61);
            this._flooringBtn.Name = "_flooringBtn";
            this._flooringBtn.Size = new System.Drawing.Size(81, 20);
            this._flooringBtn.TabIndex = 69;
            this._flooringBtn.Text = "Breakdown";
            this._flooringBtn.UseVisualStyleBackColor = true;
            // 
            // _masterBathBtn
            // 
            this._masterBathBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._masterBathBtn.Location = new System.Drawing.Point(763, 208);
            this._masterBathBtn.Name = "_masterBathBtn";
            this._masterBathBtn.Size = new System.Drawing.Size(81, 20);
            this._masterBathBtn.TabIndex = 77;
            this._masterBathBtn.Text = "Breakdown";
            this._masterBathBtn.UseVisualStyleBackColor = true;
            // 
            // _kitchenBtn
            // 
            this._kitchenBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._kitchenBtn.Location = new System.Drawing.Point(763, 187);
            this._kitchenBtn.Name = "_kitchenBtn";
            this._kitchenBtn.Size = new System.Drawing.Size(81, 20);
            this._kitchenBtn.TabIndex = 76;
            this._kitchenBtn.Text = "Breakdown";
            this._kitchenBtn.UseVisualStyleBackColor = true;
            // 
            // _doorsBtn
            // 
            this._doorsBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._doorsBtn.Location = new System.Drawing.Point(763, 166);
            this._doorsBtn.Name = "_doorsBtn";
            this._doorsBtn.Size = new System.Drawing.Size(81, 20);
            this._doorsBtn.TabIndex = 74;
            this._doorsBtn.Text = "Breakdown";
            this._doorsBtn.UseVisualStyleBackColor = true;
            // 
            // _windowsBtn
            // 
            this._windowsBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._windowsBtn.Location = new System.Drawing.Point(763, 145);
            this._windowsBtn.Name = "_windowsBtn";
            this._windowsBtn.Size = new System.Drawing.Size(81, 20);
            this._windowsBtn.TabIndex = 73;
            this._windowsBtn.Text = "Breakdown";
            this._windowsBtn.UseVisualStyleBackColor = true;
            // 
            // _plumbingBtn
            // 
            this._plumbingBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._plumbingBtn.Location = new System.Drawing.Point(763, 124);
            this._plumbingBtn.Name = "_plumbingBtn";
            this._plumbingBtn.Size = new System.Drawing.Size(81, 20);
            this._plumbingBtn.TabIndex = 72;
            this._plumbingBtn.Text = "Breakdown";
            this._plumbingBtn.UseVisualStyleBackColor = true;
            // 
            // _generalCleanUpBtn
            // 
            this._generalCleanUpBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._generalCleanUpBtn.Location = new System.Drawing.Point(763, 313);
            this._generalCleanUpBtn.Name = "_generalCleanUpBtn";
            this._generalCleanUpBtn.Size = new System.Drawing.Size(81, 20);
            this._generalCleanUpBtn.TabIndex = 83;
            this._generalCleanUpBtn.Text = "Breakdown";
            this._generalCleanUpBtn.UseVisualStyleBackColor = true;
            // 
            // _poolBtn
            // 
            this._poolBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._poolBtn.Location = new System.Drawing.Point(763, 292);
            this._poolBtn.Name = "_poolBtn";
            this._poolBtn.Size = new System.Drawing.Size(81, 20);
            this._poolBtn.TabIndex = 82;
            this._poolBtn.Text = "Breakdown";
            this._poolBtn.UseVisualStyleBackColor = true;
            // 
            // _landscapingBtn
            // 
            this._landscapingBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._landscapingBtn.Location = new System.Drawing.Point(763, 271);
            this._landscapingBtn.Name = "_landscapingBtn";
            this._landscapingBtn.Size = new System.Drawing.Size(81, 20);
            this._landscapingBtn.TabIndex = 81;
            this._landscapingBtn.Text = "Breakdown";
            this._landscapingBtn.UseVisualStyleBackColor = true;
            // 
            // _termiteDamageBtn
            // 
            this._termiteDamageBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._termiteDamageBtn.Location = new System.Drawing.Point(763, 250);
            this._termiteDamageBtn.Name = "_termiteDamageBtn";
            this._termiteDamageBtn.Size = new System.Drawing.Size(81, 20);
            this._termiteDamageBtn.TabIndex = 80;
            this._termiteDamageBtn.Text = "Breakdown";
            this._termiteDamageBtn.UseVisualStyleBackColor = true;
            // 
            // _waterDamageBtn
            // 
            this._waterDamageBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._waterDamageBtn.Location = new System.Drawing.Point(763, 229);
            this._waterDamageBtn.Name = "_waterDamageBtn";
            this._waterDamageBtn.Size = new System.Drawing.Size(81, 20);
            this._waterDamageBtn.TabIndex = 79;
            this._waterDamageBtn.Text = "Breakdown";
            this._waterDamageBtn.UseVisualStyleBackColor = true;
            // 
            // _miscBtn
            // 
            this._miscBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._miscBtn.Location = new System.Drawing.Point(763, 333);
            this._miscBtn.Name = "_miscBtn";
            this._miscBtn.Size = new System.Drawing.Size(81, 20);
            this._miscBtn.TabIndex = 84;
            this._miscBtn.Text = "Breakdown";
            this._miscBtn.UseVisualStyleBackColor = true;
            // 
            // _total
            // 
            this._total.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._total.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._total.Location = new System.Drawing.Point(689, 359);
            this._total.Name = "_total";
            this._total.Size = new System.Drawing.Size(60, 13);
            this._total.TabIndex = 86;
            this._total.Text = "-$-";
            this._total.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label46
            // 
            this.label46.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label46.Location = new System.Drawing.Point(581, 359);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(109, 13);
            this.label46.TabIndex = 87;
            this.label46.Text = "TOTAL REPAIRS:";
            this.label46.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // _print
            // 
            this._print.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._print.Location = new System.Drawing.Point(850, 18);
            this._print.Name = "_print";
            this._print.Size = new System.Drawing.Size(75, 23);
            this._print.TabIndex = 88;
            this._print.Text = "Print";
            this._print.UseVisualStyleBackColor = true;
            // 
            // EstimateRepairsControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.Controls.Add(this._print);
            this.Controls.Add(this.label46);
            this.Controls.Add(this._total);
            this.Controls.Add(this._miscBtn);
            this.Controls.Add(this._generalCleanUpBtn);
            this.Controls.Add(this._poolBtn);
            this.Controls.Add(this._landscapingBtn);
            this.Controls.Add(this._termiteDamageBtn);
            this.Controls.Add(this._waterDamageBtn);
            this.Controls.Add(this._masterBathBtn);
            this.Controls.Add(this._kitchenBtn);
            this.Controls.Add(this._doorsBtn);
            this.Controls.Add(this._windowsBtn);
            this.Controls.Add(this._plumbingBtn);
            this.Controls.Add(this._electricalBtn);
            this.Controls.Add(this._heatCoolBtn);
            this.Controls.Add(this._flooringBtn);
            this.Controls.Add(this._roofBtn);
            this.Controls.Add(this._extPaintBtn);
            this.Controls.Add(this._miscUSD);
            this.Controls.Add(this._generalCleanUpUSD);
            this.Controls.Add(this._poolUSD);
            this.Controls.Add(this._landscapingUSD);
            this.Controls.Add(this._termiteDamageUSD);
            this.Controls.Add(this._waterDamageUSD);
            this.Controls.Add(this._masterBathUSD);
            this.Controls.Add(this._kitchenUSD);
            this.Controls.Add(this._doorsUSD);
            this.Controls.Add(this._windowsUSD);
            this.Controls.Add(this._plumbingUSD);
            this.Controls.Add(this._electricalUSD);
            this.Controls.Add(this._heatCoolUSD);
            this.Controls.Add(this._flooringUSD);
            this.Controls.Add(this._roofUSD);
            this.Controls.Add(this._extPaintUSD);
            this.Controls.Add(this._misc);
            this.Controls.Add(this._generalCleanUp);
            this.Controls.Add(this._pool);
            this.Controls.Add(this._landscaping);
            this.Controls.Add(this._termiteDamage);
            this.Controls.Add(this._waterDamage);
            this.Controls.Add(this._masterBath);
            this.Controls.Add(this._kitchen);
            this.Controls.Add(this._doors);
            this.Controls.Add(this._windows);
            this.Controls.Add(this._plumbing);
            this.Controls.Add(this._electrical);
            this.Controls.Add(this._heatCool);
            this.Controls.Add(this._flooring);
            this.Controls.Add(this._roof);
            this.Controls.Add(this._extPaint);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.Name = "EstimateRepairsControl";
            this.Size = new System.Drawing.Size(938, 378);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox _extPaint;
        private System.Windows.Forms.TextBox _roof;
        private System.Windows.Forms.TextBox _flooring;
        private System.Windows.Forms.TextBox _heatCool;
        private System.Windows.Forms.TextBox _doors;
        private System.Windows.Forms.TextBox _windows;
        private System.Windows.Forms.TextBox _plumbing;
        private System.Windows.Forms.TextBox _electrical;
        private System.Windows.Forms.TextBox _termiteDamage;
        private System.Windows.Forms.TextBox _waterDamage;
        private System.Windows.Forms.TextBox _masterBath;
        private System.Windows.Forms.TextBox _kitchen;
        private System.Windows.Forms.TextBox _misc;
        private System.Windows.Forms.TextBox _generalCleanUp;
        private System.Windows.Forms.TextBox _pool;
        private System.Windows.Forms.TextBox _landscaping;
        private System.Windows.Forms.Label _miscUSD;
        private System.Windows.Forms.Label _generalCleanUpUSD;
        private System.Windows.Forms.Label _poolUSD;
        private System.Windows.Forms.Label _landscapingUSD;
        private System.Windows.Forms.Label _termiteDamageUSD;
        private System.Windows.Forms.Label _waterDamageUSD;
        private System.Windows.Forms.Label _masterBathUSD;
        private System.Windows.Forms.Label _kitchenUSD;
        private System.Windows.Forms.Label _doorsUSD;
        private System.Windows.Forms.Label _windowsUSD;
        private System.Windows.Forms.Label _plumbingUSD;
        private System.Windows.Forms.Label _electricalUSD;
        private System.Windows.Forms.Label _heatCoolUSD;
        private System.Windows.Forms.Label _flooringUSD;
        private System.Windows.Forms.Label _roofUSD;
        private System.Windows.Forms.Label _extPaintUSD;
        private System.Windows.Forms.Button _extPaintBtn;
        private System.Windows.Forms.Button _roofBtn;
        private System.Windows.Forms.Button _electricalBtn;
        private System.Windows.Forms.Button _heatCoolBtn;
        private System.Windows.Forms.Button _flooringBtn;
        private System.Windows.Forms.Button _masterBathBtn;
        private System.Windows.Forms.Button _kitchenBtn;
        private System.Windows.Forms.Button _doorsBtn;
        private System.Windows.Forms.Button _windowsBtn;
        private System.Windows.Forms.Button _plumbingBtn;
        private System.Windows.Forms.Button _generalCleanUpBtn;
        private System.Windows.Forms.Button _poolBtn;
        private System.Windows.Forms.Button _landscapingBtn;
        private System.Windows.Forms.Button _termiteDamageBtn;
        private System.Windows.Forms.Button _waterDamageBtn;
        private System.Windows.Forms.Button _miscBtn;
        private System.Windows.Forms.Label _total;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Button _print;
    }
}
