﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Printing;
using System.Data;
using System.Text;
using System.Windows.Forms;

//Added
using DealMaker.PlainClasses.Repairs;
using DealMaker.Forms.BreakDown;
using DealMaker.Repairs;
using ProfitGrabber.Common;

namespace DealMaker.UserControls.Repairs
{
    public partial class EstimateRepairsControl : System.Windows.Forms.UserControl //: BaseShortSaleUserControl
    {
        Guid _propertyItemId = Guid.Empty;
        string _address = string.Empty;
        string _fullName = string.Empty;
        bool _enableAutoSaveEvent = true;

        public delegate void RepairObjectAutoSave();
        public event RepairObjectAutoSave RepairObjectAutoSaveEvent;

        public EstimateRepairsControl()
        {
            InitializeComponent();
            AddEventHandlers();
        }
        
        void AddEventHandlers()
        {
            _print.Click += new EventHandler(On_Print_Click);
            foreach (Control ctrl in this.Controls)
            {
                if (ctrl is Button)
                    ((Button)ctrl).Click += new EventHandler(On_Click);
                if (ctrl is TextBox)
                {
                    ((TextBox)ctrl).Enter += new EventHandler(On_TextBox_Enter);
                    ((TextBox)ctrl).Leave += new EventHandler(On_TextBox_Leave);
                    ((TextBox)ctrl).KeyDown += new KeyEventHandler(On_TextBox_KeyDown);

                }
            }
        }

        public Guid PropertyItemId
        {
            get { return _propertyItemId; }
            set { _propertyItemId = value; }
        }

        public void EnableAutoSaveEventHandler(bool enable)
        {
            _enableAutoSaveEvent = enable;
        }


        public void SetData(Guid propertyItemId, string address, string fullName)
        {
            //_propertyItemId = propertyItemId;
            _address = address;
            _fullName = fullName;

            RepairsManager.Instance.LoadData(propertyItemId);
            if (null == RepairsManager.Instance.P_RepairObject)
            {
                foreach (Control ctrl in this.Controls)
                {
                    if (ctrl is TextBox)
                    {
                        TextBox tb = (TextBox)ctrl;
                        tb.Text = string.Empty;
                    }
                    if (ctrl is Label && ctrl.Name.StartsWith("_"))
                    {
                        Label l = (Label)ctrl;
                        l.Text = "-$-";
                    }
                }
            }
            else
            {
                _extPaint.Text = RepairsManager.Instance.P_RepairObject.ExtPaint;

                _roof.Text = RepairsManager.Instance.P_RepairObject.Roof;
                _electrical.Text = RepairsManager.Instance.P_RepairObject.Electrical;
                _heatCool.Text = RepairsManager.Instance.P_RepairObject.HeatCool;
                _flooring.Text = RepairsManager.Instance.P_RepairObject.Flooring;
                _masterBath.Text = RepairsManager.Instance.P_RepairObject.MasterBath;
                _kitchen.Text = RepairsManager.Instance.P_RepairObject.Kitchen;
                _doors.Text = RepairsManager.Instance.P_RepairObject.Doors;
                _windows.Text = RepairsManager.Instance.P_RepairObject.Windows;
                _plumbing.Text = RepairsManager.Instance.P_RepairObject.Plumbing;
                _generalCleanUp.Text = RepairsManager.Instance.P_RepairObject.GeneralCleanUp;
                _pool.Text = RepairsManager.Instance.P_RepairObject.Pool;
                _landscaping.Text = RepairsManager.Instance.P_RepairObject.Landscaping;
                _termiteDamage.Text = RepairsManager.Instance.P_RepairObject.TermiteDamage;
                _waterDamage.Text = RepairsManager.Instance.P_RepairObject.WaterDamage;
                _misc.Text = RepairsManager.Instance.P_RepairObject.Misc;

                _extPaintUSD.Text = RepairsManager.Instance.P_RepairObject.ExtPaintUSD.ToString("c");
                _roofUSD.Text = RepairsManager.Instance.P_RepairObject.RoofUSD.ToString("c");
                _electricalUSD.Text = RepairsManager.Instance.P_RepairObject.ElectricalUSD.ToString("c");
                _heatCoolUSD.Text = RepairsManager.Instance.P_RepairObject.HeatCoolUSD.ToString("c");
                _flooringUSD.Text = RepairsManager.Instance.P_RepairObject.FlooringUSD.ToString("c");
                _masterBathUSD.Text = RepairsManager.Instance.P_RepairObject.MasterBathUSD.ToString("c");
                _kitchenUSD.Text = RepairsManager.Instance.P_RepairObject.KitchenUSD.ToString("c");
                _doorsUSD.Text = RepairsManager.Instance.P_RepairObject.DoorsUSD.ToString("c");
                _windowsUSD.Text = RepairsManager.Instance.P_RepairObject.WindowsUSD.ToString("c");
                _plumbingUSD.Text = RepairsManager.Instance.P_RepairObject.PlumbingUSD.ToString("c");
                _generalCleanUpUSD.Text = RepairsManager.Instance.P_RepairObject.GeneralCleanUpUSD.ToString("c");
                _poolUSD.Text = RepairsManager.Instance.P_RepairObject.PoolUSD.ToString("c");
                _landscapingUSD.Text = RepairsManager.Instance.P_RepairObject.LandscapingUSD.ToString("c");
                _termiteDamageUSD.Text = RepairsManager.Instance.P_RepairObject.TermiteDamageUSD.ToString("c");
                _waterDamageUSD.Text = RepairsManager.Instance.P_RepairObject.WaterDamageUSD.ToString("c");
                _miscUSD.Text = RepairsManager.Instance.P_RepairObject.MiscUSD.ToString("c");
            }

            SetTotals();
        }

        void On_TextBox_Enter(object sender, EventArgs e)
        {
            //color change if needed?
        }

        void On_TextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Enter)
                On_TextBox_Leave(sender, null);
        }        

        void On_TextBox_Leave(object sender, EventArgs e)
        {
            //AUTOSAVE
            ForceAutoSave();
            //if (null != RepairObjectAutoSaveEvent && _enableAutoSaveEvent)
            //    RepairObjectAutoSaveEvent();

            //SaveData(_propertyItemId);
        }

        void ForceAutoSave()
        {
            if (null != RepairObjectAutoSaveEvent && _enableAutoSaveEvent)
                RepairObjectAutoSaveEvent();
        }

        public void SaveData(Guid propertyItemId)
        {
            //_propertyItemId = propertyItemId;

            if (null == RepairsManager.Instance.P_RepairObject)
                RepairsManager.Instance.P_RepairObject = new RepairObject();
            
            RepairsManager.Instance.P_RepairObject.ExtPaint = _extPaint.Text;        
            RepairsManager.Instance.P_RepairObject.Roof = _roof.Text;        
            RepairsManager.Instance.P_RepairObject.Electrical = _electrical.Text;        
            RepairsManager.Instance.P_RepairObject.HeatCool = _heatCool.Text;        
            RepairsManager.Instance.P_RepairObject.Flooring = _flooring.Text;        
            RepairsManager.Instance.P_RepairObject.MasterBath = _masterBath.Text;        
            RepairsManager.Instance.P_RepairObject.Kitchen = _kitchen.Text;        
            RepairsManager.Instance.P_RepairObject.Doors = _doors.Text;        
            RepairsManager.Instance.P_RepairObject.Windows = _windows.Text;        
            RepairsManager.Instance.P_RepairObject.Plumbing = _plumbing.Text;        
            RepairsManager.Instance.P_RepairObject.GeneralCleanUp = _generalCleanUp.Text;        
            RepairsManager.Instance.P_RepairObject.Pool = _pool.Text;        
            RepairsManager.Instance.P_RepairObject.Landscaping = _landscaping.Text;        
            RepairsManager.Instance.P_RepairObject.TermiteDamage = _termiteDamage.Text;        
            RepairsManager.Instance.P_RepairObject.WaterDamage = _waterDamage.Text;       
            RepairsManager.Instance.P_RepairObject.Misc = _misc.Text;
            
            RepairsManager.Instance.SaveData(propertyItemId);
        }

        void On_Click(object sender, EventArgs e)
        {
            if (null == RepairsManager.Instance.P_RepairObject)
                RepairsManager.Instance.P_RepairObject = new RepairObject();

            FormBreakDown fbd = new FormBreakDown();
                                                
            if (sender == _extPaintBtn)
            {
                if (null != RepairsManager.Instance.P_RepairObject.ExtPaintBD)
                    fbd.userControl_BreakDown.DataArrayCostItems = RepairsManager.Instance.P_RepairObject.ExtPaintBD;
                else
                {                   
                    CostItem[] fields = new CostItem[] 
                    {
                        new CostItem("Walls cracks (exterior)"),
                        new CostItem("Paint preparation (exterior)"),
                        new CostItem("Paint Material (exterior)"),
                        new CostItem("Paint Labor (exterior)"),
                        new CostItem("Siding (exterior)"),
                        new CostItem("Walls cracks (exterior)"),
                        new CostItem("Paint preparation (exterior)"),
                        new CostItem("Paint Material (exterior)"),
                        new CostItem("Paint Labor (exterior)"),
                        new CostItem("Wallpaper (exterior)"),
                        new CostItem("Ceiling (exterior)"),
                        new CostItem("Other (exterior)"),
                        new CostItem("Walls cracks (interior)"),
                        new CostItem("Paint preparation (interior)"),
                        new CostItem("Paint Material (interior)"),
                        new CostItem("Paint Labor (interior)"),
                        new CostItem("Siding (interior)"),
                        new CostItem("Walls cracks (interior)"),
                        new CostItem("Paint preparation (interior)"),
                        new CostItem("Paint Material (interior)"),
                        new CostItem("Paint Labor (interior)"),
                        new CostItem("Wallpaper (interior)"),
                        new CostItem("Ceiling (interior)"),
                        new CostItem("Other (interior)"),
                        new CostItem("Click here to enter an item not listed")
                    };

                    fbd.userControl_BreakDown.DataArrayCostItems = fields;
                }
                fbd.ShowDialog();
                RepairsManager.Instance.P_RepairObject.ExtPaintBD = fbd.userControl_BreakDown.DataArrayCostItems;
                RepairsManager.Instance.P_RepairObject.ExtPaintUSD = fbd.userControl_BreakDown.Total;
                _extPaintUSD.Text = fbd.userControl_BreakDown.Total.ToString("c");
            }            
            else if (sender == _roofBtn)
            {
                if (null != RepairsManager.Instance.P_RepairObject.RoofBD)
                {
                    fbd.userControl_BreakDown.DataArrayCostItems = RepairsManager.Instance.P_RepairObject.RoofBD;
                }
                else
                {
                    CostItem[] fields = new CostItem[] 
                    {
                        new CostItem("Fix leaks"),
                        new CostItem("New roof"),
                        new CostItem("Roof repair"),
                        new CostItem("Gutters"),
                        new CostItem("Other"),
                        new CostItem("Click here to enter an item not listed")
                    };

                    fbd.userControl_BreakDown.DataArrayCostItems = fields;
                }

                fbd.ShowDialog();
                RepairsManager.Instance.P_RepairObject.RoofBD = fbd.userControl_BreakDown.DataArrayCostItems;
                RepairsManager.Instance.P_RepairObject.RoofUSD = fbd.userControl_BreakDown.Total;
                _roofUSD.Text = fbd.userControl_BreakDown.Total.ToString("c");
            }
            else if (sender == _electricalBtn)
            {
                if (null != RepairsManager.Instance.P_RepairObject.ElectricalBD)
                {
                    fbd.userControl_BreakDown.DataArrayCostItems = RepairsManager.Instance.P_RepairObject.ElectricalBD;
                }
                else
                {
                    CostItem[] fields = new CostItem[] 
                    {
                        new CostItem("Switches"),
                        new CostItem("Outlets"),
                        new CostItem("Breaker box"),
                        new CostItem("Panel"),
                        new CostItem("Circuits"),
                        new CostItem("Light fixtures"),
                        new CostItem("Ceiling fans"),
                        new CostItem("Other"),
                        new CostItem("Click here to enter an item not listed")
                    };

                    fbd.userControl_BreakDown.DataArrayCostItems = fields;
                }

                fbd.ShowDialog();
                RepairsManager.Instance.P_RepairObject.ElectricalBD = fbd.userControl_BreakDown.DataArrayCostItems;
                RepairsManager.Instance.P_RepairObject.ElectricalUSD = fbd.userControl_BreakDown.Total;
                _electricalUSD.Text = fbd.userControl_BreakDown.Total.ToString("c");
            }
            else if (sender == _heatCoolBtn)
            {
                if (null != RepairsManager.Instance.P_RepairObject.HeatCoolBD)
                {
                    fbd.userControl_BreakDown.DataArrayCostItems = RepairsManager.Instance.P_RepairObject.HeatCoolBD;
                }
                else
                {

                    CostItem[] fields = new CostItem[] 
                    {
                        new CostItem("Heating - service"),
                        new CostItem("Heating - change"),
                        new CostItem("Cooling - service"),
                        new CostItem("Cooling - change"),
                        new CostItem("Other"),
                        new CostItem("Click here to enter an item not listed")
                    };

                    fbd.userControl_BreakDown.DataArrayCostItems = fields;
                }

                fbd.ShowDialog();
                RepairsManager.Instance.P_RepairObject.HeatCoolBD = fbd.userControl_BreakDown.DataArrayCostItems;
                RepairsManager.Instance.P_RepairObject.HeatCoolUSD = fbd.userControl_BreakDown.Total;
                _heatCoolUSD.Text = fbd.userControl_BreakDown.Total.ToString("c");
            }
            else if (sender == _flooringBtn)
            {
                if (null != RepairsManager.Instance.P_RepairObject.FlooringBD)
                {
                    fbd.userControl_BreakDown.DataArrayCostItems = RepairsManager.Instance.P_RepairObject.FlooringBD;
                }
                else
                {
                    CostItem[] fields = new CostItem[] 
                    {
                        new CostItem("Fix any rot or weakness"),
                        new CostItem("Carpet"),
                        new CostItem("Vinyl"),
                        new CostItem("Tile"),
                        new CostItem("Other"),
                        new CostItem("Click here to enter an item not listed")
                    };

                    fbd.userControl_BreakDown.DataArrayCostItems = fields;
                }

                fbd.ShowDialog();
                RepairsManager.Instance.P_RepairObject.FlooringBD = fbd.userControl_BreakDown.DataArrayCostItems;
                RepairsManager.Instance.P_RepairObject.FlooringUSD = fbd.userControl_BreakDown.Total;
                _flooringUSD.Text = fbd.userControl_BreakDown.Total.ToString("c");
            }
            else if (sender == _masterBathBtn)
            {
                if (null != RepairsManager.Instance.P_RepairObject.MasterBathBD)
                {
                    fbd.userControl_BreakDown.DataArrayCostItems = RepairsManager.Instance.P_RepairObject.MasterBathBD;
                }
                else
                {
                    CostItem[] fields = new CostItem[] 
                    {
                        new CostItem("Fixtures"),
                        new CostItem("Plumbing"),
                        new CostItem("Cabinetry"),
                        new CostItem("Fixtures"),
                        new CostItem("Plumbing"),
                        new CostItem("Cabinetry"),
                        new CostItem("Other"),
                        new CostItem("Click here to enter an item not listed")
                    };

                    fbd.userControl_BreakDown.DataArrayCostItems = fields;
                }

                fbd.ShowDialog();
                RepairsManager.Instance.P_RepairObject.MasterBathBD = fbd.userControl_BreakDown.DataArrayCostItems;
                RepairsManager.Instance.P_RepairObject.MasterBathUSD = fbd.userControl_BreakDown.Total;
                _masterBathUSD.Text = fbd.userControl_BreakDown.Total.ToString("c");
            }
            else if (sender == _kitchenBtn)
            {
                if (null != RepairsManager.Instance.P_RepairObject.KitchenBD)
                {
                    fbd.userControl_BreakDown.DataArrayCostItems = RepairsManager.Instance.P_RepairObject.KitchenBD;
                }
                else
                {
                    CostItem[] fields = new CostItem[] 
                    {
                        new CostItem("Stove"),
                        new CostItem("Sink"),
                        new CostItem("Fridge"),
                        new CostItem("Garbage disposal"),
                        new CostItem("Other"),
                        new CostItem("Click here to enter an item not listed")
                    };

                    fbd.userControl_BreakDown.DataArrayCostItems = fields;
                }

                fbd.ShowDialog();
                RepairsManager.Instance.P_RepairObject.KitchenBD = fbd.userControl_BreakDown.DataArrayCostItems;
                RepairsManager.Instance.P_RepairObject.KitchenUSD = fbd.userControl_BreakDown.Total;
                _kitchenUSD.Text = fbd.userControl_BreakDown.Total.ToString("c");
            }            
            else if (sender == _doorsBtn)
            {
                if (null != RepairsManager.Instance.P_RepairObject.DoorsBD)
                {
                    fbd.userControl_BreakDown.DataArrayCostItems = RepairsManager.Instance.P_RepairObject.DoorsBD;
                }
                else
                {
                    CostItem[] fields = new CostItem[] 
                    {
                        new CostItem("Repair"),
                        new CostItem("Replace"),
                        new CostItem("Garage door"),
                        new CostItem("Shed"),
                        new CostItem("Carport"),
                        new CostItem("Other"),
                        new CostItem("Click here to enter an item not listed")  
                    };

                    fbd.userControl_BreakDown.DataArrayCostItems = fields;
                }

                fbd.ShowDialog();
                RepairsManager.Instance.P_RepairObject.DoorsBD = fbd.userControl_BreakDown.DataArrayCostItems;
                RepairsManager.Instance.P_RepairObject.DoorsUSD = fbd.userControl_BreakDown.Total;
                _doorsUSD.Text = fbd.userControl_BreakDown.Total.ToString("c");
            }
            else if (sender == _windowsBtn)
            {
                if (null != RepairsManager.Instance.P_RepairObject.WindowsBD)
                {
                    fbd.userControl_BreakDown.DataArrayCostItems = RepairsManager.Instance.P_RepairObject.WindowsBD;
                }
                else
                {
                    CostItem[] fields = new CostItem[] 
                    {
                        new CostItem("Repair"),
                        new CostItem("Replace"),
                        new CostItem("Other"),
                        new CostItem("Click here to enter an item not listed")
                    };

                    fbd.userControl_BreakDown.DataArrayCostItems = fields;
                }

                fbd.ShowDialog();
                RepairsManager.Instance.P_RepairObject.WindowsBD = fbd.userControl_BreakDown.DataArrayCostItems;
                RepairsManager.Instance.P_RepairObject.WindowsUSD = fbd.userControl_BreakDown.Total;
                _windowsUSD.Text = fbd.userControl_BreakDown.Total.ToString("c");
            }
            else if (sender == _plumbingBtn)
            {
                if (null != RepairsManager.Instance.P_RepairObject.PlumbingBD)
                {
                    fbd.userControl_BreakDown.DataArrayCostItems = RepairsManager.Instance.P_RepairObject.PlumbingBD;
                }
                else
                {
                    CostItem[] fields = new CostItem[] 
                    {
                        new CostItem("Utility"),
                        new CostItem("Water heater"),
                        new CostItem("Other"),
                        new CostItem("Click here to enter an item not listed")
                    };

                    fbd.userControl_BreakDown.DataArrayCostItems = fields;
                }

                fbd.ShowDialog();
                RepairsManager.Instance.P_RepairObject.PlumbingBD = fbd.userControl_BreakDown.DataArrayCostItems;
                RepairsManager.Instance.P_RepairObject.PlumbingUSD = fbd.userControl_BreakDown.Total;
                _plumbingUSD.Text = fbd.userControl_BreakDown.Total.ToString("c");
            }
            else if (sender == _generalCleanUpBtn)
            {
                if (null != RepairsManager.Instance.P_RepairObject.GeneralCleanUpBD)
                {
                    fbd.userControl_BreakDown.DataArrayCostItems = RepairsManager.Instance.P_RepairObject.GeneralCleanUpBD;
                }
                else
                {
                    CostItem[] fields = new CostItem[] 
                    {
                        new CostItem("Interior"),
                        new CostItem("Windows"),
                        new CostItem("Exterior"),
                        new CostItem("Click here to enter an item not listed")
                    };

                    fbd.userControl_BreakDown.DataArrayCostItems = fields;
                }

                fbd.ShowDialog();
                RepairsManager.Instance.P_RepairObject.GeneralCleanUpBD = fbd.userControl_BreakDown.DataArrayCostItems;
                RepairsManager.Instance.P_RepairObject.GeneralCleanUpUSD = fbd.userControl_BreakDown.Total;
                _generalCleanUpUSD.Text = fbd.userControl_BreakDown.Total.ToString("c");
            }
            else if (sender == _poolBtn)
            {
                if (null != RepairsManager.Instance.P_RepairObject.PoolBD)
                {
                    fbd.userControl_BreakDown.DataArrayCostItems = RepairsManager.Instance.P_RepairObject.PoolBD;
                }
                else
                {
                    CostItem[] fields = new CostItem[] 
                    {
                        new CostItem("Cleanup"),
                        new CostItem("Equipment repair/replace"),
                        new CostItem("Other"),
                        new CostItem("Click here to enter an item not listed")
                    };

                    fbd.userControl_BreakDown.DataArrayCostItems = fields;
                }

                fbd.ShowDialog();
                RepairsManager.Instance.P_RepairObject.PoolBD = fbd.userControl_BreakDown.DataArrayCostItems;
                RepairsManager.Instance.P_RepairObject.PoolUSD = fbd.userControl_BreakDown.Total;
                _poolUSD.Text = fbd.userControl_BreakDown.Total.ToString("c");
            }
            else if (sender == _landscapingBtn)
            {
                if (null != RepairsManager.Instance.P_RepairObject.LandscapingBD)
                {
                    fbd.userControl_BreakDown.DataArrayCostItems = RepairsManager.Instance.P_RepairObject.LandscapingBD;
                }
                else
                {
                    CostItem[] fields = new CostItem[] 
                    {
                        new CostItem("Sprinkler system"),
                        new CostItem("Lawn"),
                        new CostItem("Rocks"),
                        new CostItem("Maintenance"),
                        new CostItem("Other"),
                        new CostItem("Click here to enter an item not listed")
                    };

                    fbd.userControl_BreakDown.DataArrayCostItems = fields;
                }

                fbd.ShowDialog();
                RepairsManager.Instance.P_RepairObject.LandscapingBD = fbd.userControl_BreakDown.DataArrayCostItems;
                RepairsManager.Instance.P_RepairObject.LandscapingUSD = fbd.userControl_BreakDown.Total;
                _landscapingUSD.Text = fbd.userControl_BreakDown.Total.ToString("c");
            }
            else if (sender == _termiteDamageBtn)
            {
                if (null != RepairsManager.Instance.P_RepairObject.TermiteDamageBD)
                {
                    fbd.userControl_BreakDown.DataArrayCostItems = RepairsManager.Instance.P_RepairObject.TermiteDamageBD;
                }
                else
                {
                    CostItem[] fields = new CostItem[] 
                    {
                        new CostItem("Fixing cost"),
                        new CostItem("Treatment"),
                        new CostItem("Other"),
                        new CostItem("Click here to enter an item not listed")
                    };

                    fbd.userControl_BreakDown.DataArrayCostItems = fields;
                }

                fbd.ShowDialog();
                RepairsManager.Instance.P_RepairObject.TermiteDamageBD = fbd.userControl_BreakDown.DataArrayCostItems;
                RepairsManager.Instance.P_RepairObject.TermiteDamageUSD = fbd.userControl_BreakDown.Total;
                _termiteDamageUSD.Text = fbd.userControl_BreakDown.Total.ToString("c");
            }
            else if (sender == _waterDamageBtn)
            {
                if (null != RepairsManager.Instance.P_RepairObject.WaterDamageBD)
                {
                    fbd.userControl_BreakDown.DataArrayCostItems = RepairsManager.Instance.P_RepairObject.WaterDamageBD;
                }
                else
                {
                    CostItem[] fields = new CostItem[] 
                    {
                        new CostItem("Fixing cost"),
                        new CostItem("Mold"),
                        new CostItem("Click here to enter an item not listed")
                    };

                    fbd.userControl_BreakDown.DataArrayCostItems = fields;
                }

                fbd.ShowDialog();
                RepairsManager.Instance.P_RepairObject.WaterDamageBD = fbd.userControl_BreakDown.DataArrayCostItems;
                RepairsManager.Instance.P_RepairObject.WaterDamageUSD = fbd.userControl_BreakDown.Total;
                _waterDamageUSD.Text = fbd.userControl_BreakDown.Total.ToString("c");
            }            
            else if (sender == _miscBtn)
            {
                if (null != RepairsManager.Instance.P_RepairObject.MiscBD)
                {
                    fbd.userControl_BreakDown.DataArrayCostItems = RepairsManager.Instance.P_RepairObject.MiscBD;
                }
                else
                {
                    CostItem[] fields = new CostItem[] 
                    {
                        new CostItem("Click here to enter an item not listed")
                    };

                    fbd.userControl_BreakDown.DataArrayCostItems = fields;
                }

                fbd.ShowDialog();
                RepairsManager.Instance.P_RepairObject.MiscBD = fbd.userControl_BreakDown.DataArrayCostItems;
                RepairsManager.Instance.P_RepairObject.MiscUSD = fbd.userControl_BreakDown.Total;
                _miscUSD.Text = fbd.userControl_BreakDown.Total.ToString("c");
            }

            SetTotals();
            //RepairsManager.Instance.SaveData(_propertyItemId);
            ForceAutoSave();
        }

        void SetTotals()
        {
            decimal total = 0;
            
            if (null != RepairsManager.Instance.P_RepairObject)
            {
                total += RepairsManager.Instance.P_RepairObject.ExtPaintUSD;
                total += RepairsManager.Instance.P_RepairObject.IntPaintUSD;
                total += RepairsManager.Instance.P_RepairObject.RoofUSD;
                total += RepairsManager.Instance.P_RepairObject.ElectricalUSD;
                total += RepairsManager.Instance.P_RepairObject.HeatCoolUSD;
                total += RepairsManager.Instance.P_RepairObject.FlooringUSD;
                total += RepairsManager.Instance.P_RepairObject.MasterBathUSD;
                total += RepairsManager.Instance.P_RepairObject.KitchenUSD;
                total += RepairsManager.Instance.P_RepairObject.GarageDoorUSD;
                total += RepairsManager.Instance.P_RepairObject.DoorsUSD;
                total += RepairsManager.Instance.P_RepairObject.WindowsUSD;
                total += RepairsManager.Instance.P_RepairObject.PlumbingUSD;
                total += RepairsManager.Instance.P_RepairObject.GeneralCleanUpUSD;
                total += RepairsManager.Instance.P_RepairObject.PoolUSD;
                total += RepairsManager.Instance.P_RepairObject.LandscapingUSD;
                total += RepairsManager.Instance.P_RepairObject.TermiteDamageUSD;
                total += RepairsManager.Instance.P_RepairObject.WaterDamageUSD;
                total += RepairsManager.Instance.P_RepairObject.OtherBathsUSD;
                total += RepairsManager.Instance.P_RepairObject.OtherUSD;
                total += RepairsManager.Instance.P_RepairObject.MiscUSD;
            }

            _total.Text = total.ToString("c");

            if (null != RepairsManager.Instance.P_RepairObject)
                RepairsManager.Instance.P_RepairObject.Total = total;

            RepairsManager.Instance.SaveData(_propertyItemId);
        }

        EstimateRepairsReportGenerator gen = null;
        void On_Print_Click(object sender, EventArgs e)
        {
            if (null != gen)
                gen.ResetGen();            

            PrintDialog pd = new PrintDialog();
            pd.UseEXDialog = true;

            PrintDocument tmpprndoc = new PrintDocument();
            tmpprndoc.PrintPage += new PrintPageEventHandler(On_PrintPage);
            tmpprndoc.EndPrint += new PrintEventHandler(Ontmpprndoc_EndPrint);

            pd.Document = tmpprndoc;
            pd.Document.DefaultPageSettings.Landscape = false;

            PrintPreviewDialogEx tmpprdiag = new PrintPreviewDialogEx();

            tmpprdiag.Document = tmpprndoc;
            tmpprdiag.ShowDialog();
        }

        void Ontmpprndoc_EndPrint(object sender, PrintEventArgs e)
        {
            if (null != gen)
                gen.ResetGen();
        }

        private void On_PrintPage(object sender, PrintPageEventArgs e)
        {
            if (null == gen)
                gen = new EstimateRepairsReportGenerator(e);

            string address = "TODO";// DataManager.Instance.GetFullSiteAddress();
            string fullName = "TODO";// DataManager.Instance.GetCurrentShortSaleDocument().ContactInfo.FullName;
            string primSSNr = string.Empty;// DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.PrimarySS;
            string secSSNr = string.Empty;// DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.SecondarySS;            

            gen.Generate(e, _address, _fullName, primSSNr, secSSNr);
        }
    }
}
