using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;

namespace DealMaker
{
	/// <summary>
	/// Summary description for ReadMe.
	/// </summary>
	public class ReadMe : System.Windows.Forms.UserControl
	{
		private System.Windows.Forms.PictureBox pbLogo;
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public ReadMe()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();		
			this.pbLogo.MouseEnter += new EventHandler(pbLogo_MouseEnter);
			this.pbLogo.MouseLeave += new EventHandler(pbLogo_MouseLeave);
		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		


		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(ReadMe));
			this.pbLogo = new System.Windows.Forms.PictureBox();
			this.SuspendLayout();
			// 
			// pbLogo
			// 
			this.pbLogo.Image = ((System.Drawing.Image)(resources.GetObject("pbLogo.Image")));
			this.pbLogo.Location = new System.Drawing.Point(0, 0);
			this.pbLogo.Name = "pbLogo";
			this.pbLogo.Size = new System.Drawing.Size(16, 16);
			this.pbLogo.TabIndex = 0;
			this.pbLogo.TabStop = false;
			// 
			// ReadMe
			// 
			this.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(210)), ((System.Byte)(216)), ((System.Byte)(240)));
			this.Controls.Add(this.pbLogo);
			this.Name = "ReadMe";
			this.Size = new System.Drawing.Size(16, 16);
			this.ResumeLayout(false);

		}
		#endregion
		

		private void pbLogo_MouseEnter(object sender, EventArgs e)
		{
			OnMouseEnter(e);
		}

		private void pbLogo_MouseLeave(object sender, EventArgs e)
		{
			OnMouseLeave(e);
		}
	}
}
