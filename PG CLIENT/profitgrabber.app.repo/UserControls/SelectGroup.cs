using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;

//Floaters
using DealMaker.Forms.Floaters;

//ReturnMailWizard enhanced process
using DealMaker.PlainClasses.ReturnMailWizard;

namespace DealMaker
{
	/// <summary>
	/// Summary description for SelectGroup.
	/// </summary>
	public class SelectGroup : System.Windows.Forms.UserControl
	{
		public DealMaker.CustomizedTreeViewUserControl customizedTreeViewUserControl1;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox tbRecordsInGroup;
		private System.Windows.Forms.TextBox tbNumberOfSubGroups;
		
		private DealMaker.Forms.SchedulingActicities.TaskAndHistoryList taskAndHistoryList = new DealMaker.Forms.SchedulingActicities.TaskAndHistoryList();
		private System.Windows.Forms.Label taskAndHistoryListPosition;
		private DealMaker.ReadMe readMe1;
		private DealMaker.Forms.Floaters.SelectGroup floater = null;

		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;


		//sasa, 30.01.06: bugfix, task list was not refreshed on page reload
		public void RefreshTaskList()
		{
			taskAndHistoryList.FillTaskList();
		}


		public SelectGroup()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();
			AddEventHandlers();

            this.customizedTreeViewUserControl1.AllowMoveAndCombineGroups = true;
        }

		void AddEventHandlers()
		{
			this.customizedTreeViewUserControl1.tv_Nodes.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.tv_Nodes_AfterSelect);
			CustomizedTreeViewUserControl.REFRESH_GROUPS += new DealMaker.CustomizedTreeViewUserControl.RefreshHandler(RefreshGroups);
			OneTwoThreeSold.REFRESH_GROUPS_BY_123SOLD += new DealMaker.OneTwoThreeSold.NewGroupAddedBy123Sold(RefreshGroupsBy123Sold);
			
			SkipTracedContactAssistant.GROUPS_CREATED_BY_SKIPTRACED_CONTACT_ASSISTANT += new DealMaker.PlainClasses.ReturnMailWizard.SkipTracedContactAssistant.NewGroupsCreatedBySkipTracedContactAssistant(On_GROUPS_CREATED_BY_SKIPTRACED_CONTACT_ASSISTANT);
			SkipTracedContactAssistant.SHOW_GROUP_CREATED_BY_SKIPTRACED_CONTACT_ASSISTANT += new DealMaker.PlainClasses.ReturnMailWizard.SkipTracedContactAssistant.ShowGroupCreatedBySkipTracedContactAssistant(On_SHOW_GROUP_CREATED_BY_SKIPTRACED_CONTACT_ASSISTANT);

			ServiceWorker.GetInstance.FireServiceFinishedEvent += new DealMaker.ServiceWorker.ServiceFinishedHandler(GetInstance_FireServiceFinishedEvent);
			//Floaters
			this.readMe1.MouseEnter += new EventHandler(ShowFloater);
			this.readMe1.MouseLeave += new EventHandler(HideFloater);	
		
			this.taskAndHistoryList.IconVisible = false;
		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.customizedTreeViewUserControl1 = new DealMaker.CustomizedTreeViewUserControl();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.tbRecordsInGroup = new System.Windows.Forms.TextBox();
			this.tbNumberOfSubGroups = new System.Windows.Forms.TextBox();
			this.taskAndHistoryListPosition = new System.Windows.Forms.Label();
			this.readMe1 = new DealMaker.ReadMe();
			this.SuspendLayout();
			// 
			// customizedTreeViewUserControl1
			// 
			this.customizedTreeViewUserControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left)));
			this.customizedTreeViewUserControl1.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(210)), ((System.Byte)(216)), ((System.Byte)(240)));
			this.customizedTreeViewUserControl1.Location = new System.Drawing.Point(8, 8);
			this.customizedTreeViewUserControl1.Name = "customizedTreeViewUserControl1";
			this.customizedTreeViewUserControl1.SelectedTreeNode = null;
			this.customizedTreeViewUserControl1.Size = new System.Drawing.Size(288, 285);
			this.customizedTreeViewUserControl1.TabIndex = 0;
			this.customizedTreeViewUserControl1.UpdateGlobals = false;
			// 
			// label1
			// 
			this.label1.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.label1.Location = new System.Drawing.Point(312, 16);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(184, 23);
			this.label1.TabIndex = 1;
			this.label1.Text = "Number of Records in Group:";
			// 
			// label2
			// 
			this.label2.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.label2.Location = new System.Drawing.Point(312, 40);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(152, 23);
			this.label2.TabIndex = 2;
			this.label2.Text = "Number of Subgroups:";
			// 
			// tbRecordsInGroup
			// 
			this.tbRecordsInGroup.Location = new System.Drawing.Point(504, 16);
			this.tbRecordsInGroup.Name = "tbRecordsInGroup";
			this.tbRecordsInGroup.ReadOnly = true;
			this.tbRecordsInGroup.Size = new System.Drawing.Size(184, 22);
			this.tbRecordsInGroup.TabIndex = 3;
			this.tbRecordsInGroup.Text = "";
			// 
			// tbNumberOfSubGroups
			// 
			this.tbNumberOfSubGroups.Location = new System.Drawing.Point(504, 40);
			this.tbNumberOfSubGroups.Name = "tbNumberOfSubGroups";
			this.tbNumberOfSubGroups.ReadOnly = true;
			this.tbNumberOfSubGroups.Size = new System.Drawing.Size(184, 22);
			this.tbNumberOfSubGroups.TabIndex = 4;
			this.tbNumberOfSubGroups.Text = "";
			// 
			// taskAndHistoryListPosition
			// 
			this.taskAndHistoryListPosition.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.taskAndHistoryListPosition.Location = new System.Drawing.Point(304, 88);
			this.taskAndHistoryListPosition.Name = "taskAndHistoryListPosition";
			this.taskAndHistoryListPosition.Size = new System.Drawing.Size(606, 523);
			this.taskAndHistoryListPosition.TabIndex = 5;
			this.taskAndHistoryListPosition.Text = "taskAndHistoryListPosition";
			this.taskAndHistoryListPosition.Visible = false;
			// 
			// readMe1
			// 
			this.readMe1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.readMe1.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(210)), ((System.Byte)(216)), ((System.Byte)(240)));
			this.readMe1.Location = new System.Drawing.Point(894, 8);
			this.readMe1.Name = "readMe1";
			this.readMe1.Size = new System.Drawing.Size(16, 16);
			this.readMe1.TabIndex = 6;
			// 
			// SelectGroup
			// 
			this.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(210)), ((System.Byte)(216)), ((System.Byte)(240)));
			this.Controls.Add(this.readMe1);
			this.Controls.Add(this.taskAndHistoryListPosition);
			this.Controls.Add(this.tbNumberOfSubGroups);
			this.Controls.Add(this.tbRecordsInGroup);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.customizedTreeViewUserControl1);
			this.Name = "SelectGroup";
			this.Size = new System.Drawing.Size(918, 704);
			this.Load += new System.EventHandler(this.SelectGroup_Load);
			this.ResumeLayout(false);

		}
		#endregion
		
		#region � tv_Nodes_AfterSelect �
		private void tv_Nodes_AfterSelect(object sender, System.Windows.Forms.TreeViewEventArgs e)
		{
			if (null != ((TreeView)sender).SelectedNode)
			{
				TreeNode selectedNode = ((TreeView)sender).SelectedNode;
				Node n = (Node)selectedNode.Tag;
				this.tbRecordsInGroup.Text = ApplicationHelper.GetPropertyCountInNode(n.nodeId).ToString();
				this.tbNumberOfSubGroups.Text = selectedNode.Nodes.Count.ToString();
			}
		}
		#endregion � tv_Nodes_AfterSelect �

		private void SelectGroup_Load(object sender, System.EventArgs e)
		{
			taskAndHistoryList.Show();
			
			taskAndHistoryList.Location = taskAndHistoryListPosition.Location;
			taskAndHistoryList.Size = taskAndHistoryListPosition.Size;
			taskAndHistoryList.Anchor = AnchorStyles.Right | AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left;

			Globals.customizedTreeViewUserControl = this.customizedTreeViewUserControl1;

			taskAndHistoryList.SetEventHandler();

			this.Controls.Add(taskAndHistoryList);

			if (-1 != SkipTracedContactAssistant.Instance.LastGroupIdCreatedByAssistant)
				On_SHOW_GROUP_CREATED_BY_SKIPTRACED_CONTACT_ASSISTANT(SkipTracedContactAssistant.Instance.LastGroupIdCreatedByAssistant);
		}

		private void RefreshGroupsBy123Sold(int nodeId)
		{
			this.RefreshGroups();
		}

		private void On_GROUPS_CREATED_BY_SKIPTRACED_CONTACT_ASSISTANT()
		{
			this.RefreshGroups();
		}

		private delegate void GetInstance_FireServiceFinishedEventDelegate();
		private void GetInstance_FireServiceFinishedEvent()
		{
            if (InvokeRequired)
                this.Invoke(new GetInstance_FireServiceFinishedEventDelegate(GetInstance_FireServiceFinishedEvent));
            else
            {

                TreeNode tnTemp = this.customizedTreeViewUserControl1.SelectedTreeNode;
                if (null != tnTemp)
                {
                    On_SHOW_GROUP_CREATED_BY_SKIPTRACED_CONTACT_ASSISTANT(((Node)tnTemp.Tag).nodeId);
                }
                else
                {
                    this.RefreshGroups();
                }
            }
		}

		private delegate void RefreshGroupsDelegate();
		private void RefreshGroups()
		{
            if (true == this.customizedTreeViewUserControl1.InvokeRequired)
            {
                this.Invoke(new RefreshGroupsDelegate(RefreshGroups));
            }
            else
            {
                TreeNode tnTemp = this.customizedTreeViewUserControl1.SelectedTreeNode;
                this.customizedTreeViewUserControl1.UpdateTreeView();
                if (null != tnTemp)
                {
                    this.customizedTreeViewUserControl1.SelectedTreeNode = tnTemp;
                }
                //tnTemp.Expand();
            }
		}

		delegate void On_SHOW_GROUP_CREATED_BY_SKIPTRACED_CONTACT_ASSISTANT_Async (int groupId);
		private void On_SHOW_GROUP_CREATED_BY_SKIPTRACED_CONTACT_ASSISTANT(int groupId)
		{
			if (InvokeRequired)            
                Invoke(new On_SHOW_GROUP_CREATED_BY_SKIPTRACED_CONTACT_ASSISTANT_Async(On_SHOW_GROUP_CREATED_BY_SKIPTRACED_CONTACT_ASSISTANT), new object[] { groupId });
            else
            {
                this.customizedTreeViewUserControl1.UpdateTreeView();
                TreeNode tn = GetTreeNode(this.customizedTreeViewUserControl1.tv_Nodes.Nodes[0], groupId);
                this.customizedTreeViewUserControl1.SelectedTreeNode = tn;
                this.customizedTreeViewUserControl1.SelectedTreeNode.Expand();

                tbRecordsInGroup.Text = ApplicationHelper.GetPropertyCountInNode(groupId).ToString();
                tbNumberOfSubGroups.Text = customizedTreeViewUserControl1.SelectedTreeNode.Nodes.Count.ToString();
            }
		}

		TreeNode GetTreeNode(TreeNode tn, int groupId)
		{
			if (((Node)tn.Tag).nodeId == groupId)
				return tn;

			foreach (TreeNode child in tn.Nodes)
			{
				TreeNode tnRes = GetTreeNode(child, groupId);
				if (null != tnRes)
					return tnRes;
			}
			
			return null;
		}

		
		private void ShowFloater(object sender, EventArgs e)
		{
			this.floater = new DealMaker.Forms.Floaters.SelectGroup();


			Point pReadMe = this.readMe1.Location;
			Size fSize = this.floater.Size;
						
			//this.floater.Location = new Point(pReadMe.X - fSize.Width, pReadMe.Y + this.readMe1.Height);
			this.floater.Location = PointToScreen(new Point (pReadMe.X - fSize.Width, pReadMe.Y) );

			this.floater.Show();

			

		}

		private void HideFloater(object sender, EventArgs e)
		{
			this.floater.Hide();
		}		
	}
}
