#region � Using �
using System;
using System.Collections;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using NHibernate;
using NHibernate.Cfg;
using DealMaker.Export;
using System.Net;
using Newtonsoft.Json;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using DealMaker.UserControls.Routing;
#endregion � Using �
#region � Namespace DealMaker �
namespace DealMaker
{
	#region � class PropertyItemListViewControl �
	/// <summary>
	/// Summary description for PropertyItemListViewControl.
	/// </summary>
	public class PropertyItemListViewControlRouting : System.Windows.Forms.UserControl
	{
		#region � Data �
		private DealMaker.Forms.Floaters.ListView floater = null;
        bool updating;

        private bool asc = false;
		private int currentSelectedGroup = -1;		
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox tbSelectedGroup;
		private System.Windows.Forms.ListView lvPropertyItems;
		private System.Windows.Forms.ColumnHeader FullName;
		private System.Windows.Forms.ColumnHeader SiteAddr;
		private System.Windows.Forms.Button bBrowseGroup;
		private System.Windows.Forms.ColumnHeader siteZIP;
		private System.Windows.Forms.ColumnHeader tag;
		private System.Windows.Forms.ColumnHeader siteCity;
		private System.Windows.Forms.ColumnHeader siteState;
		private System.Windows.Forms.ColumnHeader foreclosureFileDate;
		private System.Windows.Forms.ColumnHeader foreclosureSaleDate;
        private Button _showRoute;
        private Label label2;
        private Label label3;
        private Label label4;
        private ComboBox _selectedRange;
        private Label label5;
        private Button _filter;
        private Label label6;

        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.Container components = null;
		#endregion � Data �
		
		public Hashtable GetSelectedPropertyItems
		{
			get 
			{
				Hashtable htPropertyItems = new Hashtable();
				for (int i = 0; i < this.lvPropertyItems.Items.Count; i++)
				{
					ListViewItem lvi = (ListViewItem)this.lvPropertyItems.Items[i];
					htPropertyItems.Add(i + 1, lvi.Tag);
				}
				return htPropertyItems;
			}
		}
		
		#region � CTR && Dispose �
		public PropertyItemListViewControlRouting()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

			// TODO: Add any initialization after the InitializeComponent call
			
			OneTwoThreeSold.NEW_CONTACT_IN_GROUP_BY_123_SOLD += new DealMaker.OneTwoThreeSold.NewContactInGroupFrom123Sold(NewContactInGroupBy123Sold);
		}

		public PropertyItemListViewControlRouting(int selectedGroupId, string selectedGroupName)
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();
			this.currentSelectedGroup = selectedGroupId;
			this.ShowData_FastAccess(selectedGroupId);			
			this.tbSelectedGroup.Text = selectedGroupName;

			
		}

        bool loaded = false;
        protected override void OnLoad(EventArgs e)
        {
            loaded = true;
            base.OnLoad(e);
        }

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}
        #endregion � CTR && Dispose �


        List<ListViewItem> lvis = new List<ListViewItem>();
        private delegate void ShowData_FastAccessDelegate(int nodeId);
		private void ShowData_FastAccess (int nodeId)
		{
			Globals.SelectedGroupInListView = nodeId;			
			
			this.lvPropertyItems.BeginUpdate();
			this.lvPropertyItems.Items.Clear();
            this.lvPropertyItems.EndUpdate();

            Cursor.Current = Cursors.WaitCursor;
			SqlDataReader dataReader = null;
			try
			{																	
				if (ConnectionState.Open != Connection.getInstance().HatchConn.State)
				{
					Connection.getInstance().HatchConn.Open();
				}

				//SqlCommand sp_GetPropertyItemsDescInNode = new SqlCommand("GetPropertyItemsDescInNode", Connection.getInstance().HatchConn);

				//ApplicationHelper.TestDatabase_CreateSPL_GetPropertyItemsDescInNodeExt();
				SqlCommand sp_GetPropertyItemsDescInNode = new SqlCommand("GetPropertyItemsDescInNodeExt", Connection.getInstance().HatchConn);
				sp_GetPropertyItemsDescInNode.CommandType = CommandType.StoredProcedure;
                                
                lvis = new List<ListViewItem>();

				sp_GetPropertyItemsDescInNode.Parameters.AddWithValue("@NodeId", nodeId);			
				dataReader = sp_GetPropertyItemsDescInNode.ExecuteReader();
				while (dataReader.Read())
				{                    
					Guid	propertyItemId		= dataReader.GetGuid(0);
					DateTime dtFirstEntered		= dataReader.GetDateTime(1);
					
					string fullName				= string.Empty;
					if (DBNull.Value != dataReader[2])
						fullName				= dataReader.GetString(2);					

					string siteAddress			= string.Empty;
					if (DBNull.Value != dataReader[3])
						siteAddress				= dataReader.GetString(3);
					
					string siteCity				= string.Empty;
					if (DBNull.Value != dataReader[4])
						//TA++:16.04.2005.
						//Bug Nr.: RQ_0003
						//PREVIOUS++
						/*
							siteAddress				= dataReader.GetString(4);
						*/
						//PREVIOUS--

						//NEW++
						siteCity				= dataReader.GetString(4);	
						//NEW--

					string siteState			= string.Empty;
					if (DBNull.Value != dataReader[5])
						siteState				= dataReader.GetString(5);

					string siteCityState		= string.Empty;
					if (DBNull.Value != dataReader[6])
						siteCityState			= dataReader.GetString(6);

					string siteZIP				= string.Empty;
					if (DBNull.Value != dataReader[7])
						siteZIP					= dataReader.GetString(7);

					string mailAddress			= string.Empty;
					if (DBNull.Value != dataReader[8])
						mailAddress				= dataReader.GetString(8);
					
					string mailCity				= string.Empty;
					if (DBNull.Value != dataReader[9])
						mailCity				= dataReader.GetString(9);

					string mailState			= string.Empty;
					if (DBNull.Value != dataReader[10])
						mailState				= dataReader.GetString(10);

					string mailCityState		= string.Empty;
					if (DBNull.Value != dataReader[11])
						mailCityState				= dataReader.GetString(11);

					string mailZIP				= string.Empty;
					if (DBNull.Value != dataReader[12])
						mailZIP					= dataReader.GetString(12);

					decimal homeSqFt			= 0;
					if (DBNull.Value != dataReader[13])
						homeSqFt				= dataReader.GetDecimal(13);

					string listingAgentName		= string.Empty;
					if (DBNull.Value != dataReader[14])
						listingAgentName		= dataReader.GetString(14);

					decimal listingPrice		= 0;
					if (DBNull.Value != dataReader[15])
						listingPrice			= dataReader.GetDecimal(15);

					string listingAgentFaxNr	= string.Empty;
					if (DBNull.Value != dataReader[16])
						listingAgentFaxNr		= dataReader.GetString(16);

					string foreClosureFileDateValue	= string.Empty;
					if (DBNull.Value != dataReader[17])
						foreClosureFileDateValue		= dataReader.GetString(17);

					string foreClosureSaleDate	= string.Empty;
					if (DBNull.Value != dataReader[18])
						foreClosureSaleDate		= dataReader.GetString(18);


					
					ListViewItem lvi = new ListViewItem(string.Empty);

                    lvi.SubItems.Add(foreClosureSaleDate);
                    lvi.SubItems.Add(foreClosureFileDateValue);
                    lvi.SubItems.Add(siteZIP);
                    lvi.SubItems.Add(siteAddress);
                    lvi.SubItems.Add(siteCity);
                    lvi.SubItems.Add(siteState);
                    lvi.SubItems.Add(fullName);					
					lvi.Tag = propertyItemId;

                    lvis.Add(lvi);                    					
				}

                updating = true;
                lvPropertyItems.BeginUpdate();

                lvis.Sort(delegate (ListViewItem i1, ListViewItem i2)
                {
                    int xAsInt, yAsInt;

                    int.TryParse(i1.SubItems[3].Text, out xAsInt);
                    int.TryParse(i2.SubItems[3].Text, out yAsInt);

                    return xAsInt.CompareTo(yAsInt);
                });

                lvPropertyItems.Items.AddRange(lvis.ToArray());
                this.lvPropertyItems.EndUpdate();                                
                updating = false;

                UpdateSelectedRange();
            }
			catch (Exception exc)
			{	
				MessageBox.Show(exc.Message);
			}
			finally
			{
				dataReader.Close();				
				Cursor.Current = Cursors.Arrow;                
            }            
        }

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.label1 = new System.Windows.Forms.Label();
            this.tbSelectedGroup = new System.Windows.Forms.TextBox();
            this.lvPropertyItems = new System.Windows.Forms.ListView();
            this.tag = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.foreclosureSaleDate = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.foreclosureFileDate = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.siteZIP = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.SiteAddr = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.siteCity = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.siteState = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.FullName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.bBrowseGroup = new System.Windows.Forms.Button();
            this._showRoute = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this._selectedRange = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this._filter = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.1F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(5, 33);
            this.label1.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(122, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "STEP 1: Select Group:";
            // 
            // tbSelectedGroup
            // 
            this.tbSelectedGroup.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbSelectedGroup.Location = new System.Drawing.Point(124, 32);
            this.tbSelectedGroup.Margin = new System.Windows.Forms.Padding(1);
            this.tbSelectedGroup.Name = "tbSelectedGroup";
            this.tbSelectedGroup.ReadOnly = true;
            this.tbSelectedGroup.Size = new System.Drawing.Size(290, 20);
            this.tbSelectedGroup.TabIndex = 1;
            // 
            // lvPropertyItems
            // 
            this.lvPropertyItems.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lvPropertyItems.CheckBoxes = true;
            this.lvPropertyItems.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.tag,
            this.foreclosureSaleDate,
            this.foreclosureFileDate,
            this.siteZIP,
            this.SiteAddr,
            this.siteCity,
            this.siteState,
            this.FullName});
            this.lvPropertyItems.FullRowSelect = true;
            this.lvPropertyItems.GridLines = true;
            this.lvPropertyItems.HideSelection = false;
            this.lvPropertyItems.Location = new System.Drawing.Point(11, 100);
            this.lvPropertyItems.Margin = new System.Windows.Forms.Padding(1);
            this.lvPropertyItems.MultiSelect = false;
            this.lvPropertyItems.Name = "lvPropertyItems";
            this.lvPropertyItems.Size = new System.Drawing.Size(487, 250);
            this.lvPropertyItems.TabIndex = 0;
            this.lvPropertyItems.UseCompatibleStateImageBehavior = false;
            this.lvPropertyItems.View = System.Windows.Forms.View.Details;
            this.lvPropertyItems.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.lvPropertyItems_ColumnClick);
            this.lvPropertyItems.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.lvPropertyItems_ItemChecked);
            // 
            // tag
            // 
            this.tag.Text = "Ignore";
            this.tag.Width = 50;
            // 
            // foreclosureSaleDate
            // 
            this.foreclosureSaleDate.Text = "Foreclosure Sale Date";
            this.foreclosureSaleDate.Width = 120;
            // 
            // foreclosureFileDate
            // 
            this.foreclosureFileDate.Text = "Foreclosure File Date";
            this.foreclosureFileDate.Width = 120;
            // 
            // siteZIP
            // 
            this.siteZIP.Text = "Site ZIP";
            // 
            // SiteAddr
            // 
            this.SiteAddr.Text = "Site Address";
            this.SiteAddr.Width = 120;
            // 
            // siteCity
            // 
            this.siteCity.Text = "Site City";
            this.siteCity.Width = 100;
            // 
            // siteState
            // 
            this.siteState.Text = "Site State";
            this.siteState.Width = 100;
            // 
            // FullName
            // 
            this.FullName.Text = "Full Name";
            this.FullName.Width = 167;
            // 
            // bBrowseGroup
            // 
            this.bBrowseGroup.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.bBrowseGroup.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.bBrowseGroup.Location = new System.Drawing.Point(417, 31);
            this.bBrowseGroup.Margin = new System.Windows.Forms.Padding(1);
            this.bBrowseGroup.Name = "bBrowseGroup";
            this.bBrowseGroup.Size = new System.Drawing.Size(27, 22);
            this.bBrowseGroup.TabIndex = 3;
            this.bBrowseGroup.Text = "...";
            this.bBrowseGroup.Click += new System.EventHandler(this.bBrowseGroup_Click);
            // 
            // _showRoute
            // 
            this._showRoute.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._showRoute.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._showRoute.Location = new System.Drawing.Point(188, 382);
            this._showRoute.Margin = new System.Windows.Forms.Padding(1);
            this._showRoute.Name = "_showRoute";
            this._showRoute.Size = new System.Drawing.Size(81, 26);
            this._showRoute.TabIndex = 12;
            this._showRoute.Text = "Show Route";
            this._showRoute.Click += new System.EventHandler(this._showRoute_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.1F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(5, 62);
            this.label2.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(366, 13);
            this.label2.TabIndex = 13;
            this.label2.Text = "STEP 2: Decide on your Route (max. 23 address points) - Select ZIP Codes:";
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.1F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(5, 360);
            this.label3.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(175, 16);
            this.label3.TabIndex = 14;
            this.label3.Text = "STEP 3: Select Address Range";
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.1F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(5, 387);
            this.label4.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(181, 18);
            this.label4.TabIndex = 15;
            this.label4.Text = "STEP 4: Get Optimized Route";
            // 
            // _selectedRange
            // 
            this._selectedRange.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._selectedRange.FormattingEnabled = true;
            this._selectedRange.Location = new System.Drawing.Point(189, 359);
            this._selectedRange.Margin = new System.Windows.Forms.Padding(1);
            this._selectedRange.Name = "_selectedRange";
            this._selectedRange.Size = new System.Drawing.Size(80, 21);
            this._selectedRange.TabIndex = 16;
            this._selectedRange.SelectedIndexChanged += new System.EventHandler(this._selectedRange_SelectedIndexChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(5, 2);
            this.label5.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(335, 13);
            this.label5.TabIndex = 17;
            this.label5.Text = "Doorknocker Route Mapping (Optimized for shortest driving distance).";
            // 
            // _filter
            // 
            this._filter.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._filter.Location = new System.Drawing.Point(417, 57);
            this._filter.Margin = new System.Windows.Forms.Padding(1);
            this._filter.Name = "_filter";
            this._filter.Size = new System.Drawing.Size(27, 22);
            this._filter.TabIndex = 18;
            this._filter.Text = "...";
            this._filter.Click += new System.EventHandler(this._filter_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.1F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(49, 81);
            this.label6.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(314, 13);
            this.label6.TabIndex = 19;
            this.label6.Text = "(on the next screen you\'ll have the option to select 23 at the time)";
            // 
            // PropertyItemListViewControlRouting
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.Controls.Add(this.label6);
            this.Controls.Add(this._filter);
            this.Controls.Add(this.label5);
            this.Controls.Add(this._selectedRange);
            this.Controls.Add(this._showRoute);
            this.Controls.Add(this.lvPropertyItems);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.bBrowseGroup);
            this.Controls.Add(this.tbSelectedGroup);
            this.Controls.Add(this.label1);
            this.Margin = new System.Windows.Forms.Padding(1);
            this.Name = "PropertyItemListViewControlRouting";
            this.Size = new System.Drawing.Size(504, 412);
            this.Load += new System.EventHandler(this.PropertyItemListViewControl_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		#endregion

		#region � Methods �		
		#region � PropertyItemListViewControl_Load �
		private void PropertyItemListViewControl_Load(object sender, System.EventArgs e)
		{
			this.tbSelectedGroup.Text = Globals.selectedNodeName.TrimEnd();
		}
		#endregion � PropertyItemListViewControl_Load �
		#region � lvPropertyItems_ColumnClick �
		private void lvPropertyItems_ColumnClick(object sender, System.Windows.Forms.ColumnClickEventArgs e)
		{
			// Set the ListViewItemSorter property to a new ListViewItemComparer 
			// object. Setting this property immediately sorts the 
			// ListView using the ListViewItemComparer object.			
			this.asc = !this.asc;
			if (true == this.asc)
				this.lvPropertyItems.ListViewItemSorter = new ListViewItemComparer(e.Column);
			else
				this.lvPropertyItems.ListViewItemSorter = new ListViewItemComparerDesc(e.Column);

		}
		#endregion � lvPropertyItems_ColumnClick �
		#region � bBrowseGroup_Click �
		private void bBrowseGroup_Click(object sender, System.EventArgs e)
		{
			SelectGroupForm sgf = new SelectGroupForm();
			sgf.ShowDialog();
			//Hashtable htNodePosPropertyItemId = ApplicationHelper.GetPropertyItemsInNode(sgf.customizedTreeViewUserControl1.SelectedNodeId);
			this.currentSelectedGroup = sgf.customizedTreeViewUserControl1.SelectedNodeId;
			this.tbSelectedGroup.Text = sgf.customizedTreeViewUserControl1.tv_Nodes.SelectedNode.Text;
			//this.ShowData(htNodePosPropertyItemId);
			this.ShowData_FastAccess(sgf.customizedTreeViewUserControl1.SelectedNodeId);
			//Globals.oldListView = this;
		}
		#endregion � bBrowseGroup_Click �
		
		
        private void _showRoute_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem lvi in lvPropertyItems.Items)            
                lvi.ForeColor = Color.Black;
            
            /*RouteQuality rq = CheckWayPoints();

            bool showQualityDialog = false;

            foreach (var aq in rq.AddressQuality)
            {
                if (aq.Value != "AAA")
                {
                    foreach (ListViewItem lvi in lvPropertyItems.Items)
                    {                        
                        var address = lvi.SubItems[4].Text;
                        if (aq.Key.Contains(address) && 
                            "AAA" != aq.Value.Substring(aq.Value.Length - 3, 3)
                        )
                        {
                            lvi.ForeColor = Color.Red;
                            showQualityDialog = true;
                        }
                    }                    
                }
            }

            if (showQualityDialog)
            {
                DialogResult dr = MessageBox.Show($"Addresses marked red are not 100% accurate.{System.Environment.NewLine}{System.Environment.NewLine}Click Cancel button to alter or exclude those addresses, or click OK button to continue.", "ProfitGrabber Pro", MessageBoxButtons.OKCancel, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1 );

                if (dr == DialogResult.Cancel)
                    return;
            }*/

            ArrayList taggedItems = new ArrayList();
            for (int i = 0; i < this.lvPropertyItems.Items.Count; i++)
            {
                ListViewItem lvi = (ListViewItem)this.lvPropertyItems.Items[i];
                if (1 == (int)lvi.Tag)
                {
                    taggedItems.Add(lvi);
                }
            }

            if (taggedItems.Count > 0)
            {
                RoutingForm rf = new RoutingForm(taggedItems, true);                
                rf.ShowDialog();
            }
        }


        #endregion � Methods �


        void UpdateSelectedRange()
        {
            _selectedRange.SelectedText = string.Empty;
            _selectedRange.SelectedItem = null;
            _selectedRange.Text = string.Empty;

            _selectedRange.Items.Clear();
            int active = 0;
            foreach (ListViewItem lvi in this.lvPropertyItems.Items)
                if (!lvi.Checked)
                    active++;

            int nrOfRange = active / 23;
            if (0 != active % 23)
                nrOfRange++;

            if (1 == nrOfRange)
            {                              
                _selectedRange.Items.Add(new CustomRange { min = 1, max = active});
            }
            else
            {
                for (int i = 0; i < nrOfRange; i++)
                {
                    int minVal = i * 23 + 1;
                    int maxVal = (i == nrOfRange - 1) ? active : i * 23 + 23;
                    
                    _selectedRange.Items.Add(new CustomRange { min = minVal, max = maxVal});
                }
            }                        
        }
		
		private delegate void RefreshListView(int nodeId);
		private void NewContactInGroupBy123Sold(int nodeId)
		{
			if (Globals.SelectedGroupInListView == nodeId)
			{																
				ShowData_FastAccess(nodeId);					
			}
		}

        private void lvPropertyItems_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            if (loaded && !updating)
            {
                UpdateSelectedRange();
                ResetLVBackColor();
                ResetTag();
            }
        }

        void ResetLVBackColor()
        {
            for (int i = 0; i < lvPropertyItems.Items.Count; i++)
                lvPropertyItems.Items[i].BackColor = Color.White;
        }

        void ResetTag()
        {
            for (int i = 0; i < lvPropertyItems.Items.Count; i++)
                lvPropertyItems.Items[i].Tag = 0;
        }

        private void _selectedRange_SelectedIndexChanged(object sender, EventArgs e)
        {
            ResetLVBackColor();
            ResetTag();

            CustomRange cr = ((ComboBox)sender).SelectedItem as CustomRange;
            int cnt = cr.max - cr.min + 1;

            if (1 == cnt)
            {
                lvPropertyItems.Items[cr.max - 1].BackColor = Color.LightGray;
                lvPropertyItems.Items[cr.max - 1].Tag = 1;
            }
            else
            {
                int i = 0;
                int offset = 0;
                while (i < cnt)
                {
                    if (!lvPropertyItems.Items[cr.min - 1 + i + offset].Checked)
                    {
                        lvPropertyItems.Items[cr.min - 1 + i + offset].BackColor = Color.LightGray;
                        lvPropertyItems.Items[cr.min - 1 + i + offset].Tag = 1;
                        i++;
                    }
                    else
                        offset++;
                }
            }
        }

        RouteQuality CheckWayPoints()
        {
            System.Collections.Generic.List<string> locations = new System.Collections.Generic.List<string>();
            foreach (ListViewItem lvi in lvPropertyItems.Items)
            {
                if (1 == (int)lvi.Tag)
                {
                    string location = string.Format("{0} {1}, {2} {3}", lvi.SubItems[3].Text, lvi.SubItems[4].Text, lvi.SubItems[5].Text, lvi.SubItems[6].Text);
                    locations.Add(location);
                }
            }
            
            string requestId = Guid.NewGuid().ToString();

            using (var wb = new WebClient())
            {
                var data = new NameValueCollection();
                data["keyCode"] = Globals.RegistrationRegistration.KeyCode;
                data["id"] = requestId;
                data["locations"] = JsonConvert.SerializeObject(locations);

                string url = "https://www.profitgrabber.com/DoorKnockingModule/api/route/checkpoints";

                var response = wb.UploadValues(url, "POST", data);
                string responseInString = Encoding.UTF8.GetString(response);

                var routeQuality = JsonConvert.DeserializeObject<RouteQuality>(responseInString);

                return routeQuality;
            }
        }

        private void _filter_Click(object sender, EventArgs e)
        {
            List<string> zipsFound = new List<string>();

            SqlCommand sp_GetPropertyItemsDescInNode = new SqlCommand("GetPropertyItemsDescInNodeExt", Connection.getInstance().HatchConn);
            sp_GetPropertyItemsDescInNode.CommandType = CommandType.StoredProcedure;
            sp_GetPropertyItemsDescInNode.Parameters.AddWithValue("@NodeId", this.currentSelectedGroup);

            using (var dataReader = sp_GetPropertyItemsDescInNode.ExecuteReader())
            {
                while (dataReader.Read())
                {
                    var ZIP = string.Empty;
                    if (DBNull.Value != dataReader[7])
                    {
                        ZIP = dataReader.GetString(7);
                        zipsFound.Add(ZIP);
                    }
                }
            }

            var groupsFound = zipsFound.GroupBy(i => i);

            Dictionary<string, int> zipCounts = new Dictionary<string, int>();
            foreach (var grp in groupsFound)
            {
                var zip = grp.Key;
                var total = grp.Count();
                
                zipCounts.Add(zip, total);                
            }

            FilterRoute filterRoute = new FilterRoute(zipCounts);
            filterRoute.ShowDialog();

            var selectedZIPs = filterRoute.CheckedZIPs;

            UpdateListView(selectedZIPs);
        }

        private void UpdateListView(List<string> selectedZIPs)
        {
            if (null == selectedZIPs)
                return;

            List<ListViewItem> tempLvis = new List<ListViewItem>();
            foreach (ListViewItem lvi in lvis)
            {
                if (selectedZIPs.Contains(lvi.SubItems[3].Text))
                {
                    tempLvis.Add(lvi);
                }
            }

            updating = true;
            lvPropertyItems.BeginUpdate();
            lvPropertyItems.Items.Clear();

            tempLvis.Sort(delegate (ListViewItem i1, ListViewItem i2)
            {
                int xAsInt, yAsInt;

                int.TryParse(i1.SubItems[3].Text, out xAsInt);
                int.TryParse(i2.SubItems[3].Text, out yAsInt);

                return xAsInt.CompareTo(yAsInt);
            });

            lvPropertyItems.Items.AddRange(tempLvis.ToArray());
            this.lvPropertyItems.EndUpdate();
            updating = false;

            UpdateSelectedRange();
        }
    }
    #endregion � class PropertyItemListViewControl �

    public class CustomRange
    {
        public int min { get; set; }
        public int max { get; set; }

        public override string ToString()
        {
            return $"{min} - {max}";
        }
    }

    class RouteQuality
    {
        public bool Error { get; set; }
        public bool ValidInput { get; set; }
        public List<KeyValuePair<string, string>> AddressQuality { get; set; }                
    }


}
#endregion � Namespace DealMaker � 