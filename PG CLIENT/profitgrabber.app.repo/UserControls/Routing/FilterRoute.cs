﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Windows.Forms;

namespace DealMaker.UserControls.Routing
{
    public partial class FilterRoute : Form
    {
        private bool Asc { get; set; }        
        public List<string> CheckedZIPs { get; private set; }

        public FilterRoute(Dictionary<string, int> zipDistribution)
        {
            InitializeComponent();
            AddEventHanlders();
            FillListView(zipDistribution);
        }

        void FillListView(Dictionary<string, int> zipDistribution)
        {
            List<ListViewItem> lviList = new List<ListViewItem>();

            foreach (var kvp in zipDistribution)
            {
                var lvi = new ListViewItem();
                lvi.SubItems.Add($"{kvp.Key}");
                lvi.SubItems.Add($"{kvp.Value}");
                lviList.Add(lvi);
            }

            lviList.Sort(delegate (ListViewItem i1, ListViewItem i2) 
            {
                int xAsInt, yAsInt;

                int.TryParse(i1.SubItems[1].Text, out xAsInt);
                int.TryParse(i2.SubItems[1].Text, out yAsInt);

                return xAsInt.CompareTo(yAsInt);
            });

            //var sortedLviList = lviList.OrderBy(o => o.SubItems[1].Text).ToList();
            //zipDistributionList.Items.AddRange(sortedLviList.ToArray());
            zipDistributionList.Items.AddRange(lviList.ToArray());
        }

        

        private void ZipDistributionList_ColumnClick(object sender, ColumnClickEventArgs e)
        {            
            Asc = !Asc;
            if (true == this.Asc)
                zipDistributionList.ListViewItemSorter = new ItemComparer(e.Column);
            else
                zipDistributionList.ListViewItemSorter = new ItemComparerDesc(e.Column);
        }

        
        private void _OK_Click(object sender, EventArgs e)
        {
            CheckedZIPs = GetCheckedZIPs();
            if (0 == CheckedZIPs.Count())
            {
                MessageBox.Show($"Please select at least one ZIP from the list", "ProfitGrabber Pro", MessageBoxButtons.OK, MessageBoxIcon.Information);
                DialogResult = DialogResult.None;
            }
        }

        private void _cancel_Click(object sender, EventArgs e)
        {

        }

        private void AddEventHanlders()
        {
            zipDistributionList.ColumnClick += ZipDistributionList_ColumnClick;
        }

        private List<string> GetCheckedZIPs()
        {
            var checkedZIPs = new List<string>();

            foreach (ListViewItem lvi in zipDistributionList.Items)
            {
                if (lvi.Checked)
                    checkedZIPs.Add(lvi.SubItems[1].Text);
            }

            return checkedZIPs;
        }

        class ItemComparer : IComparer
        {
            private int col;
            public ItemComparer()
            {
                col = 0;
            }
            public ItemComparer(int column)
            {
                col = column;
            }
            public int Compare(object x, object y)
            {
                int xAsInt, yAsInt;

                int.TryParse(((ListViewItem)x).SubItems[col].Text, out xAsInt);
                int.TryParse(((ListViewItem)y).SubItems[col].Text, out yAsInt);
                
                return xAsInt.CompareTo(yAsInt);
            }
        }

        class ItemComparerDesc : IComparer
        {
            private int col;
            public ItemComparerDesc()
            {
                col = 0;
            }
            public ItemComparerDesc(int column)
            {
                col = column;
            }
            public int Compare(object x, object y)
            {
                int xAsInt, yAsInt;

                int.TryParse(((ListViewItem)x).SubItems[col].Text, out xAsInt);
                int.TryParse(((ListViewItem)y).SubItems[col].Text, out yAsInt);

                return yAsInt.CompareTo(xAsInt);
            }
        }
    }
}
