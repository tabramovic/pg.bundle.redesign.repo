#region � Using �
using System;
using System.Collections;
using System.Drawing;
using System.Data;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Diagnostics;
using System.Linq;
using DealMaker.Export;
using DealMaker.Classes;
using DealMaker.DALC;

//ADDED
using DealMaker.PlainClasses.ReturnMailWizard;
using DealMaker.PlainClasses.ContactTypes;
using DealMaker.PlainClasses.AppendData;
using DealMaker.PlainClasses.SkipTrace;
using ProfitGrabber.RVM.Forms;
using System.Collections.Generic;
using System.Threading.Tasks;
using DataAppendService.Lib.Models.IDI;
using Newtonsoft.Json;


#endregion � Using �
#region � Namespace �
namespace DealMaker
{
    public interface IUpdateTreeViewAsync
    {
        void UpdateTreeViewAsync(int nodeId);
    }

    #region � Class CustomizedTreeViewUserControl �
    /// <summary>
    /// Summary description for CustomizedTreeViewUserControl.
    /// </summary>
    public class CustomizedTreeViewUserControl : System.Windows.Forms.UserControl, IUpdateTreeViewAsync
    {

		#region � Network UDP �
		//sasa 14.11.05

		private delegate void UpdateTreeViewDelegate();

		protected void NetworkUDP_Refresh()
		{
			//			if (true == this.tv_Nodes.InvokeRequired)
			//			{
			//				this.BeginInvoke(new UpdateTreeViewDelegate(UpdateTreeView));
			//				return;
			//			}
			
			if (null != REFRESH_GROUPS)
			{
				alExpandedNodes = this.GetExpandedNodes(this.tv_Nodes.TopNode);	
				REFRESH_GROUPS();
			}
		}

		private void NetworkUDP_ChangedNode(Network.NetworkUDP.Packet p)
		{
			//this.BeginInvoke(new UpdateTreeViewDelegate(NetworkUDP_Refresh));
			Globals.MainForm.BeginInvoke(new UpdateTreeViewDelegate(NetworkUDP_Refresh));
		}
		#endregion � Network UDP �

		public delegate void RefreshHandler();
		public static event RefreshHandler REFRESH_GROUPS;

		public void OnNewGroup()
		{
			if (null != REFRESH_GROUPS)
			{
				REFRESH_GROUPS();
			}
		}

		#region � Data �
		private bool bUpdateGlobals = false;
		ArrayList alExpandedNodes = new ArrayList();
        TreeNode _rootSellerNode = null;
        TreeNode _buyerNode = null;
        TreeNode _lenderNode = null;
		
		int selectedNodeId = -1;
		private string selectedNodeName = string.Empty;
		internal System.Windows.Forms.TreeView tv_Nodes;
		private Hashtable htNodeId_TreeNode = new Hashtable();
		private System.Windows.Forms.ImageList imageListTreeView;
		private System.Windows.Forms.ContextMenu ctxMenu;
		private System.Windows.Forms.MenuItem menuItem1;
		private System.Windows.Forms.TextBox tbSelectedNode;
		private System.Windows.Forms.Button bExpandAll;
		private System.Windows.Forms.Button bCollapseAll;
		private System.Windows.Forms.RichTextBox rtbNodeDesc;
		private System.Windows.Forms.MenuItem menuItem3;
		private System.Windows.Forms.MenuItem menuItem4;
		private System.Windows.Forms.Button bCopyGroup;
		private System.Windows.Forms.Button bNewGroup;
		private System.Windows.Forms.Button bRenameGroup;
		private System.Windows.Forms.Button bDeleteGroup;
		private System.Windows.Forms.MenuItem miDeleteGroup;
		private System.Windows.Forms.MenuItem menuItem2;
		private System.Windows.Forms.MenuItem miExportGroup;
		private System.Windows.Forms.MenuItem miRemoveContacts;
		private System.Windows.Forms.MenuItem menuItem5;
		private System.Windows.Forms.MenuItem _applyParentMarketing;
		private System.Windows.Forms.MenuItem _forceParentMarketing;
        private MenuItem appendDataAll;
        private MenuItem _sendRVM;
		private MenuItem _uploadGroupForTextOrRVM;
        private MenuItem miMoveGroup;
        private MenuItem miCombineGroups;
		private MenuItem _createSkipTracedSubgroups;
		private System.ComponentModel.IContainer components;

		public delegate void NodeChangedEventHandler(object sender, EventArgs e);
		public event NodeChangedEventHandler NodeChanged;

		#endregion � Data �
		#region � CTR �
		public CustomizedTreeViewUserControl()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();	
#if DEBUG
			//we leave some tracing info !
#else
			this.tbSelectedNode.Enabled = false;
			this.tbSelectedNode.Visible = false;
#endif 
			_applyParentMarketing.Click += new EventHandler(On_ApplyParentMarketing_Click);
			_forceParentMarketing.Click += new EventHandler(On_ForceParentMarketing_Click);
            _sendRVM.Click += _sendRVM_Click;
            miMoveGroup.Click += MiMoveGroup_Click;
            miCombineGroups.Click += MiCombineGroups_Click;
            _createSkipTracedSubgroups.Click += _createSkipTracedSubgroups_Click;


			ContactTypesManager.Instance.TestCreateNewContactTypes();			
		}

        private void _createSkipTracedSubgroups_Click(object sender, EventArgs e)
        {
            CreateAndAssignSkipTracedSubgroups();
        }

        private void CreateAndAssignSkipTracedSubgroups()
        {
            if (null == tv_Nodes.SelectedNode)
                return;

            var selectedNode = (Node) tv_Nodes.SelectedNode.Tag;

            var refreshGUI = SkipTraceHelper.TestCreateAppendedSubGroups(selectedNode.nodeId, out int emailFoundNodeId,
                out int mobileFoundNodeId, out int onlyResidentalPhoneFoundNodeId, out int contactInfoNotFoundNodeId);

            SkipTraceHelper.AssignSkiptraceDataToChildNodes(selectedNode.nodeId, emailFoundNodeId, mobileFoundNodeId,
                onlyResidentalPhoneFoundNodeId, contactInfoNotFoundNodeId);

            if (null != REFRESH_GROUPS && refreshGUI)
                REFRESH_GROUPS();

            var tn = SkipTraceHelper.GetTreeNode(tv_Nodes.Nodes[0], selectedNode.nodeId);
            tn.Expand();
        }

        #endregion � CTR �
		#region � Dispose �
		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}
		#endregion � Dispose �
		#region � Properties �
		#region � UpdateGlobals �
		public bool UpdateGlobals
		{
			get { return this.bUpdateGlobals; }
			set { this.bUpdateGlobals = value;}
		}
		#endregion � UpdateGlobals �
		#endregion � Properties �

        public bool AllowMoveAndCombineGroups { set => miCombineGroups.Visible = miMoveGroup.Visible = value; }

        delegate TreeNode GetSelectedTreeNodeDelegate();
        public TreeNode GetSelectedTreeNodeAsync()
        {
            if (this.tv_Nodes.InvokeRequired)
            {                
                return (TreeNode)(BeginInvoke(new GetSelectedTreeNodeDelegate(GetSelectedTreeNodeAsync), null));
            }
            else
                return this.tv_Nodes.SelectedNode;
        }

		public TreeNode SelectedTreeNode
		{
			get 
            {
                //return this.tv_Nodes.SelectedNode;
                return GetSelectedTreeNodeAsync();
            }
			set 
			{                
				this.tv_Nodes.SelectedNode = value;                
			}
		}

        public TreeNode BuyerNode
        {
            get { return _buyerNode; }
        }

        public TreeNode LenderNode
        {
            get { return _lenderNode; }
        }

        public TreeNode RootSellerNode
        {
            get { return _rootSellerNode;}
        }
		

		#region � SelectedNodeId �
		public int SelectedNodeId
		{
			get {return this.selectedNodeId;}
		}
		#endregion � SelectedNodeId �
		#region � SelectedNodeName �
		public string SelectedNodeName
		{
			get {return this.selectedNodeName;}
		}
		#endregion � SelectedNodeName �
		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CustomizedTreeViewUserControl));
            this.tv_Nodes = new System.Windows.Forms.TreeView();
            this.ctxMenu = new System.Windows.Forms.ContextMenu();
            this.menuItem1 = new System.Windows.Forms.MenuItem();
            this.menuItem4 = new System.Windows.Forms.MenuItem();
            this.menuItem3 = new System.Windows.Forms.MenuItem();
            this.miDeleteGroup = new System.Windows.Forms.MenuItem();
            this.miRemoveContacts = new System.Windows.Forms.MenuItem();
            this.menuItem2 = new System.Windows.Forms.MenuItem();
            this.miExportGroup = new System.Windows.Forms.MenuItem();
            this.menuItem5 = new System.Windows.Forms.MenuItem();
            this._applyParentMarketing = new System.Windows.Forms.MenuItem();
            this._forceParentMarketing = new System.Windows.Forms.MenuItem();
            this.appendDataAll = new System.Windows.Forms.MenuItem();
            this._uploadGroupForTextOrRVM = new System.Windows.Forms.MenuItem();
            this._sendRVM = new System.Windows.Forms.MenuItem();
			this._createSkipTracedSubgroups = new MenuItem();
			this.imageListTreeView = new System.Windows.Forms.ImageList(this.components);
            this.tbSelectedNode = new System.Windows.Forms.TextBox();
            this.bExpandAll = new System.Windows.Forms.Button();
            this.bCollapseAll = new System.Windows.Forms.Button();
            this.rtbNodeDesc = new System.Windows.Forms.RichTextBox();
            this.bCopyGroup = new System.Windows.Forms.Button();
            this.bNewGroup = new System.Windows.Forms.Button();
            this.bRenameGroup = new System.Windows.Forms.Button();
            this.bDeleteGroup = new System.Windows.Forms.Button();
            this.miMoveGroup = new System.Windows.Forms.MenuItem();
            this.miCombineGroups = new System.Windows.Forms.MenuItem();
            this.SuspendLayout();
            // 
            // tv_Nodes
            // 
            this.tv_Nodes.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tv_Nodes.ContextMenu = this.ctxMenu;
            this.tv_Nodes.FullRowSelect = true;
            this.tv_Nodes.HideSelection = false;
            this.tv_Nodes.HotTracking = true;
            this.tv_Nodes.ImageIndex = 0;
            this.tv_Nodes.ImageList = this.imageListTreeView;
            this.tv_Nodes.Location = new System.Drawing.Point(0, 8);
            this.tv_Nodes.Name = "tv_Nodes";
            this.tv_Nodes.SelectedImageIndex = 1;
            this.tv_Nodes.Size = new System.Drawing.Size(384, 336);
            this.tv_Nodes.TabIndex = 0;
            this.tv_Nodes.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.tv_Nodes_AfterSelect);
            this.tv_Nodes.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tv_Nodes_KeyDown);
            this.tv_Nodes.MouseDown += new System.Windows.Forms.MouseEventHandler(this.tv_Nodes_MouseDown);
            // 
            // ctxMenu
            // 
            this.ctxMenu.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.menuItem1,
            this.menuItem4,
            this.menuItem3,
            this.miDeleteGroup,
            this.miMoveGroup,
            this.miCombineGroups,
            this.miRemoveContacts,
            this.menuItem2,
            this.miExportGroup,
            this.menuItem5,
            this._applyParentMarketing,
            this._forceParentMarketing,
            this.appendDataAll,
            this._uploadGroupForTextOrRVM,
            this._sendRVM,
			this._createSkipTracedSubgroups});
            // 
            // menuItem1
            // 
            this.menuItem1.Index = 0;
            this.menuItem1.Text = "New Group";
            this.menuItem1.Click += new System.EventHandler(this.addNewSegment_Click);
            // 
            // menuItem4
            // 
            this.menuItem4.Index = 1;
            this.menuItem4.Text = "Copy Group";
            this.menuItem4.Visible = false;
            // 
            // menuItem3
            // 
            this.menuItem3.Index = 2;
            this.menuItem3.Text = "Rename Group";
            this.menuItem3.Click += new System.EventHandler(this.menuItem3_Click);
            // 
            // miDeleteGroup
            // 
            this.miDeleteGroup.Index = 3;
            this.miDeleteGroup.Text = "Delete Group";
            this.miDeleteGroup.Click += new System.EventHandler(this.deleteSegment_Click);
            // 
            // miRemoveContacts
            // 
            this.miRemoveContacts.Index = 6;
            this.miRemoveContacts.Text = "Remove Contacts";
            this.miRemoveContacts.Click += new System.EventHandler(this.miRemoveContacts_Click);
            // 
            // menuItem2
            // 
            this.menuItem2.Index = 7;
            this.menuItem2.Text = "-";
            // 
            // miExportGroup
            // 
            this.miExportGroup.Index = 8;
            this.miExportGroup.Text = "Export Group";
            this.miExportGroup.Click += new System.EventHandler(this.miExportGroup_Click);
            // 
            // menuItem5
            // 
            this.menuItem5.Index = 9;
            this.menuItem5.Text = "-";
            // 
            // _applyParentMarketing
            // 
            this._applyParentMarketing.Enabled = false;
            this._applyParentMarketing.Index = 10;
            this._applyParentMarketing.Text = "Apply Parent Marketing ";
            // 
            // _forceParentMarketing
            // 
            this._forceParentMarketing.Enabled = false;
            this._forceParentMarketing.Index = 11;
            this._forceParentMarketing.Text = "Force Parent Marketing";
            // 
            // appendDataAll
            // 
            this.appendDataAll.Index = 12;
            this.appendDataAll.Text = "Get Phone && Email - Skip Trace This Group";
            this.appendDataAll.Click += new System.EventHandler(this.appendDataAll_Click);
            // 
            // _uploadGroupForTextOrRVM
            // 
            this._uploadGroupForTextOrRVM.Index = 13;
            this._uploadGroupForTextOrRVM.Text = "Upload Group for Text/RVM";
            this._uploadGroupForTextOrRVM.Click += new System.EventHandler(this._uploadGroupForTextOrRVM_Click);
            // 
            // _sendRVM
            // 
            this._sendRVM.Index = 14;
            this._sendRVM.Text = "Send Text/RVM";
			//
			// _createSkipTracedSubgroups
			//
			this._createSkipTracedSubgroups.Index = 15;
			this._createSkipTracedSubgroups.Text = "Create && Assign Skip Traced Subgroups";
			// 
			// imageListTreeView
			// 
			this.imageListTreeView.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListTreeView.ImageStream")));
            this.imageListTreeView.TransparentColor = System.Drawing.Color.Transparent;
            this.imageListTreeView.Images.SetKeyName(0, "");
            this.imageListTreeView.Images.SetKeyName(1, "");
            this.imageListTreeView.Images.SetKeyName(2, "");
            // 
            // tbSelectedNode
            // 
            this.tbSelectedNode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.tbSelectedNode.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.tbSelectedNode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbSelectedNode.Location = new System.Drawing.Point(0, 392);
            this.tbSelectedNode.Name = "tbSelectedNode";
            this.tbSelectedNode.ReadOnly = true;
            this.tbSelectedNode.Size = new System.Drawing.Size(56, 20);
            this.tbSelectedNode.TabIndex = 1;
            // 
            // bExpandAll
            // 
            this.bExpandAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.bExpandAll.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.bExpandAll.Location = new System.Drawing.Point(96, 392);
            this.bExpandAll.Name = "bExpandAll";
            this.bExpandAll.Size = new System.Drawing.Size(96, 23);
            this.bExpandAll.TabIndex = 2;
            this.bExpandAll.Text = "Expand All";
            this.bExpandAll.Click += new System.EventHandler(this.bExpandAll_Click);
            // 
            // bCollapseAll
            // 
            this.bCollapseAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.bCollapseAll.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.bCollapseAll.Location = new System.Drawing.Point(192, 392);
            this.bCollapseAll.Name = "bCollapseAll";
            this.bCollapseAll.Size = new System.Drawing.Size(96, 23);
            this.bCollapseAll.TabIndex = 3;
            this.bCollapseAll.Text = "Collapse All";
            this.bCollapseAll.Click += new System.EventHandler(this.bCollapseAll_Click);
            // 
            // rtbNodeDesc
            // 
            this.rtbNodeDesc.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.rtbNodeDesc.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.rtbNodeDesc.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rtbNodeDesc.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.rtbNodeDesc.Location = new System.Drawing.Point(8, 422);
            this.rtbNodeDesc.Name = "rtbNodeDesc";
            this.rtbNodeDesc.ReadOnly = true;
            this.rtbNodeDesc.Size = new System.Drawing.Size(280, 16);
            this.rtbNodeDesc.TabIndex = 4;
            this.rtbNodeDesc.TabStop = false;
            this.rtbNodeDesc.Text = "";
            // 
            // bCopyGroup
            // 
            this.bCopyGroup.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.bCopyGroup.Enabled = false;
            this.bCopyGroup.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.bCopyGroup.Location = new System.Drawing.Point(56, 392);
            this.bCopyGroup.Name = "bCopyGroup";
            this.bCopyGroup.Size = new System.Drawing.Size(40, 24);
            this.bCopyGroup.TabIndex = 6;
            this.bCopyGroup.Text = "Copy Group";
            this.bCopyGroup.Visible = false;
            // 
            // bNewGroup
            // 
            this.bNewGroup.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.bNewGroup.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.bNewGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bNewGroup.Location = new System.Drawing.Point(0, 352);
            this.bNewGroup.Name = "bNewGroup";
            this.bNewGroup.Size = new System.Drawing.Size(96, 40);
            this.bNewGroup.TabIndex = 5;
            this.bNewGroup.Text = "New Group";
            this.bNewGroup.Click += new System.EventHandler(this.bNewGroup_Click);
            // 
            // bRenameGroup
            // 
            this.bRenameGroup.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.bRenameGroup.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.bRenameGroup.Location = new System.Drawing.Point(96, 352);
            this.bRenameGroup.Name = "bRenameGroup";
            this.bRenameGroup.Size = new System.Drawing.Size(96, 40);
            this.bRenameGroup.TabIndex = 7;
            this.bRenameGroup.Text = "Rename Group";
            this.bRenameGroup.Click += new System.EventHandler(this.bRenameGroup_Click);
            // 
            // bDeleteGroup
            // 
            this.bDeleteGroup.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.bDeleteGroup.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.bDeleteGroup.Location = new System.Drawing.Point(192, 352);
            this.bDeleteGroup.Name = "bDeleteGroup";
            this.bDeleteGroup.Size = new System.Drawing.Size(96, 40);
            this.bDeleteGroup.TabIndex = 8;
            this.bDeleteGroup.Text = "Delete Group";
            this.bDeleteGroup.Click += new System.EventHandler(this.bDeleteGroup_Click);
            // 
            // miMoveGroup
            // 
            this.miMoveGroup.Index = 4;
            this.miMoveGroup.Text = "Move Group";
            this.miMoveGroup.Visible = false;
            // 
            // miCombineGroups
            // 
            this.miCombineGroups.Index = 5;
            this.miCombineGroups.Text = "Combine Groups";
            this.miCombineGroups.Visible = false;
            // 
            // CustomizedTreeViewUserControl
            // 
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.Controls.Add(this.rtbNodeDesc);
            this.Controls.Add(this.bDeleteGroup);
            this.Controls.Add(this.bRenameGroup);
            this.Controls.Add(this.bCopyGroup);
            this.Controls.Add(this.bNewGroup);
            this.Controls.Add(this.bCollapseAll);
            this.Controls.Add(this.bExpandAll);
            this.Controls.Add(this.tbSelectedNode);
            this.Controls.Add(this.tv_Nodes);
            this.Name = "CustomizedTreeViewUserControl";
            this.Size = new System.Drawing.Size(392, 464);
            this.Load += new System.EventHandler(this.CustomizedTreeViewUserControl_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		#endregion
		#region � Methods �
		#region � Build Structure �
		private TreeNode BuildStructure(TreeNode tn)
		{
			#region � Data �
			ArrayList tempTreeNodeList = new ArrayList();
			bool hasChildren = false;			
			#endregion � Data �
			
			// get children nodes
			try
			{	
				Node n = (Node)tn.Tag;
				
				
				//SqlCommand sp_getChildrenFromNode = new SqlCommand("getChildrenFromNode", Connection.getInstance().HatchConn);
				SqlConnection connection = new SqlConnection();				
				connection.ConnectionString = Globals.GetConnectionString();
				connection.Open();
				//MessageBox.Show(connection.ConnectionString);
				SqlCommand sp_getChildrenFromNode = new SqlCommand("getChildrenFromNode", connection);
				sp_getChildrenFromNode.CommandType = CommandType.StoredProcedure;								
				sp_getChildrenFromNode.Parameters.Add("@parentNode", n.nodeId);
									
				if (ConnectionState.Open != Connection.getInstance().HatchConn.State)
					Connection.getInstance().HatchConn.Open();

				sp_getChildrenFromNode.ExecuteNonQuery();

				SqlDataReader nodeReader = sp_getChildrenFromNode.ExecuteReader();

				while (nodeReader.Read())
				{
					Int32	nodeId				= nodeReader.GetInt32(0);
					Int32	parentNodeId		= nodeReader.GetInt32(1);					
					string	nodeDesc			= nodeReader.GetString(2).Trim();
					string	nodeName			= nodeReader.GetString(3).Trim();
					SqlBoolean isPreDefined		= nodeReader.GetBoolean(4);

					TreeNode tempTreeNode = new TreeNode(nodeName);
					
					//if (true == isPreDefined.Value)
					if (
                            nodeId <= 28
                            || nodeId == ContactTypesManager.Instance.CashBuyersGroupId
                            || nodeId == ContactTypesManager.Instance.PrivateLenderGroupId
                        )
						    tempTreeNode.ForeColor = Color.Blue;

                                        
					tempTreeNode.Tag = new Node(nodeId, parentNodeId, nodeDesc, nodeName, false, isPreDefined.Value);
					tempTreeNodeList.Add(tempTreeNode);
					hasChildren = true;

                    if (1 == nodeId)
                        _rootSellerNode = tempTreeNode;

                    if (ContactTypesManager.Instance.CashBuyersGroupId == nodeId)
                        _buyerNode = tempTreeNode;

                    if (ContactTypesManager.Instance.PrivateLenderGroupId == nodeId)
                        _lenderNode = tempTreeNode;


					
					#region � Test For Expanded Property �
					foreach (TreeNode expandedTreeNode in this.alExpandedNodes)
					{
						Node node = (Node)expandedTreeNode.Tag;
						if (node.nodeId == nodeId)
						{	
							Node savedNode = (Node)tempTreeNode.Tag;
							savedNode.isExpanded = true;
						}
					}
					#endregion � Test For Expanded Property �

					//Add Nodes to the hashtable;
					//Key = nodeId, Value = TreeNode
					if (false == this.htNodeId_TreeNode.Contains(nodeId))
					{
						this.htNodeId_TreeNode.Add(nodeId, tempTreeNode);
					}

				}				
				nodeReader.Close();
				//TA++:23.05.2005.
				//Max Pool Size was reached
				connection.Close();
				//TA--:23.05.2005.
			}
			catch( Exception ex)
			{
				MessageBox.Show(ex.ToString());
				return null;
			}

			// has children
			if (true == hasChildren)
			{
				while (tempTreeNodeList.Count > 0)
				{	
					TreeNode tnTemp = (TreeNode)tempTreeNodeList[0];
					tempTreeNodeList.RemoveAt(0);
					TreeNode outTn = BuildStructure(tnTemp);
				
					tn.Nodes.Add(outTn);
				}
				return tn;
			}
			else
			{
				return tn;
			}
		}
		#endregion � Build Structure �				

		#region � UpdateTreeView �        
		public void UpdateTreeView()
		{
            if (this.InvokeRequired)
                this.BeginInvoke(new UpdateTreeViewDelegate(UpdateTreeView), null);
            else
            {
                // Display a wait cursor while the TreeNodes are being created.
                Cursor.Current = Cursors.WaitCursor;

                // Suppress repainting the TreeView until all the objects have been created.
                tv_Nodes.BeginUpdate();

                // Clear the TreeView each time the method is called.
                tv_Nodes.Nodes.Clear();

                TreeNode tn = new TreeNode(Globals.RootNode, 2, 2);
                tn.Tag = new Node(1);
                tn.Expand();

                this.htNodeId_TreeNode.Clear();
                TreeNode fullTreeNode = BuildStructure(tn);

                if (null != fullTreeNode)
                {
                    //tv_Nodes.Nodes.Insert(1, fullTreeNode);
                    this.InsertNode(1, fullTreeNode);
                }
                else
                {
                    MessageBox.Show("ERROR!!!");
                }
                this.RefreshExpandedNodes();

                // Reset the cursor to the default for all controls.
                Cursor.Current = Cursors.Default;

                // End repainting the TreeView
                tv_Nodes.EndUpdate();
            }
		}
		#endregion � UpdateTreeView �	
	
		private delegate void InsertNodeDelegate(int idx, TreeNode fullTreeNode);
		private void InsertNode(int idx, TreeNode fullTreeNode)
		{
			if (true == this.tv_Nodes.InvokeRequired)
			{
				this.Invoke(new InsertNodeDelegate(InsertNode), new object[] { idx, fullTreeNode });
			}
			if (false == this.tv_Nodes.Nodes.Contains(fullTreeNode))
			{
				this.tv_Nodes.Nodes.Insert(idx, fullTreeNode);
			}
		}


        public void UpdateTreeViewAsync(int nodeId)
        {
            UpdateTreeViewAsyncInternal(nodeId);
        }

        delegate void UpdateTreeViewDelegateForNode(int nodeId);
        private void UpdateTreeViewAsyncInternal(int nodeId)
        {
            if (this.InvokeRequired)
                this.BeginInvoke(new UpdateTreeViewDelegateForNode(UpdateTreeView), new object[] { nodeId });            
        }

        #region � UpdateTreeView(int nodeId) �
        private void UpdateTreeView(int nodeId)
		{			
			TreeNode tnSelected = new TreeNode();

			// Display a wait cursor while the TreeNodes are being created.
			Cursor.Current = Cursors.WaitCursor;
        
			// Suppress repainting the TreeView until all the objects have been created.
			tv_Nodes.BeginUpdate();

			// Clear the TreeView each time the method is called.
			tv_Nodes.Nodes.Clear();	

			TreeNode tn = new TreeNode(Globals.RootNode, 2, 2);
			tn.Tag = new Node(1);
			tn.Expand();

			this.htNodeId_TreeNode.Clear();
			TreeNode fullTreeNode = BuildStructure(tn);

			if (null != fullTreeNode)
			{
				tv_Nodes.Nodes.Insert(1, fullTreeNode);
			}
			else
			{
				MessageBox.Show("ERROR!");
			}
			
			
			// Reset the cursor to the default for all controls.
			Cursor.Current = Cursors.Default;
			
			//tvStruktura.ExpandAll();			
			
			tnSelected = (TreeNode)this.htNodeId_TreeNode[nodeId];
			tv_Nodes.SelectedNode = tnSelected;
			tv_Nodes.SelectedNode.Expand();
			this.rtbNodeDesc.Text = ((Node)tnSelected.Tag).nodeDesc;

			this.RefreshExpandedNodes();

			// Begin repainting the TreeView.
			tv_Nodes.EndUpdate();
		}
		#endregion � UpdateTreeView(int nodeId) �	
		#region � Refresh Expanded Nodes �
		private void RefreshExpandedNodes()
		{
			IDictionaryEnumerator ide = this.htNodeId_TreeNode.GetEnumerator();
			while (ide.MoveNext())
			{
				TreeNode tn = (TreeNode)ide.Value;
				Node n = (Node)tn.Tag;
				if (true == n.isExpanded)
					tn.Expand();
			}
		}
		#endregion � Refresh Expanded Nodes �
		#region � CustomizedTreeViewUserControl_Load �
		private void CustomizedTreeViewUserControl_Load(object sender, System.EventArgs e)
		{
			//bottom = 200;
			Size parentSize = this.Parent.Size;
			this.Size = new Size(this.Size.Width, parentSize.Height - 25);
			this.tv_Nodes.Size = new Size(this.tv_Nodes.Width, this.Size.Height - 120);
			//this.tv_Nodes.SelectedNode = 
			UpdateTreeView();
			this.tv_Nodes.Focus();

			//sasa, 13.11.05: nw broadcast udp packet changed node
			Network.NetworkUDP.ChangedNode += new DealMaker.Network.NetworkChangedEventHandler(NetworkUDP_ChangedNode);
		}
		#endregion � CustomizedTreeViewUserControl_Load �
		#region � addNewSegment_Click �
		private void bNewGroup_Click(object sender, System.EventArgs e)
		{
			this.addNewSegment_Click(sender, e);
		}
		private void addNewSegment_Click(object sender, System.EventArgs e)
		{
			alExpandedNodes = this.GetExpandedNodes(this.tv_Nodes.TopNode);	
			
			//add group
			TreeNode tn = tv_Nodes.SelectedNode;

			if (null == tn)
			{
				MessageBox.Show("Select group");
				return;
			}
			

			Node n = (Node)tn.Tag;
			
			CreateUpdateGroup ns = new CreateUpdateGroup();
			ns.ShowDialog();

			if (true == ns.Valid)
			{
				try
				{
					int retVal = 0;
					int newNodeId = 0;

					//SqlCommand sp_dodaj_Node = new SqlCommand("dodaj_Node", Connection.getInstance().HatchConn);
					SqlConnection connection = new SqlConnection();
					connection.ConnectionString = Globals.GetConnectionString();
					connection.Open();
					SqlCommand sp_dodaj_Node = new SqlCommand("dodaj_Node", connection);

					sp_dodaj_Node.CommandType = CommandType.StoredProcedure;
					
					sp_dodaj_Node.Parameters.Add("@parentNode", n.nodeId);
					sp_dodaj_Node.Parameters.Add("@nodeDesc", ns.NodeDesc);
					sp_dodaj_Node.Parameters.Add("@nodeName", ns.NodeName);
					

					SqlParameter param = sp_dodaj_Node.Parameters.Add("@retValue", retVal);
					param.Direction = ParameterDirection.Output;

					SqlParameter new_NodeId_param = sp_dodaj_Node.Parameters.Add("@newNodeId ", newNodeId);
					new_NodeId_param.Direction = ParameterDirection.Output;
									
					if (ConnectionState.Open != Connection.getInstance().HatchConn.State)
						Connection.getInstance().HatchConn.Open();

					sp_dodaj_Node.ExecuteNonQuery();
	
					if (-1 == (int)param.Value)
					{
						MessageBox.Show("Error while adding node!");
					}
					else
					{
						//UpdateTreeView();
						newNodeId = (System.Int32)new_NodeId_param.Value;
						UpdateTreeView(newNodeId);

						//sasa, 13.11.05: nw broadcast udp packet changed node
						Network.NetworkUDP.Broadcast(n);
					}
					//TA NewGroup
					OnNewGroup();
				}
				catch( Exception ex)
				{
					MessageBox.Show(ex.ToString());
					return; 
				}
			}
		}
		#endregion � addNewSegment_Click �
		#region � deleteSegment_Click �
		private void bDeleteGroup_Click(object sender, System.EventArgs e)
		{
			this.deleteSegment_Click(sender, e);
		}
		private void deleteSegment_Click(object sender, System.EventArgs e)
		{			
			this.rtbNodeDesc.Text = string.Empty;
			// delete group
			TreeNode tn = tv_Nodes.SelectedNode;

			if (null == tn)
				return;

			Node n = (Node)tn.Tag;

			if (n.nodeId <= 28)
			{
				//MessageBox.Show("deleting root node !?!?!?!");
				MessageBox.Show("You are not allowed to delete predefined group!");
				return;
			}
			if (true == n.isPreDefined)
			{
				MessageBox.Show("You are not allowed to delete predefined group!");
				return;
			}

			TreeNode parentNode = tn.Parent;			
			
			DeleteGroup bris = new DeleteGroup();
			bris.Description = n.nodeName;
			bris.ShowDialog();

			if (true == bris.Valid)
			{
				if (false == bris.Brisi)				
					return;				
			}
			else
				return;

			alExpandedNodes = this.GetExpandedNodes(this.tv_Nodes.TopNode);	
			ArrayList alNodesToBeDeleted = this.GetNodeChildren(tn);

			foreach (TreeNode tnTemp in alNodesToBeDeleted)
			{
				if (alExpandedNodes.Contains(tnTemp))
					alExpandedNodes.Remove(tnTemp);
			}
			
			try
			{
				foreach (TreeNode tnDelete in alNodesToBeDeleted)
				{
					n = (Node)tnDelete.Tag;
					
					if (true == bris.Destroy)					
					{
						ApplicationHelper.DeletePropertyItemsFromGroups(null, n.nodeId, 1);
					}
					else
					{
						ApplicationHelper.DeletePropertyItemsFromGroups(null, n.nodeId, 0);
					}
										
					int retVal = 0;
					//SqlCommand sp_delete_Node = new SqlCommand("delete_Node", Connection.getInstance().HatchConn);
					SqlConnection connection = new SqlConnection();
					connection.ConnectionString = Globals.GetConnectionString();
					connection.Open();
					SqlCommand sp_delete_Node = new SqlCommand("delete_Node", connection);
					sp_delete_Node.CommandType = CommandType.StoredProcedure;					
					sp_delete_Node.Parameters.Add("@nodeId", n.nodeId);	

					SqlParameter param = sp_delete_Node.Parameters.Add("@retVal", retVal);
					param.Direction = ParameterDirection.Output;
									
					if (ConnectionState.Open != Connection.getInstance().HatchConn.State)
						Connection.getInstance().HatchConn.Open();

					sp_delete_Node.ExecuteNonQuery();
	
					if (-1 == (int)param.Value)
					{
						MessageBox.Show("Error");
					}
					else
					{
						UpdateTreeView();

						//sasa, 13.11.05: nw broadcast udp packet changed node
						Network.NetworkUDP.Broadcast(n);
					}
				}
				this.tv_Nodes.SelectedNode = parentNode;
				//TA NewGroup
				OnNewGroup();
			}
			catch( Exception ex)
			{
				MessageBox.Show(ex.ToString());
				return; 
			}	
		}
		#endregion � deleteSegment_Click �
		#region � tv_Nodes_MouseDown �
		private void tv_Nodes_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			if (e.Button == MouseButtons.Left)
			{					
				return;
			}
			Control ctrl = (Control)sender;
			Point clickedPoint = new Point(e.X, e.Y);
			TreeView tvNodes = (TreeView)sender;

			TreeNodeCollection nodes = tvNodes.Nodes;
			IEnumerator ieNodes = nodes.GetEnumerator();
			string s = string.Empty;
			while (ieNodes.MoveNext())
			{
				TreeNode tn = (TreeNode)ieNodes.Current;
				this.SelectNewNode(tn, clickedPoint);
			}

#if DEBUG
			Node n = (Node)this.tv_Nodes.SelectedNode.Tag;
			this.tbSelectedNode.Text = n.nodeId.ToString() + ", " + n.nodeDesc + ", " + n.nodeName;
#endif
			this.tv_Nodes.SelectedNode.Expand();		
		}
		#endregion � tv_Nodes_MouseDown �
		#region � SelectNewNode �
		private void SelectNewNode(TreeNode currentTreeNode, Point clickedPoint)
		{
			Rectangle rect = (Rectangle)currentTreeNode.Bounds;								
			Rectangle expandedRect = new Rectangle(0 , rect.Y, this.tv_Nodes.Width , rect.Height);

			if (expandedRect.Contains(clickedPoint))
			{
				this.tv_Nodes.SelectedNode = currentTreeNode;						
				return;
			}
			else
			{
				TreeNodeCollection tnc = currentTreeNode.Nodes;
				IEnumerator ieNodes = tnc.GetEnumerator();

				while (ieNodes.MoveNext())
				{
					TreeNode tn = (TreeNode)ieNodes.Current;
					SelectNewNode(tn, clickedPoint);						
				}				
			}			
		}
		#endregion � SelectNewNode �
		#region � GetNodeChildren �
		private ArrayList GetNodeChildren (TreeNode currentTreeNode)
		{
			ArrayList alChildrenNodes = new ArrayList();
			TreeNodeCollection tnc = currentTreeNode.Nodes;
			IEnumerator ieNodes = tnc.GetEnumerator();
			
			while (ieNodes.MoveNext())
			{
				TreeNode tn = (TreeNode)ieNodes.Current;	
				alChildrenNodes.AddRange(GetNodeChildren(tn));
			}
			alChildrenNodes.Add(currentTreeNode);
			return alChildrenNodes;
		}
		#endregion � GetNodeChildren �
		#region � GetExpandedNodes �
		public ArrayList GetExpandedNodes(TreeNode treeNode)
		{
			ArrayList alExpandedNodes = new ArrayList();
			
			TreeNodeCollection tnc = treeNode.Nodes;
			IEnumerator ieNodes = tnc.GetEnumerator();
			
			while (ieNodes.MoveNext())
			{
				TreeNode tn = (TreeNode)ieNodes.Current;
				if (tn.IsExpanded)
					alExpandedNodes.Add(tn);
				alExpandedNodes.AddRange(GetExpandedNodes(tn));
			}			
			return alExpandedNodes;	
		}
		#endregion � GetExpandedNodes �		
		#region � bExpandAll_Click �
		private void bExpandAll_Click(object sender, System.EventArgs e)
		{
			this.tv_Nodes.ExpandAll();
		}
		#endregion � bExpandAll_Click �
		#region � bCollapseAll_Click �
		private void bCollapseAll_Click(object sender, System.EventArgs e)
		{
			this.tv_Nodes.CollapseAll();
		}
		#endregion � bCollapseAll_Click �						
		#region � tv_Nodes_KeyDown �
		private void tv_Nodes_KeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
		{
			if (null != ((TreeView)sender).SelectedNode)
			{
				this.rtbNodeDesc.Text = ((Node)((TreeView)sender).SelectedNode.Tag).nodeDesc;
				if (e.KeyData == Keys.Delete)	//delete
				{
					this.deleteSegment_Click(sender, EventArgs.Empty);
				}
				if (e.KeyData == Keys.F2)		//rename group
				{
					this.bRenameGroup_Click(sender, e);
				}	
				if (e.KeyData == Keys.F3)		//new group
				{
					this.addNewSegment_Click(sender, e);
				}
				if (e.KeyData == Keys.F4)		//copy group
				{

				}
			}
		}
		#endregion � tv_Nodes_KeyDown �
		#region � tv_Nodes_AfterSelect �
		private void tv_Nodes_AfterSelect(object sender, System.Windows.Forms.TreeViewEventArgs e)
		{
			this.rtbNodeDesc.Text = ((Node)((TreeView)sender).SelectedNode.Tag).nodeDesc;
			if (true == this.UpdateGlobals)
			{
				Globals.selectedNodeId = ((Node)((TreeView)sender).SelectedNode.Tag).nodeId;
				Globals.selectedNodeName = ((Node)((TreeView)sender).SelectedNode.Tag).nodeName;
			}
			this.selectedNodeId = ((Node)((TreeView)sender).SelectedNode.Tag).nodeId;
			this.selectedNodeName = ((Node)((TreeView)sender).SelectedNode.Tag).nodeName;

			//MFW
			bool isAutoGeneratedGroup = SkipTracedContactAssistant.Instance.IsAutoGeneratedGroup((Node)((TreeView)sender).SelectedNode.Tag);						
			_applyParentMarketing.Enabled = isAutoGeneratedGroup;
			_forceParentMarketing.Enabled = isAutoGeneratedGroup;
			//~MFW
		


			if (NodeChanged != null)
				NodeChanged(this, e);
		}
		#endregion � tv_Nodes_AfterSelect �
		#region � Update Group �		
		private void bRenameGroup_Click(object sender, System.EventArgs e)
		{
			this.menuItem3_Click(sender, e);
		}

		private void menuItem3_Click(object sender, System.EventArgs e)
		{
			TreeNode tn = tv_Nodes.SelectedNode;
			if (null == tn)
				return;
			Node n = (Node)tn.Tag;
			if (n.nodeId == 1)
			{				
				return;
			}
			if (
                    true == n.isPreDefined 
                    || n.nodeId == ContactTypesManager.Instance.PrivateLenderGroupId
                    || n.nodeId == ContactTypesManager.Instance.CashBuyersGroupId
                )
			{
				MessageBox.Show("You are not allowed to modify predefined groups !");
				return;
			}
			CreateUpdateGroup group = new CreateUpdateGroup();
			group.NodeName = n.nodeName;
			group.NodeDesc = n.nodeDesc;
			group.ShowDialog();

			if (true == group.Valid)
			{
				alExpandedNodes = this.GetExpandedNodes(this.tv_Nodes.TopNode);	
				//update group
				try
				{										
					//SqlCommand sp_update_Node = new SqlCommand("update_Node", Connection.getInstance().HatchConn);
					SqlConnection connection = new SqlConnection();
					connection.ConnectionString = Globals.GetConnectionString();
					connection.Open();
					SqlCommand sp_update_Node = new SqlCommand("update_Node", connection);
					sp_update_Node.CommandType = CommandType.StoredProcedure;			
					sp_update_Node.Parameters.Add("@nodeId", n.nodeId);
					sp_update_Node.Parameters.Add("@nodeName", group.NodeName);
					sp_update_Node.Parameters.Add("@nodeDesc", group.NodeDesc);

					if (ConnectionState.Open != Connection.getInstance().HatchConn.State)
						Connection.getInstance().HatchConn.Open();

					sp_update_Node.ExecuteNonQuery();

					//sasa, 13.11.05: nw broadcast udp packet changed node
					Network.NetworkUDP.Broadcast(n);

					UpdateTreeView(n.nodeId);
					//TA NewGroup
					OnNewGroup();
				}
				catch( Exception ex)
				{
					MessageBox.Show(ex.ToString());
					return; 
				}	
			}
		}
		#endregion � Update Group �
		#region � miExportGroup_Click �
		private void miExportGroup_Click(object sender, System.EventArgs e)
		{
			Group group = GroupFactory.GetInstance().GetGroup(this.selectedNodeId);
			Hashtable htPropertyItem = ApplicationHelper.GetPropertyItemsInNode(group.GroupId);
			if (null != htPropertyItem && htPropertyItem.Count > 0)
			{
				PropertyItemsExportWizard testWizard = new PropertyItemsExportWizard(group.PropertyItems);
				testWizard.Execute();
			}
			else
			{
				MessageBox.Show("\"" + group.Name + "\""+ " group does not contain any contact to export", "Please select another group to export", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}
		#endregion � miExportGroup_Click �

		private void miRemoveContacts_Click(object sender, System.EventArgs e)
		{
			this.rtbNodeDesc.Text = string.Empty;
			// delete group
			TreeNode tn = tv_Nodes.SelectedNode;

			if (null == tn)
				return;

			Node n = (Node)tn.Tag;

			if (DialogResult.Yes == MessageBox.Show(this, "Are you sure you want to Remove ALL contacts from " + n.nodeName + " group?", "Remove Contacts", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2 ))
			{
				ApplicationHelper.DeletePropertyItemsFromGroups(null, n.nodeId, 0);
				UpdateTreeView(n.nodeId);
			}
		}

		public void Immobilize()
		{
			bDeleteGroup.Visible = false;
			bRenameGroup.Visible = false;
			bNewGroup.Visible = false;
			bCollapseAll.Visible = false;
			bExpandAll.Visible = false;
			bCopyGroup.Visible = false;
			tbSelectedNode.Visible = false;
			rtbNodeDesc.Visible = false;
			tv_Nodes.Dock = DockStyle.Fill;
			tv_Nodes.ContextMenu = null;		
		}


		#endregion � Methods �

		private void On_ApplyParentMarketing_Click(object sender, EventArgs e)
		{
			TreeNode tn = tv_Nodes.SelectedNode;
			
			if (null == tn)
				return;

			Node n = (Node)tn.Tag;
			SkipTracedContactAssistant.Instance.ApplyParentGroupMarketingCapmaign(n.nodeId);
		}

		private void On_ForceParentMarketing_Click(object sender, EventArgs e)
		{
			TreeNode tn = tv_Nodes.SelectedNode;

			if (null == tn)
				return;

			Node n = (Node)tn.Tag;
			SkipTracedContactAssistant.Instance.ForceParentGroupMarketingCampaign(n.nodeId);
		}
        
        private async Task<bool> AppendData(AppendDataType appendDataType)
        {			            
            var tn = tv_Nodes.SelectedNode;

            if (null == tn)
                return false;

            Node n = (Node)tn.Tag;

			var amounts = await new AppendDataWalletCheck(n.nodeId, Globals.UserRegistration.IDUser).Check();
						
			var fundsForm = new Forms.SkipTrace.AddSkipTraceFunds(amounts.Item1, amounts.Item2);
			var dr = fundsForm.ShowDialog();
									
			if (DialogResult.Abort == dr)
			{
				Process.Start($"https://www.ProfitGrabber.com/SkipTracing/plans.php?pgid={Globals.UserRegistration.IDUser}");
				return false;
			}
			else if (DialogResult.Cancel == dr)				
				return false;

			if (AppendDataType.All == appendDataType && !fundsForm.SkipTraceAllRecordsIncludingPreviouslySkipTraced)
				appendDataType = AppendDataType.AllExceptAlreadyMatched;
										
			using (var adf = new Forms.AppendDataForm.AppendDataForm(n.nodeId, appendDataType, this as IUpdateTreeViewAsync))
			{
				adf.ShowDialog();
			}

            return true;
        }

        private async void appendDataAll_Click(object sender, EventArgs e)
        {
            if (await AppendData(AppendDataType.All))
                CreateAndAssignSkipTracedSubgroups();
		}

		private void _uploadGroupForTextOrRVM_Click(object sender, EventArgs e)
		{
			StartDropCowboy();
		}

		private void _sendRVM_Click(object sender, EventArgs e)
        {
			Process.Start(ProfitGrabber.RVM.DropCowboy.DCConsts.DCLogInURL);
        }
                
		void StartDropCowboy()
		{
			var psForm = new PhoneSelector();
			var dr = psForm.ShowDialog();

			if (DialogResult.OK != dr)
				return;

			var selectedFlags = psForm.SelectedPhoneTypesFlags;

			var tn = tv_Nodes.SelectedNode;

			if (null == tn)
				return;

			var n = (Node)tn.Tag;
			
			var res = ApplicationHelper.CreateDropCowboyRequest(n.nodeId, selectedFlags);

			if (0 == res.Values.Length)
			{
				MessageBox.Show("The Selected Group does not contain any Mobile Phone Numbers.", "Skip Trace", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}

			ProfitGrabber.RVM.DropCowboy.Forms.RVMWizard wizard = new ProfitGrabber.RVM.DropCowboy.Forms.RVMWizard(n.nodeName, OSFolderManager.Instance.GetPlainLocalUserAppDataPath());
			wizard.Request = res;

			wizard.ShowDialog();			
		}

        private async void MiMoveGroup_Click(object sender, EventArgs e)
        {
            if (null == tv_Nodes.SelectedNode)
                return;
            
			var sourceNode = tv_Nodes.SelectedNode;
			var sourceNodeId = ((Node)sourceNode.Tag).nodeId;

			var sgf = new SelectGroupForm();
            sgf.ShowDialog();

            if (DialogResult.OK != sgf.DialogResult || !sgf.Valid)
                return;

            var destinationNodeId = sgf.SelectedNodeId;

            if (sourceNodeId.Equals(destinationNodeId))
            {
                MessageBox.Show("Source and destination groups can not be the same");
                return;
            }

            var rowsAffected = await ApplicationHelper.MoveGroup(sourceNodeId, destinationNodeId);

            if (1 != rowsAffected) 
                return;

            OnNewGroup();
            tv_Nodes.Refresh();
                
            var fn = FindNode(tv_Nodes.Nodes[0], sourceNodeId);

            tv_Nodes.SelectedNode = fn;
        }

		private void MiCombineGroups_Click(object sender, EventArgs e)
        {			
			var dr = MessageBox.Show($"This will combine the selected group (source group) with the group you choose next (destination group).{Environment.NewLine}{Environment.NewLine}" +
                                     $"Make sure this is what you intend to do before proceeding.", "Combine Groups", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);

			if (dr != DialogResult.OK)
				return;

			if (null == tv_Nodes.SelectedNode)
                return;

            var sourceNode = tv_Nodes.SelectedNode;
            var sourceNodeId = ((Node)sourceNode.Tag).nodeId;

            var sgf = new SelectGroupForm();
            sgf.ShowDialog();

            if (DialogResult.OK != sgf.DialogResult || !sgf.Valid)
                return;

            var destinationNodeId = sgf.SelectedNodeId;

            if (sourceNodeId.Equals(destinationNodeId))
            {
                MessageBox.Show("Source and destination groups can not be the same");
                return;
            }

			var propertyItems = ApplicationHelper.GetPropertyItemIDsInNode(sourceNodeId);

            if (propertyItems?.Any() ?? false)
                propertyItems.ForEach(piid => ApplicationHelper.AssignPropertyItemToGroup(destinationNodeId, piid));

			var fn = FindNode(tv_Nodes.Nodes[0], destinationNodeId);
            tv_Nodes.SelectedNode = fn;

            CreateAndAssignSkipTracedSubgroups();

            MessageBox.Show(this, $"Selected Groups successfully Combined!{System.Environment.NewLine}{System.Environment.NewLine}" +
                                  $"If you did not Skip Trace one of the groups that were combined and want Skip Traced data for all contacts, you should skip trace the Destination group NOW.{System.Environment.NewLine}{System.Environment.NewLine}" +
                                  $"Do it NOW before you forget!", "ProfitGrabber Pro", MessageBoxButtons.OK, MessageBoxIcon.Information);
		}

        private TreeNode FindNode(TreeNode tn, int nodeId)
        {
            if (((Node)tn.Tag).nodeId.Equals(nodeId))
                return tn;

            foreach (TreeNode n in tn.Nodes)
            {
                var fn = FindNode(n, nodeId);

                if (null != fn)
                    return fn;
            }

            return null;
		}		
	}
	#endregion � Class CustomizedTreeViewUserControl �
}
#endregion � Namespace �
