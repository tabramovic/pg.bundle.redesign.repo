﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Printing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;

//Added
using ProfitGrabber.Common;


namespace DealMaker
{
    public partial class BuyerInfoOverView : UserControl
    {
        BuyerInfoReportGenerator _biGen = new BuyerInfoReportGenerator();
        string _address = string.Empty;
        ListViewColumnSorter _lvwColumnSorter = new ListViewColumnSorter();
        bool _enableAutoSaveEvent = true;

        public delegate void BuyerObjectAutoSave();
        public event BuyerObjectAutoSave BuyerObjectAutoSaveEvent;

        public BuyerInfoOverView()
        {
            InitializeComponent();
            AddEventHandlers();
            FillForm();
            _biView.ListViewItemSorter = _lvwColumnSorter;

            foreach (ColumnHeader ch in _biView.Columns)
            {
                ch.Width = -2;
            }
        }

        void AddEventHandlers()
        {
            _generalNotes.Leave += new EventHandler(On_generalNotes_Leave);
            _add.Click += new EventHandler(On_Add);
            _edit.Click += new EventHandler(On_Edit);
            _delete.Click += new EventHandler(On_Delete);
            _print.Click += new EventHandler(On_Print);
            _biView.ColumnClick += new ColumnClickEventHandler(On_ColumnClick);
            _biView.DoubleClick += new EventHandler(On_DoubleClick);
            _browse.Click += new EventHandler(On_Browse_Click);
            _open.Click += new EventHandler(On_Open_Click);
            _create.Click += new EventHandler(On_Create_Click);
        }

        
        void On_Create_Click(object sender, EventArgs e)
        {
            //throw new NotImplementedException();
        }

        void On_Open_Click(object sender, EventArgs e)
        {
            string folder = BuyerInfoObject.Instance.BuyerInfoDataProp.DataFolder;            

            if (string.Empty != folder)
                if (Directory.Exists(folder))
                    Process.Start(@"explorer.exe", folder);		
        }

        void On_Browse_Click(object sender, EventArgs e)
        {
            string folder = BuyerInfoObject.Instance.BuyerInfoDataProp.DataFolder;

            FolderBrowserDialog browserDialog = new FolderBrowserDialog();

            if (null != folder && string.Empty != folder)
                browserDialog.SelectedPath = folder;

            if (DialogResult.OK == browserDialog.ShowDialog())
            {
                BuyerInfoObject.Instance.BuyerInfoDataProp.DataFolder = browserDialog.SelectedPath;
                _folder.Text = browserDialog.SelectedPath;
                BuyerInfoManager.Instance.SaveData(BuyerInfoManager.Instance.CurrentPropertyItem);
            }
        }

        void On_DoubleClick(object sender, EventArgs e)
        {            
            if (null != _biView.SelectedItems && null != _biView.SelectedItems[0])
                On_Edit(sender, e);
        }

        #region COLUMN CLICK HANDLERS
        void On_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            // Determine if clicked column is already the column that is being sorted.
            if (e.Column == _lvwColumnSorter.SortColumn)
            {
                // Reverse the current sort direction for this column.
                if (_lvwColumnSorter.Order == SortOrder.Ascending)
                {
                    _lvwColumnSorter.Order = SortOrder.Descending;
                }
                else
                {
                    _lvwColumnSorter.Order = SortOrder.Ascending;
                }
            }
            else
            {
                // Set the column number that is to be sorted; default to ascending.
                _lvwColumnSorter.SortColumn = e.Column;
                _lvwColumnSorter.Order = SortOrder.Ascending;
            }

            // Perform the sort with these new sort options.
            _biView.Sort();
        }
        #endregion

        #region PRINT
        void On_Print(object sender, EventArgs e)
        {
            PageSettings pageSettings = null;
            PageSetupDialog pageSetupDialog = new PageSetupDialog();
            pageSetupDialog.PageSettings = new PageSettings();
            pageSetupDialog.PrinterSettings = new PrinterSettings();
            pageSetupDialog.ShowNetwork = false;

            pageSetupDialog.PageSettings.PaperSize = new PaperSize("A4", 827, 1169);
            pageSetupDialog.PageSettings.Margins = new Margins(0, 0, 0, 0);

            PrintDialog pd = new PrintDialog();
            pd.UseEXDialog = true;

            PrintDocument tmpprndoc = new PrintDocument();

            tmpprndoc.PrintPage += new PrintPageEventHandler(On_PrintPage);
            tmpprndoc.EndPrint += new PrintEventHandler(On_EndPrint);

            pd.Document = tmpprndoc;
            PrintPreviewDialogEx tmpprdiag = new PrintPreviewDialogEx();

            tmpprdiag.Document = tmpprndoc;
            //tmpprdiag.Document.DefaultPageSettings = pageSettings;
            tmpprdiag.ShowDialog();

        }

        void On_PrintPage(object sender, PrintPageEventArgs e)
        {
            _biGen.Generate(e, BuyerInfoObject.Instance.BuyerInfoDataProp, _address);
        }

        void On_EndPrint(object sender, PrintEventArgs e)
        {
            _biGen.ResetGen();
        }
        #endregion                

        #region SET DATA, FILL FORM
        public void SetData(string address)
        {
            _address = address;
        }

        public void FillForm()
        {
            _biView.Items.Clear();            


            if (null == BuyerInfoObject.Instance.BuyerInfoDataProp)
                return;
            else
            {
                _folder.Text = BuyerInfoObject.Instance.BuyerInfoDataProp.DataFolder;
                _generalNotes.Text = BuyerInfoObject.Instance.BuyerInfoDataProp.GeneralNotes;
                if (null != BuyerInfoObject.Instance.BuyerInfoDataProp.Buyers)
                {
                    foreach (BuyerInfoDataSingleBuyerData bid in BuyerInfoObject.Instance.BuyerInfoDataProp.Buyers)
                    {
                        ListViewItem lvi = new ListViewItem(bid.DateOf);
                        lvi.SubItems.Add(bid.SpokeWith);
                        lvi.SubItems.Add(bid.LeadSource);
                        lvi.SubItems.Add(bid.YourInterest);
                        lvi.SubItems.Add(bid.TypeOfBuyer);
                        lvi.SubItems.Add(bid.AvailDown);
                        lvi.SubItems.Add(bid.Credit);
                        lvi.SubItems.Add(bid.Name);
                        lvi.SubItems.Add(bid.Agent);
                        lvi.SubItems.Add(bid.PhoneNr);
                        lvi.SubItems.Add(bid.FaxNr);
                        lvi.SubItems.Add(bid.Email);
                        lvi.SubItems.Add(bid.Notes);

                        lvi.Tag = bid;

                        _biView.Items.Add(lvi);
                    }
                    
                }
            }
        }
        #endregion

        #region ADD EDIT DELETE
        void On_Add(object sender, EventArgs e)
        {
            BuyerInfoForm bif = new BuyerInfoForm(null);
            if (DialogResult.OK == bif.ShowDialog())
            {
                if (null != BuyerInfoObject.Instance.BuyerInfoDataProp)
                {
                    BuyerInfoDataSingleBuyerData[] tempBuyers = BuyerInfoObject.Instance.BuyerInfoDataProp.Buyers;
                    List<BuyerInfoDataSingleBuyerData> tempList = new List<BuyerInfoDataSingleBuyerData>();
                    if (null != tempBuyers)
                        tempList = new List<BuyerInfoDataSingleBuyerData>(tempBuyers);
                    tempList.Add(bif.OutputBuyerInfoData);
                    BuyerInfoObject.Instance.BuyerInfoDataProp.Buyers = tempList.ToArray();
                    //BuyerInfoManager.Instance.SaveData(BuyerInfoManager.Instance.CurrentPropertyItem);
                    TriggerAutoSave();
                }
                FillForm();
            }            
        }

        void On_Edit(object sender, EventArgs e)
        {            
            BuyerInfoDataSingleBuyerData bd = (BuyerInfoDataSingleBuyerData)_biView.SelectedItems[0].Tag;
            BuyerInfoForm bif = new BuyerInfoForm(bd);

            if (DialogResult.OK == bif.ShowDialog())
            {
                if (null != BuyerInfoObject.Instance.BuyerInfoDataProp && null != BuyerInfoObject.Instance.BuyerInfoDataProp.Buyers)
                {
                    BuyerInfoDataSingleBuyerData[] tempBuyers = BuyerInfoObject.Instance.BuyerInfoDataProp.Buyers;
                    List<BuyerInfoDataSingleBuyerData> tempList = new List<BuyerInfoDataSingleBuyerData>();
                    if (null != tempBuyers)
                        tempList = new List<BuyerInfoDataSingleBuyerData>(tempBuyers);
                    int idx = tempList.IndexOf(bd);
                    tempList.Remove(bd);
                    tempList.Insert(idx, bif.OutputBuyerInfoData);
                    BuyerInfoObject.Instance.BuyerInfoDataProp.Buyers = tempList.ToArray();
                    //BuyerInfoManager.Instance.SaveData(BuyerInfoManager.Instance.CurrentPropertyItem);
                    TriggerAutoSave();
                }
                FillForm();
            }
        }

        void On_Delete(object sender, EventArgs e)
        {
            BuyerInfoDataSingleBuyerData bd = (BuyerInfoDataSingleBuyerData)_biView.SelectedItems[0].Tag;
            BuyerInfoForm bif = new BuyerInfoForm(bd);

            if (DialogResult.Yes == MessageBox.Show("Are you sure you want to delete selected buyer info record?", "Confirm Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2))
            {
                if (null != BuyerInfoObject.Instance.BuyerInfoDataProp && null != BuyerInfoObject.Instance.BuyerInfoDataProp.Buyers)
                {
                    BuyerInfoDataSingleBuyerData[] tempBuyers = BuyerInfoObject.Instance.BuyerInfoDataProp.Buyers;
                    List<BuyerInfoDataSingleBuyerData> tempList = new List<BuyerInfoDataSingleBuyerData>();
                    if (null != tempBuyers)
                        tempList = new List<BuyerInfoDataSingleBuyerData>(tempBuyers);
                    int idx = tempList.IndexOf(bd);
                    tempList.Remove(bd);                    
                    BuyerInfoObject.Instance.BuyerInfoDataProp.Buyers = tempList.ToArray();
                    //BuyerInfoManager.Instance.SaveData(BuyerInfoManager.Instance.CurrentPropertyItem);
                    TriggerAutoSave();
                }
                FillForm();
            }
        }
        #endregion

        #region GENERAL NOTES
        void On_generalNotes_Leave(object sender, EventArgs e)
        {
            BuyerInfoObject.Instance.BuyerInfoDataProp.GeneralNotes = _generalNotes.Text;
            //BuyerInfoManager.Instance.SaveData(BuyerInfoManager.Instance.CurrentPropertyItem);
            TriggerAutoSave();
        }
        #endregion

        public void SaveData()
        {
            BuyerInfoManager.Instance.SaveData(BuyerInfoManager.Instance.CurrentPropertyItem);
        }

        public void EnableAutoSaveEventHandler(bool enable)
        {
            _enableAutoSaveEvent = enable;
        }

        void TriggerAutoSave()
        {
            if (null != BuyerObjectAutoSaveEvent && _enableAutoSaveEvent)
                BuyerObjectAutoSaveEvent();
        }
    }
}
