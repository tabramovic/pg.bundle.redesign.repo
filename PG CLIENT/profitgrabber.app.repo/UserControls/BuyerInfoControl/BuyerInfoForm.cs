﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace DealMaker
{
    public partial class BuyerInfoForm : Form
    {
        BuyerInfoDataSingleBuyerData _bid = null;
        Dictionary<Guid, string> _officeStaffList = null;
        Dictionary<Guid, string> _leadSourceList = null;

        BuyerInfoDataSingleBuyerData _outputBid = null;

        public BuyerInfoForm(BuyerInfoDataSingleBuyerData bid)
        {
            InitializeComponent();
            AddEventHandlers();
            LoadLists();
            SetData(bid);    
        }

        public BuyerInfoDataSingleBuyerData OutputBuyerInfoData
        {
            get { return _outputBid; }
        }

        void AddEventHandlers()
        {
            _save.Click += new EventHandler(On_Save);
        }

        void On_Save(object sender, EventArgs e)
        {
             _outputBid = GetData();
        }

        void LoadLists()
        {
            _officeStaffList = UserInformationManager.Instance.GetValues(UserEntries.OfficeStaffList);
            _leadSourceList = UserInformationManager.Instance.GetValues(UserEntries.LeadSourceForSellingList);

            SetLists();
        }

        void SetLists()
        {
            _spokeWith.Items.Add("-- Select --");
            if (null != _officeStaffList)
            {
                Dictionary<Guid, string>.Enumerator enumerator = _officeStaffList.GetEnumerator();

                while (enumerator.MoveNext())                
                    _spokeWith.Items.Add(enumerator.Current.Value);                                
            }

            _leadSource.Items.Add("-- Select --");
            if (null != _officeStaffList)
            {
                Dictionary<Guid, string>.Enumerator enumerator = _leadSourceList.GetEnumerator();

                while (enumerator.MoveNext())
                    _leadSource.Items.Add(enumerator.Current.Value);
            }
        }

        void SetData(BuyerInfoDataSingleBuyerData bid)
        {
            _bid = bid;

            if (null == bid)
            {
                _dateOf.Value = DateTime.Now;
                _spokeWith.SelectedIndex = 0; // NOT SET
                _leadSource.SelectedIndex = 0; // NOT SET
                _yourInterest.SelectedIndex = 0; // NOT SET
                _typeOfBuyer.SelectedIndex = 0; // NOT SET
                _availDown.Text = string.Empty;
                _credit.SelectedIndex = 0; //NOT SET
                _name.Text = string.Empty;
                _agent.SelectedIndex = 0; // NOT SET
                _phone.Text = string.Empty;
                _fax.Text = string.Empty;
                _email.Text = string.Empty;
                _notes.Text = string.Empty;
            }
            else
            {
                //DATE OF
                DateTime tempVal = DateTime.Now;
                try { tempVal =  Convert.ToDateTime(bid.DateOf); }
                catch { tempVal = DateTime.Now; }
                _dateOf.Value = tempVal;

                //SPOKE WITH
                int iCnt = 0;
                bool found = false;
                foreach (object o in _spokeWith.Items)
                {
                    if ((string)o == bid.SpokeWith)
                    {
                        found = true;
                        break;
                    }
                    else
                        iCnt++;
                }

                if (found)
                {
                    try { _spokeWith.SelectedIndex = iCnt; }
                    catch { _spokeWith.Text = bid.SpokeWith; }
                }
                else
                {
                    //_spokeWith.SelectedIndex = 0;
                    _spokeWith.Text = bid.SpokeWith;
                }

                //LEAD SOURCE
                iCnt = 0;
                foreach (object o in _leadSource.Items)
                {
                    if ((string)o == bid.LeadSource)
                    {
                        found = true;
                        break;
                    }
                    else
                        iCnt++;
                }

                if (found)
                {
                    try { _leadSource.SelectedIndex = iCnt; }
                    catch { _leadSource.Text = bid.LeadSource; }
                }
                else
                {
                    //_leadSource.SelectedIndex = 0;
                    _leadSource.Text = bid.LeadSource;
                }

                //YOUR INTEREST
                iCnt = 0;
                foreach (object o in _yourInterest.Items)
                {
                    if ((string)o == bid.YourInterest)
                    {
                        found = true;
                        break;
                    }
                    else
                        iCnt++;
                }

                if (found)
                {
                    try { _yourInterest.SelectedIndex = iCnt; }
                    catch { _yourInterest.Text = bid.YourInterest; }
                }
                else
                {
                    //_yourInterest.SelectedIndex = 0;
                    _yourInterest.Text = bid.YourInterest;
                }

                //AVAIL $ DOWN
                _availDown.Text = bid.AvailDown;
                

                //TYPE OF BUYER
                iCnt = 0;
                foreach (object o in _typeOfBuyer.Items)
                {
                    if ((string)o == bid.TypeOfBuyer)
                    {
                        found = true;
                        break;
                    }
                    else
                        iCnt++;
                }

                if (found)
                {
                    try { _typeOfBuyer.SelectedIndex = iCnt; }
                    catch { _typeOfBuyer.Text = bid.TypeOfBuyer; }
                }
                else
                {
                    //_typeOfBuyer.SelectedIndex = 0;
                    _typeOfBuyer.Text = bid.TypeOfBuyer;
                }
                

                //CREDIT
                iCnt = 0;
                foreach (object o in _credit.Items)
                {
                    if ((string)o == bid.Credit)
                    {
                        found = true;
                        break;
                    }
                    else
                        iCnt++;
                }

                if (found)
                {
                    try { _credit.SelectedIndex = iCnt; }
                    catch { _credit.Text = bid.Credit; }
                }
                else
                {
                    //_credit.SelectedIndex = 0;
                    _credit.Text = bid.Credit;
                }
                

                //NAME
                _name.Text = bid.Name;

                //AGENT
                iCnt = 0;
                foreach (object o in _agent.Items)
                {
                    if ((string)o == bid.Agent)
                    {
                        found = true;
                        break;
                    }
                    else
                        iCnt++;
                }

                if (found)
                {
                    try { _agent.SelectedIndex = iCnt; }
                    catch { _agent.Text = bid.Agent; }
                }
                else
                {
                    //_agent.SelectedIndex = 0;
                    _agent.Text = bid.Agent;
                }
                

                //PHONE NR
                _phone.Text = bid.PhoneNr;

                //FAX NR
                _fax.Text = bid.FaxNr;

                //EMAIL
                _email.Text = bid.Email;

                //NOTES
                _notes.Text = bid.Notes;
            }
        }

        BuyerInfoDataSingleBuyerData GetData()
        {
            BuyerInfoDataSingleBuyerData bid = new BuyerInfoDataSingleBuyerData();

            //DATE OF
            bid.DateOf = _dateOf.Value.ToShortDateString();            

            //SPOKE WITH
            bid.SpokeWith = _spokeWith.Text.ToString();

            //LEAD SOURCE
            bid.LeadSource = _leadSource.Text.ToString();

            //YOUR INTEREST
            bid.YourInterest = _yourInterest.Text.ToString();

            //TYPE OF BUYER
            bid.TypeOfBuyer = _typeOfBuyer.Text.ToString();

            //AVAIL $ DOWN
            bid.AvailDown = _availDown.Text;
            
            //CREDIT
            bid.Credit = _credit.Text.ToString();       

            //NAME
            bid.Name = _name.Text;

            //AGENT
            bid.Agent = _agent.Text.ToString();  

            //PHONE NR
            bid.PhoneNr = _phone.Text;

            //FAX NR
            bid.FaxNr = _fax.Text;

            //EMAIL
            bid.Email = _email.Text;

            //NOTES
            bid.Notes = _notes.Text;

            return bid;
        }
    }
}
