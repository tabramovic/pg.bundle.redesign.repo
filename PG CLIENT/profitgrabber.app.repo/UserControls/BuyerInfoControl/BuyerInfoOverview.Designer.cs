﻿namespace DealMaker
{
    partial class BuyerInfoOverView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this._generalNotes = new System.Windows.Forms.TextBox();
            this._biView = new System.Windows.Forms.ListView();
            this.columnHeader1 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader2 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader3 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader4 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader5 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader6 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader7 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader8 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader9 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader11 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader12 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader13 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader14 = new System.Windows.Forms.ColumnHeader();
            this._ctxMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this._add = new System.Windows.Forms.ToolStripMenuItem();
            this._edit = new System.Windows.Forms.ToolStripMenuItem();
            this._delete = new System.Windows.Forms.ToolStripMenuItem();
            this.label2 = new System.Windows.Forms.Label();
            this._folder = new System.Windows.Forms.TextBox();
            this._browse = new System.Windows.Forms.Button();
            this._open = new System.Windows.Forms.Button();
            this._create = new System.Windows.Forms.Button();
            this._print = new System.Windows.Forms.Button();
            this._ctxMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(314, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Enter General Notes on Selling Terms; Lockbox; Gate code, etc.:";
            // 
            // _generalNotes
            // 
            this._generalNotes.Location = new System.Drawing.Point(6, 33);
            this._generalNotes.Multiline = true;
            this._generalNotes.Name = "_generalNotes";
            this._generalNotes.Size = new System.Drawing.Size(891, 97);
            this._generalNotes.TabIndex = 1;
            // 
            // _biView
            // 
            this._biView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader5,
            this.columnHeader6,
            this.columnHeader7,
            this.columnHeader8,
            this.columnHeader9,
            this.columnHeader11,
            this.columnHeader12,
            this.columnHeader13,
            this.columnHeader14});
            this._biView.ContextMenuStrip = this._ctxMenu;
            this._biView.FullRowSelect = true;
            this._biView.GridLines = true;
            this._biView.Location = new System.Drawing.Point(6, 150);
            this._biView.Name = "_biView";
            this._biView.Size = new System.Drawing.Size(891, 368);
            this._biView.TabIndex = 2;
            this._biView.UseCompatibleStateImageBehavior = false;
            this._biView.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "DateOf";
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Spoke With";
            this.columnHeader2.Width = 75;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Lead Source";
            this.columnHeader3.Width = 80;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Your Interest";
            this.columnHeader4.Width = 80;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Type Of Buyer";
            this.columnHeader5.Width = 86;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "Avail. $ Down";
            this.columnHeader6.Width = 83;
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "Credit";
            // 
            // columnHeader8
            // 
            this.columnHeader8.Text = "Name";
            // 
            // columnHeader9
            // 
            this.columnHeader9.Text = "Agent";
            // 
            // columnHeader11
            // 
            this.columnHeader11.Text = "Phone Nr.";
            // 
            // columnHeader12
            // 
            this.columnHeader12.Text = "Fax Nr.";
            // 
            // columnHeader13
            // 
            this.columnHeader13.Text = "Email";
            this.columnHeader13.Width = 72;
            // 
            // columnHeader14
            // 
            this.columnHeader14.Text = "Notes";
            this.columnHeader14.Width = 66;
            // 
            // _ctxMenu
            // 
            this._ctxMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._add,
            this._edit,
            this._delete});
            this._ctxMenu.Name = "_ctxMenu";
            this._ctxMenu.Size = new System.Drawing.Size(165, 70);
            // 
            // _add
            // 
            this._add.Name = "_add";
            this._add.Size = new System.Drawing.Size(164, 22);
            this._add.Text = "Add New Buyer";
            // 
            // _edit
            // 
            this._edit.Name = "_edit";
            this._edit.Size = new System.Drawing.Size(164, 22);
            this._edit.Text = "Edit Buyer Info";
            // 
            // _delete
            // 
            this._delete.Name = "_delete";
            this._delete.Size = new System.Drawing.Size(164, 22);
            this._delete.Text = "Delete Buyer Info";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 528);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(160, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Buyer Document Archive Folder:";
            // 
            // _folder
            // 
            this._folder.Location = new System.Drawing.Point(169, 525);
            this._folder.Name = "_folder";
            this._folder.ReadOnly = true;
            this._folder.Size = new System.Drawing.Size(529, 20);
            this._folder.TabIndex = 4;
            // 
            // _browse
            // 
            this._browse.Location = new System.Drawing.Point(704, 524);
            this._browse.Name = "_browse";
            this._browse.Size = new System.Drawing.Size(31, 23);
            this._browse.TabIndex = 5;
            this._browse.Text = "...";
            this._browse.UseVisualStyleBackColor = true;
            // 
            // _open
            // 
            this._open.Location = new System.Drawing.Point(741, 524);
            this._open.Name = "_open";
            this._open.Size = new System.Drawing.Size(75, 23);
            this._open.TabIndex = 6;
            this._open.Text = "Open";
            this._open.UseVisualStyleBackColor = true;
            // 
            // _create
            // 
            this._create.Location = new System.Drawing.Point(871, 505);
            this._create.Name = "_create";
            this._create.Size = new System.Drawing.Size(26, 23);
            this._create.TabIndex = 7;
            this._create.Text = "Create Standard Folder";
            this._create.UseVisualStyleBackColor = true;
            this._create.Visible = false;
            // 
            // _print
            // 
            this._print.Location = new System.Drawing.Point(822, 524);
            this._print.Name = "_print";
            this._print.Size = new System.Drawing.Size(75, 23);
            this._print.TabIndex = 8;
            this._print.Text = "Print";
            this._print.UseVisualStyleBackColor = true;
            // 
            // BuyerInfoOverView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.Controls.Add(this._print);
            this.Controls.Add(this._open);
            this.Controls.Add(this._folder);
            this.Controls.Add(this._browse);
            this.Controls.Add(this.label2);
            this.Controls.Add(this._biView);
            this.Controls.Add(this._generalNotes);
            this.Controls.Add(this.label1);
            this.Controls.Add(this._create);
            this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.Name = "BuyerInfoOverView";
            this.Size = new System.Drawing.Size(900, 550);
            this._ctxMenu.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox _generalNotes;
        private System.Windows.Forms.ListView _biView;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.ColumnHeader columnHeader8;
        private System.Windows.Forms.ColumnHeader columnHeader9;
        private System.Windows.Forms.ColumnHeader columnHeader11;
        private System.Windows.Forms.ColumnHeader columnHeader12;
        private System.Windows.Forms.ColumnHeader columnHeader13;
        private System.Windows.Forms.ColumnHeader columnHeader14;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox _folder;
        private System.Windows.Forms.Button _browse;
        private System.Windows.Forms.Button _open;
        private System.Windows.Forms.Button _create;
        private System.Windows.Forms.Button _print;
        private System.Windows.Forms.ContextMenuStrip _ctxMenu;
        private System.Windows.Forms.ToolStripMenuItem _add;
        private System.Windows.Forms.ToolStripMenuItem _edit;
        private System.Windows.Forms.ToolStripMenuItem _delete;
    }
}
