﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient; 

namespace DealMaker
{
    public sealed class BuyerInfoManager
    {
        static readonly BuyerInfoManager _instance = new BuyerInfoManager();

        static BuyerInfoManager()
        { }

        BuyerInfoManager()
        { }

        public static BuyerInfoManager Instance
        {
            get { return _instance; }
        }

        Guid _currentPropertyItem = Guid.Empty;

        public Guid CurrentPropertyItem 
        {
            get { return _currentPropertyItem; }
        }


        bool _existingDBLayer = false;
        SqlConnection _connection;

        public void SetConnection(SqlConnection conn)
        {
            if (null == _connection)
            {
                _connection = conn;
                _existingDBLayer = TestIfExistsDBLayer();

                if (!_existingDBLayer)
                    CreateDBLayer();
            }
        }

        public bool TestIfExistsDBLayer()
        {
            bool retVal = false;
            SqlDataReader reader = null;
            string cmdText = "SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'BuyersInfo'";

            try
            {
                SqlCommand testForLayer = new SqlCommand(cmdText, _connection);
                reader = testForLayer.ExecuteReader();
                while (reader.Read())
                {
                    retVal = true;
                    break;
                }
            }
            catch
            {
                retVal = false;
            }
            finally
            {
                reader.Close();
            }
            return retVal;
        }

        void CreateDBLayer()
        {
            CreateTable();
            CreateInsertSPL();
            CreateUpdateSPL();
        }

        bool CreateTable()
        {
            bool retVal = false;
            string cmdText = "CREATE TABLE BuyersInfo (piId uniqueidentifier PRIMARY KEY, buyerInfo image)";

            try
            {
                SqlCommand createLayer = new SqlCommand(cmdText, _connection);
                createLayer.ExecuteNonQuery();
                retVal = true;
            }
            catch
            {
                retVal = false;
            }
            return retVal;
        }

        void CreateInsertSPL()
        {
            try
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("CREATE PROCEDURE InsertBuyerInfo ");
                sb.Append("( ");
                sb.Append("@propertyItemId uniqueidentifier, ");
                sb.Append("@buyerInfoStream image");                
                sb.Append(") ");
                sb.Append("AS ");
                sb.Append("INSERT INTO BuyersInfo (piId , buyerInfo) ");
                sb.Append("VALUES ( @propertyItemId , @buyerInfoStream) ");

                string splText = sb.ToString();
                SqlCommand createSPL = new SqlCommand(splText, _connection);
                createSPL.ExecuteNonQuery();

            }
            catch (Exception exc)
            {
                System.Windows.Forms.MessageBox.Show("Can't create INSERT procedure for BUYER_INFO module" + System.Environment.NewLine + exc.Message);
            }
        }

        void CreateUpdateSPL()
        {
            try
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("CREATE PROCEDURE UpdateBuyerInfo ");
                sb.Append("( ");
                sb.Append("@propertyItemId uniqueidentifier, ");
                sb.Append("@buyerInfoStream image ");                
                sb.Append(") ");
                sb.Append("AS ");
                sb.Append("UPDATE BuyersInfo SET buyerInfo=@buyerInfoStream where piId=@propertyItemId; ");

                string splText = sb.ToString();
                SqlCommand createSPL = new SqlCommand(splText, _connection);
                createSPL.ExecuteNonQuery();

            }
            catch (Exception exc)
            {
                System.Windows.Forms.MessageBox.Show("Can't create UPDATE procedure for BUYERINFO module" + System.Environment.NewLine + exc.Message);
            }
        }

        public void GetData(Guid propertyItem)
        {
            _currentPropertyItem = propertyItem;

            string cmdText = "SELECT buyerInfo FROM BuyersInfo WHERE piId='" + propertyItem.ToString() + "'";
            SqlDataReader reader = null;
            byte[] buyerInfo = null;
            
            try
            {
                SqlCommand getData = new SqlCommand(cmdText, _connection);
                reader = getData.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        buyerInfo = !reader.IsDBNull(0) ? (byte[])reader.GetValue(0) : null;
                        BuyerInfoObject.Instance.DeserializeFromByteArray(buyerInfo);                        
                    }
                }
                else
                {
                    BuyerInfoObject.Instance.ResetObject();                    
                }
            }
            catch
            {
                buyerInfo = null;                
            }
            finally
            {
                reader.Close();
            }
        }

        public bool SaveData(Guid propertyItem)
        {
            if (!TestForExistingRecord(propertyItem))
                return InsertData(propertyItem);
            else
                return UpdateData(propertyItem);
        }

        bool TestForExistingRecord(Guid propertyItem)
        {
            bool retVal = false;

            SqlDataReader reader = null;
            string cmdText = "SELECT piId FROM BuyersInfo where piId='" + propertyItem.ToString() + "'";

            try
            {
                SqlCommand testForExistingRecord = new SqlCommand(cmdText, _connection);
                reader = testForExistingRecord.ExecuteReader();
                while (reader.Read())
                {
                    retVal = true;
                    break;
                }
            }
            catch
            {
                retVal = false;
            }
            finally
            {
                if (null != reader)
                    reader.Close();
            }
            return retVal;
        }

        bool InsertData(Guid propertyItem)
        {
            bool retVal = false;

            try
            {
                SqlCommand sp_insert_comps = new SqlCommand("InsertBuyerInfo", _connection);
                sp_insert_comps.CommandType = CommandType.StoredProcedure;

                sp_insert_comps.Parameters.AddWithValue("@propertyItemId", propertyItem);
                sp_insert_comps.Parameters.AddWithValue("@buyerInfoStream", BuyerInfoObject.Instance.SerializeToByteArray());                
                sp_insert_comps.ExecuteNonQuery();

                retVal = true;
            }
            catch
            {
                retVal = false;
            }
            return retVal;
        }

        bool UpdateData(Guid propertyItem)
        {
            bool retVal = false;

            try
            {
                SqlCommand sp_insert_comps = new SqlCommand("UpdateBuyerInfo", _connection);
                sp_insert_comps.CommandType = CommandType.StoredProcedure;

                sp_insert_comps.Parameters.AddWithValue("@propertyItemId", propertyItem);
                sp_insert_comps.Parameters.AddWithValue("@buyerInfoStream", BuyerInfoObject.Instance.SerializeToByteArray());                
                sp_insert_comps.ExecuteNonQuery();

                retVal = true;
            }
            catch
            {
                retVal = false;
            }
            return retVal;
        }

        bool DeleteData(Guid propertyItem)
        {
            bool retVal = false;
            string cmdText = "DELETE FROM BuyersInfo WHERE piId = '" + propertyItem.ToString() + "'";

            try
            {
                SqlCommand deleteData = new SqlCommand(cmdText, _connection);
                deleteData.ExecuteNonQuery();
                retVal = true;
            }
            catch
            {
                retVal = false;
            }
            return retVal;
        }

    }
}
