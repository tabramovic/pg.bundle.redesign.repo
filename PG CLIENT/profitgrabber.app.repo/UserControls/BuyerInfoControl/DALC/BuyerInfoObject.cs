﻿using System;
using System.Collections.Generic;
using System.Text;

using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization.Formatters;
using System.Runtime.Serialization;
using System.IO;

namespace DealMaker
{
    public sealed class BuyerInfoObject
    {
        static BuyerInfoObject _instance = new BuyerInfoObject();

        static BuyerInfoObject()
        {}

        BuyerInfoObject()
        {}

        public static BuyerInfoObject Instance
        {
            get { return _instance; }
        }

        BuyerInfoData _buyerInfoData = new BuyerInfoData();

        public BuyerInfoData BuyerInfoDataProp
        {
            get { return _buyerInfoData; }
            set { _buyerInfoData = value; }
        }

        public void ResetObject()
        {
            _instance = new BuyerInfoObject();
        }

        public byte[] SerializeToByteArray()
        {
            try
            {
                BinaryFormatter bf = new BinaryFormatter();
                MemoryStream ms = new MemoryStream();
                bf.Serialize(ms, BuyerInfoObject.Instance.BuyerInfoDataProp);
                return ms.ToArray();
            }
            catch
            {
                return null;
            }
        }

        public BuyerInfoObject DeserializeFromByteArray(byte[] bytes)
        {
            try
            {
                MemoryStream ms = new MemoryStream(bytes);
                BinaryFormatter bf = new BinaryFormatter();

                ms.Position = 0;
                object obj = bf.Deserialize(ms);

                _instance.BuyerInfoDataProp = (BuyerInfoData)obj;
                return _instance;
            }
            catch
            {
                return null;
            }
        }
    }
}
