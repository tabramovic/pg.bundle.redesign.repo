﻿using System;
using System.Collections;
using System.Drawing;
using System.Drawing.Printing;

namespace DealMaker
{
    public enum ePosition
    {
        Center, 
        Left, 
        Right,
    }

    public class BaseBuyerInfoReportGenerator
    {
        public string CurrFormatter = "#,##0.00";
        public string CurrFormatterEx = "#,##0";
        public int FormatterLimit = 1000000;

        protected Graphics _gfx;
        protected PrintPageEventArgs _e;
        protected int _pageCnt = 0;
        protected int _paperWidth = 0;
        protected int _paperHeight;
        protected int _pageBreakIterator = 0;
        protected int _currBuyerIdx = 0;

        protected Brush _blackBrush = Brushes.Black;
        protected Pen _blackPen = Pens.Black;
        protected Font _arial8I = new Font("Arial", 8, FontStyle.Italic);
        protected Font _arial9 = new Font("Arial", 9);
        protected Font _arial10 = new Font("Arial", 10);
        protected Font _arial10B = new Font("Arial", 10, FontStyle.Bold);
        protected Font _arial10I = new Font("Arial", 10, FontStyle.Italic);
        protected Font _arial12 = new Font("Arial", 12);
        protected Font _arial12B = new Font("Arial", 12, FontStyle.Bold);
        protected Font _arial14 = new Font("Arial", 14);
        protected Font _arial16 = new Font("Arial", 16);
        protected Font _arial18 = new Font("Arial", 18);
        protected Font _arial20 = new Font("Arial", 20);
        protected Font _arial22 = new Font("Arial", 22);
        protected Font _arial24 = new Font("Arial", 24);
        protected Font _arial26 = new Font("Arial", 26);

        public BaseBuyerInfoReportGenerator()
        { }

        public void ResetGen()
        {
            _pageCnt = 0;
            _pageBreakIterator = 0;
            _currBuyerIdx = 0;
        }

        protected string[] GetPartialLinesFromString(Graphics gfx, Font font, int maxWidth, string stringToBreak)
        {
            ArrayList resultLists = new ArrayList();
            string tempLine = string.Empty;
            int i = stringToBreak.IndexOf(" ");
            while (-1 != i)
            {
                if ((int)(gfx.MeasureString(tempLine + stringToBreak.Substring(0, i), font).Width) < maxWidth)
                {
                    tempLine += stringToBreak.Substring(0, i + 1);
                    stringToBreak = stringToBreak.Substring(i + 1, stringToBreak.Length - (i + 1));
                }
                else
                {
                    resultLists.Add(tempLine.TrimEnd());
                    tempLine = string.Empty;
                }


                i = stringToBreak.IndexOf(" ");
            }

            tempLine += stringToBreak;
            resultLists.Add(tempLine);
            return (string[])resultLists.ToArray(typeof(string));
        }

        protected Point GetStringPosInACell(Graphics gfx, ePosition pos, string text, Font font, Rectangle rect)
        {
            SizeF size = gfx.MeasureString(text, font);
                        
            if (ePosition.Center == pos)
            {
                int x = (int)((rect.Width - size.Width) / 2);
                int y = (int)((rect.Height - size.Height) / 2);

                return new Point(rect.X + x, rect.Y + y);
            }

            return new Point(0, 0);
        }

        protected Point GetOrigin(Graphics gfx, ePosition pos, string text, Font font, int width, int currentY)
        {
            SizeF size = gfx.MeasureString(text, font);
            
            if (ePosition.Center == pos)
            {
                int x = (int)((width - size.Width) / 2);
                return new Point(x, currentY);
            }

            return new Point(0, 0);
        }

        protected Point GetOriginUponAlreadySetText(Graphics gfx, ePosition pos, string text, Point textPos, Font font)
        {
            SizeF size = gfx.MeasureString(text, font);

            if (ePosition.Right == pos)
            {
                int x = (int)(textPos.X + size.Width);
                return new Point(x, textPos.Y);
            }

            return new Point(0, 0);
        }
    }
}

