﻿using System;
using System.Collections;
using System.Drawing;
using System.Drawing.Printing;

namespace DealMaker
{
    public class BuyerInfoReportGenerator : BaseBuyerInfoReportGenerator
    {
        BuyerInfoData _obj = null;
        const string ReportName = "BUYER INFO SHEET";

        
        bool _dateOfRendered = false;
        bool _spokeWithRendered = false;
        bool _leadSourceRendered = false;
        bool _yourInterestRendered = false;
        bool _typeOfBuyerRendered = false;
        bool _creditRendered = false;
        bool _nameRendered = false;
        bool _agentRendered = false;
        bool _phoneNrRendered = false;
        bool _faxRendered = false;
        bool _emailRendered = false;
        bool _notesRendered = false;

        public BuyerInfoReportGenerator()
        {            
            
        }

        public void Generate(PrintPageEventArgs e, BuyerInfoData obj, string address)
        {
            _obj = obj;
            _e = e;
            _gfx = e.Graphics;
            _paperWidth = e.PageSettings.PaperSize.Width;
            _paperHeight = e.PageSettings.PaperSize.Height;

            int currY = 0;
            string[] lines = new string[1] { string.Empty };

            if (0 == _pageCnt)
            {
                _gfx.DrawString(ReportName, base._arial22, base._blackBrush, base.GetOrigin(_gfx, ePosition.Center, ReportName, base._arial22, _paperWidth, 20));

                currY = 60;
                //1st ROW
                _gfx.DrawString("PROPERTY SITE ADDRESS:", _arial12, _blackBrush, new PointF(20, currY));
                _gfx.DrawString(address, _arial12, _blackBrush, GetOriginUponAlreadySetText(_gfx, ePosition.Right, "PROPERTY SITE ADDRESS:", new Point(20, currY), _arial12));

                //2nd ROW                                    
                currY += 40;
                _gfx.DrawString("GENERAL NOTES ON SELLING:", _arial12, _blackBrush, new PointF(20, currY));

                //3rd ROW
                currY += 25;

                lines = GetPartialLinesFromString(_gfx, _arial12, _paperWidth - 40, _obj.GeneralNotes);
                if (null != lines && lines.Length > 0)
                {
                    foreach (string line in lines)
                    {
                        _gfx.DrawString(line, _arial12, _blackBrush, new PointF(20, currY));
                        currY += 18;
                    }
                }

                currY += 25;
            }

            _gfx.DrawString("Profitgrabber Pro - Buyer Info Report", base._arial10I, base._blackBrush, new Point(20, _paperHeight - 30));
            _gfx.DrawString("Page " + (_pageCnt + 1).ToString(), base._arial10I, base._blackBrush, new Point(_paperWidth - 60, _paperHeight - 30));
            
            currY += 25;
            

            if (null == obj.Buyers || 0 == obj.Buyers.Length)
                return;
            
            for (int i = _currBuyerIdx; i < obj.Buyers.Length; i++)
            {
                BuyerInfoDataSingleBuyerData buyer = obj.Buyers[i];
                if (!_dateOfRendered)
                {
                    if (currY + 40 < _paperHeight - 30)
                    {
                        currY += 25;
                        _gfx.DrawString("DATE OF:", _arial12, _blackBrush, new PointF(20, currY));
                        _gfx.DrawString(buyer.DateOf, _arial12, _blackBrush, GetOriginUponAlreadySetText(_gfx, ePosition.Right, "DATE OF:", new Point(20, currY), _arial12));
                        _dateOfRendered = true;
                    }
                    else
                    {
                        _pageCnt++;
                        e.HasMorePages = true;
                        return;
                    }
                }

                if (!_spokeWithRendered)
                {
                    if (currY + 40 < _paperHeight - 30)
                    {
                        currY += 25;
                        _gfx.DrawString("SPOKE WITH:", _arial12, _blackBrush, new PointF(20, currY));
                        _gfx.DrawString(buyer.SpokeWith, _arial12, _blackBrush, GetOriginUponAlreadySetText(_gfx, ePosition.Right, "SPOKE WITH:", new Point(20, currY), _arial12));
                        _spokeWithRendered = true;
                    }
                    else
                    {
                        _pageCnt++;
                        e.HasMorePages = true;
                        return;
                    }
                }

                if (!_leadSourceRendered)
                {
                    if (currY + 40 < _paperHeight - 30)
                    {
                        currY += 25;
                        _gfx.DrawString("LEAD SOURCE:", _arial12, _blackBrush, new PointF(20, currY));
                        _gfx.DrawString(buyer.LeadSource, _arial12, _blackBrush, GetOriginUponAlreadySetText(_gfx, ePosition.Right, "LEAD SOURCE:", new Point(20, currY), _arial12));
                        _leadSourceRendered = true;
                    }
                    else
                    {
                        _pageCnt++;
                        e.HasMorePages = true;
                        return;
                    }
                }

                if (!_yourInterestRendered)
                {
                    if (currY + 40 < _paperHeight - 30)
                    {
                        currY += 25;
                        _gfx.DrawString("YOUR INTEREST:", _arial12, _blackBrush, new PointF(20, currY));
                        _gfx.DrawString(buyer.YourInterest, _arial12, _blackBrush, GetOriginUponAlreadySetText(_gfx, ePosition.Right, "YOUR INTEREST:", new Point(20, currY), _arial12));
                        _yourInterestRendered = true;
                    }
                    else
                    {
                        _pageCnt++;
                        e.HasMorePages = true;
                        return;
                    }
                }

                if (!_typeOfBuyerRendered)
                {
                    if (currY + 40 < _paperHeight - 30)
                    {
                        currY += 25;
                        _gfx.DrawString("TYPE OF BUYER:", _arial12, _blackBrush, new PointF(20, currY));
                        _gfx.DrawString(buyer.TypeOfBuyer, _arial12, _blackBrush, GetOriginUponAlreadySetText(_gfx, ePosition.Right, "TYPE OF BUYER:", new Point(20, currY), _arial12));
                        _typeOfBuyerRendered = true;
                    }
                    else
                    {
                        _pageCnt++;
                        e.HasMorePages = true;
                        return;
                    }
                }

                if (!_creditRendered)
                {
                    if (currY + 40 < _paperHeight - 30)
                    {
                        currY += 25;
                        _gfx.DrawString("CREDIT:", _arial12, _blackBrush, new PointF(20, currY));
                        _gfx.DrawString(buyer.Credit, _arial12, _blackBrush, GetOriginUponAlreadySetText(_gfx, ePosition.Right, "CREDIT:", new Point(20, currY), _arial12));
                        _creditRendered = true;
                    }
                    else
                    {
                        _pageCnt++;
                        e.HasMorePages = true;
                        return;
                    }
                }

                if (!_nameRendered)
                {
                    if (currY + 40 < _paperHeight - 30)
                    {
                        currY += 25;
                        _gfx.DrawString("NAME:", _arial12, _blackBrush, new PointF(20, currY));
                        _gfx.DrawString(buyer.Name, _arial12, _blackBrush, GetOriginUponAlreadySetText(_gfx, ePosition.Right, "NAME:", new Point(20, currY), _arial12));
                        _nameRendered = true;
                    }
                    else
                    {
                        _pageCnt++;
                        e.HasMorePages = true;
                        return;
                    }
                }

                if (!_agentRendered)
                {
                    if (currY + 40 < _paperHeight - 30)
                    {
                        currY += 25;
                        _gfx.DrawString("AGENT:", _arial12, _blackBrush, new PointF(20, currY));
                        _gfx.DrawString(buyer.Agent, _arial12, _blackBrush, GetOriginUponAlreadySetText(_gfx, ePosition.Right, "AGENT:", new Point(20, currY), _arial12));
                        _agentRendered = true;
                    }
                    else
                    {
                        _pageCnt++;
                        e.HasMorePages = true;
                        return;
                    }
                }

                if (!_phoneNrRendered)
                {
                    if (currY + 40 < _paperHeight - 30)
                    {
                        currY += 25;
                        _gfx.DrawString("PHONE NR.:", _arial12, _blackBrush, new PointF(20, currY));
                        _gfx.DrawString(buyer.PhoneNr, _arial12, _blackBrush, GetOriginUponAlreadySetText(_gfx, ePosition.Right, "PHONE NR:", new Point(20, currY), _arial12));
                        _phoneNrRendered = true;
                    }
                    else
                    {
                        _pageCnt++;
                        e.HasMorePages = true;
                        return;
                    }
                }

                if (!_faxRendered)
                {
                    if (currY + 40 < _paperHeight - 30)
                    {
                        currY += 25;
                        _gfx.DrawString("FAX NR.:", _arial12, _blackBrush, new PointF(20, currY));
                        _gfx.DrawString(buyer.FaxNr, _arial12, _blackBrush, GetOriginUponAlreadySetText(_gfx, ePosition.Right, "FAX NR.:", new Point(20, currY), _arial12));
                        _faxRendered = true;
                    }
                    else
                    {
                        _pageCnt++;
                        e.HasMorePages = true;
                        return;
                    }
                }

                if (!_emailRendered)
                {
                    if (currY + 40 < _paperHeight - 30)
                    {
                        currY += 25;
                        _gfx.DrawString("EMAIL:", _arial12, _blackBrush, new PointF(20, currY));
                        _gfx.DrawString(buyer.Email, _arial12, _blackBrush, GetOriginUponAlreadySetText(_gfx, ePosition.Right, "EMAIL:", new Point(20, currY), _arial12));
                        _emailRendered = true;
                    }
                    else
                    {
                        _pageCnt++;
                        e.HasMorePages = true;
                        return;
                    }
                }


                lines = GetPartialLinesFromString(_gfx, _arial12, _paperWidth - 40, buyer.Notes);
                if (!_notesRendered)
                {
                    if (null != lines && (currY + (lines.Length * 25) + 15) < _paperHeight - 30)
                    {

                        currY += 25;
                        _gfx.DrawString("NOTES:", _arial12, _blackBrush, new PointF(20, currY));


                        if (null != lines && lines.Length > 0)
                        {
                            foreach (string line in lines)
                            {
                                _gfx.DrawString(line, _arial12, _blackBrush, GetOriginUponAlreadySetText(_gfx, ePosition.Right, "NOTES:", new Point(20, currY), _arial12));
                                currY += 18;
                            }
                        }
                        _notesRendered = true;
                    }
                    else
                    {
                        _pageCnt++;
                        e.HasMorePages = true;
                        return;
                    }
                }

                if (_dateOfRendered && _spokeWithRendered && _leadSourceRendered && _yourInterestRendered && _typeOfBuyerRendered && _creditRendered &&
                    _nameRendered && _agentRendered && _phoneNrRendered && _faxRendered && _emailRendered && _notesRendered )
                {
                    _currBuyerIdx++;

                    _dateOfRendered = false;
                    _spokeWithRendered = false;
                    _leadSourceRendered = false;
                    _yourInterestRendered = false;
                    _typeOfBuyerRendered = false;
                    _creditRendered = false;
                    _nameRendered = false;
                    _agentRendered = false;
                    _phoneNrRendered = false;
                    _faxRendered = false;
                    _emailRendered = false;
                    _notesRendered = false;

                }
            }
        }
    }
}
