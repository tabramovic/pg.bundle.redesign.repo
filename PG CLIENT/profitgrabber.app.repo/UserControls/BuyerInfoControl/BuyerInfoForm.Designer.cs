﻿namespace DealMaker
{
    partial class BuyerInfoForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this._dateOf = new System.Windows.Forms.DateTimePicker();
            this._yourInterest = new System.Windows.Forms.ComboBox();
            this._availDown = new System.Windows.Forms.TextBox();
            this._credit = new System.Windows.Forms.ComboBox();
            this._name = new System.Windows.Forms.TextBox();
            this._agent = new System.Windows.Forms.ComboBox();
            this._phone = new System.Windows.Forms.TextBox();
            this._fax = new System.Windows.Forms.TextBox();
            this._email = new System.Windows.Forms.TextBox();
            this._notes = new System.Windows.Forms.TextBox();
            this._save = new System.Windows.Forms.Button();
            this._spokeWith = new System.Windows.Forms.ComboBox();
            this._leadSource = new System.Windows.Forms.ComboBox();
            this._typeOfBuyer = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(42, 9);
            this.label1.Name = "label1";
            this.label1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label1.Size = new System.Drawing.Size(47, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Date Of:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(23, 42);
            this.label2.Name = "label2";
            this.label2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label2.Size = new System.Drawing.Size(66, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Spoke With:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(18, 75);
            this.label3.Name = "label3";
            this.label3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label3.Size = new System.Drawing.Size(71, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Lead Source:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(19, 108);
            this.label4.Name = "label4";
            this.label4.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label4.Size = new System.Drawing.Size(70, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Your Interest:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(13, 174);
            this.label5.Name = "label5";
            this.label5.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label5.Size = new System.Drawing.Size(76, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Avail. $ Down:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(52, 207);
            this.label6.Name = "label6";
            this.label6.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label6.Size = new System.Drawing.Size(37, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "Credit:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(51, 240);
            this.label7.Name = "label7";
            this.label7.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label7.Size = new System.Drawing.Size(38, 13);
            this.label7.TabIndex = 6;
            this.label7.Text = "Name:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(51, 273);
            this.label8.Name = "label8";
            this.label8.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label8.Size = new System.Drawing.Size(38, 13);
            this.label8.TabIndex = 7;
            this.label8.Text = "Agent:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(31, 306);
            this.label10.Name = "label10";
            this.label10.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label10.Size = new System.Drawing.Size(58, 13);
            this.label10.TabIndex = 9;
            this.label10.Text = "Phone Nr.:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(45, 339);
            this.label11.Name = "label11";
            this.label11.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label11.Size = new System.Drawing.Size(44, 13);
            this.label11.TabIndex = 10;
            this.label11.Text = "Fax Nr.:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(54, 372);
            this.label12.Name = "label12";
            this.label12.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label12.Size = new System.Drawing.Size(35, 13);
            this.label12.TabIndex = 11;
            this.label12.Text = "Email:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(51, 405);
            this.label13.Name = "label13";
            this.label13.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label13.Size = new System.Drawing.Size(38, 13);
            this.label13.TabIndex = 12;
            this.label13.Text = "Notes:";
            // 
            // _dateOf
            // 
            this._dateOf.Location = new System.Drawing.Point(95, 3);
            this._dateOf.Name = "_dateOf";
            this._dateOf.Size = new System.Drawing.Size(200, 20);
            this._dateOf.TabIndex = 13;
            // 
            // _yourInterest
            // 
            this._yourInterest.FormattingEnabled = true;
            this._yourInterest.Items.AddRange(new object[] {
            "-- Select --",
            "5 - Hot",
            "4 - Good",
            "3 - Average",
            "2 - Possible",
            "1 - Dead"});
            this._yourInterest.Location = new System.Drawing.Point(95, 105);
            this._yourInterest.Name = "_yourInterest";
            this._yourInterest.Size = new System.Drawing.Size(200, 21);
            this._yourInterest.TabIndex = 16;
            // 
            // _availDown
            // 
            this._availDown.Location = new System.Drawing.Point(95, 171);
            this._availDown.Name = "_availDown";
            this._availDown.Size = new System.Drawing.Size(115, 20);
            this._availDown.TabIndex = 17;
            // 
            // _credit
            // 
            this._credit.FormattingEnabled = true;
            this._credit.Items.AddRange(new object[] {
            "-- Select --",
            "Excellent",
            "Good",
            "Fair",
            "Poor",
            "Don\'t Know"});
            this._credit.Location = new System.Drawing.Point(95, 204);
            this._credit.Name = "_credit";
            this._credit.Size = new System.Drawing.Size(200, 21);
            this._credit.TabIndex = 18;
            // 
            // _name
            // 
            this._name.Location = new System.Drawing.Point(95, 237);
            this._name.Name = "_name";
            this._name.Size = new System.Drawing.Size(314, 20);
            this._name.TabIndex = 19;
            // 
            // _agent
            // 
            this._agent.FormattingEnabled = true;
            this._agent.Items.AddRange(new object[] {
            "-- Select --",
            "Yes",
            "No"});
            this._agent.Location = new System.Drawing.Point(95, 270);
            this._agent.Name = "_agent";
            this._agent.Size = new System.Drawing.Size(200, 21);
            this._agent.TabIndex = 20;
            // 
            // _phone
            // 
            this._phone.Location = new System.Drawing.Point(95, 303);
            this._phone.Name = "_phone";
            this._phone.Size = new System.Drawing.Size(314, 20);
            this._phone.TabIndex = 21;
            // 
            // _fax
            // 
            this._fax.Location = new System.Drawing.Point(95, 336);
            this._fax.Name = "_fax";
            this._fax.Size = new System.Drawing.Size(314, 20);
            this._fax.TabIndex = 22;
            // 
            // _email
            // 
            this._email.Location = new System.Drawing.Point(95, 369);
            this._email.Name = "_email";
            this._email.Size = new System.Drawing.Size(314, 20);
            this._email.TabIndex = 23;
            // 
            // _notes
            // 
            this._notes.Location = new System.Drawing.Point(95, 402);
            this._notes.Multiline = true;
            this._notes.Name = "_notes";
            this._notes.Size = new System.Drawing.Size(451, 57);
            this._notes.TabIndex = 24;
            // 
            // _save
            // 
            this._save.DialogResult = System.Windows.Forms.DialogResult.OK;
            this._save.Location = new System.Drawing.Point(485, 9);
            this._save.Name = "_save";
            this._save.Size = new System.Drawing.Size(75, 23);
            this._save.TabIndex = 25;
            this._save.Text = "Save";
            this._save.UseVisualStyleBackColor = true;
            // 
            // _spokeWith
            // 
            this._spokeWith.FormattingEnabled = true;
            this._spokeWith.Location = new System.Drawing.Point(95, 39);
            this._spokeWith.Name = "_spokeWith";
            this._spokeWith.Size = new System.Drawing.Size(200, 21);
            this._spokeWith.TabIndex = 26;
            // 
            // _leadSource
            // 
            this._leadSource.FormattingEnabled = true;
            this._leadSource.Location = new System.Drawing.Point(95, 72);
            this._leadSource.Name = "_leadSource";
            this._leadSource.Size = new System.Drawing.Size(200, 21);
            this._leadSource.TabIndex = 27;
            // 
            // _typeOfBuyer
            // 
            this._typeOfBuyer.FormattingEnabled = true;
            this._typeOfBuyer.Items.AddRange(new object[] {
            "-- Select --",
            "Lease Option",
            "Owner Finance",
            "Wholesale",
            "Cash",
            "Rent",
            "Conventional",
            "FHA",
            "VA",
            "Other"});
            this._typeOfBuyer.Location = new System.Drawing.Point(95, 138);
            this._typeOfBuyer.Name = "_typeOfBuyer";
            this._typeOfBuyer.Size = new System.Drawing.Size(200, 21);
            this._typeOfBuyer.TabIndex = 29;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(19, 141);
            this.label9.Name = "label9";
            this.label9.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label9.Size = new System.Drawing.Size(75, 13);
            this.label9.TabIndex = 28;
            this.label9.Text = "Type Of Buyer";
            // 
            // button1
            // 
            this.button1.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.button1.Location = new System.Drawing.Point(485, 37);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 30;
            this.button1.Text = "Cancel";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // BuyerInfoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.ClientSize = new System.Drawing.Size(572, 481);
            this.Controls.Add(this.button1);
            this.Controls.Add(this._typeOfBuyer);
            this.Controls.Add(this.label9);
            this.Controls.Add(this._leadSource);
            this.Controls.Add(this._spokeWith);
            this.Controls.Add(this._save);
            this.Controls.Add(this._notes);
            this.Controls.Add(this._email);
            this.Controls.Add(this._fax);
            this.Controls.Add(this._phone);
            this.Controls.Add(this._agent);
            this.Controls.Add(this._name);
            this.Controls.Add(this._credit);
            this.Controls.Add(this._availDown);
            this.Controls.Add(this._yourInterest);
            this.Controls.Add(this._dateOf);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "BuyerInfoForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Edit Buyer Info";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.DateTimePicker _dateOf;
        private System.Windows.Forms.ComboBox _yourInterest;
        private System.Windows.Forms.TextBox _availDown;
        private System.Windows.Forms.ComboBox _credit;
        private System.Windows.Forms.TextBox _name;
        private System.Windows.Forms.ComboBox _agent;
        private System.Windows.Forms.TextBox _phone;
        private System.Windows.Forms.TextBox _fax;
        private System.Windows.Forms.TextBox _email;
        private System.Windows.Forms.TextBox _notes;
        private System.Windows.Forms.Button _save;
        private System.Windows.Forms.ComboBox _spokeWith;
        private System.Windows.Forms.ComboBox _leadSource;
        private System.Windows.Forms.ComboBox _typeOfBuyer;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button button1;
    }
}