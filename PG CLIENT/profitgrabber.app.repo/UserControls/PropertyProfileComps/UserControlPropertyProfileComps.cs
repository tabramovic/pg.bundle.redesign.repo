
//#define NEW_DTD
////#define DUMP_XML_OBJECT

//using System;
//using System.Collections;
//using System.ComponentModel;
//using System.Drawing;
//using System.Data;
//using System.Windows.Forms;
//using System.IO;
//using System.Xml.Serialization;
//using System.Web;
//using System.Text;
//using System.Xml;
//using System.Net;
//using System.Diagnostics;
//using System.Data.SqlClient;
//using System.Drawing.Printing;

////Added
//using ProfitGrabber.Common;

////Floaters
//using DealMaker.Forms.Floaters;

//namespace DealMaker
//{
//    /// <summary>
//    /// Summary description for UserControl1.
//    /// </summary>
//    public class UserControlPropertyProfileComps : System.Windows.Forms.UserControl
//    {
//        //AUTOSAVE
//        public delegate void Success_PP_Download();
//        public event Success_PP_Download PP_Downloaded_Successfully;
//        bool _allowAutoSaveTriggerEvents = false;
//        public bool AllowAutoSaveTriggerEvents
//        {
//            get { return _allowAutoSaveTriggerEvents; }
//            set { _allowAutoSaveTriggerEvents = value; }
//        }

//        #region . variables .
//        DataConsumption.ServiceDataConsumption sdc = null;
		
//        private DealMaker.Forms.Floaters.PropertyProfile floater = null;
		
//        private DTD.Response.RESPONSE_GROUP response;
//        private DTD.Response.PROPERTY property;
//        private string comparableNumber;
//        private string distanceFromSubject;
//        private DealMaker.PropertyProfile[] dbPropertyProfile = null;
//        private SqlConnection _dbConn = null;
//        private bool _PPInDb = false;

		
//        /// <summary> 
//        /// Required designer variable.
//        /// </summary>
//        private System.ComponentModel.Container components = null;
//        private System.Windows.Forms.TextBox textBoxOwnerName;
//        private System.Windows.Forms.Label labelOwnerName;
//        private System.Windows.Forms.TextBox textBoxSchoolDistrict;
//        private System.Windows.Forms.TextBox textBoxLotNumber;
//        private System.Windows.Forms.TextBox textBoxSubdivision;
//        private System.Windows.Forms.TextBox textBoxComparableNumber;
//        private System.Windows.Forms.TextBox textBoxDistanceFromSubject;
//        private System.Windows.Forms.TabPage tabPagePropertyCharacteristics;
//        private System.Windows.Forms.TextBox textBoxCounty;
//        private System.Windows.Forms.Label labelSchoolDistrict;
//        private System.Windows.Forms.Label labelLotNumber;
//        private System.Windows.Forms.Label labelSubdivision;
//        private System.Windows.Forms.Label labelComparableNumber;
//        private System.Windows.Forms.Label labelDistanceFromSubject;
//        private System.Windows.Forms.Label labelCounty;
//        private System.Windows.Forms.TabPage tabPageOwnershipNLocations;
//        private System.Windows.Forms.TabControl tabControlOnlinePropertyProfileNComps;
//        private System.Windows.Forms.TabPage tabPageTaxesNLegal;
//        private System.Windows.Forms.TabPage tabPageFinancials;
//        private System.Windows.Forms.TextBox textBoxPostDirection;
//        private System.Windows.Forms.Label labelPostDirection;
//        private System.Windows.Forms.TextBox textBoxMailingZipCode;
//        private System.Windows.Forms.Label labelMailingZipCode;
//        private System.Windows.Forms.TextBox textBoxMailingCityAndState;
//        private System.Windows.Forms.Label labelMailingCityAndState;
//        private System.Windows.Forms.TextBox textBoxMailingAddress;
//        private System.Windows.Forms.Label labelMailingAddress;
//        private System.Windows.Forms.TextBox textBoxMapReferenceTwo;
//        private System.Windows.Forms.Label labelMapReferenceTwo;
//        private System.Windows.Forms.TextBox textBoxMapReferenceOne;
//        private System.Windows.Forms.Label labelMapReferenceOne;
//        private System.Windows.Forms.TextBox textBoxZipCode;
//        private System.Windows.Forms.Label labelZipCode;
//        private System.Windows.Forms.TextBox textBoxState;
//        private System.Windows.Forms.Label labelState;
//        private System.Windows.Forms.TextBox textBoxCity;
//        private System.Windows.Forms.Label labelCity;
//        private System.Windows.Forms.TextBox textBoxUnitNumber;
//        private System.Windows.Forms.Label labelUnitNumber;
//        private System.Windows.Forms.TextBox textBoxSuffix;
//        private System.Windows.Forms.Label labelSuffix;
//        private System.Windows.Forms.TextBox textBoxStreetnName;
//        private System.Windows.Forms.Label labelStreetnName;
//        private System.Windows.Forms.TextBox textBoxPreDirection;
//        private System.Windows.Forms.Label labelPreDirection;
//        private System.Windows.Forms.TextBox textBoxHouseNumber;
//        private System.Windows.Forms.Label labelHouseNumber;
//        private System.Windows.Forms.Label labelOwnerNameGroup;
//        private System.Windows.Forms.Label labelSiteAddress;
//        private System.Windows.Forms.TextBox textBoxTotalBaths;
//        private System.Windows.Forms.Label labelTotalBaths;
//        private System.Windows.Forms.TextBox textBoxTotalBedrooms;
//        private System.Windows.Forms.Label labelTotalBedrooms;
//        private System.Windows.Forms.TextBox textBoxTotalRooms;
//        private System.Windows.Forms.Label labelTotalRooms;
//        private System.Windows.Forms.TextBox textBoxZoning;
//        private System.Windows.Forms.Label labelZoning;
//        private System.Windows.Forms.TextBox textBoxNumberOfStories;
//        private System.Windows.Forms.Label labelNumberOfStories;
//        private System.Windows.Forms.TextBox textBoxAirConditioning;
//        private System.Windows.Forms.Label labelAirConditioning;
//        private System.Windows.Forms.TextBox textBoxPool;
//        private System.Windows.Forms.Label labelPool;
//        private System.Windows.Forms.TextBox textBoxRoofType;
//        private System.Windows.Forms.Label labelRoofType;
//        private System.Windows.Forms.TextBox textBoxParkingCapacity;
//        private System.Windows.Forms.Label labelParkingCapacity;
//        private System.Windows.Forms.TextBox textBoxParkingType;
//        private System.Windows.Forms.Label labelParkingType;
//        private System.Windows.Forms.TextBox textBoxLotSize;
//        private System.Windows.Forms.Label labelLotSize;
//        private System.Windows.Forms.TextBox textBoxLotArea;
//        private System.Windows.Forms.Label labelLotArea;
//        private System.Windows.Forms.TextBox textBoxTotalLivingArea;
//        private System.Windows.Forms.Label labelTotalLivingArea;
//        private System.Windows.Forms.TextBox textBoxYearBuilt;
//        private System.Windows.Forms.Label labelYearBuilt;
//        private System.Windows.Forms.TextBox textBoxSellerName;
//        private System.Windows.Forms.Label labelSellerName;
//        private System.Windows.Forms.TextBox textBox4PricePerSqFt;
//        private System.Windows.Forms.Label labelPricePerSqFt;
//        private System.Windows.Forms.TextBox textBoxCashDown;
//        private System.Windows.Forms.Label labelCashDown;
//        private System.Windows.Forms.TextBox textBoxFirstMortgageLender;
//        private System.Windows.Forms.Label labelFirstMortgageLender;
//        private System.Windows.Forms.TextBox textBoxType;
//        private System.Windows.Forms.Label labelFirstMortgageInterestRateType;
//        private System.Windows.Forms.TextBox textBoxFirstMortgageInterestRateTerm;
//        private System.Windows.Forms.Label labelFirstMortgageInterestRateTerm;
//        private System.Windows.Forms.TextBox textBoxFirstMortgageInterestRate;
//        private System.Windows.Forms.Label labelFirstMortgageInterestRate;
//        private System.Windows.Forms.TextBox textBoxFirstMortgageAmount;
//        private System.Windows.Forms.Label labelFirstMortgageAmount;
//        private System.Windows.Forms.TextBox textBoxRecordingDate;
//        private System.Windows.Forms.Label labelRecordingDate;
//        private System.Windows.Forms.TextBox textBoxDocumentNumber;
//        private System.Windows.Forms.Label labelDocumentNumber;
//        private System.Windows.Forms.TextBox textBoxDeedType;
//        private System.Windows.Forms.Label labelDeedType;
//        private System.Windows.Forms.TextBox textBoxSaleType1;
//        private System.Windows.Forms.Label labelSaleType1;
//        private System.Windows.Forms.TextBox textBoxSalePrice;
//        private System.Windows.Forms.Label labelSalePrice;
//        private System.Windows.Forms.TextBox textBoxSaleDate;
//        private System.Windows.Forms.Label labelSaleDate;
//        private System.Windows.Forms.TextBox textBoxTownshipName;
//        private System.Windows.Forms.Label labelTownshipName;
//        private System.Windows.Forms.TextBox textBoxLegalDescription;
//        private System.Windows.Forms.Label labelLegalDescription;
//        private System.Windows.Forms.TextBox textBoxDelinquentYear;
//        private System.Windows.Forms.Label labelDelinquentYear;
//        private System.Windows.Forms.TextBox textBoxImprovementValue;
//        private System.Windows.Forms.Label labelImprovementValue;
//        private System.Windows.Forms.TextBox textBoxLandValue;
//        private System.Windows.Forms.Label labelLandValue;
//        private System.Windows.Forms.TextBox textBoxLandUseType;
//        private System.Windows.Forms.Label labelLandUseType;
//        private System.Windows.Forms.TextBox textBoxExemptionType;
//        private System.Windows.Forms.Label labelExemptionType;
//        private System.Windows.Forms.TextBox textBoxTaxYear;
//        private System.Windows.Forms.Label labelTaxYear;
//        private System.Windows.Forms.TextBox textBoxTaxArea;
//        private System.Windows.Forms.Label labelTaxArea;
//        private System.Windows.Forms.TextBox textBoxPropertyTax;
//        private System.Windows.Forms.Label labelPropertyTax;
//        private System.Windows.Forms.TextBox textBoxAssesmentYear;
//        private System.Windows.Forms.Label labelAssesmentYear;
//        private System.Windows.Forms.TextBox textBoxTotalAssesedValue;
//        private System.Windows.Forms.Label labelTotalAssesedValue;
//        private System.Windows.Forms.TextBox textBoxAlternateAPN;
//        private System.Windows.Forms.Label labelAlternateAPN;
//        private System.Windows.Forms.TextBox textBoxAPN;
//        private System.Windows.Forms.Label labelAPN;
//        private System.Windows.Forms.TextBox textBoxPhoneNumber;
//        private System.Windows.Forms.TextBox textBoxLandUse;
//        private System.Windows.Forms.Label labelLandUse;
//        private System.Windows.Forms.TextBox textBoxLotWidth;
//        private System.Windows.Forms.Label label1;
//        private System.Windows.Forms.TextBox textBoxLotDepth;
//        private System.Windows.Forms.Label label2;
//        private System.Windows.Forms.TextBox textBoxGarageArea;
//        private System.Windows.Forms.Label label3;
//        private System.Windows.Forms.TextBox textBoxFireplaces;
//        private System.Windows.Forms.Label label4;
//        private System.Windows.Forms.TextBox textBoxRoofMaterials;
//        private System.Windows.Forms.Label label5;
//        private System.Windows.Forms.TextBox textBoxFoundation;
//        private System.Windows.Forms.Label label6;
//        private System.Windows.Forms.TextBox textBoxQuality;
//        private System.Windows.Forms.Label label7;
//        private System.Windows.Forms.TextBox textBoxCondition;
//        private System.Windows.Forms.Label label8;
//        private System.Windows.Forms.TextBox textBoxBasementArea;
//        private System.Windows.Forms.Label label9;
//        private System.Windows.Forms.TextBox textBoxBasement;
//        private System.Windows.Forms.Label label10;
//        private System.Windows.Forms.Label label11;
//        private System.Windows.Forms.Label label12;
//        private System.Windows.Forms.Label label13;
//        private System.Windows.Forms.Label label14;
//        private System.Windows.Forms.TextBox textBoxSaleType2;
//        private System.Windows.Forms.Label label15;
//        private System.Windows.Forms.Label label16;
//        private System.Windows.Forms.Label label17;
//        private System.Windows.Forms.Label label18;
//        private System.Windows.Forms.Label label20;
//        private System.Windows.Forms.Label label21;
//        private System.Windows.Forms.Label label22;
//        private System.Windows.Forms.Label label23;
//        private System.Windows.Forms.Label label24;
//        private System.Windows.Forms.Label label25;
//        private System.Windows.Forms.Label label26;
//        private System.Windows.Forms.Label label27;
//        private System.Windows.Forms.Label label28;
//        private System.Windows.Forms.Label label29;
//        private System.Windows.Forms.Label label19;
//        private System.Windows.Forms.Label label30;
//        private System.Windows.Forms.Label label31;
//        private System.Windows.Forms.Label label32;
//        private System.Windows.Forms.Label label33;
//        private System.Windows.Forms.Label label34;
//        private System.Windows.Forms.Label label35;
//        private System.Windows.Forms.Label label36;
//        private System.Windows.Forms.Label label37;
//        private System.Windows.Forms.Label label38;
//        private System.Windows.Forms.Label label39;
//        private System.Windows.Forms.Label label40;
//        private System.Windows.Forms.Label label41;
//        private System.Windows.Forms.Label label42;
//        private System.Windows.Forms.Label label43;
//        private System.Windows.Forms.Label label44;
//        private System.Windows.Forms.Label label45;
//        private System.Windows.Forms.Label label46;
//        private System.Windows.Forms.Label label47;
//        private System.Windows.Forms.Label label48;
//        private System.Windows.Forms.Label label49;
//        private System.Windows.Forms.Label label50;
//        private System.Windows.Forms.Label label51;
//        private System.Windows.Forms.Label label52;
//        private System.Windows.Forms.Label label53;
//        private System.Windows.Forms.Label label54;
//        private System.Windows.Forms.Label label55;
//        private System.Windows.Forms.Label label56;
//        private System.Windows.Forms.Label label58;
//        private System.Windows.Forms.Label label59;
//        private System.Windows.Forms.Label label60;
//        private System.Windows.Forms.Label label61;
//        private System.Windows.Forms.Label label62;
//        private System.Windows.Forms.TextBox textBoxStateC;
//        private System.Windows.Forms.TextBox textBoxZipC;
//        private System.Windows.Forms.TextBox textBoxLatNameC;
//        private System.Windows.Forms.TextBox textBoxFirstNameC;
//        private System.Windows.Forms.Label label63;
//        private System.Windows.Forms.Label label64;
//        private System.Windows.Forms.Label label65;
//        private System.Windows.Forms.Label label66;
//        private System.Windows.Forms.Label label67;
//        private System.Windows.Forms.TextBox textBoxStateB;
//        private System.Windows.Forms.TextBox textBoxZipB;
//        private System.Windows.Forms.TextBox textBoxApnB;
//        private System.Windows.Forms.Label label68;
//        private System.Windows.Forms.Label label69;
//        private System.Windows.Forms.Label label70;
//        private System.Windows.Forms.Label label71;
//        private System.Windows.Forms.Button buttonSearchC;
//        private System.Windows.Forms.Label label72;
//        private System.Windows.Forms.Button buttonSearchB;
//        private System.Windows.Forms.Label label73;
//        private System.Windows.Forms.TextBox textBoxStateA;
//        private System.Windows.Forms.TextBox textBoxZipA;
//        private System.Windows.Forms.TextBox textBoxUnitNoA;
//        private System.Windows.Forms.TextBox textBoxSuffixA;
//        private System.Windows.Forms.TextBox textBoxPostDirA;
//        private System.Windows.Forms.TextBox textBoxStreetA;
//        private System.Windows.Forms.TextBox textBoxPreDirA;
//        private System.Windows.Forms.TextBox textBoxStreetNoA;
//        private System.Windows.Forms.Label label74;
//        private System.Windows.Forms.Label label75;
//        private System.Windows.Forms.Label label76;
//        private System.Windows.Forms.Label label77;
//        private System.Windows.Forms.Label label78;
//        private System.Windows.Forms.Label label79;
//        private System.Windows.Forms.Label label80;
//        private System.Windows.Forms.Label label81;
//        private System.Windows.Forms.Label label82;
//        private System.Windows.Forms.Button buttonSearchA;
//        private System.Windows.Forms.Label label83;
//        private System.Windows.Forms.Label label84;
//        private System.Windows.Forms.TextBox textBoxCensusTract;
//        private System.Windows.Forms.TextBox textBoxLegalBlockBldg;
//        private System.Windows.Forms.TextBox textBoxLegalBookPage;
//        private System.Windows.Forms.TextBox textBoxPSMInterestRateType;
//        private System.Windows.Forms.TextBox textBoxPSMInterestRateTerm;
//        private System.Windows.Forms.TextBox textBoxPSMInterestRate;
//        private System.Windows.Forms.TextBox textBoxPSMMortgageAmount;
//        private System.Windows.Forms.TextBox textBoxPFMLender;
//        private System.Windows.Forms.TextBox textBoxPFMInterestRateType;
//        private System.Windows.Forms.TextBox textBoxPFMInterestRateTerm;
//        private System.Windows.Forms.TextBox textBoxPFMInterestRate;
//        private System.Windows.Forms.TextBox textBoxPFMMortgageAmount;
//        private System.Windows.Forms.TextBox textBoxPSStampAmt;
//        private System.Windows.Forms.TextBox textBoxPSDocumentNumber;
//        private System.Windows.Forms.TextBox textBoxPSRecordingDate;
//        private System.Windows.Forms.TextBox textBoxPSDeedType;
//        private System.Windows.Forms.TextBox textBoxPSSaleType1;
//        private System.Windows.Forms.TextBox textBoxPSSalePrice;
//        private System.Windows.Forms.TextBox textBoxPSSaleDate;
//        private System.Windows.Forms.TextBox textBoxSecInterestRateType;
//        private System.Windows.Forms.TextBox textBoxSecInterestRateTerm;
//        private System.Windows.Forms.TextBox textBoxSecInterestRate;
//        private System.Windows.Forms.TextBox textBoxSecMortgageAmount;
//        private System.Windows.Forms.TextBox textBoxStampAmt;
//        private System.Windows.Forms.TextBox textBoxTitleCompany;
//        private System.Windows.Forms.Panel panel1;
//        private System.Windows.Forms.Panel panel2;
//        private System.Windows.Forms.Label labelLine;
//        private System.Windows.Forms.Panel panel3;
//        private System.Windows.Forms.TextBox textBoxOtherImprovements;
//        private System.Windows.Forms.Label label57;
//        private System.Windows.Forms.Label label86;
//        private System.Windows.Forms.Label label87;
//        private System.Windows.Forms.TextBox textBoxCountyC;
//        private System.Windows.Forms.TextBox textBoxCountyB;
//        private System.Windows.Forms.TextBox textBoxCountyA;
//        private System.Windows.Forms.PictureBox pictureBoxTaxesLegal;
//        private System.Windows.Forms.PictureBox pictureBoxPropCharact;
//        private System.Windows.Forms.PictureBox pictureBoxFinancials;
//        private System.Windows.Forms.PictureBox pictureBoxOwnershipLocations;
//        private System.Windows.Forms.Label label88;
//        private System.Windows.Forms.Label label89;
//        private System.Windows.Forms.Label label85;
//        private System.Windows.Forms.Label label90;
//        private System.Windows.Forms.Button bPrintReport;
//        private System.Windows.Forms.Label labelPhoneNumber;
//        #endregion
//        #region . constructor .
//        public UserControlPropertyProfileComps()
//        {	
//            // TODO: do we need this?
//            System.Threading.Thread.CurrentThread.CurrentCulture=System.Globalization.CultureInfo.CreateSpecificCulture("EN-US");

//            // This call is required by the Windows.Forms Form Designer.
//            InitializeComponent();
//            panel3.Visible=false;
			
//            this.pictureBoxOwnershipLocations.MouseEnter += new EventHandler(Show_Floater);
//            this.pictureBoxOwnershipLocations.MouseLeave += new EventHandler(Hide_Floater);
			
//        }
//        #endregion
//        #region . connection .
//        public SqlConnection DBConn
//        {
//            get { return this._dbConn; }
//            set { this._dbConn = value; }
//        }
//        #endregion
//        #region . clear form .
//        public void clearForm()
//        {
//            foreach (object obj in this.Controls)
//            {
//                if (obj.GetType() == typeof(TextBox))
//                    ((TextBox)obj).Text = "";
//            }
//            foreach (TabPage tbp in this.tabControlOnlinePropertyProfileNComps.TabPages)
//            {
//                foreach (object obj in tbp.Controls)
//                {
//                    if (obj.GetType() == typeof(TextBox))
//                        ((TextBox)obj).Text = "";
//                    if (obj.GetType() == typeof(Panel))
//                        foreach (object obj2 in ((Panel)obj).Controls)
//                        {
//                            if (obj2.GetType() == typeof(TextBox))
//                                ((TextBox)obj2).Text = "";
//                        }
//                }
//            }
//            dbPropertyProfile = null;
//            _PPInDb = false;
//        }
//        #endregion . clear form .
//        #region . start .
//        public void setInitialData(
//            //string StreetNo
//            //, string PreDir
//            //, string StreetName
//            //, string PostDir
//            //, string StreetSuffix
//            //, string UnitOrAppartment
//            //, string ZIP
//            //, string County
//            //, string State
//            //, double SqFt
//             Guid PropertyGuid
//            , SqlConnection dbConn
//            )
//        {
//            clearForm();
//            _dbConn = dbConn;

//            /*textBoxStreetNoA.Text =StreetNo;
//            textBoxPreDirA.Text = PreDir;
//            textBoxStreetA.Text = StreetName;
//            textBoxPostDirA.Text = PostDir;
//            textBoxSuffixA.Text = StreetSuffix;
//            textBoxUnitNoA.Text = UnitOrAppartment;
//            textBoxZipA.Text = ZIP;
//            textBoxCountyA.Text = County;
//            textBoxStateA.Text = State;
//            textBoxOrigSqFt.Text = getNumber(originalSQFT.ToString());
//            */


//            dbPropertyProfile = DealMaker.PropertyProfile.Select(_dbConn, PropertyGuid);
//            if (dbPropertyProfile.Length==0)
//            {
//                dbPropertyProfile = new DealMaker.PropertyProfile[1];
//                dbPropertyProfile[0] = new DealMaker.PropertyProfile(PropertyGuid,  null);
//                _PPInDb=false;
//            }
//            else
//            {
//                MemoryStream stream = new MemoryStream(dbPropertyProfile[0].P_xmlStream);
//                XmlSerializer serializer = new XmlSerializer(typeof(DTD.Response.RESPONSE_GROUP));
//                response = (DTD.Response.RESPONSE_GROUP)serializer.Deserialize(stream);
//                if (CheckValid())
//                {
//                    //property = response.RESPONSE.RESPONSE_DATA[0].PROPERTY_INFORMATION_RESPONSE._PROPERTY_INFORMATION[0].PROPERTY;

//                    //NEW dtd 4.2.2.
//                    //BEGIN
//                    DTD.Response._PROPERTY_INFORMATION propInfo = null;				
//                    try
//                    {
//                        //propInfo = response.RESPONSE.RESPONSE_DATA[0].PROPERTY_INFORMATION_RESPONSE.Items[0] as DTD.Response._PROPERTY_INFORMATION;
//                        foreach (object o in response.RESPONSE.RESPONSE_DATA[0].PROPERTY_INFORMATION_RESPONSE.Items)
//                        {
//                            if (o is DTD.Response._PROPERTY_INFORMATION)
//                            {
//                                propInfo = o as DTD.Response._PROPERTY_INFORMATION;
//                                break;
//                            }
//                        }
//                    }
//                    catch 
//                    {
//                        propInfo = null;
//                    }
//                    if (null != propInfo && null != propInfo.PROPERTY)
//                        property = propInfo.PROPERTY;					
//                    //END
//                }
//                fillComps(property,"0","0");
//                _PPInDb=true;
//            }
//        }	
//        #endregion
//        #region . prepare form for comparable screening .
//        public void prepareComparable()
//        {
//            //120 DPI
//            panel3.Top = 5;
//            //120 DPI
//            panel2.Top = panel3.Bottom;
//            panel1.Visible=false;
//            panel3.Visible = true;
//            labelLine.Visible=false;
//        }
//        #endregion		

//        #region . fillComps .
//        public void fillComps
//            (
//            DTD.Response.PROPERTY _property
//            , string _comparableNumber
//            , string _distanceFromSubject
//            )
//        {
//            this.property = _property;
//            this.comparableNumber = _comparableNumber;
//            this.distanceFromSubject = _distanceFromSubject;

//            fillData();
//        }
//        #endregion
//        #region . fillData .
//        private void fillData()
//        {
//            try{textBoxOwnerName.Text = property.PROPERTY_OWNER[0]._OwnerName;}
//            catch{}
//            try{textBoxSchoolDistrict.Text = property._NEIGHBORHOOD_AREA_INFORMATION._SchoolDistrictName;}
//            catch{}
//            try{textBoxLotNumber.Text = property._LotIdentifier;}
//            catch{}
//            try{textBoxSubdivision.Text = property._SubdivisionIdentifier;}
//            catch{}
//            try{textBoxComparableNumber.Text = comparableNumber;}
//            catch{}
//            try{textBoxDistanceFromSubject.Text = distanceFromSubject;}
//            catch{}
//            try{textBoxCounty.Text = property._County;}
//            catch{}
//            try{textBoxPostDirection.Text = property._PARSED_STREET_ADDRESS._DirectionSuffix;}
//            catch{}
//            try{textBoxMailingZipCode.Text = property.PROPERTY_OWNER[0]._MailingPostalCode;}
//            catch{}
//            try{textBoxMailingCityAndState.Text = property.PROPERTY_OWNER[0]._MailingCityAndState;}
//            catch{}
//            try{textBoxMailingAddress.Text = property.PROPERTY_OWNER[0]._MailingAddress;}
//            catch{}
//            try{textBoxMapReferenceTwo.Text = property._NEIGHBORHOOD_AREA_INFORMATION._MapReferenceTwoIdentifier;}
//            catch{}
//            try{textBoxMapReferenceOne.Text = property._NEIGHBORHOOD_AREA_INFORMATION._MapReferenceOneIdentifier;}
//            catch{}
//            try{textBoxZipCode.Text = property._PostalCode;}
//            catch{}
//            try{textBoxState.Text = property._State;}
//            catch{}
//            try{textBoxCity.Text = property._City;}
//            catch{}
//            try{textBoxUnitNumber.Text = property._PARSED_STREET_ADDRESS._ApartmentOrUnit;}
//            catch{}
//            try{textBoxSuffix.Text = property._PARSED_STREET_ADDRESS._StreetSuffix;}
//            catch{}
//            try{textBoxStreetnName.Text = property._PARSED_STREET_ADDRESS._StreetName;}
//            catch{}
//            try{textBoxPreDirection.Text = property._PARSED_STREET_ADDRESS._DirectionPrefix;}
//            catch{}
//            try{textBoxHouseNumber.Text = property._PARSED_STREET_ADDRESS._HouseNumber;}
//            catch{}
//            try{textBoxTotalBaths.Text = property._PROPERTY_CHARACTERISTICS._IMPROVEMENTS._ROOM_COUNT._TotalBathsCount;}
//            catch{}
//            try{textBoxTotalBedrooms.Text = property._PROPERTY_CHARACTERISTICS._IMPROVEMENTS._ROOM_COUNT._TotalBedroomsCount;}
//            catch{}
//            try{textBoxTotalRooms.Text = property._PROPERTY_CHARACTERISTICS._IMPROVEMENTS._ROOM_COUNT._TotalRoomCount;}
//            catch{}
//            try{textBoxZoning.Text = property._PROPERTY_CHARACTERISTICS._SITE._ZONING._ClassificationIdentifier;}
//            catch{}
//            try{textBoxNumberOfStories.Text = property._PROPERTY_CHARACTERISTICS._IMPROVEMENTS._GENERAL_DESCRIPTION._TotalStoriesNumber;}
//            catch{}
//            try{textBoxFireplaces.Text = property._PROPERTY_CHARACTERISTICS._IMPROVEMENTS._FEATURES._FIREPLACES._HasFeatureIndicator;}
//            catch{}
//            try{textBoxAirConditioning.Text = property._PROPERTY_CHARACTERISTICS._IMPROVEMENTS._COOLING._CentralizedIndicator;}
//            catch{}
//            try{textBoxPool.Text = property._PROPERTY_CHARACTERISTICS._IMPROVEMENTS._FEATURES._POOL._HasFeatureIndicator;}
//            catch{}
//            try{textBoxRoofType.Text = property._PROPERTY_CHARACTERISTICS._IMPROVEMENTS._EXTERIOR_DESCRIPTION._RoofTypeDescription;}
//            catch{}
//            try{textBoxParkingCapacity.Text = property._PROPERTY_CHARACTERISTICS._IMPROVEMENTS._PARKING._GarageTotalCarCount;}
//            catch{}
//            try{textBoxParkingType.Text = property._PROPERTY_CHARACTERISTICS._IMPROVEMENTS._PARKING._TypeDescription;}
//            catch{}
//            try{textBoxLotSize.Text = getDecimal(property._PROPERTY_CHARACTERISTICS._SITE._DIMENSIONS._LotAreaAcresNumber);}
//            catch{}
//            try{textBoxLotArea.Text = getDecimal(property._PROPERTY_CHARACTERISTICS._SITE._DIMENSIONS._LotAreaSquareFeetNumber);}
//            catch{}
//            try{textBoxYearBuilt.Text = property._PROPERTY_CHARACTERISTICS._IMPROVEMENTS._GENERAL_DESCRIPTION._YearBuiltDateIdentifier;}
//            catch{}
//            try{textBoxSecInterestRateType.Text = property._PROPERTY_HISTORY._MORTGAGE_HISTORY[0]._SecondMortgageInterestRateTypeDescription;}
//            catch{}
//            try{textBoxSecInterestRateTerm.Text = property._PROPERTY_HISTORY._MORTGAGE_HISTORY[0]._SecondMortgageTermNumber;}
//            catch{}
//            try{textBoxSecInterestRate.Text = getDecimal(property._PROPERTY_HISTORY._MORTGAGE_HISTORY[0]._SecondMortgageInterestRatePercent);}
//            catch{}
//            try{textBoxSecMortgageAmount.Text = getDecimal(property._PROPERTY_HISTORY._MORTGAGE_HISTORY[0]._SecondMortgageAmount);}
//            catch{}
//            try{textBoxSellerName.Text = property._PROPERTY_HISTORY._SALES_HISTORY[0]._SellerName;}
//            catch{}
//            try{textBoxPSSalePrice.Text = getDecimal(property._PROPERTY_HISTORY._SALES_HISTORY[0]._PriorSalePriceAmount);}
//            catch{}
//            try{textBoxPSSaleDate.Text = getDate(property._PROPERTY_HISTORY._SALES_HISTORY[0]._PriorSaleDate /*_LastSalesDate*/);}
//            catch{}
//            try{textBox4PricePerSqFt.Text = getDecimal(property._PROPERTY_HISTORY._SALES_HISTORY[0]._PricePerSquareFootAmount);}
//            catch{}
//            try{textBoxCashDown.Text = getDecimal(property._PROPERTY_HISTORY._SALES_HISTORY[0]._CashDownAmount);}
//            catch{}
//            try{textBoxFirstMortgageLender.Text = property._PROPERTY_HISTORY._SALES_HISTORY[0]._LenderName;}
//            catch{}
//            try{textBoxType.Text = property._PROPERTY_HISTORY._MORTGAGE_HISTORY[0]._FirstMortgageInterestRateTypeDescription;}
//            catch{}
//            try{textBoxFirstMortgageInterestRateTerm.Text = property._PROPERTY_HISTORY._MORTGAGE_HISTORY[0]._FirstMortgageTermNumber;}
//            catch{}
//            try{textBoxFirstMortgageInterestRate.Text = getDecimal(property._PROPERTY_HISTORY._MORTGAGE_HISTORY[0]._FirstMortgageInterestRatePercent);}
//            catch{}
//            try{textBoxFirstMortgageAmount.Text = getDecimal(property._PROPERTY_HISTORY._MORTGAGE_HISTORY[0]._FirstMortgageAmount);}
//            catch{}
//            try{textBoxRecordingDate.Text = getDate(property._PROPERTY_HISTORY._SALES_HISTORY[0]._LastRecordingDate);}
//            catch{}
//            try{textBoxDocumentNumber.Text = property._PROPERTY_HISTORY._SALES_HISTORY[0]._DocumentNumberIdentifier;}
//            catch{}
//            try{textBoxDeedType.Text = property._PROPERTY_HISTORY._SALES_HISTORY[0]._DeedTypeDescription;}
//            catch{}
//            try{textBoxSaleType1.Text = property._PROPERTY_HISTORY._SALES_HISTORY[0]._OneSaleTypeDescription;}
//            catch{}
//            try{textBoxSalePrice.Text = getDecimal(property._PROPERTY_HISTORY._SALES_HISTORY[0]._LastSalesPriceAmount);}
//            catch{}
//            try{textBoxSaleDate.Text = getDate(property._PROPERTY_HISTORY._SALES_HISTORY[0]._LastSalesDate);}
//            catch{}
//            try{textBoxTownshipName.Text = property._Municipality;}
//            catch{}
//            try{textBoxLegalDescription.Text = property._LEGAL_DESCRIPTION._TextDescription;}
//            catch{}
//            try{textBoxDelinquentYear.Text = property._PROPERTY_TAX._DelinquentYear;}
//            catch{}
//            try{textBoxImprovementValue.Text = getDecimal(property._PROPERTY_TAX._ImprovementValueAmount);}
//            catch{}
//            try{textBoxLandValue.Text = getDecimal(property._PROPERTY_TAX._LandValueAmount);}
//            catch{}
//            try{textBoxLandUse.Text = property._PROPERTY_CHARACTERISTICS._SITE._CHARACTERISTICS._LandUseDescription;}
//            catch{}
//            try{textBoxLandUseType.Text = property._PROPERTY_CHARACTERISTICS._SITE._CHARACTERISTICS._LandUseDescription;}
//            catch{}
//            try{textBoxExemptionType.Text = property._PROPERTY_TAX._ExemptionTypeDescription;}
//            catch{}
//            try{textBoxTaxYear.Text = property._PROPERTY_TAX._TaxYear;}
//            catch{}
//            try{textBoxTaxArea.Text = property._PROPERTY_TAX._RateAreaIdentifier;}
//            catch{}
//            try{textBoxPropertyTax.Text = getDecimal(property._PROPERTY_TAX._RealEstateTotalTaxAmount);}
//            catch{}
//            try{textBoxAssesmentYear.Text = property._PROPERTY_TAX._AssessmentYear;}
//            catch{}
//            try{textBoxTotalAssesedValue.Text = getDecimal(property._PROPERTY_TAX._TotalAssessedValueAmount);}
//            catch{}
//            try{textBoxAlternateAPN.Text = property._AssessorsAlternateParcelIdentifier;}
//            catch{}
//            try{textBoxAPN.Text = property._AssessorsParcelIdentifier;}
//            catch{}
//            try{textBoxPhoneNumber.Text = property.PROPERTY_OWNER[0]._PhoneNumber;}
//            catch{}
//            try{textBoxOtherImprovements.Text = property.PROPERTY_ANALYSIS._IMPROVEMENT_ANALYSIS._OtherPropertyImprovementsDescription;}
//            catch{}
//            try{textBoxTotalLivingArea.Text = getDecimal(property._PROPERTY_CHARACTERISTICS._IMPROVEMENTS._ROOM_COUNT._TotalLivingAreaSquareFeetNumber);}
//            catch{}
//            //textBoxSaleType2.Text=

//            try
//            {
//                textBoxStreetnName.Text = 
//                    textBoxHouseNumber.Text + " " 
//                    + textBoxPreDirection.Text + " " 
//                    + textBoxStreetnName.Text + " "
//                    + textBoxPostDirection.Text + " "
//                    + textBoxSuffix.Text + " "
//                    + textBoxUnitNumber.Text + " ";}
//            catch{}
//        }
//        #endregion
//        #region . Search A .
//        private void buttonSearchA_Click(object sender, System.EventArgs e)
//        {

			
//            DialogResult dlg = 
//                MessageBox.Show
//                (
//                "You will incur charge or credit reduction!\nDo you want to continue?"
//                , "Warning!"
//                , MessageBoxButtons.YesNo
//                , MessageBoxIcon.Warning
//                );
//            if (dlg == DialogResult.No)
//            {
//                return;
//            }
			
//            int totalCost = 0;
//            int totalAmount = 0;
//            string errMsg;
//            int monthlyCredits;
//            int topUpCredits;

//            if (!UserCredits.Instance.TestForEnough(Globals.DataConsumptionReg.KeyCode /*DealMaker.Globals.UserRegistration.IDUser*/, TypeOfCharge.PP, 1, out totalCost, out totalAmount, out monthlyCredits, out topUpCredits, out errMsg))
//            {									
//                UserCreditForm ucf = new UserCreditForm(DealMaker.Globals.UserRegistration.IDUser, monthlyCredits, topUpCredits, totalAmount, TypeOfCharge.PP, 1, totalCost);
//                ucf.ShowDialog();
//                return;
//            }

//            /*string log_filename =
//                @".\PropertyProfile_usage_trace.log";
//            TextWriter txtwrtr = new StreamWriter(log_filename,
//                true, Encoding.UTF8);
//            txtwrtr.WriteLine
//                (
//                DateTime.Now.ToString("yyyy.MM.dd HH:mm:ss")
//                + " " +
//                "Property Profile Requested"
//                );
//            txtwrtr.Flush();
//            txtwrtr.Close();*/

//            DTD.Request._PROPERTY_CRITERIA pc 
//                = new DTD.Request._PROPERTY_CRITERIA();

//            DTD.Request.PARSED_STREET_ADDRESS psa 
//                = new DTD.Request.PARSED_STREET_ADDRESS();

//            psa._HouseNumber = textBoxStreetNoA.Text;
//            psa._DirectionPrefix = textBoxPreDirA.Text;
//            psa._StreetName = textBoxStreetA.Text;
//            psa._DirectionSuffix =textBoxPostDirA.Text;
//            psa._StreetSuffix = textBoxSuffixA.Text;
//            psa._ApartmentOrUnit = textBoxUnitNoA.Text;

//            pc.PARSED_STREET_ADDRESS = psa;
//            pc._PostalCode = textBoxZipA.Text;
//            pc._County = textBoxCountyA.Text;
//            pc._State = textBoxStateA.Text;

//            getSubject(null,pc);
//        }
//        #endregion
//        #region . Search B .
//        private void buttonSearchB_Click(object sender, System.EventArgs e)
//        {
			
//            DialogResult dlg = 
//                MessageBox.Show
//                (
//                "You will incur charge or credit reduction!\nDo you want to continue?"
//                , "Warning!"
//                , MessageBoxButtons.YesNo
//                , MessageBoxIcon.Warning
//                );
//            if (dlg == DialogResult.No)
//            {
//                return;
//            }

//            int totalCost = 0;
//            int totalAmount = 0;
//            string errMsg;
//            int monthlyCredits;
//            int topUpCredits;

//            if (!UserCredits.Instance.TestForEnough(Globals.DataConsumptionReg.KeyCode /*DealMaker.Globals.UserRegistration.IDUser*/, TypeOfCharge.PP, 1, out totalCost, out totalAmount, out monthlyCredits, out topUpCredits, out errMsg))
//            {									
//                UserCreditForm ucf = new UserCreditForm(DealMaker.Globals.UserRegistration.IDUser, monthlyCredits, topUpCredits, totalAmount, TypeOfCharge.PP, 1, totalCost);
//                ucf.ShowDialog();
//                return;
//            }

//            /*string log_filename =
//                @".\PropertyProfile_usage_trace.log";
//            TextWriter txtwrtr = new StreamWriter(log_filename,
//                true, Encoding.UTF8);
//            txtwrtr.WriteLine
//                (
//                DateTime.Now.ToString("yyyy.MM.dd HH:mm:ss")
//                + " " +
//                "Property Profile Requested"
//                );
//            txtwrtr.Flush();
//            txtwrtr.Close();*/

//            DTD.Request._PROPERTY_CRITERIA pc 
//                = new DTD.Request._PROPERTY_CRITERIA();

//            DTD.Request._SUBJECT_SEARCH ss 
//                = new DTD.Request._SUBJECT_SEARCH();

		
//            ss._AssessorsParcelIdentifier = textBoxApnB.Text;
//            pc._PostalCode = textBoxZipB.Text;
//            pc._County = textBoxCountyB.Text;
//            pc._State = textBoxStateB.Text;

//            getSubject(ss, pc);
//        }
//        #endregion
//        #region . Search C .
//        private void buttonSearchC_Click(object sender, System.EventArgs e)
//        {
			
//            DialogResult dlg = 
//                MessageBox.Show
//                (
//                "You will incur charge or credit reduction!\nDo you want to continue?"
//                , "Warning!"
//                , MessageBoxButtons.YesNo
//                , MessageBoxIcon.Warning
//                );
//            if (dlg == DialogResult.No)
//            {
//                return;
//            }

//            int totalCost = 0;
//            int totalAmount = 0;
//            string errMsg;
//            int monthlyCredits;
//            int topUpCredits;

//            if (!UserCredits.Instance.TestForEnough(Globals.DataConsumptionReg.KeyCode /*DealMaker.Globals.UserRegistration.IDUser*/, TypeOfCharge.PP, 1, out totalCost, out totalAmount, out monthlyCredits, out topUpCredits, out errMsg))
//            {									
//                UserCreditForm ucf = new UserCreditForm(DealMaker.Globals.UserRegistration.IDUser, monthlyCredits, topUpCredits, totalAmount, TypeOfCharge.PP, 1, totalCost);
//                ucf.ShowDialog();
//                return;
//            }

//            /*string log_filename =
//                @".\PropertyProfile_usage_trace.log";
//            TextWriter txtwrtr = new StreamWriter(log_filename,
//                true, Encoding.UTF8);
//            txtwrtr.WriteLine
//                (
//                DateTime.Now.ToString("yyyy.MM.dd HH:mm:ss")
//                + " " +
//                "Property Profile Requested"
//                );
//            txtwrtr.Flush();
//            txtwrtr.Close();*/
			
//            DTD.Request._PROPERTY_CRITERIA pc 
//                = new DTD.Request._PROPERTY_CRITERIA();

//            DTD.Request._SUBJECT_SEARCH ss 
//                = new DTD.Request._SUBJECT_SEARCH();
			
//            ss._OwnerFirstName = textBoxFirstNameC.Text;
//            ss._OwnerLastName = textBoxLatNameC.Text;
//            pc._PostalCode = textBoxZipC.Text;
//            pc._County = textBoxCountyC.Text;
//            pc._State = textBoxStateC.Text;


//            getSubject(ss,pc);
//        }
//        #endregion
//        #region . Build Request .
//        private DTD.Request.REQUEST_GROUP buildRequest(
//            DTD.Request._SUBJECT_SEARCH ss
//            , DTD.Request._PROPERTY_CRITERIA pc)
//        {
//            DTD.Request.REQUEST_GROUP rqg 
//                = new DTD.Request.REQUEST_GROUP();


//            rqg.REQUEST 
//                = new DTD.Request.REQUEST();
//            rqg.REQUEST.REQUESTDATA 
//                = new DTD.Request.REQUESTDATA[1];
//            rqg.REQUEST.REQUESTDATA[0] 
//                = new DTD.Request.REQUESTDATA();
//            rqg.REQUEST.REQUESTDATA[0].PROPERTY_INFORMATION_REQUEST 
//                = new DTD.Request.PROPERTY_INFORMATION_REQUEST();
//            rqg.REQUEST.REQUESTDATA[0].PROPERTY_INFORMATION_REQUEST._SEARCH_CRITERIA 
//                = new DTD.Request._SEARCH_CRITERIA();

//            DTD.Request._CONNECT2DATA_PRODUCT c2dp
//                = new DTD.Request._CONNECT2DATA_PRODUCT();

//            c2dp._DetailedSubjectReport
//                = DTD.Request._CONNECT2DATA_PRODUCT_DetailedSubjectReport.Y;

//            rqg.REQUEST.REQUESTDATA[0].PROPERTY_INFORMATION_REQUEST._CONNECT2DATA_PRODUCT = c2dp;
			
//            rqg.REQUEST.REQUESTDATA[0].PROPERTY_INFORMATION_REQUEST._ActionType 
//                = DTD.Request.PROPERTY_INFORMATION_REQUEST_ActionType.Submit;
//            rqg.REQUEST.REQUESTDATA[0].PROPERTY_INFORMATION_REQUEST._ActionTypeSpecified = true;

//            rqg.REQUEST.REQUESTDATA[0].PROPERTY_INFORMATION_REQUEST._PROPERTY_CRITERIA
//                = pc;
//            //rqg.REQUEST.REQUESTDATA[0].PROPERTY_INFORMATION_REQUEST._SEARCH_CRITERIA.Item 
//            //	= ss;

//            //dtd 4.2.2
//            //BEGIN
//            //OLD
//#if !NEW_DTD
//            rqg.REQUEST.REQUESTDATA[0].PROPERTY_INFORMATION_REQUEST._SEARCH_CRITERIA.Item = ss;
//#else
//            //NEW
//            if (null != ss)
//                rqg.REQUEST.REQUESTDATA[0].PROPERTY_INFORMATION_REQUEST._SEARCH_CRITERIA.Items = new object[1] { ss };			
//            else 
//                rqg.REQUEST.REQUESTDATA[0].PROPERTY_INFORMATION_REQUEST._SEARCH_CRITERIA.Items = null;			
//#endif
//            //END

//            rqg.REQUESTING_PARTY 
//                = new DTD.Request.REQUESTING_PARTY[1];
			
//            rqg.MISMOVersionID = "2.1";

//            DTD.Request._RESPONSE_CRITERIA rescrit 
//                = new DTD.Request._RESPONSE_CRITERIA();

//            rescrit._NumberSubjectPropertiesType 
//                = DTD.Request._RESPONSE_CRITERIA_NumberSubjectPropertiesType.Item100;
			
//            rqg.REQUEST.REQUESTDATA[0].PROPERTY_INFORMATION_REQUEST._RESPONSE_CRITERIA
//                = rescrit;


//            /*
//            rqg.REQUESTING_PARTY[0] 
//                = new DTD.Request.REQUESTING_PARTY();
			
//            rqg.REQUESTING_PARTY[0]._Name="Blue Sky Lending"; 
//            rqg.REQUESTING_PARTY[0]._StreetAddress="123 Main"; 
//            rqg.REQUESTING_PARTY[0]._City="Anaheim"; 
//            rqg.REQUESTING_PARTY[0]._State="Ca"; 
//            rqg.REQUESTING_PARTY[0]._PostalCode="92840";

//            rqg.REQUESTING_PARTY[0].PREFERRED_RESPONSE
//                = new DTD.Request.PREFERRED_RESPONSE[1];

//            rqg.REQUESTING_PARTY[0].PREFERRED_RESPONSE[0]
//                = new DTD.Request.PREFERRED_RESPONSE();

//            rqg.REQUESTING_PARTY[0].PREFERRED_RESPONSE[0]._Format="XML";
//            rqg.REQUESTING_PARTY[0].PREFERRED_RESPONSE[0]._DestinationDescription="";

//            rqg.RECEIVING_PARTY = new DTD.Request.RECEIVING_PARTY();


//            rqg.RECEIVING_PARTY._Name="Blue Sky Lending"; 
//            rqg.RECEIVING_PARTY._StreetAddress="123 Main";
//            rqg.RECEIVING_PARTY._City="Anaheim";
//            rqg.RECEIVING_PARTY._State="Ca";
//            rqg.RECEIVING_PARTY._PostalCode="92840";
//            */			

//            rqg.REQUEST._JobIdentifier="TEST MASTER V2.0"; 
//            rqg.REQUEST._RecordIdentifier="12345A8";
//            rqg.REQUEST._HVERequestTypeSpecified = true;
//            rqg.REQUEST._HVERequestType 
//                = DTD.Request.REQUEST_HVERequestType.Item02;
//            rqg.REQUEST._HVCustomerIdentifier="";

//            return rqg;
//        }
//        #endregion
//        #region . getSubject .
//        private void getSubject(
//            DTD.Request._SUBJECT_SEARCH ss
//            , DTD.Request._PROPERTY_CRITERIA pc)
//        {
//            DTD.Request.REQUEST_GROUP request = buildRequest(ss, pc);
//#if DUMP_XML_OBJECT
//            //dumpObject(request, @"c:\req.xml");
//#endif
			
//            XmlSerializer requestSerializer  
//                = new XmlSerializer(typeof(DTD.Request.REQUEST_GROUP));
//            XmlSerializer responseSerializer  
//                = new XmlSerializer(typeof(DTD.Response.RESPONSE_GROUP));

//            MemoryStream requestStream = new MemoryStream();
//            MemoryStream responseStream = new MemoryStream();

//            // IMPORTANT  -  we do not need a namespace!
//            System.Xml.Serialization.XmlSerializerNamespaces nmspace = new XmlSerializerNamespaces();
//            nmspace.Add("","");
//            // IMPORTANT  -  we do not need a namespace!
				
//            System.Xml.XmlWriter writer = new XmlTextWriter(requestStream, System.Text.UTF8Encoding.UTF8);
//            //writer.Formatting = System.Xml.Formatting.Indented;
//            writer.WriteStartDocument();
//            //TODO: define string for request..
//            writer.WriteDocType("REQUEST_GROUP", null, "C2DRequestv2.0.dtd",null);
//            requestSerializer.Serialize(writer, request,nmspace);
//            writer.Flush();
	
//            //requestSerializer.Serialize(requestStream, request);
//            byte[] requestArray = null;
//            byte[] responseArray = null;
//            string resultMessage = string.Empty;
//            try
//            {
//                Cursor.Current = Cursors.WaitCursor;
//                Application.DoEvents();


//                int count =(int)requestStream.Length-3;
//                requestArray = new byte[count];
//                long newPos = requestStream.Seek(0, SeekOrigin.Begin);
		
//                //TA++:08.04.2005
//                byte[] reqStream = requestStream.ToArray();
//                for (int i = 3; i < requestStream.Length; i++)
//                {
//                    requestArray[i - 3] = (byte)reqStream[i];
//                }
//                //TA--:08.04.2005
//                writer.Close();

//                // ++ web
//                //DataConsumption.ServiceDataConsumption sdc
//                sdc	= new DealMaker.DataConsumption.ServiceDataConsumption();
			
//                if (string.Empty != Globals.ProfitGrabberWebSupportURLDataConsumption)
//                {
//                    sdc.Url = Globals.ProfitGrabberWebSupportURLDataConsumption;
//                }
		
				

//                sdc.RequestDataPropertyProfile(
//                    Globals.DataConsumptionReg,
//                    requestArray,
//                    out responseArray, 
//                    out resultMessage);
//                // -- web

//                // ++ local
//                //			SoftwareRegistration.ClassLibrary.WebServiceCore.Connect(requestArray,
//                //				out responseArray);
//                // -- local

//                //			System.IO.FileStream fs = new FileStream("response2.xml", FileMode.Create);
//                //			fs.Write((byte[])responseStream.ToArray(), 0, (int)responseStream.Length);
//                //			fs.Flush();
//                //			fs.Close();

//                //sdc = null;

//                if (null == responseArray)
//                {
//                    if (string.Empty == resultMessage)
//                    {
//                        MessageBox.Show(this, "Web services did not answer correctly.", "Web Services", MessageBoxButtons.OK, MessageBoxIcon.Information);
//                    }
//                    else
//                    {
//                        if (resultMessage == Globals.NoCreditForGivenMonth)
//                        {
//                            MessageBox.Show(this, "You don't have any credits for this month!\nGo to Property Data on the menu above, and select Subscribe To Data!", "Get the data advantage!", MessageBoxButtons.OK, MessageBoxIcon.Information);
//                        }
//                        else
//                        {
//                            MessageBox.Show(this, resultMessage, "Web Services", MessageBoxButtons.OK, MessageBoxIcon.Information);
//                        }
//                    }
//                    return;
//                }
				

//                if (0==responseArray.Length)
//                {
//                    //TA: We don't just throw exceptions around...
//                    //throw new Exception
//                        //(resultMessage + " - at GetSubject");

//                    MessageBox.Show(this, "Web services answered incorrectly.", "Web Services", MessageBoxButtons.OK, MessageBoxIcon.Information);
//                    return;

//                }
//                else
//                {
//                    //TA: WE take out LOGS
//                    /*string log_filename =
//                        @".\PropertyProfile_usage_trace.log";
//                    TextWriter txtwrtr = new StreamWriter(log_filename,
//                        true, Encoding.UTF8);
//                    txtwrtr.WriteLine
//                        (
//                        DateTime.Now.ToString("yyyy.MM.dd HH:mm:ss")
//                        + " " +
//                        "Property Profile Request Successfull!"
//                        );
//                    txtwrtr.Flush();
//                    txtwrtr.Close();*/
//                }

//                responseStream.Write(responseArray,0,responseArray.Length);

//                responseStream.Position = 0;
//                response = (DTD.Response.RESPONSE_GROUP)responseSerializer.Deserialize(responseStream);
//#if DUMP_XML_OBJECT
//                //dumpObject(response, @"c:\response.xml");
//#endif
//            }
//            catch (Exception ex)
//            {
//                MessageBox.Show(this, "Too many conections on server.\nTry again later.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Information);
//                throw ex;
//            }
//            finally
//            {
//                Cursor.Current = Cursors.Default;
//            }
			

//            if (CheckValid())
//            {				
//                DateTime storedDate = DateTime.Now;
//                if (null == dbPropertyProfile || null == dbPropertyProfile[0])
//                {
//                    dbPropertyProfile = new DealMaker.PropertyProfile[1];
//                    dbPropertyProfile[0] = new DealMaker.PropertyProfile(new Guid(), null);
//                }
				
//                dbPropertyProfile[0].P_xmlStream = responseStream.ToArray();

//                //property = response.
//                //	RESPONSE.
//                //	RESPONSE_DATA[0].
//                //	PROPERTY_INFORMATION_RESPONSE.
//                //	_PROPERTY_INFORMATION[0].
//                //	PROPERTY;

//                //dtd 4.2.2
//                //BEGIN
//#if !NEW_DTD
//                //OLD
//                property = response.RESPONSE.RESPONSE_DATA[0].PROPERTY_INFORMATION_RESPONSE._PROPERTY_INFORMATION[0].PROPERTY;
//#else
//                //NEW
//                DTD.Response._PROPERTY_INFORMATION propInfo = null;

				
//                try
//                {
//                    //propInfo = response.RESPONSE.RESPONSE_DATA[0].PROPERTY_INFORMATION_RESPONSE.Items[0] as DTD.Response._PROPERTY_INFORMATION;
//                    foreach (object o in response.RESPONSE.RESPONSE_DATA[0].PROPERTY_INFORMATION_RESPONSE.Items)
//                    {
//                        if (o is DTD.Response._PROPERTY_INFORMATION)
//                        {
//                            propInfo = o as DTD.Response._PROPERTY_INFORMATION;
//                            break;
//                        }
//                    }
//                }
//                catch 
//                {
//                    propInfo = null;
//                }
//                if (null != propInfo && null != propInfo.PROPERTY)
//                    property = propInfo.PROPERTY;
//#endif
//                //END

//                fillComps(property,"0","0");

//                //AUTOSAVE
//                if (null != PP_Downloaded_Successfully && _allowAutoSaveTriggerEvents)
//                    PP_Downloaded_Successfully();

//                try
//                {					
//                    string outMsg = string.Empty;
//                    //0 = COMPS
//                    //1 = PP
//                    //2 = Matching Module
//                    string msg;
//                    Guid userId = DealMaker.Globals.UserRegistration.IDUser;

//                    if (Globals.NwVerEnabled && null != Globals.activeModules && Globals.activeModules.MOD_6)
//                        sdc.QueryGuidFromKey(Globals.DataConsumptionReg.KeyCode, out userId, out msg);

//                    bool bRes = sdc.ConfirmDataRequestSuccess(userId/*DealMaker.Globals.UserRegistration.IDUser*/, 1, 1, out outMsg);					

//#if DEBUG 
//                    if (true == bRes)
//                    {
//                        MessageBox.Show("Charged!");
//                    }
//                    else
//                    {
//                        MessageBox.Show("PROBLEM IN CHARGE: " + outMsg);
//                    }
//#endif
//                }
//                catch(Exception 
//#if DEBUG
//                    exc
//#endif
//                    )
//                {
//#if DEBUG
//                    MessageBox.Show(exc.ToString());
//#endif
//                }
//            }

//            sdc = null;
//            return;
//            #region . commented ... useful .
//            /*
//                 *  EXAMPLE HOW TO CONVERT STRING TO OBJECT
//                 *  NOTE - ALWAYS GO DOWN TO MEMORY STREAM!!!
//                 * 
//                string xml = 
//                    "<?xml version='1.0' encoding='UTF-8'?>"
//                    +"<RESPONSE_GROUP>"
//                    +"<RESPONDING_PARTY/>"
//                    +"<RESPOND_TO_PARTY _Name='null'/>"
//                    +"<RESPONSE ResponseDateTime='Wed Feb 23 01:58:59 PST 2005' InternalAccountIdentifier='null' _JobIdentifier='null'>"
//                    +"<RESPONSE_DATA>"
//                    +"<PROPERTY_INFORMATION_RESPONSE>"
//                    +"<STATUS _Condition='FAILURE           ' _Code='0005' _Description='XML IS NOT WELL-FORMED OR IS INVALID. ERROR ON LINE 1: CONTENT IS NOT ALLOWED IN PROLOG.'/>"
//                    +"</PROPERTY_INFORMATION_RESPONSE>"
//                    +"</RESPONSE_DATA>"
//                    +"</RESPONSE>"
//                    +"</RESPONSE_GROUP>";

//                System.IO.MemoryStream stream = new MemoryStream();
//                stream.Write(UTF8Encoding.UTF8.GetBytes(xml),0,UTF8Encoding.UTF8.GetByteCount(xml));
//                stream.Seek(0,SeekOrigin.Begin);
			
//                System.Xml.Serialization.XmlSerializer deser 
//                    = new XmlSerializer(typeof(DTD.Response.RESPONSE_GROUP));

//                DTD.Response.RESPONSE_GROUP x 
//                    = deser.Deserialize(stream)
//                    as DTD.Response.RESPONSE_GROUP;
//                */
		
	
//            //RESPONSE_GROUP y = DeserializeXmlFile(@"c:\xml\dcrr.xml");
			

//            // dump request to see how it looks





//            /*
//            string sNow = DateTime.Now.ToString("yyMMddHHmmss");

//            DTD.Request.REQUEST_GROUP rqg = buildRequest(cs);

//            //			dumpObject(rqg, "request.xml");
//            //return;
//            // create serializers
//            XmlSerializer requestSerializer = 
//                new XmlSerializer(typeof(DTD.Request.REQUEST_GROUP));
//            XmlSerializer responseSerializer = 
//                new XmlSerializer(typeof(DTD.Response.RESPONSE_GROUP));

//            MemoryStream stream = new MemoryStream();
//            UTF8Encoding encoding = new UTF8Encoding();


//            //xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
//            // IMPORTANT  -  we do not need a namespace!
//            System.Xml.Serialization.XmlSerializerNamespaces nmspace = new XmlSerializerNamespaces();
//            nmspace.Add("","");
//            // IMPORTANT  -  we do not need a namespace!
				
//            System.Xml.XmlTextWriter xmlWriter = new XmlTextWriter(stream, encoding);
//            xmlWriter.Formatting = System.Xml.Formatting.Indented;
//            xmlWriter.WriteStartDocument();
//            xmlWriter.WriteDocType("REQUEST_GROUP", null, "C2DRequestv2.0.PropertyProfileComps",null);
//            requestSerializer.Serialize(xmlWriter, rqg, nmspace);
//            xmlWriter.Flush();
//            //xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx



//            //get previusly prepared object
//            //XmlDocument xmldoc = new XmlDocument();
//            //xmldoc.Load(@"request_short_comparable_report.xml");
//            //string xml = xmldoc.InnerXml.ToString();

//            // get serialized object ready for sending
//            //requestSerializer.Serialize(stream,rqg);
//            string xml = encoding.GetString(stream.ToArray());
//            //xml = "xml=" + HttpUtility.UrlEncode(xml);
//            xml = 
//                //"xml=" + 
//                //HttpUtility.UrlEncode(xml);
//                xml;

//            //xxxxxxxxxxxxxxxxxxxxxxxxxxx
//            System.IO.FileStream edc = new FileStream(sNow + "request.xml",System.IO.FileMode.Create);
//            edc.Write(stream.ToArray(), 0, (int)stream.Length);
//            edc.Flush();
//            edc.Close();
//            //xxxxxxxxxxxxxxxxxxxxxxxxxxx

//            // build request
//            HttpWebRequest request = (HttpWebRequest)
//                WebRequest.Create(@"https://staging.connect2data.com");		
//            request.Credentials = new
//                NetworkCredential("PROFGRAB","C2STAGE");
//            request.Method = "POST";
//            request.ContentType="text/xml";
//            request.ContentLength = encoding.GetByteCount(xml);
//            request.UserAgent = "Mozilla/4.0 (compatible; MSIE 6.0;	Windows NT 5.1)";

//            request.Timeout = 300000;

//            Trace.Listeners.Add(new TextWriterTraceListener(@".\trace.log"));
//            Trace.AutoFlush = true;

//            Stream writeStream;
//            Stream responseStream;

//            try
//            {
//                Trace.WriteLine("getting RequestStream");
//                writeStream = request.GetRequestStream();

//                Trace.WriteLine("writing RequestStream");
//                writeStream.Write(encoding.GetBytes(xml), 0, encoding.GetByteCount(xml));
//                writeStream.Flush();
//                writeStream.Close();
				

//                Trace.WriteLine("getting Response");
//                HttpWebResponse response =
//                    (HttpWebResponse)request.GetResponse();
//                responseStream = response.GetResponseStream();

//                Trace.WriteLine("getting ResponseStream");
				
//                / *
//                 * Un needed
					

//                    // Pipe the stream to a higher level stream reader with the required encoding format. 
//                    StreamReader readStream = new StreamReader(responseStream, encoding);
//                    Trace.WriteLine("\nResponse stream received");
//                    Char[] read = new Char[256];

//                    // Read 256 charcters at a time.    
//                    int count = readStream.Read( read, 0, 256 );
//                    Trace.WriteLine("HTML...\r\n");

//                    string stringRresponse = "";

//                    while (count > 0) 
//                    {
//                        // Dump the 256 characters on a string and display the string onto the console.
//                        String str = new String(read, 0, count);
//                        stringRresponse += str;
//                        Trace.Write(str);
//                        count = readStream.Read(read, 0, 256);
//                    }
				
//                * /
//                Trace.WriteLine("deserializing");
//                DTD.Response.RESPONSE_GROUP rsg 
//                    = responseSerializer.Deserialize(responseStream) 
//                    as DTD.Response.RESPONSE_GROUP;

//                response.Close();
//                stream.Close();

				
//                Trace.WriteLine("serializing response");
//                dumpObject(rsg, sNow + "response.xml");


//                //RESPONSE_GROUP rsg = DeserializeXmlFile(@"response.xml");
				
//                Trace.WriteLine("Filling form");
//                if (null == rsg)
//                {
//                    Trace.WriteLine("null == rsg");
//                }

//                property = rsg.
//                    RESPONSE.
//                    RESPONSE_DATA[0].
//                    PROPERTY_INFORMATION_RESPONSE.
//                    _PROPERTY_INFORMATION[0].
//                    PROPERTY;
//                fillComps(property,"0","0");
//                Trace.WriteLine("Filled form");


//                //_PROPERTY_INFORMATION x = 
//                //	y.RESPONSE.RESPONSE_DATA[0].PROPERTY_INFORMATION_RESPONSE.Items[1]
//                //	as _PROPERTY_INFORMATION;
//                //comps =  x._DATA_PROVIDER_COMPARABLE_SALES;
//                //lvFill();
//            }
//            catch (Exception wex)
//            {
//                System.Diagnostics.Debug.WriteLine(wex.Message);
//                MessageBox.Show(wex.Message);
//                Trace.WriteLine("exception " + wex.Message );
//                Trace.WriteLine(wex.StackTrace);
//                Trace.WriteLine(wex.InnerException);
//                Trace.WriteLine(wex.Source);
//                throw wex;
//            }
//            */
//            #endregion . commented ... useful .
//        }
//        #endregion
//        #region . CheckValid .
//        private bool CheckValid()
//        {
//            DTD.Response.STATUS status = null;
//            bool error = false;

//            try 
//            {
//                status = response.RESPONSE.STATUS[0];
//                if (!status._Condition.Trim().StartsWith("SUCCES"))
//                {
//                    MessageBox.Show(status._Description, status._Condition);
//                    error = true;
//                }
//            }
//            catch{};
//            try 
//            {
//                status =response.RESPONSE.RESPONSE_DATA[0].PROPERTY_INFORMATION_RESPONSE.STATUS;
//                if (!status._Condition.Trim().StartsWith("SUCCES"))
//                {
//                    MessageBox.Show(status._Description, status._Condition);
//                    error = true;
//                }
//            }
//            catch{};
//            try 
//            {
				
//                //NEW
//                DTD.Response._PRODUCT product = response.RESPONSE.RESPONSE_DATA[0].PROPERTY_INFORMATION_RESPONSE.Items[0] as DTD.Response._PRODUCT;													
//                if (null != product && null != product.STATUS && product.STATUS.Length > 0)
//                    status = product.STATUS[0];
//                //END
//                if (!status._Condition.Trim().StartsWith("SUCCES"))
//                {
//                    MessageBox.Show(status._Description, status._Condition);
//                    error = true;
//                }
//            }
//            catch{};

//            try 
//            {
//                //DTD.Response._MULTIPLE_RECORDS[] multipla 
//                //	= response.RESPONSE.RESPONSE_DATA[0].PROPERTY_INFORMATION_RESPONSE._PROPERTY_INFORMATION[0]._MULTIPLE_RECORDS;

//                //dtd 4.2.2
//                //BEGIN
//#if !NEW_DTD
//                //OLD
//                DTD.Response._MULTIPLE_RECORDS[] multipla = response.RESPONSE.RESPONSE_DATA[0].PROPERTY_INFORMATION_RESPONSE._PROPERTY_INFORMATION[0]._MULTIPLE_RECORDS;
//#else
//                //NEW
//                DTD.Response._MULTIPLE_RECORDS[] multipla = null;
//                DTD.Response._PROPERTY_INFORMATION propInfo = null;
									
//                //propInfo = response.RESPONSE.RESPONSE_DATA[0].PROPERTY_INFORMATION_RESPONSE.Items[0] as DTD.Response._PROPERTY_INFORMATION;
//                foreach (object o in response.RESPONSE.RESPONSE_DATA[0].PROPERTY_INFORMATION_RESPONSE.Items)
//                {
//                    if (o is DTD.Response._PROPERTY_INFORMATION)
//                    {
//                        propInfo = o as DTD.Response._PROPERTY_INFORMATION;
//                        break;
//                    }
//                }

//                if (null != propInfo)
//                    multipla = propInfo._MULTIPLE_RECORDS;
//#endif
//                //END
				
//                DealMaker.FormMultipleProperiesFound form = new DealMaker.FormMultipleProperiesFound();

//                form.lvFill(multipla);
//                form.ShowDialog();

//                int iterator = form.selected;

//                if (iterator >= 0)
//                {
				
//                    try
//                    {
//                        textBoxStreetNoA.Text = getSecureString(multipla[iterator].PROPERTY._PARSED_STREET_ADDRESS._HouseNumber);
//                    }
//                    catch{}
//                    try
//                    {
//                        textBoxPreDirA.Text = getSecureString(multipla[iterator].PROPERTY._PARSED_STREET_ADDRESS._DirectionPrefix);
//                    }
//                    catch{}
//                    try
//                    {
//                        textBoxStreetA.Text = getSecureString(multipla[iterator].PROPERTY._PARSED_STREET_ADDRESS._StreetName);
//                    }
//                    catch{}
//                    try
//                    {
//                        textBoxPostDirA.Text = getSecureString(multipla[iterator].PROPERTY._PARSED_STREET_ADDRESS._DirectionSuffix);
//                    }
//                    catch{}
//                    try
//                    {
//                        textBoxSuffixA.Text = getSecureString(multipla[iterator].PROPERTY._PARSED_STREET_ADDRESS._StreetSuffix);
//                    }
//                    catch{}
//                    try
//                    {
//                        textBoxUnitNoA.Text = getSecureString(multipla[iterator].PROPERTY._PARSED_STREET_ADDRESS._ApartmentOrUnit);
//                    }
//                    catch{}
//                    try
//                    {
//                        textBoxZipC.Text = textBoxZipB.Text = textBoxZipA.Text = getSecureString(multipla[iterator].PROPERTY._PostalCode);
//                    }
//                    catch{}
//                    try
//                    {
//                        textBoxCountyC.Text =textBoxCountyB.Text = textBoxCountyA.Text = getSecureString(multipla[iterator].PROPERTY._County);
//                    }
//                    catch{}
//                    try
//                    {
//                        textBoxStateC.Text =textBoxStateB.Text = textBoxStateA.Text = getSecureString(multipla[iterator].PROPERTY._State);
//                    }
//                    catch{}
//                    try
//                    {
//                        textBoxApnB.Text = getSecureString(multipla[iterator].PROPERTY._AssessorsParcelIdentifier);
//                    }
//                    catch{}
//                    try
//                    {
//                        string ownerName = getSecureString(multipla[iterator].PROPERTY.PROPERTY_OWNER[0]._OwnerName);
//                        ownerName = ownerName.TrimStart();
//                        ownerName = ownerName.TrimEnd();

//                        int pos = -1;
//                        pos = ownerName.IndexOf(" ");
//                        textBoxLatNameC.Text = ownerName.Substring(0, pos);
//                        textBoxFirstNameC.Text = ownerName.Substring(pos + 1, ownerName.Length - (pos + 1));						
//                    }
//                    catch{}
//                }
//                error = true;
			
//            }
//            catch{};
//            return !error;
//        }
//        #endregion
//        #region . save data to db .
//        public bool Save(Guid PropertyGuid)
//        {
//            bool success = false;		
//            try
//            {
//                if (null != dbPropertyProfile 
//                    && null != dbPropertyProfile[0] 
//                    && null != dbPropertyProfile[0].P_xmlStream)//TA++: 02.04.2004. - To Prevent Runtime Crashes
//                {
//                    dbPropertyProfile[0].P_propertyItemId = PropertyGuid;
					
//                    if (null != this._dbConn)
//                    {
//                        if (_PPInDb) 
//                            dbPropertyProfile[0].Update(_dbConn);
//                        else
//                        {
//                            dbPropertyProfile[0].Insert(_dbConn);		
//                            _PPInDb=true;
//                        }
//                    }
//                    else
//                    {
//                        success = false;
//                        return success;
//                    }
//                }
//                success = true;
//            }
//            catch (Exception ex)
//            {
//                MessageBox.Show(ex.StackTrace,"Problem: Data not saved.");
//            }
//            return success;
//        }
//        #endregion
//        #region . deserialize .
//        private DTD.Response.RESPONSE_GROUP DeserializeXmlFile(string path)
//        {
//            DTD.Response.RESPONSE_GROUP x ;
//            FileStream fs = new FileStream(path, FileMode.Open);
			
//            XmlSerializer serializer = new XmlSerializer(typeof(DTD.Response.RESPONSE_GROUP));
//            x = (DTD.Response.RESPONSE_GROUP)serializer.Deserialize(fs);;
//            fs.Close();
//            return x;
//        }
//        #endregion
//        #region . dumpObject to file .
//        private void dumpObject(object theObject, string filename)
//        {
//#if DUMP_XML_OBJECT
//#if DEBUG
//            System.Xml.Serialization.XmlSerializer serializer
//                = new System.Xml.Serialization.XmlSerializer(theObject.GetType());

//            // IMPORTANT  -  we do not need a namespace!
//            System.Xml.Serialization.XmlSerializerNamespaces nmspace = new XmlSerializerNamespaces();
//            nmspace.Add("","");
//            // IMPORTANT  -  we do not need a namespace!
				
//            System.Xml.XmlTextWriter writer = new XmlTextWriter(filename, System.Text.UTF8Encoding.UTF8);
//            writer.Formatting = System.Xml.Formatting.Indented;
//            writer.WriteStartDocument();
//            writer.WriteDocType("REQUEST_GROUP", null, "C2DRequestv2.0.PropertyProfileComps",null);
//            serializer.Serialize(writer, theObject,nmspace);
//            writer.Flush();
//            writer.Close();
//#endif
//#endif
//        }
//        #endregion
//        #region . num dat parsing .
//        private string getMoney(string sMoney)
//        {
//            try
//            {
//                sMoney = Convert.ToDouble(sMoney).ToString("C");
//            }
//            catch
//            {
//            }

//            return sMoney;
//        }

//        private string getDecimal(string sDecimal)
//        {
//            try
//            {
//                sDecimal = Convert.ToDouble(sDecimal).ToString("N");
//            }
//            catch
//            {
//            }

//            return sDecimal;
//        }

//        private string getDate(string sDate)
//        {
//            if (sDate.Length==8)
//            {
//                try
//                {
//                    DateTime d = new DateTime
//                        (
//                        (int.Parse(sDate.Substring(0,4))),
//                        (int.Parse(sDate.Substring(4,2))),
//                        (int.Parse(sDate.Substring(6,2)))
//                        );

//                    sDate = d.ToShortDateString();
//                }
//                catch
//                {
//                }
//            }

//            return sDate;
//        }
//        private string getSecureString(object str)
//        {
//            string SecureString = "";
//            try
//            {
//                SecureString = str.ToString();
//            }
//            catch
//            {}
//            return SecureString;
//        }

//        #endregion
//        #region Dipose
//        /// <summary> 
//        /// Clean up any resources being used.
//        /// </summary>
//        protected override void Dispose( bool disposing )
//        {
//            if( disposing )
//            {
//                if(components != null)
//                {
//                    components.Dispose();
//                }
//            }
//            base.Dispose( disposing );
//        }
//        #endregion
//        #region Component Designer generated code
//        /// <summary> 
//        /// Required method for Designer support - do not modify 
//        /// the contents of this method with the code editor.
//        /// </summary>
//        private void InitializeComponent()
//        {
//            System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(UserControlPropertyProfileComps));
//            this.textBoxMailingZipCode = new System.Windows.Forms.TextBox();
//            this.labelMailingZipCode = new System.Windows.Forms.Label();
//            this.textBoxMailingCityAndState = new System.Windows.Forms.TextBox();
//            this.labelMailingCityAndState = new System.Windows.Forms.Label();
//            this.textBoxMailingAddress = new System.Windows.Forms.TextBox();
//            this.labelMailingAddress = new System.Windows.Forms.Label();
//            this.textBoxMapReferenceTwo = new System.Windows.Forms.TextBox();
//            this.labelMapReferenceTwo = new System.Windows.Forms.Label();
//            this.textBoxMapReferenceOne = new System.Windows.Forms.TextBox();
//            this.labelMapReferenceOne = new System.Windows.Forms.Label();
//            this.textBoxZipCode = new System.Windows.Forms.TextBox();
//            this.labelZipCode = new System.Windows.Forms.Label();
//            this.textBoxState = new System.Windows.Forms.TextBox();
//            this.labelState = new System.Windows.Forms.Label();
//            this.textBoxCity = new System.Windows.Forms.TextBox();
//            this.labelCity = new System.Windows.Forms.Label();
//            this.textBoxUnitNumber = new System.Windows.Forms.TextBox();
//            this.labelUnitNumber = new System.Windows.Forms.Label();
//            this.textBoxSuffix = new System.Windows.Forms.TextBox();
//            this.labelSuffix = new System.Windows.Forms.Label();
//            this.textBoxPostDirection = new System.Windows.Forms.TextBox();
//            this.labelPostDirection = new System.Windows.Forms.Label();
//            this.textBoxStreetnName = new System.Windows.Forms.TextBox();
//            this.labelStreetnName = new System.Windows.Forms.Label();
//            this.textBoxPreDirection = new System.Windows.Forms.TextBox();
//            this.labelPreDirection = new System.Windows.Forms.Label();
//            this.textBoxHouseNumber = new System.Windows.Forms.TextBox();
//            this.labelHouseNumber = new System.Windows.Forms.Label();
//            this.textBoxPhoneNumber = new System.Windows.Forms.TextBox();
//            this.labelPhoneNumber = new System.Windows.Forms.Label();
//            this.textBoxOwnerName = new System.Windows.Forms.TextBox();
//            this.labelOwnerName = new System.Windows.Forms.Label();
//            this.labelOwnerNameGroup = new System.Windows.Forms.Label();
//            this.textBoxSchoolDistrict = new System.Windows.Forms.TextBox();
//            this.textBoxLotNumber = new System.Windows.Forms.TextBox();
//            this.textBoxSubdivision = new System.Windows.Forms.TextBox();
//            this.textBoxComparableNumber = new System.Windows.Forms.TextBox();
//            this.textBoxDistanceFromSubject = new System.Windows.Forms.TextBox();
//            this.tabPagePropertyCharacteristics = new System.Windows.Forms.TabPage();
//            this.textBoxOtherImprovements = new System.Windows.Forms.TextBox();
//            this.label57 = new System.Windows.Forms.Label();
//            this.textBoxAirConditioning = new System.Windows.Forms.TextBox();
//            this.labelAirConditioning = new System.Windows.Forms.Label();
//            this.pictureBoxPropCharact = new System.Windows.Forms.PictureBox();
//            this.textBoxBasementArea = new System.Windows.Forms.TextBox();
//            this.label9 = new System.Windows.Forms.Label();
//            this.textBoxBasement = new System.Windows.Forms.TextBox();
//            this.label10 = new System.Windows.Forms.Label();
//            this.textBoxCondition = new System.Windows.Forms.TextBox();
//            this.label8 = new System.Windows.Forms.Label();
//            this.textBoxQuality = new System.Windows.Forms.TextBox();
//            this.label7 = new System.Windows.Forms.Label();
//            this.textBoxFoundation = new System.Windows.Forms.TextBox();
//            this.label6 = new System.Windows.Forms.Label();
//            this.textBoxRoofMaterials = new System.Windows.Forms.TextBox();
//            this.label5 = new System.Windows.Forms.Label();
//            this.textBoxFireplaces = new System.Windows.Forms.TextBox();
//            this.label4 = new System.Windows.Forms.Label();
//            this.textBoxGarageArea = new System.Windows.Forms.TextBox();
//            this.label3 = new System.Windows.Forms.Label();
//            this.textBoxLotWidth = new System.Windows.Forms.TextBox();
//            this.label1 = new System.Windows.Forms.Label();
//            this.textBoxLotDepth = new System.Windows.Forms.TextBox();
//            this.label2 = new System.Windows.Forms.Label();
//            this.textBoxLandUse = new System.Windows.Forms.TextBox();
//            this.labelLandUse = new System.Windows.Forms.Label();
//            this.textBoxTotalBaths = new System.Windows.Forms.TextBox();
//            this.labelTotalBaths = new System.Windows.Forms.Label();
//            this.textBoxTotalBedrooms = new System.Windows.Forms.TextBox();
//            this.labelTotalBedrooms = new System.Windows.Forms.Label();
//            this.textBoxTotalRooms = new System.Windows.Forms.TextBox();
//            this.labelTotalRooms = new System.Windows.Forms.Label();
//            this.textBoxZoning = new System.Windows.Forms.TextBox();
//            this.labelZoning = new System.Windows.Forms.Label();
//            this.textBoxNumberOfStories = new System.Windows.Forms.TextBox();
//            this.labelNumberOfStories = new System.Windows.Forms.Label();
//            this.textBoxPool = new System.Windows.Forms.TextBox();
//            this.labelPool = new System.Windows.Forms.Label();
//            this.textBoxRoofType = new System.Windows.Forms.TextBox();
//            this.labelRoofType = new System.Windows.Forms.Label();
//            this.textBoxParkingCapacity = new System.Windows.Forms.TextBox();
//            this.labelParkingCapacity = new System.Windows.Forms.Label();
//            this.textBoxParkingType = new System.Windows.Forms.TextBox();
//            this.labelParkingType = new System.Windows.Forms.Label();
//            this.textBoxLotSize = new System.Windows.Forms.TextBox();
//            this.labelLotSize = new System.Windows.Forms.Label();
//            this.textBoxLotArea = new System.Windows.Forms.TextBox();
//            this.labelLotArea = new System.Windows.Forms.Label();
//            this.textBoxTotalLivingArea = new System.Windows.Forms.TextBox();
//            this.labelTotalLivingArea = new System.Windows.Forms.Label();
//            this.textBoxYearBuilt = new System.Windows.Forms.TextBox();
//            this.labelYearBuilt = new System.Windows.Forms.Label();
//            this.label11 = new System.Windows.Forms.Label();
//            this.label13 = new System.Windows.Forms.Label();
//            this.label14 = new System.Windows.Forms.Label();
//            this.label12 = new System.Windows.Forms.Label();
//            this.textBoxCounty = new System.Windows.Forms.TextBox();
//            this.labelSchoolDistrict = new System.Windows.Forms.Label();
//            this.labelLotNumber = new System.Windows.Forms.Label();
//            this.labelSubdivision = new System.Windows.Forms.Label();
//            this.labelComparableNumber = new System.Windows.Forms.Label();
//            this.labelDistanceFromSubject = new System.Windows.Forms.Label();
//            this.tabPageOwnershipNLocations = new System.Windows.Forms.TabPage();
//            this.pictureBoxOwnershipLocations = new System.Windows.Forms.PictureBox();
//            this.panel1 = new System.Windows.Forms.Panel();
//            this.textBoxStateA = new System.Windows.Forms.TextBox();
//            this.textBoxCountyA = new System.Windows.Forms.TextBox();
//            this.textBoxZipA = new System.Windows.Forms.TextBox();
//            this.label74 = new System.Windows.Forms.Label();
//            this.label75 = new System.Windows.Forms.Label();
//            this.label76 = new System.Windows.Forms.Label();
//            this.bPrintReport = new System.Windows.Forms.Button();
//            this.label90 = new System.Windows.Forms.Label();
//            this.label85 = new System.Windows.Forms.Label();
//            this.label89 = new System.Windows.Forms.Label();
//            this.label88 = new System.Windows.Forms.Label();
//            this.textBoxStateC = new System.Windows.Forms.TextBox();
//            this.textBoxCountyC = new System.Windows.Forms.TextBox();
//            this.textBoxZipC = new System.Windows.Forms.TextBox();
//            this.textBoxLatNameC = new System.Windows.Forms.TextBox();
//            this.textBoxFirstNameC = new System.Windows.Forms.TextBox();
//            this.label64 = new System.Windows.Forms.Label();
//            this.label66 = new System.Windows.Forms.Label();
//            this.label67 = new System.Windows.Forms.Label();
//            this.label65 = new System.Windows.Forms.Label();
//            this.textBoxStateB = new System.Windows.Forms.TextBox();
//            this.textBoxCountyB = new System.Windows.Forms.TextBox();
//            this.textBoxZipB = new System.Windows.Forms.TextBox();
//            this.textBoxApnB = new System.Windows.Forms.TextBox();
//            this.label68 = new System.Windows.Forms.Label();
//            this.label69 = new System.Windows.Forms.Label();
//            this.label70 = new System.Windows.Forms.Label();
//            this.label71 = new System.Windows.Forms.Label();
//            this.buttonSearchC = new System.Windows.Forms.Button();
//            this.label72 = new System.Windows.Forms.Label();
//            this.buttonSearchB = new System.Windows.Forms.Button();
//            this.label73 = new System.Windows.Forms.Label();
//            this.textBoxUnitNoA = new System.Windows.Forms.TextBox();
//            this.textBoxSuffixA = new System.Windows.Forms.TextBox();
//            this.textBoxPostDirA = new System.Windows.Forms.TextBox();
//            this.textBoxStreetA = new System.Windows.Forms.TextBox();
//            this.textBoxPreDirA = new System.Windows.Forms.TextBox();
//            this.textBoxStreetNoA = new System.Windows.Forms.TextBox();
//            this.label77 = new System.Windows.Forms.Label();
//            this.label78 = new System.Windows.Forms.Label();
//            this.label79 = new System.Windows.Forms.Label();
//            this.label80 = new System.Windows.Forms.Label();
//            this.label81 = new System.Windows.Forms.Label();
//            this.label82 = new System.Windows.Forms.Label();
//            this.buttonSearchA = new System.Windows.Forms.Button();
//            this.label83 = new System.Windows.Forms.Label();
//            this.label84 = new System.Windows.Forms.Label();
//            this.label63 = new System.Windows.Forms.Label();
//            this.labelLine = new System.Windows.Forms.Label();
//            this.panel2 = new System.Windows.Forms.Panel();
//            this.labelCounty = new System.Windows.Forms.Label();
//            this.labelSiteAddress = new System.Windows.Forms.Label();
//            this.panel3 = new System.Windows.Forms.Panel();
//            this.tabControlOnlinePropertyProfileNComps = new System.Windows.Forms.TabControl();
//            this.tabPageFinancials = new System.Windows.Forms.TabPage();
//            this.textBoxPSSaleDate = new System.Windows.Forms.TextBox();
//            this.label37 = new System.Windows.Forms.Label();
//            this.pictureBoxFinancials = new System.Windows.Forms.PictureBox();
//            this.textBoxSellerName = new System.Windows.Forms.TextBox();
//            this.labelSellerName = new System.Windows.Forms.Label();
//            this.textBoxSaleType2 = new System.Windows.Forms.TextBox();
//            this.label15 = new System.Windows.Forms.Label();
//            this.textBoxSaleType1 = new System.Windows.Forms.TextBox();
//            this.labelSaleType1 = new System.Windows.Forms.Label();
//            this.textBoxSaleDate = new System.Windows.Forms.TextBox();
//            this.labelSaleDate = new System.Windows.Forms.Label();
//            this.label50 = new System.Windows.Forms.Label();
//            this.textBoxPSMInterestRateType = new System.Windows.Forms.TextBox();
//            this.label51 = new System.Windows.Forms.Label();
//            this.textBoxPSMInterestRateTerm = new System.Windows.Forms.TextBox();
//            this.label52 = new System.Windows.Forms.Label();
//            this.textBoxPSMInterestRate = new System.Windows.Forms.TextBox();
//            this.label53 = new System.Windows.Forms.Label();
//            this.textBoxPSMMortgageAmount = new System.Windows.Forms.TextBox();
//            this.label54 = new System.Windows.Forms.Label();
//            this.label44 = new System.Windows.Forms.Label();
//            this.textBoxPFMLender = new System.Windows.Forms.TextBox();
//            this.label45 = new System.Windows.Forms.Label();
//            this.textBoxPFMInterestRateType = new System.Windows.Forms.TextBox();
//            this.label46 = new System.Windows.Forms.Label();
//            this.textBoxPFMInterestRateTerm = new System.Windows.Forms.TextBox();
//            this.label47 = new System.Windows.Forms.Label();
//            this.textBoxPFMInterestRate = new System.Windows.Forms.TextBox();
//            this.label48 = new System.Windows.Forms.Label();
//            this.textBoxPFMMortgageAmount = new System.Windows.Forms.TextBox();
//            this.label49 = new System.Windows.Forms.Label();
//            this.label42 = new System.Windows.Forms.Label();
//            this.textBoxPSStampAmt = new System.Windows.Forms.TextBox();
//            this.label43 = new System.Windows.Forms.Label();
//            this.textBoxPSDocumentNumber = new System.Windows.Forms.TextBox();
//            this.label41 = new System.Windows.Forms.Label();
//            this.textBoxPSRecordingDate = new System.Windows.Forms.TextBox();
//            this.label40 = new System.Windows.Forms.Label();
//            this.textBoxPSDeedType = new System.Windows.Forms.TextBox();
//            this.label39 = new System.Windows.Forms.Label();
//            this.textBoxPSSaleType1 = new System.Windows.Forms.TextBox();
//            this.label38 = new System.Windows.Forms.Label();
//            this.label35 = new System.Windows.Forms.Label();
//            this.textBoxPSSalePrice = new System.Windows.Forms.TextBox();
//            this.label36 = new System.Windows.Forms.Label();
//            this.label19 = new System.Windows.Forms.Label();
//            this.textBoxSecInterestRateType = new System.Windows.Forms.TextBox();
//            this.label30 = new System.Windows.Forms.Label();
//            this.textBoxSecInterestRateTerm = new System.Windows.Forms.TextBox();
//            this.label31 = new System.Windows.Forms.Label();
//            this.textBoxSecInterestRate = new System.Windows.Forms.TextBox();
//            this.label32 = new System.Windows.Forms.Label();
//            this.textBoxSecMortgageAmount = new System.Windows.Forms.TextBox();
//            this.label33 = new System.Windows.Forms.Label();
//            this.label29 = new System.Windows.Forms.Label();
//            this.label24 = new System.Windows.Forms.Label();
//            this.textBoxStampAmt = new System.Windows.Forms.TextBox();
//            this.textBoxTitleCompany = new System.Windows.Forms.TextBox();
//            this.label23 = new System.Windows.Forms.Label();
//            this.label22 = new System.Windows.Forms.Label();
//            this.label21 = new System.Windows.Forms.Label();
//            this.label18 = new System.Windows.Forms.Label();
//            this.label17 = new System.Windows.Forms.Label();
//            this.label16 = new System.Windows.Forms.Label();
//            this.textBox4PricePerSqFt = new System.Windows.Forms.TextBox();
//            this.labelPricePerSqFt = new System.Windows.Forms.Label();
//            this.textBoxCashDown = new System.Windows.Forms.TextBox();
//            this.labelCashDown = new System.Windows.Forms.Label();
//            this.textBoxFirstMortgageLender = new System.Windows.Forms.TextBox();
//            this.labelFirstMortgageLender = new System.Windows.Forms.Label();
//            this.textBoxType = new System.Windows.Forms.TextBox();
//            this.labelFirstMortgageInterestRateType = new System.Windows.Forms.Label();
//            this.textBoxFirstMortgageInterestRateTerm = new System.Windows.Forms.TextBox();
//            this.labelFirstMortgageInterestRateTerm = new System.Windows.Forms.Label();
//            this.textBoxFirstMortgageInterestRate = new System.Windows.Forms.TextBox();
//            this.labelFirstMortgageInterestRate = new System.Windows.Forms.Label();
//            this.textBoxFirstMortgageAmount = new System.Windows.Forms.TextBox();
//            this.labelFirstMortgageAmount = new System.Windows.Forms.Label();
//            this.textBoxRecordingDate = new System.Windows.Forms.TextBox();
//            this.labelRecordingDate = new System.Windows.Forms.Label();
//            this.textBoxDocumentNumber = new System.Windows.Forms.TextBox();
//            this.labelDocumentNumber = new System.Windows.Forms.Label();
//            this.textBoxDeedType = new System.Windows.Forms.TextBox();
//            this.labelDeedType = new System.Windows.Forms.Label();
//            this.textBoxSalePrice = new System.Windows.Forms.TextBox();
//            this.labelSalePrice = new System.Windows.Forms.Label();
//            this.label20 = new System.Windows.Forms.Label();
//            this.label28 = new System.Windows.Forms.Label();
//            this.label27 = new System.Windows.Forms.Label();
//            this.label26 = new System.Windows.Forms.Label();
//            this.label25 = new System.Windows.Forms.Label();
//            this.label34 = new System.Windows.Forms.Label();
//            this.tabPageTaxesNLegal = new System.Windows.Forms.TabPage();
//            this.pictureBoxTaxesLegal = new System.Windows.Forms.PictureBox();
//            this.label87 = new System.Windows.Forms.Label();
//            this.label86 = new System.Windows.Forms.Label();
//            this.textBoxCensusTract = new System.Windows.Forms.TextBox();
//            this.label62 = new System.Windows.Forms.Label();
//            this.textBoxLegalBlockBldg = new System.Windows.Forms.TextBox();
//            this.label61 = new System.Windows.Forms.Label();
//            this.textBoxLegalBookPage = new System.Windows.Forms.TextBox();
//            this.label60 = new System.Windows.Forms.Label();
//            this.label59 = new System.Windows.Forms.Label();
//            this.label58 = new System.Windows.Forms.Label();
//            this.textBoxTownshipName = new System.Windows.Forms.TextBox();
//            this.labelTownshipName = new System.Windows.Forms.Label();
//            this.textBoxLegalDescription = new System.Windows.Forms.TextBox();
//            this.labelLegalDescription = new System.Windows.Forms.Label();
//            this.textBoxDelinquentYear = new System.Windows.Forms.TextBox();
//            this.labelDelinquentYear = new System.Windows.Forms.Label();
//            this.textBoxImprovementValue = new System.Windows.Forms.TextBox();
//            this.labelImprovementValue = new System.Windows.Forms.Label();
//            this.textBoxLandValue = new System.Windows.Forms.TextBox();
//            this.labelLandValue = new System.Windows.Forms.Label();
//            this.textBoxLandUseType = new System.Windows.Forms.TextBox();
//            this.labelLandUseType = new System.Windows.Forms.Label();
//            this.textBoxExemptionType = new System.Windows.Forms.TextBox();
//            this.labelExemptionType = new System.Windows.Forms.Label();
//            this.textBoxTaxYear = new System.Windows.Forms.TextBox();
//            this.labelTaxYear = new System.Windows.Forms.Label();
//            this.textBoxTaxArea = new System.Windows.Forms.TextBox();
//            this.labelTaxArea = new System.Windows.Forms.Label();
//            this.textBoxPropertyTax = new System.Windows.Forms.TextBox();
//            this.labelPropertyTax = new System.Windows.Forms.Label();
//            this.textBoxAssesmentYear = new System.Windows.Forms.TextBox();
//            this.labelAssesmentYear = new System.Windows.Forms.Label();
//            this.textBoxTotalAssesedValue = new System.Windows.Forms.TextBox();
//            this.labelTotalAssesedValue = new System.Windows.Forms.Label();
//            this.textBoxAlternateAPN = new System.Windows.Forms.TextBox();
//            this.labelAlternateAPN = new System.Windows.Forms.Label();
//            this.textBoxAPN = new System.Windows.Forms.TextBox();
//            this.labelAPN = new System.Windows.Forms.Label();
//            this.label56 = new System.Windows.Forms.Label();
//            this.label55 = new System.Windows.Forms.Label();
//            this.tabPagePropertyCharacteristics.SuspendLayout();
//            this.tabPageOwnershipNLocations.SuspendLayout();
//            this.panel1.SuspendLayout();
//            this.panel2.SuspendLayout();
//            this.panel3.SuspendLayout();
//            this.tabControlOnlinePropertyProfileNComps.SuspendLayout();
//            this.tabPageFinancials.SuspendLayout();
//            this.tabPageTaxesNLegal.SuspendLayout();
//            this.SuspendLayout();
//            // 
//            // textBoxMailingZipCode
//            // 
//            this.textBoxMailingZipCode.Location = new System.Drawing.Point(656, 40);
//            this.textBoxMailingZipCode.Name = "textBoxMailingZipCode";
//            this.textBoxMailingZipCode.Size = new System.Drawing.Size(64, 22);
//            this.textBoxMailingZipCode.TabIndex = 95;
//            this.textBoxMailingZipCode.Text = "";
//            // 
//            // labelMailingZipCode
//            // 
//            this.labelMailingZipCode.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.labelMailingZipCode.Location = new System.Drawing.Point(656, 24);
//            this.labelMailingZipCode.Name = "labelMailingZipCode";
//            this.labelMailingZipCode.Size = new System.Drawing.Size(32, 24);
//            this.labelMailingZipCode.TabIndex = 43;
//            this.labelMailingZipCode.Text = "ZIP";
//            // 
//            // textBoxMailingCityAndState
//            // 
//            this.textBoxMailingCityAndState.Location = new System.Drawing.Point(456, 40);
//            this.textBoxMailingCityAndState.Name = "textBoxMailingCityAndState";
//            this.textBoxMailingCityAndState.Size = new System.Drawing.Size(192, 22);
//            this.textBoxMailingCityAndState.TabIndex = 94;
//            this.textBoxMailingCityAndState.Text = "";
//            // 
//            // labelMailingCityAndState
//            // 
//            this.labelMailingCityAndState.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.labelMailingCityAndState.Location = new System.Drawing.Point(456, 24);
//            this.labelMailingCityAndState.Name = "labelMailingCityAndState";
//            this.labelMailingCityAndState.Size = new System.Drawing.Size(144, 24);
//            this.labelMailingCityAndState.TabIndex = 41;
//            this.labelMailingCityAndState.Text = "Mailing City and State";
//            // 
//            // textBoxMailingAddress
//            // 
//            this.textBoxMailingAddress.Location = new System.Drawing.Point(288, 40);
//            this.textBoxMailingAddress.Name = "textBoxMailingAddress";
//            this.textBoxMailingAddress.Size = new System.Drawing.Size(160, 22);
//            this.textBoxMailingAddress.TabIndex = 93;
//            this.textBoxMailingAddress.Text = "";
//            // 
//            // labelMailingAddress
//            // 
//            this.labelMailingAddress.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.labelMailingAddress.Location = new System.Drawing.Point(288, 24);
//            this.labelMailingAddress.Name = "labelMailingAddress";
//            this.labelMailingAddress.Size = new System.Drawing.Size(112, 24);
//            this.labelMailingAddress.TabIndex = 39;
//            this.labelMailingAddress.Text = "Mailing Address";
//            // 
//            // textBoxMapReferenceTwo
//            // 
//            this.textBoxMapReferenceTwo.Location = new System.Drawing.Point(272, 152);
//            this.textBoxMapReferenceTwo.Name = "textBoxMapReferenceTwo";
//            this.textBoxMapReferenceTwo.Size = new System.Drawing.Size(136, 22);
//            this.textBoxMapReferenceTwo.TabIndex = 140;
//            this.textBoxMapReferenceTwo.Text = "";
//            // 
//            // labelMapReferenceTwo
//            // 
//            this.labelMapReferenceTwo.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.labelMapReferenceTwo.Location = new System.Drawing.Point(272, 136);
//            this.labelMapReferenceTwo.Name = "labelMapReferenceTwo";
//            this.labelMapReferenceTwo.Size = new System.Drawing.Size(128, 24);
//            this.labelMapReferenceTwo.TabIndex = 37;
//            this.labelMapReferenceTwo.Text = "Map Reference Two";
//            // 
//            // textBoxMapReferenceOne
//            // 
//            this.textBoxMapReferenceOne.Location = new System.Drawing.Point(128, 152);
//            this.textBoxMapReferenceOne.Name = "textBoxMapReferenceOne";
//            this.textBoxMapReferenceOne.Size = new System.Drawing.Size(136, 22);
//            this.textBoxMapReferenceOne.TabIndex = 130;
//            this.textBoxMapReferenceOne.Text = "";
//            // 
//            // labelMapReferenceOne
//            // 
//            this.labelMapReferenceOne.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.labelMapReferenceOne.Location = new System.Drawing.Point(128, 136);
//            this.labelMapReferenceOne.Name = "labelMapReferenceOne";
//            this.labelMapReferenceOne.Size = new System.Drawing.Size(128, 24);
//            this.labelMapReferenceOne.TabIndex = 35;
//            this.labelMapReferenceOne.Text = "Map Reference One";
//            // 
//            // textBoxZipCode
//            // 
//            this.textBoxZipCode.Location = new System.Drawing.Point(448, 104);
//            this.textBoxZipCode.Name = "textBoxZipCode";
//            this.textBoxZipCode.Size = new System.Drawing.Size(64, 22);
//            this.textBoxZipCode.TabIndex = 99;
//            this.textBoxZipCode.Text = "";
//            // 
//            // labelZipCode
//            // 
//            this.labelZipCode.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.labelZipCode.Location = new System.Drawing.Point(448, 88);
//            this.labelZipCode.Name = "labelZipCode";
//            this.labelZipCode.Size = new System.Drawing.Size(32, 24);
//            this.labelZipCode.TabIndex = 33;
//            this.labelZipCode.Text = "ZIP";
//            // 
//            // textBoxState
//            // 
//            this.textBoxState.Location = new System.Drawing.Point(344, 104);
//            this.textBoxState.MaxLength = 2;
//            this.textBoxState.Name = "textBoxState";
//            this.textBoxState.Size = new System.Drawing.Size(24, 22);
//            this.textBoxState.TabIndex = 98;
//            this.textBoxState.Text = "";
//            // 
//            // labelState
//            // 
//            this.labelState.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.labelState.Location = new System.Drawing.Point(344, 88);
//            this.labelState.Name = "labelState";
//            this.labelState.Size = new System.Drawing.Size(96, 24);
//            this.labelState.TabIndex = 31;
//            this.labelState.Text = "State (i.e. CA)";
//            // 
//            // textBoxCity
//            // 
//            this.textBoxCity.Location = new System.Drawing.Point(256, 104);
//            this.textBoxCity.Name = "textBoxCity";
//            this.textBoxCity.Size = new System.Drawing.Size(72, 22);
//            this.textBoxCity.TabIndex = 97;
//            this.textBoxCity.Text = "";
//            // 
//            // labelCity
//            // 
//            this.labelCity.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.labelCity.Location = new System.Drawing.Point(256, 88);
//            this.labelCity.Name = "labelCity";
//            this.labelCity.Size = new System.Drawing.Size(72, 24);
//            this.labelCity.TabIndex = 29;
//            this.labelCity.Text = "City";
//            // 
//            // textBoxUnitNumber
//            // 
//            this.textBoxUnitNumber.Location = new System.Drawing.Point(1224, 480);
//            this.textBoxUnitNumber.Name = "textBoxUnitNumber";
//            this.textBoxUnitNumber.TabIndex = 28;
//            this.textBoxUnitNumber.Text = "Unit-Number";
//            this.textBoxUnitNumber.Visible = false;
//            // 
//            // labelUnitNumber
//            // 
//            this.labelUnitNumber.Location = new System.Drawing.Point(1216, 464);
//            this.labelUnitNumber.Name = "labelUnitNumber";
//            this.labelUnitNumber.Size = new System.Drawing.Size(72, 24);
//            this.labelUnitNumber.TabIndex = 27;
//            this.labelUnitNumber.Text = "Unit-Number";
//            this.labelUnitNumber.Visible = false;
//            // 
//            // textBoxSuffix
//            // 
//            this.textBoxSuffix.Location = new System.Drawing.Point(1104, 480);
//            this.textBoxSuffix.Name = "textBoxSuffix";
//            this.textBoxSuffix.TabIndex = 26;
//            this.textBoxSuffix.Text = "Suffix";
//            this.textBoxSuffix.Visible = false;
//            // 
//            // labelSuffix
//            // 
//            this.labelSuffix.Location = new System.Drawing.Point(1096, 464);
//            this.labelSuffix.Name = "labelSuffix";
//            this.labelSuffix.Size = new System.Drawing.Size(48, 24);
//            this.labelSuffix.TabIndex = 25;
//            this.labelSuffix.Text = "Suffix";
//            this.labelSuffix.Visible = false;
//            // 
//            // textBoxPostDirection
//            // 
//            this.textBoxPostDirection.Location = new System.Drawing.Point(984, 480);
//            this.textBoxPostDirection.Name = "textBoxPostDirection";
//            this.textBoxPostDirection.TabIndex = 24;
//            this.textBoxPostDirection.Text = "Post-Direction";
//            this.textBoxPostDirection.Visible = false;
//            // 
//            // labelPostDirection
//            // 
//            this.labelPostDirection.Location = new System.Drawing.Point(976, 464);
//            this.labelPostDirection.Name = "labelPostDirection";
//            this.labelPostDirection.Size = new System.Drawing.Size(80, 24);
//            this.labelPostDirection.TabIndex = 23;
//            this.labelPostDirection.Text = "Post-Direction";
//            this.labelPostDirection.Visible = false;
//            // 
//            // textBoxStreetnName
//            // 
//            this.textBoxStreetnName.Location = new System.Drawing.Point(24, 104);
//            this.textBoxStreetnName.Name = "textBoxStreetnName";
//            this.textBoxStreetnName.Size = new System.Drawing.Size(224, 22);
//            this.textBoxStreetnName.TabIndex = 96;
//            this.textBoxStreetnName.Text = "";
//            // 
//            // labelStreetnName
//            // 
//            this.labelStreetnName.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.labelStreetnName.Location = new System.Drawing.Point(24, 88);
//            this.labelStreetnName.Name = "labelStreetnName";
//            this.labelStreetnName.Size = new System.Drawing.Size(96, 24);
//            this.labelStreetnName.TabIndex = 21;
//            this.labelStreetnName.Text = "Street Address";
//            // 
//            // textBoxPreDirection
//            // 
//            this.textBoxPreDirection.Location = new System.Drawing.Point(784, 480);
//            this.textBoxPreDirection.Name = "textBoxPreDirection";
//            this.textBoxPreDirection.TabIndex = 20;
//            this.textBoxPreDirection.Text = "Pre-Direction";
//            this.textBoxPreDirection.Visible = false;
//            // 
//            // labelPreDirection
//            // 
//            this.labelPreDirection.Location = new System.Drawing.Point(782, 464);
//            this.labelPreDirection.Name = "labelPreDirection";
//            this.labelPreDirection.Size = new System.Drawing.Size(104, 24);
//            this.labelPreDirection.TabIndex = 19;
//            this.labelPreDirection.Text = "Pre-Direction";
//            this.labelPreDirection.Visible = false;
//            // 
//            // textBoxHouseNumber
//            // 
//            this.textBoxHouseNumber.Location = new System.Drawing.Point(672, 480);
//            this.textBoxHouseNumber.Name = "textBoxHouseNumber";
//            this.textBoxHouseNumber.TabIndex = 18;
//            this.textBoxHouseNumber.Text = "House Number";
//            this.textBoxHouseNumber.Visible = false;
//            // 
//            // labelHouseNumber
//            // 
//            this.labelHouseNumber.Location = new System.Drawing.Point(670, 464);
//            this.labelHouseNumber.Name = "labelHouseNumber";
//            this.labelHouseNumber.Size = new System.Drawing.Size(104, 24);
//            this.labelHouseNumber.TabIndex = 17;
//            this.labelHouseNumber.Text = "House Number";
//            this.labelHouseNumber.Visible = false;
//            // 
//            // textBoxPhoneNumber
//            // 
//            this.textBoxPhoneNumber.Location = new System.Drawing.Point(176, 40);
//            this.textBoxPhoneNumber.Name = "textBoxPhoneNumber";
//            this.textBoxPhoneNumber.Size = new System.Drawing.Size(104, 22);
//            this.textBoxPhoneNumber.TabIndex = 92;
//            this.textBoxPhoneNumber.Text = "";
//            // 
//            // labelPhoneNumber
//            // 
//            this.labelPhoneNumber.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.labelPhoneNumber.Location = new System.Drawing.Point(176, 24);
//            this.labelPhoneNumber.Name = "labelPhoneNumber";
//            this.labelPhoneNumber.Size = new System.Drawing.Size(96, 24);
//            this.labelPhoneNumber.TabIndex = 15;
//            this.labelPhoneNumber.Text = "Phone Number";
//            // 
//            // textBoxOwnerName
//            // 
//            this.textBoxOwnerName.Location = new System.Drawing.Point(24, 40);
//            this.textBoxOwnerName.Name = "textBoxOwnerName";
//            this.textBoxOwnerName.Size = new System.Drawing.Size(144, 22);
//            this.textBoxOwnerName.TabIndex = 91;
//            this.textBoxOwnerName.Text = "";
//            // 
//            // labelOwnerName
//            // 
//            this.labelOwnerName.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.labelOwnerName.Location = new System.Drawing.Point(24, 24);
//            this.labelOwnerName.Name = "labelOwnerName";
//            this.labelOwnerName.Size = new System.Drawing.Size(96, 24);
//            this.labelOwnerName.TabIndex = 13;
//            this.labelOwnerName.Text = "Owner Name";
//            // 
//            // labelOwnerNameGroup
//            // 
//            this.labelOwnerNameGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
//            this.labelOwnerNameGroup.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.labelOwnerNameGroup.Location = new System.Drawing.Point(8, 8);
//            this.labelOwnerNameGroup.Name = "labelOwnerNameGroup";
//            this.labelOwnerNameGroup.Size = new System.Drawing.Size(304, 23);
//            this.labelOwnerNameGroup.TabIndex = 12;
//            this.labelOwnerNameGroup.Text = "OWNERSHIP && MAILING INFO";
//            // 
//            // textBoxSchoolDistrict
//            // 
//            this.textBoxSchoolDistrict.Location = new System.Drawing.Point(416, 152);
//            this.textBoxSchoolDistrict.Name = "textBoxSchoolDistrict";
//            this.textBoxSchoolDistrict.Size = new System.Drawing.Size(120, 22);
//            this.textBoxSchoolDistrict.TabIndex = 150;
//            this.textBoxSchoolDistrict.Text = "";
//            // 
//            // textBoxLotNumber
//            // 
//            this.textBoxLotNumber.Location = new System.Drawing.Point(24, 152);
//            this.textBoxLotNumber.Name = "textBoxLotNumber";
//            this.textBoxLotNumber.TabIndex = 120;
//            this.textBoxLotNumber.Text = "";
//            // 
//            // textBoxSubdivision
//            // 
//            this.textBoxSubdivision.Location = new System.Drawing.Point(592, 104);
//            this.textBoxSubdivision.Name = "textBoxSubdivision";
//            this.textBoxSubdivision.Size = new System.Drawing.Size(128, 22);
//            this.textBoxSubdivision.TabIndex = 110;
//            this.textBoxSubdivision.Text = "";
//            // 
//            // textBoxComparableNumber
//            // 
//            this.textBoxComparableNumber.Location = new System.Drawing.Point(160, 24);
//            this.textBoxComparableNumber.Name = "textBoxComparableNumber";
//            this.textBoxComparableNumber.Size = new System.Drawing.Size(112, 22);
//            this.textBoxComparableNumber.TabIndex = 8;
//            this.textBoxComparableNumber.Text = "";
//            // 
//            // textBoxDistanceFromSubject
//            // 
//            this.textBoxDistanceFromSubject.Location = new System.Drawing.Point(16, 24);
//            this.textBoxDistanceFromSubject.Name = "textBoxDistanceFromSubject";
//            this.textBoxDistanceFromSubject.Size = new System.Drawing.Size(128, 22);
//            this.textBoxDistanceFromSubject.TabIndex = 7;
//            this.textBoxDistanceFromSubject.Text = "";
//            // 
//            // tabPagePropertyCharacteristics
//            // 
//            this.tabPagePropertyCharacteristics.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(210)), ((System.Byte)(216)), ((System.Byte)(240)));
//            this.tabPagePropertyCharacteristics.Controls.Add(this.textBoxOtherImprovements);
//            this.tabPagePropertyCharacteristics.Controls.Add(this.label57);
//            this.tabPagePropertyCharacteristics.Controls.Add(this.textBoxAirConditioning);
//            this.tabPagePropertyCharacteristics.Controls.Add(this.labelAirConditioning);
//            this.tabPagePropertyCharacteristics.Controls.Add(this.pictureBoxPropCharact);
//            this.tabPagePropertyCharacteristics.Controls.Add(this.textBoxBasementArea);
//            this.tabPagePropertyCharacteristics.Controls.Add(this.label9);
//            this.tabPagePropertyCharacteristics.Controls.Add(this.textBoxBasement);
//            this.tabPagePropertyCharacteristics.Controls.Add(this.label10);
//            this.tabPagePropertyCharacteristics.Controls.Add(this.textBoxCondition);
//            this.tabPagePropertyCharacteristics.Controls.Add(this.label8);
//            this.tabPagePropertyCharacteristics.Controls.Add(this.textBoxQuality);
//            this.tabPagePropertyCharacteristics.Controls.Add(this.label7);
//            this.tabPagePropertyCharacteristics.Controls.Add(this.textBoxFoundation);
//            this.tabPagePropertyCharacteristics.Controls.Add(this.label6);
//            this.tabPagePropertyCharacteristics.Controls.Add(this.textBoxRoofMaterials);
//            this.tabPagePropertyCharacteristics.Controls.Add(this.label5);
//            this.tabPagePropertyCharacteristics.Controls.Add(this.textBoxFireplaces);
//            this.tabPagePropertyCharacteristics.Controls.Add(this.label4);
//            this.tabPagePropertyCharacteristics.Controls.Add(this.textBoxGarageArea);
//            this.tabPagePropertyCharacteristics.Controls.Add(this.label3);
//            this.tabPagePropertyCharacteristics.Controls.Add(this.textBoxLotWidth);
//            this.tabPagePropertyCharacteristics.Controls.Add(this.label1);
//            this.tabPagePropertyCharacteristics.Controls.Add(this.textBoxLotDepth);
//            this.tabPagePropertyCharacteristics.Controls.Add(this.label2);
//            this.tabPagePropertyCharacteristics.Controls.Add(this.textBoxLandUse);
//            this.tabPagePropertyCharacteristics.Controls.Add(this.labelLandUse);
//            this.tabPagePropertyCharacteristics.Controls.Add(this.textBoxTotalBaths);
//            this.tabPagePropertyCharacteristics.Controls.Add(this.labelTotalBaths);
//            this.tabPagePropertyCharacteristics.Controls.Add(this.textBoxTotalBedrooms);
//            this.tabPagePropertyCharacteristics.Controls.Add(this.labelTotalBedrooms);
//            this.tabPagePropertyCharacteristics.Controls.Add(this.textBoxTotalRooms);
//            this.tabPagePropertyCharacteristics.Controls.Add(this.labelTotalRooms);
//            this.tabPagePropertyCharacteristics.Controls.Add(this.textBoxZoning);
//            this.tabPagePropertyCharacteristics.Controls.Add(this.labelZoning);
//            this.tabPagePropertyCharacteristics.Controls.Add(this.textBoxNumberOfStories);
//            this.tabPagePropertyCharacteristics.Controls.Add(this.labelNumberOfStories);
//            this.tabPagePropertyCharacteristics.Controls.Add(this.textBoxPool);
//            this.tabPagePropertyCharacteristics.Controls.Add(this.labelPool);
//            this.tabPagePropertyCharacteristics.Controls.Add(this.textBoxRoofType);
//            this.tabPagePropertyCharacteristics.Controls.Add(this.labelRoofType);
//            this.tabPagePropertyCharacteristics.Controls.Add(this.textBoxParkingCapacity);
//            this.tabPagePropertyCharacteristics.Controls.Add(this.labelParkingCapacity);
//            this.tabPagePropertyCharacteristics.Controls.Add(this.textBoxParkingType);
//            this.tabPagePropertyCharacteristics.Controls.Add(this.labelParkingType);
//            this.tabPagePropertyCharacteristics.Controls.Add(this.textBoxLotSize);
//            this.tabPagePropertyCharacteristics.Controls.Add(this.labelLotSize);
//            this.tabPagePropertyCharacteristics.Controls.Add(this.textBoxLotArea);
//            this.tabPagePropertyCharacteristics.Controls.Add(this.labelLotArea);
//            this.tabPagePropertyCharacteristics.Controls.Add(this.textBoxTotalLivingArea);
//            this.tabPagePropertyCharacteristics.Controls.Add(this.labelTotalLivingArea);
//            this.tabPagePropertyCharacteristics.Controls.Add(this.textBoxYearBuilt);
//            this.tabPagePropertyCharacteristics.Controls.Add(this.labelYearBuilt);
//            this.tabPagePropertyCharacteristics.Controls.Add(this.label11);
//            this.tabPagePropertyCharacteristics.Controls.Add(this.label13);
//            this.tabPagePropertyCharacteristics.Controls.Add(this.label14);
//            this.tabPagePropertyCharacteristics.Controls.Add(this.label12);
//            this.tabPagePropertyCharacteristics.Location = new System.Drawing.Point(4, 22);
//            this.tabPagePropertyCharacteristics.Name = "tabPagePropertyCharacteristics";
//            this.tabPagePropertyCharacteristics.Size = new System.Drawing.Size(902, 528);
//            this.tabPagePropertyCharacteristics.TabIndex = 1;
//            this.tabPagePropertyCharacteristics.Text = "Property Characteristics";
//            // 
//            // textBoxOtherImprovements
//            // 
//            this.textBoxOtherImprovements.Location = new System.Drawing.Point(464, 336);
//            this.textBoxOtherImprovements.Name = "textBoxOtherImprovements";
//            this.textBoxOtherImprovements.Size = new System.Drawing.Size(128, 22);
//            this.textBoxOtherImprovements.TabIndex = 116;
//            this.textBoxOtherImprovements.Text = "";
//            // 
//            // label57
//            // 
//            this.label57.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.label57.Location = new System.Drawing.Point(464, 320);
//            this.label57.Name = "label57";
//            this.label57.Size = new System.Drawing.Size(128, 24);
//            this.label57.TabIndex = 115;
//            this.label57.Text = "Other Improvements";
//            // 
//            // textBoxAirConditioning
//            // 
//            this.textBoxAirConditioning.Location = new System.Drawing.Point(360, 336);
//            this.textBoxAirConditioning.Name = "textBoxAirConditioning";
//            this.textBoxAirConditioning.Size = new System.Drawing.Size(96, 22);
//            this.textBoxAirConditioning.TabIndex = 58;
//            this.textBoxAirConditioning.Text = "";
//            // 
//            // labelAirConditioning
//            // 
//            this.labelAirConditioning.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.labelAirConditioning.Location = new System.Drawing.Point(360, 320);
//            this.labelAirConditioning.Name = "labelAirConditioning";
//            this.labelAirConditioning.Size = new System.Drawing.Size(104, 24);
//            this.labelAirConditioning.TabIndex = 57;
//            this.labelAirConditioning.Text = "Air Conditioning";
//            // 
//            // pictureBoxPropCharact
//            // 
//            this.pictureBoxPropCharact.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
//            this.pictureBoxPropCharact.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxPropCharact.Image")));
//            this.pictureBoxPropCharact.Location = new System.Drawing.Point(878, 8);
//            this.pictureBoxPropCharact.Name = "pictureBoxPropCharact";
//            this.pictureBoxPropCharact.Size = new System.Drawing.Size(16, 16);
//            this.pictureBoxPropCharact.TabIndex = 117;
//            this.pictureBoxPropCharact.TabStop = false;
//            // 
//            // textBoxBasementArea
//            // 
//            this.textBoxBasementArea.Location = new System.Drawing.Point(256, 336);
//            this.textBoxBasementArea.Name = "textBoxBasementArea";
//            this.textBoxBasementArea.Size = new System.Drawing.Size(88, 22);
//            this.textBoxBasementArea.TabIndex = 110;
//            this.textBoxBasementArea.Text = "";
//            // 
//            // label9
//            // 
//            this.label9.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.label9.Location = new System.Drawing.Point(256, 320);
//            this.label9.Name = "label9";
//            this.label9.Size = new System.Drawing.Size(104, 24);
//            this.label9.TabIndex = 109;
//            this.label9.Text = "Basement Area";
//            // 
//            // textBoxBasement
//            // 
//            this.textBoxBasement.Location = new System.Drawing.Point(160, 336);
//            this.textBoxBasement.Name = "textBoxBasement";
//            this.textBoxBasement.Size = new System.Drawing.Size(80, 22);
//            this.textBoxBasement.TabIndex = 108;
//            this.textBoxBasement.Text = "";
//            // 
//            // label10
//            // 
//            this.label10.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.label10.Location = new System.Drawing.Point(160, 320);
//            this.label10.Name = "label10";
//            this.label10.Size = new System.Drawing.Size(96, 24);
//            this.label10.TabIndex = 107;
//            this.label10.Text = "Basement";
//            // 
//            // textBoxCondition
//            // 
//            this.textBoxCondition.Location = new System.Drawing.Point(440, 296);
//            this.textBoxCondition.Name = "textBoxCondition";
//            this.textBoxCondition.Size = new System.Drawing.Size(152, 22);
//            this.textBoxCondition.TabIndex = 106;
//            this.textBoxCondition.Text = "";
//            // 
//            // label8
//            // 
//            this.label8.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.label8.Location = new System.Drawing.Point(440, 280);
//            this.label8.Name = "label8";
//            this.label8.Size = new System.Drawing.Size(96, 24);
//            this.label8.TabIndex = 105;
//            this.label8.Text = "Condition";
//            // 
//            // textBoxQuality
//            // 
//            this.textBoxQuality.Location = new System.Drawing.Point(352, 296);
//            this.textBoxQuality.Name = "textBoxQuality";
//            this.textBoxQuality.Size = new System.Drawing.Size(80, 22);
//            this.textBoxQuality.TabIndex = 104;
//            this.textBoxQuality.Text = "";
//            // 
//            // label7
//            // 
//            this.label7.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.label7.Location = new System.Drawing.Point(352, 280);
//            this.label7.Name = "label7";
//            this.label7.Size = new System.Drawing.Size(64, 24);
//            this.label7.TabIndex = 103;
//            this.label7.Text = "Quality";
//            // 
//            // textBoxFoundation
//            // 
//            this.textBoxFoundation.Location = new System.Drawing.Point(256, 296);
//            this.textBoxFoundation.Name = "textBoxFoundation";
//            this.textBoxFoundation.Size = new System.Drawing.Size(88, 22);
//            this.textBoxFoundation.TabIndex = 102;
//            this.textBoxFoundation.Text = "";
//            // 
//            // label6
//            // 
//            this.label6.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.label6.Location = new System.Drawing.Point(256, 280);
//            this.label6.Name = "label6";
//            this.label6.Size = new System.Drawing.Size(96, 24);
//            this.label6.TabIndex = 101;
//            this.label6.Text = "Foundation";
//            // 
//            // textBoxRoofMaterials
//            // 
//            this.textBoxRoofMaterials.Location = new System.Drawing.Point(144, 296);
//            this.textBoxRoofMaterials.Name = "textBoxRoofMaterials";
//            this.textBoxRoofMaterials.Size = new System.Drawing.Size(96, 22);
//            this.textBoxRoofMaterials.TabIndex = 100;
//            this.textBoxRoofMaterials.Text = "";
//            // 
//            // label5
//            // 
//            this.label5.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.label5.Location = new System.Drawing.Point(144, 280);
//            this.label5.Name = "label5";
//            this.label5.Size = new System.Drawing.Size(96, 24);
//            this.label5.TabIndex = 99;
//            this.label5.Text = "Roof Materials";
//            // 
//            // textBoxFireplaces
//            // 
//            this.textBoxFireplaces.Location = new System.Drawing.Point(480, 232);
//            this.textBoxFireplaces.Name = "textBoxFireplaces";
//            this.textBoxFireplaces.Size = new System.Drawing.Size(60, 22);
//            this.textBoxFireplaces.TabIndex = 98;
//            this.textBoxFireplaces.Text = "";
//            // 
//            // label4
//            // 
//            this.label4.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.label4.Location = new System.Drawing.Point(480, 216);
//            this.label4.Name = "label4";
//            this.label4.Size = new System.Drawing.Size(96, 24);
//            this.label4.TabIndex = 97;
//            this.label4.Text = "Fireplaces";
//            // 
//            // textBoxGarageArea
//            // 
//            this.textBoxGarageArea.Location = new System.Drawing.Point(288, 232);
//            this.textBoxGarageArea.Name = "textBoxGarageArea";
//            this.textBoxGarageArea.Size = new System.Drawing.Size(88, 22);
//            this.textBoxGarageArea.TabIndex = 96;
//            this.textBoxGarageArea.Text = "";
//            // 
//            // label3
//            // 
//            this.label3.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.label3.Location = new System.Drawing.Point(288, 216);
//            this.label3.Name = "label3";
//            this.label3.Size = new System.Drawing.Size(96, 24);
//            this.label3.TabIndex = 95;
//            this.label3.Text = "Garage Area";
//            // 
//            // textBoxLotWidth
//            // 
//            this.textBoxLotWidth.Location = new System.Drawing.Point(432, 168);
//            this.textBoxLotWidth.Name = "textBoxLotWidth";
//            this.textBoxLotWidth.Size = new System.Drawing.Size(60, 22);
//            this.textBoxLotWidth.TabIndex = 94;
//            this.textBoxLotWidth.Text = "";
//            // 
//            // label1
//            // 
//            this.label1.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.label1.Location = new System.Drawing.Point(432, 152);
//            this.label1.Name = "label1";
//            this.label1.Size = new System.Drawing.Size(88, 24);
//            this.label1.TabIndex = 93;
//            this.label1.Text = "Lot Width";
//            // 
//            // textBoxLotDepth
//            // 
//            this.textBoxLotDepth.Location = new System.Drawing.Point(336, 168);
//            this.textBoxLotDepth.Name = "textBoxLotDepth";
//            this.textBoxLotDepth.Size = new System.Drawing.Size(60, 22);
//            this.textBoxLotDepth.TabIndex = 92;
//            this.textBoxLotDepth.Text = "";
//            // 
//            // label2
//            // 
//            this.label2.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.label2.Location = new System.Drawing.Point(336, 152);
//            this.label2.Name = "label2";
//            this.label2.Size = new System.Drawing.Size(96, 24);
//            this.label2.TabIndex = 91;
//            this.label2.Text = "Lot Depth";
//            // 
//            // textBoxLandUse
//            // 
//            this.textBoxLandUse.Location = new System.Drawing.Point(24, 32);
//            this.textBoxLandUse.Name = "textBoxLandUse";
//            this.textBoxLandUse.Size = new System.Drawing.Size(90, 22);
//            this.textBoxLandUse.TabIndex = 90;
//            this.textBoxLandUse.Text = "";
//            // 
//            // labelLandUse
//            // 
//            this.labelLandUse.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
//            this.labelLandUse.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.labelLandUse.Location = new System.Drawing.Point(24, 16);
//            this.labelLandUse.Name = "labelLandUse";
//            this.labelLandUse.Size = new System.Drawing.Size(80, 24);
//            this.labelLandUse.TabIndex = 89;
//            this.labelLandUse.Text = "LAND USE";
//            // 
//            // textBoxTotalBaths
//            // 
//            this.textBoxTotalBaths.Location = new System.Drawing.Point(264, 104);
//            this.textBoxTotalBaths.Name = "textBoxTotalBaths";
//            this.textBoxTotalBaths.Size = new System.Drawing.Size(48, 22);
//            this.textBoxTotalBaths.TabIndex = 70;
//            this.textBoxTotalBaths.Text = "";
//            // 
//            // labelTotalBaths
//            // 
//            this.labelTotalBaths.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.labelTotalBaths.Location = new System.Drawing.Point(264, 88);
//            this.labelTotalBaths.Name = "labelTotalBaths";
//            this.labelTotalBaths.Size = new System.Drawing.Size(88, 24);
//            this.labelTotalBaths.TabIndex = 69;
//            this.labelTotalBaths.Text = "Total baths";
//            // 
//            // textBoxTotalBedrooms
//            // 
//            this.textBoxTotalBedrooms.Location = new System.Drawing.Point(144, 104);
//            this.textBoxTotalBedrooms.Name = "textBoxTotalBedrooms";
//            this.textBoxTotalBedrooms.Size = new System.Drawing.Size(48, 22);
//            this.textBoxTotalBedrooms.TabIndex = 68;
//            this.textBoxTotalBedrooms.Text = "";
//            // 
//            // labelTotalBedrooms
//            // 
//            this.labelTotalBedrooms.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.labelTotalBedrooms.Location = new System.Drawing.Point(144, 88);
//            this.labelTotalBedrooms.Name = "labelTotalBedrooms";
//            this.labelTotalBedrooms.Size = new System.Drawing.Size(112, 24);
//            this.labelTotalBedrooms.TabIndex = 67;
//            this.labelTotalBedrooms.Text = "Total Bedrooms";
//            // 
//            // textBoxTotalRooms
//            // 
//            this.textBoxTotalRooms.Location = new System.Drawing.Point(40, 104);
//            this.textBoxTotalRooms.Name = "textBoxTotalRooms";
//            this.textBoxTotalRooms.Size = new System.Drawing.Size(48, 22);
//            this.textBoxTotalRooms.TabIndex = 66;
//            this.textBoxTotalRooms.Text = "";
//            // 
//            // labelTotalRooms
//            // 
//            this.labelTotalRooms.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.labelTotalRooms.Location = new System.Drawing.Point(40, 88);
//            this.labelTotalRooms.Name = "labelTotalRooms";
//            this.labelTotalRooms.Size = new System.Drawing.Size(96, 24);
//            this.labelTotalRooms.TabIndex = 65;
//            this.labelTotalRooms.Text = "Total Rooms";
//            // 
//            // textBoxZoning
//            // 
//            this.textBoxZoning.Location = new System.Drawing.Point(144, 32);
//            this.textBoxZoning.Name = "textBoxZoning";
//            this.textBoxZoning.Size = new System.Drawing.Size(90, 22);
//            this.textBoxZoning.TabIndex = 64;
//            this.textBoxZoning.Text = "";
//            // 
//            // labelZoning
//            // 
//            this.labelZoning.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
//            this.labelZoning.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.labelZoning.Location = new System.Drawing.Point(144, 16);
//            this.labelZoning.Name = "labelZoning";
//            this.labelZoning.Size = new System.Drawing.Size(96, 24);
//            this.labelZoning.TabIndex = 63;
//            this.labelZoning.Text = "ZONING";
//            // 
//            // textBoxNumberOfStories
//            // 
//            this.textBoxNumberOfStories.Location = new System.Drawing.Point(40, 336);
//            this.textBoxNumberOfStories.Name = "textBoxNumberOfStories";
//            this.textBoxNumberOfStories.Size = new System.Drawing.Size(104, 22);
//            this.textBoxNumberOfStories.TabIndex = 62;
//            this.textBoxNumberOfStories.Text = "";
//            // 
//            // labelNumberOfStories
//            // 
//            this.labelNumberOfStories.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.labelNumberOfStories.Location = new System.Drawing.Point(40, 320);
//            this.labelNumberOfStories.Name = "labelNumberOfStories";
//            this.labelNumberOfStories.Size = new System.Drawing.Size(112, 24);
//            this.labelNumberOfStories.TabIndex = 61;
//            this.labelNumberOfStories.Text = "Number of Stories";
//            // 
//            // textBoxPool
//            // 
//            this.textBoxPool.Location = new System.Drawing.Point(384, 232);
//            this.textBoxPool.Name = "textBoxPool";
//            this.textBoxPool.Size = new System.Drawing.Size(80, 22);
//            this.textBoxPool.TabIndex = 56;
//            this.textBoxPool.Text = "";
//            // 
//            // labelPool
//            // 
//            this.labelPool.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.labelPool.Location = new System.Drawing.Point(384, 216);
//            this.labelPool.Name = "labelPool";
//            this.labelPool.Size = new System.Drawing.Size(96, 24);
//            this.labelPool.TabIndex = 55;
//            this.labelPool.Text = "Pool";
//            // 
//            // textBoxRoofType
//            // 
//            this.textBoxRoofType.Location = new System.Drawing.Point(40, 296);
//            this.textBoxRoofType.Name = "textBoxRoofType";
//            this.textBoxRoofType.TabIndex = 54;
//            this.textBoxRoofType.Text = "";
//            // 
//            // labelRoofType
//            // 
//            this.labelRoofType.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.labelRoofType.Location = new System.Drawing.Point(40, 280);
//            this.labelRoofType.Name = "labelRoofType";
//            this.labelRoofType.Size = new System.Drawing.Size(88, 24);
//            this.labelRoofType.TabIndex = 53;
//            this.labelRoofType.Text = "Roof Type";
//            // 
//            // textBoxParkingCapacity
//            // 
//            this.textBoxParkingCapacity.Location = new System.Drawing.Point(168, 232);
//            this.textBoxParkingCapacity.Name = "textBoxParkingCapacity";
//            this.textBoxParkingCapacity.Size = new System.Drawing.Size(60, 22);
//            this.textBoxParkingCapacity.TabIndex = 52;
//            this.textBoxParkingCapacity.Text = "";
//            // 
//            // labelParkingCapacity
//            // 
//            this.labelParkingCapacity.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.labelParkingCapacity.Location = new System.Drawing.Point(168, 216);
//            this.labelParkingCapacity.Name = "labelParkingCapacity";
//            this.labelParkingCapacity.Size = new System.Drawing.Size(112, 24);
//            this.labelParkingCapacity.TabIndex = 51;
//            this.labelParkingCapacity.Text = "Parking Capacity";
//            // 
//            // textBoxParkingType
//            // 
//            this.textBoxParkingType.Location = new System.Drawing.Point(40, 232);
//            this.textBoxParkingType.Name = "textBoxParkingType";
//            this.textBoxParkingType.Size = new System.Drawing.Size(120, 22);
//            this.textBoxParkingType.TabIndex = 50;
//            this.textBoxParkingType.Text = "";
//            // 
//            // labelParkingType
//            // 
//            this.labelParkingType.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.labelParkingType.Location = new System.Drawing.Point(40, 216);
//            this.labelParkingType.Name = "labelParkingType";
//            this.labelParkingType.Size = new System.Drawing.Size(96, 24);
//            this.labelParkingType.TabIndex = 49;
//            this.labelParkingType.Text = "Parking Type";
//            // 
//            // textBoxLotSize
//            // 
//            this.textBoxLotSize.Location = new System.Drawing.Point(240, 168);
//            this.textBoxLotSize.Name = "textBoxLotSize";
//            this.textBoxLotSize.Size = new System.Drawing.Size(60, 22);
//            this.textBoxLotSize.TabIndex = 48;
//            this.textBoxLotSize.Text = "";
//            // 
//            // labelLotSize
//            // 
//            this.labelLotSize.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.labelLotSize.Location = new System.Drawing.Point(240, 152);
//            this.labelLotSize.Name = "labelLotSize";
//            this.labelLotSize.Size = new System.Drawing.Size(88, 24);
//            this.labelLotSize.TabIndex = 47;
//            this.labelLotSize.Text = "Lot Size";
//            // 
//            // textBoxLotArea
//            // 
//            this.textBoxLotArea.Location = new System.Drawing.Point(152, 168);
//            this.textBoxLotArea.Name = "textBoxLotArea";
//            this.textBoxLotArea.Size = new System.Drawing.Size(60, 22);
//            this.textBoxLotArea.TabIndex = 46;
//            this.textBoxLotArea.Text = "";
//            // 
//            // labelLotArea
//            // 
//            this.labelLotArea.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.labelLotArea.Location = new System.Drawing.Point(152, 152);
//            this.labelLotArea.Name = "labelLotArea";
//            this.labelLotArea.Size = new System.Drawing.Size(88, 24);
//            this.labelLotArea.TabIndex = 45;
//            this.labelLotArea.Text = "Lot Area";
//            // 
//            // textBoxTotalLivingArea
//            // 
//            this.textBoxTotalLivingArea.Location = new System.Drawing.Point(40, 168);
//            this.textBoxTotalLivingArea.Name = "textBoxTotalLivingArea";
//            this.textBoxTotalLivingArea.Size = new System.Drawing.Size(60, 22);
//            this.textBoxTotalLivingArea.TabIndex = 44;
//            this.textBoxTotalLivingArea.Text = "";
//            // 
//            // labelTotalLivingArea
//            // 
//            this.labelTotalLivingArea.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.labelTotalLivingArea.Location = new System.Drawing.Point(40, 152);
//            this.labelTotalLivingArea.Name = "labelTotalLivingArea";
//            this.labelTotalLivingArea.Size = new System.Drawing.Size(112, 24);
//            this.labelTotalLivingArea.TabIndex = 43;
//            this.labelTotalLivingArea.Text = "Total Living Area";
//            // 
//            // textBoxYearBuilt
//            // 
//            this.textBoxYearBuilt.Location = new System.Drawing.Point(264, 32);
//            this.textBoxYearBuilt.Name = "textBoxYearBuilt";
//            this.textBoxYearBuilt.Size = new System.Drawing.Size(48, 22);
//            this.textBoxYearBuilt.TabIndex = 42;
//            this.textBoxYearBuilt.Text = "";
//            // 
//            // labelYearBuilt
//            // 
//            this.labelYearBuilt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
//            this.labelYearBuilt.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.labelYearBuilt.Location = new System.Drawing.Point(264, 16);
//            this.labelYearBuilt.Name = "labelYearBuilt";
//            this.labelYearBuilt.Size = new System.Drawing.Size(96, 24);
//            this.labelYearBuilt.TabIndex = 41;
//            this.labelYearBuilt.Text = "YEAR BUILT";
//            // 
//            // label11
//            // 
//            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
//            this.label11.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.label11.Location = new System.Drawing.Point(16, 72);
//            this.label11.Name = "label11";
//            this.label11.Size = new System.Drawing.Size(240, 24);
//            this.label11.TabIndex = 111;
//            this.label11.Text = "ROOMS && BATHROOMS";
//            // 
//            // label13
//            // 
//            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
//            this.label13.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.label13.Location = new System.Drawing.Point(16, 136);
//            this.label13.Name = "label13";
//            this.label13.TabIndex = 113;
//            this.label13.Text = "SIZE && AREA";
//            // 
//            // label14
//            // 
//            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
//            this.label14.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.label14.Location = new System.Drawing.Point(16, 200);
//            this.label14.Name = "label14";
//            this.label14.TabIndex = 114;
//            this.label14.Text = "AMENITIES";
//            // 
//            // label12
//            // 
//            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
//            this.label12.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.label12.Location = new System.Drawing.Point(16, 264);
//            this.label12.Name = "label12";
//            this.label12.TabIndex = 112;
//            this.label12.Text = "BUILDING";
//            // 
//            // textBoxCounty
//            // 
//            this.textBoxCounty.Location = new System.Drawing.Point(520, 104);
//            this.textBoxCounty.Name = "textBoxCounty";
//            this.textBoxCounty.Size = new System.Drawing.Size(64, 22);
//            this.textBoxCounty.TabIndex = 100;
//            this.textBoxCounty.Text = "";
//            // 
//            // labelSchoolDistrict
//            // 
//            this.labelSchoolDistrict.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.labelSchoolDistrict.Location = new System.Drawing.Point(416, 136);
//            this.labelSchoolDistrict.Name = "labelSchoolDistrict";
//            this.labelSchoolDistrict.Size = new System.Drawing.Size(104, 24);
//            this.labelSchoolDistrict.TabIndex = 5;
//            this.labelSchoolDistrict.Text = "School District";
//            // 
//            // labelLotNumber
//            // 
//            this.labelLotNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
//            this.labelLotNumber.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.labelLotNumber.Location = new System.Drawing.Point(24, 136);
//            this.labelLotNumber.Name = "labelLotNumber";
//            this.labelLotNumber.Size = new System.Drawing.Size(96, 24);
//            this.labelLotNumber.TabIndex = 4;
//            this.labelLotNumber.Text = "Lot Number";
//            // 
//            // labelSubdivision
//            // 
//            this.labelSubdivision.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.labelSubdivision.Location = new System.Drawing.Point(584, 88);
//            this.labelSubdivision.Name = "labelSubdivision";
//            this.labelSubdivision.Size = new System.Drawing.Size(80, 24);
//            this.labelSubdivision.TabIndex = 3;
//            this.labelSubdivision.Text = "Subdivision";
//            // 
//            // labelComparableNumber
//            // 
//            this.labelComparableNumber.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.labelComparableNumber.Location = new System.Drawing.Point(158, 8);
//            this.labelComparableNumber.Name = "labelComparableNumber";
//            this.labelComparableNumber.Size = new System.Drawing.Size(136, 24);
//            this.labelComparableNumber.TabIndex = 2;
//            this.labelComparableNumber.Text = "Comparable Number";
//            // 
//            // labelDistanceFromSubject
//            // 
//            this.labelDistanceFromSubject.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.labelDistanceFromSubject.Location = new System.Drawing.Point(14, 8);
//            this.labelDistanceFromSubject.Name = "labelDistanceFromSubject";
//            this.labelDistanceFromSubject.Size = new System.Drawing.Size(144, 24);
//            this.labelDistanceFromSubject.TabIndex = 1;
//            this.labelDistanceFromSubject.Text = "Distance from Subject";
//            // 
//            // tabPageOwnershipNLocations
//            // 
//            this.tabPageOwnershipNLocations.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(210)), ((System.Byte)(216)), ((System.Byte)(240)));
//            this.tabPageOwnershipNLocations.Controls.Add(this.pictureBoxOwnershipLocations);
//            this.tabPageOwnershipNLocations.Controls.Add(this.textBoxUnitNumber);
//            this.tabPageOwnershipNLocations.Controls.Add(this.labelUnitNumber);
//            this.tabPageOwnershipNLocations.Controls.Add(this.textBoxSuffix);
//            this.tabPageOwnershipNLocations.Controls.Add(this.labelSuffix);
//            this.tabPageOwnershipNLocations.Controls.Add(this.textBoxPostDirection);
//            this.tabPageOwnershipNLocations.Controls.Add(this.labelPostDirection);
//            this.tabPageOwnershipNLocations.Controls.Add(this.textBoxPreDirection);
//            this.tabPageOwnershipNLocations.Controls.Add(this.labelPreDirection);
//            this.tabPageOwnershipNLocations.Controls.Add(this.textBoxHouseNumber);
//            this.tabPageOwnershipNLocations.Controls.Add(this.labelHouseNumber);
//            this.tabPageOwnershipNLocations.Controls.Add(this.panel1);
//            this.tabPageOwnershipNLocations.Controls.Add(this.panel2);
//            this.tabPageOwnershipNLocations.Controls.Add(this.panel3);
//            this.tabPageOwnershipNLocations.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.tabPageOwnershipNLocations.Location = new System.Drawing.Point(4, 22);
//            this.tabPageOwnershipNLocations.Name = "tabPageOwnershipNLocations";
//            this.tabPageOwnershipNLocations.Size = new System.Drawing.Size(902, 528);
//            this.tabPageOwnershipNLocations.TabIndex = 2;
//            this.tabPageOwnershipNLocations.Text = "Ownership & Locations";
//            // 
//            // pictureBoxOwnershipLocations
//            // 
//            this.pictureBoxOwnershipLocations.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
//            this.pictureBoxOwnershipLocations.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxOwnershipLocations.Image")));
//            this.pictureBoxOwnershipLocations.Location = new System.Drawing.Point(878, 8);
//            this.pictureBoxOwnershipLocations.Name = "pictureBoxOwnershipLocations";
//            this.pictureBoxOwnershipLocations.Size = new System.Drawing.Size(16, 16);
//            this.pictureBoxOwnershipLocations.TabIndex = 134;
//            this.pictureBoxOwnershipLocations.TabStop = false;
//            // 
//            // panel1
//            // 
//            this.panel1.Controls.Add(this.textBoxStateA);
//            this.panel1.Controls.Add(this.textBoxCountyA);
//            this.panel1.Controls.Add(this.textBoxZipA);
//            this.panel1.Controls.Add(this.label74);
//            this.panel1.Controls.Add(this.label75);
//            this.panel1.Controls.Add(this.label76);
//            this.panel1.Controls.Add(this.bPrintReport);
//            this.panel1.Controls.Add(this.label90);
//            this.panel1.Controls.Add(this.label85);
//            this.panel1.Controls.Add(this.label89);
//            this.panel1.Controls.Add(this.label88);
//            this.panel1.Controls.Add(this.textBoxStateC);
//            this.panel1.Controls.Add(this.textBoxCountyC);
//            this.panel1.Controls.Add(this.textBoxZipC);
//            this.panel1.Controls.Add(this.textBoxLatNameC);
//            this.panel1.Controls.Add(this.textBoxFirstNameC);
//            this.panel1.Controls.Add(this.label64);
//            this.panel1.Controls.Add(this.label66);
//            this.panel1.Controls.Add(this.label67);
//            this.panel1.Controls.Add(this.label65);
//            this.panel1.Controls.Add(this.textBoxStateB);
//            this.panel1.Controls.Add(this.textBoxCountyB);
//            this.panel1.Controls.Add(this.textBoxZipB);
//            this.panel1.Controls.Add(this.textBoxApnB);
//            this.panel1.Controls.Add(this.label68);
//            this.panel1.Controls.Add(this.label69);
//            this.panel1.Controls.Add(this.label70);
//            this.panel1.Controls.Add(this.label71);
//            this.panel1.Controls.Add(this.buttonSearchC);
//            this.panel1.Controls.Add(this.label72);
//            this.panel1.Controls.Add(this.buttonSearchB);
//            this.panel1.Controls.Add(this.label73);
//            this.panel1.Controls.Add(this.textBoxUnitNoA);
//            this.panel1.Controls.Add(this.textBoxSuffixA);
//            this.panel1.Controls.Add(this.textBoxPostDirA);
//            this.panel1.Controls.Add(this.textBoxStreetA);
//            this.panel1.Controls.Add(this.textBoxPreDirA);
//            this.panel1.Controls.Add(this.textBoxStreetNoA);
//            this.panel1.Controls.Add(this.label77);
//            this.panel1.Controls.Add(this.label78);
//            this.panel1.Controls.Add(this.label79);
//            this.panel1.Controls.Add(this.label80);
//            this.panel1.Controls.Add(this.label81);
//            this.panel1.Controls.Add(this.label82);
//            this.panel1.Controls.Add(this.buttonSearchA);
//            this.panel1.Controls.Add(this.label83);
//            this.panel1.Controls.Add(this.label84);
//            this.panel1.Controls.Add(this.label63);
//            this.panel1.Controls.Add(this.labelLine);
//            this.panel1.Location = new System.Drawing.Point(0, 0);
//            this.panel1.Name = "panel1";
//            this.panel1.Size = new System.Drawing.Size(872, 264);
//            this.panel1.TabIndex = 132;
//            // 
//            // textBoxStateA
//            // 
//            this.textBoxStateA.Location = new System.Drawing.Point(576, 64);
//            this.textBoxStateA.MaxLength = 2;
//            this.textBoxStateA.Name = "textBoxStateA";
//            this.textBoxStateA.Size = new System.Drawing.Size(24, 22);
//            this.textBoxStateA.TabIndex = 66;
//            this.textBoxStateA.Text = "";
//            // 
//            // textBoxCountyA
//            // 
//            this.textBoxCountyA.Location = new System.Drawing.Point(504, 64);
//            this.textBoxCountyA.Name = "textBoxCountyA";
//            this.textBoxCountyA.Size = new System.Drawing.Size(64, 22);
//            this.textBoxCountyA.TabIndex = 65;
//            this.textBoxCountyA.Text = "";
//            // 
//            // textBoxZipA
//            // 
//            this.textBoxZipA.Location = new System.Drawing.Point(432, 64);
//            this.textBoxZipA.Name = "textBoxZipA";
//            this.textBoxZipA.Size = new System.Drawing.Size(64, 22);
//            this.textBoxZipA.TabIndex = 64;
//            this.textBoxZipA.Text = "";
//            // 
//            // label74
//            // 
//            this.label74.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.label74.Location = new System.Drawing.Point(576, 48);
//            this.label74.Name = "label74";
//            this.label74.Size = new System.Drawing.Size(104, 23);
//            this.label74.TabIndex = 57;
//            this.label74.Text = "State (i.e. CA)";
//            // 
//            // label75
//            // 
//            this.label75.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.label75.Location = new System.Drawing.Point(504, 48);
//            this.label75.Name = "label75";
//            this.label75.Size = new System.Drawing.Size(64, 23);
//            this.label75.TabIndex = 56;
//            this.label75.Text = "County";
//            // 
//            // label76
//            // 
//            this.label76.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.label76.Location = new System.Drawing.Point(432, 48);
//            this.label76.Name = "label76";
//            this.label76.Size = new System.Drawing.Size(64, 23);
//            this.label76.TabIndex = 55;
//            this.label76.Text = "ZIP";
//            // 
//            // bPrintReport
//            // 
//            this.bPrintReport.FlatStyle = System.Windows.Forms.FlatStyle.System;
//            this.bPrintReport.ForeColor = System.Drawing.SystemColors.ControlText;
//            this.bPrintReport.Location = new System.Drawing.Point(688, 96);
//            this.bPrintReport.Name = "bPrintReport";
//            this.bPrintReport.Size = new System.Drawing.Size(75, 48);
//            this.bPrintReport.TabIndex = 90;
//            this.bPrintReport.Text = "Print Report";
//            this.bPrintReport.Click += new System.EventHandler(this.bPrintReport_Click);
//            // 
//            // label90
//            // 
//            this.label90.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
//            this.label90.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.label90.Location = new System.Drawing.Point(432, 208);
//            this.label90.Name = "label90";
//            this.label90.Size = new System.Drawing.Size(16, 23);
//            this.label90.TabIndex = 135;
//            this.label90.Text = "&&";
//            // 
//            // label85
//            // 
//            this.label85.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
//            this.label85.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.label85.Location = new System.Drawing.Point(320, 208);
//            this.label85.Name = "label85";
//            this.label85.Size = new System.Drawing.Size(24, 23);
//            this.label85.TabIndex = 134;
//            this.label85.Text = "or";
//            // 
//            // label89
//            // 
//            this.label89.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
//            this.label89.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.label89.Location = new System.Drawing.Point(304, 136);
//            this.label89.Name = "label89";
//            this.label89.Size = new System.Drawing.Size(16, 23);
//            this.label89.TabIndex = 133;
//            this.label89.Text = "&&";
//            // 
//            // label88
//            // 
//            this.label88.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
//            this.label88.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.label88.Location = new System.Drawing.Point(192, 136);
//            this.label88.Name = "label88";
//            this.label88.Size = new System.Drawing.Size(24, 23);
//            this.label88.TabIndex = 132;
//            this.label88.Text = "or";
//            // 
//            // textBoxStateC
//            // 
//            this.textBoxStateC.Location = new System.Drawing.Point(456, 208);
//            this.textBoxStateC.MaxLength = 2;
//            this.textBoxStateC.Name = "textBoxStateC";
//            this.textBoxStateC.Size = new System.Drawing.Size(24, 22);
//            this.textBoxStateC.TabIndex = 88;
//            this.textBoxStateC.Text = "";
//            // 
//            // textBoxCountyC
//            // 
//            this.textBoxCountyC.Location = new System.Drawing.Point(360, 208);
//            this.textBoxCountyC.Name = "textBoxCountyC";
//            this.textBoxCountyC.Size = new System.Drawing.Size(64, 22);
//            this.textBoxCountyC.TabIndex = 87;
//            this.textBoxCountyC.Text = "";
//            // 
//            // textBoxZipC
//            // 
//            this.textBoxZipC.Location = new System.Drawing.Point(240, 208);
//            this.textBoxZipC.Name = "textBoxZipC";
//            this.textBoxZipC.Size = new System.Drawing.Size(64, 22);
//            this.textBoxZipC.TabIndex = 86;
//            this.textBoxZipC.Text = "";
//            // 
//            // textBoxLatNameC
//            // 
//            this.textBoxLatNameC.Location = new System.Drawing.Point(136, 208);
//            this.textBoxLatNameC.Name = "textBoxLatNameC";
//            this.textBoxLatNameC.Size = new System.Drawing.Size(88, 22);
//            this.textBoxLatNameC.TabIndex = 85;
//            this.textBoxLatNameC.Text = "";
//            // 
//            // textBoxFirstNameC
//            // 
//            this.textBoxFirstNameC.Location = new System.Drawing.Point(32, 208);
//            this.textBoxFirstNameC.Name = "textBoxFirstNameC";
//            this.textBoxFirstNameC.Size = new System.Drawing.Size(88, 22);
//            this.textBoxFirstNameC.TabIndex = 84;
//            this.textBoxFirstNameC.Text = "";
//            // 
//            // label64
//            // 
//            this.label64.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.label64.Location = new System.Drawing.Point(360, 192);
//            this.label64.Name = "label64";
//            this.label64.Size = new System.Drawing.Size(64, 23);
//            this.label64.TabIndex = 82;
//            this.label64.Text = "County";
//            // 
//            // label66
//            // 
//            this.label66.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.label66.Location = new System.Drawing.Point(136, 192);
//            this.label66.Name = "label66";
//            this.label66.Size = new System.Drawing.Size(80, 23);
//            this.label66.TabIndex = 80;
//            this.label66.Text = "Last Name";
//            // 
//            // label67
//            // 
//            this.label67.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.label67.Location = new System.Drawing.Point(32, 192);
//            this.label67.Name = "label67";
//            this.label67.Size = new System.Drawing.Size(88, 23);
//            this.label67.TabIndex = 79;
//            this.label67.Text = "First Name";
//            // 
//            // label65
//            // 
//            this.label65.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.label65.Location = new System.Drawing.Point(240, 192);
//            this.label65.Name = "label65";
//            this.label65.Size = new System.Drawing.Size(64, 23);
//            this.label65.TabIndex = 81;
//            this.label65.Text = "ZIP";
//            // 
//            // textBoxStateB
//            // 
//            this.textBoxStateB.Location = new System.Drawing.Point(328, 136);
//            this.textBoxStateB.MaxLength = 2;
//            this.textBoxStateB.Name = "textBoxStateB";
//            this.textBoxStateB.Size = new System.Drawing.Size(24, 22);
//            this.textBoxStateB.TabIndex = 78;
//            this.textBoxStateB.Text = "";
//            // 
//            // textBoxCountyB
//            // 
//            this.textBoxCountyB.Location = new System.Drawing.Point(232, 136);
//            this.textBoxCountyB.Name = "textBoxCountyB";
//            this.textBoxCountyB.Size = new System.Drawing.Size(64, 22);
//            this.textBoxCountyB.TabIndex = 77;
//            this.textBoxCountyB.Text = "";
//            // 
//            // textBoxZipB
//            // 
//            this.textBoxZipB.Location = new System.Drawing.Point(128, 136);
//            this.textBoxZipB.Name = "textBoxZipB";
//            this.textBoxZipB.Size = new System.Drawing.Size(40, 22);
//            this.textBoxZipB.TabIndex = 76;
//            this.textBoxZipB.Text = "";
//            // 
//            // textBoxApnB
//            // 
//            this.textBoxApnB.Location = new System.Drawing.Point(32, 136);
//            this.textBoxApnB.Name = "textBoxApnB";
//            this.textBoxApnB.Size = new System.Drawing.Size(88, 22);
//            this.textBoxApnB.TabIndex = 75;
//            this.textBoxApnB.Text = "";
//            // 
//            // label68
//            // 
//            this.label68.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.label68.Location = new System.Drawing.Point(328, 120);
//            this.label68.Name = "label68";
//            this.label68.Size = new System.Drawing.Size(96, 23);
//            this.label68.TabIndex = 74;
//            this.label68.Text = "State (i.e. CA)";
//            // 
//            // label69
//            // 
//            this.label69.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.label69.Location = new System.Drawing.Point(232, 120);
//            this.label69.Name = "label69";
//            this.label69.Size = new System.Drawing.Size(64, 23);
//            this.label69.TabIndex = 73;
//            this.label69.Text = "County";
//            // 
//            // label70
//            // 
//            this.label70.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.label70.Location = new System.Drawing.Point(128, 120);
//            this.label70.Name = "label70";
//            this.label70.Size = new System.Drawing.Size(64, 23);
//            this.label70.TabIndex = 72;
//            this.label70.Text = "ZIP";
//            // 
//            // label71
//            // 
//            this.label71.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.label71.Location = new System.Drawing.Point(32, 120);
//            this.label71.Name = "label71";
//            this.label71.Size = new System.Drawing.Size(64, 23);
//            this.label71.TabIndex = 71;
//            this.label71.Text = "APN";
//            // 
//            // buttonSearchC
//            // 
//            this.buttonSearchC.FlatStyle = System.Windows.Forms.FlatStyle.System;
//            this.buttonSearchC.ForeColor = System.Drawing.SystemColors.ControlText;
//            this.buttonSearchC.Location = new System.Drawing.Point(320, 168);
//            this.buttonSearchC.Name = "buttonSearchC";
//            this.buttonSearchC.TabIndex = 89;
//            this.buttonSearchC.Text = "SEARCH";
//            this.buttonSearchC.Click += new System.EventHandler(this.buttonSearchC_Click);
//            // 
//            // label72
//            // 
//            this.label72.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
//            this.label72.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.label72.Location = new System.Drawing.Point(16, 176);
//            this.label72.Name = "label72";
//            this.label72.Size = new System.Drawing.Size(296, 23);
//            this.label72.TabIndex = 69;
//            this.label72.Text = "C.   OWNER NAME SEARCH (try last only):";
//            // 
//            // buttonSearchB
//            // 
//            this.buttonSearchB.FlatStyle = System.Windows.Forms.FlatStyle.System;
//            this.buttonSearchB.ForeColor = System.Drawing.SystemColors.ControlText;
//            this.buttonSearchB.Location = new System.Drawing.Point(320, 96);
//            this.buttonSearchB.Name = "buttonSearchB";
//            this.buttonSearchB.TabIndex = 79;
//            this.buttonSearchB.Text = "SEARCH";
//            this.buttonSearchB.Click += new System.EventHandler(this.buttonSearchB_Click);
//            // 
//            // label73
//            // 
//            this.label73.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
//            this.label73.Location = new System.Drawing.Point(16, 104);
//            this.label73.Name = "label73";
//            this.label73.Size = new System.Drawing.Size(232, 23);
//            this.label73.TabIndex = 67;
//            this.label73.Text = "B.   PARCEL NUMBER SEARCH:";
//            // 
//            // textBoxUnitNoA
//            // 
//            this.textBoxUnitNoA.Location = new System.Drawing.Point(368, 64);
//            this.textBoxUnitNoA.Name = "textBoxUnitNoA";
//            this.textBoxUnitNoA.Size = new System.Drawing.Size(56, 22);
//            this.textBoxUnitNoA.TabIndex = 63;
//            this.textBoxUnitNoA.Text = "";
//            // 
//            // textBoxSuffixA
//            // 
//            this.textBoxSuffixA.BackColor = System.Drawing.Color.LightGray;
//            this.textBoxSuffixA.Location = new System.Drawing.Point(312, 64);
//            this.textBoxSuffixA.Name = "textBoxSuffixA";
//            this.textBoxSuffixA.Size = new System.Drawing.Size(48, 22);
//            this.textBoxSuffixA.TabIndex = 62;
//            this.textBoxSuffixA.TabStop = false;
//            this.textBoxSuffixA.Text = "";
//            // 
//            // textBoxPostDirA
//            // 
//            this.textBoxPostDirA.BackColor = System.Drawing.Color.LightGray;
//            this.textBoxPostDirA.Location = new System.Drawing.Point(256, 64);
//            this.textBoxPostDirA.Name = "textBoxPostDirA";
//            this.textBoxPostDirA.Size = new System.Drawing.Size(48, 22);
//            this.textBoxPostDirA.TabIndex = 61;
//            this.textBoxPostDirA.TabStop = false;
//            this.textBoxPostDirA.Text = "";
//            // 
//            // textBoxStreetA
//            // 
//            this.textBoxStreetA.Location = new System.Drawing.Point(160, 64);
//            this.textBoxStreetA.Name = "textBoxStreetA";
//            this.textBoxStreetA.Size = new System.Drawing.Size(88, 22);
//            this.textBoxStreetA.TabIndex = 60;
//            this.textBoxStreetA.Text = "";
//            // 
//            // textBoxPreDirA
//            // 
//            this.textBoxPreDirA.BackColor = System.Drawing.Color.LightGray;
//            this.textBoxPreDirA.Location = new System.Drawing.Point(104, 64);
//            this.textBoxPreDirA.Name = "textBoxPreDirA";
//            this.textBoxPreDirA.Size = new System.Drawing.Size(48, 22);
//            this.textBoxPreDirA.TabIndex = 59;
//            this.textBoxPreDirA.TabStop = false;
//            this.textBoxPreDirA.Text = "";
//            // 
//            // textBoxStreetNoA
//            // 
//            this.textBoxStreetNoA.Location = new System.Drawing.Point(32, 64);
//            this.textBoxStreetNoA.Name = "textBoxStreetNoA";
//            this.textBoxStreetNoA.Size = new System.Drawing.Size(64, 22);
//            this.textBoxStreetNoA.TabIndex = 58;
//            this.textBoxStreetNoA.Text = "";
//            // 
//            // label77
//            // 
//            this.label77.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.label77.Location = new System.Drawing.Point(368, 48);
//            this.label77.Name = "label77";
//            this.label77.Size = new System.Drawing.Size(64, 23);
//            this.label77.TabIndex = 54;
//            this.label77.Text = "Unit No.";
//            // 
//            // label78
//            // 
//            this.label78.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.label78.Location = new System.Drawing.Point(312, 48);
//            this.label78.Name = "label78";
//            this.label78.Size = new System.Drawing.Size(64, 23);
//            this.label78.TabIndex = 53;
//            this.label78.Text = "Suffix";
//            // 
//            // label79
//            // 
//            this.label79.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.label79.Location = new System.Drawing.Point(256, 48);
//            this.label79.Name = "label79";
//            this.label79.Size = new System.Drawing.Size(56, 23);
//            this.label79.TabIndex = 52;
//            this.label79.Text = "Post-dir";
//            // 
//            // label80
//            // 
//            this.label80.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.label80.Location = new System.Drawing.Point(160, 48);
//            this.label80.Name = "label80";
//            this.label80.Size = new System.Drawing.Size(80, 23);
//            this.label80.TabIndex = 51;
//            this.label80.Text = "Street Name";
//            // 
//            // label81
//            // 
//            this.label81.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.label81.Location = new System.Drawing.Point(104, 48);
//            this.label81.Name = "label81";
//            this.label81.Size = new System.Drawing.Size(48, 23);
//            this.label81.TabIndex = 50;
//            this.label81.Text = "Pre-dir";
//            // 
//            // label82
//            // 
//            this.label82.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.label82.Location = new System.Drawing.Point(32, 48);
//            this.label82.Name = "label82";
//            this.label82.Size = new System.Drawing.Size(72, 23);
//            this.label82.TabIndex = 49;
//            this.label82.Text = "Street No.";
//            // 
//            // buttonSearchA
//            // 
//            this.buttonSearchA.FlatStyle = System.Windows.Forms.FlatStyle.System;
//            this.buttonSearchA.ForeColor = System.Drawing.SystemColors.ControlText;
//            this.buttonSearchA.Location = new System.Drawing.Point(320, 24);
//            this.buttonSearchA.Name = "buttonSearchA";
//            this.buttonSearchA.TabIndex = 67;
//            this.buttonSearchA.Text = "SEARCH";
//            this.buttonSearchA.Click += new System.EventHandler(this.buttonSearchA_Click);
//            // 
//            // label83
//            // 
//            this.label83.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
//            this.label83.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.label83.Location = new System.Drawing.Point(16, 24);
//            this.label83.Name = "label83";
//            this.label83.Size = new System.Drawing.Size(264, 23);
//            this.label83.TabIndex = 47;
//            this.label83.Text = "A.   ADDRESS SEARCH (preferred):";
//            // 
//            // label84
//            // 
//            this.label84.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
//            this.label84.Location = new System.Drawing.Point(8, 0);
//            this.label84.Name = "label84";
//            this.label84.Size = new System.Drawing.Size(296, 23);
//            this.label84.TabIndex = 46;
//            this.label84.Text = "Enter your SEARCH PARAMETERS:";
//            // 
//            // label63
//            // 
//            this.label63.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.label63.Location = new System.Drawing.Point(456, 192);
//            this.label63.Name = "label63";
//            this.label63.Size = new System.Drawing.Size(128, 23);
//            this.label63.TabIndex = 83;
//            this.label63.Text = "State (i.e. CA)";
//            // 
//            // labelLine
//            // 
//            this.labelLine.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.labelLine.Location = new System.Drawing.Point(8, 248);
//            this.labelLine.Name = "labelLine";
//            this.labelLine.Size = new System.Drawing.Size(848, 16);
//            this.labelLine.TabIndex = 131;
//            this.labelLine.Text = "_______________________________________SEARCH RESULT_____________________________" +
//                "__________";
//            this.labelLine.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
//            // 
//            // panel2
//            // 
//            this.panel2.Controls.Add(this.textBoxMapReferenceOne);
//            this.panel2.Controls.Add(this.textBoxCounty);
//            this.panel2.Controls.Add(this.textBoxStreetnName);
//            this.panel2.Controls.Add(this.labelStreetnName);
//            this.panel2.Controls.Add(this.labelCounty);
//            this.panel2.Controls.Add(this.labelSiteAddress);
//            this.panel2.Controls.Add(this.labelMapReferenceOne);
//            this.panel2.Controls.Add(this.textBoxMailingZipCode);
//            this.panel2.Controls.Add(this.labelMailingZipCode);
//            this.panel2.Controls.Add(this.textBoxMailingCityAndState);
//            this.panel2.Controls.Add(this.labelMailingCityAndState);
//            this.panel2.Controls.Add(this.textBoxMailingAddress);
//            this.panel2.Controls.Add(this.labelMailingAddress);
//            this.panel2.Controls.Add(this.textBoxMapReferenceTwo);
//            this.panel2.Controls.Add(this.labelMapReferenceTwo);
//            this.panel2.Controls.Add(this.textBoxZipCode);
//            this.panel2.Controls.Add(this.labelZipCode);
//            this.panel2.Controls.Add(this.textBoxState);
//            this.panel2.Controls.Add(this.labelState);
//            this.panel2.Controls.Add(this.textBoxCity);
//            this.panel2.Controls.Add(this.labelCity);
//            this.panel2.Controls.Add(this.textBoxPhoneNumber);
//            this.panel2.Controls.Add(this.labelPhoneNumber);
//            this.panel2.Controls.Add(this.textBoxOwnerName);
//            this.panel2.Controls.Add(this.labelOwnerName);
//            this.panel2.Controls.Add(this.labelOwnerNameGroup);
//            this.panel2.Controls.Add(this.textBoxSchoolDistrict);
//            this.panel2.Controls.Add(this.textBoxLotNumber);
//            this.panel2.Controls.Add(this.textBoxSubdivision);
//            this.panel2.Controls.Add(this.labelSchoolDistrict);
//            this.panel2.Controls.Add(this.labelLotNumber);
//            this.panel2.Controls.Add(this.labelSubdivision);
//            this.panel2.Location = new System.Drawing.Point(0, 272);
//            this.panel2.Name = "panel2";
//            this.panel2.Size = new System.Drawing.Size(872, 192);
//            this.panel2.TabIndex = 132;
//            // 
//            // labelCounty
//            // 
//            this.labelCounty.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.labelCounty.Location = new System.Drawing.Point(512, 88);
//            this.labelCounty.Name = "labelCounty";
//            this.labelCounty.Size = new System.Drawing.Size(64, 23);
//            this.labelCounty.TabIndex = 0;
//            this.labelCounty.Text = "County";
//            // 
//            // labelSiteAddress
//            // 
//            this.labelSiteAddress.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
//            this.labelSiteAddress.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.labelSiteAddress.Location = new System.Drawing.Point(8, 72);
//            this.labelSiteAddress.Name = "labelSiteAddress";
//            this.labelSiteAddress.TabIndex = 45;
//            this.labelSiteAddress.Text = "SITE INFO";
//            // 
//            // panel3
//            // 
//            this.panel3.Controls.Add(this.textBoxDistanceFromSubject);
//            this.panel3.Controls.Add(this.textBoxComparableNumber);
//            this.panel3.Controls.Add(this.labelComparableNumber);
//            this.panel3.Controls.Add(this.labelDistanceFromSubject);
//            this.panel3.Location = new System.Drawing.Point(312, 464);
//            this.panel3.Name = "panel3";
//            this.panel3.Size = new System.Drawing.Size(336, 56);
//            this.panel3.TabIndex = 133;
//            // 
//            // tabControlOnlinePropertyProfileNComps
//            // 
//            this.tabControlOnlinePropertyProfileNComps.Controls.Add(this.tabPageOwnershipNLocations);
//            this.tabControlOnlinePropertyProfileNComps.Controls.Add(this.tabPagePropertyCharacteristics);
//            this.tabControlOnlinePropertyProfileNComps.Controls.Add(this.tabPageFinancials);
//            this.tabControlOnlinePropertyProfileNComps.Controls.Add(this.tabPageTaxesNLegal);
//            this.tabControlOnlinePropertyProfileNComps.Dock = System.Windows.Forms.DockStyle.Fill;
//            this.tabControlOnlinePropertyProfileNComps.ItemSize = new System.Drawing.Size(128, 18);
//            this.tabControlOnlinePropertyProfileNComps.Location = new System.Drawing.Point(0, 0);
//            this.tabControlOnlinePropertyProfileNComps.Name = "tabControlOnlinePropertyProfileNComps";
//            this.tabControlOnlinePropertyProfileNComps.SelectedIndex = 0;
//            this.tabControlOnlinePropertyProfileNComps.Size = new System.Drawing.Size(910, 554);
//            this.tabControlOnlinePropertyProfileNComps.TabIndex = 0;
//            // 
//            // tabPageFinancials
//            // 
//            this.tabPageFinancials.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(210)), ((System.Byte)(216)), ((System.Byte)(240)));
//            this.tabPageFinancials.Controls.Add(this.textBoxPSSaleDate);
//            this.tabPageFinancials.Controls.Add(this.label37);
//            this.tabPageFinancials.Controls.Add(this.pictureBoxFinancials);
//            this.tabPageFinancials.Controls.Add(this.textBoxSellerName);
//            this.tabPageFinancials.Controls.Add(this.labelSellerName);
//            this.tabPageFinancials.Controls.Add(this.textBoxSaleType2);
//            this.tabPageFinancials.Controls.Add(this.label15);
//            this.tabPageFinancials.Controls.Add(this.textBoxSaleType1);
//            this.tabPageFinancials.Controls.Add(this.labelSaleType1);
//            this.tabPageFinancials.Controls.Add(this.textBoxSaleDate);
//            this.tabPageFinancials.Controls.Add(this.labelSaleDate);
//            this.tabPageFinancials.Controls.Add(this.label50);
//            this.tabPageFinancials.Controls.Add(this.textBoxPSMInterestRateType);
//            this.tabPageFinancials.Controls.Add(this.label51);
//            this.tabPageFinancials.Controls.Add(this.textBoxPSMInterestRateTerm);
//            this.tabPageFinancials.Controls.Add(this.label52);
//            this.tabPageFinancials.Controls.Add(this.textBoxPSMInterestRate);
//            this.tabPageFinancials.Controls.Add(this.label53);
//            this.tabPageFinancials.Controls.Add(this.textBoxPSMMortgageAmount);
//            this.tabPageFinancials.Controls.Add(this.label54);
//            this.tabPageFinancials.Controls.Add(this.label44);
//            this.tabPageFinancials.Controls.Add(this.textBoxPFMLender);
//            this.tabPageFinancials.Controls.Add(this.label45);
//            this.tabPageFinancials.Controls.Add(this.textBoxPFMInterestRateType);
//            this.tabPageFinancials.Controls.Add(this.label46);
//            this.tabPageFinancials.Controls.Add(this.textBoxPFMInterestRateTerm);
//            this.tabPageFinancials.Controls.Add(this.label47);
//            this.tabPageFinancials.Controls.Add(this.textBoxPFMInterestRate);
//            this.tabPageFinancials.Controls.Add(this.label48);
//            this.tabPageFinancials.Controls.Add(this.textBoxPFMMortgageAmount);
//            this.tabPageFinancials.Controls.Add(this.label49);
//            this.tabPageFinancials.Controls.Add(this.label42);
//            this.tabPageFinancials.Controls.Add(this.textBoxPSStampAmt);
//            this.tabPageFinancials.Controls.Add(this.label43);
//            this.tabPageFinancials.Controls.Add(this.textBoxPSDocumentNumber);
//            this.tabPageFinancials.Controls.Add(this.label41);
//            this.tabPageFinancials.Controls.Add(this.textBoxPSRecordingDate);
//            this.tabPageFinancials.Controls.Add(this.label40);
//            this.tabPageFinancials.Controls.Add(this.textBoxPSDeedType);
//            this.tabPageFinancials.Controls.Add(this.label39);
//            this.tabPageFinancials.Controls.Add(this.textBoxPSSaleType1);
//            this.tabPageFinancials.Controls.Add(this.label38);
//            this.tabPageFinancials.Controls.Add(this.label35);
//            this.tabPageFinancials.Controls.Add(this.textBoxPSSalePrice);
//            this.tabPageFinancials.Controls.Add(this.label36);
//            this.tabPageFinancials.Controls.Add(this.label19);
//            this.tabPageFinancials.Controls.Add(this.textBoxSecInterestRateType);
//            this.tabPageFinancials.Controls.Add(this.label30);
//            this.tabPageFinancials.Controls.Add(this.textBoxSecInterestRateTerm);
//            this.tabPageFinancials.Controls.Add(this.label31);
//            this.tabPageFinancials.Controls.Add(this.textBoxSecInterestRate);
//            this.tabPageFinancials.Controls.Add(this.label32);
//            this.tabPageFinancials.Controls.Add(this.textBoxSecMortgageAmount);
//            this.tabPageFinancials.Controls.Add(this.label33);
//            this.tabPageFinancials.Controls.Add(this.label29);
//            this.tabPageFinancials.Controls.Add(this.label24);
//            this.tabPageFinancials.Controls.Add(this.textBoxStampAmt);
//            this.tabPageFinancials.Controls.Add(this.textBoxTitleCompany);
//            this.tabPageFinancials.Controls.Add(this.label23);
//            this.tabPageFinancials.Controls.Add(this.label22);
//            this.tabPageFinancials.Controls.Add(this.label21);
//            this.tabPageFinancials.Controls.Add(this.label18);
//            this.tabPageFinancials.Controls.Add(this.label17);
//            this.tabPageFinancials.Controls.Add(this.label16);
//            this.tabPageFinancials.Controls.Add(this.textBox4PricePerSqFt);
//            this.tabPageFinancials.Controls.Add(this.labelPricePerSqFt);
//            this.tabPageFinancials.Controls.Add(this.textBoxCashDown);
//            this.tabPageFinancials.Controls.Add(this.labelCashDown);
//            this.tabPageFinancials.Controls.Add(this.textBoxFirstMortgageLender);
//            this.tabPageFinancials.Controls.Add(this.labelFirstMortgageLender);
//            this.tabPageFinancials.Controls.Add(this.textBoxType);
//            this.tabPageFinancials.Controls.Add(this.labelFirstMortgageInterestRateType);
//            this.tabPageFinancials.Controls.Add(this.textBoxFirstMortgageInterestRateTerm);
//            this.tabPageFinancials.Controls.Add(this.labelFirstMortgageInterestRateTerm);
//            this.tabPageFinancials.Controls.Add(this.textBoxFirstMortgageInterestRate);
//            this.tabPageFinancials.Controls.Add(this.labelFirstMortgageInterestRate);
//            this.tabPageFinancials.Controls.Add(this.textBoxFirstMortgageAmount);
//            this.tabPageFinancials.Controls.Add(this.labelFirstMortgageAmount);
//            this.tabPageFinancials.Controls.Add(this.textBoxRecordingDate);
//            this.tabPageFinancials.Controls.Add(this.labelRecordingDate);
//            this.tabPageFinancials.Controls.Add(this.textBoxDocumentNumber);
//            this.tabPageFinancials.Controls.Add(this.labelDocumentNumber);
//            this.tabPageFinancials.Controls.Add(this.textBoxDeedType);
//            this.tabPageFinancials.Controls.Add(this.labelDeedType);
//            this.tabPageFinancials.Controls.Add(this.textBoxSalePrice);
//            this.tabPageFinancials.Controls.Add(this.labelSalePrice);
//            this.tabPageFinancials.Controls.Add(this.label20);
//            this.tabPageFinancials.Controls.Add(this.label28);
//            this.tabPageFinancials.Controls.Add(this.label27);
//            this.tabPageFinancials.Controls.Add(this.label26);
//            this.tabPageFinancials.Controls.Add(this.label25);
//            this.tabPageFinancials.Controls.Add(this.label34);
//            this.tabPageFinancials.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.tabPageFinancials.Location = new System.Drawing.Point(4, 22);
//            this.tabPageFinancials.Name = "tabPageFinancials";
//            this.tabPageFinancials.Size = new System.Drawing.Size(902, 528);
//            this.tabPageFinancials.TabIndex = 3;
//            this.tabPageFinancials.Text = "Financials";
//            this.tabPageFinancials.Click += new System.EventHandler(this.tabPageFinancials_Click);
//            // 
//            // textBoxPSSaleDate
//            // 
//            this.textBoxPSSaleDate.Location = new System.Drawing.Point(128, 296);
//            this.textBoxPSSaleDate.Name = "textBoxPSSaleDate";
//            this.textBoxPSSaleDate.Size = new System.Drawing.Size(72, 22);
//            this.textBoxPSSaleDate.TabIndex = 132;
//            this.textBoxPSSaleDate.Text = "";
//            // 
//            // label37
//            // 
//            this.label37.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
//            this.label37.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.label37.Location = new System.Drawing.Point(128, 280);
//            this.label37.Name = "label37";
//            this.label37.Size = new System.Drawing.Size(72, 24);
//            this.label37.TabIndex = 131;
//            this.label37.Text = "Sale Date";
//            // 
//            // pictureBoxFinancials
//            // 
//            this.pictureBoxFinancials.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
//            this.pictureBoxFinancials.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxFinancials.Image")));
//            this.pictureBoxFinancials.Location = new System.Drawing.Point(878, 8);
//            this.pictureBoxFinancials.Name = "pictureBoxFinancials";
//            this.pictureBoxFinancials.Size = new System.Drawing.Size(16, 16);
//            this.pictureBoxFinancials.TabIndex = 168;
//            this.pictureBoxFinancials.TabStop = false;
//            // 
//            // textBoxSellerName
//            // 
//            this.textBoxSellerName.Location = new System.Drawing.Point(672, 48);
//            this.textBoxSellerName.Name = "textBoxSellerName";
//            this.textBoxSellerName.Size = new System.Drawing.Size(128, 22);
//            this.textBoxSellerName.TabIndex = 94;
//            this.textBoxSellerName.Text = "";
//            // 
//            // labelSellerName
//            // 
//            this.labelSellerName.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.labelSellerName.Location = new System.Drawing.Point(672, 32);
//            this.labelSellerName.Name = "labelSellerName";
//            this.labelSellerName.Size = new System.Drawing.Size(88, 24);
//            this.labelSellerName.TabIndex = 93;
//            this.labelSellerName.Text = "Seller Name";
//            // 
//            // textBoxSaleType2
//            // 
//            this.textBoxSaleType2.Location = new System.Drawing.Point(560, 48);
//            this.textBoxSaleType2.Name = "textBoxSaleType2";
//            this.textBoxSaleType2.Size = new System.Drawing.Size(96, 22);
//            this.textBoxSaleType2.TabIndex = 104;
//            this.textBoxSaleType2.Text = "";
//            // 
//            // label15
//            // 
//            this.label15.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.label15.Location = new System.Drawing.Point(560, 32);
//            this.label15.Name = "label15";
//            this.label15.Size = new System.Drawing.Size(88, 24);
//            this.label15.TabIndex = 103;
//            this.label15.Text = "Sale Type (2)";
//            // 
//            // textBoxSaleType1
//            // 
//            this.textBoxSaleType1.Location = new System.Drawing.Point(456, 48);
//            this.textBoxSaleType1.Name = "textBoxSaleType1";
//            this.textBoxSaleType1.Size = new System.Drawing.Size(88, 22);
//            this.textBoxSaleType1.TabIndex = 60;
//            this.textBoxSaleType1.Text = "";
//            // 
//            // labelSaleType1
//            // 
//            this.labelSaleType1.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.labelSaleType1.Location = new System.Drawing.Point(456, 32);
//            this.labelSaleType1.Name = "labelSaleType1";
//            this.labelSaleType1.Size = new System.Drawing.Size(88, 24);
//            this.labelSaleType1.TabIndex = 59;
//            this.labelSaleType1.Text = "Sale Type (1)";
//            // 
//            // textBoxSaleDate
//            // 
//            this.textBoxSaleDate.Location = new System.Drawing.Point(136, 48);
//            this.textBoxSaleDate.Name = "textBoxSaleDate";
//            this.textBoxSaleDate.Size = new System.Drawing.Size(80, 22);
//            this.textBoxSaleDate.TabIndex = 56;
//            this.textBoxSaleDate.Text = "";
//            // 
//            // labelSaleDate
//            // 
//            this.labelSaleDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
//            this.labelSaleDate.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.labelSaleDate.Location = new System.Drawing.Point(136, 32);
//            this.labelSaleDate.Name = "labelSaleDate";
//            this.labelSaleDate.Size = new System.Drawing.Size(72, 24);
//            this.labelSaleDate.TabIndex = 55;
//            this.labelSaleDate.Text = "Sale Date";
//            // 
//            // label50
//            // 
//            this.label50.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.label50.Location = new System.Drawing.Point(224, 424);
//            this.label50.Name = "label50";
//            this.label50.Size = new System.Drawing.Size(16, 24);
//            this.label50.TabIndex = 167;
//            this.label50.Text = "%";
//            this.label50.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
//            // 
//            // textBoxPSMInterestRateType
//            // 
//            this.textBoxPSMInterestRateType.Location = new System.Drawing.Point(384, 424);
//            this.textBoxPSMInterestRateType.Name = "textBoxPSMInterestRateType";
//            this.textBoxPSMInterestRateType.Size = new System.Drawing.Size(60, 22);
//            this.textBoxPSMInterestRateType.TabIndex = 166;
//            this.textBoxPSMInterestRateType.Text = "";
//            // 
//            // label51
//            // 
//            this.label51.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.label51.Location = new System.Drawing.Point(384, 408);
//            this.label51.Name = "label51";
//            this.label51.Size = new System.Drawing.Size(120, 24);
//            this.label51.TabIndex = 165;
//            this.label51.Text = "Interest Rate Type";
//            // 
//            // textBoxPSMInterestRateTerm
//            // 
//            this.textBoxPSMInterestRateTerm.Location = new System.Drawing.Point(256, 424);
//            this.textBoxPSMInterestRateTerm.Name = "textBoxPSMInterestRateTerm";
//            this.textBoxPSMInterestRateTerm.Size = new System.Drawing.Size(60, 22);
//            this.textBoxPSMInterestRateTerm.TabIndex = 164;
//            this.textBoxPSMInterestRateTerm.Text = "";
//            // 
//            // label52
//            // 
//            this.label52.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.label52.Location = new System.Drawing.Point(256, 408);
//            this.label52.Name = "label52";
//            this.label52.Size = new System.Drawing.Size(120, 24);
//            this.label52.TabIndex = 163;
//            this.label52.Text = "Interest Rate Term";
//            // 
//            // textBoxPSMInterestRate
//            // 
//            this.textBoxPSMInterestRate.Location = new System.Drawing.Point(160, 424);
//            this.textBoxPSMInterestRate.Name = "textBoxPSMInterestRate";
//            this.textBoxPSMInterestRate.Size = new System.Drawing.Size(64, 22);
//            this.textBoxPSMInterestRate.TabIndex = 162;
//            this.textBoxPSMInterestRate.Text = "";
//            // 
//            // label53
//            // 
//            this.label53.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.label53.Location = new System.Drawing.Point(160, 408);
//            this.label53.Name = "label53";
//            this.label53.Size = new System.Drawing.Size(112, 24);
//            this.label53.TabIndex = 161;
//            this.label53.Text = "Interest Rate";
//            // 
//            // textBoxPSMMortgageAmount
//            // 
//            this.textBoxPSMMortgageAmount.Location = new System.Drawing.Point(40, 424);
//            this.textBoxPSMMortgageAmount.Name = "textBoxPSMMortgageAmount";
//            this.textBoxPSMMortgageAmount.Size = new System.Drawing.Size(104, 22);
//            this.textBoxPSMMortgageAmount.TabIndex = 160;
//            this.textBoxPSMMortgageAmount.Text = "";
//            // 
//            // label54
//            // 
//            this.label54.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.label54.Location = new System.Drawing.Point(40, 408);
//            this.label54.Name = "label54";
//            this.label54.Size = new System.Drawing.Size(112, 24);
//            this.label54.TabIndex = 159;
//            this.label54.Text = "Mortgage Amount";
//            // 
//            // label44
//            // 
//            this.label44.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.label44.Location = new System.Drawing.Point(224, 360);
//            this.label44.Name = "label44";
//            this.label44.Size = new System.Drawing.Size(16, 24);
//            this.label44.TabIndex = 157;
//            this.label44.Text = "%";
//            this.label44.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
//            // 
//            // textBoxPFMLender
//            // 
//            this.textBoxPFMLender.Location = new System.Drawing.Point(504, 360);
//            this.textBoxPFMLender.Name = "textBoxPFMLender";
//            this.textBoxPFMLender.Size = new System.Drawing.Size(144, 22);
//            this.textBoxPFMLender.TabIndex = 156;
//            this.textBoxPFMLender.Text = "";
//            // 
//            // label45
//            // 
//            this.label45.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.label45.Location = new System.Drawing.Point(504, 344);
//            this.label45.Name = "label45";
//            this.label45.Size = new System.Drawing.Size(56, 24);
//            this.label45.TabIndex = 155;
//            this.label45.Text = "Lender";
//            // 
//            // textBoxPFMInterestRateType
//            // 
//            this.textBoxPFMInterestRateType.Location = new System.Drawing.Point(384, 360);
//            this.textBoxPFMInterestRateType.Name = "textBoxPFMInterestRateType";
//            this.textBoxPFMInterestRateType.Size = new System.Drawing.Size(60, 22);
//            this.textBoxPFMInterestRateType.TabIndex = 154;
//            this.textBoxPFMInterestRateType.Text = "";
//            // 
//            // label46
//            // 
//            this.label46.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.label46.Location = new System.Drawing.Point(384, 344);
//            this.label46.Name = "label46";
//            this.label46.Size = new System.Drawing.Size(120, 24);
//            this.label46.TabIndex = 153;
//            this.label46.Text = "Interest Rate Type";
//            // 
//            // textBoxPFMInterestRateTerm
//            // 
//            this.textBoxPFMInterestRateTerm.Location = new System.Drawing.Point(256, 360);
//            this.textBoxPFMInterestRateTerm.Name = "textBoxPFMInterestRateTerm";
//            this.textBoxPFMInterestRateTerm.Size = new System.Drawing.Size(60, 22);
//            this.textBoxPFMInterestRateTerm.TabIndex = 152;
//            this.textBoxPFMInterestRateTerm.Text = "";
//            // 
//            // label47
//            // 
//            this.label47.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.label47.Location = new System.Drawing.Point(256, 344);
//            this.label47.Name = "label47";
//            this.label47.Size = new System.Drawing.Size(120, 24);
//            this.label47.TabIndex = 151;
//            this.label47.Text = "Interest Rate Term";
//            // 
//            // textBoxPFMInterestRate
//            // 
//            this.textBoxPFMInterestRate.Location = new System.Drawing.Point(160, 360);
//            this.textBoxPFMInterestRate.Name = "textBoxPFMInterestRate";
//            this.textBoxPFMInterestRate.Size = new System.Drawing.Size(64, 22);
//            this.textBoxPFMInterestRate.TabIndex = 150;
//            this.textBoxPFMInterestRate.Text = "";
//            // 
//            // label48
//            // 
//            this.label48.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.label48.Location = new System.Drawing.Point(160, 344);
//            this.label48.Name = "label48";
//            this.label48.Size = new System.Drawing.Size(112, 24);
//            this.label48.TabIndex = 149;
//            this.label48.Text = "Interest Rate";
//            // 
//            // textBoxPFMMortgageAmount
//            // 
//            this.textBoxPFMMortgageAmount.Location = new System.Drawing.Point(40, 360);
//            this.textBoxPFMMortgageAmount.Name = "textBoxPFMMortgageAmount";
//            this.textBoxPFMMortgageAmount.Size = new System.Drawing.Size(104, 22);
//            this.textBoxPFMMortgageAmount.TabIndex = 148;
//            this.textBoxPFMMortgageAmount.Text = "";
//            // 
//            // label49
//            // 
//            this.label49.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.label49.Location = new System.Drawing.Point(40, 344);
//            this.label49.Name = "label49";
//            this.label49.Size = new System.Drawing.Size(112, 24);
//            this.label49.TabIndex = 147;
//            this.label49.Text = "Mortgage Amount";
//            // 
//            // label42
//            // 
//            this.label42.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.label42.Location = new System.Drawing.Point(688, 296);
//            this.label42.Name = "label42";
//            this.label42.Size = new System.Drawing.Size(16, 24);
//            this.label42.TabIndex = 146;
//            this.label42.Text = "$";
//            this.label42.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
//            // 
//            // textBoxPSStampAmt
//            // 
//            this.textBoxPSStampAmt.Location = new System.Drawing.Point(704, 296);
//            this.textBoxPSStampAmt.Name = "textBoxPSStampAmt";
//            this.textBoxPSStampAmt.Size = new System.Drawing.Size(64, 22);
//            this.textBoxPSStampAmt.TabIndex = 144;
//            this.textBoxPSStampAmt.Text = "";
//            // 
//            // label43
//            // 
//            this.label43.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.label43.Location = new System.Drawing.Point(704, 280);
//            this.label43.Name = "label43";
//            this.label43.Size = new System.Drawing.Size(72, 23);
//            this.label43.TabIndex = 145;
//            this.label43.Text = "Stamp Amt";
//            // 
//            // textBoxPSDocumentNumber
//            // 
//            this.textBoxPSDocumentNumber.Location = new System.Drawing.Point(568, 296);
//            this.textBoxPSDocumentNumber.Name = "textBoxPSDocumentNumber";
//            this.textBoxPSDocumentNumber.Size = new System.Drawing.Size(112, 22);
//            this.textBoxPSDocumentNumber.TabIndex = 143;
//            this.textBoxPSDocumentNumber.Text = "";
//            // 
//            // label41
//            // 
//            this.label41.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.label41.Location = new System.Drawing.Point(568, 280);
//            this.label41.Name = "label41";
//            this.label41.Size = new System.Drawing.Size(120, 24);
//            this.label41.TabIndex = 142;
//            this.label41.Text = "Document Number";
//            // 
//            // textBoxPSRecordingDate
//            // 
//            this.textBoxPSRecordingDate.Location = new System.Drawing.Point(456, 296);
//            this.textBoxPSRecordingDate.Name = "textBoxPSRecordingDate";
//            this.textBoxPSRecordingDate.Size = new System.Drawing.Size(96, 22);
//            this.textBoxPSRecordingDate.TabIndex = 141;
//            this.textBoxPSRecordingDate.Text = "";
//            // 
//            // label40
//            // 
//            this.label40.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.label40.Location = new System.Drawing.Point(456, 280);
//            this.label40.Name = "label40";
//            this.label40.Size = new System.Drawing.Size(104, 24);
//            this.label40.TabIndex = 140;
//            this.label40.Text = "Recording Date";
//            // 
//            // textBoxPSDeedType
//            // 
//            this.textBoxPSDeedType.Location = new System.Drawing.Point(296, 296);
//            this.textBoxPSDeedType.Name = "textBoxPSDeedType";
//            this.textBoxPSDeedType.Size = new System.Drawing.Size(152, 22);
//            this.textBoxPSDeedType.TabIndex = 139;
//            this.textBoxPSDeedType.Text = "";
//            // 
//            // label39
//            // 
//            this.label39.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.label39.Location = new System.Drawing.Point(296, 280);
//            this.label39.Name = "label39";
//            this.label39.Size = new System.Drawing.Size(72, 24);
//            this.label39.TabIndex = 138;
//            this.label39.Text = "Deed Type";
//            // 
//            // textBoxPSSaleType1
//            // 
//            this.textBoxPSSaleType1.Location = new System.Drawing.Point(208, 296);
//            this.textBoxPSSaleType1.Name = "textBoxPSSaleType1";
//            this.textBoxPSSaleType1.Size = new System.Drawing.Size(80, 22);
//            this.textBoxPSSaleType1.TabIndex = 137;
//            this.textBoxPSSaleType1.Text = "";
//            // 
//            // label38
//            // 
//            this.label38.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.label38.Location = new System.Drawing.Point(208, 280);
//            this.label38.Name = "label38";
//            this.label38.Size = new System.Drawing.Size(88, 24);
//            this.label38.TabIndex = 136;
//            this.label38.Text = "Sale Type (1)";
//            // 
//            // label35
//            // 
//            this.label35.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.label35.Location = new System.Drawing.Point(24, 296);
//            this.label35.Name = "label35";
//            this.label35.Size = new System.Drawing.Size(16, 24);
//            this.label35.TabIndex = 135;
//            this.label35.Text = "$";
//            this.label35.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
//            // 
//            // textBoxPSSalePrice
//            // 
//            this.textBoxPSSalePrice.Location = new System.Drawing.Point(40, 296);
//            this.textBoxPSSalePrice.Name = "textBoxPSSalePrice";
//            this.textBoxPSSalePrice.Size = new System.Drawing.Size(80, 22);
//            this.textBoxPSSalePrice.TabIndex = 134;
//            this.textBoxPSSalePrice.Text = "";
//            // 
//            // label36
//            // 
//            this.label36.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
//            this.label36.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.label36.Location = new System.Drawing.Point(40, 280);
//            this.label36.Name = "label36";
//            this.label36.Size = new System.Drawing.Size(80, 24);
//            this.label36.TabIndex = 133;
//            this.label36.Text = "Sale Price";
//            // 
//            // label19
//            // 
//            this.label19.Location = new System.Drawing.Point(224, 214);
//            this.label19.Name = "label19";
//            this.label19.Size = new System.Drawing.Size(16, 24);
//            this.label19.TabIndex = 129;
//            this.label19.Text = "%";
//            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
//            // 
//            // textBoxSecInterestRateType
//            // 
//            this.textBoxSecInterestRateType.Location = new System.Drawing.Point(384, 216);
//            this.textBoxSecInterestRateType.Name = "textBoxSecInterestRateType";
//            this.textBoxSecInterestRateType.Size = new System.Drawing.Size(60, 22);
//            this.textBoxSecInterestRateType.TabIndex = 128;
//            this.textBoxSecInterestRateType.Text = "";
//            // 
//            // label30
//            // 
//            this.label30.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.label30.Location = new System.Drawing.Point(384, 200);
//            this.label30.Name = "label30";
//            this.label30.Size = new System.Drawing.Size(120, 24);
//            this.label30.TabIndex = 127;
//            this.label30.Text = "Interest Rate Type";
//            // 
//            // textBoxSecInterestRateTerm
//            // 
//            this.textBoxSecInterestRateTerm.Location = new System.Drawing.Point(256, 216);
//            this.textBoxSecInterestRateTerm.Name = "textBoxSecInterestRateTerm";
//            this.textBoxSecInterestRateTerm.Size = new System.Drawing.Size(60, 22);
//            this.textBoxSecInterestRateTerm.TabIndex = 126;
//            this.textBoxSecInterestRateTerm.Text = "";
//            // 
//            // label31
//            // 
//            this.label31.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.label31.Location = new System.Drawing.Point(256, 200);
//            this.label31.Name = "label31";
//            this.label31.Size = new System.Drawing.Size(120, 24);
//            this.label31.TabIndex = 125;
//            this.label31.Text = "Interest Rate Term";
//            // 
//            // textBoxSecInterestRate
//            // 
//            this.textBoxSecInterestRate.Location = new System.Drawing.Point(160, 216);
//            this.textBoxSecInterestRate.Name = "textBoxSecInterestRate";
//            this.textBoxSecInterestRate.Size = new System.Drawing.Size(64, 22);
//            this.textBoxSecInterestRate.TabIndex = 124;
//            this.textBoxSecInterestRate.Text = "";
//            // 
//            // label32
//            // 
//            this.label32.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.label32.Location = new System.Drawing.Point(160, 200);
//            this.label32.Name = "label32";
//            this.label32.Size = new System.Drawing.Size(112, 24);
//            this.label32.TabIndex = 123;
//            this.label32.Text = "Interest Rate";
//            // 
//            // textBoxSecMortgageAmount
//            // 
//            this.textBoxSecMortgageAmount.Location = new System.Drawing.Point(40, 216);
//            this.textBoxSecMortgageAmount.Name = "textBoxSecMortgageAmount";
//            this.textBoxSecMortgageAmount.Size = new System.Drawing.Size(104, 22);
//            this.textBoxSecMortgageAmount.TabIndex = 122;
//            this.textBoxSecMortgageAmount.Text = "";
//            // 
//            // label33
//            // 
//            this.label33.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.label33.Location = new System.Drawing.Point(40, 200);
//            this.label33.Name = "label33";
//            this.label33.Size = new System.Drawing.Size(112, 24);
//            this.label33.TabIndex = 121;
//            this.label33.Text = "Mortgage Amount";
//            // 
//            // label29
//            // 
//            this.label29.Location = new System.Drawing.Point(224, 152);
//            this.label29.Name = "label29";
//            this.label29.Size = new System.Drawing.Size(16, 24);
//            this.label29.TabIndex = 120;
//            this.label29.Text = "%";
//            this.label29.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
//            // 
//            // label24
//            // 
//            this.label24.Location = new System.Drawing.Point(376, 88);
//            this.label24.Name = "label24";
//            this.label24.Size = new System.Drawing.Size(16, 24);
//            this.label24.TabIndex = 115;
//            this.label24.Text = "$";
//            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
//            // 
//            // textBoxStampAmt
//            // 
//            this.textBoxStampAmt.Location = new System.Drawing.Point(392, 88);
//            this.textBoxStampAmt.Name = "textBoxStampAmt";
//            this.textBoxStampAmt.Size = new System.Drawing.Size(104, 22);
//            this.textBoxStampAmt.TabIndex = 111;
//            this.textBoxStampAmt.Text = "";
//            // 
//            // textBoxTitleCompany
//            // 
//            this.textBoxTitleCompany.Location = new System.Drawing.Point(504, 88);
//            this.textBoxTitleCompany.Name = "textBoxTitleCompany";
//            this.textBoxTitleCompany.Size = new System.Drawing.Size(176, 22);
//            this.textBoxTitleCompany.TabIndex = 112;
//            this.textBoxTitleCompany.Text = "";
//            // 
//            // label23
//            // 
//            this.label23.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.label23.Location = new System.Drawing.Point(504, 72);
//            this.label23.Name = "label23";
//            this.label23.TabIndex = 114;
//            this.label23.Text = "Title Company";
//            // 
//            // label22
//            // 
//            this.label22.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.label22.Location = new System.Drawing.Point(392, 72);
//            this.label22.Name = "label22";
//            this.label22.TabIndex = 113;
//            this.label22.Text = "Stamp Amt";
//            // 
//            // label21
//            // 
//            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
//            this.label21.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.label21.Location = new System.Drawing.Point(16, 264);
//            this.label21.Name = "label21";
//            this.label21.TabIndex = 110;
//            this.label21.Text = "PRIOR SALE";
//            // 
//            // label18
//            // 
//            this.label18.Location = new System.Drawing.Point(24, 48);
//            this.label18.Name = "label18";
//            this.label18.Size = new System.Drawing.Size(16, 24);
//            this.label18.TabIndex = 107;
//            this.label18.Text = "$";
//            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
//            // 
//            // label17
//            // 
//            this.label17.Location = new System.Drawing.Point(216, 48);
//            this.label17.Name = "label17";
//            this.label17.Size = new System.Drawing.Size(16, 24);
//            this.label17.TabIndex = 106;
//            this.label17.Text = "$";
//            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
//            // 
//            // label16
//            // 
//            this.label16.Location = new System.Drawing.Point(328, 48);
//            this.label16.Name = "label16";
//            this.label16.Size = new System.Drawing.Size(16, 24);
//            this.label16.TabIndex = 105;
//            this.label16.Text = "$";
//            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
//            // 
//            // textBox4PricePerSqFt
//            // 
//            this.textBox4PricePerSqFt.Location = new System.Drawing.Point(344, 48);
//            this.textBox4PricePerSqFt.Name = "textBox4PricePerSqFt";
//            this.textBox4PricePerSqFt.Size = new System.Drawing.Size(96, 22);
//            this.textBox4PricePerSqFt.TabIndex = 88;
//            this.textBox4PricePerSqFt.Text = "";
//            // 
//            // labelPricePerSqFt
//            // 
//            this.labelPricePerSqFt.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.labelPricePerSqFt.Location = new System.Drawing.Point(344, 32);
//            this.labelPricePerSqFt.Name = "labelPricePerSqFt";
//            this.labelPricePerSqFt.Size = new System.Drawing.Size(104, 24);
//            this.labelPricePerSqFt.TabIndex = 87;
//            this.labelPricePerSqFt.Text = "Price per Sq.Ft.";
//            // 
//            // textBoxCashDown
//            // 
//            this.textBoxCashDown.Location = new System.Drawing.Point(232, 48);
//            this.textBoxCashDown.Name = "textBoxCashDown";
//            this.textBoxCashDown.Size = new System.Drawing.Size(88, 22);
//            this.textBoxCashDown.TabIndex = 86;
//            this.textBoxCashDown.Text = "";
//            // 
//            // labelCashDown
//            // 
//            this.labelCashDown.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
//            this.labelCashDown.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.labelCashDown.Location = new System.Drawing.Point(232, 32);
//            this.labelCashDown.Name = "labelCashDown";
//            this.labelCashDown.Size = new System.Drawing.Size(88, 24);
//            this.labelCashDown.TabIndex = 85;
//            this.labelCashDown.Text = "Cash Down";
//            // 
//            // textBoxFirstMortgageLender
//            // 
//            this.textBoxFirstMortgageLender.Location = new System.Drawing.Point(504, 152);
//            this.textBoxFirstMortgageLender.Name = "textBoxFirstMortgageLender";
//            this.textBoxFirstMortgageLender.Size = new System.Drawing.Size(176, 22);
//            this.textBoxFirstMortgageLender.TabIndex = 76;
//            this.textBoxFirstMortgageLender.Text = "";
//            // 
//            // labelFirstMortgageLender
//            // 
//            this.labelFirstMortgageLender.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.labelFirstMortgageLender.Location = new System.Drawing.Point(504, 136);
//            this.labelFirstMortgageLender.Name = "labelFirstMortgageLender";
//            this.labelFirstMortgageLender.Size = new System.Drawing.Size(56, 24);
//            this.labelFirstMortgageLender.TabIndex = 75;
//            this.labelFirstMortgageLender.Text = "Lender";
//            // 
//            // textBoxType
//            // 
//            this.textBoxType.Location = new System.Drawing.Point(384, 152);
//            this.textBoxType.Name = "textBoxType";
//            this.textBoxType.Size = new System.Drawing.Size(60, 22);
//            this.textBoxType.TabIndex = 74;
//            this.textBoxType.Text = "";
//            // 
//            // labelFirstMortgageInterestRateType
//            // 
//            this.labelFirstMortgageInterestRateType.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.labelFirstMortgageInterestRateType.Location = new System.Drawing.Point(384, 136);
//            this.labelFirstMortgageInterestRateType.Name = "labelFirstMortgageInterestRateType";
//            this.labelFirstMortgageInterestRateType.Size = new System.Drawing.Size(120, 24);
//            this.labelFirstMortgageInterestRateType.TabIndex = 73;
//            this.labelFirstMortgageInterestRateType.Text = "Interest Rate Type";
//            // 
//            // textBoxFirstMortgageInterestRateTerm
//            // 
//            this.textBoxFirstMortgageInterestRateTerm.Location = new System.Drawing.Point(256, 152);
//            this.textBoxFirstMortgageInterestRateTerm.Name = "textBoxFirstMortgageInterestRateTerm";
//            this.textBoxFirstMortgageInterestRateTerm.Size = new System.Drawing.Size(60, 22);
//            this.textBoxFirstMortgageInterestRateTerm.TabIndex = 72;
//            this.textBoxFirstMortgageInterestRateTerm.Text = "";
//            // 
//            // labelFirstMortgageInterestRateTerm
//            // 
//            this.labelFirstMortgageInterestRateTerm.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.labelFirstMortgageInterestRateTerm.Location = new System.Drawing.Point(256, 136);
//            this.labelFirstMortgageInterestRateTerm.Name = "labelFirstMortgageInterestRateTerm";
//            this.labelFirstMortgageInterestRateTerm.Size = new System.Drawing.Size(120, 24);
//            this.labelFirstMortgageInterestRateTerm.TabIndex = 71;
//            this.labelFirstMortgageInterestRateTerm.Text = "Interest Rate Term";
//            // 
//            // textBoxFirstMortgageInterestRate
//            // 
//            this.textBoxFirstMortgageInterestRate.Location = new System.Drawing.Point(160, 152);
//            this.textBoxFirstMortgageInterestRate.Name = "textBoxFirstMortgageInterestRate";
//            this.textBoxFirstMortgageInterestRate.Size = new System.Drawing.Size(64, 22);
//            this.textBoxFirstMortgageInterestRate.TabIndex = 70;
//            this.textBoxFirstMortgageInterestRate.Text = "";
//            // 
//            // labelFirstMortgageInterestRate
//            // 
//            this.labelFirstMortgageInterestRate.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.labelFirstMortgageInterestRate.Location = new System.Drawing.Point(160, 136);
//            this.labelFirstMortgageInterestRate.Name = "labelFirstMortgageInterestRate";
//            this.labelFirstMortgageInterestRate.Size = new System.Drawing.Size(88, 24);
//            this.labelFirstMortgageInterestRate.TabIndex = 69;
//            this.labelFirstMortgageInterestRate.Text = "Interest Rate";
//            // 
//            // textBoxFirstMortgageAmount
//            // 
//            this.textBoxFirstMortgageAmount.Location = new System.Drawing.Point(40, 152);
//            this.textBoxFirstMortgageAmount.Name = "textBoxFirstMortgageAmount";
//            this.textBoxFirstMortgageAmount.Size = new System.Drawing.Size(104, 22);
//            this.textBoxFirstMortgageAmount.TabIndex = 68;
//            this.textBoxFirstMortgageAmount.Text = "";
//            // 
//            // labelFirstMortgageAmount
//            // 
//            this.labelFirstMortgageAmount.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.labelFirstMortgageAmount.Location = new System.Drawing.Point(40, 136);
//            this.labelFirstMortgageAmount.Name = "labelFirstMortgageAmount";
//            this.labelFirstMortgageAmount.Size = new System.Drawing.Size(112, 24);
//            this.labelFirstMortgageAmount.TabIndex = 67;
//            this.labelFirstMortgageAmount.Text = "Mortgage Amount";
//            // 
//            // textBoxRecordingDate
//            // 
//            this.textBoxRecordingDate.Location = new System.Drawing.Point(136, 88);
//            this.textBoxRecordingDate.Name = "textBoxRecordingDate";
//            this.textBoxRecordingDate.Size = new System.Drawing.Size(104, 22);
//            this.textBoxRecordingDate.TabIndex = 66;
//            this.textBoxRecordingDate.Text = "";
//            // 
//            // labelRecordingDate
//            // 
//            this.labelRecordingDate.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.labelRecordingDate.Location = new System.Drawing.Point(136, 72);
//            this.labelRecordingDate.Name = "labelRecordingDate";
//            this.labelRecordingDate.Size = new System.Drawing.Size(104, 24);
//            this.labelRecordingDate.TabIndex = 65;
//            this.labelRecordingDate.Text = "Recording Date";
//            // 
//            // textBoxDocumentNumber
//            // 
//            this.textBoxDocumentNumber.Location = new System.Drawing.Point(248, 88);
//            this.textBoxDocumentNumber.Name = "textBoxDocumentNumber";
//            this.textBoxDocumentNumber.Size = new System.Drawing.Size(120, 22);
//            this.textBoxDocumentNumber.TabIndex = 64;
//            this.textBoxDocumentNumber.Text = "";
//            // 
//            // labelDocumentNumber
//            // 
//            this.labelDocumentNumber.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.labelDocumentNumber.Location = new System.Drawing.Point(248, 72);
//            this.labelDocumentNumber.Name = "labelDocumentNumber";
//            this.labelDocumentNumber.Size = new System.Drawing.Size(120, 24);
//            this.labelDocumentNumber.TabIndex = 63;
//            this.labelDocumentNumber.Text = "Document Number";
//            // 
//            // textBoxDeedType
//            // 
//            this.textBoxDeedType.Location = new System.Drawing.Point(40, 88);
//            this.textBoxDeedType.Name = "textBoxDeedType";
//            this.textBoxDeedType.Size = new System.Drawing.Size(80, 22);
//            this.textBoxDeedType.TabIndex = 62;
//            this.textBoxDeedType.Text = "";
//            // 
//            // labelDeedType
//            // 
//            this.labelDeedType.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.labelDeedType.Location = new System.Drawing.Point(40, 72);
//            this.labelDeedType.Name = "labelDeedType";
//            this.labelDeedType.Size = new System.Drawing.Size(72, 24);
//            this.labelDeedType.TabIndex = 61;
//            this.labelDeedType.Text = "Deed Type";
//            // 
//            // textBoxSalePrice
//            // 
//            this.textBoxSalePrice.Location = new System.Drawing.Point(40, 48);
//            this.textBoxSalePrice.Name = "textBoxSalePrice";
//            this.textBoxSalePrice.Size = new System.Drawing.Size(80, 22);
//            this.textBoxSalePrice.TabIndex = 58;
//            this.textBoxSalePrice.Text = "";
//            // 
//            // labelSalePrice
//            // 
//            this.labelSalePrice.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
//            this.labelSalePrice.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.labelSalePrice.Location = new System.Drawing.Point(40, 32);
//            this.labelSalePrice.Name = "labelSalePrice";
//            this.labelSalePrice.Size = new System.Drawing.Size(104, 24);
//            this.labelSalePrice.TabIndex = 57;
//            this.labelSalePrice.Text = "Sale Price";
//            // 
//            // label20
//            // 
//            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
//            this.label20.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.label20.Location = new System.Drawing.Point(16, 6);
//            this.label20.Name = "label20";
//            this.label20.TabIndex = 109;
//            this.label20.Text = "LAST SALE";
//            // 
//            // label28
//            // 
//            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
//            this.label28.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.label28.Location = new System.Drawing.Point(16, 392);
//            this.label28.Name = "label28";
//            this.label28.Size = new System.Drawing.Size(280, 23);
//            this.label28.TabIndex = 119;
//            this.label28.Text = "PRIOR SECOND MORTGAGE";
//            // 
//            // label27
//            // 
//            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
//            this.label27.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.label27.Location = new System.Drawing.Point(16, 328);
//            this.label27.Name = "label27";
//            this.label27.Size = new System.Drawing.Size(200, 23);
//            this.label27.TabIndex = 118;
//            this.label27.Text = "PRIOR FIRST MORGAGE";
//            // 
//            // label26
//            // 
//            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
//            this.label26.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.label26.Location = new System.Drawing.Point(16, 184);
//            this.label26.Name = "label26";
//            this.label26.Size = new System.Drawing.Size(200, 23);
//            this.label26.TabIndex = 117;
//            this.label26.Text = "SECOND MORTGAGE";
//            // 
//            // label25
//            // 
//            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
//            this.label25.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.label25.Location = new System.Drawing.Point(16, 120);
//            this.label25.Name = "label25";
//            this.label25.Size = new System.Drawing.Size(208, 23);
//            this.label25.TabIndex = 116;
//            this.label25.Text = "FIRST MORTGAGE";
//            // 
//            // label34
//            // 
//            this.label34.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.label34.Location = new System.Drawing.Point(24, 240);
//            this.label34.Name = "label34";
//            this.label34.Size = new System.Drawing.Size(816, 16);
//            this.label34.TabIndex = 130;
//            this.label34.Text = "_________________________________________________________________________________" +
//                "_________________________________________________";
//            // 
//            // tabPageTaxesNLegal
//            // 
//            this.tabPageTaxesNLegal.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(210)), ((System.Byte)(216)), ((System.Byte)(240)));
//            this.tabPageTaxesNLegal.Controls.Add(this.pictureBoxTaxesLegal);
//            this.tabPageTaxesNLegal.Controls.Add(this.label87);
//            this.tabPageTaxesNLegal.Controls.Add(this.label86);
//            this.tabPageTaxesNLegal.Controls.Add(this.textBoxCensusTract);
//            this.tabPageTaxesNLegal.Controls.Add(this.label62);
//            this.tabPageTaxesNLegal.Controls.Add(this.textBoxLegalBlockBldg);
//            this.tabPageTaxesNLegal.Controls.Add(this.label61);
//            this.tabPageTaxesNLegal.Controls.Add(this.textBoxLegalBookPage);
//            this.tabPageTaxesNLegal.Controls.Add(this.label60);
//            this.tabPageTaxesNLegal.Controls.Add(this.label59);
//            this.tabPageTaxesNLegal.Controls.Add(this.label58);
//            this.tabPageTaxesNLegal.Controls.Add(this.textBoxTownshipName);
//            this.tabPageTaxesNLegal.Controls.Add(this.labelTownshipName);
//            this.tabPageTaxesNLegal.Controls.Add(this.textBoxLegalDescription);
//            this.tabPageTaxesNLegal.Controls.Add(this.labelLegalDescription);
//            this.tabPageTaxesNLegal.Controls.Add(this.textBoxDelinquentYear);
//            this.tabPageTaxesNLegal.Controls.Add(this.labelDelinquentYear);
//            this.tabPageTaxesNLegal.Controls.Add(this.textBoxImprovementValue);
//            this.tabPageTaxesNLegal.Controls.Add(this.labelImprovementValue);
//            this.tabPageTaxesNLegal.Controls.Add(this.textBoxLandValue);
//            this.tabPageTaxesNLegal.Controls.Add(this.labelLandValue);
//            this.tabPageTaxesNLegal.Controls.Add(this.textBoxLandUseType);
//            this.tabPageTaxesNLegal.Controls.Add(this.labelLandUseType);
//            this.tabPageTaxesNLegal.Controls.Add(this.textBoxExemptionType);
//            this.tabPageTaxesNLegal.Controls.Add(this.labelExemptionType);
//            this.tabPageTaxesNLegal.Controls.Add(this.textBoxTaxYear);
//            this.tabPageTaxesNLegal.Controls.Add(this.labelTaxYear);
//            this.tabPageTaxesNLegal.Controls.Add(this.textBoxTaxArea);
//            this.tabPageTaxesNLegal.Controls.Add(this.labelTaxArea);
//            this.tabPageTaxesNLegal.Controls.Add(this.textBoxPropertyTax);
//            this.tabPageTaxesNLegal.Controls.Add(this.labelPropertyTax);
//            this.tabPageTaxesNLegal.Controls.Add(this.textBoxAssesmentYear);
//            this.tabPageTaxesNLegal.Controls.Add(this.labelAssesmentYear);
//            this.tabPageTaxesNLegal.Controls.Add(this.textBoxTotalAssesedValue);
//            this.tabPageTaxesNLegal.Controls.Add(this.labelTotalAssesedValue);
//            this.tabPageTaxesNLegal.Controls.Add(this.textBoxAlternateAPN);
//            this.tabPageTaxesNLegal.Controls.Add(this.labelAlternateAPN);
//            this.tabPageTaxesNLegal.Controls.Add(this.textBoxAPN);
//            this.tabPageTaxesNLegal.Controls.Add(this.labelAPN);
//            this.tabPageTaxesNLegal.Controls.Add(this.label56);
//            this.tabPageTaxesNLegal.Controls.Add(this.label55);
//            this.tabPageTaxesNLegal.Location = new System.Drawing.Point(4, 22);
//            this.tabPageTaxesNLegal.Name = "tabPageTaxesNLegal";
//            this.tabPageTaxesNLegal.Size = new System.Drawing.Size(902, 528);
//            this.tabPageTaxesNLegal.TabIndex = 0;
//            this.tabPageTaxesNLegal.Text = "Taxes & Legal";
//            // 
//            // pictureBoxTaxesLegal
//            // 
//            this.pictureBoxTaxesLegal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
//            this.pictureBoxTaxesLegal.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxTaxesLegal.Image")));
//            this.pictureBoxTaxesLegal.Location = new System.Drawing.Point(878, 8);
//            this.pictureBoxTaxesLegal.Name = "pictureBoxTaxesLegal";
//            this.pictureBoxTaxesLegal.Size = new System.Drawing.Size(16, 16);
//            this.pictureBoxTaxesLegal.TabIndex = 117;
//            this.pictureBoxTaxesLegal.TabStop = false;
//            // 
//            // label87
//            // 
//            this.label87.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.label87.Location = new System.Drawing.Point(144, 144);
//            this.label87.Name = "label87";
//            this.label87.Size = new System.Drawing.Size(16, 24);
//            this.label87.TabIndex = 116;
//            this.label87.Text = "$";
//            this.label87.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
//            // 
//            // label86
//            // 
//            this.label86.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.label86.Location = new System.Drawing.Point(264, 144);
//            this.label86.Name = "label86";
//            this.label86.Size = new System.Drawing.Size(16, 24);
//            this.label86.TabIndex = 115;
//            this.label86.Text = "$";
//            this.label86.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
//            // 
//            // textBoxCensusTract
//            // 
//            this.textBoxCensusTract.Location = new System.Drawing.Point(416, 328);
//            this.textBoxCensusTract.Name = "textBoxCensusTract";
//            this.textBoxCensusTract.Size = new System.Drawing.Size(104, 22);
//            this.textBoxCensusTract.TabIndex = 114;
//            this.textBoxCensusTract.Text = "";
//            // 
//            // label62
//            // 
//            this.label62.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.label62.Location = new System.Drawing.Point(416, 312);
//            this.label62.Name = "label62";
//            this.label62.Size = new System.Drawing.Size(96, 24);
//            this.label62.TabIndex = 113;
//            this.label62.Text = "Census Tract";
//            // 
//            // textBoxLegalBlockBldg
//            // 
//            this.textBoxLegalBlockBldg.Location = new System.Drawing.Point(32, 328);
//            this.textBoxLegalBlockBldg.Name = "textBoxLegalBlockBldg";
//            this.textBoxLegalBlockBldg.Size = new System.Drawing.Size(120, 22);
//            this.textBoxLegalBlockBldg.TabIndex = 112;
//            this.textBoxLegalBlockBldg.Text = "";
//            // 
//            // label61
//            // 
//            this.label61.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.label61.Location = new System.Drawing.Point(32, 312);
//            this.label61.Name = "label61";
//            this.label61.Size = new System.Drawing.Size(112, 24);
//            this.label61.TabIndex = 111;
//            this.label61.Text = "Legal Block/Bldg";
//            // 
//            // textBoxLegalBookPage
//            // 
//            this.textBoxLegalBookPage.Location = new System.Drawing.Point(160, 328);
//            this.textBoxLegalBookPage.Name = "textBoxLegalBookPage";
//            this.textBoxLegalBookPage.Size = new System.Drawing.Size(120, 22);
//            this.textBoxLegalBookPage.TabIndex = 110;
//            this.textBoxLegalBookPage.Text = "";
//            // 
//            // label60
//            // 
//            this.label60.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.label60.Location = new System.Drawing.Point(160, 312);
//            this.label60.Name = "label60";
//            this.label60.Size = new System.Drawing.Size(112, 24);
//            this.label60.TabIndex = 109;
//            this.label60.Text = "Legal Book/Page";
//            // 
//            // label59
//            // 
//            this.label59.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.label59.Location = new System.Drawing.Point(408, 48);
//            this.label59.Name = "label59";
//            this.label59.Size = new System.Drawing.Size(16, 24);
//            this.label59.TabIndex = 108;
//            this.label59.Text = "$";
//            this.label59.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
//            // 
//            // label58
//            // 
//            this.label58.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.label58.Location = new System.Drawing.Point(264, 48);
//            this.label58.Name = "label58";
//            this.label58.Size = new System.Drawing.Size(16, 24);
//            this.label58.TabIndex = 107;
//            this.label58.Text = "$";
//            this.label58.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
//            // 
//            // textBoxTownshipName
//            // 
//            this.textBoxTownshipName.Location = new System.Drawing.Point(288, 328);
//            this.textBoxTownshipName.Name = "textBoxTownshipName";
//            this.textBoxTownshipName.Size = new System.Drawing.Size(120, 22);
//            this.textBoxTownshipName.TabIndex = 100;
//            this.textBoxTownshipName.Text = "";
//            // 
//            // labelTownshipName
//            // 
//            this.labelTownshipName.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.labelTownshipName.Location = new System.Drawing.Point(288, 312);
//            this.labelTownshipName.Name = "labelTownshipName";
//            this.labelTownshipName.Size = new System.Drawing.Size(112, 24);
//            this.labelTownshipName.TabIndex = 99;
//            this.labelTownshipName.Text = "Township Name";
//            // 
//            // textBoxLegalDescription
//            // 
//            this.textBoxLegalDescription.Location = new System.Drawing.Point(32, 216);
//            this.textBoxLegalDescription.Multiline = true;
//            this.textBoxLegalDescription.Name = "textBoxLegalDescription";
//            this.textBoxLegalDescription.Size = new System.Drawing.Size(632, 88);
//            this.textBoxLegalDescription.TabIndex = 98;
//            this.textBoxLegalDescription.Text = "";
//            // 
//            // labelLegalDescription
//            // 
//            this.labelLegalDescription.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.labelLegalDescription.Location = new System.Drawing.Point(32, 200);
//            this.labelLegalDescription.Name = "labelLegalDescription";
//            this.labelLegalDescription.Size = new System.Drawing.Size(128, 24);
//            this.labelLegalDescription.TabIndex = 97;
//            this.labelLegalDescription.Text = "Legal Description";
//            // 
//            // textBoxDelinquentYear
//            // 
//            this.textBoxDelinquentYear.Location = new System.Drawing.Point(392, 96);
//            this.textBoxDelinquentYear.Name = "textBoxDelinquentYear";
//            this.textBoxDelinquentYear.TabIndex = 96;
//            this.textBoxDelinquentYear.Text = "";
//            // 
//            // labelDelinquentYear
//            // 
//            this.labelDelinquentYear.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.labelDelinquentYear.Location = new System.Drawing.Point(392, 80);
//            this.labelDelinquentYear.Name = "labelDelinquentYear";
//            this.labelDelinquentYear.Size = new System.Drawing.Size(104, 24);
//            this.labelDelinquentYear.TabIndex = 95;
//            this.labelDelinquentYear.Text = "Delinquent Year";
//            // 
//            // textBoxImprovementValue
//            // 
//            this.textBoxImprovementValue.Location = new System.Drawing.Point(280, 144);
//            this.textBoxImprovementValue.Name = "textBoxImprovementValue";
//            this.textBoxImprovementValue.Size = new System.Drawing.Size(104, 22);
//            this.textBoxImprovementValue.TabIndex = 94;
//            this.textBoxImprovementValue.Text = "";
//            // 
//            // labelImprovementValue
//            // 
//            this.labelImprovementValue.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.labelImprovementValue.Location = new System.Drawing.Point(280, 128);
//            this.labelImprovementValue.Name = "labelImprovementValue";
//            this.labelImprovementValue.Size = new System.Drawing.Size(104, 24);
//            this.labelImprovementValue.TabIndex = 93;
//            this.labelImprovementValue.Text = "Improvement Value";
//            // 
//            // textBoxLandValue
//            // 
//            this.textBoxLandValue.Location = new System.Drawing.Point(160, 144);
//            this.textBoxLandValue.Name = "textBoxLandValue";
//            this.textBoxLandValue.Size = new System.Drawing.Size(88, 22);
//            this.textBoxLandValue.TabIndex = 92;
//            this.textBoxLandValue.Text = "";
//            // 
//            // labelLandValue
//            // 
//            this.labelLandValue.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.labelLandValue.Location = new System.Drawing.Point(152, 128);
//            this.labelLandValue.Name = "labelLandValue";
//            this.labelLandValue.Size = new System.Drawing.Size(72, 24);
//            this.labelLandValue.TabIndex = 91;
//            this.labelLandValue.Text = "Land Value";
//            // 
//            // textBoxLandUseType
//            // 
//            this.textBoxLandUseType.Location = new System.Drawing.Point(32, 144);
//            this.textBoxLandUseType.Name = "textBoxLandUseType";
//            this.textBoxLandUseType.TabIndex = 90;
//            this.textBoxLandUseType.Text = "";
//            // 
//            // labelLandUseType
//            // 
//            this.labelLandUseType.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.labelLandUseType.Location = new System.Drawing.Point(32, 128);
//            this.labelLandUseType.Name = "labelLandUseType";
//            this.labelLandUseType.Size = new System.Drawing.Size(88, 24);
//            this.labelLandUseType.TabIndex = 89;
//            this.labelLandUseType.Text = "Land Use";
//            // 
//            // textBoxExemptionType
//            // 
//            this.textBoxExemptionType.Location = new System.Drawing.Point(280, 96);
//            this.textBoxExemptionType.Name = "textBoxExemptionType";
//            this.textBoxExemptionType.Size = new System.Drawing.Size(104, 22);
//            this.textBoxExemptionType.TabIndex = 86;
//            this.textBoxExemptionType.Text = "";
//            // 
//            // labelExemptionType
//            // 
//            this.labelExemptionType.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.labelExemptionType.Location = new System.Drawing.Point(280, 80);
//            this.labelExemptionType.Name = "labelExemptionType";
//            this.labelExemptionType.Size = new System.Drawing.Size(104, 24);
//            this.labelExemptionType.TabIndex = 85;
//            this.labelExemptionType.Text = "Exemption Type";
//            // 
//            // textBoxTaxYear
//            // 
//            this.textBoxTaxYear.Location = new System.Drawing.Point(152, 96);
//            this.textBoxTaxYear.Name = "textBoxTaxYear";
//            this.textBoxTaxYear.TabIndex = 84;
//            this.textBoxTaxYear.Text = "";
//            // 
//            // labelTaxYear
//            // 
//            this.labelTaxYear.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.labelTaxYear.Location = new System.Drawing.Point(152, 80);
//            this.labelTaxYear.Name = "labelTaxYear";
//            this.labelTaxYear.Size = new System.Drawing.Size(72, 24);
//            this.labelTaxYear.TabIndex = 83;
//            this.labelTaxYear.Text = "Tax Year";
//            // 
//            // textBoxTaxArea
//            // 
//            this.textBoxTaxArea.Location = new System.Drawing.Point(32, 96);
//            this.textBoxTaxArea.Name = "textBoxTaxArea";
//            this.textBoxTaxArea.Size = new System.Drawing.Size(104, 22);
//            this.textBoxTaxArea.TabIndex = 82;
//            this.textBoxTaxArea.Text = "";
//            // 
//            // labelTaxArea
//            // 
//            this.labelTaxArea.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.labelTaxArea.Location = new System.Drawing.Point(32, 80);
//            this.labelTaxArea.Name = "labelTaxArea";
//            this.labelTaxArea.Size = new System.Drawing.Size(64, 24);
//            this.labelTaxArea.TabIndex = 81;
//            this.labelTaxArea.Text = "Tax Area";
//            // 
//            // textBoxPropertyTax
//            // 
//            this.textBoxPropertyTax.Location = new System.Drawing.Point(424, 48);
//            this.textBoxPropertyTax.Name = "textBoxPropertyTax";
//            this.textBoxPropertyTax.Size = new System.Drawing.Size(88, 22);
//            this.textBoxPropertyTax.TabIndex = 80;
//            this.textBoxPropertyTax.Text = "";
//            // 
//            // labelPropertyTax
//            // 
//            this.labelPropertyTax.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.labelPropertyTax.Location = new System.Drawing.Point(424, 32);
//            this.labelPropertyTax.Name = "labelPropertyTax";
//            this.labelPropertyTax.Size = new System.Drawing.Size(72, 24);
//            this.labelPropertyTax.TabIndex = 79;
//            this.labelPropertyTax.Text = "Property Tax";
//            // 
//            // textBoxAssesmentYear
//            // 
//            this.textBoxAssesmentYear.Location = new System.Drawing.Point(536, 48);
//            this.textBoxAssesmentYear.Name = "textBoxAssesmentYear";
//            this.textBoxAssesmentYear.Size = new System.Drawing.Size(88, 22);
//            this.textBoxAssesmentYear.TabIndex = 78;
//            this.textBoxAssesmentYear.Text = "";
//            // 
//            // labelAssesmentYear
//            // 
//            this.labelAssesmentYear.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.labelAssesmentYear.Location = new System.Drawing.Point(536, 32);
//            this.labelAssesmentYear.Name = "labelAssesmentYear";
//            this.labelAssesmentYear.Size = new System.Drawing.Size(120, 24);
//            this.labelAssesmentYear.TabIndex = 77;
//            this.labelAssesmentYear.Text = "Assesment Year";
//            // 
//            // textBoxTotalAssesedValue
//            // 
//            this.textBoxTotalAssesedValue.Location = new System.Drawing.Point(280, 48);
//            this.textBoxTotalAssesedValue.Name = "textBoxTotalAssesedValue";
//            this.textBoxTotalAssesedValue.Size = new System.Drawing.Size(104, 22);
//            this.textBoxTotalAssesedValue.TabIndex = 76;
//            this.textBoxTotalAssesedValue.Text = "";
//            // 
//            // labelTotalAssesedValue
//            // 
//            this.labelTotalAssesedValue.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.labelTotalAssesedValue.Location = new System.Drawing.Point(280, 32);
//            this.labelTotalAssesedValue.Name = "labelTotalAssesedValue";
//            this.labelTotalAssesedValue.Size = new System.Drawing.Size(128, 24);
//            this.labelTotalAssesedValue.TabIndex = 75;
//            this.labelTotalAssesedValue.Text = "Total Assesed Value";
//            // 
//            // textBoxAlternateAPN
//            // 
//            this.textBoxAlternateAPN.Location = new System.Drawing.Point(152, 48);
//            this.textBoxAlternateAPN.Name = "textBoxAlternateAPN";
//            this.textBoxAlternateAPN.TabIndex = 74;
//            this.textBoxAlternateAPN.Text = "";
//            // 
//            // labelAlternateAPN
//            // 
//            this.labelAlternateAPN.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.labelAlternateAPN.Location = new System.Drawing.Point(152, 32);
//            this.labelAlternateAPN.Name = "labelAlternateAPN";
//            this.labelAlternateAPN.Size = new System.Drawing.Size(104, 24);
//            this.labelAlternateAPN.TabIndex = 73;
//            this.labelAlternateAPN.Text = "Alternate APN";
//            // 
//            // textBoxAPN
//            // 
//            this.textBoxAPN.Location = new System.Drawing.Point(32, 48);
//            this.textBoxAPN.Name = "textBoxAPN";
//            this.textBoxAPN.Size = new System.Drawing.Size(104, 22);
//            this.textBoxAPN.TabIndex = 72;
//            this.textBoxAPN.Text = "";
//            // 
//            // labelAPN
//            // 
//            this.labelAPN.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.labelAPN.Location = new System.Drawing.Point(32, 32);
//            this.labelAPN.Name = "labelAPN";
//            this.labelAPN.Size = new System.Drawing.Size(72, 24);
//            this.labelAPN.TabIndex = 71;
//            this.labelAPN.Text = "APN";
//            // 
//            // label56
//            // 
//            this.label56.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
//            this.label56.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.label56.Location = new System.Drawing.Point(16, 184);
//            this.label56.Name = "label56";
//            this.label56.Size = new System.Drawing.Size(184, 23);
//            this.label56.TabIndex = 102;
//            this.label56.Text = "LEGAL DESCRIPTION";
//            // 
//            // label55
//            // 
//            this.label55.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
//            this.label55.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
//            this.label55.Location = new System.Drawing.Point(16, 16);
//            this.label55.Name = "label55";
//            this.label55.Size = new System.Drawing.Size(184, 23);
//            this.label55.TabIndex = 101;
//            this.label55.Text = "TAX INFO";
//            // 
//            // UserControlPropertyProfileComps
//            // 
//            this.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(210)), ((System.Byte)(216)), ((System.Byte)(240)));
//            this.Controls.Add(this.tabControlOnlinePropertyProfileNComps);
//            this.Name = "UserControlPropertyProfileComps";
//            this.Size = new System.Drawing.Size(910, 554);
//            this.tabPagePropertyCharacteristics.ResumeLayout(false);
//            this.tabPageOwnershipNLocations.ResumeLayout(false);
//            this.panel1.ResumeLayout(false);
//            this.panel2.ResumeLayout(false);
//            this.panel3.ResumeLayout(false);
//            this.tabControlOnlinePropertyProfileNComps.ResumeLayout(false);
//            this.tabPageFinancials.ResumeLayout(false);
//            this.tabPageTaxesNLegal.ResumeLayout(false);
//            this.ResumeLayout(false);

//        }
//        #endregion

//        private void bPrintReport_Click(object sender, System.EventArgs e)
//        {
//            try 
//            {
//                PrintDocument tmpprndoc = new PrintDocument();
//                tmpprndoc.PrintPage += new PrintPageEventHandler(toPrinter);
//                PrintPreviewDialogEx tmpprdiag = new PrintPreviewDialogEx();								
//                tmpprdiag.Document = tmpprndoc;
			
//                tmpprdiag.ShowDialog();

//            }
//            catch (Exception)
//            {
//                //silent catch (not a mistake)
//                //when no printer installed -> Print -> Cancel print -> Cancel would crash app.
//            }	
			
//        }

//        private void toPrinter(object sender, PrintPageEventArgs e)
//        {
//            ApplicationHelper.PrintDataReport(e, this.property, 0, 1, 1);
//        }

//        private void bPrintMultRepts_Click(object sender, System.EventArgs e)
//        {
			
//        }

//        private void Show_Floater(object sender, EventArgs e)
//        {
//            this.floater = new DealMaker.Forms.Floaters.PropertyProfile();


//            Point pReadMe = this.pictureBoxOwnershipLocations.Location;
//            Size fSize = this.floater.Size;
						
//            //this.floater.Location = new Point(pReadMe.X - fSize.Width, pReadMe.Y + this.readMe1.Height);
//            this.floater.Location = PointToScreen(new Point (pReadMe.X - fSize.Width, pReadMe.Y - 175) );

//            this.floater.Show();
//        }

//        private void Hide_Floater(object sender, EventArgs e)
//        {
//            this.floater.Hide();
//        }

//        private void tabPageFinancials_Click(object sender, System.EventArgs e)
//        {
		
//        }
				
		
//    }
//    #region . PropertyProfile .
//    public class PropertyProfile
//    {
//        #region � Data �
//        private Guid propertyItemId;
//        private Byte[] xmlStream;
//        #endregion � Data �
//        #region � CTR �
//        public PropertyProfile()
//        {
//        }

//        public PropertyProfile(Guid propertyItemId, Byte[]
//            xmlStream)
//        {
//            this.propertyItemId = propertyItemId;
//            this.xmlStream = xmlStream;
//        }
//        #endregion � CTR �
//        #region � Properties �
//        #region �  P_propertyItemId�
//        [Description("propertyItemId")]
//        public Guid P_propertyItemId
//        {
//            get
//            {
//                return this.propertyItemId;
//            }
//            set
//            {
//                this.propertyItemId = value;
//            }
//        }
//        #endregion �  P_propertyItemId�
//        #region �  P_xmlStream�
//        [Description("xmlStream")]
//        public Byte[] P_xmlStream
//        {
//            get
//            {
//                return this.xmlStream;
//            }
//            set
//            {
//                this.xmlStream = value;
//            }
//        }
//        #endregion �  P_xmlStream�
//        #endregion � Properties �
//        #region � Methods �
//        public int Insert(SqlConnection dbConn)
//        {
//            int retVal = 0;

//            try
//            {
//                SqlCommand sp_insert_PropertyProfile = new
//                    SqlCommand("insert_PropertyProfile", dbConn);

//                sp_insert_PropertyProfile.CommandType =
//                    CommandType.StoredProcedure;

	
//                sp_insert_PropertyProfile.Parameters.Add("@propertyItemId",
//                    this.propertyItemId);
	
//                sp_insert_PropertyProfile.Parameters.Add("@xmlStream", this.xmlStream);
//                retVal =
//                    sp_insert_PropertyProfile.ExecuteNonQuery();
//            }
//            catch (Exception exc)
//            {
//                throw exc;
//            }
//            return retVal;
//        }

//        public int Update(SqlConnection dbConn)
//        {
//            int retVal = 0;

//            try
//            {
//                SqlCommand sp_update_PropertyProfile = new
//                    SqlCommand("update_PropertyProfile", dbConn);

//                sp_update_PropertyProfile.CommandType =
//                    CommandType.StoredProcedure;

	
//                sp_update_PropertyProfile.Parameters.Add("@propertyItemId",
//                    this.propertyItemId);
	
//                sp_update_PropertyProfile.Parameters.Add("@xmlStream", this.xmlStream);
//                retVal =
//                    sp_update_PropertyProfile.ExecuteNonQuery();
//            }
//            catch (Exception exc)
//            {
//                throw exc;
//            }
//            return retVal;
//        }

//        public int Delete(SqlConnection dbConn)
//        {
//            int retVal = 0;

//            try
//            {
//                SqlCommand sp_delete_PropertyProfile = new
//                    SqlCommand("delete_PropertyProfile", dbConn);

//                sp_delete_PropertyProfile.CommandType =
//                    CommandType.StoredProcedure;

	
//                sp_delete_PropertyProfile.Parameters.Add("@propertyItemId",
//                    this.propertyItemId);
//                retVal =
//                    sp_delete_PropertyProfile.ExecuteNonQuery();
//            }
//            catch (Exception exc)
//            {
//                throw exc;
//            }
//            return retVal;
//        }

//        public static PropertyProfile[] Select(SqlConnection dbConn,
//            params object[] list)
//        {
//            ArrayList alObjects = new ArrayList();
//            SqlDataReader reader = null;
//            PropertyProfile tempPropertyProfile = null;
//            try
//            {
//                SqlCommand sp_select_PropertyProfile = new
//                    SqlCommand("select_PropertyProfile", dbConn);

//                sp_select_PropertyProfile.CommandType =
//                    CommandType.StoredProcedure;

//                if (null != list && 0 != list.Length)
//                {
	
//                    sp_select_PropertyProfile.Parameters.Add("@propertyItemId",
//                        (System.Guid)list[0]);
//                }
//                else
//                {
	
//                    sp_select_PropertyProfile.Parameters.Add("@propertyItemId", DBNull.Value);
//                }
//                reader =
//                    sp_select_PropertyProfile.ExecuteReader();
//                while (reader.Read())
//                {
//                    System.Data.SqlTypes.SqlBinary
//                        sqlBinary1 = reader.GetSqlBinary(1);
//                    tempPropertyProfile = new
//                        PropertyProfile(reader.GetGuid(0), (byte[])sqlBinary1.Value);
//                    alObjects.Add(tempPropertyProfile);
//                }
//            }
//            catch (Exception exc)
//            {
//                throw exc;
//            }
//            finally
//            {
//                reader.Close();
//            }
//            return (PropertyProfile[])
//                alObjects.ToArray(typeof(PropertyProfile));
//        }
//        #endregion � Methods �
//    }
//    #endregion
//    #region . TEMPLATE .

///*
//        textBoxOwnerName;
//        textBoxSchoolDistrict;
//        textBoxLotNumber;
//        textBoxSubdivision;
//        textBoxComparableNumber;
//        textBoxDistanceFromSubject;
//        textBoxCounty;
//        textBoxPostDirection;
//        textBoxMailingZipCode;
//        textBoxMailingCityAndState;
//        textBoxMailingAddress;
//        textBoxMapReferenceTwo;
//        textBoxMapReferenceOne;
//        textBoxZipCode;
//        textBoxState;
//        textBoxCity;
//        textBoxUnitNumber;
//        textBoxSuffix;
//        textBoxStreetnName;
//        textBoxPreDirection;
//        textBoxHouseNumber;
//        textBoxTotalBaths;
//        textBoxTotalBedrooms;
//        textBoxTotalRooms;
//        textBoxZoning;
//        textBoxNumberOfStories;
//        textBoxFireplace;
//        textBoxAirConditioning;
//        textBoxPool;
//        textBoxRoofType;
//        textBoxParkingCapacity;
//        textBoxParkingType;
//        textBoxLotSize;
//        textBoxLotArea;
//        textBoxTotalLivingArea;
//        textBoxYearBuilt;
//        textBoxSecondlMortgageInterestRateType;
//        textBoxSecondlMortgageInterestRateTerm;
//        textBoxSecondlMortgageInterestRate;
//        textBoxSecondlMortgageAmount;
//        textBoxSellerName;
//        textBoxPriorSalePrice;
//        textBoxPriorSaleDate;
//        textBox4PricePerSqFt;
//        textBoxCashDown;
//        textBoxFirstMortgageLender;
//        textBoxType;
//        textBoxFirstMortgageInterestRateTerm;
//        textBoxFirstMortgageInterestRate;
//        textBoxFirstMortgageAmount;
//        textBoxRecordingDate;
//        textBoxDocumentNumber;
//        textBoxDeedType;
//        textBoxSaleType1;
//        textBoxSalePrice;
//        textBoxSaleDate;
//        textBoxTownshipName;
//        textBoxLegalDescription;
//        textBoxDelinquentYear;
//        textBoxImprovementValue;
//        textBoxLandValue;
//        textBoxLandUseType;
//        textBoxLandUse;
//        textBoxExemptionType;
//        textBoxTaxYear;
//        textBoxTaxArea;
//        textBoxPropertyTax;
//        textBoxAssesmentYear;
//        textBoxTotalAssesedValue;
//        textBoxAlternateAPN;
//        textBoxAPN;
//        textBoxPhoneNumber;
		
//        qwerty = textBoxOwnerName;
//        qwerty = textBoxSchoolDistrict;
//        qwerty = textBoxLotNumber;
//        qwerty = textBoxSubdivision;
//        qwerty = textBoxComparableNumber;
//        qwerty = textBoxDistanceFromSubject;
//        qwerty = textBoxCounty;
//        qwerty = textBoxPostDirection;
//        qwerty = textBoxMailingZipCode;
//        qwerty = textBoxMailingCityAndState;
//        qwerty = textBoxMailingAddress;
//        qwerty = textBoxMapReferenceTwo;
//        qwerty = textBoxMapReferenceOne;
//        qwerty = textBoxZipCode;
//        qwerty = textBoxState;
//        qwerty = textBoxCity;
//        qwerty = textBoxUnitNumber;
//        qwerty = textBoxSuffix;
//        qwerty = textBoxStreetnName;
//        qwerty = textBoxPreDirection;
//        qwerty = textBoxHouseNumber;
//        qwerty = textBoxTotalBaths;
//        qwerty = textBoxTotalBedrooms;
//        qwerty = textBoxTotalRooms;
//        qwerty = textBoxZoning;
//        qwerty = textBoxNumberOfStories;
//        qwerty = textBoxFireplace;
//        qwerty = textBoxAirConditioning;
//        qwerty = textBoxPool;
//        qwerty = textBoxRoofType;
//        qwerty = textBoxParkingCapacity;
//        qwerty = textBoxParkingType;
//        qwerty = textBoxLotSize;
//        qwerty = textBoxLotArea;
//        qwerty = textBoxTotalLivingArea;
//        qwerty = textBoxYearBuilt;
//        qwerty = textBoxSecondlMortgageInterestRateType;
//        qwerty = textBoxSecondlMortgageInterestRateTerm;
//        qwerty = textBoxSecondlMortgageInterestRate;
//        qwerty = textBoxSecondlMortgageAmount;
//        qwerty = textBoxSellerName;
//        qwerty = textBoxPriorSalePrice;
//        qwerty = textBoxPriorSaleDate;
//        qwerty = textBox4PricePerSqFt;
//        qwerty = textBoxCashDown;
//        qwerty = textBoxFirstMortgageLender;
//        qwerty = textBoxType;
//        qwerty = textBoxFirstMortgageInterestRateTerm;
//        qwerty = textBoxFirstMortgageInterestRate;
//        qwerty = textBoxFirstMortgageAmount;
//        qwerty = textBoxRecordingDate;
//        qwerty = textBoxDocumentNumber;
//        qwerty = textBoxDeedType;
//        qwerty = textBoxSaleType1;
//        qwerty = textBoxSalePrice;
//        qwerty = textBoxSaleDate;
//        qwerty = textBoxTownshipName;
//        qwerty = textBoxLegalDescription;
//        qwerty = textBoxDelinquentYear;
//        qwerty = textBoxImprovementValue;
//        qwerty = textBoxLandValue;
//        qwerty = textBoxLandUseType;
//        qwerty = textBoxLandUse;
//        qwerty = textBoxExemptionType;
//        qwerty = textBoxTaxYear;
//        qwerty = textBoxTaxArea;
//        qwerty = textBoxPropertyTax;
//        qwerty = textBoxAssesmentYear;
//        qwerty = textBoxTotalAssesedValue;
//        qwerty = textBoxAlternateAPN;
//        qwerty = textBoxAPN;
//        qwerty = textBoxPhoneNumber;

 
//*/
//#endregion
//}
