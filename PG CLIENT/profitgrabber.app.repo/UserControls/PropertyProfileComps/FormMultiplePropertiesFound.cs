
using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;

namespace DealMaker
{
	/// <summary>
	/// Summary description for UserControlMultipleProperiesFound.
	/// </summary>
	public class FormMultipleProperiesFound : System.Windows.Forms.Form
	{
		private System.Windows.Forms.ColumnHeader columnHeaderStreetNum;
		private System.Windows.Forms.ColumnHeader columnHeaderPredir;
		private System.Windows.Forms.ColumnHeader columnHeaderSreetName;
		private System.Windows.Forms.ColumnHeader columnHeaderPostDir;
		private System.Windows.Forms.ColumnHeader columnHeaderSuffix;
		private System.Windows.Forms.ColumnHeader columnHeaderUnitNo;
		private System.Windows.Forms.ColumnHeader columnHeaderZip;
		private System.Windows.Forms.ColumnHeader columnHeaderCounty;
		private System.Windows.Forms.ColumnHeader columnHeaderState;
		private System.Windows.Forms.ColumnHeader columnHeaderOwner;
		private bool asc = false;
		private System.Windows.Forms.ListView listViewPropertyProfilesComps;
		private System.Windows.Forms.ColumnHeader columnHeaderAPN;
		private System.Windows.Forms.ColumnHeader columnHeaderID;
		private int _selected= -1;

		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public FormMultipleProperiesFound()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

			// TODO: Add any initialization after the InitializeComponent call

		}

		public FormMultipleProperiesFound(bool OwnerNameVisible)
		{
			InitializeComponent();
			if (!OwnerNameVisible)
			{
				this.columnHeaderOwner.Width = 0;
			}

		}

		#region � lvFill �
		public void lvFill(DTD.Response._MULTIPLE_RECORDS[] multipla)
		{
			ListViewItem lvItem;
			for(
				int iterator = multipla.GetLowerBound(0); 
				iterator <= multipla.GetUpperBound(0); 
				iterator++
				)
			{
				string cel01="";
				string cel02="";
				string cel03="";
				string cel04="";
				string cel05="";
				string cel06="";
				string cel07="";
				string cel08="";
				string cel09="";
				string cel10="";
				string cel11="";
			
				try
				{
					cel01 = getSecureString(multipla[iterator].PROPERTY._PARSED_STREET_ADDRESS._HouseNumber);
				}
				catch{}
				try
				{
					cel02 = getSecureString(multipla[iterator].PROPERTY._PARSED_STREET_ADDRESS._DirectionPrefix);
				}
				catch{}
				try
				{
					cel03 = getSecureString(multipla[iterator].PROPERTY._PARSED_STREET_ADDRESS._StreetName);
				}
				catch{}
				try
				{
					cel04 = getSecureString(multipla[iterator].PROPERTY._PARSED_STREET_ADDRESS._DirectionSuffix);
				}
				catch{}
				try
				{
					cel05 = getSecureString(multipla[iterator].PROPERTY._PARSED_STREET_ADDRESS._StreetSuffix);
				}
				catch{}
				try
				{
					cel06 = getSecureString(multipla[iterator].PROPERTY._PARSED_STREET_ADDRESS._ApartmentOrUnit);
				}
				catch{}
				try
				{
					cel07 = getSecureString(multipla[iterator].PROPERTY._PostalCode);
				}
				catch{}
				try
				{
					cel08 = getSecureString(multipla[iterator].PROPERTY._County);
				}
				catch{}
				try
				{
					cel09 = getSecureString(multipla[iterator].PROPERTY._State);
				}
				catch{}
				try
				{
					cel10 = getSecureString(multipla[iterator].PROPERTY._AssessorsParcelIdentifier);
				}
				catch{}
				try
				{
					cel11 = getSecureString(multipla[iterator].PROPERTY.PROPERTY_OWNER[0]._OwnerName);
				}
				catch{}
			

				lvItem = new ListViewItem
					(new string[] {
									  cel01
									  ,cel02
									  ,cel03
									  ,cel04
									  ,cel05
									  ,cel06
									  ,cel07
									  ,cel08
									  ,cel09
									  ,cel10
									  ,cel11
									  ,iterator.ToString()
				});
				listViewPropertyProfilesComps.Items.Add(lvItem);
			}
		}
		#endregion

		public int selected
		{
			get{return _selected;}
		}


		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}
		#region � lvPropertyItems_ColumnClick �
		#region . num dat parsing .
		private string getSecureString(object str)
		{
			string SecureString = "";
			try
			{
				SecureString = str.ToString();
			}
			catch
			{}
			return SecureString;
		}
		private string getMoney(string sMoney)
		{
			try
			{
				sMoney = Convert.ToDouble(sMoney).ToString("C");
			}
			catch
			{
			}

			return sMoney;
		}

		private string getDecimal(string sDecimal)
		{
			try
			{
				sDecimal = Convert.ToDouble(sDecimal).ToString("N");
			}
			catch
			{
			}

			return sDecimal;
		}
		private string getNumber(string sDecimal)
		{
			try
			{
				sDecimal = Convert.ToDouble(sDecimal).ToString("D");
			}
			catch
			{
			}

			return sDecimal;
		}

		private string getDate(string sDate)
		{
			if (sDate.Length==8)
			{
				try
				{
					DateTime d = new DateTime
						(
						(int.Parse(sDate.Substring(0,4))),
						(int.Parse(sDate.Substring(4,2))),
						(int.Parse(sDate.Substring(6,2)))
						);

					sDate = d.ToShortDateString();
				}
				catch
				{
				}
			}

			return sDate;
		}


		#endregion
		private void lvPropertyItems_ColumnClick(object sender, System.Windows.Forms.ColumnClickEventArgs e)

		{

			// Set the ListViewItemSorter property to a new ListViewItemComparer 

			// object. Setting this property immediately sorts the 

			// ListView using the ListViewItemComparer object.                

			this.asc = !this.asc;

			if (true == this.asc)

				this.listViewPropertyProfilesComps.ListViewItemSorter = new ListViewItemComparer(e.Column);

			else

				this.listViewPropertyProfilesComps.ListViewItemSorter = new ListViewItemComparerDesc(e.Column);

 

		}

		#endregion � lvPropertyItems_ColumnClick �
		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.listViewPropertyProfilesComps = new System.Windows.Forms.ListView();
			this.columnHeaderStreetNum = new System.Windows.Forms.ColumnHeader();
			this.columnHeaderPredir = new System.Windows.Forms.ColumnHeader();
			this.columnHeaderSreetName = new System.Windows.Forms.ColumnHeader();
			this.columnHeaderPostDir = new System.Windows.Forms.ColumnHeader();
			this.columnHeaderSuffix = new System.Windows.Forms.ColumnHeader();
			this.columnHeaderUnitNo = new System.Windows.Forms.ColumnHeader();
			this.columnHeaderZip = new System.Windows.Forms.ColumnHeader();
			this.columnHeaderCounty = new System.Windows.Forms.ColumnHeader();
			this.columnHeaderState = new System.Windows.Forms.ColumnHeader();
			this.columnHeaderAPN = new System.Windows.Forms.ColumnHeader();
			this.columnHeaderOwner = new System.Windows.Forms.ColumnHeader();
			this.columnHeaderID = new System.Windows.Forms.ColumnHeader();
			this.SuspendLayout();
			// 
			// listViewPropertyProfilesComps
			// 
			this.listViewPropertyProfilesComps.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
																											this.columnHeaderStreetNum,
																											this.columnHeaderPredir,
																											this.columnHeaderSreetName,
																											this.columnHeaderPostDir,
																											this.columnHeaderSuffix,
																											this.columnHeaderUnitNo,
																											this.columnHeaderZip,
																											this.columnHeaderCounty,
																											this.columnHeaderState,
																											this.columnHeaderAPN,
																											this.columnHeaderOwner,
																											this.columnHeaderID});
			this.listViewPropertyProfilesComps.Dock = System.Windows.Forms.DockStyle.Fill;
			this.listViewPropertyProfilesComps.FullRowSelect = true;
			this.listViewPropertyProfilesComps.GridLines = true;
			this.listViewPropertyProfilesComps.Location = new System.Drawing.Point(0, 0);
			this.listViewPropertyProfilesComps.MultiSelect = false;
			this.listViewPropertyProfilesComps.Name = "listViewPropertyProfilesComps";
			this.listViewPropertyProfilesComps.Size = new System.Drawing.Size(658, 187);
			this.listViewPropertyProfilesComps.TabIndex = 5;
			this.listViewPropertyProfilesComps.View = System.Windows.Forms.View.Details;
			this.listViewPropertyProfilesComps.DoubleClick += new System.EventHandler(this.listViewPropertyProfilesComps_DoubleClick);
			this.listViewPropertyProfilesComps.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.lvPropertyItems_ColumnClick);
			// 
			// columnHeaderStreetNum
			// 
			this.columnHeaderStreetNum.Text = "Street #";
			this.columnHeaderStreetNum.Width = 40;
			// 
			// columnHeaderPredir
			// 
			this.columnHeaderPredir.Text = "Pre-dir";
			this.columnHeaderPredir.Width = 40;
			// 
			// columnHeaderSreetName
			// 
			this.columnHeaderSreetName.Text = "Street Name";
			this.columnHeaderSreetName.Width = 100;
			// 
			// columnHeaderPostDir
			// 
			this.columnHeaderPostDir.Text = "Post-dir";
			this.columnHeaderPostDir.Width = 40;
			// 
			// columnHeaderSuffix
			// 
			this.columnHeaderSuffix.Text = "Suffix";
			this.columnHeaderSuffix.Width = 40;
			// 
			// columnHeaderUnitNo
			// 
			this.columnHeaderUnitNo.Text = "Unit No.";
			this.columnHeaderUnitNo.Width = 40;
			// 
			// columnHeaderZip
			// 
			this.columnHeaderZip.Text = "ZIP";
			this.columnHeaderZip.Width = 50;
			// 
			// columnHeaderCounty
			// 
			this.columnHeaderCounty.Text = "County";
			this.columnHeaderCounty.Width = 80;
			// 
			// columnHeaderState
			// 
			this.columnHeaderState.Text = "State";
			// 
			// columnHeaderAPN
			// 
			this.columnHeaderAPN.Text = "APN";
			this.columnHeaderAPN.Width = 90;
			// 
			// columnHeaderOwner
			// 
			this.columnHeaderOwner.Text = "Owner";
			this.columnHeaderOwner.Width = 120;
			// 
			// columnHeaderID
			// 
			this.columnHeaderID.Text = "ID";
			this.columnHeaderID.Width = 0;
			// 
			// FormMultipleProperiesFound
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(658, 187);
			this.Controls.Add(this.listViewPropertyProfilesComps);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
			this.Name = "FormMultipleProperiesFound";
			this.Text = "Select the Subject Property!";
			this.ResumeLayout(false);

		}
		#endregion

		private void listViewPropertyProfilesComps_DoubleClick(object sender, System.EventArgs e)
		{
			_selected = int.Parse(listViewPropertyProfilesComps.SelectedItems[0].SubItems[11].Text);
			this.Close();
		}


	}
}
