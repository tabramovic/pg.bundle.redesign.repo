
//#define NEW_DTD
////#define DUMP_XML_OBJECT

//using System;
//using System.Collections;
//using System.ComponentModel;
//using System.Drawing;
//using System.Data;
//using System.Windows.Forms;
//using System.IO;
//using System.Xml.Serialization;
//using System.Web;
//using System.Text;
//using System.Xml;
//using System.Net;
//using System.Data.SqlClient;
//using System.Diagnostics;
//using System.Drawing.Printing;

////Added
//using ProfitGrabber.Common;

////Floaters
//using DealMaker.Forms.Floaters;

//namespace DealMaker
//{
//    /// <summary>
//    /// Summary description for UserControlPropertyProfileCompsListView.
//    /// </summary>
//    public class UserControlPropertyProfileCompsListView : System.Windows.Forms.UserControl
//    {
//        //AUTOSAVE
//        public delegate void Success_COMPS_Download();
//        public event Success_COMPS_Download COMPS_Downloaded_Successfully;
//        bool _allowAutoSaveTriggerEvents = false;
//        public bool AllowAutoSaveTriggerEvents
//        {
//            get { return _allowAutoSaveTriggerEvents; }
//            set { _allowAutoSaveTriggerEvents = value; }
//        }

//        #region . variables .
//        private Hashtable htPrintMappingInfo = new Hashtable();
		
//        private DataConsumption.ServiceDataConsumption sdc;
//        private DealMaker.Forms.Floaters.Comps floater = null;
		
//        private DTD.Response.RESPONSE_GROUP response;
//        private DTD.Response._DATA_PROVIDER_COMPARABLE_SALES[] comps;
//        private bool asc = false;
//        private double originalSQFT;
//        private DealMaker.comps[] dbComps = null;
//        private SqlConnection _dbConn = null;
//        private bool _compsInDb = false;
//        private decimal _estimatedValue = 0;

//        /// <summary> 
//        /// Required designer variable.
//        /// </summary>
//        private System.ComponentModel.Container components = null;
//        private System.Windows.Forms.Label labelRequestParameters;
//        private System.Windows.Forms.Button buttonGetComps;
//        private System.Windows.Forms.ListView listViewPropertyProfilesComps;
//        private System.Windows.Forms.Label labelSubjectPropertySquareFootage;
//        private System.Windows.Forms.ColumnHeader columnHeaderTag;
//        private System.Windows.Forms.ColumnHeader columnHeaderDistnace;
//        private System.Windows.Forms.ColumnHeader columnHeaderSubdivision;
//        private System.Windows.Forms.ColumnHeader columnHeaderCity;
//        private System.Windows.Forms.ColumnHeader columnHeaderSQFT;
//        private System.Windows.Forms.ColumnHeader columnHeaderYB;
//        private System.Windows.Forms.ColumnHeader columnHeaderRooms;
//        private System.Windows.Forms.ColumnHeader columnHeaderBedrooms;
//        private System.Windows.Forms.ColumnHeader columnHeaderBath;
//        private System.Windows.Forms.ColumnHeader columnHeaderSaleDate;
//        private System.Windows.Forms.ColumnHeader columnHeaderSalePrice;
//        private System.Windows.Forms.ColumnHeader columnHeaderLotArea;
//        private System.Windows.Forms.ColumnHeader columnHeaderAssesed;
//        private System.Windows.Forms.ColumnHeader columnHeaderStreetName;
//        private System.Windows.Forms.ColumnHeader columnHeaderZIP;
//        private System.Windows.Forms.ColumnHeader columnHeaderPool;
//        private System.Windows.Forms.Button buttonInvert;
//        private System.Windows.Forms.Button buttonUntagAll;
//        private System.Windows.Forms.Button buttonTagAll;
//        private System.Windows.Forms.Label label4;
//        private System.Windows.Forms.Label label5;
//        private System.Windows.Forms.Label label6;
//        private System.Windows.Forms.Label label7;
//        private System.Windows.Forms.ComboBox comboBoxSqFtRange;
//        private System.Windows.Forms.ComboBox comboBoxSearchRadius;
//        private System.Windows.Forms.ComboBox comboBoxSalesInLast;
//        private System.Windows.Forms.TextBox textBoxEstValue;
//        private System.Windows.Forms.TextBox textBoxEstValuePrivate;
//        private System.Windows.Forms.TextBox textBoxDate;
//        private System.Windows.Forms.Button buttonRecalculate;
//        private System.Windows.Forms.Label label8;
//        private System.Windows.Forms.ColumnHeader columnHeader1;
//        private System.Windows.Forms.ColumnHeader columnHeaderStreetNum;
//        private System.Windows.Forms.ColumnHeader columnHeaderSqFtPrice;
//        private System.Windows.Forms.TextBox textBoxStateA;
//        private System.Windows.Forms.TextBox textBoxZipA;
//        private System.Windows.Forms.TextBox textBoxUnitNoA;
//        private System.Windows.Forms.TextBox textBoxStreetA;
//        private System.Windows.Forms.TextBox textBoxStreetNoA;
//        private System.Windows.Forms.Label label76;
//        private System.Windows.Forms.Label label77;
//        private System.Windows.Forms.Label label80;
//        private System.Windows.Forms.Label label82;
//        private System.Windows.Forms.TextBox textBoxCountyA;
//        private System.Windows.Forms.Label label74;
//        private System.Windows.Forms.Label label75;
//        private System.Windows.Forms.Label label1;
//        private System.Windows.Forms.Label label2;
//        private System.Windows.Forms.TextBox textBoxPreDirA;
//        private System.Windows.Forms.Label label81;
//        private System.Windows.Forms.TextBox textBoxSuffixA;
//        private System.Windows.Forms.TextBox textBoxPostDirA;
//        private System.Windows.Forms.Label label78;
//        private System.Windows.Forms.Label label79;
//        private System.Windows.Forms.Label label3;
//        private System.Windows.Forms.PictureBox pictureExclamation;
//        private System.Windows.Forms.Label label9;
//        private System.Windows.Forms.Label label10;
//        private System.Windows.Forms.Button bPrint;
//        private System.Windows.Forms.Label label11;
//        private System.Windows.Forms.TextBox tbAPN;
//        private System.Windows.Forms.Button _exportCOMPS;
//        private System.Windows.Forms.TextBox textBoxOrigSqFt;

		
//        #endregion
//        #region . constructror .
//        public UserControlPropertyProfileCompsListView()
//        {
//            //TODO: Define if we need this here !!!
//            System.Threading.Thread.CurrentThread.CurrentCulture=System.Globalization.CultureInfo.CreateSpecificCulture("EN-US");

//            // This call is required by the Windows.Forms Form Designer.
//            InitializeComponent();

//            this.bPrint.Click += new EventHandler(bPrint_Click);
//            this.pictureExclamation.MouseEnter += new EventHandler(Show_Floater);
//            this.pictureExclamation.MouseLeave += new EventHandler(Hide_Floater);

//            //TA++
//            //EXPORT COMPS July 07th, 2007
//            _exportCOMPS.Click += new EventHandler(On_ExportCOMPS);
//            //TA--

//            //TA:++ 15.03.2005
//            //Change: Commented out "clearForm();" statement
//            //clearForm();

//            //TA:-- 15.03.2005
//            //this.dummyFill();

//            comboBoxSearchRadius.Items.Clear();
//            comboBoxSqFtRange.Items.Clear();
//            comboBoxSalesInLast.Items.Clear();


//            comboBoxSearchRadius.Items.AddRange(new string[] {
//                                                                 "0.1"
//                                                                 ,"0.2"
//                                                                 ,"0.3"
//                                                                 ,"0.4"																 
//                                                                 ,"0.5"
//                                                                 ,"0.6"
//                                                                 ,"0.7"
//                                                                 ,"0.8"
//                                                                 ,"0.9"
//                                                                 ,"1.0"
//                                                                 ,"1.1"
//                                                                 ,"1.2"
//                                                                 ,"1.3"
//                                                                 ,"1.4"
//                                                                 ,"1.5"
//                                                                 ,"1.6"
//                                                                 ,"1.7"
//                                                                 ,"1.8"
//                                                                 ,"1.9"
//                                                                 ,"2.0"
//                                                                 ,"2.1"
//                                                                 ,"2.2"
//                                                                 ,"2.3"
//                                                                 ,"2.4"
//                                                                 ,"2.5"
//                                                                 ,"2.6"
//                                                                 ,"2.7"
//                                                                 ,"2.8"
//                                                                 ,"2.9"
//                                                                 ,"3.0"
//                                                             });
//            comboBoxSqFtRange.Items.AddRange(new string[] {
//                                                              "+/-10%"
//                                                              ,"+/-15%"
//                                                              ,"+/-20%"
//                                                              ,"+/-25%"
//                                                              ,"+/-30%"
//                                                              ,"+/-35%"
//                                                              ,"+/-40%"
//                                                              ,"+/-45%"
//                                                              ,"+/-50%"
//                                                              ,"+/-55%"});
//            comboBoxSalesInLast.Items.AddRange(new string[] {
//                                                                "24 mo."
//                                                                ,"20 mo."
//                                                                ,"15 mo."
//                                                                ,"12 mo."
//                                                                ,"10 mo."
//                                                                ,"8 mo."
//                                                                ,"6 mo."
//                                                            });

//            comboBoxSqFtRange.SelectedIndex = 3;
//            comboBoxSearchRadius.SelectedIndex = 9;			
//            comboBoxSalesInLast.SelectedIndex = 3;

//        }
//        #endregion
//        #region . connection .
//        public SqlConnection DBConn
//        {
//            get { return this._dbConn; }
//            set { this._dbConn = value; }
//        }
//        #endregion
//        #region . clear form .
//        public void clearForm()
//        {
//            listViewPropertyProfilesComps.Items.Clear();
//            foreach (object obj in this.Controls)
//            {
//                if (obj.GetType() == typeof(TextBox))
//                    ((TextBox)obj).Text = "";
//            }
//            textBoxEstValue.Text=getMoney("0");
//            textBoxEstValuePrivate.Text=getMoney("0");
//            dbComps=null;
//            _compsInDb = false;
//        }
//        #endregion . clear form .
//        #region . start .
//        public void setInitialData(
//              string StreetNo
//            , string PreDir
//            , string StreetName
//            , string PostDir
//            , string StreetSuffix
//            , string UnitOrAppartment
//            , string ZIP
//            , string County
//            , string State
//            , double SqFt
//            , Guid PropertyGuid
//            , SqlConnection dbConn
//            )
//        {
//            clearForm();
//            originalSQFT = SqFt;
//            _dbConn = dbConn;

//            textBoxStreetNoA.Text =StreetNo;
//            textBoxPreDirA.Text = PreDir;
//            textBoxStreetA.Text = StreetName;
//            textBoxPostDirA.Text = PostDir;
//            textBoxSuffixA.Text = StreetSuffix;
//            textBoxUnitNoA.Text = UnitOrAppartment;
//            textBoxZipA.Text = ZIP;
//            textBoxCountyA.Text = County;
//            textBoxStateA.Text = State;
//            textBoxOrigSqFt.Text = getNumber(originalSQFT.ToString());
			
//            //TODO: Set response...
//            //response = 



//            dbComps = DealMaker.comps.Select(_dbConn, PropertyGuid);
//            if (dbComps.Length==0)
//            {
//                dbComps = new DealMaker.comps[1];
//                dbComps[0] = new DealMaker.comps(PropertyGuid, DateTime.Now, 0, 0,  null, "");
//                _compsInDb=false;
//            }
//            else
//            {
//                MemoryStream stream = new MemoryStream(dbComps[0].P_xmlStream);
//                XmlSerializer serializer = new XmlSerializer(typeof(DTD.Response.RESPONSE_GROUP));
//                response = (DTD.Response.RESPONSE_GROUP)serializer.Deserialize(stream);
//                if (CheckValid())
//                {
//                    //comps = response.
//                    //	RESPONSE.
//                    //	RESPONSE_DATA[0].
//                    //	PROPERTY_INFORMATION_RESPONSE.
//                    //	_PROPERTY_INFORMATION[0].
//                    //	_DATA_PROVIDER_COMPARABLE_SALES;

//                    //NEW dtd 4.2.2
//                    //BEGIN
//                    DTD.Response._PROPERTY_INFORMATION propInfo = null; 					
//                    foreach (object o in response.RESPONSE.RESPONSE_DATA[0].PROPERTY_INFORMATION_RESPONSE.Items)
//                    {
//                        if (o is DTD.Response._PROPERTY_INFORMATION)
//                        {
//                            propInfo = o as DTD.Response._PROPERTY_INFORMATION;
//                            break;
//                        }
//                    }

//                    if (null != propInfo)
//                        comps = propInfo._DATA_PROVIDER_COMPARABLE_SALES;
//                    //END

//                    try
//                    {
//                        textBoxDate.Text = dbComps[0].P_dateOfRetrieve.ToShortDateString();
//                    }
//                    catch{}
//                    try
//                    {
//                        textBoxEstValue.Text = getMoney(dbComps[0].P_estimatedValue.ToString());}
//                    catch{}
//                    try
//                    {
//                        textBoxEstValuePrivate.Text = getMoney(dbComps[0].P_privateEstimatedValue.ToString());}
//                    catch{}

//                }
//                FillList();
//                try
//                {
//                    string[] selectedArr = dbComps[0].P_taggedCSV.Split(new char[]{','});
//                    foreach(ListViewItem lvi in listViewPropertyProfilesComps.Items)
//                    {
//                        if(isInStringArray(selectedArr, lvi.SubItems[18].Text))
//                        {
//                            lvi.Checked=true;
//                        }
//                    }
//                }
//                catch
//                {
//                    //intended! no need to break if error...
//                }
//                _compsInDb=true;
//            }
//        }
//        #endregion
//        #region . CheckValid .
//        public bool CheckValid()
//        {
//            DTD.Response.STATUS status = null;
//            bool error = false;

//            try 
//            {
//                status = response.RESPONSE.STATUS[0];
//                if (!status._Condition.Trim().StartsWith("SUCCES"))
//                {

//                    MessageBox.Show(status._Description, status._Condition);
//                    error = true;
//                }
//            }
//            catch{};
//            try 
//            {
//                status =response.RESPONSE.RESPONSE_DATA[0].PROPERTY_INFORMATION_RESPONSE.STATUS;
//                if (!status._Condition.Trim().StartsWith("SUCCES"))
//                {
//                    MessageBox.Show(status._Description, status._Condition);
//                    error = true;
//                }
//            }
//            catch{};
//            try 
//            {				
//                DTD.Response._PRODUCT prod = response.RESPONSE.RESPONSE_DATA[0].PROPERTY_INFORMATION_RESPONSE.Items[0] as DTD.Response._PRODUCT;			
				
//                if (null != prod && null != prod.STATUS && prod.STATUS.Length > 0)
//                    status = prod.STATUS[0];

//                //END
//                if (!status._Condition.Trim().StartsWith("SUCCES"))
//                {
//                    MessageBox.Show(status._Description, status._Condition);
//                    error = true;
//                }
//            }
//            catch{};
//            try 
//            {
//                //DTD.Response._MULTIPLE_RECORDS[] multipla 
//                //	= response.RESPONSE.RESPONSE_DATA[0].PROPERTY_INFORMATION_RESPONSE._PROPERTY_INFORMATION[0]._MULTIPLE_RECORDS;
//                //dtd 4.2.2
//                //BEGIN
//#if !NEW_DTD
//                //OLD
//                DTD.Response._MULTIPLE_RECORDS[] multipla = response.RESPONSE.RESPONSE_DATA[0].PROPERTY_INFORMATION_RESPONSE._PROPERTY_INFORMATION[0]._MULTIPLE_RECORDS;
//#else
//                //NEW
//                DTD.Response._PROPERTY_INFORMATION propInfo = null;
//                DTD.Response._MULTIPLE_RECORDS[] multipla = null;

//                foreach (object o in response.RESPONSE.RESPONSE_DATA[0].PROPERTY_INFORMATION_RESPONSE.Items)
//                {
//                    if (o is DTD.Response._PROPERTY_INFORMATION)
//                    {
//                        propInfo = response.RESPONSE.RESPONSE_DATA[0].PROPERTY_INFORMATION_RESPONSE.Items[1] as DTD.Response._PROPERTY_INFORMATION;					
//                        break;
//                    }
//                }

//                if (null != propInfo)
//                    multipla = propInfo._MULTIPLE_RECORDS;
//#endif
//                //END
				
//                FormMultipleProperiesFound form = new FormMultipleProperiesFound();

//                form.lvFill(multipla);
//                form.ShowDialog();

//                int iterator = form.selected;
//                if (iterator>=0)
//                {
				
//                    try
//                    {
//                        textBoxStreetNoA.Text = getSecureString(multipla[iterator].PROPERTY._PARSED_STREET_ADDRESS._HouseNumber);
//                    }
//                    catch{}
//                    try
//                    {
//                        textBoxPreDirA.Text = getSecureString(multipla[iterator].PROPERTY._PARSED_STREET_ADDRESS._DirectionPrefix);
//                    }
//                    catch{}
//                    try
//                    {
//                        textBoxStreetA.Text = getSecureString(multipla[iterator].PROPERTY._PARSED_STREET_ADDRESS._StreetName);
//                    }
//                    catch{}
//                    try
//                    {
//                        textBoxPostDirA.Text = getSecureString(multipla[iterator].PROPERTY._PARSED_STREET_ADDRESS._DirectionSuffix);
//                    }
//                    catch{}
//                    try
//                    {
//                        textBoxSuffixA.Text = getSecureString(multipla[iterator].PROPERTY._PARSED_STREET_ADDRESS._StreetSuffix);
//                    }
//                    catch{}
//                    try
//                    {
//                        textBoxUnitNoA.Text = getSecureString(multipla[iterator].PROPERTY._PARSED_STREET_ADDRESS._ApartmentOrUnit);
//                    }
//                    catch{}
//                    try
//                    {
//                        textBoxZipA.Text = getSecureString(multipla[iterator].PROPERTY._PostalCode);
//                    }
//                    catch{}
//                    try
//                    {
//                        textBoxCountyA.Text = getSecureString(multipla[iterator].PROPERTY._County);
//                    }
//                    catch{}
//                    try
//                    {
//                        textBoxStateA.Text = getSecureString(multipla[iterator].PROPERTY._State);
//                    }
//                    catch{}
//                    try
//                    {
//                        tbAPN.Text = getSecureString(multipla[iterator].PROPERTY._AssessorsParcelIdentifier);
//                    }
//                    catch{}
//                }
//                error = true;
			
//            }
//            catch{};


//            listViewPropertyProfilesComps.Items.Clear();

//            return !error;

//            //FillList();
			
//        }
//        #endregion
//        #region . getComps .

//        private void buttonGetComps_reenginered_Click(object sender, EventArgs e) 
//        {
//            return;
//        }


//        private void buttonGetComps_Click(object sender, System.EventArgs e)
//        {
//            if (textBoxZipA.Text.Trim().Equals("")
//                &&
//                (textBoxCountyA.Text.Trim().Equals("")
//                ||
//                textBoxStateA.Text.Trim().Equals("")
//                )
//                ) 
//            {
//                MessageBox.Show("Please enter ZIP or County & State info!","Address Info Incomplete!");
//                return;
//            }

//            DialogResult dlg = 
//                MessageBox.Show
//                (
//                "You will incur charge or credit reduction!\nDo you want to continue?"
//                , "Warning!"
//                , MessageBoxButtons.YesNo
//                , MessageBoxIcon.Warning
//                );
//            if (dlg == DialogResult.No)
//            {
//                return;
//            }

//            int totalCost = 0;
//            int totalAmount = 0;
//            string errMsg;
//            int monthlyCredits;
//            int topUpCredits;
									
//            if (!UserCredits.Instance.TestForEnough(Globals.DataConsumptionReg.KeyCode /*DealMaker.Globals.UserRegistration.IDUser*/, TypeOfCharge.COMPS, 1, out totalCost, out totalAmount, out monthlyCredits, out topUpCredits, out errMsg))
//            {									
//                UserCreditForm ucf = new UserCreditForm(DealMaker.Globals.UserRegistration.IDUser, monthlyCredits, topUpCredits, totalAmount, TypeOfCharge.COMPS, 1, totalCost);
//                ucf.ShowDialog();
//                return;
//            }

//            /*string log_filename = @".\COMPS_usage_trace.log";
//            TextWriter txtwrtr = new StreamWriter(log_filename,
//                true, Encoding.UTF8);
//            txtwrtr.WriteLine
//                (
//                DateTime.Now.ToString("yyyy.MM.dd HH:mm:ss")
//                + " " +
//                "COMPS Requested"
//                );
//            txtwrtr.Flush();
//            txtwrtr.Close();*/

//            DTD.Request.REQUEST_GROUP request = buildRequest();
			
//#if DUMP_XML_OBJECT
//            dumpObject(request, "_REQUEST.xml");
//#endif

//            XmlSerializer requestSerializer  
//                = new XmlSerializer(typeof(DTD.Request.REQUEST_GROUP));
//            XmlSerializer responseSerializer  
//                = new XmlSerializer(typeof(DTD.Response.RESPONSE_GROUP));

//            MemoryStream requestStream = new MemoryStream();
//            MemoryStream responseStream = new MemoryStream();

//            // IMPORTANT  -  we do not need a namespace!
//            System.Xml.Serialization.XmlSerializerNamespaces nmspace = new XmlSerializerNamespaces();
//            nmspace.Add("","");
//            // IMPORTANT  -  we do not need a namespace!
				
//            System.Xml.XmlWriter writer = new XmlTextWriter(requestStream, System.Text.UTF8Encoding.UTF8);
//            //writer.Formatting = System.Xml.Formatting.Indented;
//            writer.WriteStartDocument();
//            //TODO: define string for request..
//            writer.WriteDocType("REQUEST_GROUP", null, "C2DRequestv2.0.dtd",null);
//            requestSerializer.Serialize(writer, request,nmspace);
//            writer.Flush();
	
//            //requestSerializer.Serialize(requestStream, request);
//            byte[] requestArray = null;
//            byte[] responseArray = null;
//            string resultMessage = string.Empty;
//            try
//            {
//                Cursor.Current = Cursors.WaitCursor;
//                Application.DoEvents();

//                int count =(int)requestStream.Length-3;
//                requestArray = new byte[count];
//                long newPos = requestStream.Seek(0, SeekOrigin.Begin);
		
//                //TA++:08.04.2005
//                byte[] reqStream = requestStream.ToArray();
//                for (int i = 3; i < requestStream.Length; i++)
//                {
//                    requestArray[i - 3] = (byte)reqStream[i];
//                }
//                //TA--:08.04.2005
//                writer.Close();

//                // ++ web
//                //DataConsumption.ServiceDataConsumption sdc
//                sdc	= new DealMaker.DataConsumption.ServiceDataConsumption();
			
//                if (string.Empty != Globals.ProfitGrabberWebSupportURLDataConsumption)
//                {
//                    sdc.Url = Globals.ProfitGrabberWebSupportURLDataConsumption;
//                }
		
//                sdc.RequestDataCOMPS2(
//                    Globals.DataConsumptionReg,
//                    requestArray,
//                    out responseArray, 
//                    out resultMessage);

//                //sdc = null;

//                if (null == responseArray)
//                {
//                    if (string.Empty == resultMessage)
//                    {
//                        MessageBox.Show(this, "Web services did not answer correctly.", "Web Services", MessageBoxButtons.OK, MessageBoxIcon.Information);
//                    }
//                    else
//                    {
//                        if (resultMessage == Globals.NoCreditForGivenMonth)
//                        {
//                            MessageBox.Show(this, "You don't have any credits for this month!\nGo to Property Data on the menu above, and select Subscribe To Data!", "Get the data advantage!", MessageBoxButtons.OK, MessageBoxIcon.Information);
//                        }
//                        else
//                        {
//                            MessageBox.Show(this, resultMessage, "Web Services", MessageBoxButtons.OK, MessageBoxIcon.Information);
//                        }
//                    }
//                    return;
//                }

//                if (0==responseArray.Length)
//                {
//                    MessageBox.Show(this, "Web services answered incorrectly.", "Web Services", MessageBoxButtons.OK, MessageBoxIcon.Information);
//                    return;
//                    //TA++28.06.2005
//                    //COMPS
//                    //BEGIN
//                    //OLD++
//                    //throw new Exception
//                        //(resultMessage + " - at GetComps");
//                    //OLD--
//                    //NEW++
//                    //string message = string.Empty;
//                    //int iPos1 = resultMessage.IndexOf("#");
//                    //int iPos2 = resultMessage.IndexOf("#", iPos1 + 1);
//                    //if (-1 != iPos1 && -1 != iPos2)
//                    //	message = resultMessage.Substring(iPos1 + 1, iPos2 - (iPos1 + 1));
//                    //else
//                    //	message = resultMessage;
//                    //MessageBox.Show(this, message, string.Empty, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
//                    //return;
//                    //NEW--
//                    //END
//                }
//                else
//                {
//                    //TA: WE take out LOGS
//                    /*txtwrtr = new StreamWriter(log_filename,
//                        true, Encoding.UTF8);
//                    txtwrtr.WriteLine
//                        (
//                        DateTime.Now.ToString("yyyy.MM.dd HH:mm:ss")
//                        + " " +
//                        "COMPS Request Successful!"
//                        );
//                    txtwrtr.Flush();
//                    txtwrtr.Close();*/
//                }
		
//                // -- web

//                // ++ local
//                //			SoftwareRegistration.ClassLibrary.WebServiceCore.Connect(requestArray,
//                //				out responseArray);
//                // -- local

//                //			System.IO.FileStream fs = new FileStream("response2.xml", FileMode.Create);
//                //			fs.Write((byte[])responseStream.ToArray(), 0, (int)responseStream.Length);
//                //			fs.Flush();
//                //			fs.Close();

//                responseStream.Write(responseArray,0,responseArray.Length);

//                responseStream.Position = 0;
//                response = (DTD.Response.RESPONSE_GROUP)responseSerializer.Deserialize(responseStream);
//                if (CheckValid())
//                {

//                    //TODO: SAVE DATA AND STORE COMPS
//                    DateTime storedDate = DateTime.Now;
//                    if (null == dbComps || null == dbComps[0])
//                    {
//                        dbComps = new DealMaker.comps[1];
//                        dbComps[0] = new DealMaker.comps(new Guid(), DateTime.Now, 0, 0,  null,"");
//                    }

//                    dbComps[0].P_xmlStream = responseStream.ToArray();
//                    dbComps[0].P_dateOfRetrieve = storedDate;
//                    dbComps[0].P_taggedCSV="";
//                    dbComps[0].P_estimatedValue=0;
//                    textBoxEstValue.Text=getMoney("0");

//                    textBoxDate.Text = storedDate.ToShortDateString();
//                    //comps = response.
//                    //	RESPONSE.
//                    //	RESPONSE_DATA[0].
//                    //	PROPERTY_INFORMATION_RESPONSE.
//                    //	_PROPERTY_INFORMATION[0].
//                    //	_DATA_PROVIDER_COMPARABLE_SALES;

//                    //dtd 4.2.2
//                    //BEGIN
//#if !NEW_DTD
//                    //OLD
//                    comps = response.RESPONSE.RESPONSE_DATA[0].PROPERTY_INFORMATION_RESPONSE._PROPERTY_INFORMATION[0]._DATA_PROVIDER_COMPARABLE_SALES;
//#else
//                    //NEW						
//                    DTD.Response._PROPERTY_INFORMATION propInfo = null; //response.RESPONSE.RESPONSE_DATA[0].PROPERTY_INFORMATION_RESPONSE.Items[0] as DTD.Response._PROPERTY_INFORMATION;
					

//                    foreach (object o in response.RESPONSE.RESPONSE_DATA[0].PROPERTY_INFORMATION_RESPONSE.Items)
//                    {
//                        if (o is DTD.Response._PROPERTY_INFORMATION)
//                        {
//                            propInfo = o as DTD.Response._PROPERTY_INFORMATION;
//                            break;
//                        }
//                    }

//                    if (null != propInfo)
//                        comps = propInfo._DATA_PROVIDER_COMPARABLE_SALES;
//#endif					
//                    //END

//                    FillList();

//                    //AUTOSAVE
//                    if (null != COMPS_Downloaded_Successfully && _allowAutoSaveTriggerEvents)
//                        COMPS_Downloaded_Successfully();									

//                    try
//                    {
//                        string outMsg = string.Empty;
//                        //0 = COMPS
//                        //1 = PP
//                        //2 = Matching Module
//                        string msg;
//                        Guid userId = DealMaker.Globals.UserRegistration.IDUser;

//                        if (Globals.NwVerEnabled && null != Globals.activeModules && Globals.activeModules.MOD_6)
//                            sdc.QueryGuidFromKey(Globals.DataConsumptionReg.KeyCode, out userId, out msg);

//                        bool bRes = sdc.ConfirmDataRequestSuccess(userId/*DealMaker.Globals.UserRegistration.IDUser*/, 1, 0, out outMsg);

//#if DEBUG 
//                        if (true == bRes)
//                        {
//                            MessageBox.Show("Charged!");
//                        }
//                        else
//                        {
//                            MessageBox.Show("PROBLEM IN CHARGE: " + outMsg);
//                        }
//#endif
//                    }
//                    catch(Exception 
//#if DEBUG
//                        exc
//#endif
//                        )
//                    {
//#if DEBUG
//                        MessageBox.Show(exc.ToString());
//#endif
//                    }
//                }				
//            }
//            catch(Exception ex)
//            {
//                //MessageBox.Show(this, "Too many connections on server.\nTry again later.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Information);
////#if DEBUG
//                MessageBox.Show(ex.Message + "\n" + ex.InnerException);
////#endif
//                //throw ex;
//            }
//            finally
//            {
//                Cursor.Current = Cursors.Default;
//            }
//            sdc = null;
//            return;
//            #region . commented ... useful .			
//            /*
//                 *  EXAMPLE HOW TO CONVERT STRING TO OBJECT
//                 *  NOTE - ALWAYS GO DOWN TO MEMORY STREAM!!!
//                 * 
//                string xml = 
//                    "<?xml version='1.0' encoding='UTF-8'?>"
//                    +"<RESPONSE_GROUP>"
//                    +"<RESPONDING_PARTY/>"
//                    +"<RESPOND_TO_PARTY _Name='null'/>"
//                    +"<RESPONSE ResponseDateTime='Wed Feb 23 01:58:59 PST 2005' InternalAccountIdentifier='null' _JobIdentifier='null'>"
//                    +"<RESPONSE_DATA>"
//                    +"<PROPERTY_INFORMATION_RESPONSE>"
//                    +"<STATUS _Condition='FAILURE           ' _Code='0005' _Description='XML IS NOT WELL-FORMED OR IS INVALID. ERROR ON LINE 1: CONTENT IS NOT ALLOWED IN PROLOG.'/>"
//                    +"</PROPERTY_INFORMATION_RESPONSE>"
//                    +"</RESPONSE_DATA>"
//                    +"</RESPONSE>"
//                    +"</RESPONSE_GROUP>";

//                System.IO.MemoryStream stream = new MemoryStream();
//                stream.Write(UTF8Encoding.UTF8.GetBytes(xml),0,UTF8Encoding.UTF8.GetByteCount(xml));
//                stream.Seek(0,SeekOrigin.Begin);
			
//                System.Xml.Serialization.XmlSerializer deser 
//                    = new XmlSerializer(typeof(DTD.Response.RESPONSE_GROUP));

//                DTD.Response.RESPONSE_GROUP x 
//                    = deser.Deserialize(stream)
//                    as DTD.Response.RESPONSE_GROUP;
//                */
		
	
//            //RESPONSE_GROUP y = DeserializeXmlFile(@"c:\xml\dcrr.xml");
			

//            // dump request to see how it looks

//            /*
//            string sNow = DateTime.Now.ToString("yyMMddHHmmss");

//            DTD.Request.REQUEST_GROUP rqg = buildRequest();

//            //			dumpObject(rqg, "request.xml");
//            //return;
//            // create serializers
//            XmlSerializer requestSerializer = 
//                new XmlSerializer(typeof(DTD.Request.REQUEST_GROUP));
//            XmlSerializer responseSerializer = 
//                new XmlSerializer(typeof(DTD.Response.RESPONSE_GROUP));

//            MemoryStream stream = new MemoryStream();
//            UTF8Encoding encoding = new UTF8Encoding();


//            //xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
//            // IMPORTANT  -  we do not need a namespace!
//            System.Xml.Serialization.XmlSerializerNamespaces nmspace = new XmlSerializerNamespaces();
//            nmspace.Add("","");
//            // IMPORTANT  -  we do not need a namespace!
				
//            System.Xml.XmlTextWriter xmlWriter = new XmlTextWriter(stream, encoding);
//            xmlWriter.Formatting = System.Xml.Formatting.Indented;
//            xmlWriter.WriteStartDocument();
//            xmlWriter.WriteDocType("REQUEST_GROUP", null, "C2DRequestv2.0.PropertyProfileComps",null);
//            requestSerializer.Serialize(xmlWriter, rqg, nmspace);
//            xmlWriter.Flush();
//            //xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx



//            //get previusly prepared object
//            //XmlDocument xmldoc = new XmlDocument();
//            //xmldoc.Load(@"request_short_comparable_report.xml");
//            //string xml = xmldoc.InnerXml.ToString();

//            // get serialized object ready for sending
//            //requestSerializer.Serialize(stream,rqg);
//            string xml = encoding.GetString(stream.ToArray());
//            xml = 
//                //"xml=" + 
//                //HttpUtility.UrlEncode(xml);
//                xml;

//            Trace.WriteLine("sent: " + xml);
//            Trace.Flush();

//            //xxxxxxxxxxxxxxxxxxxxxxxxxxx
//            System.IO.FileStream edc = new FileStream(sNow + "request.xml",System.IO.FileMode.Create);
//            edc.Write(stream.ToArray(), 0, (int)stream.Length);
//            edc.Flush();
//            edc.Close();
//            //xxxxxxxxxxxxxxxxxxxxxxxxxxx


//            try
//            {
//                Trace.WriteLine("deserializing");
//                DTD.Response.RESPONSE_GROUP rsg 
//                    = responseSerializer.Deserialize(responseStream) 
//                    as DTD.Response.RESPONSE_GROUP;

//                response.Close();
//                stream.Close();

				
//                Trace.WriteLine("serializing response");
//                dumpObject(rsg, sNow + "response.xml");


//                //RESPONSE_GROUP rsg = DeserializeXmlFile(@"response.xml");
				
//                Trace.WriteLine("Filling form");
//                if (null == rsg)
//                {
//                    Trace.WriteLine("null == rsg");
//                }
//                Start(rsg,1200);
//                Trace.WriteLine("Filled form");


//                //_PROPERTY_INFORMATION x = 
//                //	y.RESPONSE.RESPONSE_DATA[0].PROPERTY_INFORMATION_RESPONSE.Items[1]
//                //	as _PROPERTY_INFORMATION;
//                //comps =  x._DATA_PROVIDER_COMPARABLE_SALES;
//                //lvFill();
//            }
//            catch (Exception wex)
//            {
//                System.Diagnostics.Debug.WriteLine(wex.Message);
//                MessageBox.Show(wex.Message);
//                Trace.WriteLine("exception " + wex.Message );
//                Trace.WriteLine(wex.StackTrace);
//                Trace.WriteLine(wex.InnerException);
//                Trace.WriteLine(wex.Source);
//                throw wex;
//            }

//            return;
			
//            */
//            #endregion . commented ... useful .			
//        }
//        #endregion
//        #region . buildRequest .
//        private DTD.Request.REQUEST_GROUP buildRequest()
//        {

//            DTD.Request.REQUEST_GROUP rqg 
//                = new DTD.Request.REQUEST_GROUP();

//            DTD.Request._SEARCH_CRITERIA sc
//                = new DTD.Request._SEARCH_CRITERIA();

//            DTD.Request._COMPARABLE_SEARCH cmps
//                = new DTD.Request._COMPARABLE_SEARCH();

//            DTD.Request._PROPERTY_CRITERIA pc
//                = new DTD.Request._PROPERTY_CRITERIA();


//            rqg.REQUEST 
//                = new DTD.Request.REQUEST();
//            rqg.REQUEST.REQUESTDATA 
//                = new DTD.Request.REQUESTDATA[1];
//            rqg.REQUEST.REQUESTDATA[0] 
//                = new DTD.Request.REQUESTDATA();
//            rqg.REQUEST.REQUESTDATA[0].PROPERTY_INFORMATION_REQUEST 
//                = new DTD.Request.PROPERTY_INFORMATION_REQUEST();
//            rqg.REQUEST.REQUESTDATA[0].PROPERTY_INFORMATION_REQUEST._SEARCH_CRITERIA 
//                = new DTD.Request._SEARCH_CRITERIA();

//            DTD.Request._CONNECT2DATA_PRODUCT c2dp
//                = new DTD.Request._CONNECT2DATA_PRODUCT();

//            c2dp._DetailedComparableReport 
//                = DTD.Request._CONNECT2DATA_PRODUCT_DetailedComparableReport.Y;

//            rqg.REQUEST.REQUESTDATA[0].PROPERTY_INFORMATION_REQUEST._CONNECT2DATA_PRODUCT = c2dp;
			
//            rqg.REQUEST.REQUESTDATA[0].PROPERTY_INFORMATION_REQUEST._ActionType 
//                = DTD.Request.PROPERTY_INFORMATION_REQUEST_ActionType.Submit;
//            rqg.REQUEST.REQUESTDATA[0].PROPERTY_INFORMATION_REQUEST._ActionTypeSpecified = true;

//            DTD.Request._RESPONSE_CRITERIA rescrit 
//                = new DTD.Request._RESPONSE_CRITERIA();

//            rescrit._NumberComparablesType 
//                = DTD.Request._RESPONSE_CRITERIA_NumberComparablesType.Item50;
//            rescrit._NumberCompFarmRecordsType
//                = DTD.Request._RESPONSE_CRITERIA_NumberCompFarmRecordsType.Item100;
//            rescrit._NumberSubjectPropertiesType
//                = DTD.Request._RESPONSE_CRITERIA_NumberSubjectPropertiesType.Item100;



//            rqg.REQUEST.REQUESTDATA[0].PROPERTY_INFORMATION_REQUEST._RESPONSE_CRITERIA
//                = rescrit;
 
//            pc.PARSED_STREET_ADDRESS = new DTD.Request.PARSED_STREET_ADDRESS();

//            pc.PARSED_STREET_ADDRESS._StreetName = textBoxStreetA.Text;
//            pc.PARSED_STREET_ADDRESS._HouseNumber = textBoxStreetNoA.Text;
//            pc.PARSED_STREET_ADDRESS._ApartmentOrUnit = textBoxUnitNoA.Text;

//            pc.PARSED_STREET_ADDRESS._DirectionPrefix = textBoxPreDirA.Text;
//            pc.PARSED_STREET_ADDRESS._DirectionSuffix = textBoxPostDirA.Text;
//            pc.PARSED_STREET_ADDRESS._StreetSuffix = textBoxSuffixA.Text;

//            pc._PostalCode = textBoxZipA.Text;

//            pc._County = textBoxCountyA.Text;
//            pc._State = textBoxStateA.Text;

//            /*
//            pc._StreetAddress = textBoxAddress.Text;
//            pc._PostalCode = textBoxSiteZIP.Text;
//            int a = textBoxSiteCityST.Text.IndexOf("/");
//            if (a == -1) 
//            {
//                a = textBoxSiteCityST.Text.Length;
//            }
//            else
//            {
//                pc._State = textBoxSiteCityST.Text.Substring(a+1,textBoxSiteCityST.Text.Length-1-a);
//            }
//            pc._City = textBoxSiteCityST.Text.Substring(0,a);
//            */

//            DTD.Request._LAND_USE lu = new DTD.Request._LAND_USE();
//            //lu._ResidentialType 
//            //	= DTD.Request._LAND_USE_ResidentialType.SingleFamilyResidential;
//            //lu._ResidentialTypeSpecified = true;

//            //pc._LAND_USE = new DTD.Request._LAND_USE[1];
//            //pc._LAND_USE[0] = lu;

//            rqg.REQUEST.REQUESTDATA[0].PROPERTY_INFORMATION_REQUEST._PROPERTY_CRITERIA = pc;

////TA++
//// APN SEARCH FOR HAWAII
////			DTD.Request._SUBJECT_SEARCH ss = new DTD.Request._SUBJECT_SEARCH();
////			ss._AssessorsParcelIdentifier = "1-9-5-014-022-0000";
////			rqg.REQUEST.REQUESTDATA[0].PROPERTY_INFORMATION_REQUEST._SEARCH_CRITERIA = new DTD.Request._SEARCH_CRITERIA();
////			rqg.REQUEST.REQUESTDATA[0].PROPERTY_INFORMATION_REQUEST._SEARCH_CRITERIA.Item = ss;
////TA--
			
//            //cmps._PoolOptionType = DTD.Request._COMPARABLE_SEARCH_PoolOptionType.PropertiesWithAndWithoutPools;
//            cmps._GeographicConstraintTypeSpecified = true;
//            cmps._GeographicConstraintType = DTD.Request._COMPARABLE_SEARCH_GeographicConstraintType.SameCity;
			
//            cmps._MonthsBackNumber=comboBoxSalesInLast.Text.TrimEnd(new char[] {' ','m','o', '.'});
//            cmps._LivingAreaVariancePercent=comboBoxSqFtRange.Text.Trim(new char[] {'+','-','/', '%',' '});
//            cmps._DistanceFromSubjectNumber=comboBoxSearchRadius.Text;

//            DTD.Request._LAND_USE lu2 = new DTD.Request._LAND_USE();
//            lu2._SameAsSubjectType 
//                = DTD.Request._LAND_USE_SameAsSubjectType.Yes;
//            lu2._SameAsSubjectTypeSpecified=true;

//            cmps._LAND_USE = new DTD.Request._LAND_USE[1];
//            cmps._LAND_USE[0] = lu2;

//            //rqg.REQUEST.REQUESTDATA[0].PROPERTY_INFORMATION_REQUEST._SEARCH_CRITERIA.Item 
//            //	= cmps;
//            //dtd 4.2.2
//            //BEGIN
//#if !NEW_DTD
//            //OLD
//            rqg.REQUEST.REQUESTDATA[0].PROPERTY_INFORMATION_REQUEST._SEARCH_CRITERIA.Item = cmps;
//#else
//            //NEW
//            if (string.Empty != tbAPN.Text)
//            {
//                DTD.Request._SUBJECT_SEARCH ss = new DTD.Request._SUBJECT_SEARCH();
//                ss._AssessorsParcelIdentifier = tbAPN.Text;
//                rqg.REQUEST.REQUESTDATA[0].PROPERTY_INFORMATION_REQUEST._SEARCH_CRITERIA.Items = new object[2] { ss, cmps };	
//            }
//            else
//                rqg.REQUEST.REQUESTDATA[0].PROPERTY_INFORMATION_REQUEST._SEARCH_CRITERIA.Items = new object[1] { cmps };
//#endif
//            //END

//            rqg.REQUESTING_PARTY 
//                = new DTD.Request.REQUESTING_PARTY[1];
			
//            rqg.MISMOVersionID = "2.1";
			
//            /*
//            rqg.REQUESTING_PARTY[0] 
//                = new DTD.Request.REQUESTING_PARTY();
			
//            rqg.REQUESTING_PARTY[0]._Name="Blue Sky Lending"; 
//            rqg.REQUESTING_PARTY[0]._StreetAddress="123 Main"; 
//            rqg.REQUESTING_PARTY[0]._City="Anaheim"; 
//            rqg.REQUESTING_PARTY[0]._State="Ca"; 
//            rqg.REQUESTING_PARTY[0]._PostalCode="92840";

//            rqg.REQUESTING_PARTY[0].PREFERRED_RESPONSE
//                = new DTD.Request.PREFERRED_RESPONSE[1];

//            rqg.REQUESTING_PARTY[0].PREFERRED_RESPONSE[0]
//                = new DTD.Request.PREFERRED_RESPONSE();

//            rqg.REQUESTING_PARTY[0].PREFERRED_RESPONSE[0]._Format="XML";
//            rqg.REQUESTING_PARTY[0].PREFERRED_RESPONSE[0]._DestinationDescription="";

//            rqg.RECEIVING_PARTY = new DTD.Request.RECEIVING_PARTY();

//            rqg.RECEIVING_PARTY._Name="Blue Sky Lending"; 
//            rqg.RECEIVING_PARTY._StreetAddress="123 Main";
//            rqg.RECEIVING_PARTY._City="Anaheim";
//            rqg.RECEIVING_PARTY._State="Ca";
//            rqg.RECEIVING_PARTY._PostalCode="92840";
//            */
			
												
//            //OLD
//            //rqg.REQUEST.LoginAccountPassword="GRABR1"; 
//            //rqg.REQUEST.LoginAccountPassword="MRUBEL01"; 
//            //rqg.REQUEST.LoginAccountIdentifier="PROFGRAB";
//            //rqg.REQUEST.LoginAccountPassword="C2STAGE"; 
//            //rqg.REQUEST.LoginAccountIdentifier="RU316149";


//            //NEW
//            //rqg.REQUEST.LoginAccountIdentifier = "";
//            //rqg.REQUEST.LoginAccountPassword=""; 
//            //rqg.REQUEST._JobIdentifier="TEST MASTER V2.0"; 
//            //rqg.REQUEST._RecordIdentifier="12345A8";

//            rqg.REQUEST._HVERequestTypeSpecified = true;
//            rqg.REQUEST._HVERequestType 
//                = DTD.Request.REQUEST_HVERequestType.Item02;
//            rqg.REQUEST._HVCustomerIdentifier="";

//            return rqg;
//        }
//        #endregion
//        #region . dumpObject to file .
//        private void dumpObject(object theObject, string filename)
//        {
//#if DUMP_XML_OBJECT
//            System.Xml.Serialization.XmlSerializer serializer
//                = new System.Xml.Serialization.XmlSerializer(theObject.GetType());

//            // IMPORTANT  -  we do not need a namespace!
//            System.Xml.Serialization.XmlSerializerNamespaces nmspace = new XmlSerializerNamespaces();
//            nmspace.Add("","");
//            // IMPORTANT  -  we do not need a namespace!
				
//            System.Xml.XmlTextWriter writer = new XmlTextWriter(filename, System.Text.UTF8Encoding.UTF8);
//            writer.Formatting = System.Xml.Formatting.Indented;
//            writer.WriteStartDocument();
//            //TODO: define string for request..
//            writer.WriteDocType("REQUEST_GROUP", null, "C2DRequestv2.0.PropertyProfileComps",null);
//            serializer.Serialize(writer, theObject,nmspace);
//            writer.Flush();
//            writer.Close();
//#endif
//        }
//        #endregion
//        #region . deserialize .
//        private DTD.Response.RESPONSE_GROUP DeserializeXmlFile(string path)
//        {
//            DTD.Response.RESPONSE_GROUP x ;
//            FileStream fs = new FileStream(path, FileMode.Open);
			
//            XmlSerializer serializer = new XmlSerializer(typeof(DTD.Response.RESPONSE_GROUP));
//            x = (DTD.Response.RESPONSE_GROUP)serializer.Deserialize(fs);;
//            fs.Close();
//            return x;
//        }
//        #endregion
//        #region ˇ FillList ˇ
//        private void FillList()
//        {
			
//            ListViewItem lvItem;
//            this.htPrintMappingInfo.Clear();

//            Guid _printId = new Guid();
						
//            for(
//                int iterator = comps.GetLowerBound(0); 
//                iterator <= comps.GetUpperBound(0); 
//                iterator++
//                )
//            {
//                string cel01="";
//                string cel02="";
//                string cel03="";
//                string cel04="";
//                string cel05="";
//                string cel06="";
//                string cel07="";
//                string cel08="";
//                string cel09="";
//                string cel10="";
//                string cel11="";
//                string cel12="";
//                string cel13="";
//                string cel14="";
//                string cel15="";
//                string cel16="";
//                string cel17="";
//                string cel18="";

//                if (null == comps[iterator].PROPERTY)
//                    continue;

//                _printId = Guid.NewGuid();

//                try
//                {
//                    this.htPrintMappingInfo.Add(_printId, comps[iterator]);
//                }
//                catch {}

//                try
//                {
//                    cel01 = getSecureString(comps[iterator]._DistanceFromSubjectNumber);
//                }
//                catch{}
//                try
//                {
//                    cel02 = getSecureString(comps[iterator].PROPERTY._SubdivisionIdentifier);
//                }
//                catch{}
//                try
//                {
//                    cel03 = getNumber(getSecureString(comps[iterator].PROPERTY._PROPERTY_CHARACTERISTICS._IMPROVEMENTS._ROOM_COUNT._TotalLivingAreaSquareFeetNumber));
//                }
//                catch{}
//                try
//                {
//                    cel04 = getSecureString(comps[iterator].PROPERTY._PROPERTY_CHARACTERISTICS._IMPROVEMENTS._GENERAL_DESCRIPTION._YearBuiltDateIdentifier);
//                }
//                catch{}
//                try
//                {
//                    cel05 = getSecureString(comps[iterator].PROPERTY._PROPERTY_CHARACTERISTICS._IMPROVEMENTS._ROOM_COUNT._TotalRoomCount);
//                }
//                catch{}
//                try
//                {
//                    cel06 = getSecureString(comps[iterator].PROPERTY._PROPERTY_CHARACTERISTICS._IMPROVEMENTS._ROOM_COUNT._TotalBedroomsCount);
//                }
//                catch{}
//                try
//                {
//                    cel07 = getSecureString(comps[iterator].PROPERTY._PROPERTY_CHARACTERISTICS._IMPROVEMENTS._ROOM_COUNT._TotalBathsCount);
//                }
//                catch{}
//                try
//                {
//                    cel08 = getSecureString(comps[iterator].PROPERTY._PROPERTY_CHARACTERISTICS._IMPROVEMENTS._FEATURES._POOL._HasFeatureIndicator);
//                }
//                catch{}
//                try
//                {					
//                    string ss = getSecureString(comps[iterator].PROPERTY._PROPERTY_HISTORY._SALES_HISTORY[0]._LastSalesDate);
//                    string sd = getDate(ss);					
//                    try
//                    {
//                        string temp = Convert.ToDateTime(sd).ToString("yyyy/MM/dd");
//                        cel09 = temp;
//                    }
//                    catch
//                    {
//                        cel09 = sd;
//                    }
					
//                    //cel09 = getDate(getSecureString(comps[iterator].PROPERTY._PROPERTY_HISTORY._SALES_HISTORY[0]._LastSalesDate));					
//                }
//                catch{}
//                try
//                {
//                    cel10 = getMoney(getSecureString(comps[iterator].PROPERTY._PROPERTY_HISTORY._SALES_HISTORY[0]._LastSalesPriceAmount));
//                }
//                catch{}
//                try
//                {
//                    cel11 = getMoney(getSecureString(comps[iterator].PROPERTY._PROPERTY_HISTORY._SALES_HISTORY[0]._PricePerSquareFootAmount));
//                }
//                catch{}
//                try
//                {
//                    cel12 = getNumber(getSecureString(comps[iterator].PROPERTY._PROPERTY_CHARACTERISTICS._SITE._DIMENSIONS._LotAreaSquareFeetNumber));
//                }
//                catch{}
//                try
//                {
//                    cel13 = getMoney(getSecureString(comps[iterator].PROPERTY._PROPERTY_TAX._TotalAssessedValueAmount));
//                }
//                catch{}
//                try
//                {
//                    cel14 = getSecureString(comps[iterator].PROPERTY._PARSED_STREET_ADDRESS._HouseNumber);
//                }
//                catch{}
//                try
//                {
//                    cel15 = getSecureString(comps[iterator].PROPERTY._PARSED_STREET_ADDRESS._StreetName);					
//                }
//                catch{}
//                try
//                {
//                    cel16 = getSecureString(comps[iterator].PROPERTY._PostalCode);
//                }
//                catch{}
//                try
//                {
//                    cel17 = getSecureString(comps[iterator].PROPERTY._City);
//                }
//                catch{}
//                try
//                {
//                    cel18 = getSecureString(comps[iterator]._ComparableNumber);
//                }
//                catch{}

//                lvItem = new ListViewItem
//                    (new string[] {
//                                      ""
//                                      ,cel01
//                                      ,cel02
//                                      ,cel03
//                                      ,cel04
//                                      ,cel05
//                                      ,cel06
//                                      ,cel07
//                                      ,cel08
//                                      ,cel09
//                                      ,cel10
//                                      ,cel11
//                                      ,cel12
//                                      ,cel13
//                                      ,cel14
//                                      ,cel15
//                                      ,cel16
//                                      ,cel17
//                                      ,cel18
//                });
//                lvItem.Tag = _printId;
//                listViewPropertyProfilesComps.Items.Add(lvItem);
//            }
//        }
//        #endregion
//        #region ˇ lvPropertyItems_ColumnClick ˇ

//        private void lvPropertyItems_ColumnClick(object sender, System.Windows.Forms.ColumnClickEventArgs e)

//        {

//            // Set the ListViewItemSorter property to a new ListViewItemComparer 

//            // object. Setting this property immediately sorts the 

//            // ListView using the ListViewItemComparer object.                

//            this.asc = !this.asc;

//            if (true == this.asc)

//                this.listViewPropertyProfilesComps.ListViewItemSorter = new ListViewItemComparer(e.Column);

//            else

//                this.listViewPropertyProfilesComps.ListViewItemSorter = new ListViewItemComparerDesc(e.Column);

 

//        }

//        #endregion ˇ lvPropertyItems_ColumnClick ˇ
//        #region . tagging buttons .
//        private void buttonTagAll_Click(object sender, System.EventArgs e)
//        {
//            foreach(ListViewItem lvi in listViewPropertyProfilesComps.Items)
//            {
//                lvi.Checked = true;
//            }
//        }

//        private void buttonUntagAll_Click(object sender, System.EventArgs e)
//        {
//            foreach(ListViewItem lvi in listViewPropertyProfilesComps.Items)
//            {
//                lvi.Checked = false;
//            }
		
//        }

//        private void buttonInvert_Click(object sender, System.EventArgs e)
//        {
//            foreach(ListViewItem lvi in listViewPropertyProfilesComps.Items)
//            {
//                lvi.Checked = !lvi.Checked;
//            }
//        }
//        #endregion
//        #region . recalculate .
//        private void buttonRecalculate_Click(object sender, System.EventArgs e)
//        {
//            if (0==listViewPropertyProfilesComps.Items.Count)
//            {
//                return;
//            }
//            string strSelected = "";
//            double dorigsqft;
//            try
//            {
//                dorigsqft = Convert.ToDouble(textBoxOrigSqFt.Text);
//                if (0==dorigsqft)
//                    throw new Exception();
//            }
//            catch
//            {
//                MessageBox.Show("Please enter the Subject Property Sq. Ft. before Recalculating");
//                textBoxEstValue.Text = getMoney("0");
//                return;
//            }

//            double estimate = 0;
//            int counter = 0;
//            try
//            {
//                foreach(ListViewItem lvi in listViewPropertyProfilesComps.Items)
//                {
//                    if (lvi.Checked)
//                    {
//                        string price =lvi.SubItems[10].Text.Replace("$","");

//                        string sqft = lvi.SubItems[3].Text;
//                        double dprice;
//                        double dsqft;
//                        try
//                        {
//                            dprice = Convert.ToDouble(price);
//                            dsqft = Convert.ToDouble(sqft);

//                        }
//                        catch
//                        {
//                            MessageBox.Show("Please Tag only the properties that have the Sales Price and Sq. Ft.!");
//                            textBoxEstValue.Text = "";
//                            return;
//                        }

//                        estimate = estimate + (dprice / dsqft);
//                        counter++;
//                        strSelected = strSelected + lvi.SubItems[18].Text + ",";
//                    }
//                }
//                if (counter==0)
//                {
//                    MessageBox.Show("You must select (tag) at least one property!");
//                    textBoxEstValue.Text = "";
//                    return;
//                }
//                strSelected = strSelected.Substring(0,strSelected.Length-1);
				
//                estimate = estimate / counter;
//                estimate = estimate * dorigsqft;
//                _estimatedValue = (decimal)estimate;
				
//                dbComps[0].P_taggedCSV = strSelected;
//                dbComps[0].P_estimatedValue = (decimal)estimate;
				
//                textBoxEstValue.Text = getMoney(estimate.ToString());
//                //TA++, 04.04.2005. Added due to additional user request - as quickly as possible
//                try
//                {
//                    object parent = this.Parent;
//                    if (parent is TabPage)
//                    {
//                        TabPage tp = (TabPage)parent;
//                        parent = tp.Parent;
//                        if (parent is TabControl)
//                        {
//                            TabControl tc = (TabControl)parent;
//                            parent = tc.Parent;
//                            if (parent is Panel)
//                            {
//                                Panel p = (Panel)parent;
//                                parent = p.Parent;
//                                if (parent is ContactScreen)
//                                {
//                                    ContactScreen sc = (ContactScreen)parent;
//                                    System.IFormatProvider format =	new System.Globalization.CultureInfo("en-US", true);
//                                    sc.ARV = _estimatedValue.ToString("c", format);
//                                }
//                            }
//                        }
//                    }
//                }
//                catch (Exception)
//                {
//                    //No Action !!!
//                }
//                //TA--, 04.04.2005. Added due to additional user request - as quickly as possible
//            }
//            catch
//            {
//                MessageBox.Show("Error calculating data", "", MessageBoxButtons.OK, MessageBoxIcon.Stop);
//                textBoxEstValue.Text = "Error";
//            }
//        }
//        #endregion
//        #region . estimatedValue .
//        public decimal estimatedValue
//        {
//            get {return _estimatedValue;}
//        }
//        #endregion
//        #region . save data to db .
//        public bool Save(Guid PropertyGuid)
//        {
//            bool success = false;		
//            /*
//            try
//            {
//                dbComps[0].P_estimatedValue = decimal.Parse(textBoxEstValue.Text,System.Globalization.NumberStyles.Currency);
//            }
//            catch{}
//            */
//            try
//            {
//                dbComps[0].P_privateEstimatedValue = decimal.Parse(textBoxEstValuePrivate.Text,System.Globalization.NumberStyles.Currency);
//            }
//            catch{}

//            try
//            {
//                if (null != dbComps && null != dbComps[0] && null != dbComps[0].P_xmlStream)//TA++: 02.04.2004. - To Prevent Runtime Crashes
//                {
//                    dbComps[0].P_propertyItemId = PropertyGuid;
					
//                    if (null != this._dbConn)
//                    {
//                        if (_compsInDb) 
//                            dbComps[0].Update(_dbConn);
//                        else
//                        {
//                            dbComps[0].Insert(_dbConn);		
//                            _compsInDb=true;
//                        }
//                    }
//                    else
//                    {
//                        success = false;
//                        return success;
//                    }
//                }
//                success = true;
//            }
//            catch (Exception ex)
//            {
//                MessageBox.Show(ex.StackTrace,"Problem: Data not saved.");
//            }
//            return success;
//        }
//        #endregion
//        #region . dummyfill .
//        private void dummyFill()
//        { 
//            System.Windows.Forms.ListViewItem listViewItem1 = new System.Windows.Forms.ListViewItem(new string[] {"","0.2","Warner Ranch","Chandler","1,834","1997","6","3","2,5","11/11/04","$280,000.00","7,650","$178,000.00"});
//            System.Windows.Forms.ListViewItem listViewItem2 = new System.Windows.Forms.ListViewItem(new string[] {"","0.3","Warner Ranch","Chandler","2,214","1997","6","3","2,5","24/06/04","$276,000.00","6,850","$171,000.00"});
//            System.Windows.Forms.ListViewItem listViewItem3 = new System.Windows.Forms.ListViewItem(new string[] {"","0.3","Warner Ranch","Chandler","2,066","1998","7","4","3","05/10/04","$266,000.00","7,230","$181,000.00"});
//            System.Windows.Forms.ListViewItem listViewItem4 = new System.Windows.Forms.ListViewItem(new string[] {"","0.5","Warner Ranch","Chandler","2,066","1997","7","4","3","08/09/04","$258,000.00","7,660","$174,000.00"});
//            System.Windows.Forms.ListViewItem listViewItem5 = new System.Windows.Forms.ListViewItem(new string[] {"","0.6","Pecan Grove","Chandler","2,116","1996","7","4","3","02/10/04","$269,000.00","8,040","$169,000.00"});
//            System.Windows.Forms.ListViewItem listViewItem6 = new System.Windows.Forms.ListViewItem(new string[] {"","0.7","Pecan Grove","Chandler","1,905","1996","6","3","2,5","17/11/04","$262,000.00","7,880","$170,500.00"});
//            System.Windows.Forms.ListViewItem listViewItem7 = new System.Windows.Forms.ListViewItem(new string[] {"","0.7","Pecan Grove","Chandler","2,116","1997","6","3","2","13/07/04","$291,000.00","6,950","$173,500.00"});
//            System.Windows.Forms.ListViewItem listViewItem8 = new System.Windows.Forms.ListViewItem(new string[] {"","0.7","Parke Tempe","Chandler","2,050","1998","7","4","2","12/10/04","$290,000.00","6,720","$178,200.00"});
//            System.Windows.Forms.ListViewItem listViewItem9 = new System.Windows.Forms.ListViewItem(new string[] {"","0.8","Parke Tempe","Chandler","2,050","1997","7","4","2","06/08/04","$267,000.00","7,550","$178,000.00"});
//            System.Windows.Forms.ListViewItem listViewItem10 = new System.Windows.Forms.ListViewItem(new string[] {"","0.8","Parke Tempe","Chandler","2,180","1997","7","4","2","19/11/04","$280,000.00","7,320","$168,300.00"});
//            listViewPropertyProfilesComps.Items.Add(listViewItem1); 
//            listViewPropertyProfilesComps.Items.Add(listViewItem2); 
//            listViewPropertyProfilesComps.Items.Add(listViewItem3); 
//            listViewPropertyProfilesComps.Items.Add(listViewItem4); 
//            listViewPropertyProfilesComps.Items.Add(listViewItem5); 
//            listViewPropertyProfilesComps.Items.Add(listViewItem6); 
//            listViewPropertyProfilesComps.Items.Add(listViewItem7); 
//            listViewPropertyProfilesComps.Items.Add(listViewItem8); 
//            listViewPropertyProfilesComps.Items.Add(listViewItem9); 
//            listViewPropertyProfilesComps.Items.Add(listViewItem10); 
//        }
//        #endregion
//        #region . open property .
//        private void listViewPropertyProfilesComps_DoubleClick(object sender, System.EventArgs e)
//        {
//            listViewPropertyProfilesComps.SelectedItems[0].Checked = !listViewPropertyProfilesComps.SelectedItems[0].Checked;
//            int selected = int.Parse(listViewPropertyProfilesComps.SelectedItems[0].SubItems[18].Text);
//            Form x = new Form();

//            UserControlPropertyProfileComps pc = new UserControlPropertyProfileComps();

//            x.Controls.Add(pc);

//            int newIdx = 0;
//            for (newIdx = 0; newIdx < comps.Length; newIdx ++)
//            {
//                int xmlComparableNumber;

//                try	
//                { xmlComparableNumber = Convert.ToInt32(comps[newIdx]._ComparableNumber);	}
//                catch 
//                { xmlComparableNumber = 0; }

//                if (xmlComparableNumber == selected)
//                {
//                    selected = newIdx;			
//                    break;
//                }
//            }

//            pc.fillComps(
//                comps[selected /*-1*/ ].PROPERTY
//                ,comps[selected /*-1*/]._ComparableNumber
//                ,listViewPropertyProfilesComps.SelectedItems[0].SubItems[1].Text
//                );
//            x.Height= pc.Height;
//            x.Width = pc.Width;

//            pc.prepareComparable();
//            pc.Dock = DockStyle.Fill;

//            x.ShowInTaskbar = false;
//            x.FormBorderStyle = FormBorderStyle.FixedToolWindow;
//            x.Text="Property Details";
//            x.ShowDialog();
			
//        }
//        #endregion
//        #region . num dat parsing .
//        private string getSecureString(object str)
//        {
//            string SecureString = "";
//            try
//            {
//                SecureString = str.ToString();
//            }
//            catch
//            {}
//            return SecureString;
//        }
//        private string getMoney(string sMoney)
//        {
//            try
//            {
//                sMoney = Convert.ToDouble(sMoney).ToString("C");
//            }
//            catch
//            {
//            }

//            return sMoney;
//        }

//        private string getDecimal(string sDecimal)
//        {
//            try
//            {
//                sDecimal = Convert.ToDouble(sDecimal).ToString("N");
//            }
//            catch
//            {
//            }

//            return sDecimal;
//        }
//        private string getNumber(string sDecimal)
//        {
//            try
//            {
//                sDecimal = Convert.ToDouble(sDecimal).ToString("#,##0.00");
//            }
//            catch
//            {
//            }

//            return sDecimal;
//        }

//        private string getDate(string sDate)
//        {
//            if (sDate.Length==8)
//            {
//                try
//                {
//                    DateTime d = new DateTime
//                        (
//                        (int.Parse(sDate.Substring(0,4))),
//                        (int.Parse(sDate.Substring(4,2))),
//                        (int.Parse(sDate.Substring(6,2)))
//                        );

//                    sDate = d.ToShortDateString();
//                }
//                catch
//                {
//                }
//            }

//            return sDate;
//        }


//        #endregion
//        #region . isInStringArray .
//        private bool isInStringArray(string[] arr, string str)
//        {
//            foreach(string x in arr)
//            {
//                if (x.Equals(str))
//                    return true;
//            }
//            return false;
//        }
//        #endregion
//        #region Dispose
//        /// <summary> 
//        /// Clean up any resources being used.
//        /// </summary>
//        protected override void Dispose( bool disposing )
//        {
//            if( disposing )
//            {
//                if(components != null)
//                {
//                    components.Dispose();
//                }
//            }
//            base.Dispose( disposing );
//        }
//        #endregion
//        #region Component Designer generated code
//        /// <summary> 
//        /// Required method for Designer support - do not modify 
//        /// the contents of this method with the code editor.
//        /// </summary>
//        private void InitializeComponent()
//        {
//            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UserControlPropertyProfileCompsListView));
//            this.labelRequestParameters = new System.Windows.Forms.Label();
//            this.listViewPropertyProfilesComps = new System.Windows.Forms.ListView();
//            this.columnHeaderTag = new System.Windows.Forms.ColumnHeader();
//            this.columnHeaderDistnace = new System.Windows.Forms.ColumnHeader();
//            this.columnHeaderSubdivision = new System.Windows.Forms.ColumnHeader();
//            this.columnHeaderSQFT = new System.Windows.Forms.ColumnHeader();
//            this.columnHeaderYB = new System.Windows.Forms.ColumnHeader();
//            this.columnHeaderRooms = new System.Windows.Forms.ColumnHeader();
//            this.columnHeaderBedrooms = new System.Windows.Forms.ColumnHeader();
//            this.columnHeaderBath = new System.Windows.Forms.ColumnHeader();
//            this.columnHeaderPool = new System.Windows.Forms.ColumnHeader();
//            this.columnHeaderSaleDate = new System.Windows.Forms.ColumnHeader();
//            this.columnHeaderSalePrice = new System.Windows.Forms.ColumnHeader();
//            this.columnHeaderSqFtPrice = new System.Windows.Forms.ColumnHeader();
//            this.columnHeaderLotArea = new System.Windows.Forms.ColumnHeader();
//            this.columnHeaderAssesed = new System.Windows.Forms.ColumnHeader();
//            this.columnHeaderStreetNum = new System.Windows.Forms.ColumnHeader();
//            this.columnHeaderStreetName = new System.Windows.Forms.ColumnHeader();
//            this.columnHeaderZIP = new System.Windows.Forms.ColumnHeader();
//            this.columnHeaderCity = new System.Windows.Forms.ColumnHeader();
//            this.columnHeader1 = new System.Windows.Forms.ColumnHeader();
//            this.buttonGetComps = new System.Windows.Forms.Button();
//            this.labelSubjectPropertySquareFootage = new System.Windows.Forms.Label();
//            this.buttonInvert = new System.Windows.Forms.Button();
//            this.buttonUntagAll = new System.Windows.Forms.Button();
//            this.buttonTagAll = new System.Windows.Forms.Button();
//            this.textBoxEstValue = new System.Windows.Forms.TextBox();
//            this.textBoxEstValuePrivate = new System.Windows.Forms.TextBox();
//            this.label4 = new System.Windows.Forms.Label();
//            this.label5 = new System.Windows.Forms.Label();
//            this.label6 = new System.Windows.Forms.Label();
//            this.comboBoxSqFtRange = new System.Windows.Forms.ComboBox();
//            this.comboBoxSearchRadius = new System.Windows.Forms.ComboBox();
//            this.comboBoxSalesInLast = new System.Windows.Forms.ComboBox();
//            this.label7 = new System.Windows.Forms.Label();
//            this.textBoxDate = new System.Windows.Forms.TextBox();
//            this.buttonRecalculate = new System.Windows.Forms.Button();
//            this.textBoxOrigSqFt = new System.Windows.Forms.TextBox();
//            this.label8 = new System.Windows.Forms.Label();
//            this.textBoxStateA = new System.Windows.Forms.TextBox();
//            this.textBoxCountyA = new System.Windows.Forms.TextBox();
//            this.textBoxZipA = new System.Windows.Forms.TextBox();
//            this.textBoxUnitNoA = new System.Windows.Forms.TextBox();
//            this.textBoxStreetA = new System.Windows.Forms.TextBox();
//            this.textBoxStreetNoA = new System.Windows.Forms.TextBox();
//            this.label76 = new System.Windows.Forms.Label();
//            this.label77 = new System.Windows.Forms.Label();
//            this.label80 = new System.Windows.Forms.Label();
//            this.label82 = new System.Windows.Forms.Label();
//            this.label74 = new System.Windows.Forms.Label();
//            this.label75 = new System.Windows.Forms.Label();
//            this.label1 = new System.Windows.Forms.Label();
//            this.label2 = new System.Windows.Forms.Label();
//            this.textBoxPreDirA = new System.Windows.Forms.TextBox();
//            this.label81 = new System.Windows.Forms.Label();
//            this.textBoxSuffixA = new System.Windows.Forms.TextBox();
//            this.textBoxPostDirA = new System.Windows.Forms.TextBox();
//            this.label78 = new System.Windows.Forms.Label();
//            this.label79 = new System.Windows.Forms.Label();
//            this.label3 = new System.Windows.Forms.Label();
//            this.pictureExclamation = new System.Windows.Forms.PictureBox();
//            this.label9 = new System.Windows.Forms.Label();
//            this.label10 = new System.Windows.Forms.Label();
//            this.bPrint = new System.Windows.Forms.Button();
//            this.tbAPN = new System.Windows.Forms.TextBox();
//            this.label11 = new System.Windows.Forms.Label();
//            this._exportCOMPS = new System.Windows.Forms.Button();
//            ((System.ComponentModel.ISupportInitialize)(this.pictureExclamation)).BeginInit();
//            this.SuspendLayout();
//            // 
//            // labelRequestParameters
//            // 
//            this.labelRequestParameters.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
//            this.labelRequestParameters.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
//            this.labelRequestParameters.Location = new System.Drawing.Point(8, 8);
//            this.labelRequestParameters.Name = "labelRequestParameters";
//            this.labelRequestParameters.Size = new System.Drawing.Size(216, 23);
//            this.labelRequestParameters.TabIndex = 1;
//            this.labelRequestParameters.Text = "REQUEST PARAMETERS:";
//            // 
//            // listViewPropertyProfilesComps
//            // 
//            this.listViewPropertyProfilesComps.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
//                        | System.Windows.Forms.AnchorStyles.Left)
//                        | System.Windows.Forms.AnchorStyles.Right)));
//            this.listViewPropertyProfilesComps.CheckBoxes = true;
//            this.listViewPropertyProfilesComps.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
//            this.columnHeaderTag,
//            this.columnHeaderDistnace,
//            this.columnHeaderSubdivision,
//            this.columnHeaderSQFT,
//            this.columnHeaderYB,
//            this.columnHeaderRooms,
//            this.columnHeaderBedrooms,
//            this.columnHeaderBath,
//            this.columnHeaderPool,
//            this.columnHeaderSaleDate,
//            this.columnHeaderSalePrice,
//            this.columnHeaderSqFtPrice,
//            this.columnHeaderLotArea,
//            this.columnHeaderAssesed,
//            this.columnHeaderStreetNum,
//            this.columnHeaderStreetName,
//            this.columnHeaderZIP,
//            this.columnHeaderCity,
//            this.columnHeader1});
//            this.listViewPropertyProfilesComps.FullRowSelect = true;
//            this.listViewPropertyProfilesComps.GridLines = true;
//            this.listViewPropertyProfilesComps.Location = new System.Drawing.Point(8, 120);
//            this.listViewPropertyProfilesComps.MultiSelect = false;
//            this.listViewPropertyProfilesComps.Name = "listViewPropertyProfilesComps";
//            this.listViewPropertyProfilesComps.Size = new System.Drawing.Size(894, 340);
//            this.listViewPropertyProfilesComps.TabIndex = 86;
//            this.listViewPropertyProfilesComps.TabStop = false;
//            this.listViewPropertyProfilesComps.UseCompatibleStateImageBehavior = false;
//            this.listViewPropertyProfilesComps.View = System.Windows.Forms.View.Details;
//            this.listViewPropertyProfilesComps.DoubleClick += new System.EventHandler(this.listViewPropertyProfilesComps_DoubleClick);
//            this.listViewPropertyProfilesComps.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.lvPropertyItems_ColumnClick);
//            // 
//            // columnHeaderTag
//            // 
//            this.columnHeaderTag.Text = "Tag";
//            this.columnHeaderTag.Width = 37;
//            // 
//            // columnHeaderDistnace
//            // 
//            this.columnHeaderDistnace.Text = "Distance";
//            this.columnHeaderDistnace.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
//            this.columnHeaderDistnace.Width = 69;
//            // 
//            // columnHeaderSubdivision
//            // 
//            this.columnHeaderSubdivision.Text = "Subdivision";
//            this.columnHeaderSubdivision.Width = 86;
//            // 
//            // columnHeaderSQFT
//            // 
//            this.columnHeaderSQFT.Text = "SQFT";
//            this.columnHeaderSQFT.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
//            this.columnHeaderSQFT.Width = 62;
//            // 
//            // columnHeaderYB
//            // 
//            this.columnHeaderYB.Text = "YB";
//            this.columnHeaderYB.Width = 40;
//            // 
//            // columnHeaderRooms
//            // 
//            this.columnHeaderRooms.Text = "Rooms";
//            this.columnHeaderRooms.Width = 57;
//            // 
//            // columnHeaderBedrooms
//            // 
//            this.columnHeaderBedrooms.Text = "Bedrooms";
//            this.columnHeaderBedrooms.Width = 77;
//            // 
//            // columnHeaderBath
//            // 
//            this.columnHeaderBath.Text = "Baths";
//            this.columnHeaderBath.Width = 50;
//            // 
//            // columnHeaderPool
//            // 
//            this.columnHeaderPool.Text = "Pool";
//            this.columnHeaderPool.Width = 40;
//            // 
//            // columnHeaderSaleDate
//            // 
//            this.columnHeaderSaleDate.Text = "Sale Date";
//            this.columnHeaderSaleDate.Width = 80;
//            // 
//            // columnHeaderSalePrice
//            // 
//            this.columnHeaderSalePrice.Text = "Sale Price";
//            this.columnHeaderSalePrice.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
//            this.columnHeaderSalePrice.Width = 80;
//            // 
//            // columnHeaderSqFtPrice
//            // 
//            this.columnHeaderSqFtPrice.Text = "$/sq.ft.";
//            this.columnHeaderSqFtPrice.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
//            // 
//            // columnHeaderLotArea
//            // 
//            this.columnHeaderLotArea.Text = "Lot Area";
//            this.columnHeaderLotArea.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
//            this.columnHeaderLotArea.Width = 69;
//            // 
//            // columnHeaderAssesed
//            // 
//            this.columnHeaderAssesed.Text = "Assessed Value";
//            this.columnHeaderAssesed.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
//            this.columnHeaderAssesed.Width = 83;
//            // 
//            // columnHeaderStreetNum
//            // 
//            this.columnHeaderStreetNum.Text = "Street #";
//            // 
//            // columnHeaderStreetName
//            // 
//            this.columnHeaderStreetName.Text = "Street Name";
//            this.columnHeaderStreetName.Width = 77;
//            // 
//            // columnHeaderZIP
//            // 
//            this.columnHeaderZIP.Text = "ZIP";
//            // 
//            // columnHeaderCity
//            // 
//            this.columnHeaderCity.Text = "City";
//            // 
//            // columnHeader1
//            // 
//            this.columnHeader1.Width = 0;
//            // 
//            // buttonGetComps
//            // 
//            this.buttonGetComps.FlatStyle = System.Windows.Forms.FlatStyle.System;
//            this.buttonGetComps.ForeColor = System.Drawing.SystemColors.ControlText;
//            this.buttonGetComps.Location = new System.Drawing.Point(664, 87);
//            this.buttonGetComps.Name = "buttonGetComps";
//            this.buttonGetComps.Size = new System.Drawing.Size(88, 24);
//            this.buttonGetComps.TabIndex = 85;
//            this.buttonGetComps.Text = "Get Comps";
//            this.buttonGetComps.Click += new System.EventHandler(this.buttonGetComps_Click);
//            // 
//            // labelSubjectPropertySquareFootage
//            // 
//            this.labelSubjectPropertySquareFootage.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
//            this.labelSubjectPropertySquareFootage.Location = new System.Drawing.Point(376, 64);
//            this.labelSubjectPropertySquareFootage.Name = "labelSubjectPropertySquareFootage";
//            this.labelSubjectPropertySquareFootage.Size = new System.Drawing.Size(88, 32);
//            this.labelSubjectPropertySquareFootage.TabIndex = 11;
//            this.labelSubjectPropertySquareFootage.Text = "Sq.Ft. range:";
//            this.labelSubjectPropertySquareFootage.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
//            // 
//            // buttonInvert
//            // 
//            this.buttonInvert.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
//            this.buttonInvert.FlatStyle = System.Windows.Forms.FlatStyle.System;
//            this.buttonInvert.ForeColor = System.Drawing.SystemColors.ControlText;
//            this.buttonInvert.Location = new System.Drawing.Point(232, 468);
//            this.buttonInvert.Name = "buttonInvert";
//            this.buttonInvert.Size = new System.Drawing.Size(104, 24);
//            this.buttonInvert.TabIndex = 95;
//            this.buttonInvert.Text = "Invert Tagged";
//            this.buttonInvert.Click += new System.EventHandler(this.buttonInvert_Click);
//            // 
//            // buttonUntagAll
//            // 
//            this.buttonUntagAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
//            this.buttonUntagAll.FlatStyle = System.Windows.Forms.FlatStyle.System;
//            this.buttonUntagAll.ForeColor = System.Drawing.SystemColors.ControlText;
//            this.buttonUntagAll.Location = new System.Drawing.Point(120, 468);
//            this.buttonUntagAll.Name = "buttonUntagAll";
//            this.buttonUntagAll.Size = new System.Drawing.Size(104, 24);
//            this.buttonUntagAll.TabIndex = 94;
//            this.buttonUntagAll.Text = "Untag All";
//            this.buttonUntagAll.Click += new System.EventHandler(this.buttonUntagAll_Click);
//            // 
//            // buttonTagAll
//            // 
//            this.buttonTagAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
//            this.buttonTagAll.FlatStyle = System.Windows.Forms.FlatStyle.System;
//            this.buttonTagAll.ForeColor = System.Drawing.SystemColors.ControlText;
//            this.buttonTagAll.Location = new System.Drawing.Point(8, 468);
//            this.buttonTagAll.Name = "buttonTagAll";
//            this.buttonTagAll.Size = new System.Drawing.Size(104, 24);
//            this.buttonTagAll.TabIndex = 93;
//            this.buttonTagAll.Text = "Tag all";
//            this.buttonTagAll.Click += new System.EventHandler(this.buttonTagAll_Click);
//            // 
//            // textBoxEstValue
//            // 
//            this.textBoxEstValue.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
//            this.textBoxEstValue.Location = new System.Drawing.Point(808, 500);
//            this.textBoxEstValue.Name = "textBoxEstValue";
//            this.textBoxEstValue.Size = new System.Drawing.Size(96, 20);
//            this.textBoxEstValue.TabIndex = 99;
//            // 
//            // textBoxEstValuePrivate
//            // 
//            this.textBoxEstValuePrivate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
//            this.textBoxEstValuePrivate.Location = new System.Drawing.Point(808, 524);
//            this.textBoxEstValuePrivate.Name = "textBoxEstValuePrivate";
//            this.textBoxEstValuePrivate.Size = new System.Drawing.Size(96, 20);
//            this.textBoxEstValuePrivate.TabIndex = 100;
//            // 
//            // label4
//            // 
//            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
//            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
//            this.label4.Location = new System.Drawing.Point(544, 524);
//            this.label4.Name = "label4";
//            this.label4.Size = new System.Drawing.Size(256, 24);
//            this.label4.TabIndex = 20;
//            this.label4.Text = "Your Estimated Value from other Sources:";
//            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
//            // 
//            // label5
//            // 
//            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
//            this.label5.Location = new System.Drawing.Point(472, 64);
//            this.label5.Name = "label5";
//            this.label5.Size = new System.Drawing.Size(88, 32);
//            this.label5.TabIndex = 21;
//            this.label5.Text = "Sales in Last";
//            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
//            // 
//            // label6
//            // 
//            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
//            this.label6.Location = new System.Drawing.Point(568, 64);
//            this.label6.Name = "label6";
//            this.label6.Size = new System.Drawing.Size(96, 32);
//            this.label6.TabIndex = 23;
//            this.label6.Text = "Search Radius";
//            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
//            // 
//            // comboBoxSqFtRange
//            // 
//            this.comboBoxSqFtRange.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
//            this.comboBoxSqFtRange.Location = new System.Drawing.Point(376, 88);
//            this.comboBoxSqFtRange.Name = "comboBoxSqFtRange";
//            this.comboBoxSqFtRange.Size = new System.Drawing.Size(88, 21);
//            this.comboBoxSqFtRange.TabIndex = 82;
//            // 
//            // comboBoxSearchRadius
//            // 
//            this.comboBoxSearchRadius.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
//            this.comboBoxSearchRadius.Location = new System.Drawing.Point(568, 88);
//            this.comboBoxSearchRadius.Name = "comboBoxSearchRadius";
//            this.comboBoxSearchRadius.Size = new System.Drawing.Size(88, 21);
//            this.comboBoxSearchRadius.TabIndex = 84;
//            // 
//            // comboBoxSalesInLast
//            // 
//            this.comboBoxSalesInLast.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
//            this.comboBoxSalesInLast.Location = new System.Drawing.Point(472, 88);
//            this.comboBoxSalesInLast.Name = "comboBoxSalesInLast";
//            this.comboBoxSalesInLast.Size = new System.Drawing.Size(88, 21);
//            this.comboBoxSalesInLast.TabIndex = 83;
//            // 
//            // label7
//            // 
//            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
//            this.label7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
//            this.label7.Location = new System.Drawing.Point(704, 476);
//            this.label7.Name = "label7";
//            this.label7.Size = new System.Drawing.Size(96, 24);
//            this.label7.TabIndex = 28;
//            this.label7.Text = "COMPS Date:";
//            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
//            // 
//            // textBoxDate
//            // 
//            this.textBoxDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
//            this.textBoxDate.Location = new System.Drawing.Point(808, 476);
//            this.textBoxDate.Name = "textBoxDate";
//            this.textBoxDate.Size = new System.Drawing.Size(96, 20);
//            this.textBoxDate.TabIndex = 98;
//            // 
//            // buttonRecalculate
//            // 
//            this.buttonRecalculate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
//            this.buttonRecalculate.FlatStyle = System.Windows.Forms.FlatStyle.System;
//            this.buttonRecalculate.ForeColor = System.Drawing.Color.Black;
//            this.buttonRecalculate.Location = new System.Drawing.Point(344, 468);
//            this.buttonRecalculate.Name = "buttonRecalculate";
//            this.buttonRecalculate.Size = new System.Drawing.Size(104, 24);
//            this.buttonRecalculate.TabIndex = 96;
//            this.buttonRecalculate.Text = "Recalculate";
//            this.buttonRecalculate.Click += new System.EventHandler(this.buttonRecalculate_Click);
//            // 
//            // textBoxOrigSqFt
//            // 
//            this.textBoxOrigSqFt.Location = new System.Drawing.Point(144, 88);
//            this.textBoxOrigSqFt.Name = "textBoxOrigSqFt";
//            this.textBoxOrigSqFt.Size = new System.Drawing.Size(56, 20);
//            this.textBoxOrigSqFt.TabIndex = 81;
//            // 
//            // label8
//            // 
//            this.label8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
//            this.label8.Location = new System.Drawing.Point(144, 72);
//            this.label8.Name = "label8";
//            this.label8.Size = new System.Drawing.Size(40, 23);
//            this.label8.TabIndex = 32;
//            this.label8.Text = "Sq.Ft.";
//            // 
//            // textBoxStateA
//            // 
//            this.textBoxStateA.Location = new System.Drawing.Point(632, 48);
//            this.textBoxStateA.MaxLength = 2;
//            this.textBoxStateA.Name = "textBoxStateA";
//            this.textBoxStateA.Size = new System.Drawing.Size(32, 20);
//            this.textBoxStateA.TabIndex = 79;
//            // 
//            // textBoxCountyA
//            // 
//            this.textBoxCountyA.Location = new System.Drawing.Point(528, 48);
//            this.textBoxCountyA.Name = "textBoxCountyA";
//            this.textBoxCountyA.Size = new System.Drawing.Size(72, 20);
//            this.textBoxCountyA.TabIndex = 78;
//            // 
//            // textBoxZipA
//            // 
//            this.textBoxZipA.Location = new System.Drawing.Point(424, 48);
//            this.textBoxZipA.Name = "textBoxZipA";
//            this.textBoxZipA.Size = new System.Drawing.Size(56, 20);
//            this.textBoxZipA.TabIndex = 77;
//            // 
//            // textBoxUnitNoA
//            // 
//            this.textBoxUnitNoA.Location = new System.Drawing.Point(368, 48);
//            this.textBoxUnitNoA.Name = "textBoxUnitNoA";
//            this.textBoxUnitNoA.Size = new System.Drawing.Size(40, 20);
//            this.textBoxUnitNoA.TabIndex = 76;
//            // 
//            // textBoxStreetA
//            // 
//            this.textBoxStreetA.Location = new System.Drawing.Point(136, 48);
//            this.textBoxStreetA.Name = "textBoxStreetA";
//            this.textBoxStreetA.Size = new System.Drawing.Size(96, 20);
//            this.textBoxStreetA.TabIndex = 73;
//            // 
//            // textBoxStreetNoA
//            // 
//            this.textBoxStreetNoA.Location = new System.Drawing.Point(8, 48);
//            this.textBoxStreetNoA.Name = "textBoxStreetNoA";
//            this.textBoxStreetNoA.Size = new System.Drawing.Size(56, 20);
//            this.textBoxStreetNoA.TabIndex = 71;
//            // 
//            // label76
//            // 
//            this.label76.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
//            this.label76.Location = new System.Drawing.Point(424, 32);
//            this.label76.Name = "label76";
//            this.label76.Size = new System.Drawing.Size(64, 23);
//            this.label76.TabIndex = 70;
//            this.label76.Text = "ZIP";
//            // 
//            // label77
//            // 
//            this.label77.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
//            this.label77.Location = new System.Drawing.Point(368, 32);
//            this.label77.Name = "label77";
//            this.label77.Size = new System.Drawing.Size(64, 23);
//            this.label77.TabIndex = 69;
//            this.label77.Text = "Unit No.";
//            // 
//            // label80
//            // 
//            this.label80.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
//            this.label80.Location = new System.Drawing.Point(136, 32);
//            this.label80.Name = "label80";
//            this.label80.Size = new System.Drawing.Size(72, 23);
//            this.label80.TabIndex = 68;
//            this.label80.Text = "Street Name";
//            // 
//            // label82
//            // 
//            this.label82.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
//            this.label82.Location = new System.Drawing.Point(4, 32);
//            this.label82.Name = "label82";
//            this.label82.Size = new System.Drawing.Size(68, 23);
//            this.label82.TabIndex = 67;
//            this.label82.Text = "Street No.";
//            // 
//            // label74
//            // 
//            this.label74.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
//            this.label74.Location = new System.Drawing.Point(632, 32);
//            this.label74.Name = "label74";
//            this.label74.Size = new System.Drawing.Size(96, 23);
//            this.label74.TabIndex = 78;
//            this.label74.Text = "State (i.e. CA)";
//            // 
//            // label75
//            // 
//            this.label75.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
//            this.label75.Location = new System.Drawing.Point(528, 32);
//            this.label75.Name = "label75";
//            this.label75.Size = new System.Drawing.Size(64, 23);
//            this.label75.TabIndex = 77;
//            this.label75.Text = "County";
//            // 
//            // label1
//            // 
//            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
//            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
//            this.label1.Location = new System.Drawing.Point(496, 44);
//            this.label1.Name = "label1";
//            this.label1.Size = new System.Drawing.Size(19, 23);
//            this.label1.TabIndex = 79;
//            this.label1.Text = "or";
//            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
//            // 
//            // label2
//            // 
//            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
//            this.label2.Location = new System.Drawing.Point(608, 48);
//            this.label2.Name = "label2";
//            this.label2.Size = new System.Drawing.Size(16, 23);
//            this.label2.TabIndex = 80;
//            this.label2.Text = "&&";
//            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
//            // 
//            // textBoxPreDirA
//            // 
//            this.textBoxPreDirA.BackColor = System.Drawing.Color.LightGray;
//            this.textBoxPreDirA.Location = new System.Drawing.Point(80, 48);
//            this.textBoxPreDirA.Name = "textBoxPreDirA";
//            this.textBoxPreDirA.Size = new System.Drawing.Size(40, 20);
//            this.textBoxPreDirA.TabIndex = 72;
//            // 
//            // label81
//            // 
//            this.label81.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
//            this.label81.Location = new System.Drawing.Point(80, 32);
//            this.label81.Name = "label81";
//            this.label81.Size = new System.Drawing.Size(64, 23);
//            this.label81.TabIndex = 81;
//            this.label81.Text = "Pre-dir";
//            // 
//            // textBoxSuffixA
//            // 
//            this.textBoxSuffixA.BackColor = System.Drawing.Color.LightGray;
//            this.textBoxSuffixA.Location = new System.Drawing.Point(312, 48);
//            this.textBoxSuffixA.Name = "textBoxSuffixA";
//            this.textBoxSuffixA.Size = new System.Drawing.Size(40, 20);
//            this.textBoxSuffixA.TabIndex = 75;
//            // 
//            // textBoxPostDirA
//            // 
//            this.textBoxPostDirA.BackColor = System.Drawing.Color.LightGray;
//            this.textBoxPostDirA.Location = new System.Drawing.Point(248, 48);
//            this.textBoxPostDirA.Name = "textBoxPostDirA";
//            this.textBoxPostDirA.Size = new System.Drawing.Size(40, 20);
//            this.textBoxPostDirA.TabIndex = 74;
//            // 
//            // label78
//            // 
//            this.label78.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
//            this.label78.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
//            this.label78.Location = new System.Drawing.Point(312, 32);
//            this.label78.Name = "label78";
//            this.label78.Size = new System.Drawing.Size(64, 23);
//            this.label78.TabIndex = 83;
//            this.label78.Text = "Suffix";
//            // 
//            // label79
//            // 
//            this.label79.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
//            this.label79.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
//            this.label79.Location = new System.Drawing.Point(248, 32);
//            this.label79.Name = "label79";
//            this.label79.Size = new System.Drawing.Size(64, 23);
//            this.label79.TabIndex = 82;
//            this.label79.Text = "Post-dir";
//            // 
//            // label3
//            // 
//            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
//            this.label3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
//            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
//            this.label3.Location = new System.Drawing.Point(376, 500);
//            this.label3.Name = "label3";
//            this.label3.Size = new System.Drawing.Size(424, 24);
//            this.label3.TabIndex = 19;
//            this.label3.Text = "Estimated Value of your Subject Property based on Tagged COMPS:";
//            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
//            // 
//            // pictureExclamation
//            // 
//            this.pictureExclamation.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
//            this.pictureExclamation.Image = ((System.Drawing.Image)(resources.GetObject("pictureExclamation.Image")));
//            this.pictureExclamation.Location = new System.Drawing.Point(886, 8);
//            this.pictureExclamation.Name = "pictureExclamation";
//            this.pictureExclamation.Size = new System.Drawing.Size(16, 16);
//            this.pictureExclamation.TabIndex = 87;
//            this.pictureExclamation.TabStop = false;
//            // 
//            // label9
//            // 
//            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
//            this.label9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
//            this.label9.Location = new System.Drawing.Point(224, 88);
//            this.label9.Name = "label9";
//            this.label9.Size = new System.Drawing.Size(152, 23);
//            this.label9.TabIndex = 101;
//            this.label9.Text = "SEARCH CRITERIA:";
//            // 
//            // label10
//            // 
//            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
//            this.label10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
//            this.label10.Location = new System.Drawing.Point(8, 88);
//            this.label10.Name = "label10";
//            this.label10.Size = new System.Drawing.Size(120, 23);
//            this.label10.TabIndex = 102;
//            this.label10.Text = "SUBJECT AREA:";
//            // 
//            // bPrint
//            // 
//            this.bPrint.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
//            this.bPrint.FlatStyle = System.Windows.Forms.FlatStyle.System;
//            this.bPrint.ForeColor = System.Drawing.SystemColors.ControlText;
//            this.bPrint.Location = new System.Drawing.Point(456, 468);
//            this.bPrint.Name = "bPrint";
//            this.bPrint.Size = new System.Drawing.Size(104, 24);
//            this.bPrint.TabIndex = 97;
//            this.bPrint.Text = "Print COMPS";            
//            // 
//            // tbAPN
//            // 
//            this.tbAPN.Location = new System.Drawing.Point(808, 48);
//            this.tbAPN.MaxLength = 128;
//            this.tbAPN.Name = "tbAPN";
//            this.tbAPN.Size = new System.Drawing.Size(88, 20);
//            this.tbAPN.TabIndex = 80;
//            // 
//            // label11
//            // 
//            this.label11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
//            this.label11.Location = new System.Drawing.Point(808, 32);
//            this.label11.Name = "label11";
//            this.label11.Size = new System.Drawing.Size(32, 23);
//            this.label11.TabIndex = 105;
//            this.label11.Text = "APN";
//            // 
//            // _exportCOMPS
//            // 
//            this._exportCOMPS.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
//            this._exportCOMPS.FlatStyle = System.Windows.Forms.FlatStyle.System;
//            this._exportCOMPS.ForeColor = System.Drawing.SystemColors.ControlText;
//            this._exportCOMPS.Location = new System.Drawing.Point(568, 468);
//            this._exportCOMPS.Name = "_exportCOMPS";
//            this._exportCOMPS.Size = new System.Drawing.Size(104, 24);
//            this._exportCOMPS.TabIndex = 106;
//            this._exportCOMPS.Text = "Exp. COMPS";
//            // 
//            // UserControlPropertyProfileCompsListView
//            // 
//            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
//            this.Controls.Add(this._exportCOMPS);
//            this.Controls.Add(this.tbAPN);
//            this.Controls.Add(this.label11);
//            this.Controls.Add(this.bPrint);
//            this.Controls.Add(this.comboBoxSqFtRange);
//            this.Controls.Add(this.label10);
//            this.Controls.Add(this.label9);
//            this.Controls.Add(this.pictureExclamation);
//            this.Controls.Add(this.textBoxSuffixA);
//            this.Controls.Add(this.textBoxPostDirA);
//            this.Controls.Add(this.textBoxPreDirA);
//            this.Controls.Add(this.label2);
//            this.Controls.Add(this.label1);
//            this.Controls.Add(this.textBoxStateA);
//            this.Controls.Add(this.textBoxCountyA);
//            this.Controls.Add(this.label74);
//            this.Controls.Add(this.label75);
//            this.Controls.Add(this.textBoxZipA);
//            this.Controls.Add(this.textBoxUnitNoA);
//            this.Controls.Add(this.textBoxStreetA);
//            this.Controls.Add(this.textBoxStreetNoA);
//            this.Controls.Add(this.label76);
//            this.Controls.Add(this.label77);
//            this.Controls.Add(this.label80);
//            this.Controls.Add(this.label82);
//            this.Controls.Add(this.buttonGetComps);
//            this.Controls.Add(this.textBoxOrigSqFt);
//            this.Controls.Add(this.comboBoxSearchRadius);
//            this.Controls.Add(this.label6);
//            this.Controls.Add(this.label7);
//            this.Controls.Add(this.textBoxDate);
//            this.Controls.Add(this.buttonInvert);
//            this.Controls.Add(this.comboBoxSalesInLast);
//            this.Controls.Add(this.label5);
//            this.Controls.Add(this.label4);
//            this.Controls.Add(this.label3);
//            this.Controls.Add(this.textBoxEstValuePrivate);
//            this.Controls.Add(this.textBoxEstValue);
//            this.Controls.Add(this.buttonTagAll);
//            this.Controls.Add(this.buttonUntagAll);
//            this.Controls.Add(this.labelSubjectPropertySquareFootage);
//            this.Controls.Add(this.listViewPropertyProfilesComps);
//            this.Controls.Add(this.labelRequestParameters);
//            this.Controls.Add(this.buttonRecalculate);
//            this.Controls.Add(this.label8);
//            this.Controls.Add(this.label78);
//            this.Controls.Add(this.label79);
//            this.Controls.Add(this.label81);
//            this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
//            this.Name = "UserControlPropertyProfileCompsListView";
//            this.Size = new System.Drawing.Size(910, 554);
//            this.Load += new System.EventHandler(this.UserControlPropertyProfileCompsListView_Load);
//            ((System.ComponentModel.ISupportInitialize)(this.pictureExclamation)).EndInit();
//            this.ResumeLayout(false);
//            this.PerformLayout();

//        }
//        #endregion

//        private void bPrint_Click(object sender, EventArgs e)
//        {
//            if ((this.textBoxOrigSqFt.Text == string.Empty) || (this.textBoxOrigSqFt.Text == "0"))
//            {
//                MessageBox.Show(this, "Enter Subject Area Square Footage!", "Missing data!", MessageBoxButtons.OK, MessageBoxIcon.Information);
//                return;
//            }

//            try
//            {
//                if (string.Empty == textBoxStreetA.Text || string.Empty == this.textBoxStreetNoA.Text)
//                {
//                    MessageBox.Show(this, "The address fields are empty, please re-enter!", "Missing data!", MessageBoxButtons.OK, MessageBoxIcon.Information);
//                    return;
//                }
//            }
//            catch {}

//            if (this.listViewPropertyProfilesComps.Items.Count == 0)
//            {
//                MessageBox.Show(this, "Select the comparable properties by tagging them!!", "Select properties!", MessageBoxButtons.OK, MessageBoxIcon.Information);
//                return;
//            }

//            int iTagged = 0;
//            for (int i = 0; i < this.listViewPropertyProfilesComps.Items.Count; i++)
//            {
//                if (true == this.listViewPropertyProfilesComps.Items[i].Checked)
//                {
//                    iTagged++;
//                    break;
//                }
//            }

//            if (0 == iTagged)
//            {
//                MessageBox.Show(this, "Select the comparable properties by tagging them!!", "Select properties!", MessageBoxButtons.OK, MessageBoxIcon.Information);
//                return;
//            }
			
//            try 
//            {				
//                PrintDocument tmpprndoc = new PrintDocument();
//                tmpprndoc.PrintPage += new PrintPageEventHandler(Multi_PrintPage);
//                PrintPreviewDialogEx tmpprdiag = new PrintPreviewDialogEx();								
//                tmpprdiag.Document = tmpprndoc;
			
//                tmpprdiag.ShowDialog();

//            }
//            catch (Exception)
//            {
//                //silent catch (not a mistake)
//                //when no printer installed -> Print -> Cancel print -> Cancel would crash app.
//            }	
//        }

		

//        private DTD.Response.PROPERTY[] GetTaggedComps()
//        {
//            int iTaggedComps = 0;
//            ArrayList alProperties = new ArrayList();
//            for (int i = 0; i < this.listViewPropertyProfilesComps.Items.Count; i++)
//            {
//                if (true == ((ListViewItem)(this.listViewPropertyProfilesComps.Items[i])).Checked) 
//                {
//                    iTaggedComps++;
//                    //alProperties.Add(comps[i].PROPERTY);
//                    Guid _printId = ((Guid)(this.listViewPropertyProfilesComps.Items[i]).Tag);
//                    if (this.htPrintMappingInfo.Contains(_printId))
//                    {
//                        alProperties.Add(((DTD.Response._DATA_PROVIDER_COMPARABLE_SALES)this.htPrintMappingInfo[_printId]).PROPERTY);
//                    }
//                }
//            }
//            DTD.Response.PROPERTY[] property = new DTD.Response.PROPERTY[iTaggedComps];
//            property = (DTD.Response.PROPERTY[]) alProperties.ToArray(typeof(DTD.Response.PROPERTY));
//            return property;
//        }
//        int iPageCnt = 0;
//        DTD.Response.PROPERTY[] selectedProperties = null;

//        private void Multi_PrintPage(object sender, PrintPageEventArgs e)
//        {			
//            //if (null == selectedProperties)
//            selectedProperties = this.GetTaggedComps();
//            if (null != selectedProperties)
//            {
//                string sFor = string.Empty;
//                sFor += textBoxStreetNoA.Text;
//                if (sFor != string.Empty)
//                    sFor += " ";

//                if (textBoxPreDirA.Text != string.Empty)
//                {
//                    sFor += textBoxPreDirA.Text + " "; 
//                }

//                if (textBoxStreetA.Text != string.Empty)
//                {
//                    sFor += textBoxStreetA.Text + " "; 
//                }

//                if (textBoxPostDirA.Text != string.Empty)
//                {
//                    sFor += textBoxPostDirA.Text + " "; 
//                }

//                if (textBoxSuffixA.Text != string.Empty)
//                {
//                    sFor += textBoxSuffixA.Text + " "; 
//                }

//                if (textBoxUnitNoA.Text != string.Empty)
//                {
//                    sFor += textBoxUnitNoA.Text + " "; 
//                }

				
//                /*
//                 * TA++ 27.Nov.2005
//                 * RQ 0118
//                if (textBoxZipA.Text != string.Empty)
//                {
//                    if (sFor != string.Empty)
//                        sFor += ", ";
//                    sFor +=  textBoxZipA.Text + " "; 
//                }

//                if (textBoxCountyA.Text != string.Empty)
//                {
//                    sFor += textBoxCountyA.Text + " "; 
//                }

//                if (textBoxStateA.Text != string.Empty)
//                {
//                    sFor += textBoxStateA.Text + " "; 
//                }
//                */
				 
//                ApplicationHelper.PrintMultipleDataReport(e, selectedProperties, iPageCnt++,					 
//                    string.Empty,
//                    sFor,
//                    textBoxOrigSqFt.Text, 
//                    string.Empty, 
//                    comboBoxSqFtRange.SelectedItem.ToString(), 
//                    comboBoxSalesInLast.SelectedItem.ToString(), 
//                    comboBoxSearchRadius.SelectedItem.ToString(), 
//                    textBoxEstValue.Text, 
//                    selectedProperties.Length.ToString()
//                    );
//                if (iPageCnt == selectedProperties.Length)
//                    iPageCnt = 0;
//            }
//        }

//        private void Show_Floater(object sender, EventArgs e)
//        {
//            this.floater = new DealMaker.Forms.Floaters.Comps();


//            Point pReadMe = this.pictureExclamation.Location;
//            Size fSize = this.floater.Size;
						
//            //this.floater.Location = new Point(pReadMe.X - fSize.Width, pReadMe.Y + this.readMe1.Height);
//            this.floater.Location = PointToScreen(new Point (pReadMe.X - fSize.Width, pReadMe.Y - 175) );

//            this.floater.Show();
//        }

//        private void Hide_Floater(object sender, EventArgs e)
//        {
//            this.floater.Hide();
//        }

//        private void On_ExportCOMPS(object sender, EventArgs e)
//        {
//            string csvDelimiter = ",";
//            string quote = "\"";
//            DTD.Response.PROPERTY[] taggedProperties = null;
//            taggedProperties = GetTaggedComps();

//            if (null == taggedProperties || 0 == taggedProperties.Length)
//            {
//                MessageBox.Show(this, "Select the comparable properties by tagging them!", "Select properties!", MessageBoxButtons.OK, MessageBoxIcon.Information);
//                return;
//            }

//            SaveFileDialog sfd = new SaveFileDialog();			
//            sfd .Filter = "Comma Separated Values(*.csv)|*.csv";
//            if (DialogResult.OK == sfd.ShowDialog())
//            {
//                string fileName = sfd.FileName;
//                try
//                {
//                    StreamWriter sw = new StreamWriter(fileName, false);
//                    string headerLine = string.Empty;
//                    headerLine += "Owner Name,Phone Number,Mailing Address,Total Living Area,Year Built,Total Rooms,Lot Area,Parking Type,Total Baths,Pool,";
//                    headerLine += "Garage,Fireplaces,Zoning,Land Use,Last Sale Date,Last Sale Price,Price Per Sq.Ft.,First Loan,Second Loan,";
//                    headerLine += "Cash Down,Deed Type,Rec. Date,Doc.No.,APN,Prop. Tax.,Assesm. Year,Deliq. Year,Tax Area,Tax Year,Exempt. Type,";
//                    headerLine += "Assessed Value,Land Use,Improvement Value,Legal Description,School District,Map Ref. One,Map Ref Two,";
//                    sw.WriteLine(headerLine);

//                    for (int i = 0; i < taggedProperties.Length; i++)
//                    {
//                        string line = string.Empty;
//                        DTD.Response.PROPERTY property = taggedProperties[i];
						
//                        //Owner Name
//                        line += quote;
//                        try {line += property.PROPERTY_OWNER[0]._OwnerName;} catch {}						
//                        line += quote;
//                        line += csvDelimiter;
						
//                        //Phone Number
//                        line += quote;
//                        try {line += property.PROPERTY_OWNER[0]._PhoneNumber;} catch {}
//                        line += quote;
//                        line += csvDelimiter;

//                        //Mailing Address
//                        line += quote;
//                        try
//                        {
//                            string subjectProperty = property._PARSED_STREET_ADDRESS._HouseNumber + " " + 
//                                property._PARSED_STREET_ADDRESS._StreetName + " " +
//                                property._City + ", " +
//                                property._State + " " +
//                                property._PostalCode + " ";
//                            line += subjectProperty;
//                        }
//                        catch {}
//                        //try {line += property.PROPERTY_OWNER[0]._MailingAddress + " " + property.PROPERTY_OWNER[0]._MailingCityAndState + " " + property.PROPERTY_OWNER[0]._MailingPostalCode;} catch {}
//                        line += quote;
//                        line += csvDelimiter;

//                        //Total Living Area
//                        line += quote;
//                        try 
//                        {
//                            string totalLivingArea = Convert.ToDouble(property._PROPERTY_CHARACTERISTICS._IMPROVEMENTS._ROOM_COUNT._TotalLivingAreaSquareFeetNumber).ToString("N");
//                            totalLivingArea = totalLivingArea.Substring(0, totalLivingArea.Length - 3);
//                            line += totalLivingArea;
//                        } 
//                        catch {}
//                        line += quote;
//                        line += csvDelimiter;

//                        //Year Built
//                        line += quote;
//                        try {line += property._PROPERTY_CHARACTERISTICS._IMPROVEMENTS._GENERAL_DESCRIPTION._YearBuiltDateIdentifier;} catch {}
//                        line += quote;
//                        line += csvDelimiter;

//                        //Total Rooms
//                        line += quote;
//                        try {line += property._PROPERTY_CHARACTERISTICS._IMPROVEMENTS._ROOM_COUNT._TotalRoomCount;} catch {}
//                        line += quote;
//                        line += csvDelimiter;

//                        //Lot Area
//                        line += quote;
//                        try 
//                        {				
//                            string lotArea = Convert.ToDouble(property._PROPERTY_CHARACTERISTICS._SITE._DIMENSIONS._LotAreaSquareFeetNumber).ToString("N");
//                            lotArea = lotArea.Substring(0, lotArea.Length - 3);
//                            line += lotArea;
//                        } 
//                        catch {}
//                        line += quote;
//                        line += csvDelimiter;

//                        //Parking Type
//                        line += quote;
//                        try {line += property._PROPERTY_CHARACTERISTICS._IMPROVEMENTS._PARKING._TypeDescription;} catch {}
//                        line += quote;
//                        line += csvDelimiter;

//                        //Total Baths
//                        line += quote;
//                        try {line += property._PROPERTY_CHARACTERISTICS._IMPROVEMENTS._ROOM_COUNT._TotalBathsCount;} catch {}
//                        line += quote;
//                        line += csvDelimiter;

//                        //Pool
//                        line += quote;
//                        try {line += property._PROPERTY_CHARACTERISTICS._IMPROVEMENTS._FEATURES._POOL._HasFeatureIndicator;} catch {}
//                        line += quote;
//                        line += csvDelimiter;

//                        //Garage
//                        line += quote;
//                        try {line += property._PROPERTY_CHARACTERISTICS._IMPROVEMENTS._PARKING._TypeDescription;} catch {}
//                        line += quote;
//                        line += csvDelimiter;

//                        //Fireplaces
//                        line += quote;
//                        try {line += property._PROPERTY_CHARACTERISTICS._IMPROVEMENTS._FEATURES._FIREPLACES._HasFeatureIndicator;} catch {}
//                        line += quote;
//                        line += csvDelimiter;

//                        //Zoning
//                        line += quote;
//                        try {line += property._PROPERTY_CHARACTERISTICS._SITE._ZONING._ClassificationIdentifier;} catch {}
//                        line += quote;
//                        line += csvDelimiter;

//                        //Land Use
//                        line += quote;
//                        try {line += property._PROPERTY_CHARACTERISTICS._SITE._CHARACTERISTICS._LandUseDescription;} catch {}
//                        line += quote;
//                        line += csvDelimiter;

//                        //Last Sale Date
//                        line += quote;
//                        try 
//                        {
//                            string lsd = property._PROPERTY_HISTORY._SALES_HISTORY[0]._LastSalesDate;
//                            DateTime d = new DateTime
//                                (
//                                (int.Parse(lsd.Substring(0,4))),
//                                (int.Parse(lsd.Substring(4,2))),
//                                (int.Parse(lsd.Substring(6,2)))
//                                );				
//                            line += d.ToShortDateString();
//                        } 
//                        catch {}
//                        line += quote;
//                        line += csvDelimiter;

//                        //Last Sale Price
//                        line += quote;
//                        try {line += Convert.ToDouble(property._PROPERTY_HISTORY._SALES_HISTORY[0]._LastSalesPriceAmount).ToString("C");} catch {}
//                        line += quote;
//                        line += csvDelimiter;

//                        //Price Per Sq.Ft.
//                        line += quote;
//                        try {line += Convert.ToDouble(property._PROPERTY_HISTORY._SALES_HISTORY[0]._PricePerSquareFootAmount).ToString("C");} catch {}
//                        line += quote;
//                        line += csvDelimiter;

//                        //First Loan
//                        line += quote;
//                        try {line += Convert.ToDouble(property._PROPERTY_HISTORY._MORTGAGE_HISTORY[0]._FirstMortgageAmount).ToString("C");} catch {}
//                        line += quote;
//                        line += csvDelimiter;

//                        //Second Loan
//                        line += quote;
//                        try {line += Convert.ToDouble(property._PROPERTY_HISTORY._MORTGAGE_HISTORY[0]._SecondMortgageAmount).ToString("C");} catch {}
//                        line += quote;
//                        line += csvDelimiter;

//                        //Cash Down
//                        line += quote;
//                        try {line += Convert.ToDouble(property._PROPERTY_HISTORY._SALES_HISTORY[0]._CashDownAmount).ToString("C");} catch {}
//                        line += quote;
//                        line += csvDelimiter;

//                        //Deed Type
//                        line += quote;
//                        try {line += property._PROPERTY_HISTORY._SALES_HISTORY[0]._DeedTypeDescription;} catch {}
//                        line += quote;
//                        line +=	csvDelimiter;

//                        //Rec. Date
//                        line += quote;
//                        try 
//                        {
//                            string rd = property._PROPERTY_HISTORY._SALES_HISTORY[0]._LastRecordingDate;
//                            DateTime d = new DateTime
//                                (
//                                (int.Parse(rd.Substring(0,4))),
//                                (int.Parse(rd.Substring(4,2))),
//                                (int.Parse(rd.Substring(6,2)))
//                                );

//                            line += d.ToShortDateString();
//                        } 
//                        catch {}
//                        line += quote;
//                        line +=	csvDelimiter;

//                        //Doc.No.
//                        line += quote;
//                        try {line += property._PROPERTY_HISTORY._SALES_HISTORY[0]._DocumentNumberIdentifier;} catch {}
//                        line += quote;
//                        line +=	csvDelimiter;

//                        //APN
//                        line += quote;
//                        try {line += property._AssessorsParcelIdentifier;}  catch {}
//                        line += quote;
//                        line +=	csvDelimiter;

//                        //Prop. Tax.
//                        line += quote;
//                        try {line += Convert.ToDouble(property._PROPERTY_TAX._RealEstateTotalTaxAmount).ToString("C");}  catch {}
//                        line += quote;
//                        line +=	csvDelimiter;

//                        //Assesm. Year
//                        line += quote;
//                        try {line += property._PROPERTY_TAX._AssessmentYear;}  catch {}
//                        line += quote;
//                        line +=	csvDelimiter;						

//                        //Deliq. Year
//                        line += quote;
//                        try {line += property._PROPERTY_TAX._DelinquentYear;}  catch {}
//                        line += quote;
//                        line +=	csvDelimiter;						

//                        //Tax Area
//                        line += quote;
//                        try {line += property._PROPERTY_TAX._RateAreaIdentifier;}  catch {}
//                        line += quote;
//                        line +=	csvDelimiter;

//                        //Tax Year
//                        line += quote;
//                        try {line += property._PROPERTY_TAX._TaxYear;}  catch {}
//                        line += quote;
//                        line +=	csvDelimiter;

//                        //Exempt. Type
//                        line += quote;
//                        try {line += property._PROPERTY_TAX._ExemptionTypeDescription;}  catch {}
//                        line += quote;
//                        line +=	csvDelimiter;

//                        //Assessed Value
//                        line += quote;
//                        try {line += Convert.ToDouble(property._PROPERTY_TAX._TotalAssessedValueAmount).ToString("C");}  catch {}
//                        line += quote;
//                        line +=	csvDelimiter;
						
//                        //Land Use
//                        line += quote;
//                        try {line += property._PROPERTY_TAX._LandValueAmount;}  catch {}
//                        line += quote;
//                        line +=	csvDelimiter;

//                        //Improvement Value
//                        line += quote;
//                        try {line += Convert.ToDouble(property._PROPERTY_TAX._ImprovementValueAmount).ToString("C");}  catch {}
//                        line += quote;
//                        line +=	csvDelimiter;

//                        //Legal Description
//                        line += quote;
//                        try {line += property._LEGAL_DESCRIPTION._TextDescription;}  catch {}
//                        line += quote;
//                        line +=	csvDelimiter;
						
//                        //School District
//                        line += quote;
//                        try {line += property._NEIGHBORHOOD_AREA_INFORMATION._SchoolDistrictName;}  catch {}
//                        line += quote;
//                        line +=	csvDelimiter;

//                        //Map Ref. One
//                        line += quote;
//                        try {line += property._NEIGHBORHOOD_AREA_INFORMATION._MapReferenceOneIdentifier;}  catch {}
//                        line += quote;
//                        line +=	csvDelimiter;

//                        //Map Ref Two
//                        line += quote;
//                        try {line += property._NEIGHBORHOOD_AREA_INFORMATION._MapReferenceTwoIdentifier;}  catch {}
//                        line += quote;
//                        line +=	csvDelimiter;

//                        sw.WriteLine(line);
//                    }

//                    sw.Flush();
//                    sw.Close();
//                }
//                catch (Exception exc)
//                {
//                    MessageBox.Show(this, exc.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Information);
//                    return;
//                }
//            }		
//        }

//        private void UserControlPropertyProfileCompsListView_Load(object sender, System.EventArgs e)
//        {
		
//        }

        
//    }
//    #region . comps .
//    public class comps
//    {
//        #region ˇ Data ˇ
//        private Guid propertyItemId;
//        private DateTime dateOfRetrieve;
//        private Decimal estimatedValue;
//        private Decimal privateEstimatedValue;
//        private Byte[] xmlStream;
//        private String taggedCSV;
//        #endregion ˇ Data ˇ
//        #region ˇ CTR ˇ
//        public comps()
//        {
//        }

//        public comps(Guid propertyItemId, DateTime dateOfRetrieve,
//            Decimal estimatedValue, Decimal privateEstimatedValue, Byte[] xmlStream,
//            String taggedCSV)
//        {
//            this.propertyItemId = propertyItemId;
//            this.dateOfRetrieve = dateOfRetrieve;
//            this.estimatedValue = estimatedValue;
//            this.privateEstimatedValue = privateEstimatedValue;
//            this.xmlStream = xmlStream;
//            this.taggedCSV = taggedCSV;
//        }
//        #endregion ˇ CTR ˇ
//        #region ˇ Properties ˇ
//        #region ˇ  P_propertyItemIdˇ
//        [Description("propertyItemId")]
//        public Guid P_propertyItemId
//        {
//            get
//            {
//                return this.propertyItemId;
//            }
//            set
//            {
//                this.propertyItemId = value;
//            }
//        }
//        #endregion ˇ  P_propertyItemIdˇ
//        #region ˇ  P_dateOfRetrieveˇ
//        [Description("dateOfRetrieve")]
//        public DateTime P_dateOfRetrieve
//        {
//            get
//            {
//                return this.dateOfRetrieve;
//            }
//            set
//            {
//                this.dateOfRetrieve = value;
//            }
//        }
//        #endregion ˇ  P_dateOfRetrieveˇ
//        #region ˇ  P_estimatedValueˇ
//        [Description("estimatedValue")]
//        public Decimal P_estimatedValue
//        {
//            get
//            {
//                return this.estimatedValue;
//            }
//            set
//            {
//                this.estimatedValue = value;
//            }
//        }
//        #endregion ˇ  P_estimatedValueˇ
//        #region ˇ  P_privateEstimatedValueˇ
//        [Description("privateEstimatedValue")]
//        public Decimal P_privateEstimatedValue
//        {
//            get
//            {
//                return this.privateEstimatedValue;
//            }
//            set
//            {
//                this.privateEstimatedValue = value;
//            }
//        }
//        #endregion ˇ  P_privateEstimatedValueˇ
//        #region ˇ  P_xmlStreamˇ
//        [Description("xmlStream")]
//        public Byte[] P_xmlStream
//        {
//            get
//            {
//                return this.xmlStream;
//            }
//            set
//            {
//                this.xmlStream = value;
//            }
//        }
//        #endregion ˇ  P_xmlStreamˇ
//        #region ˇ  P_taggedCSVˇ
//        [Description("taggedCSV")]
//        public String P_taggedCSV
//        {
//            get
//            {
//                return this.taggedCSV;
//            }
//            set
//            {
//                this.taggedCSV = value;
//            }
//        }
//        #endregion ˇ  P_taggedCSVˇ
//        #endregion ˇ Properties ˇ
//        #region ˇ Methods ˇ
//        public int Insert(SqlConnection dbConn)
//        {
//            int retVal = 0;

//            try
//            {
//                SqlCommand sp_insert_comps = new
//                    SqlCommand("insert_comps", dbConn);

//                sp_insert_comps.CommandType =
//                    CommandType.StoredProcedure;

	
//                sp_insert_comps.Parameters.Add("@propertyItemId", this.propertyItemId);
	
//                sp_insert_comps.Parameters.Add("@dateOfRetrieve", this.dateOfRetrieve);
	
//                sp_insert_comps.Parameters.Add("@estimatedValue", this.estimatedValue);
	
//                sp_insert_comps.Parameters.Add("@privateEstimatedValue",
//                    this.privateEstimatedValue);
//                sp_insert_comps.Parameters.Add("@xmlStream",
//                    this.xmlStream);
//                sp_insert_comps.Parameters.Add("@taggedCSV",
//                    this.taggedCSV);
//                retVal = sp_insert_comps.ExecuteNonQuery();
//            }
//            catch (Exception exc)
//            {
//                throw exc;
//            }
//            return retVal;
//        }

//        public int Update(SqlConnection dbConn)
//        {
//            int retVal = 0;

//            try
//            {
//                SqlCommand sp_update_comps = new
//                    SqlCommand("update_comps", dbConn);

//                sp_update_comps.CommandType =
//                    CommandType.StoredProcedure;

	
//                sp_update_comps.Parameters.Add("@propertyItemId", this.propertyItemId);
	
//                sp_update_comps.Parameters.Add("@dateOfRetrieve", this.dateOfRetrieve);
	
//                sp_update_comps.Parameters.Add("@estimatedValue", this.estimatedValue);
	
//                sp_update_comps.Parameters.Add("@privateEstimatedValue",
//                    this.privateEstimatedValue);
//                sp_update_comps.Parameters.Add("@xmlStream",
//                    this.xmlStream);
//                sp_update_comps.Parameters.Add("@taggedCSV",
//                    this.taggedCSV);
//                retVal = sp_update_comps.ExecuteNonQuery();
//            }
//            catch (Exception exc)
//            {
//                throw exc;
//            }
//            return retVal;
//        }

//        public int Delete(SqlConnection dbConn)
//        {
//            int retVal = 0;

//            try
//            {
//                SqlCommand sp_delete_comps = new
//                    SqlCommand("delete_comps", dbConn);

//                sp_delete_comps.CommandType =
//                    CommandType.StoredProcedure;

	
//                sp_delete_comps.Parameters.Add("@propertyItemId", this.propertyItemId);
//                retVal = sp_delete_comps.ExecuteNonQuery();
//            }
//            catch (Exception exc)
//            {
//                throw exc;
//            }
//            return retVal;
//        }

//        public static comps[] Select(SqlConnection dbConn, params
//            object[] list)
//        {
//            ArrayList alObjects = new ArrayList();
//            SqlDataReader reader = null;
//            comps tempcomps = null;
//            try
//            {
//                SqlCommand sp_select_comps = new
//                    SqlCommand("select_comps", dbConn);

//                sp_select_comps.CommandType =
//                    CommandType.StoredProcedure;

//                if (null != list && 0 != list.Length)
//                {
	
//                    sp_select_comps.Parameters.Add("@propertyItemId", (System.Guid)list[0]);
//                }
//                else
//                {
	
//                    sp_select_comps.Parameters.Add("@propertyItemId", DBNull.Value);
//                }
//                reader = sp_select_comps.ExecuteReader();
//                while (reader.Read())
//                {
//                    System.Data.SqlTypes.SqlBinary
//                        sqlBinary4 = reader.GetSqlBinary(4);
//                    tempcomps = new
//                        comps(reader.GetGuid(0), reader.GetDateTime(1), reader.GetDecimal(2),
//                        reader.GetDecimal(3), (byte[])sqlBinary4.Value, reader.GetString(5));
//                    alObjects.Add(tempcomps);
//                }
//            }
//            catch (Exception exc)
//            {
//                throw exc;
//            }
//            finally
//            {
//                reader.Close();
//            }
//            return (comps[]) alObjects.ToArray(typeof(comps));
//        }
//        #endregion ˇ Methods ˇ
//    }
//    #endregion
//    #region ˇ COMMENTED OUT ˇ
//    /*
//    public class comps
//    {
//        #region ˇ Data ˇ

//        private Guid propertyItemId;

//        private DateTime dateOfRetrieve;

//        private Decimal estimatedValue;

//        private Decimal privateEstimatedValue;

//        private Byte[] xmlStream;

//        #endregion ˇ Data ˇ
//        #region ˇ CTR ˇ

//        public comps()

//        {

//        }

 

//        public comps(Guid propertyItemId, DateTime dateOfRetrieve, Decimal estimatedValue, Decimal privateEstimatedValue, Byte[] xmlStream)

//        {

//            this.propertyItemId = propertyItemId;

//            this.dateOfRetrieve = dateOfRetrieve;

//            this.estimatedValue = estimatedValue;

//            this.privateEstimatedValue = privateEstimatedValue;

//            this.xmlStream = xmlStream;

//        }

//        #endregion ˇ CTR ˇ
//        #region ˇ Properties ˇ

//        #region ˇ  P_propertyItemIdˇ

//        public Guid P_propertyItemId

//        {

//            get

//            {

//                return this.propertyItemId;

//            }

//            set

//            {

//                this.propertyItemId = value;

//            }

//        }

//        #endregion ˇ  P_propertyItemIdˇ

//        #region ˇ  P_dateOfRetrieveˇ

//        public DateTime P_dateOfRetrieve

//        {

//            get

//            {

//                return this.dateOfRetrieve;

//            }

//            set

//            {

//                this.dateOfRetrieve = value;

//            }

//        }

//        #endregion ˇ  P_dateOfRetrieveˇ

//        #region ˇ  P_estimatedValueˇ

//        public Decimal P_estimatedValue

//        {

//            get

//            {

//                return this.estimatedValue;

//            }

//            set

//            {

//                this.estimatedValue = value;

//            }

//        }

//        #endregion ˇ  P_estimatedValueˇ

//        #region ˇ  P_privateEstimatedValueˇ

//        public Decimal P_privateEstimatedValue

//        {

//            get

//            {

//                return this.privateEstimatedValue;

//            }

//            set

//            {

//                this.privateEstimatedValue = value;

//            }

//        }

//        #endregion ˇ  P_privateEstimatedValueˇ

//        #region ˇ  P_xmlStreamˇ

//        public Byte[] P_xmlStream

//        {

//            get

//            {

//                return this.xmlStream;

//            }

//            set

//            {

//                this.xmlStream = value;

//            }

//        }

//        #endregion ˇ  P_xmlStreamˇ

//        #endregion ˇ Properties ˇ
//        #region ˇ Methods ˇ

//        public void Insert(SqlConnection dbConn)

//        {

//            try

//            {

//                SqlCommand sp_insert_comps = new SqlCommand("insert_comps", dbConn);

 

//                sp_insert_comps.CommandType = CommandType.StoredProcedure;

 

//                sp_insert_comps.Parameters.Add("@propertyItemId", this.propertyItemId);

//                sp_insert_comps.Parameters.Add("@dateOfRetrieve", this.dateOfRetrieve);

//                sp_insert_comps.Parameters.Add("@estimatedValue", this.estimatedValue);

//                sp_insert_comps.Parameters.Add("@privateEstimatedValue", this.privateEstimatedValue);

//                sp_insert_comps.Parameters.Add("@xmlStream", this.xmlStream);

//                sp_insert_comps.ExecuteNonQuery();

//            }

//            catch (Exception exc)

//            {

//                throw exc;

//            }

//        }

 

//        public void Update(SqlConnection dbConn)

//        {

//            try

//            {

//                SqlCommand sp_update_comps = new SqlCommand("update_comps", dbConn);

 

//                sp_update_comps.CommandType = CommandType.StoredProcedure;

 

//                sp_update_comps.Parameters.Add("@propertyItemId", this.propertyItemId);

//                sp_update_comps.Parameters.Add("@dateOfRetrieve", this.dateOfRetrieve);

//                sp_update_comps.Parameters.Add("@estimatedValue", this.estimatedValue);

//                sp_update_comps.Parameters.Add("@privateEstimatedValue", this.privateEstimatedValue);

//                sp_update_comps.Parameters.Add("@xmlStream", this.xmlStream);

//                sp_update_comps.ExecuteNonQuery();

//            }

//            catch (Exception exc)

//            {

//                throw exc;

//            }

//        }

 

//        public void Delete(SqlConnection dbConn)

//        {

//            try

//            {

//                SqlCommand sp_delete_comps = new SqlCommand("delete_comps", dbConn);

 

//                sp_delete_comps.CommandType = CommandType.StoredProcedure;

 

//                sp_delete_comps.Parameters.Add("@propertyItemId", this.propertyItemId);

//                sp_delete_comps.ExecuteNonQuery();

//            }

//            catch (Exception exc)

//            {

//                throw exc;

//            }

//        }

 
//        public static comps[] Select(SqlConnection dbConn, bool getAllRecords,  System.Guid propertyItemId)
//        {
//            ArrayList alObjects = new ArrayList();
//            SqlDataReader reader = null;
//            comps tempcomps = null;
//            try
//            {
//                SqlCommand sp_select_comps = new SqlCommand("select_comps", dbConn);

//                sp_select_comps.CommandType = CommandType.StoredProcedure;

//                if (true == getAllRecords)
//                {
//                    sp_select_comps.Parameters.Add("@propertyItemId", DBNull.Value);
//                    reader = sp_select_comps.ExecuteReader();
//                }
//                else
//                {
//                    sp_select_comps.Parameters.Add("@propertyItemId", propertyItemId);
//                    reader = sp_select_comps.ExecuteReader();
//                }
//                while (reader.Read())
//                {
//                    System.Data.SqlTypes.SqlBinary sqlBinary4 = reader.GetSqlBinary(4);
//                    tempcomps = new comps(reader.GetGuid(0), reader.GetDateTime(1), reader.GetDecimal(2), reader.GetDecimal(3), (byte[])sqlBinary4.Value);
//                    alObjects.Add(tempcomps);
//                }
//            }
//            catch (Exception exc)
//            {
//                throw exc;
//            }
//            finally
//            {
//                reader.Close();
//            }
//            return (comps[]) alObjects.ToArray(typeof(comps));
//        }
//        #endregion ˇ Methods ˇ
//    }
//    */
	
//    /*
//    #region ˇ Class ListViewItemComparer ˇ

//    // Implements the manual sorting of items by columns.

//    class ListViewItemComparer : IComparer

//    {

//        private int col;

//        public ListViewItemComparer()

//        {

//            col = 0;

//        }

//        public ListViewItemComparer(int column)

//        {

//            col = column;

//        }

//        public int Compare(object x, object y)

//        {

//            return String.Compare(((ListViewItem)x).SubItems[col].Text, ((ListViewItem)y).SubItems[col].Text);

//        }

//    }

//    #endregion ˇ Class ListViewItemComparer ˇ
//    #region ˇ Class ListViewItemComparerDesc ˇ

//    // Implements the manual sorting of items by columns.

//    class ListViewItemComparerDesc : IComparer

//    {

//        private int col;

//        public ListViewItemComparerDesc()

//        {

//            col = 0;

//        }

//        public ListViewItemComparerDesc(int column)

//        {

//            col = column;

//        }

//        public int Compare(object x, object y)

//        {

//            return String.Compare(((ListViewItem)y).SubItems[col].Text, ((ListViewItem)x).SubItems[col].Text);

//        }

//    }

//    #endregion ˇ Class ListViewItemComparerDesc ˇ
//    */
//    #endregion ˇ COMMENTED OUT ˇ
//}
