using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;
using System.Drawing.Printing;

//Added
using DealMaker.ShortSale.Utils;
using DealMaker.PlainClasses.ShortSale.Adapters;

namespace DealMaker.UserControls.ShortSale
{
	/// <summary>
	/// Summary description for DocumentTemplate.
	/// </summary>
	public class DocumentTemplate : System.Windows.Forms.UserControl
    {
        #region DATA
        private TXTextControl.ButtonBar _buttonBar;
		private TXTextControl.RulerBar _rulerBar;
		private TXTextControl.StatusBar _statusBar;
		private TXTextControl.TextControl _textControl;
		private System.Windows.Forms.ContextMenuStrip _ctxMenu;
		private System.Windows.Forms.ToolStripMenuItem menuItem16;
		private System.Windows.Forms.ToolStripMenuItem menuItem17;
		private System.Windows.Forms.ToolStripMenuItem menuItem18;
		private System.Windows.Forms.ToolStripMenuItem menuItem20;
		private System.Windows.Forms.ToolStripMenuItem menuItem21;
		private System.Windows.Forms.ToolStripMenuItem _1_CompanyName;
		private System.Windows.Forms.ToolStripMenuItem menuItem55;
		private System.Windows.Forms.ToolStripMenuItem menuItem56;
		private System.Windows.Forms.ToolStripMenuItem menuItem57;
		private System.Windows.Forms.ToolStripMenuItem menuItem58;
		private System.Windows.Forms.ToolStripMenuItem menuItem59;
		private System.Windows.Forms.ToolStripMenuItem menuItem60;
		private System.Windows.Forms.ToolStripMenuItem _new;
		private System.Windows.Forms.ToolStripMenuItem _copy;
		private System.Windows.Forms.ToolStripMenuItem _paste;
		private System.Windows.Forms.ToolStripMenuItem _undo;
		private System.Windows.Forms.ToolStripMenuItem _redo;
		private System.Windows.Forms.ToolStripMenuItem _openFile;
		private System.Windows.Forms.ToolStripMenuItem _saveFile;
		private System.Windows.Forms.ToolStripMenuItem _openMergeTemplate;
		private System.Windows.Forms.ToolStripMenuItem _saveMergeTemplate;
		private System.Windows.Forms.ToolStripMenuItem _pageSetup;
		private System.Windows.Forms.ToolStripMenuItem _printPreview;
		private System.Windows.Forms.ToolStripMenuItem _print;
		private System.Windows.Forms.ToolStripMenuItem _merge;
		private System.Windows.Forms.ToolStripMenuItem _switchToTemplate;
		private System.Windows.Forms.ToolStripMenuItem subMenu2;
		private System.Windows.Forms.ToolStripMenuItem _ai_FirstName;
		private System.Windows.Forms.ToolStripMenuItem _ai_LastName;
		private System.Windows.Forms.ToolStripMenuItem _ai_FullName;
		private System.Windows.Forms.ToolStripMenuItem _ai_Address;
		private System.Windows.Forms.ToolStripMenuItem _ai_Phone;
		private System.Windows.Forms.ToolStripMenuItem _ai_Fax;
		private System.Windows.Forms.ToolStripMenuItem _ai_Email;
		private System.Windows.Forms.ToolStripMenuItem _bi_FirstName;
		private System.Windows.Forms.ToolStripMenuItem _bi_LastName;
		private System.Windows.Forms.ToolStripMenuItem _bi_FullName;
		private System.Windows.Forms.ToolStripMenuItem _bi_Address;
		private System.Windows.Forms.ToolStripMenuItem _bi_Phone;
		private System.Windows.Forms.ToolStripMenuItem _bi_Fax;
		private System.Windows.Forms.ToolStripMenuItem _bi_Email;
		private System.Windows.Forms.ToolStripMenuItem _opi_LastName;
		private System.Windows.Forms.ToolStripMenuItem _opi_FullName;
		private System.Windows.Forms.ToolStripMenuItem _opi_PropertyAddress;
		private System.Windows.Forms.ToolStripMenuItem _opi_MailingAddress;
		private System.Windows.Forms.ToolStripMenuItem _3_CompanyName;
		private System.Windows.Forms.ToolStripMenuItem _3_AccountNr;
		private System.Windows.Forms.ToolStripMenuItem _2_CompanyName;
		private System.Windows.Forms.ToolStripMenuItem _2_AccountNr;
		private System.Windows.Forms.ToolStripMenuItem _1_AccountNr;
		private System.Windows.Forms.ToolStripMenuItem _ci_firstName;
		private System.Windows.Forms.ToolStripMenuItem _ci_middleName;
		private System.Windows.Forms.ToolStripMenuItem _ci_LastName;
		private System.Windows.Forms.ToolStripMenuItem _ci_FullName;
		private System.Windows.Forms.ToolStripMenuItem _ci_SiteAddress;
		private System.Windows.Forms.ToolStripMenuItem _ci_SiteCity;
		private System.Windows.Forms.ToolStripMenuItem _ci_SiteState;
		private System.Windows.Forms.ToolStripMenuItem _ci_SiteZIP;
		private System.Windows.Forms.ToolStripMenuItem opi;
		private System.Windows.Forms.ToolStripMenuItem _opi_FirstName;
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		string _fileName = string.Empty;
		private ToolStripMenuItem _opi_MiddleName;
        private ToolStripMenuItem menuItem1;
        private ToolStripMenuItem _currentDate;
        private ToolStripMenuItem _currentTime;
		const string _pathSuffix = @"\Document Templates\Short Sale Templates";
		LienSet _lienSet = LienSet.NotSelected;
        private ToolStripMenuItem menuItem2;
        private ToolStripMenuItem _companyName;
        private ToolStripMenuItem _companyAddress;
        private ToolStripMenuItem _companyCity;
        private ToolStripMenuItem _companyState;
        private ToolStripMenuItem _companyZIP;
        private ToolStripMenuItem _companyPhone;
        private ToolStripMenuItem _companyFax;
        private ToolStripMenuItem _companyEmail;
        private ToolStripMenuItem menuItem11;
        private ToolStripMenuItem _1_SetupDept_Name;
        private ToolStripMenuItem _1_SetupDept_Phone;
        private ToolStripMenuItem _1_SetupDept_Fax;
        private ToolStripMenuItem menuItem15;
        private ToolStripMenuItem _1_SetupMgr_Name;
        private ToolStripMenuItem _1_SetupMgr_Phone;
        private ToolStripMenuItem _1_SetupMgr_Fax;
        private ToolStripMenuItem menuItem24;
        private ToolStripMenuItem _1_Negotiator_Name;
        private ToolStripMenuItem _1_Negotiator_Phone;
        private ToolStripMenuItem _1_Negotiator_Fax;
        private ToolStripMenuItem menuItem28;
        private ToolStripMenuItem _1_NegMgr_Name;
        private ToolStripMenuItem _1_NegMgr_Phone;
        private ToolStripMenuItem _1_NegMgr_Fax;
        private ToolStripMenuItem menuItem32;
        private ToolStripMenuItem _2_SetupDept_Name;
        private ToolStripMenuItem _2_SetupDept_Phone;
        private ToolStripMenuItem _2_SetupDept_Fax;
        private ToolStripMenuItem menuItem36;
        private ToolStripMenuItem _2_SetupMgr_Name;
        private ToolStripMenuItem _2_SetupMgr_Phone;
        private ToolStripMenuItem _2_SetupMgr_Fax;
        private ToolStripMenuItem menuItem40;
        private ToolStripMenuItem _2_Negotiator_Name;
        private ToolStripMenuItem _2_Negotiator_Phone;
        private ToolStripMenuItem _2_Negotiator_Fax;
        private ToolStripMenuItem menuItem44;
        private ToolStripMenuItem _2_NegMgr_Name;
        private ToolStripMenuItem _2_NegMgr_Phone;
        private ToolStripMenuItem _2_NegMgr_Fax;
        private ToolStripMenuItem menuItem48;
        private ToolStripMenuItem _3_SetupDept_Name;
        private ToolStripMenuItem _3_SetupDept_Phone;
        private ToolStripMenuItem _3_SetupDept_Fax;
        private ToolStripMenuItem menuItem52;
        private ToolStripMenuItem _3_SetupMgr_Name;
        private ToolStripMenuItem _3_SetupMgr_Phone;
        private ToolStripMenuItem _3_SetupMgr_Fax;
        private ToolStripMenuItem menuItem62;
        private ToolStripMenuItem _3_Negotiator_Name;
        private ToolStripMenuItem _3_Negotiator_Phone;
        private ToolStripMenuItem _3_Negotiator_Fax;
        private ToolStripMenuItem menuItem66;
        private ToolStripMenuItem _3_NegMgr_Name;
        private ToolStripMenuItem _3_NegMgr_Phone;
        private ToolStripMenuItem _3_NegMgr_Fax;
        string _userAppFolder = string.Empty;
        #endregion
        
        public DocumentTemplate()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

            AddEventHandlers();

			// TODO: Add any initialization after the InitializeComponent call            
            _textControl.ButtonBar = _buttonBar;
            _textControl.RulerBar = _rulerBar;
            _textControl.StatusBar = _statusBar;					
		}

		void AddEventHandlers()
		{			
			_new.Click += new EventHandler(On_New_Click);
			
			_copy.Click += new EventHandler(On_Copy_Click);
			_paste.Click += new EventHandler(On_Paste_Click);
			_undo.Click += new EventHandler(On_Undo_Click);
			_redo.Click += new EventHandler(On_Redo_Click);

			_openFile.Click += new EventHandler(On_OpenFile_Click);
			_saveFile.Click += new EventHandler(On_SaveFile_Click);
			_openMergeTemplate.Click += new EventHandler(On_OpenMergeTemplate_Click);
			_saveMergeTemplate.Click += new EventHandler(On_SaveMergeTemplate_Click);

			_pageSetup.Click += new EventHandler(On_PageSetup_Click);
			_printPreview.Click += new EventHandler(On_PrintPreview_Click);
			_print.Click += new EventHandler(On_Print_Click);

			_merge.Click += new EventHandler(On_Merge_Click);
			_switchToTemplate.Click += new EventHandler(On_SwitchToTemplate_Click);            
		}

                

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}


        public bool _loaded = false;
		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad (e);

            this._textControl.CreateControl();
            _loaded = true;


            //if (string.Empty != _fileName)
            //    OpenMailMergeTemplate(_fileName);
		}

        public void CreateControl()
        {
            this._textControl.CreateControl();
        }

        public void OpenTemplate()
        {
            if (string.Empty != _fileName)
                OpenMailMergeTemplate(_fileName);

        }


		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this._buttonBar = new TXTextControl.ButtonBar();
            this._rulerBar = new TXTextControl.RulerBar();
            this._statusBar = new TXTextControl.StatusBar();
            this._textControl = new TXTextControl.TextControl();
            this._ctxMenu = new System.Windows.Forms.ContextMenuStrip();
            this._new = new ToolStripMenuItem();
            this.menuItem55 = new ToolStripMenuItem();
            this._copy = new ToolStripMenuItem();
            this._paste = new ToolStripMenuItem();
            this._undo = new ToolStripMenuItem();
            this._redo = new ToolStripMenuItem();
            this.menuItem56 = new ToolStripMenuItem();
            this._openFile = new ToolStripMenuItem();
            this._saveFile = new ToolStripMenuItem();
            this.menuItem57 = new ToolStripMenuItem();
            this._openMergeTemplate = new ToolStripMenuItem();
            this._saveMergeTemplate = new ToolStripMenuItem();
            this.menuItem58 = new ToolStripMenuItem();
            this._pageSetup = new ToolStripMenuItem();
            this._printPreview = new ToolStripMenuItem();
            this._print = new ToolStripMenuItem();
            this.menuItem59 = new ToolStripMenuItem();
            this._merge = new ToolStripMenuItem();
            this._switchToTemplate = new ToolStripMenuItem();
            this.menuItem60 = new ToolStripMenuItem();
            this.menuItem2 = new ToolStripMenuItem();
            this._companyName = new ToolStripMenuItem();
            this._companyAddress = new ToolStripMenuItem();
            this._companyCity = new ToolStripMenuItem();
            this._companyState = new ToolStripMenuItem();
            this._companyZIP = new ToolStripMenuItem();
            this._companyPhone = new ToolStripMenuItem();
            this._companyFax = new ToolStripMenuItem();
            this._companyEmail = new ToolStripMenuItem();
            this.menuItem18 = new ToolStripMenuItem();
            this._ci_firstName = new ToolStripMenuItem();
            this._ci_middleName = new ToolStripMenuItem();
            this._ci_LastName = new ToolStripMenuItem();
            this._ci_FullName = new ToolStripMenuItem();
            this._ci_SiteAddress = new ToolStripMenuItem();
            this._ci_SiteCity = new ToolStripMenuItem();
            this._ci_SiteState = new ToolStripMenuItem();
            this._ci_SiteZIP = new ToolStripMenuItem();
            this.subMenu2 = new ToolStripMenuItem();
            this._1_CompanyName = new ToolStripMenuItem();
            this._1_AccountNr = new ToolStripMenuItem();
            this.menuItem11 = new ToolStripMenuItem();
            this._1_SetupDept_Name = new ToolStripMenuItem();
            this._1_SetupDept_Phone = new ToolStripMenuItem();
            this._1_SetupDept_Fax = new ToolStripMenuItem();
            this.menuItem15 = new ToolStripMenuItem();
            this._1_SetupMgr_Name = new ToolStripMenuItem();
            this._1_SetupMgr_Phone = new ToolStripMenuItem();
            this._1_SetupMgr_Fax = new ToolStripMenuItem();
            this.menuItem24 = new ToolStripMenuItem();
            this._1_Negotiator_Name = new ToolStripMenuItem();
            this._1_Negotiator_Phone = new ToolStripMenuItem();
            this._1_Negotiator_Fax = new ToolStripMenuItem();
            this.menuItem28 = new ToolStripMenuItem();
            this._1_NegMgr_Name = new ToolStripMenuItem();
            this._1_NegMgr_Phone = new ToolStripMenuItem();
            this._1_NegMgr_Fax = new ToolStripMenuItem();
            this.menuItem16 = new ToolStripMenuItem();
            this._2_CompanyName = new ToolStripMenuItem();
            this._2_AccountNr = new ToolStripMenuItem();
            this.menuItem32 = new ToolStripMenuItem();
            this._2_SetupDept_Name = new ToolStripMenuItem();
            this._2_SetupDept_Phone = new ToolStripMenuItem();
            this._2_SetupDept_Fax = new ToolStripMenuItem();
            this.menuItem36 = new ToolStripMenuItem();
            this._2_SetupMgr_Name = new ToolStripMenuItem();
            this._2_SetupMgr_Phone = new ToolStripMenuItem();
            this._2_SetupMgr_Fax = new ToolStripMenuItem();
            this.menuItem40 = new ToolStripMenuItem();
            this._2_Negotiator_Name = new ToolStripMenuItem();
            this._2_Negotiator_Phone = new ToolStripMenuItem();
            this._2_Negotiator_Fax = new ToolStripMenuItem();
            this.menuItem44 = new ToolStripMenuItem();
            this._2_NegMgr_Name = new ToolStripMenuItem();
            this._2_NegMgr_Phone = new ToolStripMenuItem();
            this._2_NegMgr_Fax = new ToolStripMenuItem();
            this.menuItem17 = new ToolStripMenuItem();
            this._3_CompanyName = new ToolStripMenuItem();
            this._3_AccountNr = new ToolStripMenuItem();
            this.menuItem48 = new ToolStripMenuItem();
            this._3_SetupDept_Name = new ToolStripMenuItem();
            this._3_SetupDept_Phone = new ToolStripMenuItem();
            this._3_SetupDept_Fax = new ToolStripMenuItem();
            this.menuItem52 = new ToolStripMenuItem();
            this._3_SetupMgr_Name = new ToolStripMenuItem();
            this._3_SetupMgr_Phone = new ToolStripMenuItem();
            this._3_SetupMgr_Fax = new ToolStripMenuItem();
            this.menuItem62 = new ToolStripMenuItem();
            this._3_Negotiator_Name = new ToolStripMenuItem();
            this._3_Negotiator_Phone = new ToolStripMenuItem();
            this._3_Negotiator_Fax = new ToolStripMenuItem();
            this.menuItem66 = new ToolStripMenuItem();
            this._3_NegMgr_Name = new ToolStripMenuItem();
            this._3_NegMgr_Phone = new ToolStripMenuItem();
            this._3_NegMgr_Fax = new ToolStripMenuItem();
            this.opi = new ToolStripMenuItem();
            this._opi_FirstName = new ToolStripMenuItem();
            this._opi_MiddleName = new ToolStripMenuItem();
            this._opi_LastName = new ToolStripMenuItem();
            this._opi_FullName = new ToolStripMenuItem();
            this._opi_PropertyAddress = new ToolStripMenuItem();
            this._opi_MailingAddress = new ToolStripMenuItem();
            this.menuItem20 = new ToolStripMenuItem();
            this._bi_FirstName = new ToolStripMenuItem();
            this._bi_LastName = new ToolStripMenuItem();
            this._bi_FullName = new ToolStripMenuItem();
            this._bi_Address = new ToolStripMenuItem();
            this._bi_Phone = new ToolStripMenuItem();
            this._bi_Fax = new ToolStripMenuItem();
            this._bi_Email = new ToolStripMenuItem();
            this.menuItem21 = new ToolStripMenuItem();
            this._ai_FirstName = new ToolStripMenuItem();
            this._ai_LastName = new ToolStripMenuItem();
            this._ai_FullName = new ToolStripMenuItem();
            this._ai_Address = new ToolStripMenuItem();
            this._ai_Phone = new ToolStripMenuItem();
            this._ai_Fax = new ToolStripMenuItem();
            this._ai_Email = new ToolStripMenuItem();
            this.menuItem1 = new ToolStripMenuItem();
            this._currentDate = new ToolStripMenuItem();
            this._currentTime = new ToolStripMenuItem();
            this.SuspendLayout();
            // 
            // _buttonBar
            // 
            this._buttonBar.Dock = System.Windows.Forms.DockStyle.Top;
            this._buttonBar.Location = new System.Drawing.Point(0, 0);
            this._buttonBar.Name = "_buttonBar";
            this._buttonBar.Size = new System.Drawing.Size(360, 28);
            this._buttonBar.TabIndex = 0;
            this._buttonBar.Text = "buttonBar1";
            // 
            // _rulerBar
            // 
            this._rulerBar.Dock = System.Windows.Forms.DockStyle.Top;
            this._rulerBar.Location = new System.Drawing.Point(0, 28);
            this._rulerBar.Name = "_rulerBar";
            this._rulerBar.Size = new System.Drawing.Size(360, 25);
            this._rulerBar.TabIndex = 1;
            this._rulerBar.Text = "rulerBar1";
            // 
            // _statusBar
            // 
            this._statusBar.Dock = System.Windows.Forms.DockStyle.Bottom;
            this._statusBar.Location = new System.Drawing.Point(0, 253);
            this._statusBar.Name = "_statusBar";
            this._statusBar.Size = new System.Drawing.Size(360, 19);
            this._statusBar.TabIndex = 2;
            // 
            // _textControl
            // 
            this._textControl.ContextMenuStrip = this._ctxMenu;
            this._textControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this._textControl.Font = new System.Drawing.Font("Arial", 10F);
            this._textControl.Location = new System.Drawing.Point(0, 53);
            this._textControl.Name = "_textControl";
            this._textControl.PageMargins.Bottom = 79;
            this._textControl.PageMargins.Left = 79;
            this._textControl.PageMargins.Right = 79;
            this._textControl.PageMargins.Top = 79;
            this._textControl.Size = new System.Drawing.Size(360, 200);
            this._textControl.TabIndex = 3;
            this._textControl.ViewMode = TXTextControl.ViewMode.PageView;
            // 
            // _ctxMenu
            // 
            this._ctxMenu.Items.AddRange(new ToolStripMenuItem[] {
            this._new,
            this.menuItem55,
            this._copy,
            this._paste,
            this._undo,
            this._redo,
            this.menuItem56,
            this._openFile,
            this._saveFile,
            this.menuItem57,
            this._openMergeTemplate,
            this._saveMergeTemplate,
            this.menuItem58,
            this._pageSetup,
            this._printPreview,
            this._print,
            this.menuItem59,
            this._merge,
            this._switchToTemplate,
            this.menuItem60,
            this.menuItem2,
            this.menuItem18,
            this.subMenu2,
            this.menuItem16,
            this.menuItem17,
            this.opi,
            this.menuItem20,
            this.menuItem21,
            this.menuItem1,
            this._currentDate,
            this._currentTime});
            // 
            // _new
            // 
            //this._new.Index = 0;
            this._new.Text = "New";
            // 
            // menuItem55
            // 
            //this.menuItem55.Index = 1;
            this.menuItem55.Text = "-";
            // 
            // _copy
            // 
            //this._copy.Index = 2;
            this._copy.Text = "Copy";
            // 
            // _paste
            // 
            //this._paste.Index = 3;
            this._paste.Text = "Paste";
            // 
            // _undo
            // 
            //this._undo.Index = 4;
            this._undo.Text = "Undo";
            // 
            // _redo
            // 
            //this._redo.Index = 5;
            this._redo.Text = "Redo";
            // 
            // menuItem56
            // 
            //this.menuItem56.Index = 6;
            this.menuItem56.Text = "-";
            // 
            // _openFile
            // 
            //this._openFile.Index = 7;
            this._openFile.Text = "Open File";
            // 
            // _saveFile
            // 
            //this._saveFile.Index = 8;
            this._saveFile.Text = "Save File";
            // 
            // menuItem57
            // 
            //this.menuItem57.Index = 9;
            this.menuItem57.Text = "-";
            // 
            // _openMergeTemplate
            // 
            //this._openMergeTemplate.Index = 10;
            this._openMergeTemplate.Text = "Open Merge Template";
            // 
            // _saveMergeTemplate
            // 
            //this._saveMergeTemplate.Index = 11;
            this._saveMergeTemplate.Text = "Save Merge Template";
            // 
            // menuItem58
            // 
            //this.menuItem58.Index = 12;
            this.menuItem58.Text = "-";
            // 
            // _pageSetup
            // 
            //this._pageSetup.Index = 13;
            this._pageSetup.Text = "Page Setup";
            // 
            // _printPreview
            // 
            //this._printPreview.Index = 14;
            this._printPreview.Text = "Print Preview";
            // 
            // _print
            // 
            //this._print.Index = 15;
            this._print.Text = "Print";
            // 
            // menuItem59
            // 
            //this.menuItem59.Index = 16;
            this.menuItem59.Text = "-";
            // 
            // _merge
            // 
            //this._merge.Index = 17;
            this._merge.Text = "Merge";
            // 
            // _switchToTemplate
            // 
            //this._switchToTemplate.Index = 18;
            this._switchToTemplate.Text = "Switch To Template";
            // 
            // menuItem60
            // 
            //this.menuItem60.Index = 19;
            this.menuItem60.Text = "-";
            // 
            // menuItem2
            // 
            //this.menuItem2.Index = 20;
            this.menuItem2.DropDownItems.AddRange(new ToolStripMenuItem[] {
            this._companyName,
            this._companyAddress,
            this._companyCity,
            this._companyState,
            this._companyZIP,
            this._companyPhone,
            this._companyFax,
            this._companyEmail});
            this.menuItem2.Text = "Company Info";
            // 
            // _companyName
            // 
            //this._companyName.Index = 0;
            this._companyName.Text = "Name";
            this._companyName.Click += new System.EventHandler(this.On_MergeItem_Click);
            // 
            // _companyAddress
            // 
            //this._companyAddress.Index = 1;
            this._companyAddress.Text = "Address";
            this._companyAddress.Click += new System.EventHandler(this.On_MergeItem_Click);
            // 
            // _companyCity
            // 
            //this._companyCity.Index = 2;
            this._companyCity.Text = "City";
            this._companyCity.Click += new System.EventHandler(this.On_MergeItem_Click);
            // 
            // _companyState
            // 
            //this._companyState.Index = 3;
            this._companyState.Text = "State";
            this._companyState.Click += new System.EventHandler(this.On_MergeItem_Click);
            // 
            // _companyZIP
            // 
            //this._companyZIP.Index = 4;
            this._companyZIP.Text = "ZIP";
            this._companyZIP.Click += new System.EventHandler(this.On_MergeItem_Click);
            // 
            // _companyPhone
            // 
            //this._companyPhone.Index = 5;
            this._companyPhone.Text = "Phone";
            this._companyPhone.Click += new System.EventHandler(this.On_MergeItem_Click);
            // 
            // _companyFax
            // 
            //this._companyFax.Index = 6;
            this._companyFax.Text = "Fax";
            this._companyFax.Click += new System.EventHandler(this.On_MergeItem_Click);
            // 
            // _companyEmail
            // 
            //this._companyEmail.Index = 7;
            this._companyEmail.Text = "Email";
            this._companyEmail.Click += new System.EventHandler(this.On_MergeItem_Click);
            // 
            // menuItem18
            // 
            //this.menuItem18.Index = 21;
            this.menuItem18.DropDownItems.AddRange(new ToolStripMenuItem[] {
            this._ci_firstName,
            this._ci_middleName,
            this._ci_LastName,
            this._ci_FullName,
            this._ci_SiteAddress,
            this._ci_SiteCity,
            this._ci_SiteState,
            this._ci_SiteZIP});
            this.menuItem18.Text = "Contact Info";
            // 
            // _ci_firstName
            // 
            //this._ci_firstName.Index = 0;
            this._ci_firstName.Text = "First Name";
            this._ci_firstName.Click += new System.EventHandler(this.On_MergeItem_Click);
            // 
            // _ci_middleName
            // 
            //this._ci_middleName.Index = 1;
            this._ci_middleName.Text = "Middle Name";
            this._ci_middleName.Click += new System.EventHandler(this.On_MergeItem_Click);
            // 
            // _ci_LastName
            // 
            //this._ci_LastName.Index = 2;
            this._ci_LastName.Text = "Last Name";
            this._ci_LastName.Click += new System.EventHandler(this.On_MergeItem_Click);
            // 
            // _ci_FullName
            // 
            //this._ci_FullName.Index = 3;
            this._ci_FullName.Text = "Full Name";
            this._ci_FullName.Click += new System.EventHandler(this.On_MergeItem_Click);
            // 
            // _ci_SiteAddress
            // 
            //this._ci_SiteAddress.Index = 4;
            this._ci_SiteAddress.Text = "Site Address";
            this._ci_SiteAddress.Click += new System.EventHandler(this.On_MergeItem_Click);
            // 
            // _ci_SiteCity
            // 
            //this._ci_SiteCity.Index = 5;
            this._ci_SiteCity.Text = "Site City";
            this._ci_SiteCity.Click += new System.EventHandler(this.On_MergeItem_Click);
            // 
            // _ci_SiteState
            // 
            //this._ci_SiteState.Index = 6;
            this._ci_SiteState.Text = "Site State";
            this._ci_SiteState.Click += new System.EventHandler(this.On_MergeItem_Click);
            // 
            // _ci_SiteZIP
            // 
            //this._ci_SiteZIP.Index = 7;
            this._ci_SiteZIP.Text = "Site ZIP";
            this._ci_SiteZIP.Click += new System.EventHandler(this.On_MergeItem_Click);
            // 
            // subMenu2
            // 
            //this.subMenu2.Index = 22;
            this.subMenu2.DropDownItems.AddRange(new ToolStripMenuItem[] {
            this._1_CompanyName,
            this._1_AccountNr,
            this.menuItem11,
            this.menuItem15,
            this.menuItem24,
            this.menuItem28});
            this.subMenu2.Text = "1st Lien Info";
            // 
            // _1_CompanyName
            // 
            //this._1_CompanyName.Index = 0;
            this._1_CompanyName.Text = "Company Name";
            this._1_CompanyName.Click += new System.EventHandler(this.On_MergeItem_Click);
            // 
            // _1_AccountNr
            // 
            //this._1_AccountNr.Index = 1;
            this._1_AccountNr.Text = "Account #";
            this._1_AccountNr.Click += new System.EventHandler(this.On_MergeItem_Click);
            // 
            // menuItem11
            // 
            //this.menuItem11.Index = 2;
            this.menuItem11.DropDownItems.AddRange(new ToolStripMenuItem[] {
            this._1_SetupDept_Name,
            this._1_SetupDept_Phone,
            this._1_SetupDept_Fax});
            this.menuItem11.Text = "Setup Department";
            // 
            // _1_SetupDept_Name
            // 
            //this._1_SetupDept_Name.Index = 0;
            this._1_SetupDept_Name.Text = "Name";
            this._1_SetupDept_Name.Click += new System.EventHandler(this.On_MergeItem_Click);
            // 
            // _1_SetupDept_Phone
            // 
            //this._1_SetupDept_Phone.Index = 1;
            this._1_SetupDept_Phone.Text = "Phone";
            this._1_SetupDept_Phone.Click += new System.EventHandler(this.On_MergeItem_Click);
            // 
            // _1_SetupDept_Fax
            // 
            //this._1_SetupDept_Fax.Index = 2;
            this._1_SetupDept_Fax.Text = "Fax";
            this._1_SetupDept_Fax.Click += new System.EventHandler(this.On_MergeItem_Click);
            // 
            // menuItem15
            // 
            //this.menuItem15.Index = 3;
            this.menuItem15.DropDownItems.AddRange(new ToolStripMenuItem[] {
            this._1_SetupMgr_Name,
            this._1_SetupMgr_Phone,
            this._1_SetupMgr_Fax});
            this.menuItem15.Text = "Setup Manager";
            // 
            // _1_SetupMgr_Name
            // 
            //this._1_SetupMgr_Name.Index = 0;
            this._1_SetupMgr_Name.Text = "Name";
            this._1_SetupMgr_Name.Click += new System.EventHandler(this.On_MergeItem_Click);
            // 
            // _1_SetupMgr_Phone
            // 
            //this._1_SetupMgr_Phone.Index = 1;
            this._1_SetupMgr_Phone.Text = "Phone";
            this._1_SetupMgr_Phone.Click += new System.EventHandler(this.On_MergeItem_Click);
            // 
            // _1_SetupMgr_Fax
            // 
            //this._1_SetupMgr_Fax.Index = 2;
            this._1_SetupMgr_Fax.Text = "Fax";
            this._1_SetupMgr_Fax.Click += new System.EventHandler(this.On_MergeItem_Click);
            // 
            // menuItem24
            // 
            //this.menuItem24.Index = 4;
            this.menuItem24.DropDownItems.AddRange(new ToolStripMenuItem[] {
            this._1_Negotiator_Name,
            this._1_Negotiator_Phone,
            this._1_Negotiator_Fax});
            this.menuItem24.Text = "Negotiator";
            // 
            // _1_Negotiator_Name
            // 
            //this._1_Negotiator_Name.Index = 0;
            this._1_Negotiator_Name.Text = "Name";
            this._1_Negotiator_Name.Click += new System.EventHandler(this.On_MergeItem_Click);
            // 
            // _1_Negotiator_Phone
            // 
            //this._1_Negotiator_Phone.Index = 1;
            this._1_Negotiator_Phone.Text = "Phone";
            this._1_Negotiator_Phone.Click += new System.EventHandler(this.On_MergeItem_Click);
            // 
            // _1_Negotiator_Fax
            // 
            //this._1_Negotiator_Fax.Index = 2;
            this._1_Negotiator_Fax.Text = "Fax";
            this._1_Negotiator_Fax.Click += new System.EventHandler(this.On_MergeItem_Click);
            // 
            // menuItem28
            // 
            //this.menuItem28.Index = 5;
            this.menuItem28.DropDownItems.AddRange(new ToolStripMenuItem[] {
            this._1_NegMgr_Name,
            this._1_NegMgr_Phone,
            this._1_NegMgr_Fax});
            this.menuItem28.Text = "Neg. Manager";
            // 
            // _1_NegMgr_Name
            // 
            //this._1_NegMgr_Name.Index = 0;
            this._1_NegMgr_Name.Text = "Name";
            this._1_NegMgr_Name.Click += new System.EventHandler(this.On_MergeItem_Click);
            // 
            // _1_NegMgr_Phone
            // 
            //this._1_NegMgr_Phone.Index = 1;
            this._1_NegMgr_Phone.Text = "Phone";
            this._1_NegMgr_Phone.Click += new System.EventHandler(this.On_MergeItem_Click);
            // 
            // _1_NegMgr_Fax
            // 
            //this._1_NegMgr_Fax.Index = 2;
            this._1_NegMgr_Fax.Text = "Fax";
            this._1_NegMgr_Fax.Click += new System.EventHandler(this.On_MergeItem_Click);
            // 
            // menuItem16
            // 
            //this.menuItem16.Index = 23;
            this.menuItem16.DropDownItems.AddRange(new ToolStripMenuItem[] {
            this._2_CompanyName,
            this._2_AccountNr,
            this.menuItem32,
            this.menuItem36,
            this.menuItem40,
            this.menuItem44});
            this.menuItem16.Text = "2nd Lien Info";
            // 
            // _2_CompanyName
            // 
            //this._2_CompanyName.Index = 0;
            this._2_CompanyName.Text = "Company Name";
            this._2_CompanyName.Click += new System.EventHandler(this.On_MergeItem_Click);
            // 
            // _2_AccountNr
            // 
            //this._2_AccountNr.Index = 1;
            this._2_AccountNr.Text = "Account #";
            this._2_AccountNr.Click += new System.EventHandler(this.On_MergeItem_Click);
            // 
            // menuItem32
            // 
            //this.menuItem32.Index = 2;
            this.menuItem32.DropDownItems.AddRange(new ToolStripMenuItem[] {
            this._2_SetupDept_Name,
            this._2_SetupDept_Phone,
            this._2_SetupDept_Fax});
            this.menuItem32.Text = "Setup Department";
            // 
            // _2_SetupDept_Name
            // 
            //this._2_SetupDept_Name.Index = 0;
            this._2_SetupDept_Name.Text = "Name";
            this._2_SetupDept_Name.Click += new System.EventHandler(this.On_MergeItem_Click);
            // 
            // _2_SetupDept_Phone
            // 
            //this._2_SetupDept_Phone.Index = 1;
            this._2_SetupDept_Phone.Text = "Phone";
            this._2_SetupDept_Phone.Click += new System.EventHandler(this.On_MergeItem_Click);
            // 
            // _2_SetupDept_Fax
            // 
            //this._2_SetupDept_Fax.Index = 2;
            this._2_SetupDept_Fax.Text = "Fax";
            this._2_SetupDept_Fax.Click += new System.EventHandler(this.On_MergeItem_Click);
            // 
            // menuItem36
            // 
            //this.menuItem36.Index = 3;
            this.menuItem36.DropDownItems.AddRange(new ToolStripMenuItem[] {
            this._2_SetupMgr_Name,
            this._2_SetupMgr_Phone,
            this._2_SetupMgr_Fax});
            this.menuItem36.Text = "Setup Manager";
            // 
            // _2_SetupMgr_Name
            // 
            //this._2_SetupMgr_Name.Index = 0;
            this._2_SetupMgr_Name.Text = "Name";
            this._2_SetupMgr_Name.Click += new System.EventHandler(this.On_MergeItem_Click);
            // 
            // _2_SetupMgr_Phone
            // 
            //this._2_SetupMgr_Phone.Index = 1;
            this._2_SetupMgr_Phone.Text = "Phone";
            this._2_SetupMgr_Phone.Click += new System.EventHandler(this.On_MergeItem_Click);
            // 
            // _2_SetupMgr_Fax
            // 
            //this._2_SetupMgr_Fax.Index = 2;
            this._2_SetupMgr_Fax.Text = "Fax";
            this._2_SetupMgr_Fax.Click += new System.EventHandler(this.On_MergeItem_Click);
            // 
            // menuItem40
            // 
            //this.menuItem40.Index = 4;
            this.menuItem40.DropDownItems.AddRange(new ToolStripMenuItem[] {
            this._2_Negotiator_Name,
            this._2_Negotiator_Phone,
            this._2_Negotiator_Fax});
            this.menuItem40.Text = "Negotiator";
            // 
            // _2_Negotiator_Name
            // 
            //this._2_Negotiator_Name.Index = 0;
            this._2_Negotiator_Name.Text = "Name";
            this._2_Negotiator_Name.Click += new System.EventHandler(this.On_MergeItem_Click);
            // 
            // _2_Negotiator_Phone
            // 
            //this._2_Negotiator_Phone.Index = 1;
            this._2_Negotiator_Phone.Text = "Phone";
            this._2_Negotiator_Phone.Click += new System.EventHandler(this.On_MergeItem_Click);
            // 
            // _2_Negotiator_Fax
            // 
            //this._2_Negotiator_Fax.Index = 2;
            this._2_Negotiator_Fax.Text = "Fax";
            this._2_Negotiator_Fax.Click += new System.EventHandler(this.On_MergeItem_Click);
            // 
            // menuItem44
            // 
            //this.menuItem44.Index = 5;
            this.menuItem44.DropDownItems.AddRange(new ToolStripMenuItem[] {
            this._2_NegMgr_Name,
            this._2_NegMgr_Phone,
            this._2_NegMgr_Fax});
            this.menuItem44.Text = "Neg. Manager";
            // 
            // _2_NegMgr_Name
            // 
            //this._2_NegMgr_Name.Index = 0;
            this._2_NegMgr_Name.Text = "Name";
            this._2_NegMgr_Name.Click += new System.EventHandler(this.On_MergeItem_Click);
            // 
            // _2_NegMgr_Phone
            // 
            //this._2_NegMgr_Phone.Index = 1;
            this._2_NegMgr_Phone.Text = "Phone";
            this._2_NegMgr_Phone.Click += new System.EventHandler(this.On_MergeItem_Click);
            // 
            // _2_NegMgr_Fax
            // 
            //this._2_NegMgr_Fax.Index = 2;
            this._2_NegMgr_Fax.Text = "Fax";
            this._2_NegMgr_Fax.Click += new System.EventHandler(this.On_MergeItem_Click);
            // 
            // menuItem17
            // 
            //this.menuItem17.Index = 24;
            this.menuItem17.DropDownItems.AddRange(new ToolStripMenuItem[] {
            this._3_CompanyName,
            this._3_AccountNr,
            this.menuItem48,
            this.menuItem52,
            this.menuItem62,
            this.menuItem66});
            this.menuItem17.Text = "3rd Lien Info";
            // 
            // _3_CompanyName
            // 
            //this._3_CompanyName.Index = 0;
            this._3_CompanyName.Text = "Company Name";
            this._3_CompanyName.Click += new System.EventHandler(this.On_MergeItem_Click);
            // 
            // _3_AccountNr
            // 
            //this._3_AccountNr.Index = 1;
            this._3_AccountNr.Text = "Account #";
            this._3_AccountNr.Click += new System.EventHandler(this.On_MergeItem_Click);
            // 
            // menuItem48
            // 
            //this.menuItem48.Index = 2;
            this.menuItem48.DropDownItems.AddRange(new ToolStripMenuItem[] {
            this._3_SetupDept_Name,
            this._3_SetupDept_Phone,
            this._3_SetupDept_Fax});
            this.menuItem48.Text = "Setup Department";
            // 
            // _3_SetupDept_Name
            // 
            //this._3_SetupDept_Name.Index = 0;
            this._3_SetupDept_Name.Text = "Name";
            this._3_SetupDept_Name.Click += new System.EventHandler(this.On_MergeItem_Click);
            // 
            // _3_SetupDept_Phone
            // 
            //this._3_SetupDept_Phone.Index = 1;
            this._3_SetupDept_Phone.Text = "Phone";
            this._3_SetupDept_Phone.Click += new System.EventHandler(this.On_MergeItem_Click);
            // 
            // _3_SetupDept_Fax
            // 
            //this._3_SetupDept_Fax.Index = 2;
            this._3_SetupDept_Fax.Text = "Fax";
            this._3_SetupDept_Fax.Click += new System.EventHandler(this.On_MergeItem_Click);
            // 
            // menuItem52
            // 
            //this.menuItem52.Index = 3;
            this.menuItem52.DropDownItems.AddRange(new ToolStripMenuItem[] {
            this._3_SetupMgr_Name,
            this._3_SetupMgr_Phone,
            this._3_SetupMgr_Fax});
            this.menuItem52.Text = "Setup Manager";
            // 
            // _3_SetupMgr_Name
            // 
            //this._3_SetupMgr_Name.Index = 0;
            this._3_SetupMgr_Name.Text = "Name";
            this._3_SetupMgr_Name.Click += new System.EventHandler(this.On_MergeItem_Click);
            // 
            // _3_SetupMgr_Phone
            // 
            //this._3_SetupMgr_Phone.Index = 1;
            this._3_SetupMgr_Phone.Text = "Phone";
            this._3_SetupMgr_Phone.Click += new System.EventHandler(this.On_MergeItem_Click);
            // 
            // _3_SetupMgr_Fax
            // 
            //this._3_SetupMgr_Fax.Index = 2;
            this._3_SetupMgr_Fax.Text = "Fax";
            this._3_SetupMgr_Fax.Click += new System.EventHandler(this.On_MergeItem_Click);
            // 
            // menuItem62
            // 
            //this.menuItem62.Index = 4;
            this.menuItem62.DropDownItems.AddRange(new ToolStripMenuItem[] {
            this._3_Negotiator_Name,
            this._3_Negotiator_Phone,
            this._3_Negotiator_Fax});
            this.menuItem62.Text = "Negotiator";
            // 
            // _3_Negotiator_Name
            // 
            //this._3_Negotiator_Name.Index = 0;
            this._3_Negotiator_Name.Text = "Name";
            this._3_Negotiator_Name.Click += new System.EventHandler(this.On_MergeItem_Click);
            // 
            // _3_Negotiator_Phone
            // 
            //this._3_Negotiator_Phone.Index = 1;
            this._3_Negotiator_Phone.Text = "Phone";
            this._3_Negotiator_Phone.Click += new System.EventHandler(this.On_MergeItem_Click);
            // 
            // _3_Negotiator_Fax
            // 
            //this._3_Negotiator_Fax.Index = 2;
            this._3_Negotiator_Fax.Text = "Fax";
            this._3_Negotiator_Fax.Click += new System.EventHandler(this.On_MergeItem_Click);
            // 
            // menuItem66
            // 
            //this.menuItem66.Index = 5;
            this.menuItem66.DropDownItems.AddRange(new ToolStripMenuItem[] {
            this._3_NegMgr_Name,
            this._3_NegMgr_Phone,
            this._3_NegMgr_Fax});
            this.menuItem66.Text = "Neg. Manager";
            // 
            // _3_NegMgr_Name
            // 
            //this._3_NegMgr_Name.Index = 0;
            this._3_NegMgr_Name.Text = "Name";
            this._3_NegMgr_Name.Click += new System.EventHandler(this.On_MergeItem_Click);
            // 
            // _3_NegMgr_Phone
            // 
            //this._3_NegMgr_Phone.Index = 1;
            this._3_NegMgr_Phone.Text = "Phone";
            this._3_NegMgr_Phone.Click += new System.EventHandler(this.On_MergeItem_Click);
            // 
            // _3_NegMgr_Fax
            // 
            //this._3_NegMgr_Fax.Index = 2;
            this._3_NegMgr_Fax.Text = "Fax";
            this._3_NegMgr_Fax.Click += new System.EventHandler(this.On_MergeItem_Click);
            // 
            // opi
            // 
            //this.opi.Index = 25;
            this.opi.DropDownItems.AddRange(new ToolStripMenuItem[] {
            this._opi_FirstName,
            this._opi_MiddleName,
            this._opi_LastName,
            this._opi_FullName,
            this._opi_PropertyAddress,
            this._opi_MailingAddress});
            this.opi.Text = "Owner Property Info";
            // 
            // _opi_FirstName
            // 
            //this._opi_FirstName.Index = 0;
            this._opi_FirstName.Text = "First Name";
            this._opi_FirstName.Click += new System.EventHandler(this.On_MergeItem_Click);
            // 
            // _opi_MiddleName
            // 
            //this._opi_MiddleName.Index = 1;
            this._opi_MiddleName.Text = "Middle Name";
            this._opi_MiddleName.Click += new System.EventHandler(this.On_MergeItem_Click);
            // 
            // _opi_LastName
            // 
            //this._opi_LastName.Index = 2;
            this._opi_LastName.Text = "Last Name";
            this._opi_LastName.Click += new System.EventHandler(this.On_MergeItem_Click);
            // 
            // _opi_FullName
            // 
            //this._opi_FullName.Index = 3;
            this._opi_FullName.Text = "Full Name";
            this._opi_FullName.Click += new System.EventHandler(this.On_MergeItem_Click);
            // 
            // _opi_PropertyAddress
            // 
            //this._opi_PropertyAddress.Index = 4;
            this._opi_PropertyAddress.Text = "Property Address";
            this._opi_PropertyAddress.Click += new System.EventHandler(this.On_MergeItem_Click);
            // 
            // _opi_MailingAddress
            // 
            //this._opi_MailingAddress.Index = 5;
            this._opi_MailingAddress.Text = "Mailing Address";
            this._opi_MailingAddress.Click += new System.EventHandler(this.On_MergeItem_Click);
            // 
            // menuItem20
            // 
            //this.menuItem20.Index = 26;
            this.menuItem20.DropDownItems.AddRange(new ToolStripMenuItem[] {
            this._bi_FirstName,
            this._bi_LastName,
            this._bi_FullName,
            this._bi_Address,
            this._bi_Phone,
            this._bi_Fax,
            this._bi_Email});
            this.menuItem20.Text = "Buyer Info";
            // 
            // _bi_FirstName
            // 
            //this._bi_FirstName.Index = 0;
            this._bi_FirstName.Text = "First Name";
            this._bi_FirstName.Click += new System.EventHandler(this.On_MergeItem_Click);
            // 
            // _bi_LastName
            // 
            //this._bi_LastName.Index = 1;
            this._bi_LastName.Text = "Last Name";
            this._bi_LastName.Click += new System.EventHandler(this.On_MergeItem_Click);
            // 
            // _bi_FullName
            // 
            //this._bi_FullName.Index = 2;
            this._bi_FullName.Text = "Full Name";
            this._bi_FullName.Click += new System.EventHandler(this.On_MergeItem_Click);
            // 
            // _bi_Address
            // 
            //this._bi_Address.Index = 3;
            this._bi_Address.Text = "Address";
            this._bi_Address.Click += new System.EventHandler(this.On_MergeItem_Click);
            // 
            // _bi_Phone
            // 
            //this._bi_Phone.Index = 4;
            this._bi_Phone.Text = "Phone";
            this._bi_Phone.Click += new System.EventHandler(this.On_MergeItem_Click);
            // 
            // _bi_Fax
            // 
            //this._bi_Fax.Index = 5;
            this._bi_Fax.Text = "Fax";
            this._bi_Fax.Click += new System.EventHandler(this.On_MergeItem_Click);
            // 
            // _bi_Email
            // 
            //this._bi_Email.Index = 6;
            this._bi_Email.Text = "Email";
            this._bi_Email.Click += new System.EventHandler(this.On_MergeItem_Click);
            // 
            // menuItem21
            // 
            //this.menuItem21.Index = 27;
            this.menuItem21.DropDownItems.AddRange(new ToolStripMenuItem[] {
            this._ai_FirstName,
            this._ai_LastName,
            this._ai_FullName,
            this._ai_Address,
            this._ai_Phone,
            this._ai_Fax,
            this._ai_Email});
            this.menuItem21.Text = "Agent Info";
            // 
            // _ai_FirstName
            // 
            //this._ai_FirstName.Index = 0;
            this._ai_FirstName.Text = "First Name";
            this._ai_FirstName.Click += new System.EventHandler(this.On_MergeItem_Click);
            // 
            // _ai_LastName
            // 
            //this._ai_LastName.Index = 1;
            this._ai_LastName.Text = "Last Name";
            this._ai_LastName.Click += new System.EventHandler(this.On_MergeItem_Click);
            // 
            // _ai_FullName
            // 
            //this._ai_FullName.Index = 2;
            this._ai_FullName.Text = "Full Name";
            this._ai_FullName.Click += new System.EventHandler(this.On_MergeItem_Click);
            // 
            // _ai_Address
            // 
            //this._ai_Address.Index = 3;
            this._ai_Address.Text = "Address";
            this._ai_Address.Click += new System.EventHandler(this.On_MergeItem_Click);
            // 
            // _ai_Phone
            // 
            //this._ai_Phone.Index = 4;
            this._ai_Phone.Text = "Phone";
            this._ai_Phone.Click += new System.EventHandler(this.On_MergeItem_Click);
            // 
            // _ai_Fax
            // 
            //this._ai_Fax.Index = 5;
            this._ai_Fax.Text = "Fax";
            this._ai_Fax.Click += new System.EventHandler(this.On_MergeItem_Click);
            // 
            // _ai_Email
            // 
            //this._ai_Email.Index = 6;
            this._ai_Email.Text = "Email";
            this._ai_Email.Click += new System.EventHandler(this.On_MergeItem_Click);
            // 
            // menuItem1
            // 
            //this.menuItem1.Index = 28;
            this.menuItem1.Text = "-";
            // 
            // _currentDate
            // 
            //this._currentDate.Index = 29;
            this._currentDate.Text = "Current Date";
            this._currentDate.Click += new System.EventHandler(this.On_MergeItem_Click);
            // 
            // _currentTime
            // 
            //this._currentTime.Index = 30;
            this._currentTime.Text = "Current Time";
            this._currentTime.Click += new System.EventHandler(this.On_MergeItem_Click);
            // 
            // DocumentTemplate
            // 
            this.Controls.Add(this._textControl);
            this.Controls.Add(this._statusBar);
            this.Controls.Add(this._rulerBar);
            this.Controls.Add(this._buttonBar);
            this.Name = "DocumentTemplate";
            this.Size = new System.Drawing.Size(360, 272);
            this.ResumeLayout(false);

		}
		#endregion


        public string UserAppFolder
        {
            get { return _userAppFolder; }
            set { _userAppFolder = value; }
        }


		public void SetMailMergeTemplateReadyForOpen(LienSet lienSet, string fileName)
		{
			_fileName = fileName;
			_lienSet = lienSet;
		}

		private void On_New_Click(object sender, EventArgs e)
		{
			DialogResult dr = MessageBox.Show("Do you want to save an existing file?", StringStore.AppName, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);
			if (DialogResult.Yes == dr)
				On_SaveFile_Click(null, null);
			if (DialogResult.Cancel == dr)
				return;

			if (false == _textControl.Focused)
				_textControl.Focus();

			_textControl.SelectAll();
			_textControl.Clear();							
						
			try { _textControl.TextFrames.Clear(); }
			catch {}	
		}

		private void On_Copy_Click(object sender, EventArgs e)
		{
			_textControl.Copy();
		}

		private void On_Paste_Click(object sender, EventArgs e)
		{
			_textControl.Paste();
		}

		private void On_Undo_Click(object sender, EventArgs e)
		{
			_textControl.Undo();
		}

		private void On_Redo_Click(object sender, EventArgs e)
		{
			_textControl.Redo();
		}

		private void On_OpenFile_Click(object sender, EventArgs e)
		{
			try
			{				
				TXTextControl.LoadSettings loadSettings = new TXTextControl.LoadSettings();
				OpenFileDialog dlgOpenFile = new OpenFileDialog();			
				
				if (null != DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.ArchiveFolder && string.Empty != DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.ArchiveFolder)
					dlgOpenFile.InitialDirectory = DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.ArchiveFolder + GetLienSetSuffix();
				else
					dlgOpenFile.InitialDirectory = UserAppFolder + _pathSuffix;

				dlgOpenFile.Filter = "Rich Text Format Files (*.rtf)|*.rtf|MS Word Files (*.doc)|*.doc";			
				dlgOpenFile.ShowDialog();
				try
				{
					if (dlgOpenFile.FileName != "") 
					{
						string ext = dlgOpenFile.FileName.Substring(dlgOpenFile.FileName.Length - 3, 3);
						if (ext.ToUpper() == "DOC")
						{
							_textControl.Load(dlgOpenFile.FileName, TXTextControl.StreamType.MSWord, loadSettings);
						}
						else if (ext.ToUpper() == "RTF")
						{
							_textControl.Load(dlgOpenFile.FileName, TXTextControl.StreamType.RichTextFormat, loadSettings);
						}
						else
							return;

						_textControl.PageMargins = loadSettings.PageMargins;
						_textControl.PageSize = loadSettings.PageSize;
					}
				}
				catch (Exception exc)
				{
					string s = exc.Message;
				}
				this._textControl.Focus();
                
				//TA--
			}
			catch (Exception exc)
			{
				MessageBox.Show(exc.Message, StringStore.AppName);
				return;
			}
		}

		string GetLienSetSuffix()
		{			
			switch (_lienSet)
			{
				case LienSet.FirstLienSet:
					return @"\1stLien";
					

				case LienSet.SecondLienSet:
					return @"\2ndLien";

				case LienSet.ThirdLienSet:
					return @"\3rdLien";

				default:
					return string.Empty;
			}
		}

		private void On_SaveFile_Click(object sender, EventArgs e)
		{
			System.Windows.Forms.SaveFileDialog dlgSaveFile = new SaveFileDialog();
			dlgSaveFile.Filter = "Text Control Files (*.rtf)|*.rtf|MS Word Files (*.doc)|*.doc";
			
			

			if (null != DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.ArchiveFolder && string.Empty != DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.ArchiveFolder)
				dlgSaveFile.InitialDirectory = DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.ArchiveFolder + GetLienSetSuffix();//DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.ArchiveFolder + DataManager.Instance.ShortDocumentName + GetLienSetSuffix();
			else
				dlgSaveFile.InitialDirectory = UserAppFolder + _pathSuffix;

			dlgSaveFile.ShowDialog();

			if (dlgSaveFile.FileName != "") 
			{
				string ext = dlgSaveFile.FileName.Substring(dlgSaveFile.FileName.Length - 3, 3);
				if (ext.ToUpper() == "DOC")
				{
					_textControl.Save(dlgSaveFile.FileName, TXTextControl.StreamType.MSWord);
				}
				else if (ext.ToUpper() == "RTF")
				{
					_textControl.Save(dlgSaveFile.FileName, TXTextControl.StreamType.RichTextFormat);
				}
				else
					return;
			}
		}

		void OpenMailMergeTemplate(string fileName)
		{
			TXTextControl.LoadSettings loadSettings = new TXTextControl.LoadSettings();
			try 
			{                
				_textControl.Load(Application.StartupPath + _pathSuffix + fileName, TXTextControl.StreamType.InternalFormat, loadSettings);
				_textControl.PageMargins = loadSettings.PageMargins;
				_textControl.PageSize = loadSettings.PageSize;

                //Merge upon Open
                On_Merge_Click(null, null);
			}
			catch (Exception exc)
			{
				MessageBox.Show(exc.Message, StringStore.AppName);
			}
		}

		private void On_OpenMergeTemplate_Click(object sender, EventArgs e)
		{
			try
			{
				TXTextControl.LoadSettings loadSettings = new TXTextControl.LoadSettings();
				OpenFileDialog dlgOpenFile = new OpenFileDialog();			
				dlgOpenFile.InitialDirectory = UserAppFolder + _pathSuffix;
				dlgOpenFile.Filter = "Text Control Files (*.tx)|*.tx";			
				dlgOpenFile.ShowDialog();
				if (dlgOpenFile.FileName != "") 
				{
					_textControl.Load(dlgOpenFile.FileName, TXTextControl.StreamType.InternalFormat, loadSettings);
					_textControl.PageMargins = loadSettings.PageMargins;
					_textControl.PageSize = loadSettings.PageSize;
				}
			}
			catch (Exception exc)
			{
				MessageBox.Show(exc.Message, StringStore.AppName);
			}            

			_textControl.Focus();
		}

		private void On_SaveMergeTemplate_Click(object sender, EventArgs e)
		{
			TXTextControl.SaveSettings saveSettings = new TXTextControl.SaveSettings();
			saveSettings.PageMargins = _textControl.PageMargins;
			saveSettings.PageSize	 = _textControl.PageSize;
			
			SaveFileDialog dlgSaveFile = new SaveFileDialog();
			dlgSaveFile.InitialDirectory = Application.StartupPath + _pathSuffix;//DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.ArchiveFolder + DataManager.Instance.ShortDocumentName;//Application.StartupPath + _pathSuffix;
			dlgSaveFile.Filter = "Text Control Files (*.tx)|*.tx";
			dlgSaveFile.ShowDialog();
			if (dlgSaveFile.FileName != "") 
				_textControl.Save(dlgSaveFile.FileName, TXTextControl.StreamType.InternalFormat, saveSettings);	
		}

		private void On_PageSetup_Click(object sender, EventArgs e)
		{
            DealMaker.UserControls.ShortSale.TxSupport.PageSetup ps = new DealMaker.UserControls.ShortSale.TxSupport.PageSetup();
			ps.ShowDialog(_textControl);
		}

		private void On_PrintPreview_Click(object sender, EventArgs e)
		{
			try
			{
                //Merge before print
                On_Merge_Click(null, null);

				PrintDocument pd = new PrintDocument();			
				_textControl.PrintPreview(pd);
			}
			catch(Exception exc)
			{
				MessageBox.Show(exc.Message, StringStore.AppName);
			}
		}

		private void On_Print_Click(object sender, EventArgs e)
		{
			try
			{
                //Merge before print
                On_Merge_Click(null, null);

				PrintDialog pDlg = new PrintDialog();
				PrintDocument pDoc = new PrintDocument();

                pDoc.DocumentName = "DocumentName";
                pDoc.PrinterSettings.PrintFileName = "FileName";

				pDlg.Document = pDoc;
				pDlg.AllowSomePages = true;
				pDlg.AllowPrintToFile = true;
				pDlg.PrinterSettings.FromPage = 1;
				pDlg.PrinterSettings.ToPage = _textControl.Pages;
                


				if (pDlg.ShowDialog() == DialogResult.OK )
				{                    
					_textControl.Print(pDoc);
				}
			}
			catch(Exception exc)
			{
				MessageBox.Show(exc.Message, StringStore.AppName);
			}
		}		
		
		private void On_MergeItem_Click(object sender, System.EventArgs e)
		{	
			TXTextControl.TextField textField = new TXTextControl.TextField();
								
			if (_ai_FirstName == sender)			
			{
				textField.Text = MailMergeItem.Instance.GetDescriptor("_ai_FirstName");			
				textField.Name = "_ai_FirstName";
			}
			else if (_ai_LastName == sender)
			{
				textField.Text = MailMergeItem.Instance.GetDescriptor("_ai_LastName");			
				textField.Name = "_ai_LastName";
			}
			else if (_ai_FullName == sender)
			{
				textField.Text = MailMergeItem.Instance.GetDescriptor("_ai_FullName");			
				textField.Name = "_ai_FullName";
			}
			else if (_ai_Address == sender)
			{
				textField.Text = MailMergeItem.Instance.GetDescriptor("_ai_Address");			
				textField.Name = "_ai_Address";
			}
			else if (_ai_Phone == sender)
			{
				textField.Text = MailMergeItem.Instance.GetDescriptor("_ai_Phone");			
				textField.Name = "_ai_Phone";
			}
			else if (_ai_Fax == sender)
			{
				textField.Text = MailMergeItem.Instance.GetDescriptor("_ai_Fax");			
				textField.Name = "_ai_Fax";
			}
			else if (_ai_Email == sender)
			{
				textField.Text = MailMergeItem.Instance.GetDescriptor("_ai_Email");			
				textField.Name = "_ai_Email";
			}
			else if (_bi_FirstName == sender)
			{
				textField.Text = MailMergeItem.Instance.GetDescriptor("_bi_FirstName");			
				textField.Name = "_bi_FirstName";
			}
			else if (_bi_LastName == sender)
			{
				textField.Text = MailMergeItem.Instance.GetDescriptor("_bi_LastName");			
				textField.Name = "_bi_LastName";
			}
			else if (_bi_FullName == sender)
			{
				textField.Text = MailMergeItem.Instance.GetDescriptor("_bi_FullName");			
				textField.Name = "_bi_FullName";
			}
			else if (_bi_Address == sender)
			{
				textField.Text = MailMergeItem.Instance.GetDescriptor("_bi_Address");			
				textField.Name = "_bi_Address";
			}
			else if (_bi_Phone == sender)
			{
				textField.Text = MailMergeItem.Instance.GetDescriptor("_bi_Phone");			
				textField.Name = "_bi_Phone";
			}
			else if (_bi_Fax == sender)
			{
				textField.Text = MailMergeItem.Instance.GetDescriptor("_bi_Fax");			
				textField.Name = "_bi_Fax";
			}
			else if (_bi_Email == sender)
			{
				textField.Text = MailMergeItem.Instance.GetDescriptor("_bi_Email");			
				textField.Name = "_bi_Email";
			}
			else if (_opi_FirstName == sender)
			{
				textField.Text = MailMergeItem.Instance.GetDescriptor("_opi_FirstName");			
				textField.Name = "_opi_FirstName";
			}
			else if (_opi_LastName == sender)
			{
				textField.Text = MailMergeItem.Instance.GetDescriptor("_opi_LastName");			
				textField.Name = "_opi_LastName";
			}
			else if (_opi_MiddleName == sender)
			{
				textField.Text = MailMergeItem.Instance.GetDescriptor("_opi_MiddleName");			
				textField.Name = "_opi_MiddleName";
			}
			else if (_opi_FullName == sender)
			{
				textField.Text = MailMergeItem.Instance.GetDescriptor("_opi_FullName");			
				textField.Name = "_opi_FullName";
			}
			else if (_opi_PropertyAddress == sender)
			{
				textField.Text = MailMergeItem.Instance.GetDescriptor("_opi_PropertyAddress");			
				textField.Name = "_opi_PropertyAddress";
			}
			else if (_opi_MailingAddress == sender)
			{
				textField.Text = MailMergeItem.Instance.GetDescriptor("_opi_MailingAddress");			
				textField.Name = "_opi_MailingAddress";
			}
			else if (_3_CompanyName == sender)
			{
				textField.Text = MailMergeItem.Instance.GetDescriptor("_3_CompanyName");			
				textField.Name = "_3_CompanyName";
			}
			else if (_3_AccountNr == sender)
			{
				textField.Text =  MailMergeItem.Instance.GetDescriptor("_3_AccountNr");			
				textField.Name = "_3_AccountNr";
			}
			else if (_2_CompanyName == sender)
			{
				textField.Text = MailMergeItem.Instance.GetDescriptor("_2_CompanyName");			
				textField.Name = "_2_CompanyName";
			}
			else if (_2_AccountNr == sender)
			{
				textField.Text = MailMergeItem.Instance.GetDescriptor("_2_AccountNr");			
				textField.Name = "_2_AccountNr";
			}
            else if (_1_CompanyName == sender)
			{
				textField.Text = MailMergeItem.Instance.GetDescriptor("_1_CompanyName");			
				textField.Name = "_1_CompanyName";
			}
			else if (_1_AccountNr == sender)
			{
				textField.Text = MailMergeItem.Instance.GetDescriptor("_1_AccountNr");			
				textField.Name = "_1_AccountNr";
			}
			else if (_ci_firstName == sender)
			{
				textField.Text = MailMergeItem.Instance.GetDescriptor("_ci_firstName");			
				textField.Name = "_ci_firstName";
			}
			else if (_ci_middleName == sender)
			{
				textField.Text = MailMergeItem.Instance.GetDescriptor("_ci_middleName");			
				textField.Name = "_ci_middleName";
			}
			else if (_ci_LastName == sender)
			{
				textField.Text = MailMergeItem.Instance.GetDescriptor("_ci_LastName");			
				textField.Name = "_ci_LastName";
			}
			else if (_ci_FullName == sender)
			{
				textField.Text = MailMergeItem.Instance.GetDescriptor("_ci_FullName");			
				textField.Name = "_ci_FullName";
			}
			else if (_ci_SiteAddress == sender)
			{
				textField.Text = MailMergeItem.Instance.GetDescriptor("_ci_SiteAddress");			
				textField.Name = "_ci_SiteAddress";
			}
			else if (_ci_SiteCity == sender)
			{
				textField.Text = MailMergeItem.Instance.GetDescriptor("_ci_SiteCity");			
				textField.Name = "_ci_SiteCity";
			}
			else if (_ci_SiteState == sender)
			{
				textField.Text = MailMergeItem.Instance.GetDescriptor("_ci_SiteState");			
				textField.Name = "_ci_SiteState";
			}
			else if (_ci_SiteZIP == sender)
			{
				textField.Text = MailMergeItem.Instance.GetDescriptor("_ci_SiteZIP");			
				textField.Name = "_ci_SiteZIP";
			}
            else if (_currentDate == sender)
            {
                textField.Text = MailMergeItem.Instance.GetDescriptor("_currentDate");
                textField.Name = "_currentDate";
            }
            else if (_currentTime == sender)
            {
                textField.Text = MailMergeItem.Instance.GetDescriptor("_currentTime");
                textField.Name = "_currentTime";
            }


            //COMPANY DATA
            else if (_companyName == sender)
            {
                textField.Text = MailMergeItem.Instance.GetDescriptor("_companyName");
                textField.Name = "_companyName";
            }
            else if (_companyAddress == sender)
            {
                textField.Text = MailMergeItem.Instance.GetDescriptor("_companyAddress");
                textField.Name = "_companyAddress";
            }
            else if (_companyCity == sender)
            {
                textField.Text = MailMergeItem.Instance.GetDescriptor("_companyCity");
                textField.Name = "_companyCity";
            }
            else if (_companyState == sender)
            {
                textField.Text = MailMergeItem.Instance.GetDescriptor("_companyState");
                textField.Name = "_companyState";
            }
            else if (_companyZIP == sender)
            {
                textField.Text = MailMergeItem.Instance.GetDescriptor("_companyZIP");
                textField.Name = "_companyZIP";
            }
            else if (_companyPhone == sender)
            {
                textField.Text = MailMergeItem.Instance.GetDescriptor("_companyPhone");
                textField.Name = "_companyPhone";
            }
            else if (_companyFax == sender)
            {
                textField.Text = MailMergeItem.Instance.GetDescriptor("_companyFax");
                textField.Name = "_companyFax";
            }
            else if (_companyEmail == sender)
            {
                textField.Text = MailMergeItem.Instance.GetDescriptor("_companyEmail");
                textField.Name = "_companyEmail";
            }

                        
            //LIEN DATA
            else if (_1_SetupDept_Name == sender)
            {
                textField.Text = MailMergeItem.Instance.GetDescriptor("_1_SetupDept_Name");
                textField.Name = "_1_SetupDept_Name";
            }
            else if (_1_SetupDept_Phone == sender)
            {
                textField.Text = MailMergeItem.Instance.GetDescriptor("_1_SetupDept_Phone");
                textField.Name = "_1_SetupDept_Phone";
            }
            else if (_1_SetupDept_Fax == sender)
            {
                textField.Text = MailMergeItem.Instance.GetDescriptor("_1_SetupDept_Fax");
                textField.Name = "_1_SetupDept_Fax";
            }
            else if (_1_SetupMgr_Name == sender)
            {
                textField.Text = MailMergeItem.Instance.GetDescriptor("_1_SetupMgr_Name");
                textField.Name = "_1_SetupMgr_Name";
            }
            else if (_1_SetupMgr_Phone == sender)
            {
                textField.Text = MailMergeItem.Instance.GetDescriptor("_1_SetupMgr_Phone");
                textField.Name = "_1_SetupMgr_Phone";
            }
            else if (_1_SetupMgr_Fax == sender)
            {
                textField.Text = MailMergeItem.Instance.GetDescriptor("_1_SetupMgr_Fax");
                textField.Name = "_1_SetupMgr_Fax";
            }
            else if (_1_Negotiator_Name == sender)
            {
                textField.Text = MailMergeItem.Instance.GetDescriptor("_1_Negotiator_Name");
                textField.Name = "_1_Negotiator_Name";
            }
            else if (_1_Negotiator_Phone == sender)
            {
                textField.Text = MailMergeItem.Instance.GetDescriptor("_1_Negotiator_Phone");
                textField.Name = "_1_Negotiator_Phone";
            }
            else if (_1_Negotiator_Fax == sender)
            {
                textField.Text = MailMergeItem.Instance.GetDescriptor("_1_Negotiator_Fax");
                textField.Name = "_1_Negotiator_Fax";
            }
            else if (_1_NegMgr_Name == sender)
            {
                textField.Text = MailMergeItem.Instance.GetDescriptor("_1_NegMgr_Name");
                textField.Name = "_1_NegMgr_Name";
            }
            else if (_1_NegMgr_Phone == sender)
            {
                textField.Text = MailMergeItem.Instance.GetDescriptor("_1_NegMgr_Phone");
                textField.Name = "_1_NegMgr_Phone";
            }
            else if (_1_NegMgr_Fax == sender)
            {
                textField.Text = MailMergeItem.Instance.GetDescriptor("_1_NegMgr_Fax");
                textField.Name = "_1_NegMgr_Fax";
            }
            else if (_2_SetupDept_Name == sender)
            {
                textField.Text = MailMergeItem.Instance.GetDescriptor("_2_SetupDept_Name");
                textField.Name = "_2_SetupDept_Name";
            }
            else if (_2_SetupDept_Phone == sender)
            {
                textField.Text = MailMergeItem.Instance.GetDescriptor("_2_SetupDept_Phone");
                textField.Name = "_2_SetupDept_Phone";
            }
            else if (_2_SetupDept_Fax == sender)
            {
                textField.Text = MailMergeItem.Instance.GetDescriptor("_2_SetupDept_Fax");
                textField.Name = "_2_SetupDept_Fax";
            }
            else if (_2_SetupMgr_Name == sender)
            {
                textField.Text = MailMergeItem.Instance.GetDescriptor("_2_SetupMgr_Name");
                textField.Name = "_2_SetupMgr_Name";
            }
            else if (_2_SetupMgr_Phone == sender)
            {
                textField.Text = MailMergeItem.Instance.GetDescriptor("_2_SetupMgr_Phone");
                textField.Name = "_2_SetupMgr_Phone";
            }
            else if (_2_SetupMgr_Fax == sender)
            {
                textField.Text = MailMergeItem.Instance.GetDescriptor("_2_SetupMgr_Fax");
                textField.Name = "_2_SetupMgr_Fax";
            }
            else if (_2_Negotiator_Name == sender)
            {
                textField.Text = MailMergeItem.Instance.GetDescriptor("_2_Negotiator_Name");
                textField.Name = "_2_Negotiator_Name";
            }
            else if (_2_Negotiator_Phone == sender)
            {
                textField.Text = MailMergeItem.Instance.GetDescriptor("_2_Negotiator_Phone");
                textField.Name = "_2_Negotiator_Phone";
            }
            else if (_2_Negotiator_Fax == sender)
            {
                textField.Text = MailMergeItem.Instance.GetDescriptor("_2_Negotiator_Fax");
                textField.Name = "_2_Negotiator_Fax";
            }
            else if (_2_NegMgr_Name == sender)
            {
                textField.Text = MailMergeItem.Instance.GetDescriptor("_2_NegMgr_Name");
                textField.Name = "_2_NegMgr_Name";
            }
            else if (_2_NegMgr_Phone == sender)
            {
                textField.Text = MailMergeItem.Instance.GetDescriptor("_2_NegMgr_Phone");
                textField.Name = "_2_NegMgr_Phone";
            }
            else if (_2_NegMgr_Fax == sender)
            {
                textField.Text = MailMergeItem.Instance.GetDescriptor("_2_NegMgr_Fax");
                textField.Name = "_2_NegMgr_Fax";
            }
            else if (_3_SetupDept_Name == sender)
            {
                textField.Text = MailMergeItem.Instance.GetDescriptor("_3_SetupDept_Name");
                textField.Name = "_3_SetupDept_Name";
            }
            else if (_3_SetupDept_Phone == sender)
            {
                textField.Text = MailMergeItem.Instance.GetDescriptor("_3_SetupDept_Phone");
                textField.Name = "_3_SetupDept_Phone";
            }
            else if (_3_SetupDept_Fax == sender)
            {
                textField.Text = MailMergeItem.Instance.GetDescriptor("_3_SetupDept_Fax");
                textField.Name = "_3_SetupDept_Fax";
            }
            else if (_3_SetupMgr_Name == sender)
            {
                textField.Text = MailMergeItem.Instance.GetDescriptor("_3_SetupMgr_Name");
                textField.Name = "_3_SetupMgr_Name";
            }
            else if (_3_SetupMgr_Phone == sender)
            {
                textField.Text = MailMergeItem.Instance.GetDescriptor("_3_SetupMgr_Phone");
                textField.Name = "_3_SetupMgr_Phone";
            }
            else if (_3_SetupMgr_Fax == sender)
            {
                textField.Text = MailMergeItem.Instance.GetDescriptor("_3_SetupMgr_Fax");
                textField.Name = "_3_SetupMgr_Fax";
            }
            else if (_3_Negotiator_Name == sender)
            {
                textField.Text = MailMergeItem.Instance.GetDescriptor("_3_Negotiator_Name");
                textField.Name = "_3_Negotiator_Name";
            }
            else if (_3_Negotiator_Phone == sender)
            {
                textField.Text = MailMergeItem.Instance.GetDescriptor("_3_Negotiator_Phone");
                textField.Name = "_3_Negotiator_Phone";
            }
            else if (_3_Negotiator_Fax == sender)
            {
                textField.Text = MailMergeItem.Instance.GetDescriptor("_3_Negotiator_Fax");
                textField.Name = "_3_Negotiator_Fax";
            }
            else if (_3_NegMgr_Name == sender)
            {
                textField.Text = MailMergeItem.Instance.GetDescriptor("_3_NegMgr_Name");
                textField.Name = "_3_NegMgr_Name";
            }
            else if (_3_NegMgr_Phone == sender)
            {
                textField.Text = MailMergeItem.Instance.GetDescriptor("_3_NegMgr_Phone");
                textField.Name = "_3_NegMgr_Phone";
            }
            else if (_3_NegMgr_Fax == sender)
            {
                textField.Text = MailMergeItem.Instance.GetDescriptor("_3_NegMgr_Fax");
                textField.Name = "_3_NegMgr_Fax";
            }                                                                    
                                                                              
                                                                                                                          	
			textField.ShowActivated = true;
			textField.DoubledInputPosition = true;
			_textControl.TextFields.Add(textField);
		}

		private void On_Merge_Click(object sender, System.EventArgs e)
		{
			if (null != _textControl.TextFields)
			{
				foreach (TXTextControl.TextField Field in _textControl.TextFields)														
                {
					string retVal = MailMergeItem.Instance.GetValue(Field.Name);
                    Field.Text = (null != retVal) ? retVal : string.Empty;
                }
			}
		}

		private void On_SwitchToTemplate_Click(object sender, EventArgs e)
		{
			if (null != _textControl.TextFields)
			{
				foreach (TXTextControl.TextField Field in _textControl.TextFields) 			
					Field.Text = MailMergeItem.Instance.GetDescriptor(Field.Name);								
			}
		}                  
	}
}
