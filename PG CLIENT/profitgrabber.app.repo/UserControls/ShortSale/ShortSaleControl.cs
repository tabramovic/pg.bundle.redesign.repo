using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Data;
using System.Windows.Forms;
using System.Drawing.Printing;
using System.Diagnostics;
using System.IO;

//Added
using DealMaker.ShortSale.Interfaces;
using DealMaker.UserControls.Repairs;
using DealMaker.PlainClasses.ShortSale.Adapters;
using DealMaker.PlainClasses.CalculateOffer;
using DealMaker.ShortSale.Utils;
using DealMaker.UserControls.Lien;
using ProfitGrabber.Common;


namespace DealMaker.UserControls.ShortSale
{
	/// <summary>
	/// Summary description for UserControl1.
	/// </summary>
	public class ShortSaleControl : BaseShortSaleUserControl, IDisplayControlObject
	{
		#region DATA
        int _liensRendered = 0;
        string _userAppFolder = string.Empty;

        TabControl _tabControl = new TabControl();        
        TabPage[] _tabPages = new TabPage[3];       
 
        DocumentGenerationControl _dgControl = null;
        DealMaker.OnlineData.UserControlPropertyProfileComps _propertyProfileControl = null;
        DealMaker.OnlineData.UserControlPropertyProfileCompsListView _compsControl = null;
        EstimateRepairsControl _repairsControl = null;

        Brush _blackBrush = Brushes.Black;
        Font _arial9 = new Font("Arial", 9);
        Font _arial10 = new Font("Arial", 10);
        Font _arial10I = new Font("Arial", 10, FontStyle.Italic);
        Font _arial12 = new Font("Arial", 12);
        Font _arial14 = new Font("Arial", 14);
        Font _arial16 = new Font("Arial", 16);
        Font _arial18 = new Font("Arial", 18);
        Font _arial20 = new Font("Arial", 20);
        Font _arial22 = new Font("Arial", 22);
        Font _arial24 = new Font("Arial", 24);
        Font _arial26 = new Font("Arial", 26);

		private System.Windows.Forms.TextBox _overAllShortSaleStrategy;
        private System.Windows.Forms.Label _overallShortSaleStrategyLabel;
        private System.Windows.Forms.TextBox _overAllMaxToPay;
		private System.Windows.Forms.Button _printContactInfo;
		private System.ComponentModel.IContainer components;
        private System.Windows.Forms.Button _addLien;

		
		private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Panel _panel;
        private ToolTip toolTip2;
        private Label _primSSLabel;
        private TextBox _secondarySSNr;
        private TextBox _primarySSNr;
        private Label _secSSLabel;
        private Label _foreclosureDateLabel;
        private TextBox _foreClosureDate;
        private ContextMenuStrip _ctxPictureMenu;
        private ToolStripMenuItem _setPicture;
        private Label _overallMaxToPayLabel;
        private TextBox _secPhoneNr;
        private TextBox _primPhoneNr;
        private Label _ssNrLabel;
        private Label _phoneNrLabel;
        private Label label1;
        Dictionary<Guid, string> _officeStaffListDict = null;
		#endregion
		
        #region CTR
        public ShortSaleControl()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

			AddEventHandlers();

            //add Dummy GUI
            //AddLien();
        }
        #endregion

        #region ADD / REMOVE EVENT HANDLERS
        void AddEventHandlers()
		{
			DataManager.Instance.DocumentCreated += new DataManager.DocumentCreatedEventHandler(On_DocumentCreated);
			DataManager.Instance.DocumentModified += new DataManager.DocumentModifiedEventHandler(On_DocumentModified);            
			_addLien.Click += new EventHandler(On_AddLien_Click);
            _foreClosureDate.Click	+= new EventHandler(On_ForeClosureDate_Click);
            _printContactInfo.Click += new EventHandler(On_PrintContactInfo_Click);
		}                                	
	
		public void RemoveEventHandlers()
		{
			DataManager.Instance.DocumentCreated -= new DataManager.DocumentCreatedEventHandler(On_DocumentCreated);
			DataManager.Instance.DocumentModified -= new DataManager.DocumentModifiedEventHandler(On_DocumentModified);            

			foreach (Control ctrl in this.Controls)
			{
				if (ctrl is TextBox)
				{
					TextBox tb = (TextBox)ctrl;
					tb.Leave -= new System.EventHandler(this.OnTextBoxLeave);
					tb.Enter -= new System.EventHandler(this.OnTextBoxEnter);
				}
			}

			foreach (TabPage tp in _tabControl.TabPages)
			{				
				if (tp.Controls[0] is DocumentGenerationControl)
				{
					DocumentGenerationControl dgc = (DocumentGenerationControl)tp.Controls[0];
					dgc.RemoveEventHandlers();
				}				
			}

        }
        #endregion

        #region DISPOSE
        /// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if( components != null )
					components.Dispose();
			}
			base.Dispose( disposing );
        }
        #endregion

        #region Component Designer generated code
        /// <summary>
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this._overAllShortSaleStrategy = new System.Windows.Forms.TextBox();
            this._overallShortSaleStrategyLabel = new System.Windows.Forms.Label();
            this._overAllMaxToPay = new System.Windows.Forms.TextBox();
            this._printContactInfo = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this._panel = new System.Windows.Forms.Panel();
            this._addLien = new System.Windows.Forms.Button();
            this.toolTip2 = new System.Windows.Forms.ToolTip(this.components);
            this._primSSLabel = new System.Windows.Forms.Label();
            this._secondarySSNr = new System.Windows.Forms.TextBox();
            this._primarySSNr = new System.Windows.Forms.TextBox();
            this._secSSLabel = new System.Windows.Forms.Label();
            this._foreclosureDateLabel = new System.Windows.Forms.Label();
            this._foreClosureDate = new System.Windows.Forms.TextBox();
            this._ctxPictureMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this._setPicture = new System.Windows.Forms.ToolStripMenuItem();
            this._overallMaxToPayLabel = new System.Windows.Forms.Label();
            this._secPhoneNr = new System.Windows.Forms.TextBox();
            this._primPhoneNr = new System.Windows.Forms.TextBox();
            this._ssNrLabel = new System.Windows.Forms.Label();
            this._phoneNrLabel = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this._panel.SuspendLayout();
            this._ctxPictureMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // _overAllShortSaleStrategy
            // 
            this._overAllShortSaleStrategy.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this._overAllShortSaleStrategy.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._overAllShortSaleStrategy.Location = new System.Drawing.Point(324, 3);
            this._overAllShortSaleStrategy.Multiline = true;
            this._overAllShortSaleStrategy.Name = "_overAllShortSaleStrategy";
            this._overAllShortSaleStrategy.Size = new System.Drawing.Size(510, 52);
            this._overAllShortSaleStrategy.TabIndex = 5;
            this._overAllShortSaleStrategy.Leave += new System.EventHandler(this.OnTextBoxLeave);
            this._overAllShortSaleStrategy.Enter += new System.EventHandler(this.OnTextBoxEnter);
            // 
            // _overallShortSaleStrategyLabel
            // 
            this._overallShortSaleStrategyLabel.AutoSize = true;
            this._overallShortSaleStrategyLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._overallShortSaleStrategyLabel.Location = new System.Drawing.Point(269, 1);
            this._overallShortSaleStrategyLabel.Name = "_overallShortSaleStrategyLabel";
            this._overallShortSaleStrategyLabel.Size = new System.Drawing.Size(49, 13);
            this._overallShortSaleStrategyLabel.TabIndex = 2;
            this._overallShortSaleStrategyLabel.Text = "Strategy:";
            // 
            // _overAllMaxToPay
            // 
            this._overAllMaxToPay.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._overAllMaxToPay.Location = new System.Drawing.Point(856, 3);
            this._overAllMaxToPay.MaxLength = 12;
            this._overAllMaxToPay.Name = "_overAllMaxToPay";
            this._overAllMaxToPay.Size = new System.Drawing.Size(17, 20);
            this._overAllMaxToPay.TabIndex = 22;
            this._overAllMaxToPay.Visible = false;
            this._overAllMaxToPay.Leave += new System.EventHandler(this.OnTextBoxLeave);
            this._overAllMaxToPay.Enter += new System.EventHandler(this.OnTextBoxEnter);
            // 
            // _printContactInfo
            // 
            this._printContactInfo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._printContactInfo.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._printContactInfo.Location = new System.Drawing.Point(879, 1);
            this._printContactInfo.Name = "_printContactInfo";
            this._printContactInfo.Size = new System.Drawing.Size(125, 27);
            this._printContactInfo.TabIndex = 6;
            this._printContactInfo.Text = "Print Contact Info";
            // 
            // _panel
            // 
            this._panel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this._panel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this._panel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._panel.Controls.Add(this.label1);
            this._panel.Location = new System.Drawing.Point(0, 56);
            this._panel.Name = "_panel";
            this._panel.Size = new System.Drawing.Size(1004, 540);
            this._panel.TabIndex = 27;
            // 
            // _addLien
            // 
            this._addLien.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._addLien.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._addLien.Location = new System.Drawing.Point(879, 28);
            this._addLien.Name = "_addLien";
            this._addLien.Size = new System.Drawing.Size(125, 27);
            this._addLien.TabIndex = 7;
            this._addLien.Text = "Add Lien";
            // 
            // _primSSLabel
            // 
            this._primSSLabel.AutoSize = true;
            this._primSSLabel.Location = new System.Drawing.Point(4, 20);
            this._primSSLabel.Name = "_primSSLabel";
            this._primSSLabel.Size = new System.Drawing.Size(44, 13);
            this._primSSLabel.TabIndex = 66;
            this._primSSLabel.Text = "Primary:";
            // 
            // _secondarySSNr
            // 
            this._secondarySSNr.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._secondarySSNr.Location = new System.Drawing.Point(71, 35);
            this._secondarySSNr.MaxLength = 40;
            this._secondarySSNr.Name = "_secondarySSNr";
            this._secondarySSNr.Size = new System.Drawing.Size(86, 20);
            this._secondarySSNr.TabIndex = 3;
            this._secondarySSNr.Leave += new System.EventHandler(this.OnTextBoxLeave);
            this._secondarySSNr.Enter += new System.EventHandler(this.OnTextBoxEnter);
            // 
            // _primarySSNr
            // 
            this._primarySSNr.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._primarySSNr.Location = new System.Drawing.Point(71, 15);
            this._primarySSNr.MaxLength = 150;
            this._primarySSNr.Name = "_primarySSNr";
            this._primarySSNr.Size = new System.Drawing.Size(86, 20);
            this._primarySSNr.TabIndex = 1;
            this._primarySSNr.Leave += new System.EventHandler(this.OnTextBoxLeave);
            this._primarySSNr.Enter += new System.EventHandler(this.OnTextBoxEnter);
            // 
            // _secSSLabel
            // 
            this._secSSLabel.AutoSize = true;
            this._secSSLabel.Location = new System.Drawing.Point(4, 36);
            this._secSSLabel.Name = "_secSSLabel";
            this._secSSLabel.Size = new System.Drawing.Size(61, 13);
            this._secSSLabel.TabIndex = 67;
            this._secSSLabel.Text = "Secondary:";
            // 
            // _foreclosureDateLabel
            // 
            this._foreclosureDateLabel.Location = new System.Drawing.Point(843, 28);
            this._foreclosureDateLabel.Name = "_foreclosureDateLabel";
            this._foreclosureDateLabel.Size = new System.Drawing.Size(15, 15);
            this._foreclosureDateLabel.TabIndex = 68;
            this._foreclosureDateLabel.Text = "Foreclosure Date:";
            this._foreclosureDateLabel.Visible = false;
            // 
            // _foreClosureDate
            // 
            this._foreClosureDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._foreClosureDate.Location = new System.Drawing.Point(856, 28);
            this._foreClosureDate.MaxLength = 40;
            this._foreClosureDate.Name = "_foreClosureDate";
            this._foreClosureDate.Size = new System.Drawing.Size(17, 20);
            this._foreClosureDate.TabIndex = 14;
            this._foreClosureDate.Visible = false;
            this._foreClosureDate.Leave += new System.EventHandler(this.OnTextBoxLeave);
            this._foreClosureDate.Enter += new System.EventHandler(this.OnTextBoxEnter);
            // 
            // _ctxPictureMenu
            // 
            this._ctxPictureMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._setPicture});
            this._ctxPictureMenu.Name = "_ctxPictureMenu";
            this._ctxPictureMenu.Size = new System.Drawing.Size(131, 26);
            // 
            // _setPicture
            // 
            this._setPicture.Name = "_setPicture";
            this._setPicture.Size = new System.Drawing.Size(130, 22);
            this._setPicture.Text = "Set Picture";
            // 
            // _overallMaxToPayLabel
            // 
            this._overallMaxToPayLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._overallMaxToPayLabel.Location = new System.Drawing.Point(840, 3);
            this._overallMaxToPayLabel.Name = "_overallMaxToPayLabel";
            this._overallMaxToPayLabel.Size = new System.Drawing.Size(10, 19);
            this._overallMaxToPayLabel.TabIndex = 4;
            this._overallMaxToPayLabel.Text = "Overall Max To Pay: ($)";
            this._overallMaxToPayLabel.Visible = false;
            // 
            // _secPhoneNr
            // 
            this._secPhoneNr.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._secPhoneNr.Location = new System.Drawing.Point(175, 35);
            this._secPhoneNr.MaxLength = 40;
            this._secPhoneNr.Name = "_secPhoneNr";
            this._secPhoneNr.Size = new System.Drawing.Size(86, 20);
            this._secPhoneNr.TabIndex = 4;
            this._secPhoneNr.Leave += new System.EventHandler(this.OnTextBoxLeave);
            this._secPhoneNr.Enter += new System.EventHandler(this.OnTextBoxEnter);
            // 
            // _primPhoneNr
            // 
            this._primPhoneNr.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._primPhoneNr.Location = new System.Drawing.Point(175, 15);
            this._primPhoneNr.MaxLength = 150;
            this._primPhoneNr.Name = "_primPhoneNr";
            this._primPhoneNr.Size = new System.Drawing.Size(86, 20);
            this._primPhoneNr.TabIndex = 2;
            this._primPhoneNr.Leave += new System.EventHandler(this.OnTextBoxLeave);
            this._primPhoneNr.Enter += new System.EventHandler(this.OnTextBoxEnter);
            // 
            // _ssNrLabel
            // 
            this._ssNrLabel.AutoSize = true;
            this._ssNrLabel.Location = new System.Drawing.Point(62, 1);
            this._ssNrLabel.Name = "_ssNrLabel";
            this._ssNrLabel.Size = new System.Drawing.Size(77, 13);
            this._ssNrLabel.TabIndex = 71;
            this._ssNrLabel.Text = "Soc. Security#";
            // 
            // _phoneNrLabel
            // 
            this._phoneNrLabel.AutoSize = true;
            this._phoneNrLabel.Location = new System.Drawing.Point(165, 1);
            this._phoneNrLabel.Name = "_phoneNrLabel";
            this._phoneNrLabel.Size = new System.Drawing.Size(45, 13);
            this._phoneNrLabel.TabIndex = 72;
            this._phoneNrLabel.Text = "Phone#";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(191, 13);
            this.label1.TabIndex = 68;
            this.label1.Text = "Click \"Add Lien\" button above to Start!";
            // 
            // ShortSaleControl
            // 
            this.AutoSize = true;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.Controls.Add(this._primPhoneNr);
            this.Controls.Add(this._secPhoneNr);
            this.Controls.Add(this._secondarySSNr);
            this.Controls.Add(this._primarySSNr);
            this.Controls.Add(this._foreclosureDateLabel);
            this.Controls.Add(this._foreClosureDate);
            this.Controls.Add(this._phoneNrLabel);
            this.Controls.Add(this._ssNrLabel);
            this.Controls.Add(this._overallMaxToPayLabel);
            this.Controls.Add(this._overAllMaxToPay);
            this.Controls.Add(this._overallShortSaleStrategyLabel);
            this.Controls.Add(this._overAllShortSaleStrategy);
            this.Controls.Add(this._printContactInfo);
            this.Controls.Add(this._secSSLabel);
            this.Controls.Add(this._primSSLabel);
            this.Controls.Add(this._panel);
            this.Controls.Add(this._addLien);
            this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.Name = "ShortSaleControl";
            this.Size = new System.Drawing.Size(1010, 596);
            this._panel.ResumeLayout(false);
            this._panel.PerformLayout();
            this._ctxPictureMenu.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		#endregion

		#region PROPERTIES
        public string UserAppFolder
        {
            get { return _userAppFolder; }
            set { _userAppFolder = value; }
        }        

		public string overAllShortSaleStrategy
		{
			get
			{
				return _overAllShortSaleStrategy.Text;
			}
			set
			{
				if (_overAllShortSaleStrategy.Text == value)
					return;
				_overAllShortSaleStrategy.Text = value;
			}
		}
		public string overAllMaxToPay
		{
			get
			{
				return _overAllMaxToPay.Text;
			}
			set
			{
				if (_overAllMaxToPay.Text == value)
					return;
				_overAllMaxToPay.Text = value;
			}
		}
        public string primarySSNr
        {
            get
            {
                return _primarySSNr.Text;
            }
            set
            {
                if (_primarySSNr.Text == value)
                    return;
                _primarySSNr.Text = value;
            }
        }
        public string secondarySSNr
        {
            get
            {
                return _secondarySSNr.Text;
            }
            set
            {
                if (_secondarySSNr.Text == value)
                    return;
                _secondarySSNr.Text = value;
            }
        }

        public string primaryPhoneNr
        {
            get
            {
                return _primPhoneNr.Text;
            }
            set
            {
                if (_primPhoneNr.Text == value)
                    return;
                _primPhoneNr.Text = value;
            }
        }
        public string secondaryPhoneNr
        {
            get
            {
                return _secPhoneNr.Text;
            }
            set
            {
                if (_secPhoneNr.Text == value)
                    return;
                _secPhoneNr.Text = value;
            }
        }

        public string foreclosureDate
        {
            get { return _foreClosureDate.Text; }
            set { _foreClosureDate.Text = value; }
        }
		#endregion				

        public void SetOfficeStaffList(Dictionary<Guid, string> officeStaffListDict)
        {
            _officeStaffListDict = officeStaffListDict;
        }
			
		private void On_AddLien_Click(object sender, EventArgs e)
		{
            AddLien();
		}

        void AddLien()
        {            
            if (DataManager.Instance.AddLienToExistingShortSaleDocument())
            {
                //reImport SSX objects hotfix - BEGIN
                if (null == CalculateOfferManager.Instance.P_CalculateOfferData)
                    CalculateOfferManager.Instance.P_CalculateOfferData = new CalculateOfferData();
                //reImport SSX objects hotfix - END


                CalculateOfferManager.Instance.P_CalculateOfferData.AddNextLien();
                CalculateOfferManager.Instance.SaveData();
                AddLienGUI();
            }
            else
            {
                MessageBox.Show("Maximum allowed number of liens is set to 3", StringStore.AppName, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        void On_approvalTabControl_TabSelected(object sender, TabControlEventArgs e)
        {
            TabControl tCtrl = (TabControl)sender;
            if (0 == tCtrl.SelectedIndex)
            {
                foreach (TabControl tc in tCtrl.SelectedTab.Controls)
                {
                    //update subtabpage names
                    int i = 0;
                    foreach (TabPage tp in tc.TabPages)
                    {
                        //update its name
                        string updatedName = string.Empty;
                        try { updatedName = (i + 1).ToString() + GetSuffix(i + 1) + " Lien" + " (" + DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[i].CompanyName + ")"; }
                        catch { updatedName = "1st Lien"; }
                        tp.Text = updatedName;
                        i++;
                    }
                }
            }
        }

        class CurrTabPage 
        {
            public int depth1 = -1;
            public int depth2 = -1;
            public int depth3 = -1;

            public CurrTabPage()
            { }

            public CurrTabPage(int d1, int d2, int d3)
            {
                depth1 = d1;
                depth2 = d2;
                depth3 = d3;
            }
        }

        CurrTabPage _ctp = new CurrTabPage(1, 1, 0);    //default
		void AddLienGUI()
		{
            //SSX User Objects Count fix - BEGIN
            if (null != _panel.Controls && _panel.Controls.Count > 0)
                _panel.Controls[0].Dispose();
            //SSX User Objects Count fix - END

			_panel.Controls.Clear();
			
            #region INIT TAB CONTROL
            bool liensFound = true;
			int numberOfLiens;

            _tabControl = new TabControl();
            _tabControl.Dock = DockStyle.Fill;
            _tabControl.SelectedIndexChanged += new EventHandler(On_MainSSXTabControl_SelectedIndexChanged);

			try { numberOfLiens = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien.Length; }
			catch 
			{
				numberOfLiens = 0;
				liensFound = false;
			}

            if (0 == numberOfLiens)
            {
                Label l = new Label();
                l.Text = "Click \"Add Lien\" button above to Start!";
                l.Location = new Point(3, 14);
                l.AutoSize = true;
                _panel.Controls.Add(l);

                return;
            }

            _tabPages = new TabPage[2];
            #endregion
            
            #region CALCULATE OFFER TAB
            _tabPages[0] = new TabPage("   CALCULATE OFFER   ");
            TabControl offerLiens = new TabControl();
            offerLiens.Dock = DockStyle.Fill;
            offerLiens.SelectedIndexChanged += new EventHandler(On_OfferLiens_SelectedIndexChanged);

            //1st Lien
            TabPage calcOfferFirstTabControl = new TabPage("1st Lien");
            DealMaker.UserControls.CalculateOffer.CalcOfferFirstLien calcOfferFirstlien = new DealMaker.UserControls.CalculateOffer.CalcOfferFirstLien();            
            calcOfferFirstlien.Dock = DockStyle.Fill;
            calcOfferFirstTabControl.Controls.Add(calcOfferFirstlien);
            offerLiens.Controls.Add(calcOfferFirstTabControl);            

            //next Liens
            for (int i = 1; i < numberOfLiens; i++)
            {
                TabPage calcOfferNextTabControl = new TabPage((i + 1).ToString() + GetSuffix(i + 1) + " Lien");
                DealMaker.UserControls.CalculateOffer.CalcOfferLienNext calcOfferNextLien = new DealMaker.UserControls.CalculateOffer.CalcOfferLienNext(i);                
                calcOfferNextLien.Dock = DockStyle.Fill;
                calcOfferNextTabControl.Controls.Add(calcOfferNextLien);
                offerLiens.Controls.Add(calcOfferNextTabControl);                
            }

            
            _tabPages[0].Controls.Add(offerLiens);            
            #endregion

            #region GET APPROVAL TAB
            _tabPages[1] = new TabPage("    GET APPROVAL     ");
            TabControl approvalTabControl = new TabControl();
            approvalTabControl.Dock = DockStyle.Fill;
            approvalTabControl.SelectedIndexChanged += new EventHandler(On_ApprovalTabControl_SelectedIndexChanged);

            //TRACK PROGRESS
            TabPage trackProgress = new TabPage("Track Progress");            
            approvalTabControl.TabPages.Add(trackProgress);
            approvalTabControl.Selected += new TabControlEventHandler(On_ApprovalTabControl_SelectedIndexChanged);

            TabControl trackProgressTabControl = new TabControl();
            trackProgressTabControl.Dock = DockStyle.Fill;
            trackProgressTabControl.SelectedIndexChanged += new EventHandler(On_TrackProgressTabControl_SelectedIndexChanged);
        
            //realLiens
            for (int i = 0; i < numberOfLiens; i++)
            {
                LienStatusControl lienStatusControl = new LienStatusControl();
                lienStatusControl.DisplayControlObject = this;
                lienStatusControl.Dock = DockStyle.Fill;
                lienStatusControl.DisplayData(i);

                TabPage lienStatusControlTabPage = new TabPage((i + 1).ToString() + GetSuffix(i + 1) + " Lien" + " (" + DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[i].CompanyName + ")");
                lienStatusControlTabPage.Controls.Add(lienStatusControl);
                trackProgressTabControl.TabPages.Add(lienStatusControlTabPage);
            }            

            trackProgress.Controls.Add(trackProgressTabControl);

            //LIENS
            for (int i = 0; i < numberOfLiens; i++)
            {
                NegotiationControl lienNegotiationControl = new NegotiationControl();
                lienNegotiationControl.DisplayControlObject = this;
                lienNegotiationControl.Dock = DockStyle.Fill;
                lienNegotiationControl.DisplayData(i);

                TabPage lienNegotiationTabPage = new TabPage((i + 1).ToString() + GetSuffix(i + 1) + " Lien" + " (" + DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[i].CompanyName + ")");
                lienNegotiationTabPage.Controls.Add(lienNegotiationControl);

                approvalTabControl.TabPages.Add(lienNegotiationTabPage);                
            }


            //GENERATE SS PACKAGE (AUTO DOCUMENT GENERATION)
            TabPage autoDocumentGeneration = new TabPage("Generate SS Package");
            _dgControl = new DocumentGenerationControl();
            _dgControl.UserAppFolder = _userAppFolder;
            _dgControl.Dock = DockStyle.Fill;
            _dgControl.DisplayData();
            autoDocumentGeneration.Controls.Add(_dgControl);

            approvalTabControl.TabPages.Add(autoDocumentGeneration);

            //default view            
            approvalTabControl.SelectedIndex = 1;
            
            _tabPages[1].Controls.Add(approvalTabControl);                                                                       
            #endregion 
 
            //depth 3
            if (1 == _ctp.depth1 && 0 == _ctp.depth2)
            {
                if (-1 != _ctp.depth3)
                    trackProgressTabControl.SelectedIndex = _ctp.depth3;
            }
          
            //depth 2
            if (0 == _ctp.depth1)
            {
                if (-1 != _ctp.depth2)
                    offerLiens.SelectedIndex = _ctp.depth2;
            }
            else
            {
                if (-1 != _ctp.depth2)
                    approvalTabControl.SelectedIndex = _ctp.depth2;
            }
            
            //depth 1            
            _tabControl.SelectedIndex = _ctp.depth1;
            
                        
            _tabControl.TabPages.AddRange(_tabPages);
			_panel.Controls.Add(_tabControl);

            base.AutoSaveDocument();
		}

        void On_ApprovalTabControl_SelectedIndexChanged(object sender, EventArgs e)
        {
            _ctp.depth2 = ((TabControl)sender).SelectedTab.TabIndex;
        }

        void On_TrackProgressTabControl_SelectedIndexChanged(object sender, EventArgs e)
        {
            _ctp.depth3 = ((TabControl)sender).SelectedTab.TabIndex;  
        }

        void On_OfferLiens_SelectedIndexChanged(object sender, EventArgs e)
        {
            _ctp.depth2 = ((TabControl)sender).SelectedTab.TabIndex;
        }

        void On_MainSSXTabControl_SelectedIndexChanged(object sender, EventArgs e)
        {
            _ctp.depth1 = ((TabControl)sender).SelectedTab.TabIndex;
        }                
		
		private void OnTextBoxEnter(object sender, System.EventArgs e)
		{
			//((TextBox)sender).BackColor = Color.LightSalmon;			
		}
		
		private void OnTextBoxLeave(object sender, System.EventArgs e)
		{
			((TextBox)sender).BackColor = Color.White;
			
			DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.OverallShortSaleStrategy = overAllShortSaleStrategy;			
			DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.OverallMaxToPay = overAllMaxToPay;
            DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.PrimarySS = primarySSNr;
            DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.SecondarySS = secondarySSNr;
            DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.PrimaryPhone = primaryPhoneNr;
            DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.SecondaryPhone = secondaryPhoneNr;
            DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.ForeclosureDate = foreclosureDate;

            base.AutoSaveDocument();
		}

		public void DisplayLoadedData()
		{                        
            overAllShortSaleStrategy = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.OverallShortSaleStrategy;			
			overAllMaxToPay = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.OverallMaxToPay;            
            primarySSNr = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.PrimarySS;
            secondarySSNr = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.SecondarySS;
            primaryPhoneNr = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.PrimaryPhone;
            secondaryPhoneNr =DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.SecondaryPhone;
            foreclosureDate = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.ForeclosureDate;

			AddLienGUI();						
		}

		private void On_DocumentCreated(DateTime time)
		{
            //firstEntered = time.ToShortDateString() + ", " + time.ToLongTimeString();
		}

		private void On_DocumentModified(DateTime time)
		{			
            //lastModified = time.ToShortDateString() + ", " + time.ToLongTimeString();
		}

		private void On_FirstName_TextChanged(object sender, EventArgs e)
		{
			UpdateFullName();
			UpdatePropertyOwnerMergeFields();
		}

		private void On_MiddleName_TextChanged(object sender, EventArgs e)
		{
			UpdateFullName();
			UpdatePropertyOwnerMergeFields();
		}

		private void On_LastName_TextChanged(object sender, EventArgs e)
		{
			UpdateFullName();
			UpdatePropertyOwnerMergeFields();
		}

        void On_SiteAddress_TextChanged(object sender, EventArgs e)
        {
            UpdatePropertyOwnerMergeFields();
        }

		void UpdateFullName()
		{
            
		}

		void UpdatePropertyOwnerMergeFields()
		{
			if (null != _dgControl)
			{                				
				_dgControl.PreSaveProcedure();
				_dgControl.AutoSaveDocument();
			}
		}

		

		public void On_PrintContactInfo_Click(object sender, EventArgs e)
		{
			PrintDialog pd = new PrintDialog();

			PrintDocument tmpprndoc = new PrintDocument();
			tmpprndoc.PrintPage += new PrintPageEventHandler(On_PrintPage);			
			tmpprndoc.EndPrint += new PrintEventHandler(On_EndPrint);

			pd.Document = tmpprndoc;
			pd.Document.DefaultPageSettings.Landscape = false;

			PrintPreviewDialogEx tmpprdiag = new PrintPreviewDialogEx();
				
			tmpprdiag.Document = tmpprndoc;
			tmpprdiag.ShowDialog();
		}

		string GetSuffix(int nr)
		{
			if (1 == nr)
				return "st";
			else if (2 == nr)
				return "nd";
			else if (3 == nr)
				return "rd";
			else
				return "th";
		}

        

		string GetSiteAddress()
		{
			string siteAddress = string.Empty;
			
			if (null != DataManager.Instance.GetCurrentShortSaleDocument().ContactInfo.SiteAddress && string.Empty != DataManager.Instance.GetCurrentShortSaleDocument().ContactInfo.SiteAddress)
				siteAddress += DataManager.Instance.GetCurrentShortSaleDocument().ContactInfo.SiteAddress + " ";
			
			if (null != DataManager.Instance.GetCurrentShortSaleDocument().ContactInfo.SiteCity && string.Empty != DataManager.Instance.GetCurrentShortSaleDocument().ContactInfo.SiteCity)
				siteAddress += DataManager.Instance.GetCurrentShortSaleDocument().ContactInfo.SiteCity + " ";

			if (null != DataManager.Instance.GetCurrentShortSaleDocument().ContactInfo.SiteState && string.Empty != DataManager.Instance.GetCurrentShortSaleDocument().ContactInfo.SiteState)
				siteAddress += DataManager.Instance.GetCurrentShortSaleDocument().ContactInfo.SiteState;

			if (null != DataManager.Instance.GetCurrentShortSaleDocument().ContactInfo.SiteZIP && string.Empty != DataManager.Instance.GetCurrentShortSaleDocument().ContactInfo.SiteZIP)
			{
				if (null != DataManager.Instance.GetCurrentShortSaleDocument().ContactInfo.SiteState && string.Empty != DataManager.Instance.GetCurrentShortSaleDocument().ContactInfo.SiteState)
					siteAddress += ", ";
				
				siteAddress += DataManager.Instance.GetCurrentShortSaleDocument().ContactInfo.SiteZIP;
			}			

			return siteAddress;
		}

		private void On_PrintPage(object sender, PrintPageEventArgs e)
		{
			Graphics gfx = e.Graphics;			

			int pageWidth = e.PageSettings.Bounds.Size.Width;
			int pageHeight = e.PageSettings.Bounds.Size.Height;
			int vertOffset = 0;			

			vertOffset += 10;

            gfx.DrawString("Real Estate Money LLC, " + StringStore.AppName, _arial10I, Brushes.Black, new Point(20, 1075));
			
			if (0 == _liensRendered)
			{
				//HEADER
				int boxHeight = 172;
				gfx.DrawLine(Pens.Black, new Point(10, 10), new Point(pageWidth - 10, 10)); //L-R
				gfx.DrawLine(Pens.Black, new Point(10, 10), new Point(10, boxHeight));//L-B
				gfx.DrawLine(Pens.Black, new Point(pageWidth - 10, 10), new Point(pageWidth - 10, boxHeight)); //R-B
				gfx.DrawLine(Pens.Black, new Point(10, boxHeight), new Point(pageWidth - 10, boxHeight)); //BL - BR

				gfx.DrawString("Contact Information for all Lien Holders", _arial14, _blackBrush, new Point(20, 22));
				gfx.DrawString(":Property Address", _arial12, _blackBrush, new Point(205, 54), new StringFormat(StringFormatFlags.DirectionRightToLeft));
				gfx.DrawString(":Foreclosure Date", _arial12, _blackBrush, new Point(750, 54), new StringFormat(StringFormatFlags.DirectionRightToLeft));
				gfx.DrawString(":Homeowner Full Names", _arial12, _blackBrush, new Point(205, 83), new StringFormat(StringFormatFlags.DirectionRightToLeft));
				gfx.DrawString(":Primary Phone", _arial12, _blackBrush, new Point(205, 112), new StringFormat(StringFormatFlags.DirectionRightToLeft));
				gfx.DrawString(":Primary Social", _arial12, _blackBrush, new Point(205, 142), new StringFormat(StringFormatFlags.DirectionRightToLeft));
				//gfx.DrawString(":Email", _arial12, _blackBrush, new Point(205, 120), new StringFormat(StringFormatFlags.DirectionRightToLeft));				
				gfx.DrawString(":Sec. Phone", _arial12, _blackBrush, new Point(550, 112), new StringFormat(StringFormatFlags.DirectionRightToLeft));
				gfx.DrawString(":Sec. Social", _arial12, _blackBrush, new Point(550, 142), new StringFormat(StringFormatFlags.DirectionRightToLeft));
			
				
				gfx.DrawString(DataManager.Instance.PropertyAddress /*GetSiteAddress()*/, _arial12, _blackBrush, new Point(205, 54));                
				gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.ForeclosureDate, _arial12, _blackBrush, new Point(750, 54));
				gfx.DrawString(DataManager.Instance.HomeOwnerFullName /*DataManager.Instance.GetCurrentShortSaleDocument().ContactInfo.FullName*/, _arial12, _blackBrush, new Point(205, 83));				
				gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.PrimarySS , _arial12, _blackBrush, new Point(205, 142));
				gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.PrimaryPhone, _arial12, _blackBrush, new Point(205, 112));
				gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.SecondaryPhone, _arial12, _blackBrush, new Point(550, 112));
				gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.SecondarySS, _arial12, _blackBrush, new Point(550, 142));
				//gfx.DrawString(DataManager.Instance.Email /*DataManager.Instance.GetCurrentShortSaleDocument().ContactInfo.EmailAddress*/, _arial12, _blackBrush, new Point(205, 120));
				
				vertOffset += boxHeight;
							
			}

            for (int i = _liensRendered; i < DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien.Length; i++)
			{
				if (vertOffset + 290 > pageHeight - 20)
				{
					e.HasMorePages = true;
					break;
				}
				vertOffset += 20;

				//draw mask
                gfx.DrawString((i + 1).ToString() + GetSuffix(i + 1) + " Lien " + DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[i].CompanyName + ", Account #: " + DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[i].AccountNr, _arial12, _blackBrush, new Point(20, vertOffset));
				vertOffset += 30;
				gfx.DrawString("Contact Name:", _arial12, _blackBrush, new Point(150, vertOffset));				
				gfx.DrawString("Phone:", _arial12, _blackBrush, new Point(305, vertOffset));
				gfx.DrawString("Fax:", _arial12, _blackBrush, new Point(440, vertOffset));
				gfx.DrawString("Email:", _arial12, _blackBrush, new Point(570, vertOffset));
				vertOffset += 25;
				
				//SETUP DEPT
				gfx.DrawString(":.Setup Dept", _arial12, _blackBrush, new Point(150, vertOffset), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[i].LienHolderContactInfo.SetupDept.ContactName, _arial12, _blackBrush, new Point(150, vertOffset));
                gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[i].LienHolderContactInfo.SetupDept.Phone, _arial12, _blackBrush, new Point(305, vertOffset));
                gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[i].LienHolderContactInfo.SetupDept.Fax, _arial12, _blackBrush, new Point(440, vertOffset));
                gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[i].LienHolderContactInfo.SetupDept.Email, _arial12, _blackBrush, new Point(570, vertOffset));
				vertOffset += 25;
				
				//SETUP MANAGER
				gfx.DrawString(":Setup Manager", _arial12, _blackBrush, new Point(150, vertOffset), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[i].LienHolderContactInfo.SetupManager.ContactName, _arial12, _blackBrush, new Point(150, vertOffset));
                gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[i].LienHolderContactInfo.SetupManager.Phone, _arial12, _blackBrush, new Point(305, vertOffset));
                gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[i].LienHolderContactInfo.SetupManager.Fax, _arial12, _blackBrush, new Point(440, vertOffset));
                gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[i].LienHolderContactInfo.SetupManager.Email, _arial12, _blackBrush, new Point(570, vertOffset));				
				vertOffset += 25;
				
				//NEGOTIATOR
				gfx.DrawString(":Negotiator", _arial12, _blackBrush, new Point(150, vertOffset), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[i].LienHolderContactInfo.Negotiator.ContactName, _arial12, _blackBrush, new Point(150, vertOffset));
                gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[i].LienHolderContactInfo.Negotiator.Phone, _arial12, _blackBrush, new Point(305, vertOffset));
                gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[i].LienHolderContactInfo.Negotiator.Fax, _arial12, _blackBrush, new Point(440, vertOffset));
                gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[i].LienHolderContactInfo.Negotiator.Email, _arial12, _blackBrush, new Point(570, vertOffset));				
				vertOffset += 25;
				
				//NEG. MANAGER
				gfx.DrawString(":Neg. Manager", _arial12, _blackBrush, new Point(150, vertOffset), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[i].LienHolderContactInfo.NegManager.ContactName, _arial12, _blackBrush, new Point(150, vertOffset));
                gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[i].LienHolderContactInfo.NegManager.Phone, _arial12, _blackBrush, new Point(305, vertOffset));
                gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[i].LienHolderContactInfo.NegManager.Fax, _arial12, _blackBrush, new Point(440, vertOffset));
                gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[i].LienHolderContactInfo.NegManager.Email, _arial12, _blackBrush, new Point(570, vertOffset));				
				vertOffset += 25;

				//BPO APPRAISAL
				gfx.DrawString(":BPO/Appraisal", _arial12, _blackBrush, new Point(150, vertOffset), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[i].LienHolderContactInfo.BPOAppraisal1.ContactName, _arial12, _blackBrush, new Point(150, vertOffset));
                gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[i].LienHolderContactInfo.BPOAppraisal1.Phone, _arial12, _blackBrush, new Point(305, vertOffset));
                gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[i].LienHolderContactInfo.BPOAppraisal1.Fax, _arial12, _blackBrush, new Point(440, vertOffset));
                gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[i].LienHolderContactInfo.BPOAppraisal1.Email, _arial12, _blackBrush, new Point(570, vertOffset));				
				vertOffset += 25;

                //BPO APPRAISAL2
                gfx.DrawString(":BPO/Appraisal 2", _arial12, _blackBrush, new Point(150, vertOffset), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[i].LienHolderContactInfo.BPOAppraisal2.ContactName, _arial12, _blackBrush, new Point(150, vertOffset));
                gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[i].LienHolderContactInfo.BPOAppraisal2.Phone, _arial12, _blackBrush, new Point(305, vertOffset));
                gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[i].LienHolderContactInfo.BPOAppraisal2.Fax, _arial12, _blackBrush, new Point(440, vertOffset));
                gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[i].LienHolderContactInfo.BPOAppraisal2.Email, _arial12, _blackBrush, new Point(570, vertOffset));
                vertOffset += 25;

                //BANKRUPCY DEPT.
                gfx.DrawString(":.Bankrupcy Dept", _arial12, _blackBrush, new Point(150, vertOffset), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[i].LienHolderContactInfo.BankruptcyDeptField.ContactName, _arial12, _blackBrush, new Point(150, vertOffset));
                gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[i].LienHolderContactInfo.BankruptcyDeptField.Phone, _arial12, _blackBrush, new Point(305, vertOffset));
                gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[i].LienHolderContactInfo.BankruptcyDeptField.Fax, _arial12, _blackBrush, new Point(440, vertOffset));
                gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[i].LienHolderContactInfo.BankruptcyDeptField.Email, _arial12, _blackBrush, new Point(570, vertOffset));
                vertOffset += 25;

				//CUSTOMER SERVICE                
				gfx.DrawString(":Customer Service", _arial12, _blackBrush, new Point(150, vertOffset), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[i].LienHolderContactInfo.CustomerService.ContactName, _arial12, _blackBrush, new Point(150, vertOffset));
                gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[i].LienHolderContactInfo.CustomerService.Phone, _arial12, _blackBrush, new Point(305, vertOffset));
                gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[i].LienHolderContactInfo.CustomerService.Fax, _arial12, _blackBrush, new Point(440, vertOffset));
                gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[i].LienHolderContactInfo.CustomerService.Email, _arial12, _blackBrush, new Point(570, vertOffset));
                vertOffset += 25;

                //BANKRUPCY DEPT.
                gfx.DrawString(":Forecl. Attorney", _arial12, _blackBrush, new Point(150, vertOffset), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[i].LienHolderContactInfo.ForeClosureAttorney.ContactName, _arial12, _blackBrush, new Point(150, vertOffset));
                gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[i].LienHolderContactInfo.ForeClosureAttorney.Phone, _arial12, _blackBrush, new Point(305, vertOffset));
                gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[i].LienHolderContactInfo.ForeClosureAttorney.Fax, _arial12, _blackBrush, new Point(440, vertOffset));
                gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[i].LienHolderContactInfo.ForeClosureAttorney.Email, _arial12, _blackBrush, new Point(570, vertOffset));                
                
                vertOffset += 30;
				gfx.DrawLine(Pens.Black, new Point(10, vertOffset), new Point(pageWidth - 10, vertOffset));

				_liensRendered++;				
			}            
		}

		private void On_EndPrint(object sender, PrintEventArgs e)
		{
			_liensRendered = 0;
		}

		private void On_Map_Click(object sender, EventArgs e)
		{			
			//AUTOSAVE
			base.AutoSaveDocument();						

			if (null == DataManager.Instance.GetCurrentShortSaleDocument().ContactInfo.SiteAddress || string.Empty == DataManager.Instance.GetCurrentShortSaleDocument().ContactInfo.SiteAddress)
			{
				MessageBox.Show("Please enter Site Address!", StringStore.AppName, MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;			
			}			

			if (null == DataManager.Instance.GetCurrentShortSaleDocument().ContactInfo.SiteState || string.Empty == DataManager.Instance.GetCurrentShortSaleDocument().ContactInfo.SiteState)
			{
				MessageBox.Show("Please enter Site State!", StringStore.AppName, MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;			
			}

			if (null == DataManager.Instance.GetCurrentShortSaleDocument().ContactInfo.SiteCity || string.Empty == DataManager.Instance.GetCurrentShortSaleDocument().ContactInfo.SiteCity)
			{
				MessageBox.Show("Please enter Site City!", StringStore.AppName, MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;			
			}

			if (null == DataManager.Instance.GetCurrentShortSaleDocument().ContactInfo.SiteZIP || string.Empty == DataManager.Instance.GetCurrentShortSaleDocument().ContactInfo.SiteZIP)
			{
				MessageBox.Show("Please enter Site ZIP!", StringStore.AppName, MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;			
			}

			//Process.Start("iexplore.exe", "http://maps.yahoo.com/maps_result?addr=" + DataManager.Instance.GetCurrentShortSaleDocument().ContactInfo.SiteAddress 
			//	+ "&csz=" + DataManager.Instance.GetCurrentShortSaleDocument().ContactInfo.SiteCity + "+" + DataManager.Instance.GetCurrentShortSaleDocument().ContactInfo.SiteState + "+" + DataManager.Instance.GetCurrentShortSaleDocument().ContactInfo.SiteZIP + "&country=us;");
			//"http://maps.yahoo.com/maps_result?addr=11287 W. Magnolia St.&csz=Avondale,+AZ+85323&country=us;");			

            //Process p = new Process();
            //p.StartInfo.FileName = DefaultBrowserSupport.getDefaultBrowser();
            //p.StartInfo.Arguments = "http://maps.yahoo.com/maps_result?addr=" + DataManager.Instance.GetCurrentShortSaleDocument().ContactInfo.SiteAddress
            //    + "&csz=" + DataManager.Instance.GetCurrentShortSaleDocument().ContactInfo.SiteCity + "+" + DataManager.Instance.GetCurrentShortSaleDocument().ContactInfo.SiteState + "+" + DataManager.Instance.GetCurrentShortSaleDocument().ContactInfo.SiteZIP + "&country=us;";
            //p.Start();

            Process.Start("http://maps.yahoo.com/maps_result?addr=" + DataManager.Instance.GetCurrentShortSaleDocument().ContactInfo.SiteAddress
                + "&csz=" + DataManager.Instance.GetCurrentShortSaleDocument().ContactInfo.SiteCity + "+" + DataManager.Instance.GetCurrentShortSaleDocument().ContactInfo.SiteState + "+" + DataManager.Instance.GetCurrentShortSaleDocument().ContactInfo.SiteZIP + "&country=us;");
            
		}

		#region IDisplayControlObject Members
		public delegate void RefreshGUIOnChangeAsync();
		public void RefreshGUIOnChange()
		{
			if (InvokeRequired)
				BeginInvoke(new RefreshGUIOnChangeAsync(RefreshGUIOnChange), null);
			else
				AddLienGUI();
		}

		#endregion

		private void On_ForeClosureDate_Click(object sender, EventArgs e)
		{
            _foreClosureDate.Text = GetSelectedDateFromForm(_foreClosureDate.Text);
            DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.ForeclosureDate = foreclosureDate;
            base.AutoSaveDocument();
		}

		string GetSelectedDateFromForm(string currentDate)
		{
            DealMaker.Forms.ShortSale.DateTimePickerForm dtpf = new DealMaker.Forms.ShortSale.DateTimePickerForm();
			dtpf.ShowDialog();

			switch (dtpf.DatePickerOption)
			{
				case eDatePickerOptions.Select:
					return dtpf.SelectedDateAsString;					

				case eDatePickerOptions.KeepCurrent:
					return currentDate;					

				case eDatePickerOptions.Clear:
					return string.Empty;
					
				default:
					return string.Empty;
			}			
		}

		private void On_MarkForGroupReport_Click(object sender, EventArgs e)
		{
            //if (_markForGroupReport.Checked)
            //{
            //    if (DataManager.Instance.SQLConnection == null) GroupFilesManager.Instance.AddFileToGroupReport(DataManager.Instance.DocumentName);
            //    MessageBox.Show("Document included in Summary Report", StringStore.AppName, MessageBoxButtons.OK, MessageBoxIcon.Information);				
            //}
            //else
            //{
            //    if (DataManager.Instance.SQLConnection == null) GroupFilesManager.Instance.RemoveFileFromGroupReport(DataManager.Instance.DocumentName);
            //    MessageBox.Show("Document removed from Summary Report", StringStore.AppName, MessageBoxButtons.OK, MessageBoxIcon.Information);
            //}
            //DataManager.Instance.GetCurrentShortSaleDocument().DocumentTracker.MarkForGroupReport = _markForGroupReport.Checked.ToString();
            //base.AutoSaveDocument();
		}

        public override void SetInsideProfitGrabber()
        {
            base.SetInsideProfitGrabber();

            //_firstName.Enabled = false;
            //_middleName.Enabled = false;
            //_lastName.Enabled = false;
            //_fullName.Enabled = false;
            //_siteAddress.Enabled = false;
            //_siteCity.Enabled = false;
            //_siteState.Enabled = false;
            //_siteZIP.Enabled = false;
            //_emailAddress.Enabled = false;

            //btClose.Visible = true;
            //btClose.BringToFront();
            //btClose.Click += new EventHandler(btClose_Click);
        }
        void btClose_Click(object sender, EventArgs e)
        {
            this.Parent.Visible = false;
            this.Parent.Hide();
        }               

        void SaveJpeg (string path, Image img, int quality)
        {
            EncoderParameter qualityParam = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, quality);

            ImageCodecInfo jpegCodec = GetEncoderInfo(@"image/jpeg");

            EncoderParameters encoderParams = new EncoderParameters(1);

            encoderParams.Param[0] = qualityParam;

            System.IO.MemoryStream mss = new System.IO.MemoryStream();

            System.IO.FileStream fs = new System.IO.FileStream(path, System.IO.FileMode.Create, System.IO.FileAccess.ReadWrite);

            img.Save(mss, jpegCodec, encoderParams);

            byte[] matriz = mss.ToArray();
            fs.Write(matriz, 0, matriz.Length);

            mss.Close();
            fs.Close();
        }

        private static ImageCodecInfo GetEncoderInfo(String mimeType)
        {
            int j;
            ImageCodecInfo[] encoders;
            encoders = ImageCodecInfo.GetImageEncoders();
            for (j = 0; j < encoders.Length; ++j)
            {
                if (encoders[j].MimeType == mimeType)
                    return encoders[j];
            }
            return null;
        }

        Image ScaleToFixedSize(Image imgPhoto, int Width, int Height)
        {
            int sourceWidth = imgPhoto.Width;
            int sourceHeight = imgPhoto.Height;
            int sourceX = 0;
            int sourceY = 0;
            int destX = 0;
            int destY = 0; 

            float nPercent = 0;
            float nPercentW = 0;
            float nPercentH = 0;

            nPercentW = ((float)Width/(float)sourceWidth);
            nPercentH = ((float)Height/(float)sourceHeight);
            if(nPercentH < nPercentW)
            {
                nPercent = nPercentH;
                destX = System.Convert.ToInt16((Width - (sourceWidth * nPercent))/2);
            }
            else
            {
                nPercent = nPercentW;
                destY = System.Convert.ToInt16((Height - (sourceHeight * nPercent))/2);
            }

            int destWidth  = (int)(sourceWidth * nPercent);
            int destHeight = (int)(sourceHeight * nPercent);

            Bitmap bmPhoto = new Bitmap(Width, Height, PixelFormat.Format24bppRgb);
            bmPhoto.SetResolution(imgPhoto.HorizontalResolution, imgPhoto.VerticalResolution);

            Graphics grPhoto = Graphics.FromImage(bmPhoto);            
            grPhoto.Clear(BackColor);
            grPhoto.InterpolationMode = InterpolationMode.HighQualityBicubic;

            grPhoto.DrawImage(imgPhoto, 
                new Rectangle(destX,destY,destWidth,destHeight),
                new Rectangle(sourceX,sourceY,sourceWidth,sourceHeight),
                GraphicsUnit.Pixel);

            grPhoto.Dispose();
            return bmPhoto;
        }

        public void DisableControl()
        {
            DummySSXControl dummySSXControl = new DummySSXControl();

            _ssNrLabel.Hide();
            _phoneNrLabel.Hide();
            _primPhoneNr.Hide();
            _secPhoneNr.Hide();
            _primSSLabel.Hide();
            _secSSLabel.Hide();
            _foreclosureDateLabel.Hide();
            _overallShortSaleStrategyLabel.Hide();
            _overallMaxToPayLabel.Hide();            
            _overAllShortSaleStrategy.Hide();            
            _primarySSNr.Hide();
            _secondarySSNr.Hide();
            _overAllMaxToPay.Hide();            
            _foreClosureDate.Hide();
            _addLien.Hide();
            _printContactInfo.Hide();
            _panel.Hide();


            dummySSXControl.Dock = DockStyle.Fill;
            dummySSXControl.BringToFront();
            this.Controls.Add(dummySSXControl);
        }

        public void EnableAutoSaveEventHandler(bool enable)
        {            
            AutoSaveEventHandlerEnabled = enable;            
        }
	}
}
