using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;
using System.IO;

//Added
using DealMaker.ShortSale.Utils;
using DealMaker.PlainClasses.ShortSale.Adapters;

namespace DealMaker.UserControls.ShortSale
{
	/// <summary>
	/// Summary description for BaseShortSaleUserControl.
	/// </summary>
	public class BaseShortSaleUserControl : System.Windows.Forms.UserControl
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public BaseShortSaleUserControl()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

			// TODO: Add any initialization after the InitializeComponent call

		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			components = new System.ComponentModel.Container();
		}
		#endregion

        private static bool insideProfitGrabber = true;
        virtual public void SetInsideProfitGrabber()
        {
            insideProfitGrabber = true;
        }

        public static bool AutoSaveEventHandlerEnabled = true;

		public void AutoSaveDocument()
		{
			string error = string.Empty;
            if (insideProfitGrabber)
            {
                if (null != SSXAutoSaveEvent && AutoSaveEventHandlerEnabled)
                    SSXAutoSaveEvent();

                string err = string.Empty;
                DataManager.Instance.SaveShortSaleDocument(DataManager.Instance.GetCurrentShortSaleDocument(), StringStore.AutoSave, out err);                
            }            
		}

        public delegate void SSXAutoSave();
        public event SSXAutoSave SSXAutoSaveEvent;
	}
}
