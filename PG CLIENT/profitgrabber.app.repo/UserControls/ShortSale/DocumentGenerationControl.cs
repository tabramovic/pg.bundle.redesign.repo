using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;
using System.Drawing.Printing;
using System.IO;
using System.Diagnostics;
using System.Xml.Serialization;


//Added
using HUD_MODULE;
using DealMaker.ShortSale.Utils;
using DealMaker.UserControls.Repairs;
using DealMaker.PlainClasses.Repairs;
using DealMaker.Forms.ShortSale;
using DealMaker.PlainClasses.ShortSale.Adapters;
using DealMaker.PlainClasses.CalculateOffer;
using DealMaker.PlainClasses.OnLineData.COMPS;
using DealMaker.ShortSale;
using DealMaker.ShortSale.Utils;
using ProfitGrabber.Common;


namespace DealMaker.UserControls.ShortSale
{
	/// <summary>
	/// Summary description for DocumentGenerationControl.
	/// </summary>
	public class DocumentGenerationControl : BaseShortSaleUserControl
	{
		#region DATA
		private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label2;
		private System.Windows.Forms.GroupBox groupBox5;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.GroupBox groupBox6;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.Label label13;
		private System.Windows.Forms.Label label14;
		private System.Windows.Forms.Label label15;
		private System.Windows.Forms.GroupBox groupBox7;
		private System.Windows.Forms.Label label16;
		private System.Windows.Forms.Label label17;
		private System.Windows.Forms.Label label18;
		private System.Windows.Forms.Label label19;
		private System.Windows.Forms.Label label20;
		private System.Windows.Forms.Label label21;
		private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox _1stLien_FaxLabelLine1;
        private System.Windows.Forms.Button _HUD1Generator;
		private System.Windows.Forms.TextBox _po_MailingAddress;
		private System.Windows.Forms.TextBox _po_PropertyAddress;
		private System.Windows.Forms.TextBox _po_FullName;
		private System.Windows.Forms.TextBox _po_LastName;
		private System.Windows.Forms.TextBox _po_FirstName;
		private System.Windows.Forms.Button _saveBuyerInfoAsDefault;
		private System.Windows.Forms.TextBox _bmf_Email;
		private System.Windows.Forms.TextBox _bmf_Fax;
		private System.Windows.Forms.TextBox _bmf_Phone;
		private System.Windows.Forms.TextBox _bmf_Address;
		private System.Windows.Forms.TextBox _bmf_FullName;
		private System.Windows.Forms.TextBox _bmf_LastName;
		private System.Windows.Forms.TextBox _bmf_FirstName;
		private System.Windows.Forms.TextBox _abmf_Email;
		private System.Windows.Forms.TextBox _abmf_Fax;
		private System.Windows.Forms.TextBox _abmf_Phone;
		private System.Windows.Forms.TextBox _abmf_Address;
		private System.Windows.Forms.TextBox _abmf_FullName;
		private System.Windows.Forms.TextBox _abmf_LastName;
		private System.Windows.Forms.TextBox _abmf_FirstName;
		private System.Windows.Forms.Button _saveAgentInfoAsDefault;
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
		private System.Windows.Forms.TextBox _po_MiddleName;
		private System.Windows.Forms.Label label23;
		private System.Windows.Forms.CheckBox _generic_PacketCoverLetterThey;
		private System.Windows.Forms.CheckBox _generic_PacketCoverLetterHim;
        private System.Windows.Forms.CheckBox _generic_PacketCoverLetterHer;
		private System.Windows.Forms.GroupBox groupBox9;
		private System.Windows.Forms.RadioButton _firstLienDocumentSet;
		private System.Windows.Forms.RadioButton _secondLienDocumentSet;
		private System.Windows.Forms.RadioButton _thirdLienDocumentSet;
		private System.Windows.Forms.Button _GetShortSaleDocsReady;
		private System.Windows.Forms.CheckBox _FAXCoverAuthorization;
		private System.Windows.Forms.CheckBox _FaxCoverGeneral;
		private System.Windows.Forms.CheckBox _hardshipLetter;
		private System.Windows.Forms.CheckBox _AuthorizationToRelease;
		private System.Windows.Forms.CheckBox _standardPacketCheckList;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Button _printOwnerInfoLabels;
		private System.Windows.Forms.Button _lien_PrintPage;
		private System.Windows.Forms.CheckBox _lienSpecificUserDocument3;
		private System.Windows.Forms.CheckBox _lienSpecificUserDocument2;
		private System.Windows.Forms.CheckBox _lienSpecificUserDocument1;

		bool _realDataDisplayed = false;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.PictureBox _pictureBoxDocumentControl;
        string _userAppFolder = string.Empty;        
		int _currentPrintingLien = 0;
		

		const string firstLienFolder = @"\1stLien\";
		const string secondLienFolder = @"\2ndLien\";
		const string thirdLienFolder = @"\3rdLien\";
		const string picturesFolder = @"\Pictures\";		
		const string resaleInfoFolder = @"\ResaleInfo\";
        private CheckBox _lienSpecificUserDocument5;
        private CheckBox _lienSpecificUserDocument4;
		const string homeownerInfoFolder = @"\HomeownerInfo\";
        private GroupBox groupBox3;
        private Button _comps;
        private Button _repairsEstimate;
        EstimateRepairsReportGenerator _gen = null;
        private GroupBox groupBox4;
        private Button _customHUD;
        const string UnselectedLienMsg = "You must select a Lien you want to print Documents for.";
        #endregion

        #region CTR & DISPOSE
        public DocumentGenerationControl()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();
						
			_GetShortSaleDocsReady.Click += new EventHandler(On_GetShortSaleDocsReady_Click);

			_saveBuyerInfoAsDefault.Click += new EventHandler(On_SaveBuyerInfoAsDefault_Click);
			_saveAgentInfoAsDefault.Click += new EventHandler(On_AaveAgentInfoAsDefault_Click);

			_HUD1Generator.Click += new EventHandler(On_HUD1Generator_Click);
			_lien_PrintPage.Click += new EventHandler(On_Lien_PrintPage_Click);	
			_printOwnerInfoLabels.Click += new EventHandler(On_printOwnerInfoLabels_Click);
			
			_pictureBoxDocumentControl.MouseHover += new EventHandler(On_PictureBoxDocumentControl_MouseHover);
            _repairsEstimate.Click += new EventHandler(On_RepairsEstimate_Click);
            _comps.Click += new EventHandler(On_Comps_Click);

            _customHUD.Click += new EventHandler(On_CustomHUD_Click);
		}                

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
        }
        #endregion

        #region ON LOAD
        protected override void OnLoad(EventArgs e)
		{
			base.OnLoad (e);

			if (!_realDataDisplayed)
				DisplayDefaultInfoData();
        }
        #endregion

        #region REMOVE EVENT HANDLERS
        public void RemoveEventHandlers()
		{
			foreach (Control ctrl in this.Controls)
			{
				if (ctrl is TextBox)
				{
					TextBox tb = (TextBox)ctrl;
					tb.Leave -= new System.EventHandler(this.OnTextBoxLeave);
					tb.Enter -= new System.EventHandler(this.OnTextBoxEnter);		
				}
			}
        }
        #endregion

        #region Component Designer generated code
        /// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DocumentGenerationControl));
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this._FAXCoverAuthorization = new System.Windows.Forms.CheckBox();
            this._AuthorizationToRelease = new System.Windows.Forms.CheckBox();
            this._GetShortSaleDocsReady = new System.Windows.Forms.Button();
            this._pictureBoxDocumentControl = new System.Windows.Forms.PictureBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this._1stLien_FaxLabelLine1 = new System.Windows.Forms.TextBox();
            this._printOwnerInfoLabels = new System.Windows.Forms.Button();
            this._lien_PrintPage = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this._comps = new System.Windows.Forms.Button();
            this._repairsEstimate = new System.Windows.Forms.Button();
            this._HUD1Generator = new System.Windows.Forms.Button();
            this._lienSpecificUserDocument5 = new System.Windows.Forms.CheckBox();
            this._generic_PacketCoverLetterThey = new System.Windows.Forms.CheckBox();
            this._lienSpecificUserDocument4 = new System.Windows.Forms.CheckBox();
            this._generic_PacketCoverLetterHim = new System.Windows.Forms.CheckBox();
            this._generic_PacketCoverLetterHer = new System.Windows.Forms.CheckBox();
            this._lienSpecificUserDocument3 = new System.Windows.Forms.CheckBox();
            this._lienSpecificUserDocument2 = new System.Windows.Forms.CheckBox();
            this._lienSpecificUserDocument1 = new System.Windows.Forms.CheckBox();
            this._FaxCoverGeneral = new System.Windows.Forms.CheckBox();
            this._hardshipLetter = new System.Windows.Forms.CheckBox();
            this._standardPacketCheckList = new System.Windows.Forms.CheckBox();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this._thirdLienDocumentSet = new System.Windows.Forms.RadioButton();
            this._secondLienDocumentSet = new System.Windows.Forms.RadioButton();
            this._firstLienDocumentSet = new System.Windows.Forms.RadioButton();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this._po_MailingAddress = new System.Windows.Forms.TextBox();
            this._po_PropertyAddress = new System.Windows.Forms.TextBox();
            this._po_FullName = new System.Windows.Forms.TextBox();
            this._po_MiddleName = new System.Windows.Forms.TextBox();
            this._po_LastName = new System.Windows.Forms.TextBox();
            this._po_FirstName = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this._bmf_Email = new System.Windows.Forms.TextBox();
            this._bmf_Fax = new System.Windows.Forms.TextBox();
            this._bmf_Phone = new System.Windows.Forms.TextBox();
            this._bmf_Address = new System.Windows.Forms.TextBox();
            this._bmf_FullName = new System.Windows.Forms.TextBox();
            this._bmf_LastName = new System.Windows.Forms.TextBox();
            this._bmf_FirstName = new System.Windows.Forms.TextBox();
            this._saveBuyerInfoAsDefault = new System.Windows.Forms.Button();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this._abmf_Email = new System.Windows.Forms.TextBox();
            this._abmf_Fax = new System.Windows.Forms.TextBox();
            this._abmf_Phone = new System.Windows.Forms.TextBox();
            this._abmf_Address = new System.Windows.Forms.TextBox();
            this._abmf_FullName = new System.Windows.Forms.TextBox();
            this._abmf_LastName = new System.Windows.Forms.TextBox();
            this._abmf_FirstName = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this._saveAgentInfoAsDefault = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this._customHUD = new System.Windows.Forms.Button();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._pictureBoxDocumentControl)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.groupBox4);
            this.groupBox2.Controls.Add(this.groupBox3);
            this.groupBox2.Controls.Add(this._GetShortSaleDocsReady);
            this.groupBox2.Controls.Add(this._pictureBoxDocumentControl);
            this.groupBox2.Controls.Add(this.groupBox1);
            this.groupBox2.Controls.Add(this.groupBox8);
            this.groupBox2.Controls.Add(this._lienSpecificUserDocument5);
            this.groupBox2.Controls.Add(this._generic_PacketCoverLetterThey);
            this.groupBox2.Controls.Add(this._lienSpecificUserDocument4);
            this.groupBox2.Controls.Add(this._generic_PacketCoverLetterHim);
            this.groupBox2.Controls.Add(this._generic_PacketCoverLetterHer);
            this.groupBox2.Controls.Add(this._lienSpecificUserDocument3);
            this.groupBox2.Controls.Add(this._lienSpecificUserDocument2);
            this.groupBox2.Controls.Add(this._lienSpecificUserDocument1);
            this.groupBox2.Controls.Add(this._FaxCoverGeneral);
            this.groupBox2.Controls.Add(this._hardshipLetter);
            this.groupBox2.Controls.Add(this._standardPacketCheckList);
            this.groupBox2.Controls.Add(this.groupBox9);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(366, 0);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(526, 456);
            this.groupBox2.TabIndex = 17;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Generate Lien Specific Documents";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this._FAXCoverAuthorization);
            this.groupBox3.Controls.Add(this._AuthorizationToRelease);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.Location = new System.Drawing.Point(8, 77);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(259, 58);
            this.groupBox3.TabIndex = 70;
            this.groupBox3.TabStop = false;
            // 
            // _FAXCoverAuthorization
            // 
            this._FAXCoverAuthorization.AutoSize = true;
            this._FAXCoverAuthorization.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._FAXCoverAuthorization.Location = new System.Drawing.Point(8, 12);
            this._FAXCoverAuthorization.Name = "_FAXCoverAuthorization";
            this._FAXCoverAuthorization.Size = new System.Drawing.Size(147, 18);
            this._FAXCoverAuthorization.TabIndex = 13;
            this._FAXCoverAuthorization.Text = "FAX Cover Authorization";
            // 
            // _AuthorizationToRelease
            // 
            this._AuthorizationToRelease.AutoSize = true;
            this._AuthorizationToRelease.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._AuthorizationToRelease.Location = new System.Drawing.Point(8, 36);
            this._AuthorizationToRelease.Name = "_AuthorizationToRelease";
            this._AuthorizationToRelease.Size = new System.Drawing.Size(147, 18);
            this._AuthorizationToRelease.TabIndex = 14;
            this._AuthorizationToRelease.Text = "Authorization to Release";
            // 
            // _GetShortSaleDocsReady
            // 
            this._GetShortSaleDocsReady.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._GetShortSaleDocsReady.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._GetShortSaleDocsReady.Location = new System.Drawing.Point(16, 407);
            this._GetShortSaleDocsReady.Name = "_GetShortSaleDocsReady";
            this._GetShortSaleDocsReady.Size = new System.Drawing.Size(168, 40);
            this._GetShortSaleDocsReady.TabIndex = 28;
            this._GetShortSaleDocsReady.Text = "Print Selected Docs";
            // 
            // _pictureBoxDocumentControl
            // 
            this._pictureBoxDocumentControl.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._pictureBoxDocumentControl.Image = ((System.Drawing.Image)(resources.GetObject("_pictureBoxDocumentControl.Image")));
            this._pictureBoxDocumentControl.Location = new System.Drawing.Point(500, 458);
            this._pictureBoxDocumentControl.Name = "_pictureBoxDocumentControl";
            this._pictureBoxDocumentControl.Size = new System.Drawing.Size(20, 20);
            this._pictureBoxDocumentControl.TabIndex = 51;
            this._pictureBoxDocumentControl.TabStop = false;
            this._pictureBoxDocumentControl.Visible = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this._1stLien_FaxLabelLine1);
            this.groupBox1.Controls.Add(this._printOwnerInfoLabels);
            this.groupBox1.Controls.Add(this._lien_PrintPage);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(320, 194);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(200, 176);
            this.groupBox1.TabIndex = 29;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Improve FAX Delivery";
            // 
            // _1stLien_FaxLabelLine1
            // 
            this._1stLien_FaxLabelLine1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._1stLien_FaxLabelLine1.Location = new System.Drawing.Point(20, 44);
            this._1stLien_FaxLabelLine1.MaxLength = 10;
            this._1stLien_FaxLabelLine1.Name = "_1stLien_FaxLabelLine1";
            this._1stLien_FaxLabelLine1.Size = new System.Drawing.Size(168, 20);
            this._1stLien_FaxLabelLine1.TabIndex = 26;
            this._1stLien_FaxLabelLine1.Leave += new System.EventHandler(this.OnTextBoxLeave);
            this._1stLien_FaxLabelLine1.Enter += new System.EventHandler(this.OnTextBoxEnter);
            // 
            // _printOwnerInfoLabels
            // 
            this._printOwnerInfoLabels.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._printOwnerInfoLabels.Location = new System.Drawing.Point(20, 124);
            this._printOwnerInfoLabels.Name = "_printOwnerInfoLabels";
            this._printOwnerInfoLabels.Size = new System.Drawing.Size(168, 40);
            this._printOwnerInfoLabels.TabIndex = 28;
            this._printOwnerInfoLabels.Text = "Print Owner Info Labels";
            // 
            // _lien_PrintPage
            // 
            this._lien_PrintPage.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._lien_PrintPage.Location = new System.Drawing.Point(20, 78);
            this._lien_PrintPage.Name = "_lien_PrintPage";
            this._lien_PrintPage.Size = new System.Drawing.Size(168, 40);
            this._lien_PrintPage.TabIndex = 27;
            this._lien_PrintPage.Text = "Print Page / Loan# Labels";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(8, 24);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(88, 13);
            this.label2.TabIndex = 14;
            this.label2.Text = "FAX Label Line 1";
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this._comps);
            this.groupBox8.Controls.Add(this._repairsEstimate);
            this.groupBox8.Controls.Add(this._HUD1Generator);
            this.groupBox8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox8.Location = new System.Drawing.Point(320, 23);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(200, 165);
            this.groupBox8.TabIndex = 24;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Print Additional Documents";
            // 
            // _comps
            // 
            this._comps.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._comps.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._comps.Location = new System.Drawing.Point(20, 67);
            this._comps.Name = "_comps";
            this._comps.Size = new System.Drawing.Size(168, 40);
            this._comps.TabIndex = 71;
            this._comps.Text = "CMA Analysis (Conservative COMPS)";
            // 
            // _repairsEstimate
            // 
            this._repairsEstimate.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._repairsEstimate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._repairsEstimate.Location = new System.Drawing.Point(20, 21);
            this._repairsEstimate.Name = "_repairsEstimate";
            this._repairsEstimate.Size = new System.Drawing.Size(168, 40);
            this._repairsEstimate.TabIndex = 71;
            this._repairsEstimate.Text = "Repairs Estimate";
            // 
            // _HUD1Generator
            // 
            this._HUD1Generator.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._HUD1Generator.Location = new System.Drawing.Point(20, 113);
            this._HUD1Generator.Name = "_HUD1Generator";
            this._HUD1Generator.Size = new System.Drawing.Size(168, 40);
            this._HUD1Generator.TabIndex = 4;
            this._HUD1Generator.Text = "HUD 1";
            // 
            // _lienSpecificUserDocument5
            // 
            this._lienSpecificUserDocument5.AutoSize = true;
            this._lienSpecificUserDocument5.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._lienSpecificUserDocument5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._lienSpecificUserDocument5.Location = new System.Drawing.Point(16, 381);
            this._lienSpecificUserDocument5.Name = "_lienSpecificUserDocument5";
            this._lienSpecificUserDocument5.Size = new System.Drawing.Size(115, 18);
            this._lienSpecificUserDocument5.TabIndex = 67;
            this._lienSpecificUserDocument5.Text = "User Document 5";
            // 
            // _generic_PacketCoverLetterThey
            // 
            this._generic_PacketCoverLetterThey.AutoSize = true;
            this._generic_PacketCoverLetterThey.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._generic_PacketCoverLetterThey.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._generic_PacketCoverLetterThey.Location = new System.Drawing.Point(16, 235);
            this._generic_PacketCoverLetterThey.Name = "_generic_PacketCoverLetterThey";
            this._generic_PacketCoverLetterThey.Size = new System.Drawing.Size(177, 18);
            this._generic_PacketCoverLetterThey.TabIndex = 18;
            this._generic_PacketCoverLetterThey.Text = "SS Packet Cover Letter (They)";
            // 
            // _lienSpecificUserDocument4
            // 
            this._lienSpecificUserDocument4.AutoSize = true;
            this._lienSpecificUserDocument4.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._lienSpecificUserDocument4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._lienSpecificUserDocument4.Location = new System.Drawing.Point(16, 357);
            this._lienSpecificUserDocument4.Name = "_lienSpecificUserDocument4";
            this._lienSpecificUserDocument4.Size = new System.Drawing.Size(115, 18);
            this._lienSpecificUserDocument4.TabIndex = 66;
            this._lienSpecificUserDocument4.Text = "User Document 4";
            // 
            // _generic_PacketCoverLetterHim
            // 
            this._generic_PacketCoverLetterHim.AutoSize = true;
            this._generic_PacketCoverLetterHim.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._generic_PacketCoverLetterHim.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._generic_PacketCoverLetterHim.Location = new System.Drawing.Point(16, 187);
            this._generic_PacketCoverLetterHim.Name = "_generic_PacketCoverLetterHim";
            this._generic_PacketCoverLetterHim.Size = new System.Drawing.Size(171, 18);
            this._generic_PacketCoverLetterHim.TabIndex = 16;
            this._generic_PacketCoverLetterHim.Text = "SS Packet Cover Letter (Him)";
            // 
            // _generic_PacketCoverLetterHer
            // 
            this._generic_PacketCoverLetterHer.AutoSize = true;
            this._generic_PacketCoverLetterHer.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._generic_PacketCoverLetterHer.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._generic_PacketCoverLetterHer.Location = new System.Drawing.Point(16, 211);
            this._generic_PacketCoverLetterHer.Name = "_generic_PacketCoverLetterHer";
            this._generic_PacketCoverLetterHer.Size = new System.Drawing.Size(170, 18);
            this._generic_PacketCoverLetterHer.TabIndex = 17;
            this._generic_PacketCoverLetterHer.Text = "SS Packet Cover Letter (Her)";
            // 
            // _lienSpecificUserDocument3
            // 
            this._lienSpecificUserDocument3.AutoSize = true;
            this._lienSpecificUserDocument3.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._lienSpecificUserDocument3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._lienSpecificUserDocument3.Location = new System.Drawing.Point(16, 333);
            this._lienSpecificUserDocument3.Name = "_lienSpecificUserDocument3";
            this._lienSpecificUserDocument3.Size = new System.Drawing.Size(115, 18);
            this._lienSpecificUserDocument3.TabIndex = 65;
            this._lienSpecificUserDocument3.Text = "User Document 3";
            // 
            // _lienSpecificUserDocument2
            // 
            this._lienSpecificUserDocument2.AutoSize = true;
            this._lienSpecificUserDocument2.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._lienSpecificUserDocument2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._lienSpecificUserDocument2.Location = new System.Drawing.Point(16, 309);
            this._lienSpecificUserDocument2.Name = "_lienSpecificUserDocument2";
            this._lienSpecificUserDocument2.Size = new System.Drawing.Size(115, 18);
            this._lienSpecificUserDocument2.TabIndex = 64;
            this._lienSpecificUserDocument2.Text = "User Document 2";
            // 
            // _lienSpecificUserDocument1
            // 
            this._lienSpecificUserDocument1.AutoSize = true;
            this._lienSpecificUserDocument1.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._lienSpecificUserDocument1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._lienSpecificUserDocument1.Location = new System.Drawing.Point(16, 285);
            this._lienSpecificUserDocument1.Name = "_lienSpecificUserDocument1";
            this._lienSpecificUserDocument1.Size = new System.Drawing.Size(115, 18);
            this._lienSpecificUserDocument1.TabIndex = 63;
            this._lienSpecificUserDocument1.Text = "User Document 1";
            // 
            // _FaxCoverGeneral
            // 
            this._FaxCoverGeneral.AutoSize = true;
            this._FaxCoverGeneral.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._FaxCoverGeneral.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._FaxCoverGeneral.Location = new System.Drawing.Point(16, 163);
            this._FaxCoverGeneral.Name = "_FaxCoverGeneral";
            this._FaxCoverGeneral.Size = new System.Drawing.Size(123, 18);
            this._FaxCoverGeneral.TabIndex = 15;
            this._FaxCoverGeneral.Text = "FAX Cover General";
            // 
            // _hardshipLetter
            // 
            this._hardshipLetter.AutoSize = true;
            this._hardshipLetter.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._hardshipLetter.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._hardshipLetter.Location = new System.Drawing.Point(16, 261);
            this._hardshipLetter.Name = "_hardshipLetter";
            this._hardshipLetter.Size = new System.Drawing.Size(104, 18);
            this._hardshipLetter.TabIndex = 24;
            this._hardshipLetter.Text = "Hardship Letter";
            // 
            // _standardPacketCheckList
            // 
            this._standardPacketCheckList.AutoSize = true;
            this._standardPacketCheckList.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._standardPacketCheckList.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._standardPacketCheckList.Location = new System.Drawing.Point(16, 141);
            this._standardPacketCheckList.Name = "_standardPacketCheckList";
            this._standardPacketCheckList.Size = new System.Drawing.Size(158, 18);
            this._standardPacketCheckList.TabIndex = 12;
            this._standardPacketCheckList.Text = "Standard Packet Checklist";
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this._thirdLienDocumentSet);
            this.groupBox9.Controls.Add(this._secondLienDocumentSet);
            this.groupBox9.Controls.Add(this._firstLienDocumentSet);
            this.groupBox9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox9.Location = new System.Drawing.Point(8, 23);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(259, 48);
            this.groupBox9.TabIndex = 52;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Select Lien";
            // 
            // _thirdLienDocumentSet
            // 
            this._thirdLienDocumentSet.Location = new System.Drawing.Point(174, 16);
            this._thirdLienDocumentSet.Name = "_thirdLienDocumentSet";
            this._thirdLienDocumentSet.Size = new System.Drawing.Size(81, 24);
            this._thirdLienDocumentSet.TabIndex = 2;
            this._thirdLienDocumentSet.Text = "3rd Lien";
            // 
            // _secondLienDocumentSet
            // 
            this._secondLienDocumentSet.Location = new System.Drawing.Point(88, 16);
            this._secondLienDocumentSet.Name = "_secondLienDocumentSet";
            this._secondLienDocumentSet.Size = new System.Drawing.Size(86, 24);
            this._secondLienDocumentSet.TabIndex = 1;
            this._secondLienDocumentSet.Text = "2nd Lien";
            // 
            // _firstLienDocumentSet
            // 
            this._firstLienDocumentSet.Location = new System.Drawing.Point(8, 16);
            this._firstLienDocumentSet.Name = "_firstLienDocumentSet";
            this._firstLienDocumentSet.Size = new System.Drawing.Size(80, 24);
            this._firstLienDocumentSet.TabIndex = 0;
            this._firstLienDocumentSet.Text = "1st Lien";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this._po_MailingAddress);
            this.groupBox5.Controls.Add(this._po_PropertyAddress);
            this.groupBox5.Controls.Add(this._po_FullName);
            this.groupBox5.Controls.Add(this._po_MiddleName);
            this.groupBox5.Controls.Add(this._po_LastName);
            this.groupBox5.Controls.Add(this._po_FirstName);
            this.groupBox5.Controls.Add(this.label23);
            this.groupBox5.Controls.Add(this.label8);
            this.groupBox5.Controls.Add(this.label7);
            this.groupBox5.Controls.Add(this.label6);
            this.groupBox5.Controls.Add(this.label5);
            this.groupBox5.Controls.Add(this.label4);
            this.groupBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox5.Location = new System.Drawing.Point(8, 0);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(352, 121);
            this.groupBox5.TabIndex = 0;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Property Owner Merge Fields";
            // 
            // _po_MailingAddress
            // 
            this._po_MailingAddress.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._po_MailingAddress.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._po_MailingAddress.Location = new System.Drawing.Point(120, 98);
            this._po_MailingAddress.Name = "_po_MailingAddress";
            this._po_MailingAddress.Size = new System.Drawing.Size(224, 20);
            this._po_MailingAddress.TabIndex = 5;
            this._po_MailingAddress.Leave += new System.EventHandler(this.OnTextBoxLeave);
            this._po_MailingAddress.Enter += new System.EventHandler(this.OnTextBoxEnter);
            // 
            // _po_PropertyAddress
            // 
            this._po_PropertyAddress.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._po_PropertyAddress.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._po_PropertyAddress.Location = new System.Drawing.Point(120, 77);
            this._po_PropertyAddress.Name = "_po_PropertyAddress";
            this._po_PropertyAddress.Size = new System.Drawing.Size(224, 20);
            this._po_PropertyAddress.TabIndex = 4;
            this._po_PropertyAddress.Leave += new System.EventHandler(this.OnTextBoxLeave);
            this._po_PropertyAddress.Enter += new System.EventHandler(this.OnTextBoxEnter);
            // 
            // _po_FullName
            // 
            this._po_FullName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._po_FullName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._po_FullName.Location = new System.Drawing.Point(120, 56);
            this._po_FullName.Name = "_po_FullName";
            this._po_FullName.Size = new System.Drawing.Size(224, 20);
            this._po_FullName.TabIndex = 3;
            this._po_FullName.Leave += new System.EventHandler(this.OnTextBoxLeave);
            this._po_FullName.Enter += new System.EventHandler(this.OnTextBoxEnter);
            // 
            // _po_MiddleName
            // 
            this._po_MiddleName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._po_MiddleName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._po_MiddleName.Location = new System.Drawing.Point(120, 35);
            this._po_MiddleName.Name = "_po_MiddleName";
            this._po_MiddleName.Size = new System.Drawing.Size(111, 20);
            this._po_MiddleName.TabIndex = 1;
            this._po_MiddleName.Leave += new System.EventHandler(this.OnTextBoxLeave);
            this._po_MiddleName.Enter += new System.EventHandler(this.OnTextBoxEnter);
            // 
            // _po_LastName
            // 
            this._po_LastName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._po_LastName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._po_LastName.Location = new System.Drawing.Point(232, 35);
            this._po_LastName.Name = "_po_LastName";
            this._po_LastName.Size = new System.Drawing.Size(112, 20);
            this._po_LastName.TabIndex = 2;
            this._po_LastName.Leave += new System.EventHandler(this.OnTextBoxLeave);
            this._po_LastName.Enter += new System.EventHandler(this.OnTextBoxEnter);
            // 
            // _po_FirstName
            // 
            this._po_FirstName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._po_FirstName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._po_FirstName.Location = new System.Drawing.Point(8, 35);
            this._po_FirstName.Name = "_po_FirstName";
            this._po_FirstName.Size = new System.Drawing.Size(111, 20);
            this._po_FirstName.TabIndex = 0;
            this._po_FirstName.Leave += new System.EventHandler(this.OnTextBoxLeave);
            this._po_FirstName.Enter += new System.EventHandler(this.OnTextBoxEnter);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(120, 19);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(72, 13);
            this.label23.TabIndex = 11;
            this.label23.Text = "Middle Name:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(9, 98);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(84, 13);
            this.label8.TabIndex = 9;
            this.label8.Text = "Mailing Address:";
            this.label8.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(9, 77);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(90, 13);
            this.label7.TabIndex = 8;
            this.label7.Text = "Property Address:";
            this.label7.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(9, 56);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(67, 13);
            this.label6.TabIndex = 7;
            this.label6.Text = "Full Name/s:";
            this.label6.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(240, 19);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(61, 13);
            this.label5.TabIndex = 6;
            this.label5.Text = "Last Name:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(8, 19);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(60, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "First Name:";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this._bmf_Email);
            this.groupBox6.Controls.Add(this._bmf_Fax);
            this.groupBox6.Controls.Add(this._bmf_Phone);
            this.groupBox6.Controls.Add(this._bmf_Address);
            this.groupBox6.Controls.Add(this._bmf_FullName);
            this.groupBox6.Controls.Add(this._bmf_LastName);
            this.groupBox6.Controls.Add(this._bmf_FirstName);
            this.groupBox6.Controls.Add(this._saveBuyerInfoAsDefault);
            this.groupBox6.Controls.Add(this.label14);
            this.groupBox6.Controls.Add(this.label15);
            this.groupBox6.Controls.Add(this.label9);
            this.groupBox6.Controls.Add(this.label10);
            this.groupBox6.Controls.Add(this.label11);
            this.groupBox6.Controls.Add(this.label12);
            this.groupBox6.Controls.Add(this.label13);
            this.groupBox6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox6.Location = new System.Drawing.Point(8, 122);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(352, 168);
            this.groupBox6.TabIndex = 1;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Buyer Merge Fields";
            // 
            // _bmf_Email
            // 
            this._bmf_Email.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._bmf_Email.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._bmf_Email.Location = new System.Drawing.Point(120, 144);
            this._bmf_Email.Name = "_bmf_Email";
            this._bmf_Email.Size = new System.Drawing.Size(224, 20);
            this._bmf_Email.TabIndex = 11;
            this._bmf_Email.Leave += new System.EventHandler(this.OnTextBoxLeave);
            this._bmf_Email.Enter += new System.EventHandler(this.OnTextBoxEnter);
            // 
            // _bmf_Fax
            // 
            this._bmf_Fax.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._bmf_Fax.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._bmf_Fax.Location = new System.Drawing.Point(120, 123);
            this._bmf_Fax.Name = "_bmf_Fax";
            this._bmf_Fax.Size = new System.Drawing.Size(224, 20);
            this._bmf_Fax.TabIndex = 10;
            this._bmf_Fax.Leave += new System.EventHandler(this.OnTextBoxLeave);
            this._bmf_Fax.Enter += new System.EventHandler(this.OnTextBoxEnter);
            // 
            // _bmf_Phone
            // 
            this._bmf_Phone.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._bmf_Phone.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._bmf_Phone.Location = new System.Drawing.Point(120, 102);
            this._bmf_Phone.Name = "_bmf_Phone";
            this._bmf_Phone.Size = new System.Drawing.Size(224, 20);
            this._bmf_Phone.TabIndex = 4;
            this._bmf_Phone.Leave += new System.EventHandler(this.OnTextBoxLeave);
            this._bmf_Phone.Enter += new System.EventHandler(this.OnTextBoxEnter);
            // 
            // _bmf_Address
            // 
            this._bmf_Address.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._bmf_Address.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._bmf_Address.Location = new System.Drawing.Point(120, 81);
            this._bmf_Address.Name = "_bmf_Address";
            this._bmf_Address.Size = new System.Drawing.Size(224, 20);
            this._bmf_Address.TabIndex = 3;
            this._bmf_Address.Leave += new System.EventHandler(this.OnTextBoxLeave);
            this._bmf_Address.Enter += new System.EventHandler(this.OnTextBoxEnter);
            // 
            // _bmf_FullName
            // 
            this._bmf_FullName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._bmf_FullName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._bmf_FullName.Location = new System.Drawing.Point(120, 60);
            this._bmf_FullName.Name = "_bmf_FullName";
            this._bmf_FullName.Size = new System.Drawing.Size(128, 20);
            this._bmf_FullName.TabIndex = 2;
            this._bmf_FullName.Leave += new System.EventHandler(this.OnTextBoxLeave);
            this._bmf_FullName.Enter += new System.EventHandler(this.OnTextBoxEnter);
            // 
            // _bmf_LastName
            // 
            this._bmf_LastName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._bmf_LastName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._bmf_LastName.Location = new System.Drawing.Point(120, 39);
            this._bmf_LastName.Name = "_bmf_LastName";
            this._bmf_LastName.Size = new System.Drawing.Size(128, 20);
            this._bmf_LastName.TabIndex = 1;
            this._bmf_LastName.Leave += new System.EventHandler(this.OnTextBoxLeave);
            this._bmf_LastName.Enter += new System.EventHandler(this.OnTextBoxEnter);
            // 
            // _bmf_FirstName
            // 
            this._bmf_FirstName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._bmf_FirstName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._bmf_FirstName.Location = new System.Drawing.Point(120, 18);
            this._bmf_FirstName.Name = "_bmf_FirstName";
            this._bmf_FirstName.Size = new System.Drawing.Size(128, 20);
            this._bmf_FirstName.TabIndex = 0;
            this._bmf_FirstName.Leave += new System.EventHandler(this.OnTextBoxLeave);
            this._bmf_FirstName.Enter += new System.EventHandler(this.OnTextBoxEnter);
            // 
            // _saveBuyerInfoAsDefault
            // 
            this._saveBuyerInfoAsDefault.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._saveBuyerInfoAsDefault.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._saveBuyerInfoAsDefault.Location = new System.Drawing.Point(256, 16);
            this._saveBuyerInfoAsDefault.Name = "_saveBuyerInfoAsDefault";
            this._saveBuyerInfoAsDefault.Size = new System.Drawing.Size(88, 64);
            this._saveBuyerInfoAsDefault.TabIndex = 14;
            this._saveBuyerInfoAsDefault.Text = "Save Buyer Info as Default...";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(40, 144);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(35, 13);
            this.label14.TabIndex = 13;
            this.label14.Text = "Email:";
            this.label14.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(40, 123);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(27, 13);
            this.label15.TabIndex = 12;
            this.label15.Text = "Fax:";
            this.label15.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(40, 102);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(41, 13);
            this.label9.TabIndex = 9;
            this.label9.Text = "Phone:";
            this.label9.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(40, 81);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(48, 13);
            this.label10.TabIndex = 8;
            this.label10.Text = "Address:";
            this.label10.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(40, 60);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(57, 13);
            this.label11.TabIndex = 7;
            this.label11.Text = "Full Name:";
            this.label11.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(40, 39);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(61, 13);
            this.label12.TabIndex = 6;
            this.label12.Text = "Last Name:";
            this.label12.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(40, 18);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(60, 13);
            this.label13.TabIndex = 5;
            this.label13.Text = "First Name:";
            this.label13.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this._abmf_Email);
            this.groupBox7.Controls.Add(this._abmf_Fax);
            this.groupBox7.Controls.Add(this._abmf_Phone);
            this.groupBox7.Controls.Add(this._abmf_Address);
            this.groupBox7.Controls.Add(this._abmf_FullName);
            this.groupBox7.Controls.Add(this._abmf_LastName);
            this.groupBox7.Controls.Add(this._abmf_FirstName);
            this.groupBox7.Controls.Add(this.label16);
            this.groupBox7.Controls.Add(this.label17);
            this.groupBox7.Controls.Add(this.label18);
            this.groupBox7.Controls.Add(this.label19);
            this.groupBox7.Controls.Add(this.label20);
            this.groupBox7.Controls.Add(this.label21);
            this.groupBox7.Controls.Add(this.label22);
            this.groupBox7.Controls.Add(this._saveAgentInfoAsDefault);
            this.groupBox7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox7.Location = new System.Drawing.Point(8, 290);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(352, 168);
            this.groupBox7.TabIndex = 2;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Agent / Broker Merge Fields";
            // 
            // _abmf_Email
            // 
            this._abmf_Email.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._abmf_Email.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._abmf_Email.Location = new System.Drawing.Point(120, 144);
            this._abmf_Email.Name = "_abmf_Email";
            this._abmf_Email.Size = new System.Drawing.Size(224, 20);
            this._abmf_Email.TabIndex = 11;
            this._abmf_Email.Leave += new System.EventHandler(this.OnTextBoxLeave);
            this._abmf_Email.Enter += new System.EventHandler(this.OnTextBoxEnter);
            // 
            // _abmf_Fax
            // 
            this._abmf_Fax.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._abmf_Fax.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._abmf_Fax.Location = new System.Drawing.Point(120, 123);
            this._abmf_Fax.Name = "_abmf_Fax";
            this._abmf_Fax.Size = new System.Drawing.Size(224, 20);
            this._abmf_Fax.TabIndex = 10;
            this._abmf_Fax.Leave += new System.EventHandler(this.OnTextBoxLeave);
            this._abmf_Fax.Enter += new System.EventHandler(this.OnTextBoxEnter);
            // 
            // _abmf_Phone
            // 
            this._abmf_Phone.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._abmf_Phone.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._abmf_Phone.Location = new System.Drawing.Point(120, 102);
            this._abmf_Phone.Name = "_abmf_Phone";
            this._abmf_Phone.Size = new System.Drawing.Size(224, 20);
            this._abmf_Phone.TabIndex = 4;
            this._abmf_Phone.Leave += new System.EventHandler(this.OnTextBoxLeave);
            this._abmf_Phone.Enter += new System.EventHandler(this.OnTextBoxEnter);
            // 
            // _abmf_Address
            // 
            this._abmf_Address.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._abmf_Address.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._abmf_Address.Location = new System.Drawing.Point(120, 81);
            this._abmf_Address.Name = "_abmf_Address";
            this._abmf_Address.Size = new System.Drawing.Size(224, 20);
            this._abmf_Address.TabIndex = 3;
            this._abmf_Address.Leave += new System.EventHandler(this.OnTextBoxLeave);
            this._abmf_Address.Enter += new System.EventHandler(this.OnTextBoxEnter);
            // 
            // _abmf_FullName
            // 
            this._abmf_FullName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._abmf_FullName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._abmf_FullName.Location = new System.Drawing.Point(120, 60);
            this._abmf_FullName.Name = "_abmf_FullName";
            this._abmf_FullName.Size = new System.Drawing.Size(128, 20);
            this._abmf_FullName.TabIndex = 2;
            this._abmf_FullName.Leave += new System.EventHandler(this.OnTextBoxLeave);
            this._abmf_FullName.Enter += new System.EventHandler(this.OnTextBoxEnter);
            // 
            // _abmf_LastName
            // 
            this._abmf_LastName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._abmf_LastName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._abmf_LastName.Location = new System.Drawing.Point(120, 39);
            this._abmf_LastName.Name = "_abmf_LastName";
            this._abmf_LastName.Size = new System.Drawing.Size(128, 20);
            this._abmf_LastName.TabIndex = 1;
            this._abmf_LastName.Leave += new System.EventHandler(this.OnTextBoxLeave);
            this._abmf_LastName.Enter += new System.EventHandler(this.OnTextBoxEnter);
            // 
            // _abmf_FirstName
            // 
            this._abmf_FirstName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._abmf_FirstName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._abmf_FirstName.Location = new System.Drawing.Point(120, 18);
            this._abmf_FirstName.Name = "_abmf_FirstName";
            this._abmf_FirstName.Size = new System.Drawing.Size(128, 20);
            this._abmf_FirstName.TabIndex = 0;
            this._abmf_FirstName.Leave += new System.EventHandler(this.OnTextBoxLeave);
            this._abmf_FirstName.Enter += new System.EventHandler(this.OnTextBoxEnter);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(40, 144);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(35, 13);
            this.label16.TabIndex = 13;
            this.label16.Text = "Email:";
            this.label16.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(40, 123);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(27, 13);
            this.label17.TabIndex = 12;
            this.label17.Text = "Fax:";
            this.label17.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(40, 102);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(41, 13);
            this.label18.TabIndex = 9;
            this.label18.Text = "Phone:";
            this.label18.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(40, 81);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(48, 13);
            this.label19.TabIndex = 8;
            this.label19.Text = "Address:";
            this.label19.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(40, 60);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(57, 13);
            this.label20.TabIndex = 7;
            this.label20.Text = "Full Name:";
            this.label20.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(40, 39);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(61, 13);
            this.label21.TabIndex = 6;
            this.label21.Text = "Last Name:";
            this.label21.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(40, 18);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(60, 13);
            this.label22.TabIndex = 5;
            this.label22.Text = "First Name:";
            this.label22.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // _saveAgentInfoAsDefault
            // 
            this._saveAgentInfoAsDefault.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._saveAgentInfoAsDefault.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._saveAgentInfoAsDefault.Location = new System.Drawing.Point(256, 16);
            this._saveAgentInfoAsDefault.Name = "_saveAgentInfoAsDefault";
            this._saveAgentInfoAsDefault.Size = new System.Drawing.Size(88, 64);
            this._saveAgentInfoAsDefault.TabIndex = 15;
            this._saveAgentInfoAsDefault.Text = "Save Agent Info as Default...";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this._customHUD);
            this.groupBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox4.Location = new System.Drawing.Point(320, 376);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(200, 71);
            this.groupBox4.TabIndex = 72;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Negotiating HUD Generator";
            // 
            // _customHUD
            // 
            this._customHUD.ForeColor = System.Drawing.Color.Black;
            this._customHUD.Location = new System.Drawing.Point(20, 19);
            this._customHUD.Name = "_customHUD";
            this._customHUD.Size = new System.Drawing.Size(168, 40);
            this._customHUD.TabIndex = 0;
            this._customHUD.Text = "Custom HUD";
            this._customHUD.UseVisualStyleBackColor = true;
            // 
            // DocumentGenerationControl
            // 
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.groupBox7);
            this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.Name = "DocumentGenerationControl";
            this.Size = new System.Drawing.Size(898, 464);
            this.Tag = "";
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._pictureBoxDocumentControl)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox9.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.ResumeLayout(false);

		}
		#endregion
		
		#region PROPERTIES

        public string UserAppFolder
        {
            get { return _userAppFolder; }
            set { _userAppFolder = value; }
        }
												
		public bool P_Generic_PacketCoverLetterThey
		{
			get
			{
				return _generic_PacketCoverLetterThey.Checked;
			}
			set
			{
				if (_generic_PacketCoverLetterThey.Checked == value)
					return;
				_generic_PacketCoverLetterThey.Checked = value;
			}
		}
		public string P_1stLien_FaxLabelLine1
		{
			get
			{
				return _1stLien_FaxLabelLine1.Text;
			}
			set
			{
				if (_1stLien_FaxLabelLine1.Text == value)
					return;
				_1stLien_FaxLabelLine1.Text = value;
			}
		}
		public bool P_Generic_PacketCoverLetterHim
		{
			get
			{
				return _generic_PacketCoverLetterHim.Checked;
			}
			set
			{
				if (_generic_PacketCoverLetterHim.Checked == value)
					return;
				_generic_PacketCoverLetterHim.Checked = value;
			}
		}
		
		
		public bool P_Generic_BuyerCoverLetterHer
		{
			get
			{
				return _generic_PacketCoverLetterHer.Checked;
			}
			set
			{
				if (_generic_PacketCoverLetterHer.Checked == value)
					return;
				_generic_PacketCoverLetterHer.Checked = value;
			}
		}																		
				
		public string po_MailingAddress
		{
			get
			{
				return _po_MailingAddress.Text;
			}
			set
			{
				if (_po_MailingAddress.Text == value)
					return;
				_po_MailingAddress.Text = value;
			}
		}
		public string po_PropertyAddress
		{
			get
			{
				return _po_PropertyAddress.Text;
			}
			set
			{
				if (_po_PropertyAddress.Text == value)
					return;
				_po_PropertyAddress.Text = value;
			}
		}
		public string po_FullName
		{
			get
			{
				return _po_FullName.Text;
			}
			set
			{
				if (_po_FullName.Text == value)
					return;
				_po_FullName.Text = value;
			}
		}
		public string po_LastName
		{
			get
			{
				return _po_LastName.Text;
			}
			set
			{
				if (_po_LastName.Text == value)
					return;
				_po_LastName.Text = value;
			}
		}
		public string po_MiddleName
		{
			get {return _po_MiddleName.Text;}
			set {_po_MiddleName.Text = value;}
		}
		public string po_FirstName
		{
			get
			{
				return _po_FirstName.Text;
			}
			set
			{
				if (_po_FirstName.Text == value)
					return;
				_po_FirstName.Text = value;
			}
		}
		public string bmf_Email
		{
			get
			{
				return _bmf_Email.Text;
			}
			set
			{
				if (_bmf_Email.Text == value)
					return;
				_bmf_Email.Text = value;
			}
		}
		public string bmf_Fax
		{
			get
			{
				return _bmf_Fax.Text;
			}
			set
			{
				if (_bmf_Fax.Text == value)
					return;
				_bmf_Fax.Text = value;
			}
		}
		public string bmf_Phone
		{
			get
			{
				return _bmf_Phone.Text;
			}
			set
			{
				if (_bmf_Phone.Text == value)
					return;
				_bmf_Phone.Text = value;
			}
		}
		public string bmf_Address
		{
			get
			{
				return _bmf_Address.Text;
			}
			set
			{
				if (_bmf_Address.Text == value)
					return;
				_bmf_Address.Text = value;
			}
		}
		public string bmf_FullName
		{
			get
			{
				return _bmf_FullName.Text;
			}
			set
			{
				if (_bmf_FullName.Text == value)
					return;
				_bmf_FullName.Text = value;
			}
		}
		public string bmf_LastName
		{
			get
			{
				return _bmf_LastName.Text;
			}
			set
			{
				if (_bmf_LastName.Text == value)
					return;
				_bmf_LastName.Text = value;
			}
		}
		public string bmf_FirstName
		{
			get
			{
				return _bmf_FirstName.Text;
			}
			set
			{
				if (_bmf_FirstName.Text == value)
					return;
				_bmf_FirstName.Text = value;
			}
		}
		public string abmf_Email
		{
			get
			{
				return _abmf_Email.Text;
			}
			set
			{
				if (_abmf_Email.Text == value)
					return;
				_abmf_Email.Text = value;
			}
		}
		public string abmf_Fax
		{
			get
			{
				return _abmf_Fax.Text;
			}
			set
			{
				if (_abmf_Fax.Text == value)
					return;
				_abmf_Fax.Text = value;
			}
		}
		public string abmf_Phone
		{
			get
			{
				return _abmf_Phone.Text;
			}
			set
			{
				if (_abmf_Phone.Text == value)
					return;
				_abmf_Phone.Text = value;
			}
		}
		public string abmf_Address
		{
			get
			{
				return _abmf_Address.Text;
			}
			set
			{
				if (_abmf_Address.Text == value)
					return;
				_abmf_Address.Text = value;
			}
		}
		public string abmf_FullName
		{
			get
			{
				return _abmf_FullName.Text;
			}
			set
			{
				if (_abmf_FullName.Text == value)
					return;
				_abmf_FullName.Text = value;
			}
		}
		public string abmf_LastName
		{
			get
			{
				return _abmf_LastName.Text;
			}
			set
			{
				if (_abmf_LastName.Text == value)
					return;
				_abmf_LastName.Text = value;
			}
		}
		public string abmf_FirstName
		{
			get
			{
				return _abmf_FirstName.Text;
			}
			set
			{
				if (_abmf_FirstName.Text == value)
					return;
				_abmf_FirstName.Text = value;
			}
		}
		#endregion

        #region EVENT HANDLERS
        private void OnTextBoxEnter(object sender, System.EventArgs e)
		{
			//((TextBox)sender).BackColor = Color.LightSalmon;			
		}

		private void OnTextBoxLeave(object sender, System.EventArgs e)
		{
			((TextBox)sender).BackColor = Color.White;						
			PreSaveProcedure();
			base.AutoSaveDocument();
        }
        #endregion

        public void PreSaveProcedure()
		{
			DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.OwnerInfo.FirstName = po_FirstName;
			DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.OwnerInfo.MiddleName = po_MiddleName;
			DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.OwnerInfo.LastName = po_LastName;
			DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.OwnerInfo.FullName = po_FullName;
			DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.OwnerInfo.AddressInfo.SiteAddress.Address.SiteStreetAddress = po_PropertyAddress;
			DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.OwnerInfo.AddressInfo.MailAddress.Address.SiteStreetAddress = po_MailingAddress;
			DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.BuyerInfo.FirstName = bmf_FirstName;
			DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.BuyerInfo.LastName = bmf_LastName;
			DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.BuyerInfo.FullName = bmf_FullName;
			DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.BuyerInfo.AddressInfo.SiteAddress.Address.SiteStreetAddress = bmf_Address;
			DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.BuyerInfo.PhoneNr = bmf_Phone;
			DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.BuyerInfo.FaxNr = bmf_Fax;
			DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.BuyerInfo.Email = bmf_Email;
			DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.AgentInfo.FirstName = abmf_FirstName;							
			DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.AgentInfo.LastName = abmf_LastName;							
			DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.AgentInfo.FullName = abmf_FullName;							
			DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.AgentInfo.AddressInfo.SiteAddress.Address.SiteStreetAddress = abmf_Address;							
			DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.AgentInfo.PhoneNr = abmf_Phone;							
			DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.AgentInfo.FaxNr = abmf_Fax;				
			DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.AgentInfo.Email = abmf_Email;							
		}

		public void DisplayData()
		{
			DefaultInfoValues div = DataManager.Instance.GetDefaultInfoValues();

			po_FirstName = DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.OwnerInfo.FirstName;
			po_MiddleName = DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.OwnerInfo.MiddleName;
			po_LastName = DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.OwnerInfo.LastName;
			po_FullName = DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.OwnerInfo.FullName;
			po_PropertyAddress = DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.OwnerInfo.AddressInfo.SiteAddress.Address.SiteStreetAddress;
			po_MailingAddress = DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.OwnerInfo.AddressInfo.MailAddress.Address.SiteStreetAddress;

            if (null != div && null != div.BuyerInfo)
            {
                if (null != DataManager.Instance.GetCurrentShortSaleDocument() && null != DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive && null != DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.BuyerInfo)
                {
                    bmf_FirstName = (DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.BuyerInfo.FirstName != div.BuyerInfo.FirstName) && (string.Empty != DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.BuyerInfo.FirstName) && (null != DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.BuyerInfo.FirstName) ? DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.BuyerInfo.FirstName : div.BuyerInfo.FirstName;
                    bmf_LastName = (DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.BuyerInfo.LastName != div.BuyerInfo.LastName) && (string.Empty != DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.BuyerInfo.LastName) && (null != DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.BuyerInfo.LastName) ? DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.BuyerInfo.LastName : div.BuyerInfo.LastName;
                    bmf_FullName = (DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.BuyerInfo.FullName != div.BuyerInfo.FullName) && (string.Empty != DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.BuyerInfo.FullName) && (null != DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.BuyerInfo.FullName) ? DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.BuyerInfo.FullName : div.BuyerInfo.FullName;
                    bmf_Address = (DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.BuyerInfo.AddressInfo.SiteAddress.Address.SiteStreetAddress != div.BuyerInfo.Address) && (string.Empty != DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.BuyerInfo.AddressInfo.SiteAddress.Address.SiteStreetAddress) && (null != DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.BuyerInfo.AddressInfo.SiteAddress.Address.SiteStreetAddress) ? DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.BuyerInfo.AddressInfo.SiteAddress.Address.SiteStreetAddress : div.BuyerInfo.Address;
                    bmf_Phone = (DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.BuyerInfo.PhoneNr != div.BuyerInfo.Phone) && (string.Empty != DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.BuyerInfo.PhoneNr) && (null != DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.BuyerInfo.PhoneNr) ? DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.BuyerInfo.PhoneNr : div.BuyerInfo.Phone;
                    bmf_Fax = (DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.BuyerInfo.FaxNr != div.BuyerInfo.Fax) && (string.Empty != DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.BuyerInfo.FaxNr) && (null != DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.BuyerInfo.FaxNr) ? DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.BuyerInfo.FaxNr : div.BuyerInfo.Fax;
                    bmf_Email = (DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.BuyerInfo.Email != div.BuyerInfo.Email) && (string.Empty != DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.BuyerInfo.FirstName) && (null != DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.BuyerInfo.Email) ? DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.BuyerInfo.Email : div.BuyerInfo.Email;
                }
            }
            else
            {
                //set saved user data
                bmf_FirstName = DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.BuyerInfo.FirstName;
                bmf_LastName = DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.BuyerInfo.LastName;
                bmf_FullName = DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.BuyerInfo.FullName;
                bmf_Address = DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.BuyerInfo.AddressInfo.SiteAddress.Address.SiteStreetAddress;
                bmf_Phone = DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.BuyerInfo.PhoneNr;
                bmf_Fax = DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.BuyerInfo.FaxNr;
                bmf_Email = DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.BuyerInfo.Email;
            }


            if (null != div && null != div.AgentInfo)
            {
                if (null != DataManager.Instance.GetCurrentShortSaleDocument() && null != DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive && null != DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.AgentInfo)
                {
                    abmf_FirstName = (DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.AgentInfo.FirstName != div.AgentInfo.FirstName) && (string.Empty != DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.AgentInfo.FirstName) && (null != DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.AgentInfo.FirstName) ? DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.AgentInfo.FirstName : div.AgentInfo.FirstName;
                    abmf_LastName = (DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.AgentInfo.LastName != div.AgentInfo.LastName) && (string.Empty != DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.AgentInfo.LastName) && (null != DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.AgentInfo.LastName) ? DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.AgentInfo.LastName : div.AgentInfo.LastName;
                    abmf_FullName = (DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.AgentInfo.FullName != div.AgentInfo.FullName) && (string.Empty != DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.AgentInfo.FullName) && (null != DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.AgentInfo.FullName) ? DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.AgentInfo.FullName : div.AgentInfo.FullName;
                    abmf_Address = (DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.AgentInfo.AddressInfo.SiteAddress.Address.SiteStreetAddress != div.AgentInfo.Address) && (string.Empty != DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.AgentInfo.AddressInfo.SiteAddress.Address.SiteStreetAddress) && (null != DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.AgentInfo.AddressInfo.SiteAddress.Address.SiteStreetAddress) ? DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.AgentInfo.AddressInfo.SiteAddress.Address.SiteStreetAddress : div.AgentInfo.Address;
                    abmf_Phone = (DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.AgentInfo.PhoneNr != div.AgentInfo.Phone) && (string.Empty != DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.AgentInfo.PhoneNr) && (null != DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.AgentInfo.PhoneNr) ? DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.AgentInfo.PhoneNr : div.AgentInfo.Phone;
                    abmf_Fax = (DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.AgentInfo.FaxNr != div.AgentInfo.Fax) && (string.Empty != DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.AgentInfo.FaxNr) && (null != DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.AgentInfo.FaxNr) ? DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.AgentInfo.FaxNr : div.AgentInfo.Fax;
                    abmf_Email = (DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.AgentInfo.Email != div.AgentInfo.Email) && (string.Empty != DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.AgentInfo.Email) && (null != DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.AgentInfo.Email) ? DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.AgentInfo.Email : div.AgentInfo.Email;
                }
            }
            else
            {
                //set saved user data
                abmf_FirstName = DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.AgentInfo.FirstName;
                abmf_LastName = DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.AgentInfo.LastName;
                abmf_FullName = DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.AgentInfo.FullName;
                abmf_Address = DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.AgentInfo.AddressInfo.SiteAddress.Address.SiteStreetAddress;
                abmf_Phone = DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.AgentInfo.PhoneNr;
                abmf_Fax = DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.AgentInfo.FaxNr;
                abmf_Email = DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.AgentInfo.Email;	
            }
            

			_realDataDisplayed = true;
		}

		public void DisplayDefaultInfoData()
		{
			DefaultInfoValues div = DataManager.Instance.GetDefaultInfoValues();

            if (null != div)
            {
                bmf_FirstName = div.BuyerInfo.FirstName;
                bmf_LastName = div.BuyerInfo.LastName;
                bmf_FullName = div.BuyerInfo.FullName;
                bmf_Address = div.BuyerInfo.Address;
                bmf_Phone = div.BuyerInfo.Phone;
                bmf_Fax = div.BuyerInfo.Fax;
                bmf_Email = div.BuyerInfo.Email;
                abmf_FirstName = div.AgentInfo.FirstName;
                abmf_LastName = div.AgentInfo.LastName;
                abmf_FullName = div.AgentInfo.FullName;
                abmf_Address = div.AgentInfo.Address;
                abmf_Phone = div.AgentInfo.Phone;
                abmf_Fax = div.AgentInfo.Fax;
                abmf_Email = div.AgentInfo.Email;
            }
		}		

		LienSet GetSelectedLienSet()
		{
			if (_firstLienDocumentSet.Checked)
				return LienSet.FirstLienSet;
			else if (_secondLienDocumentSet.Checked)
				return LienSet.SecondLienSet;
			else if (_thirdLienDocumentSet.Checked)
				return LienSet.ThirdLienSet;

			return LienSet.NotSelected;
		}

        void On_EndPrint(object sender, PrintEventArgs e)
        {
            if (null != _gen)
                _gen.ResetGen();
        }

        private void On_PrintRepairs(object sender, PrintPageEventArgs e)
        {            
            if (null == _gen)
                _gen = new EstimateRepairsReportGenerator(e);

            try
            {
                int selectedLienSet = (int)GetSelectedLienSet() - 1;
                string loanNr = (selectedLienSet + 1).ToString() + Util.GetSuffix(selectedLienSet + 1);
                string accountNr = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[selectedLienSet].AccountNr;
                string address = DataManager.Instance.PropertyAddress; // .GetFullSiteAddress();
                string fullName = DataManager.Instance.HomeOwnerFullName; // .GetCurrentShortSaleDocument().ContactInfo.FullName;
                string primSSNr = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.PrimarySS;
                string secSSNr = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.SecondarySS;
                string phoneNr = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[selectedLienSet].LienHolderContactInfo.SetupDept.Phone;
                string faxNr = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[selectedLienSet].LienHolderContactInfo.SetupDept.Fax;
                _gen.Generate(e, loanNr, accountNr, address, fullName, primSSNr, secSSNr);
            }
            catch { }
        }

        TemplateEditor te = null;
        SelectedDocuments sd = null;
		private void On_GetShortSaleDocsReady_Click(object sender, EventArgs e)
		{
			if (LienSet.NotSelected == GetSelectedLienSet())
			{
                MessageBox.Show(UnselectedLienMsg, StringStore.AppName, MessageBoxButtons.OK, MessageBoxIcon.Information);			
				return;
			}

            int selectedLienSet = (int)GetSelectedLienSet();

            if (DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien.Length < selectedLienSet)
            {
                MessageBox.Show("Current document does not contain " + selectedLienSet.ToString() + " liens!", StringStore.AppName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
			
            sd = new SelectedDocuments();
                        

            if (_generic_PacketCoverLetterHim.Checked)
                sd._packetCoverLetter_Him = true;
            if (_generic_PacketCoverLetterHer.Checked)
                sd._packetCoverLetter_Her = true;
            if (_generic_PacketCoverLetterThey.Checked)
                sd._packetCoverLetter_They = true;	

			if (_standardPacketCheckList.Checked)
				sd._standardPacketChecklist = true;
			if (_FAXCoverAuthorization.Checked)
				sd._faxCoverAuthorization = true;
			if (_AuthorizationToRelease.Checked)
				sd._authorizationToRelease = true;
			if (_FaxCoverGeneral.Checked)
				sd._faxCoverGeneral = true;			
			if (_hardshipLetter.Checked)
				sd._handshipLetter = true;
			if (_lienSpecificUserDocument1.Checked)
				sd._lienSpecificUserDocument1 = true;
			if (_lienSpecificUserDocument2.Checked)
				sd._lienSpecificUserDocument2 = true;
			if (_lienSpecificUserDocument3.Checked)
				sd._lienSpecificUserDocument3 = true;
            if (_lienSpecificUserDocument4.Checked)
                sd._lienSpecificUserDocument4 = true;
            if (_lienSpecificUserDocument5.Checked)
                sd._lienSpecificUserDocument5 = true;

            if (!sd.IsAnyDocumentSelected())
            {                
                MessageBox.Show("Select Document(s) using checkboxes above!", StringStore.AppName, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {                
                te = new TemplateEditor(GetSelectedLienSet(), sd);
                
                te.WindowState = FormWindowState.Maximized;
                te.UserAppFolder = _userAppFolder;
                te.Show();
            }
		}

        

                                   		

		private void On_SaveBuyerInfoAsDefault_Click(object sender, EventArgs e)
		{
			UpdateDefaultInfoValues();
		}

		private void On_AaveAgentInfoAsDefault_Click(object sender, EventArgs e)
		{
			UpdateDefaultInfoValues();
		}

		private void On_HUD1Generator_Click(object sender, EventArgs e)
		{
            LienSet ls = GetSelectedLienSet();
			if (LienSet.NotSelected == ls)
			{
                MessageBox.Show(UnselectedLienMsg, StringStore.AppName, MessageBoxButtons.OK, MessageBoxIcon.Information);			
				return;
			}

            MessageBox.Show("NOTE: Any changes to HUD Form must be made from Cacluate offer tab", StringStore.AppName, MessageBoxButtons.OK, MessageBoxIcon.Information);

			HUD hud = new HUD();

            hud.DisableSave();
            hud.SetToReadOnly();

			hud.ApplicationPath = DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.ArchiveFolder;
			hud.ConfigPath = DataManager.Instance.UserAppFolder;
			hud.SetValues(_bmf_FullName.Text, _bmf_Address.Text, _po_FullName.Text, _po_MailingAddress.Text, DataManager.Instance.GetFullSiteAddress());

            string hudFileName = string.Empty;
            bool existingHUDFileName = HUDFileNameManager.Instance.GetCreateDocument(Application.StartupPath, DataManager.Instance.DocumentName, (int)ls, out hudFileName);

            if (LienSet.FirstLienSet == GetSelectedLienSet())
            {
                //set fileName
                string path = Application.StartupPath + @"\" + StringStore.SavedExtDocumentsFolder;
                string suffix = StringStore.HUDSubFolder;
                //string docName = path + suffix;
                //string sSAddress = string.Empty;
                //sSAddress = DataManager.Instance.GetSiteAddress();
                //if (null != sSAddress && string.Empty != sSAddress)
                //    docName += DataManager.Instance.GetCurrentShortSaleDocument().DocumentId + ".1stLien" + "." + sSAddress + ".hsd";
                //else
                //    docName += DataManager.Instance.GetCurrentShortSaleDocument().DocumentId + ".1stLien" + ".hsd";

                hud.SetFileName(path, suffix, path + suffix + hudFileName);
                if (existingHUDFileName)
                    hud.ForceLoad(path + suffix + hudFileName);

                hud.SetCash();

                try
                {
                    string accountNr = string.Empty;
                    try { accountNr = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[0].AccountNr; }
                    catch { accountNr = string.Empty; }
                    hud.SetLoanNumber(accountNr);
                }
                catch { }

                try
                {
                    decimal initialOfferSalesPrice = 0;
                    decimal comission = 0;

                    try { initialOfferSalesPrice = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialOfferSalesPriceValue; }
                    catch { initialOfferSalesPrice = 0; }

                    try { comission = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialRealEstateComissionPerc; }
                    catch { comission = 0; }

                    hud.SetSalesPriceDetails(initialOfferSalesPrice, comission);

                }
                catch { }

                try
                {
                    string firstCompanyName = string.Empty;
                    string secondCompanyName = string.Empty;
                    decimal initialOfferNetToFirst = 0;
                    decimal initialOfferNetToSecond = 0;

                    try { firstCompanyName = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[0].CompanyName; }
                    catch { firstCompanyName = string.Empty; }

                    try { initialOfferNetToFirst = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialOfferToFirstLender; }
                    catch { initialOfferNetToFirst = 0; }

                    try { secondCompanyName = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[1].CompanyName; }
                    catch { secondCompanyName = string.Empty; }

                    try { initialOfferNetToSecond = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[0].InitialOfferToSecondLender; }
                    catch { initialOfferNetToSecond = 0; }

                    hud.SetCompanyDetails(firstCompanyName, initialOfferNetToFirst, secondCompanyName, initialOfferNetToSecond);
                }
                catch { }

                //set initial offer sales price
                try
                {
                    decimal initOfferSalesPrice = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialOfferSalesPriceValue;
                    hud.SetIntialOfferSalesPrice(initOfferSalesPrice);
                }
                catch { }

                string nameOfListingAgency = string.Empty;
                try { nameOfListingAgency = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialRealEstateCommisionBreakDown.NameOfListingAgency; }
                catch { nameOfListingAgency = string.Empty; }

                string nameOfSellingAgency = "";
                try { nameOfSellingAgency = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialRealEstateCommisionBreakDown.NameOfSellingAgency; }
                catch { nameOfSellingAgency = string.Empty; }

                decimal listingAgencyValue = 0;
                try { listingAgencyValue = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialRealEstateCommisionBreakDown.ListingAgencyValue * CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialOfferSalesPriceValue / 100; }
                catch { listingAgencyValue = 0; }

                decimal sellingAgencyValue = 0;
                try { sellingAgencyValue = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialRealEstateCommisionBreakDown.SellingAgencyValue * CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialOfferSalesPriceValue / 100; }
                catch { sellingAgencyValue = 0; }

                hud.SetRealEstateCommissionValues(nameOfListingAgency, nameOfSellingAgency, listingAgencyValue, sellingAgencyValue);

                decimal loanOriginationFee = 0;
                try { loanOriginationFee = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialSellerConcessionBreakDown.LoanOriginationFeeValue * CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialOfferSalesPriceValue / 100; }
                catch { loanOriginationFee = 0; }

                decimal loanDiscountFee = 0;
                try { loanDiscountFee = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialSellerConcessionBreakDown.LoanDiscountFeeValue * CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialOfferSalesPriceValue / 100; }
                catch { loanDiscountFee = 0; }

                hud.SetSellerConcessionValues(loanOriginationFee, loanDiscountFee);

                decimal EscrowFeeValue = 0;
                decimal TitleInsuranceValue = 0;
                decimal AppraisalFeeValue = 0;
                decimal CreditReportValue = 0;
                decimal RecordingFeeValue = 0;
                decimal HoaTransferFeeValue = 0;
                string Others1Name = string.Empty;
                decimal Others1Value = 0;
                string Others2Name = string.Empty;
                decimal Others2Value = 0;
                string Others3Name = string.Empty;
                decimal Others3Value = 0;
                string Others4Name = string.Empty;
                decimal Others4Value = 0;
                string Others5Name = string.Empty;
                decimal Others5Value = 0;
                string Others6Name = string.Empty;
                decimal Others6Value = 0;

                try { EscrowFeeValue = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialBuyerClosingCostBreakDown.EscrowFeeValue; }
                catch { EscrowFeeValue = 0; }

                try { TitleInsuranceValue = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialBuyerClosingCostBreakDown.TitleInsuranceValue; }
                catch { TitleInsuranceValue = 0; }

                try { AppraisalFeeValue = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialBuyerClosingCostBreakDown.AppraisalFeeValue; }
                catch { AppraisalFeeValue = 0; }

                try { CreditReportValue = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialBuyerClosingCostBreakDown.CreditReportValue; }
                catch { CreditReportValue = 0; }

                try { RecordingFeeValue = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialBuyerClosingCostBreakDown.RecordingFeeValue; }
                catch { RecordingFeeValue = 0; }

                try { HoaTransferFeeValue = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialBuyerClosingCostBreakDown.HoaTransferFeeValue; }
                catch { HoaTransferFeeValue = 0; }

                try { Others1Name = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialBuyerClosingCostBreakDown.Others1Name; }
                catch { Others1Name = string.Empty; }

                try { Others1Value = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialBuyerClosingCostBreakDown.Others1Value; }
                catch { Others1Value = 0; }

                try { Others2Name = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialBuyerClosingCostBreakDown.Others2Name; }
                catch { Others2Name = string.Empty; }

                try { Others2Value = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialBuyerClosingCostBreakDown.Others2Value; }
                catch { Others2Value = 0; }

                try { Others3Name = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialBuyerClosingCostBreakDown.Others3Name; }
                catch { Others3Name = string.Empty; }

                try { Others3Value = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialBuyerClosingCostBreakDown.Others3Value; }
                catch { Others3Value = 0; }

                try { Others4Name = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialBuyerClosingCostBreakDown.Others4Name; }
                catch { Others4Name = string.Empty; }

                try { Others4Value = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialBuyerClosingCostBreakDown.Others4Value; }
                catch { Others4Value = 0; }

                try { Others5Name = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialBuyerClosingCostBreakDown.Others5Name; }
                catch { Others5Name = string.Empty; }

                try { Others5Value = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialBuyerClosingCostBreakDown.Others5Value; }
                catch { Others5Value = 0; }

                try { Others6Name = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialBuyerClosingCostBreakDown.Others6Name; }
                catch { Others6Name = string.Empty; }

                try { Others6Value = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialBuyerClosingCostBreakDown.Others6Value; }
                catch { Others6Value = 0; }


                hud.SetBuyerClosingCostValues(
                    EscrowFeeValue,
                    TitleInsuranceValue,
                    AppraisalFeeValue,
                    CreditReportValue,
                    RecordingFeeValue,
                    "HOA Transferr Fee",
                    HoaTransferFeeValue,
                    Others1Name,
                    Others1Value,
                    Others2Name,
                    Others2Value,
                    Others3Name,
                    Others3Value,
                    Others4Name,
                    Others4Value,
                    Others5Name,
                    Others5Value,
                    Others6Name,
                    Others6Value);

                try { EscrowFeeValue = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialSellerClosingCostBreakDown.EscrowFeeValue; }
                catch { EscrowFeeValue = 0; }

                try { TitleInsuranceValue = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialSellerClosingCostBreakDown.TitleInsuranceValue; }
                catch { TitleInsuranceValue = 0; }

                try { AppraisalFeeValue = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialSellerClosingCostBreakDown.AppraisalFeeValue; }
                catch { AppraisalFeeValue = 0; }

                try { CreditReportValue = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialSellerClosingCostBreakDown.CreditReportValue; }
                catch { CreditReportValue = 0; }

                try { RecordingFeeValue = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialSellerClosingCostBreakDown.RecordingFeeValue; }
                catch { RecordingFeeValue = 0; }

                try { HoaTransferFeeValue = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialSellerClosingCostBreakDown.HoaTransferFeeValue; }
                catch { HoaTransferFeeValue = 0; }

                try { Others1Name = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialSellerClosingCostBreakDown.Others1Name; }
                catch { Others1Name = string.Empty; }

                try { Others1Value = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialSellerClosingCostBreakDown.Others1Value; }
                catch { Others1Value = 0; }

                try { Others2Name = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialSellerClosingCostBreakDown.Others2Name; }
                catch { Others2Name = string.Empty; }

                try { Others2Value = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialSellerClosingCostBreakDown.Others2Value; }
                catch { Others2Value = 0; }

                try { Others3Name = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialSellerClosingCostBreakDown.Others3Name; }
                catch { Others3Name = string.Empty; }

                try { Others3Value = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialSellerClosingCostBreakDown.Others3Value; }
                catch { Others3Value = 0; }

                try { Others4Name = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialSellerClosingCostBreakDown.Others4Name; }
                catch { Others4Name = string.Empty; }

                try { Others4Value = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialSellerClosingCostBreakDown.Others4Value; }
                catch { Others4Value = 0; }

                try { Others5Name = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialSellerClosingCostBreakDown.Others5Name; }
                catch { Others5Name = string.Empty; }

                try { Others5Value = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialSellerClosingCostBreakDown.Others5Value; }
                catch { Others5Value = 0; }

                try { Others6Name = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialSellerClosingCostBreakDown.Others6Name; }
                catch { Others6Name = string.Empty; }

                try { Others6Value = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialSellerClosingCostBreakDown.Others6Value; }
                catch { Others6Value = 0; }

                hud.SetSellerClosingCostValues(
                    EscrowFeeValue,
                    TitleInsuranceValue,
                    AppraisalFeeValue,
                    CreditReportValue,
                    RecordingFeeValue,
                    "HOA Transferr Fee",
                    HoaTransferFeeValue,
                    Others1Name,
                    Others1Value,
                    Others2Name,
                    Others2Value,
                    Others3Name,
                    Others3Value,
                    Others4Name,
                    Others4Value,
                    Others5Name,
                    Others5Value,
                    Others6Name,
                    Others6Value);
            }
            else
            {
                int _lienIdx = (int)GetSelectedLienSet() - 1;

                //set fileName
                string path = Application.StartupPath + @"\" + StringStore.SavedExtDocumentsFolder;
                string suffix = StringStore.HUDSubFolder;
                //string docName = path + suffix;
                //string sSAddress = string.Empty;
                //sSAddress = DataManager.Instance.GetSiteAddress();
                //if (null != sSAddress && string.Empty != sSAddress)
                //    docName += DataManager.Instance.GetCurrentShortSaleDocument().DocumentId + "." + (_lienIdx + 1).ToString() + Util.GetSuffix((_lienIdx + 1)) + "Lien" + "." + sSAddress + ".hsd";
                //else
                //    docName += DataManager.Instance.GetCurrentShortSaleDocument().DocumentId + "." + (_lienIdx + 1).ToString() + Util.GetSuffix((_lienIdx + 1)) + "Lien" + ".hsd";                
                //hud.SetFileName(path, suffix, docName);

                hud.SetFileName(path, suffix, path + suffix + hudFileName);
                if (existingHUDFileName)
                    hud.ForceLoad(path + suffix + hudFileName);

                hud.SetCash();

                try
                {
                    string accountNr = string.Empty;
                    try { accountNr = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[_lienIdx].AccountNr; }
                    catch { accountNr = string.Empty; }
                    hud.SetLoanNumber(accountNr);
                }
                catch { }

                try
                {
                    decimal initialOfferSalesPrice = 0;
                    decimal comission = 0;

                    try { initialOfferSalesPrice = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialOfferSalesPriceValue; }
                    catch { initialOfferSalesPrice = 0; }

                    try { comission = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialRealEstateComissionPerc; }
                    catch { comission = 0; }

                    hud.SetSalesPriceDetails(initialOfferSalesPrice, comission);

                }
                catch { }

                try
                {
                    string firstCompanyName = string.Empty;
                    string secondCompanyName = string.Empty;
                    decimal initialOfferNetToFirst = 0;
                    decimal initialOfferNetToSecond = 0;

                    try { firstCompanyName = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[0].CompanyName; }
                    catch { firstCompanyName = string.Empty; }

                    try { initialOfferNetToFirst = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialOfferToFirstLender; }
                    catch { initialOfferNetToFirst = 0; }

                    try { secondCompanyName = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[1].CompanyName; }
                    catch { secondCompanyName = string.Empty; }

                    try { initialOfferNetToSecond = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[0].InitialOfferToSecondLender; }
                    catch { initialOfferNetToSecond = 0; }

                    hud.SetCompanyDetails(firstCompanyName, initialOfferNetToFirst, secondCompanyName, initialOfferNetToSecond);
                }
                catch { }

                //set initial offer sales price
                try
                {
                    decimal initOfferSalesPrice = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialOfferSalesPriceValue;
                    hud.SetIntialOfferSalesPrice(initOfferSalesPrice);
                }
                catch { }

                string nameOfListingAgency = string.Empty;
                try { nameOfListingAgency = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialRealEstateCommisionBreakDown.NameOfListingAgency; }
                catch { nameOfListingAgency = string.Empty; }

                string nameOfSellingAgency = "";
                try { nameOfSellingAgency = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialRealEstateCommisionBreakDown.NameOfSellingAgency; }
                catch { nameOfSellingAgency = string.Empty; }

                decimal listingAgencyValue = 0;
                try { listingAgencyValue = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialRealEstateCommisionBreakDown.ListingAgencyValue * CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialOfferSalesPriceValue / 100; }
                catch { listingAgencyValue = 0; }

                decimal sellingAgencyValue = 0;
                try { sellingAgencyValue = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialRealEstateCommisionBreakDown.SellingAgencyValue * CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialOfferSalesPriceValue / 100; }
                catch { sellingAgencyValue = 0; }

                hud.SetRealEstateCommissionValues(nameOfListingAgency, nameOfSellingAgency, listingAgencyValue, sellingAgencyValue);

                decimal loanOriginationFee = 0;
                try { loanOriginationFee = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialSellerConcessionBreakDown.LoanOriginationFeeValue * CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialOfferSalesPriceValue / 100; }
                catch { loanOriginationFee = 0; }

                decimal loanDiscountFee = 0;
                try { loanDiscountFee = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialSellerConcessionBreakDown.LoanDiscountFeeValue * CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialOfferSalesPriceValue / 100; }
                catch { loanDiscountFee = 0; }

                hud.SetSellerConcessionValues(loanOriginationFee, loanDiscountFee);

                decimal EscrowFeeValue = 0;
                decimal TitleInsuranceValue = 0;
                decimal AppraisalFeeValue = 0;
                decimal CreditReportValue = 0;
                decimal RecordingFeeValue = 0;
                decimal HoaTransferFeeValue = 0;
                string Others1Name = string.Empty;
                decimal Others1Value = 0;
                string Others2Name = string.Empty;
                decimal Others2Value = 0;
                string Others3Name = string.Empty;
                decimal Others3Value = 0;
                string Others4Name = string.Empty;
                decimal Others4Value = 0;
                string Others5Name = string.Empty;
                decimal Others5Value = 0;
                string Others6Name = string.Empty;
                decimal Others6Value = 0;

                try { EscrowFeeValue = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialBuyerClosingCostBreakDown.EscrowFeeValue; }
                catch { EscrowFeeValue = 0; }

                try { TitleInsuranceValue = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialBuyerClosingCostBreakDown.TitleInsuranceValue; }
                catch { TitleInsuranceValue = 0; }

                try { AppraisalFeeValue = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialBuyerClosingCostBreakDown.AppraisalFeeValue; }
                catch { AppraisalFeeValue = 0; }

                try { CreditReportValue = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialBuyerClosingCostBreakDown.CreditReportValue; }
                catch { CreditReportValue = 0; }

                try { RecordingFeeValue = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialBuyerClosingCostBreakDown.RecordingFeeValue; }
                catch { RecordingFeeValue = 0; }

                try { HoaTransferFeeValue = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialBuyerClosingCostBreakDown.HoaTransferFeeValue; }
                catch { HoaTransferFeeValue = 0; }

                try { Others1Name = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialBuyerClosingCostBreakDown.Others1Name; }
                catch { Others1Name = string.Empty; }

                try { Others1Value = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialBuyerClosingCostBreakDown.Others1Value; }
                catch { Others1Value = 0; }

                try { Others2Name = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialBuyerClosingCostBreakDown.Others2Name; }
                catch { Others2Name = string.Empty; }

                try { Others2Value = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialBuyerClosingCostBreakDown.Others2Value; }
                catch { Others2Value = 0; }

                try { Others3Name = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialBuyerClosingCostBreakDown.Others3Name; }
                catch { Others3Name = string.Empty; }

                try { Others3Value = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialBuyerClosingCostBreakDown.Others3Value; }
                catch { Others3Value = 0; }

                try { Others4Name = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialBuyerClosingCostBreakDown.Others4Name; }
                catch { Others4Name = string.Empty; }

                try { Others4Value = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialBuyerClosingCostBreakDown.Others4Value; }
                catch { Others4Value = 0; }

                try { Others5Name = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialBuyerClosingCostBreakDown.Others5Name; }
                catch { Others5Name = string.Empty; }

                try { Others5Value = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialBuyerClosingCostBreakDown.Others5Value; }
                catch { Others5Value = 0; }

                try { Others6Name = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialBuyerClosingCostBreakDown.Others6Name; }
                catch { Others6Name = string.Empty; }

                try { Others6Value = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialBuyerClosingCostBreakDown.Others6Value; }
                catch { Others6Value = 0; }


                hud.SetBuyerClosingCostValues(
                    EscrowFeeValue,
                    TitleInsuranceValue,
                    AppraisalFeeValue,
                    CreditReportValue,
                    RecordingFeeValue,
                    "HOA Transferr Fee",
                    HoaTransferFeeValue,
                    Others1Name,
                    Others1Value,
                    Others2Name,
                    Others2Value,
                    Others3Name,
                    Others3Value,
                    Others4Name,
                    Others4Value,
                    Others5Name,
                    Others5Value,
                    Others6Name,
                    Others6Value);

                try { EscrowFeeValue = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialSellerClosingCostBreakDown.EscrowFeeValue; }
                catch { EscrowFeeValue = 0; }

                try { TitleInsuranceValue = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialSellerClosingCostBreakDown.TitleInsuranceValue; }
                catch { TitleInsuranceValue = 0; }

                try { AppraisalFeeValue = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialSellerClosingCostBreakDown.AppraisalFeeValue; }
                catch { AppraisalFeeValue = 0; }

                try { CreditReportValue = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialSellerClosingCostBreakDown.CreditReportValue; }
                catch { CreditReportValue = 0; }

                try { RecordingFeeValue = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialSellerClosingCostBreakDown.RecordingFeeValue; }
                catch { RecordingFeeValue = 0; }

                try { HoaTransferFeeValue = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialSellerClosingCostBreakDown.HoaTransferFeeValue; }
                catch { HoaTransferFeeValue = 0; }

                try { Others1Name = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialSellerClosingCostBreakDown.Others1Name; }
                catch { Others1Name = string.Empty; }

                try { Others1Value = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialSellerClosingCostBreakDown.Others1Value; }
                catch { Others1Value = 0; }

                try { Others2Name = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialSellerClosingCostBreakDown.Others2Name; }
                catch { Others2Name = string.Empty; }

                try { Others2Value = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialSellerClosingCostBreakDown.Others2Value; }
                catch { Others2Value = 0; }

                try { Others3Name = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialSellerClosingCostBreakDown.Others3Name; }
                catch { Others3Name = string.Empty; }

                try { Others3Value = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialSellerClosingCostBreakDown.Others3Value; }
                catch { Others3Value = 0; }

                try { Others4Name = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialSellerClosingCostBreakDown.Others4Name; }
                catch { Others4Name = string.Empty; }

                try { Others4Value = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialSellerClosingCostBreakDown.Others4Value; }
                catch { Others4Value = 0; }

                try { Others5Name = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialSellerClosingCostBreakDown.Others5Name; }
                catch { Others5Name = string.Empty; }

                try { Others5Value = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialSellerClosingCostBreakDown.Others5Value; }
                catch { Others5Value = 0; }

                try { Others6Name = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialSellerClosingCostBreakDown.Others6Name; }
                catch { Others6Name = string.Empty; }

                try { Others6Value = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialSellerClosingCostBreakDown.Others6Value; }
                catch { Others6Value = 0; }

                hud.SetSellerClosingCostValues(
                    EscrowFeeValue,
                    TitleInsuranceValue,
                    AppraisalFeeValue,
                    CreditReportValue,
                    RecordingFeeValue,
                    "HOA Transferr Fee",
                    HoaTransferFeeValue,                    
                    Others1Name,
                    Others1Value,
                    Others2Name,
                    Others2Value,
                    Others3Name,
                    Others3Value,
                    Others4Name,
                    Others4Value,
                    Others5Name,
                    Others5Value,
                    Others6Name,
                    Others6Value);
            }
            
            hud.Show();
            hud.On_PrintDocuments_Click(null, null);
		}

		void UpdateDefaultInfoValues()
		{
			DefaultInfoValues div = new DefaultInfoValues();
			div.AgentInfo = new DefaultInfoValuesAgentInfo();
			div.BuyerInfo = new DefaultInfoValuesBuyerInfo();
			
			div.AgentInfo.Address =  abmf_Address;
			div.AgentInfo.Email = abmf_Email;
			div.AgentInfo.Fax = abmf_Fax;
			div.AgentInfo.FirstName = abmf_FirstName;
			div.AgentInfo.FullName = abmf_FullName;
			div.AgentInfo.LastName = abmf_LastName;
			div.AgentInfo.Phone = abmf_Phone;

			div.BuyerInfo.Address = bmf_Address;
			div.BuyerInfo.Email = bmf_Email;
			div.BuyerInfo.Fax = bmf_Fax;
			div.BuyerInfo.FirstName = bmf_FirstName;
			div.BuyerInfo.FullName = bmf_FullName;
			div.BuyerInfo.LastName = bmf_LastName;
			div.BuyerInfo.Phone = bmf_Phone;

			string err = string.Empty;
            if (!DataManager.Instance.SaveDefaultInfoValues(div, out err))
                MessageBox.Show(err, StringStore.AppName);
            else            
                MessageBox.Show("Agent & Buyer Default Merge Values successfully saved!", StringStore.AppName, MessageBoxButtons.OK, MessageBoxIcon.Information);            
		}

		private void On_printOwnerInfoLabels_Click(object sender, EventArgs e)
		{			
			PrintDialog pd = new PrintDialog();

			PrintDocument tmpprndoc = new PrintDocument();
			tmpprndoc.PrintPage += new PrintPageEventHandler(On_OwnerInfoLabels_PrintPage);

			pd.Document = tmpprndoc;
			pd.Document.DefaultPageSettings.Landscape = false;

			PrintPreviewDialogEx tmpprdiag = new PrintPreviewDialogEx();
				
			tmpprdiag.Document = tmpprndoc;
			tmpprdiag.ShowDialog();			
		}

		private void On_Lien_PrintPage_Click(object sender, EventArgs e)
		{
			LienSet ls = GetSelectedLienSet();
			if (LienSet.NotSelected == ls)
			{
                MessageBox.Show(UnselectedLienMsg, StringStore.AppName, MessageBoxButtons.OK, MessageBoxIcon.Information);			
				return;
			}
			else if (LienSet.FirstLienSet == ls)
				OnPrintPageClick(1);
			else if (LienSet.SecondLienSet == ls)
				OnPrintPageClick(2);
			if (LienSet.ThirdLienSet == ls)
				OnPrintPageClick(3);			
		}			

		void OnPrintPageClick(int lienNr)
		{
			_currentPrintingLien = lienNr;

			PrintDialog pd = new PrintDialog();

			PrintDocument tmpprndoc = new PrintDocument();
			tmpprndoc.PrintPage += new PrintPageEventHandler(On_PrintPage);			

			pd.Document = tmpprndoc;
			pd.Document.DefaultPageSettings.Landscape = false;

			PrintPreviewDialogEx tmpprdiag = new PrintPreviewDialogEx();
				
			tmpprdiag.Document = tmpprndoc;
			tmpprdiag.ShowDialog();			
		}
		
		private void On_PrintPage(object sender, PrintPageEventArgs e)
		{
						
			Font font = new Font("Tahoma", 12);
			Font font_B = new Font("Tahoma", 12, FontStyle.Bold);
			Font font_BU = new Font("Tahoma", 12, FontStyle.Bold | FontStyle.Underline);

            Font font_10 = new Font("Tahoma", 10);
            Font font_10B = new Font("Tahoma", 10, FontStyle.Bold);

			Graphics gfx = e.Graphics;			

			Brush brush = new SolidBrush(Color.Black);				

			int labelCnt  = 80;						
			int iVerticalDistance = 50;
			int rowCnt = 0;
						
			for (int i = 0; i < labelCnt; i++)
			{
				string acctNr = string.Empty;
				try 
				{ 
					if (DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien.Length >= _currentPrintingLien)
						acctNr = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[_currentPrintingLien - 1].AccountNr; 
				}
				catch { acctNr = string.Empty; }

				string prefix = string.Empty;				 
				switch (_currentPrintingLien)
				{
					case 1:
						prefix = _1stLien_FaxLabelLine1.Text;
						break;							

					default:
						prefix = string.Empty;
						break;
				}
									
				rowCnt = (int)(i / 4);
                gfx.DrawString(prefix, font_B, brush, new Point(GetHorizontalOffset(i % 4) + DealMaker.ShortSale.Utils.OffsetData.Instance.HorizontalOffset, (rowCnt % 20) * iVerticalDistance + 50 + DealMaker.ShortSale.Utils.OffsetData.Instance.VerticalOffset));
                gfx.DrawString("PG" + (i + 1).ToString(), font_BU, brush, new Point(GetHorizontalOffset(i % 4) + 170 + DealMaker.ShortSale.Utils.OffsetData.Instance.HorizontalOffset, (rowCnt % 20) * iVerticalDistance + 50 + DealMaker.ShortSale.Utils.OffsetData.Instance.VerticalOffset), new StringFormat(StringFormatFlags.DirectionRightToLeft));
				
				if (string.Empty != acctNr)
				{
                    gfx.DrawString("Acct# ", font_10B, brush, new Point(GetHorizontalOffset(i % 4) + DealMaker.ShortSale.Utils.OffsetData.Instance.HorizontalOffset, (rowCnt % 20) * iVerticalDistance + 68 + DealMaker.ShortSale.Utils.OffsetData.Instance.VerticalOffset));
                    gfx.DrawString(acctNr, font_10, brush, new Point(GetHorizontalOffset(i % 4) + 45 + DealMaker.ShortSale.Utils.OffsetData.Instance.HorizontalOffset, (rowCnt % 20) * iVerticalDistance + 68 + DealMaker.ShortSale.Utils.OffsetData.Instance.VerticalOffset));
				}
			}
		}

		int GetHorizontalOffset (int columnNr)
		{
			switch (columnNr)
			{
				case 0:
					return 50;					
				case 1:
					return 250;					
				case 2:
					return 450;					
				case 3:
					return 650;
				default:
					return 50;
			}
		}
		
		private void On_PictureBoxDocumentControl_MouseHover(object sender, EventArgs e)
		{
            //Help_DocumentGeneration help = new Help_DocumentGeneration();
            //help.ShowDialog();
		}		

		private void On_OwnerInfoLabels_PrintPage(object sender, PrintPageEventArgs e)
		{
			Graphics gfx = e.Graphics;			
			Font font = new Font("Tahoma", 9);									
			Brush brush = new SolidBrush(Color.Black);				

			int labelCnt  = 80;						
			int iVerticalDistance = 50;
			int rowCnt = 0;
						
			for (int i = 0; i < labelCnt; i++)
			{													
				rowCnt = (int)(i / 4);
                gfx.DrawString((null != DataManager.Instance.HomeOwnerFullName /*.GetCurrentShortSaleDocument().ContactInfo.FullName*/) ? DataManager.Instance.HomeOwnerFullName.ToUpper() : string.Empty, font, brush, new Point(GetHorizontalOffset(i % 4) + DealMaker.ShortSale.Utils.OffsetData.Instance.HorizontalOffset, (rowCnt % 20) * iVerticalDistance + 50 + DealMaker.ShortSale.Utils.OffsetData.Instance.VerticalOffset));
                gfx.DrawString(DataManager.Instance.PropertyPlainAddress.ToUpper() /*.GetSiteAddress().ToUpper()*/, font, brush, new Point(GetHorizontalOffset(i % 4) + DealMaker.ShortSale.Utils.OffsetData.Instance.HorizontalOffset, (rowCnt % 20) * iVerticalDistance + 68 + DealMaker.ShortSale.Utils.OffsetData.Instance.VerticalOffset));
                gfx.DrawString(DataManager.Instance.PropertyCityStateZIP.ToUpper() /*.GetCityStateZIPFromAddress().ToUpper()*/, font, brush, new Point(GetHorizontalOffset(i % 4) + DealMaker.ShortSale.Utils.OffsetData.Instance.HorizontalOffset, (rowCnt % 20) * iVerticalDistance + 80 + DealMaker.ShortSale.Utils.OffsetData.Instance.VerticalOffset));									
			}
		}

        int iPageCnt = 0;
        void On_Print_COMPS(object sender, PrintPageEventArgs e)
        {
            object obj =  CompsManager.Instance.P_comps;

            //LOAD FROM XML
            DTD.Response.RESPONSE_GROUP response;
            DTD.Response._DATA_PROVIDER_COMPARABLE_SALES[] comps = null;
            DealMaker.PlainClasses.OnLineData.COMPS.comps[] dbComps = null;

            dbComps = new DealMaker.PlainClasses.OnLineData.COMPS.comps[1] { CompsManager.Instance.P_comps };

            if (null == dbComps[0].P_xmlStream)
                return;
						
			MemoryStream stream = new MemoryStream(dbComps[0].P_xmlStream);
			XmlSerializer serializer = new XmlSerializer(typeof(DTD.Response.RESPONSE_GROUP));
			response = (DTD.Response.RESPONSE_GROUP)serializer.Deserialize(stream);
            
            
            DTD.Response._PROPERTY_INFORMATION propInfo = null;
            foreach (object o in response.RESPONSE.RESPONSE_DATA[0].PROPERTY_INFORMATION_RESPONSE.Items)
            {
                if (o is DTD.Response._PROPERTY_INFORMATION)
                {
                    propInfo = o as DTD.Response._PROPERTY_INFORMATION;
                    break;
                }
            }

            if (null != propInfo)
                comps = propInfo._DATA_PROVIDER_COMPARABLE_SALES;

            string conservativeIndices = CompsManager.Instance.P_comps.P_taggedConservativeCSV;
            string[] splittedConservativeIndices = conservativeIndices.Split(',');

            int[] conIndices = new int[splittedConservativeIndices.Length];
            for (int i = 0; i < splittedConservativeIndices.Length; i++)
            {
                int temp = 0;
                try 
                { 
                    temp = Convert.ToInt32(splittedConservativeIndices[i]);
                    conIndices[i] = temp;
                }
                catch { temp = 0; }
            }

            List<DTD.Response.PROPERTY> selectedProperties = new List<DTD.Response.PROPERTY>();
            foreach (int i in conIndices)
            {
                selectedProperties.Add(comps[i].PROPERTY);
            }

            string compsAddress = CompsManager.Instance.GetCOMPSAddress();


            COMPSReportGenerator.PrintMultipleDataReport(e, "Comparative Market Analysis (CMA) Report", selectedProperties.ToArray() , iPageCnt++, string.Empty, compsAddress,
                CompsManager.Instance.P_comps.Sqft, string.Empty, CompsManager.Instance.P_comps.SqFtRange, CompsManager.Instance.P_comps.AllSalesInLast, CompsManager.Instance.P_comps.SearchRadius, CompsManager.Instance.P_comps.P_ConservativeValue.ToString("c"), selectedProperties.Count.ToString());
        }

        void On_EndPrint_COMPS(object sender, PrintEventArgs e)
        {
            iPageCnt = 0;           
        }

        void On_RepairsEstimate_Click(object sender, EventArgs e)
        {
            if (LienSet.NotSelected == GetSelectedLienSet())
            {
                MessageBox.Show(UnselectedLienMsg, StringStore.AppName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            PrintDialog pd = new PrintDialog();
            pd.UseEXDialog = true;

            PrintDocument tmpprndoc = new PrintDocument();
            tmpprndoc.PrintPage += new PrintPageEventHandler(On_PrintRepairs);
            tmpprndoc.EndPrint += new PrintEventHandler(On_EndPrint);

            pd.Document = tmpprndoc;
            pd.Document.DefaultPageSettings.Landscape = false;

            PrintPreviewDialogEx tmpprdiag = new PrintPreviewDialogEx();            

            tmpprdiag.Document = tmpprndoc;
            tmpprdiag.Show();
            tmpprdiag.BringToFront();    
        }

        void On_Comps_Click(object sender, EventArgs e)
        {
            //CHECK FOR CONSERVATIVE VALUE
            if (null == CompsManager.Instance.P_comps || 0 == CompsManager.Instance.P_comps.P_ConservativeValue)
            {                
                MessageBox.Show("You don�t have this report yet!  Go back to Evaluate � COMPS tab and calculate the Conservative Value first.", "CMA (Conservative COMPS) Analysis", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            PrintDialog pd = new PrintDialog();
            pd.UseEXDialog = true;

            PrintDocument tmpprndoc = new PrintDocument();
            tmpprndoc.PrintPage += new PrintPageEventHandler(On_Print_COMPS);
            tmpprndoc.EndPrint += new PrintEventHandler(On_EndPrint_COMPS);

            pd.Document = tmpprndoc;
            pd.Document.DefaultPageSettings.Landscape = false;

            PrintPreviewDialogEx tmpprdiag = new PrintPreviewDialogEx();

            tmpprdiag.Document = tmpprndoc;
            tmpprdiag.Show();
            tmpprdiag.BringToFront();    
        }

        void On_CustomHUD_Click(object sender, EventArgs e)
        {
            LienSet ls = GetSelectedLienSet();
            if (LienSet.NotSelected == ls)
            {
                MessageBox.Show(UnselectedLienMsg, StringStore.AppName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            HUD hud = new HUD();

            hud.EnableSave();
            hud.CustomMode = true;

            hud.ApplicationPath = DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.ArchiveFolder;
            hud.ConfigPath = DataManager.Instance.UserAppFolder;
            hud.SetValues(_bmf_FullName.Text, _bmf_Address.Text, _po_FullName.Text, _po_MailingAddress.Text, DataManager.Instance.GetFullSiteAddress());

            string hudFileName = string.Empty;
            bool existingHUDFileName = HUDFileNameManager.Instance.GetCreateDocument(Util.GetStartupPath(), DataManager.Instance.DocumentName, (int)ls, out hudFileName);

            if (LienSet.FirstLienSet == GetSelectedLienSet())
            {
                //set fileName
                string path = Util.GetStartupPath() + @"\" + StringStore.SavedExtDocumentsFolder;
                string suffix = StringStore.HUDSubFolder;


                hud.SetFileName(path, suffix, path + suffix + hudFileName);
                if (existingHUDFileName)
                    hud.ForceLoad(path + suffix + hudFileName);

                hud.SetCash();

                try
                {
                    string accountNr = string.Empty;
                    try { accountNr = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[0].AccountNr; }
                    catch { accountNr = string.Empty; }
                    hud.SetLoanNumber(accountNr);
                }
                catch { }

                try
                {
                    decimal initialOfferSalesPrice = 0;
                    decimal comission = 0;

                    try { initialOfferSalesPrice = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialOfferSalesPriceValue; }
                    catch { initialOfferSalesPrice = 0; }

                    try { comission = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialRealEstateComissionPerc; }
                    catch { comission = 0; }

                    hud.SetSalesPriceDetails(initialOfferSalesPrice, comission);

                }
                catch { }

                try
                {
                    string firstCompanyName = string.Empty;
                    string secondCompanyName = string.Empty;
                    decimal initialOfferNetToFirst = 0;
                    decimal initialOfferNetToSecond = 0;

                    try { firstCompanyName = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[0].CompanyName; }
                    catch { firstCompanyName = string.Empty; }

                    try { initialOfferNetToFirst = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialOfferToFirstLender; }
                    catch { initialOfferNetToFirst = 0; }

                    try { secondCompanyName = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[1].CompanyName; }
                    catch { secondCompanyName = string.Empty; }

                    try { initialOfferNetToSecond = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[0].InitialOfferToSecondLender; }
                    catch { initialOfferNetToSecond = 0; }

                    hud.SetCompanyDetails(firstCompanyName, initialOfferNetToFirst, secondCompanyName, initialOfferNetToSecond);
                }
                catch { }

                //set initial offer sales price
                try
                {
                    decimal initOfferSalesPrice = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialOfferSalesPriceValue;
                    hud.SetIntialOfferSalesPrice(initOfferSalesPrice);
                }
                catch { }

                string nameOfListingAgency = string.Empty;
                try { nameOfListingAgency = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialRealEstateCommisionBreakDown.NameOfListingAgency; }
                catch { nameOfListingAgency = string.Empty; }

                string nameOfSellingAgency = "";
                try { nameOfSellingAgency = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialRealEstateCommisionBreakDown.NameOfSellingAgency; }
                catch { nameOfSellingAgency = string.Empty; }

                decimal listingAgencyValue = 0;
                try { listingAgencyValue = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialRealEstateCommisionBreakDown.ListingAgencyValue * CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialOfferSalesPriceValue / 100; }
                catch { listingAgencyValue = 0; }

                decimal sellingAgencyValue = 0;
                try { sellingAgencyValue = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialRealEstateCommisionBreakDown.SellingAgencyValue * CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialOfferSalesPriceValue / 100; }
                catch { sellingAgencyValue = 0; }

                hud.SetRealEstateCommissionValues(nameOfListingAgency, nameOfSellingAgency, listingAgencyValue, sellingAgencyValue);

                decimal loanOriginationFee = 0;
                try { loanOriginationFee = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialSellerConcessionBreakDown.LoanOriginationFeeValue * CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialOfferSalesPriceValue / 100; }
                catch { loanOriginationFee = 0; }

                decimal loanDiscountFee = 0;
                try { loanDiscountFee = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialSellerConcessionBreakDown.LoanDiscountFeeValue * CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialOfferSalesPriceValue / 100; }
                catch { loanDiscountFee = 0; }

                hud.SetSellerConcessionValues(loanOriginationFee, loanDiscountFee);

                decimal EscrowFeeValue = 0;
                decimal TitleInsuranceValue = 0;
                decimal AppraisalFeeValue = 0;
                decimal CreditReportValue = 0;
                decimal RecordingFeeValue = 0;
                decimal HoaTransferFeeValue = 0;
                string Others1Name = string.Empty;
                decimal Others1Value = 0;
                string Others2Name = string.Empty;
                decimal Others2Value = 0;
                string Others3Name = string.Empty;
                decimal Others3Value = 0;
                string Others4Name = string.Empty;
                decimal Others4Value = 0;
                string Others5Name = string.Empty;
                decimal Others5Value = 0;
                string Others6Name = string.Empty;
                decimal Others6Value = 0;

                try { EscrowFeeValue = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialBuyerClosingCostBreakDown.EscrowFeeValue; }
                catch { EscrowFeeValue = 0; }

                try { TitleInsuranceValue = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialBuyerClosingCostBreakDown.TitleInsuranceValue; }
                catch { TitleInsuranceValue = 0; }

                try { AppraisalFeeValue = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialBuyerClosingCostBreakDown.AppraisalFeeValue; }
                catch { AppraisalFeeValue = 0; }

                try { CreditReportValue = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialBuyerClosingCostBreakDown.CreditReportValue; }
                catch { CreditReportValue = 0; }

                try { RecordingFeeValue = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialBuyerClosingCostBreakDown.RecordingFeeValue; }
                catch { RecordingFeeValue = 0; }

                try { HoaTransferFeeValue = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialBuyerClosingCostBreakDown.HoaTransferFeeValue; }
                catch { HoaTransferFeeValue = 0; }

                try { Others1Name = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialBuyerClosingCostBreakDown.Others1Name; }
                catch { Others1Name = string.Empty; }

                try { Others1Value = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialBuyerClosingCostBreakDown.Others1Value; }
                catch { Others1Value = 0; }

                try { Others2Name = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialBuyerClosingCostBreakDown.Others2Name; }
                catch { Others2Name = string.Empty; }

                try { Others2Value = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialBuyerClosingCostBreakDown.Others2Value; }
                catch { Others2Value = 0; }

                try { Others3Name = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialBuyerClosingCostBreakDown.Others3Name; }
                catch { Others3Name = string.Empty; }

                try { Others3Value = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialBuyerClosingCostBreakDown.Others3Value; }
                catch { Others3Value = 0; }

                try { Others4Name = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialBuyerClosingCostBreakDown.Others4Name; }
                catch { Others4Name = string.Empty; }

                try { Others4Value = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialBuyerClosingCostBreakDown.Others4Value; }
                catch { Others4Value = 0; }

                try { Others5Name = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialBuyerClosingCostBreakDown.Others5Name; }
                catch { Others5Name = string.Empty; }

                try { Others5Value = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialBuyerClosingCostBreakDown.Others5Value; }
                catch { Others5Value = 0; }

                try { Others6Name = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialBuyerClosingCostBreakDown.Others6Name; }
                catch { Others6Name = string.Empty; }

                try { Others6Value = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialBuyerClosingCostBreakDown.Others6Value; }
                catch { Others6Value = 0; }


                hud.SetBuyerClosingCostValues(
                    EscrowFeeValue,
                    TitleInsuranceValue,
                    AppraisalFeeValue,
                    CreditReportValue,
                    RecordingFeeValue,
                    "HOA Transferr Fee",
                    HoaTransferFeeValue,
                    Others1Name,
                    Others1Value,
                    Others2Name,
                    Others2Value,
                    Others3Name,
                    Others3Value,
                    Others4Name,
                    Others4Value,
                    Others5Name,
                    Others5Value,
                    Others6Name,
                    Others6Value);

                try { EscrowFeeValue = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialSellerClosingCostBreakDown.EscrowFeeValue; }
                catch { EscrowFeeValue = 0; }

                try { TitleInsuranceValue = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialSellerClosingCostBreakDown.TitleInsuranceValue; }
                catch { TitleInsuranceValue = 0; }

                try { AppraisalFeeValue = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialSellerClosingCostBreakDown.AppraisalFeeValue; }
                catch { AppraisalFeeValue = 0; }

                try { CreditReportValue = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialSellerClosingCostBreakDown.CreditReportValue; }
                catch { CreditReportValue = 0; }

                try { RecordingFeeValue = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialSellerClosingCostBreakDown.RecordingFeeValue; }
                catch { RecordingFeeValue = 0; }

                try { HoaTransferFeeValue = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialSellerClosingCostBreakDown.HoaTransferFeeValue; }
                catch { HoaTransferFeeValue = 0; }

                try { Others1Name = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialSellerClosingCostBreakDown.Others1Name; }
                catch { Others1Name = string.Empty; }

                try { Others1Value = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialSellerClosingCostBreakDown.Others1Value; }
                catch { Others1Value = 0; }

                try { Others2Name = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialSellerClosingCostBreakDown.Others2Name; }
                catch { Others2Name = string.Empty; }

                try { Others2Value = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialSellerClosingCostBreakDown.Others2Value; }
                catch { Others2Value = 0; }

                try { Others3Name = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialSellerClosingCostBreakDown.Others3Name; }
                catch { Others3Name = string.Empty; }

                try { Others3Value = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialSellerClosingCostBreakDown.Others3Value; }
                catch { Others3Value = 0; }

                try { Others4Name = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialSellerClosingCostBreakDown.Others4Name; }
                catch { Others4Name = string.Empty; }

                try { Others4Value = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialSellerClosingCostBreakDown.Others4Value; }
                catch { Others4Value = 0; }

                try { Others5Name = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialSellerClosingCostBreakDown.Others5Name; }
                catch { Others5Name = string.Empty; }

                try { Others5Value = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialSellerClosingCostBreakDown.Others5Value; }
                catch { Others5Value = 0; }

                try { Others6Name = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialSellerClosingCostBreakDown.Others6Name; }
                catch { Others6Name = string.Empty; }

                try { Others6Value = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialSellerClosingCostBreakDown.Others6Value; }
                catch { Others6Value = 0; }

                hud.SetSellerClosingCostValues(
                    EscrowFeeValue,
                    TitleInsuranceValue,
                    AppraisalFeeValue,
                    CreditReportValue,
                    RecordingFeeValue,
                    "HOA Transferr Fee",
                    HoaTransferFeeValue,
                    Others1Name,
                    Others1Value,
                    Others2Name,
                    Others2Value,
                    Others3Name,
                    Others3Value,
                    Others4Name,
                    Others4Value,
                    Others5Name,
                    Others5Value,
                    Others6Name,
                    Others6Value);
            }
            else
            {
                int _lienIdx = (int)GetSelectedLienSet() - 1;

                //set fileName
                string path = Util.GetStartupPath() + @"\" + StringStore.SavedExtDocumentsFolder;
                string suffix = StringStore.HUDSubFolder;

                hud.SetFileName(path, suffix, path + suffix + hudFileName);
                if (existingHUDFileName)
                    hud.ForceLoad(path + suffix + hudFileName);

                hud.SetCash();

                try
                {
                    string accountNr = string.Empty;
                    try { accountNr = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[_lienIdx].AccountNr; }
                    catch { accountNr = string.Empty; }
                    hud.SetLoanNumber(accountNr);
                }
                catch { }

                try
                {
                    decimal initialOfferSalesPrice = 0;
                    decimal comission = 0;

                    try { initialOfferSalesPrice = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialOfferSalesPriceValue; }
                    catch { initialOfferSalesPrice = 0; }

                    try { comission = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialRealEstateComissionPerc; }
                    catch { comission = 0; }

                    hud.SetSalesPriceDetails(initialOfferSalesPrice, comission);

                }
                catch { }

                try
                {
                    string firstCompanyName = string.Empty;
                    string secondCompanyName = string.Empty;
                    decimal initialOfferNetToFirst = 0;
                    decimal initialOfferNetToSecond = 0;

                    try { firstCompanyName = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[0].CompanyName; }
                    catch { firstCompanyName = string.Empty; }

                    try { initialOfferNetToFirst = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialOfferToFirstLender; }
                    catch { initialOfferNetToFirst = 0; }

                    try { secondCompanyName = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[1].CompanyName; }
                    catch { secondCompanyName = string.Empty; }

                    try { initialOfferNetToSecond = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[0].InitialOfferToSecondLender; }
                    catch { initialOfferNetToSecond = 0; }

                    hud.SetCompanyDetails(firstCompanyName, initialOfferNetToFirst, secondCompanyName, initialOfferNetToSecond);
                }
                catch { }

                //set initial offer sales price
                try
                {
                    decimal initOfferSalesPrice = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialOfferSalesPriceValue;
                    hud.SetIntialOfferSalesPrice(initOfferSalesPrice);
                }
                catch { }

                string nameOfListingAgency = string.Empty;
                try { nameOfListingAgency = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialRealEstateCommisionBreakDown.NameOfListingAgency; }
                catch { nameOfListingAgency = string.Empty; }

                string nameOfSellingAgency = "";
                try { nameOfSellingAgency = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialRealEstateCommisionBreakDown.NameOfSellingAgency; }
                catch { nameOfSellingAgency = string.Empty; }

                decimal listingAgencyValue = 0;
                try { listingAgencyValue = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialRealEstateCommisionBreakDown.ListingAgencyValue * CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialOfferSalesPriceValue / 100; }
                catch { listingAgencyValue = 0; }

                decimal sellingAgencyValue = 0;
                try { sellingAgencyValue = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialRealEstateCommisionBreakDown.SellingAgencyValue * CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialOfferSalesPriceValue / 100; }
                catch { sellingAgencyValue = 0; }

                hud.SetRealEstateCommissionValues(nameOfListingAgency, nameOfSellingAgency, listingAgencyValue, sellingAgencyValue);

                decimal loanOriginationFee = 0;
                try { loanOriginationFee = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialSellerConcessionBreakDown.LoanOriginationFeeValue * CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialOfferSalesPriceValue / 100; }
                catch { loanOriginationFee = 0; }

                decimal loanDiscountFee = 0;
                try { loanDiscountFee = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialSellerConcessionBreakDown.LoanDiscountFeeValue * CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialOfferSalesPriceValue / 100; }
                catch { loanDiscountFee = 0; }

                hud.SetSellerConcessionValues(loanOriginationFee, loanDiscountFee);

                decimal EscrowFeeValue = 0;
                decimal TitleInsuranceValue = 0;
                decimal AppraisalFeeValue = 0;
                decimal CreditReportValue = 0;
                decimal RecordingFeeValue = 0;
                decimal HoaTransferFeeValue = 0;
                string Others1Name = string.Empty;
                decimal Others1Value = 0;
                string Others2Name = string.Empty;
                decimal Others2Value = 0;
                string Others3Name = string.Empty;
                decimal Others3Value = 0;
                string Others4Name = string.Empty;
                decimal Others4Value = 0;
                string Others5Name = string.Empty;
                decimal Others5Value = 0;
                string Others6Name = string.Empty;
                decimal Others6Value = 0;

                try { EscrowFeeValue = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialBuyerClosingCostBreakDown.EscrowFeeValue; }
                catch { EscrowFeeValue = 0; }

                try { TitleInsuranceValue = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialBuyerClosingCostBreakDown.TitleInsuranceValue; }
                catch { TitleInsuranceValue = 0; }

                try { AppraisalFeeValue = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialBuyerClosingCostBreakDown.AppraisalFeeValue; }
                catch { AppraisalFeeValue = 0; }

                try { CreditReportValue = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialBuyerClosingCostBreakDown.CreditReportValue; }
                catch { CreditReportValue = 0; }

                try { RecordingFeeValue = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialBuyerClosingCostBreakDown.RecordingFeeValue; }
                catch { RecordingFeeValue = 0; }

                try { HoaTransferFeeValue = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialBuyerClosingCostBreakDown.HoaTransferFeeValue; }
                catch { HoaTransferFeeValue = 0; }

                try { Others1Name = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialBuyerClosingCostBreakDown.Others1Name; }
                catch { Others1Name = string.Empty; }

                try { Others1Value = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialBuyerClosingCostBreakDown.Others1Value; }
                catch { Others1Value = 0; }

                try { Others2Name = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialBuyerClosingCostBreakDown.Others2Name; }
                catch { Others2Name = string.Empty; }

                try { Others2Value = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialBuyerClosingCostBreakDown.Others2Value; }
                catch { Others2Value = 0; }

                try { Others3Name = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialBuyerClosingCostBreakDown.Others3Name; }
                catch { Others3Name = string.Empty; }

                try { Others3Value = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialBuyerClosingCostBreakDown.Others3Value; }
                catch { Others3Value = 0; }

                try { Others4Name = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialBuyerClosingCostBreakDown.Others4Name; }
                catch { Others4Name = string.Empty; }

                try { Others4Value = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialBuyerClosingCostBreakDown.Others4Value; }
                catch { Others4Value = 0; }

                try { Others5Name = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialBuyerClosingCostBreakDown.Others5Name; }
                catch { Others5Name = string.Empty; }

                try { Others5Value = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialBuyerClosingCostBreakDown.Others5Value; }
                catch { Others5Value = 0; }

                try { Others6Name = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialBuyerClosingCostBreakDown.Others6Name; }
                catch { Others6Name = string.Empty; }

                try { Others6Value = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialBuyerClosingCostBreakDown.Others6Value; }
                catch { Others6Value = 0; }


                hud.SetBuyerClosingCostValues(
                    EscrowFeeValue,
                    TitleInsuranceValue,
                    AppraisalFeeValue,
                    CreditReportValue,
                    RecordingFeeValue,
                    "HOA Transferr Fee",
                    HoaTransferFeeValue,
                    Others1Name,
                    Others1Value,
                    Others2Name,
                    Others2Value,
                    Others3Name,
                    Others3Value,
                    Others4Name,
                    Others4Value,
                    Others5Name,
                    Others5Value,
                    Others6Name,
                    Others6Value);

                try { EscrowFeeValue = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialSellerClosingCostBreakDown.EscrowFeeValue; }
                catch { EscrowFeeValue = 0; }

                try { TitleInsuranceValue = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialSellerClosingCostBreakDown.TitleInsuranceValue; }
                catch { TitleInsuranceValue = 0; }

                try { AppraisalFeeValue = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialSellerClosingCostBreakDown.AppraisalFeeValue; }
                catch { AppraisalFeeValue = 0; }

                try { CreditReportValue = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialSellerClosingCostBreakDown.CreditReportValue; }
                catch { CreditReportValue = 0; }

                try { RecordingFeeValue = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialSellerClosingCostBreakDown.RecordingFeeValue; }
                catch { RecordingFeeValue = 0; }

                try { HoaTransferFeeValue = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialSellerClosingCostBreakDown.HoaTransferFeeValue; }
                catch { HoaTransferFeeValue = 0; }

                try { Others1Name = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialSellerClosingCostBreakDown.Others1Name; }
                catch { Others1Name = string.Empty; }

                try { Others1Value = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialSellerClosingCostBreakDown.Others1Value; }
                catch { Others1Value = 0; }

                try { Others2Name = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialSellerClosingCostBreakDown.Others2Name; }
                catch { Others2Name = string.Empty; }

                try { Others2Value = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialSellerClosingCostBreakDown.Others2Value; }
                catch { Others2Value = 0; }

                try { Others3Name = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialSellerClosingCostBreakDown.Others3Name; }
                catch { Others3Name = string.Empty; }

                try { Others3Value = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialSellerClosingCostBreakDown.Others3Value; }
                catch { Others3Value = 0; }

                try { Others4Name = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialSellerClosingCostBreakDown.Others4Name; }
                catch { Others4Name = string.Empty; }

                try { Others4Value = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialSellerClosingCostBreakDown.Others4Value; }
                catch { Others4Value = 0; }

                try { Others5Name = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialSellerClosingCostBreakDown.Others5Name; }
                catch { Others5Name = string.Empty; }

                try { Others5Value = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialSellerClosingCostBreakDown.Others5Value; }
                catch { Others5Value = 0; }

                try { Others6Name = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialSellerClosingCostBreakDown.Others6Name; }
                catch { Others6Name = string.Empty; }

                try { Others6Value = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialSellerClosingCostBreakDown.Others6Value; }
                catch { Others6Value = 0; }

                hud.SetSellerClosingCostValues(
                    EscrowFeeValue,
                    TitleInsuranceValue,
                    AppraisalFeeValue,
                    CreditReportValue,
                    RecordingFeeValue,
                    "HOA Transferr Fee",
                    HoaTransferFeeValue,
                    Others1Name,
                    Others1Value,
                    Others2Name,
                    Others2Value,
                    Others3Name,
                    Others3Value,
                    Others4Name,
                    Others4Value,
                    Others5Name,
                    Others5Value,
                    Others6Name,
                    Others6Value);
            }

            hud.Show();
        }                        
        
	}
}
