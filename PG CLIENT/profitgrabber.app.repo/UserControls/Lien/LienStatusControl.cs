﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;
using System.Drawing.Printing;
using System.IO;
using System.Diagnostics;
using System.Collections.Generic;

//Added
using DealMaker.Forms.SchedulingActicities;
using DealMaker.UserControls.ShortSale;
using DealMaker.ShortSale.Interfaces;
using DealMaker.PlainClasses.ShortSale.Adapters;
using DealMaker.ShortSale.Utils;
using ProfitGrabber.Common;


namespace DealMaker.UserControls.Lien
{
    public partial class LienStatusControl : BaseShortSaleUserControl
    {
        #region DATA
        Dictionary<Guid, string> _officeStaffListDict = null;

        Brush _blackBrush = Brushes.Black;
        Font _arial9 = new Font("Arial", 9);
        Font _arial10 = new Font("Arial", 10);
        Font _arial10I = new Font("Arial", 10, FontStyle.Italic);
        Font _arial10B = new Font("Arial", 10, FontStyle.Bold);
        Font _arial10U = new Font("Arial", 10, FontStyle.Underline);
        Font _arial10BU = new Font("Arial", 10, FontStyle.Bold | FontStyle.Underline);
        Font _arial12 = new Font("Arial", 12);
        Font _arial14 = new Font("Arial", 14);
        Font _arial16 = new Font("Arial", 16);
        Font _arial18 = new Font("Arial", 18);
        Font _arial20 = new Font("Arial", 20);
        Font _arial22 = new Font("Arial", 22);
        Font _arial24 = new Font("Arial", 24);
        Font _arial26 = new Font("Arial", 26);
        private System.Windows.Forms.Button _removeLien;

        int _liensRendered = 0;
        bool _updateTextBoxStates = false;

        const string NA = "N/A";
        const string Blank = "### Not Complete ###";
        #endregion

        #region IDisplayControlObject
        IDisplayControlObject _displayControlObject;
        public IDisplayControlObject DisplayControlObject
        {
            get { return _displayControlObject; }
            set { _displayControlObject = value; }
        }
        #endregion

        #region CTR
        public LienStatusControl()
        {
            InitializeComponent();

            _officeStaffListDict = UserInformationManager.Instance.GetValues(UserEntries.OfficeStaffList);

            //InitWhoDropDownBoxes();
            //SetDropDownBoxesToDefault();

            AddEventHandlers();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            InitWhoDropDownBoxes();
            SetDropDownBoxesToDefault();

            _updateTextBoxStates = true;
        }
        #endregion

        #region PROPERTIES
        public string authorizationFaxedIn
        {
            get
            {
                return _authorizationFaxedIn.Text;
            }
            set
            {
                if (_authorizationFaxedIn.Text == value)
                    return;
                _authorizationFaxedIn.Text = value;
            }
        }
        
        public string authorizationVerifiedReceived
        {
            get
            {
                return _authorizationVerifiedReceived.Text;
            }
            set
            {
                if (_authorizationVerifiedReceived.Text == value)
                    return;
                _authorizationVerifiedReceived.Text = value;
            }
        }
        
        public string allIntakeDocsFromSeller
        {
            get
            {
                return _allIntakeDocsFromSeller.Text;
            }
            set
            {
                if (_allIntakeDocsFromSeller.Text == value)
                    return;
                _allIntakeDocsFromSeller.Text = value;
            }
        }
        public string requiredDocsScanned
        {
            get
            {
                return _requiredDocsScanned.Text;
            }
            set
            {
                if (_requiredDocsScanned.Text == value)
                    return;
                _requiredDocsScanned.Text = value;
            }
        }
        public string requiredDocsRecorded
        {
            get
            {
                return _requiredDocsRecorded.Text;
            }
            set
            {
                if (_requiredDocsRecorded.Text == value)
                    return;
                _requiredDocsRecorded.Text = value;
            }
        }
        public string returnOriginalsToHomeOwner
        {
            get
            {
                return _returnOriginalsToHomeOwner.Text;
            }
            set
            {
                if (_returnOriginalsToHomeOwner.Text == value)
                    return;
                _returnOriginalsToHomeOwner.Text = value;
            }
        }
        public string allInfoEnteredInProfitGrabber
        {
            get
            {
                return _allInfoEnteredInProfitGrabber.Text;
            }
            set
            {
                if (_allInfoEnteredInProfitGrabber.Text == value)
                    return;
                _allInfoEnteredInProfitGrabber.Text = value;
            }
        }
        public string propertyProfileInProfitGrabber
        {
            get
            {
                return _propertyProfileInProfitGrabber.Text;
            }
            set
            {
                if (_propertyProfileInProfitGrabber.Text == value)
                    return;
                _propertyProfileInProfitGrabber.Text = value;
            }
        }
        public string preliminaryTitleSearchedOrdered
        {
            get
            {
                return _preliminaryTitleSearchedOrdered.Text;
            }
            set
            {
                if (_preliminaryTitleSearchedOrdered.Text == value)
                    return;
                _preliminaryTitleSearchedOrdered.Text = value;
            }
        }
        public string preliminaryTitleSearchReceived
        {
            get
            {
                return _preliminaryTitleSearchReceived.Text;
            }
            set
            {
                if (_preliminaryTitleSearchReceived.Text == value)
                    return;
                _preliminaryTitleSearchReceived.Text = value;
            }
        }
        public string anyTitleIssues
        {
            get
            {
                return _anyTitleIssues.Text;
            }
            set
            {
                if (_anyTitleIssues.Text == value)
                    return;
                _anyTitleIssues.Text = value;
            }
        }
        public string PayoffOrdered
        {
            get
            {
                return _payoffOrdered.Text;
            }
            set
            {
                if (_payoffOrdered.Text == value)
                    return;
                _payoffOrdered.Text = value;
            }
        }
        public string ReinstatementOrdered
        {
            get
            {
                return _reinstatementOrdered.Text;
            }
            set
            {
                if (_reinstatementOrdered.Text == value)
                    return;
                _reinstatementOrdered.Text = value;
            }
        }
        public string PayoffAmount
        {
            get
            {
                return _payoffAmount.Text;
            }
            set
            {
                if (_payoffAmount.Text == value)
                    return;
                _payoffAmount.Text = value;
            }
        }
        public string ReinstatementAmount
        {
            get
            {
                return _reinstatementAmount.Text;
            }
            set
            {
                if (_reinstatementAmount.Text == value)
                    return;
                _reinstatementAmount.Text = value;
            }
        }
       
        public string ssPackedComplete
        {
            get
            {
                return _ssPackedComplete.Text;
            }
            set
            {
                if (_ssPackedComplete.Text == value)
                    return;
                _ssPackedComplete.Text = value;
            }
        }
        public string ssPacketReviewed
        {
            get
            {
                return _ssPacketReviewed.Text;
            }
            set
            {
                if (_ssPacketReviewed.Text == value)
                    return;
                _ssPacketReviewed.Text = value;
            }
        }
        public string ssPacketSentToLienHolder
        {
            get
            {
                return _ssPacketSentToLienHolder.Text;
            }
            set
            {
                if (_ssPacketSentToLienHolder.Text == value)
                    return;
                _ssPacketSentToLienHolder.Text = value;
            }
        }
        public string ssPacketVerifiedReceived
        {
            get
            {
                return _ssPacketVerifiedReceived.Text;
            }
            set
            {
                if (_ssPacketVerifiedReceived.Text == value)
                    return;
                _ssPacketVerifiedReceived.Text = value;
            }
        }
        
        public string prelimInspectionOrdered
        {
            get
            {
                return _prelimInspectionOrdered.Text;
            }
            set
            {
                if (_prelimInspectionOrdered.Text == value)
                    return;
                _prelimInspectionOrdered.Text = value;
            }
        }
        public string prelimInspectionComplete
        {
            get
            {
                return _prelimInspectionDate.Text;
            }
            set
            {
                if (_prelimInspectionDate.Text == value)
                    return;
                _prelimInspectionDate.Text = value;
            }
        }
        public string bpoPacketComplete
        {
            get
            {
                return _bpoPacketComplete.Text;
            }
            set
            {
                if (_bpoPacketComplete.Text == value)
                    return;
                _bpoPacketComplete.Text = value;
            }
        }
        public string negotiatorAssigned
        {
            get
            {
                return _negotiatorAssigned.Text;
            }
            set
            {
                if (_negotiatorAssigned.Text == value)
                    return;
                _negotiatorAssigned.Text = value;
            }
        }
        
        public string bpoAppraisalOrdered
        {
            get
            {
                return _bpoAppraisalOrdered.Text;
            }
            set
            {
                if (_bpoAppraisalOrdered.Text == value)
                    return;
                _bpoAppraisalOrdered.Text = value;
            }
        }
        public string bpoAppraisalComplete
        {
            get
            {
                return _bpoAppraisalCompleted.Text;
            }
            set
            {
                if (_bpoAppraisalCompleted.Text == value)
                    return;
                _bpoAppraisalCompleted.Text = value;
            }
        }

        public string bpoAppraisalValue
        {
            get
            {
                return _bpoAppraisalValue.Text;
            }
            set
            {
                if (_bpoAppraisalValue.Text == value)
                    return;
                _bpoAppraisalValue.Text = value;
            }
        }
        public string repairListSentToNegotiator
        {
            get
            {
                return _repairListSentToNegotiator.Text;
            }
            set
            {
                if (_repairListSentToNegotiator.Text == value)
                    return;
                _repairListSentToNegotiator.Text = value;
            }
        }
        public string marketDataSentToNegotiator
        {
            get
            {
                return _marketDataSentToNegotiator.Text;
            }
            set
            {
                if (_marketDataSentToNegotiator.Text == value)
                    return;
                _marketDataSentToNegotiator.Text = value;
            }
        }

        public string NoteHolder
        {
            get { return _noteHolder.Text; }
            set { _noteHolder.Text = value; }
        }
        public string minPercentOfBPOAccepted
        {
            get { return _minPercentOfBPOAccepted.Text; }
            set { _minPercentOfBPOAccepted.Text = value; }
        }
        public string acceptanceLetterReceived
        {
            get { return _acceptanceLetterReceived.Text; }
            set { _acceptanceLetterReceived.Text = value; }
        }
        public string deficiencyJudgmentWaived
        {
            get { return _deficiencyJudgmentWaived.Text; }
            set { _deficiencyJudgmentWaived.Text = value; }
        }
        public string sellerAgreedToLetterTerms
        {
            get { return _sellerAgreedToLetterTerms.Text; }
            set { _sellerAgreedToLetterTerms.Text = value; }
        }
        public string titleSeasonDate
        {
            get { return _titleSeasonDate.Text; }
            set { _titleSeasonDate.Text = value; }
        }
        public string accept1ExpDate
        {
            get { return _accept1ExpDate.Text; }
            set { _accept1ExpDate.Text = value; }
        }
        public string accept2ExpDate
        {
            get { return _accept2ExpDate.Text; }
            set { _accept2ExpDate.Text = value; }
        }
        public string accept3ExpDate
        {
            get { return _accept3ExpDate.Text; }
            set { _accept3ExpDate.Text = value; }
        }
        public string foreclosureDate
        {
            get { return _foreclosureDate.Text; }
            set { _foreclosureDate.Text = value; }
        }                
        #endregion

        #region CLASS HELPER METHODS
        void AddEventHandlers()
        {
            _removeLien.Click += new EventHandler(On_RemoveLien_Click);
            _pictureBoxStatusProgress.MouseHover += new EventHandler(On_PictureBox_MouseHover);            
            _printStatusPage.Click += new EventHandler(On_PrintStatusPage_Click);

            foreach (Control ctrl in this.Controls)
            {
                if (ctrl is ComboBox)
                {
                    ComboBox cb = (ComboBox)ctrl;
                    cb.SelectedIndexChanged += new EventHandler(On_DropDown_SelectedIndexChanged);
                }

                if (ctrl is Button && ctrl.Name != "_printStatusPage" && ctrl.Name != "_removeLien" && ctrl.Name != "_setDefaults")
                {
                    Button btn = (Button)ctrl;
                    btn.Click += new EventHandler(On_AssignButon_Click);
                }
            }
        }

        public void RemoveEventHandlers()
        {
            _removeLien.Click -= new EventHandler(On_RemoveLien_Click);
            _pictureBoxStatusProgress.MouseHover -= new EventHandler(On_PictureBox_MouseHover);            

            foreach (Control ctrl in this.Controls)
            {
                if (ctrl is ComboBox)
                {
                    ComboBox cb = (ComboBox)ctrl;
                    cb.Leave -= new System.EventHandler(this.OnDropDownLeave);
                }
            
                if (ctrl is TextBox)
                {
                    TextBox tb = (TextBox)ctrl;
                    tb.Leave -= new System.EventHandler(this.OnTextBoxLeave);
                    tb.Enter -= new System.EventHandler(this.OnTextBoxEnter);
                }
            }


        }

        string GetLienSuffix(int idx)
        {
            string retVal = string.Empty;
            retVal = idx.ToString() + GetSuffix(idx);
            retVal += " Lien";
            return retVal;
        }

        string GetSuffix(int nr)
        {
            if (1 == nr)
                return "st";
            else if (2 == nr)
                return "nd";
            else if (3 == nr)
                return "rd";
            else
                return "th";
        }

        int GetNumberOfLiens()
        {
            try { return DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien.Length; }
            catch { return 0; }
        }

        int GetCurrentLienIdx()
        {
            int retVal = -1;

            try { retVal = (int)this.Tag; }
            catch { retVal = -1; }

            return retVal;
        }
        
        string GetSelectedDateFromForm(string currentDate)
        {
            DealMaker.Forms.ShortSale.DateTimePickerForm dtpf = new DealMaker.Forms.ShortSale.DateTimePickerForm();
            dtpf.ShowDialog();

            switch (dtpf.DatePickerOption)
            {
                case eDatePickerOptions.Select:
                    return dtpf.SelectedDateAsString;

                case eDatePickerOptions.KeepCurrent:
                    return currentDate;

                case eDatePickerOptions.Clear:
                    return string.Empty;

                default:
                    return string.Empty;
            }
        }

        string GetSiteAddress()
        {
            string siteAddress = string.Empty;

            if (string.Empty != DataManager.Instance.GetCurrentShortSaleDocument().ContactInfo.SiteAddress)
                siteAddress += DataManager.Instance.GetCurrentShortSaleDocument().ContactInfo.SiteAddress + " ";

            if (string.Empty != DataManager.Instance.GetCurrentShortSaleDocument().ContactInfo.SiteCity)
                siteAddress += DataManager.Instance.GetCurrentShortSaleDocument().ContactInfo.SiteCity + " ";

            if (string.Empty != DataManager.Instance.GetCurrentShortSaleDocument().ContactInfo.SiteState)
                siteAddress += DataManager.Instance.GetCurrentShortSaleDocument().ContactInfo.SiteState;

            if (string.Empty != DataManager.Instance.GetCurrentShortSaleDocument().ContactInfo.SiteZIP)
            {
                if (string.Empty != DataManager.Instance.GetCurrentShortSaleDocument().ContactInfo.SiteState)
                    siteAddress += ", ";

                siteAddress += DataManager.Instance.GetCurrentShortSaleDocument().ContactInfo.SiteZIP;
            }

            return siteAddress;
        }
       
        bool GetBoolanValueFromString(string text)
        {
            if (null == text || string.Empty == text)
                return true;

            bool val = false;

            try { val = bool.Parse(text); }
            catch { val = false; }
            return val;
        }

        string[] GetPartialLinesFromString(Graphics gfx, Font font, int maxWidth, string stringToBreak)
        {
            ArrayList resultLists = new ArrayList();
            string tempLine = string.Empty;
            int i = stringToBreak.IndexOf(" ");
            //string x = stringToBreak.Substring(0, 11);
            while (-1 != i)
            {
                if ((int)(gfx.MeasureString(tempLine + stringToBreak.Substring(0, i), font).Width) < maxWidth)
                {
                    tempLine += stringToBreak.Substring(0, i + 1);
                    stringToBreak = stringToBreak.Substring(i + 1, stringToBreak.Length - (i + 1));
                }
                else
                {
                    string[] splittedLine = tempLine.Split('#');
                    resultLists.AddRange(splittedLine);
                    tempLine = string.Empty;
                }


                i = stringToBreak.IndexOf(" ");
            }

            tempLine += stringToBreak;
            resultLists.Add(tempLine);
            return (string[])resultLists.ToArray(typeof(string));
        }

        void SetDropDownBoxesToDefault()
        {
            //STATUS PAGE
            //1st COLUMN
            if (string.Empty == _authorizationFaxedIn_Who.Text)
                _authorizationFaxedIn_Who.SelectedIndex = 0;

            if (string.Empty == _authorizationVerifiedReceived_Who.Text)
                _authorizationVerifiedReceived_Who.SelectedIndex = 0;

            if (string.Empty == _allIntakeDocsFromSeller_Who.Text)
                _allIntakeDocsFromSeller_Who.SelectedIndex = 0;

            if (string.Empty == _requiredDocsScanned_Who.Text)
                _requiredDocsScanned_Who.SelectedIndex = 0;

            if (string.Empty == _requiredDocsRecorded_Who.Text)
                _requiredDocsRecorded_Who.SelectedIndex = 0;

            if (string.Empty == _returnOriginalsToHomeOwner_Who.Text)
                _returnOriginalsToHomeOwner_Who.SelectedIndex = 0;

            if (string.Empty == _allInfoEnteredInProfitGrabber_Who.Text)
                _allInfoEnteredInProfitGrabber_Who.SelectedIndex = 0;

            if (string.Empty == _propertyProfileInProfitGrabber_Who.Text)
                _propertyProfileInProfitGrabber_Who.SelectedIndex = 0;

            if (string.Empty == _preliminaryTitleSearchedOrdered_Who.Text)
                _preliminaryTitleSearchedOrdered_Who.SelectedIndex = 0;

            if (string.Empty == _preliminaryTitleSearchReceived_Who.Text)
                _preliminaryTitleSearchReceived_Who.SelectedIndex = 0;

            if (string.Empty == _anyTitleIssues_Who.Text)
                _anyTitleIssues_Who.SelectedIndex = 0;

            //2nd COLUMN
            if (string.Empty == _payoffOrdered_Who.Text)
                _payoffOrdered_Who.SelectedIndex = 0;

            if (string.Empty == _reinstatementOrdered_Who.Text)
                _reinstatementOrdered_Who.SelectedIndex = 0;

            if (string.Empty == _ssPackedComplete_Who.Text)
                _ssPackedComplete_Who.SelectedIndex = 0;

            if (string.Empty == _ssPacketReviewed_Who.Text)
                _ssPacketReviewed_Who.SelectedIndex = 0;

            if (string.Empty == _ssPacketSentToLienHolder_Who.Text)
                _ssPacketSentToLienHolder_Who.SelectedIndex = 0;

            if (string.Empty == _ssPacketVerifiedReceived_Who.Text)
                _ssPacketVerifiedReceived_Who.SelectedIndex = 0;

            if (string.Empty == _prelimInspectionOrdered_Who.Text)
                _prelimInspectionOrdered_Who.SelectedIndex = 0;

            if (string.Empty == _prelimInspectionDate_Who.Text)
                _prelimInspectionDate_Who.SelectedIndex = 0;

            if (string.Empty == _bpoPacketComplete_Who.Text)
                _bpoPacketComplete_Who.SelectedIndex = 0;

            //3rd COLUMN
            if (string.Empty == _negotiatorAssigned_Who.Text)
                _negotiatorAssigned_Who.SelectedIndex = 0;

            if (string.Empty == _bpoAppraisalOrdered_Who.Text)
                _bpoAppraisalOrdered_Who.SelectedIndex = 0;

            if (string.Empty == _bpoAppraisalCompleted_Who.Text)
                _bpoAppraisalCompleted_Who.SelectedIndex = 0;

            if (string.Empty == _bpoAppraisalValue_Who.Text)
                _bpoAppraisalValue_Who.SelectedIndex = 0;

            if (string.Empty == _repairListSentToNegotiator_Who.Text)
                _repairListSentToNegotiator_Who.SelectedIndex = 0;

            if (string.Empty == _marketDataSentToNegotiator_Who.Text)
                _marketDataSentToNegotiator_Who.SelectedIndex = 0;

            if (string.Empty == _noteHolder_Who.Text)
                _noteHolder_Who.SelectedIndex = 0;

            if (string.Empty == _minPercentOfBPOAccepted_Who.Text)
                _minPercentOfBPOAccepted_Who.SelectedIndex = 0;

            if (string.Empty == _acceptanceLetterReceived_Who.Text)
                _acceptanceLetterReceived_Who.SelectedIndex = 0;

            if (string.Empty == _deficiencyJudgmentWaived_Who.Text)
                _deficiencyJudgmentWaived_Who.SelectedIndex = 0;

            if (string.Empty == _sellerAgreedToLetterTerms_Who.Text)
                _sellerAgreedToLetterTerms_Who.SelectedIndex = 0;            
        }
        #endregion        

        #region DISPLAY DATA
        bool GetState(string text)
        {
            if ("True" == text)
                return true;
            else
                return false;
        }
        public void DisplayData(int lienIdx)
        {
            this.Tag = lienIdx;

            //STATUS PAGE
            //1st COLUMN
            authorizationFaxedIn = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.AuthorizationFaxedIn.Date;
            authorizationVerifiedReceived = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.AuthorizationVerifiedReceived.Date;
            allIntakeDocsFromSeller = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.AllIntakeDocsFromSeller.Date;
            requiredDocsScanned = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.RequiredDocsScanned.Date;
            requiredDocsRecorded = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.RequiredDocsRecorded.Date;
            returnOriginalsToHomeOwner = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.ReturnOriginalsToHomeOwner.Date;
            allInfoEnteredInProfitGrabber = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.AllInfoEnteredInProfitGrabber.Date;
            propertyProfileInProfitGrabber = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.PropertyProfileInProfitGrabber.Date;
            preliminaryTitleSearchedOrdered = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.PreliminaryTitleSearchOrdered.Date;
            preliminaryTitleSearchReceived = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.PreliminaryTitleSearchReceived.Date;
                        
            //2nd COLUMN
            PayoffOrdered = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.PayoffOrdered.Date;
            ReinstatementOrdered = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.ReinstatementOrdered.Date;
            PayoffAmount = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.PayoffAmount.Value;
            ReinstatementAmount = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.ReinstatementAmount.Value;
            ssPackedComplete = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.SSPacketComplete.Date;
            ssPacketReviewed = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.SSPacketReviewed.Date;
            ssPacketSentToLienHolder = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.SSPacketSentToLienHolder.Date;
            ssPacketVerifiedReceived = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.SSPacketVerifiedReceived.Date;
            prelimInspectionOrdered = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.PrelimInspectionOrdered.Date;
            prelimInspectionComplete = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.PrelimInspectionDate.Date;
            bpoPacketComplete = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.BPOPacketComplete.Date;
                        
            //3rd COLUMN
            negotiatorAssigned = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.NegotiatorAssigned.Date;
            bpoAppraisalOrdered = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.BPOAppraisalOrdered.Date;
            bpoAppraisalComplete = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.BPOAppraisalCompleted.Date;
            bpoAppraisalValue = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.BPOAppraisalValue.Value;
            repairListSentToNegotiator = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.RepairListSentToNegotiator.Date;
            marketDataSentToNegotiator = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.MarketDataSentToNegotiator.Date;
            NoteHolder = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.NoteHolderInvestorName.Value;
            minPercentOfBPOAccepted = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.MinPercentOfBPOAccepted.Value;
            acceptanceLetterReceived = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.AcceptanceLetterReceived.Date;
            deficiencyJudgmentWaived = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.DeficiencyJudgmentWaived.Date;
            sellerAgreedToLetterTerms = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.SellerAgreedToLetterTerms.Date;
                        
            //4th COLUMN
            titleSeasonDate = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.TitleSeasonDate;
            accept1ExpDate = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Accept1ExpDate;
            accept2ExpDate = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Accept2ExpDate;
            accept3ExpDate = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Accept3ExpDate;
            foreclosureDate = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.ForeclosureDate;

            _authorizationFaxedIn_Who.Text = (null != DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.AuthorizationFaxedIn.Who) ? DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.AuthorizationFaxedIn.Who : _authorizationFaxedIn_Who.Text;
            _authorizationVerifiedReceived_Who.Text = (null != DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.AuthorizationVerifiedReceived.Who) ? DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.AuthorizationVerifiedReceived.Who : _authorizationVerifiedReceived_Who.Text;
            _allIntakeDocsFromSeller_Who.Text = (null != DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.AllIntakeDocsFromSeller.Who) ? DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.AllIntakeDocsFromSeller.Who : _allIntakeDocsFromSeller_Who.Text;
            _requiredDocsScanned_Who.Text = (null != DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.RequiredDocsScanned.Who) ? DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.RequiredDocsScanned.Who : _requiredDocsScanned_Who.Text;
            _requiredDocsRecorded_Who.Text = (null != DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.RequiredDocsRecorded.Who) ? DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.RequiredDocsRecorded.Who : _requiredDocsRecorded_Who.Text;
            _returnOriginalsToHomeOwner_Who.Text = (null != DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.ReturnOriginalsToHomeOwner.Who) ? DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.ReturnOriginalsToHomeOwner.Who : _returnOriginalsToHomeOwner_Who.Text;
            _allInfoEnteredInProfitGrabber_Who.Text = (null != DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.AllInfoEnteredInProfitGrabber.Who) ? DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.AllInfoEnteredInProfitGrabber.Who : _allInfoEnteredInProfitGrabber_Who.Text;
            if (null != DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.PropertyProfileInProfitGrabber.Who)
                _propertyProfileInProfitGrabber_Who.Text = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.PropertyProfileInProfitGrabber.Who;
            if (null != DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.PreliminaryTitleSearchOrdered.Who)
                _preliminaryTitleSearchedOrdered_Who.Text = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.PreliminaryTitleSearchOrdered.Who;
            if (null != DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.PreliminaryTitleSearchReceived.Who)
                _preliminaryTitleSearchReceived_Who.Text = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.PreliminaryTitleSearchReceived.Who;
            if (null != DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.AnyTitleIssues.Who)
                _anyTitleIssues_Who.Text = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.AnyTitleIssues.Who;

            _authorizationFaxedIn_Checked.Checked = GetState(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.AuthorizationFaxedIn.Checked);
            _authorizationVerifiedReceived_Checked.Checked = GetState(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.AuthorizationVerifiedReceived.Checked);
            _allIntakeDocsFromSeller_Checked.Checked = GetState(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.AllIntakeDocsFromSeller.Checked);
            _requiredDocsScanned_Checked.Checked = GetState(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.RequiredDocsScanned.Checked);
            _requiredDocsRecorded_Checked.Checked = GetState(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.RequiredDocsRecorded.Checked);
            _returnOriginalsToHomeOwner_Checked.Checked = GetState(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.ReturnOriginalsToHomeOwner.Checked);
            _allInfoEnteredInProfitGrabber_Checked.Checked = GetState(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.AllInfoEnteredInProfitGrabber.Checked);
            _propertyProfileInProfitGrabber_Checked.Checked = GetState(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.PropertyProfileInProfitGrabber.Checked);
            _preliminaryTitleSearchedOrdered_Checked.Checked = GetState(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.PreliminaryTitleSearchOrdered.Checked);
            _preliminaryTitleSearchReceived_Checked.Checked = GetState(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.PreliminaryTitleSearchReceived.Checked);
            _anyTitleIssues_Checked.Checked = GetState(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.AnyTitleIssues.Checked);

            if (null != DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.PayoffOrdered.Who)
                _payoffOrdered_Who.Text = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.PayoffOrdered.Who;
            if (null != DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.ReinstatementOrdered.Who)
                _reinstatementOrdered_Who.Text = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.ReinstatementOrdered.Who;
            if (null != DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.SSPacketComplete.Who)
                _ssPackedComplete_Who.Text = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.SSPacketComplete.Who;
            if (null != DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.SSPacketReviewed.Who)
                _ssPacketReviewed_Who.Text = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.SSPacketReviewed.Who;
            if (null != DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.SSPacketSentToLienHolder.Who)
                _ssPacketSentToLienHolder_Who.Text = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.SSPacketSentToLienHolder.Who;
            if (null != DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.SSPacketVerifiedReceived.Who)
                _ssPacketVerifiedReceived_Who.Text = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.SSPacketVerifiedReceived.Who;
            if (null != DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.PrelimInspectionOrdered.Who)
                _prelimInspectionOrdered_Who.Text = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.PrelimInspectionOrdered.Who;
            if (null != DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.PrelimInspectionDate.Who)
                _prelimInspectionDate_Who.Text = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.PrelimInspectionDate.Who;
            if (null != DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.BPOPacketComplete.Who)
                _bpoPacketComplete_Who.Text = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.BPOPacketComplete.Who;

            _payoffOrdered_Checked.Checked = GetState(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.PayoffOrdered.Checked);
            _reinstatementOrdered_Checked.Checked = GetState(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.ReinstatementOrdered.Checked);
            _ssPackedComplete_Checked.Checked = GetState(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.SSPacketComplete.Checked);
            _ssPacketReviewed_Checked.Checked = GetState(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.SSPacketReviewed.Checked);
            _ssPacketSentToLienHolder_Checked.Checked = GetState(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.SSPacketSentToLienHolder.Checked);
            _ssPacketVerifiedReceived_Checked.Checked = GetState(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.SSPacketVerifiedReceived.Checked);
            _prelimInspectionOrdered_Checked.Checked = GetState(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.PrelimInspectionOrdered.Checked);
            _prelimInspectionDate_Checked.Checked = GetState(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.PrelimInspectionDate.Checked);
            _bpoPacketComplete_Checked.Checked = GetState(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.BPOPacketComplete.Checked);


            if (null != DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.NegotiatorAssigned.Who)
                _negotiatorAssigned_Who.Text = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.NegotiatorAssigned.Who;
            if (null != DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.BPOAppraisalOrdered.Who)
                _bpoAppraisalOrdered_Who.Text = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.BPOAppraisalOrdered.Who;
            if (null != DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.BPOPacketComplete.Who)
                _bpoAppraisalCompleted_Who.Text = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.BPOPacketComplete.Who;
            if (null != DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.BPOAppraisalValue.Who)
                _bpoAppraisalValue_Who.Text = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.BPOAppraisalValue.Who;
            if (null != DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.RepairListSentToNegotiator.Who)
                _repairListSentToNegotiator_Who.Text = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.RepairListSentToNegotiator.Who;
            if (null != DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.MarketDataSentToNegotiator.Who)
                _marketDataSentToNegotiator_Who.Text = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.MarketDataSentToNegotiator.Who;
            if (null != DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.NoteHolderInvestorName.Who)
                _noteHolder_Who.Text = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.NoteHolderInvestorName.Who;
            if (null != DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.MinPercentOfBPOAccepted.Who)
                _minPercentOfBPOAccepted_Who.Text = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.MinPercentOfBPOAccepted.Who;
            if (null != DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.AcceptanceLetterReceived.Who)
                _acceptanceLetterReceived_Who.Text = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.AcceptanceLetterReceived.Who;
            if (null != DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.DeficiencyJudgmentWaived.Who)
                _deficiencyJudgmentWaived_Who.Text = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.DeficiencyJudgmentWaived.Who;
            if (null != DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.SellerAgreedToLetterTerms.Who)
                _sellerAgreedToLetterTerms_Who.Text = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.SellerAgreedToLetterTerms.Who;

            _negotiatorAssigned_Checked.Checked = GetState(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.NegotiatorAssigned.Checked);
            _bpoAppraisalOrdered_Checked.Checked = GetState(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.BPOAppraisalOrdered.Checked);
            _bpoAppraisalCompleted_Checked.Checked = GetState(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.BPOPacketComplete.Checked);
            _bpoAppraisalValue_Checked.Checked = GetState(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.BPOAppraisalValue.Checked);
            _repairListSentToNegotiator_Checked.Checked = GetState(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.RepairListSentToNegotiator.Checked);
            _marketDataSentToNegotiator_Checked.Checked = GetState(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.MarketDataSentToNegotiator.Checked);
            _noteHolder_Checked.Checked = GetState(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.NoteHolderInvestorName.Checked);
            _minPercentOfBPOAccepted_Checked.Checked = GetState(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.MinPercentOfBPOAccepted.Checked);
            _acceptanceLetterReceived_Checked.Checked = GetState(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.AcceptanceLetterReceived.Checked);
            _deficiencyJudgmentWaived_Checked.Checked = GetState(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.DeficiencyJudgmentWaived.Checked);
            _sellerAgreedToLetterTerms_Checked.Checked = GetState(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.SellerAgreedToLetterTerms.Checked);

        }

        #endregion

        #region PRINT DATA
        private void On_PrintPage(object sender, PrintPageEventArgs e)
        {
            Graphics gfx = e.Graphics;

            int pageWidth = e.PageSettings.Bounds.Size.Width;
            int pageHeight = e.PageSettings.Bounds.Size.Height;
            int vertOffset = 0;

            vertOffset += 10;


            if (0 == _liensRendered)
            {
                //HEADER
                int boxHeight = 150;
                gfx.DrawLine(Pens.Black, new Point(10, 10), new Point(pageWidth - 10, 10)); //L-R
                gfx.DrawLine(Pens.Black, new Point(10, 10), new Point(10, boxHeight));//L-B
                gfx.DrawLine(Pens.Black, new Point(pageWidth - 10, 10), new Point(pageWidth - 10, boxHeight)); //R-B
                gfx.DrawLine(Pens.Black, new Point(10, boxHeight), new Point(pageWidth - 10, boxHeight)); //BL - BR

                gfx.DrawString("Contact Information for all Lien Holders", _arial14, _blackBrush, new Point(20, 22));
                gfx.DrawString(":Property Address", _arial12, _blackBrush, new Point(240, 54), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                gfx.DrawString(":HomeOwner Full Names", _arial12, _blackBrush, new Point(240, 76), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                gfx.DrawString(":Email", _arial12, _blackBrush, new Point(240, 98), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                gfx.DrawString(":Primary Social", _arial12, _blackBrush, new Point(240, 120), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                gfx.DrawString(":Cell", _arial12, _blackBrush, new Point(600, 98), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                gfx.DrawString(":Secondary Social", _arial12, _blackBrush, new Point(600, 120), new StringFormat(StringFormatFlags.DirectionRightToLeft));


                gfx.DrawString(GetSiteAddress(), _arial12, _blackBrush, new Point(240, 54));
                gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ContactInfo.FullName, _arial12, _blackBrush, new Point(240, 76));
                gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.PrimarySS, _arial12, _blackBrush, new Point(240, 120));
                gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.PrimaryPhone, _arial12, _blackBrush, new Point(600, 98));
                gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.SecondarySS, _arial12, _blackBrush, new Point(600, 120));

                vertOffset += boxHeight;

            }

            for (int i = _liensRendered; i < DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien.Length; i++)
            {
                if (vertOffset + 290 > pageHeight - 20)
                {
                    e.HasMorePages = true;
                    break;
                }
                vertOffset += 20;

                //draw mask
                gfx.DrawString((i + 1).ToString() + GetSuffix(i + 1) + " Lien " + DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[i].CompanyName + ", Account#: " + DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[i].AccountNr, _arial12, _blackBrush, new Point(20, vertOffset));
                vertOffset += 30;
                gfx.DrawString("Contact Name:", _arial12, _blackBrush, new Point(165, vertOffset));
                gfx.DrawString("Phone:", _arial12, _blackBrush, new Point(370, vertOffset));
                gfx.DrawString("Fax:", _arial12, _blackBrush, new Point(510, vertOffset));
                gfx.DrawString("Email:", _arial12, _blackBrush, new Point(645, vertOffset));
                vertOffset += 25;

                //SETUP DEPT
                gfx.DrawString(":.Setup Dept", _arial12, _blackBrush, new Point(150, vertOffset), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[i].LienHolderContactInfo.SetupDept.ContactName, _arial12, _blackBrush, new Point(165, vertOffset));
                gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[i].LienHolderContactInfo.SetupDept.Phone, _arial12, _blackBrush, new Point(370, vertOffset));
                gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[i].LienHolderContactInfo.SetupDept.Fax, _arial12, _blackBrush, new Point(510, vertOffset));
                gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[i].LienHolderContactInfo.SetupDept.Email, _arial12, _blackBrush, new Point(645, vertOffset));
                vertOffset += 25;

                //SETUP MANAGER
                gfx.DrawString(":Setup Manager", _arial12, _blackBrush, new Point(150, vertOffset), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[i].LienHolderContactInfo.SetupManager.ContactName, _arial12, _blackBrush, new Point(165, vertOffset));
                gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[i].LienHolderContactInfo.SetupManager.Phone, _arial12, _blackBrush, new Point(370, vertOffset));
                gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[i].LienHolderContactInfo.SetupManager.Fax, _arial12, _blackBrush, new Point(510, vertOffset));
                gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[i].LienHolderContactInfo.SetupManager.Email, _arial12, _blackBrush, new Point(645, vertOffset));
                vertOffset += 25;

                //NEGOTIATOR
                gfx.DrawString(":Negotiator", _arial12, _blackBrush, new Point(150, vertOffset), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[i].LienHolderContactInfo.Negotiator.ContactName, _arial12, _blackBrush, new Point(165, vertOffset));
                gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[i].LienHolderContactInfo.Negotiator.Phone, _arial12, _blackBrush, new Point(370, vertOffset));
                gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[i].LienHolderContactInfo.Negotiator.Fax, _arial12, _blackBrush, new Point(510, vertOffset));
                gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[i].LienHolderContactInfo.Negotiator.Email, _arial12, _blackBrush, new Point(645, vertOffset));
                vertOffset += 25;

                //NEG. MANAGER
                gfx.DrawString(":Neg. Manager", _arial12, _blackBrush, new Point(150, vertOffset), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[i].LienHolderContactInfo.NegManager.ContactName, _arial12, _blackBrush, new Point(165, vertOffset));
                gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[i].LienHolderContactInfo.NegManager.Phone, _arial12, _blackBrush, new Point(370, vertOffset));
                gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[i].LienHolderContactInfo.NegManager.Fax, _arial12, _blackBrush, new Point(510, vertOffset));
                gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[i].LienHolderContactInfo.NegManager.Email, _arial12, _blackBrush, new Point(645, vertOffset));
                vertOffset += 25;

                //BPO APPRAISAL
                gfx.DrawString(":BPO/Appraisal 1", _arial12, _blackBrush, new Point(150, vertOffset), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[i].LienHolderContactInfo.BPOAppraisal1.ContactName, _arial12, _blackBrush, new Point(165, vertOffset));
                gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[i].LienHolderContactInfo.BPOAppraisal1.Phone, _arial12, _blackBrush, new Point(370, vertOffset));
                gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[i].LienHolderContactInfo.BPOAppraisal1.Fax, _arial12, _blackBrush, new Point(510, vertOffset));
                gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[i].LienHolderContactInfo.BPOAppraisal1.Email, _arial12, _blackBrush, new Point(645, vertOffset));
                vertOffset += 25;

                //BPO APPRAISAL2
                gfx.DrawString(":BPO/Appraisal 2", _arial12, _blackBrush, new Point(150, vertOffset), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[i].LienHolderContactInfo.BPOAppraisal2.ContactName, _arial12, _blackBrush, new Point(165, vertOffset));
                gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[i].LienHolderContactInfo.BPOAppraisal2.Phone, _arial12, _blackBrush, new Point(370, vertOffset));
                gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[i].LienHolderContactInfo.BPOAppraisal2.Fax, _arial12, _blackBrush, new Point(510, vertOffset));
                gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[i].LienHolderContactInfo.BPOAppraisal2.Email, _arial12, _blackBrush, new Point(645, vertOffset));
                vertOffset += 25;

                //BANKRUPCY DEPT.
                gfx.DrawString(":.Bankrupcy Dept", _arial12, _blackBrush, new Point(150, vertOffset), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[i].LienHolderContactInfo.BankruptcyDeptField.ContactName, _arial12, _blackBrush, new Point(165, vertOffset));
                gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[i].LienHolderContactInfo.BankruptcyDeptField.Phone, _arial12, _blackBrush, new Point(370, vertOffset));
                gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[i].LienHolderContactInfo.BankruptcyDeptField.Fax, _arial12, _blackBrush, new Point(510, vertOffset));
                gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[i].LienHolderContactInfo.BankruptcyDeptField.Email, _arial12, _blackBrush, new Point(645, vertOffset));
                vertOffset += 25;

                //CUSTOMER SERVICE
                gfx.DrawString(":Customer Service", _arial12, _blackBrush, new Point(150, vertOffset), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[i].LienHolderContactInfo.CustomerService.Phone, _arial12, _blackBrush, new Point(370, vertOffset));
                gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[i].LienHolderContactInfo.CustomerService.Fax, _arial12, _blackBrush, new Point(510, vertOffset));
                vertOffset += 25;

                //BANKRUPCY DEPT.
                gfx.DrawString(":Forecl. Attorney", _arial12, _blackBrush, new Point(150, vertOffset), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[i].LienHolderContactInfo.ForeClosureAttorney.ContactName, _arial12, _blackBrush, new Point(165, vertOffset));
                gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[i].LienHolderContactInfo.ForeClosureAttorney.Phone, _arial12, _blackBrush, new Point(370, vertOffset));
                gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[i].LienHolderContactInfo.ForeClosureAttorney.Fax, _arial12, _blackBrush, new Point(510, vertOffset));
                gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[i].LienHolderContactInfo.ForeClosureAttorney.Email, _arial12, _blackBrush, new Point(645, vertOffset));

                vertOffset += 30;
                gfx.DrawLine(Pens.Black, new Point(10, vertOffset), new Point(pageWidth - 10, vertOffset));

                _liensRendered++;
            }
        }

        #endregion

        #region EVENT HANDLERS
        private void OnTextBoxEnter(object sender, System.EventArgs e)
        {
            //((TextBox)sender).BackColor = Color.LightSalmon;

            if (_authorizationFaxedIn == sender)
                authorizationFaxedIn = GetSelectedDateFromForm(authorizationFaxedIn);
            else if (_authorizationVerifiedReceived == sender)
                authorizationVerifiedReceived = GetSelectedDateFromForm(authorizationVerifiedReceived);
            else if (_allIntakeDocsFromSeller == sender)
                allIntakeDocsFromSeller = GetSelectedDateFromForm(allIntakeDocsFromSeller);
            else if (_requiredDocsScanned == sender)
                requiredDocsScanned = GetSelectedDateFromForm(requiredDocsScanned);
            else if (_requiredDocsRecorded == sender)
                requiredDocsRecorded = GetSelectedDateFromForm(requiredDocsRecorded);
            else if (_returnOriginalsToHomeOwner == sender)
                returnOriginalsToHomeOwner = GetSelectedDateFromForm(returnOriginalsToHomeOwner);
            else if (_allInfoEnteredInProfitGrabber == sender)
                allInfoEnteredInProfitGrabber = GetSelectedDateFromForm(allInfoEnteredInProfitGrabber);
            else if (_propertyProfileInProfitGrabber == sender)
                propertyProfileInProfitGrabber = GetSelectedDateFromForm(propertyProfileInProfitGrabber);
            else if (_preliminaryTitleSearchedOrdered == sender)
                preliminaryTitleSearchedOrdered = GetSelectedDateFromForm(preliminaryTitleSearchedOrdered);
            else if (_preliminaryTitleSearchReceived == sender)
                preliminaryTitleSearchReceived = GetSelectedDateFromForm(preliminaryTitleSearchReceived);            
            else if (_payoffOrdered == sender)
                PayoffOrdered = GetSelectedDateFromForm(PayoffOrdered);
            else if (_reinstatementOrdered == sender)
                ReinstatementOrdered = GetSelectedDateFromForm(ReinstatementOrdered);            
            else if (_ssPackedComplete == sender)
                ssPackedComplete = GetSelectedDateFromForm(ssPackedComplete);
            else if (_ssPacketReviewed == sender)
                ssPacketReviewed = GetSelectedDateFromForm(ssPacketReviewed);
            else if (_ssPacketSentToLienHolder == sender)
                ssPacketSentToLienHolder = GetSelectedDateFromForm(ssPacketSentToLienHolder);
            else if (_ssPacketVerifiedReceived == sender)
                ssPacketVerifiedReceived = GetSelectedDateFromForm(ssPacketVerifiedReceived);
            else if (_prelimInspectionOrdered == sender)
                prelimInspectionOrdered = GetSelectedDateFromForm(prelimInspectionOrdered);
            else if (_prelimInspectionDate == sender)
                prelimInspectionComplete = GetSelectedDateFromForm(prelimInspectionComplete);
            else if (_bpoPacketComplete == sender)
                bpoPacketComplete = GetSelectedDateFromForm(bpoPacketComplete);
            else if (_negotiatorAssigned == sender)
                negotiatorAssigned = GetSelectedDateFromForm(negotiatorAssigned);
            else if (_bpoAppraisalOrdered == sender)
                bpoAppraisalOrdered = GetSelectedDateFromForm(bpoAppraisalOrdered);
            else if (_bpoAppraisalCompleted == sender)
                bpoAppraisalComplete = GetSelectedDateFromForm(bpoAppraisalComplete);            
            else if (_repairListSentToNegotiator == sender)
                repairListSentToNegotiator = GetSelectedDateFromForm(repairListSentToNegotiator);
            else if (_marketDataSentToNegotiator == sender)
                marketDataSentToNegotiator = GetSelectedDateFromForm(marketDataSentToNegotiator);            
            else if (_acceptanceLetterReceived == sender)
                acceptanceLetterReceived = GetSelectedDateFromForm(acceptanceLetterReceived);            
            else if (_titleSeasonDate == sender)
                titleSeasonDate = GetSelectedDateFromForm(titleSeasonDate);
            else if (_accept1ExpDate == sender)
                accept1ExpDate = GetSelectedDateFromForm(accept1ExpDate);
            else if (_accept2ExpDate == sender)
                accept2ExpDate = GetSelectedDateFromForm(accept2ExpDate);
            else if (_accept3ExpDate == sender)
                accept3ExpDate = GetSelectedDateFromForm(accept3ExpDate);
            else if (_foreclosureDate == sender)
                foreclosureDate = GetSelectedDateFromForm(foreclosureDate);
        }


        private void OnTextBoxLeave(object sender, System.EventArgs e)
        {
            try
            {
                int lienIdx = GetCurrentLienIdx();

                ((TextBox)sender).BackColor = Color.White;
                
                //STATUS PAGE
                //1st COLUMN
                DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.AuthorizationFaxedIn.Date = authorizationFaxedIn;
                DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.AuthorizationVerifiedReceived.Date = authorizationVerifiedReceived;
                DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.AllIntakeDocsFromSeller.Date = allIntakeDocsFromSeller;
                DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.RequiredDocsScanned.Date = requiredDocsScanned;
                DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.RequiredDocsRecorded.Date = requiredDocsRecorded;
                DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.ReturnOriginalsToHomeOwner.Date = returnOriginalsToHomeOwner;
                DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.AllInfoEnteredInProfitGrabber.Date = allInfoEnteredInProfitGrabber;
                DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.PropertyProfileInProfitGrabber.Date = propertyProfileInProfitGrabber;
                DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.PreliminaryTitleSearchOrdered.Date = preliminaryTitleSearchedOrdered;
                DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.PreliminaryTitleSearchReceived.Date = preliminaryTitleSearchReceived;

                //2nd COLUMN
                DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.PayoffOrdered.Date = PayoffOrdered;
                DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.ReinstatementOrdered.Date = ReinstatementOrdered;
                DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.PayoffAmount.Value = PayoffAmount;
                DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.ReinstatementAmount.Value = ReinstatementAmount;
                DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.SSPacketComplete.Date = ssPackedComplete;
                DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.SSPacketReviewed.Date = ssPacketReviewed;
                DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.SSPacketSentToLienHolder.Date = ssPacketSentToLienHolder;
                DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.SSPacketVerifiedReceived.Date = ssPacketVerifiedReceived;
                DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.PrelimInspectionOrdered.Date = prelimInspectionOrdered;
                DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.PrelimInspectionDate.Date = prelimInspectionComplete;
                DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.BPOPacketComplete.Date = bpoPacketComplete;

                //3rd COLUMN
                DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.NegotiatorAssigned.Date = negotiatorAssigned;
                DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.BPOAppraisalOrdered.Date = bpoAppraisalOrdered;
                DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.BPOAppraisalCompleted.Date = bpoAppraisalComplete;
                DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.BPOAppraisalValue.Value = bpoAppraisalValue;
                DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.RepairListSentToNegotiator.Date = repairListSentToNegotiator;
                DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.MarketDataSentToNegotiator.Date = marketDataSentToNegotiator;
                DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.NoteHolderInvestorName.Value = NoteHolder;
                DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.MinPercentOfBPOAccepted.Value = minPercentOfBPOAccepted;
                DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.AcceptanceLetterReceived.Date = acceptanceLetterReceived;
                DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.DeficiencyJudgmentWaived.Date = deficiencyJudgmentWaived;
                DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.SellerAgreedToLetterTerms.Date = sellerAgreedToLetterTerms;

                //4th COLUMN
                DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.TitleSeasonDate = titleSeasonDate;
                DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Accept1ExpDate = accept1ExpDate;
                DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Accept2ExpDate = accept2ExpDate;
                DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Accept3ExpDate = accept3ExpDate;
                DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.ForeclosureDate = foreclosureDate;
                
                base.AutoSaveDocument();
            }
            catch { }
        }        

        private void On_PictureBox_MouseHover(object sender, EventArgs e)
        {
            //Help_StatusProgreess helpForm = new Help_StatusProgreess();
            //helpForm.ShowDialog();
        }

        private void On_PictureBoxGeneralShortSale_MouseHover(object sender, EventArgs e)
        {
            //Help_GeneralShortSale helpForm = new Help_GeneralShortSale();
            //helpForm.ShowDialog();
        }        

        private void On_RemoveLien_Click(object sender, EventArgs e)
        {
            if (1 >= GetNumberOfLiens())
            {
                MessageBox.Show("1st Lien can't be removed.", StringStore.AppName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            if (DialogResult.Yes != MessageBox.Show("Are you sure you want to remove lien?", StringStore.AppName, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2))
                return;

            int selectedLienIdx = GetCurrentLienIdx();

            DataManager.Instance.RemoveLienFromExistingShortSaleDocument(selectedLienIdx);
            DisplayControlObject.RefreshGUIOnChange();
        }


        private void On_PrintContactInfo_Click(object sender, EventArgs e)
        {
            PrintDialog pd = new PrintDialog();

            PrintDocument tmpprndoc = new PrintDocument();
            tmpprndoc.PrintPage += new PrintPageEventHandler(On_PrintPage);
            tmpprndoc.EndPrint += new PrintEventHandler(On_EndPrint);

            pd.Document = tmpprndoc;
            pd.Document.DefaultPageSettings.Landscape = false;

            PrintPreviewDialogEx tmpprdiag = new PrintPreviewDialogEx();

            tmpprdiag.Document = tmpprndoc;
            tmpprdiag.ShowDialog();
        }


        private void On_EndPrint(object sender, PrintEventArgs e)
        {
            _liensRendered = 0;
        }
        #endregion

        #region PRINTING
        private void On_PrintEventLog_Click(object sender, EventArgs e)
        {
            PrintDialog pd = new PrintDialog();

            PrintDocument tmpprndoc = new PrintDocument();
            tmpprndoc.PrintPage += new PrintPageEventHandler(On_PrintEventLog);            

            pd.Document = tmpprndoc;
            pd.Document.DefaultPageSettings.Landscape = false;

            PrintPreviewDialogEx tmpprdiag = new PrintPreviewDialogEx();

            tmpprdiag.Document = tmpprndoc;
            tmpprdiag.ShowDialog();
        }

        private void On_PrintBlankEventLog_Click(object sender, EventArgs e)
        {
            PrintDialog pd = new PrintDialog();

            PrintDocument tmpprndoc = new PrintDocument();
            tmpprndoc.PrintPage += new PrintPageEventHandler(On_PrintBlankEventLog);            

            pd.Document = tmpprndoc;
            pd.Document.DefaultPageSettings.Landscape = false;

            PrintPreviewDialogEx tmpprdiag = new PrintPreviewDialogEx();

            tmpprdiag.Document = tmpprndoc;
            tmpprdiag.ShowDialog();
        }

        private void On_PrintEventLog(object sender, PrintPageEventArgs e)
        {
            Graphics gfx = e.Graphics;
            RenderEventLogHeader(gfx, e.PageSettings.Bounds.Size.Width, e.PageSettings.Bounds.Size.Height);            
        }

        

        private void On_PrintBlankEventLog(object sender, PrintPageEventArgs e)
        {
            Graphics gfx = e.Graphics;
            RenderEventLogHeader(gfx, e.PageSettings.Bounds.Size.Width, e.PageSettings.Bounds.Size.Height);

            gfx.DrawString("Date", _arial10, _blackBrush, new Point(15, 302));
            gfx.DrawString("Call Comments", _arial10, _blackBrush, new Point(150, 302));

            for (int i = 0; i < 40; i++)
                gfx.DrawLine(Pens.Black, new Point(10, 300 + (i * 20)), new Point(e.PageSettings.Bounds.Size.Width - 20, 300 + (i * 20)));

            gfx.DrawLine(Pens.Black, new Point(140, 300), new Point(140, 1080));
        }

        

        void RenderEventLogHeader(Graphics gfx, int PageWidth, int PageHeight)
        {
            int pageWidth = PageWidth;
            int pageHeight = PageHeight;
            int vertOffset = 0;

            vertOffset += 10;


            //HEADER
            int boxHeight = 255;
            gfx.DrawLine(Pens.Black, new Point(10, 10), new Point(pageWidth - 10, 10)); //L-R
            gfx.DrawLine(Pens.Black, new Point(10, 10), new Point(10, boxHeight));//L-B
            gfx.DrawLine(Pens.Black, new Point(pageWidth - 10, 10), new Point(pageWidth - 10, boxHeight)); //R-B
            gfx.DrawLine(Pens.Black, new Point(10, boxHeight), new Point(pageWidth - 10, boxHeight)); //BL - BR

            gfx.DrawString("Event Log for " + GetLienSuffix(GetCurrentLienIdx() + 1), _arial10BU, _blackBrush, new Point(20, 22));

            gfx.DrawString(":Company Name", _arial10, _blackBrush, new Point(140, 50), new StringFormat(StringFormatFlags.DirectionRightToLeft));
            gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[GetCurrentLienIdx()].CompanyName, _arial10U, _blackBrush, new Point(140, 50));

            gfx.DrawString(":Loan Number", _arial10, _blackBrush, new Point(140, 70), new StringFormat(StringFormatFlags.DirectionRightToLeft));
            gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[GetCurrentLienIdx()].AccountNr, _arial10, _blackBrush, new Point(140, 70));

            gfx.DrawString(":Address", _arial10, _blackBrush, new Point(500, 70), new StringFormat(StringFormatFlags.DirectionRightToLeft));
            gfx.DrawString(GetSiteAddress(), _arial10, _blackBrush, new Point(500, 70));

            gfx.DrawString(":Owner/s Name/s", _arial10, _blackBrush, new Point(140, 90), new StringFormat(StringFormatFlags.DirectionRightToLeft));
            gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ContactInfo.FullName, _arial10, _blackBrush, new Point(140, 90));

            gfx.DrawString(":Primary Social", _arial10, _blackBrush, new Point(140, 110), new StringFormat(StringFormatFlags.DirectionRightToLeft));
            gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.PrimarySS, _arial10U, _blackBrush, new Point(140, 110));

            gfx.DrawString(":Primary Phone", _arial10, _blackBrush, new Point(500, 110), new StringFormat(StringFormatFlags.DirectionRightToLeft));
            gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.PrimaryPhone, _arial10, _blackBrush, new Point(500, 110));

            gfx.DrawString(":Secondary Social", _arial10, _blackBrush, new Point(140, 130), new StringFormat(StringFormatFlags.DirectionRightToLeft));
            gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.SecondarySS, _arial10U, _blackBrush, new Point(140, 130));

            gfx.DrawString(":Secondary Phone", _arial10, _blackBrush, new Point(500, 130), new StringFormat(StringFormatFlags.DirectionRightToLeft));
            gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.SecondaryPhone, _arial10, _blackBrush, new Point(500, 130));

            gfx.DrawString(":Negotiator", _arial10, _blackBrush, new Point(140, 170), new StringFormat(StringFormatFlags.DirectionRightToLeft));
            gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[GetCurrentLienIdx()].LienHolderContactInfo.Negotiator.ContactName, _arial10, _blackBrush, new Point(140, 170));

            gfx.DrawString(":Negotiator Fax Number", _arial10, _blackBrush, new Point(500, 170), new StringFormat(StringFormatFlags.DirectionRightToLeft));
            gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[GetCurrentLienIdx()].LienHolderContactInfo.Negotiator.Fax, _arial10, _blackBrush, new Point(500, 170));

            gfx.DrawString(":Negotiator Phone", _arial10, _blackBrush, new Point(140, 190), new StringFormat(StringFormatFlags.DirectionRightToLeft));
            gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[GetCurrentLienIdx()].LienHolderContactInfo.Negotiator.Phone, _arial10, _blackBrush, new Point(140, 190));

            gfx.DrawString(":Negotiator Email", _arial10, _blackBrush, new Point(500, 190), new StringFormat(StringFormatFlags.DirectionRightToLeft));
            gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[GetCurrentLienIdx()].LienHolderContactInfo.Negotiator.Email, _arial10, _blackBrush, new Point(500, 190));

            gfx.DrawString(":Setup Department", _arial10, _blackBrush, new Point(140, 210), new StringFormat(StringFormatFlags.DirectionRightToLeft));
            gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[GetCurrentLienIdx()].LienHolderContactInfo.SetupDept.ContactName, _arial10, _blackBrush, new Point(140, 210));

            gfx.DrawString(":Setup Fax", _arial10, _blackBrush, new Point(500, 210), new StringFormat(StringFormatFlags.DirectionRightToLeft));
            gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[GetCurrentLienIdx()].LienHolderContactInfo.SetupDept.Fax, _arial10, _blackBrush, new Point(500, 210));

            gfx.DrawString(":Setup Phone", _arial10, _blackBrush, new Point(140, 230), new StringFormat(StringFormatFlags.DirectionRightToLeft));
            gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[GetCurrentLienIdx()].LienHolderContactInfo.SetupDept.Phone, _arial10, _blackBrush, new Point(140, 230));

            gfx.DrawString(":Setup Email", _arial10, _blackBrush, new Point(500, 230), new StringFormat(StringFormatFlags.DirectionRightToLeft));
            gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[GetCurrentLienIdx()].LienHolderContactInfo.SetupDept.Email, _arial10, _blackBrush, new Point(500, 230));
        }

        #endregion

        #region INIT STATUS PAGE (WHO) DROP DOWN BOXES
        public void SetOfficeStaffList(Dictionary<Guid, string> officeStaffListDict)
        {
            _officeStaffListDict = officeStaffListDict;
        }
        void InitWhoDropDownBoxes()
        {
            Dictionary<Guid, string>.Enumerator enumerator;

            foreach (Control ctrl in this.Controls)
            {
                if (ctrl is ComboBox)
                {
                    ComboBox cb = (ComboBox)ctrl;
                    cb.Items.Add("-- Select --");
                    cb.Items.Add("NA");

                    if (null != _officeStaffListDict)
                    {
                        enumerator = _officeStaffListDict.GetEnumerator();
                        while (enumerator.MoveNext())
                            cb.Items.Add(enumerator.Current.Value);
                    }
                }
            }
        }
        #endregion
                        
        #region PRINT STATUS PAGE
        private void On_PrintStatusPage_Click(object sender, EventArgs e)
        {
            PrintDialog pd = new PrintDialog();

            PrintDocument tmpprndoc = new PrintDocument();
            tmpprndoc.PrintPage += new PrintPageEventHandler(OnStatus_PrintPage);

            pd.Document = tmpprndoc;
            pd.Document.DefaultPageSettings.Landscape = false;

            PrintPreviewDialogEx tmpprdiag = new PrintPreviewDialogEx();

            tmpprdiag.Document = tmpprndoc;
            tmpprdiag.ShowDialog();
        }

        private void OnStatus_PrintPage(object sender, PrintPageEventArgs e)
        {
            Graphics gfx = e.Graphics;
            gfx.DrawString(StringStore.AppName + " - Status Report", _arial14, Brushes.Black, new Point(20, 20));

            //FIRST NAME, MIDDLE NAME, LAST NAME, SITE ADDRESS, CITY, STATE ZIP
            gfx.DrawString("Name: " + DataManager.Instance.GetCurrentShortSaleDocument().ContactInfo.FullName, _arial14, Brushes.Black, new Point(20, 50));
            gfx.DrawString("Address: " + DataManager.Instance.GetFullSiteAddress(), _arial14, Brushes.Black, new Point(20, 75));

            gfx.DrawString("Date / Time Printed: " + DateTime.Now.ToShortDateString() + ", " + DateTime.Now.ToShortTimeString(), _arial14, Brushes.Black, new Point(20, 100));

            gfx.DrawLine(Pens.Black, new Point(20, 130), new Point(835, 130));

            gfx.DrawString("Authorization Faxed In: " + ((string.Empty == authorizationFaxedIn ? Blank : authorizationFaxedIn)), _arial10, Brushes.Black, new Point(20, 140));
            gfx.DrawString("Authorization Verified Received: " + ((string.Empty == authorizationVerifiedReceived ? Blank : authorizationVerifiedReceived)), _arial10, Brushes.Black, new Point(20, 160));
            gfx.DrawString("All Intake Docs from Seller: " + ((string.Empty == allIntakeDocsFromSeller ? Blank : allIntakeDocsFromSeller)), _arial10, Brushes.Black, new Point(20, 180));
            gfx.DrawString("Required Docs Scanned: " + ((string.Empty == requiredDocsScanned ? Blank : requiredDocsScanned)), _arial10, Brushes.Black, new Point(20, 200));
            gfx.DrawString("Required Docs Recorded: " + ((string.Empty == requiredDocsRecorded ? Blank : requiredDocsRecorded)), _arial10, Brushes.Black, new Point(20, 220));
            gfx.DrawString("Return Originals to Homeowner: " + ((string.Empty == returnOriginalsToHomeOwner ? Blank : returnOriginalsToHomeOwner)), _arial10, Brushes.Black, new Point(20, 240));
            gfx.DrawString("All Info entered in ProfitGrabber: " + ((string.Empty == allInfoEnteredInProfitGrabber ? Blank : allInfoEnteredInProfitGrabber)), _arial10, Brushes.Black, new Point(20, 260));
            gfx.DrawString("Property Profile in ProfitGrabber: " + ((string.Empty == propertyProfileInProfitGrabber ? Blank : propertyProfileInProfitGrabber)), _arial10, Brushes.Black, new Point(20, 280));
            gfx.DrawString("Prelim. Title Search Ordered: " + ((string.Empty == preliminaryTitleSearchedOrdered ? Blank : preliminaryTitleSearchedOrdered)), _arial10, Brushes.Black, new Point(20, 300));
            gfx.DrawString("Preliminary Title Search Received: " + ((string.Empty == preliminaryTitleSearchReceived ? Blank : preliminaryTitleSearchReceived)), _arial10, Brushes.Black, new Point(20, 320));
            gfx.DrawString("Any Title Issues: " + ((string.Empty == anyTitleIssues ? Blank : anyTitleIssues)), _arial10, Brushes.Black, new Point(20, 340));


            gfx.DrawString("Payoff Ordered: " + ((string.Empty == PayoffOrdered ? Blank : PayoffOrdered)), _arial10, Brushes.Black, new Point(20, 360));
            gfx.DrawString("Reinstatement Ordered: " + ((string.Empty == ReinstatementOrdered ? Blank : ReinstatementOrdered)), _arial10, Brushes.Black, new Point(20, 380));
            gfx.DrawString("Payoff Amount: " + ((string.Empty == PayoffAmount ? Blank : PayoffAmount)), _arial10, Brushes.Black, new Point(20, 400));
            gfx.DrawString("Reinstatement Amount: " + ((string.Empty == ReinstatementAmount ? Blank : ReinstatementAmount)), _arial10, Brushes.Black, new Point(20, 420));
            gfx.DrawString("SS Packet Complete: " + ((string.Empty == ssPackedComplete ? Blank : ssPackedComplete)), _arial10, Brushes.Black, new Point(20, 440));
            gfx.DrawString("SS Packet Reviewed: " + ((string.Empty == ssPacketReviewed ? Blank : ssPacketReviewed)), _arial10, Brushes.Black, new Point(20, 460));
            gfx.DrawString("SS Packet Sent to Lienholder: " + ((string.Empty == ssPacketSentToLienHolder ? Blank : ssPacketSentToLienHolder)), _arial10, Brushes.Black, new Point(20, 480));
            gfx.DrawString("SS Packet Verified Received: " + ((string.Empty == ssPacketVerifiedReceived ? Blank : ssPacketVerifiedReceived)), _arial10, Brushes.Black, new Point(20, 500));
            gfx.DrawString("Prelim. Inspection Ordered: " + ((string.Empty == prelimInspectionOrdered ? Blank : prelimInspectionOrdered)), _arial10, Brushes.Black, new Point(20, 520));
            gfx.DrawString("Prelim. Inspection Completed: " + ((string.Empty == prelimInspectionComplete ? Blank : prelimInspectionComplete)), _arial10, Brushes.Black, new Point(20, 540));
            gfx.DrawString("BPO Packet Complete: " + ((string.Empty == bpoPacketComplete ? Blank : bpoPacketComplete)), _arial10, Brushes.Black, new Point(20, 560));


            gfx.DrawString("Negotiator Assigned: " + ((string.Empty == negotiatorAssigned ? Blank : negotiatorAssigned)), _arial10, Brushes.Black, new Point(20, 580));
            gfx.DrawString("BPO / Appraisal Ordered: " + ((string.Empty == bpoAppraisalOrdered ? Blank : bpoAppraisalOrdered)), _arial10, Brushes.Black, new Point(20, 600));
            gfx.DrawString("BPO / Appraisal Completed: " + ((string.Empty == bpoAppraisalComplete ? Blank : bpoAppraisalComplete)), _arial10, Brushes.Black, new Point(20, 620));
            gfx.DrawString("BPO / Appraisal Value: " + ((string.Empty == bpoAppraisalValue ? Blank : bpoAppraisalValue)), _arial10, Brushes.Black, new Point(20, 640));
            gfx.DrawString("Repair List sent to Negotiator: " + ((string.Empty == repairListSentToNegotiator ? Blank : repairListSentToNegotiator)), _arial10, Brushes.Black, new Point(20, 660));
            gfx.DrawString("Market Data sent to Negotiator: " + ((string.Empty == marketDataSentToNegotiator ? Blank : marketDataSentToNegotiator)), _arial10, Brushes.Black, new Point(20, 680));
            gfx.DrawString("Note Holder/Investor Name: " + ((string.Empty == NoteHolder ? Blank : NoteHolder)), _arial10, Brushes.Black, new Point(20, 700));
            gfx.DrawString("Minimum % of BPO Accepted: " + ((string.Empty == minPercentOfBPOAccepted ? Blank : minPercentOfBPOAccepted)), _arial10, Brushes.Black, new Point(20, 720));
            gfx.DrawString("Acceptance Letter Received: " + ((string.Empty == acceptanceLetterReceived ? Blank : acceptanceLetterReceived)), _arial10, Brushes.Black, new Point(20, 740));
            gfx.DrawString("Deficiency Judgment Waived: " + ((string.Empty == deficiencyJudgmentWaived ? Blank : deficiencyJudgmentWaived)), _arial10, Brushes.Black, new Point(20, 760));
            gfx.DrawString("Seller Agreed to Letter Terms: " + ((string.Empty == sellerAgreedToLetterTerms ? Blank : sellerAgreedToLetterTerms)), _arial10, Brushes.Black, new Point(20, 780));


            gfx.DrawString("Title Season Date: " + ((string.Empty == titleSeasonDate ? Blank : titleSeasonDate)), _arial10, Brushes.Black, new Point(20, 800));
            gfx.DrawString("Accept. 1 Exp. Date: " + ((string.Empty == accept1ExpDate ? Blank : accept1ExpDate)), _arial10, Brushes.Black, new Point(20, 820));
            gfx.DrawString("Accept. 2 Exp. Date: " + ((string.Empty == accept2ExpDate ? Blank : accept2ExpDate)), _arial10, Brushes.Black, new Point(20, 840));
            gfx.DrawString("Accept. 3 Exp. Date: " + ((string.Empty == accept3ExpDate ? Blank : accept3ExpDate)), _arial10, Brushes.Black, new Point(20, 860));
            gfx.DrawString("Foreclosure Date: " + ((string.Empty == foreclosureDate ? Blank : foreclosureDate)), _arial10, Brushes.Black, new Point(20, 880));



            gfx.DrawString("Real Estate Money LLC, " + StringStore.AppName, _arial10I, Brushes.Black, new Point(20, 1075));





        }
        #endregion        

        #region SET COLOR
        void On_DropDown_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetColor(sender);
        }

        void SetColor(object sender)
        {
            Color newColor = Color.White;
            bool enabled = true;
            string selectedText = ((ComboBox)sender).Text;

            if (selectedText == "-- Select --")
                newColor = Color.PaleGoldenrod;
            if (selectedText == "NA")
            {
                newColor = Color.Gray;
                enabled = false;
            }

            if (_authorizationFaxedIn_Who == sender)
            {
                _authorizationFaxedIn.BackColor = newColor;
                _authorizationFaxedIn.Enabled = enabled;
            }
            if (_authorizationVerifiedReceived_Who == sender)
            {
                _authorizationVerifiedReceived.BackColor = newColor;
                _authorizationVerifiedReceived.Enabled = enabled;
            }
            if (_allIntakeDocsFromSeller_Who == sender)
            {
                _allIntakeDocsFromSeller.BackColor = newColor;
                _allIntakeDocsFromSeller.Enabled = enabled;
            }
            if (_requiredDocsScanned_Who == sender)
            {
                _requiredDocsScanned.BackColor = newColor;
                _requiredDocsScanned.Enabled = enabled;
            }
            if (_requiredDocsRecorded_Who == sender)
            {
                _requiredDocsRecorded.BackColor = newColor;
                _requiredDocsRecorded.Enabled = enabled;
            }
            if (_returnOriginalsToHomeOwner_Who == sender)
            {
                _returnOriginalsToHomeOwner.BackColor = newColor;
                _returnOriginalsToHomeOwner.Enabled = enabled;
            }
            if (_allInfoEnteredInProfitGrabber_Who == sender)
            {
                _allInfoEnteredInProfitGrabber.BackColor = newColor;
                _allInfoEnteredInProfitGrabber.Enabled = enabled;
            }
            if (_propertyProfileInProfitGrabber_Who == sender)
            {
                _propertyProfileInProfitGrabber.BackColor = newColor;
                _propertyProfileInProfitGrabber.Enabled = enabled;
            }
            if (_preliminaryTitleSearchedOrdered_Who == sender)
            {
                _preliminaryTitleSearchedOrdered.BackColor = newColor;
                _preliminaryTitleSearchedOrdered.Enabled = enabled;
            }
            if (_preliminaryTitleSearchReceived_Who == sender)
            {
                _preliminaryTitleSearchReceived.BackColor = newColor;
                _preliminaryTitleSearchReceived.Enabled = enabled;
            }
            if (_anyTitleIssues_Who == sender)
            {
                _anyTitleIssues.BackColor = newColor;
                _anyTitleIssues.Enabled = enabled;
            }
            if (_payoffOrdered_Who == sender)
            {
                _payoffOrdered.BackColor = newColor;
                _payoffOrdered.Enabled = enabled;
            }
            if (_reinstatementOrdered_Who == sender)
            {
                _reinstatementOrdered.BackColor = newColor;
                _reinstatementOrdered.Enabled = enabled;
            }
            if (_ssPackedComplete_Who == sender)
            {
                _ssPackedComplete.BackColor = newColor;
                _ssPackedComplete.Enabled = enabled;
            }
            if (_ssPacketReviewed_Who == sender)
            {
                _ssPacketReviewed.BackColor = newColor;
                _ssPacketReviewed.Enabled = enabled;
            }
            if (_ssPacketSentToLienHolder_Who == sender)
            {
                _ssPacketSentToLienHolder.BackColor = newColor;
                _ssPacketSentToLienHolder.Enabled = enabled;
            }
            if (_ssPacketVerifiedReceived_Who == sender)
            {
                _ssPacketVerifiedReceived.BackColor = newColor;
                _ssPacketVerifiedReceived.Enabled = enabled;
            }
            if (_prelimInspectionOrdered_Who == sender)
            {
                _prelimInspectionOrdered.BackColor = newColor;
                _prelimInspectionOrdered.Enabled = enabled;
            }
            if (_prelimInspectionDate_Who == sender)
            {
                _prelimInspectionDate.BackColor = newColor;
                _prelimInspectionDate.Enabled = enabled;
            }
            if (_bpoPacketComplete_Who == sender)
            {
                _bpoPacketComplete.BackColor = newColor;
                _bpoPacketComplete.Enabled = enabled;
            }
            if (_negotiatorAssigned_Who == sender)
            {
                _negotiatorAssigned.BackColor = newColor;
                _negotiatorAssigned.Enabled = enabled;
            }
            if (_bpoAppraisalOrdered_Who == sender)
            {
                _bpoAppraisalOrdered.BackColor = newColor;
                _bpoAppraisalOrdered.Enabled = enabled;
            }
            if (_bpoAppraisalCompleted_Who == sender)
            {
                _bpoAppraisalCompleted.BackColor = newColor;
                _bpoAppraisalCompleted.Enabled = enabled;
            }
            if (_bpoAppraisalValue_Who == sender)
            {
                _bpoAppraisalValue.BackColor = newColor;
                _bpoAppraisalValue.Enabled = enabled;
            }
            if (_repairListSentToNegotiator_Who == sender)
            {
                _repairListSentToNegotiator.BackColor = newColor;
                _repairListSentToNegotiator.Enabled = enabled;
            }
            if (_marketDataSentToNegotiator_Who == sender)
            {
                _marketDataSentToNegotiator.BackColor = newColor;
                _marketDataSentToNegotiator.Enabled = enabled;
            }
            if (_noteHolder_Who == sender)
            {
                _noteHolder.BackColor = newColor;
                _noteHolder.Enabled = enabled;
            }
            if (_minPercentOfBPOAccepted_Who == sender)
            {
                _minPercentOfBPOAccepted.BackColor = newColor;
                _minPercentOfBPOAccepted.Enabled = enabled;
            }
            if (_acceptanceLetterReceived_Who == sender)
            {
                _acceptanceLetterReceived.BackColor = newColor;
                _acceptanceLetterReceived.Enabled = enabled;
            }
            if (_deficiencyJudgmentWaived_Who == sender)
            {
                _deficiencyJudgmentWaived.BackColor = newColor;
                _deficiencyJudgmentWaived.Enabled = enabled;
            }
            if (_sellerAgreedToLetterTerms_Who == sender)
            {
                _sellerAgreedToLetterTerms.BackColor = newColor;
                _sellerAgreedToLetterTerms.Enabled = enabled;
            }

        }
        #endregion

        #region ASSIGN TASK
        void On_AssignButon_Click(object sender, EventArgs e)
        {
            AssignTaskStep2 assignTask = new AssignTaskStep2();

            Guid pgId = DataManager.Instance.PropertyItemID;
            PropertyItem pi = ApplicationHelper.LoadPropertyItem(pgId);
            int currLienIdx = GetCurrentLienIdx();

            Task task = new Task();
            task.MoveToBuisnessDay = true;
            task.DatesFrom = 0;
            task.ContactOrGroup = pi.Owner.FullName;
            task.IdPropertyItem = pgId;
            task.Type = 1; //ToDo    
            task.Status = (int)Status.Pending;


            if (_authorizationFaxedIn_Assign == sender)
            {
                if ("NA" == _authorizationFaxedIn_Who.Text)
                {
                    MessageBox.Show("Task cannot be assigned if NA is selected.  Please select the responsible person and try again.", "ProfitGrabber Pro", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                if (string.Empty != _authorizationFaxedIn.Text)
                {
                    MessageBox.Show("You can only assign a task if the completion date is clear. Please clear the date first.", "ProfitGrabber Pro", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    task = null;
                    return;
                }
                //Fax Auth–Company Name
                task.Description = "Fax Auth–" + DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[currLienIdx].CompanyName;
                task.CreatedFor = _authorizationFaxedIn_Who.Text;
                try { task.EventDate = Convert.ToDateTime(_authorizationFaxedIn.Text); }
                catch { task.EventDate = DateTime.MinValue; }
            }
            if (_authorizationVerifiedReceived_Assign == sender)
            {
                if ("NA" == _authorizationVerifiedReceived_Who.Text)
                {
                    MessageBox.Show("Task cannot be assigned if NA is selected.  Please select the responsible person and try again.", "ProfitGrabber Pro", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                if (string.Empty != _authorizationVerifiedReceived.Text)
                {
                    MessageBox.Show("You can only assign a task if the completion date is clear. Please clear the date first.", "ProfitGrabber Pro", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    task = null;
                    return;
                }

                //Auth Verified–Company Name
                task.Description = "Auth Verified–" + DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[currLienIdx].CompanyName;
                task.CreatedFor = _authorizationVerifiedReceived_Who.Text;
                try { task.EventDate = Convert.ToDateTime(_authorizationVerifiedReceived.Text); }
                catch { task.EventDate = DateTime.MinValue; }
            }
            if (_allIntakeDocsFromSeller_Assign == sender)
            {
                if ("NA" == _allIntakeDocsFromSeller_Who.Text)
                {
                    MessageBox.Show("Task cannot be assigned if NA is selected.  Please select the responsible person and try again.", "ProfitGrabber Pro", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                if (string.Empty != _allIntakeDocsFromSeller.Text)
                {
                    MessageBox.Show("You can only assign a task if the completion date is clear. Please clear the date first.", "ProfitGrabber Pro", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    task = null;
                    return;
                }

                //Get all docs from Seller
                task.Description = "Get all docs from Seller";
                task.CreatedFor = _allIntakeDocsFromSeller_Who.Text;
                try { task.EventDate = Convert.ToDateTime(_allIntakeDocsFromSeller.Text); }
                catch { task.EventDate = DateTime.MinValue; }
            }
            if (_requiredDocsScanned_Assign == sender)
            {
                if ("NA" == _requiredDocsScanned_Who.Text)
                {
                    MessageBox.Show("Task cannot be assigned if NA is selected.  Please select the responsible person and try again.", "ProfitGrabber Pro", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                if (string.Empty != _requiredDocsScanned.Text)
                {
                    MessageBox.Show("You can only assign a task if the completion date is clear. Please clear the date first.", "ProfitGrabber Pro", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    task = null;
                    return;
                }

                //Scan Required Docs
                task.Description = "Scan Required Docs";
                task.CreatedFor = _requiredDocsScanned_Who.Text;
                try { task.EventDate = Convert.ToDateTime(_requiredDocsScanned.Text); }
                catch { task.EventDate = DateTime.MinValue; }
            }
            if (_requiredDocsRecorded_Assign == sender)
            {
                if ("NA" == _requiredDocsRecorded_Who.Text)
                {
                    MessageBox.Show("Task cannot be assigned if NA is selected.  Please select the responsible person and try again.", "ProfitGrabber Pro", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                if (string.Empty != _requiredDocsRecorded.Text)
                {
                    MessageBox.Show("You can only assign a task if the completion date is clear. Please clear the date first.", "ProfitGrabber Pro", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    task = null;
                    return;
                }

                //Record Required Docs
                task.Description = "Record Required Docs";
                task.CreatedFor = _requiredDocsRecorded_Who.Text;
                try { task.EventDate = Convert.ToDateTime(_requiredDocsRecorded.Text); }
                catch { task.EventDate = DateTime.MinValue; }
            }
            if (_returnOriginalsToHomeOwner_Assign == sender)
            {
                if ("NA" == _returnOriginalsToHomeOwner_Who.Text)
                {
                    MessageBox.Show("Task cannot be assigned if NA is selected.  Please select the responsible person and try again.", "ProfitGrabber Pro", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                if (string.Empty != _returnOriginalsToHomeOwner.Text)
                {
                    MessageBox.Show("You can only assign a task if the completion date is clear. Please clear the date first.", "ProfitGrabber Pro", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    task = null;
                    return;
                }

                //Return Docs to Homeowner
                task.Description = "Return Docs to Homeowner";
                task.CreatedFor = _returnOriginalsToHomeOwner_Who.Text;
                try { task.EventDate = Convert.ToDateTime(_returnOriginalsToHomeOwner.Text); }
                catch { task.EventDate = DateTime.MinValue; }
            }
            if (_allInfoEnteredInProfitGrabber_Assign == sender)
            {
                if ("NA" == _allInfoEnteredInProfitGrabber_Who.Text)
                {
                    MessageBox.Show("Task cannot be assigned if NA is selected.  Please select the responsible person and try again.", "ProfitGrabber Pro", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                if (string.Empty != _allInfoEnteredInProfitGrabber.Text)
                {
                    MessageBox.Show("You can only assign a task if the completion date is clear. Please clear the date first.", "ProfitGrabber Pro", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    task = null;
                    return;
                }

                //Enter all seller info in Profit Grabber
                task.Description = "Enter all seller info in Profit Grabber";
                task.CreatedFor = _allInfoEnteredInProfitGrabber_Who.Text;
                try { task.EventDate = Convert.ToDateTime(_allInfoEnteredInProfitGrabber.Text); }
                catch { task.EventDate = DateTime.MinValue; }
            }
            if (_propertyProfileInProfitGrabber_Assign == sender)
            {
                if ("NA" == _propertyProfileInProfitGrabber_Who.Text)
                {
                    MessageBox.Show("Task cannot be assigned if NA is selected.  Please select the responsible person and try again.", "ProfitGrabber Pro", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                if (string.Empty != _propertyProfileInProfitGrabber.Text)
                {
                    MessageBox.Show("You can only assign a task if the completion date is clear. Please clear the date first.", "ProfitGrabber Pro", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    task = null;
                    return;
                }

                //Get Property Profile in Profit Grabber
                task.Description = "Get Property Profile in Profit Grabber";
                task.CreatedFor = _propertyProfileInProfitGrabber_Who.Text;
                try { task.EventDate = Convert.ToDateTime(_propertyProfileInProfitGrabber.Text); }
                catch { task.EventDate = DateTime.MinValue; }
            }
            if (_preliminaryTitleSearchedOrdered_Assign == sender)
            {
                if ("NA" == _preliminaryTitleSearchedOrdered_Who.Text)
                {
                    MessageBox.Show("Task cannot be assigned if NA is selected.  Please select the responsible person and try again.", "ProfitGrabber Pro", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                if (string.Empty != _preliminaryTitleSearchedOrdered.Text)
                {
                    MessageBox.Show("You can only assign a task if the completion date is clear. Please clear the date first.", "ProfitGrabber Pro", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    task = null;
                    return;
                }

                //Order Title Prelim
                task.Description = "Order Title Prelim";
                task.CreatedFor = _preliminaryTitleSearchedOrdered_Who.Text;
                try { task.EventDate = Convert.ToDateTime(_preliminaryTitleSearchedOrdered.Text); }
                catch { task.EventDate = DateTime.MinValue; }
            }
            if (_preliminaryTitleSearchReceived_Assign == sender)
            {
                if ("NA" == _preliminaryTitleSearchReceived_Who.Text)
                {
                    MessageBox.Show("Task cannot be assigned if NA is selected.  Please select the responsible person and try again.", "ProfitGrabber Pro", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                if (string.Empty != _preliminaryTitleSearchReceived.Text)
                {
                    MessageBox.Show("You can only assign a task if the completion date is clear. Please clear the date first.", "ProfitGrabber Pro", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    task = null;
                    return;
                }

                //Title Prelim Received?
                task.Description = "Title Prelim Received?";
                task.CreatedFor = _preliminaryTitleSearchReceived_Who.Text;
                try { task.EventDate = Convert.ToDateTime(_preliminaryTitleSearchReceived.Text); }
                catch { task.EventDate = DateTime.MinValue; }
            }
            if (_anyTitleIssues_Assign == sender)
            {
                if ("NA" == _anyTitleIssues_Who.Text)
                {
                    MessageBox.Show("Task cannot be assigned if NA is selected.  Please select the responsible person and try again.", "ProfitGrabber Pro", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                if (string.Empty != _anyTitleIssues.Text)
                {
                    MessageBox.Show("You can only assign a task if the completion date is clear. Please clear the date first.", "ProfitGrabber Pro", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    task = null;
                    return;
                }

                //Review Title Prelim
                task.Description = "Review Title Prelim";
                task.CreatedFor = _anyTitleIssues_Who.Text;
                try { task.EventDate = Convert.ToDateTime(_anyTitleIssues.Text); }
                catch { task.EventDate = DateTime.MinValue; }
            }
            if (_payoffOrdered_Assign == sender)
            {
                if ("NA" == _payoffOrdered_Who.Text)
                {
                    MessageBox.Show("Task cannot be assigned if NA is selected.  Please select the responsible person and try again.", "ProfitGrabber Pro", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                if (string.Empty != _payoffOrdered.Text)
                {
                    MessageBox.Show("You can only assign a task if the completion date is clear. Please clear the date first.", "ProfitGrabber Pro", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    task = null;
                    return;
                }

                //Order Payoff–Company Name
                task.Description = "Order Payoff–" + DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[currLienIdx].CompanyName;
                task.CreatedFor = _payoffOrdered_Who.Text;
                try { task.EventDate = Convert.ToDateTime(_payoffOrdered.Text); }
                catch { task.EventDate = DateTime.MinValue; }
            }
            if (_reinstatementOrdered_Assign == sender)
            {
                if ("NA" == _reinstatementOrdered_Who.Text)
                {
                    MessageBox.Show("Task cannot be assigned if NA is selected.  Please select the responsible person and try again.", "ProfitGrabber Pro", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                if (string.Empty != _reinstatementOrdered.Text)
                {
                    MessageBox.Show("You can only assign a task if the completion date is clear. Please clear the date first.", "ProfitGrabber Pro", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    task = null;
                    return;
                }

                //Order Reinstatement–Company Name
                task.Description = "Order Reinstatement-" + DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[currLienIdx].CompanyName;
                task.CreatedFor = _reinstatementOrdered_Who.Text;
                try { task.EventDate = Convert.ToDateTime(_reinstatementOrdered.Text); }
                catch { task.EventDate = DateTime.MinValue; }
            }
            if (_ssPackedComplete_Assign == sender)
            {
                if ("NA" == _ssPackedComplete_Who.Text)
                {
                    MessageBox.Show("Task cannot be assigned if NA is selected.  Please select the responsible person and try again.", "ProfitGrabber Pro", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                if (string.Empty != _ssPackedComplete.Text)
                {
                    MessageBox.Show("You can only assign a task if the completion date is clear. Please clear the date first.", "ProfitGrabber Pro", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    task = null;
                    return;
                }

                //Prepare SS Packet–Company Name
                task.Description = "Prepare SS Packet–" + DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[currLienIdx].CompanyName;
                task.CreatedFor = _ssPackedComplete_Who.Text;
                try { task.EventDate = Convert.ToDateTime(_ssPackedComplete.Text); }
                catch { task.EventDate = DateTime.MinValue; }
            }
            if (_ssPacketReviewed_Assign == sender)
            {
                if ("NA" == _ssPacketReviewed_Who.Text)
                {
                    MessageBox.Show("Task cannot be assigned if NA is selected.  Please select the responsible person and try again.", "ProfitGrabber Pro", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                if (string.Empty != _ssPacketReviewed.Text)
                {
                    MessageBox.Show("You can only assign a task if the completion date is clear. Please clear the date first.", "ProfitGrabber Pro", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    task = null;
                    return;
                }

                //Review SS Packet–Company Name
                task.Description = "Review SS Packet–" + DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[currLienIdx].CompanyName;
                task.CreatedFor = _ssPacketReviewed_Who.Text;
                try { task.EventDate = Convert.ToDateTime(_ssPacketReviewed.Text); }
                catch { task.EventDate = DateTime.MinValue; }
            }
            if (_ssPacketSentToLienHolder_Assign == sender)
            {
                if ("NA" == _ssPacketSentToLienHolder_Who.Text)
                {
                    MessageBox.Show("Task cannot be assigned if NA is selected.  Please select the responsible person and try again.", "ProfitGrabber Pro", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                if (string.Empty != _ssPacketSentToLienHolder.Text)
                {
                    MessageBox.Show("You can only assign a task if the completion date is clear. Please clear the date first.", "ProfitGrabber Pro", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    task = null;
                    return;
                }

                //Send SS Packet–Company Name
                task.Description = "Send SS Packet–" + DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[currLienIdx].CompanyName;
                task.CreatedFor = _ssPacketSentToLienHolder_Who.Text;
                try { task.EventDate = Convert.ToDateTime(_ssPacketSentToLienHolder.Text); }
                catch { task.EventDate = DateTime.MinValue; }
            }
            if (_ssPacketVerifiedReceived_Assign == sender)
            {
                if ("NA" == _ssPacketVerifiedReceived_Who.Text)
                {
                    MessageBox.Show("Task cannot be assigned if NA is selected.  Please select the responsible person and try again.", "ProfitGrabber Pro", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                if (string.Empty != _ssPacketVerifiedReceived.Text)
                {
                    MessageBox.Show("You can only assign a task if the completion date is clear. Please clear the date first.", "ProfitGrabber Pro", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    task = null;
                    return;
                }

                //Verify Packet Rcvd–Company Name
                task.Description = "Verify Packet Rcvd–" + DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[currLienIdx].CompanyName;
                task.CreatedFor = _ssPacketVerifiedReceived_Who.Text;
                try { task.EventDate = Convert.ToDateTime(_ssPacketVerifiedReceived.Text); }
                catch { task.EventDate = DateTime.MinValue; }
            }
            if (_prelimInspectionOrdered_Assign == sender)
            {
                if ("NA" == _prelimInspectionOrdered_Who.Text)
                {
                    MessageBox.Show("Task cannot be assigned if NA is selected.  Please select the responsible person and try again.", "ProfitGrabber Pro", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                if (string.Empty != _prelimInspectionOrdered.Text)
                {
                    MessageBox.Show("You can only assign a task if the completion date is clear. Please clear the date first.", "ProfitGrabber Pro", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    task = null;
                    return;
                }

                //Order Prelim Inspection
                task.Description = "Order Prelim Inspection";
                task.CreatedFor = _prelimInspectionOrdered_Who.Text;
                try { task.EventDate = Convert.ToDateTime(_prelimInspectionOrdered.Text); }
                catch { task.EventDate = DateTime.MinValue; }
            }
            if (_prelimInspectionDate_Assign == sender)
            {
                if ("NA" == _prelimInspectionDate_Who.Text)
                {
                    MessageBox.Show("Task cannot be assigned if NA is selected.  Please select the responsible person and try again.", "ProfitGrabber Pro", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                if (string.Empty != _prelimInspectionDate.Text)
                {
                    MessageBox.Show("You can only assign a task if the completion date is clear. Please clear the date first.", "ProfitGrabber Pro", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    task = null;
                    return;
                }

                //Is Prelim Inspection Complete?
                task.Description = "Is Prelim Inspection Complete?";
                task.CreatedFor = _prelimInspectionDate_Who.Text;
                try { task.EventDate = Convert.ToDateTime(_prelimInspectionDate.Text); }
                catch { task.EventDate = DateTime.MinValue; }
            }
            if (_bpoPacketComplete_Assign == sender)
            {
                if ("NA" == _bpoPacketComplete_Who.Text)
                {
                    MessageBox.Show("Task cannot be assigned if NA is selected.  Please select the responsible person and try again.", "ProfitGrabber Pro", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                if (string.Empty != _bpoPacketComplete.Text)
                {
                    MessageBox.Show("You can only assign a task if the completion date is clear. Please clear the date first.", "ProfitGrabber Pro", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    task = null;
                    return;
                }

                //Prepare BPO Packet
                task.Description = "Prepare BPO Packet";
                task.CreatedFor = _bpoPacketComplete_Who.Text;
                try { task.EventDate = Convert.ToDateTime(_bpoPacketComplete.Text); }
                catch { task.EventDate = DateTime.MinValue; }
            }
            if (_negotiatorAssigned_Assign == sender)
            {
                if ("NA" == _negotiatorAssigned_Who.Text)
                {
                    MessageBox.Show("Task cannot be assigned if NA is selected.  Please select the responsible person and try again.", "ProfitGrabber Pro", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                if (string.Empty != _negotiatorAssigned.Text)
                {
                    MessageBox.Show("You can only assign a task if the completion date is clear. Please clear the date first.", "ProfitGrabber Pro", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    task = null;
                    return;
                }

                //Is negotiator assigned?
                task.Description = "Is negotiator assigned?";
                task.CreatedFor = _negotiatorAssigned_Who.Text;
                try { task.EventDate = Convert.ToDateTime(_negotiatorAssigned.Text); }
                catch { task.EventDate = DateTime.MinValue; }
            }
            if (_bpoAppraisalOrdered_Assign == sender)
            {
                if ("NA" == _bpoAppraisalOrdered_Who.Text)
                {
                    MessageBox.Show("Task cannot be assigned if NA is selected.  Please select the responsible person and try again.", "ProfitGrabber Pro", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                if (string.Empty != _bpoAppraisalOrdered.Text)
                {
                    MessageBox.Show("You can only assign a task if the completion date is clear. Please clear the date first.", "ProfitGrabber Pro", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    task = null;
                    return;
                }

                //Get BPO Ordered
                task.Description = "Get BPO Ordered";
                task.CreatedFor = _bpoAppraisalOrdered_Who.Text;
                try { task.EventDate = Convert.ToDateTime(_bpoAppraisalOrdered.Text); }
                catch { task.EventDate = DateTime.MinValue; }
            }
            if (_bpoAppraisalCompleted_Assign == sender)
            {
                if ("NA" == _bpoAppraisalCompleted_Who.Text)
                {
                    MessageBox.Show("Task cannot be assigned if NA is selected.  Please select the responsible person and try again.", "ProfitGrabber Pro", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                if (string.Empty != _bpoAppraisalCompleted.Text)
                {
                    MessageBox.Show("You can only assign a task if the completion date is clear. Please clear the date first.", "ProfitGrabber Pro", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    task = null;
                    return;
                }

                //BPO Complete?
                task.Description = "BPO Complete?";
                task.CreatedFor = _bpoAppraisalCompleted_Who.Text;
                try { task.EventDate = Convert.ToDateTime(_bpoAppraisalCompleted.Text); }
                catch { task.EventDate = DateTime.MinValue; }
            }
            if (_bpoAppraisalValue_Assign == sender)
            {
                if ("NA" == _bpoAppraisalValue_Who.Text)
                {
                    MessageBox.Show("Task cannot be assigned if NA is selected.  Please select the responsible person and try again.", "ProfitGrabber Pro", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                if (string.Empty != _bpoAppraisalValue.Text)
                {
                    MessageBox.Show("You can only assign a task if the completion date is clear. Please clear the date first.", "ProfitGrabber Pro", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    task = null;
                    return;
                }

                //Get BPO Value!
                task.Description = "Get BPO Value!";
                task.CreatedFor = _bpoAppraisalValue_Who.Text;
                try { task.EventDate = Convert.ToDateTime(_bpoAppraisalValue.Text); }
                catch { task.EventDate = DateTime.MinValue; }
            }
            if (_repairListSentToNegotiator_Assign == sender)
            {
                if ("NA" == _repairListSentToNegotiator_Who.Text)
                {
                    MessageBox.Show("Task cannot be assigned if NA is selected.  Please select the responsible person and try again.", "ProfitGrabber Pro", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                if (string.Empty != _repairListSentToNegotiator.Text)
                {
                    MessageBox.Show("You can only assign a task if the completion date is clear. Please clear the date first.", "ProfitGrabber Pro", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    task = null;
                    return;
                }

                //Repair List To Company Name
                task.Description = "Repair List To " + DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[currLienIdx].CompanyName;
                task.CreatedFor = _repairListSentToNegotiator_Who.Text;
                try { task.EventDate = Convert.ToDateTime(_repairListSentToNegotiator.Text); }
                catch { task.EventDate = DateTime.MinValue; }
            }
            if (_marketDataSentToNegotiator_Assign == sender)
            {
                if ("NA" == _marketDataSentToNegotiator_Who.Text)
                {
                    MessageBox.Show("Task cannot be assigned if NA is selected.  Please select the responsible person and try again.", "ProfitGrabber Pro", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                if (string.Empty != _marketDataSentToNegotiator.Text)
                {
                    MessageBox.Show("You can only assign a task if the completion date is clear. Please clear the date first.", "ProfitGrabber Pro", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    task = null;
                    return;
                }

                //Market Data to Company Name
                task.Description = "Market Data To " + DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[currLienIdx].CompanyName;
                task.CreatedFor = _marketDataSentToNegotiator_Who.Text;
                try { task.EventDate = Convert.ToDateTime(_marketDataSentToNegotiator.Text); }
                catch { task.EventDate = DateTime.MinValue; }
            }
            if (_noteHolder_Assign == sender)
            {
                if ("NA" == _noteHolder_Who.Text)
                {
                    MessageBox.Show("Task cannot be assigned if NA is selected.  Please select the responsible person and try again.", "ProfitGrabber Pro", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                if (string.Empty != _noteHolder.Text)
                {
                    MessageBox.Show("You can only assign a task if the completion date is clear. Please clear the date first.", "ProfitGrabber Pro", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    task = null;
                    return;
                }

                //Get Investor Name–Company Name
                task.Description = "Get Investor Name–" + DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[currLienIdx].CompanyName;
                task.CreatedFor = _noteHolder_Who.Text;
                try { task.EventDate = Convert.ToDateTime(_noteHolder.Text); }
                catch { task.EventDate = DateTime.MinValue; }
            }
            if (_minPercentOfBPOAccepted_Assign == sender)
            {
                if ("NA" == _minPercentOfBPOAccepted_Who.Text)
                {
                    MessageBox.Show("Task cannot be assigned if NA is selected.  Please select the responsible person and try again.", "ProfitGrabber Pro", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                if (string.Empty != _minPercentOfBPOAccepted.Text)
                {
                    MessageBox.Show("You can only assign a task if the completion date is clear. Please clear the date first.", "ProfitGrabber Pro", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    task = null;
                    return;
                }

                //Get % Guideline–Company Name
                task.Description = "Get % Guideline–" + DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[currLienIdx].CompanyName;
                task.CreatedFor = _minPercentOfBPOAccepted_Who.Text;
                try { task.EventDate = Convert.ToDateTime(_minPercentOfBPOAccepted.Text); }
                catch { task.EventDate = DateTime.MinValue; }
            }
            if (_acceptanceLetterReceived_Assign == sender)
            {
                if ("NA" == _acceptanceLetterReceived_Who.Text)
                {
                    MessageBox.Show("Task cannot be assigned if NA is selected.  Please select the responsible person and try again.", "ProfitGrabber Pro", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                if (string.Empty != _acceptanceLetterReceived.Text)
                {
                    MessageBox.Show("You can only assign a task if the completion date is clear. Please clear the date first.", "ProfitGrabber Pro", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    task = null;
                    return;
                }

                //Received Acceptance–Company Name?
                task.Description = "Received Acceptance–" + DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[currLienIdx].CompanyName;
                task.CreatedFor = _acceptanceLetterReceived_Who.Text;
                try { task.EventDate = Convert.ToDateTime(_acceptanceLetterReceived.Text); }
                catch { task.EventDate = DateTime.MinValue; }
            }
            if (_deficiencyJudgmentWaived_Assign == sender)
            {
                if ("NA" == _deficiencyJudgmentWaived_Who.Text)
                {
                    MessageBox.Show("Task cannot be assigned if NA is selected.  Please select the responsible person and try again.", "ProfitGrabber Pro", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                if (string.Empty != _deficiencyJudgmentWaived.Text)
                {
                    MessageBox.Show("You can only assign a task if the completion date is clear. Please clear the date first.", "ProfitGrabber Pro", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    task = null;
                    return;
                }

                //Review for Deficiency Judgement
                task.Description = "Review for Deficiency Judgement";
                task.CreatedFor = _deficiencyJudgmentWaived_Who.Text;
                try { task.EventDate = Convert.ToDateTime(_deficiencyJudgmentWaived.Text); }
                catch { task.EventDate = DateTime.MinValue; }
            }
            if (_sellerAgreedToLetterTerms_Assign == sender)
            {
                if ("NA" == _sellerAgreedToLetterTerms_Who.Text)
                {
                    MessageBox.Show("Task cannot be assigned if NA is selected.  Please select the responsible person and try again.", "ProfitGrabber Pro", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                if (string.Empty != _sellerAgreedToLetterTerms.Text)
                {
                    MessageBox.Show("You can only assign a task if the completion date is clear. Please clear the date first.", "ProfitGrabber Pro", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    task = null;
                    return;
                }

                //Seller agrees to letter terms?
                task.Description = "Seller agrees to letter terms?";
                task.CreatedFor = _sellerAgreedToLetterTerms_Who.Text;
                try { task.EventDate = Convert.ToDateTime(_sellerAgreedToLetterTerms.Text); }
                catch { task.EventDate = DateTime.MinValue; }
            }

            task.EventDate = DateTime.Now;  //override
            assignTask.Task = task;

            if (DialogResult.OK == assignTask.ShowDialog())
            {
                Static.AssignTaskToUser(task, pi);

                DataManager.Instance.OnAssignedTask();

                if (_authorizationFaxedIn_Assign == sender)
                {
                    _authorizationFaxedIn_Checked.Checked = true;
                }
                if (_authorizationVerifiedReceived_Assign == sender)
                {
                    _authorizationVerifiedReceived_Checked.Checked = true;
                }
                if (_allIntakeDocsFromSeller_Assign == sender)
                {
                    _allIntakeDocsFromSeller_Checked.Checked = true;
                }
                if (_requiredDocsScanned_Assign == sender)
                {
                    _requiredDocsScanned_Checked.Checked = true;
                }
                if (_requiredDocsRecorded_Assign == sender)
                {
                    _requiredDocsRecorded_Checked.Checked = true;
                }
                if (_returnOriginalsToHomeOwner_Assign == sender)
                {
                    _returnOriginalsToHomeOwner_Checked.Checked = true;
                }
                if (_allInfoEnteredInProfitGrabber_Assign == sender)
                {
                    _allInfoEnteredInProfitGrabber_Checked.Checked = true;
                }
                if (_propertyProfileInProfitGrabber_Assign == sender)
                {
                    _propertyProfileInProfitGrabber_Checked.Checked = true;
                }
                if (_preliminaryTitleSearchedOrdered_Assign == sender)
                {
                    _preliminaryTitleSearchedOrdered_Checked.Checked = true;
                }
                if (_preliminaryTitleSearchReceived_Assign == sender)
                {
                    _preliminaryTitleSearchReceived_Checked.Checked = true;
                }
                if (_anyTitleIssues_Assign == sender)
                {
                    _anyTitleIssues_Checked.Checked = true;
                }
                if (_payoffOrdered_Assign == sender)
                {
                    _payoffOrdered_Checked.Checked = true;
                }
                if (_reinstatementOrdered_Assign == sender)
                {
                    _reinstatementOrdered_Checked.Checked = true;
                }
                if (_ssPackedComplete_Assign == sender)
                {
                    _ssPackedComplete_Checked.Checked = true;
                }
                if (_ssPacketReviewed_Assign == sender)
                {
                    _ssPacketReviewed_Checked.Checked = true;
                }
                if (_ssPacketSentToLienHolder_Assign == sender)
                {
                    _ssPacketSentToLienHolder_Checked.Checked = true;
                }
                if (_ssPacketVerifiedReceived_Assign == sender)
                {
                    _ssPacketVerifiedReceived_Checked.Checked = true;
                }
                if (_prelimInspectionOrdered_Assign == sender)
                {
                    _prelimInspectionOrdered_Checked.Checked = true;
                }
                if (_prelimInspectionDate_Assign == sender)
                {
                    _prelimInspectionDate_Checked.Checked = true;
                }
                if (_bpoPacketComplete_Assign == sender)
                {
                    _bpoPacketComplete_Checked.Checked = true;
                }
                if (_negotiatorAssigned_Assign == sender)
                {
                    _negotiatorAssigned_Checked.Checked = true;
                }
                if (_bpoAppraisalOrdered_Assign == sender)
                {
                    _bpoAppraisalOrdered_Checked.Checked = true;
                }
                if (_bpoAppraisalCompleted_Assign == sender)
                {
                    _bpoAppraisalCompleted_Checked.Checked = true;
                }
                if (_bpoAppraisalValue_Assign == sender)
                {
                    _bpoAppraisalValue_Checked.Checked = true;
                }
                if (_repairListSentToNegotiator_Assign == sender)
                {
                    _repairListSentToNegotiator_Checked.Checked = true;
                }
                if (_marketDataSentToNegotiator_Assign == sender)
                {
                    _marketDataSentToNegotiator_Checked.Checked = true;
                }
                if (_noteHolder_Assign == sender)
                {
                    _noteHolder_Checked.Checked = true;
                }
                if (_minPercentOfBPOAccepted_Assign == sender)
                {
                    _minPercentOfBPOAccepted_Checked.Checked = true;
                }
                if (_acceptanceLetterReceived_Assign == sender)
                {
                    _acceptanceLetterReceived_Checked.Checked = true;
                }
                if (_deficiencyJudgmentWaived_Assign == sender)
                {
                    _deficiencyJudgmentWaived_Checked.Checked = true;
                }
                if (_sellerAgreedToLetterTerms_Assign == sender)
                {
                    _sellerAgreedToLetterTerms_Checked.Checked = true;
                }

                OnCheckBoxStateChanged(null, null);
            }
        }
        #endregion

        #region ON DROP DOWN LEAVE
        private void OnDropDownLeave(object sender, System.EventArgs e)
        {
            int lienIdx = GetCurrentLienIdx();

            //if (_strategy == sender && null != _strategy.SelectedItem)
            //    DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienHolderContactInfo.Strategy.SelectedStrategy = _strategy.SelectedItem.ToString();
            //else if (_mustCloseDate == sender)
            //    DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens[lienIdx].LienHolderContactInfo.Financials.Offers.MustCloseDate = mustCloseDate;

            //STATUS PAGE
            //1st COLUMN
            if (_authorizationFaxedIn_Who == sender)
                DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.AuthorizationFaxedIn.Who = _authorizationFaxedIn_Who.Text;
            else if (_authorizationVerifiedReceived_Who == sender)
                DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.AuthorizationVerifiedReceived.Who = _authorizationVerifiedReceived_Who.Text;
            else if (_allIntakeDocsFromSeller_Who == sender)
                DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.AllIntakeDocsFromSeller.Who = _allIntakeDocsFromSeller_Who.Text;
            else if (_requiredDocsScanned_Who == sender)
                DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.RequiredDocsScanned.Who = _requiredDocsScanned_Who.Text;
            else if (_requiredDocsRecorded_Who == sender)
                DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.RequiredDocsRecorded.Who = _requiredDocsRecorded_Who.Text;
            else if (_returnOriginalsToHomeOwner_Who == sender)
                DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.ReturnOriginalsToHomeOwner.Who = _returnOriginalsToHomeOwner_Who.Text;
            else if (_allInfoEnteredInProfitGrabber_Who == sender)
                DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.AllInfoEnteredInProfitGrabber.Who = _allInfoEnteredInProfitGrabber_Who.Text;
            else if (_propertyProfileInProfitGrabber_Who == sender)
                DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.PropertyProfileInProfitGrabber.Who = _propertyProfileInProfitGrabber_Who.Text;
            else if (_preliminaryTitleSearchedOrdered_Who == sender)
                DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.PreliminaryTitleSearchOrdered.Who = _preliminaryTitleSearchedOrdered_Who.Text;
            else if (_preliminaryTitleSearchReceived_Who == sender)
                DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.PreliminaryTitleSearchReceived.Who = _preliminaryTitleSearchReceived_Who.Text;
            else if (_anyTitleIssues_Who == sender)
                DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.AnyTitleIssues.Who = _anyTitleIssues_Who.Text;

            //2nd COLUMN
            else if (_payoffOrdered_Who == sender)
                DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.PayoffOrdered.Who = _payoffOrdered_Who.Text;
            else if (_reinstatementOrdered_Who == sender)
                DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.ReinstatementOrdered.Who = _reinstatementOrdered_Who.Text;
            else if (_ssPackedComplete_Who == sender)
                DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.SSPacketComplete.Who = _ssPackedComplete_Who.Text;
            else if (_ssPacketReviewed_Who == sender)
                DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.SSPacketReviewed.Who = _ssPacketReviewed_Who.Text;
            else if (_ssPacketSentToLienHolder_Who == sender)
                DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.SSPacketSentToLienHolder.Who = _ssPacketSentToLienHolder_Who.Text;
            else if (_ssPacketVerifiedReceived_Who == sender)
                DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.SSPacketVerifiedReceived.Who = _ssPacketVerifiedReceived_Who.Text;
            else if (_prelimInspectionOrdered_Who == sender)
                DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.PrelimInspectionOrdered.Who = _prelimInspectionOrdered_Who.Text;
            else if (_prelimInspectionDate_Who == sender)
                DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.PrelimInspectionDate.Who = _prelimInspectionDate_Who.Text;
            else if (_bpoPacketComplete_Who == sender)
                DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.BPOPacketComplete.Who = _bpoPacketComplete_Who.Text;

            //3rd COLUMN
            else if (_negotiatorAssigned_Who == sender)
                DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.NegotiatorAssigned.Who = _negotiatorAssigned_Who.Text;
            else if (_bpoAppraisalOrdered_Who == sender)
                DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.BPOAppraisalOrdered.Who = _bpoAppraisalOrdered_Who.Text;
            else if (_bpoAppraisalCompleted_Who == sender)
                DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.BPOPacketComplete.Who = _bpoAppraisalCompleted_Who.Text;
            else if (_bpoAppraisalValue_Who == sender)
                DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.BPOAppraisalValue.Who = _bpoAppraisalValue_Who.Text;
            else if (_repairListSentToNegotiator_Who == sender)
                DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.RepairListSentToNegotiator.Who = _repairListSentToNegotiator_Who.Text;
            else if (_marketDataSentToNegotiator_Who == sender)
                DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.MarketDataSentToNegotiator.Who = _marketDataSentToNegotiator_Who.Text;
            else if (_noteHolder_Who == sender)
                DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.NoteHolderInvestorName.Who = _noteHolder_Who.Text;
            else if (_minPercentOfBPOAccepted_Who == sender)
                DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.MinPercentOfBPOAccepted.Who = _minPercentOfBPOAccepted_Who.Text;
            else if (_acceptanceLetterReceived_Who == sender)
                DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.AcceptanceLetterReceived.Who = _acceptanceLetterReceived_Who.Text;
            else if (_deficiencyJudgmentWaived_Who == sender)
                DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.DeficiencyJudgmentWaived.Who = _deficiencyJudgmentWaived_Who.Text;
            else if (_sellerAgreedToLetterTerms_Who == sender)
                DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.SellerAgreedToLetterTerms.Who = _sellerAgreedToLetterTerms_Who.Text;

            //UpdateTextBoxState();

            base.AutoSaveDocument();
        }
        #endregion

        #region HANDLE TEXT BOX STATES
        private void OnCheckBoxStateChanged(object sender, System.EventArgs e)
        {
            //UpdateTextBoxState();
            int lienIdx = GetCurrentLienIdx();
            DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.AuthorizationFaxedIn.Checked = _authorizationFaxedIn_Checked.Checked.ToString();
            DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.AuthorizationVerifiedReceived.Checked = _authorizationVerifiedReceived_Checked.Checked.ToString();
            DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.AllIntakeDocsFromSeller.Checked = _allIntakeDocsFromSeller_Checked.Checked.ToString();
            DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.RequiredDocsScanned.Checked = _requiredDocsScanned_Checked.Checked.ToString();
            DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.RequiredDocsRecorded.Checked = _requiredDocsRecorded_Checked.Checked.ToString();
            DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.ReturnOriginalsToHomeOwner.Checked = _returnOriginalsToHomeOwner_Checked.Checked.ToString();
            DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.AllInfoEnteredInProfitGrabber.Checked = _allInfoEnteredInProfitGrabber_Checked.Checked.ToString();
            DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.PropertyProfileInProfitGrabber.Checked = _propertyProfileInProfitGrabber_Checked.Checked.ToString();
            DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.PreliminaryTitleSearchOrdered.Checked = _preliminaryTitleSearchedOrdered_Checked.Checked.ToString();
            DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.PreliminaryTitleSearchReceived.Checked = _preliminaryTitleSearchReceived_Checked.Checked.ToString();
            DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.AnyTitleIssues.Checked = _anyTitleIssues_Checked.Checked.ToString();

            DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.PayoffOrdered.Checked = _payoffOrdered_Checked.Checked.ToString();
            DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.ReinstatementOrdered.Checked = _reinstatementOrdered_Checked.Checked.ToString();
            DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.SSPacketComplete.Checked = _ssPackedComplete_Checked.Checked.ToString();
            DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.SSPacketReviewed.Checked = _ssPacketReviewed_Checked.Checked.ToString();
            DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.SSPacketSentToLienHolder.Checked = _ssPacketSentToLienHolder_Checked.Checked.ToString();
            DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.SSPacketVerifiedReceived.Checked = _ssPacketVerifiedReceived_Checked.Checked.ToString();
            DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.PrelimInspectionOrdered.Checked = _prelimInspectionOrdered_Checked.Checked.ToString();
            DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.PrelimInspectionDate.Checked = _prelimInspectionDate_Checked.Checked.ToString();
            DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.BPOPacketComplete.Checked = _bpoPacketComplete_Checked.Checked.ToString();


            DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.NegotiatorAssigned.Checked = _negotiatorAssigned_Checked.Checked.ToString();
            DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.BPOAppraisalOrdered.Checked = _bpoAppraisalOrdered_Checked.Checked.ToString();
            DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.BPOPacketComplete.Checked = _bpoAppraisalCompleted_Checked.Checked.ToString();
            DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.BPOAppraisalValue.Checked = _bpoAppraisalValue_Checked.Checked.ToString();
            DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.RepairListSentToNegotiator.Checked = _repairListSentToNegotiator_Checked.Checked.ToString();
            DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.MarketDataSentToNegotiator.Checked = _marketDataSentToNegotiator_Checked.Checked.ToString();
            DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.NoteHolderInvestorName.Checked = _noteHolder_Checked.Checked.ToString();
            DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.MinPercentOfBPOAccepted.Checked = _minPercentOfBPOAccepted_Checked.Checked.ToString();
            DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.AcceptanceLetterReceived.Checked = _acceptanceLetterReceived_Checked.Checked.ToString();
            DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.DeficiencyJudgmentWaived.Checked = _deficiencyJudgmentWaived_Checked.Checked.ToString();
            DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienStatus.SellerAgreedToLetterTerms.Checked = _sellerAgreedToLetterTerms_Checked.Checked.ToString();

            base.AutoSaveDocument();
        }

        void UpdateTextBoxState()
        {
            //Example:
            //Authorization Faxed In: defaults to clear.  Since it is clear, it should default to red.
            //With the new implementation with the "Not Needed" checkbox - Fields will either default to grey or red.  When a field is entered, background becomes white. 
            //When "Not Needed" is unchecked, background goes from grey to red.  When field is entered, background is white.

            //switch (_authorizationFaxedIn_by.SelectedText)
            //{
            //    case "-- Select --":
            //        _authorizationFaxedIn.Enabled = true;
            //        _authorizationFaxedIn.BackColor = Color.Yellow;
            //        break;

            //    case "NA":
            //        _authorizationFaxedIn.Text = string.Empty;
            //        _authorizationFaxedIn.Enabled = false;
            //        _authorizationFaxedIn.BackColor = Color.DarkGray;
            //        break;
            //    default:
            //        _authorizationFaxedIn.Enabled = true;
            //        _authorizationFaxedIn.BackColor = Color.White;
            //        _authorizationFaxedIn.Enabled = true;
            //        break;
            //}


            /*
            if (_updateTextBoxStates)
            {     
           
                Dictionary<string, string> values = new Dictionary<string, string>();
                values.Add("_authorizationFaxedIn_by", "_authorizationFaxedIn");
                values.Add("_authorizationVerifiedReceived_Who", "_authorizationVerifiedReceived");
                values.Add("_allIntakeDocsFromSeller_Who", "_allIntakeDocsFromSeller");
                values.Add("_requiredDocsScanned_Who", "_requiredDocsScanned");
                
                
                switch (_requiredDocsRecorded_Who.SelectedText)
                {
                    case "-- Select --":
                        _requiredDocsRecorded.Enabled = true;
                        _requiredDocsRecorded.BackColor = Color.Yellow;
                        break;

                    case "NA":
                        _requiredDocsRecorded.Text = string.Empty;
                        _requiredDocsRecorded.Enabled = false;
                        _requiredDocsRecorded.BackColor = Color.DarkGray;
                        break;
                    default:
                        _requiredDocsRecorded.Enabled = true;
                        _requiredDocsRecorded.BackColor = Color.White;
                        _requiredDocsRecorded.Enabled = true;
                        break;
                }

                switch (_returnOriginalsToHomeOwner_Who.SelectedText)
                {
                    case "-- Select --":
                        _returnOriginalsToHomeOwner.Enabled = true;
                        _returnOriginalsToHomeOwner.BackColor = Color.Yellow;
                        break;

                    case "NA":
                        _returnOriginalsToHomeOwner.Text = string.Empty;
                        _returnOriginalsToHomeOwner.Enabled = false;
                        _returnOriginalsToHomeOwner.BackColor = Color.DarkGray;
                        break;
                    default:
                        _returnOriginalsToHomeOwner.Enabled = true;
                        _returnOriginalsToHomeOwner.BackColor = Color.White;
                        _returnOriginalsToHomeOwner.Enabled = true;
                        break;
                }

                switch (_allInfoEnteredInProfitGrabber_Who.SelectedText)
                {
                    case "-- Select --":
                        _allInfoEnteredInProfitGrabber.Enabled = true;
                        _allInfoEnteredInProfitGrabber.BackColor = Color.Yellow;
                        break;

                    case "NA":
                        _allInfoEnteredInProfitGrabber.Text = string.Empty;
                        _allInfoEnteredInProfitGrabber.Enabled = false;
                        _allInfoEnteredInProfitGrabber.BackColor = Color.DarkGray;
                        break;
                    default:
                        _allInfoEnteredInProfitGrabber.Enabled = true;
                        _allInfoEnteredInProfitGrabber.BackColor = Color.White;
                        _allInfoEnteredInProfitGrabber.Enabled = true;
                        break;
                }

                switch (_propertyProfileInProfitGrabber_Who.SelectedText)
                {
                    case "-- Select --":
                        _propertyProfileInProfitGrabber.Enabled = true;
                        _propertyProfileInProfitGrabber.BackColor = Color.Yellow;
                        break;

                    case "NA":
                        _propertyProfileInProfitGrabber.Text = string.Empty;
                        _propertyProfileInProfitGrabber.Enabled = false;
                        _propertyProfileInProfitGrabber.BackColor = Color.DarkGray;
                        break;
                    default:
                        _propertyProfileInProfitGrabber.Enabled = true;
                        _propertyProfileInProfitGrabber.BackColor = Color.White;
                        _propertyProfileInProfitGrabber.Enabled = true;
                        break;
                }

                switch (_preliminaryTitleSearchedOrdered_Who.SelectedText)
                {
                    case "-- Select --":
                        _preliminaryTitleSearchedOrdered.Enabled = true;
                        _preliminaryTitleSearchedOrdered.BackColor = Color.Yellow;
                        break;

                    case "NA":
                        _preliminaryTitleSearchedOrdered.Text = string.Empty;
                        _preliminaryTitleSearchedOrdered.Enabled = false;
                        _preliminaryTitleSearchedOrdered.BackColor = Color.DarkGray;
                        break;
                    default:
                        _preliminaryTitleSearchedOrdered.Enabled = true;
                        _preliminaryTitleSearchedOrdered.BackColor = Color.White;
                        _preliminaryTitleSearchedOrdered.Enabled = true;
                        break;
                }

                switch (_preliminaryTitleSearchReceived_Who.SelectedText)
                {
                    case "-- Select --":
                        _preliminaryTitleSearchReceived.Enabled = true;
                        _preliminaryTitleSearchReceived.BackColor = Color.Yellow;
                        break;

                    case "NA":
                        _preliminaryTitleSearchReceived.Text = string.Empty;
                        _preliminaryTitleSearchReceived.Enabled = false;
                        _preliminaryTitleSearchReceived.BackColor = Color.DarkGray;
                        break;
                    default:
                        _preliminaryTitleSearchReceived.Enabled = true;
                        _preliminaryTitleSearchReceived.BackColor = Color.White;
                        _preliminaryTitleSearchReceived.Enabled = true;
                        break;
                }

                switch (_anyTitleIssues_Who.SelectedText)
                {
                    case "-- Select --":
                        _anyTitleIssues.Enabled = true;
                        _anyTitleIssues.BackColor = Color.Yellow;
                        break;

                    case "NA":
                        _anyTitleIssues.Text = string.Empty;
                        _anyTitleIssues.Enabled = false;
                        _anyTitleIssues.BackColor = Color.DarkGray;
                        break;
                    default:
                        _anyTitleIssues.Enabled = true;
                        _anyTitleIssues.BackColor = Color.White;
                        _anyTitleIssues.Enabled = true;
                        break;
                }

                switch (_payoffOrdered_Who.SelectedText)
                {
                    case "-- Select --":
                        _payoffOrdered.Enabled = true;
                        _payoffOrdered.BackColor = Color.Yellow;
                        break;

                    case "NA":
                        _payoffOrdered.Text = string.Empty;
                        _payoffOrdered.Enabled = false;
                        _payoffOrdered.BackColor = Color.DarkGray;
                        break;
                    default:
                        _payoffOrdered.Enabled = true;
                        _payoffOrdered.BackColor = Color.White;
                        _payoffOrdered.Enabled = true;
                        break;
                }

                switch (_reinstatementOrdered_Who.SelectedText)
                {
                    case "-- Select --":
                        _reinstatementOrdered.Enabled = true;
                        _reinstatementOrdered.BackColor = Color.Yellow;
                        break;

                    case "NA":
                        _reinstatementOrdered.Text = string.Empty;
                        _reinstatementOrdered.Enabled = false;
                        _reinstatementOrdered.BackColor = Color.DarkGray;
                        break;
                    default:
                        _reinstatementOrdered.Enabled = true;
                        _reinstatementOrdered.BackColor = Color.White;
                        _reinstatementOrdered.Enabled = true;
                        break;
                }

                switch (_ssPackedComplete_Who.SelectedText)
                {
                    case "-- Select --":
                        _ssPackedComplete.Enabled = true;
                        _ssPackedComplete.BackColor = Color.Yellow;
                        break;

                    case "NA":
                        _ssPackedComplete.Text = string.Empty;
                        _ssPackedComplete.Enabled = false;
                        _ssPackedComplete.BackColor = Color.DarkGray;
                        break;
                    default:
                        _ssPackedComplete.Enabled = true;
                        _ssPackedComplete.BackColor = Color.White;
                        _ssPackedComplete.Enabled = true;
                        break;
                }
                                
               
            
            //_ssPackedComplete_Checked.Checked
            //_ssPacketReviewed_Checked.Checked
            //_ssPacketSentToLienHolder_Checked.Checked
            //_ssPacketVerifiedReceived_Checked.Checked
            //_prelimInspectionOrdered_Checked.Checked
            //_prelimInspectionDate_Checked.Checked
            //_bpoPacketComplete_Checked.Checked
            // * 
            //_negotiatorAssigned_Checked.Checked
            //_bpoAppraisalOrdered_Checked.Checked
            //_bpoAppraisalCompleted_Checked.Checked
            //_bpoAppraisalValue_Checked.Checked
            //_repairListSentToNegotiator_Checked.Checked
            //_marketDataSentToNegotiator_Checked.Checked
            //_noteHolder_Checked.Checked
            //_minPercentOfBPOAccepted_Checked.Checked
            //_acceptanceLetterReceived_Checked.Checked
            //_deficiencyJudgmentWaived_Checked.Checked
            //_sellerAgreedToLetterTerms_Checked.Checked
                
            }
            */

        }
        #endregion
    }
}
