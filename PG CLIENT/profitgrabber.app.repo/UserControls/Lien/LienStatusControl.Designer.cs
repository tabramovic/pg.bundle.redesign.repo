﻿namespace DealMaker.UserControls.Lien
{
    partial class LienStatusControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LienStatusControl));
            this._deficiencyJudgmentWaived = new System.Windows.Forms.TextBox();
            this._prelimInspectionDate = new System.Windows.Forms.TextBox();
            this._preliminaryTitleSearchReceived = new System.Windows.Forms.TextBox();
            this._prelimInspectionOrdered = new System.Windows.Forms.TextBox();
            this._foreclosureDate = new System.Windows.Forms.TextBox();
            this.label55 = new System.Windows.Forms.Label();
            this._accept3ExpDate = new System.Windows.Forms.TextBox();
            this.label54 = new System.Windows.Forms.Label();
            this._accept2ExpDate = new System.Windows.Forms.TextBox();
            this.label53 = new System.Windows.Forms.Label();
            this._accept1ExpDate = new System.Windows.Forms.TextBox();
            this.label52 = new System.Windows.Forms.Label();
            this._titleSeasonDate = new System.Windows.Forms.TextBox();
            this.label48 = new System.Windows.Forms.Label();
            this._sellerAgreedToLetterTerms = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this._acceptanceLetterReceived = new System.Windows.Forms.TextBox();
            this.label45 = new System.Windows.Forms.Label();
            this._marketDataSentToNegotiator = new System.Windows.Forms.TextBox();
            this.label44 = new System.Windows.Forms.Label();
            this._repairListSentToNegotiator = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this._negotiatorAssigned = new System.Windows.Forms.TextBox();
            this._printStatusPage = new System.Windows.Forms.Button();
            this.label80 = new System.Windows.Forms.Label();
            this._bpoPacketComplete = new System.Windows.Forms.TextBox();
            this.label78 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.label77 = new System.Windows.Forms.Label();
            this._ssPacketReviewed = new System.Windows.Forms.TextBox();
            this.label76 = new System.Windows.Forms.Label();
            this._anyTitleIssues = new System.Windows.Forms.TextBox();
            this.label70 = new System.Windows.Forms.Label();
            this.label68 = new System.Windows.Forms.Label();
            this._propertyProfileInProfitGrabber = new System.Windows.Forms.TextBox();
            this.label69 = new System.Windows.Forms.Label();
            this.label66 = new System.Windows.Forms.Label();
            this._allInfoEnteredInProfitGrabber = new System.Windows.Forms.TextBox();
            this.label67 = new System.Windows.Forms.Label();
            this.label64 = new System.Windows.Forms.Label();
            this._returnOriginalsToHomeOwner = new System.Windows.Forms.TextBox();
            this.label65 = new System.Windows.Forms.Label();
            this.label62 = new System.Windows.Forms.Label();
            this._requiredDocsRecorded = new System.Windows.Forms.TextBox();
            this.label63 = new System.Windows.Forms.Label();
            this.label60 = new System.Windows.Forms.Label();
            this._requiredDocsScanned = new System.Windows.Forms.TextBox();
            this.label61 = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this.label57 = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this._minPercentOfBPOAccepted = new System.Windows.Forms.TextBox();
            this.label42 = new System.Windows.Forms.Label();
            this._noteHolder = new System.Windows.Forms.TextBox();
            this.label41 = new System.Windows.Forms.Label();
            this._reinstatementAmount = new System.Windows.Forms.TextBox();
            this.label39 = new System.Windows.Forms.Label();
            this._reinstatementOrdered = new System.Windows.Forms.TextBox();
            this.labelx = new System.Windows.Forms.Label();
            this._removeLien = new System.Windows.Forms.Button();
            this._preliminaryTitleSearchedOrdered = new System.Windows.Forms.TextBox();
            this._pictureBoxStatusProgress = new System.Windows.Forms.PictureBox();
            this._bpoAppraisalValue = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this._bpoAppraisalCompleted = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this._bpoAppraisalOrdered = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this._ssPacketVerifiedReceived = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this._ssPacketSentToLienHolder = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this._ssPackedComplete = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this._payoffAmount = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this._payoffOrdered = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this._allIntakeDocsFromSeller = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this._authorizationVerifiedReceived = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this._authorizationFaxedIn = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label59 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this._authorizationFaxedIn_Who = new System.Windows.Forms.ComboBox();
            this._authorizationFaxedIn_Assign = new System.Windows.Forms.Button();
            this._authorizationFaxedIn_Checked = new System.Windows.Forms.CheckBox();
            this._authorizationVerifiedReceived_Assign = new System.Windows.Forms.Button();
            this._authorizationVerifiedReceived_Who = new System.Windows.Forms.ComboBox();
            this._authorizationVerifiedReceived_Checked = new System.Windows.Forms.CheckBox();
            this._allIntakeDocsFromSeller_Assign = new System.Windows.Forms.Button();
            this._allIntakeDocsFromSeller_Who = new System.Windows.Forms.ComboBox();
            this._allIntakeDocsFromSeller_Checked = new System.Windows.Forms.CheckBox();
            this._requiredDocsScanned_Assign = new System.Windows.Forms.Button();
            this._requiredDocsScanned_Who = new System.Windows.Forms.ComboBox();
            this._requiredDocsScanned_Checked = new System.Windows.Forms.CheckBox();
            this._requiredDocsRecorded_Assign = new System.Windows.Forms.Button();
            this._requiredDocsRecorded_Who = new System.Windows.Forms.ComboBox();
            this._requiredDocsRecorded_Checked = new System.Windows.Forms.CheckBox();
            this._returnOriginalsToHomeOwner_Assign = new System.Windows.Forms.Button();
            this._returnOriginalsToHomeOwner_Who = new System.Windows.Forms.ComboBox();
            this._returnOriginalsToHomeOwner_Checked = new System.Windows.Forms.CheckBox();
            this._allInfoEnteredInProfitGrabber_Assign = new System.Windows.Forms.Button();
            this._allInfoEnteredInProfitGrabber_Who = new System.Windows.Forms.ComboBox();
            this._allInfoEnteredInProfitGrabber_Checked = new System.Windows.Forms.CheckBox();
            this._propertyProfileInProfitGrabber_Assign = new System.Windows.Forms.Button();
            this._propertyProfileInProfitGrabber_Who = new System.Windows.Forms.ComboBox();
            this._propertyProfileInProfitGrabber_Checked = new System.Windows.Forms.CheckBox();
            this._preliminaryTitleSearchedOrdered_Assign = new System.Windows.Forms.Button();
            this._preliminaryTitleSearchedOrdered_Who = new System.Windows.Forms.ComboBox();
            this._preliminaryTitleSearchedOrdered_Checked = new System.Windows.Forms.CheckBox();
            this._preliminaryTitleSearchReceived_Assign = new System.Windows.Forms.Button();
            this._preliminaryTitleSearchReceived_Who = new System.Windows.Forms.ComboBox();
            this._preliminaryTitleSearchReceived_Checked = new System.Windows.Forms.CheckBox();
            this._anyTitleIssues_Assign = new System.Windows.Forms.Button();
            this._anyTitleIssues_Who = new System.Windows.Forms.ComboBox();
            this._anyTitleIssues_Checked = new System.Windows.Forms.CheckBox();
            this._payoffOrdered_Who = new System.Windows.Forms.ComboBox();
            this._payoffOrdered_Assign = new System.Windows.Forms.Button();
            this._payoffOrdered_Checked = new System.Windows.Forms.CheckBox();
            this._reinstatementOrdered_Assign = new System.Windows.Forms.Button();
            this._reinstatementOrdered_Who = new System.Windows.Forms.ComboBox();
            this._reinstatementOrdered_Checked = new System.Windows.Forms.CheckBox();
            this._ssPackedComplete_Checked = new System.Windows.Forms.CheckBox();
            this._ssPackedComplete_Assign = new System.Windows.Forms.Button();
            this._ssPackedComplete_Who = new System.Windows.Forms.ComboBox();
            this._ssPacketReviewed_Checked = new System.Windows.Forms.CheckBox();
            this._ssPacketReviewed_Assign = new System.Windows.Forms.Button();
            this._ssPacketReviewed_Who = new System.Windows.Forms.ComboBox();
            this._ssPacketSentToLienHolder_Checked = new System.Windows.Forms.CheckBox();
            this._ssPacketSentToLienHolder_Assign = new System.Windows.Forms.Button();
            this._ssPacketSentToLienHolder_Who = new System.Windows.Forms.ComboBox();
            this._ssPacketVerifiedReceived_Checked = new System.Windows.Forms.CheckBox();
            this._ssPacketVerifiedReceived_Assign = new System.Windows.Forms.Button();
            this._ssPacketVerifiedReceived_Who = new System.Windows.Forms.ComboBox();
            this._prelimInspectionOrdered_Assign = new System.Windows.Forms.Button();
            this._prelimInspectionOrdered_Who = new System.Windows.Forms.ComboBox();
            this._prelimInspectionOrdered_Checked = new System.Windows.Forms.CheckBox();
            this._prelimInspectionDate_Assign = new System.Windows.Forms.Button();
            this._prelimInspectionDate_Who = new System.Windows.Forms.ComboBox();
            this._prelimInspectionDate_Checked = new System.Windows.Forms.CheckBox();
            this._bpoPacketComplete_Assign = new System.Windows.Forms.Button();
            this._bpoPacketComplete_Who = new System.Windows.Forms.ComboBox();
            this._bpoPacketComplete_Checked = new System.Windows.Forms.CheckBox();
            this._negotiatorAssigned_Assign = new System.Windows.Forms.Button();
            this._negotiatorAssigned_Who = new System.Windows.Forms.ComboBox();
            this._negotiatorAssigned_Checked = new System.Windows.Forms.CheckBox();
            this._bpoAppraisalOrdered_Assign = new System.Windows.Forms.Button();
            this._bpoAppraisalOrdered_Who = new System.Windows.Forms.ComboBox();
            this._bpoAppraisalOrdered_Checked = new System.Windows.Forms.CheckBox();
            this._bpoAppraisalCompleted_Assign = new System.Windows.Forms.Button();
            this._bpoAppraisalCompleted_Who = new System.Windows.Forms.ComboBox();
            this._bpoAppraisalCompleted_Checked = new System.Windows.Forms.CheckBox();
            this._bpoAppraisalValue_Assign = new System.Windows.Forms.Button();
            this._bpoAppraisalValue_Who = new System.Windows.Forms.ComboBox();
            this._bpoAppraisalValue_Checked = new System.Windows.Forms.CheckBox();
            this._repairListSentToNegotiator_Assign = new System.Windows.Forms.Button();
            this._repairListSentToNegotiator_Who = new System.Windows.Forms.ComboBox();
            this._repairListSentToNegotiator_Checked = new System.Windows.Forms.CheckBox();
            this._marketDataSentToNegotiator_Assign = new System.Windows.Forms.Button();
            this._marketDataSentToNegotiator_Who = new System.Windows.Forms.ComboBox();
            this._marketDataSentToNegotiator_Checked = new System.Windows.Forms.CheckBox();
            this._noteHolder_Assign = new System.Windows.Forms.Button();
            this._noteHolder_Who = new System.Windows.Forms.ComboBox();
            this._noteHolder_Checked = new System.Windows.Forms.CheckBox();
            this._minPercentOfBPOAccepted_Assign = new System.Windows.Forms.Button();
            this._minPercentOfBPOAccepted_Who = new System.Windows.Forms.ComboBox();
            this._minPercentOfBPOAccepted_Checked = new System.Windows.Forms.CheckBox();
            this._acceptanceLetterReceived_Assign = new System.Windows.Forms.Button();
            this._acceptanceLetterReceived_Who = new System.Windows.Forms.ComboBox();
            this._acceptanceLetterReceived_Checked = new System.Windows.Forms.CheckBox();
            this._deficiencyJudgmentWaived_Assign = new System.Windows.Forms.Button();
            this._deficiencyJudgmentWaived_Who = new System.Windows.Forms.ComboBox();
            this._deficiencyJudgmentWaived_Checked = new System.Windows.Forms.CheckBox();
            this._sellerAgreedToLetterTerms_Assign = new System.Windows.Forms.Button();
            this._sellerAgreedToLetterTerms_Who = new System.Windows.Forms.ComboBox();
            this._sellerAgreedToLetterTerms_Checked = new System.Windows.Forms.CheckBox();
            this.label71 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label73 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            ((System.ComponentModel.ISupportInitialize)(this._pictureBoxStatusProgress)).BeginInit();
            this.SuspendLayout();
            // 
            // _deficiencyJudgmentWaived
            // 
            this._deficiencyJudgmentWaived.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._deficiencyJudgmentWaived.Location = new System.Drawing.Point(666, 185);
            this._deficiencyJudgmentWaived.MaxLength = 50;
            this._deficiencyJudgmentWaived.Name = "_deficiencyJudgmentWaived";
            this._deficiencyJudgmentWaived.Size = new System.Drawing.Size(71, 20);
            this._deficiencyJudgmentWaived.TabIndex = 355;
            this._deficiencyJudgmentWaived.Leave += new System.EventHandler(this.OnTextBoxLeave);
            // 
            // _prelimInspectionDate
            // 
            this._prelimInspectionDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._prelimInspectionDate.Location = new System.Drawing.Point(449, 111);
            this._prelimInspectionDate.MaxLength = 50;
            this._prelimInspectionDate.Name = "_prelimInspectionDate";
            this._prelimInspectionDate.Size = new System.Drawing.Size(71, 20);
            this._prelimInspectionDate.TabIndex = 299;
            this._prelimInspectionDate.Leave += new System.EventHandler(this.OnTextBoxLeave);
            this._prelimInspectionDate.Enter += new System.EventHandler(this.OnTextBoxEnter);
            // 
            // _preliminaryTitleSearchReceived
            // 
            this._preliminaryTitleSearchReceived.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._preliminaryTitleSearchReceived.Location = new System.Drawing.Point(228, 37);
            this._preliminaryTitleSearchReceived.MaxLength = 50;
            this._preliminaryTitleSearchReceived.Name = "_preliminaryTitleSearchReceived";
            this._preliminaryTitleSearchReceived.Size = new System.Drawing.Size(71, 20);
            this._preliminaryTitleSearchReceived.TabIndex = 259;
            this._preliminaryTitleSearchReceived.Leave += new System.EventHandler(this.OnTextBoxLeave);
            this._preliminaryTitleSearchReceived.Enter += new System.EventHandler(this.OnTextBoxEnter);
            // 
            // _prelimInspectionOrdered
            // 
            this._prelimInspectionOrdered.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._prelimInspectionOrdered.Location = new System.Drawing.Point(449, 74);
            this._prelimInspectionOrdered.MaxLength = 50;
            this._prelimInspectionOrdered.Name = "_prelimInspectionOrdered";
            this._prelimInspectionOrdered.Size = new System.Drawing.Size(71, 20);
            this._prelimInspectionOrdered.TabIndex = 296;
            this._prelimInspectionOrdered.Leave += new System.EventHandler(this.OnTextBoxLeave);
            this._prelimInspectionOrdered.Enter += new System.EventHandler(this.OnTextBoxEnter);
            // 
            // _foreclosureDate
            // 
            this._foreclosureDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._foreclosureDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._foreclosureDate.Location = new System.Drawing.Point(679, 301);
            this._foreclosureDate.MaxLength = 50;
            this._foreclosureDate.Name = "_foreclosureDate";
            this._foreclosureDate.Size = new System.Drawing.Size(92, 20);
            this._foreclosureDate.TabIndex = 383;
            this._foreclosureDate.Leave += new System.EventHandler(this.OnTextBoxLeave);
            this._foreclosureDate.Enter += new System.EventHandler(this.OnTextBoxEnter);
            // 
            // label55
            // 
            this.label55.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label55.AutoSize = true;
            this.label55.Location = new System.Drawing.Point(671, 285);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(91, 13);
            this.label55.TabIndex = 422;
            this.label55.Text = "Foreclosure Date:";
            // 
            // _accept3ExpDate
            // 
            this._accept3ExpDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._accept3ExpDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._accept3ExpDate.Location = new System.Drawing.Point(829, 262);
            this._accept3ExpDate.MaxLength = 50;
            this._accept3ExpDate.Name = "_accept3ExpDate";
            this._accept3ExpDate.Size = new System.Drawing.Size(71, 20);
            this._accept3ExpDate.TabIndex = 379;
            this.toolTip1.SetToolTip(this._accept3ExpDate, "3rd Title Seasoning Date");
            this._accept3ExpDate.Leave += new System.EventHandler(this.OnTextBoxLeave);
            this._accept3ExpDate.Enter += new System.EventHandler(this.OnTextBoxEnter);
            // 
            // label54
            // 
            this.label54.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label54.AutoSize = true;
            this.label54.Location = new System.Drawing.Point(821, 246);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(67, 13);
            this.label54.TabIndex = 421;
            this.label54.Text = "Acc. Date 3:";
            // 
            // _accept2ExpDate
            // 
            this._accept2ExpDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._accept2ExpDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._accept2ExpDate.Location = new System.Drawing.Point(754, 262);
            this._accept2ExpDate.MaxLength = 50;
            this._accept2ExpDate.Name = "_accept2ExpDate";
            this._accept2ExpDate.Size = new System.Drawing.Size(71, 20);
            this._accept2ExpDate.TabIndex = 374;
            this.toolTip1.SetToolTip(this._accept2ExpDate, "2nd Title Seasoning Date");
            this._accept2ExpDate.Leave += new System.EventHandler(this.OnTextBoxLeave);
            this._accept2ExpDate.Enter += new System.EventHandler(this.OnTextBoxEnter);
            // 
            // label53
            // 
            this.label53.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label53.AutoSize = true;
            this.label53.Location = new System.Drawing.Point(745, 246);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(67, 13);
            this.label53.TabIndex = 420;
            this.label53.Text = "Acc. Date 2:";
            // 
            // _accept1ExpDate
            // 
            this._accept1ExpDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._accept1ExpDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._accept1ExpDate.Location = new System.Drawing.Point(679, 262);
            this._accept1ExpDate.MaxLength = 50;
            this._accept1ExpDate.Name = "_accept1ExpDate";
            this._accept1ExpDate.Size = new System.Drawing.Size(71, 20);
            this._accept1ExpDate.TabIndex = 369;
            this.toolTip1.SetToolTip(this._accept1ExpDate, "1st Title Seasoning Date");
            this._accept1ExpDate.Leave += new System.EventHandler(this.OnTextBoxLeave);
            this._accept1ExpDate.Enter += new System.EventHandler(this.OnTextBoxEnter);
            // 
            // label52
            // 
            this.label52.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label52.AutoSize = true;
            this.label52.Location = new System.Drawing.Point(671, 246);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(67, 13);
            this.label52.TabIndex = 419;
            this.label52.Text = "Acc. Date 1:";
            // 
            // _titleSeasonDate
            // 
            this._titleSeasonDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._titleSeasonDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._titleSeasonDate.Location = new System.Drawing.Point(778, 301);
            this._titleSeasonDate.MaxLength = 50;
            this._titleSeasonDate.Name = "_titleSeasonDate";
            this._titleSeasonDate.Size = new System.Drawing.Size(92, 20);
            this._titleSeasonDate.TabIndex = 366;
            this._titleSeasonDate.Leave += new System.EventHandler(this.OnTextBoxLeave);
            this._titleSeasonDate.Enter += new System.EventHandler(this.OnTextBoxEnter);
            // 
            // label48
            // 
            this.label48.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(771, 285);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(95, 13);
            this.label48.TabIndex = 418;
            this.label48.Text = "Title Season Date:";
            // 
            // _sellerAgreedToLetterTerms
            // 
            this._sellerAgreedToLetterTerms.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._sellerAgreedToLetterTerms.Location = new System.Drawing.Point(666, 222);
            this._sellerAgreedToLetterTerms.MaxLength = 50;
            this._sellerAgreedToLetterTerms.Name = "_sellerAgreedToLetterTerms";
            this._sellerAgreedToLetterTerms.Size = new System.Drawing.Size(71, 20);
            this._sellerAgreedToLetterTerms.TabIndex = 360;
            this._sellerAgreedToLetterTerms.Leave += new System.EventHandler(this.OnTextBoxLeave);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(658, 206);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(154, 13);
            this.label6.TabIndex = 414;
            this.label6.Text = "Seller Agreed To Letter Terms?";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(658, 169);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(152, 13);
            this.label47.TabIndex = 410;
            this.label47.Text = "Deficiency Judgment Waived?";
            // 
            // _acceptanceLetterReceived
            // 
            this._acceptanceLetterReceived.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._acceptanceLetterReceived.Location = new System.Drawing.Point(666, 148);
            this._acceptanceLetterReceived.MaxLength = 50;
            this._acceptanceLetterReceived.Name = "_acceptanceLetterReceived";
            this._acceptanceLetterReceived.Size = new System.Drawing.Size(71, 20);
            this._acceptanceLetterReceived.TabIndex = 350;
            this._acceptanceLetterReceived.Leave += new System.EventHandler(this.OnTextBoxLeave);
            this._acceptanceLetterReceived.Enter += new System.EventHandler(this.OnTextBoxEnter);
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(658, 132);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(147, 13);
            this.label45.TabIndex = 405;
            this.label45.Text = "Acceptance Letter Received:";
            // 
            // _marketDataSentToNegotiator
            // 
            this._marketDataSentToNegotiator.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._marketDataSentToNegotiator.Location = new System.Drawing.Point(666, 37);
            this._marketDataSentToNegotiator.MaxLength = 50;
            this._marketDataSentToNegotiator.Name = "_marketDataSentToNegotiator";
            this._marketDataSentToNegotiator.Size = new System.Drawing.Size(71, 20);
            this._marketDataSentToNegotiator.TabIndex = 336;
            this._marketDataSentToNegotiator.Leave += new System.EventHandler(this.OnTextBoxLeave);
            this._marketDataSentToNegotiator.Enter += new System.EventHandler(this.OnTextBoxEnter);
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(658, 22);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(162, 13);
            this.label44.TabIndex = 396;
            this.label44.Text = "Market Data Sent To Negotiator:";
            // 
            // _repairListSentToNegotiator
            // 
            this._repairListSentToNegotiator.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._repairListSentToNegotiator.Location = new System.Drawing.Point(449, 335);
            this._repairListSentToNegotiator.MaxLength = 50;
            this._repairListSentToNegotiator.Name = "_repairListSentToNegotiator";
            this._repairListSentToNegotiator.Size = new System.Drawing.Size(71, 20);
            this._repairListSentToNegotiator.TabIndex = 330;
            this._repairListSentToNegotiator.Leave += new System.EventHandler(this.OnTextBoxLeave);
            this._repairListSentToNegotiator.Enter += new System.EventHandler(this.OnTextBoxEnter);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(441, 319);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(153, 13);
            this.label19.TabIndex = 392;
            this.label19.Text = "Repair List Sent To Negotiator:";
            // 
            // _negotiatorAssigned
            // 
            this._negotiatorAssigned.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._negotiatorAssigned.Location = new System.Drawing.Point(449, 185);
            this._negotiatorAssigned.MaxLength = 50;
            this._negotiatorAssigned.Name = "_negotiatorAssigned";
            this._negotiatorAssigned.Size = new System.Drawing.Size(71, 20);
            this._negotiatorAssigned.TabIndex = 309;
            this._negotiatorAssigned.Leave += new System.EventHandler(this.OnTextBoxLeave);
            this._negotiatorAssigned.Enter += new System.EventHandler(this.OnTextBoxEnter);
            // 
            // _printStatusPage
            // 
            this._printStatusPage.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._printStatusPage.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._printStatusPage.Location = new System.Drawing.Point(679, 327);
            this._printStatusPage.Name = "_printStatusPage";
            this._printStatusPage.Size = new System.Drawing.Size(92, 27);
            this._printStatusPage.TabIndex = 397;
            this._printStatusPage.Text = "Print";
            // 
            // label80
            // 
            this.label80.AutoSize = true;
            this.label80.Location = new System.Drawing.Point(441, 169);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(105, 13);
            this.label80.TabIndex = 382;
            this.label80.Text = "Negotiator Assigned:";
            // 
            // _bpoPacketComplete
            // 
            this._bpoPacketComplete.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._bpoPacketComplete.Location = new System.Drawing.Point(449, 148);
            this._bpoPacketComplete.MaxLength = 50;
            this._bpoPacketComplete.Name = "_bpoPacketComplete";
            this._bpoPacketComplete.Size = new System.Drawing.Size(71, 20);
            this._bpoPacketComplete.TabIndex = 304;
            this._bpoPacketComplete.Leave += new System.EventHandler(this.OnTextBoxLeave);
            this._bpoPacketComplete.Enter += new System.EventHandler(this.OnTextBoxEnter);
            // 
            // label78
            // 
            this.label78.AutoSize = true;
            this.label78.Location = new System.Drawing.Point(441, 132);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(116, 13);
            this.label78.TabIndex = 377;
            this.label78.Text = "BPO Packet Complete:";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(441, 95);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(116, 13);
            this.label46.TabIndex = 372;
            this.label46.Text = "Prelim Inspection Date:";
            // 
            // label77
            // 
            this.label77.AutoSize = true;
            this.label77.Location = new System.Drawing.Point(441, 58);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(131, 13);
            this.label77.TabIndex = 367;
            this.label77.Text = "Prelim Inspection Ordered:";
            // 
            // _ssPacketReviewed
            // 
            this._ssPacketReviewed.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._ssPacketReviewed.Location = new System.Drawing.Point(228, 298);
            this._ssPacketReviewed.MaxLength = 50;
            this._ssPacketReviewed.Name = "_ssPacketReviewed";
            this._ssPacketReviewed.Size = new System.Drawing.Size(71, 20);
            this._ssPacketReviewed.TabIndex = 283;
            this._ssPacketReviewed.Leave += new System.EventHandler(this.OnTextBoxLeave);
            this._ssPacketReviewed.Enter += new System.EventHandler(this.OnTextBoxEnter);
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.Location = new System.Drawing.Point(220, 282);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(112, 13);
            this.label76.TabIndex = 356;
            this.label76.Text = "SS Packet Reviewed:";
            // 
            // _anyTitleIssues
            // 
            this._anyTitleIssues.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._anyTitleIssues.Location = new System.Drawing.Point(228, 74);
            this._anyTitleIssues.MaxLength = 50;
            this._anyTitleIssues.Name = "_anyTitleIssues";
            this._anyTitleIssues.Size = new System.Drawing.Size(71, 20);
            this._anyTitleIssues.TabIndex = 262;
            this._anyTitleIssues.Leave += new System.EventHandler(this.OnTextBoxLeave);
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Location = new System.Drawing.Point(220, 58);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(87, 13);
            this.label70.TabIndex = 343;
            this.label70.Text = "Any Title Issues?";
            // 
            // label68
            // 
            this.label68.Location = new System.Drawing.Point(203, 298);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(10, 23);
            this.label68.TabIndex = 334;
            this.label68.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label68.Visible = false;
            // 
            // _propertyProfileInProfitGrabber
            // 
            this._propertyProfileInProfitGrabber.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._propertyProfileInProfitGrabber.Location = new System.Drawing.Point(11, 298);
            this._propertyProfileInProfitGrabber.MaxLength = 50;
            this._propertyProfileInProfitGrabber.Name = "_propertyProfileInProfitGrabber";
            this._propertyProfileInProfitGrabber.Size = new System.Drawing.Size(71, 20);
            this._propertyProfileInProfitGrabber.TabIndex = 255;
            this._propertyProfileInProfitGrabber.Leave += new System.EventHandler(this.OnTextBoxLeave);
            this._propertyProfileInProfitGrabber.Enter += new System.EventHandler(this.OnTextBoxEnter);
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Location = new System.Drawing.Point(3, 282);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(157, 13);
            this.label69.TabIndex = 332;
            this.label69.Text = "Property Profile in ProfitGrabber:";
            // 
            // label66
            // 
            this.label66.Location = new System.Drawing.Point(203, 259);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(10, 23);
            this.label66.TabIndex = 328;
            this.label66.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label66.Visible = false;
            // 
            // _allInfoEnteredInProfitGrabber
            // 
            this._allInfoEnteredInProfitGrabber.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._allInfoEnteredInProfitGrabber.Location = new System.Drawing.Point(11, 259);
            this._allInfoEnteredInProfitGrabber.MaxLength = 50;
            this._allInfoEnteredInProfitGrabber.Name = "_allInfoEnteredInProfitGrabber";
            this._allInfoEnteredInProfitGrabber.Size = new System.Drawing.Size(71, 20);
            this._allInfoEnteredInProfitGrabber.TabIndex = 253;
            this._allInfoEnteredInProfitGrabber.Leave += new System.EventHandler(this.OnTextBoxLeave);
            this._allInfoEnteredInProfitGrabber.Enter += new System.EventHandler(this.OnTextBoxEnter);
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Location = new System.Drawing.Point(3, 243);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(158, 13);
            this.label67.TabIndex = 326;
            this.label67.Text = "All Info Entered in ProfitGrabber:";
            // 
            // label64
            // 
            this.label64.Location = new System.Drawing.Point(203, 222);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(10, 23);
            this.label64.TabIndex = 322;
            this.label64.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label64.Visible = false;
            // 
            // _returnOriginalsToHomeOwner
            // 
            this._returnOriginalsToHomeOwner.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._returnOriginalsToHomeOwner.Location = new System.Drawing.Point(11, 222);
            this._returnOriginalsToHomeOwner.MaxLength = 50;
            this._returnOriginalsToHomeOwner.Name = "_returnOriginalsToHomeOwner";
            this._returnOriginalsToHomeOwner.Size = new System.Drawing.Size(71, 20);
            this._returnOriginalsToHomeOwner.TabIndex = 251;
            this._returnOriginalsToHomeOwner.Leave += new System.EventHandler(this.OnTextBoxLeave);
            this._returnOriginalsToHomeOwner.Enter += new System.EventHandler(this.OnTextBoxEnter);
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Location = new System.Drawing.Point(3, 206);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(166, 13);
            this.label65.TabIndex = 319;
            this.label65.Text = "Return Originals To Home Owner:";
            // 
            // label62
            // 
            this.label62.Location = new System.Drawing.Point(203, 185);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(10, 23);
            this.label62.TabIndex = 316;
            this.label62.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label62.Visible = false;
            // 
            // _requiredDocsRecorded
            // 
            this._requiredDocsRecorded.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._requiredDocsRecorded.Location = new System.Drawing.Point(11, 185);
            this._requiredDocsRecorded.MaxLength = 50;
            this._requiredDocsRecorded.Name = "_requiredDocsRecorded";
            this._requiredDocsRecorded.Size = new System.Drawing.Size(71, 20);
            this._requiredDocsRecorded.TabIndex = 249;
            this._requiredDocsRecorded.Leave += new System.EventHandler(this.OnTextBoxLeave);
            this._requiredDocsRecorded.Enter += new System.EventHandler(this.OnTextBoxEnter);
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Location = new System.Drawing.Point(3, 169);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(131, 13);
            this.label63.TabIndex = 313;
            this.label63.Text = "Required Docs Recorded:";
            // 
            // label60
            // 
            this.label60.Location = new System.Drawing.Point(203, 148);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(10, 23);
            this.label60.TabIndex = 310;
            this.label60.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label60.Visible = false;
            // 
            // _requiredDocsScanned
            // 
            this._requiredDocsScanned.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._requiredDocsScanned.Location = new System.Drawing.Point(11, 148);
            this._requiredDocsScanned.MaxLength = 50;
            this._requiredDocsScanned.Name = "_requiredDocsScanned";
            this._requiredDocsScanned.Size = new System.Drawing.Size(71, 20);
            this._requiredDocsScanned.TabIndex = 248;
            this._requiredDocsScanned.Leave += new System.EventHandler(this.OnTextBoxLeave);
            this._requiredDocsScanned.Enter += new System.EventHandler(this.OnTextBoxEnter);
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Location = new System.Drawing.Point(3, 132);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(127, 13);
            this.label61.TabIndex = 307;
            this.label61.Text = "Required Docs Scanned:";
            // 
            // label58
            // 
            this.label58.Location = new System.Drawing.Point(203, 37);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(10, 23);
            this.label58.TabIndex = 294;
            this.label58.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label58.Visible = false;
            // 
            // label57
            // 
            this.label57.Location = new System.Drawing.Point(203, 74);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(10, 23);
            this.label57.TabIndex = 292;
            this.label57.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label57.Visible = false;
            // 
            // label56
            // 
            this.label56.Location = new System.Drawing.Point(203, 111);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(10, 23);
            this.label56.TabIndex = 289;
            this.label56.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label56.Visible = false;
            // 
            // label43
            // 
            this.label43.Location = new System.Drawing.Point(448, 131);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(10, 23);
            this.label43.TabIndex = 281;
            this.label43.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // _minPercentOfBPOAccepted
            // 
            this._minPercentOfBPOAccepted.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._minPercentOfBPOAccepted.Location = new System.Drawing.Point(666, 111);
            this._minPercentOfBPOAccepted.MaxLength = 50;
            this._minPercentOfBPOAccepted.Name = "_minPercentOfBPOAccepted";
            this._minPercentOfBPOAccepted.Size = new System.Drawing.Size(71, 20);
            this._minPercentOfBPOAccepted.TabIndex = 346;
            this._minPercentOfBPOAccepted.Leave += new System.EventHandler(this.OnTextBoxLeave);
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(658, 95);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(148, 13);
            this.label42.TabIndex = 267;
            this.label42.Text = "Minimum % of BPO Accepted:";
            // 
            // _noteHolder
            // 
            this._noteHolder.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._noteHolder.Location = new System.Drawing.Point(666, 74);
            this._noteHolder.MaxLength = 50;
            this._noteHolder.Name = "_noteHolder";
            this._noteHolder.Size = new System.Drawing.Size(71, 20);
            this._noteHolder.TabIndex = 341;
            this._noteHolder.Leave += new System.EventHandler(this.OnTextBoxLeave);
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(658, 58);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(144, 13);
            this.label41.TabIndex = 266;
            this.label41.Text = "Note Holder/Investor Name: ";
            // 
            // _reinstatementAmount
            // 
            this._reinstatementAmount.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._reinstatementAmount.Location = new System.Drawing.Point(228, 222);
            this._reinstatementAmount.MaxLength = 50;
            this._reinstatementAmount.Name = "_reinstatementAmount";
            this._reinstatementAmount.Size = new System.Drawing.Size(71, 20);
            this._reinstatementAmount.TabIndex = 276;
            this._reinstatementAmount.Leave += new System.EventHandler(this.OnTextBoxLeave);
            this._reinstatementAmount.Enter += new System.EventHandler(this.OnTextBoxEnter);
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(220, 206);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(117, 13);
            this.label39.TabIndex = 264;
            this.label39.Text = "Reinstatement Amount:";
            // 
            // _reinstatementOrdered
            // 
            this._reinstatementOrdered.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._reinstatementOrdered.Location = new System.Drawing.Point(228, 148);
            this._reinstatementOrdered.MaxLength = 50;
            this._reinstatementOrdered.Name = "_reinstatementOrdered";
            this._reinstatementOrdered.Size = new System.Drawing.Size(71, 20);
            this._reinstatementOrdered.TabIndex = 268;
            this._reinstatementOrdered.Leave += new System.EventHandler(this.OnTextBoxLeave);
            this._reinstatementOrdered.Enter += new System.EventHandler(this.OnTextBoxEnter);
            // 
            // labelx
            // 
            this.labelx.AutoSize = true;
            this.labelx.Location = new System.Drawing.Point(220, 132);
            this.labelx.Name = "labelx";
            this.labelx.Size = new System.Drawing.Size(119, 13);
            this.labelx.TabIndex = 263;
            this.labelx.Text = "Reinstatement Ordered:";
            // 
            // _removeLien
            // 
            this._removeLien.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._removeLien.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._removeLien.Location = new System.Drawing.Point(778, 327);
            this._removeLien.Name = "_removeLien";
            this._removeLien.Size = new System.Drawing.Size(92, 27);
            this._removeLien.TabIndex = 407;
            this._removeLien.Text = "Remove Lien";
            // 
            // _preliminaryTitleSearchedOrdered
            // 
            this._preliminaryTitleSearchedOrdered.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._preliminaryTitleSearchedOrdered.Location = new System.Drawing.Point(11, 335);
            this._preliminaryTitleSearchedOrdered.MaxLength = 50;
            this._preliminaryTitleSearchedOrdered.Name = "_preliminaryTitleSearchedOrdered";
            this._preliminaryTitleSearchedOrdered.Size = new System.Drawing.Size(71, 20);
            this._preliminaryTitleSearchedOrdered.TabIndex = 257;
            this._preliminaryTitleSearchedOrdered.Leave += new System.EventHandler(this.OnTextBoxLeave);
            this._preliminaryTitleSearchedOrdered.Enter += new System.EventHandler(this.OnTextBoxEnter);
            // 
            // _pictureBoxStatusProgress
            // 
            this._pictureBoxStatusProgress.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._pictureBoxStatusProgress.Image = ((System.Drawing.Image)(resources.GetObject("_pictureBoxStatusProgress.Image")));
            this._pictureBoxStatusProgress.Location = new System.Drawing.Point(876, 299);
            this._pictureBoxStatusProgress.Name = "_pictureBoxStatusProgress";
            this._pictureBoxStatusProgress.Size = new System.Drawing.Size(20, 20);
            this._pictureBoxStatusProgress.TabIndex = 261;
            this._pictureBoxStatusProgress.TabStop = false;
            this._pictureBoxStatusProgress.Visible = false;
            // 
            // _bpoAppraisalValue
            // 
            this._bpoAppraisalValue.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._bpoAppraisalValue.Location = new System.Drawing.Point(449, 298);
            this._bpoAppraisalValue.MaxLength = 50;
            this._bpoAppraisalValue.Name = "_bpoAppraisalValue";
            this._bpoAppraisalValue.Size = new System.Drawing.Size(71, 20);
            this._bpoAppraisalValue.TabIndex = 325;
            this._bpoAppraisalValue.Leave += new System.EventHandler(this.OnTextBoxLeave);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(441, 282);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(116, 13);
            this.label25.TabIndex = 260;
            this.label25.Text = "BPO / Appraisal Value:";
            // 
            // _bpoAppraisalCompleted
            // 
            this._bpoAppraisalCompleted.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._bpoAppraisalCompleted.Location = new System.Drawing.Point(449, 259);
            this._bpoAppraisalCompleted.MaxLength = 50;
            this._bpoAppraisalCompleted.Name = "_bpoAppraisalCompleted";
            this._bpoAppraisalCompleted.Size = new System.Drawing.Size(71, 20);
            this._bpoAppraisalCompleted.TabIndex = 320;
            this._bpoAppraisalCompleted.Leave += new System.EventHandler(this.OnTextBoxLeave);
            this._bpoAppraisalCompleted.Enter += new System.EventHandler(this.OnTextBoxEnter);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(441, 243);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(139, 13);
            this.label23.TabIndex = 258;
            this.label23.Text = "BPO / Appraisal Completed:";
            // 
            // _bpoAppraisalOrdered
            // 
            this._bpoAppraisalOrdered.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._bpoAppraisalOrdered.Location = new System.Drawing.Point(449, 222);
            this._bpoAppraisalOrdered.MaxLength = 50;
            this._bpoAppraisalOrdered.Name = "_bpoAppraisalOrdered";
            this._bpoAppraisalOrdered.Size = new System.Drawing.Size(71, 20);
            this._bpoAppraisalOrdered.TabIndex = 314;
            this._bpoAppraisalOrdered.Leave += new System.EventHandler(this.OnTextBoxLeave);
            this._bpoAppraisalOrdered.Enter += new System.EventHandler(this.OnTextBoxEnter);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(441, 206);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(127, 13);
            this.label24.TabIndex = 256;
            this.label24.Text = "BPO / Appraisal Ordered:";
            // 
            // _ssPacketVerifiedReceived
            // 
            this._ssPacketVerifiedReceived.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._ssPacketVerifiedReceived.Location = new System.Drawing.Point(449, 37);
            this._ssPacketVerifiedReceived.MaxLength = 50;
            this._ssPacketVerifiedReceived.Name = "_ssPacketVerifiedReceived";
            this._ssPacketVerifiedReceived.Size = new System.Drawing.Size(71, 20);
            this._ssPacketVerifiedReceived.TabIndex = 290;
            this._ssPacketVerifiedReceived.Leave += new System.EventHandler(this.OnTextBoxLeave);
            this._ssPacketVerifiedReceived.Enter += new System.EventHandler(this.OnTextBoxEnter);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(441, 22);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(148, 13);
            this.label20.TabIndex = 254;
            this.label20.Text = "SS Packet Verified Received:";
            // 
            // _ssPacketSentToLienHolder
            // 
            this._ssPacketSentToLienHolder.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._ssPacketSentToLienHolder.Location = new System.Drawing.Point(228, 335);
            this._ssPacketSentToLienHolder.MaxLength = 50;
            this._ssPacketSentToLienHolder.Name = "_ssPacketSentToLienHolder";
            this._ssPacketSentToLienHolder.Size = new System.Drawing.Size(71, 20);
            this._ssPacketSentToLienHolder.TabIndex = 287;
            this._ssPacketSentToLienHolder.Leave += new System.EventHandler(this.OnTextBoxLeave);
            this._ssPacketSentToLienHolder.Enter += new System.EventHandler(this.OnTextBoxEnter);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(220, 319);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(155, 13);
            this.label15.TabIndex = 252;
            this.label15.Text = "SS Packet Sent to Lien Holder:";
            // 
            // _ssPackedComplete
            // 
            this._ssPackedComplete.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._ssPackedComplete.Location = new System.Drawing.Point(228, 259);
            this._ssPackedComplete.MaxLength = 50;
            this._ssPackedComplete.Name = "_ssPackedComplete";
            this._ssPackedComplete.Size = new System.Drawing.Size(71, 20);
            this._ssPackedComplete.TabIndex = 282;
            this._ssPackedComplete.Leave += new System.EventHandler(this.OnTextBoxLeave);
            this._ssPackedComplete.Enter += new System.EventHandler(this.OnTextBoxEnter);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(220, 243);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(179, 13);
            this.label16.TabIndex = 250;
            this.label16.Text = "SS Packet Complete (Per Checklist):";
            // 
            // _payoffAmount
            // 
            this._payoffAmount.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._payoffAmount.Location = new System.Drawing.Point(228, 185);
            this._payoffAmount.MaxLength = 50;
            this._payoffAmount.Name = "_payoffAmount";
            this._payoffAmount.Size = new System.Drawing.Size(71, 20);
            this._payoffAmount.TabIndex = 273;
            this._payoffAmount.Leave += new System.EventHandler(this.OnTextBoxLeave);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(220, 169);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(79, 13);
            this.label8.TabIndex = 247;
            this.label8.Text = "Payoff Amount:";
            // 
            // _payoffOrdered
            // 
            this._payoffOrdered.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._payoffOrdered.Location = new System.Drawing.Point(228, 111);
            this._payoffOrdered.MaxLength = 50;
            this._payoffOrdered.Name = "_payoffOrdered";
            this._payoffOrdered.Size = new System.Drawing.Size(71, 20);
            this._payoffOrdered.TabIndex = 265;
            this._payoffOrdered.Leave += new System.EventHandler(this.OnTextBoxLeave);
            this._payoffOrdered.Enter += new System.EventHandler(this.OnTextBoxEnter);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(220, 95);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(81, 13);
            this.label7.TabIndex = 246;
            this.label7.Text = "Payoff Ordered:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(220, 22);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(169, 13);
            this.label5.TabIndex = 244;
            this.label5.Text = "Preliminary Title Search Received:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 319);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(164, 13);
            this.label4.TabIndex = 243;
            this.label4.Text = "Preliminary Title Search  Ordered:";
            // 
            // _allIntakeDocsFromSeller
            // 
            this._allIntakeDocsFromSeller.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._allIntakeDocsFromSeller.Location = new System.Drawing.Point(11, 111);
            this._allIntakeDocsFromSeller.MaxLength = 50;
            this._allIntakeDocsFromSeller.Name = "_allIntakeDocsFromSeller";
            this._allIntakeDocsFromSeller.Size = new System.Drawing.Size(71, 20);
            this._allIntakeDocsFromSeller.TabIndex = 245;
            this._allIntakeDocsFromSeller.Leave += new System.EventHandler(this.OnTextBoxLeave);
            this._allIntakeDocsFromSeller.Enter += new System.EventHandler(this.OnTextBoxEnter);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 95);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(134, 13);
            this.label3.TabIndex = 241;
            this.label3.Text = "All Intake Docs from Seller:";
            // 
            // _authorizationVerifiedReceived
            // 
            this._authorizationVerifiedReceived.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._authorizationVerifiedReceived.Location = new System.Drawing.Point(11, 74);
            this._authorizationVerifiedReceived.MaxLength = 50;
            this._authorizationVerifiedReceived.Name = "_authorizationVerifiedReceived";
            this._authorizationVerifiedReceived.Size = new System.Drawing.Size(71, 20);
            this._authorizationVerifiedReceived.TabIndex = 242;
            this._authorizationVerifiedReceived.Leave += new System.EventHandler(this.OnTextBoxLeave);
            this._authorizationVerifiedReceived.Enter += new System.EventHandler(this.OnTextBoxEnter);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 58);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(166, 13);
            this.label2.TabIndex = 240;
            this.label2.Text = "Authorization Verified / Received:";
            // 
            // _authorizationFaxedIn
            // 
            this._authorizationFaxedIn.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._authorizationFaxedIn.Location = new System.Drawing.Point(11, 37);
            this._authorizationFaxedIn.MaxLength = 50;
            this._authorizationFaxedIn.Name = "_authorizationFaxedIn";
            this._authorizationFaxedIn.Size = new System.Drawing.Size(71, 20);
            this._authorizationFaxedIn.TabIndex = 239;
            this._authorizationFaxedIn.Leave += new System.EventHandler(this.OnTextBoxLeave);
            this._authorizationFaxedIn.Enter += new System.EventHandler(this.OnTextBoxEnter);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(115, 13);
            this.label1.TabIndex = 238;
            this.label1.Text = "Authorization Faxed In:";
            // 
            // label59
            // 
            this.label59.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Italic | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label59.Location = new System.Drawing.Point(3, -1);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(100, 23);
            this.label59.TabIndex = 423;
            this.label59.Text = "Date Completed";
            // 
            // label9
            // 
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Italic | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(220, -1);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(100, 23);
            this.label9.TabIndex = 424;
            this.label9.Text = "Date Completed";
            // 
            // label10
            // 
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Italic | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(441, -1);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(100, 23);
            this.label10.TabIndex = 425;
            this.label10.Text = "Date Completed";
            // 
            // label11
            // 
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Italic | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(658, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(100, 23);
            this.label11.TabIndex = 426;
            this.label11.Text = "Date Completed";
            // 
            // _authorizationFaxedIn_Who
            // 
            this._authorizationFaxedIn_Who.FormattingEnabled = true;
            this._authorizationFaxedIn_Who.Location = new System.Drawing.Point(85, 36);
            this._authorizationFaxedIn_Who.Name = "_authorizationFaxedIn_Who";
            this._authorizationFaxedIn_Who.Size = new System.Drawing.Size(82, 21);
            this._authorizationFaxedIn_Who.TabIndex = 427;
            this._authorizationFaxedIn_Who.Leave += new System.EventHandler(this.OnDropDownLeave);
            // 
            // _authorizationFaxedIn_Assign
            // 
            this._authorizationFaxedIn_Assign.Location = new System.Drawing.Point(170, 34);
            this._authorizationFaxedIn_Assign.Name = "_authorizationFaxedIn_Assign";
            this._authorizationFaxedIn_Assign.Size = new System.Drawing.Size(25, 23);
            this._authorizationFaxedIn_Assign.TabIndex = 428;
            this._authorizationFaxedIn_Assign.Text = "...";
            this._authorizationFaxedIn_Assign.UseVisualStyleBackColor = true;
            // 
            // _authorizationFaxedIn_Checked
            // 
            this._authorizationFaxedIn_Checked.Checked = true;
            this._authorizationFaxedIn_Checked.CheckState = System.Windows.Forms.CheckState.Checked;
            this._authorizationFaxedIn_Checked.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._authorizationFaxedIn_Checked.Location = new System.Drawing.Point(199, 33);
            this._authorizationFaxedIn_Checked.Name = "_authorizationFaxedIn_Checked";
            this._authorizationFaxedIn_Checked.Size = new System.Drawing.Size(24, 24);
            this._authorizationFaxedIn_Checked.TabIndex = 429;
            // 
            // _authorizationVerifiedReceived_Assign
            // 
            this._authorizationVerifiedReceived_Assign.Location = new System.Drawing.Point(170, 72);
            this._authorizationVerifiedReceived_Assign.Name = "_authorizationVerifiedReceived_Assign";
            this._authorizationVerifiedReceived_Assign.Size = new System.Drawing.Size(25, 23);
            this._authorizationVerifiedReceived_Assign.TabIndex = 431;
            this._authorizationVerifiedReceived_Assign.Text = "...";
            this._authorizationVerifiedReceived_Assign.UseVisualStyleBackColor = true;
            // 
            // _authorizationVerifiedReceived_Who
            // 
            this._authorizationVerifiedReceived_Who.FormattingEnabled = true;
            this._authorizationVerifiedReceived_Who.Location = new System.Drawing.Point(85, 74);
            this._authorizationVerifiedReceived_Who.Name = "_authorizationVerifiedReceived_Who";
            this._authorizationVerifiedReceived_Who.Size = new System.Drawing.Size(82, 21);
            this._authorizationVerifiedReceived_Who.TabIndex = 430;
            this._authorizationVerifiedReceived_Who.Leave += new System.EventHandler(this.OnDropDownLeave);
            // 
            // _authorizationVerifiedReceived_Checked
            // 
            this._authorizationVerifiedReceived_Checked.Checked = true;
            this._authorizationVerifiedReceived_Checked.CheckState = System.Windows.Forms.CheckState.Checked;
            this._authorizationVerifiedReceived_Checked.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._authorizationVerifiedReceived_Checked.Location = new System.Drawing.Point(199, 71);
            this._authorizationVerifiedReceived_Checked.Name = "_authorizationVerifiedReceived_Checked";
            this._authorizationVerifiedReceived_Checked.Size = new System.Drawing.Size(24, 24);
            this._authorizationVerifiedReceived_Checked.TabIndex = 432;
            // 
            // _allIntakeDocsFromSeller_Assign
            // 
            this._allIntakeDocsFromSeller_Assign.Location = new System.Drawing.Point(170, 109);
            this._allIntakeDocsFromSeller_Assign.Name = "_allIntakeDocsFromSeller_Assign";
            this._allIntakeDocsFromSeller_Assign.Size = new System.Drawing.Size(25, 23);
            this._allIntakeDocsFromSeller_Assign.TabIndex = 434;
            this._allIntakeDocsFromSeller_Assign.Text = "...";
            this._allIntakeDocsFromSeller_Assign.UseVisualStyleBackColor = true;
            // 
            // _allIntakeDocsFromSeller_Who
            // 
            this._allIntakeDocsFromSeller_Who.FormattingEnabled = true;
            this._allIntakeDocsFromSeller_Who.Location = new System.Drawing.Point(85, 111);
            this._allIntakeDocsFromSeller_Who.Name = "_allIntakeDocsFromSeller_Who";
            this._allIntakeDocsFromSeller_Who.Size = new System.Drawing.Size(82, 21);
            this._allIntakeDocsFromSeller_Who.TabIndex = 433;
            this._allIntakeDocsFromSeller_Who.Leave += new System.EventHandler(this.OnDropDownLeave);
            // 
            // _allIntakeDocsFromSeller_Checked
            // 
            this._allIntakeDocsFromSeller_Checked.Checked = true;
            this._allIntakeDocsFromSeller_Checked.CheckState = System.Windows.Forms.CheckState.Checked;
            this._allIntakeDocsFromSeller_Checked.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._allIntakeDocsFromSeller_Checked.Location = new System.Drawing.Point(199, 108);
            this._allIntakeDocsFromSeller_Checked.Name = "_allIntakeDocsFromSeller_Checked";
            this._allIntakeDocsFromSeller_Checked.Size = new System.Drawing.Size(24, 24);
            this._allIntakeDocsFromSeller_Checked.TabIndex = 435;
            // 
            // _requiredDocsScanned_Assign
            // 
            this._requiredDocsScanned_Assign.Location = new System.Drawing.Point(170, 145);
            this._requiredDocsScanned_Assign.Name = "_requiredDocsScanned_Assign";
            this._requiredDocsScanned_Assign.Size = new System.Drawing.Size(25, 23);
            this._requiredDocsScanned_Assign.TabIndex = 437;
            this._requiredDocsScanned_Assign.Text = "...";
            this._requiredDocsScanned_Assign.UseVisualStyleBackColor = true;
            // 
            // _requiredDocsScanned_Who
            // 
            this._requiredDocsScanned_Who.FormattingEnabled = true;
            this._requiredDocsScanned_Who.Location = new System.Drawing.Point(85, 147);
            this._requiredDocsScanned_Who.Name = "_requiredDocsScanned_Who";
            this._requiredDocsScanned_Who.Size = new System.Drawing.Size(82, 21);
            this._requiredDocsScanned_Who.TabIndex = 436;
            this._requiredDocsScanned_Who.Leave += new System.EventHandler(this.OnDropDownLeave);
            // 
            // _requiredDocsScanned_Checked
            // 
            this._requiredDocsScanned_Checked.Checked = true;
            this._requiredDocsScanned_Checked.CheckState = System.Windows.Forms.CheckState.Checked;
            this._requiredDocsScanned_Checked.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._requiredDocsScanned_Checked.Location = new System.Drawing.Point(199, 145);
            this._requiredDocsScanned_Checked.Name = "_requiredDocsScanned_Checked";
            this._requiredDocsScanned_Checked.Size = new System.Drawing.Size(24, 24);
            this._requiredDocsScanned_Checked.TabIndex = 438;
            // 
            // _requiredDocsRecorded_Assign
            // 
            this._requiredDocsRecorded_Assign.Location = new System.Drawing.Point(170, 183);
            this._requiredDocsRecorded_Assign.Name = "_requiredDocsRecorded_Assign";
            this._requiredDocsRecorded_Assign.Size = new System.Drawing.Size(25, 23);
            this._requiredDocsRecorded_Assign.TabIndex = 440;
            this._requiredDocsRecorded_Assign.Text = "...";
            this._requiredDocsRecorded_Assign.UseVisualStyleBackColor = true;
            // 
            // _requiredDocsRecorded_Who
            // 
            this._requiredDocsRecorded_Who.FormattingEnabled = true;
            this._requiredDocsRecorded_Who.Location = new System.Drawing.Point(85, 185);
            this._requiredDocsRecorded_Who.Name = "_requiredDocsRecorded_Who";
            this._requiredDocsRecorded_Who.Size = new System.Drawing.Size(82, 21);
            this._requiredDocsRecorded_Who.TabIndex = 439;
            this._requiredDocsRecorded_Who.Leave += new System.EventHandler(this.OnDropDownLeave);
            // 
            // _requiredDocsRecorded_Checked
            // 
            this._requiredDocsRecorded_Checked.Checked = true;
            this._requiredDocsRecorded_Checked.CheckState = System.Windows.Forms.CheckState.Checked;
            this._requiredDocsRecorded_Checked.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._requiredDocsRecorded_Checked.Location = new System.Drawing.Point(199, 182);
            this._requiredDocsRecorded_Checked.Name = "_requiredDocsRecorded_Checked";
            this._requiredDocsRecorded_Checked.Size = new System.Drawing.Size(24, 24);
            this._requiredDocsRecorded_Checked.TabIndex = 441;
            // 
            // _returnOriginalsToHomeOwner_Assign
            // 
            this._returnOriginalsToHomeOwner_Assign.Location = new System.Drawing.Point(170, 220);
            this._returnOriginalsToHomeOwner_Assign.Name = "_returnOriginalsToHomeOwner_Assign";
            this._returnOriginalsToHomeOwner_Assign.Size = new System.Drawing.Size(25, 23);
            this._returnOriginalsToHomeOwner_Assign.TabIndex = 443;
            this._returnOriginalsToHomeOwner_Assign.Text = "...";
            this._returnOriginalsToHomeOwner_Assign.UseVisualStyleBackColor = true;
            // 
            // _returnOriginalsToHomeOwner_Who
            // 
            this._returnOriginalsToHomeOwner_Who.FormattingEnabled = true;
            this._returnOriginalsToHomeOwner_Who.Location = new System.Drawing.Point(85, 222);
            this._returnOriginalsToHomeOwner_Who.Name = "_returnOriginalsToHomeOwner_Who";
            this._returnOriginalsToHomeOwner_Who.Size = new System.Drawing.Size(82, 21);
            this._returnOriginalsToHomeOwner_Who.TabIndex = 442;
            this._returnOriginalsToHomeOwner_Who.Leave += new System.EventHandler(this.OnDropDownLeave);
            // 
            // _returnOriginalsToHomeOwner_Checked
            // 
            this._returnOriginalsToHomeOwner_Checked.Checked = true;
            this._returnOriginalsToHomeOwner_Checked.CheckState = System.Windows.Forms.CheckState.Checked;
            this._returnOriginalsToHomeOwner_Checked.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._returnOriginalsToHomeOwner_Checked.Location = new System.Drawing.Point(199, 220);
            this._returnOriginalsToHomeOwner_Checked.Name = "_returnOriginalsToHomeOwner_Checked";
            this._returnOriginalsToHomeOwner_Checked.Size = new System.Drawing.Size(24, 24);
            this._returnOriginalsToHomeOwner_Checked.TabIndex = 444;
            // 
            // _allInfoEnteredInProfitGrabber_Assign
            // 
            this._allInfoEnteredInProfitGrabber_Assign.Location = new System.Drawing.Point(170, 256);
            this._allInfoEnteredInProfitGrabber_Assign.Name = "_allInfoEnteredInProfitGrabber_Assign";
            this._allInfoEnteredInProfitGrabber_Assign.Size = new System.Drawing.Size(25, 23);
            this._allInfoEnteredInProfitGrabber_Assign.TabIndex = 446;
            this._allInfoEnteredInProfitGrabber_Assign.Text = "...";
            this._allInfoEnteredInProfitGrabber_Assign.UseVisualStyleBackColor = true;
            // 
            // _allInfoEnteredInProfitGrabber_Who
            // 
            this._allInfoEnteredInProfitGrabber_Who.FormattingEnabled = true;
            this._allInfoEnteredInProfitGrabber_Who.Location = new System.Drawing.Point(85, 258);
            this._allInfoEnteredInProfitGrabber_Who.Name = "_allInfoEnteredInProfitGrabber_Who";
            this._allInfoEnteredInProfitGrabber_Who.Size = new System.Drawing.Size(82, 21);
            this._allInfoEnteredInProfitGrabber_Who.TabIndex = 445;
            this._allInfoEnteredInProfitGrabber_Who.Leave += new System.EventHandler(this.OnDropDownLeave);
            // 
            // _allInfoEnteredInProfitGrabber_Checked
            // 
            this._allInfoEnteredInProfitGrabber_Checked.Checked = true;
            this._allInfoEnteredInProfitGrabber_Checked.CheckState = System.Windows.Forms.CheckState.Checked;
            this._allInfoEnteredInProfitGrabber_Checked.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._allInfoEnteredInProfitGrabber_Checked.Location = new System.Drawing.Point(199, 255);
            this._allInfoEnteredInProfitGrabber_Checked.Name = "_allInfoEnteredInProfitGrabber_Checked";
            this._allInfoEnteredInProfitGrabber_Checked.Size = new System.Drawing.Size(24, 24);
            this._allInfoEnteredInProfitGrabber_Checked.TabIndex = 447;
            // 
            // _propertyProfileInProfitGrabber_Assign
            // 
            this._propertyProfileInProfitGrabber_Assign.Location = new System.Drawing.Point(170, 296);
            this._propertyProfileInProfitGrabber_Assign.Name = "_propertyProfileInProfitGrabber_Assign";
            this._propertyProfileInProfitGrabber_Assign.Size = new System.Drawing.Size(25, 23);
            this._propertyProfileInProfitGrabber_Assign.TabIndex = 449;
            this._propertyProfileInProfitGrabber_Assign.Text = "...";
            this._propertyProfileInProfitGrabber_Assign.UseVisualStyleBackColor = true;
            // 
            // _propertyProfileInProfitGrabber_Who
            // 
            this._propertyProfileInProfitGrabber_Who.FormattingEnabled = true;
            this._propertyProfileInProfitGrabber_Who.Location = new System.Drawing.Point(85, 297);
            this._propertyProfileInProfitGrabber_Who.Name = "_propertyProfileInProfitGrabber_Who";
            this._propertyProfileInProfitGrabber_Who.Size = new System.Drawing.Size(82, 21);
            this._propertyProfileInProfitGrabber_Who.TabIndex = 448;
            this._propertyProfileInProfitGrabber_Who.Leave += new System.EventHandler(this.OnDropDownLeave);
            // 
            // _propertyProfileInProfitGrabber_Checked
            // 
            this._propertyProfileInProfitGrabber_Checked.Checked = true;
            this._propertyProfileInProfitGrabber_Checked.CheckState = System.Windows.Forms.CheckState.Checked;
            this._propertyProfileInProfitGrabber_Checked.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._propertyProfileInProfitGrabber_Checked.Location = new System.Drawing.Point(199, 295);
            this._propertyProfileInProfitGrabber_Checked.Name = "_propertyProfileInProfitGrabber_Checked";
            this._propertyProfileInProfitGrabber_Checked.Size = new System.Drawing.Size(24, 24);
            this._propertyProfileInProfitGrabber_Checked.TabIndex = 450;
            // 
            // _preliminaryTitleSearchedOrdered_Assign
            // 
            this._preliminaryTitleSearchedOrdered_Assign.Location = new System.Drawing.Point(170, 332);
            this._preliminaryTitleSearchedOrdered_Assign.Name = "_preliminaryTitleSearchedOrdered_Assign";
            this._preliminaryTitleSearchedOrdered_Assign.Size = new System.Drawing.Size(25, 23);
            this._preliminaryTitleSearchedOrdered_Assign.TabIndex = 452;
            this._preliminaryTitleSearchedOrdered_Assign.Text = "...";
            this._preliminaryTitleSearchedOrdered_Assign.UseVisualStyleBackColor = true;
            // 
            // _preliminaryTitleSearchedOrdered_Who
            // 
            this._preliminaryTitleSearchedOrdered_Who.FormattingEnabled = true;
            this._preliminaryTitleSearchedOrdered_Who.Location = new System.Drawing.Point(85, 334);
            this._preliminaryTitleSearchedOrdered_Who.Name = "_preliminaryTitleSearchedOrdered_Who";
            this._preliminaryTitleSearchedOrdered_Who.Size = new System.Drawing.Size(82, 21);
            this._preliminaryTitleSearchedOrdered_Who.TabIndex = 451;
            this._preliminaryTitleSearchedOrdered_Who.Leave += new System.EventHandler(this.OnDropDownLeave);
            // 
            // _preliminaryTitleSearchedOrdered_Checked
            // 
            this._preliminaryTitleSearchedOrdered_Checked.Checked = true;
            this._preliminaryTitleSearchedOrdered_Checked.CheckState = System.Windows.Forms.CheckState.Checked;
            this._preliminaryTitleSearchedOrdered_Checked.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._preliminaryTitleSearchedOrdered_Checked.Location = new System.Drawing.Point(199, 331);
            this._preliminaryTitleSearchedOrdered_Checked.Name = "_preliminaryTitleSearchedOrdered_Checked";
            this._preliminaryTitleSearchedOrdered_Checked.Size = new System.Drawing.Size(24, 24);
            this._preliminaryTitleSearchedOrdered_Checked.TabIndex = 453;
            // 
            // _preliminaryTitleSearchReceived_Assign
            // 
            this._preliminaryTitleSearchReceived_Assign.Location = new System.Drawing.Point(389, 35);
            this._preliminaryTitleSearchReceived_Assign.Name = "_preliminaryTitleSearchReceived_Assign";
            this._preliminaryTitleSearchReceived_Assign.Size = new System.Drawing.Size(25, 23);
            this._preliminaryTitleSearchReceived_Assign.TabIndex = 455;
            this._preliminaryTitleSearchReceived_Assign.Text = "...";
            this._preliminaryTitleSearchReceived_Assign.UseVisualStyleBackColor = true;
            // 
            // _preliminaryTitleSearchReceived_Who
            // 
            this._preliminaryTitleSearchReceived_Who.FormattingEnabled = true;
            this._preliminaryTitleSearchReceived_Who.Location = new System.Drawing.Point(303, 36);
            this._preliminaryTitleSearchReceived_Who.Name = "_preliminaryTitleSearchReceived_Who";
            this._preliminaryTitleSearchReceived_Who.Size = new System.Drawing.Size(82, 21);
            this._preliminaryTitleSearchReceived_Who.TabIndex = 454;
            this._preliminaryTitleSearchReceived_Who.Leave += new System.EventHandler(this.OnDropDownLeave);
            // 
            // _preliminaryTitleSearchReceived_Checked
            // 
            this._preliminaryTitleSearchReceived_Checked.Checked = true;
            this._preliminaryTitleSearchReceived_Checked.CheckState = System.Windows.Forms.CheckState.Checked;
            this._preliminaryTitleSearchReceived_Checked.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._preliminaryTitleSearchReceived_Checked.Location = new System.Drawing.Point(419, 33);
            this._preliminaryTitleSearchReceived_Checked.Name = "_preliminaryTitleSearchReceived_Checked";
            this._preliminaryTitleSearchReceived_Checked.Size = new System.Drawing.Size(24, 24);
            this._preliminaryTitleSearchReceived_Checked.TabIndex = 456;
            // 
            // _anyTitleIssues_Assign
            // 
            this._anyTitleIssues_Assign.Location = new System.Drawing.Point(389, 71);
            this._anyTitleIssues_Assign.Name = "_anyTitleIssues_Assign";
            this._anyTitleIssues_Assign.Size = new System.Drawing.Size(25, 23);
            this._anyTitleIssues_Assign.TabIndex = 458;
            this._anyTitleIssues_Assign.Text = "...";
            this._anyTitleIssues_Assign.UseVisualStyleBackColor = true;
            // 
            // _anyTitleIssues_Who
            // 
            this._anyTitleIssues_Who.FormattingEnabled = true;
            this._anyTitleIssues_Who.Location = new System.Drawing.Point(303, 73);
            this._anyTitleIssues_Who.Name = "_anyTitleIssues_Who";
            this._anyTitleIssues_Who.Size = new System.Drawing.Size(82, 21);
            this._anyTitleIssues_Who.TabIndex = 457;
            this._anyTitleIssues_Who.Leave += new System.EventHandler(this.OnDropDownLeave);
            // 
            // _anyTitleIssues_Checked
            // 
            this._anyTitleIssues_Checked.Checked = true;
            this._anyTitleIssues_Checked.CheckState = System.Windows.Forms.CheckState.Checked;
            this._anyTitleIssues_Checked.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._anyTitleIssues_Checked.Location = new System.Drawing.Point(419, 70);
            this._anyTitleIssues_Checked.Name = "_anyTitleIssues_Checked";
            this._anyTitleIssues_Checked.Size = new System.Drawing.Size(24, 24);
            this._anyTitleIssues_Checked.TabIndex = 459;
            // 
            // _payoffOrdered_Who
            // 
            this._payoffOrdered_Who.FormattingEnabled = true;
            this._payoffOrdered_Who.Location = new System.Drawing.Point(303, 110);
            this._payoffOrdered_Who.Name = "_payoffOrdered_Who";
            this._payoffOrdered_Who.Size = new System.Drawing.Size(82, 21);
            this._payoffOrdered_Who.TabIndex = 460;
            this._payoffOrdered_Who.Leave += new System.EventHandler(this.OnDropDownLeave);
            // 
            // _payoffOrdered_Assign
            // 
            this._payoffOrdered_Assign.Location = new System.Drawing.Point(389, 108);
            this._payoffOrdered_Assign.Name = "_payoffOrdered_Assign";
            this._payoffOrdered_Assign.Size = new System.Drawing.Size(25, 23);
            this._payoffOrdered_Assign.TabIndex = 461;
            this._payoffOrdered_Assign.Text = "...";
            this._payoffOrdered_Assign.UseVisualStyleBackColor = true;
            // 
            // _payoffOrdered_Checked
            // 
            this._payoffOrdered_Checked.Checked = true;
            this._payoffOrdered_Checked.CheckState = System.Windows.Forms.CheckState.Checked;
            this._payoffOrdered_Checked.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._payoffOrdered_Checked.Location = new System.Drawing.Point(419, 107);
            this._payoffOrdered_Checked.Name = "_payoffOrdered_Checked";
            this._payoffOrdered_Checked.Size = new System.Drawing.Size(24, 24);
            this._payoffOrdered_Checked.TabIndex = 462;
            // 
            // _reinstatementOrdered_Assign
            // 
            this._reinstatementOrdered_Assign.Location = new System.Drawing.Point(389, 146);
            this._reinstatementOrdered_Assign.Name = "_reinstatementOrdered_Assign";
            this._reinstatementOrdered_Assign.Size = new System.Drawing.Size(25, 23);
            this._reinstatementOrdered_Assign.TabIndex = 464;
            this._reinstatementOrdered_Assign.Text = "...";
            this._reinstatementOrdered_Assign.UseVisualStyleBackColor = true;
            // 
            // _reinstatementOrdered_Who
            // 
            this._reinstatementOrdered_Who.FormattingEnabled = true;
            this._reinstatementOrdered_Who.Location = new System.Drawing.Point(303, 148);
            this._reinstatementOrdered_Who.Name = "_reinstatementOrdered_Who";
            this._reinstatementOrdered_Who.Size = new System.Drawing.Size(82, 21);
            this._reinstatementOrdered_Who.TabIndex = 463;
            this._reinstatementOrdered_Who.Leave += new System.EventHandler(this.OnDropDownLeave);
            // 
            // _reinstatementOrdered_Checked
            // 
            this._reinstatementOrdered_Checked.Checked = true;
            this._reinstatementOrdered_Checked.CheckState = System.Windows.Forms.CheckState.Checked;
            this._reinstatementOrdered_Checked.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._reinstatementOrdered_Checked.Location = new System.Drawing.Point(419, 145);
            this._reinstatementOrdered_Checked.Name = "_reinstatementOrdered_Checked";
            this._reinstatementOrdered_Checked.Size = new System.Drawing.Size(24, 24);
            this._reinstatementOrdered_Checked.TabIndex = 465;
            // 
            // _ssPackedComplete_Checked
            // 
            this._ssPackedComplete_Checked.Checked = true;
            this._ssPackedComplete_Checked.CheckState = System.Windows.Forms.CheckState.Checked;
            this._ssPackedComplete_Checked.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._ssPackedComplete_Checked.Location = new System.Drawing.Point(419, 256);
            this._ssPackedComplete_Checked.Name = "_ssPackedComplete_Checked";
            this._ssPackedComplete_Checked.Size = new System.Drawing.Size(24, 24);
            this._ssPackedComplete_Checked.TabIndex = 468;
            // 
            // _ssPackedComplete_Assign
            // 
            this._ssPackedComplete_Assign.Location = new System.Drawing.Point(389, 257);
            this._ssPackedComplete_Assign.Name = "_ssPackedComplete_Assign";
            this._ssPackedComplete_Assign.Size = new System.Drawing.Size(25, 23);
            this._ssPackedComplete_Assign.TabIndex = 467;
            this._ssPackedComplete_Assign.Text = "...";
            this._ssPackedComplete_Assign.UseVisualStyleBackColor = true;
            // 
            // _ssPackedComplete_Who
            // 
            this._ssPackedComplete_Who.FormattingEnabled = true;
            this._ssPackedComplete_Who.Location = new System.Drawing.Point(303, 259);
            this._ssPackedComplete_Who.Name = "_ssPackedComplete_Who";
            this._ssPackedComplete_Who.Size = new System.Drawing.Size(82, 21);
            this._ssPackedComplete_Who.TabIndex = 466;
            this._ssPackedComplete_Who.Leave += new System.EventHandler(this.OnDropDownLeave);
            // 
            // _ssPacketReviewed_Checked
            // 
            this._ssPacketReviewed_Checked.Checked = true;
            this._ssPacketReviewed_Checked.CheckState = System.Windows.Forms.CheckState.Checked;
            this._ssPacketReviewed_Checked.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._ssPacketReviewed_Checked.Location = new System.Drawing.Point(419, 294);
            this._ssPacketReviewed_Checked.Name = "_ssPacketReviewed_Checked";
            this._ssPacketReviewed_Checked.Size = new System.Drawing.Size(24, 24);
            this._ssPacketReviewed_Checked.TabIndex = 471;
            // 
            // _ssPacketReviewed_Assign
            // 
            this._ssPacketReviewed_Assign.Location = new System.Drawing.Point(389, 295);
            this._ssPacketReviewed_Assign.Name = "_ssPacketReviewed_Assign";
            this._ssPacketReviewed_Assign.Size = new System.Drawing.Size(25, 23);
            this._ssPacketReviewed_Assign.TabIndex = 470;
            this._ssPacketReviewed_Assign.Text = "...";
            this._ssPacketReviewed_Assign.UseVisualStyleBackColor = true;
            // 
            // _ssPacketReviewed_Who
            // 
            this._ssPacketReviewed_Who.FormattingEnabled = true;
            this._ssPacketReviewed_Who.Location = new System.Drawing.Point(303, 297);
            this._ssPacketReviewed_Who.Name = "_ssPacketReviewed_Who";
            this._ssPacketReviewed_Who.Size = new System.Drawing.Size(82, 21);
            this._ssPacketReviewed_Who.TabIndex = 469;
            this._ssPacketReviewed_Who.Leave += new System.EventHandler(this.OnDropDownLeave);
            // 
            // _ssPacketSentToLienHolder_Checked
            // 
            this._ssPacketSentToLienHolder_Checked.Checked = true;
            this._ssPacketSentToLienHolder_Checked.CheckState = System.Windows.Forms.CheckState.Checked;
            this._ssPacketSentToLienHolder_Checked.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._ssPacketSentToLienHolder_Checked.Location = new System.Drawing.Point(419, 330);
            this._ssPacketSentToLienHolder_Checked.Name = "_ssPacketSentToLienHolder_Checked";
            this._ssPacketSentToLienHolder_Checked.Size = new System.Drawing.Size(24, 24);
            this._ssPacketSentToLienHolder_Checked.TabIndex = 474;
            // 
            // _ssPacketSentToLienHolder_Assign
            // 
            this._ssPacketSentToLienHolder_Assign.Location = new System.Drawing.Point(389, 331);
            this._ssPacketSentToLienHolder_Assign.Name = "_ssPacketSentToLienHolder_Assign";
            this._ssPacketSentToLienHolder_Assign.Size = new System.Drawing.Size(25, 23);
            this._ssPacketSentToLienHolder_Assign.TabIndex = 473;
            this._ssPacketSentToLienHolder_Assign.Text = "...";
            this._ssPacketSentToLienHolder_Assign.UseVisualStyleBackColor = true;
            // 
            // _ssPacketSentToLienHolder_Who
            // 
            this._ssPacketSentToLienHolder_Who.FormattingEnabled = true;
            this._ssPacketSentToLienHolder_Who.Location = new System.Drawing.Point(303, 333);
            this._ssPacketSentToLienHolder_Who.Name = "_ssPacketSentToLienHolder_Who";
            this._ssPacketSentToLienHolder_Who.Size = new System.Drawing.Size(82, 21);
            this._ssPacketSentToLienHolder_Who.TabIndex = 472;
            this._ssPacketSentToLienHolder_Who.Leave += new System.EventHandler(this.OnDropDownLeave);
            // 
            // _ssPacketVerifiedReceived_Checked
            // 
            this._ssPacketVerifiedReceived_Checked.Checked = true;
            this._ssPacketVerifiedReceived_Checked.CheckState = System.Windows.Forms.CheckState.Checked;
            this._ssPacketVerifiedReceived_Checked.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._ssPacketVerifiedReceived_Checked.Location = new System.Drawing.Point(638, 33);
            this._ssPacketVerifiedReceived_Checked.Name = "_ssPacketVerifiedReceived_Checked";
            this._ssPacketVerifiedReceived_Checked.Size = new System.Drawing.Size(24, 24);
            this._ssPacketVerifiedReceived_Checked.TabIndex = 477;
            // 
            // _ssPacketVerifiedReceived_Assign
            // 
            this._ssPacketVerifiedReceived_Assign.Location = new System.Drawing.Point(609, 35);
            this._ssPacketVerifiedReceived_Assign.Name = "_ssPacketVerifiedReceived_Assign";
            this._ssPacketVerifiedReceived_Assign.Size = new System.Drawing.Size(25, 23);
            this._ssPacketVerifiedReceived_Assign.TabIndex = 476;
            this._ssPacketVerifiedReceived_Assign.Text = "...";
            this._ssPacketVerifiedReceived_Assign.UseVisualStyleBackColor = true;
            // 
            // _ssPacketVerifiedReceived_Who
            // 
            this._ssPacketVerifiedReceived_Who.FormattingEnabled = true;
            this._ssPacketVerifiedReceived_Who.Location = new System.Drawing.Point(523, 36);
            this._ssPacketVerifiedReceived_Who.Name = "_ssPacketVerifiedReceived_Who";
            this._ssPacketVerifiedReceived_Who.Size = new System.Drawing.Size(82, 21);
            this._ssPacketVerifiedReceived_Who.TabIndex = 475;
            this._ssPacketVerifiedReceived_Who.Leave += new System.EventHandler(this.OnDropDownLeave);
            // 
            // _prelimInspectionOrdered_Assign
            // 
            this._prelimInspectionOrdered_Assign.Location = new System.Drawing.Point(609, 71);
            this._prelimInspectionOrdered_Assign.Name = "_prelimInspectionOrdered_Assign";
            this._prelimInspectionOrdered_Assign.Size = new System.Drawing.Size(25, 23);
            this._prelimInspectionOrdered_Assign.TabIndex = 479;
            this._prelimInspectionOrdered_Assign.Text = "...";
            this._prelimInspectionOrdered_Assign.UseVisualStyleBackColor = true;
            // 
            // _prelimInspectionOrdered_Who
            // 
            this._prelimInspectionOrdered_Who.FormattingEnabled = true;
            this._prelimInspectionOrdered_Who.Location = new System.Drawing.Point(523, 73);
            this._prelimInspectionOrdered_Who.Name = "_prelimInspectionOrdered_Who";
            this._prelimInspectionOrdered_Who.Size = new System.Drawing.Size(82, 21);
            this._prelimInspectionOrdered_Who.TabIndex = 478;
            this._prelimInspectionOrdered_Who.Leave += new System.EventHandler(this.OnDropDownLeave);
            // 
            // _prelimInspectionOrdered_Checked
            // 
            this._prelimInspectionOrdered_Checked.Checked = true;
            this._prelimInspectionOrdered_Checked.CheckState = System.Windows.Forms.CheckState.Checked;
            this._prelimInspectionOrdered_Checked.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._prelimInspectionOrdered_Checked.Location = new System.Drawing.Point(638, 72);
            this._prelimInspectionOrdered_Checked.Name = "_prelimInspectionOrdered_Checked";
            this._prelimInspectionOrdered_Checked.Size = new System.Drawing.Size(24, 24);
            this._prelimInspectionOrdered_Checked.TabIndex = 480;
            // 
            // _prelimInspectionDate_Assign
            // 
            this._prelimInspectionDate_Assign.Location = new System.Drawing.Point(609, 107);
            this._prelimInspectionDate_Assign.Name = "_prelimInspectionDate_Assign";
            this._prelimInspectionDate_Assign.Size = new System.Drawing.Size(25, 23);
            this._prelimInspectionDate_Assign.TabIndex = 482;
            this._prelimInspectionDate_Assign.Text = "...";
            this._prelimInspectionDate_Assign.UseVisualStyleBackColor = true;
            // 
            // _prelimInspectionDate_Who
            // 
            this._prelimInspectionDate_Who.FormattingEnabled = true;
            this._prelimInspectionDate_Who.Location = new System.Drawing.Point(523, 109);
            this._prelimInspectionDate_Who.Name = "_prelimInspectionDate_Who";
            this._prelimInspectionDate_Who.Size = new System.Drawing.Size(82, 21);
            this._prelimInspectionDate_Who.TabIndex = 481;
            this._prelimInspectionDate_Who.Leave += new System.EventHandler(this.OnDropDownLeave);
            // 
            // _prelimInspectionDate_Checked
            // 
            this._prelimInspectionDate_Checked.Checked = true;
            this._prelimInspectionDate_Checked.CheckState = System.Windows.Forms.CheckState.Checked;
            this._prelimInspectionDate_Checked.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._prelimInspectionDate_Checked.Location = new System.Drawing.Point(638, 106);
            this._prelimInspectionDate_Checked.Name = "_prelimInspectionDate_Checked";
            this._prelimInspectionDate_Checked.Size = new System.Drawing.Size(24, 24);
            this._prelimInspectionDate_Checked.TabIndex = 483;
            // 
            // _bpoPacketComplete_Assign
            // 
            this._bpoPacketComplete_Assign.Location = new System.Drawing.Point(609, 146);
            this._bpoPacketComplete_Assign.Name = "_bpoPacketComplete_Assign";
            this._bpoPacketComplete_Assign.Size = new System.Drawing.Size(25, 23);
            this._bpoPacketComplete_Assign.TabIndex = 485;
            this._bpoPacketComplete_Assign.Text = "...";
            this._bpoPacketComplete_Assign.UseVisualStyleBackColor = true;
            // 
            // _bpoPacketComplete_Who
            // 
            this._bpoPacketComplete_Who.FormattingEnabled = true;
            this._bpoPacketComplete_Who.Location = new System.Drawing.Point(523, 148);
            this._bpoPacketComplete_Who.Name = "_bpoPacketComplete_Who";
            this._bpoPacketComplete_Who.Size = new System.Drawing.Size(82, 21);
            this._bpoPacketComplete_Who.TabIndex = 484;
            this._bpoPacketComplete_Who.Leave += new System.EventHandler(this.OnDropDownLeave);
            // 
            // _bpoPacketComplete_Checked
            // 
            this._bpoPacketComplete_Checked.Checked = true;
            this._bpoPacketComplete_Checked.CheckState = System.Windows.Forms.CheckState.Checked;
            this._bpoPacketComplete_Checked.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._bpoPacketComplete_Checked.Location = new System.Drawing.Point(638, 145);
            this._bpoPacketComplete_Checked.Name = "_bpoPacketComplete_Checked";
            this._bpoPacketComplete_Checked.Size = new System.Drawing.Size(24, 24);
            this._bpoPacketComplete_Checked.TabIndex = 486;
            // 
            // _negotiatorAssigned_Assign
            // 
            this._negotiatorAssigned_Assign.Location = new System.Drawing.Point(609, 182);
            this._negotiatorAssigned_Assign.Name = "_negotiatorAssigned_Assign";
            this._negotiatorAssigned_Assign.Size = new System.Drawing.Size(25, 23);
            this._negotiatorAssigned_Assign.TabIndex = 488;
            this._negotiatorAssigned_Assign.Text = "...";
            this._negotiatorAssigned_Assign.UseVisualStyleBackColor = true;
            // 
            // _negotiatorAssigned_Who
            // 
            this._negotiatorAssigned_Who.FormattingEnabled = true;
            this._negotiatorAssigned_Who.Location = new System.Drawing.Point(523, 184);
            this._negotiatorAssigned_Who.Name = "_negotiatorAssigned_Who";
            this._negotiatorAssigned_Who.Size = new System.Drawing.Size(82, 21);
            this._negotiatorAssigned_Who.TabIndex = 487;
            this._negotiatorAssigned_Who.Leave += new System.EventHandler(this.OnDropDownLeave);
            // 
            // _negotiatorAssigned_Checked
            // 
            this._negotiatorAssigned_Checked.Checked = true;
            this._negotiatorAssigned_Checked.CheckState = System.Windows.Forms.CheckState.Checked;
            this._negotiatorAssigned_Checked.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._negotiatorAssigned_Checked.Location = new System.Drawing.Point(638, 181);
            this._negotiatorAssigned_Checked.Name = "_negotiatorAssigned_Checked";
            this._negotiatorAssigned_Checked.Size = new System.Drawing.Size(24, 24);
            this._negotiatorAssigned_Checked.TabIndex = 489;
            // 
            // _bpoAppraisalOrdered_Assign
            // 
            this._bpoAppraisalOrdered_Assign.Location = new System.Drawing.Point(609, 220);
            this._bpoAppraisalOrdered_Assign.Name = "_bpoAppraisalOrdered_Assign";
            this._bpoAppraisalOrdered_Assign.Size = new System.Drawing.Size(25, 23);
            this._bpoAppraisalOrdered_Assign.TabIndex = 491;
            this._bpoAppraisalOrdered_Assign.Text = "...";
            this._bpoAppraisalOrdered_Assign.UseVisualStyleBackColor = true;
            // 
            // _bpoAppraisalOrdered_Who
            // 
            this._bpoAppraisalOrdered_Who.FormattingEnabled = true;
            this._bpoAppraisalOrdered_Who.Location = new System.Drawing.Point(523, 221);
            this._bpoAppraisalOrdered_Who.Name = "_bpoAppraisalOrdered_Who";
            this._bpoAppraisalOrdered_Who.Size = new System.Drawing.Size(82, 21);
            this._bpoAppraisalOrdered_Who.TabIndex = 490;
            this._bpoAppraisalOrdered_Who.Leave += new System.EventHandler(this.OnDropDownLeave);
            // 
            // _bpoAppraisalOrdered_Checked
            // 
            this._bpoAppraisalOrdered_Checked.Checked = true;
            this._bpoAppraisalOrdered_Checked.CheckState = System.Windows.Forms.CheckState.Checked;
            this._bpoAppraisalOrdered_Checked.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._bpoAppraisalOrdered_Checked.Location = new System.Drawing.Point(636, 220);
            this._bpoAppraisalOrdered_Checked.Name = "_bpoAppraisalOrdered_Checked";
            this._bpoAppraisalOrdered_Checked.Size = new System.Drawing.Size(24, 24);
            this._bpoAppraisalOrdered_Checked.TabIndex = 492;
            // 
            // _bpoAppraisalCompleted_Assign
            // 
            this._bpoAppraisalCompleted_Assign.Location = new System.Drawing.Point(609, 257);
            this._bpoAppraisalCompleted_Assign.Name = "_bpoAppraisalCompleted_Assign";
            this._bpoAppraisalCompleted_Assign.Size = new System.Drawing.Size(25, 23);
            this._bpoAppraisalCompleted_Assign.TabIndex = 494;
            this._bpoAppraisalCompleted_Assign.Text = "...";
            this._bpoAppraisalCompleted_Assign.UseVisualStyleBackColor = true;
            // 
            // _bpoAppraisalCompleted_Who
            // 
            this._bpoAppraisalCompleted_Who.FormattingEnabled = true;
            this._bpoAppraisalCompleted_Who.Location = new System.Drawing.Point(523, 259);
            this._bpoAppraisalCompleted_Who.Name = "_bpoAppraisalCompleted_Who";
            this._bpoAppraisalCompleted_Who.Size = new System.Drawing.Size(82, 21);
            this._bpoAppraisalCompleted_Who.TabIndex = 493;
            this._bpoAppraisalCompleted_Who.Leave += new System.EventHandler(this.OnDropDownLeave);
            // 
            // _bpoAppraisalCompleted_Checked
            // 
            this._bpoAppraisalCompleted_Checked.Checked = true;
            this._bpoAppraisalCompleted_Checked.CheckState = System.Windows.Forms.CheckState.Checked;
            this._bpoAppraisalCompleted_Checked.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._bpoAppraisalCompleted_Checked.Location = new System.Drawing.Point(636, 256);
            this._bpoAppraisalCompleted_Checked.Name = "_bpoAppraisalCompleted_Checked";
            this._bpoAppraisalCompleted_Checked.Size = new System.Drawing.Size(24, 24);
            this._bpoAppraisalCompleted_Checked.TabIndex = 495;
            // 
            // _bpoAppraisalValue_Assign
            // 
            this._bpoAppraisalValue_Assign.Location = new System.Drawing.Point(609, 296);
            this._bpoAppraisalValue_Assign.Name = "_bpoAppraisalValue_Assign";
            this._bpoAppraisalValue_Assign.Size = new System.Drawing.Size(25, 23);
            this._bpoAppraisalValue_Assign.TabIndex = 497;
            this._bpoAppraisalValue_Assign.Text = "...";
            this._bpoAppraisalValue_Assign.UseVisualStyleBackColor = true;
            // 
            // _bpoAppraisalValue_Who
            // 
            this._bpoAppraisalValue_Who.FormattingEnabled = true;
            this._bpoAppraisalValue_Who.Location = new System.Drawing.Point(523, 298);
            this._bpoAppraisalValue_Who.Name = "_bpoAppraisalValue_Who";
            this._bpoAppraisalValue_Who.Size = new System.Drawing.Size(82, 21);
            this._bpoAppraisalValue_Who.TabIndex = 496;
            this._bpoAppraisalValue_Who.Leave += new System.EventHandler(this.OnDropDownLeave);
            // 
            // _bpoAppraisalValue_Checked
            // 
            this._bpoAppraisalValue_Checked.Checked = true;
            this._bpoAppraisalValue_Checked.CheckState = System.Windows.Forms.CheckState.Checked;
            this._bpoAppraisalValue_Checked.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._bpoAppraisalValue_Checked.Location = new System.Drawing.Point(636, 295);
            this._bpoAppraisalValue_Checked.Name = "_bpoAppraisalValue_Checked";
            this._bpoAppraisalValue_Checked.Size = new System.Drawing.Size(24, 24);
            this._bpoAppraisalValue_Checked.TabIndex = 498;
            // 
            // _repairListSentToNegotiator_Assign
            // 
            this._repairListSentToNegotiator_Assign.Location = new System.Drawing.Point(609, 332);
            this._repairListSentToNegotiator_Assign.Name = "_repairListSentToNegotiator_Assign";
            this._repairListSentToNegotiator_Assign.Size = new System.Drawing.Size(25, 23);
            this._repairListSentToNegotiator_Assign.TabIndex = 500;
            this._repairListSentToNegotiator_Assign.Text = "...";
            this._repairListSentToNegotiator_Assign.UseVisualStyleBackColor = true;
            // 
            // _repairListSentToNegotiator_Who
            // 
            this._repairListSentToNegotiator_Who.FormattingEnabled = true;
            this._repairListSentToNegotiator_Who.Location = new System.Drawing.Point(523, 334);
            this._repairListSentToNegotiator_Who.Name = "_repairListSentToNegotiator_Who";
            this._repairListSentToNegotiator_Who.Size = new System.Drawing.Size(82, 21);
            this._repairListSentToNegotiator_Who.TabIndex = 499;
            this._repairListSentToNegotiator_Who.Leave += new System.EventHandler(this.OnDropDownLeave);
            // 
            // _repairListSentToNegotiator_Checked
            // 
            this._repairListSentToNegotiator_Checked.Checked = true;
            this._repairListSentToNegotiator_Checked.CheckState = System.Windows.Forms.CheckState.Checked;
            this._repairListSentToNegotiator_Checked.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._repairListSentToNegotiator_Checked.Location = new System.Drawing.Point(636, 331);
            this._repairListSentToNegotiator_Checked.Name = "_repairListSentToNegotiator_Checked";
            this._repairListSentToNegotiator_Checked.Size = new System.Drawing.Size(24, 24);
            this._repairListSentToNegotiator_Checked.TabIndex = 501;
            // 
            // _marketDataSentToNegotiator_Assign
            // 
            this._marketDataSentToNegotiator_Assign.Location = new System.Drawing.Point(825, 34);
            this._marketDataSentToNegotiator_Assign.Name = "_marketDataSentToNegotiator_Assign";
            this._marketDataSentToNegotiator_Assign.Size = new System.Drawing.Size(25, 23);
            this._marketDataSentToNegotiator_Assign.TabIndex = 503;
            this._marketDataSentToNegotiator_Assign.Text = "...";
            this._marketDataSentToNegotiator_Assign.UseVisualStyleBackColor = true;
            // 
            // _marketDataSentToNegotiator_Who
            // 
            this._marketDataSentToNegotiator_Who.FormattingEnabled = true;
            this._marketDataSentToNegotiator_Who.Location = new System.Drawing.Point(740, 36);
            this._marketDataSentToNegotiator_Who.Name = "_marketDataSentToNegotiator_Who";
            this._marketDataSentToNegotiator_Who.Size = new System.Drawing.Size(82, 21);
            this._marketDataSentToNegotiator_Who.TabIndex = 502;
            this._marketDataSentToNegotiator_Who.Leave += new System.EventHandler(this.OnDropDownLeave);
            // 
            // _marketDataSentToNegotiator_Checked
            // 
            this._marketDataSentToNegotiator_Checked.Checked = true;
            this._marketDataSentToNegotiator_Checked.CheckState = System.Windows.Forms.CheckState.Checked;
            this._marketDataSentToNegotiator_Checked.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._marketDataSentToNegotiator_Checked.Location = new System.Drawing.Point(854, 33);
            this._marketDataSentToNegotiator_Checked.Name = "_marketDataSentToNegotiator_Checked";
            this._marketDataSentToNegotiator_Checked.Size = new System.Drawing.Size(24, 24);
            this._marketDataSentToNegotiator_Checked.TabIndex = 504;
            // 
            // _noteHolder_Assign
            // 
            this._noteHolder_Assign.Location = new System.Drawing.Point(825, 72);
            this._noteHolder_Assign.Name = "_noteHolder_Assign";
            this._noteHolder_Assign.Size = new System.Drawing.Size(25, 23);
            this._noteHolder_Assign.TabIndex = 506;
            this._noteHolder_Assign.Text = "...";
            this._noteHolder_Assign.UseVisualStyleBackColor = true;
            // 
            // _noteHolder_Who
            // 
            this._noteHolder_Who.FormattingEnabled = true;
            this._noteHolder_Who.Location = new System.Drawing.Point(740, 74);
            this._noteHolder_Who.Name = "_noteHolder_Who";
            this._noteHolder_Who.Size = new System.Drawing.Size(82, 21);
            this._noteHolder_Who.TabIndex = 505;
            this._noteHolder_Who.Leave += new System.EventHandler(this.OnDropDownLeave);
            // 
            // _noteHolder_Checked
            // 
            this._noteHolder_Checked.Checked = true;
            this._noteHolder_Checked.CheckState = System.Windows.Forms.CheckState.Checked;
            this._noteHolder_Checked.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._noteHolder_Checked.Location = new System.Drawing.Point(854, 71);
            this._noteHolder_Checked.Name = "_noteHolder_Checked";
            this._noteHolder_Checked.Size = new System.Drawing.Size(24, 24);
            this._noteHolder_Checked.TabIndex = 507;
            // 
            // _minPercentOfBPOAccepted_Assign
            // 
            this._minPercentOfBPOAccepted_Assign.Location = new System.Drawing.Point(825, 106);
            this._minPercentOfBPOAccepted_Assign.Name = "_minPercentOfBPOAccepted_Assign";
            this._minPercentOfBPOAccepted_Assign.Size = new System.Drawing.Size(25, 23);
            this._minPercentOfBPOAccepted_Assign.TabIndex = 509;
            this._minPercentOfBPOAccepted_Assign.Text = "...";
            this._minPercentOfBPOAccepted_Assign.UseVisualStyleBackColor = true;
            // 
            // _minPercentOfBPOAccepted_Who
            // 
            this._minPercentOfBPOAccepted_Who.FormattingEnabled = true;
            this._minPercentOfBPOAccepted_Who.Location = new System.Drawing.Point(740, 110);
            this._minPercentOfBPOAccepted_Who.Name = "_minPercentOfBPOAccepted_Who";
            this._minPercentOfBPOAccepted_Who.Size = new System.Drawing.Size(82, 21);
            this._minPercentOfBPOAccepted_Who.TabIndex = 508;
            this._minPercentOfBPOAccepted_Who.Leave += new System.EventHandler(this.OnDropDownLeave);
            // 
            // _minPercentOfBPOAccepted_Checked
            // 
            this._minPercentOfBPOAccepted_Checked.Checked = true;
            this._minPercentOfBPOAccepted_Checked.CheckState = System.Windows.Forms.CheckState.Checked;
            this._minPercentOfBPOAccepted_Checked.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._minPercentOfBPOAccepted_Checked.Location = new System.Drawing.Point(854, 107);
            this._minPercentOfBPOAccepted_Checked.Name = "_minPercentOfBPOAccepted_Checked";
            this._minPercentOfBPOAccepted_Checked.Size = new System.Drawing.Size(24, 24);
            this._minPercentOfBPOAccepted_Checked.TabIndex = 510;
            // 
            // _acceptanceLetterReceived_Assign
            // 
            this._acceptanceLetterReceived_Assign.Location = new System.Drawing.Point(825, 148);
            this._acceptanceLetterReceived_Assign.Name = "_acceptanceLetterReceived_Assign";
            this._acceptanceLetterReceived_Assign.Size = new System.Drawing.Size(25, 23);
            this._acceptanceLetterReceived_Assign.TabIndex = 512;
            this._acceptanceLetterReceived_Assign.Text = "...";
            this._acceptanceLetterReceived_Assign.UseVisualStyleBackColor = true;
            // 
            // _acceptanceLetterReceived_Who
            // 
            this._acceptanceLetterReceived_Who.FormattingEnabled = true;
            this._acceptanceLetterReceived_Who.Location = new System.Drawing.Point(740, 147);
            this._acceptanceLetterReceived_Who.Name = "_acceptanceLetterReceived_Who";
            this._acceptanceLetterReceived_Who.Size = new System.Drawing.Size(82, 21);
            this._acceptanceLetterReceived_Who.TabIndex = 511;
            this._acceptanceLetterReceived_Who.Leave += new System.EventHandler(this.OnDropDownLeave);
            // 
            // _acceptanceLetterReceived_Checked
            // 
            this._acceptanceLetterReceived_Checked.Checked = true;
            this._acceptanceLetterReceived_Checked.CheckState = System.Windows.Forms.CheckState.Checked;
            this._acceptanceLetterReceived_Checked.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._acceptanceLetterReceived_Checked.Location = new System.Drawing.Point(854, 148);
            this._acceptanceLetterReceived_Checked.Name = "_acceptanceLetterReceived_Checked";
            this._acceptanceLetterReceived_Checked.Size = new System.Drawing.Size(24, 24);
            this._acceptanceLetterReceived_Checked.TabIndex = 513;
            // 
            // _deficiencyJudgmentWaived_Assign
            // 
            this._deficiencyJudgmentWaived_Assign.Location = new System.Drawing.Point(825, 182);
            this._deficiencyJudgmentWaived_Assign.Name = "_deficiencyJudgmentWaived_Assign";
            this._deficiencyJudgmentWaived_Assign.Size = new System.Drawing.Size(25, 23);
            this._deficiencyJudgmentWaived_Assign.TabIndex = 515;
            this._deficiencyJudgmentWaived_Assign.Text = "...";
            this._deficiencyJudgmentWaived_Assign.UseVisualStyleBackColor = true;
            // 
            // _deficiencyJudgmentWaived_Who
            // 
            this._deficiencyJudgmentWaived_Who.FormattingEnabled = true;
            this._deficiencyJudgmentWaived_Who.Location = new System.Drawing.Point(738, 184);
            this._deficiencyJudgmentWaived_Who.Name = "_deficiencyJudgmentWaived_Who";
            this._deficiencyJudgmentWaived_Who.Size = new System.Drawing.Size(82, 21);
            this._deficiencyJudgmentWaived_Who.TabIndex = 514;
            this._deficiencyJudgmentWaived_Who.Leave += new System.EventHandler(this.OnDropDownLeave);
            // 
            // _deficiencyJudgmentWaived_Checked
            // 
            this._deficiencyJudgmentWaived_Checked.Checked = true;
            this._deficiencyJudgmentWaived_Checked.CheckState = System.Windows.Forms.CheckState.Checked;
            this._deficiencyJudgmentWaived_Checked.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._deficiencyJudgmentWaived_Checked.Location = new System.Drawing.Point(854, 182);
            this._deficiencyJudgmentWaived_Checked.Name = "_deficiencyJudgmentWaived_Checked";
            this._deficiencyJudgmentWaived_Checked.Size = new System.Drawing.Size(24, 24);
            this._deficiencyJudgmentWaived_Checked.TabIndex = 516;
            // 
            // _sellerAgreedToLetterTerms_Assign
            // 
            this._sellerAgreedToLetterTerms_Assign.Location = new System.Drawing.Point(825, 220);
            this._sellerAgreedToLetterTerms_Assign.Name = "_sellerAgreedToLetterTerms_Assign";
            this._sellerAgreedToLetterTerms_Assign.Size = new System.Drawing.Size(25, 23);
            this._sellerAgreedToLetterTerms_Assign.TabIndex = 518;
            this._sellerAgreedToLetterTerms_Assign.Text = "...";
            this._sellerAgreedToLetterTerms_Assign.UseVisualStyleBackColor = true;
            // 
            // _sellerAgreedToLetterTerms_Who
            // 
            this._sellerAgreedToLetterTerms_Who.FormattingEnabled = true;
            this._sellerAgreedToLetterTerms_Who.Location = new System.Drawing.Point(740, 222);
            this._sellerAgreedToLetterTerms_Who.Name = "_sellerAgreedToLetterTerms_Who";
            this._sellerAgreedToLetterTerms_Who.Size = new System.Drawing.Size(82, 21);
            this._sellerAgreedToLetterTerms_Who.TabIndex = 517;
            this._sellerAgreedToLetterTerms_Who.Leave += new System.EventHandler(this.OnDropDownLeave);
            // 
            // _sellerAgreedToLetterTerms_Checked
            // 
            this._sellerAgreedToLetterTerms_Checked.Checked = true;
            this._sellerAgreedToLetterTerms_Checked.CheckState = System.Windows.Forms.CheckState.Checked;
            this._sellerAgreedToLetterTerms_Checked.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._sellerAgreedToLetterTerms_Checked.Location = new System.Drawing.Point(854, 219);
            this._sellerAgreedToLetterTerms_Checked.Name = "_sellerAgreedToLetterTerms_Checked";
            this._sellerAgreedToLetterTerms_Checked.Size = new System.Drawing.Size(24, 24);
            this._sellerAgreedToLetterTerms_Checked.TabIndex = 519;
            // 
            // label71
            // 
            this.label71.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Italic | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label71.Location = new System.Drawing.Point(92, 0);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(45, 23);
            this.label71.TabIndex = 520;
            this.label71.Text = "Who";
            // 
            // label12
            // 
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Italic | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(314, -1);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(45, 23);
            this.label12.TabIndex = 521;
            this.label12.Text = "Who";
            // 
            // label13
            // 
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Italic | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(535, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(45, 23);
            this.label13.TabIndex = 522;
            this.label13.Text = "Who";
            // 
            // label14
            // 
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Italic | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(754, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(45, 23);
            this.label14.TabIndex = 523;
            this.label14.Text = "Who";
            // 
            // label73
            // 
            this.label73.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Italic | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label73.Location = new System.Drawing.Point(167, 0);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(45, 23);
            this.label73.TabIndex = 524;
            this.label73.Text = "Assign";
            // 
            // label17
            // 
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Italic | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(390, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(45, 23);
            this.label17.TabIndex = 525;
            this.label17.Text = "Assign";
            // 
            // label18
            // 
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Italic | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(606, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(45, 23);
            this.label18.TabIndex = 526;
            this.label18.Text = "Assign";
            // 
            // label21
            // 
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Italic | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(822, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(45, 23);
            this.label21.TabIndex = 527;
            this.label21.Text = "Assign";
            // 
            // LienStatusControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.Controls.Add(this._accept1ExpDate);
            this.Controls.Add(this._accept2ExpDate);
            this.Controls.Add(this._accept3ExpDate);
            this.Controls.Add(this._sellerAgreedToLetterTerms_Checked);
            this.Controls.Add(this._sellerAgreedToLetterTerms_Assign);
            this.Controls.Add(this._sellerAgreedToLetterTerms_Who);
            this.Controls.Add(this._sellerAgreedToLetterTerms);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label73);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label71);
            this.Controls.Add(this._deficiencyJudgmentWaived_Assign);
            this.Controls.Add(this._deficiencyJudgmentWaived_Who);
            this.Controls.Add(this._deficiencyJudgmentWaived_Checked);
            this.Controls.Add(this._acceptanceLetterReceived_Assign);
            this.Controls.Add(this._acceptanceLetterReceived_Who);
            this.Controls.Add(this._acceptanceLetterReceived_Checked);
            this.Controls.Add(this._minPercentOfBPOAccepted_Assign);
            this.Controls.Add(this._minPercentOfBPOAccepted_Who);
            this.Controls.Add(this._minPercentOfBPOAccepted_Checked);
            this.Controls.Add(this._noteHolder_Assign);
            this.Controls.Add(this._noteHolder_Who);
            this.Controls.Add(this._noteHolder_Checked);
            this.Controls.Add(this._marketDataSentToNegotiator_Assign);
            this.Controls.Add(this._marketDataSentToNegotiator_Who);
            this.Controls.Add(this._marketDataSentToNegotiator_Checked);
            this.Controls.Add(this._repairListSentToNegotiator_Assign);
            this.Controls.Add(this._repairListSentToNegotiator_Who);
            this.Controls.Add(this._repairListSentToNegotiator_Checked);
            this.Controls.Add(this._bpoAppraisalValue_Assign);
            this.Controls.Add(this._bpoAppraisalValue_Who);
            this.Controls.Add(this._bpoAppraisalValue_Checked);
            this.Controls.Add(this._bpoAppraisalCompleted_Assign);
            this.Controls.Add(this._bpoAppraisalCompleted_Who);
            this.Controls.Add(this._bpoAppraisalCompleted_Checked);
            this.Controls.Add(this._bpoAppraisalOrdered_Assign);
            this.Controls.Add(this._bpoAppraisalOrdered_Who);
            this.Controls.Add(this._bpoAppraisalOrdered_Checked);
            this.Controls.Add(this._negotiatorAssigned_Assign);
            this.Controls.Add(this._negotiatorAssigned_Who);
            this.Controls.Add(this._negotiatorAssigned_Checked);
            this.Controls.Add(this._bpoPacketComplete_Assign);
            this.Controls.Add(this._bpoPacketComplete_Who);
            this.Controls.Add(this._bpoPacketComplete_Checked);
            this.Controls.Add(this._prelimInspectionDate_Assign);
            this.Controls.Add(this._prelimInspectionDate_Who);
            this.Controls.Add(this._prelimInspectionDate_Checked);
            this.Controls.Add(this._prelimInspectionOrdered_Assign);
            this.Controls.Add(this._prelimInspectionOrdered_Who);
            this.Controls.Add(this._prelimInspectionOrdered_Checked);
            this.Controls.Add(this._ssPacketVerifiedReceived_Checked);
            this.Controls.Add(this._ssPacketVerifiedReceived_Assign);
            this.Controls.Add(this._ssPacketVerifiedReceived_Who);
            this.Controls.Add(this._ssPacketSentToLienHolder_Checked);
            this.Controls.Add(this._ssPacketSentToLienHolder_Assign);
            this.Controls.Add(this._ssPacketSentToLienHolder_Who);
            this.Controls.Add(this._ssPacketReviewed_Checked);
            this.Controls.Add(this._ssPacketReviewed_Assign);
            this.Controls.Add(this._ssPacketReviewed_Who);
            this.Controls.Add(this._ssPackedComplete_Checked);
            this.Controls.Add(this._ssPackedComplete_Assign);
            this.Controls.Add(this._ssPackedComplete_Who);
            this.Controls.Add(this._reinstatementOrdered_Assign);
            this.Controls.Add(this._reinstatementOrdered_Who);
            this.Controls.Add(this._reinstatementOrdered_Checked);
            this.Controls.Add(this._payoffOrdered_Who);
            this.Controls.Add(this._payoffOrdered_Assign);
            this.Controls.Add(this._payoffOrdered_Checked);
            this.Controls.Add(this._anyTitleIssues_Assign);
            this.Controls.Add(this._anyTitleIssues_Who);
            this.Controls.Add(this._anyTitleIssues_Checked);
            this.Controls.Add(this._preliminaryTitleSearchReceived_Assign);
            this.Controls.Add(this._preliminaryTitleSearchReceived_Who);
            this.Controls.Add(this._preliminaryTitleSearchReceived_Checked);
            this.Controls.Add(this._preliminaryTitleSearchedOrdered_Assign);
            this.Controls.Add(this._preliminaryTitleSearchedOrdered_Who);
            this.Controls.Add(this._preliminaryTitleSearchedOrdered_Checked);
            this.Controls.Add(this._propertyProfileInProfitGrabber_Assign);
            this.Controls.Add(this._propertyProfileInProfitGrabber_Who);
            this.Controls.Add(this._propertyProfileInProfitGrabber_Checked);
            this.Controls.Add(this._allInfoEnteredInProfitGrabber_Assign);
            this.Controls.Add(this._allInfoEnteredInProfitGrabber_Who);
            this.Controls.Add(this._allInfoEnteredInProfitGrabber_Checked);
            this.Controls.Add(this._returnOriginalsToHomeOwner_Assign);
            this.Controls.Add(this._returnOriginalsToHomeOwner_Who);
            this.Controls.Add(this._returnOriginalsToHomeOwner_Checked);
            this.Controls.Add(this._requiredDocsRecorded_Assign);
            this.Controls.Add(this._requiredDocsRecorded_Who);
            this.Controls.Add(this._requiredDocsRecorded_Checked);
            this.Controls.Add(this._requiredDocsScanned_Assign);
            this.Controls.Add(this._requiredDocsScanned_Who);
            this.Controls.Add(this._requiredDocsScanned_Checked);
            this.Controls.Add(this._allIntakeDocsFromSeller_Assign);
            this.Controls.Add(this._allIntakeDocsFromSeller_Who);
            this.Controls.Add(this._allIntakeDocsFromSeller_Checked);
            this.Controls.Add(this._authorizationVerifiedReceived_Assign);
            this.Controls.Add(this._authorizationVerifiedReceived_Who);
            this.Controls.Add(this._authorizationVerifiedReceived_Checked);
            this.Controls.Add(this._authorizationFaxedIn_Who);
            this.Controls.Add(this._authorizationFaxedIn_Assign);
            this.Controls.Add(this._authorizationFaxedIn_Checked);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label59);
            this.Controls.Add(this._deficiencyJudgmentWaived);
            this.Controls.Add(this._prelimInspectionDate);
            this.Controls.Add(this._preliminaryTitleSearchReceived);
            this.Controls.Add(this._prelimInspectionOrdered);
            this.Controls.Add(this._foreclosureDate);
            this.Controls.Add(this._titleSeasonDate);
            this.Controls.Add(this._acceptanceLetterReceived);
            this.Controls.Add(this._marketDataSentToNegotiator);
            this.Controls.Add(this._repairListSentToNegotiator);
            this.Controls.Add(this._negotiatorAssigned);
            this.Controls.Add(this._bpoPacketComplete);
            this.Controls.Add(this._ssPacketReviewed);
            this.Controls.Add(this._anyTitleIssues);
            this.Controls.Add(this._propertyProfileInProfitGrabber);
            this.Controls.Add(this._allInfoEnteredInProfitGrabber);
            this.Controls.Add(this._returnOriginalsToHomeOwner);
            this.Controls.Add(this._requiredDocsRecorded);
            this.Controls.Add(this._requiredDocsScanned);
            this.Controls.Add(this._minPercentOfBPOAccepted);
            this.Controls.Add(this._noteHolder);
            this.Controls.Add(this._reinstatementAmount);
            this.Controls.Add(this._reinstatementOrdered);
            this.Controls.Add(this._preliminaryTitleSearchedOrdered);
            this.Controls.Add(this._bpoAppraisalValue);
            this.Controls.Add(this._bpoAppraisalCompleted);
            this.Controls.Add(this._bpoAppraisalOrdered);
            this.Controls.Add(this._ssPacketVerifiedReceived);
            this.Controls.Add(this._ssPacketSentToLienHolder);
            this.Controls.Add(this._ssPackedComplete);
            this.Controls.Add(this._payoffAmount);
            this.Controls.Add(this._payoffOrdered);
            this.Controls.Add(this._allIntakeDocsFromSeller);
            this.Controls.Add(this._authorizationVerifiedReceived);
            this.Controls.Add(this._authorizationFaxedIn);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label55);
            this.Controls.Add(this.label54);
            this.Controls.Add(this.label53);
            this.Controls.Add(this.label52);
            this.Controls.Add(this.label48);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label47);
            this.Controls.Add(this.label45);
            this.Controls.Add(this.label44);
            this.Controls.Add(this.label19);
            this.Controls.Add(this._printStatusPage);
            this.Controls.Add(this.label80);
            this.Controls.Add(this.label78);
            this.Controls.Add(this.label46);
            this.Controls.Add(this.label77);
            this.Controls.Add(this.label76);
            this.Controls.Add(this.label70);
            this.Controls.Add(this.label68);
            this.Controls.Add(this.label69);
            this.Controls.Add(this.label66);
            this.Controls.Add(this.label67);
            this.Controls.Add(this.label64);
            this.Controls.Add(this.label65);
            this.Controls.Add(this.label62);
            this.Controls.Add(this.label63);
            this.Controls.Add(this.label60);
            this.Controls.Add(this.label61);
            this.Controls.Add(this.label58);
            this.Controls.Add(this.label57);
            this.Controls.Add(this.label56);
            this.Controls.Add(this.label43);
            this.Controls.Add(this.label42);
            this.Controls.Add(this.label41);
            this.Controls.Add(this.label39);
            this.Controls.Add(this.labelx);
            this.Controls.Add(this._removeLien);
            this.Controls.Add(this._pictureBoxStatusProgress);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.Name = "LienStatusControl";
            this.Size = new System.Drawing.Size(907, 357);
            ((System.ComponentModel.ISupportInitialize)(this._pictureBoxStatusProgress)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox _deficiencyJudgmentWaived;
        private System.Windows.Forms.TextBox _prelimInspectionDate;
        private System.Windows.Forms.TextBox _preliminaryTitleSearchReceived;
        private System.Windows.Forms.TextBox _prelimInspectionOrdered;
        private System.Windows.Forms.TextBox _foreclosureDate;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.TextBox _accept3ExpDate;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.TextBox _accept2ExpDate;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.TextBox _accept1ExpDate;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.TextBox _titleSeasonDate;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.TextBox _sellerAgreedToLetterTerms;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.TextBox _acceptanceLetterReceived;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.TextBox _marketDataSentToNegotiator;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.TextBox _repairListSentToNegotiator;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox _negotiatorAssigned;
        private System.Windows.Forms.Button _printStatusPage;
        private System.Windows.Forms.Label label80;
        private System.Windows.Forms.TextBox _bpoPacketComplete;
        private System.Windows.Forms.Label label78;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label77;
        private System.Windows.Forms.TextBox _ssPacketReviewed;
        private System.Windows.Forms.Label label76;
        private System.Windows.Forms.TextBox _anyTitleIssues;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.TextBox _propertyProfileInProfitGrabber;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.TextBox _allInfoEnteredInProfitGrabber;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.TextBox _returnOriginalsToHomeOwner;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.TextBox _requiredDocsRecorded;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.TextBox _requiredDocsScanned;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.TextBox _minPercentOfBPOAccepted;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.TextBox _noteHolder;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.TextBox _reinstatementAmount;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.TextBox _reinstatementOrdered;
        private System.Windows.Forms.Label labelx;
        //private System.Windows.Forms.Button _removeLien;
        private System.Windows.Forms.TextBox _preliminaryTitleSearchedOrdered;
        private System.Windows.Forms.PictureBox _pictureBoxStatusProgress;
        private System.Windows.Forms.TextBox _bpoAppraisalValue;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox _bpoAppraisalCompleted;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox _bpoAppraisalOrdered;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox _ssPacketVerifiedReceived;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox _ssPacketSentToLienHolder;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox _ssPackedComplete;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox _payoffAmount;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox _payoffOrdered;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox _allIntakeDocsFromSeller;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox _authorizationVerifiedReceived;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox _authorizationFaxedIn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox _authorizationFaxedIn_Who;
        private System.Windows.Forms.Button _authorizationFaxedIn_Assign;
        private System.Windows.Forms.CheckBox _authorizationFaxedIn_Checked;
        private System.Windows.Forms.Button _authorizationVerifiedReceived_Assign;
        private System.Windows.Forms.ComboBox _authorizationVerifiedReceived_Who;
        private System.Windows.Forms.CheckBox _authorizationVerifiedReceived_Checked;
        private System.Windows.Forms.Button _allIntakeDocsFromSeller_Assign;
        private System.Windows.Forms.ComboBox _allIntakeDocsFromSeller_Who;
        private System.Windows.Forms.CheckBox _allIntakeDocsFromSeller_Checked;
        private System.Windows.Forms.Button _requiredDocsScanned_Assign;
        private System.Windows.Forms.ComboBox _requiredDocsScanned_Who;
        private System.Windows.Forms.CheckBox _requiredDocsScanned_Checked;
        private System.Windows.Forms.Button _requiredDocsRecorded_Assign;
        private System.Windows.Forms.ComboBox _requiredDocsRecorded_Who;
        private System.Windows.Forms.CheckBox _requiredDocsRecorded_Checked;
        private System.Windows.Forms.Button _returnOriginalsToHomeOwner_Assign;
        private System.Windows.Forms.ComboBox _returnOriginalsToHomeOwner_Who;
        private System.Windows.Forms.CheckBox _returnOriginalsToHomeOwner_Checked;
        private System.Windows.Forms.Button _allInfoEnteredInProfitGrabber_Assign;
        private System.Windows.Forms.ComboBox _allInfoEnteredInProfitGrabber_Who;
        private System.Windows.Forms.CheckBox _allInfoEnteredInProfitGrabber_Checked;
        private System.Windows.Forms.Button _propertyProfileInProfitGrabber_Assign;
        private System.Windows.Forms.ComboBox _propertyProfileInProfitGrabber_Who;
        private System.Windows.Forms.CheckBox _propertyProfileInProfitGrabber_Checked;
        private System.Windows.Forms.Button _preliminaryTitleSearchedOrdered_Assign;
        private System.Windows.Forms.ComboBox _preliminaryTitleSearchedOrdered_Who;
        private System.Windows.Forms.CheckBox _preliminaryTitleSearchedOrdered_Checked;
        private System.Windows.Forms.Button _preliminaryTitleSearchReceived_Assign;
        private System.Windows.Forms.ComboBox _preliminaryTitleSearchReceived_Who;
        private System.Windows.Forms.CheckBox _preliminaryTitleSearchReceived_Checked;
        private System.Windows.Forms.Button _anyTitleIssues_Assign;
        private System.Windows.Forms.ComboBox _anyTitleIssues_Who;
        private System.Windows.Forms.CheckBox _anyTitleIssues_Checked;
        private System.Windows.Forms.ComboBox _payoffOrdered_Who;
        private System.Windows.Forms.Button _payoffOrdered_Assign;
        private System.Windows.Forms.CheckBox _payoffOrdered_Checked;
        private System.Windows.Forms.Button _reinstatementOrdered_Assign;
        private System.Windows.Forms.ComboBox _reinstatementOrdered_Who;
        private System.Windows.Forms.CheckBox _reinstatementOrdered_Checked;
        private System.Windows.Forms.CheckBox _ssPackedComplete_Checked;
        private System.Windows.Forms.Button _ssPackedComplete_Assign;
        private System.Windows.Forms.ComboBox _ssPackedComplete_Who;
        private System.Windows.Forms.CheckBox _ssPacketReviewed_Checked;
        private System.Windows.Forms.Button _ssPacketReviewed_Assign;
        private System.Windows.Forms.ComboBox _ssPacketReviewed_Who;
        private System.Windows.Forms.CheckBox _ssPacketSentToLienHolder_Checked;
        private System.Windows.Forms.Button _ssPacketSentToLienHolder_Assign;
        private System.Windows.Forms.ComboBox _ssPacketSentToLienHolder_Who;
        private System.Windows.Forms.CheckBox _ssPacketVerifiedReceived_Checked;
        private System.Windows.Forms.Button _ssPacketVerifiedReceived_Assign;
        private System.Windows.Forms.ComboBox _ssPacketVerifiedReceived_Who;
        private System.Windows.Forms.Button _prelimInspectionOrdered_Assign;
        private System.Windows.Forms.ComboBox _prelimInspectionOrdered_Who;
        private System.Windows.Forms.CheckBox _prelimInspectionOrdered_Checked;
        private System.Windows.Forms.Button _prelimInspectionDate_Assign;
        private System.Windows.Forms.ComboBox _prelimInspectionDate_Who;
        private System.Windows.Forms.CheckBox _prelimInspectionDate_Checked;
        private System.Windows.Forms.Button _bpoPacketComplete_Assign;
        private System.Windows.Forms.ComboBox _bpoPacketComplete_Who;
        private System.Windows.Forms.CheckBox _bpoPacketComplete_Checked;
        private System.Windows.Forms.Button _negotiatorAssigned_Assign;
        private System.Windows.Forms.ComboBox _negotiatorAssigned_Who;
        private System.Windows.Forms.CheckBox _negotiatorAssigned_Checked;
        private System.Windows.Forms.Button _bpoAppraisalOrdered_Assign;
        private System.Windows.Forms.ComboBox _bpoAppraisalOrdered_Who;
        private System.Windows.Forms.CheckBox _bpoAppraisalOrdered_Checked;
        private System.Windows.Forms.Button _bpoAppraisalCompleted_Assign;
        private System.Windows.Forms.ComboBox _bpoAppraisalCompleted_Who;
        private System.Windows.Forms.CheckBox _bpoAppraisalCompleted_Checked;
        private System.Windows.Forms.Button _bpoAppraisalValue_Assign;
        private System.Windows.Forms.ComboBox _bpoAppraisalValue_Who;
        private System.Windows.Forms.CheckBox _bpoAppraisalValue_Checked;
        private System.Windows.Forms.Button _repairListSentToNegotiator_Assign;
        private System.Windows.Forms.ComboBox _repairListSentToNegotiator_Who;
        private System.Windows.Forms.CheckBox _repairListSentToNegotiator_Checked;
        private System.Windows.Forms.Button _marketDataSentToNegotiator_Assign;
        private System.Windows.Forms.ComboBox _marketDataSentToNegotiator_Who;
        private System.Windows.Forms.CheckBox _marketDataSentToNegotiator_Checked;
        private System.Windows.Forms.Button _noteHolder_Assign;
        private System.Windows.Forms.ComboBox _noteHolder_Who;
        private System.Windows.Forms.CheckBox _noteHolder_Checked;
        private System.Windows.Forms.Button _minPercentOfBPOAccepted_Assign;
        private System.Windows.Forms.ComboBox _minPercentOfBPOAccepted_Who;
        private System.Windows.Forms.CheckBox _minPercentOfBPOAccepted_Checked;
        private System.Windows.Forms.Button _acceptanceLetterReceived_Assign;
        private System.Windows.Forms.ComboBox _acceptanceLetterReceived_Who;
        private System.Windows.Forms.CheckBox _acceptanceLetterReceived_Checked;
        private System.Windows.Forms.Button _deficiencyJudgmentWaived_Assign;
        private System.Windows.Forms.ComboBox _deficiencyJudgmentWaived_Who;
        private System.Windows.Forms.CheckBox _deficiencyJudgmentWaived_Checked;
        private System.Windows.Forms.Button _sellerAgreedToLetterTerms_Assign;
        private System.Windows.Forms.ComboBox _sellerAgreedToLetterTerms_Who;
        private System.Windows.Forms.CheckBox _sellerAgreedToLetterTerms_Checked;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label73;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.ToolTip toolTip1;
    }
}
