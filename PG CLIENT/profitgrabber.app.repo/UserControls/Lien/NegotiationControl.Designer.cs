﻿namespace DealMaker.UserControls.Lien
{
    partial class NegotiationControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NegotiationControl));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this._fa_web = new System.Windows.Forms.TextBox();
            this._cs_web = new System.Windows.Forms.TextBox();
            this._bd_web = new System.Windows.Forms.TextBox();
            this._bpo2_web = new System.Windows.Forms.TextBox();
            this._bpo_web = new System.Windows.Forms.TextBox();
            this._nm_web = new System.Windows.Forms.TextBox();
            this._n_web = new System.Windows.Forms.TextBox();
            this._sm_web = new System.Windows.Forms.TextBox();
            this._se_web = new System.Windows.Forms.TextBox();
            this.label86 = new System.Windows.Forms.Label();
            this._fa_Email = new System.Windows.Forms.TextBox();
            this._fa_ContactName = new System.Windows.Forms.TextBox();
            this._cs_Email = new System.Windows.Forms.TextBox();
            this._cs_Fax = new System.Windows.Forms.TextBox();
            this._cs_Phone = new System.Windows.Forms.TextBox();
            this._cs_ContactName = new System.Windows.Forms.TextBox();
            this.label84 = new System.Windows.Forms.Label();
            this._fa_Fax = new System.Windows.Forms.TextBox();
            this._fa_Phone = new System.Windows.Forms.TextBox();
            this._bd_Email = new System.Windows.Forms.TextBox();
            this._bd_ContactName = new System.Windows.Forms.TextBox();
            this._bpo2_Email = new System.Windows.Forms.TextBox();
            this._bpo2_FaxNr = new System.Windows.Forms.TextBox();
            this._bpo2_PhoneNr = new System.Windows.Forms.TextBox();
            this._bpo2_ContactName = new System.Windows.Forms.TextBox();
            this.label40 = new System.Windows.Forms.Label();
            this._bd_FaxNr = new System.Windows.Forms.TextBox();
            this._bd_PhoneNr = new System.Windows.Forms.TextBox();
            this._bpo_Email = new System.Windows.Forms.TextBox();
            this._bpo_FaxNr = new System.Windows.Forms.TextBox();
            this._bpo_PhonrNr = new System.Windows.Forms.TextBox();
            this._bpo_ContactName = new System.Windows.Forms.TextBox();
            this._nm_Email = new System.Windows.Forms.TextBox();
            this._nm_FaxNr = new System.Windows.Forms.TextBox();
            this._nm_PhoneNr = new System.Windows.Forms.TextBox();
            this._nm_ContactName = new System.Windows.Forms.TextBox();
            this._n_Email = new System.Windows.Forms.TextBox();
            this._n_FaxNr = new System.Windows.Forms.TextBox();
            this._n_PhoneNr = new System.Windows.Forms.TextBox();
            this._n_ContactName = new System.Windows.Forms.TextBox();
            this._sm_Email = new System.Windows.Forms.TextBox();
            this._sm_FaxNr = new System.Windows.Forms.TextBox();
            this._sm_PhoneNr = new System.Windows.Forms.TextBox();
            this._sm_ContactName = new System.Windows.Forms.TextBox();
            this._se_Email = new System.Windows.Forms.TextBox();
            this._se_FaxNr = new System.Windows.Forms.TextBox();
            this._se_PhoneNr = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this._se_ContactName = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label85 = new System.Windows.Forms.Label();
            this._printContactInfo = new System.Windows.Forms.Button();
            this._accountNr = new System.Windows.Forms.TextBox();
            this._openLogFile = new System.Windows.Forms.Button();
            this._browseLogFile = new System.Windows.Forms.Button();
            this._correspondenceLogFile = new System.Windows.Forms.TextBox();
            this._pictureBoxGeneralShortSale = new System.Windows.Forms.PictureBox();
            this.label10 = new System.Windows.Forms.Label();
            this._companyName = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this._strategyDesc = new System.Windows.Forms.TextBox();
            this._strategy = new System.Windows.Forms.ComboBox();
            this.label38 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this._counter_Fifth = new System.Windows.Forms.TextBox();
            this._offer_Fifth = new System.Windows.Forms.TextBox();
            this._counter_Fourth = new System.Windows.Forms.TextBox();
            this._offer_Fourth = new System.Windows.Forms.TextBox();
            this._counter_Third = new System.Windows.Forms.TextBox();
            this._offer_Third = new System.Windows.Forms.TextBox();
            this._counter_Second = new System.Windows.Forms.TextBox();
            this._offer_Second = new System.Windows.Forms.TextBox();
            this._counter_First = new System.Windows.Forms.TextBox();
            this._offer_First = new System.Windows.Forms.TextBox();
            this.label34 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this._maxToPay = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this._custom_assignWorkingFolder = new System.Windows.Forms.Button();
            this._custom_assignLogFile = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._pictureBoxGeneralShortSale)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this._fa_web);
            this.groupBox1.Controls.Add(this._cs_web);
            this.groupBox1.Controls.Add(this._bd_web);
            this.groupBox1.Controls.Add(this._bpo2_web);
            this.groupBox1.Controls.Add(this._bpo_web);
            this.groupBox1.Controls.Add(this._nm_web);
            this.groupBox1.Controls.Add(this._n_web);
            this.groupBox1.Controls.Add(this._sm_web);
            this.groupBox1.Controls.Add(this._se_web);
            this.groupBox1.Controls.Add(this.label86);
            this.groupBox1.Controls.Add(this._fa_Email);
            this.groupBox1.Controls.Add(this._fa_ContactName);
            this.groupBox1.Controls.Add(this._cs_Email);
            this.groupBox1.Controls.Add(this._cs_Fax);
            this.groupBox1.Controls.Add(this._cs_Phone);
            this.groupBox1.Controls.Add(this._cs_ContactName);
            this.groupBox1.Controls.Add(this.label84);
            this.groupBox1.Controls.Add(this._fa_Fax);
            this.groupBox1.Controls.Add(this._fa_Phone);
            this.groupBox1.Controls.Add(this._bd_Email);
            this.groupBox1.Controls.Add(this._bd_ContactName);
            this.groupBox1.Controls.Add(this._bpo2_Email);
            this.groupBox1.Controls.Add(this._bpo2_FaxNr);
            this.groupBox1.Controls.Add(this._bpo2_PhoneNr);
            this.groupBox1.Controls.Add(this._bpo2_ContactName);
            this.groupBox1.Controls.Add(this.label40);
            this.groupBox1.Controls.Add(this._bd_FaxNr);
            this.groupBox1.Controls.Add(this._bd_PhoneNr);
            this.groupBox1.Controls.Add(this._bpo_Email);
            this.groupBox1.Controls.Add(this._bpo_FaxNr);
            this.groupBox1.Controls.Add(this._bpo_PhonrNr);
            this.groupBox1.Controls.Add(this._bpo_ContactName);
            this.groupBox1.Controls.Add(this._nm_Email);
            this.groupBox1.Controls.Add(this._nm_FaxNr);
            this.groupBox1.Controls.Add(this._nm_PhoneNr);
            this.groupBox1.Controls.Add(this._nm_ContactName);
            this.groupBox1.Controls.Add(this._n_Email);
            this.groupBox1.Controls.Add(this._n_FaxNr);
            this.groupBox1.Controls.Add(this._n_PhoneNr);
            this.groupBox1.Controls.Add(this._n_ContactName);
            this.groupBox1.Controls.Add(this._sm_Email);
            this.groupBox1.Controls.Add(this._sm_FaxNr);
            this.groupBox1.Controls.Add(this._sm_PhoneNr);
            this.groupBox1.Controls.Add(this._sm_ContactName);
            this.groupBox1.Controls.Add(this._se_Email);
            this.groupBox1.Controls.Add(this._se_FaxNr);
            this.groupBox1.Controls.Add(this._se_PhoneNr);
            this.groupBox1.Controls.Add(this.label21);
            this.groupBox1.Controls.Add(this.label18);
            this.groupBox1.Controls.Add(this.label17);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this._se_ContactName);
            this.groupBox1.Controls.Add(this.label28);
            this.groupBox1.Controls.Add(this.label27);
            this.groupBox1.Controls.Add(this.label26);
            this.groupBox1.Controls.Add(this.label22);
            this.groupBox1.Controls.Add(this.label85);
            this.groupBox1.Location = new System.Drawing.Point(12, 53);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(892, 220);
            this.groupBox1.TabIndex = 59;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Lien Holder Contact Info";
            // 
            // _fa_web
            // 
            this._fa_web.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this._fa_web.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._fa_web.Location = new System.Drawing.Point(730, 195);
            this._fa_web.MaxLength = 80;
            this._fa_web.Name = "_fa_web";
            this._fa_web.Size = new System.Drawing.Size(156, 20);
            this._fa_web.TabIndex = 54;
            // 
            // _cs_web
            // 
            this._cs_web.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this._cs_web.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._cs_web.Location = new System.Drawing.Point(730, 174);
            this._cs_web.MaxLength = 80;
            this._cs_web.Name = "_cs_web";
            this._cs_web.Size = new System.Drawing.Size(156, 20);
            this._cs_web.TabIndex = 49;
            this._cs_web.Leave += new System.EventHandler(this.OnTextBoxLeave);
            this._cs_web.Enter += new System.EventHandler(this.OnTextBoxEnter);
            // 
            // _bd_web
            // 
            this._bd_web.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this._bd_web.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._bd_web.Location = new System.Drawing.Point(730, 153);
            this._bd_web.MaxLength = 80;
            this._bd_web.Name = "_bd_web";
            this._bd_web.Size = new System.Drawing.Size(156, 20);
            this._bd_web.TabIndex = 44;
            this._bd_web.Leave += new System.EventHandler(this.OnTextBoxLeave);
            this._bd_web.Enter += new System.EventHandler(this.OnTextBoxEnter);
            // 
            // _bpo2_web
            // 
            this._bpo2_web.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this._bpo2_web.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._bpo2_web.Location = new System.Drawing.Point(730, 132);
            this._bpo2_web.MaxLength = 80;
            this._bpo2_web.Name = "_bpo2_web";
            this._bpo2_web.Size = new System.Drawing.Size(156, 20);
            this._bpo2_web.TabIndex = 39;
            this._bpo2_web.Leave += new System.EventHandler(this.OnTextBoxLeave);
            this._bpo2_web.Enter += new System.EventHandler(this.OnTextBoxEnter);
            // 
            // _bpo_web
            // 
            this._bpo_web.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this._bpo_web.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._bpo_web.Location = new System.Drawing.Point(730, 111);
            this._bpo_web.MaxLength = 80;
            this._bpo_web.Name = "_bpo_web";
            this._bpo_web.Size = new System.Drawing.Size(156, 20);
            this._bpo_web.TabIndex = 34;
            this._bpo_web.Leave += new System.EventHandler(this.OnTextBoxLeave);
            this._bpo_web.Enter += new System.EventHandler(this.OnTextBoxEnter);
            // 
            // _nm_web
            // 
            this._nm_web.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this._nm_web.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._nm_web.Location = new System.Drawing.Point(730, 90);
            this._nm_web.MaxLength = 80;
            this._nm_web.Name = "_nm_web";
            this._nm_web.Size = new System.Drawing.Size(156, 20);
            this._nm_web.TabIndex = 29;
            this._nm_web.Leave += new System.EventHandler(this.OnTextBoxLeave);
            this._nm_web.Enter += new System.EventHandler(this.OnTextBoxEnter);
            // 
            // _n_web
            // 
            this._n_web.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this._n_web.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._n_web.Location = new System.Drawing.Point(730, 69);
            this._n_web.MaxLength = 80;
            this._n_web.Name = "_n_web";
            this._n_web.Size = new System.Drawing.Size(156, 20);
            this._n_web.TabIndex = 24;
            this._n_web.Leave += new System.EventHandler(this.OnTextBoxLeave);
            this._n_web.Enter += new System.EventHandler(this.OnTextBoxEnter);
            // 
            // _sm_web
            // 
            this._sm_web.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this._sm_web.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._sm_web.Location = new System.Drawing.Point(730, 48);
            this._sm_web.MaxLength = 80;
            this._sm_web.Name = "_sm_web";
            this._sm_web.Size = new System.Drawing.Size(156, 20);
            this._sm_web.TabIndex = 19;
            this._sm_web.Leave += new System.EventHandler(this.OnTextBoxLeave);
            this._sm_web.Enter += new System.EventHandler(this.OnTextBoxEnter);
            // 
            // _se_web
            // 
            this._se_web.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this._se_web.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._se_web.Location = new System.Drawing.Point(730, 27);
            this._se_web.MaxLength = 80;
            this._se_web.Name = "_se_web";
            this._se_web.Size = new System.Drawing.Size(156, 20);
            this._se_web.TabIndex = 14;
            this._se_web.Leave += new System.EventHandler(this.OnTextBoxLeave);
            this._se_web.Enter += new System.EventHandler(this.OnTextBoxEnter);
            // 
            // label86
            // 
            this.label86.AutoSize = true;
            this.label86.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label86.Location = new System.Drawing.Point(727, 11);
            this.label86.Name = "label86";
            this.label86.Size = new System.Drawing.Size(46, 13);
            this.label86.TabIndex = 48;
            this.label86.Text = "Website";
            this.label86.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // _fa_Email
            // 
            this._fa_Email.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._fa_Email.Location = new System.Drawing.Point(624, 195);
            this._fa_Email.MaxLength = 80;
            this._fa_Email.Name = "_fa_Email";
            this._fa_Email.Size = new System.Drawing.Size(99, 20);
            this._fa_Email.TabIndex = 53;
            this._fa_Email.Leave += new System.EventHandler(this.OnTextBoxLeave);
            this._fa_Email.Enter += new System.EventHandler(this.OnTextBoxEnter);
            // 
            // _fa_ContactName
            // 
            this._fa_ContactName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._fa_ContactName.Location = new System.Drawing.Point(128, 195);
            this._fa_ContactName.MaxLength = 100;
            this._fa_ContactName.Name = "_fa_ContactName";
            this._fa_ContactName.Size = new System.Drawing.Size(264, 20);
            this._fa_ContactName.TabIndex = 50;
            this._fa_ContactName.Leave += new System.EventHandler(this.OnTextBoxLeave);
            this._fa_ContactName.Enter += new System.EventHandler(this.OnTextBoxEnter);
            // 
            // _cs_Email
            // 
            this._cs_Email.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._cs_Email.Location = new System.Drawing.Point(624, 174);
            this._cs_Email.MaxLength = 80;
            this._cs_Email.Name = "_cs_Email";
            this._cs_Email.Size = new System.Drawing.Size(99, 20);
            this._cs_Email.TabIndex = 48;
            this._cs_Email.Leave += new System.EventHandler(this.OnTextBoxLeave);
            this._cs_Email.Enter += new System.EventHandler(this.OnTextBoxEnter);
            // 
            // _cs_Fax
            // 
            this._cs_Fax.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._cs_Fax.Location = new System.Drawing.Point(512, 174);
            this._cs_Fax.MaxLength = 40;
            this._cs_Fax.Name = "_cs_Fax";
            this._cs_Fax.Size = new System.Drawing.Size(104, 20);
            this._cs_Fax.TabIndex = 47;
            this._cs_Fax.Leave += new System.EventHandler(this.OnTextBoxLeave);
            this._cs_Fax.Enter += new System.EventHandler(this.OnTextBoxEnter);
            // 
            // _cs_Phone
            // 
            this._cs_Phone.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._cs_Phone.Location = new System.Drawing.Point(400, 174);
            this._cs_Phone.MaxLength = 40;
            this._cs_Phone.Name = "_cs_Phone";
            this._cs_Phone.Size = new System.Drawing.Size(104, 20);
            this._cs_Phone.TabIndex = 46;
            this._cs_Phone.Leave += new System.EventHandler(this.OnTextBoxLeave);
            this._cs_Phone.Enter += new System.EventHandler(this.OnTextBoxEnter);
            // 
            // _cs_ContactName
            // 
            this._cs_ContactName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._cs_ContactName.Location = new System.Drawing.Point(128, 174);
            this._cs_ContactName.MaxLength = 100;
            this._cs_ContactName.Name = "_cs_ContactName";
            this._cs_ContactName.Size = new System.Drawing.Size(264, 20);
            this._cs_ContactName.TabIndex = 45;
            this._cs_ContactName.Leave += new System.EventHandler(this.OnTextBoxLeave);
            this._cs_ContactName.Enter += new System.EventHandler(this.OnTextBoxEnter);
            // 
            // label84
            // 
            this.label84.AutoSize = true;
            this.label84.Location = new System.Drawing.Point(27, 176);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(93, 13);
            this.label84.TabIndex = 43;
            this.label84.Text = "Customer Service:";
            this.label84.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // _fa_Fax
            // 
            this._fa_Fax.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._fa_Fax.Location = new System.Drawing.Point(512, 195);
            this._fa_Fax.MaxLength = 40;
            this._fa_Fax.Name = "_fa_Fax";
            this._fa_Fax.Size = new System.Drawing.Size(104, 20);
            this._fa_Fax.TabIndex = 52;
            this._fa_Fax.Leave += new System.EventHandler(this.OnTextBoxLeave);
            this._fa_Fax.Enter += new System.EventHandler(this.OnTextBoxEnter);
            // 
            // _fa_Phone
            // 
            this._fa_Phone.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._fa_Phone.Location = new System.Drawing.Point(400, 195);
            this._fa_Phone.MaxLength = 40;
            this._fa_Phone.Name = "_fa_Phone";
            this._fa_Phone.Size = new System.Drawing.Size(104, 20);
            this._fa_Phone.TabIndex = 51;
            this._fa_Phone.Leave += new System.EventHandler(this.OnTextBoxLeave);
            this._fa_Phone.Enter += new System.EventHandler(this.OnTextBoxEnter);
            // 
            // _bd_Email
            // 
            this._bd_Email.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._bd_Email.Location = new System.Drawing.Point(624, 153);
            this._bd_Email.MaxLength = 80;
            this._bd_Email.Name = "_bd_Email";
            this._bd_Email.Size = new System.Drawing.Size(99, 20);
            this._bd_Email.TabIndex = 43;
            this._bd_Email.Leave += new System.EventHandler(this.OnTextBoxLeave);
            this._bd_Email.Enter += new System.EventHandler(this.OnTextBoxEnter);
            // 
            // _bd_ContactName
            // 
            this._bd_ContactName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._bd_ContactName.Location = new System.Drawing.Point(128, 153);
            this._bd_ContactName.MaxLength = 100;
            this._bd_ContactName.Name = "_bd_ContactName";
            this._bd_ContactName.Size = new System.Drawing.Size(264, 20);
            this._bd_ContactName.TabIndex = 40;
            this._bd_ContactName.Leave += new System.EventHandler(this.OnTextBoxLeave);
            this._bd_ContactName.Enter += new System.EventHandler(this.OnTextBoxEnter);
            // 
            // _bpo2_Email
            // 
            this._bpo2_Email.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._bpo2_Email.Location = new System.Drawing.Point(624, 132);
            this._bpo2_Email.MaxLength = 80;
            this._bpo2_Email.Name = "_bpo2_Email";
            this._bpo2_Email.Size = new System.Drawing.Size(99, 20);
            this._bpo2_Email.TabIndex = 38;
            this._bpo2_Email.Leave += new System.EventHandler(this.OnTextBoxLeave);
            this._bpo2_Email.Enter += new System.EventHandler(this.OnTextBoxEnter);
            // 
            // _bpo2_FaxNr
            // 
            this._bpo2_FaxNr.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._bpo2_FaxNr.Location = new System.Drawing.Point(512, 132);
            this._bpo2_FaxNr.MaxLength = 40;
            this._bpo2_FaxNr.Name = "_bpo2_FaxNr";
            this._bpo2_FaxNr.Size = new System.Drawing.Size(104, 20);
            this._bpo2_FaxNr.TabIndex = 37;
            this._bpo2_FaxNr.Leave += new System.EventHandler(this.OnTextBoxLeave);
            this._bpo2_FaxNr.Enter += new System.EventHandler(this.OnTextBoxEnter);
            // 
            // _bpo2_PhoneNr
            // 
            this._bpo2_PhoneNr.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._bpo2_PhoneNr.Location = new System.Drawing.Point(400, 132);
            this._bpo2_PhoneNr.MaxLength = 40;
            this._bpo2_PhoneNr.Name = "_bpo2_PhoneNr";
            this._bpo2_PhoneNr.Size = new System.Drawing.Size(104, 20);
            this._bpo2_PhoneNr.TabIndex = 36;
            this._bpo2_PhoneNr.Leave += new System.EventHandler(this.OnTextBoxLeave);
            this._bpo2_PhoneNr.Enter += new System.EventHandler(this.OnTextBoxEnter);
            // 
            // _bpo2_ContactName
            // 
            this._bpo2_ContactName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._bpo2_ContactName.Location = new System.Drawing.Point(128, 132);
            this._bpo2_ContactName.MaxLength = 100;
            this._bpo2_ContactName.Name = "_bpo2_ContactName";
            this._bpo2_ContactName.Size = new System.Drawing.Size(264, 20);
            this._bpo2_ContactName.TabIndex = 35;
            this._bpo2_ContactName.Leave += new System.EventHandler(this.OnTextBoxLeave);
            this._bpo2_ContactName.Enter += new System.EventHandler(this.OnTextBoxEnter);
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(25, 134);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(95, 13);
            this.label40.TabIndex = 34;
            this.label40.Text = "BPO / Appraisal 2:";
            this.label40.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // _bd_FaxNr
            // 
            this._bd_FaxNr.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._bd_FaxNr.Location = new System.Drawing.Point(512, 153);
            this._bd_FaxNr.MaxLength = 40;
            this._bd_FaxNr.Name = "_bd_FaxNr";
            this._bd_FaxNr.Size = new System.Drawing.Size(104, 20);
            this._bd_FaxNr.TabIndex = 42;
            this._bd_FaxNr.Leave += new System.EventHandler(this.OnTextBoxLeave);
            this._bd_FaxNr.Enter += new System.EventHandler(this.OnTextBoxEnter);
            // 
            // _bd_PhoneNr
            // 
            this._bd_PhoneNr.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._bd_PhoneNr.Location = new System.Drawing.Point(400, 153);
            this._bd_PhoneNr.MaxLength = 40;
            this._bd_PhoneNr.Name = "_bd_PhoneNr";
            this._bd_PhoneNr.Size = new System.Drawing.Size(104, 20);
            this._bd_PhoneNr.TabIndex = 41;
            this._bd_PhoneNr.Leave += new System.EventHandler(this.OnTextBoxLeave);
            this._bd_PhoneNr.Enter += new System.EventHandler(this.OnTextBoxEnter);
            // 
            // _bpo_Email
            // 
            this._bpo_Email.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._bpo_Email.Location = new System.Drawing.Point(624, 111);
            this._bpo_Email.MaxLength = 80;
            this._bpo_Email.Name = "_bpo_Email";
            this._bpo_Email.Size = new System.Drawing.Size(99, 20);
            this._bpo_Email.TabIndex = 33;
            this._bpo_Email.Leave += new System.EventHandler(this.OnTextBoxLeave);
            this._bpo_Email.Enter += new System.EventHandler(this.OnTextBoxEnter);
            // 
            // _bpo_FaxNr
            // 
            this._bpo_FaxNr.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._bpo_FaxNr.Location = new System.Drawing.Point(512, 111);
            this._bpo_FaxNr.MaxLength = 40;
            this._bpo_FaxNr.Name = "_bpo_FaxNr";
            this._bpo_FaxNr.Size = new System.Drawing.Size(104, 20);
            this._bpo_FaxNr.TabIndex = 32;
            this._bpo_FaxNr.Leave += new System.EventHandler(this.OnTextBoxLeave);
            this._bpo_FaxNr.Enter += new System.EventHandler(this.OnTextBoxEnter);
            // 
            // _bpo_PhonrNr
            // 
            this._bpo_PhonrNr.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._bpo_PhonrNr.Location = new System.Drawing.Point(400, 111);
            this._bpo_PhonrNr.MaxLength = 40;
            this._bpo_PhonrNr.Name = "_bpo_PhonrNr";
            this._bpo_PhonrNr.Size = new System.Drawing.Size(104, 20);
            this._bpo_PhonrNr.TabIndex = 31;
            this._bpo_PhonrNr.Leave += new System.EventHandler(this.OnTextBoxLeave);
            this._bpo_PhonrNr.Enter += new System.EventHandler(this.OnTextBoxEnter);
            // 
            // _bpo_ContactName
            // 
            this._bpo_ContactName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._bpo_ContactName.Location = new System.Drawing.Point(128, 111);
            this._bpo_ContactName.MaxLength = 100;
            this._bpo_ContactName.Name = "_bpo_ContactName";
            this._bpo_ContactName.Size = new System.Drawing.Size(264, 20);
            this._bpo_ContactName.TabIndex = 30;
            this._bpo_ContactName.Leave += new System.EventHandler(this.OnTextBoxLeave);
            this._bpo_ContactName.Enter += new System.EventHandler(this.OnTextBoxEnter);
            // 
            // _nm_Email
            // 
            this._nm_Email.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._nm_Email.Location = new System.Drawing.Point(624, 90);
            this._nm_Email.MaxLength = 80;
            this._nm_Email.Name = "_nm_Email";
            this._nm_Email.Size = new System.Drawing.Size(99, 20);
            this._nm_Email.TabIndex = 28;
            this._nm_Email.Leave += new System.EventHandler(this.OnTextBoxLeave);
            this._nm_Email.Enter += new System.EventHandler(this.OnTextBoxEnter);
            // 
            // _nm_FaxNr
            // 
            this._nm_FaxNr.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._nm_FaxNr.Location = new System.Drawing.Point(512, 90);
            this._nm_FaxNr.MaxLength = 40;
            this._nm_FaxNr.Name = "_nm_FaxNr";
            this._nm_FaxNr.Size = new System.Drawing.Size(104, 20);
            this._nm_FaxNr.TabIndex = 27;
            this._nm_FaxNr.Leave += new System.EventHandler(this.OnTextBoxLeave);
            this._nm_FaxNr.Enter += new System.EventHandler(this.OnTextBoxEnter);
            // 
            // _nm_PhoneNr
            // 
            this._nm_PhoneNr.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._nm_PhoneNr.Location = new System.Drawing.Point(400, 90);
            this._nm_PhoneNr.MaxLength = 40;
            this._nm_PhoneNr.Name = "_nm_PhoneNr";
            this._nm_PhoneNr.Size = new System.Drawing.Size(104, 20);
            this._nm_PhoneNr.TabIndex = 26;
            this._nm_PhoneNr.Leave += new System.EventHandler(this.OnTextBoxLeave);
            this._nm_PhoneNr.Enter += new System.EventHandler(this.OnTextBoxEnter);
            // 
            // _nm_ContactName
            // 
            this._nm_ContactName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._nm_ContactName.Location = new System.Drawing.Point(128, 90);
            this._nm_ContactName.MaxLength = 100;
            this._nm_ContactName.Name = "_nm_ContactName";
            this._nm_ContactName.Size = new System.Drawing.Size(264, 20);
            this._nm_ContactName.TabIndex = 25;
            this._nm_ContactName.Leave += new System.EventHandler(this.OnTextBoxLeave);
            this._nm_ContactName.Enter += new System.EventHandler(this.OnTextBoxEnter);
            // 
            // _n_Email
            // 
            this._n_Email.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._n_Email.Location = new System.Drawing.Point(624, 69);
            this._n_Email.MaxLength = 80;
            this._n_Email.Name = "_n_Email";
            this._n_Email.Size = new System.Drawing.Size(99, 20);
            this._n_Email.TabIndex = 23;
            this._n_Email.Leave += new System.EventHandler(this.OnTextBoxLeave);
            this._n_Email.Enter += new System.EventHandler(this.OnTextBoxEnter);
            // 
            // _n_FaxNr
            // 
            this._n_FaxNr.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._n_FaxNr.Location = new System.Drawing.Point(512, 69);
            this._n_FaxNr.MaxLength = 40;
            this._n_FaxNr.Name = "_n_FaxNr";
            this._n_FaxNr.Size = new System.Drawing.Size(104, 20);
            this._n_FaxNr.TabIndex = 22;
            this._n_FaxNr.Leave += new System.EventHandler(this.OnTextBoxLeave);
            this._n_FaxNr.Enter += new System.EventHandler(this.OnTextBoxEnter);
            // 
            // _n_PhoneNr
            // 
            this._n_PhoneNr.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._n_PhoneNr.Location = new System.Drawing.Point(400, 69);
            this._n_PhoneNr.MaxLength = 40;
            this._n_PhoneNr.Name = "_n_PhoneNr";
            this._n_PhoneNr.Size = new System.Drawing.Size(104, 20);
            this._n_PhoneNr.TabIndex = 21;
            this._n_PhoneNr.Leave += new System.EventHandler(this.OnTextBoxLeave);
            this._n_PhoneNr.Enter += new System.EventHandler(this.OnTextBoxEnter);
            // 
            // _n_ContactName
            // 
            this._n_ContactName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._n_ContactName.Location = new System.Drawing.Point(128, 69);
            this._n_ContactName.MaxLength = 100;
            this._n_ContactName.Name = "_n_ContactName";
            this._n_ContactName.Size = new System.Drawing.Size(264, 20);
            this._n_ContactName.TabIndex = 20;
            this._n_ContactName.Leave += new System.EventHandler(this.OnTextBoxLeave);
            this._n_ContactName.Enter += new System.EventHandler(this.OnTextBoxEnter);
            // 
            // _sm_Email
            // 
            this._sm_Email.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._sm_Email.Location = new System.Drawing.Point(624, 48);
            this._sm_Email.MaxLength = 80;
            this._sm_Email.Name = "_sm_Email";
            this._sm_Email.Size = new System.Drawing.Size(99, 20);
            this._sm_Email.TabIndex = 18;
            this._sm_Email.Leave += new System.EventHandler(this.OnTextBoxLeave);
            this._sm_Email.Enter += new System.EventHandler(this.OnTextBoxEnter);
            // 
            // _sm_FaxNr
            // 
            this._sm_FaxNr.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._sm_FaxNr.Location = new System.Drawing.Point(512, 48);
            this._sm_FaxNr.MaxLength = 40;
            this._sm_FaxNr.Name = "_sm_FaxNr";
            this._sm_FaxNr.Size = new System.Drawing.Size(104, 20);
            this._sm_FaxNr.TabIndex = 17;
            this._sm_FaxNr.Leave += new System.EventHandler(this.OnTextBoxLeave);
            this._sm_FaxNr.Enter += new System.EventHandler(this.OnTextBoxEnter);
            // 
            // _sm_PhoneNr
            // 
            this._sm_PhoneNr.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._sm_PhoneNr.Location = new System.Drawing.Point(400, 48);
            this._sm_PhoneNr.MaxLength = 40;
            this._sm_PhoneNr.Name = "_sm_PhoneNr";
            this._sm_PhoneNr.Size = new System.Drawing.Size(104, 20);
            this._sm_PhoneNr.TabIndex = 16;
            this._sm_PhoneNr.Leave += new System.EventHandler(this.OnTextBoxLeave);
            this._sm_PhoneNr.Enter += new System.EventHandler(this.OnTextBoxEnter);
            // 
            // _sm_ContactName
            // 
            this._sm_ContactName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._sm_ContactName.Location = new System.Drawing.Point(128, 48);
            this._sm_ContactName.MaxLength = 100;
            this._sm_ContactName.Name = "_sm_ContactName";
            this._sm_ContactName.Size = new System.Drawing.Size(264, 20);
            this._sm_ContactName.TabIndex = 15;
            this._sm_ContactName.Leave += new System.EventHandler(this.OnTextBoxLeave);
            this._sm_ContactName.Enter += new System.EventHandler(this.OnTextBoxEnter);
            // 
            // _se_Email
            // 
            this._se_Email.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._se_Email.Location = new System.Drawing.Point(624, 27);
            this._se_Email.MaxLength = 80;
            this._se_Email.Name = "_se_Email";
            this._se_Email.Size = new System.Drawing.Size(99, 20);
            this._se_Email.TabIndex = 13;
            this._se_Email.Leave += new System.EventHandler(this.OnTextBoxLeave);
            this._se_Email.Enter += new System.EventHandler(this.OnTextBoxEnter);
            // 
            // _se_FaxNr
            // 
            this._se_FaxNr.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._se_FaxNr.Location = new System.Drawing.Point(512, 27);
            this._se_FaxNr.MaxLength = 40;
            this._se_FaxNr.Name = "_se_FaxNr";
            this._se_FaxNr.Size = new System.Drawing.Size(104, 20);
            this._se_FaxNr.TabIndex = 12;
            this._se_FaxNr.Leave += new System.EventHandler(this.OnTextBoxLeave);
            this._se_FaxNr.Enter += new System.EventHandler(this.OnTextBoxEnter);
            // 
            // _se_PhoneNr
            // 
            this._se_PhoneNr.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._se_PhoneNr.Location = new System.Drawing.Point(400, 27);
            this._se_PhoneNr.MaxLength = 40;
            this._se_PhoneNr.Name = "_se_PhoneNr";
            this._se_PhoneNr.Size = new System.Drawing.Size(104, 20);
            this._se_PhoneNr.TabIndex = 11;
            this._se_PhoneNr.Leave += new System.EventHandler(this.OnTextBoxLeave);
            this._se_PhoneNr.Enter += new System.EventHandler(this.OnTextBoxEnter);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(27, 155);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(93, 13);
            this.label21.TabIndex = 6;
            this.label21.Text = "Bankruptcy Dept.:";
            this.label21.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(25, 113);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(95, 13);
            this.label18.TabIndex = 5;
            this.label18.Text = "BPO / Appraisal 1:";
            this.label18.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(42, 92);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(78, 13);
            this.label17.TabIndex = 4;
            this.label17.Text = "Neg. Manager:";
            this.label17.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(61, 71);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(59, 13);
            this.label14.TabIndex = 3;
            this.label14.Text = "Negotiator:";
            this.label14.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(37, 50);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(83, 13);
            this.label13.TabIndex = 2;
            this.label13.Text = "Setup Manager:";
            this.label13.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(53, 29);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(67, 13);
            this.label12.TabIndex = 1;
            this.label12.Text = "Setup Dept.:";
            this.label12.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // _se_ContactName
            // 
            this._se_ContactName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._se_ContactName.Location = new System.Drawing.Point(128, 27);
            this._se_ContactName.MaxLength = 100;
            this._se_ContactName.Name = "_se_ContactName";
            this._se_ContactName.Size = new System.Drawing.Size(264, 20);
            this._se_ContactName.TabIndex = 10;
            this._se_ContactName.Leave += new System.EventHandler(this.OnTextBoxLeave);
            this._se_ContactName.Enter += new System.EventHandler(this.OnTextBoxEnter);
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(621, 11);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(32, 13);
            this.label28.TabIndex = 10;
            this.label28.Text = "Email";
            this.label28.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(510, 11);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(24, 13);
            this.label27.TabIndex = 9;
            this.label27.Text = "Fax";
            this.label27.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(400, 11);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(38, 13);
            this.label26.TabIndex = 8;
            this.label26.Text = "Phone";
            this.label26.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(125, 11);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(75, 13);
            this.label22.TabIndex = 7;
            this.label22.Text = "Contact Name";
            this.label22.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // label85
            // 
            this.label85.AutoSize = true;
            this.label85.Location = new System.Drawing.Point(13, 197);
            this.label85.Name = "label85";
            this.label85.Size = new System.Drawing.Size(107, 13);
            this.label85.TabIndex = 38;
            this.label85.Text = "Foreclosure Attorney:";
            this.label85.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // _printContactInfo
            // 
            this._printContactInfo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._printContactInfo.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._printContactInfo.Location = new System.Drawing.Point(316, 6);
            this._printContactInfo.Name = "_printContactInfo";
            this._printContactInfo.Size = new System.Drawing.Size(78, 23);
            this._printContactInfo.TabIndex = 58;
            this._printContactInfo.Text = "Print Contact Info";
            this._printContactInfo.Visible = false;
            // 
            // _accountNr
            // 
            this._accountNr.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._accountNr.Location = new System.Drawing.Point(102, 30);
            this._accountNr.MaxLength = 40;
            this._accountNr.Name = "_accountNr";
            this._accountNr.Size = new System.Drawing.Size(152, 20);
            this._accountNr.TabIndex = 54;
            this._accountNr.Leave += new System.EventHandler(this.OnTextBoxLeave);
            this._accountNr.Enter += new System.EventHandler(this.OnTextBoxEnter);
            // 
            // _openLogFile
            // 
            this._openLogFile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._openLogFile.Location = new System.Drawing.Point(677, 30);
            this._openLogFile.Name = "_openLogFile";
            this._openLogFile.Size = new System.Drawing.Size(158, 23);
            this._openLogFile.TabIndex = 57;
            this._openLogFile.Text = "Open Log File";
            this._openLogFile.UseVisualStyleBackColor = true;
            // 
            // _browseLogFile
            // 
            this._browseLogFile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._browseLogFile.Location = new System.Drawing.Point(677, 6);
            this._browseLogFile.Name = "_browseLogFile";
            this._browseLogFile.Size = new System.Drawing.Size(158, 23);
            this._browseLogFile.TabIndex = 56;
            this._browseLogFile.Text = "Open Working Folder";
            this._browseLogFile.UseVisualStyleBackColor = true;
            // 
            // _correspondenceLogFile
            // 
            this._correspondenceLogFile.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this._correspondenceLogFile.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._correspondenceLogFile.Location = new System.Drawing.Point(433, 30);
            this._correspondenceLogFile.MaxLength = 40;
            this._correspondenceLogFile.Name = "_correspondenceLogFile";
            this._correspondenceLogFile.ReadOnly = true;
            this._correspondenceLogFile.Size = new System.Drawing.Size(83, 20);
            this._correspondenceLogFile.TabIndex = 55;
            this._correspondenceLogFile.Visible = false;
            // 
            // _pictureBoxGeneralShortSale
            // 
            this._pictureBoxGeneralShortSale.Image = ((System.Drawing.Image)(resources.GetObject("_pictureBoxGeneralShortSale.Image")));
            this._pictureBoxGeneralShortSale.Location = new System.Drawing.Point(269, 30);
            this._pictureBoxGeneralShortSale.Name = "_pictureBoxGeneralShortSale";
            this._pictureBoxGeneralShortSale.Size = new System.Drawing.Size(20, 20);
            this._pictureBoxGeneralShortSale.TabIndex = 62;
            this._pictureBoxGeneralShortSale.TabStop = false;
            this._pictureBoxGeneralShortSale.Visible = false;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(37, 32);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(60, 13);
            this.label10.TabIndex = 53;
            this.label10.Text = "Account #:";
            this.label10.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // _companyName
            // 
            this._companyName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._companyName.Location = new System.Drawing.Point(102, 9);
            this._companyName.MaxLength = 150;
            this._companyName.Name = "_companyName";
            this._companyName.Size = new System.Drawing.Size(152, 20);
            this._companyName.TabIndex = 52;
            this._companyName.Leave += new System.EventHandler(this.OnTextBoxLeave);
            this._companyName.Enter += new System.EventHandler(this.OnTextBoxEnter);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(12, 11);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(85, 13);
            this.label9.TabIndex = 51;
            this.label9.Text = "Company Name:";
            this.label9.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this._strategyDesc);
            this.groupBox2.Controls.Add(this._strategy);
            this.groupBox2.Controls.Add(this.label38);
            this.groupBox2.Controls.Add(this.label37);
            this.groupBox2.Controls.Add(this._counter_Fifth);
            this.groupBox2.Controls.Add(this._offer_Fifth);
            this.groupBox2.Controls.Add(this._counter_Fourth);
            this.groupBox2.Controls.Add(this._offer_Fourth);
            this.groupBox2.Controls.Add(this._counter_Third);
            this.groupBox2.Controls.Add(this._offer_Third);
            this.groupBox2.Controls.Add(this._counter_Second);
            this.groupBox2.Controls.Add(this._offer_Second);
            this.groupBox2.Controls.Add(this._counter_First);
            this.groupBox2.Controls.Add(this._offer_First);
            this.groupBox2.Controls.Add(this.label34);
            this.groupBox2.Controls.Add(this.label33);
            this.groupBox2.Controls.Add(this.label32);
            this.groupBox2.Controls.Add(this.label31);
            this.groupBox2.Controls.Add(this.label30);
            this.groupBox2.Controls.Add(this._maxToPay);
            this.groupBox2.Controls.Add(this.label29);
            this.groupBox2.Location = new System.Drawing.Point(12, 279);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(892, 103);
            this.groupBox2.TabIndex = 60;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Strategy";
            // 
            // _strategyDesc
            // 
            this._strategyDesc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this._strategyDesc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._strategyDesc.Location = new System.Drawing.Point(232, 15);
            this._strategyDesc.MaxLength = 1000;
            this._strategyDesc.Multiline = true;
            this._strategyDesc.Name = "_strategyDesc";
            this._strategyDesc.Size = new System.Drawing.Size(654, 21);
            this._strategyDesc.TabIndex = 1;
            this._strategyDesc.Leave += new System.EventHandler(this.OnTextBoxLeave);
            this._strategyDesc.Enter += new System.EventHandler(this.OnTextBoxEnter);
            // 
            // _strategy
            // 
            this._strategy.Items.AddRange(new object[] {
            "--select below--",
            "Full Payoff",
            "Short Sale",
            "Reinstate"});
            this._strategy.Location = new System.Drawing.Point(12, 15);
            this._strategy.Name = "_strategy";
            this._strategy.Size = new System.Drawing.Size(208, 21);
            this._strategy.TabIndex = 0;
            this._strategy.Leave += new System.EventHandler(this.OnDropDownLeave);
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(178, 73);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(47, 13);
            this.label38.TabIndex = 24;
            this.label38.Text = "Counter:";
            this.label38.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(192, 52);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(33, 13);
            this.label37.TabIndex = 23;
            this.label37.Text = "Offer:";
            this.label37.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // _counter_Fifth
            // 
            this._counter_Fifth.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._counter_Fifth.Location = new System.Drawing.Point(647, 73);
            this._counter_Fifth.MaxLength = 30;
            this._counter_Fifth.Name = "_counter_Fifth";
            this._counter_Fifth.Size = new System.Drawing.Size(96, 20);
            this._counter_Fifth.TabIndex = 20;
            this._counter_Fifth.Leave += new System.EventHandler(this.OnTextBoxLeave);
            this._counter_Fifth.Enter += new System.EventHandler(this.OnTextBoxEnter);
            // 
            // _offer_Fifth
            // 
            this._offer_Fifth.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._offer_Fifth.Location = new System.Drawing.Point(647, 52);
            this._offer_Fifth.MaxLength = 30;
            this._offer_Fifth.Name = "_offer_Fifth";
            this._offer_Fifth.Size = new System.Drawing.Size(96, 20);
            this._offer_Fifth.TabIndex = 19;
            this._offer_Fifth.Leave += new System.EventHandler(this.OnTextBoxLeave);
            this._offer_Fifth.Enter += new System.EventHandler(this.OnTextBoxEnter);
            // 
            // _counter_Fourth
            // 
            this._counter_Fourth.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._counter_Fourth.Location = new System.Drawing.Point(543, 73);
            this._counter_Fourth.MaxLength = 30;
            this._counter_Fourth.Name = "_counter_Fourth";
            this._counter_Fourth.Size = new System.Drawing.Size(96, 20);
            this._counter_Fourth.TabIndex = 18;
            this._counter_Fourth.Leave += new System.EventHandler(this.OnTextBoxLeave);
            this._counter_Fourth.Enter += new System.EventHandler(this.OnTextBoxEnter);
            // 
            // _offer_Fourth
            // 
            this._offer_Fourth.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._offer_Fourth.Location = new System.Drawing.Point(543, 52);
            this._offer_Fourth.MaxLength = 30;
            this._offer_Fourth.Name = "_offer_Fourth";
            this._offer_Fourth.Size = new System.Drawing.Size(96, 20);
            this._offer_Fourth.TabIndex = 17;
            this._offer_Fourth.Leave += new System.EventHandler(this.OnTextBoxLeave);
            this._offer_Fourth.Enter += new System.EventHandler(this.OnTextBoxEnter);
            // 
            // _counter_Third
            // 
            this._counter_Third.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._counter_Third.Location = new System.Drawing.Point(439, 73);
            this._counter_Third.MaxLength = 30;
            this._counter_Third.Name = "_counter_Third";
            this._counter_Third.Size = new System.Drawing.Size(96, 20);
            this._counter_Third.TabIndex = 16;
            this._counter_Third.Leave += new System.EventHandler(this.OnTextBoxLeave);
            this._counter_Third.Enter += new System.EventHandler(this.OnTextBoxEnter);
            // 
            // _offer_Third
            // 
            this._offer_Third.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._offer_Third.Location = new System.Drawing.Point(439, 52);
            this._offer_Third.MaxLength = 30;
            this._offer_Third.Name = "_offer_Third";
            this._offer_Third.Size = new System.Drawing.Size(96, 20);
            this._offer_Third.TabIndex = 15;
            this._offer_Third.Leave += new System.EventHandler(this.OnTextBoxLeave);
            this._offer_Third.Enter += new System.EventHandler(this.OnTextBoxEnter);
            // 
            // _counter_Second
            // 
            this._counter_Second.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._counter_Second.Location = new System.Drawing.Point(335, 73);
            this._counter_Second.MaxLength = 30;
            this._counter_Second.Name = "_counter_Second";
            this._counter_Second.Size = new System.Drawing.Size(96, 20);
            this._counter_Second.TabIndex = 14;
            this._counter_Second.Leave += new System.EventHandler(this.OnTextBoxLeave);
            this._counter_Second.Enter += new System.EventHandler(this.OnTextBoxEnter);
            // 
            // _offer_Second
            // 
            this._offer_Second.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._offer_Second.Location = new System.Drawing.Point(335, 52);
            this._offer_Second.MaxLength = 30;
            this._offer_Second.Name = "_offer_Second";
            this._offer_Second.Size = new System.Drawing.Size(96, 20);
            this._offer_Second.TabIndex = 13;
            this._offer_Second.Leave += new System.EventHandler(this.OnTextBoxLeave);
            this._offer_Second.Enter += new System.EventHandler(this.OnTextBoxEnter);
            // 
            // _counter_First
            // 
            this._counter_First.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._counter_First.Location = new System.Drawing.Point(231, 73);
            this._counter_First.MaxLength = 30;
            this._counter_First.Name = "_counter_First";
            this._counter_First.Size = new System.Drawing.Size(96, 20);
            this._counter_First.TabIndex = 12;
            this._counter_First.Leave += new System.EventHandler(this.OnTextBoxLeave);
            this._counter_First.Enter += new System.EventHandler(this.OnTextBoxEnter);
            // 
            // _offer_First
            // 
            this._offer_First.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._offer_First.Location = new System.Drawing.Point(231, 52);
            this._offer_First.MaxLength = 30;
            this._offer_First.Name = "_offer_First";
            this._offer_First.Size = new System.Drawing.Size(96, 20);
            this._offer_First.TabIndex = 11;
            this._offer_First.Leave += new System.EventHandler(this.OnTextBoxLeave);
            this._offer_First.Enter += new System.EventHandler(this.OnTextBoxEnter);
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(647, 37);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(30, 13);
            this.label34.TabIndex = 8;
            this.label34.Text = "Fifth:";
            this.label34.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(543, 37);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(40, 13);
            this.label33.TabIndex = 7;
            this.label33.Text = "Fourth:";
            this.label33.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(439, 37);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(34, 13);
            this.label32.TabIndex = 6;
            this.label32.Text = "Third:";
            this.label32.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(335, 37);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(47, 13);
            this.label31.TabIndex = 5;
            this.label31.Text = "Second:";
            this.label31.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(231, 37);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(29, 13);
            this.label30.TabIndex = 4;
            this.label30.Text = "First:";
            this.label30.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // _maxToPay
            // 
            this._maxToPay.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._maxToPay.Location = new System.Drawing.Point(12, 52);
            this._maxToPay.Name = "_maxToPay";
            this._maxToPay.Size = new System.Drawing.Size(104, 20);
            this._maxToPay.TabIndex = 3;
            this._maxToPay.Leave += new System.EventHandler(this.OnTextBoxLeave);
            this._maxToPay.Enter += new System.EventHandler(this.OnTextBoxEnter);
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(16, 38);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(67, 13);
            this.label29.TabIndex = 2;
            this.label29.Text = "Max To Pay:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(299, 32);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(128, 13);
            this.label11.TabIndex = 61;
            this.label11.Text = "Correspondence Log File:";
            this.label11.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.label11.Visible = false;
            // 
            // _custom_assignWorkingFolder
            // 
            this._custom_assignWorkingFolder.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._custom_assignWorkingFolder.Location = new System.Drawing.Point(841, 6);
            this._custom_assignWorkingFolder.Name = "_custom_assignWorkingFolder";
            this._custom_assignWorkingFolder.Size = new System.Drawing.Size(63, 23);
            this._custom_assignWorkingFolder.TabIndex = 63;
            this._custom_assignWorkingFolder.Text = "Assign...";
            this._custom_assignWorkingFolder.UseVisualStyleBackColor = true;
            // 
            // _custom_assignLogFile
            // 
            this._custom_assignLogFile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._custom_assignLogFile.Location = new System.Drawing.Point(841, 30);
            this._custom_assignLogFile.Name = "_custom_assignLogFile";
            this._custom_assignLogFile.Size = new System.Drawing.Size(63, 23);
            this._custom_assignLogFile.TabIndex = 64;
            this._custom_assignLogFile.Text = "Assign...";
            this._custom_assignLogFile.UseVisualStyleBackColor = true;
            // 
            // NegotiationControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.Controls.Add(this._custom_assignLogFile);
            this.Controls.Add(this._custom_assignWorkingFolder);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this._printContactInfo);
            this.Controls.Add(this._accountNr);
            this.Controls.Add(this._openLogFile);
            this.Controls.Add(this._browseLogFile);
            this.Controls.Add(this._correspondenceLogFile);
            this.Controls.Add(this._pictureBoxGeneralShortSale);
            this.Controls.Add(this.label10);
            this.Controls.Add(this._companyName);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.label11);
            this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.Name = "NegotiationControl";
            this.Size = new System.Drawing.Size(907, 389);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._pictureBoxGeneralShortSale)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox _fa_web;
        private System.Windows.Forms.TextBox _cs_web;
        private System.Windows.Forms.TextBox _bd_web;
        private System.Windows.Forms.TextBox _bpo2_web;
        private System.Windows.Forms.TextBox _bpo_web;
        private System.Windows.Forms.TextBox _nm_web;
        private System.Windows.Forms.TextBox _n_web;
        private System.Windows.Forms.TextBox _sm_web;
        private System.Windows.Forms.TextBox _se_web;
        private System.Windows.Forms.Label label86;
        private System.Windows.Forms.TextBox _fa_Email;
        private System.Windows.Forms.TextBox _fa_ContactName;
        private System.Windows.Forms.TextBox _cs_Email;
        private System.Windows.Forms.TextBox _cs_Fax;
        private System.Windows.Forms.TextBox _cs_Phone;
        private System.Windows.Forms.TextBox _cs_ContactName;
        private System.Windows.Forms.Label label84;
        private System.Windows.Forms.TextBox _fa_Fax;
        private System.Windows.Forms.TextBox _fa_Phone;
        private System.Windows.Forms.TextBox _bd_Email;
        private System.Windows.Forms.TextBox _bd_ContactName;
        private System.Windows.Forms.TextBox _bpo2_Email;
        private System.Windows.Forms.TextBox _bpo2_FaxNr;
        private System.Windows.Forms.TextBox _bpo2_PhoneNr;
        private System.Windows.Forms.TextBox _bpo2_ContactName;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.TextBox _bd_FaxNr;
        private System.Windows.Forms.TextBox _bd_PhoneNr;
        private System.Windows.Forms.TextBox _bpo_Email;
        private System.Windows.Forms.TextBox _bpo_FaxNr;
        private System.Windows.Forms.TextBox _bpo_PhonrNr;
        private System.Windows.Forms.TextBox _bpo_ContactName;
        private System.Windows.Forms.TextBox _nm_Email;
        private System.Windows.Forms.TextBox _nm_FaxNr;
        private System.Windows.Forms.TextBox _nm_PhoneNr;
        private System.Windows.Forms.TextBox _nm_ContactName;
        private System.Windows.Forms.TextBox _n_Email;
        private System.Windows.Forms.TextBox _n_FaxNr;
        private System.Windows.Forms.TextBox _n_PhoneNr;
        private System.Windows.Forms.TextBox _n_ContactName;
        private System.Windows.Forms.TextBox _sm_Email;
        private System.Windows.Forms.TextBox _sm_FaxNr;
        private System.Windows.Forms.TextBox _sm_PhoneNr;
        private System.Windows.Forms.TextBox _sm_ContactName;
        private System.Windows.Forms.TextBox _se_Email;
        private System.Windows.Forms.TextBox _se_FaxNr;
        private System.Windows.Forms.TextBox _se_PhoneNr;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox _se_ContactName;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label85;
        private System.Windows.Forms.Button _printContactInfo;
        private System.Windows.Forms.TextBox _accountNr;
        private System.Windows.Forms.Button _openLogFile;
        private System.Windows.Forms.Button _browseLogFile;
        private System.Windows.Forms.TextBox _correspondenceLogFile;
        private System.Windows.Forms.PictureBox _pictureBoxGeneralShortSale;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox _companyName;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox _strategyDesc;
        private System.Windows.Forms.ComboBox _strategy;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.TextBox _counter_Fifth;
        private System.Windows.Forms.TextBox _offer_Fifth;
        private System.Windows.Forms.TextBox _counter_Fourth;
        private System.Windows.Forms.TextBox _offer_Fourth;
        private System.Windows.Forms.TextBox _counter_Third;
        private System.Windows.Forms.TextBox _offer_Third;
        private System.Windows.Forms.TextBox _counter_Second;
        private System.Windows.Forms.TextBox _offer_Second;
        private System.Windows.Forms.TextBox _counter_First;
        private System.Windows.Forms.TextBox _offer_First;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox _maxToPay;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button _custom_assignWorkingFolder;
        private System.Windows.Forms.Button _custom_assignLogFile;
    }
}
