﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;
using System.Drawing.Printing;
using System.IO;
using System.Diagnostics;
using System.Collections.Generic;

//Added
using DealMaker.ShortSale;
using DealMaker.ShortSale.Utils;
using DealMaker.ShortSale.Interfaces;
using DealMaker.UserControls.ShortSale;
using DealMaker.PlainClasses.ShortSale.Adapters;
using DealMaker.Forms.ShortSale;
using DealMaker.ShortSale.Forms;
using ProfitGrabber.Common;





namespace DealMaker.UserControls.Lien
{
    public partial class NegotiationControl : BaseShortSaleUserControl
    {
        #region DATA
        Brush _blackBrush = Brushes.Black;
        Font _arial9 = new Font("Arial", 9);
        Font _arial10 = new Font("Arial", 10);
        Font _arial10I = new Font("Arial", 10, FontStyle.Italic);
        Font _arial10B = new Font("Arial", 10, FontStyle.Bold);
        Font _arial10U = new Font("Arial", 10, FontStyle.Underline);
        Font _arial10BU = new Font("Arial", 10, FontStyle.Bold | FontStyle.Underline);
        Font _arial12 = new Font("Arial", 12);
        Font _arial14 = new Font("Arial", 14);
        Font _arial16 = new Font("Arial", 16);
        Font _arial18 = new Font("Arial", 18);
        Font _arial20 = new Font("Arial", 20);
        Font _arial22 = new Font("Arial", 22);
        Font _arial24 = new Font("Arial", 24);
        Font _arial26 = new Font("Arial", 26);
        
        int _liensRendered = 0;        
        const string NA = "N/A";
        const string Blank = "### Not Complete ###";
        CustomSettings _cs = null;
        #endregion

        #region CTR
        public NegotiationControl()
        {
            InitializeComponent();

            AddEventHandlers();

            string error;
            _cs = new CustomSettingsManager().LoadCustomSettings(out error);
            ApplyCustomSSXSettings((((null != _cs) && (1 == _cs.SpecialSSX)) ? true : false));
        }
        #endregion

        #region IDisplayControlObject
        IDisplayControlObject _displayControlObject;
        public IDisplayControlObject DisplayControlObject
        {
            get { return _displayControlObject; }
            set { _displayControlObject = value; }
        }
        #endregion

        #region PROPERTIES
                                
        public string counter_Fifth
        {
            get
            {
                return _counter_Fifth.Text;
            }
            set
            {
                if (_counter_Fifth.Text == value)
                    return;
                _counter_Fifth.Text = value;
            }
        }
        public string offer_Fifth
        {
            get
            {
                return _offer_Fifth.Text;
            }
            set
            {
                if (_offer_Fifth.Text == value)
                    return;
                _offer_Fifth.Text = value;
            }
        }
        public string counter_Fourth
        {
            get
            {
                return _counter_Fourth.Text;
            }
            set
            {
                if (_counter_Fourth.Text == value)
                    return;
                _counter_Fourth.Text = value;
            }
        }
        public string offer_Fourth
        {
            get
            {
                return _offer_Fourth.Text;
            }
            set
            {
                if (_offer_Fourth.Text == value)
                    return;
                _offer_Fourth.Text = value;
            }
        }
        public string counter_Third
        {
            get
            {
                return _counter_Third.Text;
            }
            set
            {
                if (_counter_Third.Text == value)
                    return;
                _counter_Third.Text = value;
            }
        }
        public string offer_Third
        {
            get
            {
                return _offer_Third.Text;
            }
            set
            {
                if (_offer_Third.Text == value)
                    return;
                _offer_Third.Text = value;
            }
        }
        public string counter_Second
        {
            get
            {
                return _counter_Second.Text;
            }
            set
            {
                if (_counter_Second.Text == value)
                    return;
                _counter_Second.Text = value;
            }
        }
        public string offer_Second
        {
            get
            {
                return _offer_Second.Text;
            }
            set
            {
                if (_offer_Second.Text == value)
                    return;
                _offer_Second.Text = value;
            }
        }
        public string counter_First
        {
            get
            {
                return _counter_First.Text;
            }
            set
            {
                if (_counter_First.Text == value)
                    return;
                _counter_First.Text = value;
            }
        }
        public string offer_First
        {
            get
            {
                return _offer_First.Text;
            }
            set
            {
                if (_offer_First.Text == value)
                    return;
                _offer_First.Text = value;
            }
        }
        public string maxToPay
        {
            get
            {
                return _maxToPay.Text;
            }
            set
            {
                if (_maxToPay.Text == value)
                    return;
                _maxToPay.Text = value;
            }
        }
        public string strategyDesc
        {
            get
            {
                return _strategyDesc.Text;
            }
            set
            {
                if (_strategyDesc.Text == value)
                    return;
                _strategyDesc.Text = value;
            }
        }
        public string strategy
        {
            get
            {
                return _strategy.Text;
            }
            set
            {
                if (_strategy.Text == value)
                    return;
                _strategy.Text = value;
            }
        }
        public string fa_Web
        {
            get { return _fa_web.Text; }
            set { _fa_web.Text = value; }
        }
        public string fa_FaxNr
        {
            get
            {
                return _fa_Fax.Text;
            }
            set
            {
                if (_fa_Fax.Text == value)
                    return;
                _fa_Fax.Text = value;
            }
        }
        public string fa_PhoneNr
        {
            get
            {
                return _fa_Phone.Text;
            }
            set
            {
                if (_fa_Phone.Text == value)
                    return;
                _fa_Phone.Text = value;
            }
        }
        public string fa_ContactName
        {
            get { return _fa_ContactName.Text; }
            set { _fa_ContactName.Text = value; }
        }
        public string fa_Email
        {
            get { return _fa_Email.Text; }
            set { _fa_Email.Text = value; }
        }
        public string cs_Web
        {
            get { return _cs_web.Text; }
            set { _cs_web.Text = value; }
        }
        public string cs_FaxNr
        {
            get
            {
                return _cs_Fax.Text;
            }
            set
            {
                if (_cs_Fax.Text == value)
                    return;
                _cs_Fax.Text = value;
            }
        }
        public string cs_PhoneNr
        {
            get
            {
                return _cs_Phone.Text;
            }
            set
            {
                if (_cs_Phone.Text == value)
                    return;
                _cs_Phone.Text = value;
            }
        }
        public string cs_ContactName
        {
            get { return _cs_ContactName.Text; }
            set { _cs_ContactName.Text = value; }
        }
        public string cs_Email
        {
            get { return _cs_Email.Text; }
            set { _cs_Email.Text = value; }
        }
        public string bd_Web
        {
            get { return _bd_web.Text; }
            set { _bd_web.Text = value; }
        }
        public string bd_ContactName
        {
            get { return _bd_ContactName.Text; }
            set { _bd_ContactName.Text = value; }
        }
        public string bd_PhoneNr
        {
            get { return _bd_PhoneNr.Text; }
            set { _bd_PhoneNr.Text = value; }
        }
        public string bd_FaxNr
        {
            get { return _bd_FaxNr.Text; }
            set { _bd_FaxNr.Text = value; }
        }
        public string bd_Email
        {
            get { return _bd_Email.Text; }
            set { _bd_Email.Text = value; }
        }
        public string bpo2_Web
        {
            get { return _bpo2_web.Text; }
            set { _bpo2_web.Text = value; }
        }
        public string bpo2_Email
        {
            get
            {
                return _bpo2_Email.Text;
            }
            set
            {
                if (_bpo2_Email.Text == value)
                    return;
                _bpo2_Email.Text = value;
            }
        }
        public string bpo2_FaxNr
        {
            get
            {
                return _bpo2_FaxNr.Text;
            }
            set
            {
                if (_bpo2_FaxNr.Text == value)
                    return;
                _bpo2_FaxNr.Text = value;
            }
        }
        public string bpo2_PhoneNr
        {
            get
            {
                return _bpo2_PhoneNr.Text;
            }
            set
            {
                if (_bpo2_PhoneNr.Text == value)
                    return;
                _bpo2_PhoneNr.Text = value;
            }
        }
        public string bpo2_ContactName
        {
            get
            {
                return _bpo2_ContactName.Text;
            }
            set
            {
                if (_bpo2_ContactName.Text == value)
                    return;
                _bpo2_ContactName.Text = value;
            }
        }
        public string bpo_Web
        {
            get { return _bpo_web.Text; }
            set { _bpo_web.Text = value; }
        }
        public string bpo_Email
        {
            get
            {
                return _bpo_Email.Text;
            }
            set
            {
                if (_bpo_Email.Text == value)
                    return;
                _bpo_Email.Text = value;
            }
        }
        public string bpo_FaxNr
        {
            get
            {
                return _bpo_FaxNr.Text;
            }
            set
            {
                if (_bpo_FaxNr.Text == value)
                    return;
                _bpo_FaxNr.Text = value;
            }
        }
        public string bpo_PhonrNr
        {
            get
            {
                return _bpo_PhonrNr.Text;
            }
            set
            {
                if (_bpo_PhonrNr.Text == value)
                    return;
                _bpo_PhonrNr.Text = value;
            }
        }
        public string bpo_ContactName
        {
            get
            {
                return _bpo_ContactName.Text;
            }
            set
            {
                if (_bpo_ContactName.Text == value)
                    return;
                _bpo_ContactName.Text = value;
            }
        }
        public string nm_Web
        {
            get { return _nm_web.Text; }
            set { _nm_web.Text = value; }
        }
        public string nm_Email
        {
            get
            {
                return _nm_Email.Text;
            }
            set
            {
                if (_nm_Email.Text == value)
                    return;
                _nm_Email.Text = value;
            }
        }
        public string nm_FaxNr
        {
            get
            {
                return _nm_FaxNr.Text;
            }
            set
            {
                if (_nm_FaxNr.Text == value)
                    return;
                _nm_FaxNr.Text = value;
            }
        }
        public string nm_PhoneNr
        {
            get
            {
                return _nm_PhoneNr.Text;
            }
            set
            {
                if (_nm_PhoneNr.Text == value)
                    return;
                _nm_PhoneNr.Text = value;
            }
        }
        public string nm_ContactName
        {
            get
            {
                return _nm_ContactName.Text;
            }
            set
            {
                if (_nm_ContactName.Text == value)
                    return;
                _nm_ContactName.Text = value;
            }
        }
        public string n_Web
        {
            get { return _n_web.Text; }
            set { _n_web.Text = value; }
        }
        public string n_Email
        {
            get
            {
                return _n_Email.Text;
            }
            set
            {
                if (_n_Email.Text == value)
                    return;
                _n_Email.Text = value;
            }
        }
        public string n_FaxNr
        {
            get
            {
                return _n_FaxNr.Text;
            }
            set
            {
                if (_n_FaxNr.Text == value)
                    return;
                _n_FaxNr.Text = value;
            }
        }
        public string n_PhoneNr
        {
            get
            {
                return _n_PhoneNr.Text;
            }
            set
            {
                if (_n_PhoneNr.Text == value)
                    return;
                _n_PhoneNr.Text = value;
            }
        }
        public string n_ContactName
        {
            get
            {
                return _n_ContactName.Text;
            }
            set
            {
                if (_n_ContactName.Text == value)
                    return;
                _n_ContactName.Text = value;
            }
        }
        public string sm_Web
        {
            get { return _sm_web.Text; }
            set { _sm_web.Text = value; }
        }
        public string sm_Email
        {
            get
            {
                return _sm_Email.Text;
            }
            set
            {
                if (_sm_Email.Text == value)
                    return;
                _sm_Email.Text = value;
            }
        }
        public string sm_FaxNr
        {
            get
            {
                return _sm_FaxNr.Text;
            }
            set
            {
                if (_sm_FaxNr.Text == value)
                    return;
                _sm_FaxNr.Text = value;
            }
        }
        public string sm_PhoneNr
        {
            get
            {
                return _sm_PhoneNr.Text;
            }
            set
            {
                if (_sm_PhoneNr.Text == value)
                    return;
                _sm_PhoneNr.Text = value;
            }
        }
        public string sm_ContactName
        {
            get
            {
                return _sm_ContactName.Text;
            }
            set
            {
                if (_sm_ContactName.Text == value)
                    return;
                _sm_ContactName.Text = value;
            }
        }

        public string se_Web
        {
            get { return _se_web.Text; }
            set { _se_web.Text = value; }
        }
        public string se_Email
        {
            get
            {
                return _se_Email.Text;
            }
            set
            {
                if (_se_Email.Text == value)
                    return;
                _se_Email.Text = value;
            }
        }
        public string se_FaxNr
        {
            get
            {
                return _se_FaxNr.Text;
            }
            set
            {
                if (_se_FaxNr.Text == value)
                    return;
                _se_FaxNr.Text = value;
            }
        }
        public string se_PhoneNr
        {
            get
            {
                return _se_PhoneNr.Text;
            }
            set
            {
                if (_se_PhoneNr.Text == value)
                    return;
                _se_PhoneNr.Text = value;
            }
        }
        public string se_ContactName
        {
            get
            {
                return _se_ContactName.Text;
            }
            set
            {
                if (_se_ContactName.Text == value)
                    return;
                _se_ContactName.Text = value;
            }
        }
        public string correspondenceLogFile
        {
            get
            {
                return _correspondenceLogFile.Text;
            }
            set
            {
                if (_correspondenceLogFile.Text == value)
                    return;
                _correspondenceLogFile.Text = value;
            }
        }
        
        public string accountNr
        {
            get
            {
                return _accountNr.Text;
            }
            set
            {
                if (_accountNr.Text == value)
                    return;
                _accountNr.Text = value;
            }
        }
        public string companyName
        {
            get
            {
                return _companyName.Text;
            }
            set
            {
                if (_companyName.Text == value)
                    return;
                _companyName.Text = value;
            }
        }
        #endregion

        #region CLASS HELPER METHODS
        void AddEventHandlers()
        {            
            _pictureBoxGeneralShortSale.MouseHover += new EventHandler(On_PictureBoxGeneralShortSale_MouseHover);
            _companyName.Leave += new EventHandler(On_CompanyName_Leave_AdditionalHandler);
            _printContactInfo.Click += new EventHandler(On_PrintContactInfo_Click);                        
            _browseLogFile.Click += new EventHandler(On_browseLogFile_Click);
            _openLogFile.Click += new EventHandler(On_OpenLogFile_Click);

            _custom_assignWorkingFolder.Click += new EventHandler(On_Custom_assignWorkingFolder_Click);
            _custom_assignLogFile.Click += new EventHandler(On_Custom_assignLogFile_Click);
        }        

        public void RemoveEventHandlers()
        {            
            _pictureBoxGeneralShortSale.MouseHover -= new EventHandler(On_PictureBoxGeneralShortSale_MouseHover);
            _companyName.Leave -= new EventHandler(On_CompanyName_Leave_AdditionalHandler);
            _printContactInfo.Click -= new EventHandler(On_PrintContactInfo_Click);

            foreach (Control ctrl in this.Controls)
            {
                if (ctrl is ComboBox)
                {
                    ComboBox cb = (ComboBox)ctrl;
                    cb.Leave -= new System.EventHandler(this.OnDropDownLeave);
                }

                if (ctrl is TextBox)
                {
                    TextBox tb = (TextBox)ctrl;
                    tb.Leave -= new System.EventHandler(this.OnTextBoxLeave);
                    tb.Enter -= new System.EventHandler(this.OnTextBoxEnter);
                }
            }


        }


        string GetLienSuffix(int idx)
        {
            string retVal = string.Empty;
            retVal = idx.ToString() + GetSuffix(idx);
            retVal += " Lien";
            return retVal;
        }

        string GetSuffix(int nr)
        {
            if (1 == nr)
                return "st";
            else if (2 == nr)
                return "nd";
            else if (3 == nr)
                return "rd";
            else
                return "th";
        }


        int GetNumberOfLiens()
        {
            try { return DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien.Length; }
            catch { return 0; }
        }

        int GetCurrentLienIdx()
        {
            int retVal = -1;

            try { retVal = (int)this.Tag; }
            catch { retVal = -1; }

            return retVal;
        }
        string GetSelectedDateFromForm(string currentDate)
        {
            DealMaker.Forms.ShortSale.DateTimePickerForm dtpf = new DealMaker.Forms.ShortSale.DateTimePickerForm();
            dtpf.ShowDialog();

            switch (dtpf.DatePickerOption)
            {
                case eDatePickerOptions.Select:
                    return dtpf.SelectedDateAsString;

                case eDatePickerOptions.KeepCurrent:
                    return currentDate;

                case eDatePickerOptions.Clear:
                    return string.Empty;

                default:
                    return string.Empty;
            }
        }


        string GetSiteAddress()
        {
            string siteAddress = string.Empty;

            if (string.Empty != DataManager.Instance.GetCurrentShortSaleDocument().ContactInfo.SiteAddress)
                siteAddress += DataManager.Instance.GetCurrentShortSaleDocument().ContactInfo.SiteAddress + " ";

            if (string.Empty != DataManager.Instance.GetCurrentShortSaleDocument().ContactInfo.SiteCity)
                siteAddress += DataManager.Instance.GetCurrentShortSaleDocument().ContactInfo.SiteCity + " ";

            if (string.Empty != DataManager.Instance.GetCurrentShortSaleDocument().ContactInfo.SiteState)
                siteAddress += DataManager.Instance.GetCurrentShortSaleDocument().ContactInfo.SiteState;

            if (string.Empty != DataManager.Instance.GetCurrentShortSaleDocument().ContactInfo.SiteZIP)
            {
                if (string.Empty != DataManager.Instance.GetCurrentShortSaleDocument().ContactInfo.SiteState)
                    siteAddress += ", ";

                siteAddress += DataManager.Instance.GetCurrentShortSaleDocument().ContactInfo.SiteZIP;
            }

            return siteAddress;
        }        

        bool GetBoolanValueFromString(string text)
        {
            if (null == text || string.Empty == text)
                return true;

            bool val = false;

            try { val = bool.Parse(text); }
            catch { val = false; }
            return val;
        }

        string[] GetPartialLinesFromString(Graphics gfx, Font font, int maxWidth, string stringToBreak)
        {
            ArrayList resultLists = new ArrayList();
            string tempLine = string.Empty;
            int i = stringToBreak.IndexOf(" ");
            //string x = stringToBreak.Substring(0, 11);
            while (-1 != i)
            {
                if ((int)(gfx.MeasureString(tempLine + stringToBreak.Substring(0, i), font).Width) < maxWidth)
                {
                    tempLine += stringToBreak.Substring(0, i + 1);
                    stringToBreak = stringToBreak.Substring(i + 1, stringToBreak.Length - (i + 1));
                }
                else
                {
                    string[] splittedLine = tempLine.Split('#');
                    resultLists.AddRange(splittedLine);
                    tempLine = string.Empty;
                }


                i = stringToBreak.IndexOf(" ");
            }

            tempLine += stringToBreak;
            resultLists.Add(tempLine);
            return (string[])resultLists.ToArray(typeof(string));
        }


        #endregion

        #region DISPLAY DATA
        bool GetState(string text)
        {
            if ("True" == text)
                return true;
            else
                return false;
        }
        public void DisplayData(int lienIdx)
        {
            this.Tag = lienIdx;
            
            //NEGOTIATION PAGE
            //HEADER
            companyName = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].CompanyName;
            accountNr = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].AccountNr;
            
            correspondenceLogFile = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LogFilePath;

            //SETUP DEPT.
            se_ContactName = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienHolderContactInfo.SetupDept.ContactName;
            se_PhoneNr = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienHolderContactInfo.SetupDept.Phone;
            se_FaxNr = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienHolderContactInfo.SetupDept.Fax;
            se_Email = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienHolderContactInfo.SetupDept.Email;
            se_Web = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienHolderContactInfo.SetupDept.Web;

            //SETUP MANAGER
            sm_ContactName = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienHolderContactInfo.SetupManager.ContactName;
            sm_PhoneNr = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienHolderContactInfo.SetupManager.Phone;
            sm_FaxNr = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienHolderContactInfo.SetupManager.Fax;
            sm_Email = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienHolderContactInfo.SetupManager.Email;
            sm_Web = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienHolderContactInfo.SetupManager.Web;

            //NEGOTIATOR
            n_ContactName = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienHolderContactInfo.Negotiator.ContactName;
            n_PhoneNr = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienHolderContactInfo.Negotiator.Phone;
            n_FaxNr = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienHolderContactInfo.Negotiator.Fax;
            n_Email = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienHolderContactInfo.Negotiator.Email;
            n_Web = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienHolderContactInfo.Negotiator.Web;

            //NEG. MANAGER
            nm_ContactName = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienHolderContactInfo.NegManager.ContactName;
            nm_PhoneNr = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienHolderContactInfo.NegManager.Phone;
            nm_FaxNr = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienHolderContactInfo.NegManager.Fax;
            nm_Email = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienHolderContactInfo.NegManager.Email;
            nm_Web = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienHolderContactInfo.NegManager.Web;

            //BPO1
            bpo_ContactName = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienHolderContactInfo.BPOAppraisal1.ContactName;
            bpo_PhonrNr = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienHolderContactInfo.BPOAppraisal1.Phone;
            bpo_FaxNr = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienHolderContactInfo.BPOAppraisal1.Fax;
            bpo_Email = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienHolderContactInfo.BPOAppraisal1.Email;
            bpo_Web = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienHolderContactInfo.BPOAppraisal1.Web;

            //BPO2
            bpo2_ContactName = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienHolderContactInfo.BPOAppraisal2.ContactName;
            bpo2_PhoneNr = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienHolderContactInfo.BPOAppraisal2.Phone;
            bpo2_FaxNr = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienHolderContactInfo.BPOAppraisal2.Fax;
            bpo2_Email = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienHolderContactInfo.BPOAppraisal2.Email;
            bpo2_Web = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienHolderContactInfo.BPOAppraisal2.Web;

            //BANKRUPCY DEPT
            bd_ContactName = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienHolderContactInfo.BankruptcyDeptField.ContactName;
            bd_PhoneNr = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienHolderContactInfo.BankruptcyDeptField.Phone;
            bd_FaxNr = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienHolderContactInfo.BankruptcyDeptField.Fax;
            bd_Email = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienHolderContactInfo.BankruptcyDeptField.Email;
            bd_Web = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienHolderContactInfo.BankruptcyDeptField.Web;

            //CUSTOMER SERVICE
            cs_ContactName = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienHolderContactInfo.CustomerService.ContactName;
            cs_PhoneNr = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienHolderContactInfo.CustomerService.Phone;
            cs_FaxNr = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienHolderContactInfo.CustomerService.Fax;
            cs_Email = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienHolderContactInfo.CustomerService.Email;
            cs_Web = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienHolderContactInfo.CustomerService.Web;

            //FORECLOSURE ATTORNEY
            fa_ContactName = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienHolderContactInfo.ForeClosureAttorney.ContactName;
            fa_PhoneNr = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienHolderContactInfo.ForeClosureAttorney.Phone;
            fa_FaxNr = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienHolderContactInfo.ForeClosureAttorney.Fax;
            fa_Email = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienHolderContactInfo.ForeClosureAttorney.Email;
            fa_Web = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienHolderContactInfo.ForeClosureAttorney.Web;

            //STRATEGY
            strategyDesc = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienHolderContactInfo.Strategy.SelectedStrategyDesc;
            maxToPay = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienHolderContactInfo.Strategy.MaxToPay;
            offer_First = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienHolderContactInfo.Financials.Offers.First.Offer;
            offer_Second = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienHolderContactInfo.Financials.Offers.Second.Offer;
            offer_Third = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienHolderContactInfo.Financials.Offers.Third.Offer;
            offer_Fourth = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienHolderContactInfo.Financials.Offers.Fourth.Offer;
            offer_Fifth = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienHolderContactInfo.Financials.Offers.Fifth.Offer;
            counter_First = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienHolderContactInfo.Financials.Offers.First.Counter;
            counter_Second = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienHolderContactInfo.Financials.Offers.Second.Counter;
            counter_Third = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienHolderContactInfo.Financials.Offers.Third.Counter;
            counter_Fourth = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienHolderContactInfo.Financials.Offers.Fourth.Counter;
            counter_Fifth = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienHolderContactInfo.Financials.Offers.Fifth.Counter;

            _strategy.SelectedItem = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienHolderContactInfo.Strategy.SelectedStrategy;
            _correspondenceLogFile.Text = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[GetCurrentLienIdx()].LogFilePath;
            
        }

        #endregion

        #region PRINT DATA
        private void On_PrintPage(object sender, PrintPageEventArgs e)
        {
            Graphics gfx = e.Graphics;

            int pageWidth = e.PageSettings.Bounds.Size.Width;
            int pageHeight = e.PageSettings.Bounds.Size.Height;
            int vertOffset = 0;

            vertOffset += 10;


            if (0 == _liensRendered)
            {
                //HEADER
                int boxHeight = 150;
                gfx.DrawLine(Pens.Black, new Point(10, 10), new Point(pageWidth - 10, 10)); //L-R
                gfx.DrawLine(Pens.Black, new Point(10, 10), new Point(10, boxHeight));//L-B
                gfx.DrawLine(Pens.Black, new Point(pageWidth - 10, 10), new Point(pageWidth - 10, boxHeight)); //R-B
                gfx.DrawLine(Pens.Black, new Point(10, boxHeight), new Point(pageWidth - 10, boxHeight)); //BL - BR

                gfx.DrawString("Contact Information for all Lien Holders", _arial14, _blackBrush, new Point(20, 22));
                gfx.DrawString(":Property Address", _arial12, _blackBrush, new Point(240, 54), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                gfx.DrawString(":HomeOwner Full Names", _arial12, _blackBrush, new Point(240, 76), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                gfx.DrawString(":Email", _arial12, _blackBrush, new Point(240, 98), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                gfx.DrawString(":Primary Social", _arial12, _blackBrush, new Point(240, 120), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                gfx.DrawString(":Cell", _arial12, _blackBrush, new Point(600, 98), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                gfx.DrawString(":Secondary Social", _arial12, _blackBrush, new Point(600, 120), new StringFormat(StringFormatFlags.DirectionRightToLeft));


                gfx.DrawString(GetSiteAddress(), _arial12, _blackBrush, new Point(240, 54));
                gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ContactInfo.FullName, _arial12, _blackBrush, new Point(240, 76));
                gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.PrimarySS, _arial12, _blackBrush, new Point(240, 120));
                gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.PrimaryPhone, _arial12, _blackBrush, new Point(600, 98));
                gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.SecondarySS, _arial12, _blackBrush, new Point(600, 120));

                vertOffset += boxHeight;

            }

            for (int i = _liensRendered; i < DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien.Length; i++)
            {
                if (vertOffset + 290 > pageHeight - 20)
                {
                    e.HasMorePages = true;
                    break;
                }
                vertOffset += 20;

                //draw mask
                gfx.DrawString((i + 1).ToString() + GetSuffix(i + 1) + " Lien " + DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[i].CompanyName + ", Account#: " + DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[i].AccountNr, _arial12, _blackBrush, new Point(20, vertOffset));
                vertOffset += 30;
                gfx.DrawString("Contact Name:", _arial12, _blackBrush, new Point(165, vertOffset));
                gfx.DrawString("Phone:", _arial12, _blackBrush, new Point(370, vertOffset));
                gfx.DrawString("Fax:", _arial12, _blackBrush, new Point(510, vertOffset));
                gfx.DrawString("Email:", _arial12, _blackBrush, new Point(645, vertOffset));
                vertOffset += 25;

                //SETUP DEPT
                gfx.DrawString(":.Setup Dept", _arial12, _blackBrush, new Point(150, vertOffset), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[i].LienHolderContactInfo.SetupDept.ContactName, _arial12, _blackBrush, new Point(165, vertOffset));
                gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[i].LienHolderContactInfo.SetupDept.Phone, _arial12, _blackBrush, new Point(370, vertOffset));
                gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[i].LienHolderContactInfo.SetupDept.Fax, _arial12, _blackBrush, new Point(510, vertOffset));
                gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[i].LienHolderContactInfo.SetupDept.Email, _arial12, _blackBrush, new Point(645, vertOffset));
                vertOffset += 25;

                //SETUP MANAGER
                gfx.DrawString(":Setup Manager", _arial12, _blackBrush, new Point(150, vertOffset), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[i].LienHolderContactInfo.SetupManager.ContactName, _arial12, _blackBrush, new Point(165, vertOffset));
                gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[i].LienHolderContactInfo.SetupManager.Phone, _arial12, _blackBrush, new Point(370, vertOffset));
                gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[i].LienHolderContactInfo.SetupManager.Fax, _arial12, _blackBrush, new Point(510, vertOffset));
                gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[i].LienHolderContactInfo.SetupManager.Email, _arial12, _blackBrush, new Point(645, vertOffset));
                vertOffset += 25;

                //NEGOTIATOR
                gfx.DrawString(":Negotiator", _arial12, _blackBrush, new Point(150, vertOffset), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[i].LienHolderContactInfo.Negotiator.ContactName, _arial12, _blackBrush, new Point(165, vertOffset));
                gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[i].LienHolderContactInfo.Negotiator.Phone, _arial12, _blackBrush, new Point(370, vertOffset));
                gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[i].LienHolderContactInfo.Negotiator.Fax, _arial12, _blackBrush, new Point(510, vertOffset));
                gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[i].LienHolderContactInfo.Negotiator.Email, _arial12, _blackBrush, new Point(645, vertOffset));
                vertOffset += 25;

                //NEG. MANAGER
                gfx.DrawString(":Neg. Manager", _arial12, _blackBrush, new Point(150, vertOffset), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[i].LienHolderContactInfo.NegManager.ContactName, _arial12, _blackBrush, new Point(165, vertOffset));
                gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[i].LienHolderContactInfo.NegManager.Phone, _arial12, _blackBrush, new Point(370, vertOffset));
                gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[i].LienHolderContactInfo.NegManager.Fax, _arial12, _blackBrush, new Point(510, vertOffset));
                gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[i].LienHolderContactInfo.NegManager.Email, _arial12, _blackBrush, new Point(645, vertOffset));
                vertOffset += 25;

                //BPO APPRAISAL
                gfx.DrawString(":BPO/Appraisal 1", _arial12, _blackBrush, new Point(150, vertOffset), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[i].LienHolderContactInfo.BPOAppraisal1.ContactName, _arial12, _blackBrush, new Point(165, vertOffset));
                gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[i].LienHolderContactInfo.BPOAppraisal1.Phone, _arial12, _blackBrush, new Point(370, vertOffset));
                gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[i].LienHolderContactInfo.BPOAppraisal1.Fax, _arial12, _blackBrush, new Point(510, vertOffset));
                gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[i].LienHolderContactInfo.BPOAppraisal1.Email, _arial12, _blackBrush, new Point(645, vertOffset));
                vertOffset += 25;

                //BPO APPRAISAL2
                gfx.DrawString(":BPO/Appraisal 2", _arial12, _blackBrush, new Point(150, vertOffset), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[i].LienHolderContactInfo.BPOAppraisal2.ContactName, _arial12, _blackBrush, new Point(165, vertOffset));
                gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[i].LienHolderContactInfo.BPOAppraisal2.Phone, _arial12, _blackBrush, new Point(370, vertOffset));
                gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[i].LienHolderContactInfo.BPOAppraisal2.Fax, _arial12, _blackBrush, new Point(510, vertOffset));
                gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[i].LienHolderContactInfo.BPOAppraisal2.Email, _arial12, _blackBrush, new Point(645, vertOffset));
                vertOffset += 25;

                //BANKRUPCY DEPT.
                gfx.DrawString(":.Bankrupcy Dept", _arial12, _blackBrush, new Point(150, vertOffset), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[i].LienHolderContactInfo.BankruptcyDeptField.ContactName, _arial12, _blackBrush, new Point(165, vertOffset));
                gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[i].LienHolderContactInfo.BankruptcyDeptField.Phone, _arial12, _blackBrush, new Point(370, vertOffset));
                gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[i].LienHolderContactInfo.BankruptcyDeptField.Fax, _arial12, _blackBrush, new Point(510, vertOffset));
                gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[i].LienHolderContactInfo.BankruptcyDeptField.Email, _arial12, _blackBrush, new Point(645, vertOffset));
                vertOffset += 25;

                //CUSTOMER SERVICE
                gfx.DrawString(":Customer Service", _arial12, _blackBrush, new Point(150, vertOffset), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[i].LienHolderContactInfo.CustomerService.Phone, _arial12, _blackBrush, new Point(370, vertOffset));
                gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[i].LienHolderContactInfo.CustomerService.Fax, _arial12, _blackBrush, new Point(510, vertOffset));
                vertOffset += 25;

                //BANKRUPCY DEPT.
                gfx.DrawString(":Forecl. Attorney", _arial12, _blackBrush, new Point(150, vertOffset), new StringFormat(StringFormatFlags.DirectionRightToLeft));
                gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[i].LienHolderContactInfo.ForeClosureAttorney.ContactName, _arial12, _blackBrush, new Point(165, vertOffset));
                gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[i].LienHolderContactInfo.ForeClosureAttorney.Phone, _arial12, _blackBrush, new Point(370, vertOffset));
                gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[i].LienHolderContactInfo.ForeClosureAttorney.Fax, _arial12, _blackBrush, new Point(510, vertOffset));
                gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[i].LienHolderContactInfo.ForeClosureAttorney.Email, _arial12, _blackBrush, new Point(645, vertOffset));

                vertOffset += 30;
                gfx.DrawLine(Pens.Black, new Point(10, vertOffset), new Point(pageWidth - 10, vertOffset));

                _liensRendered++;
            }
        }

        #endregion

        #region EVENT HANDLERS
        private void OnTextBoxEnter(object sender, System.EventArgs e)
        {
            //((TextBox)sender).BackColor = Color.LightSalmon;                                                            
        }


        private void OnTextBoxLeave(object sender, System.EventArgs e)
        {
            try
            {
                int lienIdx = GetCurrentLienIdx();
                ((TextBox)sender).BackColor = Color.White;
                               
                //NEGOTIATION PAGE
                //HEADER
                DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].CompanyName = companyName;
                DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].AccountNr = accountNr;
                
                //DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LogFilePath = correspondenceLogFile;

                //SETUP DEPT
                DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienHolderContactInfo.SetupDept.ContactName = se_ContactName;
                DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienHolderContactInfo.SetupDept.Phone = se_PhoneNr;
                DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienHolderContactInfo.SetupDept.Fax = se_FaxNr;
                DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienHolderContactInfo.SetupDept.Email = se_Email;
                DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienHolderContactInfo.SetupDept.Web = se_Web;

                //SETUP MANAGER
                DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienHolderContactInfo.SetupManager.ContactName = sm_ContactName;
                DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienHolderContactInfo.SetupManager.Phone = sm_PhoneNr;
                DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienHolderContactInfo.SetupManager.Fax = sm_FaxNr;
                DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienHolderContactInfo.SetupManager.Email = sm_Email;
                DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienHolderContactInfo.SetupManager.Web = sm_Web;

                //NEGOTIATOR
                DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienHolderContactInfo.Negotiator.ContactName = n_ContactName;
                DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienHolderContactInfo.Negotiator.Phone = n_PhoneNr;
                DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienHolderContactInfo.Negotiator.Fax = n_FaxNr;
                DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienHolderContactInfo.Negotiator.Email = n_Email;
                DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienHolderContactInfo.Negotiator.Web = n_Web;

                //NEG. MANAGER
                DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienHolderContactInfo.NegManager.ContactName = nm_ContactName;
                DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienHolderContactInfo.NegManager.Phone = nm_PhoneNr;
                DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienHolderContactInfo.NegManager.Fax = nm_FaxNr;
                DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienHolderContactInfo.NegManager.Email = nm_Email;
                DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienHolderContactInfo.NegManager.Web = nm_Web;

                //BPO 1
                DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienHolderContactInfo.BPOAppraisal1.ContactName = bpo_ContactName;
                DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienHolderContactInfo.BPOAppraisal1.Phone = bpo_PhonrNr;
                DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienHolderContactInfo.BPOAppraisal1.Fax = bpo_FaxNr;
                DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienHolderContactInfo.BPOAppraisal1.Email = bpo_Email;
                DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienHolderContactInfo.BPOAppraisal1.Web = bpo_Web;

                //BPO 2
                DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienHolderContactInfo.BPOAppraisal2.ContactName = bpo2_ContactName;
                DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienHolderContactInfo.BPOAppraisal2.Phone = bpo2_PhoneNr;
                DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienHolderContactInfo.BPOAppraisal2.Fax = bpo2_FaxNr;
                DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienHolderContactInfo.BPOAppraisal2.Email = bpo2_Email;
                DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienHolderContactInfo.BPOAppraisal2.Web = bpo2_Web;

                //BANKRUPCY
                DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienHolderContactInfo.BankruptcyDeptField.ContactName = bd_ContactName;
                DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienHolderContactInfo.BankruptcyDeptField.Phone = bd_PhoneNr;
                DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienHolderContactInfo.BankruptcyDeptField.Fax = bd_FaxNr;
                DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienHolderContactInfo.BankruptcyDeptField.Email = bd_Email;
                DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienHolderContactInfo.BankruptcyDeptField.Web = bd_Web;

                //CUSTOMER SERVICE
                DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienHolderContactInfo.CustomerService.ContactName = cs_ContactName;
                DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienHolderContactInfo.CustomerService.Phone = cs_PhoneNr;
                DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienHolderContactInfo.CustomerService.Fax = cs_FaxNr;
                DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienHolderContactInfo.CustomerService.Email = cs_Email;
                DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienHolderContactInfo.CustomerService.Web = cs_Web;

                //FORECLOSURE ATTORNEY
                DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienHolderContactInfo.ForeClosureAttorney.ContactName = fa_ContactName;
                DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienHolderContactInfo.ForeClosureAttorney.Phone = fa_PhoneNr;
                DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienHolderContactInfo.ForeClosureAttorney.Fax = fa_FaxNr;
                DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienHolderContactInfo.ForeClosureAttorney.Email = fa_Email;
                DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienHolderContactInfo.ForeClosureAttorney.Web = fa_Web;

                DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienHolderContactInfo.Strategy.SelectedStrategyDesc = strategyDesc;
                DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienHolderContactInfo.Strategy.MaxToPay = maxToPay;
                DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienHolderContactInfo.Financials.Offers.First.Offer = offer_First;
                DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienHolderContactInfo.Financials.Offers.Second.Offer = offer_Second;
                DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienHolderContactInfo.Financials.Offers.Third.Offer = offer_Third;
                DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienHolderContactInfo.Financials.Offers.Fourth.Offer = offer_Fourth;
                DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienHolderContactInfo.Financials.Offers.Fifth.Offer = offer_Fifth;
                DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienHolderContactInfo.Financials.Offers.First.Counter = counter_First;
                DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienHolderContactInfo.Financials.Offers.Second.Counter = counter_Second;
                DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienHolderContactInfo.Financials.Offers.Third.Counter = counter_Third;
                DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienHolderContactInfo.Financials.Offers.Fourth.Counter = counter_Fourth;
                DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienHolderContactInfo.Financials.Offers.Fifth.Counter = counter_Fifth;
                
                base.AutoSaveDocument();
            }
            catch { }            
        }


        private void OnDropDownLeave(object sender, System.EventArgs e)
        {
            int lienIdx = GetCurrentLienIdx();

            if (_strategy == sender && null != _strategy.SelectedItem)
                DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[lienIdx].LienHolderContactInfo.Strategy.SelectedStrategy = _strategy.SelectedItem.ToString();
                        
            base.AutoSaveDocument();
        }


        private void On_PictureBox_MouseHover(object sender, EventArgs e)
        {
            //Help_StatusProgreess helpForm = new Help_StatusProgreess();
            //helpForm.ShowDialog();
        }

        private void On_PictureBoxGeneralShortSale_MouseHover(object sender, EventArgs e)
        {
            //Help_GeneralShortSale helpForm = new Help_GeneralShortSale();
            //helpForm.ShowDialog();
        }


        private void On_CompanyName_Leave_AdditionalHandler(object sender, EventArgs e)
        {
            string tabText = this.Parent.Text;
            int pos = tabText.IndexOf("(");

            if (-1 == pos)
                tabText += " (" + companyName + ")";
            else
                tabText = tabText.Substring(0, pos - 1) + " (" + companyName + ")";

            this.Parent.Text = tabText;
            base.AutoSaveDocument();
        }


        private void On_RemoveLien_Click(object sender, EventArgs e)
        {
            if (1 >= GetNumberOfLiens())
            {
                MessageBox.Show("1st Lien can't be removed.", StringStore.AppName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            if (DialogResult.Yes != MessageBox.Show("Are you sure you want to remove lien?", StringStore.AppName, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2))
                return;

            int selectedLienIdx = GetCurrentLienIdx();

            DataManager.Instance.RemoveLienFromExistingShortSaleDocument(selectedLienIdx);
            DisplayControlObject.RefreshGUIOnChange();
        }


        private void On_PrintContactInfo_Click(object sender, EventArgs e)
        {
            PrintDialog pd = new PrintDialog();

            PrintDocument tmpprndoc = new PrintDocument();
            tmpprndoc.PrintPage += new PrintPageEventHandler(On_PrintPage);
            tmpprndoc.EndPrint += new PrintEventHandler(On_EndPrint);

            pd.Document = tmpprndoc;
            pd.Document.DefaultPageSettings.Landscape = false;

            PrintPreviewDialogEx tmpprdiag = new PrintPreviewDialogEx();

            tmpprdiag.Document = tmpprndoc;
            tmpprdiag.ShowDialog();
        }


        private void On_EndPrint(object sender, PrintEventArgs e)
        {
            _liensRendered = 0;
        }
        #endregion

        #region PRINTING
        private void On_PrintEventLog_Click(object sender, EventArgs e)
        {
            PrintDialog pd = new PrintDialog();

            PrintDocument tmpprndoc = new PrintDocument();
            tmpprndoc.PrintPage += new PrintPageEventHandler(On_PrintEventLog);
            tmpprndoc.EndPrint += new PrintEventHandler(On_EndPrintEventLog);

            pd.Document = tmpprndoc;
            pd.Document.DefaultPageSettings.Landscape = false;

            PrintPreviewDialogEx tmpprdiag = new PrintPreviewDialogEx();

            tmpprdiag.Document = tmpprndoc;
            tmpprdiag.ShowDialog();
        }

        private void On_PrintBlankEventLog_Click(object sender, EventArgs e)
        {
            PrintDialog pd = new PrintDialog();

            PrintDocument tmpprndoc = new PrintDocument();
            tmpprndoc.PrintPage += new PrintPageEventHandler(On_PrintBlankEventLog);
            tmpprndoc.EndPrint += new PrintEventHandler(On_EndPrintBlankEventLog);

            pd.Document = tmpprndoc;
            pd.Document.DefaultPageSettings.Landscape = false;

            PrintPreviewDialogEx tmpprdiag = new PrintPreviewDialogEx();

            tmpprdiag.Document = tmpprndoc;
            tmpprdiag.ShowDialog();
        }

        private void On_PrintEventLog(object sender, PrintPageEventArgs e)
        {
            Graphics gfx = e.Graphics;
            RenderEventLogHeader(gfx, e.PageSettings.Bounds.Size.Width, e.PageSettings.Bounds.Size.Height);

            
        }

        private void On_EndPrintEventLog(object sender, PrintEventArgs e)
        {

        }

        private void On_PrintBlankEventLog(object sender, PrintPageEventArgs e)
        {
            Graphics gfx = e.Graphics;
            RenderEventLogHeader(gfx, e.PageSettings.Bounds.Size.Width, e.PageSettings.Bounds.Size.Height);

            gfx.DrawString("Date", _arial10, _blackBrush, new Point(15, 302));
            gfx.DrawString("Call Comments", _arial10, _blackBrush, new Point(150, 302));

            for (int i = 0; i < 40; i++)
                gfx.DrawLine(Pens.Black, new Point(10, 300 + (i * 20)), new Point(e.PageSettings.Bounds.Size.Width - 20, 300 + (i * 20)));

            gfx.DrawLine(Pens.Black, new Point(140, 300), new Point(140, 1080));
        }

        private void On_EndPrintBlankEventLog(object sender, PrintEventArgs e)
        {

        }

        void RenderEventLogHeader(Graphics gfx, int PageWidth, int PageHeight)
        {
            int pageWidth = PageWidth;
            int pageHeight = PageHeight;
            int vertOffset = 0;

            vertOffset += 10;


            //HEADER
            int boxHeight = 255;
            gfx.DrawLine(Pens.Black, new Point(10, 10), new Point(pageWidth - 10, 10)); //L-R
            gfx.DrawLine(Pens.Black, new Point(10, 10), new Point(10, boxHeight));//L-B
            gfx.DrawLine(Pens.Black, new Point(pageWidth - 10, 10), new Point(pageWidth - 10, boxHeight)); //R-B
            gfx.DrawLine(Pens.Black, new Point(10, boxHeight), new Point(pageWidth - 10, boxHeight)); //BL - BR

            gfx.DrawString("Event Log for " + GetLienSuffix(GetCurrentLienIdx() + 1), _arial10BU, _blackBrush, new Point(20, 22));

            gfx.DrawString(":Company Name", _arial10, _blackBrush, new Point(140, 50), new StringFormat(StringFormatFlags.DirectionRightToLeft));
            gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[GetCurrentLienIdx()].CompanyName, _arial10U, _blackBrush, new Point(140, 50));

            gfx.DrawString(":Loan Number", _arial10, _blackBrush, new Point(140, 70), new StringFormat(StringFormatFlags.DirectionRightToLeft));
            gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[GetCurrentLienIdx()].AccountNr, _arial10, _blackBrush, new Point(140, 70));

            gfx.DrawString(":Address", _arial10, _blackBrush, new Point(500, 70), new StringFormat(StringFormatFlags.DirectionRightToLeft));
            gfx.DrawString(GetSiteAddress(), _arial10, _blackBrush, new Point(500, 70));

            gfx.DrawString(":Owner/s Name/s", _arial10, _blackBrush, new Point(140, 90), new StringFormat(StringFormatFlags.DirectionRightToLeft));
            gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ContactInfo.FullName, _arial10, _blackBrush, new Point(140, 90));

            gfx.DrawString(":Primary Social", _arial10, _blackBrush, new Point(140, 110), new StringFormat(StringFormatFlags.DirectionRightToLeft));
            gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.PrimarySS, _arial10U, _blackBrush, new Point(140, 110));

            gfx.DrawString(":Primary Phone", _arial10, _blackBrush, new Point(500, 110), new StringFormat(StringFormatFlags.DirectionRightToLeft));
            gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.PrimaryPhone, _arial10, _blackBrush, new Point(500, 110));

            gfx.DrawString(":Secondary Social", _arial10, _blackBrush, new Point(140, 130), new StringFormat(StringFormatFlags.DirectionRightToLeft));
            gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.SecondarySS, _arial10U, _blackBrush, new Point(140, 130));

            gfx.DrawString(":Secondary Phone", _arial10, _blackBrush, new Point(500, 130), new StringFormat(StringFormatFlags.DirectionRightToLeft));
            gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.SecondaryPhone, _arial10, _blackBrush, new Point(500, 130));

            gfx.DrawString(":Negotiator", _arial10, _blackBrush, new Point(140, 170), new StringFormat(StringFormatFlags.DirectionRightToLeft));
            gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[GetCurrentLienIdx()].LienHolderContactInfo.Negotiator.ContactName, _arial10, _blackBrush, new Point(140, 170));

            gfx.DrawString(":Negotiator Fax Number", _arial10, _blackBrush, new Point(500, 170), new StringFormat(StringFormatFlags.DirectionRightToLeft));
            gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[GetCurrentLienIdx()].LienHolderContactInfo.Negotiator.Fax, _arial10, _blackBrush, new Point(500, 170));

            gfx.DrawString(":Negotiator Phone", _arial10, _blackBrush, new Point(140, 190), new StringFormat(StringFormatFlags.DirectionRightToLeft));
            gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[GetCurrentLienIdx()].LienHolderContactInfo.Negotiator.Phone, _arial10, _blackBrush, new Point(140, 190));

            gfx.DrawString(":Negotiator Email", _arial10, _blackBrush, new Point(500, 190), new StringFormat(StringFormatFlags.DirectionRightToLeft));
            gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[GetCurrentLienIdx()].LienHolderContactInfo.Negotiator.Email, _arial10, _blackBrush, new Point(500, 190));

            gfx.DrawString(":Setup Department", _arial10, _blackBrush, new Point(140, 210), new StringFormat(StringFormatFlags.DirectionRightToLeft));
            gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[GetCurrentLienIdx()].LienHolderContactInfo.SetupDept.ContactName, _arial10, _blackBrush, new Point(140, 210));

            gfx.DrawString(":Setup Fax", _arial10, _blackBrush, new Point(500, 210), new StringFormat(StringFormatFlags.DirectionRightToLeft));
            gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[GetCurrentLienIdx()].LienHolderContactInfo.SetupDept.Fax, _arial10, _blackBrush, new Point(500, 210));

            gfx.DrawString(":Setup Phone", _arial10, _blackBrush, new Point(140, 230), new StringFormat(StringFormatFlags.DirectionRightToLeft));
            gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[GetCurrentLienIdx()].LienHolderContactInfo.SetupDept.Phone, _arial10, _blackBrush, new Point(140, 230));

            gfx.DrawString(":Setup Email", _arial10, _blackBrush, new Point(500, 230), new StringFormat(StringFormatFlags.DirectionRightToLeft));
            gfx.DrawString(DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[GetCurrentLienIdx()].LienHolderContactInfo.SetupDept.Email, _arial10, _blackBrush, new Point(500, 230));
        }

        #endregion
        
        #region HANDLE TEXT BOX STATES
        private void OnCheckBoxStateChanged(object sender, System.EventArgs e)
        {
            //UpdateTextBoxState();
            int lienIdx = GetCurrentLienIdx();
            
            base.AutoSaveDocument();
        }

       
        #endregion
                
        #region LOG FILE (BROWSE FOR, OPEN)
        void On_OpenLogFile_Click(object sender, EventArgs e)
        {
            if (1 == _cs.SpecialSSX)
            {
                string cFile = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[(int)this.Tag].LogFilePath;

                if (null != cFile && string.Empty != cFile)
                {

                    FileInfo f = new FileInfo(cFile);

                    if (".xls" == f.Extension || ".xlsx" == f.Extension)
                    {
                        try { Process.Start("excel.exe", "\"" + f.FullName + "\""); }
                        catch { MessageBox.Show("Could not start Excel!", StringStore.AppName, MessageBoxButtons.OK, MessageBoxIcon.Stop); }
                    }
                    else if (".doc" == f.Extension || ".docx" == f.Extension)
                    {
                        try { Process.Start("winword.exe", "\"" + f.FullName + "\""); }
                        catch { MessageBox.Show("Could not start Word!", StringStore.AppName, MessageBoxButtons.OK, MessageBoxIcon.Stop); }
                    }
                    else
                    {
                        try { Process.Start("notepad.exe", "\"" + f.FullName + "\""); }
                        catch { MessageBox.Show("Could not start Notepad!", StringStore.AppName, MessageBoxButtons.OK, MessageBoxIcon.Stop); }
                    }
                }
                else
                    MessageBox.Show("You must first assign the File before opening it.", StringStore.AppName, MessageBoxButtons.OK, MessageBoxIcon.Information);


                return;
            }

            string currFile = DataManager.Instance.Log;
            if (null == currFile || string.Empty == currFile)
            {               
                DefaultInfoValues div = null;
                string error = string.Empty;
                DataManager.Instance.LoadDefaultInfoValues(out div, out error);

                if (null == div)
                {
                    MessageBox.Show("Before you select your Log File for your shortcut button, you must define location of your Working Folders." + System.Environment.NewLine + System.Environment.NewLine + "Click OK and set your folders by going to Set Your Working Folder command under File menu (it takes only a few clicks to complete this setup).", StringStore.AppName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                else
                {                    
                    CustomPopUpForm cpf = new CustomPopUpForm();
                    cpf.ShowDialog();
                    OpenFileDialog ofd = new OpenFileDialog();

                    ofd.InitialDirectory = div.WorkingFileFolder + @"\" + Path.GetFileNameWithoutExtension(DataManager.Instance.DocumentName);
                    ofd.Filter = "Excel Files (*.xls; *.xlsx)|*.xls;*.xlsx|Word Files (*.doc; *.docx)|*.doc;*.docx|Text Files (*.txt)|*.txt";

                    if (DialogResult.OK == ofd.ShowDialog())
                    {
                        DataManager.Instance.Log = currFile = ofd.FileName;
                    }
                    else
                        return;
                }
            }            

            FileInfo file = new FileInfo(currFile);
            string nameonly = file.Name;

            if (".xls" == file.Extension || ".xlsx" == file.Extension)
            {
                try { Process.Start("excel.exe", "\"" + file.FullName + "\""); }
                catch { MessageBox.Show("Could not start Excel!", StringStore.AppName, MessageBoxButtons.OK, MessageBoxIcon.Stop); }
            }
            else if (".doc" == file.Extension || ".docx" == file.Extension)
            {
                try { Process.Start("winword.exe", "\"" + file.FullName + "\""); }
                catch { MessageBox.Show("Could not start Word!", StringStore.AppName, MessageBoxButtons.OK, MessageBoxIcon.Stop); }
            }
            else
            {
                try { Process.Start("notepad.exe", "\"" + file.FullName + "\""); }
                catch { MessageBox.Show("Could not start Notepad!", StringStore.AppName, MessageBoxButtons.OK, MessageBoxIcon.Stop); }
            }                            
        }

        void On_browseLogFile_Click(object sender, EventArgs e)
        {
            DefaultInfoValues div = null;
            string error = string.Empty;

            DataManager.Instance.LoadDefaultInfoValues(out div, out error);

            if ((null != _cs) && (1 == _cs.SpecialSSX))  //special hander for Kit
            {
                //NEW
                string tempPath = string.Empty;
                try
                {
                    tempPath = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[(int)this.Tag].WorkingFolderPath;
                }
                catch
                {
                    tempPath = string.Empty;
                }

                if (null == tempPath || string.Empty == tempPath)
                {
                    MessageBox.Show("Assign Working Folder First", StringStore.AppName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                if (null != tempPath && string.Empty != tempPath)
                    if (Directory.Exists(tempPath))
                        Process.Start(@"explorer.exe", tempPath);

                return;

                /* OLD
                string tempPath = string.Empty;
                try
                {
                    tempPath = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[(int)this.Tag].WorkingFolderPath;
                }
                catch 
                { 
                    tempPath = string.Empty; 
                }

                if (string.Empty == tempPath)
                {
                    MessageBox.Show("Assign Working Folder First", StringStore.AppName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                string baseFolder = tempPath;// +@"\" + Path.GetFileNameWithoutExtension(DataManager.Instance.HomeOwnerFullName);

                if (!Directory.Exists(baseFolder + @"\" + Path.GetFileNameWithoutExtension(DataManager.Instance.HomeOwnerFullName)))
                {
                    baseFolder = CreateSubFolderStructure(baseFolder, div);
                    DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[(int)this.Tag].LogFilePath = baseFolder;
                    base.AutoSaveDocument();
                }


                if (null != baseFolder && string.Empty != baseFolder)
                    if (Directory.Exists(baseFolder))
                        Process.Start(@"explorer.exe", baseFolder);

                return;
                */
            }           
                        
            
            if (null == div)            
                div = SetWorkingFileFolder();
            
            if (null != div)
            {

                string baseFolder = div.WorkingFileFolder + @"\" + Path.GetFileNameWithoutExtension(DataManager.Instance.HomeOwnerFullName);
                if (!Directory.Exists(baseFolder))
                {
                    baseFolder = CreateSubFolderStructure(div);
                    DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[(int)this.Tag].LogFilePath = baseFolder;
                    base.AutoSaveDocument();
                }
                

                if (null != baseFolder && string.Empty != baseFolder)
                    if (Directory.Exists(baseFolder))
                        Process.Start(@"explorer.exe", baseFolder);
            }                     
        }

        DefaultInfoValues SetWorkingFileFolder()
        {
            string error = string.Empty;

            DefaultInfoValues div = new DefaultInfoValues();                            

            FolderBrowserDialog fbd = new FolderBrowserDialog();
            fbd.ShowNewFolderButton = true;

            fbd.Description += "STEP 1: Select location or a folder in which you will keep all the documents for each property.";
            fbd.Description += "Then click “Make New Folder” button to create a new folder and call it “My Short Sale Deals”.";

            if (string.Empty != div.WorkingFileFolder)
                if (Directory.Exists(div.WorkingFileFolder))
                {
                    fbd.SelectedPath = div.WorkingFileFolder;
                }

            if (DialogResult.OK == fbd.ShowDialog())
            {
                div.WorkingFileFolder = fbd.SelectedPath;
                DataManager.Instance.UpdateDefaultInfoValues(div, out error);                

                SubfolderStructure subStruct = new SubfolderStructure();

                if (DialogResult.OK == subStruct.ShowDialog())                
                    div = subStruct.div;                                
            }

            return div;
        }
        #endregion

        #region CREATE SUB FOLDER STRUCTURE
        string CreateSubFolderStructure(DefaultInfoValues div)
        {            
            if (!Directory.Exists(div.WorkingFileFolder))
            {
                // set one
                MessageBox.Show("Set working file location first." + Environment.NewLine + Environment.NewLine + "Go to File - Set Working File Location menu!", StringStore.AppName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return string.Empty;
            }
            else
            {
                foreach (string subFolder in StringStore.PredefinedFolders)
                {
                    Directory.CreateDirectory(div.WorkingFileFolder + @"\" + Path.GetFileNameWithoutExtension(DataManager.Instance.HomeOwnerFullName) + @"\" + subFolder);
                }

                if (null != div.UserSubFolderStructField && 0 != div.UserSubFolderStructField.Length)
                {
                    foreach (string subFolder in div.UserSubFolderStructField)
                    {
                        Directory.CreateDirectory(div.WorkingFileFolder + @"\" + Path.GetFileNameWithoutExtension(DataManager.Instance.HomeOwnerFullName) + @"\" + subFolder);
                    }
                }

                File.Copy(Application.StartupPath + @"\AllSheets.xls", div.WorkingFileFolder + @"\" + Path.GetFileNameWithoutExtension(DataManager.Instance.HomeOwnerFullName) + @"\" + "PhoneLog_" + Path.GetFileNameWithoutExtension(DataManager.Instance.DocumentName) + "_.xls");

                return div.WorkingFileFolder + @"\" + Path.GetFileNameWithoutExtension(DataManager.Instance.HomeOwnerFullName);
            }
        }

        string CreateSubFolderStructure(string baseFolder, DefaultInfoValues div)
        {                        
            foreach (string subFolder in StringStore.PredefinedFolders)
            {
                Directory.CreateDirectory(baseFolder + @"\" + Path.GetFileNameWithoutExtension(DataManager.Instance.HomeOwnerFullName) + @"\" + subFolder);
            }

            if (null != div.UserSubFolderStructField && 0 != div.UserSubFolderStructField.Length)
            {
                foreach (string subFolder in div.UserSubFolderStructField)
                {
                    Directory.CreateDirectory(div.WorkingFileFolder + @"\" + Path.GetFileNameWithoutExtension(DataManager.Instance.HomeOwnerFullName) + @"\" + subFolder);
                }
            }

            File.Copy(Application.StartupPath + @"\AllSheets.xls", baseFolder + @"\" + Path.GetFileNameWithoutExtension(DataManager.Instance.HomeOwnerFullName) + @"\" + "PhoneLog_" + Path.GetFileNameWithoutExtension(DataManager.Instance.DocumentName) + "_.xls");

            return baseFolder +@"\" + Path.GetFileNameWithoutExtension(DataManager.Instance.HomeOwnerFullName);
            
        }
        #endregion

        void ApplyCustomSSXSettings(bool customSSXSettings)
        {
            _custom_assignLogFile.Visible = customSSXSettings;
            _custom_assignWorkingFolder.Visible = customSSXSettings;

            //_browseLogFile.Visible = !customSSXSettings;
        }

        void On_Custom_assignLogFile_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();

            try { ofd.InitialDirectory = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[(int)this.Tag].WorkingFolderPath; }
            catch { }

            ofd.Filter = "Excel Files (*.xls; *.xlsx)|*.xls;*.xlsx|Word Files (*.doc; *.docx)|*.doc;*.docx|Text Files (*.txt)|*.txt";

            if (DialogResult.OK == ofd.ShowDialog())
            {
                DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[(int)this.Tag].LogFilePath = ofd.FileName;
                base.AutoSaveDocument();
            }
        }

        void On_Custom_assignWorkingFolder_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            fbd.ShowNewFolderButton = true;

            fbd.Description += "Assign Working Folder";

            string tempPath = string.Empty;
            try
            {
                tempPath = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[(int)this.Tag].WorkingFolderPath;
            }
            catch { tempPath = string.Empty; }

            if (string.Empty != tempPath && Directory.Exists(tempPath))
                fbd.SelectedPath = tempPath;
            
            if (DialogResult.OK == fbd.ShowDialog())
            {
                DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[(int)this.Tag].WorkingFolderPath = fbd.SelectedPath;
                base.AutoSaveDocument();                                    
            }            
        }

    }
}
