using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;

namespace DealMaker
{
	/// <summary>
	/// Summary description for CustomersOverview.
	/// </summary>
	public class CustomersOverview : System.Windows.Forms.UserControl
	{
		private System.Windows.Forms.ListView listView1;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public CustomersOverview()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

			// TODO: Add any initialization after the InitializeComponent call

		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.listView1 = new System.Windows.Forms.ListView();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// listView1
			// 
			this.listView1.Location = new System.Drawing.Point(48, 72);
			this.listView1.Name = "listView1";
			this.listView1.Size = new System.Drawing.Size(944, 240);
			this.listView1.TabIndex = 0;
			// 
			// label1
			// 
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label1.Location = new System.Drawing.Point(24, 32);
			this.label1.Name = "label1";
			this.label1.TabIndex = 1;
			this.label1.Text = "Contacts";
			// 
			// label2
			// 
			this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label2.Location = new System.Drawing.Point(24, 352);
			this.label2.Name = "label2";
			this.label2.TabIndex = 2;
			this.label2.Text = "Smart Filters";
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(48, 392);
			this.label3.Name = "label3";
			this.label3.TabIndex = 3;
			this.label3.Text = "Last Name:";
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(48, 424);
			this.label4.Name = "label4";
			this.label4.TabIndex = 4;
			this.label4.Text = "Name";
			// 
			// CustomersOverview
			// 
			this.Controls.Add(this.label4);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.listView1);
			this.Name = "CustomersOverview";
			this.Size = new System.Drawing.Size(1128, 680);
			this.ResumeLayout(false);

		}
		#endregion
	}
}
