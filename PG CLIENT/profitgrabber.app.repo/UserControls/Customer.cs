using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;

namespace DealMaker
{
	/// <summary>
	/// Summary description for Customers.
	/// </summary>
	public class Customer : System.Windows.Forms.UserControl
	{
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.TextBox tbPersonId;
		private System.Windows.Forms.TextBox tbLastName;
		private System.Windows.Forms.TextBox tbFirstName;
		private System.Windows.Forms.TextBox tbIdNumber;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.ListView lvContacts;
		private System.Windows.Forms.ColumnHeader country;
		private System.Windows.Forms.ColumnHeader state;
		private System.Windows.Forms.ColumnHeader city;
		private System.Windows.Forms.ColumnHeader part;
		private System.Windows.Forms.ColumnHeader street;
		private System.Windows.Forms.ColumnHeader streetNr;
		private System.Windows.Forms.ColumnHeader flatId;
		private System.Windows.Forms.ColumnHeader importance;
		private System.Windows.Forms.ColumnHeader valid;
		private System.Windows.Forms.Button bAddContact;
		private System.Windows.Forms.Button bUpdateContact;
		private System.Windows.Forms.Button bDeleteContact;
		private System.Windows.Forms.TextBox tbMessage;
		private System.Windows.Forms.TextBox tbDateOfBirth;
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public Customer()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

			// TODO: Add any initialization after the InitializeComponent call

		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.tbPersonId = new System.Windows.Forms.TextBox();
			this.tbLastName = new System.Windows.Forms.TextBox();
			this.tbFirstName = new System.Windows.Forms.TextBox();
			this.tbIdNumber = new System.Windows.Forms.TextBox();
			this.label7 = new System.Windows.Forms.Label();
			this.lvContacts = new System.Windows.Forms.ListView();
			this.country = new System.Windows.Forms.ColumnHeader();
			this.state = new System.Windows.Forms.ColumnHeader();
			this.city = new System.Windows.Forms.ColumnHeader();
			this.part = new System.Windows.Forms.ColumnHeader();
			this.street = new System.Windows.Forms.ColumnHeader();
			this.streetNr = new System.Windows.Forms.ColumnHeader();
			this.flatId = new System.Windows.Forms.ColumnHeader();
			this.importance = new System.Windows.Forms.ColumnHeader();
			this.valid = new System.Windows.Forms.ColumnHeader();
			this.bAddContact = new System.Windows.Forms.Button();
			this.bUpdateContact = new System.Windows.Forms.Button();
			this.bDeleteContact = new System.Windows.Forms.Button();
			this.tbMessage = new System.Windows.Forms.TextBox();
			this.tbDateOfBirth = new System.Windows.Forms.TextBox();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label1.Location = new System.Drawing.Point(8, 8);
			this.label1.Name = "label1";
			this.label1.TabIndex = 0;
			this.label1.Text = "Person";
			// 
			// label2
			// 
			this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label2.Location = new System.Drawing.Point(24, 40);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(96, 23);
			this.label2.TabIndex = 1;
			this.label2.Text = "Person ID";
			this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label3
			// 
			this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label3.Location = new System.Drawing.Point(24, 72);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(96, 23);
			this.label3.TabIndex = 2;
			this.label3.Text = "Last Name";
			this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label4
			// 
			this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label4.Location = new System.Drawing.Point(24, 104);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(96, 23);
			this.label4.TabIndex = 3;
			this.label4.Text = "First Name";
			this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label5
			// 
			this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label5.Location = new System.Drawing.Point(24, 136);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(96, 23);
			this.label5.TabIndex = 4;
			this.label5.Text = "ID Number";
			this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label6
			// 
			this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label6.Location = new System.Drawing.Point(24, 168);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(96, 23);
			this.label6.TabIndex = 5;
			this.label6.Text = "Date of Birth";
			this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// tbPersonId
			// 
			this.tbPersonId.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.tbPersonId.Location = new System.Drawing.Point(128, 40);
			this.tbPersonId.Name = "tbPersonId";
			this.tbPersonId.ReadOnly = true;
			this.tbPersonId.Size = new System.Drawing.Size(176, 22);
			this.tbPersonId.TabIndex = 6;
			this.tbPersonId.TabStop = false;
			this.tbPersonId.Text = "";
			// 
			// tbLastName
			// 
			this.tbLastName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.tbLastName.Location = new System.Drawing.Point(128, 72);
			this.tbLastName.Name = "tbLastName";
			this.tbLastName.Size = new System.Drawing.Size(232, 22);
			this.tbLastName.TabIndex = 7;
			this.tbLastName.Text = "";
			// 
			// tbFirstName
			// 
			this.tbFirstName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.tbFirstName.Location = new System.Drawing.Point(128, 104);
			this.tbFirstName.Name = "tbFirstName";
			this.tbFirstName.Size = new System.Drawing.Size(232, 22);
			this.tbFirstName.TabIndex = 8;
			this.tbFirstName.Text = "";
			// 
			// tbIdNumber
			// 
			this.tbIdNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.tbIdNumber.Location = new System.Drawing.Point(128, 136);
			this.tbIdNumber.Name = "tbIdNumber";
			this.tbIdNumber.Size = new System.Drawing.Size(176, 22);
			this.tbIdNumber.TabIndex = 9;
			this.tbIdNumber.Text = "";
			// 
			// label7
			// 
			this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label7.Location = new System.Drawing.Point(8, 208);
			this.label7.Name = "label7";
			this.label7.TabIndex = 11;
			this.label7.Text = "Contacts";
			// 
			// lvContacts
			// 
			this.lvContacts.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
																						 this.country,
																						 this.state,
																						 this.city,
																						 this.part,
																						 this.street,
																						 this.streetNr,
																						 this.flatId,
																						 this.importance,
																						 this.valid});
			this.lvContacts.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lvContacts.FullRowSelect = true;
			this.lvContacts.GridLines = true;
			this.lvContacts.HoverSelection = true;
			this.lvContacts.Location = new System.Drawing.Point(24, 304);
			this.lvContacts.Name = "lvContacts";
			this.lvContacts.Size = new System.Drawing.Size(720, 216);
			this.lvContacts.TabIndex = 12;
			this.lvContacts.TabStop = false;
			this.lvContacts.View = System.Windows.Forms.View.Details;
			// 
			// country
			// 
			this.country.Text = "Country";
			this.country.Width = 62;
			// 
			// state
			// 
			this.state.Text = "State";
			this.state.Width = 52;
			// 
			// city
			// 
			this.city.Text = "City";
			this.city.Width = 87;
			// 
			// part
			// 
			this.part.Text = "Part";
			this.part.Width = 85;
			// 
			// street
			// 
			this.street.Text = "Street";
			this.street.Width = 144;
			// 
			// streetNr
			// 
			this.streetNr.Text = "Street Nr.";
			this.streetNr.Width = 73;
			// 
			// flatId
			// 
			this.flatId.Text = "Flat ID";
			this.flatId.Width = 54;
			// 
			// importance
			// 
			this.importance.Text = "Importance";
			this.importance.Width = 97;
			// 
			// valid
			// 
			this.valid.Text = "Valid";
			this.valid.Width = 62;
			// 
			// bAddContact
			// 
			this.bAddContact.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.bAddContact.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.bAddContact.Location = new System.Drawing.Point(24, 240);
			this.bAddContact.Name = "bAddContact";
			this.bAddContact.Size = new System.Drawing.Size(208, 56);
			this.bAddContact.TabIndex = 13;
			this.bAddContact.Text = "Add Contact";
			// 
			// bUpdateContact
			// 
			this.bUpdateContact.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.bUpdateContact.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.bUpdateContact.Location = new System.Drawing.Point(240, 240);
			this.bUpdateContact.Name = "bUpdateContact";
			this.bUpdateContact.Size = new System.Drawing.Size(208, 56);
			this.bUpdateContact.TabIndex = 14;
			this.bUpdateContact.Text = "Update Contact";
			// 
			// bDeleteContact
			// 
			this.bDeleteContact.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.bDeleteContact.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.bDeleteContact.Location = new System.Drawing.Point(456, 240);
			this.bDeleteContact.Name = "bDeleteContact";
			this.bDeleteContact.Size = new System.Drawing.Size(208, 56);
			this.bDeleteContact.TabIndex = 15;
			this.bDeleteContact.Text = "Delete Contact";
			// 
			// tbMessage
			// 
			this.tbMessage.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.tbMessage.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.tbMessage.Location = new System.Drawing.Point(120, 216);
			this.tbMessage.Name = "tbMessage";
			this.tbMessage.ReadOnly = true;
			this.tbMessage.Size = new System.Drawing.Size(304, 15);
			this.tbMessage.TabIndex = 16;
			this.tbMessage.TabStop = false;
			this.tbMessage.Text = "";
			// 
			// tbDateOfBirth
			// 
			this.tbDateOfBirth.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.tbDateOfBirth.Location = new System.Drawing.Point(128, 168);
			this.tbDateOfBirth.Name = "tbDateOfBirth";
			this.tbDateOfBirth.Size = new System.Drawing.Size(176, 22);
			this.tbDateOfBirth.TabIndex = 10;
			this.tbDateOfBirth.Text = "";
			// 
			// Customer
			// 
			this.Controls.Add(this.tbDateOfBirth);
			this.Controls.Add(this.tbMessage);
			this.Controls.Add(this.bDeleteContact);
			this.Controls.Add(this.bUpdateContact);
			this.Controls.Add(this.bAddContact);
			this.Controls.Add(this.lvContacts);
			this.Controls.Add(this.label7);
			this.Controls.Add(this.tbIdNumber);
			this.Controls.Add(this.tbFirstName);
			this.Controls.Add(this.tbLastName);
			this.Controls.Add(this.tbPersonId);
			this.Controls.Add(this.label6);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Name = "Customer";
			this.Size = new System.Drawing.Size(760, 536);
			this.Load += new System.EventHandler(this.Customer_Load);
			this.ResumeLayout(false);

		}
		#endregion

		private void Customer_Load(object sender, System.EventArgs e)
		{
			Size currentControlSize = this.Size;			
			Size lvSize =  this.lvContacts.Size;
			lvSize.Width = currentControlSize.Width - 100;
			lvSize.Height += currentControlSize.Height - 530;
			this.lvContacts.Size = lvSize;
			int columnNumber = this.lvContacts.Columns.Count;
			double dColumnWidht = this.lvContacts.Width / columnNumber;
			int columnWidth = (int)dColumnWidht;
			foreach (ColumnHeader ch in this.lvContacts.Columns)
			{
				ch.Width = columnWidth;
			}
			
		}
	}
}
