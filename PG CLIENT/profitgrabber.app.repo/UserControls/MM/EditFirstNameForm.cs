using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace DealMaker
{
	/// <summary>
	/// Summary description for EditFirstNameForm.
	/// </summary>
	public class EditFirstNameForm : System.Windows.Forms.Form
	{		
		bool _validSelection = false;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox _tbFirstName;
		private System.Windows.Forms.Button _bOK;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public EditFirstNameForm()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

						
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this._tbFirstName = new System.Windows.Forms.TextBox();
			this._bOK = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label1.Location = new System.Drawing.Point(16, 16);
			this.label1.Name = "label1";
			this.label1.TabIndex = 0;
			this.label1.Text = "First Name";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// _tbFirstName
			// 
			this._tbFirstName.Location = new System.Drawing.Point(16, 40);
			this._tbFirstName.Name = "_tbFirstName";
			this._tbFirstName.Size = new System.Drawing.Size(192, 20);
			this._tbFirstName.TabIndex = 1;
			this._tbFirstName.Text = "";
			// 
			// _bOK
			// 
			this._bOK.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this._bOK.Location = new System.Drawing.Point(136, 72);
			this._bOK.Name = "_bOK";
			this._bOK.TabIndex = 2;
			this._bOK.Text = "OK";
			this._bOK.Click += new System.EventHandler(this._bOK_Click);
			// 
			// EditFirstNameForm
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(210)), ((System.Byte)(216)), ((System.Byte)(240)));
			this.ClientSize = new System.Drawing.Size(226, 110);
			this.Controls.Add(this._bOK);
			this.Controls.Add(this._tbFirstName);
			this.Controls.Add(this.label1);
			this.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
			this.Name = "EditFirstNameForm";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Edit First Name";
			this.ResumeLayout(false);

		}
		#endregion

		private void _bOK_Click(object sender, System.EventArgs e)
		{
			_validSelection = true;
			this.Close();
		}

		public bool ValidSelection
		{
			get { return _validSelection; }
		}

		public string FirstName
		{
			get { return _tbFirstName.Text; }
			set { _tbFirstName.Text = value; }
		}

		
	}
}
