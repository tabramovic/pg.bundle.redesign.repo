using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace DealMaker
{
	/// <summary>
	/// Summary description for MM_Form.
	/// </summary>
	public class MM_Form : System.Windows.Forms.Form
	{
		int _currIdx = 0;		
		bool _matchingFinished = false;
		bool _closingAllowed = false;
		private System.Windows.Forms.ProgressBar _pb_progress;
		private System.Windows.Forms.Button _b_Cancel;
		private System.Windows.Forms.Label _l_Status;
		private System.Windows.Forms.Label _l_SuccessfulyProcessed;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public MM_Form()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			
			_b_Cancel.Click += new EventHandler(CancelMatchingProcess);
                        
            ServiceWorker.GetInstance.FireServiceFinishedEvent += new DealMaker.ServiceWorker.ServiceFinishedHandler(MatchingFinished);
            ServiceWorker.GetInstance.FireQueryPositivelySolvedEvent += new DealMaker.ServiceWorker.QueryPositivelySolvedHandler(PositiveMatching);
            ServiceWorker.GetInstance.FireQueryNegativelySolvedEvent += new DealMaker.ServiceWorker.QueryNegativelySolvedHandler(NegativeMatching);
            ServiceWorker.GetInstance.FireServiceErrorEvent += new DealMaker.ServiceWorker.ServiceErrorHandler(ErrorInMatching);                        
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this._pb_progress = new System.Windows.Forms.ProgressBar();
			this._b_Cancel = new System.Windows.Forms.Button();
			this._l_SuccessfulyProcessed = new System.Windows.Forms.Label();
			this._l_Status = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// _pb_progress
			// 
			this._pb_progress.Dock = System.Windows.Forms.DockStyle.Bottom;
			this._pb_progress.Location = new System.Drawing.Point(0, 65);
			this._pb_progress.Name = "_pb_progress";
			this._pb_progress.Size = new System.Drawing.Size(292, 23);
			this._pb_progress.TabIndex = 0;
			// 
			// _b_Cancel
			// 
			this._b_Cancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this._b_Cancel.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this._b_Cancel.Location = new System.Drawing.Point(208, 40);
			this._b_Cancel.Name = "_b_Cancel";
			this._b_Cancel.TabIndex = 1;
			this._b_Cancel.Text = "Cancel";
			// 
			// _l_SuccessfulyProcessed
			// 
			this._l_SuccessfulyProcessed.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this._l_SuccessfulyProcessed.Location = new System.Drawing.Point(0, 40);
			this._l_SuccessfulyProcessed.Name = "_l_SuccessfulyProcessed";
			this._l_SuccessfulyProcessed.Size = new System.Drawing.Size(128, 23);
			this._l_SuccessfulyProcessed.TabIndex = 2;
			this._l_SuccessfulyProcessed.Text = "Processed:";
			this._l_SuccessfulyProcessed.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
			this._l_SuccessfulyProcessed.Visible = false;
			// 
			// _l_Status
			// 
			this._l_Status.Dock = System.Windows.Forms.DockStyle.Top;
			this._l_Status.Location = new System.Drawing.Point(0, 0);
			this._l_Status.Name = "_l_Status";
			this._l_Status.Size = new System.Drawing.Size(292, 23);
			this._l_Status.TabIndex = 3;
			this._l_Status.Text = "Waiting for Web services...";
			this._l_Status.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
			// 
			// MM_Form
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(210)), ((System.Byte)(216)), ((System.Byte)(240)));
			this.ClientSize = new System.Drawing.Size(292, 88);
			this.Controls.Add(this._l_Status);
			this.Controls.Add(this._l_SuccessfulyProcessed);
			this.Controls.Add(this._b_Cancel);
			this.Controls.Add(this._pb_progress);
			this.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
			this.Name = "MM_Form";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Matching Progress";
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// We will ignore user's try to close the form
		///		(We have to wait for current pi to be matched completely)
		/// </summary>
		/// <param name="e"></param>
		protected override void OnClosing(CancelEventArgs e)
		{
			if (!_closingAllowed)
				e.Cancel = true;

            ServiceWorker.GetInstance.FireServiceFinishedEvent -= new DealMaker.ServiceWorker.ServiceFinishedHandler(MatchingFinished);
            ServiceWorker.GetInstance.FireQueryPositivelySolvedEvent -= new DealMaker.ServiceWorker.QueryPositivelySolvedHandler(PositiveMatching);
            ServiceWorker.GetInstance.FireQueryNegativelySolvedEvent -= new DealMaker.ServiceWorker.QueryNegativelySolvedHandler(NegativeMatching);
            ServiceWorker.GetInstance.FireServiceErrorEvent -= new DealMaker.ServiceWorker.ServiceErrorHandler(ErrorInMatching);

            base.OnClosing (e);
		}

		private void CancelMatchingProcess(object sender, EventArgs e)
		{
			if (!_matchingFinished)
				ServiceWorker.GetInstance.AbortWork();
			else				
				this.Close();
		}

		private void MatchingFinished()
		{
			_matchingFinished = true;
			_closingAllowed = true;
			//_b_Cancel.Text = "Close";
            SetButtonTextAsync("Close");
		}

        delegate void SetButtonTextDelegate(string val);
        void SetButtonTextAsync(string val)
        {
            if (InvokeRequired)
                BeginInvoke(new SetButtonTextDelegate(SetButtonTextAsync), new object[] { val });
            else
                _b_Cancel.Text = val;

        }

		private void PositiveMatching(PropertyItem pi, bool previousValueContained, bool previousMailingAddressContained, bool previousPhoneNumberContained)
		{
			IncreaseCounter();
			_currIdx++;						
			UpdateStatusText("Processed: " + _currIdx.ToString() + "/" + _pb_progress.Maximum.ToString());
		}

		private void NegativeMatching(PropertyItem pi, String message)
		{			
			IncreaseCounter();
			_currIdx++;
			UpdateStatusText("Processed: " + _currIdx.ToString() + "/" + _pb_progress.Maximum.ToString());
		}

		private void ErrorInMatching(string ErrorMessage, Exception innerException)
		{
			IncreaseCounter();
			_currIdx++;
			UpdateStatusText("Processed: " + _currIdx.ToString() + "/" + _pb_progress.Maximum.ToString());
		}

		private void DecrementProgressBarSizeValue()
		{
			try
			{
				if (_pb_progress.Value < _pb_progress.Maximum)
					_pb_progress.Maximum--;
			}
			catch {}
		}

        delegate void IncreaseCounterDelegate();
        private void IncreaseCounterAsync()
        {
            if (InvokeRequired)
                BeginInvoke(new IncreaseCounterDelegate(IncreaseCounterAsync), null);
            else
                _pb_progress.Value++;
        }
        private void IncreaseCounter()
		{
			try
			{
                if (_pb_progress.Value < _pb_progress.Maximum)
                {
                    IncreaseCounterAsync();
                    //_pb_progress.Value++;
                }
			}
			catch {}
		}		

		public int GroupMembersCount
		{
			get { return _pb_progress.Maximum; }
			set { _pb_progress.Maximum = value; }
		}

		delegate void UpdateStatusTextDelegate(string text);
		void UpdateStatusText(string text)
		{
			if (this.InvokeRequired)			
				this.BeginInvoke(new UpdateStatusTextDelegate(UpdateStatusText), new object[] {text});			
			else
			{
				_l_Status.Text = text;

				if (!_l_SuccessfulyProcessed.Visible)
					_l_SuccessfulyProcessed.Visible = true;
			}
		}
	}
}
