//#define DUMP_XML_OBJECT
using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.IO;
using DTD.Response;
using DTD.Request;
using System.Xml;
using System.Xml.Serialization;
using System.Text;

namespace DealMaker
{
	/// <summary>
	/// Summary description for ServiceWorker.
	/// </summary>
	public sealed class ServiceWorker
	{
		bool _overrideExitingNames = true;
		bool _overrideExistingMailingAddress = true;
		bool _overrideExistingPhoneNumber = true;
		bool _doNotProcess_NM_Records = true;
		bool _useAPN = false;

		#region decarations				
		public delegate void ServiceFinishedHandler();
		public event ServiceFinishedHandler FireServiceFinishedEvent;	

		public delegate void QueryPositivelySolvedHandler(PropertyItem pi, bool previousValueContained, bool previousMailingAddressContained, bool previousPhoneNumberContained);
		public event QueryPositivelySolvedHandler FireQueryPositivelySolvedEvent;	

		public delegate void QueryNegativelySolvedHandler(PropertyItem pi, String message);
		public event QueryNegativelySolvedHandler FireQueryNegativelySolvedEvent;	

		public delegate void ServiceErrorHandler(string ErrorMessage, Exception innerException);
		public event ServiceErrorHandler FireServiceErrorEvent;

		private ArrayList propertyItems = new ArrayList();
		DTD.Response.RESPONSE_GROUP response = null;
		private bool m_ManualStepping;
		private bool abortionAsked;
		private int totalErrCount=0;
		#endregion declarations				 

		static readonly ServiceWorker instance=new ServiceWorker();
		
		static ServiceWorker()
		{
            
        }

		ServiceWorker()
		{
            SubscribedEHs = false;
        }

		public static ServiceWorker GetInstance
		{
			get
			{
				return instance;
			}
		}

        public bool SubscribedEHs { get; set; }
        

		public bool OverrideExistingNames
		{
			get { return _overrideExitingNames; }
			set { _overrideExitingNames = value; }
		}

		public bool OverrideExistingMailingAddress
		{
			get { return _overrideExistingMailingAddress; }
			set { _overrideExistingMailingAddress = value; }
		}

		public bool OverrideExistingPhoneNumber
		{
			get { return _overrideExistingPhoneNumber; }
			set { _overrideExistingPhoneNumber = value; }
		}

		public bool DoNotProcess_NM_Records
		{
			get {return _doNotProcess_NM_Records; }
			set {_doNotProcess_NM_Records = value; }
		}

		public bool UseAPN
		{
			get { return _useAPN; }
			set { _useAPN = value; }
		}

		public void startWorking()
		{			
			totalErrCount = 0;
			abortionAsked = false;
			foreach(PropertyItem pi in propertyItems)
			{
				if (abortionAsked)
				{
					break;
				}

				PropertyItem pix = pi;
				getSubject(SearchType.AddressSearch, ref pix);
			}
			FireServiceFinishedEvent();
		}
		public void AbortWork()
		{
			abortionAsked = true;
		}
		public void AddPropertyItem(PropertyItem pi)
		{
			propertyItems.Add(pi);
		}
		public void ClearPropertyItems()
		{
			propertyItems.Clear();
		}
		public bool ManualStepping
		{
			get{return m_ManualStepping;}
			set{m_ManualStepping = value;}
		}
		public int TotalErrorCount
		{
			get {return totalErrCount;}
		}

		public int NumberOfItemsInList
		{
			get 
			{ 
				if (null != propertyItems)
					return propertyItems.Count; 
				else
					return 0;
			}
		}

		
		#region . Build Property Criteria .
		private DTD.Request._PROPERTY_CRITERIA buildPropertyCriteria(PropertyItem pi)
		{
			DTD.Request._PROPERTY_CRITERIA pc 
				= new DTD.Request._PROPERTY_CRITERIA();

			#region comented out - parsed street address
			/*
				DTD.Request.PARSED_STREET_ADDRESS psa 
					= new DTD.Request.PARSED_STREET_ADDRESS();
			
				psa._HouseNumber = pi.Owner.SiteAddress.SiteStreetNumber;
				psa._DirectionPrefix = pi.Owner.SiteAddress.SiteStreetPreDirectional;
				psa._StreetName = pi.Owner.SiteAddress.SiteStreetName;
				psa._DirectionSuffix = pi.Owner.SiteAddress.SiteStreetPostDirectional;
				psa._StreetSuffix = pi.Owner.SiteAddress.SiteStreetSuffix;
				psa._ApartmentOrUnit = pi.Owner.SiteAddress.SiteStreetUnitNumber;

				pc.PARSED_STREET_ADDRESS = psa;
			*/
			#endregion

			if (null != pi && null != pi.Owner && null != pi.Owner.SiteAddress)
			{
				pc._StreetAddress = pi.Owner.SiteAddress.FullSiteStreetAddress;			
				pc._City = pi.Owner.SiteAddress.SiteCity;
				pc._PostalCode = pi.Owner.SiteAddress.SiteZIP;
				pc._County = pi.Owner.SiteAddress.SiteCountry;
				pc._State = pi.Owner.SiteAddress.SiteState;
			}

			return pc;
		}

		private DTD.Request._PROPERTY_CRITERIA buildAPNCriteria(PropertyItem pi)
		{
			DTD.Request._PROPERTY_CRITERIA pc 
				= new DTD.Request._PROPERTY_CRITERIA();
			
			if (null != pi && null != pi.Owner && null != pi.Owner.SiteAddress)
			{
				pc._PostalCode = pi.Owner.SiteAddress.SiteZIP;
			
				pc._County = pi.Owner.SiteAddress.SiteCountry;
				pc._State = pi.Owner.SiteAddress.SiteState;
			}
			

			return pc;
		}
		#endregion
		#region . Build Request .
        private byte[] buildRequestDATA(DTD.Request._SUBJECT_SEARCH ss, DTD.Request._PROPERTY_CRITERIA pc)
        {
            DTD.Request.REQUEST_GROUP rqg = new DTD.Request.REQUEST_GROUP();
            rqg.REQUEST = new DTD.Request.REQUEST();
            rqg.REQUEST.REQUESTDATA = new DTD.Request.REQUESTDATA[1];
            rqg.REQUEST.REQUESTDATA[0] = new DTD.Request.REQUESTDATA();
            rqg.REQUEST.REQUESTDATA[0].PROPERTY_INFORMATION_REQUEST = new DTD.Request.PROPERTY_INFORMATION_REQUEST();
            rqg.REQUEST.REQUESTDATA[0].PROPERTY_INFORMATION_REQUEST._SEARCH_CRITERIA = new DTD.Request._SEARCH_CRITERIA();

            DTD.Request._CONNECT2DATA_PRODUCT c2dp = new DTD.Request._CONNECT2DATA_PRODUCT();

            //Added phone Nr
            c2dp._DetailedSubjectReport = DTD.Request._CONNECT2DATA_PRODUCT_DetailedSubjectReport.Y;
            rqg.REQUEST.REQUESTDATA[0].PROPERTY_INFORMATION_REQUEST._CONNECT2DATA_PRODUCT = c2dp;
            rqg.REQUEST.REQUESTDATA[0].PROPERTY_INFORMATION_REQUEST._ActionType = DTD.Request.PROPERTY_INFORMATION_REQUEST_ActionType.Submit;
            rqg.REQUEST.REQUESTDATA[0].PROPERTY_INFORMATION_REQUEST._ActionTypeSpecified = true;
            rqg.REQUEST.REQUESTDATA[0].PROPERTY_INFORMATION_REQUEST._PROPERTY_CRITERIA = pc;
            
            //NEW
            if (null != ss)
                rqg.REQUEST.REQUESTDATA[0].PROPERTY_INFORMATION_REQUEST._SEARCH_CRITERIA.Items = new object[1] { ss };
            else
                rqg.REQUEST.REQUESTDATA[0].PROPERTY_INFORMATION_REQUEST._SEARCH_CRITERIA.Items = null;
            //END


            rqg.REQUESTING_PARTY = new DTD.Request.REQUESTING_PARTY[1];

            rqg.MISMOVersionID = "2.1";

            DTD.Request._RESPONSE_CRITERIA rescrit = new DTD.Request._RESPONSE_CRITERIA();
            rescrit._NumberSubjectPropertiesType = DTD.Request._RESPONSE_CRITERIA_NumberSubjectPropertiesType.Item100;
            rqg.REQUEST.REQUESTDATA[0].PROPERTY_INFORMATION_REQUEST._RESPONSE_CRITERIA = rescrit;

            

            rqg.REQUEST._JobIdentifier = "TEST MASTER V2.0";
            rqg.REQUEST._RecordIdentifier = "12345A8";
            rqg.REQUEST._HVERequestTypeSpecified = true;
            rqg.REQUEST._HVERequestType = DTD.Request.REQUEST_HVERequestType.Item02;
            rqg.REQUEST._HVCustomerIdentifier = "";

            //return rqg;                                                 

#if DUMP_XML_OBJECT
            //dumpObject(rqg.REQUEST, "_MM_REQ");
#endif

            MemoryStream stream = new MemoryStream();
            XmlSerializer formatter = new XmlSerializer(rqg.REQUEST.REQUESTDATA[0].GetType());
            formatter.Serialize(stream, rqg.REQUEST.REQUESTDATA[0]);
            return stream.ToArray();
        }
        
        
        private DTD.Request.REQUEST_GROUP buildRequest(
			DTD.Request._SUBJECT_SEARCH ss
			, DTD.Request._PROPERTY_CRITERIA pc)
		{
			DTD.Request.REQUEST_GROUP rqg 
				= new DTD.Request.REQUEST_GROUP();


			rqg.REQUEST 
				= new DTD.Request.REQUEST();
			rqg.REQUEST.REQUESTDATA 
				= new DTD.Request.REQUESTDATA[1];
			rqg.REQUEST.REQUESTDATA[0] 
				= new DTD.Request.REQUESTDATA();
			rqg.REQUEST.REQUESTDATA[0].PROPERTY_INFORMATION_REQUEST 
				= new DTD.Request.PROPERTY_INFORMATION_REQUEST();
			rqg.REQUEST.REQUESTDATA[0].PROPERTY_INFORMATION_REQUEST._SEARCH_CRITERIA 
				= new DTD.Request._SEARCH_CRITERIA();

			DTD.Request._CONNECT2DATA_PRODUCT c2dp
				= new DTD.Request._CONNECT2DATA_PRODUCT();

			

			//adding mailing address - commented out short and added standard
			//c2dp._ShortSubjectReport = DTD.Request._CONNECT2DATA_PRODUCT_ShortSubjectReport.Y;
			//c2dp._StandardSubjectReport = DTD.Request._CONNECT2DATA_PRODUCT_StandardSubjectReport.Y;
			
			//Added phone Nr
			c2dp._DetailedSubjectReport = DTD.Request._CONNECT2DATA_PRODUCT_DetailedSubjectReport.Y;

			rqg.REQUEST.REQUESTDATA[0].PROPERTY_INFORMATION_REQUEST._CONNECT2DATA_PRODUCT = c2dp;
			
			rqg.REQUEST.REQUESTDATA[0].PROPERTY_INFORMATION_REQUEST._ActionType 
				= DTD.Request.PROPERTY_INFORMATION_REQUEST_ActionType.Submit;
			rqg.REQUEST.REQUESTDATA[0].PROPERTY_INFORMATION_REQUEST._ActionTypeSpecified = true;

			rqg.REQUEST.REQUESTDATA[0].PROPERTY_INFORMATION_REQUEST._PROPERTY_CRITERIA
				= pc;
			
			//dtd 4.2.2
			//BEGIN
			//OLD

				//rqg.REQUEST.REQUESTDATA[0].PROPERTY_INFORMATION_REQUEST._SEARCH_CRITERIA.Item 
				//	= ss;

			//NEW
			if (null != ss)
				rqg.REQUEST.REQUESTDATA[0].PROPERTY_INFORMATION_REQUEST._SEARCH_CRITERIA.Items = new object[1] { ss };			
			else 
				rqg.REQUEST.REQUESTDATA[0].PROPERTY_INFORMATION_REQUEST._SEARCH_CRITERIA.Items = null;			
			//END


			rqg.REQUESTING_PARTY 
				= new DTD.Request.REQUESTING_PARTY[1];
			
			rqg.MISMOVersionID = "2.1";

			DTD.Request._RESPONSE_CRITERIA rescrit 
				= new DTD.Request._RESPONSE_CRITERIA();

			rescrit._NumberSubjectPropertiesType 
				= DTD.Request._RESPONSE_CRITERIA_NumberSubjectPropertiesType.Item100;
			
			rqg.REQUEST.REQUESTDATA[0].PROPERTY_INFORMATION_REQUEST._RESPONSE_CRITERIA
				= rescrit;

	
			

			//rqg.REQUEST.LoginAccountIdentifier="PROFT2";						
			//rqg.REQUEST.LoginAccountPassword="TKS2GRABR"; 

			rqg.REQUEST._JobIdentifier="TEST MASTER V2.0"; 
			rqg.REQUEST._RecordIdentifier="12345A8";
			rqg.REQUEST._HVERequestTypeSpecified = true;
			rqg.REQUEST._HVERequestType 
				= DTD.Request.REQUEST_HVERequestType.Item02;
			rqg.REQUEST._HVCustomerIdentifier="";

			return rqg;
		}
		#endregion

		public bool resolveData(SearchType searchType, ref PropertyItem pi)
		{
			return getSubject(searchType, ref pi);
		}

		private bool getSubject(SearchType searchType, ref PropertyItem pi)
		{			   
			
			#region buid data
			DTD.Request._SUBJECT_SEARCH ss = null;
			DTD.Request._PROPERTY_CRITERIA pc = null;
			
			if (searchType == SearchType.AddressSearch)
			{
				pc = buildPropertyCriteria(pi);
				
				
				if (_useAPN && null != pi)
				{
					//IF BOTH EXIST (USER DATA & IMPORTED DATA) --> TAKE USER DATA
					if (null != pi.DealProperty && null != pi.DealProperty.Apn && string.Empty != pi.DealProperty.Apn && 
						null != pi.DealInfo && null != pi.DealInfo.FinancialProperty && string.Empty != pi.DealInfo.FinancialProperty.ParcelNumber)
					{
						ss = new DTD.Request._SUBJECT_SEARCH();						
						ss._UnformattedParcelIdentifier = pi.DealProperty.Apn;
					}

					//ONLY USER DATA EXISTS
					if (null != pi.DealProperty && null != pi.DealProperty.Apn && string.Empty != pi.DealProperty.Apn)
					{
						ss = new DTD.Request._SUBJECT_SEARCH();
						//ss._AssessorsParcelIdentifier = pi.DealProperty.Apn;
						ss._UnformattedParcelIdentifier = pi.DealProperty.Apn;											
					}

					//ONLY IMPORTED DATA EXISTS
					if (null != pi.DealInfo && null != pi.DealInfo.FinancialProperty && string.Empty != pi.DealInfo.FinancialProperty.ParcelNumber)
					{
						ss = new DTD.Request._SUBJECT_SEARCH();
						ss._UnformattedParcelIdentifier = pi.DealInfo.FinancialProperty.ParcelNumber;
					}					
				}
				
				

			}
			else
			{				
				pc = buildAPNCriteria(pi);
				ss = new DTD.Request._SUBJECT_SEARCH();		
				ss._AssessorsParcelIdentifier = pi.DealProperty.Apn;
			}
			
				
			DTD.Request.REQUEST_GROUP request = buildRequest(ss, pc);

			//DEBUG
#if DUMP_XML_OBJECT && DEBUG
			dumpObject(request, "_MM_REQUEST.xml");
#endif
			//DEBUG
			
			XmlSerializer requestSerializer  
				= new XmlSerializer(typeof(DTD.Request.REQUEST_GROUP));
			XmlSerializer responseSerializer  
				= new XmlSerializer(typeof(DTD.Response.RESPONSE_GROUP));

			MemoryStream requestStream = new MemoryStream();
			MemoryStream responseStream = new MemoryStream();

			// IMPORTANT  -  we do not need a namespace!
			System.Xml.Serialization.XmlSerializerNamespaces nmspace = new XmlSerializerNamespaces();
			nmspace.Add("","");
			// IMPORTANT  -  we do not need a namespace!
				
			System.Xml.XmlWriter writer = new XmlTextWriter(requestStream, System.Text.UTF8Encoding.UTF8);
			//writer.Formatting = System.Xml.Formatting.Indented;
			writer.WriteStartDocument();
			//TODO: define string for request..
			writer.WriteDocType("REQUEST_GROUP", null, "C2DRequestv2.0.dtd",null);
			requestSerializer.Serialize(writer, request,nmspace);
			writer.Flush();
			#endregion

			//requestSerializer.Serialize(requestStream, request);
			byte[] requestArray = null;
			byte[] responseArray = null;
			string resultMessage = string.Empty;
			try
			{
				Cursor.Current = Cursors.WaitCursor;
				Application.DoEvents();


				int count =(int)requestStream.Length-3;
				requestArray = new byte[count];
				long newPos = requestStream.Seek(0, SeekOrigin.Begin);
		
				//TA++:08.04.2005
				byte[] reqStream = requestStream.ToArray();
				for (int i = 3; i < requestStream.Length; i++)
				{
					requestArray[i - 3] = (byte)reqStream[i];
				}
				//TA--:08.04.2005
				writer.Close();

				// ++ web
				DataConsumption.ServiceDataConsumption sdc
					= new DealMaker.DataConsumption.ServiceDataConsumption();
			
				if (string.Empty != Globals.ProfitGrabberWebSupportURLDataConsumption)
				{
					sdc.Url = Globals.ProfitGrabberWebSupportURLDataConsumption;
				}

				bool resolved = false   ;
				int tryer =0;
				while(!resolved && tryer++ < 3)
				{
					try
					{													
						/*sdc.RequestDataCOMPS(                        
							Globals.DataConsumptionReg,
							requestArray,
							out responseArray, 
							out resultMessage);*/

                        sdc.RequestDataCOMPS3(
                            Globals.DataConsumptionReg,
                            buildRequestDATA(ss, pc),
                            out responseArray,
                            out resultMessage);	

						resolved = true;
					}
					catch  (Exception xxx)
					{
						totalErrCount++;
						if (tryer == 3 && FireServiceErrorEvent != null)
						{
							FireServiceErrorEvent("1.) At getting service data ("+totalErrCount+")" + " ResMsg:" + resultMessage, xxx);
							return false;
						}
						else
						{
							if (tryer == 3)
							{
								throw	new Exception("1.+) At getting service data, tryer = 3", xxx);
							}
						}
					}
				}
	
			

				sdc = null;

				if (null == responseArray || 0==responseArray.Length)
				{	
                    //OLD - BEGIN
					//FireServiceErrorEvent(resultMessage, null);					
                    //return false;
                    //OLD - END

                    //NEW - BEGIN
                    FireQueryNegativelySolvedEvent(pi, "Connection Failure!");
                    return true;
                    //NEW - END

				}
				else
				{
					//todo(ta)
				}

				responseStream.Write(responseArray,0,responseArray.Length);

				responseStream.Position = 0;
				response = (DTD.Response.RESPONSE_GROUP)responseSerializer.Deserialize(responseStream);

#if DUMP_XML_OBJECT && DEBUG
                dumpObject(response, "_MM_RESPONSE.xml");
#endif
			}
			catch (Exception ex)
			{
				
				if (FireServiceErrorEvent != null)
				{
					FireServiceErrorEvent("3.) At manipulating service data.", ex);
					return false;
				}
				else
				{
					throw	new Exception("4.) At manipulating service data.", ex);
				}
			}
			finally
			{
				Cursor.Current = Cursors.Default;
			}
			
			string error = "";
			
			if (CheckValid(ref pi, ref error))
			{
				if (string.Empty == error)
				{
					#region CHARGING
					DataConsumption.ServiceDataConsumption sdc2 = new DealMaker.DataConsumption.ServiceDataConsumption();

					if (string.Empty != Globals.ProfitGrabberWebSupportURLDataConsumption)
					{
						sdc2.Url = Globals.ProfitGrabberWebSupportURLDataConsumption;
					}
					bool resolved = false   ;
					int tryer =0;
					while(!resolved && tryer++ < 3)
					{
						try
						{
							string msg;
							Guid userId = DealMaker.Globals.UserRegistration.IDUser;

							if (Globals.NwVerEnabled && null != Globals.activeModules && Globals.activeModules.MOD_6)
								sdc2.QueryGuidFromKey(Globals.DataConsumptionReg.KeyCode, out userId, out msg);

							sdc2.ConfirmDataRequestSuccess(userId/*DealMaker.Globals.UserRegistration.IDUser*/, 1, 2, out resultMessage);						
							resolved = true;
						}
						catch  (Exception xxx)
						{
							totalErrCount++;
							if (tryer == 3 && FireServiceErrorEvent != null)
							{
								FireServiceErrorEvent("5.) At charging service data. ("+totalErrCount+")", xxx);
								return false;
							}
							else
							{
								if (tryer == 3)
									throw	new Exception("6.) At charging service data", xxx);
							}
						}
					}
					sdc2 = null;
					#endregion CHARGING
				}

				bool previousValueContained = false;
				bool previousMailingAddressContained = false;
				bool previousPhoneNumberContained = false;
									
				if (-1 == error.IndexOf("UNSUCCESSFUL", 0))
				{
					//this would sign that multiple properties
					//are found so pi is already filled.
					if (!"multimatched".Equals(error))
					{
						try
						{
							//DTD.Response.PROPERTY prop = response.RESPONSE.RESPONSE_DATA[0].PROPERTY_INFORMATION_RESPONSE.PROPERTY_INFORMATION[0].PROPERTY;
							DTD.Response.PROPERTY prop = null;
							_PROPERTY_INFORMATION propInfo = null;
							object[] items = response.RESPONSE.RESPONSE_DATA[0].PROPERTY_INFORMATION_RESPONSE.Items;
							foreach (object obj in items)
							{
								if (obj is DTD.Response._PROPERTY_INFORMATION)
								{
									propInfo = obj as DTD.Response._PROPERTY_INFORMATION;
									if (null != propInfo)
										prop = propInfo.PROPERTY;									
								}
							}

							

							//TA++ Owner Name Integration
							if (!string.IsNullOrWhiteSpace(pi?.Owner?.FullName))
								previousValueContained = true;
							
							if (!previousValueContained || (previousValueContained && OverrideExistingNames ))
								pi.Owner.FullName= ApplicationHelper.CaseString(prop.PROPERTY_OWNER[0]._OwnerName);
							//TA-- Owner Name Integration

							pi.Owner.SiteAddress.SiteStreetName	= prop._PARSED_STREET_ADDRESS._StreetName;
							pi.Owner.SiteAddress.SiteStreetNumber	= prop._PARSED_STREET_ADDRESS._HouseNumber;
							pi.Owner.SiteAddress.SiteStreetSuffix	= prop._PARSED_STREET_ADDRESS._StreetSuffix;
							pi.Owner.SiteAddress.SiteStreetPreDirectional	= prop._PARSED_STREET_ADDRESS._DirectionPrefix;
							pi.Owner.SiteAddress.SiteStreetPostDirectional	= prop._PARSED_STREET_ADDRESS._DirectionSuffix;
							pi.Owner.SiteAddress.SiteStreetUnitNumber	= prop._PARSED_STREET_ADDRESS._ApartmentOrUnit;				
							pi.Owner.SiteAddress.SiteCity = prop._City;
							pi.Owner.SiteAddress.SiteState = prop._State;
							pi.Owner.SiteAddress.SiteZIP = prop._PostalCode;
							pi.Owner.SiteAddress.SiteCountry = prop._County;


							PROPERTY_OWNER owner = prop.PROPERTY_OWNER[0];

                            if (!string.IsNullOrWhiteSpace(pi.Owner?.MailAddress?.FullSiteStreetAddress))
                                previousMailingAddressContained = true;

							if (!previousMailingAddressContained || OverrideExistingMailingAddress)
							{
								if (null != prop.PROPERTY_OWNER && prop.PROPERTY_OWNER.Length > 0)
								{															
									//MAILING ADDRESS
									if (null != owner && "N/AVAIL" != owner._MailingAddress)	// N/AVAIL
									{
										if (!DoNotProcess_NM_Records || !(DoNotProcess_NM_Records && IsContainingNoMailPrefix(owner._MailingAddress))) // (No Mail) prefix
										{

											if (null == pi.Owner.MailAddress)
											{
												pi.Owner.MailAddress = new Address();
												pi.Owner.MailAddress.save = true;
											}
											else
											{
												pi.Owner.MailAddress.save = false; // just update
												previousMailingAddressContained = true;
											}

											if (OverrideExistingMailingAddress || string.IsNullOrWhiteSpace(pi?.Owner?.MailAddress?.SiteStreetName))
											{                                                
												pi.Owner.MailAddress.FullSiteStreetAddress = string.Empty;	//FORCE IT!!!
												pi.Owner.MailAddress.SiteStreetName	= ((null != owner._MailingAddress) ? owner._MailingAddress : string.Empty);												
                                                pi.Owner.MailAddress.SiteCitySiteState = ((null != owner._MailingCityAndState) ? owner._MailingCityAndState : string.Empty);

                                                string tempCity = string.Empty;
                                                string tempState = string.Empty;
                                                if (null != pi && null != pi.Owner && null != pi.Owner.MailAddress && null != pi.Owner.MailAddress.SiteCitySiteState)                                                    
                                                    SplitCityState(pi.Owner.MailAddress.SiteCitySiteState, out tempCity, out tempState);

                                                pi.Owner.MailAddress.SiteCity = tempCity;
                                                pi.Owner.MailAddress.SiteState = tempState;

												pi.Owner.MailAddress.SiteZIP = ((null != owner._MailingPostalCode) ? owner._MailingPostalCode : string.Empty);								
											}											
										}
									}
								}
							}



							//PHONE NUMBER
                            if (!string.IsNullOrEmpty(pi.Owner?.HomePhone?.PhoneNr))
                                previousPhoneNumberContained = true;

							if (OverrideExistingPhoneNumber)
							{
								if (null == pi.Owner.HomePhone)
								{
									pi.Owner.HomePhone = new Phone();
									pi.Owner.HomePhone.save = true;
								}
								
								else
								{
									pi.Owner.HomePhone.save = false;
									previousPhoneNumberContained = true;
								}																

								if (null == pi.Owner.HomePhone ||(null != pi.Owner.HomePhone && OverrideExistingPhoneNumber))
								{
									pi.Owner.HomePhone.PhoneNr = owner._PhoneNumber;
								}
								else
								{
									pi.Owner.HomePhone.PhoneNr = string.Empty;
								}
							}
						}
						catch {}
					}
				}
				else
				{
#if DEBUG
					MessageBox.Show(error, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
#endif
					return false;
				}

				if (ManualStepping)
				{
					return true;
				}
				else
				{
					FireQueryPositivelySolvedEvent(pi, previousValueContained, previousMailingAddressContained, previousPhoneNumberContained);	
				}
			}
			else
			{
				if (ManualStepping)
				{
					MessageBox.Show(error, "ProfitGrabber Pro");
					return false;
				}
				else
				{	  
					FireQueryNegativelySolvedEvent(pi, error);
				}
			}
	
			return true;
			
		}

        void SplitCityState(string cityState, out string city, out string state)
        {
            city = string.Empty;
            state = string.Empty;

            if (null != cityState && string.Empty != cityState)
            {
                try
                {
                    string[] splittedString = cityState.Split(' ');
                    state = splittedString[splittedString.Length - 1];
                    city = cityState.Substring(0, cityState.Length - state.Length - 1);
                }
                catch { }
            }
        }

		#region . CheckValid.
		private bool CheckValid(ref PropertyItem pi, ref string error)
		{
			DTD.Response.STATUS status;

			try 
			{
                if (null != response && null != response.RESPONSE.STATUS && response.RESPONSE.STATUS.Length > 0)
                {
                    status = response.RESPONSE.STATUS[0];
                    if (!status._Condition.Trim().StartsWith("SUCCES"))
                    {
                        error = status._Condition + " " + status._Description;
                        return false;
                    }
                }
			}
			catch{};
			try 
			{
				status =response.RESPONSE.RESPONSE_DATA[0].PROPERTY_INFORMATION_RESPONSE.STATUS;
				if (!status._Condition.Trim().StartsWith("SUCCES"))
				{
					error = status._Condition + " " +status._Description;
					return false;
				}
			}
			catch{};
			try 
			{
				//status = response.RESPONSE.RESPONSE_DATA[0].PROPERTY_INFORMATION_RESPONSE._PRODUCT[0].STATUS;
				status = null;

				DTD.Response._PRODUCT product = null;				
				object[] items = response.RESPONSE.RESPONSE_DATA[0].PROPERTY_INFORMATION_RESPONSE.Items;
				foreach (object obj in items)
				{
					if (obj is DTD.Response._PRODUCT)
					{
						product = obj as DTD.Response._PRODUCT;
						if (null != product)
							if (null != product.STATUS && product.STATUS.Length > 0)
								status = product.STATUS[0];									
					}
				}

				if (!status._Condition.Trim().StartsWith("SUCCES"))
				{
					error = status._Condition + " " +status._Description;
					if (!ManualStepping) return false;
				}
			}
			catch{};

			try 
			{
				//DTD.Response._MULTIPLE_RECORDS[] multipla 
				//	= response.RESPONSE.RESPONSE_DATA[0].PROPERTY_INFORMATION_RESPONSE._PROPERTY_INFORMATION[0]._MULTIPLE_RECORDS;

				DTD.Response._MULTIPLE_RECORDS[] multipla = null;
				
				_PROPERTY_INFORMATION propInfo = null;
				object[] items = response.RESPONSE.RESPONSE_DATA[0].PROPERTY_INFORMATION_RESPONSE.Items;
				foreach (object obj in items)
				{
					if (obj is DTD.Response._PROPERTY_INFORMATION)
					{
						propInfo = obj as DTD.Response._PROPERTY_INFORMATION;
						if (null != propInfo)
							multipla = propInfo._MULTIPLE_RECORDS;									
					}
				}

				if (multipla != null)				
				{
					if (ManualStepping)
					{
						DealMaker.FormMultipleProperiesFound form = new DealMaker.FormMultipleProperiesFound(false);

						form.lvFill(multipla);
						form.ShowDialog();

						int iterator = form.selected;

						if (iterator >= 0)
						{
							pi.Owner.FirstName = getSecureString(multipla[iterator].PROPERTY.PROPERTY_OWNER[0]._OwnerName);
							pi.Owner.SiteAddress.SiteStreetName	= getSecureString(multipla[iterator].PROPERTY._PARSED_STREET_ADDRESS._StreetName);
							pi.Owner.SiteAddress.SiteStreetNumber	= getSecureString(multipla[iterator].PROPERTY._PARSED_STREET_ADDRESS._HouseNumber);
							pi.Owner.SiteAddress.SiteStreetSuffix	= getSecureString(multipla[iterator].PROPERTY._PARSED_STREET_ADDRESS._StreetSuffix);
							pi.Owner.SiteAddress.SiteStreetPreDirectional	= getSecureString(multipla[iterator].PROPERTY._PARSED_STREET_ADDRESS._DirectionPrefix);
							pi.Owner.SiteAddress.SiteStreetPostDirectional	= getSecureString(multipla[iterator].PROPERTY._PARSED_STREET_ADDRESS._DirectionSuffix);
							pi.Owner.SiteAddress.SiteStreetUnitNumber	= getSecureString(multipla[iterator].PROPERTY._PARSED_STREET_ADDRESS._ApartmentOrUnit);

							pi.Owner.SiteAddress.SiteCity=getSecureString(multipla[iterator].PROPERTY._City);
							pi.Owner.SiteAddress.SiteState=getSecureString(multipla[iterator].PROPERTY._State);
							pi.Owner.SiteAddress.SiteZIP=getSecureString(multipla[iterator].PROPERTY._PostalCode);
							pi.Owner.SiteAddress.SiteCountry = getSecureString(multipla[iterator].PROPERTY._County);

							error ="multimatched";
							return true;
						}
					}
					else
					{
						error = "WARNING Multiple Properties Found!";
						return false;
					}
				}
			}
			catch{};
			return true;
		}
		#endregion

		#region getSecureString
		private string getSecureString(object str)
		{
			string SecureString = "";
			try
			{
				SecureString = str.ToString();
			}
			catch
			{}
			return SecureString;
		}
		#endregion		

		bool IsContainingNoMailPrefix(string address)
		{
			if (-1 == address.IndexOf("(No Mail)"))
				return false;
			else
				return true;
		}

		#region DUMP OBJECT TO FILE
		private void dumpObject(object theObject, string filename)
		{
#if DUMP_XML_OBJECT && DEBUG
            //System.Xml.Serialization.XmlSerializer serializer
            //    = new System.Xml.Serialization.XmlSerializer(theObject.GetType());

            //// IMPORTANT  -  we do not need a namespace!
            //System.Xml.Serialization.XmlSerializerNamespaces nmspace = new XmlSerializerNamespaces();
            //nmspace.Add("","");
            //// IMPORTANT  -  we do not need a namespace!
				
            //System.Xml.XmlTextWriter writer = new XmlTextWriter(filename, System.Text.UTF8Encoding.UTF8);
            //writer.Formatting = System.Xml.Formatting.Indented;
            //writer.WriteStartDocument();
            //writer.WriteDocType("REQUEST_GROUP", null, "C2DRequestv2.0.PropertyProfileComps",null);
            //serializer.Serialize(writer, theObject,nmspace);
            //writer.Flush();
            //writer.Close();
#endif

		}
		#endregion
	}
}
