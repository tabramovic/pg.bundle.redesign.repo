using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace DealMaker
{
	/// <summary>
	/// Summary description for PropertyItemMailingData.
	/// </summary>
	public class PropertyItemMailingData : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Label label94;
		private System.Windows.Forms.TextBox tbSiteCity;
		private System.Windows.Forms.Button btnMatch;
		private System.Windows.Forms.Button btnEndMatch;
		private System.Windows.Forms.Button btnSkip;

		public bool Match=false;
		public bool Skip=false;
		public bool Cancel=false;

		PropertyItem propertyItem;
		private System.Windows.Forms.TextBox textBoxStateA;
		private System.Windows.Forms.TextBox textBoxCountyA;
		private System.Windows.Forms.TextBox textBoxZipA;
		private System.Windows.Forms.Label label74;
		private System.Windows.Forms.Label label75;
		private System.Windows.Forms.Label label76;
		private System.Windows.Forms.TextBox textBoxUnitNoA;
		private System.Windows.Forms.TextBox textBoxSuffixA;
		private System.Windows.Forms.TextBox textBoxPostDirA;
		private System.Windows.Forms.TextBox textBoxStreetA;
		private System.Windows.Forms.TextBox textBoxPreDirA;
		private System.Windows.Forms.TextBox textBoxStreetNoA;
		private System.Windows.Forms.Label label77;
		private System.Windows.Forms.Label label78;
		private System.Windows.Forms.Label label79;
		private System.Windows.Forms.Label label80;
		private System.Windows.Forms.Label label81;
		private System.Windows.Forms.Label label82;
		private System.Windows.Forms.Label label89;
		private System.Windows.Forms.Label label88;
		private System.Windows.Forms.TextBox textBoxStateB;
		private System.Windows.Forms.TextBox textBoxCountyB;
		private System.Windows.Forms.TextBox textBoxZipB;
		private System.Windows.Forms.TextBox textBoxApnB;
		private System.Windows.Forms.Label label68;
		private System.Windows.Forms.Label label69;
		private System.Windows.Forms.Label label70;
		private System.Windows.Forms.Label label71;
		private System.Windows.Forms.RadioButton _rbAddress;
		private System.Windows.Forms.RadioButton _rbParcelNr;
		private System.Windows.Forms.Label label1;

		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public PropertyItemMailingData()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			_rbAddress.CheckedChanged += new EventHandler(OnrbAddressCheckedChanged);

			textBoxApnB.Enabled = false;
			textBoxZipB.Enabled = false;
			textBoxCountyB.Enabled = false;
			textBoxStateB.Enabled = false;
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label94 = new System.Windows.Forms.Label();
			this.tbSiteCity = new System.Windows.Forms.TextBox();
			this.btnMatch = new System.Windows.Forms.Button();
			this.btnEndMatch = new System.Windows.Forms.Button();
			this.btnSkip = new System.Windows.Forms.Button();
			this.textBoxStateA = new System.Windows.Forms.TextBox();
			this.textBoxCountyA = new System.Windows.Forms.TextBox();
			this.textBoxZipA = new System.Windows.Forms.TextBox();
			this.label74 = new System.Windows.Forms.Label();
			this.label75 = new System.Windows.Forms.Label();
			this.label76 = new System.Windows.Forms.Label();
			this.textBoxUnitNoA = new System.Windows.Forms.TextBox();
			this.textBoxSuffixA = new System.Windows.Forms.TextBox();
			this.textBoxPostDirA = new System.Windows.Forms.TextBox();
			this.textBoxStreetA = new System.Windows.Forms.TextBox();
			this.textBoxPreDirA = new System.Windows.Forms.TextBox();
			this.textBoxStreetNoA = new System.Windows.Forms.TextBox();
			this.label77 = new System.Windows.Forms.Label();
			this.label78 = new System.Windows.Forms.Label();
			this.label79 = new System.Windows.Forms.Label();
			this.label80 = new System.Windows.Forms.Label();
			this.label81 = new System.Windows.Forms.Label();
			this.label82 = new System.Windows.Forms.Label();
			this.label89 = new System.Windows.Forms.Label();
			this.label88 = new System.Windows.Forms.Label();
			this.textBoxStateB = new System.Windows.Forms.TextBox();
			this.textBoxCountyB = new System.Windows.Forms.TextBox();
			this.textBoxZipB = new System.Windows.Forms.TextBox();
			this.textBoxApnB = new System.Windows.Forms.TextBox();
			this.label68 = new System.Windows.Forms.Label();
			this.label69 = new System.Windows.Forms.Label();
			this.label70 = new System.Windows.Forms.Label();
			this.label71 = new System.Windows.Forms.Label();
			this._rbAddress = new System.Windows.Forms.RadioButton();
			this._rbParcelNr = new System.Windows.Forms.RadioButton();
			this.label1 = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// label94
			// 
			this.label94.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.label94.Location = new System.Drawing.Point(472, 64);
			this.label94.Name = "label94";
			this.label94.Size = new System.Drawing.Size(24, 16);
			this.label94.TabIndex = 88;
			this.label94.Text = "City:";
			// 
			// tbSiteCity
			// 
			this.tbSiteCity.Location = new System.Drawing.Point(464, 80);
			this.tbSiteCity.Name = "tbSiteCity";
			this.tbSiteCity.Size = new System.Drawing.Size(136, 20);
			this.tbSiteCity.TabIndex = 7;
			this.tbSiteCity.Text = "";
			// 
			// btnMatch
			// 
			this.btnMatch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnMatch.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnMatch.Location = new System.Drawing.Point(576, 200);
			this.btnMatch.Name = "btnMatch";
			this.btnMatch.Size = new System.Drawing.Size(96, 24);
			this.btnMatch.TabIndex = 94;
			this.btnMatch.Text = "Match";
			this.btnMatch.Click += new System.EventHandler(this.btnMatch_Click);
			// 
			// btnEndMatch
			// 
			this.btnEndMatch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnEndMatch.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnEndMatch.Location = new System.Drawing.Point(800, 200);
			this.btnEndMatch.Name = "btnEndMatch";
			this.btnEndMatch.Size = new System.Drawing.Size(96, 24);
			this.btnEndMatch.TabIndex = 95;
			this.btnEndMatch.Text = "End Matching";
			this.btnEndMatch.Click += new System.EventHandler(this.btnEndMatch_Click);
			// 
			// btnSkip
			// 
			this.btnSkip.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnSkip.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnSkip.Location = new System.Drawing.Point(688, 200);
			this.btnSkip.Name = "btnSkip";
			this.btnSkip.Size = new System.Drawing.Size(96, 24);
			this.btnSkip.TabIndex = 96;
			this.btnSkip.Text = "Skip";
			this.btnSkip.Click += new System.EventHandler(this.btnSkip_Click);
			// 
			// textBoxStateA
			// 
			this.textBoxStateA.Location = new System.Drawing.Point(792, 80);
			this.textBoxStateA.MaxLength = 2;
			this.textBoxStateA.Name = "textBoxStateA";
			this.textBoxStateA.Size = new System.Drawing.Size(24, 20);
			this.textBoxStateA.TabIndex = 10;
			this.textBoxStateA.Text = "";
			// 
			// textBoxCountyA
			// 
			this.textBoxCountyA.Location = new System.Drawing.Point(664, 80);
			this.textBoxCountyA.Name = "textBoxCountyA";
			this.textBoxCountyA.Size = new System.Drawing.Size(120, 20);
			this.textBoxCountyA.TabIndex = 9;
			this.textBoxCountyA.Text = "";
			// 
			// textBoxZipA
			// 
			this.textBoxZipA.Location = new System.Drawing.Point(608, 80);
			this.textBoxZipA.Name = "textBoxZipA";
			this.textBoxZipA.Size = new System.Drawing.Size(40, 20);
			this.textBoxZipA.TabIndex = 8;
			this.textBoxZipA.Text = "";
			// 
			// label74
			// 
			this.label74.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.label74.Location = new System.Drawing.Point(792, 64);
			this.label74.Name = "label74";
			this.label74.Size = new System.Drawing.Size(88, 23);
			this.label74.TabIndex = 142;
			this.label74.Text = "State (i.e. CA)";
			// 
			// label75
			// 
			this.label75.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.label75.Location = new System.Drawing.Point(664, 64);
			this.label75.Name = "label75";
			this.label75.Size = new System.Drawing.Size(64, 23);
			this.label75.TabIndex = 141;
			this.label75.Text = "County";
			// 
			// label76
			// 
			this.label76.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.label76.Location = new System.Drawing.Point(608, 64);
			this.label76.Name = "label76";
			this.label76.Size = new System.Drawing.Size(64, 23);
			this.label76.TabIndex = 140;
			this.label76.Text = "ZIP";
			// 
			// textBoxUnitNoA
			// 
			this.textBoxUnitNoA.Location = new System.Drawing.Point(408, 80);
			this.textBoxUnitNoA.Name = "textBoxUnitNoA";
			this.textBoxUnitNoA.Size = new System.Drawing.Size(40, 20);
			this.textBoxUnitNoA.TabIndex = 6;
			this.textBoxUnitNoA.Text = "";
			// 
			// textBoxSuffixA
			// 
			this.textBoxSuffixA.BackColor = System.Drawing.Color.LightGray;
			this.textBoxSuffixA.Location = new System.Drawing.Point(360, 80);
			this.textBoxSuffixA.Name = "textBoxSuffixA";
			this.textBoxSuffixA.Size = new System.Drawing.Size(32, 20);
			this.textBoxSuffixA.TabIndex = 5;
			this.textBoxSuffixA.TabStop = false;
			this.textBoxSuffixA.Text = "";
			// 
			// textBoxPostDirA
			// 
			this.textBoxPostDirA.BackColor = System.Drawing.Color.LightGray;
			this.textBoxPostDirA.Location = new System.Drawing.Point(312, 80);
			this.textBoxPostDirA.Name = "textBoxPostDirA";
			this.textBoxPostDirA.Size = new System.Drawing.Size(30, 20);
			this.textBoxPostDirA.TabIndex = 4;
			this.textBoxPostDirA.TabStop = false;
			this.textBoxPostDirA.Text = "";
			// 
			// textBoxStreetA
			// 
			this.textBoxStreetA.Location = new System.Drawing.Point(176, 80);
			this.textBoxStreetA.Name = "textBoxStreetA";
			this.textBoxStreetA.Size = new System.Drawing.Size(128, 20);
			this.textBoxStreetA.TabIndex = 3;
			this.textBoxStreetA.Text = "";
			// 
			// textBoxPreDirA
			// 
			this.textBoxPreDirA.BackColor = System.Drawing.Color.LightGray;
			this.textBoxPreDirA.Location = new System.Drawing.Point(128, 80);
			this.textBoxPreDirA.Name = "textBoxPreDirA";
			this.textBoxPreDirA.Size = new System.Drawing.Size(30, 20);
			this.textBoxPreDirA.TabIndex = 2;
			this.textBoxPreDirA.TabStop = false;
			this.textBoxPreDirA.Text = "";
			// 
			// textBoxStreetNoA
			// 
			this.textBoxStreetNoA.Location = new System.Drawing.Point(64, 80);
			this.textBoxStreetNoA.Name = "textBoxStreetNoA";
			this.textBoxStreetNoA.Size = new System.Drawing.Size(48, 20);
			this.textBoxStreetNoA.TabIndex = 1;
			this.textBoxStreetNoA.Text = "";
			// 
			// label77
			// 
			this.label77.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.label77.Location = new System.Drawing.Point(400, 64);
			this.label77.Name = "label77";
			this.label77.Size = new System.Drawing.Size(64, 23);
			this.label77.TabIndex = 139;
			this.label77.Text = "Unit No.";
			// 
			// label78
			// 
			this.label78.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.label78.Location = new System.Drawing.Point(352, 64);
			this.label78.Name = "label78";
			this.label78.Size = new System.Drawing.Size(64, 23);
			this.label78.TabIndex = 138;
			this.label78.Text = "Suffix";
			// 
			// label79
			// 
			this.label79.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.label79.Location = new System.Drawing.Point(304, 64);
			this.label79.Name = "label79";
			this.label79.Size = new System.Drawing.Size(64, 23);
			this.label79.TabIndex = 137;
			this.label79.Text = "Post-dir";
			// 
			// label80
			// 
			this.label80.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.label80.Location = new System.Drawing.Point(168, 64);
			this.label80.Name = "label80";
			this.label80.Size = new System.Drawing.Size(72, 23);
			this.label80.TabIndex = 136;
			this.label80.Text = "Street Name";
			// 
			// label81
			// 
			this.label81.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.label81.Location = new System.Drawing.Point(120, 64);
			this.label81.Name = "label81";
			this.label81.Size = new System.Drawing.Size(64, 23);
			this.label81.TabIndex = 135;
			this.label81.Text = "Pre-dir";
			// 
			// label82
			// 
			this.label82.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.label82.Location = new System.Drawing.Point(56, 64);
			this.label82.Name = "label82";
			this.label82.Size = new System.Drawing.Size(64, 23);
			this.label82.TabIndex = 134;
			this.label82.Text = "Street No.";
			// 
			// label89
			// 
			this.label89.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label89.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.label89.Location = new System.Drawing.Point(336, 160);
			this.label89.Name = "label89";
			this.label89.Size = new System.Drawing.Size(16, 23);
			this.label89.TabIndex = 163;
			this.label89.Text = "&&";
			// 
			// label88
			// 
			this.label88.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label88.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.label88.Location = new System.Drawing.Point(232, 160);
			this.label88.Name = "label88";
			this.label88.Size = new System.Drawing.Size(16, 23);
			this.label88.TabIndex = 162;
			this.label88.Text = "or";
			// 
			// textBoxStateB
			// 
			this.textBoxStateB.Location = new System.Drawing.Point(352, 160);
			this.textBoxStateB.MaxLength = 2;
			this.textBoxStateB.Name = "textBoxStateB";
			this.textBoxStateB.Size = new System.Drawing.Size(24, 20);
			this.textBoxStateB.TabIndex = 14;
			this.textBoxStateB.Text = "";
			// 
			// textBoxCountyB
			// 
			this.textBoxCountyB.Location = new System.Drawing.Point(264, 160);
			this.textBoxCountyB.Name = "textBoxCountyB";
			this.textBoxCountyB.Size = new System.Drawing.Size(64, 20);
			this.textBoxCountyB.TabIndex = 13;
			this.textBoxCountyB.Text = "";
			// 
			// textBoxZipB
			// 
			this.textBoxZipB.Location = new System.Drawing.Point(176, 160);
			this.textBoxZipB.Name = "textBoxZipB";
			this.textBoxZipB.Size = new System.Drawing.Size(40, 20);
			this.textBoxZipB.TabIndex = 12;
			this.textBoxZipB.Text = "";
			// 
			// textBoxApnB
			// 
			this.textBoxApnB.Location = new System.Drawing.Point(72, 160);
			this.textBoxApnB.Name = "textBoxApnB";
			this.textBoxApnB.Size = new System.Drawing.Size(88, 20);
			this.textBoxApnB.TabIndex = 11;
			this.textBoxApnB.Text = "";
			// 
			// label68
			// 
			this.label68.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.label68.Location = new System.Drawing.Point(344, 144);
			this.label68.Name = "label68";
			this.label68.Size = new System.Drawing.Size(96, 23);
			this.label68.TabIndex = 156;
			this.label68.Text = "State (i.e. CA)";
			// 
			// label69
			// 
			this.label69.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.label69.Location = new System.Drawing.Point(256, 144);
			this.label69.Name = "label69";
			this.label69.Size = new System.Drawing.Size(64, 23);
			this.label69.TabIndex = 155;
			this.label69.Text = "County";
			// 
			// label70
			// 
			this.label70.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.label70.Location = new System.Drawing.Point(168, 144);
			this.label70.Name = "label70";
			this.label70.Size = new System.Drawing.Size(64, 23);
			this.label70.TabIndex = 154;
			this.label70.Text = "ZIP";
			// 
			// label71
			// 
			this.label71.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.label71.Location = new System.Drawing.Point(64, 144);
			this.label71.Name = "label71";
			this.label71.Size = new System.Drawing.Size(64, 23);
			this.label71.TabIndex = 153;
			this.label71.Text = "APN";
			// 
			// _rbAddress
			// 
			this._rbAddress.Checked = true;
			this._rbAddress.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this._rbAddress.Location = new System.Drawing.Point(40, 40);
			this._rbAddress.Name = "_rbAddress";
			this._rbAddress.Size = new System.Drawing.Size(224, 24);
			this._rbAddress.TabIndex = 165;
			this._rbAddress.TabStop = true;
			this._rbAddress.Text = "A.   ADDRESS SEARCH (preferred):";
			// 
			// _rbParcelNr
			// 
			this._rbParcelNr.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this._rbParcelNr.Location = new System.Drawing.Point(40, 120);
			this._rbParcelNr.Name = "_rbParcelNr";
			this._rbParcelNr.Size = new System.Drawing.Size(208, 24);
			this._rbParcelNr.TabIndex = 166;
			this._rbParcelNr.Text = "B.   PARCEL NUMBER SEARCH:";
			// 
			// label1
			// 
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label1.Location = new System.Drawing.Point(8, 16);
			this.label1.Name = "label1";
			this.label1.TabIndex = 167;
			this.label1.Text = "Choose Search:";
			// 
			// PropertyItemMailingData
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(210)), ((System.Byte)(216)), ((System.Byte)(240)));
			this.ClientSize = new System.Drawing.Size(914, 240);
			this.Controls.Add(this.label1);
			this.Controls.Add(this._rbParcelNr);
			this.Controls.Add(this._rbAddress);
			this.Controls.Add(this.btnMatch);
			this.Controls.Add(this.textBoxStateA);
			this.Controls.Add(this.textBoxCountyA);
			this.Controls.Add(this.textBoxZipA);
			this.Controls.Add(this.textBoxUnitNoA);
			this.Controls.Add(this.textBoxSuffixA);
			this.Controls.Add(this.textBoxPostDirA);
			this.Controls.Add(this.textBoxStreetA);
			this.Controls.Add(this.textBoxPreDirA);
			this.Controls.Add(this.textBoxStreetNoA);
			this.Controls.Add(this.label74);
			this.Controls.Add(this.label75);
			this.Controls.Add(this.label76);
			this.Controls.Add(this.label77);
			this.Controls.Add(this.label78);
			this.Controls.Add(this.label79);
			this.Controls.Add(this.label80);
			this.Controls.Add(this.label81);
			this.Controls.Add(this.label82);
			this.Controls.Add(this.label89);
			this.Controls.Add(this.label88);
			this.Controls.Add(this.textBoxStateB);
			this.Controls.Add(this.textBoxCountyB);
			this.Controls.Add(this.textBoxZipB);
			this.Controls.Add(this.textBoxApnB);
			this.Controls.Add(this.label68);
			this.Controls.Add(this.label69);
			this.Controls.Add(this.label70);
			this.Controls.Add(this.label71);
			this.Controls.Add(this.btnSkip);
			this.Controls.Add(this.btnEndMatch);
			this.Controls.Add(this.tbSiteCity);
			this.Controls.Add(this.label94);
			this.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
			this.Name = "PropertyItemMailingData";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Property Data Search";
			this.Load += new System.EventHandler(this.PropertyItemMailingData_Load);
			this.ResumeLayout(false);

		}
		#endregion

		private void btnSkip_Click(object sender, System.EventArgs e)
		{
			Skip = true;
			EndForm();
		}

		private void btnEndMatch_Click(object sender, System.EventArgs e)
		{
			Cancel = true;
			EndForm();
		}

		private void btnMatch_Click(object sender, System.EventArgs e)
		{
			Match = true;

			if (_rbAddress.Checked)
			{
				propertyItem.Owner.SiteAddress.SiteStreetNumber = textBoxStreetNoA.Text;
				propertyItem.Owner.SiteAddress.SiteStreetPreDirectional = textBoxPreDirA.Text;
				propertyItem.Owner.SiteAddress.SiteStreetName = textBoxStreetA.Text;
				propertyItem.Owner.SiteAddress.SiteStreetPostDirectional = textBoxPostDirA.Text;
				propertyItem.Owner.SiteAddress.SiteStreetSuffix = textBoxSuffixA.Text;
				propertyItem.Owner.SiteAddress.SiteStreetUnitNumber = textBoxUnitNoA.Text;			
				propertyItem.Owner.SiteAddress.SiteCity=tbSiteCity.Text;
				propertyItem.Owner.SiteAddress.SiteState=textBoxStateA.Text;
				propertyItem.Owner.SiteAddress.SiteZIP=textBoxZipA.Text;
				propertyItem.Owner.SiteAddress.SiteCountry=textBoxCountyA.Text;

				propertyItem.Owner.SiteAddress.FullSiteStreetAddress = string.Empty;
			}
			else
			{
				if (null == propertyItem.DealProperty)
					propertyItem.DealProperty = new DealProperty();

				propertyItem.DealProperty.Apn = textBoxApnB.Text;
				propertyItem.Owner.SiteAddress.SiteState=textBoxStateB.Text;
				propertyItem.Owner.SiteAddress.SiteZIP=textBoxZipB.Text;
				propertyItem.Owner.SiteAddress.SiteCountry=textBoxCountyB.Text;
			}
			
			EndForm();
		}
		private void EndForm()
		{
			this.Close();
		}

		private void PropertyItemMailingData_Load(object sender, System.EventArgs e)
		{	
			if (null != propertyItem && null != propertyItem.Owner && null != propertyItem.Owner.SiteAddress)
			{
				if (propertyItem.Owner.SiteAddress.SiteStreetNumber != string.Empty ||
					propertyItem.Owner.SiteAddress.SiteStreetPreDirectional != string.Empty ||
					propertyItem.Owner.SiteAddress.SiteStreetName != string.Empty ||
					propertyItem.Owner.SiteAddress.SiteStreetPostDirectional != string.Empty ||
					propertyItem.Owner.SiteAddress.SiteStreetSuffix != string.Empty ||
					propertyItem.Owner.SiteAddress.SiteStreetUnitNumber != string.Empty )
				{

					textBoxStreetNoA.Text = propertyItem.Owner.SiteAddress.SiteStreetNumber;
					textBoxPreDirA.Text = propertyItem.Owner.SiteAddress.SiteStreetPreDirectional;
					textBoxStreetA.Text = propertyItem.Owner.SiteAddress.SiteStreetName;
					textBoxPostDirA.Text = propertyItem.Owner.SiteAddress.SiteStreetPostDirectional;
					textBoxSuffixA.Text = propertyItem.Owner.SiteAddress.SiteStreetSuffix;
					textBoxUnitNoA.Text = propertyItem.Owner.SiteAddress.SiteStreetUnitNumber;
				}
				else
				{
					textBoxStreetNoA.Text = string.Empty;
					textBoxPreDirA.Text = string.Empty;
					textBoxStreetA.Text = propertyItem.Owner.SiteAddress.FullSiteStreetAddress;
					textBoxPostDirA.Text = string.Empty;
					textBoxSuffixA.Text = string.Empty;
					textBoxUnitNoA.Text = string.Empty;
				}

				tbSiteCity.Text = propertyItem.Owner.SiteAddress.SiteCity;
				textBoxStateA.Text = propertyItem.Owner.SiteAddress.SiteState;					
				textBoxZipA.Text = propertyItem.Owner.SiteAddress.SiteZIP;
				textBoxCountyA.Text = propertyItem.Owner.SiteAddress.SiteCountry;
			}																										
		}

		private void OnrbAddressCheckedChanged(object sender, EventArgs e)
		{
			if (_rbAddress.Checked)
			{
				textBoxStreetNoA.Enabled = _rbAddress.Checked;
				textBoxPreDirA.Enabled = _rbAddress.Checked;
				textBoxStreetA.Enabled = _rbAddress.Checked;
				textBoxPostDirA.Enabled = _rbAddress.Checked;
				textBoxSuffixA.Enabled = _rbAddress.Checked;
				textBoxUnitNoA.Enabled = _rbAddress.Checked;
				tbSiteCity.Enabled = _rbAddress.Checked;
				textBoxZipA.Enabled = _rbAddress.Checked;
				textBoxCountyA.Enabled = _rbAddress.Checked;
				textBoxStateA.Enabled = _rbAddress.Checked;

				textBoxApnB.Enabled = !_rbAddress.Checked;
				textBoxZipB.Enabled = !_rbAddress.Checked;
				textBoxCountyB.Enabled = !_rbAddress.Checked;
				textBoxStateB.Enabled = !_rbAddress.Checked;

			}
			else
			{
				textBoxStreetNoA.Enabled = _rbAddress.Checked;
				textBoxPreDirA.Enabled = _rbAddress.Checked;
				textBoxStreetA.Enabled = _rbAddress.Checked;
				textBoxPostDirA.Enabled = _rbAddress.Checked;
				textBoxSuffixA.Enabled = _rbAddress.Checked;
				textBoxUnitNoA.Enabled = _rbAddress.Checked;
				tbSiteCity.Enabled = _rbAddress.Checked;
				textBoxZipA.Enabled = _rbAddress.Checked;
				textBoxCountyA.Enabled = _rbAddress.Checked;
				textBoxStateA.Enabled = _rbAddress.Checked;

				textBoxApnB.Enabled = !_rbAddress.Checked;
				textBoxZipB.Enabled = !_rbAddress.Checked;
				textBoxCountyB.Enabled = !_rbAddress.Checked;
				textBoxStateB.Enabled = !_rbAddress.Checked;
			}
		}

		public SearchType TypeOfSearch
		{
			get 
			{
				if (_rbAddress.Checked)
					return SearchType.AddressSearch;
				else
					return SearchType.ParcelNumberSearch;
			}
		}

		public PropertyItem CurrentPropertyItem
		{
			get {return this.propertyItem;}
			set {this.propertyItem = value;}
		}
	}
}
