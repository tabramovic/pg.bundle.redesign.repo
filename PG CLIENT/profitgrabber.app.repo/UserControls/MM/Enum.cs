using System;

namespace DealMaker
{
	/// <summary>
	/// Summary description for Enum.
	/// </summary>
	public enum SearchType
	{
		AddressSearch = 1, 
		ParcelNumberSearch,
	}
}
