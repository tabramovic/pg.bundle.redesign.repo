//#define TEST
#define DO_NOT_USE_ZIP_FILTER
using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.IO;
using DTD.Response;
using DTD.Request;
using System.Xml;
using System.Xml.Serialization;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Diagnostics;

namespace DealMaker
{
	/// <summary>
	/// Summary description for MM_Matching_Module.
	/// </summary>
	public class MM_Matching_Module : System.Windows.Forms.UserControl
	{
		public delegate void SetButtonsVisibleDelegate(bool visible);
		public SetButtonsVisibleDelegate SetSecButtonsVisibleFromThread;

		public delegate void SetButtonsEnableDelegate(bool enabled);
		public SetButtonsEnableDelegate SetButtonsEnabledFromThread;

		int _notMatchedGroupId = -1;
		int _notMatchedGroup_NotMailedId = -1;
		int _notMatchedGroup_MailedWoNameId = -1;
		
		bool _stepB_Enabled = false;
		bool _updateAutomaticCounters = true;

		int _matchedManuallyCounter = 0;
		int _willUseHomeownerCoutner = 0;
		int _deletedRecordsCounter = 0;

        bool _matchContactsThatAlreadyHaveNamesAndMailingAddressesProp => _matchContactsThatAlreadyHaveNamesAndMailingAddresses.Checked; 
        

        #region variables


        private NHibernate.ISession session = null;
		private Node selectedNode =null;
		private int nodeCount=0;
		private int totalProc=0;
		private int success=0;
		int _alreadyWithNames = 0;
		int _alreadyWithMailingAddress = 0;
		int _alreadyWithPhoneNumber = 0;
		private int faliure=0;
		//private DealMaker.CustomizedTreeViewUserControl groupsTreeView;
		//private DTD.Response.RESPONSE_GROUP response = null;
		private ServiceWorker serviceWorker;
		private ArrayList unmatchedItems;
		private System.Windows.Forms.Panel panelMainContainer;
		private System.Windows.Forms.TabControl matchingModuleTabControl;
		private System.Windows.Forms.TabPage stepBTabPage;
		private System.Windows.Forms.Label label15;
		private System.Windows.Forms.Label lblUnsucessful;
		private System.Windows.Forms.Label lblSuccessful;
		private System.Windows.Forms.Label lblTotalProcessed;
		private System.Windows.Forms.Label lblNumberOfRecords;
		private System.Windows.Forms.Label label14;
		private System.Windows.Forms.Label label13;
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.ListView listViewSuccess;
		private System.Windows.Forms.ColumnHeader columnHeaderOwner;
		private System.Windows.Forms.ColumnHeader columnHeaderSreetName;
		private System.Windows.Forms.ColumnHeader columnHeaderCity;
		private System.Windows.Forms.ColumnHeader columnHeaderState;
		private System.Windows.Forms.ColumnHeader columnHeaderZip;
		private System.Windows.Forms.ColumnHeader columnHeaderCounty;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Button _TagAll;
		private System.Windows.Forms.Button _UnTagAll;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.ListView listViewFaliure;
		private System.Windows.Forms.ColumnHeader columnHeader9;
		private System.Windows.Forms.ColumnHeader columnHeader1;
		private System.Windows.Forms.ColumnHeader columnHeader2;
		private System.Windows.Forms.ColumnHeader columnHeader3;
		private System.Windows.Forms.ColumnHeader columnHeader4;
		private System.Windows.Forms.ColumnHeader columnHeader5;
		private System.Windows.Forms.ColumnHeader columnHeader6;
		private System.Windows.Forms.ColumnHeader columnHeader7;
		private System.Windows.Forms.ColumnHeader ID;
		private System.Windows.Forms.Button btnStartMatching;
		private System.Windows.Forms.Button btnStartMatchingManually;
		private System.Windows.Forms.Button btnDeleteUnmatched;
		private System.Windows.Forms.Button btnUseCurrentResident;
		private System.Windows.Forms.Button bFirstName;
		private System.Windows.Forms.Label label16;
		private System.Windows.Forms.Label label17;
		private System.Windows.Forms.Label label18;
		private System.Windows.Forms.ColumnHeader columnHeader10;
		private System.Windows.Forms.ColumnHeader columnHeader11;
		private System.Windows.Forms.ColumnHeader columnHeader12;
		private System.Windows.Forms.ColumnHeader columnHeader13;
		private System.Windows.Forms.ColumnHeader columnHeader14;
		private System.Windows.Forms.ColumnHeader columnHeader15;
		private System.Windows.Forms.ColumnHeader columnHeader16;
		private System.Windows.Forms.ColumnHeader columnHeader18;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.Label label19;
		private System.Windows.Forms.Label label20;
		private System.Windows.Forms.Label label21;
		private System.Windows.Forms.Label label23;
		private System.Windows.Forms.Button bBrowse;
		private System.Windows.Forms.Label l_GroupName;
		private System.Windows.Forms.Label _alreadyWithNamesLabel;
		private System.Windows.Forms.TabPage stepATabPage;
		private System.Windows.Forms.Label _alreadyWithNamesValue;
		private System.Windows.Forms.ListView lv_StepB;
		private System.Windows.Forms.Button _GoToStepB;
		private System.Windows.Forms.Button bLeaveBlankStepB;
		private System.Windows.Forms.Button bDeleteTaggedStepB;
		private System.Windows.Forms.Button TagAllStep2;
		private System.Windows.Forms.Button UnTagAllStep2;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.Label _DeletedRecords;
		private System.Windows.Forms.Label _WillUseHomeowner;
		private System.Windows.Forms.Label _MatchedManually;
		private System.Windows.Forms.Label label26;
		private System.Windows.Forms.Label label27;
		private System.Windows.Forms.Label label28;
		private System.Windows.Forms.Label label29;
		private System.Windows.Forms.CheckBox _overwriteMailingAddress;
		private System.Windows.Forms.CheckBox _overwriteExistingNames;
		private System.Windows.Forms.ColumnHeader columnHeader21;
		private System.Windows.Forms.ColumnHeader columnHeader8;
		private System.Windows.Forms.ColumnHeader columnHeader20;
		private System.Windows.Forms.ColumnHeader columnHeader19;
		private System.Windows.Forms.ColumnHeader columnHeader22;
		private System.Windows.Forms.ColumnHeader columnHeader23;
		private System.Windows.Forms.ColumnHeader columnHeader17;
		private System.Windows.Forms.ColumnHeader columnHeader24;
		private System.Windows.Forms.Label _alreadyWithMailingAddrLabel;
		private System.Windows.Forms.Label _alreadyWithMailingAddrValue;
		private System.Windows.Forms.CheckBox _overridePhoneNumber;
		private System.Windows.Forms.Label _alreadyWithPhoneNumberLabel;
		private System.Windows.Forms.Label _alreadyWithPhoneNumberValue;
		private System.Windows.Forms.CheckBox _doNotProcessNoMails;
		private System.Windows.Forms.CheckBox _useAPN;
        private ColumnHeader columnHeader25;
        private ColumnHeader columnHeader26;
        private Button _selectGroupForForcedSeparation;
        private Label _selectedGroupName;
        private CheckBox _forceSeparation;
        private CheckBox _matchContactsThatAlreadyHaveNamesAndMailingAddresses;
        private Label label22;
        private Button _removeMultipleFirstNames;
        private Label label24;

        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.Container components = null;
		#endregion
		#region	MM_Matching_Module
		public MM_Matching_Module()
		{
			InitializeComponent();

			_doNotProcessNoMails.Click += new EventHandler(On_DoNotProcessNoMails_Click);
			_useAPN.Click += new EventHandler(On_UseAPN_Click);

			_TagAll.Click += new EventHandler(OnTagAll_Click);
			_UnTagAll.Click += new EventHandler(OnUnTagAll_Click);
			
			bFirstName.Click += new EventHandler(OnFirstName_Click);
            _removeMultipleFirstNames.Click += _removeMultipleFirstNames_Click;
            //bDeleteTagged.Click += new EventHandler(OnDeleteTagged_Click);
            bBrowse.Click += new EventHandler(OnSelectGroup_Click);

            matchingModuleTabControl.Selected += MatchingModuleTabControl_Selected;


            System.Threading.Thread.CurrentThread.CurrentCulture = 	System.Globalization.CultureInfo.CreateSpecificCulture("EN-US");
			
			CustomizedTreeViewUserControl.REFRESH_GROUPS += new DealMaker.CustomizedTreeViewUserControl.RefreshHandler(RefreshGroups);
			OneTwoThreeSold.REFRESH_GROUPS_BY_123SOLD += new DealMaker.OneTwoThreeSold.NewGroupAddedBy123Sold(RefreshGroupsBy123Sold);

			serviceWorker = ServiceWorker.GetInstance;
           
            serviceWorker.FireServiceFinishedEvent += new DealMaker.ServiceWorker.ServiceFinishedHandler(serviceWorker_FireServiceFinishedEvent);
            serviceWorker.FireQueryPositivelySolvedEvent += new DealMaker.ServiceWorker.QueryPositivelySolvedHandler(serviceWorker_FireQueryPositivelySolvedEvent);
            serviceWorker.FireQueryNegativelySolvedEvent += new DealMaker.ServiceWorker.QueryNegativelySolvedHandler(serviceWorker_FireQueryNegativelySolvedEvent);
            serviceWorker.FireServiceErrorEvent += new DealMaker.ServiceWorker.ServiceErrorHandler(serviceWorker_FireServiceErrorEvent);

            serviceWorker.SubscribedEHs = true;
            
			setSecondaryBtnGroupVisible(false);
			unmatchedItems = new ArrayList();
			SetSecButtonsVisibleFromThread = new SetButtonsVisibleDelegate(setSecondaryBtnGroupVisible);
			SetButtonsEnabledFromThread = new SetButtonsEnableDelegate(setCommandsEnabled);

			btnStartMatching.Click += new EventHandler(btnStartMatching_Click);
			btnStartMatchingManually.Click += new EventHandler(btnStartMatchingManually_Click);
			btnUseCurrentResident.Click += new EventHandler(OnDearHomeOwner_Click);
			btnDeleteUnmatched.Click += new EventHandler(btnDeleteUnmatched_Click);


			_overwriteExistingNames.Click += new EventHandler(On_OverwriteExistingNames);
			_overwriteMailingAddress.Click += new EventHandler(On_OverwriteMailingAddress_Click);
			_overridePhoneNumber.Click += new EventHandler(On_OverridePhoneNumber_Click);

            _forceSeparation.Click += new EventHandler(On_ForceSeparation_Click);
            _selectGroupForForcedSeparation.Click += new EventHandler(On_SelectGroupForForcedSeparation_Click);

            this.Leave += MM_Matching_Module_Leave;
		}

        private void MatchingModuleTabControl_Selected(object sender, TabControlEventArgs e)
        {
            if (e.TabPage.Name.Equals(nameof(stepBTabPage)))
                CheckSuccItemsOnStep2(true);
        }
        #endregion

        private void MM_Matching_Module_Leave(object sender, EventArgs e)
        {
            RemoveEventHandlers();
        }

        void RemoveEventHandlers()
        {
            _doNotProcessNoMails.Click -= new EventHandler(On_DoNotProcessNoMails_Click);
            _useAPN.Click -= new EventHandler(On_UseAPN_Click);

            _TagAll.Click -= new EventHandler(OnTagAll_Click);
            _UnTagAll.Click += new EventHandler(OnUnTagAll_Click);

            bFirstName.Click -= new EventHandler(OnFirstName_Click);
            _removeMultipleFirstNames.Click -= _removeMultipleFirstNames_Click;
            //bDeleteTagged.Click += new EventHandler(OnDeleteTagged_Click);
            bBrowse.Click -= new EventHandler(OnSelectGroup_Click);
            
            CustomizedTreeViewUserControl.REFRESH_GROUPS -= new DealMaker.CustomizedTreeViewUserControl.RefreshHandler(RefreshGroups);
            OneTwoThreeSold.REFRESH_GROUPS_BY_123SOLD -= new DealMaker.OneTwoThreeSold.NewGroupAddedBy123Sold(RefreshGroupsBy123Sold);
            
            serviceWorker.FireServiceFinishedEvent -= new DealMaker.ServiceWorker.ServiceFinishedHandler(serviceWorker_FireServiceFinishedEvent);
            serviceWorker.FireQueryPositivelySolvedEvent -= new DealMaker.ServiceWorker.QueryPositivelySolvedHandler(serviceWorker_FireQueryPositivelySolvedEvent);
            serviceWorker.FireQueryNegativelySolvedEvent -= new DealMaker.ServiceWorker.QueryNegativelySolvedHandler(serviceWorker_FireQueryNegativelySolvedEvent);
            serviceWorker.FireServiceErrorEvent -= new DealMaker.ServiceWorker.ServiceErrorHandler(serviceWorker_FireServiceErrorEvent);
                                                            
            btnStartMatching.Click -= new EventHandler(btnStartMatching_Click);
            btnStartMatchingManually.Click -= new EventHandler(btnStartMatchingManually_Click);
            btnUseCurrentResident.Click -= new EventHandler(OnDearHomeOwner_Click);
            btnDeleteUnmatched.Click -= new EventHandler(btnDeleteUnmatched_Click);

            _overwriteExistingNames.Click -= new EventHandler(On_OverwriteExistingNames);
            _overwriteMailingAddress.Click -= new EventHandler(On_OverwriteMailingAddress_Click);
            _overridePhoneNumber.Click -= new EventHandler(On_OverridePhoneNumber_Click);

            _forceSeparation.Click -= new EventHandler(On_ForceSeparation_Click);
            _selectGroupForForcedSeparation.Click -= new EventHandler(On_SelectGroupForForcedSeparation_Click);

            this.Leave -= MM_Matching_Module_Leave;

            Console.WriteLine("MM removed event handlers (OnLeave)");
        }

      

        #region	MM_Matching_Module_Load
        /// <summary>
        /// IMPORTANT. this proc sets data after treeview is loaded.
        /// It is not possible to set the active node if treeview is not buildt ;)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MM_Matching_Module_Load(object sender, System.EventArgs e)
		{	
			lblNumberOfRecords.Text = "0";
			lblSuccessful.Text="0";
			lblUnsucessful.Text = "0";
			lblTotalProcessed.Text = "0";

			
			if (-1 < Globals.selectedNodeId)
			{
				try
				{
					//int myNode = setnode(groupsTreeView.tv_Nodes.Nodes, Globals.selectedNodeId);//, nodeID);
					//if (-1 < myNode)
					//	selectedNode = groupsTreeView.SelectedTreeNode.Tag as Node;
				}
				catch 
				{
					Globals.selectedNodeId = 1;
				}
			}
		}
		#endregion
		#region setnode
		/// <summary>
		/// recursive walk through treeview to find a node
		/// and expand it
		/// TODO: move to applicationHelper
		/// </summary>
		/// <param name="nodes"></param>
		/// <param name="searchNode"></param>
		/// <returns>If node is found, returns Tomislav's nodeID
		/// else retuns -1</returns>
		private int setnode(TreeNodeCollection nodes, int searchNode)
		{	  
			int i = -1;
			foreach (TreeNode x in nodes)
			{
				Node z = x.Tag as Node;
				if (z.nodeId == searchNode)
				{
					//groupsTreeView.tv_Nodes.SelectedNode = x;

					return z.nodeId;
				}
				 
				i=  setnode(x.Nodes, searchNode);
				if (i > -1)
					return i;
			} 
			return -1;
		}
		#endregion
		#region AfterSelectEventHandler
		private void AfterSelectEventHandler(object sender, TreeViewEventArgs e)
		{
			if (btnStartMatching.Visible)
			{
				selectedNode = (Node)e.Node.Tag;
				prepareForm();
			}

		}
		#endregion
		#region prepareForm
		private void prepareForm()
		{	
			setSecondaryBtnGroupVisible(false);

			nodeCount=0;
			totalProc=0;
			success=0;
			_alreadyWithNames = 0;
			_alreadyWithMailingAddress = 0;
			_alreadyWithPhoneNumber = 0;
			faliure=0;
			
			nodeCount = ApplicationHelper.GetPropertyCountInNode(selectedNode.nodeId);

			lblNumberOfRecords.Text = nodeCount.ToString();
			lblSuccessful.Text="0";
			lblUnsucessful.Text = "0";
			lblTotalProcessed.Text = "0";
			_alreadyWithNamesValue.Text = _alreadyWithNames.ToString();
			_alreadyWithMailingAddrValue.Text = _alreadyWithMailingAddress.ToString();
			_alreadyWithPhoneNumberValue.Text = _alreadyWithPhoneNumber.ToString();

			

			serviceWorker.ClearPropertyItems();
			unmatchedItems.Clear();
			listViewFaliure.Items.Clear();
			listViewSuccess.Items.Clear();
			lv_StepB.Items.Clear();
		}
		#endregion
		#region Dispose
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}
		#endregion
		#region . deserialize .
		private DTD.Response.RESPONSE_GROUP DeserializeXmlFile(string path)
		{
			DTD.Response.RESPONSE_GROUP x ;
			FileStream fs = new FileStream(path, FileMode.Open);
				   
			XmlSerializer serializer = new XmlSerializer(typeof(DTD.Response.RESPONSE_GROUP));
			x = (DTD.Response.RESPONSE_GROUP)serializer.Deserialize(fs);;
			fs.Close();
			return x;
		}
		#endregion
		#region . dumpObject to file .
		private void dumpObject(object theObject, string filename)
		{
            /*
			System.Xml.Serialization.XmlSerializer serializer
				= new System.Xml.Serialization.XmlSerializer(theObject.GetType());

			// IMPORTANT  -  we do not need a namespace!
			System.Xml.Serialization.XmlSerializerNamespaces nmspace = new XmlSerializerNamespaces();
			nmspace.Add("","");
			// IMPORTANT  -  we do not need a namespace!
				
			System.Xml.XmlTextWriter writer = new XmlTextWriter(filename, System.Text.UTF8Encoding.UTF8);
			writer.Formatting = System.Xml.Formatting.Indented;
			writer.WriteStartDocument();
			writer.WriteDocType("REQUEST_GROUP", null, "C2DRequestv2.0.PropertyProfileComps",null);
			serializer.Serialize(writer, theObject,nmspace);
			writer.Flush();
			writer.Close();
            */
		}
		#endregion
		#region . num dat parsing .
		private string getMoney(string sMoney)
		{
			try
			{
				sMoney = Convert.ToDouble(sMoney).ToString("C");
			}
			catch
			{
			}

			return sMoney;
		}

		private string getDecimal(string sDecimal)
		{
			try
			{
				sDecimal = Convert.ToDouble(sDecimal).ToString("N");
			}
			catch
			{
			}

			return sDecimal;
		}

		private string getDate(string sDate)
		{
			if (sDate.Length==8)
			{
				try
				{
					DateTime d = new DateTime
						(
						(int.Parse(sDate.Substring(0,4))),
						(int.Parse(sDate.Substring(4,2))),
						(int.Parse(sDate.Substring(6,2)))
						);

					sDate = d.ToShortDateString();
				}
				catch
				{
				}
			}

			return sDate;
		}
		private string getSecureString(object str)
		{
			string SecureString = "";
			try
			{
				SecureString = str.ToString();
			}
			catch
			{}
			return SecureString;
		}

		#endregion
		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.panelMainContainer = new System.Windows.Forms.Panel();
            this.matchingModuleTabControl = new System.Windows.Forms.TabControl();
            this.stepATabPage = new System.Windows.Forms.TabPage();
            this._matchContactsThatAlreadyHaveNamesAndMailingAddresses = new System.Windows.Forms.CheckBox();
            this.label22 = new System.Windows.Forms.Label();
            this._useAPN = new System.Windows.Forms.CheckBox();
            this._doNotProcessNoMails = new System.Windows.Forms.CheckBox();
            this._overridePhoneNumber = new System.Windows.Forms.CheckBox();
            this._overwriteExistingNames = new System.Windows.Forms.CheckBox();
            this._overwriteMailingAddress = new System.Windows.Forms.CheckBox();
            this.l_GroupName = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this._DeletedRecords = new System.Windows.Forms.Label();
            this._WillUseHomeowner = new System.Windows.Forms.Label();
            this._MatchedManually = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this._alreadyWithPhoneNumberValue = new System.Windows.Forms.Label();
            this._alreadyWithPhoneNumberLabel = new System.Windows.Forms.Label();
            this._alreadyWithMailingAddrValue = new System.Windows.Forms.Label();
            this._alreadyWithMailingAddrLabel = new System.Windows.Forms.Label();
            this.lblUnsucessful = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this._alreadyWithNamesValue = new System.Windows.Forms.Label();
            this._alreadyWithNamesLabel = new System.Windows.Forms.Label();
            this.lblSuccessful = new System.Windows.Forms.Label();
            this.lblTotalProcessed = new System.Windows.Forms.Label();
            this.lblNumberOfRecords = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this._GoToStepB = new System.Windows.Forms.Button();
            this.bBrowse = new System.Windows.Forms.Button();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.listViewSuccess = new System.Windows.Forms.ListView();
            this.columnHeaderOwner = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderSreetName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderCity = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderState = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderZip = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderCounty = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader8 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader20 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader21 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader17 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.label6 = new System.Windows.Forms.Label();
            this._TagAll = new System.Windows.Forms.Button();
            this._UnTagAll = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.listViewFaliure = new System.Windows.Forms.ListView();
            this.columnHeader9 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ID = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.btnStartMatching = new System.Windows.Forms.Button();
            this.btnStartMatchingManually = new System.Windows.Forms.Button();
            this.btnDeleteUnmatched = new System.Windows.Forms.Button();
            this.btnUseCurrentResident = new System.Windows.Forms.Button();
            this.stepBTabPage = new System.Windows.Forms.TabPage();
            this.label24 = new System.Windows.Forms.Label();
            this._removeMultipleFirstNames = new System.Windows.Forms.Button();
            this._selectedGroupName = new System.Windows.Forms.Label();
            this._forceSeparation = new System.Windows.Forms.CheckBox();
            this._selectGroupForForcedSeparation = new System.Windows.Forms.Button();
            this.bDeleteTaggedStepB = new System.Windows.Forms.Button();
            this.bLeaveBlankStepB = new System.Windows.Forms.Button();
            this.bFirstName = new System.Windows.Forms.Button();
            this.label23 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.TagAllStep2 = new System.Windows.Forms.Button();
            this.UnTagAllStep2 = new System.Windows.Forms.Button();
            this.lv_StepB = new System.Windows.Forms.ListView();
            this.columnHeader10 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader11 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader18 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader12 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader13 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader14 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader15 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader16 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader19 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader25 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader26 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader22 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader23 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader24 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.label18 = new System.Windows.Forms.Label();
            this.panelMainContainer.SuspendLayout();
            this.matchingModuleTabControl.SuspendLayout();
            this.stepATabPage.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.stepBTabPage.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelMainContainer
            // 
            this.panelMainContainer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.panelMainContainer.Controls.Add(this.matchingModuleTabControl);
            this.panelMainContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelMainContainer.Location = new System.Drawing.Point(0, 0);
            this.panelMainContainer.Name = "panelMainContainer";
            this.panelMainContainer.Size = new System.Drawing.Size(918, 704);
            this.panelMainContainer.TabIndex = 0;
            // 
            // matchingModuleTabControl
            // 
            this.matchingModuleTabControl.Controls.Add(this.stepATabPage);
            this.matchingModuleTabControl.Controls.Add(this.stepBTabPage);
            this.matchingModuleTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.matchingModuleTabControl.Location = new System.Drawing.Point(0, 0);
            this.matchingModuleTabControl.Name = "matchingModuleTabControl";
            this.matchingModuleTabControl.SelectedIndex = 0;
            this.matchingModuleTabControl.Size = new System.Drawing.Size(918, 704);
            this.matchingModuleTabControl.TabIndex = 35;
            // 
            // stepATabPage
            // 
            this.stepATabPage.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.stepATabPage.Controls.Add(this._matchContactsThatAlreadyHaveNamesAndMailingAddresses);
            this.stepATabPage.Controls.Add(this.label22);
            this.stepATabPage.Controls.Add(this._useAPN);
            this.stepATabPage.Controls.Add(this._doNotProcessNoMails);
            this.stepATabPage.Controls.Add(this._overridePhoneNumber);
            this.stepATabPage.Controls.Add(this._overwriteExistingNames);
            this.stepATabPage.Controls.Add(this._overwriteMailingAddress);
            this.stepATabPage.Controls.Add(this.l_GroupName);
            this.stepATabPage.Controls.Add(this.groupBox2);
            this.stepATabPage.Controls.Add(this.groupBox1);
            this.stepATabPage.Controls.Add(this._GoToStepB);
            this.stepATabPage.Controls.Add(this.bBrowse);
            this.stepATabPage.Controls.Add(this.label17);
            this.stepATabPage.Controls.Add(this.label16);
            this.stepATabPage.Controls.Add(this.label15);
            this.stepATabPage.Controls.Add(this.label14);
            this.stepATabPage.Controls.Add(this.label13);
            this.stepATabPage.Controls.Add(this.label12);
            this.stepATabPage.Controls.Add(this.label11);
            this.stepATabPage.Controls.Add(this.label10);
            this.stepATabPage.Controls.Add(this.listViewSuccess);
            this.stepATabPage.Controls.Add(this.label6);
            this.stepATabPage.Controls.Add(this._TagAll);
            this.stepATabPage.Controls.Add(this._UnTagAll);
            this.stepATabPage.Controls.Add(this.label7);
            this.stepATabPage.Controls.Add(this.listViewFaliure);
            this.stepATabPage.Controls.Add(this.btnStartMatching);
            this.stepATabPage.Controls.Add(this.btnStartMatchingManually);
            this.stepATabPage.Controls.Add(this.btnDeleteUnmatched);
            this.stepATabPage.Controls.Add(this.btnUseCurrentResident);
            this.stepATabPage.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.stepATabPage.Location = new System.Drawing.Point(4, 22);
            this.stepATabPage.Name = "stepATabPage";
            this.stepATabPage.Size = new System.Drawing.Size(910, 678);
            this.stepATabPage.TabIndex = 0;
            this.stepATabPage.Text = "STEP A";
            // 
            // _matchContactsThatAlreadyHaveNamesAndMailingAddresses
            // 
            this._matchContactsThatAlreadyHaveNamesAndMailingAddresses.AutoSize = true;
            this._matchContactsThatAlreadyHaveNamesAndMailingAddresses.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._matchContactsThatAlreadyHaveNamesAndMailingAddresses.Location = new System.Drawing.Point(356, 50);
            this._matchContactsThatAlreadyHaveNamesAndMailingAddresses.Name = "_matchContactsThatAlreadyHaveNamesAndMailingAddresses";
            this._matchContactsThatAlreadyHaveNamesAndMailingAddresses.Size = new System.Drawing.Size(50, 18);
            this._matchContactsThatAlreadyHaveNamesAndMailingAddresses.TabIndex = 82;
            this._matchContactsThatAlreadyHaveNamesAndMailingAddresses.Text = "Yes";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label22.Location = new System.Drawing.Point(16, 52);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(325, 13);
            this.label22.TabIndex = 81;
            this.label22.Text = "2. Match contacts that already have names and mailing addresses?";
            // 
            // _useAPN
            // 
            this._useAPN.AutoSize = true;
            this._useAPN.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._useAPN.Location = new System.Drawing.Point(356, 122);
            this._useAPN.Name = "_useAPN";
            this._useAPN.Size = new System.Drawing.Size(129, 18);
            this._useAPN.TabIndex = 80;
            this._useAPN.Text = "Use APN if available";
            // 
            // _doNotProcessNoMails
            // 
            this._doNotProcessNoMails.AutoSize = true;
            this._doNotProcessNoMails.Checked = true;
            this._doNotProcessNoMails.CheckState = System.Windows.Forms.CheckState.Checked;
            this._doNotProcessNoMails.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._doNotProcessNoMails.Location = new System.Drawing.Point(356, 98);
            this._doNotProcessNoMails.Name = "_doNotProcessNoMails";
            this._doNotProcessNoMails.Size = new System.Drawing.Size(194, 18);
            this._doNotProcessNoMails.TabIndex = 79;
            this._doNotProcessNoMails.Text = "Do Not Process \"No Mail\" records";
            // 
            // _overridePhoneNumber
            // 
            this._overridePhoneNumber.AutoSize = true;
            this._overridePhoneNumber.Checked = true;
            this._overridePhoneNumber.CheckState = System.Windows.Forms.CheckState.Checked;
            this._overridePhoneNumber.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._overridePhoneNumber.Location = new System.Drawing.Point(774, 74);
            this._overridePhoneNumber.Name = "_overridePhoneNumber";
            this._overridePhoneNumber.Size = new System.Drawing.Size(103, 18);
            this._overridePhoneNumber.TabIndex = 78;
            this._overridePhoneNumber.Text = "Phone Number";
            // 
            // _overwriteExistingNames
            // 
            this._overwriteExistingNames.AutoSize = true;
            this._overwriteExistingNames.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._overwriteExistingNames.Location = new System.Drawing.Point(356, 74);
            this._overwriteExistingNames.Name = "_overwriteExistingNames";
            this._overwriteExistingNames.Size = new System.Drawing.Size(122, 18);
            this._overwriteExistingNames.TabIndex = 77;
            this._overwriteExistingNames.Text = "Any existing names";
            // 
            // _overwriteMailingAddress
            // 
            this._overwriteMailingAddress.AutoSize = true;
            this._overwriteMailingAddress.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._overwriteMailingAddress.Location = new System.Drawing.Point(538, 74);
            this._overwriteMailingAddress.Name = "_overwriteMailingAddress";
            this._overwriteMailingAddress.Size = new System.Drawing.Size(174, 18);
            this._overwriteMailingAddress.TabIndex = 76;
            this._overwriteMailingAddress.Text = "Any existing mailing addresses";
            // 
            // l_GroupName
            // 
            this.l_GroupName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l_GroupName.Location = new System.Drawing.Point(552, 23);
            this.l_GroupName.Name = "l_GroupName";
            this.l_GroupName.Size = new System.Drawing.Size(350, 23);
            this.l_GroupName.TabIndex = 66;
            this.l_GroupName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this._DeletedRecords);
            this.groupBox2.Controls.Add(this._WillUseHomeowner);
            this.groupBox2.Controls.Add(this._MatchedManually);
            this.groupBox2.Controls.Add(this.label26);
            this.groupBox2.Controls.Add(this.label27);
            this.groupBox2.Controls.Add(this.label28);
            this.groupBox2.Controls.Add(this.label29);
            this.groupBox2.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.groupBox2.Location = new System.Drawing.Point(664, 405);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(241, 117);
            this.groupBox2.TabIndex = 71;
            this.groupBox2.TabStop = false;
            // 
            // _DeletedRecords
            // 
            this._DeletedRecords.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this._DeletedRecords.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this._DeletedRecords.Location = new System.Drawing.Point(176, 88);
            this._DeletedRecords.Name = "_DeletedRecords";
            this._DeletedRecords.Size = new System.Drawing.Size(56, 24);
            this._DeletedRecords.TabIndex = 75;
            this._DeletedRecords.Text = "0";
            this._DeletedRecords.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // _WillUseHomeowner
            // 
            this._WillUseHomeowner.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this._WillUseHomeowner.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this._WillUseHomeowner.Location = new System.Drawing.Point(176, 64);
            this._WillUseHomeowner.Name = "_WillUseHomeowner";
            this._WillUseHomeowner.Size = new System.Drawing.Size(56, 24);
            this._WillUseHomeowner.TabIndex = 74;
            this._WillUseHomeowner.Text = "0";
            this._WillUseHomeowner.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // _MatchedManually
            // 
            this._MatchedManually.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this._MatchedManually.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this._MatchedManually.Location = new System.Drawing.Point(176, 40);
            this._MatchedManually.Name = "_MatchedManually";
            this._MatchedManually.Size = new System.Drawing.Size(56, 24);
            this._MatchedManually.TabIndex = 73;
            this._MatchedManually.Text = "0";
            this._MatchedManually.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label26
            // 
            this.label26.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.label26.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label26.Location = new System.Drawing.Point(8, 40);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(134, 16);
            this.label26.TabIndex = 70;
            this.label26.Text = "Matched Manually";
            // 
            // label27
            // 
            this.label27.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.label27.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label27.Location = new System.Drawing.Point(8, 64);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(134, 16);
            this.label27.TabIndex = 71;
            this.label27.Text = "Will use \"Homeowner\"";
            // 
            // label28
            // 
            this.label28.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.label28.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label28.Location = new System.Drawing.Point(8, 88);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(134, 16);
            this.label28.TabIndex = 72;
            this.label28.Text = "Deleted Records";
            // 
            // label29
            // 
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(1, 8);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(100, 23);
            this.label29.TabIndex = 70;
            this.label29.Text = "Status";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this._alreadyWithPhoneNumberValue);
            this.groupBox1.Controls.Add(this._alreadyWithPhoneNumberLabel);
            this.groupBox1.Controls.Add(this._alreadyWithMailingAddrValue);
            this.groupBox1.Controls.Add(this._alreadyWithMailingAddrLabel);
            this.groupBox1.Controls.Add(this.lblUnsucessful);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this._alreadyWithNamesValue);
            this.groupBox1.Controls.Add(this._alreadyWithNamesLabel);
            this.groupBox1.Controls.Add(this.lblSuccessful);
            this.groupBox1.Controls.Add(this.lblTotalProcessed);
            this.groupBox1.Controls.Add(this.lblNumberOfRecords);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(664, 150);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(241, 213);
            this.groupBox1.TabIndex = 70;
            this.groupBox1.TabStop = false;
            // 
            // _alreadyWithPhoneNumberValue
            // 
            this._alreadyWithPhoneNumberValue.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this._alreadyWithPhoneNumberValue.Enabled = false;
            this._alreadyWithPhoneNumberValue.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this._alreadyWithPhoneNumberValue.Location = new System.Drawing.Point(176, 184);
            this._alreadyWithPhoneNumberValue.Name = "_alreadyWithPhoneNumberValue";
            this._alreadyWithPhoneNumberValue.Size = new System.Drawing.Size(56, 24);
            this._alreadyWithPhoneNumberValue.TabIndex = 73;
            this._alreadyWithPhoneNumberValue.Text = "0";
            this._alreadyWithPhoneNumberValue.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // _alreadyWithPhoneNumberLabel
            // 
            this._alreadyWithPhoneNumberLabel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this._alreadyWithPhoneNumberLabel.Enabled = false;
            this._alreadyWithPhoneNumberLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this._alreadyWithPhoneNumberLabel.Location = new System.Drawing.Point(8, 184);
            this._alreadyWithPhoneNumberLabel.Name = "_alreadyWithPhoneNumberLabel";
            this._alreadyWithPhoneNumberLabel.Size = new System.Drawing.Size(160, 16);
            this._alreadyWithPhoneNumberLabel.TabIndex = 72;
            this._alreadyWithPhoneNumberLabel.Text = "Already With Phone Number";
            // 
            // _alreadyWithMailingAddrValue
            // 
            this._alreadyWithMailingAddrValue.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this._alreadyWithMailingAddrValue.Enabled = false;
            this._alreadyWithMailingAddrValue.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this._alreadyWithMailingAddrValue.Location = new System.Drawing.Point(176, 160);
            this._alreadyWithMailingAddrValue.Name = "_alreadyWithMailingAddrValue";
            this._alreadyWithMailingAddrValue.Size = new System.Drawing.Size(56, 24);
            this._alreadyWithMailingAddrValue.TabIndex = 71;
            this._alreadyWithMailingAddrValue.Text = "0";
            this._alreadyWithMailingAddrValue.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // _alreadyWithMailingAddrLabel
            // 
            this._alreadyWithMailingAddrLabel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this._alreadyWithMailingAddrLabel.Enabled = false;
            this._alreadyWithMailingAddrLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this._alreadyWithMailingAddrLabel.Location = new System.Drawing.Point(8, 160);
            this._alreadyWithMailingAddrLabel.Name = "_alreadyWithMailingAddrLabel";
            this._alreadyWithMailingAddrLabel.Size = new System.Drawing.Size(160, 16);
            this._alreadyWithMailingAddrLabel.TabIndex = 70;
            this._alreadyWithMailingAddrLabel.Text = "Already With Mailing Address";
            // 
            // lblUnsucessful
            // 
            this.lblUnsucessful.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.lblUnsucessful.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.lblUnsucessful.Location = new System.Drawing.Point(176, 111);
            this.lblUnsucessful.Name = "lblUnsucessful";
            this.lblUnsucessful.Size = new System.Drawing.Size(56, 24);
            this.lblUnsucessful.TabIndex = 44;
            this.lblUnsucessful.Text = "20000";
            this.lblUnsucessful.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(1, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(100, 23);
            this.label2.TabIndex = 69;
            this.label2.Text = "Status";
            // 
            // _alreadyWithNamesValue
            // 
            this._alreadyWithNamesValue.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this._alreadyWithNamesValue.Enabled = false;
            this._alreadyWithNamesValue.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this._alreadyWithNamesValue.Location = new System.Drawing.Point(176, 135);
            this._alreadyWithNamesValue.Name = "_alreadyWithNamesValue";
            this._alreadyWithNamesValue.Size = new System.Drawing.Size(56, 24);
            this._alreadyWithNamesValue.TabIndex = 68;
            this._alreadyWithNamesValue.Text = "0";
            this._alreadyWithNamesValue.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // _alreadyWithNamesLabel
            // 
            this._alreadyWithNamesLabel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this._alreadyWithNamesLabel.Enabled = false;
            this._alreadyWithNamesLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this._alreadyWithNamesLabel.Location = new System.Drawing.Point(8, 135);
            this._alreadyWithNamesLabel.Name = "_alreadyWithNamesLabel";
            this._alreadyWithNamesLabel.Size = new System.Drawing.Size(135, 16);
            this._alreadyWithNamesLabel.TabIndex = 67;
            this._alreadyWithNamesLabel.Text = "Already With Names";
            // 
            // lblSuccessful
            // 
            this.lblSuccessful.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.lblSuccessful.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.lblSuccessful.Location = new System.Drawing.Point(176, 87);
            this.lblSuccessful.Name = "lblSuccessful";
            this.lblSuccessful.Size = new System.Drawing.Size(56, 24);
            this.lblSuccessful.TabIndex = 43;
            this.lblSuccessful.Text = "80000";
            this.lblSuccessful.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblTotalProcessed
            // 
            this.lblTotalProcessed.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.lblTotalProcessed.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.lblTotalProcessed.Location = new System.Drawing.Point(176, 63);
            this.lblTotalProcessed.Name = "lblTotalProcessed";
            this.lblTotalProcessed.Size = new System.Drawing.Size(56, 24);
            this.lblTotalProcessed.TabIndex = 42;
            this.lblTotalProcessed.Text = "100000";
            this.lblTotalProcessed.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblNumberOfRecords
            // 
            this.lblNumberOfRecords.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.lblNumberOfRecords.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.lblNumberOfRecords.Location = new System.Drawing.Point(176, 39);
            this.lblNumberOfRecords.Name = "lblNumberOfRecords";
            this.lblNumberOfRecords.Size = new System.Drawing.Size(56, 24);
            this.lblNumberOfRecords.TabIndex = 41;
            this.lblNumberOfRecords.Text = "123456";
            this.lblNumberOfRecords.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label1.Location = new System.Drawing.Point(8, 39);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(159, 16);
            this.label1.TabIndex = 35;
            this.label1.Text = "Nr. of Records in Group";
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label3.Location = new System.Drawing.Point(8, 63);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(134, 16);
            this.label3.TabIndex = 38;
            this.label3.Text = "Total Processed";
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label4.Location = new System.Drawing.Point(8, 87);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(159, 16);
            this.label4.TabIndex = 39;
            this.label4.Text = "Successfuly Processed";
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label5.Location = new System.Drawing.Point(8, 111);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(167, 16);
            this.label5.TabIndex = 40;
            this.label5.Text = "Unsuccessfuly Processed";
            // 
            // _GoToStepB
            // 
            this._GoToStepB.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._GoToStepB.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this._GoToStepB.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._GoToStepB.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this._GoToStepB.Location = new System.Drawing.Point(846, 648);
            this._GoToStepB.Name = "_GoToStepB";
            this._GoToStepB.Size = new System.Drawing.Size(56, 24);
            this._GoToStepB.TabIndex = 69;
            this._GoToStepB.Text = "Step B";
            this._GoToStepB.UseVisualStyleBackColor = false;
            this._GoToStepB.Click += new System.EventHandler(this._GoToStepB_Click);
            // 
            // bBrowse
            // 
            this.bBrowse.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.bBrowse.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.bBrowse.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.bBrowse.Location = new System.Drawing.Point(242, 24);
            this.bBrowse.Name = "bBrowse";
            this.bBrowse.Size = new System.Drawing.Size(132, 24);
            this.bBrowse.TabIndex = 57;
            this.bBrowse.Text = "Select Group to Match";
            this.bBrowse.UseVisualStyleBackColor = false;
            // 
            // label17
            // 
            this.label17.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label17.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label17.Location = new System.Drawing.Point(792, 651);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(48, 16);
            this.label17.TabIndex = 65;
            this.label17.Text = "Go to";
            // 
            // label16
            // 
            this.label16.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label16.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label16.Location = new System.Drawing.Point(8, 651);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(736, 16);
            this.label16.TabIndex = 64;
            this.label16.Text = "NOTE: you need to process all the records in the Unmatched Records table prior to" +
    " going to Step B.";
            // 
            // label15
            // 
            this.label15.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label15.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label15.Location = new System.Drawing.Point(16, 590);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(376, 18);
            this.label15.TabIndex = 63;
            this.label15.Text = "4. Decide on \"Unmatched Records\" using buttons below:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label14.Location = new System.Drawing.Point(16, 100);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(132, 13);
            this.label14.TabIndex = 62;
            this.label14.Text = "4.  Click to Start Matching.";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label13.Location = new System.Drawing.Point(16, 76);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(202, 13);
            this.label13.TabIndex = 59;
            this.label13.Text = "3.  Do you want this process to overwrite:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label12.Location = new System.Drawing.Point(384, 28);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(163, 13);
            this.label12.TabIndex = 58;
            this.label12.Text = "* your currently selected group is:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label11.Location = new System.Drawing.Point(16, 28);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(220, 13);
            this.label11.TabIndex = 56;
            this.label11.Text = "1.  Choose the group that you want to match:";
            // 
            // label10
            // 
            this.label10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label10.Location = new System.Drawing.Point(8, 8);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(264, 19);
            this.label10.TabIndex = 55;
            this.label10.Text = "Step A:  Match the Names by Address";
            // 
            // listViewSuccess
            // 
            this.listViewSuccess.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeaderOwner,
            this.columnHeaderSreetName,
            this.columnHeaderCity,
            this.columnHeaderState,
            this.columnHeaderZip,
            this.columnHeaderCounty,
            this.columnHeader8,
            this.columnHeader20,
            this.columnHeader21,
            this.columnHeader17});
            this.listViewSuccess.FullRowSelect = true;
            this.listViewSuccess.GridLines = true;
            this.listViewSuccess.HideSelection = false;
            this.listViewSuccess.Location = new System.Drawing.Point(16, 155);
            this.listViewSuccess.MultiSelect = false;
            this.listViewSuccess.Name = "listViewSuccess";
            this.listViewSuccess.Size = new System.Drawing.Size(633, 224);
            this.listViewSuccess.TabIndex = 45;
            this.listViewSuccess.UseCompatibleStateImageBehavior = false;
            this.listViewSuccess.View = System.Windows.Forms.View.Details;
            // 
            // columnHeaderOwner
            // 
            this.columnHeaderOwner.Text = "Full Owner Name";
            this.columnHeaderOwner.Width = 140;
            // 
            // columnHeaderSreetName
            // 
            this.columnHeaderSreetName.Text = "Site Street Address";
            this.columnHeaderSreetName.Width = 120;
            // 
            // columnHeaderCity
            // 
            this.columnHeaderCity.Text = "City";
            this.columnHeaderCity.Width = 80;
            // 
            // columnHeaderState
            // 
            this.columnHeaderState.Text = "State";
            // 
            // columnHeaderZip
            // 
            this.columnHeaderZip.Text = "ZIP";
            // 
            // columnHeaderCounty
            // 
            this.columnHeaderCounty.Text = "County";
            this.columnHeaderCounty.Width = 80;
            // 
            // columnHeader8
            // 
            this.columnHeader8.Text = "Mailing Street Address";
            this.columnHeader8.Width = 120;
            // 
            // columnHeader20
            // 
            this.columnHeader20.Text = "City, State";
            // 
            // columnHeader21
            // 
            this.columnHeader21.Text = "ZIP";
            // 
            // columnHeader17
            // 
            this.columnHeader17.Text = "Phone Nr.";
            // 
            // label6
            // 
            this.label6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label6.Location = new System.Drawing.Point(16, 131);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(224, 16);
            this.label6.TabIndex = 50;
            this.label6.Text = "Successfully matched records";
            // 
            // _TagAll
            // 
            this._TagAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._TagAll.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this._TagAll.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._TagAll.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this._TagAll.Location = new System.Drawing.Point(16, 611);
            this._TagAll.Name = "_TagAll";
            this._TagAll.Size = new System.Drawing.Size(80, 32);
            this._TagAll.TabIndex = 53;
            this._TagAll.Text = "Tag All";
            this._TagAll.UseVisualStyleBackColor = false;
            // 
            // _UnTagAll
            // 
            this._UnTagAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._UnTagAll.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this._UnTagAll.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._UnTagAll.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this._UnTagAll.Location = new System.Drawing.Point(96, 611);
            this._UnTagAll.Name = "_UnTagAll";
            this._UnTagAll.Size = new System.Drawing.Size(80, 32);
            this._UnTagAll.TabIndex = 54;
            this._UnTagAll.Text = "Untag All";
            this._UnTagAll.UseVisualStyleBackColor = false;
            // 
            // label7
            // 
            this.label7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label7.Location = new System.Drawing.Point(16, 385);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(160, 24);
            this.label7.TabIndex = 51;
            this.label7.Text = "Unmatched records";
            // 
            // listViewFaliure
            // 
            this.listViewFaliure.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.listViewFaliure.CheckBoxes = true;
            this.listViewFaliure.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader9,
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader5,
            this.columnHeader6,
            this.columnHeader7,
            this.ID});
            this.listViewFaliure.FullRowSelect = true;
            this.listViewFaliure.GridLines = true;
            this.listViewFaliure.HideSelection = false;
            this.listViewFaliure.Location = new System.Drawing.Point(15, 411);
            this.listViewFaliure.MultiSelect = false;
            this.listViewFaliure.Name = "listViewFaliure";
            this.listViewFaliure.Size = new System.Drawing.Size(634, 171);
            this.listViewFaliure.TabIndex = 49;
            this.listViewFaliure.UseCompatibleStateImageBehavior = false;
            this.listViewFaliure.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader9
            // 
            this.columnHeader9.Text = "Tag";
            this.columnHeader9.Width = 39;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Error";
            this.columnHeader1.Width = 120;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Street Address";
            this.columnHeader2.Width = 100;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "City";
            this.columnHeader3.Width = 40;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "State";
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "ZIP";
            this.columnHeader5.Width = 50;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "County";
            this.columnHeader6.Width = 80;
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "ID";
            this.columnHeader7.Width = 0;
            // 
            // ID
            // 
            this.ID.Text = "ID";
            this.ID.Width = 0;
            // 
            // btnStartMatching
            // 
            this.btnStartMatching.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.btnStartMatching.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnStartMatching.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.btnStartMatching.Location = new System.Drawing.Point(200, 96);
            this.btnStartMatching.Name = "btnStartMatching";
            this.btnStartMatching.Size = new System.Drawing.Size(132, 24);
            this.btnStartMatching.TabIndex = 36;
            this.btnStartMatching.Text = "Start Matching";
            this.btnStartMatching.UseVisualStyleBackColor = false;
            // 
            // btnStartMatchingManually
            // 
            this.btnStartMatchingManually.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnStartMatchingManually.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.btnStartMatchingManually.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnStartMatchingManually.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.btnStartMatchingManually.Location = new System.Drawing.Point(238, 611);
            this.btnStartMatchingManually.Name = "btnStartMatchingManually";
            this.btnStartMatchingManually.Size = new System.Drawing.Size(128, 32);
            this.btnStartMatchingManually.TabIndex = 47;
            this.btnStartMatchingManually.Text = "Match Manually";
            this.btnStartMatchingManually.UseVisualStyleBackColor = false;
            // 
            // btnDeleteUnmatched
            // 
            this.btnDeleteUnmatched.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnDeleteUnmatched.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.btnDeleteUnmatched.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnDeleteUnmatched.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.btnDeleteUnmatched.Location = new System.Drawing.Point(568, 611);
            this.btnDeleteUnmatched.Name = "btnDeleteUnmatched";
            this.btnDeleteUnmatched.Size = new System.Drawing.Size(144, 32);
            this.btnDeleteUnmatched.TabIndex = 46;
            this.btnDeleteUnmatched.Text = "Delete Unmatched";
            this.btnDeleteUnmatched.UseVisualStyleBackColor = false;
            // 
            // btnUseCurrentResident
            // 
            this.btnUseCurrentResident.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnUseCurrentResident.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.btnUseCurrentResident.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnUseCurrentResident.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.btnUseCurrentResident.Location = new System.Drawing.Point(374, 611);
            this.btnUseCurrentResident.Name = "btnUseCurrentResident";
            this.btnUseCurrentResident.Size = new System.Drawing.Size(186, 32);
            this.btnUseCurrentResident.TabIndex = 52;
            this.btnUseCurrentResident.Text = "Use Homeowner/Resident";
            this.btnUseCurrentResident.UseVisualStyleBackColor = false;
            // 
            // stepBTabPage
            // 
            this.stepBTabPage.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.stepBTabPage.Controls.Add(this.label24);
            this.stepBTabPage.Controls.Add(this._removeMultipleFirstNames);
            this.stepBTabPage.Controls.Add(this._selectedGroupName);
            this.stepBTabPage.Controls.Add(this._forceSeparation);
            this.stepBTabPage.Controls.Add(this._selectGroupForForcedSeparation);
            this.stepBTabPage.Controls.Add(this.bDeleteTaggedStepB);
            this.stepBTabPage.Controls.Add(this.bLeaveBlankStepB);
            this.stepBTabPage.Controls.Add(this.bFirstName);
            this.stepBTabPage.Controls.Add(this.label23);
            this.stepBTabPage.Controls.Add(this.label21);
            this.stepBTabPage.Controls.Add(this.label20);
            this.stepBTabPage.Controls.Add(this.label19);
            this.stepBTabPage.Controls.Add(this.label9);
            this.stepBTabPage.Controls.Add(this.label8);
            this.stepBTabPage.Controls.Add(this.TagAllStep2);
            this.stepBTabPage.Controls.Add(this.UnTagAllStep2);
            this.stepBTabPage.Controls.Add(this.lv_StepB);
            this.stepBTabPage.Controls.Add(this.label18);
            this.stepBTabPage.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.stepBTabPage.Location = new System.Drawing.Point(4, 22);
            this.stepBTabPage.Name = "stepBTabPage";
            this.stepBTabPage.Size = new System.Drawing.Size(910, 678);
            this.stepBTabPage.TabIndex = 1;
            this.stepBTabPage.Text = "STEP B";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(16, 541);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(401, 13);
            this.label24.TabIndex = 73;
            this.label24.Text = "2.  Tag Records && Click [Remove Multiple First Names] (for better skip trace res" +
    "ults).";
            // 
            // _removeMultipleFirstNames
            // 
            this._removeMultipleFirstNames.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._removeMultipleFirstNames.Location = new System.Drawing.Point(748, 540);
            this._removeMultipleFirstNames.Name = "_removeMultipleFirstNames";
            this._removeMultipleFirstNames.Size = new System.Drawing.Size(159, 34);
            this._removeMultipleFirstNames.TabIndex = 72;
            this._removeMultipleFirstNames.Text = "Remove Multiple First Names";
            // 
            // _selectedGroupName
            // 
            this._selectedGroupName.Enabled = false;
            this._selectedGroupName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._selectedGroupName.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this._selectedGroupName.Location = new System.Drawing.Point(616, 513);
            this._selectedGroupName.Name = "_selectedGroupName";
            this._selectedGroupName.Size = new System.Drawing.Size(287, 22);
            this._selectedGroupName.TabIndex = 69;
            this._selectedGroupName.Text = "* your currently selected group is:";
            this._selectedGroupName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // _forceSeparation
            // 
            this._forceSeparation.AutoSize = true;
            this._forceSeparation.Location = new System.Drawing.Point(582, 462);
            this._forceSeparation.Name = "_forceSeparation";
            this._forceSeparation.Size = new System.Drawing.Size(299, 17);
            this._forceSeparation.TabIndex = 71;
            this._forceSeparation.Text = "Force FIRST NAME separation on already matched group";
            this._forceSeparation.UseVisualStyleBackColor = true;
            // 
            // _selectGroupForForcedSeparation
            // 
            this._selectGroupForForcedSeparation.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this._selectGroupForForcedSeparation.Enabled = false;
            this._selectGroupForForcedSeparation.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._selectGroupForForcedSeparation.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this._selectGroupForForcedSeparation.Location = new System.Drawing.Point(598, 482);
            this._selectGroupForForcedSeparation.Name = "_selectGroupForForcedSeparation";
            this._selectGroupForForcedSeparation.Size = new System.Drawing.Size(141, 32);
            this._selectGroupForForcedSeparation.TabIndex = 68;
            this._selectGroupForForcedSeparation.Text = "Select Group";
            this._selectGroupForForcedSeparation.UseVisualStyleBackColor = false;
            // 
            // bDeleteTaggedStepB
            // 
            this.bDeleteTaggedStepB.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.bDeleteTaggedStepB.Location = new System.Drawing.Point(598, 616);
            this.bDeleteTaggedStepB.Name = "bDeleteTaggedStepB";
            this.bDeleteTaggedStepB.Size = new System.Drawing.Size(141, 34);
            this.bDeleteTaggedStepB.TabIndex = 30;
            this.bDeleteTaggedStepB.Text = "Delete Tagged";
            this.bDeleteTaggedStepB.Click += new System.EventHandler(this.bDeleteTaggedStepB_Click);
            // 
            // bLeaveBlankStepB
            // 
            this.bLeaveBlankStepB.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.bLeaveBlankStepB.Location = new System.Drawing.Point(598, 578);
            this.bLeaveBlankStepB.Name = "bLeaveBlankStepB";
            this.bLeaveBlankStepB.Size = new System.Drawing.Size(141, 34);
            this.bLeaveBlankStepB.TabIndex = 29;
            this.bLeaveBlankStepB.Text = "Leave  Blank";
            this.bLeaveBlankStepB.Click += new System.EventHandler(this.bLeaveBlankStepB_Click);
            // 
            // bFirstName
            // 
            this.bFirstName.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.bFirstName.Location = new System.Drawing.Point(598, 540);
            this.bFirstName.Name = "bFirstName";
            this.bFirstName.Size = new System.Drawing.Size(141, 34);
            this.bFirstName.TabIndex = 28;
            this.bFirstName.Text = "Separate First Name";
            // 
            // label23
            // 
            this.label23.Location = new System.Drawing.Point(32, 602);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(240, 23);
            this.label23.TabIndex = 66;
            this.label23.Text = "\"Current Resident\" on Labels).";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(16, 648);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(579, 13);
            this.label21.TabIndex = 64;
            this.label21.Text = "Note: Steps 4 && 5 are used only for records where Steps 1 && 2 do not provide ac" +
    "ceptable separation.";
            // 
            // label20
            // 
            this.label20.Location = new System.Drawing.Point(16, 626);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(584, 16);
            this.label20.TabIndex = 63;
            this.label20.Text = "5. Tag Records && Click [Delete Tagged] button to delete records. (Example: if ow" +
    "ned by a bank).";
            // 
            // label19
            // 
            this.label19.Location = new System.Drawing.Point(16, 586);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(576, 16);
            this.label19.TabIndex = 62;
            this.label19.Text = "4. Tag Records && Click [Leave Blank] button.  (PG will use \"Dear Homeowner\" in y" +
    "our letters, or";
            // 
            // label9
            // 
            this.label9.Location = new System.Drawing.Point(16, 562);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(576, 16);
            this.label9.TabIndex = 61;
            this.label9.Text = "3. Examine the First Name column and manually edit if needed, by double clicking " +
    "on the name.";
            // 
            // label8
            // 
            this.label8.Location = new System.Drawing.Point(16, 519);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(576, 16);
            this.label8.TabIndex = 60;
            this.label8.Text = "1. Tag Records && Click [Separate FIRST NAME] button.  (example: Dear \"John\").";
            // 
            // TagAllStep2
            // 
            this.TagAllStep2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.TagAllStep2.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.TagAllStep2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.TagAllStep2.Location = new System.Drawing.Point(16, 472);
            this.TagAllStep2.Name = "TagAllStep2";
            this.TagAllStep2.Size = new System.Drawing.Size(80, 32);
            this.TagAllStep2.TabIndex = 58;
            this.TagAllStep2.Text = "Tag All";
            this.TagAllStep2.UseVisualStyleBackColor = false;
            this.TagAllStep2.Click += new System.EventHandler(this.TagAllStep2_Click);
            // 
            // UnTagAllStep2
            // 
            this.UnTagAllStep2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.UnTagAllStep2.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.UnTagAllStep2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.UnTagAllStep2.Location = new System.Drawing.Point(104, 472);
            this.UnTagAllStep2.Name = "UnTagAllStep2";
            this.UnTagAllStep2.Size = new System.Drawing.Size(80, 32);
            this.UnTagAllStep2.TabIndex = 59;
            this.UnTagAllStep2.Text = "Untag All";
            this.UnTagAllStep2.UseVisualStyleBackColor = false;
            this.UnTagAllStep2.Click += new System.EventHandler(this.UnTagAllStep2_Click);
            // 
            // lv_StepB
            // 
            this.lv_StepB.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lv_StepB.CheckBoxes = true;
            this.lv_StepB.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader10,
            this.columnHeader11,
            this.columnHeader18,
            this.columnHeader12,
            this.columnHeader13,
            this.columnHeader14,
            this.columnHeader15,
            this.columnHeader16,
            this.columnHeader19,
            this.columnHeader25,
            this.columnHeader26,
            this.columnHeader22,
            this.columnHeader23,
            this.columnHeader24});
            this.lv_StepB.FullRowSelect = true;
            this.lv_StepB.GridLines = true;
            this.lv_StepB.HideSelection = false;
            this.lv_StepB.Location = new System.Drawing.Point(16, 48);
            this.lv_StepB.MultiSelect = false;
            this.lv_StepB.Name = "lv_StepB";
            this.lv_StepB.Size = new System.Drawing.Size(878, 408);
            this.lv_StepB.TabIndex = 57;
            this.lv_StepB.UseCompatibleStateImageBehavior = false;
            this.lv_StepB.View = System.Windows.Forms.View.Details;
            this.lv_StepB.DoubleClick += new System.EventHandler(this.lv_StepB_DoubleClick);
            // 
            // columnHeader10
            // 
            this.columnHeader10.Text = "Tag";
            this.columnHeader10.Width = 38;
            // 
            // columnHeader11
            // 
            this.columnHeader11.Text = "Full Owner Name";
            this.columnHeader11.Width = 172;
            // 
            // columnHeader18
            // 
            this.columnHeader18.Text = "First Name";
            this.columnHeader18.Width = 109;
            // 
            // columnHeader12
            // 
            this.columnHeader12.Text = "Street Address";
            this.columnHeader12.Width = 206;
            // 
            // columnHeader13
            // 
            this.columnHeader13.Text = "City";
            this.columnHeader13.Width = 95;
            // 
            // columnHeader14
            // 
            this.columnHeader14.Text = "State";
            this.columnHeader14.Width = 78;
            // 
            // columnHeader15
            // 
            this.columnHeader15.Text = "ZIP";
            this.columnHeader15.Width = 68;
            // 
            // columnHeader16
            // 
            this.columnHeader16.Text = "County";
            this.columnHeader16.Width = 80;
            // 
            // columnHeader19
            // 
            this.columnHeader19.Text = "Mailing Address";
            this.columnHeader19.Width = 120;
            // 
            // columnHeader25
            // 
            this.columnHeader25.Text = "Mailing City";
            this.columnHeader25.Width = 80;
            // 
            // columnHeader26
            // 
            this.columnHeader26.Text = "Mailing State";
            this.columnHeader26.Width = 80;
            // 
            // columnHeader22
            // 
            this.columnHeader22.Text = "Mailing City, State";
            this.columnHeader22.Width = 80;
            // 
            // columnHeader23
            // 
            this.columnHeader23.Text = "Mailing ZIP";
            this.columnHeader23.Width = 80;
            // 
            // columnHeader24
            // 
            this.columnHeader24.Text = "Phone Nr.";
            // 
            // label18
            // 
            this.label18.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label18.Location = new System.Drawing.Point(8, 16);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(352, 16);
            this.label18.TabIndex = 56;
            this.label18.Text = "Step B: Separate the First Name";
            // 
            // MM_Matching_Module
            // 
            this.Controls.Add(this.panelMainContainer);
            this.Name = "MM_Matching_Module";
            this.Size = new System.Drawing.Size(918, 704);
            this.Load += new System.EventHandler(this.MM_Matching_Module_Load);
            this.panelMainContainer.ResumeLayout(false);
            this.matchingModuleTabControl.ResumeLayout(false);
            this.stepATabPage.ResumeLayout(false);
            this.stepATabPage.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.stepBTabPage.ResumeLayout(false);
            this.stepBTabPage.PerformLayout();
            this.ResumeLayout(false);

		}
		#endregion

        private readonly object locker = new object();

        #region serviceWorker_FireServiceFinishedEvent
		private void serviceWorker_FireServiceFinishedEvent()
		{
			this.Invoke(SetButtonsEnabledFromThread,new object[] {true});

			if (unmatchedItems.Count > 0)
				this.Invoke(SetSecButtonsVisibleFromThread, new object[]{true});

			
			//MessageBox.Show("Matching has ended.","Profit Grabber",System.Windows.Forms.MessageBoxButtons.OK);
			// Todo: remove
			//MessageBox.Show("Errors " + serviceWorker.TotalErrorCount);
		}
		#endregion
		#region	serviceWorker_FireQueryPositivelySolvedEvent
		delegate void serviceWorker_FireQueryPositivelySolvedEventDelegate(PropertyItem pi, bool previousValueContained, bool previousMailingAddressContained, bool previousPhoneNumberContained);
		private void serviceWorker_FireQueryPositivelySolvedEvent(PropertyItem pi, bool previousValueContained, bool previousMailingAddressContained, bool previousPhoneNumberContained)
		{
			if (InvokeRequired)
			{
				BeginInvoke(new serviceWorker_FireQueryPositivelySolvedEventDelegate(serviceWorker_FireQueryPositivelySolvedEvent), new object[] {pi, previousValueContained, previousMailingAddressContained, previousPhoneNumberContained});
			}
			else
			{
                Debug.WriteLine($"MM Save: {pi.Owner.FullName}, previousValueContained:{previousValueContained}, previousMailingAddressContained:{previousMailingAddressContained}, previousPhoneNumberContained:{previousPhoneNumberContained}");
				if (_overwriteExistingNames.Checked || _overwriteMailingAddress.Checked || _overridePhoneNumber.Checked ||
					(!_overwriteExistingNames.Checked && !previousValueContained) || (!_overwriteMailingAddress.Checked && !previousMailingAddressContained) || (!_overridePhoneNumber.Checked && !previousPhoneNumberContained)) 
				{
					pi.save = false;
					pi.Owner.save = false;	
					
					lock (locker)
					{
						ApplicationHelper.SavePropertyItem(pi);											
					}

					ListViewItem lvi = new ListViewItem( new string[] 
						{
							//string.Empty,
							pi.Owner.FullName,
							pi.Owner.SiteAddress.FullSiteStreetAddress,
							pi.Owner.SiteAddress.SiteCity,
							pi.Owner.SiteAddress.SiteState,
							pi.Owner.SiteAddress.SiteZIP,
							pi.Owner.SiteAddress.SiteCountry, 
							(null != pi.Owner.MailAddress && null != pi.Owner.MailAddress.FullSiteStreetAddress) ? pi.Owner.MailAddress.FullSiteStreetAddress : string.Empty,
							(null != pi.Owner.MailAddress && null != pi.Owner.MailAddress.SiteCitySiteState) ? pi.Owner.MailAddress.SiteCitySiteState: string.Empty,							
							(null != pi.Owner.MailAddress && null != pi.Owner.MailAddress.SiteZIP) ? pi.Owner.MailAddress.SiteZIP : string.Empty, 
							(null != pi.Owner.HomePhone && null != pi.Owner.HomePhone.PhoneNr) ? pi.Owner.HomePhone.PhoneNr : string.Empty
						});
					lvi.Tag = pi.IdPropertyItem;
					this.listViewSuccess.Items.Add(lvi);

                    string tempCity = string.Empty;
                    string tempState = string.Empty;                                       

					ListViewItem lvi2 = new ListViewItem( new string[] 
						{
							string.Empty,
							pi.Owner.FullName,
							string.Empty,
							pi.Owner.SiteAddress.FullSiteStreetAddress,
							pi.Owner.SiteAddress.SiteCity,
							pi.Owner.SiteAddress.SiteState,
							pi.Owner.SiteAddress.SiteZIP,
							pi.Owner.SiteAddress.SiteCountry, 
							(null != pi.Owner.MailAddress && null != pi.Owner.MailAddress.FullSiteStreetAddress) ? pi.Owner.MailAddress.FullSiteStreetAddress : string.Empty,
                            (null != pi.Owner.MailAddress && null != pi.Owner.MailAddress.SiteCity) ? pi.Owner.MailAddress.SiteCity : string.Empty,
                            (null != pi.Owner.MailAddress && null != pi.Owner.MailAddress.SiteState) ? pi.Owner.MailAddress.SiteState : string.Empty,                            
							(null != pi.Owner.MailAddress && null != pi.Owner.MailAddress.SiteCitySiteState) ? pi.Owner.MailAddress.SiteCitySiteState: string.Empty,							
							(null != pi.Owner.MailAddress && null != pi.Owner.MailAddress.SiteZIP) ? pi.Owner.MailAddress.SiteZIP : string.Empty, 
							(null != pi.Owner.HomePhone && null != pi.Owner.HomePhone.PhoneNr) ? pi.Owner.HomePhone.PhoneNr : string.Empty
						});
					lvi2.Tag = pi.IdPropertyItem;
					lvi2.SubItems[2].BackColor = Color.Yellow;
					this.lv_StepB.Items.Add(lvi2);

                    UpdatePreviousCounterValues(previousValueContained, previousMailingAddressContained, previousPhoneNumberContained);

					if (_updateAutomaticCounters)
					{
						++success;
						lblSuccessful.Text = success.ToString();

						++totalProc;
						lblTotalProcessed.Text = totalProc.ToString();
					}
					else
					{
						_matchedManuallyCounter++;
						_MatchedManually.Text = _matchedManuallyCounter.ToString();
					}
				}
				else if (!_overwriteExistingNames.Checked || !_overwriteMailingAddress.Checked || !_overridePhoneNumber.Checked)
				{
					if (_updateAutomaticCounters)
					{
						UpdatePreviousCounterValues(previousValueContained, previousMailingAddressContained, previousPhoneNumberContained);

                        ++totalProc;
						lblTotalProcessed.Text = totalProc.ToString();						
					}
				}				
			}			
		}

        private void UpdatePreviousCounterValues(bool previousValueContained, bool previousMailingAddressContained,
            bool previousPhoneNumberContained)
        {
            if (previousValueContained)
            {
                _alreadyWithNames++;
                _alreadyWithNamesValue.Text = _alreadyWithNames.ToString();
            }

            if (previousMailingAddressContained)
            {
                _alreadyWithMailingAddress++;
                _alreadyWithMailingAddrValue.Text = _alreadyWithMailingAddress.ToString();
            }

            if (previousPhoneNumberContained)
            {
                _alreadyWithPhoneNumber++;
                _alreadyWithPhoneNumberValue.Text = _alreadyWithPhoneNumber.ToString();
            }
        }

        #endregion
		#region serviceWorker_FireQueryNegativelySolvedEvent
		delegate void serviceWorker_FireQueryNegativelySolvedEventDelegate(PropertyItem pi, String message);
		private void serviceWorker_FireQueryNegativelySolvedEvent(PropertyItem pi, String message)
		{	
			if (InvokeRequired)
			{
				BeginInvoke(new serviceWorker_FireQueryNegativelySolvedEventDelegate(serviceWorker_FireQueryNegativelySolvedEvent), new object[] {pi, message});
			}
			else
			{				
				Debug.Assert(null != selectedNode, "selectedNode is null");

				if (-1 == _notMatchedGroupId)
				{
					_notMatchedGroupId = GetCreateGroup(selectedNode.nodeId, groupSuccess.unsuccessful);										
					//groupsTreeView.UpdateTreeView();
				}
				if (-1 == _notMatchedGroup_NotMailedId)
					_notMatchedGroup_NotMailedId = GetCreateGroup(selectedNode.nodeId, groupSuccess.unsuccessful_notMailed);

				if (-1 == _notMatchedGroup_MailedWoNameId)
					_notMatchedGroup_MailedWoNameId = GetCreateGroup(selectedNode.nodeId, groupSuccess.unsuccessful_mailedWoName);


				AssignToGroup(_notMatchedGroupId, pi.IdPropertyItem);
				
				// we do not want to disable the StepB buttons once we have enabled them
				//StepBEnabled = false;
#if TEST
				
#else 
				ApplicationHelper.DeletePropertyItemFromGroup(pi.IdPropertyItem, selectedNode.nodeId);
#endif

				unmatchedItems.Add(pi);

				ListViewItem lvi = new ListViewItem( new string[] 
					{
						string.Empty,
						message, //pi.Owner.FirstName,,
						(null != pi && null != pi.Owner && null != pi.Owner.SiteAddress && null != pi.Owner.SiteAddress.FullSiteStreetAddress) ? pi.Owner.SiteAddress.FullSiteStreetAddress : string.Empty,
						(null != pi && null != pi.Owner && null != pi.Owner.SiteAddress && null != pi.Owner.SiteAddress.SiteCity) ? pi.Owner.SiteAddress.SiteCity : string.Empty,
						(null != pi && null != pi.Owner && null != pi.Owner.SiteAddress && null != pi.Owner.SiteAddress.SiteState) ? pi.Owner.SiteAddress.SiteState : string.Empty,
						(null != pi && null != pi.Owner && null != pi.Owner.SiteAddress && null != pi.Owner.SiteAddress.SiteZIP) ? pi.Owner.SiteAddress.SiteZIP : string.Empty, 
						(null != pi && null != pi.Owner && null != pi.Owner.SiteAddress && null != pi.Owner.SiteAddress.SiteCountry) ? pi.Owner.SiteAddress.SiteCountry : string.Empty,
						faliure.ToString() //NOTE !!! HERE FALIURE IS 0 BASED !!!!!
					});
				lvi.Tag = pi.IdPropertyItem;
				this.listViewFaliure.Items.Add(lvi);

				if (_updateAutomaticCounters)
				{
					++faliure;
					lblUnsucessful.Text = faliure.ToString();

					++totalProc;
					lblTotalProcessed.Text = totalProc.ToString();
				}
			}									
		}
		#endregion
		#region	serviceWorker_FireServiceErrorEvent
		private void serviceWorker_FireServiceErrorEvent(string ErrorMessage, Exception innerException)
		{
			serviceWorker.AbortWork();
#if DEBUG			
			if (null != innerException)
				MessageBox.Show(ErrorMessage.ToUpper() + "\n" + innerException.Message + "\n" + innerException.Source + "\n" + innerException.InnerException + "\n" + innerException.StackTrace, "ProfitGrabber Pro", MessageBoxButtons.OK, MessageBoxIcon.Information);
			else
				MessageBox.Show(ErrorMessage.ToUpper(), "ProfitGrabber Pro", MessageBoxButtons.OK, MessageBoxIcon.Information);
#else
			MessageBox.Show(ErrorMessage.ToUpper(), "ProfitGrabber Pro", MessageBoxButtons.OK, MessageBoxIcon.Information);
#endif
		}
		#endregion

		#region AssignToGroup
		/// <summary>		
		/// </summary>
		/// <param name="groupID">Group that PI should be assigned to</param>
		/// <param name="piID">ID of the PI</param>
		private void AssignToGroup(int groupID, Guid piID)
		{
			//insert new one
			try
			{							
				SqlCommand sp_InsertGroupMembership = new SqlCommand("InsertGroupMembership", Connection.getInstance().HatchConn);

				sp_InsertGroupMembership.CommandType = CommandType.StoredProcedure;					
				sp_InsertGroupMembership.Parameters.Add("@propertyItemId", piID);
				sp_InsertGroupMembership.Parameters.Add("@nodeId", groupID);
													
									
				if (ConnectionState.Open != Connection.getInstance().HatchConn.State)
					Connection.getInstance().HatchConn.Open();
				
				sp_InsertGroupMembership.ExecuteNonQuery();				
					
			}
			catch( Exception ex)
			{					
				MessageBox.Show(ex.ToString());				
			}
		}
		#endregion
		#region CreateGroup
		const string SuccMR = "Matched Records";
		const string USuccMR = "Un-matched";
		const string USucc_MWoN_MR = "Un-matched - Mailed w/o name";
		const string USucc_NM_MR = "Un-matched - Not Mailed ";
		enum groupSuccess
		{
			successful = 1, 
			unsuccessful = 2,
			unsuccessful_notMailed = 3,
			unsuccessful_mailedWoName = 4,
		}
		int GetCreateGroup(int parentNodeId, groupSuccess gs)
		{
			ArrayList subordinatedGroups = new ArrayList();
			try
			{
				SqlConnection connection = new SqlConnection();
				connection.ConnectionString = Globals.GetConnectionString();
				connection.Open();
				
				/*				 
					select ChildNode.nodeId 
					from Node as ParentNode, node as ChildNode
					where ParentNode.nodeId = ChildNode.parentNodeId
					and ParentNode.nodeId = 38
				 */

				string sqlComm = "select ChildNode.nodeId from Node as ParentNode, node as ChildNode where ChildNode.nodeName = '";
				if (gs == groupSuccess.successful)
					sqlComm += SuccMR;
				else if (gs == groupSuccess.unsuccessful)
					sqlComm += USuccMR;
				else if (gs == groupSuccess.unsuccessful_mailedWoName)
					sqlComm += USucc_MWoN_MR;
				else
					sqlComm += USucc_NM_MR;

				sqlComm += "' and ParentNode.nodeId = ChildNode.parentNodeId and ParentNode.nodeId = ";
				sqlComm	+= parentNodeId.ToString();

				SqlCommand cGetNodes = new SqlCommand(sqlComm, connection);					
									
				if (ConnectionState.Open != Connection.getInstance().HatchConn.State)
					Connection.getInstance().HatchConn.Open();

				SqlDataReader dataReader = cGetNodes.ExecuteReader();	
				while (dataReader.Read())
				{
					int group = dataReader.GetInt32(0);
					subordinatedGroups.Add(group);
				}
			}
			catch( Exception ex)
			{
				MessageBox.Show(ex.ToString());
			}

			if (null != subordinatedGroups &&  1 == subordinatedGroups.Count)
				return (int)subordinatedGroups[0];
			else
				return CreateGroup(parentNodeId, gs);
		}
		
		/// <summary>
		/// Creates new group in the group hierarchy of PG
		/// TODO: move to applicationHelper
		/// </summary>
		/// <param name="groupParentID">ID Of group taht is to be parrent of th new one</param>
		/// <returns>ID of the group that is newly created</returns>
		private int CreateGroup(int groupParentID, groupSuccess gs)
		{	   
			int retVal = 0;
			int newNodeId = -1;

			try
			{
				SqlConnection connection = new SqlConnection();
				connection.ConnectionString = Globals.GetConnectionString();
				connection.Open();
				SqlCommand sp_dodaj_Node = new SqlCommand("dodaj_Node", connection);

				sp_dodaj_Node.CommandType = CommandType.StoredProcedure;
					
				sp_dodaj_Node.Parameters.Add("@parentNode", groupParentID);
				if (gs == groupSuccess.unsuccessful)
				{
					sp_dodaj_Node.Parameters.Add("@nodeDesc", "Automaticaly Generated Group for " + USuccMR);
					sp_dodaj_Node.Parameters.Add("@nodeName", USuccMR);
				}
				else if (gs == groupSuccess.unsuccessful_mailedWoName)
				{
					sp_dodaj_Node.Parameters.Add("@nodeDesc", "Automaticaly Generated Group for " + USucc_MWoN_MR);
					sp_dodaj_Node.Parameters.Add("@nodeName", USucc_MWoN_MR);
				}
				else if (gs == groupSuccess.unsuccessful_notMailed)
				{
					sp_dodaj_Node.Parameters.Add("@nodeDesc", "Automaticaly Generated Group for " + USucc_NM_MR);
					sp_dodaj_Node.Parameters.Add("@nodeName", USucc_NM_MR);
				}
				else if (gs == groupSuccess.successful)
				{
					sp_dodaj_Node.Parameters.Add("@nodeDesc", "Automaticaly Generated Group for " + SuccMR);
					sp_dodaj_Node.Parameters.Add("@nodeName", SuccMR);
				}
					
				SqlParameter param = sp_dodaj_Node.Parameters.Add("@retValue", retVal);
				param.Direction = ParameterDirection.Output;

				SqlParameter new_NodeId_param = sp_dodaj_Node.Parameters.Add("@newNodeId ", newNodeId);
				new_NodeId_param.Direction = ParameterDirection.Output;
									
				if (ConnectionState.Open != Connection.getInstance().HatchConn.State)
					Connection.getInstance().HatchConn.Open();

				sp_dodaj_Node.ExecuteNonQuery();
	
				if (-1 == (int)param.Value)
				{
					MessageBox.Show("Error while adding node!");
				}
				else
				{
					//UpdateTreeView();
					newNodeId = (System.Int32)new_NodeId_param.Value;
					//this.groupsTreeView.UpdateTreeView();
				}
			}
			catch( Exception ex)
			{
				MessageBox.Show(ex.ToString());
			}
			return newNodeId;
		}
		#endregion

		#region setCommandsEnabled
		private void setCommandsEnabled(bool enabled)
		{
			//checkBoxSetCurrentResident.Enabled=enabled;
			//btnDeleteUnmatched.Enabled=enabled;
			//btnStartMatchingManually.Enabled=enabled;

			if (enabled)
				btnStartMatching.Text = "Start Matching";
			else
				btnStartMatching.Text =	"Cancel Matching";		
		}
		#endregion
		#region setSecondaryBtnGroupVisible

		private void setSecondaryBtnGroupVisible(bool visible)
		{
			btnDeleteUnmatched.Enabled = visible;
			btnStartMatchingManually.Enabled = visible;
			btnUseCurrentResident.Enabled = visible;

			//checkBoxSetCurrentResident.Visible=!visible;
			btnStartMatching.Enabled=!visible;
		}
		#endregion

		#region btnStartMatching_Click
		private void btnStartMatching_Click(object sender, System.EventArgs e)
		{
			serviceWorker.UseAPN = _useAPN.Checked;
			serviceWorker.OverrideExistingMailingAddress = _overwriteMailingAddress.Checked;
			serviceWorker.OverrideExistingNames = _overwriteExistingNames.Checked;
			serviceWorker.OverrideExistingPhoneNumber = _overridePhoneNumber.Checked;

            if (null == selectedNode)	
			{
				MessageBox.Show("Select group", "Group Info missing");
				return;
			}

			int elementCount = ApplicationHelper.GetPropertyCountInNode(selectedNode.nodeId);
			if (0 == elementCount)
			{
				MessageBox.Show("Group " + selectedNode.nodeName + " contains no Contacts", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}

			_notMatchedGroupId = -1;
			_notMatchedGroup_NotMailedId = -1;
			_notMatchedGroup_MailedWoNameId = -1;
			
			
			if ("Cancel Matching" == btnStartMatching.Text)
			{	
				serviceWorker.AbortWork();
				MessageBox.Show
					(
					"Please wait till current request is finished."
					,"Profit Grabber"
					, MessageBoxButtons.OK
					, MessageBoxIcon.Information
					);
			}
			else
			{					
				#region starting
				if (1 > this.nodeCount)
				{
					MessageBox.Show
						(
						"No records found. Please select group with records."
						, "Warning!"
						, MessageBoxButtons.OK
						, MessageBoxIcon.Warning
						);
					return;
				}

                var dialogText = "You will incur charge or credit reduction!"
                                 + "\nDo you want to continue?";

                if (_matchContactsThatAlreadyHaveNamesAndMailingAddresses.Checked)
                    dialogText += "\n\nNOTE:"
                                  + "\nAll records will be matched," + Environment.NewLine
                                  + "even those that already have names.";
                
				DialogResult dlg = MessageBox.Show
					(
					dialogText
					, "Warning!"
					, MessageBoxButtons.YesNo
					, MessageBoxIcon.Warning
					);
				if (dlg == DialogResult.No)
				{
					return;
				}

				int totalCost = 0;
				int totalAmount = 0;
				string errMsg;
				int monthlyCredits;
				int topUpCredits;

                if (!UserCredits.Instance.TestForEnough(Globals.DataConsumptionReg.KeyCode, TypeOfCharge.MM, elementCount, out totalCost, out totalAmount, out monthlyCredits, out topUpCredits, out errMsg))
                {
                    UserCreditForm ucf = new UserCreditForm(DealMaker.Globals.UserRegistration.IDUser, monthlyCredits, topUpCredits, totalAmount, TypeOfCharge.MM, elementCount, totalCost);
                    ucf.ShowDialog();
                    return;
                }

				setCommandsEnabled(false);

				prepareForm();

				_updateAutomaticCounters = true;

				_matchedManuallyCounter = 0;
				_willUseHomeownerCoutner = 0;
				_deletedRecordsCounter = 0;


				if (null != selectedNode)
				{
					try
					{
						if (selectedNode.nodeName.Length < 10)
							ApplicationHelper.UpdateGroupName(selectedNode.nodeId, selectedNode.nodeName + " - matched", string.Empty);
						else if (!selectedNode.nodeName.Substring(selectedNode.nodeName.Length - 10).Equals(" - matched"))					
							ApplicationHelper.UpdateGroupName(selectedNode.nodeId, selectedNode.nodeName + " - matched", string.Empty);					
					}
					catch 
					{
						// no op. 
					}
				}
				 
				Hashtable ht_mm_pi = ApplicationHelper.GetPropertyItemsInNode(selectedNode.nodeId);
                

				session = Globals.factory.OpenSession();
		

				//bool userWarned = false;
				int warnCnt = 0;
                var skipped = 0;
				IDictionaryEnumerator ide = ht_mm_pi.GetEnumerator();
				while (ide.MoveNext())
				{
					PropertyItem pi = (PropertyItem)session.Load(typeof(PropertyItem ), (Guid)ide.Value); 
					if (null != pi)
					{
                        if (!_matchContactsThatAlreadyHaveNamesAndMailingAddressesProp)
                        {
                            if (!string.IsNullOrEmpty(pi.Owner?.MailAddress?.FullSiteStreetAddress) && !string.IsNullOrEmpty(pi.Owner?.FullName))
                            {
                                skipped++;
                                continue;
                            }
                        }

                        if (null != pi.Owner && null != pi.Owner.SiteAddress && ExcludeZIP(pi.Owner.SiteAddress.SiteZIP))
						{
							//if (!userWarned)
							//	MessageBox.Show("Some of your requests are out of the coverage area.", "Out of coverage area", MessageBoxButtons.OK, MessageBoxIcon.Information);

							//userWarned = true;
							warnCnt++;
							continue;
						}
						serviceWorker.AddPropertyItem(pi);					                		
					}
				}
				session.Close();
	
				if (0 == serviceWorker.NumberOfItemsInList)
				{
                    if (skipped > 0)
                        MessageBox.Show("All contacts were previously matched.", "Matching Module", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    if (warnCnt > 0)
                        MessageBox.Show("Coverage not available in the area selected at this time for all of your requests.", "Coverage not available", MessageBoxButtons.OK, MessageBoxIcon.Information);

					setCommandsEnabled(true);
					return;
				}
				else
				{
					if (warnCnt > 0)
						MessageBox.Show("Coverage not available in the area selected at this time for some of your requests.", "Coverage not available", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
		
				serviceWorker.ManualStepping = false;
				System.Threading.ThreadStart workerStarter = 
					new System.Threading.ThreadStart(serviceWorker.startWorking);
				System.Threading.Thread	workerHolder = 
					new System.Threading.Thread(workerStarter);
				workerHolder.Start();

				
				//DISPLAY MODAL DIALOG
				MM_Form mmForm = new MM_Form();
				mmForm.GroupMembersCount = ht_mm_pi.Count - skipped;
				mmForm.ShowDialog();
				//DISPLAY MODAL DIALOG
				#endregion
			}
		}
		#endregion
		#region btnDeleteUnmatched_Click
		private void btnDeleteUnmatched_Click(object sender, System.EventArgs e)
		{	
			bool update = false;				
			
			if (-1 == _notMatchedGroupId)		
				_notMatchedGroupId = GetCreateGroup(selectedNode.nodeId, groupSuccess.unsuccessful);
			if (-1 == _notMatchedGroup_NotMailedId)		
				_notMatchedGroup_NotMailedId = GetCreateGroup(selectedNode.nodeId, groupSuccess.unsuccessful_notMailed);
			if (-1 == _notMatchedGroup_MailedWoNameId)		
				_notMatchedGroup_MailedWoNameId = GetCreateGroup(selectedNode.nodeId, groupSuccess.unsuccessful_mailedWoName);
				
						

			ArrayList piIds = new ArrayList();
			foreach (ListViewItem lvi in this.listViewFaliure.Items)
			{
				if (lvi.Checked)
				{
					Guid piId = (Guid)lvi.Tag;
					piIds.Add(piId);
				}				
			}

			_deletedRecordsCounter += piIds.Count;

			if (piIds.Count > 0)
			{
				update = true;
				foreach (Guid piId in piIds)
				{
					//ApplicationHelper.DeleteTaggedPropertyItems(piIds, out update);
					//MM NEW RULES
					//1.) Add to Un-Matched - Not Mailed
					AssignToGroup(_notMatchedGroup_NotMailedId, piId);				
					//3.) remove from unmatched
					ApplicationHelper.DeletePropertyItemFromGroup(piId, _notMatchedGroupId);							
					if (0 == ApplicationHelper.GetPropertyCountInNode(_notMatchedGroupId))						
					{
						ApplicationHelper.DeleteGroup(_notMatchedGroupId);
						StepBEnabled = true;
					}
					//MM NEW RULES
				}
			}
			else
				MessageBox.Show("Tag items to delete!", "Tag Items", MessageBoxButtons.OK, MessageBoxIcon.Information);
						

			if (update)
			{
				foreach (ListViewItem lvi in this.listViewFaliure.Items)
				{
					if (lvi.Checked)
						lvi.Remove();
				}
				btnStartMatching.Visible=true;
			}
			
			_DeletedRecords.Text = _deletedRecordsCounter.ToString();
		}
		#endregion
		#region btnUseCurrentResident_Click
		private void btnUseCurrentResident_Click(object sender, System.EventArgs e)
		{		
			int iCnt = 0;
			foreach (ListViewItem lvi in this.listViewFaliure.Items)
			{
				if (lvi.Checked)
				{
					iCnt++;
				}
			}
			if (0 == iCnt)
			{
				MessageBox.Show("Tag items to process!", "Tag Items", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}

			
			foreach (ListViewItem lvi in this.listViewFaliure.Items)
			{
				if (lvi.Checked)
				{
					Guid piId = (Guid)lvi.Tag;
					PropertyItem pi = PropertyItem.LoadPropertyItem(piId);

					pi.Owner.FirstName = string.Empty;
					pi.Owner.MiddleName = string.Empty;
					pi.Owner.LastName = string.Empty;

					PropertyItem.SavePropertyItem(pi);					
				}
			}

			/*
			foreach(PropertyItem pi in unmatchedItems)
			{
				pi.Owner.FirstName="Current Resident";
				pi.Owner.MiddleName="";
				pi.Owner.LastName="";
				  
				session = Globals.factory.OpenSession();
			
				pi.NhSaveOrUpdate(session);
				session.Close();
			}*/
			//unmatchedItems.Clear();

			foreach (ListViewItem lvi in this.listViewFaliure.Items)
			{
				if (lvi.Checked)
					lvi.Remove();
			}						
			
			btnStartMatching.Visible = true;
			
		}
		#endregion
		#region btnStartMatchingManually_Click
		private void btnStartMatchingManually_Click(object sender, System.EventArgs e)
		{
			_updateAutomaticCounters = false;
			ArrayList alPiIDs = new ArrayList();
			foreach (ListViewItem lvi in this.listViewFaliure.Items)
			{
				if (lvi.Checked)
					alPiIDs.Add((Guid)lvi.Tag);
			}

			if (alPiIDs.Count == 0)							
			{
				MessageBox.Show("Tag items to manually match!", "Tag Items", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}

			
			serviceWorker.ManualStepping = true;
							
			for(int i = 0; i<unmatchedItems.Count; i++)
			{				
				PropertyItem pItem = unmatchedItems[i] as PropertyItem;
				if (null != pItem && !alPiIDs.Contains(pItem.IdPropertyItem))
					continue;				

				PropertyItemMailingData piid = new PropertyItemMailingData();				
				piid.CurrentPropertyItem = unmatchedItems[i] as PropertyItem;								

				piid.ShowDialog();
				SearchType searchType = piid.TypeOfSearch;

				PropertyItem pix =	piid.CurrentPropertyItem;

				if (null != pix.Owner && null != pix.Owner.SiteAddress && ExcludeZIP(pix.Owner.SiteAddress.SiteZIP))
				{					
					MessageBox.Show("Your request is out of the coverage area", "Out of coverage area", MessageBoxButtons.OK, MessageBoxIcon.Information);					
					continue;
				}
				
				if (piid.Match)
				{
					unmatchedItems[i] = pix;
					
					if (serviceWorker.resolveData(searchType, ref pix))
					{
						//unmatchedItems.RemoveAt(i);
						unmatchedItems.Remove(pix);
						//listViewFaliure.Items.RemoveAt(i);
						ListViewItem currLvi = null;
						foreach (ListViewItem tempLvi in this.listViewFaliure.Items)
						{
							if ((Guid)tempLvi.Tag == pix.IdPropertyItem)
							{
								currLvi = tempLvi;
								break;
							}
						}
						if (null != currLvi)
							listViewFaliure.Items.Remove(currLvi);						

						//MM NEW RULES
						//1.) Add to original group
						AssignToGroup(selectedNode.nodeId, pix.IdPropertyItem);
						//2.) remove from unmatched
						ApplicationHelper.DeletePropertyItemFromGroup(pix.IdPropertyItem, _notMatchedGroupId);	
						if (0 == ApplicationHelper.GetPropertyCountInNode(_notMatchedGroupId))						
						{
							ApplicationHelper.DeleteGroup(_notMatchedGroupId);						
							StepBEnabled = true;
						}
						//MM NEW RULES
						
						--faliure;
						lblUnsucessful.Text = faliure.ToString();

						--totalProc;
						lblTotalProcessed.Text = totalProc.ToString();
						
						serviceWorker_FireQueryPositivelySolvedEvent(pix, false, false, false);
						
						for(int j =0; j<listViewFaliure.Items.Count; j++)
						{
							int x = int.Parse(listViewFaliure.Items[j].SubItems[7].Text);
							if (x==i)
							{
								listViewFaliure.Items.RemoveAt(i);
							}
							else if (x > i)
							{
								--x;
								listViewFaliure.Items[j].SubItems[7].Text=x.ToString();
							}
						} 
						i--;
					}
					else
					{	
						DialogResult res = 
							MessageBox.Show("This query was not solved.", "Profit Grabber", MessageBoxButtons.AbortRetryIgnore);
						if (DialogResult.Retry == res)
						{
							
							i--;
						}
						else if (DialogResult.Ignore == res)
						{
							//fireqneg;	  NOT NECESERY... IT ALREADY IS NEGATIVE ;)
						}
						else
						{
							//   abort or X
							break;
						}
					}    
				}
				else if (piid.Skip)
				{
					// do nothing?
				}
				else if (piid.Cancel)
				{
					break;
				}
			}
			if (0 == unmatchedItems.Count)
				setSecondaryBtnGroupVisible(false);
			serviceWorker.ManualStepping = false;
		}
		#endregion

		#region REFRESH GROUPS
		private void RefreshGroupsBy123Sold(int nodeId)
		{
			this.RefreshGroups();
		}

		private delegate void RefreshGroupsDelegate();
		private void RefreshGroups()
		{
			/*
			if (true == this.groupsTreeView.InvokeRequired)
			{
				this.Invoke(new RefreshGroupsDelegate(RefreshGroups));
			}
			TreeNode tnTemp = this.groupsTreeView.SelectedTreeNode;
			this.groupsTreeView.UpdateTreeView();
			if (null != tnTemp)
			{
				this.groupsTreeView.SelectedTreeNode = tnTemp;
			}
			//tnTemp.Expand();
			*/
		}

		#endregion REFRESH GROUPS

		public bool StepBEnabled
		{
			get { return _stepB_Enabled; }
			set 
			{
				_stepB_Enabled = value;
				lv_StepB.Enabled = value;
				TagAllStep2.Enabled = value;
				UnTagAllStep2.Enabled = value;
				bFirstName.Enabled = value;
				bLeaveBlankStepB.Enabled = value;
				bDeleteTaggedStepB.Enabled = value;
			}
		}

		
		/// <summary>
		/// Process or not records flagged as (No Mail)
		/// 
		/// Kim H., April 17th, 2008:
		/// No Mail  - means the owner does not want direct marketing/solicitation mail delivered to that address
		/// </summary>
		public bool DoNotMail_NM_Records
		{
			get { return _doNotProcessNoMails.Checked; }
		}

		
		private void OnTagAll_Click(object sender, EventArgs e)
		{
			SetCheckedStateToUnSuccList(true);
		}

		private void OnUnTagAll_Click(object sender, EventArgs e)
		{
			SetCheckedStateToUnSuccList(false);
		}

		void SetCheckedStateToSuccList(bool state)
		{
			foreach (ListViewItem lvi in listViewSuccess.Items)			
				lvi.Checked = state;			
		}

		void SetCheckedStateToUnSuccList(bool state)
		{
			foreach (ListViewItem lvi in listViewFaliure.Items)			
				lvi.Checked = state;			
		}

		private void OnDearHomeOwner_Click(object sender, EventArgs e)
		{
			try
			{	
				if (-1 == _notMatchedGroupId)		
					_notMatchedGroupId = GetCreateGroup(selectedNode.nodeId, groupSuccess.unsuccessful);
				if (-1 == _notMatchedGroup_NotMailedId)		
					_notMatchedGroup_NotMailedId = GetCreateGroup(selectedNode.nodeId, groupSuccess.unsuccessful_notMailed);
				if (-1 == _notMatchedGroup_MailedWoNameId)		
					_notMatchedGroup_MailedWoNameId = GetCreateGroup(selectedNode.nodeId, groupSuccess.unsuccessful_mailedWoName);
			
				foreach (ListViewItem lvi in this.listViewFaliure.Items)
				{
					if (lvi.Checked)
					{										
						_willUseHomeownerCoutner++;

						Guid propertyItemId = (Guid)lvi.Tag;					
						PropertyItem pi = PropertyItem.LoadPropertyItem(propertyItemId);
						if (null != pi)
						{
							if (null == pi.Owner)
							{
								pi.Owner = new Contact();
								pi.Owner.save = true;
							}
							else 
								pi.Owner.save = false;

							pi.Owner.FirstName = string.Empty;	
							pi.Owner.LastName = string.Empty;
							pi.Owner.FullName = string.Empty;
							
							PropertyItem.SavePropertyItem(pi);

							//MM NEW RULES
							//1.) Add to original group
							AssignToGroup(selectedNode.nodeId, pi.IdPropertyItem);
							//1.) Add to Un-Matched - Mailed w / o name
							AssignToGroup(_notMatchedGroup_MailedWoNameId, pi.IdPropertyItem);
							//3.) remove from unmatched
							ApplicationHelper.DeletePropertyItemFromGroup(pi.IdPropertyItem, _notMatchedGroupId);							
							if (0 == ApplicationHelper.GetPropertyCountInNode(_notMatchedGroupId))							
							{
								ApplicationHelper.DeleteGroup(_notMatchedGroupId);							
								StepBEnabled = true;
							}
							//MM NEW RULES

							lvi.Remove();
						}
					}
				}
				_WillUseHomeowner.Text = _willUseHomeownerCoutner.ToString();
			}
			catch (Exception)
			{	}
		}

		void GetNames(string fullName, bool lastNameFirst, out string firstName, out string lastName)
        {
            firstName = string.Empty;
            lastName = string.Empty;

            try
            {
                fullName = fullName.Trim();
                int blankPos1 = fullName.IndexOf(" ");

                if (-1 == blankPos1) //single word
                {
                    if (lastNameFirst)
                        lastName = fullName;
                    else
                        firstName = fullName;

                    return;
                }
                    
                lastName = fullName.Substring(0, blankPos1).Trim();
                int blankPos2 = fullName.IndexOf(" ", blankPos1 + 1);

                if (-1 == blankPos2)
                    firstName = fullName.Substring(blankPos1 + 1, fullName.Length - (blankPos1) - 1);
                else
                    firstName = fullName.Substring(blankPos1 + 1, blankPos2 - (blankPos1) - 1);
            }
            catch
            {
                firstName = string.Empty;
                lastName = string.Empty;
            }

            if (!lastNameFirst)
            {
                string temp = firstName;
                firstName = lastName;
                lastName = temp;
            }
        }


        string GetFirstName(string fullName)
        {
            string firstName = string.Empty;

            try
            {
                fullName = fullName.Trim();
                int blankPos1 = fullName.IndexOf(" ");
                                
                if (-1 != blankPos1)
                    firstName = fullName.Substring(0, blankPos1);
                else
                    firstName = fullName;
            }
            catch
            {
                firstName = string.Empty;                
            }

            return firstName;
        }

        private void _removeMultipleFirstNames_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem lvi in this.lv_StepB.Items)
            {
                if (lvi.Checked)
                {
                    try
                    {
                        string alreadySeparatedFirstName = (string)lvi.SubItems[2].Text;

                        var etIdx = alreadySeparatedFirstName.IndexOf("&");
                        if (-1 != etIdx)
                        {
                            var firstName = alreadySeparatedFirstName.Substring(0, etIdx - 1);

                            Guid propertyItemId = (Guid)lvi.Tag;
                            PropertyItem pi = PropertyItem.LoadPropertyItem(propertyItemId);
                            if (null != pi)
                            {
                                if (null == pi.Owner)
                                {
                                    pi.Owner = new Contact();
                                    pi.Owner.save = true;
                                }
                                else
                                    pi.Owner.save = false;

                                pi.Owner.FirstName = firstName;
                                pi.Owner.FullName = $"{firstName} {pi.Owner.LastName}";

                                PropertyItem.SavePropertyItem(pi);

                                lvi.SubItems[2].Text = firstName;
                                lvi.SubItems[1].Text = pi.Owner.FullName;
                            }
                        }
                    }
                    catch
                    {
                        //just continue, isn't parsable
                    }
                }
            }
        }

        private void OnFirstName_Click(object sender, EventArgs e)
		{
			/*
			 * FINAL ALG:
					IF [FULL OWNER NAME] CONTAINS "/"     
					THEN take whatever is between the first and the second >space<, ADD �>space< & >space<�, Add whatever is between the first and the second >space< counting after �/� 
					(i.e. Powers Mark / Powers Katja --> Mark & Katja)
					

					IF [FULL OWNER NAME] DOES NOT CONTAIN "/"
					THEN take whatever is between the first and the second >space< 
					(i.e. Powers Mark --> Mark)
					
			
				//ADDED Nov, 11, 2007:
					1.    if the last names are the SAME:
						- take the first names and put in between �&�
						- put last name in the last name field
						* you�re already doing it this way!

					2.    if the last names are DIFFERENT:
						- take the first and last name from the first owner only, ignore the second owner



			 * */

            bool lastNameFirst = false;

            ResolveNameForm rnf = new ResolveNameForm();
            if (DialogResult.Cancel == rnf.ShowDialog())
                return;
            else
                lastNameFirst = rnf.LastNameFirst;

			
			foreach (ListViewItem lvi in this.lv_StepB.Items)
			{
				if (lvi.Checked)
				{						
					string lastName = string.Empty;
					string firstName = string.Empty;					

					if (null == lvi || null == lvi.SubItems || null == lvi.SubItems[1])
						firstName = string.Empty;

					else
					{
                        string fullOwnerName = (string)lvi.SubItems[1].Text;

                        //if (_forceSeparation.Checked)
                        //{
                        //    firstName = GetFirstName(fullOwnerName);
                        //}
                        //else
                        {                            
                            fullOwnerName = fullOwnerName.Trim();

                            if (-1 == fullOwnerName.IndexOf("/"))
                            {
                                GetNames(fullOwnerName, lastNameFirst, out firstName, out lastName);
                            }
                            else
                            {
                                int delimiterPos = fullOwnerName.IndexOf("/");
                                string firstOwner = string.Empty;
                                string secondOwner = string.Empty;

                                try { firstOwner = fullOwnerName.Substring(0, delimiterPos); }
                                catch { firstOwner = string.Empty; }

                                try { secondOwner = fullOwnerName.Substring(delimiterPos + 1, fullOwnerName.Length - (delimiterPos + 1)); }
                                catch { secondOwner = string.Empty; }

                                string aFirstName;
                                string bFirstName;
                                string aLastName;
                                string bLastName;

                                GetNames(firstOwner, lastNameFirst, out aFirstName, out aLastName);
                                GetNames(secondOwner, lastNameFirst, out bFirstName, out bLastName);



                                if (aLastName.ToUpper().Equals(bLastName.ToUpper()))
                                {
                                    lastName = aLastName;
                                    firstName = aFirstName + " & " + bFirstName;
                                }
                                else
                                {
                                    firstName = aFirstName;
                                    lastName = aLastName;
                                }
                            }
                        }
					}
										
					lvi.SubItems[2].Text = firstName;

					Guid propertyItemId = (Guid)lvi.Tag;					
					PropertyItem pi = PropertyItem.LoadPropertyItem(propertyItemId);
					if (null != pi)
					{
						if (null == pi.Owner)
						{
							pi.Owner = new Contact();
							pi.Owner.save = true;
						}
						else 
							pi.Owner.save = false;

						pi.Owner.FirstName = firstName;
						pi.Owner.LastName = lastName;

                        //Added per Shannon's request
                        string fullName = (string.Empty == firstName) ? lastName : firstName + " " + lastName;
                        pi.Owner.FullName = fullName;                        
							
						PropertyItem.SavePropertyItem(pi);
					}
				}
			}
        }

		private void OnDeleteTagged_Click(object sender, EventArgs e)
		{
			ArrayList alPropertyItemIds = new ArrayList();
			bool updateData = false;
			foreach (ListViewItem lvi in this.listViewSuccess.Items)
			{
				if (lvi.Checked)
				{
					Guid propertyItemId = (Guid)lvi.Tag;					
					alPropertyItemIds.Add(propertyItemId);
				}
			}
			if (alPropertyItemIds.Count > 0)
				ApplicationHelper.DeleteTaggedPropertyItems(alPropertyItemIds, out updateData);

			if (updateData)
			{
				foreach (ListViewItem lvi in this.listViewSuccess.Items)
				{
					if (lvi.Checked)
						lvi.Remove();
				}
			}
		}

		private void OnSelectGroup_Click(object sender, EventArgs e)
		{
			SelectGroupForm sgf = new SelectGroupForm();
			sgf.ShowDialog();

			if (sgf.Valid)
			{
				selectedNode = new Node(sgf.customizedTreeViewUserControl1.SelectedNodeId);
				selectedNode.nodeName = sgf.customizedTreeViewUserControl1.SelectedNodeName;				
				l_GroupName.Text = sgf.customizedTreeViewUserControl1.SelectedNodeName;		
				this.prepareForm();
			}
		}

		private void On_OverwriteExistingNames(object sender, EventArgs e)
		{
			serviceWorker.OverrideExistingNames = _overwriteExistingNames.Checked;
			UpdateLabelVisibility_AlreadyWithNames(!_overwriteExistingNames.Checked);
		}	
	
		private void On_OverwriteMailingAddress_Click(object sender, EventArgs e)
		{
			serviceWorker.OverrideExistingMailingAddress = _overwriteMailingAddress.Checked;
		}

		private void On_OverridePhoneNumber_Click(object sender, EventArgs e)
		{
			serviceWorker.OverrideExistingPhoneNumber = _overridePhoneNumber.Checked;
		}

		void UpdateLabelVisibility_AlreadyWithNames(bool enabled)
		{
			_alreadyWithNamesLabel.Enabled = enabled;
			_alreadyWithNamesValue.Enabled = enabled;
		}

		private void bLeaveBlankStepB_Click(object sender, System.EventArgs e)
		{
			foreach (ListViewItem lvi in this.lv_StepB.Items)
			{
				if (lvi.Checked)
				{										
					Guid propertyItemId = (Guid)lvi.Tag;					
					PropertyItem pi = PropertyItem.LoadPropertyItem(propertyItemId);
					if (null != pi)
					{
						if (null == pi.Owner)
						{
							pi.Owner = new Contact();
							pi.Owner.save = true;
						}
						else 
							pi.Owner.save = false;

						pi.Owner.FirstName = string.Empty;	
						pi.Owner.LastName = string.Empty;
						pi.Owner.FullName = string.Empty;
							
						PropertyItem.SavePropertyItem(pi);						
						lvi.SubItems[2].Text = string.Empty;
					}
				}
			}
			CheckSuccItemsOnStep2(false);
		}

		private void bDeleteTaggedStepB_Click(object sender, System.EventArgs e)
		{
			ArrayList alPropertyItemIds = new ArrayList();			
			foreach (ListViewItem lvi in this.lv_StepB.Items)
			{
				if (lvi.Checked)
				{
					Guid propertyItemId = (Guid)lvi.Tag;					
					alPropertyItemIds.Add(propertyItemId);
				}
			}
			if (alPropertyItemIds.Count > 0)
			{
				foreach (Guid piID in alPropertyItemIds)
				{
					//MM NEW RULES
					//1.) Add to Un-Matched - Not Mailed
					AssignToGroup(_notMatchedGroup_NotMailedId, piID);
					//2.) remove from unmatched
					Debug.Assert(selectedNode != null, "selected node is null!");
					ApplicationHelper.DeletePropertyItemFromGroup(piID, selectedNode.nodeId);											
					//MM NEW RULES
				}
			}				
			
			foreach (ListViewItem lvi in this.lv_StepB.Items)
			{
				if (lvi.Checked)
					lvi.Remove();
			}
			CheckSuccItemsOnStep2(false);			
		}

		private void TagAllStep2_Click(object sender, System.EventArgs e)
		{
			CheckSuccItemsOnStep2(true);
		}

		private void UnTagAllStep2_Click(object sender, System.EventArgs e)
		{
			CheckSuccItemsOnStep2(false);
		}

		void CheckSuccItemsOnStep2(bool checkedState)
		{
			foreach (ListViewItem lvi in this.lv_StepB.Items)			
				lvi.Checked = checkedState;			
		}

		private void _GoToStepB_Click(object sender, System.EventArgs e)
		{
            if (0 == this.listViewFaliure.Items.Count)
            {
                this.matchingModuleTabControl.SelectedIndex = 1;
                CheckSuccItemsOnStep2(true);
            }
            else
				MessageBox.Show("Please process unmatched records first", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

		private void lv_StepB_DoubleClick(object sender, System.EventArgs e)
		{
			if (null != this.lv_StepB.SelectedItems && 1 == this.lv_StepB.SelectedItems.Count)
			{
				ListViewItem lvi = this.lv_StepB.SelectedItems[0];
				EditFirstNameForm efnf = new EditFirstNameForm();
				efnf.FirstName = lvi.SubItems[2].Text;
				efnf.ShowDialog();

				if (efnf.ValidSelection)
				{
					lvi.SubItems[2].Text = efnf.FirstName;

					PropertyItem pi = PropertyItem.LoadPropertyItem((Guid)lvi.Tag);
					if (null != pi)
					{
						if (null == pi.Owner)
						{
							pi.Owner = new Contact();
							pi.Owner.save = true;
						}
						else 
							pi.Owner.save = false;

						pi.Owner.FirstName = efnf.FirstName;														
						PropertyItem.SavePropertyItem(pi);
					}					
				}
			}
		}

		bool ExcludeZIP(string zip)
		{
#if DO_NOT_USE_ZIP_FILTER
			return false;
#endif

            //BEGIN
            //MOD 10 is the flag for DO_NOT_USE_ZIP_FILTER compile switch
            if (null != Globals.activeModules && true == Globals.activeModules.MOD_10)
                return false;
            //END


			zip = zip.Trim();
			for (int i = 0; i < Globals.ZIPsToExclude.Length; i++)
			{
				if (Globals.ZIPsToExclude[i].Equals(zip))
					return true;
			}
			return false;
		}

		private void On_DoNotProcessNoMails_Click(object sender, EventArgs e)
		{
			string warningMsg = "Some owners have requested with their state to be on Do Not Mail list, so it is against the law for you to mail to those records.";
			warningMsg += Environment.NewLine;
			warningMsg += Environment.NewLine;

			warningMsg += "If you're going to use these records for mailing purposes, click NO to proceed and these records will be REMOVED.";
			warningMsg += Environment.NewLine;
			warningMsg += Environment.NewLine;

			warningMsg += "If you're going to use these records for other than mailing purposes, click YES to keep all the records.";
			warningMsg += Environment.NewLine;
			warningMsg += Environment.NewLine;
			warningMsg += Environment.NewLine;

			warningMsg += "Attention: By un-checking the box, you are agreeing to comply with all applicable laws and are assuming complete legal responsibility for your action.";

			if (!_doNotProcessNoMails.Checked)			
				if (DialogResult.Yes != MessageBox.Show(warningMsg, "WARNING - LEGAL NOTICE", MessageBoxButtons.YesNo, MessageBoxIcon.Information, MessageBoxDefaultButton.Button2))				
					_doNotProcessNoMails.Checked = true;											

			serviceWorker.DoNotProcess_NM_Records = DoNotMail_NM_Records;
		}

		private void On_UseAPN_Click(object sender, EventArgs e)
		{
			serviceWorker.UseAPN = _useAPN.Checked;			
		}

        void On_ForceSeparation_Click(object sender, EventArgs e)
        {
            if (_forceSeparation.Checked)
            {
                _selectGroupForForcedSeparation.Enabled = true;
                _selectedGroupName.Enabled = true;
                bDeleteTaggedStepB.Enabled = false;
            }
            else
            {
                _selectGroupForForcedSeparation.Enabled = false;
                _selectedGroupName.Enabled = false;
                bDeleteTaggedStepB.Enabled = true;
            }
        }

        //Force FIRST NAME separation on already matched group
        void On_SelectGroupForForcedSeparation_Click(object sender, EventArgs e)
        {
            SelectGroupForm sgf = new SelectGroupForm();
            sgf.ShowDialog();

            Node tempNode =null;

            if (sgf.Valid)
            {
                tempNode = new Node(sgf.customizedTreeViewUserControl1.SelectedNodeId);
                tempNode.nodeName = sgf.customizedTreeViewUserControl1.SelectedNodeName;
                _selectedGroupName.Text = sgf.customizedTreeViewUserControl1.SelectedNodeName;                
            }

            FillStepBListView(tempNode);
        }

        void FillStepBListView(Node selectedNode)
        {
            if (null == selectedNode)
                return;

            this.lv_StepB.Items.Clear();

            Hashtable propertyItemIds = ApplicationHelper.GetPropertyItemsInNode(selectedNode.nodeId);

            IEnumerator enumerator = propertyItemIds.Values.GetEnumerator();
            while (enumerator.MoveNext())
            {
                PropertyItem pi = PropertyItem.LoadPropertyItem((Guid)enumerator.Current);
                ListViewItem lvi = new ListViewItem(new string[] 
						{
							string.Empty,
							(null != pi && null != pi.Owner && null != pi.Owner.FullName ? pi.Owner.FullName : string.Empty),
							(null != pi && null != pi.Owner && null != pi.Owner.FirstName ? pi.Owner.FirstName : string.Empty),
							(null != pi && null != pi.Owner && null != pi.Owner.SiteAddress && null != pi.Owner.SiteAddress.FullSiteStreetAddress ? pi.Owner.SiteAddress.FullSiteStreetAddress : string.Empty),
							(null != pi && null != pi.Owner && null != pi.Owner.SiteAddress && null != pi.Owner.SiteAddress.SiteCity ? pi.Owner.SiteAddress.SiteCity : string.Empty),
							(null != pi && null != pi.Owner && null != pi.Owner.SiteAddress && null != pi.Owner.SiteAddress.SiteState ? pi.Owner.SiteAddress.SiteState : string.Empty),
							(null != pi && null != pi.Owner && null != pi.Owner.SiteAddress && null != pi.Owner.SiteAddress.SiteZIP ? pi.Owner.SiteAddress.SiteZIP : string.Empty),
							(null != pi && null != pi.Owner && null != pi.Owner.SiteAddress && null != pi.Owner.SiteAddress.SiteCountry ? pi.Owner.SiteAddress.SiteCountry : string.Empty), 
							(null != pi && null != pi.Owner && null != pi.Owner.MailAddress && null != pi.Owner.MailAddress.FullSiteStreetAddress) ? pi.Owner.MailAddress.FullSiteStreetAddress : string.Empty,
                            (null != pi && null != pi.Owner && null != pi.Owner.MailAddress && null != pi.Owner.MailAddress.SiteCity) ? pi.Owner.MailAddress.SiteCity : string.Empty,
                            (null != pi && null != pi.Owner && null != pi.Owner.MailAddress && null != pi.Owner.MailAddress.SiteState) ? pi.Owner.MailAddress.SiteState : string.Empty,                            
							(null != pi && null != pi.Owner && null != pi.Owner.MailAddress && null != pi.Owner.MailAddress.SiteCitySiteState) ? pi.Owner.MailAddress.SiteCitySiteState: string.Empty,							
							(null != pi && null != pi.Owner && null != pi.Owner.MailAddress && null != pi.Owner.MailAddress.SiteZIP) ? pi.Owner.MailAddress.SiteZIP : string.Empty, 
							(null != pi && null != pi.Owner && null != pi.Owner.HomePhone && null != pi.Owner.HomePhone.PhoneNr) ? pi.Owner.HomePhone.PhoneNr : string.Empty
						});
                lvi.Tag = pi.IdPropertyItem;
                lvi.SubItems[2].BackColor = Color.Yellow;
                this.lv_StepB.Items.Add(lvi);
            }
        }
	}
}
