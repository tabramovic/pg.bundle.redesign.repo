using System;
using System.IO;
using System.Reflection;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Runtime.Remoting.Contexts;
using System.Windows.Forms;

//Added
using DealMaker;
using DealMaker.PlainClasses.Utils;
using DealMaker.PlainClasses.Duplicates;
using DealMaker.Forms.Duplicates;

//nHibernate
using NHibernate;
using NHibernate.Cfg;
using Environment = NHibernate.Cfg.Environment;

namespace DealMaker.UserControls.Duplicates
{
	/// <summary>
	/// Summary description for PurgeDuplicatesControl.
	/// </summary>
	public class PurgeDuplicatesControl : System.Windows.Forms.UserControl
	{		
		ImageList _imgList = new ImageList();
		private System.Windows.Forms.NumericUpDown _nudMonths;
		private System.Windows.Forms.Button _DeleteRecords;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.TreeView _groups;
		private System.Windows.Forms.ContextMenu _groupsCtxMenu;
		private System.Windows.Forms.MenuItem _miGroupOnlySelect;
		private System.Windows.Forms.MenuItem _miGroupAndSubGroupsSelect;
		private System.Windows.Forms.MenuItem _miUnSelectGroupAndSubGroups;
		private System.Windows.Forms.MenuItem _miUnselectGroupOnly;
		private System.Windows.Forms.TabPage tabPage1;
		private System.Windows.Forms.TabControl _TabCtrl;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.Button _gotoStepB;
		private System.Windows.Forms.TabPage _tabMergeContacts;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.Button _scanForDuplicates_Merge;
		private System.Windows.Forms.ColumnHeader columnHeader1;
		private System.Windows.Forms.ColumnHeader columnHeader2;
		private System.Windows.Forms.ColumnHeader columnHeader3;
		private System.Windows.Forms.ColumnHeader columnHeader4;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.Button _unTagAll_Merge;
		private System.Windows.Forms.Button _tagAll_Merge;
		private System.Windows.Forms.ListView _lvDuplicates_Merge;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.RadioButton _rbNamesOnly;
		private System.Windows.Forms.RadioButton _rbAddressOnly;
		private System.Windows.Forms.RadioButton _rbBoth;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Button _deleteDuplicateRecords;
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.Button _merge;
		private System.Windows.Forms.Label _status;
		private System.Windows.Forms.GroupBox _deleteGroupBox;
		private System.Windows.Forms.GroupBox _mergeGroupBox;
		private System.Windows.Forms.RadioButton _keepLastModified;
		private System.Windows.Forms.RadioButton _keepFirstEntered;
		private System.Windows.Forms.ColumnHeader columnHeader5;
		private System.Windows.Forms.ColumnHeader columnHeader6;
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
		private System.Windows.Forms.RadioButton _rbEitherNameOrAddress;
        private CheckBox _cbMergeName;

		private bool asc = false;

		public PurgeDuplicatesControl()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();
			
			_gotoStepB.Click += new EventHandler(On_GotoStepB_Click);
			
			_lvDuplicates_Merge.ColumnClick += new ColumnClickEventHandler(On_LvDuplicates_Merge_ColumnClick);

			_tagAll_Merge.Click += new EventHandler(On_tagAll_Merge_Click);
			_unTagAll_Merge.Click += new EventHandler(On_unTagAll_Merge_Click);
			_scanForDuplicates_Merge.Click += new EventHandler(On_ScanForDuplicates_Merge_Click);
			_merge.Click += new EventHandler(On_Merge_Click);
			
			_deleteDuplicateRecords.Click += new EventHandler(On_DeleteDuplicates_Click);
			_DeleteRecords.Click += new EventHandler(On_DeleteRecords_Click);						
			_groups.MouseDown += new MouseEventHandler(On_Groups_MouseDown);
			

			_miGroupAndSubGroupsSelect.Click += new EventHandler(OnGroupAndSubGroupsSelect_Click);
			_miGroupOnlySelect.Click += new EventHandler(OnGroupOnlySelect_Click);
			_miUnSelectGroupAndSubGroups.Click += new EventHandler(OnUnSelectGroup_Click);
			_miUnselectGroupOnly.Click += new EventHandler(OnUnselectGroupOnly_Click);			

			DuplicateChecker.Instance.OnDuplicatesFound += new DealMaker.PlainClasses.Utils.DuplicateChecker.FoundDuplicatesHandler(OnDuplicatesFound);

			this.Load += new EventHandler(OnStep1_Load);

		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PurgeDuplicatesControl));
            this._nudMonths = new System.Windows.Forms.NumericUpDown();
            this._DeleteRecords = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this._groups = new System.Windows.Forms.TreeView();
            this._groupsCtxMenu = new System.Windows.Forms.ContextMenu();
            this._miGroupOnlySelect = new System.Windows.Forms.MenuItem();
            this._miGroupAndSubGroupsSelect = new System.Windows.Forms.MenuItem();
            this._miUnselectGroupOnly = new System.Windows.Forms.MenuItem();
            this._miUnSelectGroupAndSubGroups = new System.Windows.Forms.MenuItem();
            this._TabCtrl = new System.Windows.Forms.TabControl();
            this._tabMergeContacts = new System.Windows.Forms.TabPage();
            this._deleteGroupBox = new System.Windows.Forms.GroupBox();
            this._deleteDuplicateRecords = new System.Windows.Forms.Button();
            this._keepLastModified = new System.Windows.Forms.RadioButton();
            this._keepFirstEntered = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this._rbEitherNameOrAddress = new System.Windows.Forms.RadioButton();
            this.label11 = new System.Windows.Forms.Label();
            this._rbNamesOnly = new System.Windows.Forms.RadioButton();
            this._rbAddressOnly = new System.Windows.Forms.RadioButton();
            this._rbBoth = new System.Windows.Forms.RadioButton();
            this._scanForDuplicates_Merge = new System.Windows.Forms.Button();
            this._status = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this._unTagAll_Merge = new System.Windows.Forms.Button();
            this._tagAll_Merge = new System.Windows.Forms.Button();
            this._lvDuplicates_Merge = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.label7 = new System.Windows.Forms.Label();
            this._mergeGroupBox = new System.Windows.Forms.GroupBox();
            this._cbMergeName = new System.Windows.Forms.CheckBox();
            this._merge = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this._gotoStepB = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this._nudMonths)).BeginInit();
            this._TabCtrl.SuspendLayout();
            this._tabMergeContacts.SuspendLayout();
            this._deleteGroupBox.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this._mergeGroupBox.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.SuspendLayout();
            // 
            // _nudMonths
            // 
            this._nudMonths.Location = new System.Drawing.Point(120, 176);
            this._nudMonths.Maximum = new decimal(new int[] {
            12,
            0,
            0,
            0});
            this._nudMonths.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this._nudMonths.Name = "_nudMonths";
            this._nudMonths.Size = new System.Drawing.Size(48, 20);
            this._nudMonths.TabIndex = 10;
            this._nudMonths.Value = new decimal(new int[] {
            3,
            0,
            0,
            0});
            // 
            // _DeleteRecords
            // 
            this._DeleteRecords.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._DeleteRecords.Location = new System.Drawing.Point(432, 176);
            this._DeleteRecords.Name = "_DeleteRecords";
            this._DeleteRecords.Size = new System.Drawing.Size(75, 23);
            this._DeleteRecords.TabIndex = 11;
            this._DeleteRecords.Text = "Purge";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.Location = new System.Drawing.Point(8, 8);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(580, 64);
            this.label2.TabIndex = 13;
            this.label2.Text = resources.GetString("label2.Text");
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(168, 176);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(52, 23);
            this.label3.TabIndex = 14;
            this.label3.Text = "months";
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(8, 176);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(112, 23);
            this.label4.TabIndex = 15;
            this.label4.Text = "Delete older than";
            // 
            // _groups
            // 
            this._groups.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this._groups.ContextMenu = this._groupsCtxMenu;
            this._groups.Location = new System.Drawing.Point(8, 8);
            this._groups.Name = "_groups";
            this._groups.Size = new System.Drawing.Size(272, 568);
            this._groups.TabIndex = 16;
            // 
            // _groupsCtxMenu
            // 
            this._groupsCtxMenu.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this._miGroupOnlySelect,
            this._miGroupAndSubGroupsSelect,
            this._miUnselectGroupOnly,
            this._miUnSelectGroupAndSubGroups});
            // 
            // _miGroupOnlySelect
            // 
            this._miGroupOnlySelect.Index = 0;
            this._miGroupOnlySelect.Text = "Select Group only";
            // 
            // _miGroupAndSubGroupsSelect
            // 
            this._miGroupAndSubGroupsSelect.Index = 1;
            this._miGroupAndSubGroupsSelect.Text = "Select Group and all subgroups";
            // 
            // _miUnselectGroupOnly
            // 
            this._miUnselectGroupOnly.Index = 2;
            this._miUnselectGroupOnly.Text = "Unselect Group only";
            // 
            // _miUnSelectGroupAndSubGroups
            // 
            this._miUnSelectGroupAndSubGroups.Index = 3;
            this._miUnSelectGroupAndSubGroups.Text = "Unselect Group and all subgroups";
            // 
            // _TabCtrl
            // 
            this._TabCtrl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._TabCtrl.Controls.Add(this._tabMergeContacts);
            this._TabCtrl.Controls.Add(this.tabPage1);
            this._TabCtrl.Location = new System.Drawing.Point(288, 8);
            this._TabCtrl.Name = "_TabCtrl";
            this._TabCtrl.SelectedIndex = 0;
            this._TabCtrl.Size = new System.Drawing.Size(596, 568);
            this._TabCtrl.TabIndex = 19;
            // 
            // _tabMergeContacts
            // 
            this._tabMergeContacts.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this._tabMergeContacts.Controls.Add(this._deleteGroupBox);
            this._tabMergeContacts.Controls.Add(this.groupBox1);
            this._tabMergeContacts.Controls.Add(this.label10);
            this._tabMergeContacts.Controls.Add(this._unTagAll_Merge);
            this._tabMergeContacts.Controls.Add(this._tagAll_Merge);
            this._tabMergeContacts.Controls.Add(this._lvDuplicates_Merge);
            this._tabMergeContacts.Controls.Add(this.label7);
            this._tabMergeContacts.Controls.Add(this._mergeGroupBox);
            this._tabMergeContacts.Location = new System.Drawing.Point(4, 22);
            this._tabMergeContacts.Name = "_tabMergeContacts";
            this._tabMergeContacts.Size = new System.Drawing.Size(588, 542);
            this._tabMergeContacts.TabIndex = 2;
            this._tabMergeContacts.Text = "Scan, Merge and Delete Duplicates Contacts";
            // 
            // _deleteGroupBox
            // 
            this._deleteGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._deleteGroupBox.Controls.Add(this._deleteDuplicateRecords);
            this._deleteGroupBox.Controls.Add(this._keepLastModified);
            this._deleteGroupBox.Controls.Add(this._keepFirstEntered);
            this._deleteGroupBox.Enabled = false;
            this._deleteGroupBox.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._deleteGroupBox.Location = new System.Drawing.Point(336, 430);
            this._deleteGroupBox.Name = "_deleteGroupBox";
            this._deleteGroupBox.Size = new System.Drawing.Size(240, 106);
            this._deleteGroupBox.TabIndex = 35;
            this._deleteGroupBox.TabStop = false;
            this._deleteGroupBox.Text = "Delete Duplicates";
            // 
            // _deleteDuplicateRecords
            // 
            this._deleteDuplicateRecords.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._deleteDuplicateRecords.Location = new System.Drawing.Point(152, 16);
            this._deleteDuplicateRecords.Name = "_deleteDuplicateRecords";
            this._deleteDuplicateRecords.Size = new System.Drawing.Size(75, 23);
            this._deleteDuplicateRecords.TabIndex = 31;
            this._deleteDuplicateRecords.Text = "Delete";
            // 
            // _keepLastModified
            // 
            this._keepLastModified.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._keepLastModified.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._keepLastModified.Location = new System.Drawing.Point(6, 64);
            this._keepLastModified.Name = "_keepLastModified";
            this._keepLastModified.Size = new System.Drawing.Size(128, 24);
            this._keepLastModified.TabIndex = 30;
            this._keepLastModified.Text = "Keep Last Modified";
            // 
            // _keepFirstEntered
            // 
            this._keepFirstEntered.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._keepFirstEntered.Checked = true;
            this._keepFirstEntered.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._keepFirstEntered.Location = new System.Drawing.Point(6, 34);
            this._keepFirstEntered.Name = "_keepFirstEntered";
            this._keepFirstEntered.Size = new System.Drawing.Size(128, 24);
            this._keepFirstEntered.TabIndex = 29;
            this._keepFirstEntered.TabStop = true;
            this._keepFirstEntered.Text = "Keep First Entered";
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this._rbEitherNameOrAddress);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this._rbNamesOnly);
            this.groupBox1.Controls.Add(this._rbAddressOnly);
            this.groupBox1.Controls.Add(this._rbBoth);
            this.groupBox1.Controls.Add(this._scanForDuplicates_Merge);
            this.groupBox1.Controls.Add(this._status);
            this.groupBox1.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.groupBox1.Location = new System.Drawing.Point(8, 102);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(572, 96);
            this.groupBox1.TabIndex = 34;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Scan For Duplicates";
            // 
            // _rbEitherNameOrAddress
            // 
            this._rbEitherNameOrAddress.Location = new System.Drawing.Point(248, 40);
            this._rbEitherNameOrAddress.Name = "_rbEitherNameOrAddress";
            this._rbEitherNameOrAddress.Size = new System.Drawing.Size(192, 24);
            this._rbEitherNameOrAddress.TabIndex = 34;
            this._rbEitherNameOrAddress.Text = "Either Name or Address";
            // 
            // label11
            // 
            this.label11.Location = new System.Drawing.Point(10, 22);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(40, 23);
            this.label11.TabIndex = 30;
            this.label11.Text = "Use:";
            // 
            // _rbNamesOnly
            // 
            this._rbNamesOnly.Location = new System.Drawing.Point(64, 18);
            this._rbNamesOnly.Name = "_rbNamesOnly";
            this._rbNamesOnly.Size = new System.Drawing.Size(104, 24);
            this._rbNamesOnly.TabIndex = 31;
            this._rbNamesOnly.Text = "Full Name Only";
            // 
            // _rbAddressOnly
            // 
            this._rbAddressOnly.AutoSize = true;
            this._rbAddressOnly.Checked = true;
            this._rbAddressOnly.Location = new System.Drawing.Point(248, 16);
            this._rbAddressOnly.Name = "_rbAddressOnly";
            this._rbAddressOnly.Size = new System.Drawing.Size(142, 17);
            this._rbAddressOnly.TabIndex = 32;
            this._rbAddressOnly.TabStop = true;
            this._rbAddressOnly.Text = " Site Street Address Only";
            // 
            // _rbBoth
            // 
            this._rbBoth.Location = new System.Drawing.Point(64, 40);
            this._rbBoth.Name = "_rbBoth";
            this._rbBoth.Size = new System.Drawing.Size(176, 24);
            this._rbBoth.TabIndex = 33;
            this._rbBoth.Text = "Both Name and Address";
            // 
            // _scanForDuplicates_Merge
            // 
            this._scanForDuplicates_Merge.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._scanForDuplicates_Merge.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._scanForDuplicates_Merge.Location = new System.Drawing.Point(448, 16);
            this._scanForDuplicates_Merge.Name = "_scanForDuplicates_Merge";
            this._scanForDuplicates_Merge.Size = new System.Drawing.Size(112, 23);
            this._scanForDuplicates_Merge.TabIndex = 20;
            this._scanForDuplicates_Merge.Text = "&Scan For Duplicates";
            // 
            // _status
            // 
            this._status.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._status.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._status.Location = new System.Drawing.Point(64, 64);
            this._status.Name = "_status";
            this._status.Size = new System.Drawing.Size(496, 24);
            this._status.TabIndex = 25;
            this._status.Text = "Status: Not scanned";
            this._status.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label10
            // 
            this.label10.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label10.Location = new System.Drawing.Point(8, 40);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(580, 64);
            this.label10.TabIndex = 29;
            this.label10.Text = resources.GetString("label10.Text");
            // 
            // _unTagAll_Merge
            // 
            this._unTagAll_Merge.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._unTagAll_Merge.Enabled = false;
            this._unTagAll_Merge.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._unTagAll_Merge.Location = new System.Drawing.Point(96, 398);
            this._unTagAll_Merge.Name = "_unTagAll_Merge";
            this._unTagAll_Merge.Size = new System.Drawing.Size(80, 23);
            this._unTagAll_Merge.TabIndex = 28;
            this._unTagAll_Merge.Text = "&Untag All";
            // 
            // _tagAll_Merge
            // 
            this._tagAll_Merge.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._tagAll_Merge.Enabled = false;
            this._tagAll_Merge.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._tagAll_Merge.Location = new System.Drawing.Point(8, 398);
            this._tagAll_Merge.Name = "_tagAll_Merge";
            this._tagAll_Merge.Size = new System.Drawing.Size(80, 23);
            this._tagAll_Merge.TabIndex = 27;
            this._tagAll_Merge.Text = "&Tag All";
            // 
            // _lvDuplicates_Merge
            // 
            this._lvDuplicates_Merge.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._lvDuplicates_Merge.CheckBoxes = true;
            this._lvDuplicates_Merge.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader5,
            this.columnHeader6});
            this._lvDuplicates_Merge.FullRowSelect = true;
            this._lvDuplicates_Merge.GridLines = true;
            this._lvDuplicates_Merge.HideSelection = false;
            this._lvDuplicates_Merge.Location = new System.Drawing.Point(8, 206);
            this._lvDuplicates_Merge.Name = "_lvDuplicates_Merge";
            this._lvDuplicates_Merge.Size = new System.Drawing.Size(572, 184);
            this._lvDuplicates_Merge.TabIndex = 26;
            this._lvDuplicates_Merge.UseCompatibleStateImageBehavior = false;
            this._lvDuplicates_Merge.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "First Name";
            this.columnHeader1.Width = 118;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Middle Name";
            this.columnHeader2.Width = 87;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Last Name";
            this.columnHeader3.Width = 83;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Full Name";
            this.columnHeader4.Width = 166;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Full Site Street Address";
            this.columnHeader5.Width = 220;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "Site City, State and ZIP";
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label7.Location = new System.Drawing.Point(8, 8);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(580, 23);
            this.label7.TabIndex = 18;
            this.label7.Text = "Select all of the groups you wish to be included in the duplicate scan.";
            // 
            // _mergeGroupBox
            // 
            this._mergeGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._mergeGroupBox.Controls.Add(this._cbMergeName);
            this._mergeGroupBox.Controls.Add(this._merge);
            this._mergeGroupBox.Controls.Add(this.label12);
            this._mergeGroupBox.Enabled = false;
            this._mergeGroupBox.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._mergeGroupBox.Location = new System.Drawing.Point(8, 430);
            this._mergeGroupBox.Name = "_mergeGroupBox";
            this._mergeGroupBox.Size = new System.Drawing.Size(320, 106);
            this._mergeGroupBox.TabIndex = 36;
            this._mergeGroupBox.TabStop = false;
            this._mergeGroupBox.Text = "Merge Duplicates Data  - Typically used for Active MLS lists";
            // 
            // _cbMergeName
            // 
            this._cbMergeName.AutoSize = true;
            this._cbMergeName.Checked = true;
            this._cbMergeName.CheckState = System.Windows.Forms.CheckState.Checked;
            this._cbMergeName.Location = new System.Drawing.Point(13, 83);
            this._cbMergeName.Name = "_cbMergeName";
            this._cbMergeName.Size = new System.Drawing.Size(104, 17);
            this._cbMergeName.TabIndex = 4;
            this._cbMergeName.Text = "Copy Name Also";
            this._cbMergeName.UseVisualStyleBackColor = true;
            // 
            // _merge
            // 
            this._merge.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._merge.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._merge.Location = new System.Drawing.Point(239, 77);
            this._merge.Name = "_merge";
            this._merge.Size = new System.Drawing.Size(75, 23);
            this._merge.TabIndex = 3;
            this._merge.Text = "Merge";
            // 
            // label12
            // 
            this.label12.Location = new System.Drawing.Point(8, 19);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(306, 39);
            this.label12.TabIndex = 0;
            this.label12.Text = "This will copy ALL useful data from the FIRST ENTERED contacts to the LAST MODIFI" +
    "ED contacts.";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.tabPage1.Controls.Add(this._gotoStepB);
            this.tabPage1.Controls.Add(this.label8);
            this.tabPage1.Controls.Add(this.label5);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this._nudMonths);
            this.tabPage1.Controls.Add(this._DeleteRecords);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Size = new System.Drawing.Size(588, 542);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Clean (purge) Your Database";
            // 
            // _gotoStepB
            // 
            this._gotoStepB.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._gotoStepB.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._gotoStepB.Location = new System.Drawing.Point(411, 499);
            this._gotoStepB.Name = "_gotoStepB";
            this._gotoStepB.Size = new System.Drawing.Size(168, 23);
            this._gotoStepB.TabIndex = 18;
            this._gotoStepB.Text = "Scan && Delete Duplicates";
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label8.Location = new System.Drawing.Point(311, 504);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(97, 23);
            this.label8.TabIndex = 19;
            this.label8.Text = "Go to next Step";
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(8, 128);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(504, 40);
            this.label5.TabIndex = 17;
            this.label5.Text = "Please select groups and subgroups you wish to purge contacts from.";
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.Location = new System.Drawing.Point(8, 80);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(580, 40);
            this.label1.TabIndex = 16;
            this.label1.Text = "Backups of your database are made using backup functions of the Update Manager. T" +
    "here is a Video  Webinar on creating a backup, found under Help.";
            // 
            // PurgeDuplicatesControl
            // 
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.Controls.Add(this._TabCtrl);
            this.Controls.Add(this._groups);
            this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.Name = "PurgeDuplicatesControl";
            this.Size = new System.Drawing.Size(900, 592);
            ((System.ComponentModel.ISupportInitialize)(this._nudMonths)).EndInit();
            this._TabCtrl.ResumeLayout(false);
            this._tabMergeContacts.ResumeLayout(false);
            this._deleteGroupBox.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this._mergeGroupBox.ResumeLayout(false);
            this._mergeGroupBox.PerformLayout();
            this.tabPage1.ResumeLayout(false);
            this.ResumeLayout(false);

		}
		#endregion

		private void On_Merge_Click(object sender, EventArgs e)
		{
			ArrayList list = new ArrayList();
			
			int iCnt = 0;
			foreach (ListViewItem lvi in _lvDuplicates_Merge.Items)
			{
				if (lvi.Checked)
				{
					ArrayList lastModifiedList = new DuplicatePurgatory((ArrayList)_lvDuplicates_Merge.Items[iCnt].Tag, true).GetSortedList();
					ArrayList firstEnteredList = new DuplicatePurgatory((ArrayList)_lvDuplicates_Merge.Items[iCnt].Tag, false).GetSortedList();
										
					//TAKE FIRST ENTERED AND LAST MODIFIED
					ArrayList filteredList = new ArrayList();					
					filteredList.Add(firstEnteredList[0]);
					filteredList.Add(lastModifiedList[lastModifiedList.Count - 1]);

					list.Add(filteredList);					
				}				
				iCnt++;
			}
			
			if (null != list && list.Count > 0)
			{
				Progress progressForm = new Progress(list.Count, eProgressBarTypes.Merge);			
				DuplicateChecker.Instance.MergeContacts(list, _cbMergeName.Checked);
				progressForm.ShowDialog();	

				MessageBox.Show(this, $"Selected Contacts successfully merged!{System.Environment.NewLine}{System.Environment.NewLine}{System.Environment.NewLine}" +
                                      $"Standard steps for a new group is -{System.Environment.NewLine}{System.Environment.NewLine}" +
                                      $"1. Merge Duplicates - Already complete.{System.Environment.NewLine}" +
                                      $"2. Run Matching Module - only if you need names and mailing addresses.{System.Environment.NewLine}" +
                                      $"3. Get Phone & Email - Skip Trace - only if needed on new contacts.{System.Environment.NewLine}" +
                                      $"4. Create & Assign Skip Traced Groups - only if you do not skip trace the new group.{System.Environment.NewLine}{System.Environment.NewLine}" +
                                      $"Do it NOW before you forget!", "ProfitGrabber Pro", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
			else 
				MessageBox.Show(this, "Tag Contacts in the Listbox to Merge", "ProfitGrabber Pro", MessageBoxButtons.OK, MessageBoxIcon.Information);
		}
			
		private void On_DeleteDuplicates_Click(object sender, EventArgs e)
		{
			ArrayList list = new ArrayList();
			
			int iCnt = 0;
			foreach (ListViewItem lvi in _lvDuplicates_Merge.Items)
			{
				if (lvi.Checked)
				{
					ArrayList newList = new DuplicatePurgatory((ArrayList)_lvDuplicates_Merge.Items[iCnt].Tag, _keepLastModified.Checked).GetNewList();
					list.AddRange(newList);
				}				
				iCnt++;
			}
			
			if (null != list && list.Count > 0)
			{
				Progress progressForm = new Progress(list.Count, eProgressBarTypes.Delete);			
				DuplicateChecker.Instance.DeleteContacts((ContactEntry[])list.ToArray(typeof(ContactEntry)));
				progressForm.ShowDialog();
					
				RemoveTaggedItemsFromContactView(); //remove tagged from contactview
				MessageBox.Show(this, "Selected Contacts successfully deleted!", "ProfitGrabber Pro", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}	
		
		void RemoveTaggedItemsFromContactView()
		{
			foreach (ListViewItem lvi in _lvDuplicates_Merge.Items)
			{
				if (lvi.Checked)
					lvi.Remove();
			}
		}
		
		private void On_DeleteRecords_Click(object sender, EventArgs e)
		{	
			int[] selectedGroups = GetSelectedGroupIds();
			if (null == selectedGroups || 0 == selectedGroups.Length)
			{
				MessageBox.Show("Select groups which you want to purge", "Select Groups", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}

			ArrayList propertyItemsToDelete = DuplicateChecker.Instance.GetContactLastModifiedOlderThenInAffectedGroups((int)_nudMonths.Value, selectedGroups);

			if (0 == propertyItemsToDelete.Count)
			{
				MessageBox.Show("None of the selected groups contains contacts older than " + ((int)_nudMonths.Value).ToString() + " months", "Contacts not found", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
			else
			{
				if (DialogResult.No != MessageBox.Show(null, "Are you sure that you want to permanently delete filtered " + propertyItemsToDelete.Count.ToString() + " records from the program?" , 
					UserMessages.PermanentRecordDelete , 
					MessageBoxButtons.YesNo, MessageBoxIcon.Warning ))
				{				
					DuplicateChecker.Instance.DeletePropertyItems(propertyItemsToDelete);

					Progress progressForm = new Progress(propertyItemsToDelete.Count, eProgressBarTypes.Delete);			
					progressForm.ShowDialog();	
				}
			}
		}

		private void OnStep1_Load(object sender, EventArgs e)
		{
			if (null == _imgList.Images || 0 == _imgList.Images.Count)
			{
				LoadImageList();
				_groups.ImageList = _imgList;
			}

			TreeNode tn = new TreeNode(Globals.RootNode, 2, 2);
			tn.Tag = new Node(1);
			tn.Expand();

			TreeNode fullTreeNode = new GroupRenderer().BuildStructureFromScratch();			

			if (null != fullTreeNode)							
				this.InsertNode(1, fullTreeNode);			
		}

		delegate void InsertNodeDelegate(int idx, TreeNode fullTreeNode);
		void InsertNode(int idx, TreeNode fullTreeNode)
		{
			if (_groups.InvokeRequired)			
				this.Invoke(new InsertNodeDelegate(InsertNode), new object[] { idx, fullTreeNode });
			
			if (!_groups.Nodes.Contains(fullTreeNode))			
				_groups.Nodes.Insert(idx, fullTreeNode);			
			
			TreeNode fullTreeNodeClone = (TreeNode)fullTreeNode.Clone();
			fullTreeNodeClone.Expand();			
		}

		void LoadImageList()
		{
			_imgList.ColorDepth = ColorDepth.Depth16Bit;
			_imgList.ImageSize = new Size(16, 16);
			_imgList.TransparentColor = System.Drawing.Color.Transparent;
			

			Assembly assembly = this.GetType().Assembly;			

			Stream stream = assembly.GetManifestResourceStream("DealMaker.res.group_plus_subgroups.png"); 
			if (null != stream)
				_imgList.Images.Add(new Bitmap(stream)); 

			stream = assembly.GetManifestResourceStream("DealMaker.res.group.png"); 
			if (null != stream)
				_imgList.Images.Add(new Bitmap(stream)); 
			
			stream = assembly.GetManifestResourceStream("DealMaker.res.group_no_group.png"); 
			if (null != stream)
				_imgList.Images.Add(new Bitmap(stream)); 						
		}

		private void On_Groups_AfterSelect(object sender, TreeViewEventArgs e)
		{							
			e.Node.SelectedImageIndex = e.Node.ImageIndex;
		}
		
		private void On_Groups_BeforeSelect(object sender, TreeViewCancelEventArgs e)
		{
			e.Node.SelectedImageIndex = e.Node.ImageIndex;						
		}
		
		private void On_Groups_MouseDown(object sender, MouseEventArgs e)
		{
			HandleRightClick(sender, e);
		}

		private void On_GroupsToDelete_MouseDown(object sender, MouseEventArgs e)
		{
			HandleRightClick(sender, e);
		}
		void HandleRightClick(object sender, MouseEventArgs e)
		{
			if (e.Button == MouseButtons.Left)								
				return;
			
			Control ctrl = (Control)sender;
			Point clickedPoint = new Point(e.X, e.Y);
			TreeView tvNodes = (TreeView)sender;

			TreeNodeCollection nodes = tvNodes.Nodes;
			IEnumerator ieNodes = nodes.GetEnumerator();
			
			while (ieNodes.MoveNext())
			{
				TreeNode tn = (TreeNode)ieNodes.Current;
				GetSelectedNode(tn, clickedPoint, sender);
			}			
		}

		private void GetSelectedNode(TreeNode currentTreeNode, Point clickedPoint, object sender)
		{
			Rectangle rect = (Rectangle)currentTreeNode.Bounds;								
			Rectangle expandedRect = new Rectangle(0 , rect.Y, _groups.Width , rect.Height);

			if (expandedRect.Contains(clickedPoint))
			{				
				((TreeView)sender).SelectedNode = currentTreeNode;
				return;
			}
			else
			{
				TreeNodeCollection tnc = currentTreeNode.Nodes;
				IEnumerator ieNodes = tnc.GetEnumerator();

				while (ieNodes.MoveNext())
				{
					TreeNode tn = (TreeNode)ieNodes.Current;
					GetSelectedNode(tn, clickedPoint, sender);
				}				
			}			
		}		

		
		//SELECT GROUPS ONLY
		void OnGroupOnlySelect_Click(object sender, EventArgs e)
		{
			SelectGroupOnly();			
		}		

		void SelectGroupOnly()
		{
			TreeView treeOriginator = _groups;

			treeOriginator.SelectedNode.ImageIndex = _groups.SelectedNode.SelectedImageIndex = 1;
			
			object[] arr = (object[])treeOriginator.SelectedNode.Tag;
			arr[1] = GroupSelectionStatus.SelectedGroup;
			treeOriginator.SelectedNode.Tag = arr;
			
			treeOriginator.Refresh();		
		}

		//SELECT GROUPS AND SUBGROUPS
		void OnGroupAndSubGroupsSelect_Click(object sender, EventArgs e)
		{
			SelectGroupsAndSubGroups();			
		}		

		void SelectGroupsAndSubGroups()
		{
			TreeView treeOriginator = _groups;	
			ArrayList subNodes = GetChildrenForNode(treeOriginator.SelectedNode);									

			subNodes.Add(treeOriginator.SelectedNode);

			foreach (TreeNode tn in subNodes)	
			{
				tn.ImageIndex = tn.SelectedImageIndex = 0;
				object[] arr = (object[])tn.Tag;
				arr[1] = GroupSelectionStatus.SelectedGroupAndSubGroups;
				tn.Tag = arr;
			}
						
			treeOriginator.Refresh();
		}

		//UNSELECT GROUP ONLY
		private void OnUnselectGroupOnly_Click(object sender, EventArgs e)
		{
			UnSelectGroupOnly();			
		}

		
		void UnSelectGroupOnly()
		{
			TreeView treeOriginator = _groups;

			treeOriginator.SelectedNode.ImageIndex = treeOriginator.SelectedNode.SelectedImageIndex = 2;			

			object[] arr = (object[])_groups.SelectedNode.Tag;
			arr[1] = GroupSelectionStatus.NotSelectedGroup;
			treeOriginator.SelectedNode.Tag = arr;
						
			
			treeOriginator.Refresh();			
		}

				
		//UNSELECT GROUP AND SUBGROUPS
		void OnUnSelectGroup_Click(object sender, EventArgs e)
		{	
			UnSelectGroupAndSubGroups();		
		}

		void UnSelectGroupAndSubGroups()
		{
			TreeView treeOriginator = _groups;

			ArrayList subNodes = GetChildrenForNode(treeOriginator.SelectedNode);				
			subNodes.Add(treeOriginator.SelectedNode);

			foreach (TreeNode tn in subNodes)			
			{
				tn.ImageIndex = tn.SelectedImageIndex = 2;

				object[] arr = (object[])tn.Tag;
				arr[1] = GroupSelectionStatus.NotSelectedGroupAndSubGroups;
				tn.Tag = arr;
			}
			
			treeOriginator.Refresh();			
		}
				
		ArrayList GetChildrenForNode(TreeNode node)
		{
			ArrayList res = new ArrayList();

			//Add direct children
			IEnumerator enumerator = node.Nodes.GetEnumerator();
			while (enumerator.MoveNext())
				res.Add((TreeNode)enumerator.Current);


			foreach (TreeNode tn in node.Nodes)
			{
				ArrayList tempRes = GetChildrenForNode(tn);
				if (tempRes.Count > 0)
					res.AddRange(tempRes);
			}

			return res;
		}
		
		int[] GetSelectedGroupIds()
		{
			ArrayList nodeIds = new ArrayList();
			ArrayList nodes = GetChildrenForNode(_groups.Nodes[0]);

			foreach (TreeNode tn in nodes)
			{
				object[] arr = (object[])tn.Tag;

				if ((GroupSelectionStatus)arr[1] == GroupSelectionStatus.SelectedGroup || 
					(GroupSelectionStatus)arr[1] == GroupSelectionStatus.SelectedGroupAndSubGroups)
				{
					nodeIds.Add(((Node)arr[0]).nodeId);
				}
			}

			return (int[])nodeIds.ToArray(typeof(int));
		}

		private void On_FindDuplicates_Click(object sender, EventArgs e)
		{
			//CLEAR ITEMS
			_lvDuplicates_Merge.Items.Clear();
			EnableBottomControls(false);

			int[] selectedGroupsToMatch = GetSelectedGroupIds();

			if (null == selectedGroupsToMatch || 0 == selectedGroupsToMatch.Length)
			{
				MessageBox.Show("Select groups where to search for duplicates", "Select Groups", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}

			int cnt;
			string error;
			if (DuplicateChecker.Instance.GetContactCountInDB(selectedGroupsToMatch, out cnt, out error))		
			{
				_status.Text = cnt.ToString() + " contacts to scan...";
			}
			else
			{
#if DEBUG
				MessageBox.Show(error);
#endif
			}

			if (0 == cnt)
			{
				MessageBox.Show("Search in selected groups did not find any contacts", "No contacts found", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
			

			DuplicateChecker.Instance.LoadContactsInSelectedGroups(selectedGroupsToMatch, cnt);
			
			Progress progressForm = new Progress(cnt, eProgressBarTypes.Load);			
			progressForm.ShowDialog();	

			while (DuplicateChecker.Instance.Loading)
			{
				System.Threading.Thread.Sleep(1000);
			}

			if (!DuplicateChecker.Instance.ForcedStopLoading)
			{				
				DuplicateChecker.Instance.FindDuplicates(GetCriteria());
							
				progressForm = new Progress(cnt, eProgressBarTypes.Match);			
				progressForm.ShowDialog();								
			}
		}

        delegate void OnDuplicatesFoundDelegate(Hashtable duplicates);
		private void OnDuplicatesFound(Hashtable duplicates)
		{
            if (this.InvokeRequired)
                this.BeginInvoke(new OnDuplicatesFoundDelegate(OnDuplicatesFound), new object[] { duplicates });
            else
            {
                bool enableBottomControls = (null != duplicates && duplicates.Count > 0) ? true : false;
                _lvDuplicates_Merge.Items.Clear();
                _status.Text = duplicates.Count.ToString() + " matches.";

                IDictionaryEnumerator ide = duplicates.GetEnumerator();
                while (ide.MoveNext())
                {
                    ArrayList alDuplicates = (ArrayList)ide.Value;
                    ContactEntry ce1 = (ContactEntry)alDuplicates[0];
                    ContactEntry ce2 = (ContactEntry)alDuplicates[1];

                    var firstName = !string.IsNullOrWhiteSpace(ce1.FirstName) ? ce1.FirstName: ce2.FirstName;
                    var middleName = !string.IsNullOrWhiteSpace(ce1.MiddleName) ? ce1.MiddleName : ce2.MiddleName;
                    var lastName = !string.IsNullOrWhiteSpace(ce1.LastName) ? ce1.LastName : ce2.LastName;
                    var fullName = !string.IsNullOrWhiteSpace(ce1.FullName) ? ce1.FullName : ce2.FullName;
                    var fullSiteStreetAddress = !string.IsNullOrWhiteSpace(ce1.FullSiteStreetAddress) ? ce1.FullSiteStreetAddress : ce2.FullSiteStreetAddress;
                    var siteCitySiteStateSiteZIP = !string.IsNullOrWhiteSpace(ce1.SiteCitySiteStateSiteZIP) ? ce1.SiteCitySiteStateSiteZIP : ce2.SiteCitySiteStateSiteZIP;

                    ListViewItem lvi = new ListViewItem(firstName + "(" + alDuplicates.Count.ToString() + ")");
                    lvi.SubItems.Add(middleName);
                    lvi.SubItems.Add(lastName);
                    lvi.SubItems.Add(fullName);
                    lvi.SubItems.Add(fullSiteStreetAddress);
                    lvi.SubItems.Add(siteCitySiteStateSiteZIP);

                    lvi.Tag = alDuplicates;

                    _lvDuplicates_Merge.Items.Add(lvi);
                }

                EnableBottomControls(enableBottomControls);
                TagListViewMergeItems(true);
            }
		}

		private void On_GotoStepB_Click(object sender, EventArgs e)
		{
			_TabCtrl.SelectedTab = _TabCtrl.TabPages[1];
		}

		void EnableBottomControls(bool val)
		{			
			_tagAll_Merge.Enabled = val;
			_unTagAll_Merge.Enabled = val;
			_mergeGroupBox.Enabled = val;
			_deleteGroupBox.Enabled = val;
		}
				
		private void On_tagAll_Merge_Click(object sender, EventArgs e)
		{
			TagListViewMergeItems(true);
		}

		private void On_unTagAll_Merge_Click(object sender, EventArgs e)
		{
			TagListViewMergeItems(false);
		}

		void TagListViewMergeItems(bool val)
		{
			foreach (ListViewItem lvi in _lvDuplicates_Merge.Items)
				lvi.Checked = val;
		}

		private void On_ScanForDuplicates_Merge_Click(object sender, EventArgs e)
		{
			DuplicateChecker.Instance.ResetForcedAttributes();

			//CLEAR ITEMS
			_lvDuplicates_Merge.Items.Clear();			
			int[] selectedGroupsToMatch = GetSelectedGroupIds();

			if (null == selectedGroupsToMatch || 0 == selectedGroupsToMatch.Length)
			{
				MessageBox.Show("Select groups where to search for duplicates", "Select Groups", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}

			int cnt;
			string error;
			if (DuplicateChecker.Instance.GetContactCountInDB(selectedGroupsToMatch, out cnt, out error))		
			{
				_status.Text = cnt.ToString() + " contacts to scan...";
			}
			else
			{
#if DEBUG
				MessageBox.Show(error);
#endif
			}

			if (0 == cnt)
			{
				MessageBox.Show("Search in selected groups did not find any contacts", "No contacts found", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
			
			
			Progress progressForm = new Progress(cnt, eProgressBarTypes.Load);			
			DuplicateChecker.Instance.LoadContactsInSelectedGroups(selectedGroupsToMatch, cnt);
			progressForm.ShowDialog();	

			while (DuplicateChecker.Instance.Loading)
			{
				System.Threading.Thread.Sleep(1000);
			}

			if (!DuplicateChecker.Instance.ForcedStopLoading)
			{															
				progressForm = new Progress(cnt, eProgressBarTypes.Match);							
				DuplicateChecker.Instance.FindDuplicates(GetCriteria());
				progressForm.ShowDialog();
            }
		}

		FindDuplicatesCriteria GetCriteria()
		{
			FindDuplicatesCriteria criteria;
			if (_rbAddressOnly.Checked)
				criteria = FindDuplicatesCriteria.AddressOnly;
			else if (_rbNamesOnly.Checked)
				criteria = FindDuplicatesCriteria.NamesOnly;
			else if (_rbEitherNameOrAddress.Checked)
				criteria = FindDuplicatesCriteria.EitherNameOrAddress;
			else
				criteria = FindDuplicatesCriteria.Both;

			return criteria;
		}

		private void On_LvDuplicates_Merge_ColumnClick(object sender, ColumnClickEventArgs e)
		{
			this.asc = !this.asc;
			if (true == this.asc)
				_lvDuplicates_Merge.ListViewItemSorter = new ListViewItemComparer(e.Column);
			else
				_lvDuplicates_Merge.ListViewItemSorter = new ListViewItemComparerDesc(e.Column);
		}
	}
}
