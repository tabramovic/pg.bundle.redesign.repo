﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace DealMaker.UserControls
{
    public partial class Line : UserControl
    {
        public Line()
        {
            InitializeComponent();
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            Point p1, p2;

            p1 = new Point(0, (int)(this.Height / 2));
            p2 = new Point(this.Width, p1.Y);

            e.Graphics.DrawLine(new Pen(Color.FromArgb(47, 65, 131)), p1, p2);

            base.OnPaint(e);
        }
    }
}
