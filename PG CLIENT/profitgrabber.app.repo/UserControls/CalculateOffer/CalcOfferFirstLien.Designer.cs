﻿namespace DealMaker.UserControls.CalculateOffer
{
    partial class CalcOfferFirstLien
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this._IO_C_PropValue = new System.Windows.Forms.TextBox();
            this._IO_C_Repairs = new System.Windows.Forms.TextBox();
            this._IO_C_AsIsValue = new System.Windows.Forms.TextBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this._IO_SalesPrice = new System.Windows.Forms.TextBox();
            this._IO_REC_Value = new System.Windows.Forms.TextBox();
            this._IO_SC_Value = new System.Windows.Forms.TextBox();
            this._IO_CC_Value = new System.Windows.Forms.TextBox();
            this._IO_PayOff = new System.Windows.Forms.TextBox();
            this._MO_RealValue = new System.Windows.Forms.TextBox();
            this._MO_Repairs = new System.Windows.Forms.TextBox();
            this._MO_SalesPrice = new System.Windows.Forms.TextBox();
            this._MO_PayOff = new System.Windows.Forms.TextBox();
            this._MO_CC_Value = new System.Windows.Forms.TextBox();
            this._MO_SC_Value = new System.Windows.Forms.TextBox();
            this._MO_REC_Value = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this._IO_C_FirstOfferMultiplier = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this._IO_REC_Perc = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this._IO_SC_Perc = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this._IO_CC_Perc = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this._IO_Net = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this._IO_REC = new System.Windows.Forms.Button();
            this._IO_SC = new System.Windows.Forms.Button();
            this._IO_CC_L = new System.Windows.Forms.Button();
            this._IO_L = new System.Windows.Forms.Button();
            this.label23 = new System.Windows.Forms.Label();
            this._MO_Real_StrategyMult = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this._MO_CC_L = new System.Windows.Forms.Button();
            this._MO_SC = new System.Windows.Forms.Button();
            this._MO_REC = new System.Windows.Forms.Button();
            this._MO_Net = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this._MO_CC_Perc = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this._MO_SC_Perc = new System.Windows.Forms.TextBox();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this._MO_REC_Perc = new System.Windows.Forms.TextBox();
            this.label36 = new System.Windows.Forms.Label();
            this._getFromComps = new System.Windows.Forms.Button();
            this._getFromRepairs = new System.Windows.Forms.Button();
            this.line1 = new DealMaker.UserControls.Line();
            this.line2 = new DealMaker.UserControls.Line();
            this.line3 = new DealMaker.UserControls.Line();
            this._gbMax = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this._gbInit = new System.Windows.Forms.GroupBox();
            this._gbMax.SuspendLayout();
            this._gbInit.SuspendLayout();
            this.SuspendLayout();
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(56, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(168, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "CONSERVATIVE  Property Value:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(136, 43);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(88, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "minus Repairs of:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(152, 71);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(72, 13);
            this.label5.TabIndex = 5;
            this.label5.Text = "\"As is\" Value:";
            // 
            // _IO_C_PropValue
            // 
            this._IO_C_PropValue.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._IO_C_PropValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._IO_C_PropValue.Location = new System.Drawing.Point(230, 19);
            this._IO_C_PropValue.Name = "_IO_C_PropValue";
            this._IO_C_PropValue.Size = new System.Drawing.Size(83, 20);
            this._IO_C_PropValue.TabIndex = 6;
            this._IO_C_PropValue.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.toolTip1.SetToolTip(this._IO_C_PropValue, "From COMPS tab");
            // 
            // _IO_C_Repairs
            // 
            this._IO_C_Repairs.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._IO_C_Repairs.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._IO_C_Repairs.Location = new System.Drawing.Point(230, 40);
            this._IO_C_Repairs.Name = "_IO_C_Repairs";
            this._IO_C_Repairs.Size = new System.Drawing.Size(83, 20);
            this._IO_C_Repairs.TabIndex = 8;
            this._IO_C_Repairs.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.toolTip1.SetToolTip(this._IO_C_Repairs, "from REPAIRS tab");
            // 
            // _IO_C_AsIsValue
            // 
            this._IO_C_AsIsValue.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._IO_C_AsIsValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._IO_C_AsIsValue.Location = new System.Drawing.Point(230, 68);
            this._IO_C_AsIsValue.Name = "_IO_C_AsIsValue";
            this._IO_C_AsIsValue.ReadOnly = true;
            this._IO_C_AsIsValue.Size = new System.Drawing.Size(83, 20);
            this._IO_C_AsIsValue.TabIndex = 10;
            this._IO_C_AsIsValue.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _IO_SalesPrice
            // 
            this._IO_SalesPrice.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._IO_SalesPrice.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._IO_SalesPrice.Location = new System.Drawing.Point(230, 94);
            this._IO_SalesPrice.Name = "_IO_SalesPrice";
            this._IO_SalesPrice.ReadOnly = true;
            this._IO_SalesPrice.Size = new System.Drawing.Size(83, 20);
            this._IO_SalesPrice.TabIndex = 12;
            this._IO_SalesPrice.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.toolTip1.SetToolTip(this._IO_SalesPrice, "From COMPS tab");
            // 
            // _IO_REC_Value
            // 
            this._IO_REC_Value.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._IO_REC_Value.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._IO_REC_Value.Location = new System.Drawing.Point(230, 115);
            this._IO_REC_Value.Name = "_IO_REC_Value";
            this._IO_REC_Value.ReadOnly = true;
            this._IO_REC_Value.Size = new System.Drawing.Size(83, 20);
            this._IO_REC_Value.TabIndex = 14;
            this._IO_REC_Value.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.toolTip1.SetToolTip(this._IO_REC_Value, "From COMPS tab");
            // 
            // _IO_SC_Value
            // 
            this._IO_SC_Value.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._IO_SC_Value.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._IO_SC_Value.Location = new System.Drawing.Point(230, 136);
            this._IO_SC_Value.Name = "_IO_SC_Value";
            this._IO_SC_Value.ReadOnly = true;
            this._IO_SC_Value.Size = new System.Drawing.Size(83, 20);
            this._IO_SC_Value.TabIndex = 17;
            this._IO_SC_Value.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.toolTip1.SetToolTip(this._IO_SC_Value, "From COMPS tab");
            // 
            // _IO_CC_Value
            // 
            this._IO_CC_Value.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._IO_CC_Value.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._IO_CC_Value.Location = new System.Drawing.Point(230, 157);
            this._IO_CC_Value.Name = "_IO_CC_Value";
            this._IO_CC_Value.ReadOnly = true;
            this._IO_CC_Value.Size = new System.Drawing.Size(83, 20);
            this._IO_CC_Value.TabIndex = 20;
            this._IO_CC_Value.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.toolTip1.SetToolTip(this._IO_CC_Value, "From COMPS tab");
            // 
            // _IO_PayOff
            // 
            this._IO_PayOff.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._IO_PayOff.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._IO_PayOff.Location = new System.Drawing.Point(230, 178);
            this._IO_PayOff.Name = "_IO_PayOff";
            this._IO_PayOff.Size = new System.Drawing.Size(83, 20);
            this._IO_PayOff.TabIndex = 22;
            this._IO_PayOff.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.toolTip1.SetToolTip(this._IO_PayOff, "From COMPS tab");
            // 
            // _MO_RealValue
            // 
            this._MO_RealValue.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._MO_RealValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._MO_RealValue.Location = new System.Drawing.Point(222, 19);
            this._MO_RealValue.Name = "_MO_RealValue";
            this._MO_RealValue.Size = new System.Drawing.Size(83, 20);
            this._MO_RealValue.TabIndex = 40;
            this._MO_RealValue.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.toolTip1.SetToolTip(this._MO_RealValue, "From COMPS tab");
            // 
            // _MO_Repairs
            // 
            this._MO_Repairs.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._MO_Repairs.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._MO_Repairs.Location = new System.Drawing.Point(222, 40);
            this._MO_Repairs.Name = "_MO_Repairs";
            this._MO_Repairs.Size = new System.Drawing.Size(83, 20);
            this._MO_Repairs.TabIndex = 42;
            this._MO_Repairs.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.toolTip1.SetToolTip(this._MO_Repairs, "from REPAIRS tab");
            // 
            // _MO_SalesPrice
            // 
            this._MO_SalesPrice.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._MO_SalesPrice.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._MO_SalesPrice.Location = new System.Drawing.Point(222, 61);
            this._MO_SalesPrice.Name = "_MO_SalesPrice";
            this._MO_SalesPrice.ReadOnly = true;
            this._MO_SalesPrice.Size = new System.Drawing.Size(83, 20);
            this._MO_SalesPrice.TabIndex = 43;
            this._MO_SalesPrice.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.toolTip1.SetToolTip(this._MO_SalesPrice, "from REPAIRS tab");
            // 
            // _MO_PayOff
            // 
            this._MO_PayOff.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._MO_PayOff.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._MO_PayOff.Location = new System.Drawing.Point(222, 145);
            this._MO_PayOff.Name = "_MO_PayOff";
            this._MO_PayOff.Size = new System.Drawing.Size(83, 20);
            this._MO_PayOff.TabIndex = 53;
            this._MO_PayOff.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.toolTip1.SetToolTip(this._MO_PayOff, "From COMPS tab");
            // 
            // _MO_CC_Value
            // 
            this._MO_CC_Value.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._MO_CC_Value.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._MO_CC_Value.Location = new System.Drawing.Point(222, 124);
            this._MO_CC_Value.Name = "_MO_CC_Value";
            this._MO_CC_Value.ReadOnly = true;
            this._MO_CC_Value.Size = new System.Drawing.Size(83, 20);
            this._MO_CC_Value.TabIndex = 51;
            this._MO_CC_Value.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.toolTip1.SetToolTip(this._MO_CC_Value, "From COMPS tab");
            // 
            // _MO_SC_Value
            // 
            this._MO_SC_Value.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._MO_SC_Value.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._MO_SC_Value.Location = new System.Drawing.Point(222, 103);
            this._MO_SC_Value.Name = "_MO_SC_Value";
            this._MO_SC_Value.ReadOnly = true;
            this._MO_SC_Value.Size = new System.Drawing.Size(83, 20);
            this._MO_SC_Value.TabIndex = 48;
            this._MO_SC_Value.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.toolTip1.SetToolTip(this._MO_SC_Value, "From COMPS tab");
            // 
            // _MO_REC_Value
            // 
            this._MO_REC_Value.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._MO_REC_Value.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._MO_REC_Value.Location = new System.Drawing.Point(222, 82);
            this._MO_REC_Value.Name = "_MO_REC_Value";
            this._MO_REC_Value.ReadOnly = true;
            this._MO_REC_Value.Size = new System.Drawing.Size(83, 20);
            this._MO_REC_Value.TabIndex = 45;
            this._MO_REC_Value.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.toolTip1.SetToolTip(this._MO_REC_Value, "From COMPS tab");
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(315, 71);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(97, 13);
            this.label9.TabIndex = 12;
            this.label9.Text = "x First Offer Multip.:";
            // 
            // _IO_C_FirstOfferMultiplier
            // 
            this._IO_C_FirstOfferMultiplier.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._IO_C_FirstOfferMultiplier.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._IO_C_FirstOfferMultiplier.Location = new System.Drawing.Point(421, 68);
            this._IO_C_FirstOfferMultiplier.Name = "_IO_C_FirstOfferMultiplier";
            this._IO_C_FirstOfferMultiplier.Size = new System.Drawing.Size(30, 20);
            this._IO_C_FirstOfferMultiplier.TabIndex = 11;
            this._IO_C_FirstOfferMultiplier.Text = "0.75";
            this._IO_C_FirstOfferMultiplier.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(20, 97);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(183, 13);
            this.label10.TabIndex = 14;
            this.label10.Text = "INITIAL OFFER SALES PRICE:";
            // 
            // _IO_REC_Perc
            // 
            this._IO_REC_Perc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._IO_REC_Perc.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._IO_REC_Perc.Location = new System.Drawing.Point(178, 115);
            this._IO_REC_Perc.Name = "_IO_REC_Perc";
            this._IO_REC_Perc.Size = new System.Drawing.Size(31, 20);
            this._IO_REC_Perc.TabIndex = 13;
            this._IO_REC_Perc.Text = "6";
            this._IO_REC_Perc.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(18, 118);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(153, 13);
            this.label11.TabIndex = 16;
            this.label11.Text = "minus Real Estate Commission:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(211, 118);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(15, 13);
            this.label12.TabIndex = 18;
            this.label12.Text = "%";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(211, 139);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(15, 13);
            this.label13.TabIndex = 22;
            this.label13.Text = "%";
            // 
            // _IO_SC_Perc
            // 
            this._IO_SC_Perc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._IO_SC_Perc.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._IO_SC_Perc.Location = new System.Drawing.Point(178, 136);
            this._IO_SC_Perc.Name = "_IO_SC_Perc";
            this._IO_SC_Perc.Size = new System.Drawing.Size(31, 20);
            this._IO_SC_Perc.TabIndex = 16;
            this._IO_SC_Perc.Text = "3";
            this._IO_SC_Perc.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(40, 139);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(131, 13);
            this.label14.TabIndex = 20;
            this.label14.Text = "minus Seller\'s Concession:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(211, 160);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(15, 13);
            this.label15.TabIndex = 26;
            this.label15.Text = "%";
            // 
            // _IO_CC_Perc
            // 
            this._IO_CC_Perc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._IO_CC_Perc.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._IO_CC_Perc.Location = new System.Drawing.Point(178, 157);
            this._IO_CC_Perc.Name = "_IO_CC_Perc";
            this._IO_CC_Perc.Size = new System.Drawing.Size(31, 20);
            this._IO_CC_Perc.TabIndex = 19;
            this._IO_CC_Perc.Text = "1";
            this._IO_CC_Perc.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(75, 160);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(98, 13);
            this.label16.TabIndex = 24;
            this.label16.Text = "minus Closing Cost:";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(75, 182);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(91, 13);
            this.label17.TabIndex = 28;
            this.label17.Text = "minus 2nd Payoff:";
            // 
            // _IO_Net
            // 
            this._IO_Net.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._IO_Net.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._IO_Net.Location = new System.Drawing.Point(230, 206);
            this._IO_Net.Name = "_IO_Net";
            this._IO_Net.ReadOnly = true;
            this._IO_Net.Size = new System.Drawing.Size(83, 20);
            this._IO_Net.TabIndex = 23;
            this._IO_Net.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this._IO_Net.TextChanged += new System.EventHandler(this._IO_Net_TextChanged);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(2, 209);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(224, 13);
            this.label20.TabIndex = 32;
            this.label20.Text = "INITIAL OFFER NET TO 1st LENDER:";
            // 
            // _IO_REC
            // 
            this._IO_REC.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._IO_REC.Location = new System.Drawing.Point(317, 115);
            this._IO_REC.Name = "_IO_REC";
            this._IO_REC.Size = new System.Drawing.Size(83, 20);
            this._IO_REC.TabIndex = 15;
            this._IO_REC.Text = "Breakdown";
            this._IO_REC.UseVisualStyleBackColor = true;
            // 
            // _IO_SC
            // 
            this._IO_SC.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._IO_SC.Location = new System.Drawing.Point(317, 136);
            this._IO_SC.Name = "_IO_SC";
            this._IO_SC.Size = new System.Drawing.Size(83, 20);
            this._IO_SC.TabIndex = 18;
            this._IO_SC.Text = "Breakdown";
            this._IO_SC.UseVisualStyleBackColor = true;
            // 
            // _IO_CC_L
            // 
            this._IO_CC_L.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._IO_CC_L.Location = new System.Drawing.Point(317, 157);
            this._IO_CC_L.Name = "_IO_CC_L";
            this._IO_CC_L.Size = new System.Drawing.Size(83, 20);
            this._IO_CC_L.TabIndex = 21;
            this._IO_CC_L.Text = "Breakdown";
            this._IO_CC_L.UseVisualStyleBackColor = true;
            // 
            // _IO_L
            // 
            this._IO_L.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._IO_L.Location = new System.Drawing.Point(317, 206);
            this._IO_L.Name = "_IO_L";
            this._IO_L.Size = new System.Drawing.Size(83, 20);
            this._IO_L.TabIndex = 24;
            this._IO_L.Text = "Open HUD";
            this._IO_L.UseVisualStyleBackColor = true;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(79, 22);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(137, 13);
            this.label23.TabIndex = 41;
            this.label23.Text = "REALISTIC Property Value:";
            // 
            // _MO_Real_StrategyMult
            // 
            this._MO_Real_StrategyMult.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._MO_Real_StrategyMult.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._MO_Real_StrategyMult.Location = new System.Drawing.Point(396, 21);
            this._MO_Real_StrategyMult.Name = "_MO_Real_StrategyMult";
            this._MO_Real_StrategyMult.Size = new System.Drawing.Size(30, 20);
            this._MO_Real_StrategyMult.TabIndex = 41;
            this._MO_Real_StrategyMult.Text = "0.85";
            this._MO_Real_StrategyMult.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(307, 22);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(37, 13);
            this.label24.TabIndex = 44;
            this.label24.Text = "x Your";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(128, 43);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(88, 13);
            this.label27.TabIndex = 46;
            this.label27.Text = "minus Repairs of:";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(4, 63);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(198, 13);
            this.label26.TabIndex = 48;
            this.label26.Text = "MAXIMUM OFFER SALES PRICE:";
            // 
            // _MO_CC_L
            // 
            this._MO_CC_L.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._MO_CC_L.Location = new System.Drawing.Point(311, 124);
            this._MO_CC_L.Name = "_MO_CC_L";
            this._MO_CC_L.Size = new System.Drawing.Size(83, 20);
            this._MO_CC_L.TabIndex = 52;
            this._MO_CC_L.Text = "Breakdown";
            this._MO_CC_L.UseVisualStyleBackColor = true;
            // 
            // _MO_SC
            // 
            this._MO_SC.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._MO_SC.Location = new System.Drawing.Point(311, 103);
            this._MO_SC.Name = "_MO_SC";
            this._MO_SC.Size = new System.Drawing.Size(83, 20);
            this._MO_SC.TabIndex = 49;
            this._MO_SC.Text = "Breakdown";
            this._MO_SC.UseVisualStyleBackColor = true;
            // 
            // _MO_REC
            // 
            this._MO_REC.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._MO_REC.Location = new System.Drawing.Point(311, 82);
            this._MO_REC.Name = "_MO_REC";
            this._MO_REC.Size = new System.Drawing.Size(83, 20);
            this._MO_REC.TabIndex = 46;
            this._MO_REC.Text = "Breakdown";
            this._MO_REC.UseVisualStyleBackColor = true;
            // 
            // _MO_Net
            // 
            this._MO_Net.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._MO_Net.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._MO_Net.Location = new System.Drawing.Point(222, 172);
            this._MO_Net.Name = "_MO_Net";
            this._MO_Net.ReadOnly = true;
            this._MO_Net.Size = new System.Drawing.Size(83, 20);
            this._MO_Net.TabIndex = 54;
            this._MO_Net.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(5, 176);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(210, 13);
            this.label28.TabIndex = 66;
            this.label28.Text = "MAX. OFFER NET TO 1st LENDER:";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(62, 145);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(91, 13);
            this.label30.TabIndex = 62;
            this.label30.Text = "minus 2nd Payoff:";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(201, 127);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(15, 13);
            this.label31.TabIndex = 60;
            this.label31.Text = "%";
            // 
            // _MO_CC_Perc
            // 
            this._MO_CC_Perc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._MO_CC_Perc.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._MO_CC_Perc.Location = new System.Drawing.Point(168, 124);
            this._MO_CC_Perc.Name = "_MO_CC_Perc";
            this._MO_CC_Perc.Size = new System.Drawing.Size(31, 20);
            this._MO_CC_Perc.TabIndex = 50;
            this._MO_CC_Perc.Text = "1";
            this._MO_CC_Perc.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(62, 127);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(98, 13);
            this.label32.TabIndex = 58;
            this.label32.Text = "minus Closing Cost:";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(201, 106);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(15, 13);
            this.label33.TabIndex = 56;
            this.label33.Text = "%";
            // 
            // _MO_SC_Perc
            // 
            this._MO_SC_Perc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._MO_SC_Perc.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._MO_SC_Perc.Location = new System.Drawing.Point(168, 103);
            this._MO_SC_Perc.Name = "_MO_SC_Perc";
            this._MO_SC_Perc.Size = new System.Drawing.Size(31, 20);
            this._MO_SC_Perc.TabIndex = 47;
            this._MO_SC_Perc.Text = "3";
            this._MO_SC_Perc.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.Location = new System.Drawing.Point(27, 106);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(131, 13);
            this.label34.TabIndex = 54;
            this.label34.Text = "minus Seller\'s Concession:";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.Location = new System.Drawing.Point(201, 85);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(15, 13);
            this.label35.TabIndex = 52;
            this.label35.Text = "%";
            // 
            // _MO_REC_Perc
            // 
            this._MO_REC_Perc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._MO_REC_Perc.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._MO_REC_Perc.Location = new System.Drawing.Point(168, 82);
            this._MO_REC_Perc.Name = "_MO_REC_Perc";
            this._MO_REC_Perc.Size = new System.Drawing.Size(31, 20);
            this._MO_REC_Perc.TabIndex = 44;
            this._MO_REC_Perc.Text = "6";
            this._MO_REC_Perc.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.Location = new System.Drawing.Point(5, 85);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(153, 13);
            this.label36.TabIndex = 50;
            this.label36.Text = "minus Real Estate Commission:";
            // 
            // _getFromComps
            // 
            this._getFromComps.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._getFromComps.Location = new System.Drawing.Point(319, 19);
            this._getFromComps.Name = "_getFromComps";
            this._getFromComps.Size = new System.Drawing.Size(137, 20);
            this._getFromComps.TabIndex = 7;
            this._getFromComps.Text = "Get Property Value";
            this._getFromComps.UseVisualStyleBackColor = true;
            // 
            // _getFromRepairs
            // 
            this._getFromRepairs.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._getFromRepairs.Location = new System.Drawing.Point(319, 39);
            this._getFromRepairs.Name = "_getFromRepairs";
            this._getFromRepairs.Size = new System.Drawing.Size(137, 20);
            this._getFromRepairs.TabIndex = 9;
            this._getFromRepairs.Text = "Get Repairs Estimate";
            this._getFromRepairs.UseVisualStyleBackColor = true;
            // 
            // line1
            // 
            this.line1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.line1.Location = new System.Drawing.Point(57, 59);
            this.line1.Name = "line1";
            this.line1.Size = new System.Drawing.Size(261, 10);
            this.line1.TabIndex = 76;
            // 
            // line2
            // 
            this.line2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.line2.Location = new System.Drawing.Point(57, 196);
            this.line2.Name = "line2";
            this.line2.Size = new System.Drawing.Size(261, 10);
            this.line2.TabIndex = 77;
            // 
            // line3
            // 
            this.line3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.line3.Location = new System.Drawing.Point(49, 163);
            this.line3.Name = "line3";
            this.line3.Size = new System.Drawing.Size(261, 10);
            this.line3.TabIndex = 78;
            // 
            // _gbMax
            // 
            this._gbMax.Controls.Add(this._MO_Real_StrategyMult);
            this._gbMax.Controls.Add(this.label6);
            this._gbMax.Controls.Add(this.label2);
            this._gbMax.Controls.Add(this.label1);
            this._gbMax.Controls.Add(this._MO_SalesPrice);
            this._gbMax.Controls.Add(this._MO_REC_Perc);
            this._gbMax.Controls.Add(this._MO_SC_Perc);
            this._gbMax.Controls.Add(this._MO_CC_Perc);
            this._gbMax.Controls.Add(this._MO_PayOff);
            this._gbMax.Controls.Add(this._MO_RealValue);
            this._gbMax.Controls.Add(this._MO_Repairs);
            this._gbMax.Controls.Add(this._MO_REC_Value);
            this._gbMax.Controls.Add(this._MO_Net);
            this._gbMax.Controls.Add(this._MO_SC_Value);
            this._gbMax.Controls.Add(this._MO_CC_Value);
            this._gbMax.Controls.Add(this.label23);
            this._gbMax.Controls.Add(this.label24);
            this._gbMax.Controls.Add(this.label27);
            this._gbMax.Controls.Add(this.line3);
            this._gbMax.Controls.Add(this.label26);
            this._gbMax.Controls.Add(this.label36);
            this._gbMax.Controls.Add(this.label35);
            this._gbMax.Controls.Add(this._MO_CC_L);
            this._gbMax.Controls.Add(this.label34);
            this._gbMax.Controls.Add(this._MO_SC);
            this._gbMax.Controls.Add(this._MO_REC);
            this._gbMax.Controls.Add(this.label33);
            this._gbMax.Controls.Add(this.label28);
            this._gbMax.Controls.Add(this.label32);
            this._gbMax.Controls.Add(this.label30);
            this._gbMax.Controls.Add(this.label31);
            this._gbMax.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._gbMax.Location = new System.Drawing.Point(475, 3);
            this._gbMax.Name = "_gbMax";
            this._gbMax.Size = new System.Drawing.Size(467, 206);
            this._gbMax.TabIndex = 81;
            this._gbMax.TabStop = false;
            this._gbMax.Text = "Your MAXIMUM OFFER to 1st";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(386, 21);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(10, 13);
            this.label6.TabIndex = 81;
            this.label6.Text = ":";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(342, 29);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(38, 13);
            this.label2.TabIndex = 80;
            this.label2.Text = "Multip.";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(342, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 13);
            this.label1.TabIndex = 79;
            this.label1.Text = "Strategy";
            // 
            // _gbInit
            // 
            this._gbInit.Controls.Add(this._IO_REC_Perc);
            this._gbInit.Controls.Add(this._IO_SC_Perc);
            this._gbInit.Controls.Add(this._IO_CC_Perc);
            this._gbInit.Controls.Add(this._IO_PayOff);
            this._gbInit.Controls.Add(this._IO_C_AsIsValue);
            this._gbInit.Controls.Add(this._IO_C_PropValue);
            this._gbInit.Controls.Add(this._IO_C_Repairs);
            this._gbInit.Controls.Add(this._IO_C_FirstOfferMultiplier);
            this._gbInit.Controls.Add(this._IO_SalesPrice);
            this._gbInit.Controls.Add(this._IO_REC_Value);
            this._gbInit.Controls.Add(this._IO_Net);
            this._gbInit.Controls.Add(this._IO_CC_Value);
            this._gbInit.Controls.Add(this._IO_SC_Value);
            this._gbInit.Controls.Add(this.label3);
            this._gbInit.Controls.Add(this.label4);
            this._gbInit.Controls.Add(this.label5);
            this._gbInit.Controls.Add(this.line2);
            this._gbInit.Controls.Add(this.label9);
            this._gbInit.Controls.Add(this.line1);
            this._gbInit.Controls.Add(this._getFromRepairs);
            this._gbInit.Controls.Add(this.label10);
            this._gbInit.Controls.Add(this._getFromComps);
            this._gbInit.Controls.Add(this._IO_L);
            this._gbInit.Controls.Add(this.label11);
            this._gbInit.Controls.Add(this._IO_CC_L);
            this._gbInit.Controls.Add(this._IO_SC);
            this._gbInit.Controls.Add(this.label12);
            this._gbInit.Controls.Add(this._IO_REC);
            this._gbInit.Controls.Add(this.label14);
            this._gbInit.Controls.Add(this.label20);
            this._gbInit.Controls.Add(this.label17);
            this._gbInit.Controls.Add(this.label13);
            this._gbInit.Controls.Add(this.label15);
            this._gbInit.Controls.Add(this.label16);
            this._gbInit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._gbInit.Location = new System.Drawing.Point(9, 3);
            this._gbInit.Name = "_gbInit";
            this._gbInit.Size = new System.Drawing.Size(460, 240);
            this._gbInit.TabIndex = 82;
            this._gbInit.TabStop = false;
            this._gbInit.Text = "Your INITIAL OFFER to 1st";
            // 
            // CalcOfferFirstLien
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.Controls.Add(this._gbInit);
            this.Controls.Add(this._gbMax);
            this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.Name = "CalcOfferFirstLien";
            this.Size = new System.Drawing.Size(948, 430);
            this._gbMax.ResumeLayout(false);
            this._gbMax.PerformLayout();
            this._gbInit.ResumeLayout(false);
            this._gbInit.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox _IO_C_PropValue;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.TextBox _IO_C_Repairs;
        private System.Windows.Forms.TextBox _IO_C_AsIsValue;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox _IO_C_FirstOfferMultiplier;
        private System.Windows.Forms.TextBox _IO_SalesPrice;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox _IO_REC_Perc;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox _IO_REC_Value;
        private System.Windows.Forms.TextBox _IO_SC_Value;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox _IO_SC_Perc;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox _IO_CC_Value;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox _IO_CC_Perc;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox _IO_PayOff;
        private System.Windows.Forms.TextBox _IO_Net;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Button _IO_REC;
        private System.Windows.Forms.Button _IO_SC;
        private System.Windows.Forms.Button _IO_CC_L;
        private System.Windows.Forms.Button _IO_L;
        private System.Windows.Forms.TextBox _MO_RealValue;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox _MO_Real_StrategyMult;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox _MO_Repairs;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox _MO_SalesPrice;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Button _MO_CC_L;
        private System.Windows.Forms.Button _MO_SC;
        private System.Windows.Forms.Button _MO_REC;
        private System.Windows.Forms.TextBox _MO_Net;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TextBox _MO_PayOff;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox _MO_CC_Value;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox _MO_CC_Perc;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.TextBox _MO_SC_Value;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TextBox _MO_SC_Perc;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.TextBox _MO_REC_Value;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.TextBox _MO_REC_Perc;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Button _getFromComps;
        private System.Windows.Forms.Button _getFromRepairs;
        private DealMaker.UserControls.Line line1;
        private DealMaker.UserControls.Line line2;
        private DealMaker.UserControls.Line line3;
        private System.Windows.Forms.GroupBox _gbMax;
        private System.Windows.Forms.GroupBox _gbInit;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label6;
    }
}
