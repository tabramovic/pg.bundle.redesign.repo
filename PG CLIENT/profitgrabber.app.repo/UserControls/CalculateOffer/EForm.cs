﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

//Added
using DealMaker.UserControls;
using DealMaker.ShortSale;
using DealMaker.ShortSale.Utils;
using DealMaker.PlainClasses.CalculateOffer;

namespace DealMaker.UserControls.CalculateOffer
{
    public partial class EForm : Form
    {
        const string Formatter = "#,0.00";

        public EForm(string formName)
        {
            this.Text = formName;

            InitializeComponent();
            AddEventHandlers();
        }

        void AddEventHandlers()
        {
            _loadDefaultValues.Click += new EventHandler(On_LoadDefaultValues_Click);
            _saveAsDefaultValues.Click += new EventHandler(On_SaveAsDefaultValues_Click);

            _others1.Click += new EventHandler(On_others_Click);
            _others2.Click += new EventHandler(On_others_Click);
            _others3.Click += new EventHandler(On_others_Click);
            _others4.Click += new EventHandler(On_others_Click);
            _others5.Click += new EventHandler(On_others_Click);
            _others6.Click += new EventHandler(On_others_Click);

            foreach (Control ctrl in this.Controls)
            {
                if (ctrl is TextBox)
                {
                    ((TextBox)ctrl).Leave += new EventHandler(On_TextBox_Leave);
                    ((TextBox)ctrl).KeyDown += new KeyEventHandler(On_TextBox_KeyDown);
                }
            }

        }

        void On_SaveAsDefaultValues_Click(object sender, EventArgs e)
        {
            CalculateOfferManager.Instance.LoadCODefaultValues();

            if (null == CalculateOfferManager.Instance.P_CO_DefaultValues)
                CalculateOfferManager.Instance.P_CO_DefaultValues = new CO_DefaultValues();

            if (null == CalculateOfferManager.Instance.P_CO_DefaultValues.ClosingCost)
                CalculateOfferManager.Instance.P_CO_DefaultValues.ClosingCost = new CO_DefaultValuesClosingCost();

            CalculateOfferManager.Instance.P_CO_DefaultValues.ClosingCost.AppraisalFee = new CO_DefaultValuesClosingCostAppraisalFee();
            CalculateOfferManager.Instance.P_CO_DefaultValues.ClosingCost.AppraisalFee.BC = _B_appraisalFee.Text;
            CalculateOfferManager.Instance.P_CO_DefaultValues.ClosingCost.AppraisalFee.SC = _S_appraisalFee.Text;

            CalculateOfferManager.Instance.P_CO_DefaultValues.ClosingCost.CreditReport = new CO_DefaultValuesClosingCostCreditReport();
            CalculateOfferManager.Instance.P_CO_DefaultValues.ClosingCost.CreditReport.BC = _B_creditReport.Text;
            CalculateOfferManager.Instance.P_CO_DefaultValues.ClosingCost.CreditReport.SC = _S_creditReport.Text;

            CalculateOfferManager.Instance.P_CO_DefaultValues.ClosingCost.EscrowFee = new CO_DefaultValuesClosingCostEscrowFee();
            CalculateOfferManager.Instance.P_CO_DefaultValues.ClosingCost.EscrowFee.BC = _B_escrowFee.Text;
            CalculateOfferManager.Instance.P_CO_DefaultValues.ClosingCost.EscrowFee.SC = _S_escrowFee.Text;

            CalculateOfferManager.Instance.P_CO_DefaultValues.ClosingCost.HOATransferrFee = new CO_DefaultValuesClosingCostHOATransferrFee();
            CalculateOfferManager.Instance.P_CO_DefaultValues.ClosingCost.HOATransferrFee.BC = _B_hoaTransferFee.Text;
            CalculateOfferManager.Instance.P_CO_DefaultValues.ClosingCost.HOATransferrFee.SC = _S_hoaTransferFee.Text;

            CalculateOfferManager.Instance.P_CO_DefaultValues.ClosingCost.RecordingFee = new CO_DefaultValuesClosingCostRecordingFee();
            CalculateOfferManager.Instance.P_CO_DefaultValues.ClosingCost.RecordingFee.BC = _B_recordingFee.Text;
            CalculateOfferManager.Instance.P_CO_DefaultValues.ClosingCost.RecordingFee.SC = _S_recordingFee.Text;

            CalculateOfferManager.Instance.P_CO_DefaultValues.ClosingCost.TitleInsurance = new CO_DefaultValuesClosingCostTitleInsurance();
            CalculateOfferManager.Instance.P_CO_DefaultValues.ClosingCost.TitleInsurance.BC = _B_titleInsurance.Text;
            CalculateOfferManager.Instance.P_CO_DefaultValues.ClosingCost.TitleInsurance.SC = _S_titleInsurance.Text;

            CalculateOfferManager.Instance.P_CO_DefaultValues.ClosingCost.Others1 = new CO_DefaultValuesClosingCostOthers1();
            CalculateOfferManager.Instance.P_CO_DefaultValues.ClosingCost.Others1.Label = _others1.Text;
            CalculateOfferManager.Instance.P_CO_DefaultValues.ClosingCost.Others1.BC = _B_others1Value.Text;
            CalculateOfferManager.Instance.P_CO_DefaultValues.ClosingCost.Others1.SC = _S_others1Value.Text;

            CalculateOfferManager.Instance.P_CO_DefaultValues.ClosingCost.Others2 = new CO_DefaultValuesClosingCostOthers2();
            CalculateOfferManager.Instance.P_CO_DefaultValues.ClosingCost.Others2.Label = _others2.Text;
            CalculateOfferManager.Instance.P_CO_DefaultValues.ClosingCost.Others2.BC = _B_others2Value.Text;
            CalculateOfferManager.Instance.P_CO_DefaultValues.ClosingCost.Others2.SC = _S_others2Value.Text;

            CalculateOfferManager.Instance.P_CO_DefaultValues.ClosingCost.Others3 = new CO_DefaultValuesClosingCostOthers3();
            CalculateOfferManager.Instance.P_CO_DefaultValues.ClosingCost.Others3.Label = _others3.Text;
            CalculateOfferManager.Instance.P_CO_DefaultValues.ClosingCost.Others3.BC = _B_others3Value.Text;
            CalculateOfferManager.Instance.P_CO_DefaultValues.ClosingCost.Others3.SC = _S_others3Value.Text;

            CalculateOfferManager.Instance.P_CO_DefaultValues.ClosingCost.Others4 = new CO_DefaultValuesClosingCostOthers4();
            CalculateOfferManager.Instance.P_CO_DefaultValues.ClosingCost.Others4.Label = _others4.Text;
            CalculateOfferManager.Instance.P_CO_DefaultValues.ClosingCost.Others4.BC = _B_others4Value.Text;
            CalculateOfferManager.Instance.P_CO_DefaultValues.ClosingCost.Others4.SC = _S_others4Value.Text;

            CalculateOfferManager.Instance.P_CO_DefaultValues.ClosingCost.Others5 = new CO_DefaultValuesClosingCostOthers5();
            CalculateOfferManager.Instance.P_CO_DefaultValues.ClosingCost.Others5.Label = _others5.Text;
            CalculateOfferManager.Instance.P_CO_DefaultValues.ClosingCost.Others5.BC = _B_others5Value.Text;
            CalculateOfferManager.Instance.P_CO_DefaultValues.ClosingCost.Others5.SC = _S_others5Value.Text;

            CalculateOfferManager.Instance.P_CO_DefaultValues.ClosingCost.Others6 = new CO_DefaultValuesClosingCostOthers6();
            CalculateOfferManager.Instance.P_CO_DefaultValues.ClosingCost.Others6.Label = _others6.Text;
            CalculateOfferManager.Instance.P_CO_DefaultValues.ClosingCost.Others6.BC = _B_others6Value.Text;
            CalculateOfferManager.Instance.P_CO_DefaultValues.ClosingCost.Others6.SC = _S_others6Value.Text;

            CalculateOfferManager.Instance.SaveCODefaultValues();
        }

        void On_LoadDefaultValues_Click(object sender, EventArgs e)
        {
            CalculateOfferManager.Instance.LoadCODefaultValues();
            try
            {
                _B_appraisalFee.Text = CalculateOfferManager.Instance.P_CO_DefaultValues.ClosingCost.AppraisalFee.BC;
                _S_appraisalFee.Text = CalculateOfferManager.Instance.P_CO_DefaultValues.ClosingCost.AppraisalFee.SC;
                
                _B_creditReport.Text = CalculateOfferManager.Instance.P_CO_DefaultValues.ClosingCost.CreditReport.BC;
                _S_creditReport.Text = CalculateOfferManager.Instance.P_CO_DefaultValues.ClosingCost.CreditReport.SC;
                
                _B_escrowFee.Text = CalculateOfferManager.Instance.P_CO_DefaultValues.ClosingCost.EscrowFee.BC;
                _S_escrowFee.Text = CalculateOfferManager.Instance.P_CO_DefaultValues.ClosingCost.EscrowFee.SC;
                
                _B_hoaTransferFee.Text = CalculateOfferManager.Instance.P_CO_DefaultValues.ClosingCost.HOATransferrFee.BC;
                _S_hoaTransferFee.Text = CalculateOfferManager.Instance.P_CO_DefaultValues.ClosingCost.HOATransferrFee.SC;
                
                _B_recordingFee.Text = CalculateOfferManager.Instance.P_CO_DefaultValues.ClosingCost.RecordingFee.BC;
                _S_recordingFee.Text = CalculateOfferManager.Instance.P_CO_DefaultValues.ClosingCost.RecordingFee.SC;
                
                _B_titleInsurance.Text = CalculateOfferManager.Instance.P_CO_DefaultValues.ClosingCost.TitleInsurance.BC;
                _S_titleInsurance.Text = CalculateOfferManager.Instance.P_CO_DefaultValues.ClosingCost.TitleInsurance.SC;
                
                _others1.Text = CalculateOfferManager.Instance.P_CO_DefaultValues.ClosingCost.Others1.Label;
                _B_others1Value.Text = CalculateOfferManager.Instance.P_CO_DefaultValues.ClosingCost.Others1.BC;
                _S_others1Value.Text = CalculateOfferManager.Instance.P_CO_DefaultValues.ClosingCost.Others1.SC;
                
                _others2.Text = CalculateOfferManager.Instance.P_CO_DefaultValues.ClosingCost.Others2.Label;
                _B_others2Value.Text = CalculateOfferManager.Instance.P_CO_DefaultValues.ClosingCost.Others2.BC;
                _S_others2Value.Text = CalculateOfferManager.Instance.P_CO_DefaultValues.ClosingCost.Others2.SC;
                
                _others3.Text = CalculateOfferManager.Instance.P_CO_DefaultValues.ClosingCost.Others3.Label;
                _B_others3Value.Text = CalculateOfferManager.Instance.P_CO_DefaultValues.ClosingCost.Others3.BC;
                _S_others3Value.Text = CalculateOfferManager.Instance.P_CO_DefaultValues.ClosingCost.Others3.SC;
                
                _others4.Text = CalculateOfferManager.Instance.P_CO_DefaultValues.ClosingCost.Others4.Label;
                _B_others4Value.Text = CalculateOfferManager.Instance.P_CO_DefaultValues.ClosingCost.Others4.BC;
                _S_others4Value.Text = CalculateOfferManager.Instance.P_CO_DefaultValues.ClosingCost.Others4.SC;
                
                _others5.Text = CalculateOfferManager.Instance.P_CO_DefaultValues.ClosingCost.Others5.Label;
                _B_others5Value.Text = CalculateOfferManager.Instance.P_CO_DefaultValues.ClosingCost.Others5.BC;
                _S_others5Value.Text = CalculateOfferManager.Instance.P_CO_DefaultValues.ClosingCost.Others5.SC;
                
                _others6.Text = CalculateOfferManager.Instance.P_CO_DefaultValues.ClosingCost.Others6.Label;
                _B_others6Value.Text = CalculateOfferManager.Instance.P_CO_DefaultValues.ClosingCost.Others6.BC;
                _S_others6Value.Text = CalculateOfferManager.Instance.P_CO_DefaultValues.ClosingCost.Others6.SC;

            }
            catch { }

            HandleTotal();
        }        

        void On_others_Click(object sender, EventArgs e)
        {
            TextBox tb = (TextBox)sender;
            tb.SelectAll();
            //SetName(sender);
        }

        void SetName(object sender)
        {
            EditLabel el = new EditLabel(((TextBox)(sender)).Text);
            if (DialogResult.OK == el.ShowDialog())
                ((TextBox)(sender)).Text = el.LabelName;
        }
        
        #region PROPRETIES
        public string S_EscrowFee
        {
            get { return _S_escrowFee.Text; }
            set { _S_escrowFee.Text = value; }
        }

        public string S_TitleInsurance
        {
            get { return _S_titleInsurance.Text; }
            set { _S_titleInsurance.Text = value; }
        }

        public string S_AppraisalFee
        {
            get { return _S_appraisalFee.Text; }
            set { _S_appraisalFee.Text = value; }
        }

        public string S_CreditReport
        {
            get { return _S_creditReport.Text; }
            set { _S_creditReport.Text = value; }
        }

        public string S_RecordingFee
        {
            get { return _S_recordingFee.Text; }
            set { _S_recordingFee.Text = value; }
        }

        public string S_HoaTransferFee
        {
            get { return _S_hoaTransferFee.Text; }
            set { _S_hoaTransferFee.Text = value; }
        }

        public string S_Others1Value
        {
            get { return _S_others1Value.Text; }
            set { _S_others1Value.Text = value; }
        }

        public string S_Others1Name
        {
            get { return _others1.Text; }
            set { _others1.Text = value; }
        }

        public string S_Others2Value
        {
            get { return _S_others2Value.Text; }
            set { _S_others2Value.Text = value; }
        }

        public string S_Others2Name
        {
            get { return _others2.Text; }
            set { _others2.Text = value; }
        }

        public string S_Others3Value
        {
            get { return _S_others3Value.Text; }
            set { _S_others3Value.Text = value; }
        }

        public string S_Others3Name
        {
            get { return _others3.Text; }
            set { _others3.Text = value; }
        }

        public string S_Others4Value
        {
            get { return _S_others4Value.Text; }
            set { _S_others4Value.Text = value; }
        }

        public string S_Others4Name
        {
            get { return _others4.Text; }
            set { _others4.Text = value; }
        }

        public string S_Others5Value
        {
            get { return _S_others5Value.Text; }
            set { _S_others5Value.Text = value; }
        }

        public string S_Others5Name
        {
            get { return _others5.Text; }
            set { _others5.Text = value; }
        }

        public string S_Others6Value
        {
            get { return _S_others6Value.Text; }
            set { _S_others6Value.Text = value; }
        }

        public string S_Others6Name
        {
            get { return _others6.Text; }
            set { _others6.Text = value; }
        }

        public string S_Total
        {
            get { return _S_total.Text; }
            set { _S_total.Text = value; }
        }

        public string B_EscrowFee
        {
            get { return _B_escrowFee.Text; }
            set { _B_escrowFee.Text = value; }
        }

        public string B_TitleInsurance
        {
            get { return _B_titleInsurance.Text; }
            set { _B_titleInsurance.Text = value; }
        }

        public string B_AppraisalFee
        {
            get { return _B_appraisalFee.Text; }
            set { _B_appraisalFee.Text = value; }
        }

        public string B_CreditReport
        {
            get { return _B_creditReport.Text; }
            set { _B_creditReport.Text = value; }
        }

        public string B_RecordingFee
        {
            get { return _B_recordingFee.Text; }
            set { _B_recordingFee.Text = value; }
        }

        public string B_HoaTransferFee
        {
            get { return _B_hoaTransferFee.Text; }
            set { _B_hoaTransferFee.Text = value; }
        }

        public string B_Others1Value
        {
            get { return _B_others1Value.Text; }
            set { _B_others1Value.Text = value; }
        }

        public string B_Others1Name
        {
            get { return _others1.Text; }
            set { _others1.Text = value; }
        }

        public string B_Others2Value
        {
            get { return _B_others2Value.Text; }
            set { _B_others2Value.Text = value; }
        }

        public string B_Others2Name
        {
            get { return _others2.Text; }
            set { _others2.Text = value; }
        }

        public string B_Others3Value
        {
            get { return _B_others3Value.Text; }
            set { _B_others3Value.Text = value; }
        }

        public string B_Others3Name
        {
            get { return _others3.Text; }
            set { _others3.Text = value; }
        }

        public string B_Others4Value
        {
            get { return _B_others4Value.Text; }
            set { _B_others4Value.Text = value; }
        }

        public string B_Others4Name
        {
            get { return _others4.Text; }
            set { _others4.Text = value; }
        }

        public string B_Others5Value
        {
            get { return _B_others5Value.Text; }
            set { _B_others5Value.Text = value; }
        }

        public string B_Others5Name
        {
            get { return _others5.Text; }
            set { _others5.Text = value; }
        }

        public string B_Others6Value
        {
            get { return _B_others6Value.Text; }
            set { _B_others6Value.Text = value; }
        }

        public string B_Others6Name
        {
            get { return _others6.Text; }
            set { _others6.Text = value; }
        }

        public string B_Total
        {
            get { return _B_total.Text; }
            set { _B_total.Text = value; }
        }

        public ClosingCostBreakDownData S_BreakDownData
        {
            get 
            {
                ClosingCostBreakDownData data = new ClosingCostBreakDownData();
                data.EscrowFeeValue = Util.GetDecimalValue(S_EscrowFee);
                data.TitleInsuranceValue = Util.GetDecimalValue(S_TitleInsurance);
                data.AppraisalFeeValue = Util.GetDecimalValue(S_AppraisalFee);
                data.CreditReportValue = Util.GetDecimalValue(S_CreditReport);
                data.RecordingFeeValue = Util.GetDecimalValue(S_RecordingFee);
                data.HoaTransferFeeValue = Util.GetDecimalValue(S_HoaTransferFee);
                data.Others1Name = S_Others1Name;
                data.Others1Value = Util.GetDecimalValue(S_Others1Value);
                data.Others2Name = S_Others2Name;
                data.Others2Value = Util.GetDecimalValue(S_Others2Value);
                data.Others3Name = S_Others3Name;
                data.Others3Value = Util.GetDecimalValue(S_Others3Value);
                data.Others4Name = S_Others4Name;
                data.Others4Value = Util.GetDecimalValue(S_Others4Value);
                data.Others5Name = S_Others5Name;
                data.Others5Value = Util.GetDecimalValue(S_Others5Value);
                data.Others6Name = S_Others6Name;
                data.Others6Value = Util.GetDecimalValue(S_Others6Value);

                return data;
            }

            set
            {
                if (null == value)
                    return;

                S_EscrowFee = value.EscrowFeeValue.ToString(Formatter);
                S_TitleInsurance = value.TitleInsuranceValue.ToString(Formatter);
                S_AppraisalFee = value.AppraisalFeeValue.ToString(Formatter);
                S_CreditReport = value.CreditReportValue.ToString(Formatter);
                S_RecordingFee = value.RecordingFeeValue.ToString(Formatter);
                S_HoaTransferFee = value.HoaTransferFeeValue.ToString(Formatter);
                S_Others1Name = value.Others1Name;
                S_Others1Value = value.Others1Value.ToString(Formatter);
                S_Others2Name = value.Others2Name;
                S_Others2Value = value.Others2Value.ToString(Formatter);
                S_Others3Name = value.Others3Name;
                S_Others3Value = value.Others3Value.ToString(Formatter);
                S_Others4Name = value.Others4Name;
                S_Others4Value = value.Others4Value.ToString(Formatter);
                S_Others5Name = value.Others5Name;
                S_Others5Value = value.Others5Value.ToString(Formatter);
                S_Others6Name = value.Others6Name;
                S_Others6Value = value.Others6Value.ToString(Formatter);

                HandleTotal();
            }
        }

        public ClosingCostBreakDownData B_BreakDownData
        {
            get
            {
                ClosingCostBreakDownData data = new ClosingCostBreakDownData();
                data.EscrowFeeValue = Util.GetDecimalValue(B_EscrowFee);
                data.TitleInsuranceValue = Util.GetDecimalValue(B_TitleInsurance);
                data.AppraisalFeeValue = Util.GetDecimalValue(B_AppraisalFee);
                data.CreditReportValue = Util.GetDecimalValue(B_CreditReport);
                data.RecordingFeeValue = Util.GetDecimalValue(B_RecordingFee);
                data.HoaTransferFeeValue = Util.GetDecimalValue(B_HoaTransferFee);
                data.Others1Name = B_Others1Name;
                data.Others1Value = Util.GetDecimalValue(B_Others1Value);
                data.Others2Name = B_Others2Name;
                data.Others2Value = Util.GetDecimalValue(B_Others2Value);
                data.Others3Name = B_Others3Name;
                data.Others3Value = Util.GetDecimalValue(B_Others3Value);
                data.Others4Name = B_Others4Name;
                data.Others4Value = Util.GetDecimalValue(B_Others4Value);
                data.Others5Name = B_Others5Name;
                data.Others5Value = Util.GetDecimalValue(B_Others5Value);
                data.Others6Name = B_Others6Name;
                data.Others6Value = Util.GetDecimalValue(B_Others5Value);

                return data;
            }

            set
            {
                if (null == value)
                    return;

                B_EscrowFee = value.EscrowFeeValue.ToString(Formatter);
                B_TitleInsurance = value.TitleInsuranceValue.ToString(Formatter);
                B_AppraisalFee = value.AppraisalFeeValue.ToString(Formatter);
                B_CreditReport = value.CreditReportValue.ToString(Formatter);
                B_RecordingFee = value.RecordingFeeValue.ToString(Formatter);
                B_HoaTransferFee = value.HoaTransferFeeValue.ToString(Formatter);
                B_Others1Name = value.Others1Name;
                B_Others1Value = value.Others1Value.ToString(Formatter);
                B_Others2Name = value.Others2Name;
                B_Others2Value = value.Others2Value.ToString(Formatter);
                B_Others3Name = value.Others3Name;
                B_Others3Value = value.Others3Value.ToString(Formatter);
                B_Others4Name = value.Others4Name;
                B_Others4Value = value.Others4Value.ToString(Formatter);
                B_Others5Name = value.Others5Name;
                B_Others5Value = value.Others5Value.ToString(Formatter);
                B_Others6Name = value.Others6Name;
                B_Others6Value = value.Others6Value.ToString(Formatter);

                HandleTotal();
            }
        }
        #endregion

        void On_TextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (sender == _others1 || sender == _others2 || sender == _others3 || sender == _others4 || sender == _others5 || sender == _others6)                                                                                
                return;

            if (e.KeyCode == Keys.Enter)
            {
                decimal temp = Util.GetDecimalValue(((TextBox)sender).Text);
                ((TextBox)sender).Text = temp.ToString(Formatter);
                HandleTotal();
            }
        }

        void On_TextBox_Leave(object sender, EventArgs e)
        {
            if (sender == _others1 || sender == _others2 || sender == _others3 || sender == _others4 || sender == _others5 || sender == _others6)
                return;

            decimal temp = Util.GetDecimalValue(((TextBox)sender).Text);
            ((TextBox)sender).Text = temp.ToString(Formatter);
            HandleTotal();
        }

        void HandleTotal()
        {
            decimal temp = 0;

            foreach (Control ctrl in this.Controls)
            {
                if (ctrl is TextBox && "_S" == ctrl.Name.Substring(0, 2) && ctrl != _S_total && ctrl != _B_total)
                {
                    temp += Util.GetDecimalValue(((TextBox)ctrl).Text);
                }
            }

            S_Total = temp.ToString(Formatter);

            temp = 0;

            foreach (Control ctrl in this.Controls)
            {
                if (ctrl is TextBox && "_B" == ctrl.Name.Substring(0, 2) && ctrl!= _S_total && ctrl != _B_total)
                {
                    temp += Util.GetDecimalValue(((TextBox)ctrl).Text);
                }
            }

            B_Total = temp.ToString(Formatter);
        }
        
    }
}
