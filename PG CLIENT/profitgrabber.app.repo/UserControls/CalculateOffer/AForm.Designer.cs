﻿namespace DealMaker.UserControls.CalculateOffer
{
    partial class AForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label3 = new System.Windows.Forms.Label();
            this._laValue = new System.Windows.Forms.TextBox();
            this._saValue = new System.Windows.Forms.TextBox();
            this._total = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this._saveAsDefaultValues = new System.Windows.Forms.Button();
            this._loadDefaultValues = new System.Windows.Forms.Button();
            this._listingAgencyName = new System.Windows.Forms.TextBox();
            this._sellingAgencyName = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(84, 62);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(167, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "TOTAL COMMISSION PAID:";
            // 
            // _laValue
            // 
            this._laValue.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._laValue.Location = new System.Drawing.Point(257, 9);
            this._laValue.Name = "_laValue";
            this._laValue.Size = new System.Drawing.Size(39, 20);
            this._laValue.TabIndex = 3;
            // 
            // _saValue
            // 
            this._saValue.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._saValue.Location = new System.Drawing.Point(257, 30);
            this._saValue.Name = "_saValue";
            this._saValue.Size = new System.Drawing.Size(39, 20);
            this._saValue.TabIndex = 4;
            // 
            // _total
            // 
            this._total.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._total.Location = new System.Drawing.Point(257, 59);
            this._total.Name = "_total";
            this._total.ReadOnly = true;
            this._total.Size = new System.Drawing.Size(39, 20);
            this._total.TabIndex = 5;
            // 
            // button1
            // 
            this.button1.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.button1.Location = new System.Drawing.Point(221, 114);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 6;
            this.button1.Text = "OK";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(299, 9);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(15, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "%";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(299, 33);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(15, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "%";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(299, 62);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(15, 13);
            this.label6.TabIndex = 9;
            this.label6.Text = "%";
            // 
            // _saveAsDefaultValues
            // 
            this._saveAsDefaultValues.Location = new System.Drawing.Point(153, 85);
            this._saveAsDefaultValues.Name = "_saveAsDefaultValues";
            this._saveAsDefaultValues.Size = new System.Drawing.Size(143, 23);
            this._saveAsDefaultValues.TabIndex = 10;
            this._saveAsDefaultValues.Text = "Save As Default Values";
            this._saveAsDefaultValues.UseVisualStyleBackColor = true;
            // 
            // _loadDefaultValues
            // 
            this._loadDefaultValues.Location = new System.Drawing.Point(4, 85);
            this._loadDefaultValues.Name = "_loadDefaultValues";
            this._loadDefaultValues.Size = new System.Drawing.Size(143, 23);
            this._loadDefaultValues.TabIndex = 11;
            this._loadDefaultValues.Text = "Load Default Values";
            this._loadDefaultValues.UseVisualStyleBackColor = true;
            // 
            // _listingAgencyName
            // 
            this._listingAgencyName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._listingAgencyName.Location = new System.Drawing.Point(12, 9);
            this._listingAgencyName.Name = "_listingAgencyName";
            this._listingAgencyName.Size = new System.Drawing.Size(239, 20);
            this._listingAgencyName.TabIndex = 12;
            this._listingAgencyName.Text = ">Enter Name of Listing Agency <";
            this._listingAgencyName.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _sellingAgencyName
            // 
            this._sellingAgencyName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._sellingAgencyName.Location = new System.Drawing.Point(12, 30);
            this._sellingAgencyName.Name = "_sellingAgencyName";
            this._sellingAgencyName.Size = new System.Drawing.Size(239, 20);
            this._sellingAgencyName.TabIndex = 13;
            this._sellingAgencyName.Text = "> Enter Name of Buyer’s Agency <";
            this._sellingAgencyName.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // AForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.ClientSize = new System.Drawing.Size(318, 149);
            this.Controls.Add(this._sellingAgencyName);
            this.Controls.Add(this._listingAgencyName);
            this.Controls.Add(this._loadDefaultValues);
            this.Controls.Add(this._saveAsDefaultValues);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.button1);
            this.Controls.Add(this._total);
            this.Controls.Add(this._saValue);
            this.Controls.Add(this._laValue);
            this.Controls.Add(this.label3);
            this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "REAL ESTATE COMISSION";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox _laValue;
        private System.Windows.Forms.TextBox _saValue;
        private System.Windows.Forms.TextBox _total;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button _saveAsDefaultValues;
        private System.Windows.Forms.Button _loadDefaultValues;
        private System.Windows.Forms.TextBox _listingAgencyName;
        private System.Windows.Forms.TextBox _sellingAgencyName;
    }
}