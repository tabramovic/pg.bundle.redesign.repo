﻿namespace DealMaker.UserControls.CalculateOffer
{
    partial class CalcOfferLienNext
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this._IO_L = new System.Windows.Forms.Button();
            this._IO_CC_L = new System.Windows.Forms.Button();
            this._IO_SC = new System.Windows.Forms.Button();
            this._IO_REC = new System.Windows.Forms.Button();
            this._initialOfferTo1st = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this._initialOfferTo2nd = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this._IO_CC_Value = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this._IO_CC_Perc = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this._IO_SC_Value = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this._IO_SC_Perc = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this._IO_REC_Value = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this._IO_REC_Perc = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this._IO_SalesPrice = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this._IO_FirstOfferMultiplier = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this._IO_AsIs = new System.Windows.Forms.TextBox();
            this._IO_Repairs = new System.Windows.Forms.TextBox();
            this._IO_C_PropertyValue = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this._initialOfferTo3rdLabel = new System.Windows.Forms.Label();
            this._initialOfferTo3rdNameLabel = new System.Windows.Forms.Label();
            this._initialOfferTo3rd = new System.Windows.Forms.TextBox();
            this._initialOfferTo2ndLabel = new System.Windows.Forms.Label();
            this._getFromRepairs = new System.Windows.Forms.Button();
            this._getFromComps = new System.Windows.Forms.Button();
            this._gb = new System.Windows.Forms.GroupBox();
            this.line1 = new Line();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this._gb.SuspendLayout();
            this.SuspendLayout();
            // 
            // _IO_L
            // 
            this._IO_L.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._IO_L.Location = new System.Drawing.Point(361, 253);
            this._IO_L.Name = "_IO_L";
            this._IO_L.Size = new System.Drawing.Size(75, 20);
            this._IO_L.TabIndex = 23;
            this._IO_L.Text = "Open HUD";
            this._IO_L.UseVisualStyleBackColor = true;
            // 
            // _IO_CC_L
            // 
            this._IO_CC_L.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._IO_CC_L.Location = new System.Drawing.Point(361, 190);
            this._IO_CC_L.Name = "_IO_CC_L";
            this._IO_CC_L.Size = new System.Drawing.Size(75, 20);
            this._IO_CC_L.TabIndex = 19;
            this._IO_CC_L.Text = "Breakdown";
            this._IO_CC_L.UseVisualStyleBackColor = true;
            // 
            // _IO_SC
            // 
            this._IO_SC.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._IO_SC.Location = new System.Drawing.Point(361, 169);
            this._IO_SC.Name = "_IO_SC";
            this._IO_SC.Size = new System.Drawing.Size(75, 20);
            this._IO_SC.TabIndex = 16;
            this._IO_SC.Text = "Breakdown";
            this._IO_SC.UseVisualStyleBackColor = true;
            // 
            // _IO_REC
            // 
            this._IO_REC.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._IO_REC.Location = new System.Drawing.Point(361, 148);
            this._IO_REC.Name = "_IO_REC";
            this._IO_REC.Size = new System.Drawing.Size(75, 20);
            this._IO_REC.TabIndex = 13;
            this._IO_REC.Text = "Breakdown";
            this._IO_REC.UseVisualStyleBackColor = true;
            // 
            // _initialOfferTo1st
            // 
            this._initialOfferTo1st.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._initialOfferTo1st.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._initialOfferTo1st.Location = new System.Drawing.Point(264, 253);
            this._initialOfferTo1st.Name = "_initialOfferTo1st";
            this._initialOfferTo1st.ReadOnly = true;
            this._initialOfferTo1st.Size = new System.Drawing.Size(88, 20);
            this._initialOfferTo1st.TabIndex = 22;
            this._initialOfferTo1st.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label20.Location = new System.Drawing.Point(56, 255);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(186, 13);
            this.label20.TabIndex = 57;
            this.label20.Text = "Initial Offer NET To 1st Lender:";
            // 
            // _initialOfferTo2nd
            // 
            this._initialOfferTo2nd.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._initialOfferTo2nd.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._initialOfferTo2nd.Location = new System.Drawing.Point(264, 232);
            this._initialOfferTo2nd.Name = "_initialOfferTo2nd";
            this._initialOfferTo2nd.Size = new System.Drawing.Size(88, 20);
            this._initialOfferTo2nd.TabIndex = 21;
            this._initialOfferTo2nd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label17.Location = new System.Drawing.Point(14, 235);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(228, 13);
            this.label17.TabIndex = 53;
            this.label17.Text = "INITIAL OFFER NET TO 2nd LENDER:";
            // 
            // _IO_CC_Value
            // 
            this._IO_CC_Value.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._IO_CC_Value.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._IO_CC_Value.Location = new System.Drawing.Point(264, 190);
            this._IO_CC_Value.Name = "_IO_CC_Value";
            this._IO_CC_Value.ReadOnly = true;
            this._IO_CC_Value.Size = new System.Drawing.Size(88, 20);
            this._IO_CC_Value.TabIndex = 18;
            this._IO_CC_Value.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(243, 193);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(15, 13);
            this.label15.TabIndex = 51;
            this.label15.Text = "%";
            // 
            // _IO_CC_Perc
            // 
            this._IO_CC_Perc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._IO_CC_Perc.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._IO_CC_Perc.Location = new System.Drawing.Point(210, 190);
            this._IO_CC_Perc.Name = "_IO_CC_Perc";
            this._IO_CC_Perc.Size = new System.Drawing.Size(31, 20);
            this._IO_CC_Perc.TabIndex = 17;
            this._IO_CC_Perc.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(105, 193);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(98, 13);
            this.label16.TabIndex = 49;
            this.label16.Text = "minus Closing Cost:";
            // 
            // _IO_SC_Value
            // 
            this._IO_SC_Value.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._IO_SC_Value.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._IO_SC_Value.Location = new System.Drawing.Point(264, 169);
            this._IO_SC_Value.Name = "_IO_SC_Value";
            this._IO_SC_Value.ReadOnly = true;
            this._IO_SC_Value.Size = new System.Drawing.Size(88, 20);
            this._IO_SC_Value.TabIndex = 15;
            this._IO_SC_Value.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(243, 173);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(15, 13);
            this.label13.TabIndex = 47;
            this.label13.Text = "%";
            // 
            // _IO_SC_Perc
            // 
            this._IO_SC_Perc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._IO_SC_Perc.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._IO_SC_Perc.Location = new System.Drawing.Point(210, 169);
            this._IO_SC_Perc.Name = "_IO_SC_Perc";
            this._IO_SC_Perc.Size = new System.Drawing.Size(31, 20);
            this._IO_SC_Perc.TabIndex = 14;
            this._IO_SC_Perc.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(69, 172);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(131, 13);
            this.label14.TabIndex = 45;
            this.label14.Text = "minus Seller\'s Concession:";
            // 
            // _IO_REC_Value
            // 
            this._IO_REC_Value.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._IO_REC_Value.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._IO_REC_Value.Location = new System.Drawing.Point(264, 148);
            this._IO_REC_Value.Name = "_IO_REC_Value";
            this._IO_REC_Value.ReadOnly = true;
            this._IO_REC_Value.Size = new System.Drawing.Size(88, 20);
            this._IO_REC_Value.TabIndex = 12;
            this._IO_REC_Value.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(243, 150);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(15, 13);
            this.label12.TabIndex = 43;
            this.label12.Text = "%";
            // 
            // _IO_REC_Perc
            // 
            this._IO_REC_Perc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._IO_REC_Perc.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._IO_REC_Perc.Location = new System.Drawing.Point(210, 148);
            this._IO_REC_Perc.Name = "_IO_REC_Perc";
            this._IO_REC_Perc.Size = new System.Drawing.Size(31, 20);
            this._IO_REC_Perc.TabIndex = 11;
            this._IO_REC_Perc.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(47, 150);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(153, 13);
            this.label11.TabIndex = 41;
            this.label11.Text = "minus Real Estate Commission:";
            // 
            // _IO_SalesPrice
            // 
            this._IO_SalesPrice.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._IO_SalesPrice.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._IO_SalesPrice.Location = new System.Drawing.Point(264, 127);
            this._IO_SalesPrice.Name = "_IO_SalesPrice";
            this._IO_SalesPrice.ReadOnly = true;
            this._IO_SalesPrice.Size = new System.Drawing.Size(88, 20);
            this._IO_SalesPrice.TabIndex = 10;
            this._IO_SalesPrice.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(49, 129);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(183, 13);
            this.label10.TabIndex = 39;
            this.label10.Text = "INITIAL OFFER SALES PRICE:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(19, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(258, 13);
            this.label2.TabIndex = 65;
            this.label2.Text = "Start with offering 5% of the balance of the 2nd Loan.";
            // 
            // _IO_FirstOfferMultiplier
            // 
            this._IO_FirstOfferMultiplier.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._IO_FirstOfferMultiplier.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._IO_FirstOfferMultiplier.Location = new System.Drawing.Point(465, 92);
            this._IO_FirstOfferMultiplier.Name = "_IO_FirstOfferMultiplier";
            this._IO_FirstOfferMultiplier.Size = new System.Drawing.Size(30, 20);
            this._IO_FirstOfferMultiplier.TabIndex = 6;
            this._IO_FirstOfferMultiplier.Text = "0.75";
            this._IO_FirstOfferMultiplier.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(358, 94);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(97, 13);
            this.label9.TabIndex = 75;
            this.label9.Text = "x First Offer Multip.:";
            // 
            // _IO_AsIs
            // 
            this._IO_AsIs.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._IO_AsIs.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._IO_AsIs.Location = new System.Drawing.Point(264, 90);
            this._IO_AsIs.Name = "_IO_AsIs";
            this._IO_AsIs.Size = new System.Drawing.Size(88, 20);
            this._IO_AsIs.TabIndex = 5;
            this._IO_AsIs.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _IO_Repairs
            // 
            this._IO_Repairs.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._IO_Repairs.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._IO_Repairs.Location = new System.Drawing.Point(264, 64);
            this._IO_Repairs.Name = "_IO_Repairs";
            this._IO_Repairs.Size = new System.Drawing.Size(88, 20);
            this._IO_Repairs.TabIndex = 3;
            this._IO_Repairs.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.toolTip1.SetToolTip(this._IO_Repairs, "From Repairs Tab");
            // 
            // _IO_C_PropertyValue
            // 
            this._IO_C_PropertyValue.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._IO_C_PropertyValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._IO_C_PropertyValue.Location = new System.Drawing.Point(264, 43);
            this._IO_C_PropertyValue.Name = "_IO_C_PropertyValue";
            this._IO_C_PropertyValue.Size = new System.Drawing.Size(88, 20);
            this._IO_C_PropertyValue.TabIndex = 1;
            this._IO_C_PropertyValue.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.toolTip1.SetToolTip(this._IO_C_PropertyValue, "From COMPS tab");
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(186, 94);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(72, 13);
            this.label5.TabIndex = 68;
            this.label5.Text = "\"As is\" Value:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(171, 67);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(88, 13);
            this.label4.TabIndex = 67;
            this.label4.Text = "minus Repairs of:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(90, 45);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(168, 13);
            this.label3.TabIndex = 66;
            this.label3.Text = "CONSERVATIVE  Property Value:";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(49, 289);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(454, 13);
            this.label18.TabIndex = 77;
            this.label18.Text = "* In the process of negotiating 2nd you must ensure you don\'t exceed the Maximum " +
                "Sales Price";
            // 
            // _initialOfferTo3rdLabel
            // 
            this._initialOfferTo3rdLabel.AutoSize = true;
            this._initialOfferTo3rdLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._initialOfferTo3rdLabel.Location = new System.Drawing.Point(361, 214);
            this._initialOfferTo3rdLabel.Name = "_initialOfferTo3rdLabel";
            this._initialOfferTo3rdLabel.Size = new System.Drawing.Size(62, 13);
            this._initialOfferTo3rdLabel.TabIndex = 78;
            this._initialOfferTo3rdLabel.Text = "*start with...";
            // 
            // _initialOfferTo3rdNameLabel
            // 
            this._initialOfferTo3rdNameLabel.AutoSize = true;
            this._initialOfferTo3rdNameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._initialOfferTo3rdNameLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this._initialOfferTo3rdNameLabel.Location = new System.Drawing.Point(16, 214);
            this._initialOfferTo3rdNameLabel.Name = "_initialOfferTo3rdNameLabel";
            this._initialOfferTo3rdNameLabel.Size = new System.Drawing.Size(225, 13);
            this._initialOfferTo3rdNameLabel.TabIndex = 82;
            this._initialOfferTo3rdNameLabel.Text = "INITIAL OFFER NET TO 3rd LENDER:";
            // 
            // _initialOfferTo3rd
            // 
            this._initialOfferTo3rd.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._initialOfferTo3rd.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._initialOfferTo3rd.Location = new System.Drawing.Point(264, 211);
            this._initialOfferTo3rd.Name = "_initialOfferTo3rd";
            this._initialOfferTo3rd.Size = new System.Drawing.Size(88, 20);
            this._initialOfferTo3rd.TabIndex = 20;
            this._initialOfferTo3rd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _initialOfferTo2ndLabel
            // 
            this._initialOfferTo2ndLabel.AutoSize = true;
            this._initialOfferTo2ndLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._initialOfferTo2ndLabel.Location = new System.Drawing.Point(361, 235);
            this._initialOfferTo2ndLabel.Name = "_initialOfferTo2ndLabel";
            this._initialOfferTo2ndLabel.Size = new System.Drawing.Size(62, 13);
            this._initialOfferTo2ndLabel.TabIndex = 84;
            this._initialOfferTo2ndLabel.Text = "*start with...";
            // 
            // _getFromRepairs
            // 
            this._getFromRepairs.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._getFromRepairs.Location = new System.Drawing.Point(358, 62);
            this._getFromRepairs.Name = "_getFromRepairs";
            this._getFromRepairs.Size = new System.Drawing.Size(137, 20);
            this._getFromRepairs.TabIndex = 4;
            this._getFromRepairs.Text = "Get Repairs Estimate";
            this._getFromRepairs.UseVisualStyleBackColor = true;
            // 
            // _getFromComps
            // 
            this._getFromComps.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._getFromComps.Location = new System.Drawing.Point(358, 41);
            this._getFromComps.Name = "_getFromComps";
            this._getFromComps.Size = new System.Drawing.Size(137, 20);
            this._getFromComps.TabIndex = 2;
            this._getFromComps.Text = "Get Property Value";
            this._getFromComps.UseVisualStyleBackColor = true;
            // 
            // _gb
            // 
            this._gb.Controls.Add(this._IO_Repairs);
            this._gb.Controls.Add(this._IO_FirstOfferMultiplier);
            this._gb.Controls.Add(this._IO_SalesPrice);
            this._gb.Controls.Add(this._IO_REC_Perc);
            this._gb.Controls.Add(this._IO_AsIs);
            this._gb.Controls.Add(this._IO_REC_Value);
            this._gb.Controls.Add(this._IO_SC_Perc);
            this._gb.Controls.Add(this._initialOfferTo3rd);
            this._gb.Controls.Add(this._IO_SC_Value);
            this._gb.Controls.Add(this._IO_CC_Perc);
            this._gb.Controls.Add(this._IO_CC_Value);
            this._gb.Controls.Add(this._IO_C_PropertyValue);
            this._gb.Controls.Add(this._initialOfferTo2nd);
            this._gb.Controls.Add(this._initialOfferTo1st);
            this._gb.Controls.Add(this.label2);
            this._gb.Controls.Add(this.label10);
            this._gb.Controls.Add(this.label9);
            this._gb.Controls.Add(this.label11);
            this._gb.Controls.Add(this.label12);
            this._gb.Controls.Add(this.label5);
            this._gb.Controls.Add(this._getFromRepairs);
            this._gb.Controls.Add(this.label14);
            this._gb.Controls.Add(this._getFromComps);
            this._gb.Controls.Add(this._initialOfferTo2ndLabel);
            this._gb.Controls.Add(this.label13);
            this._gb.Controls.Add(this._initialOfferTo3rdNameLabel);
            this._gb.Controls.Add(this.label16);
            this._gb.Controls.Add(this.line1);
            this._gb.Controls.Add(this._initialOfferTo3rdLabel);
            this._gb.Controls.Add(this.label15);
            this._gb.Controls.Add(this.label18);
            this._gb.Controls.Add(this.label17);
            this._gb.Controls.Add(this.label4);
            this._gb.Controls.Add(this.label3);
            this._gb.Controls.Add(this.label20);
            this._gb.Controls.Add(this._IO_L);
            this._gb.Controls.Add(this._IO_REC);
            this._gb.Controls.Add(this._IO_CC_L);
            this._gb.Controls.Add(this._IO_SC);
            this._gb.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._gb.Location = new System.Drawing.Point(9, 3);
            this._gb.Name = "_gb";
            this._gb.Size = new System.Drawing.Size(573, 317);
            this._gb.TabIndex = 85;
            this._gb.TabStop = false;
            this._gb.Text = "-";
            // 
            // line1
            // 
            this.line1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.line1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.line1.Location = new System.Drawing.Point(90, 81);
            this.line1.Margin = new System.Windows.Forms.Padding(4);
            this.line1.Name = "line1";
            this.line1.Size = new System.Drawing.Size(261, 10);
            this.line1.TabIndex = 79;
            // 
            // CalcOfferLienNext
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.Controls.Add(this._gb);
            this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.Name = "CalcOfferLienNext";
            this.Size = new System.Drawing.Size(713, 495);
            this._gb.ResumeLayout(false);
            this._gb.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button _IO_L;
        private System.Windows.Forms.Button _IO_CC_L;
        private System.Windows.Forms.Button _IO_SC;
        private System.Windows.Forms.Button _IO_REC;
        private System.Windows.Forms.TextBox _initialOfferTo1st;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox _initialOfferTo2nd;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox _IO_CC_Value;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox _IO_CC_Perc;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox _IO_SC_Value;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox _IO_SC_Perc;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox _IO_REC_Value;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox _IO_REC_Perc;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox _IO_SalesPrice;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox _IO_FirstOfferMultiplier;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox _IO_AsIs;
        private System.Windows.Forms.TextBox _IO_Repairs;
        private System.Windows.Forms.TextBox _IO_C_PropertyValue;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label _initialOfferTo3rdLabel;
        private DealMaker.UserControls.Line line1;
        private System.Windows.Forms.Label _initialOfferTo3rdNameLabel;
        private System.Windows.Forms.TextBox _initialOfferTo3rd;
        private System.Windows.Forms.Label _initialOfferTo2ndLabel;
        private System.Windows.Forms.Button _getFromRepairs;
        private System.Windows.Forms.Button _getFromComps;
        private System.Windows.Forms.GroupBox _gb;
        private System.Windows.Forms.ToolTip toolTip1;
    }
}
