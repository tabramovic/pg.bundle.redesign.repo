﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

//Added
using DealMaker.ShortSale;
using DealMaker.ShortSale.Utils;
using DealMaker.PlainClasses.CalculateOffer;

namespace DealMaker.UserControls.CalculateOffer
{
    public partial class BForm : Form
    {
        const string Formatter = "#,0.00";
        decimal _totalValue = 0;

        public BForm()
        {
            InitializeComponent();
            AddEventHandlers();
        }

        public BForm(decimal total)
        {           
            InitializeComponent();
            AddEventHandlers();

            _totalValue = total;
            Total = total;
        }

        public BForm(decimal discountValue, decimal originationValue, decimal total)
        {           
            InitializeComponent();
            AddEventHandlers();

            _loanOriginationFeeValue.Text = originationValue.ToString(Formatter);
            _loanDiscountFeeValue.Text = discountValue.ToString(Formatter);
            _totalValue = total;                       
        }

        void AddEventHandlers()
        {
            _loanDiscountFeeValue.Leave += new EventHandler(On_loanDiscountFeeValue_Click);
            _loanDiscountFeeValue.KeyDown += new KeyEventHandler(On_loanDiscountFeeValue_KeyDown);

            _loanOriginationFeeValue.Leave += new EventHandler(On_loanOriginationFeeValue_Click);
            _loanOriginationFeeValue.KeyDown += new KeyEventHandler(On_loanOriginationFeeValue_KeyDown);

            _saveAsDefaultValues.Click += new EventHandler(On_SaveAsDefaultValues_Click);
            _loadDefaultValues.Click += new EventHandler(On_LoadDefaultValues_Click);
        }
        
        void On_SaveAsDefaultValues_Click(object sender, EventArgs e)
        {
            CalculateOfferManager.Instance.LoadCODefaultValues();

            if (null == CalculateOfferManager.Instance.P_CO_DefaultValues)
                CalculateOfferManager.Instance.P_CO_DefaultValues = new CO_DefaultValues();

            if (null == CalculateOfferManager.Instance.P_CO_DefaultValues.SellersConcession)
                CalculateOfferManager.Instance.P_CO_DefaultValues.SellersConcession = new CO_DefaultValuesSellersConcession();

            CalculateOfferManager.Instance.P_CO_DefaultValues.SellersConcession.LoanDiscountFee = _loanDiscountFeeValue.Text;
            CalculateOfferManager.Instance.P_CO_DefaultValues.SellersConcession.LoanOriginationFee = _loanOriginationFeeValue.Text;
            
            CalculateOfferManager.Instance.SaveCODefaultValues();
        }

        void On_LoadDefaultValues_Click(object sender, EventArgs e)
        {
            CalculateOfferManager.Instance.LoadCODefaultValues();

            try
            {
                if (null != CalculateOfferManager.Instance.P_CO_DefaultValues.SellersConcession.LoanDiscountFee)
                    _loanDiscountFeeValue.Text = CalculateOfferManager.Instance.P_CO_DefaultValues.SellersConcession.LoanDiscountFee;                
                else
                    _loanDiscountFeeValue.Text = "1";                
            }
            catch { }

            try
            {
                if (null != CalculateOfferManager.Instance.P_CO_DefaultValues.SellersConcession.LoanOriginationFee)
                    _loanOriginationFeeValue.Text = CalculateOfferManager.Instance.P_CO_DefaultValues.SellersConcession.LoanOriginationFee;
                else
                    _loanOriginationFeeValue.Text = "2";
            }
            catch { }

            Total = Util.GetDecimalValue(_loanDiscountFeeValue.Text) + Util.GetDecimalValue(_loanOriginationFeeValue.Text);
            _totalValue = Total;
        }

        

        void On_loanOriginationFeeValue_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                HandleLoanOriginationFee();
        }

        void On_loanOriginationFeeValue_Click(object sender, EventArgs e)
        {            
            HandleLoanOriginationFee();
        }

        void On_loanDiscountFeeValue_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                HandleLoanDiscountFee();
        }

        void On_loanDiscountFeeValue_Click(object sender, EventArgs e)
        {
            HandleLoanDiscountFee();
        }

        void HandleLoanOriginationFee()
        {
            decimal temp = _totalValue - Util.GetDecimalValue(_loanOriginationFeeValue.Text);
            _loanDiscountFeeValue.Text = temp.ToString(Formatter);
            _loanOriginationFeeValue.Text = Util.GetDecimalValue(_loanOriginationFeeValue.Text).ToString(Formatter);
        }

        void HandleLoanDiscountFee()
        {
            decimal temp = _totalValue - Util.GetDecimalValue(_loanDiscountFeeValue.Text);
            _loanOriginationFeeValue.Text = temp.ToString(Formatter);
            _loanDiscountFeeValue.Text = Util.GetDecimalValue(_loanDiscountFeeValue.Text).ToString(Formatter);
        }

        public decimal LoanDiscountFee
        {
            get { return Util.GetDecimalValue(_loanDiscountFeeValue.Text); }
            set { _loanDiscountFeeValue.Text = value.ToString(Formatter); }
        }

        public decimal LoanOriginatorFee
        {
            get { return Util.GetDecimalValue(_loanOriginationFeeValue.Text); }
            set { _loanOriginationFeeValue.Text = value.ToString(Formatter); }
        }

        public decimal Total
        {
            get { return Util.GetDecimalValue(_total.Text); }
            set { _total.Text = value.ToString(Formatter); }
        }

        public SellerConcessionBreakDownData BreakDownData
        {
            get 
            {
                SellerConcessionBreakDownData data = new SellerConcessionBreakDownData();
                data.LoanDiscountFeeValue = LoanDiscountFee;
                data.LoanOriginationFeeValue = LoanOriginatorFee;
                data.Total = _totalValue;

                return data;
            }
            set 
            {
                if (null == value)
                    return;

                LoanDiscountFee = value.LoanDiscountFeeValue;
                LoanOriginatorFee = value.LoanOriginationFeeValue;
                Total = value.Total;
                _totalValue = value.Total;
            }
        }


        
    }
}
