﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

//Added
using DealMaker.Repairs;
using DealMaker.ShortSale;
using DealMaker.ShortSale.Utils;
using DealMaker.PlainClasses.CalculateOffer;
using DealMaker.UserControls.ShortSale;
using DealMaker.PlainClasses.OnLineData.COMPS;
using DealMaker.PlainClasses.ShortSale.Adapters;
using HUD_MODULE;

namespace DealMaker.UserControls.CalculateOffer
{
    public partial class CalcOfferFirstLien : BaseShortSaleUserControl
    {
        const string Formatter = "#,0.00";
        public CalcOfferFirstLien()
        {
            InitializeComponent();
            AddEventHandlers();
            SetData();
        }

        #region ADD EVENT HANDLERS
        void AddEventHandlers()
        {
            _IO_REC.Click += new EventHandler(On_IO_REC_Click);
            _IO_SC.Click += new EventHandler(On_IO_SC_Click);
            _IO_CC_L.Click += new EventHandler(On_IO_CC_L_Click);            
            _IO_L.Click += new EventHandler(On_IO_L_Click);

            _MO_REC.Click += new EventHandler(On_MO_REC_Click);
            _MO_SC.Click += new EventHandler(On_MO_SC_Click);
            _MO_CC_L.Click += new EventHandler(On_MO_CC_L_Click);

            Control[] ctrlList = new Control[] { _gbMax, _gbInit };

            foreach (Control ctrl in ctrlList)
            {
                foreach (Control c in ctrl.Controls)
                {
                    if (c is TextBox)
                    {
                        ((TextBox)c).Leave += new EventHandler(On_TextBox_Leave);
                        ((TextBox)c).KeyDown += new KeyEventHandler(On_TextBox_KeyDown);
                    }
                }
            }

            _getFromComps.Click += new EventHandler(On_GetFromComps_Click);
            _getFromRepairs.Click += new EventHandler(On_GetFromRepairs_Click);
        }
        #endregion

        void SaveData()
        {
            CalculateOfferManager.Instance.SaveData();
            base.AutoSaveDocument();
        }

        #region MAP DATA TO OBJECT
        void MapDataToObject()
        {
            CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.ConservativePropertyValue = Util.GetDecimalValue(_IO_C_PropValue.Text);
            CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.ConservativeRepairsValue = Util.GetDecimalValue(_IO_C_Repairs.Text);
            CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.AsIsValue = Util.GetDecimalValue(_IO_C_AsIsValue.Text);
            CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.FirstOfferMultiplierValue = Util.GetDecimalValue(_IO_C_FirstOfferMultiplier.Text);
            CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialOfferSalesPriceValue = Util.GetDecimalValue(_IO_SalesPrice.Text);
            CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialRealEstateComissionPerc = Util.GetDecimalValue(_IO_REC_Perc.Text);
            CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialRealEstateComissionValue = Util.GetDecimalValue(_IO_REC_Value.Text);
            CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialSellerConcessionPerc= Util.GetDecimalValue(_IO_SC_Perc.Text);
            CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialSellerConcessionValue= Util.GetDecimalValue(_IO_SC_Value.Text);
            CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialSellerClosingCostPerc = Util.GetDecimalValue(_IO_CC_Perc.Text);
            CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialSellerClosingCostValue = Util.GetDecimalValue(_IO_CC_Value.Text);
            CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.SecondPayOff = Util.GetDecimalValue(_IO_PayOff.Text);
            CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialOfferToFirstLender = Util.GetDecimalValue(_IO_Net.Text);

            //MAX OFFER
            CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.RealisticPropertyValue = Util.GetDecimalValue(_MO_RealValue.Text);
            CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.RealisticMultiplierValue = Util.GetDecimalValue(_MO_Real_StrategyMult.Text);
            CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.RealisticRepairsValue = Util.GetDecimalValue(_MO_Repairs.Text);
            CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.MaximumOfferSalesPriceValue = Util.GetDecimalValue(_MO_SalesPrice.Text);
            CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.RealisticRealEstateComissionPerc = Util.GetDecimalValue(_MO_REC_Perc.Text);
            CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.RealisticRealEstateComissionValue = Util.GetDecimalValue(_MO_REC_Value.Text);
            CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.RealisticSellerConcessionPerc = Util.GetDecimalValue(_MO_SC_Perc.Text);
            CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.RealisticSellerConcessionValue = Util.GetDecimalValue(_MO_SC_Value.Text);
            CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.RealisticSellerClosingCostPerc = Util.GetDecimalValue(_MO_CC_Perc.Text);
            CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.RealisticSellerClosingCostValue = Util.GetDecimalValue(_MO_CC_Value.Text);
            CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.RealisticSecondPayOff = Util.GetDecimalValue(_MO_PayOff.Text);
            CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.MaximumOfferNetToFirstLender = Util.GetDecimalValue(_MO_Net.Text);
            SaveData();            
        }
        #endregion

        #region SET DATA
        void SetData()
        {
            try
            {
                //INITIAL OFFER
                _IO_C_PropValue.Text = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.ConservativePropertyValue.ToString(Formatter);
                _IO_C_Repairs.Text = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.ConservativeRepairsValue.ToString(Formatter);
                _IO_C_AsIsValue.Text = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.AsIsValue.ToString(Formatter);
                _IO_C_FirstOfferMultiplier.Text = (0 == CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.FirstOfferMultiplierValue ? "0.75" : CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.FirstOfferMultiplierValue.ToString(Formatter));
                _IO_SalesPrice.Text = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialOfferSalesPriceValue.ToString(Formatter);
                _IO_REC_Perc.Text = (0 == CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialRealEstateComissionPerc ? "6" : CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialRealEstateComissionPerc.ToString(Formatter));
                _IO_REC_Value.Text = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialRealEstateComissionValue.ToString(Formatter);
                _IO_SC_Perc.Text = (0 == CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialSellerConcessionPerc ? "3" : CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialSellerConcessionPerc.ToString(Formatter));
                _IO_SC_Value.Text = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialSellerConcessionValue.ToString(Formatter);
                _IO_CC_Perc.Text = (0 == CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialSellerClosingCostPerc) ? "1" : CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialSellerClosingCostPerc.ToString(Formatter);
                _IO_CC_Value.Text = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialSellerClosingCostValue.ToString(Formatter);
                _IO_PayOff.Text = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.SecondPayOff.ToString(Formatter);
                _IO_Net.Text = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialOfferToFirstLender.ToString(Formatter);

                //MAX OFFER
                _MO_RealValue.Text = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.RealisticPropertyValue.ToString(Formatter);
                _MO_Real_StrategyMult.Text = (0 == CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.RealisticMultiplierValue ? "0.85" : CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.RealisticMultiplierValue.ToString(Formatter));
                _MO_Repairs.Text = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.RealisticRepairsValue.ToString(Formatter);
                _MO_SalesPrice.Text = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.MaximumOfferSalesPriceValue.ToString(Formatter);
                _MO_REC_Perc.Text = (0 == CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.RealisticRealEstateComissionPerc) ? "6" : CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.RealisticRealEstateComissionPerc.ToString(Formatter);
                _MO_REC_Value.Text = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.RealisticRealEstateComissionValue.ToString(Formatter);
                _MO_SC_Perc.Text = (0 == CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.RealisticSellerConcessionPerc ? "3" : CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.RealisticSellerClosingCostPerc.ToString(Formatter));
                _MO_SC_Value.Text = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.RealisticSellerConcessionValue.ToString(Formatter);
                _MO_CC_Perc.Text = (0 == CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.RealisticSellerClosingCostPerc) ? "1" : CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.RealisticSellerClosingCostPerc.ToString(Formatter);
                _MO_CC_Value.Text = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.RealisticSellerClosingCostValue.ToString(Formatter);
                _MO_PayOff.Text = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.RealisticSecondPayOff.ToString(Formatter);
                _MO_Net.Text = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.MaximumOfferNetToFirstLender.ToString(Formatter);
            }
            catch { }
        }
        #endregion

        void InitRealisticSellerClosingCostBreakDowns()
        {
            CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.RealisticSellerClosingCostBreakDown.EscrowFeeValue = (0.25m) * CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.RealisticSellerClosingCostValue;
            CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.RealisticSellerClosingCostBreakDown.TitleInsuranceValue = (0.5m) * CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.RealisticSellerClosingCostValue;
            CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.RealisticSellerClosingCostBreakDown.AppraisalFeeValue = (0.2m) * CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.RealisticSellerClosingCostValue;
            CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.RealisticSellerClosingCostBreakDown.RecordingFeeValue = (0.05m) * CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.RealisticSellerClosingCostValue;
        }
        
        //Max Offer Closing Cost - Left Button
        void On_MO_CC_L_Click(object sender, EventArgs e)
        {
            EForm bsForm = new EForm("CLOSING COST (entries on the Seller's side of HUD1)");

            if (null == CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.RealisticSellerClosingCostBreakDown ||
                null == CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.RealisticBuyerClosingCostBreakDown
                )
            {
                if (null == CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.RealisticSellerClosingCostBreakDown)
                    CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.RealisticSellerClosingCostBreakDown = new ClosingCostBreakDownData();

                if (null == CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.RealisticBuyerClosingCostBreakDown)
                    CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.RealisticBuyerClosingCostBreakDown = new ClosingCostBreakDownData();

                InitRealisticSellerClosingCostBreakDowns();

                bsForm.S_BreakDownData = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.RealisticSellerClosingCostBreakDown;                                
            }
            else if (Util.GetDecimalValue(_MO_CC_Value.Text) != (CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.RealisticSellerClosingCostBreakDown.GetTotal() + CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.RealisticBuyerClosingCostBreakDown.GetTotal()))
            {
                //set real just as below, since we can't recreate perc. breakdown
                bsForm.S_BreakDownData = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.RealisticSellerClosingCostBreakDown;
                bsForm.B_BreakDownData = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.RealisticBuyerClosingCostBreakDown;
            }
            else
            {
                bsForm.S_BreakDownData = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.RealisticSellerClosingCostBreakDown;
                bsForm.B_BreakDownData = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.RealisticBuyerClosingCostBreakDown;
            }

            if (DialogResult.OK == bsForm.ShowDialog())
            {
                CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.RealisticSellerClosingCostBreakDown = bsForm.S_BreakDownData;
                CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.RealisticBuyerClosingCostBreakDown = bsForm.B_BreakDownData;

                CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.RealisticSellerClosingCostValue = (/*Util.GetDecimalValue(bsForm.B_Total)*/ + Util.GetDecimalValue(bsForm.S_Total));
                _MO_CC_Value.Text = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.RealisticSellerClosingCostValue.ToString(Formatter);

                try
                {
                    CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.RealisticSellerClosingCostPerc =
                        ((/*Util.GetDecimalValue(bsForm.B_Total) +*/ Util.GetDecimalValue(bsForm.S_Total))
                        / CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.MaximumOfferSalesPriceValue) * 100;
                }
                catch
                {
                    CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.RealisticSellerClosingCostPerc = 0;
                }

                _MO_CC_Perc.Text = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.RealisticSellerClosingCostPerc.ToString(Formatter);

                UpdateRealisticTotal();
                MapDataToObject();
                //SaveData();
            }
        }

        //Max Offer Seller's Concession
        void On_MO_SC_Click(object sender, EventArgs e)
        {
            BForm sellerConcessionForm = new BForm(Util.GetDecimalValue(_MO_SC_Perc.Text));

            //if there is NO data, set dummy
            if (null == CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.RealisticSellerConcessionBreakDown)                
            {
                sellerConcessionForm.LoanOriginatorFee = (decimal)(1) / 3 * Util.GetDecimalValue(_MO_SC_Perc.Text);
                sellerConcessionForm.LoanDiscountFee = (decimal)(2) / 3 * Util.GetDecimalValue(_MO_SC_Perc.Text);
                sellerConcessionForm.Total = Util.GetDecimalValue(_MO_SC_Perc.Text);
            }
            else if (null != CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.RealisticSellerConcessionBreakDown && Util.GetDecimalValue(_MO_SC_Perc.Text) != CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.RealisticSellerConcessionBreakDown.Total)
            {
                //same as above, since we don't store label names
                sellerConcessionForm.LoanOriginatorFee = (decimal)(1) / 3 * Util.GetDecimalValue(_MO_SC_Perc.Text);
                sellerConcessionForm.LoanDiscountFee = (decimal)(2) / 3 * Util.GetDecimalValue(_MO_SC_Perc.Text);
                sellerConcessionForm.Total = Util.GetDecimalValue(_MO_SC_Perc.Text);
            }
            else
                sellerConcessionForm.BreakDownData = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.RealisticSellerConcessionBreakDown;

            if (DialogResult.OK == sellerConcessionForm.ShowDialog())
            {
                CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.RealisticSellerConcessionBreakDown = sellerConcessionForm.BreakDownData;

                _MO_SC_Perc.Text = sellerConcessionForm.BreakDownData.Total.ToString(Formatter);
                _MO_SC_Value.Text = (sellerConcessionForm.BreakDownData.Total * Util.GetDecimalValue(_MO_SalesPrice.Text) / 100).ToString(Formatter);

                UpdateRealisticTotal();
                MapDataToObject();
                //SaveData();
            }
        }

        //Max Offer Real Estate Comission
        void On_MO_REC_Click(object sender, EventArgs e)
        {
            AForm realEstateCommissionForm = new AForm(Util.GetDecimalValue(_MO_REC_Perc.Text));

            //if there is NO data --> set dummy
            if (null == CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.RealisticRealEstateCommisionBreakDown)
            {
                realEstateCommissionForm.ListingAgencyName = "> Enter  name of Listing Agency <:";
                realEstateCommissionForm.SellingAgencyName = @" Enter  name of Selling Agency <:";
                realEstateCommissionForm.ListingAgencyValue = 0.5m * Util.GetDecimalValue(_MO_REC_Perc.Text);
                realEstateCommissionForm.SellingAgencyValue = 0.5m * Util.GetDecimalValue(_MO_REC_Perc.Text);
                realEstateCommissionForm.Total = Util.GetDecimalValue(_MO_REC_Perc.Text);

            }
            else if (Util.GetDecimalValue(_MO_REC_Perc.Text) != CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.RealisticRealEstateCommisionBreakDown.Total)
            {
                //set adjusted data
                realEstateCommissionForm.ListingAgencyName = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.RealisticRealEstateCommisionBreakDown.NameOfListingAgency;
                realEstateCommissionForm.SellingAgencyName = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.RealisticRealEstateCommisionBreakDown.NameOfSellingAgency;
                realEstateCommissionForm.ListingAgencyValue = 0.5m * Util.GetDecimalValue(_MO_REC_Perc.Text);
                realEstateCommissionForm.SellingAgencyValue = 0.5m * Util.GetDecimalValue(_MO_REC_Perc.Text);
                realEstateCommissionForm.Total = Util.GetDecimalValue(_MO_REC_Perc.Text);
            }
            else
            {
                //set real data                
                realEstateCommissionForm.BreakDownData = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.RealisticRealEstateCommisionBreakDown;
            }
            
            if (DialogResult.OK == realEstateCommissionForm.ShowDialog())
            {
                CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.RealisticRealEstateCommisionBreakDown = realEstateCommissionForm.BreakDownData;

                _MO_REC_Perc.Text = realEstateCommissionForm.BreakDownData.Total.ToString(Formatter);
                _MO_REC_Value.Text = (realEstateCommissionForm.BreakDownData.Total * Util.GetDecimalValue(_MO_SalesPrice.Text) / 100).ToString(Formatter);

                UpdateRealisticTotal();
                MapDataToObject();
                //SaveData();
            }            
        }

        //Initial Offer NET to 1st Lender
        void On_IO_L_Click(object sender, EventArgs e)
        {
            // >> TO HUD
            ExportToExternal();
        }

        void InitInitialSellerClosingCostBreakDowns()
        {
            CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialSellerClosingCostBreakDown.EscrowFeeValue = (0.25m) * CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialSellerClosingCostValue;
            CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialSellerClosingCostBreakDown.TitleInsuranceValue = (0.5m) * CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialSellerClosingCostValue;
            CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialSellerClosingCostBreakDown.AppraisalFeeValue = (0.2m) * CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialSellerClosingCostValue;
            CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialSellerClosingCostBreakDown.RecordingFeeValue = (0.05m) * CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialSellerClosingCostValue; 
        }

        //Initial Offer Closing Cost - Left Button
        void On_IO_CC_L_Click(object sender, EventArgs e)
        {
            EForm bsForm = new EForm("CLOSING COST (entries on the Seller's side of HUD1)");

            //if there is NO data, set dummy
            if (null == CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialSellerClosingCostBreakDown ||
                null == CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialBuyerClosingCostBreakDown)            
            {
                if (null == CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialSellerClosingCostBreakDown)
                    CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialSellerClosingCostBreakDown = new ClosingCostBreakDownData();

                if (null == CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialBuyerClosingCostBreakDown)
                    CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialBuyerClosingCostBreakDown = new ClosingCostBreakDownData();

                InitInitialSellerClosingCostBreakDowns();                

                bsForm.S_BreakDownData = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialSellerClosingCostBreakDown;                
            }
            else if (Util.GetDecimalValue(_IO_CC_Value.Text) != (CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialSellerClosingCostBreakDown.GetTotal() + CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialBuyerClosingCostBreakDown.GetTotal()))
            {
                //set real just as below, since we can't recreate perc. breakdown
                bsForm.S_BreakDownData = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialSellerClosingCostBreakDown;
                bsForm.B_BreakDownData = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialBuyerClosingCostBreakDown;
            }
            else
            {
                bsForm.S_BreakDownData = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialSellerClosingCostBreakDown;
                bsForm.B_BreakDownData = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialBuyerClosingCostBreakDown;
            }

            if (DialogResult.OK == bsForm.ShowDialog())
            {
                CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialSellerClosingCostBreakDown = bsForm.S_BreakDownData;
                CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialBuyerClosingCostBreakDown = bsForm.B_BreakDownData;

                CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialSellerClosingCostValue = /*Util.GetDecimalValue(bsForm.B_Total) +*/ Util.GetDecimalValue(bsForm.S_Total);
                _IO_CC_Value.Text = (/*Util.GetDecimalValue(bsForm.B_Total) +*/ Util.GetDecimalValue(bsForm.S_Total)).ToString(Formatter);

                try
                {
                    CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialSellerClosingCostPerc =
                                ((/*Util.GetDecimalValue(bsForm.B_Total) + */Util.GetDecimalValue(bsForm.S_Total)) /
                                CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialOfferSalesPriceValue) * 100;
                }
                catch
                {
                    CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialSellerClosingCostPerc = 0;
                }

                _IO_CC_Perc.Text = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialSellerClosingCostPerc.ToString(Formatter);

                UpdateTotal();
                MapDataToObject();
                //SaveData();
            }
        }

        //Initial Offer Seller's Concession
        void On_IO_SC_Click(object sender, EventArgs e)
        {
            BForm sellerConcessionForm = new BForm(Util.GetDecimalValue(_IO_SC_Perc.Text));
            
            //if there is NO data, set dummy
            if (null == CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialSellerConcessionBreakDown)                
            {
                sellerConcessionForm.LoanOriginatorFee = (decimal)(1) / 3 * Util.GetDecimalValue(_IO_SC_Perc.Text);
                sellerConcessionForm.LoanDiscountFee = (decimal)(2) / 3 * Util.GetDecimalValue(_IO_SC_Perc.Text);
                sellerConcessionForm.Total = Util.GetDecimalValue(_IO_SC_Perc.Text);
            }
            else if (null != CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialSellerConcessionBreakDown && 
                Util.GetDecimalValue(_IO_SC_Perc.Text) != CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialSellerConcessionBreakDown.Total)
            {
                // same as above, since we don't store label names
                sellerConcessionForm.LoanOriginatorFee = (decimal)(1) / 3 * Util.GetDecimalValue(_IO_SC_Perc.Text);
                sellerConcessionForm.LoanDiscountFee = (decimal)(2) / 3 * Util.GetDecimalValue(_IO_SC_Perc.Text);
                sellerConcessionForm.Total = Util.GetDecimalValue(_IO_SC_Perc.Text);
            }
            else
                sellerConcessionForm.BreakDownData = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialSellerConcessionBreakDown;

            if (DialogResult.OK == sellerConcessionForm.ShowDialog())
            {
                CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialSellerConcessionBreakDown = sellerConcessionForm.BreakDownData;
                
                _IO_SC_Perc.Text = sellerConcessionForm.BreakDownData.Total.ToString(Formatter);
                _IO_SC_Value.Text = (sellerConcessionForm.BreakDownData.Total * Util.GetDecimalValue(_IO_SalesPrice.Text) / 100).ToString(Formatter);

                UpdateTotal();
                MapDataToObject();
                //SaveData();
            }
        }

        //Initial Offer Real Estate Comission
        void On_IO_REC_Click(object sender, EventArgs e)
        {
            AForm realEstateCommissionForm = new AForm(Util.GetDecimalValue(_IO_REC_Perc.Text));                        

            //if there is NO data --> set dummy
            if (null == CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialRealEstateCommisionBreakDown)
            {
                realEstateCommissionForm.ListingAgencyName = "> Enter  name of Listing Agency <:";
                realEstateCommissionForm.SellingAgencyName = "> Enter  name of Selling Agency <:";
                realEstateCommissionForm.ListingAgencyValue = 0.5m * Util.GetDecimalValue(_IO_REC_Perc.Text);
                realEstateCommissionForm.SellingAgencyValue = 0.5m * Util.GetDecimalValue(_IO_REC_Perc.Text);
                realEstateCommissionForm.Total = Util.GetDecimalValue(_IO_REC_Perc.Text);

            }
            else if (null != CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialRealEstateCommisionBreakDown &&
                Util.GetDecimalValue(_IO_REC_Perc.Text) != CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialRealEstateCommisionBreakDown.Total)
            {
                //set adjusted data
                realEstateCommissionForm.ListingAgencyName = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialRealEstateCommisionBreakDown.NameOfListingAgency;
                realEstateCommissionForm.SellingAgencyName = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialRealEstateCommisionBreakDown.NameOfSellingAgency;
                realEstateCommissionForm.ListingAgencyValue = 0.5m * Util.GetDecimalValue(_IO_REC_Perc.Text);
                realEstateCommissionForm.SellingAgencyValue = 0.5m * Util.GetDecimalValue(_IO_REC_Perc.Text);
                realEstateCommissionForm.Total = Util.GetDecimalValue(_IO_REC_Perc.Text);
            }
            else // set REAL data            
            {                                
                realEstateCommissionForm.BreakDownData = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialRealEstateCommisionBreakDown;
            }

            if (DialogResult.OK == realEstateCommissionForm.ShowDialog())
            {
                CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialRealEstateCommisionBreakDown = realEstateCommissionForm.BreakDownData;
                _IO_REC_Perc.Text = realEstateCommissionForm.BreakDownData.Total.ToString(Formatter);
                _IO_REC_Value.Text = (realEstateCommissionForm.BreakDownData.Total * Util.GetDecimalValue(_IO_SalesPrice.Text) / 100).ToString(Formatter);

                UpdateTotal();
                MapDataToObject();
                //SaveData();
            }
        }

        void On_TextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                HandleMapping(sender);
            }
        }

        void On_TextBox_Leave(object sender, EventArgs e)
        {
            HandleMapping(sender);
        }

        void HandleMapping(object sender)
        {            
            MapDataToObject();

            if (sender == _IO_C_PropValue)
            {
                UpdateAsIs();
            }

            if (sender == _IO_C_Repairs)
            {
                UpdateAsIs();
            }

            if (sender == _IO_C_FirstOfferMultiplier)
            {
                UpdateInitialOfferSalesPrice();
            }

            if (sender == _IO_C_FirstOfferMultiplier)
            { }

            if (sender == _IO_REC_Perc)
            {                
                _IO_REC_Value.Text = (Util.GetDecimalValue(_IO_SalesPrice.Text) * Util.GetDecimalValue(_IO_REC_Perc.Text) / 100).ToString(Formatter);
                UpdateTotal();

                if (null == CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialRealEstateCommisionBreakDown)
                {
                    On_IO_REC_Click(null, null);
                }
                else if (null != CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialRealEstateCommisionBreakDown)
                {
                    if (Util.GetDecimalValue(_IO_REC_Perc.Text) != CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialRealEstateCommisionBreakDown.Total)
                        On_IO_REC_Click(null, null);
                }
            }

            if (sender == _IO_SC_Perc)
            {
                _IO_SC_Value.Text = (Util.GetDecimalValue(_IO_SalesPrice.Text) * Util.GetDecimalValue(_IO_SC_Perc.Text) / 100).ToString(Formatter);
                UpdateTotal();

                if (null == CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialSellerConcessionBreakDown)
                {
                    On_IO_SC_Click(null, null);
                }
                else if (null != CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialSellerConcessionBreakDown)
                {
                    if (Util.GetDecimalValue(_IO_SC_Perc.Text) != CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialSellerConcessionBreakDown.Total)
                        On_IO_SC_Click(null, null);
                }
            }

            if (sender == _IO_CC_Perc)
            {
                _IO_CC_Value.Text = (Util.GetDecimalValue(_IO_SalesPrice.Text) * Util.GetDecimalValue(_IO_CC_Perc.Text) / 100).ToString(Formatter);

                if (null == CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialSellerClosingCostBreakDown && null != CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialBuyerClosingCostBreakDown)
                {
                    On_IO_CC_L_Click(null, null);
                }
                else if (null != CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialSellerClosingCostBreakDown && null != CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialBuyerClosingCostBreakDown)
                {
                    if (Util.GetDecimalValue(_IO_CC_Value.Text) != (CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialSellerClosingCostBreakDown.GetTotal() + CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialBuyerClosingCostBreakDown.GetTotal()))
                        On_IO_CC_L_Click(null, null);
                }

                UpdateTotal();
            }

            if (sender == _MO_RealValue)
            {
                UpdatemaximumOfferSalesPrice();
            }

            if (sender == _MO_Real_StrategyMult)
            {
                UpdatemaximumOfferSalesPrice();
            }

            if (sender == _MO_Repairs)
            {
                UpdatemaximumOfferSalesPrice();
            }

            if (sender == _MO_REC_Perc)
            {
                _MO_REC_Value.Text = (Util.GetDecimalValue(_MO_SalesPrice.Text) * Util.GetDecimalValue(_MO_REC_Perc.Text) / 100).ToString(Formatter);
                UpdateRealisticTotal();

                if (null == CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.RealisticRealEstateCommisionBreakDown)
                {
                    On_MO_REC_Click(null, null);
                }
                else if (null != CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.RealisticRealEstateCommisionBreakDown)
                {
                    if (Util.GetDecimalValue(_MO_REC_Perc.Text) != CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.RealisticRealEstateCommisionBreakDown.Total)
                        On_MO_REC_Click(null, null);
                }
            }

            if (sender == _MO_SC_Perc)
            {
                _MO_SC_Value.Text = (Util.GetDecimalValue(_MO_SalesPrice.Text) * Util.GetDecimalValue(_MO_SC_Perc.Text) / 100).ToString(Formatter);
                UpdateRealisticTotal();

                if (null == CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.RealisticSellerConcessionBreakDown)
                {
                    On_MO_SC_Click(null, null);
                }
                else if (null != CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.RealisticSellerConcessionBreakDown)
                {
                    if (Util.GetDecimalValue(_MO_SC_Perc.Text) != CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.RealisticSellerConcessionBreakDown.Total)
                        On_MO_SC_Click(null, null);
                }
            }

            if (sender == _MO_CC_Perc)
            {
                _MO_CC_Value.Text = (Util.GetDecimalValue(_MO_SalesPrice.Text) * Util.GetDecimalValue(_MO_CC_Perc.Text) / 100).ToString(Formatter);

                if (null == CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialSellerClosingCostBreakDown && null != CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialBuyerClosingCostBreakDown)
                {
                    On_MO_CC_L_Click(null, null);
                }
                else if (null != CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialSellerClosingCostBreakDown && null != CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialBuyerClosingCostBreakDown)
                {
                    if (Util.GetDecimalValue(_MO_CC_Value.Text) != (CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.RealisticSellerClosingCostBreakDown.GetTotal() + CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.RealisticBuyerClosingCostBreakDown.GetTotal()))
                        On_MO_CC_L_Click(null, null);
                }

                UpdateRealisticTotal();
            }

            if (sender == _MO_PayOff)
            {
                UpdateRealisticTotal();
            }

            if (sender == _IO_PayOff)
            {
                UpdateTotal();
            }

            ((TextBox)(sender)).Text = Util.GetDecimalValue(((TextBox)(sender)).Text).ToString(Formatter);
        }
        
        void On_GetFromRepairs_Click(object sender, EventArgs e)
        {
            if (null != RepairsManager.Instance.P_RepairObject)
            {
                _IO_C_Repairs.Text = RepairsManager.Instance.P_RepairObject.Total.ToString(Formatter);
                CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.ConservativeRepairsValue = RepairsManager.Instance.P_RepairObject.Total;
                UpdateAsIs();

                _MO_Repairs.Text = RepairsManager.Instance.P_RepairObject.Total.ToString(Formatter);
                CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.RealisticRepairsValue = RepairsManager.Instance.P_RepairObject.Total;
                UpdatemaximumOfferSalesPrice();
            }
        }

        void On_GetFromComps_Click(object sender, EventArgs e)
        {
            _IO_C_PropValue.Text = CompsManager.Instance.P_comps.P_ConservativeValue.ToString(Formatter);
            CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.ConservativePropertyValue = CompsManager.Instance.P_comps.P_ConservativeValue;
            UpdateAsIs();

            _MO_RealValue.Text = CompsManager.Instance.P_comps.P_RealisticValue.ToString(Formatter);
            CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.RealisticPropertyValue = CompsManager.Instance.P_comps.P_RealisticValue;
            UpdatemaximumOfferSalesPrice();

        }

        void UpdateAsIs()
        {
            _IO_C_AsIsValue.Text = (Util.GetDecimalValue(_IO_C_PropValue.Text) - Util.GetDecimalValue(_IO_C_Repairs.Text)).ToString(Formatter);
            UpdateInitialOfferSalesPrice();
        }

        void UpdateInitialOfferSalesPrice()
        {
            _IO_SalesPrice.Text = (Util.GetDecimalValue(_IO_C_AsIsValue.Text) * Util.GetDecimalValue(_IO_C_FirstOfferMultiplier.Text)).ToString(Formatter);
            UpdateValuePairs();
        }

        void UpdateValuePairs()
        {
            _IO_REC_Value.Text = (Util.GetDecimalValue(_IO_SalesPrice.Text) * Util.GetDecimalValue(_IO_REC_Perc.Text) / 100).ToString(Formatter);
            _IO_SC_Value.Text = (Util.GetDecimalValue(_IO_SalesPrice.Text) * Util.GetDecimalValue(_IO_SC_Perc.Text) / 100).ToString(Formatter);
            _IO_CC_Value.Text = (Util.GetDecimalValue(_IO_SalesPrice.Text) * Util.GetDecimalValue(_IO_CC_Perc.Text) / 100).ToString(Formatter);

            UpdateTotal();
        }

        void UpdateTotal()
        {
            _IO_Net.Text = (Util.GetDecimalValue(_IO_SalesPrice.Text) -
                                      Util.GetDecimalValue(_IO_REC_Value.Text) -
                                      Util.GetDecimalValue(_IO_SC_Value.Text) -
                                      Util.GetDecimalValue(_IO_CC_Value.Text) -
                                      Util.GetDecimalValue(_IO_PayOff.Text)).ToString(Formatter);

            MapDataToObject();
            //SaveData();
        }

        void UpdatemaximumOfferSalesPrice()
        {            
            _MO_SalesPrice.Text = ((Util.GetDecimalValue(_MO_RealValue.Text) * Util.GetDecimalValue(_MO_Real_StrategyMult.Text)) - Util.GetDecimalValue(_MO_Repairs.Text)).ToString(Formatter);
            UpdateRealisticValuePairs();

        }

        void UpdateRealisticValuePairs()
        {
            //_MO_REC_Value = x * _MO_REC_Perc
            _MO_REC_Value.Text = (Util.GetDecimalValue(_MO_SalesPrice.Text) * Util.GetDecimalValue(_MO_REC_Perc.Text) / 100).ToString(Formatter);
            _MO_SC_Value.Text = (Util.GetDecimalValue(_MO_SalesPrice.Text) * Util.GetDecimalValue(_MO_SC_Perc.Text) / 100).ToString(Formatter);
            _MO_CC_Value.Text = (Util.GetDecimalValue(_MO_SalesPrice.Text) * Util.GetDecimalValue(_MO_CC_Perc.Text) / 100).ToString(Formatter);
                        
            UpdateRealisticTotal();
        }

        void UpdateRealisticTotal()
        {            
            _MO_Net.Text = (Util.GetDecimalValue(_MO_SalesPrice.Text) -
                            Util.GetDecimalValue(_MO_REC_Value.Text) -
                            Util.GetDecimalValue(_MO_SC_Value.Text) -
                            Util.GetDecimalValue(_MO_CC_Value.Text) -
                            Util.GetDecimalValue(_MO_PayOff.Text)).ToString(Formatter);

            MapDataToObject();
            //SaveData();
        }

        #region EXPORT TO HUD
        void ExportToExternal()
        {
            string hudFileName = string.Empty;
            bool existingHUDFileName = HUDFileNameManager.Instance.GetCreateDocument(Application.StartupPath, DataManager.Instance.DocumentName, 1, out hudFileName);

            string path = Application.StartupPath + @"\" + StringStore.SavedExtDocumentsFolder;
            string suffix = StringStore.HUDSubFolder;
            
            HUD hud = new HUD();            
            hud.SetFileName(path, suffix, path + suffix + hudFileName);
            if (existingHUDFileName)
                hud.ForceLoad(path + suffix + hudFileName);

            hud.SetCash();

            try
            {
                string accountNr = string.Empty;
                try { accountNr = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[0].AccountNr; }
                catch { accountNr = string.Empty; }
                hud.SetLoanNumber(accountNr);
            }
            catch { }

            try
            {
                decimal initialOfferSalesPrice = 0;
                decimal comission = 0;

                try { initialOfferSalesPrice = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialOfferSalesPriceValue; }
                catch { initialOfferSalesPrice = 0; }
            
                try { comission = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialRealEstateComissionPerc; }
                catch { comission = 0; }

                hud.SetSalesPriceDetails(initialOfferSalesPrice, comission);

            }
            catch { }

            try
            {
                string firstCompanyName = string.Empty;
                string secondCompanyName = string.Empty;
                decimal initialOfferNetToFirst = 0;
                decimal initialOfferNetToSecond = 0;

                try { firstCompanyName = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[0].CompanyName; }
                catch { firstCompanyName = string.Empty; }

                try { initialOfferNetToFirst = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialOfferToFirstLender; }
                catch { initialOfferNetToFirst = 0; }

                try { secondCompanyName = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[1].CompanyName; }
                catch { secondCompanyName = string.Empty; }

                try { initialOfferNetToSecond = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[0].InitialOfferToSecondLender; }
                catch { initialOfferNetToSecond = 0; }

                hud.SetCompanyDetails(firstCompanyName, initialOfferNetToFirst, secondCompanyName, initialOfferNetToSecond);
            }
            catch { }

            try
            {
                hud.SetValues(DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.BuyerInfo.FullName,
                              DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.BuyerInfo.AddressInfo.SiteAddress.Address.SiteStreetAddress,
                              DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.OwnerInfo.FullName,
                              DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.OwnerInfo.AddressInfo.MailAddress.Address.SiteStreetAddress,
                              DataManager.Instance.GetFullSiteAddress());
            }
            catch { }

            //set initial offer sales price
            try
            {
                decimal initOfferSalesPrice = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialOfferSalesPriceValue;
                hud.SetIntialOfferSalesPrice(initOfferSalesPrice);
            }
            catch { }

            //put second lender name-value pair into 505

            string nameOfListingAgency = string.Empty;
            try { nameOfListingAgency = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialRealEstateCommisionBreakDown.NameOfListingAgency; }
            catch { nameOfListingAgency = string.Empty; }

            string nameOfSellingAgency = "";
            try { nameOfSellingAgency = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialRealEstateCommisionBreakDown.NameOfSellingAgency; }
            catch { nameOfSellingAgency = string.Empty; }

            decimal listingAgencyValue = 0;
            try { listingAgencyValue = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialRealEstateCommisionBreakDown.ListingAgencyValue * CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialOfferSalesPriceValue / 100; }
            catch { listingAgencyValue = 0; }

            decimal sellingAgencyValue = 0;
            try { sellingAgencyValue = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialRealEstateCommisionBreakDown.SellingAgencyValue * CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialOfferSalesPriceValue / 100; }
            catch { sellingAgencyValue = 0; }

            hud.SetRealEstateCommissionValues(nameOfListingAgency, nameOfSellingAgency, listingAgencyValue, sellingAgencyValue);

            decimal loanOriginationFee = 0;
            try 
            {
                if (null == CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialSellerConcessionBreakDown)
                {
                    loanOriginationFee = 1 * CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialOfferSalesPriceValue / 100;
                }
                else
                {
                    loanOriginationFee = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialSellerConcessionBreakDown.LoanOriginationFeeValue * CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialOfferSalesPriceValue / 100;
                }
            }
            catch { loanOriginationFee = 0; }

            decimal loanDiscountFee = 0;
            try 
            { 
                if (null == CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialSellerConcessionBreakDown)
                {
                    loanDiscountFee = 2 * CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialOfferSalesPriceValue / 100; 
                }
                else
                {
                    loanDiscountFee = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialSellerConcessionBreakDown.LoanDiscountFeeValue * CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialOfferSalesPriceValue / 100; 
                }
            }
            catch { loanDiscountFee = 0; }

            hud.SetSellerConcessionValues(loanOriginationFee, loanDiscountFee);

            decimal EscrowFeeValue = 0;
            decimal TitleInsuranceValue = 0;
            decimal AppraisalFeeValue = 0;
            decimal CreditReportValue = 0;
            decimal RecordingFeeValue = 0;
            decimal HoaTransferFeeValue = 0;
            string Others1Name = string.Empty;
            decimal Others1Value = 0;
            string Others2Name = string.Empty;
            decimal Others2Value = 0;
            string Others3Name = string.Empty;
            decimal Others3Value = 0;
            string Others4Name = string.Empty;
            decimal Others4Value = 0;
            string Others5Name = string.Empty;
            decimal Others5Value = 0;
            string Others6Name = string.Empty;
            decimal Others6Value = 0;

            try { EscrowFeeValue = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialBuyerClosingCostBreakDown.EscrowFeeValue; }
            catch { EscrowFeeValue = 0; }

            try { TitleInsuranceValue = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialBuyerClosingCostBreakDown.TitleInsuranceValue; }
            catch { TitleInsuranceValue = 0; }

            try { AppraisalFeeValue = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialBuyerClosingCostBreakDown.AppraisalFeeValue; }
            catch { AppraisalFeeValue = 0; }

            try { CreditReportValue = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialBuyerClosingCostBreakDown.CreditReportValue; }
            catch { CreditReportValue = 0; }

            try { RecordingFeeValue = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialBuyerClosingCostBreakDown.RecordingFeeValue; }
            catch { RecordingFeeValue = 0; }

            try { HoaTransferFeeValue = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialBuyerClosingCostBreakDown.HoaTransferFeeValue; }
            catch { HoaTransferFeeValue = 0; }

            try { Others1Name = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialBuyerClosingCostBreakDown.Others1Name; }
            catch { Others1Name = string.Empty; }

            try { Others1Value = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialBuyerClosingCostBreakDown.Others1Value; }
            catch { Others1Value = 0; }

            try { Others2Name = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialBuyerClosingCostBreakDown.Others2Name; }
            catch { Others2Name = string.Empty; }

            try { Others2Value = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialBuyerClosingCostBreakDown.Others2Value; }
            catch { Others2Value = 0; }

            try { Others3Name = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialBuyerClosingCostBreakDown.Others3Name; }
            catch { Others3Name = string.Empty; }

            try { Others3Value = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialBuyerClosingCostBreakDown.Others3Value; }
            catch { Others3Value = 0; }

            try { Others4Name = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialBuyerClosingCostBreakDown.Others4Name; }
            catch { Others4Name = string.Empty; }

            try { Others4Value = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialBuyerClosingCostBreakDown.Others4Value; }
            catch { Others4Value = 0; }

            try { Others5Name = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialBuyerClosingCostBreakDown.Others5Name; }
            catch { Others5Name = string.Empty; }

            try { Others5Value = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialBuyerClosingCostBreakDown.Others5Value; }
            catch { Others5Value = 0; }

            try { Others6Name = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialBuyerClosingCostBreakDown.Others6Name; }
            catch { Others6Name = string.Empty; }

            try { Others6Value = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialBuyerClosingCostBreakDown.Others6Value; }
            catch { Others6Value = 0; }


            hud.SetBuyerClosingCostValues(
                EscrowFeeValue,
                TitleInsuranceValue,
                AppraisalFeeValue,
                CreditReportValue,
                RecordingFeeValue,
                "HOA Transferr Fee",
                HoaTransferFeeValue,
                Others1Name,
                Others1Value,
                Others2Name,
                Others2Value,
                Others3Name,
                Others3Value,
                Others4Name,
                Others4Value,
                Others5Name,
                Others5Value,
                Others6Name,
                Others6Value);

            if (null == CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialSellerClosingCostBreakDown)
            {
                CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialSellerClosingCostBreakDown = new ClosingCostBreakDownData();
                InitInitialSellerClosingCostBreakDowns();
            }

            try { EscrowFeeValue = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialSellerClosingCostBreakDown.EscrowFeeValue; }
            catch { EscrowFeeValue = 0; }

            try { TitleInsuranceValue = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialSellerClosingCostBreakDown.TitleInsuranceValue; }
            catch { TitleInsuranceValue = 0; }

            try { AppraisalFeeValue = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialSellerClosingCostBreakDown.AppraisalFeeValue; }
            catch { AppraisalFeeValue = 0; }

            try { CreditReportValue = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialSellerClosingCostBreakDown.CreditReportValue; }
            catch { CreditReportValue = 0; }

            try { RecordingFeeValue = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialSellerClosingCostBreakDown.RecordingFeeValue; }
            catch { RecordingFeeValue = 0; }

            try { HoaTransferFeeValue = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialSellerClosingCostBreakDown.HoaTransferFeeValue; }
            catch { HoaTransferFeeValue = 0; }

            try { Others1Name = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialSellerClosingCostBreakDown.Others1Name; }
            catch { Others1Name = string.Empty; }

            try { Others1Value = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialSellerClosingCostBreakDown.Others1Value; }
            catch { Others1Value = 0; }

            try { Others2Name = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialSellerClosingCostBreakDown.Others2Name; }
            catch { Others2Name = string.Empty; }

            try { Others2Value = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialSellerClosingCostBreakDown.Others2Value; }
            catch { Others2Value = 0; }

            try { Others3Name = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialSellerClosingCostBreakDown.Others3Name; }
            catch { Others3Name = string.Empty; }

            try { Others3Value = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialSellerClosingCostBreakDown.Others3Value; }
            catch { Others3Value = 0; }

            try { Others4Name = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialSellerClosingCostBreakDown.Others4Name; }
            catch { Others4Name = string.Empty; }

            try { Others4Value = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialSellerClosingCostBreakDown.Others4Value; }
            catch { Others4Value = 0; }

            try { Others5Name = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialSellerClosingCostBreakDown.Others5Name; }
            catch { Others5Name = string.Empty; }

            try { Others5Value = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialSellerClosingCostBreakDown.Others5Value; }
            catch { Others5Value = 0; }

            try { Others6Name = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialSellerClosingCostBreakDown.Others6Name; }
            catch { Others6Name = string.Empty; }

            try { Others6Value = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialSellerClosingCostBreakDown.Others6Value; }
            catch { Others6Value = 0; }

            hud.SetSellerClosingCostValues(
                EscrowFeeValue,
                TitleInsuranceValue,
                AppraisalFeeValue,
                CreditReportValue,
                RecordingFeeValue,
                "HOA Transferr Fee",
                HoaTransferFeeValue, 
                Others1Name,
                Others1Value,
                Others2Name,
                Others2Value,
                Others3Name,
                Others3Value,
                Others4Name,
                Others4Value,
                Others5Name,
                Others5Value,
                Others6Name,
                Others6Value);

            //set 2nd payOff
            decimal secondPayOff = 0;

            try { secondPayOff = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.SecondPayOff;}
            catch { secondPayOff = 0; }

            hud.Set2ndPayOff(secondPayOff);

            hud.ShowDialog();
        }
        #endregion

        private void _IO_Net_TextChanged(object sender, EventArgs e)
        {

        }

        
    }
}
