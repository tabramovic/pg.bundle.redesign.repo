﻿namespace DealMaker.UserControls.CalculateOffer
{
    partial class EForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this._S_titleInsurance = new System.Windows.Forms.TextBox();
            this._S_escrowFee = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this._S_creditReport = new System.Windows.Forms.TextBox();
            this._S_appraisalFee = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this._S_hoaTransferFee = new System.Windows.Forms.TextBox();
            this._S_recordingFee = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this._S_others2Value = new System.Windows.Forms.TextBox();
            this._S_others1Value = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this._S_others4Value = new System.Windows.Forms.TextBox();
            this._S_others3Value = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this._S_others6Value = new System.Windows.Forms.TextBox();
            this._S_others5Value = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this._S_total = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this._B_total = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this._B_others6Value = new System.Windows.Forms.TextBox();
            this._B_others5Value = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this._B_others4Value = new System.Windows.Forms.TextBox();
            this._B_others3Value = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this._B_others2Value = new System.Windows.Forms.TextBox();
            this._B_others1Value = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this._B_hoaTransferFee = new System.Windows.Forms.TextBox();
            this._B_recordingFee = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this._B_creditReport = new System.Windows.Forms.TextBox();
            this._B_appraisalFee = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this._B_titleInsurance = new System.Windows.Forms.TextBox();
            this._B_escrowFee = new System.Windows.Forms.TextBox();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this._loadDefaultValues = new System.Windows.Forms.Button();
            this._saveAsDefaultValues = new System.Windows.Forms.Button();
            this._others1 = new System.Windows.Forms.TextBox();
            this._others2 = new System.Windows.Forms.TextBox();
            this._others3 = new System.Windows.Forms.TextBox();
            this._others4 = new System.Windows.Forms.TextBox();
            this._others5 = new System.Windows.Forms.TextBox();
            this._others6 = new System.Windows.Forms.TextBox();
            this.line2 = new Line();
            this.line1 = new Line();
            this.SuspendLayout();
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(357, 85);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(13, 13);
            this.label5.TabIndex = 24;
            this.label5.Text = "$";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(357, 63);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(13, 13);
            this.label4.TabIndex = 23;
            this.label4.Text = "$";
            // 
            // _S_titleInsurance
            // 
            this._S_titleInsurance.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._S_titleInsurance.Location = new System.Drawing.Point(376, 82);
            this._S_titleInsurance.Name = "_S_titleInsurance";
            this._S_titleInsurance.Size = new System.Drawing.Size(84, 20);
            this._S_titleInsurance.TabIndex = 4;
            this._S_titleInsurance.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _S_escrowFee
            // 
            this._S_escrowFee.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._S_escrowFee.Location = new System.Drawing.Point(376, 60);
            this._S_escrowFee.Name = "_S_escrowFee";
            this._S_escrowFee.Size = new System.Drawing.Size(84, 20);
            this._S_escrowFee.TabIndex = 2;
            this._S_escrowFee.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(112, 85);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(132, 13);
            this.label2.TabIndex = 20;
            this.label2.Text = "Title Insurance (line 1102):";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(126, 63);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(118, 13);
            this.label1.TabIndex = 19;
            this.label1.Text = "Escrow Fee (line 1101):";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(357, 129);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(13, 13);
            this.label3.TabIndex = 30;
            this.label3.Text = "$";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(357, 107);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(13, 13);
            this.label6.TabIndex = 29;
            this.label6.Text = "$";
            // 
            // _S_creditReport
            // 
            this._S_creditReport.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._S_creditReport.Location = new System.Drawing.Point(376, 126);
            this._S_creditReport.Name = "_S_creditReport";
            this._S_creditReport.Size = new System.Drawing.Size(84, 20);
            this._S_creditReport.TabIndex = 8;
            this._S_creditReport.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _S_appraisalFee
            // 
            this._S_appraisalFee.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._S_appraisalFee.Location = new System.Drawing.Point(376, 104);
            this._S_appraisalFee.Name = "_S_appraisalFee";
            this._S_appraisalFee.Size = new System.Drawing.Size(84, 20);
            this._S_appraisalFee.TabIndex = 6;
            this._S_appraisalFee.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(126, 129);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(118, 13);
            this.label7.TabIndex = 26;
            this.label7.Text = "Credit Report (line 804):";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(124, 107);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(120, 13);
            this.label8.TabIndex = 25;
            this.label8.Text = "Appraisal Fee (line 803):";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(357, 173);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(13, 13);
            this.label9.TabIndex = 36;
            this.label9.Text = "$";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(357, 151);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(13, 13);
            this.label10.TabIndex = 35;
            this.label10.Text = "$";
            // 
            // _S_hoaTransferFee
            // 
            this._S_hoaTransferFee.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._S_hoaTransferFee.Location = new System.Drawing.Point(376, 170);
            this._S_hoaTransferFee.Name = "_S_hoaTransferFee";
            this._S_hoaTransferFee.Size = new System.Drawing.Size(84, 20);
            this._S_hoaTransferFee.TabIndex = 12;
            this._S_hoaTransferFee.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _S_recordingFee
            // 
            this._S_recordingFee.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._S_recordingFee.Location = new System.Drawing.Point(376, 148);
            this._S_recordingFee.Name = "_S_recordingFee";
            this._S_recordingFee.Size = new System.Drawing.Size(84, 20);
            this._S_recordingFee.TabIndex = 10;
            this._S_recordingFee.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(93, 173);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(151, 13);
            this.label11.TabIndex = 32;
            this.label11.Text = "HOA Transferr Fee (line 1303):";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(112, 151);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(132, 13);
            this.label12.TabIndex = 31;
            this.label12.Text = "Recording Fee (line 1201):";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(357, 217);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(13, 13);
            this.label13.TabIndex = 42;
            this.label13.Text = "$";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(357, 195);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(13, 13);
            this.label14.TabIndex = 41;
            this.label14.Text = "$";
            // 
            // _S_others2Value
            // 
            this._S_others2Value.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._S_others2Value.Location = new System.Drawing.Point(376, 214);
            this._S_others2Value.Name = "_S_others2Value";
            this._S_others2Value.Size = new System.Drawing.Size(84, 20);
            this._S_others2Value.TabIndex = 18;
            this._S_others2Value.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _S_others1Value
            // 
            this._S_others1Value.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._S_others1Value.Location = new System.Drawing.Point(376, 192);
            this._S_others1Value.Name = "_S_others1Value";
            this._S_others1Value.Size = new System.Drawing.Size(84, 20);
            this._S_others1Value.TabIndex = 15;
            this._S_others1Value.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(357, 261);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(13, 13);
            this.label17.TabIndex = 48;
            this.label17.Text = "$";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(357, 240);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(13, 13);
            this.label18.TabIndex = 47;
            this.label18.Text = "$";
            // 
            // _S_others4Value
            // 
            this._S_others4Value.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._S_others4Value.Location = new System.Drawing.Point(376, 258);
            this._S_others4Value.Name = "_S_others4Value";
            this._S_others4Value.Size = new System.Drawing.Size(84, 20);
            this._S_others4Value.TabIndex = 24;
            this._S_others4Value.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _S_others3Value
            // 
            this._S_others3Value.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._S_others3Value.Location = new System.Drawing.Point(376, 236);
            this._S_others3Value.Name = "_S_others3Value";
            this._S_others3Value.Size = new System.Drawing.Size(84, 20);
            this._S_others3Value.TabIndex = 21;
            this._S_others3Value.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(357, 305);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(13, 13);
            this.label21.TabIndex = 54;
            this.label21.Text = "$";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(357, 283);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(13, 13);
            this.label22.TabIndex = 53;
            this.label22.Text = "$";
            // 
            // _S_others6Value
            // 
            this._S_others6Value.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._S_others6Value.Location = new System.Drawing.Point(376, 302);
            this._S_others6Value.Name = "_S_others6Value";
            this._S_others6Value.Size = new System.Drawing.Size(84, 20);
            this._S_others6Value.TabIndex = 30;
            this._S_others6Value.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _S_others5Value
            // 
            this._S_others5Value.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._S_others5Value.Location = new System.Drawing.Point(376, 280);
            this._S_others5Value.Name = "_S_others5Value";
            this._S_others5Value.Size = new System.Drawing.Size(84, 20);
            this._S_others5Value.TabIndex = 27;
            this._S_others5Value.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(357, 334);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(13, 13);
            this.label25.TabIndex = 58;
            this.label25.Text = "$";
            // 
            // button1
            // 
            this.button1.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.button1.Location = new System.Drawing.Point(381, 392);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 35;
            this.button1.Text = "OK";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // _S_total
            // 
            this._S_total.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._S_total.Location = new System.Drawing.Point(376, 331);
            this._S_total.Name = "_S_total";
            this._S_total.ReadOnly = true;
            this._S_total.Size = new System.Drawing.Size(84, 20);
            this._S_total.TabIndex = 32;
            this._S_total.TabStop = false;
            this._S_total.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(29, 333);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(154, 13);
            this.label26.TabIndex = 55;
            this.label26.Text = "TOTAL CLOSING COSTS:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(248, 333);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(13, 13);
            this.label15.TabIndex = 84;
            this.label15.Text = "$";
            // 
            // _B_total
            // 
            this._B_total.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._B_total.Location = new System.Drawing.Point(263, 331);
            this._B_total.Name = "_B_total";
            this._B_total.ReadOnly = true;
            this._B_total.Size = new System.Drawing.Size(84, 20);
            this._B_total.TabIndex = 31;
            this._B_total.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(248, 304);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(13, 13);
            this.label16.TabIndex = 82;
            this.label16.Text = "$";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(248, 282);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(13, 13);
            this.label19.TabIndex = 81;
            this.label19.Text = "$";
            // 
            // _B_others6Value
            // 
            this._B_others6Value.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._B_others6Value.Location = new System.Drawing.Point(263, 302);
            this._B_others6Value.Name = "_B_others6Value";
            this._B_others6Value.Size = new System.Drawing.Size(84, 20);
            this._B_others6Value.TabIndex = 29;
            this._B_others6Value.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _B_others5Value
            // 
            this._B_others5Value.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._B_others5Value.Location = new System.Drawing.Point(263, 280);
            this._B_others5Value.Name = "_B_others5Value";
            this._B_others5Value.Size = new System.Drawing.Size(84, 20);
            this._B_others5Value.TabIndex = 26;
            this._B_others5Value.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(248, 260);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(13, 13);
            this.label20.TabIndex = 78;
            this.label20.Text = "$";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(248, 238);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(13, 13);
            this.label23.TabIndex = 77;
            this.label23.Text = "$";
            // 
            // _B_others4Value
            // 
            this._B_others4Value.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._B_others4Value.Location = new System.Drawing.Point(263, 258);
            this._B_others4Value.Name = "_B_others4Value";
            this._B_others4Value.Size = new System.Drawing.Size(84, 20);
            this._B_others4Value.TabIndex = 23;
            this._B_others4Value.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _B_others3Value
            // 
            this._B_others3Value.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._B_others3Value.Location = new System.Drawing.Point(263, 236);
            this._B_others3Value.Name = "_B_others3Value";
            this._B_others3Value.Size = new System.Drawing.Size(84, 20);
            this._B_others3Value.TabIndex = 20;
            this._B_others3Value.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(248, 216);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(13, 13);
            this.label24.TabIndex = 74;
            this.label24.Text = "$";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(248, 194);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(13, 13);
            this.label27.TabIndex = 73;
            this.label27.Text = "$";
            // 
            // _B_others2Value
            // 
            this._B_others2Value.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._B_others2Value.Location = new System.Drawing.Point(263, 214);
            this._B_others2Value.Name = "_B_others2Value";
            this._B_others2Value.Size = new System.Drawing.Size(84, 20);
            this._B_others2Value.TabIndex = 17;
            this._B_others2Value.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _B_others1Value
            // 
            this._B_others1Value.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._B_others1Value.Location = new System.Drawing.Point(263, 192);
            this._B_others1Value.Name = "_B_others1Value";
            this._B_others1Value.Size = new System.Drawing.Size(84, 20);
            this._B_others1Value.TabIndex = 14;
            this._B_others1Value.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(248, 172);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(13, 13);
            this.label28.TabIndex = 70;
            this.label28.Text = "$";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(248, 150);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(13, 13);
            this.label29.TabIndex = 69;
            this.label29.Text = "$";
            // 
            // _B_hoaTransferFee
            // 
            this._B_hoaTransferFee.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._B_hoaTransferFee.Location = new System.Drawing.Point(263, 170);
            this._B_hoaTransferFee.Name = "_B_hoaTransferFee";
            this._B_hoaTransferFee.Size = new System.Drawing.Size(84, 20);
            this._B_hoaTransferFee.TabIndex = 11;
            this._B_hoaTransferFee.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _B_recordingFee
            // 
            this._B_recordingFee.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._B_recordingFee.Location = new System.Drawing.Point(263, 148);
            this._B_recordingFee.Name = "_B_recordingFee";
            this._B_recordingFee.Size = new System.Drawing.Size(84, 20);
            this._B_recordingFee.TabIndex = 9;
            this._B_recordingFee.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(248, 128);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(13, 13);
            this.label30.TabIndex = 66;
            this.label30.Text = "$";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(248, 106);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(13, 13);
            this.label31.TabIndex = 65;
            this.label31.Text = "$";
            // 
            // _B_creditReport
            // 
            this._B_creditReport.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._B_creditReport.Location = new System.Drawing.Point(263, 126);
            this._B_creditReport.Name = "_B_creditReport";
            this._B_creditReport.Size = new System.Drawing.Size(84, 20);
            this._B_creditReport.TabIndex = 7;
            this._B_creditReport.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _B_appraisalFee
            // 
            this._B_appraisalFee.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._B_appraisalFee.Location = new System.Drawing.Point(263, 104);
            this._B_appraisalFee.Name = "_B_appraisalFee";
            this._B_appraisalFee.Size = new System.Drawing.Size(84, 20);
            this._B_appraisalFee.TabIndex = 5;
            this._B_appraisalFee.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(248, 84);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(13, 13);
            this.label32.TabIndex = 62;
            this.label32.Text = "$";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(248, 62);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(13, 13);
            this.label33.TabIndex = 61;
            this.label33.Text = "$";
            // 
            // _B_titleInsurance
            // 
            this._B_titleInsurance.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._B_titleInsurance.Location = new System.Drawing.Point(263, 82);
            this._B_titleInsurance.Name = "_B_titleInsurance";
            this._B_titleInsurance.Size = new System.Drawing.Size(84, 20);
            this._B_titleInsurance.TabIndex = 3;
            this._B_titleInsurance.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _B_escrowFee
            // 
            this._B_escrowFee.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._B_escrowFee.Location = new System.Drawing.Point(263, 60);
            this._B_escrowFee.Name = "_B_escrowFee";
            this._B_escrowFee.Size = new System.Drawing.Size(84, 20);
            this._B_escrowFee.TabIndex = 1;
            this._B_escrowFee.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(369, 34);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(67, 13);
            this.label34.TabIndex = 85;
            this.label34.Text = "Seller\'s Cost:";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(260, 34);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(68, 13);
            this.label35.TabIndex = 86;
            this.label35.Text = "Buyer\'s Cost:";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(185, 195);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(59, 13);
            this.label36.TabIndex = 89;
            this.label36.Text = "(line 1304):";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(185, 217);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(59, 13);
            this.label37.TabIndex = 90;
            this.label37.Text = "(line 1305):";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(185, 239);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(59, 13);
            this.label38.TabIndex = 91;
            this.label38.Text = "(line 1306):";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(185, 261);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(59, 13);
            this.label39.TabIndex = 92;
            this.label39.Text = "(line 1307):";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(185, 283);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(59, 13);
            this.label40.TabIndex = 93;
            this.label40.Text = "(line 1308):";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(185, 305);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(59, 13);
            this.label41.TabIndex = 94;
            this.label41.Text = "(line 1309):";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(12, 9);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(238, 13);
            this.label42.TabIndex = 95;
            this.label42.Text = "Must edit, initial values are here as place holders.";
            // 
            // _loadDefaultValues
            // 
            this._loadDefaultValues.Location = new System.Drawing.Point(164, 363);
            this._loadDefaultValues.Name = "_loadDefaultValues";
            this._loadDefaultValues.Size = new System.Drawing.Size(143, 23);
            this._loadDefaultValues.TabIndex = 33;
            this._loadDefaultValues.Text = "Load Default Values";
            this._loadDefaultValues.UseVisualStyleBackColor = true;
            // 
            // _saveAsDefaultValues
            // 
            this._saveAsDefaultValues.Location = new System.Drawing.Point(313, 363);
            this._saveAsDefaultValues.Name = "_saveAsDefaultValues";
            this._saveAsDefaultValues.Size = new System.Drawing.Size(143, 23);
            this._saveAsDefaultValues.TabIndex = 34;
            this._saveAsDefaultValues.Text = "Save As Default Values";
            this._saveAsDefaultValues.UseVisualStyleBackColor = true;
            // 
            // _others1
            // 
            this._others1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._others1.Location = new System.Drawing.Point(19, 192);
            this._others1.Name = "_others1";
            this._others1.Size = new System.Drawing.Size(164, 20);
            this._others1.TabIndex = 13;
            this._others1.Text = "> Enter others <";
            this._others1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _others2
            // 
            this._others2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._others2.Location = new System.Drawing.Point(19, 214);
            this._others2.Name = "_others2";
            this._others2.Size = new System.Drawing.Size(164, 20);
            this._others2.TabIndex = 16;
            this._others2.Text = "> Enter others <";
            this._others2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _others3
            // 
            this._others3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._others3.Location = new System.Drawing.Point(19, 237);
            this._others3.Name = "_others3";
            this._others3.Size = new System.Drawing.Size(164, 20);
            this._others3.TabIndex = 19;
            this._others3.Text = "> Enter others <";
            this._others3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _others4
            // 
            this._others4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._others4.Location = new System.Drawing.Point(19, 258);
            this._others4.Name = "_others4";
            this._others4.Size = new System.Drawing.Size(164, 20);
            this._others4.TabIndex = 22;
            this._others4.Text = "> Enter others <";
            this._others4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _others5
            // 
            this._others5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._others5.Location = new System.Drawing.Point(19, 280);
            this._others5.Name = "_others5";
            this._others5.Size = new System.Drawing.Size(164, 20);
            this._others5.TabIndex = 25;
            this._others5.Text = "> Enter others <";
            this._others5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _others6
            // 
            this._others6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._others6.Location = new System.Drawing.Point(19, 302);
            this._others6.Name = "_others6";
            this._others6.Size = new System.Drawing.Size(164, 20);
            this._others6.TabIndex = 28;
            this._others6.Text = "> Enter others <";
            this._others6.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // line2
            // 
            this.line2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.line2.Location = new System.Drawing.Point(139, 321);
            this.line2.Name = "line2";
            this.line2.Size = new System.Drawing.Size(334, 10);
            this.line2.TabIndex = 88;
            // 
            // line1
            // 
            this.line1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.line1.Location = new System.Drawing.Point(139, 50);
            this.line1.Name = "line1";
            this.line1.Size = new System.Drawing.Size(334, 10);
            this.line1.TabIndex = 87;
            // 
            // EForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.ClientSize = new System.Drawing.Size(478, 427);
            this.Controls.Add(this._others6);
            this.Controls.Add(this._others5);
            this.Controls.Add(this._others4);
            this.Controls.Add(this._others3);
            this.Controls.Add(this._others2);
            this.Controls.Add(this._others1);
            this.Controls.Add(this._loadDefaultValues);
            this.Controls.Add(this._saveAsDefaultValues);
            this.Controls.Add(this.label42);
            this.Controls.Add(this.label41);
            this.Controls.Add(this.label40);
            this.Controls.Add(this.label39);
            this.Controls.Add(this.label38);
            this.Controls.Add(this.label37);
            this.Controls.Add(this.label36);
            this.Controls.Add(this._B_total);
            this.Controls.Add(this._B_others6Value);
            this.Controls.Add(this._B_others5Value);
            this.Controls.Add(this._B_others4Value);
            this.Controls.Add(this._B_others3Value);
            this.Controls.Add(this._B_others2Value);
            this.Controls.Add(this._B_others1Value);
            this.Controls.Add(this._B_hoaTransferFee);
            this.Controls.Add(this._B_recordingFee);
            this.Controls.Add(this._B_creditReport);
            this.Controls.Add(this._B_appraisalFee);
            this.Controls.Add(this._B_titleInsurance);
            this.Controls.Add(this._B_escrowFee);
            this.Controls.Add(this._S_total);
            this.Controls.Add(this._S_others6Value);
            this.Controls.Add(this._S_others5Value);
            this.Controls.Add(this._S_others4Value);
            this.Controls.Add(this._S_others3Value);
            this.Controls.Add(this._S_others2Value);
            this.Controls.Add(this._S_others1Value);
            this.Controls.Add(this._S_hoaTransferFee);
            this.Controls.Add(this._S_recordingFee);
            this.Controls.Add(this._S_creditReport);
            this.Controls.Add(this._S_appraisalFee);
            this.Controls.Add(this._S_titleInsurance);
            this.Controls.Add(this._S_escrowFee);
            this.Controls.Add(this.line2);
            this.Controls.Add(this.line1);
            this.Controls.Add(this.label35);
            this.Controls.Add(this.label34);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.label27);
            this.Controls.Add(this.label28);
            this.Controls.Add(this.label29);
            this.Controls.Add(this.label30);
            this.Controls.Add(this.label31);
            this.Controls.Add(this.label32);
            this.Controls.Add(this.label33);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "EForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "CLOSING COST";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox _S_titleInsurance;
        private System.Windows.Forms.TextBox _S_escrowFee;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox _S_creditReport;
        private System.Windows.Forms.TextBox _S_appraisalFee;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox _S_hoaTransferFee;
        private System.Windows.Forms.TextBox _S_recordingFee;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox _S_others2Value;
        private System.Windows.Forms.TextBox _S_others1Value;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox _S_others4Value;
        private System.Windows.Forms.TextBox _S_others3Value;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox _S_others6Value;
        private System.Windows.Forms.TextBox _S_others5Value;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox _S_total;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox _B_total;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox _B_others6Value;
        private System.Windows.Forms.TextBox _B_others5Value;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox _B_others4Value;
        private System.Windows.Forms.TextBox _B_others3Value;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox _B_others2Value;
        private System.Windows.Forms.TextBox _B_others1Value;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox _B_hoaTransferFee;
        private System.Windows.Forms.TextBox _B_recordingFee;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox _B_creditReport;
        private System.Windows.Forms.TextBox _B_appraisalFee;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TextBox _B_titleInsurance;
        private System.Windows.Forms.TextBox _B_escrowFee;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private DealMaker.UserControls.Line line1;
        private DealMaker.UserControls.Line line2;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Button _loadDefaultValues;
        private System.Windows.Forms.Button _saveAsDefaultValues;
        private System.Windows.Forms.TextBox _others1;
        private System.Windows.Forms.TextBox _others2;
        private System.Windows.Forms.TextBox _others3;
        private System.Windows.Forms.TextBox _others4;
        private System.Windows.Forms.TextBox _others5;
        private System.Windows.Forms.TextBox _others6;
    }
}