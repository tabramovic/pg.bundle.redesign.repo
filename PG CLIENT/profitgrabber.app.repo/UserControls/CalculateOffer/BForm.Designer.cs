﻿namespace DealMaker.UserControls.CalculateOffer
{
    partial class BForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this._total = new System.Windows.Forms.TextBox();
            this._loanDiscountFeeValue = new System.Windows.Forms.TextBox();
            this._loanOriginationFeeValue = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this._loadDefaultValues = new System.Windows.Forms.Button();
            this._saveAsDefaultValues = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(301, 62);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(15, 13);
            this.label6.TabIndex = 19;
            this.label6.Text = "%";
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(301, 33);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(15, 13);
            this.label5.TabIndex = 18;
            this.label5.Text = "%";
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(301, 9);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(15, 13);
            this.label4.TabIndex = 17;
            this.label4.Text = "%";
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.button1.Location = new System.Drawing.Point(223, 113);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 16;
            this.button1.Text = "OK";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // _total
            // 
            this._total.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._total.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._total.Location = new System.Drawing.Point(255, 59);
            this._total.Name = "_total";
            this._total.ReadOnly = true;
            this._total.Size = new System.Drawing.Size(43, 20);
            this._total.TabIndex = 15;
            // 
            // _loanDiscountFeeValue
            // 
            this._loanDiscountFeeValue.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._loanDiscountFeeValue.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._loanDiscountFeeValue.Location = new System.Drawing.Point(255, 30);
            this._loanDiscountFeeValue.Name = "_loanDiscountFeeValue";
            this._loanDiscountFeeValue.Size = new System.Drawing.Size(43, 20);
            this._loanDiscountFeeValue.TabIndex = 14;
            // 
            // _loanOriginationFeeValue
            // 
            this._loanOriginationFeeValue.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._loanOriginationFeeValue.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._loanOriginationFeeValue.Location = new System.Drawing.Point(255, 9);
            this._loanOriginationFeeValue.Name = "_loanOriginationFeeValue";
            this._loanOriginationFeeValue.Size = new System.Drawing.Size(43, 20);
            this._loanOriginationFeeValue.TabIndex = 13;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(11, 62);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(238, 13);
            this.label3.TabIndex = 12;
            this.label3.Text = "TOTAL SELLER\'S CONCESSIONS PAID:";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(149, 33);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(100, 13);
            this.label2.TabIndex = 11;
            this.label2.Text = "Loan Discount Fee:";
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(141, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(108, 13);
            this.label1.TabIndex = 10;
            this.label1.Text = "Loan Origination Fee:";
            // 
            // _loadDefaultValues
            // 
            this._loadDefaultValues.Location = new System.Drawing.Point(6, 85);
            this._loadDefaultValues.Name = "_loadDefaultValues";
            this._loadDefaultValues.Size = new System.Drawing.Size(143, 23);
            this._loadDefaultValues.TabIndex = 21;
            this._loadDefaultValues.Text = "Load Default Values";
            this._loadDefaultValues.UseVisualStyleBackColor = true;
            // 
            // _saveAsDefaultValues
            // 
            this._saveAsDefaultValues.Location = new System.Drawing.Point(155, 85);
            this._saveAsDefaultValues.Name = "_saveAsDefaultValues";
            this._saveAsDefaultValues.Size = new System.Drawing.Size(143, 23);
            this._saveAsDefaultValues.TabIndex = 20;
            this._saveAsDefaultValues.Text = "Save As Default Values";
            this._saveAsDefaultValues.UseVisualStyleBackColor = true;
            // 
            // BForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.ClientSize = new System.Drawing.Size(319, 148);
            this.Controls.Add(this._loadDefaultValues);
            this.Controls.Add(this._saveAsDefaultValues);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.button1);
            this.Controls.Add(this._total);
            this.Controls.Add(this._loanDiscountFeeValue);
            this.Controls.Add(this._loanOriginationFeeValue);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "BForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "SELLER\'S CONCESSION";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox _total;
        private System.Windows.Forms.TextBox _loanDiscountFeeValue;
        private System.Windows.Forms.TextBox _loanOriginationFeeValue;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button _loadDefaultValues;
        private System.Windows.Forms.Button _saveAsDefaultValues;
    }
}