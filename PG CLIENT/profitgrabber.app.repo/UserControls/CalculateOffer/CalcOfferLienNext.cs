﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

//Added
using HUD_MODULE;
using DealMaker.ShortSale.Utils;
using DealMaker.OnlineData;
using DealMaker.UserControls.Repairs;
using DealMaker.Repairs;
using DealMaker.PlainClasses.CalculateOffer;
using DealMaker.PlainClasses.OnLineData.COMPS;
using DealMaker.PlainClasses.ShortSale.Adapters;
using DealMaker.UserControls.ShortSale;


namespace DealMaker.UserControls.CalculateOffer
{
    public partial class CalcOfferLienNext : BaseShortSaleUserControl
    {
        const string Formatter = "#,0.00";
        int _lienIdx = 0;

        public CalcOfferLienNext(int lienIdx)
        {
            InitializeComponent();
            AddEventHandlers();
            _lienIdx = lienIdx;
            SetGUI();
            SetData();
        }

        void AddEventHandlers()
        {
            _IO_REC.Click += new EventHandler(On_IO_REC_Click);
            _IO_SC.Click += new EventHandler(On_IO_SC_Click);
            _IO_CC_L.Click += new EventHandler(On_IO_CC_L_Click);
            _IO_L.Click += new EventHandler(On_IO_L_Click);
            
            foreach (Control ctrl in _gb.Controls)
            {
                if (ctrl is TextBox)
                {
                    ((TextBox)ctrl).Leave += new EventHandler(On_TextBox_Leave);
                    ((TextBox)ctrl).KeyDown += new KeyEventHandler(On_TextBox_KeyDown);
                }
            }

            _getFromComps.Click += new EventHandler(On_GetFromComps_Click);
            _getFromRepairs.Click += new EventHandler(On_GetFromRepairs_Click);
        }

        void SetGUI()
        {
            if (1 == _lienIdx)
            {
                _initialOfferTo3rdNameLabel.Visible = false;
                _initialOfferTo3rd.Visible = false;
                _initialOfferTo3rdLabel.Visible = false;

                _initialOfferTo2ndLabel.Text = "*start with 5% of the 2nd balance";
                _gb.Text = "Your INITIAL OFFER to 2nd";
                //_controlLabel.Text = "Your INITIAL OFFER to 2nd:";
            }
            else if (2 == _lienIdx)
            {
                _initialOfferTo3rdNameLabel.Visible = true;
                _initialOfferTo3rd.Visible = true;
                _initialOfferTo3rdLabel.Visible = true;

                _initialOfferTo3rdLabel.Text = "*start with 5% of the 3rd balance";
                _initialOfferTo2ndLabel.Text = "*start with 10% of the 2nd balance";
                _gb.Text = "Your INITIAL OFFER to 3rd";
                //_controlLabel.Text = "Your INITIAL OFFER to 3rd:";
            }
        }

        void On_GetFromRepairs_Click(object sender, EventArgs e)
        {
            if (null != RepairsManager.Instance.P_RepairObject)
            {
                _IO_Repairs.Text = RepairsManager.Instance.P_RepairObject.Total.ToString(Formatter);
                CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].ConservativeRepairsValue = RepairsManager.Instance.P_RepairObject.Total;
                UpdateAsIs();
            }
        }

        void On_GetFromComps_Click(object sender, EventArgs e)
        {
            _IO_C_PropertyValue.Text = CompsManager.Instance.P_comps.P_RealisticValue.ToString(Formatter);
            CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].ConservativePropertyValue = CompsManager.Instance.P_comps.P_RealisticValue;
            UpdateAsIs();            
        }

        void SaveData()
        {
            CalculateOfferManager.Instance.SaveData();
            base.AutoSaveDocument();
        }

        void MapDataToObject()
        {
            if (null == CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData)
                return;

            CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].ConservativePropertyValue = Util.GetDecimalValue(_IO_C_PropertyValue.Text);
            CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].ConservativeRepairsValue = Util.GetDecimalValue(_IO_Repairs.Text);
            CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].AsIsValue = Util.GetDecimalValue(_IO_AsIs.Text);
            CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].FirstOfferMultiplierValue = Util.GetDecimalValue(_IO_FirstOfferMultiplier.Text);
            

            CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialOfferSalesPriceValue = Util.GetDecimalValue(_IO_SalesPrice.Text);
            CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialRealEstateComissionPerc = Util.GetDecimalValue(_IO_REC_Perc.Text);
            CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialRealEstateComissionValue = Util.GetDecimalValue(_IO_REC_Value.Text);
            CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialSellerConcessionPerc = Util.GetDecimalValue(_IO_SC_Perc.Text);
            CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialSellerConcessionValue = Util.GetDecimalValue(_IO_SC_Value.Text);
            CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialSellerClosingCostPerc = Util.GetDecimalValue(_IO_CC_Perc.Text);


            CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialSellerClosingCostValue = Util.GetDecimalValue(_IO_CC_Value.Text);
            CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialOfferToFirstLender = Util.GetDecimalValue(_initialOfferTo1st.Text);
            CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialOfferToSecondLender = Util.GetDecimalValue(_initialOfferTo2nd.Text);
            CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialOfferToThirdLender = Util.GetDecimalValue(_initialOfferTo3rd.Text);            

            SaveData();
        }

        void SetData()
        {
            try
            {
                _IO_C_PropertyValue.Text = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].ConservativePropertyValue.ToString(Formatter);
                _IO_Repairs.Text = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].ConservativeRepairsValue.ToString(Formatter);
                _IO_AsIs.Text = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].AsIsValue.ToString(Formatter);
                _IO_FirstOfferMultiplier.Text = (0 == CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].FirstOfferMultiplierValue) ? "0.75" : CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].FirstOfferMultiplierValue.ToString(Formatter);

                _IO_SalesPrice.Text = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialOfferSalesPriceValue.ToString(Formatter);
                _IO_REC_Perc.Text = (0 == CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialRealEstateComissionPerc ? "6" : CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialRealEstateComissionPerc.ToString(Formatter));
                _IO_REC_Value.Text = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialRealEstateComissionValue.ToString(Formatter);
                _IO_SC_Perc.Text = (0 == CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialSellerConcessionPerc ? "3" : CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialSellerConcessionPerc.ToString(Formatter));
                _IO_SC_Value.Text = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialSellerConcessionValue.ToString(Formatter);
                _IO_CC_Perc.Text = (0 == CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialSellerClosingCostPerc) ? "1" : CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialSellerClosingCostPerc.ToString(Formatter);
                _IO_CC_Value.Text = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialSellerClosingCostValue.ToString(Formatter);

                _IO_CC_Value.Text = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialSellerClosingCostValue.ToString(Formatter);
                _initialOfferTo1st.Text = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialOfferToFirstLender.ToString(Formatter);
                _initialOfferTo2nd.Text = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialOfferToSecondLender.ToString(Formatter);
                _initialOfferTo3rd.Text = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialOfferToThirdLender.ToString(Formatter);
            }
            catch { }
        }       

        //Initial Offer NET to 1st Lender
        void On_IO_L_Click(object sender, EventArgs e)
        {
            // >> TO HUD
            ExportToExternal();
        }

        void InitInitialSellerClosingCostBreakDowns()
        {
            CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialSellerClosingCostBreakDown.EscrowFeeValue = (0.25m) * CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialSellerClosingCostValue;
            CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialSellerClosingCostBreakDown.TitleInsuranceValue = (0.5m) * CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialSellerClosingCostValue;
            CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialSellerClosingCostBreakDown.AppraisalFeeValue = (0.2m) * CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialSellerClosingCostValue;
            CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialSellerClosingCostBreakDown.RecordingFeeValue = (0.05m) * CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialSellerClosingCostValue;
        }

        //Initial Offer Closing Cost - Left Button
        void On_IO_CC_L_Click(object sender, EventArgs e)
        {            
            EForm bsForm = new EForm("CLOSING COST (entries on the Seller's side of HUD1)");

            //if there is NO data, set dummy
            if (null == CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialSellerClosingCostBreakDown ||
                null == CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialBuyerClosingCostBreakDown
                )
            {
                if (null == CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialSellerClosingCostBreakDown)
                    CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialSellerClosingCostBreakDown = new ClosingCostBreakDownData();

                if (null == CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialBuyerClosingCostBreakDown)
                    CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialBuyerClosingCostBreakDown = new ClosingCostBreakDownData();

                InitInitialSellerClosingCostBreakDowns();

                bsForm.S_BreakDownData = CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialSellerClosingCostBreakDown;                                
            }
            else if (Util.GetDecimalValue(_IO_CC_Value.Text) != (CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialSellerClosingCostBreakDown.GetTotal() + CalculateOfferManager.Instance.P_CalculateOfferData.FirstLienData.InitialBuyerClosingCostBreakDown.GetTotal()))
            {
                //set real just as below, since we can't recreate perc. breakdown
                bsForm.S_BreakDownData = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialSellerClosingCostBreakDown;
                bsForm.B_BreakDownData = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialBuyerClosingCostBreakDown;
            }
            else
            {
                bsForm.S_BreakDownData = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialSellerClosingCostBreakDown;
                bsForm.B_BreakDownData = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialBuyerClosingCostBreakDown;
            }

            if (DialogResult.OK == bsForm.ShowDialog())
            {
                CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialSellerClosingCostBreakDown = bsForm.S_BreakDownData;
                CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialBuyerClosingCostBreakDown = bsForm.B_BreakDownData;

                CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialSellerClosingCostValue = /*Util.GetDecimalValue(bsForm.B_Total) +*/ Util.GetDecimalValue(bsForm.S_Total);
                _IO_CC_Value.Text = (/*Util.GetDecimalValue(bsForm.B_Total) +*/ Util.GetDecimalValue(bsForm.S_Total)).ToString(Formatter);

                try
                {
                    CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialSellerClosingCostPerc =
                                ((/*Util.GetDecimalValue(bsForm.B_Total) +*/ Util.GetDecimalValue(bsForm.S_Total)) /
                                CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialOfferSalesPriceValue) * 100;
                }
                catch
                {
                    CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialSellerClosingCostPerc = 0;
                }

                _IO_CC_Perc.Text = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialSellerClosingCostPerc.ToString(Formatter);

                UpdateTotal();
                MapDataToObject();
                //SaveData();
            }
        }

        //Initial Offer Seller's Concession
        void On_IO_SC_Click(object sender, EventArgs e)
        {
            BForm sellerConcessionForm = new BForm(Util.GetDecimalValue(_IO_SC_Perc.Text));

            //if there is NO data, set dummy
            if (null == CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialSellerConcessionBreakDown)
            {
                sellerConcessionForm.LoanOriginatorFee = (decimal)(1) / 3 * Util.GetDecimalValue(_IO_SC_Perc.Text);
                sellerConcessionForm.LoanDiscountFee = (decimal)(2) / 3 * Util.GetDecimalValue(_IO_SC_Perc.Text);
                sellerConcessionForm.Total = Util.GetDecimalValue(_IO_SC_Perc.Text);
            }
            else if (null != CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialSellerConcessionBreakDown && Util.GetDecimalValue(_IO_SC_Perc.Text) != CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialSellerConcessionBreakDown.Total)
            {
                //set adjusted data, same as above, since we don't store label names                
                sellerConcessionForm.LoanOriginatorFee = (decimal)(1) / 3 * Util.GetDecimalValue(_IO_SC_Perc.Text);
                sellerConcessionForm.LoanDiscountFee = (decimal)(2) / 3 * Util.GetDecimalValue(_IO_SC_Perc.Text);
                sellerConcessionForm.Total = Util.GetDecimalValue(_IO_SC_Perc.Text);
            }
            else
                sellerConcessionForm.BreakDownData = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialSellerConcessionBreakDown;

            if (DialogResult.OK == sellerConcessionForm.ShowDialog())
            {
                CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialSellerConcessionBreakDown = sellerConcessionForm.BreakDownData;

                _IO_SC_Perc.Text = sellerConcessionForm.BreakDownData.Total.ToString(Formatter);
                _IO_SC_Value.Text = (sellerConcessionForm.BreakDownData.Total * Util.GetDecimalValue(_IO_SalesPrice.Text) / 100).ToString(Formatter);

                UpdateTotal();
                MapDataToObject();
                //SaveData();
            }
        }

        //Initial Offer Real Estate Comission
        void On_IO_REC_Click(object sender, EventArgs e)
        {
            AForm realEstateCommissionForm = new AForm(Util.GetDecimalValue(_IO_REC_Perc.Text));

            //if there is NO data --> set dummy
            if (null == CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialRealEstateCommisionBreakDown)                
            {
                realEstateCommissionForm.ListingAgencyName = "> Enter  name of Listing Agency <:";
                realEstateCommissionForm.SellingAgencyName = "> Enter  name of Selling Agency <:";
                realEstateCommissionForm.ListingAgencyValue = 0.5m * Util.GetDecimalValue(_IO_REC_Perc.Text);
                realEstateCommissionForm.SellingAgencyValue = 0.5m * Util.GetDecimalValue(_IO_REC_Perc.Text);
                realEstateCommissionForm.Total = Util.GetDecimalValue(_IO_REC_Perc.Text);

            }
            else if (null != CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialRealEstateCommisionBreakDown && 
                Util.GetDecimalValue(_IO_REC_Perc.Text) != CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialRealEstateCommisionBreakDown.Total)
            {
                //set adjusted data
                realEstateCommissionForm.ListingAgencyName = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialRealEstateCommisionBreakDown.NameOfListingAgency;
                realEstateCommissionForm.SellingAgencyName = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialRealEstateCommisionBreakDown.NameOfSellingAgency;
                realEstateCommissionForm.ListingAgencyValue = 0.5m * Util.GetDecimalValue(_IO_REC_Perc.Text);
                realEstateCommissionForm.SellingAgencyValue = 0.5m * Util.GetDecimalValue(_IO_REC_Perc.Text);
                realEstateCommissionForm.Total = Util.GetDecimalValue(_IO_REC_Perc.Text);
            }
            else //set real data
                realEstateCommissionForm.BreakDownData = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialRealEstateCommisionBreakDown;

            if (DialogResult.OK == realEstateCommissionForm.ShowDialog())
            {
                CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialRealEstateCommisionBreakDown = realEstateCommissionForm.BreakDownData;
                _IO_REC_Perc.Text = realEstateCommissionForm.BreakDownData.Total.ToString(Formatter);
                _IO_REC_Value.Text = (realEstateCommissionForm.BreakDownData.Total * Util.GetDecimalValue(_IO_SalesPrice.Text) / 100).ToString(Formatter);

                UpdateTotal();
                MapDataToObject();
                //SaveData();
            }
        }

        void On_TextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                HandleMapping(sender);
            }
        }

        void On_TextBox_Leave(object sender, EventArgs e)
        {
            HandleMapping(sender);
        }

        void HandleMapping(object sender)
        {
            MapDataToObject();

            if (sender == _IO_C_PropertyValue)
            {
                UpdateAsIs();
            }

            if (sender == _IO_Repairs)
            {
                UpdateAsIs();
            }

            if (sender == _IO_FirstOfferMultiplier)
            {
                UpdateInitialOfferSalesPrice();
            }
            

            if (sender == _IO_REC_Perc)
            {
                _IO_REC_Value.Text = (Util.GetDecimalValue(_IO_SalesPrice.Text) * Util.GetDecimalValue(_IO_REC_Perc.Text) / 100).ToString(Formatter);
                UpdateTotal();

                if (null == CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialRealEstateCommisionBreakDown)
                {
                    On_IO_REC_Click(null, null);
                }
                else if (null != CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialRealEstateCommisionBreakDown)
                {
                    if (Util.GetDecimalValue(_IO_REC_Perc.Text) != CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialRealEstateCommisionBreakDown.Total)
                        On_IO_REC_Click(null, null);
                }
            }

            if (sender == _IO_SC_Perc)
            {
                _IO_SC_Value.Text = (Util.GetDecimalValue(_IO_SalesPrice.Text) * Util.GetDecimalValue(_IO_SC_Perc.Text) / 100).ToString(Formatter);
                UpdateTotal();

                if (null == CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialSellerConcessionBreakDown)
                {
                    On_IO_SC_Click(null, null);
                }
                else if (null != CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialSellerConcessionBreakDown)
                {
                    if (Util.GetDecimalValue(_IO_SC_Perc.Text) != CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialSellerConcessionBreakDown.Total)
                        On_IO_SC_Click(null, null);
                }
            }

            if (sender == _IO_CC_Perc)
            {
                _IO_CC_Value.Text = (Util.GetDecimalValue(_IO_SalesPrice.Text) * Util.GetDecimalValue(_IO_CC_Perc.Text) / 100).ToString(Formatter);
                UpdateTotal();
            }

            if (sender == _initialOfferTo3rd)
            {
                _initialOfferTo3rd.Text = (Util.GetDecimalValue(_initialOfferTo3rd.Text)).ToString(Formatter);
                UpdateTotal();
            }

            if (sender == _initialOfferTo2nd)
            {
                _initialOfferTo2nd.Text = (Util.GetDecimalValue(_initialOfferTo2nd.Text)).ToString(Formatter);
                UpdateTotal();
            }

            ((TextBox)(sender)).Text = Util.GetDecimalValue(((TextBox)(sender)).Text).ToString(Formatter);            
        }

       
        void UpdateAsIs()
        {
            _IO_AsIs.Text = (Util.GetDecimalValue(_IO_C_PropertyValue.Text) - Util.GetDecimalValue(_IO_Repairs.Text)).ToString(Formatter);
            UpdateInitialOfferSalesPrice();
        }

        void UpdateInitialOfferSalesPrice()
        {
            _IO_SalesPrice.Text = (Util.GetDecimalValue(_IO_AsIs.Text) * Util.GetDecimalValue(_IO_FirstOfferMultiplier.Text)).ToString(Formatter);
            UpdateValuePairs();
        }

        void UpdateValuePairs()
        {
            _IO_REC_Value.Text = (Util.GetDecimalValue(_IO_SalesPrice.Text) * Util.GetDecimalValue(_IO_REC_Perc.Text) / 100).ToString(Formatter);
            _IO_SC_Value.Text = (Util.GetDecimalValue(_IO_SalesPrice.Text) * Util.GetDecimalValue(_IO_SC_Perc.Text) / 100).ToString(Formatter);
            _IO_CC_Value.Text = (Util.GetDecimalValue(_IO_SalesPrice.Text) * Util.GetDecimalValue(_IO_CC_Perc.Text) / 100).ToString(Formatter);

            UpdateTotal();
        }

        void UpdateTotal()
        {
            _initialOfferTo1st.Text = (Util.GetDecimalValue(_IO_SalesPrice.Text) -
                                      Util.GetDecimalValue(_IO_REC_Value.Text) -
                                      Util.GetDecimalValue(_IO_SC_Value.Text) -
                                      Util.GetDecimalValue(_IO_CC_Value.Text) -
                                      Util.GetDecimalValue(_initialOfferTo3rd.Text) -
                                      Util.GetDecimalValue(_initialOfferTo2nd.Text)
                                      ).ToString(Formatter);

            MapDataToObject();
            //SaveData();
        }

        #region EXPORT TO HUD
        void ExportToExternal()
        {
            string hudFileName = string.Empty;
            bool existingHUDFileName = HUDFileNameManager.Instance.GetCreateDocument(Application.StartupPath, DataManager.Instance.DocumentName, _lienIdx + 1, out hudFileName);         
            string path = Application.StartupPath + @"\" + StringStore.SavedExtDocumentsFolder;
            string suffix = StringStore.HUDSubFolder;
            //string docName = path + suffix;
            //string sSAddress = string.Empty;
            //sSAddress = DataManager.Instance.GetSiteAddress();
            //if (null != sSAddress && string.Empty != sSAddress)
            //    docName += DataManager.Instance.GetCurrentShortSaleDocument().DocumentId + "." + (_lienIdx + 1).ToString() + Util.GetSuffix((_lienIdx + 1)) + "Lien" + "." + sSAddress + ".hsd";
            //else
            //    docName += DataManager.Instance.GetCurrentShortSaleDocument().DocumentId + "." + (_lienIdx + 1).ToString() + Util.GetSuffix((_lienIdx + 1)) + "Lien" + ".hsd";

            HUD hud = new HUD();
            //hud.SetFileName(path, suffix, docName);

            hud.SetFileName(path, suffix, path + suffix + hudFileName);
            if (existingHUDFileName)
                hud.ForceLoad(path + suffix + hudFileName);

            hud.SetCash();

            try
            {
                string accountNr = string.Empty;
                try { accountNr = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[_lienIdx].AccountNr; }
                catch { accountNr = string.Empty; }
                hud.SetLoanNumber(accountNr);
            }
            catch { }

            try
            {
                decimal initialOfferSalesPrice = 0;
                decimal comission = 0;

                try { initialOfferSalesPrice = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialOfferSalesPriceValue; }
                catch { initialOfferSalesPrice = 0; }

                try { comission = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialRealEstateComissionPerc; }
                catch { comission = 0; }

                hud.SetSalesPriceDetails(initialOfferSalesPrice, comission);

            }
            catch { }

            try
            {
                string firstCompanyName = string.Empty;
                string secondCompanyName = string.Empty;
                decimal initialOfferNetToFirst = 0;
                decimal initialOfferNetToSecond = 0;

                try { firstCompanyName = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[_lienIdx - 1].CompanyName; }
                catch { firstCompanyName = string.Empty; }

                try { initialOfferNetToFirst = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialOfferToFirstLender; }
                catch { initialOfferNetToFirst = 0; }

                try { secondCompanyName = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[_lienIdx - 1].CompanyName; }
                catch { secondCompanyName = string.Empty; }

                try { initialOfferNetToSecond = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialOfferToSecondLender; }
                catch { initialOfferNetToSecond = 0; }

                hud.SetCompanyDetails(firstCompanyName, initialOfferNetToFirst, secondCompanyName, initialOfferNetToSecond);
            }
            catch { }

            try
            {
                hud.SetValues(DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.BuyerInfo.FullName,
                              DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.BuyerInfo.AddressInfo.SiteAddress.Address.SiteStreetAddress,
                              DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.OwnerInfo.FullName,
                              DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.OwnerInfo.AddressInfo.MailAddress.Address.SiteStreetAddress,
                              DataManager.Instance.GetFullSiteAddress());
            }
            catch { }

            //set initial offer sales price
            try
            {
                decimal initOfferSalesPrice = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialOfferSalesPriceValue;
                hud.SetIntialOfferSalesPrice(initOfferSalesPrice);
            }
            catch { }


            string nameOfListingAgency = string.Empty;            
            try { nameOfListingAgency = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialRealEstateCommisionBreakDown.NameOfListingAgency; }
            catch { nameOfListingAgency = string.Empty; }
                            
            string nameOfSellingAgency = "";
            try { nameOfSellingAgency = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialRealEstateCommisionBreakDown.NameOfSellingAgency; }
            catch { nameOfSellingAgency = string.Empty; }

            decimal listingAgencyValue = 0;
            try { listingAgencyValue = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialRealEstateCommisionBreakDown.ListingAgencyValue * CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialOfferSalesPriceValue / 100; }
            catch { listingAgencyValue = 0; }

            decimal sellingAgencyValue = 0;
            try { sellingAgencyValue = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialRealEstateCommisionBreakDown.SellingAgencyValue * CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialOfferSalesPriceValue / 100; }
            catch { sellingAgencyValue = 0; }

            hud.SetRealEstateCommissionValues(nameOfListingAgency, nameOfSellingAgency, listingAgencyValue, sellingAgencyValue);

            decimal loanOriginationFee = 0;
            try 
            {
                if (null == CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialSellerConcessionBreakDown)
                {
                    loanOriginationFee = 1 * CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialOfferSalesPriceValue / 100;
                }
                else
                {
                    loanOriginationFee = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialSellerConcessionBreakDown.LoanOriginationFeeValue * CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialOfferSalesPriceValue / 100;
                }
            }
            catch { loanOriginationFee = 0; }

            decimal loanDiscountFee = 0;
            try 
            {
                if (null == CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialSellerConcessionBreakDown)
                {
                    loanDiscountFee = 2 * CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialOfferSalesPriceValue / 100; 
                }
                else
                {
                    loanDiscountFee = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialSellerConcessionBreakDown.LoanDiscountFeeValue * CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialOfferSalesPriceValue / 100;
                }
            }
            catch { loanDiscountFee = 0; }

            hud.SetSellerConcessionValues(loanOriginationFee, loanDiscountFee);
            
            decimal EscrowFeeValue = 0;
            decimal TitleInsuranceValue = 0;
            decimal AppraisalFeeValue = 0;
            decimal CreditReportValue = 0;
            decimal RecordingFeeValue = 0;
            decimal HoaTransferFeeValue = 0;
            string Others1Name = string.Empty;
            decimal Others1Value = 0;
            string Others2Name = string.Empty;
            decimal Others2Value = 0;
            string Others3Name = string.Empty;
            decimal Others3Value = 0;
            string Others4Name = string.Empty;
            decimal Others4Value = 0;
            string Others5Name = string.Empty;
            decimal Others5Value = 0;
            string Others6Name = string.Empty;
            decimal Others6Value = 0;

            try { EscrowFeeValue = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialBuyerClosingCostBreakDown.EscrowFeeValue; }
            catch { EscrowFeeValue = 0; }

            try { TitleInsuranceValue = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialBuyerClosingCostBreakDown.TitleInsuranceValue; }
            catch { TitleInsuranceValue = 0; }

            try { AppraisalFeeValue = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialBuyerClosingCostBreakDown.AppraisalFeeValue; }
            catch { AppraisalFeeValue = 0; }

            try { CreditReportValue = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialBuyerClosingCostBreakDown.CreditReportValue; }
            catch { CreditReportValue = 0; }

            try { RecordingFeeValue = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialBuyerClosingCostBreakDown.RecordingFeeValue; }
            catch { RecordingFeeValue = 0; }

            try { HoaTransferFeeValue = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialBuyerClosingCostBreakDown.HoaTransferFeeValue; }
            catch { HoaTransferFeeValue = 0; }

            try { Others1Name = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialBuyerClosingCostBreakDown.Others1Name; }
            catch { Others1Name = string.Empty; }

            try { Others1Value = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialBuyerClosingCostBreakDown.Others1Value; }
            catch { Others1Value = 0; }

            try { Others2Name = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialBuyerClosingCostBreakDown.Others2Name; }
            catch { Others2Name = string.Empty; }

            try { Others2Value = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialBuyerClosingCostBreakDown.Others2Value; }
            catch { Others2Value = 0; }

            try { Others3Name = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialBuyerClosingCostBreakDown.Others3Name; }
            catch { Others3Name = string.Empty; }

            try { Others3Value = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialBuyerClosingCostBreakDown.Others3Value; }
            catch { Others3Value = 0; }

            try { Others4Name = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialBuyerClosingCostBreakDown.Others4Name; }
            catch {  Others4Name = string.Empty; }

            try { Others4Value = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialBuyerClosingCostBreakDown.Others4Value; }
            catch { Others4Value = 0; }

            try { Others5Name = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialBuyerClosingCostBreakDown.Others5Name; }
            catch {  Others5Name = string.Empty; }

            try { Others5Value = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialBuyerClosingCostBreakDown.Others5Value; }
            catch { Others5Value = 0; }

            try { Others6Name = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialBuyerClosingCostBreakDown.Others6Name; }
            catch {  Others6Name = string.Empty; }

            try { Others6Value = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialBuyerClosingCostBreakDown.Others6Value; }
            catch { Others6Value = 0; }
    

            hud.SetBuyerClosingCostValues(
                EscrowFeeValue,
                TitleInsuranceValue,
                AppraisalFeeValue,
                CreditReportValue,
                RecordingFeeValue,
                "HOA Transferr Fee",
                HoaTransferFeeValue,
                Others1Name,
                Others1Value,
                Others2Name,
                Others2Value,
                Others3Name,
                Others3Value,
                Others4Name,
                Others4Value,
                Others5Name,
                Others5Value,
                Others6Name,
                Others6Value);

            if (null == CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialSellerClosingCostBreakDown)
            {
                CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialSellerClosingCostBreakDown = new ClosingCostBreakDownData();
                InitInitialSellerClosingCostBreakDowns();
            }

            try { EscrowFeeValue = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialSellerClosingCostBreakDown.EscrowFeeValue; }
            catch { EscrowFeeValue = 0; }

            try { TitleInsuranceValue = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialSellerClosingCostBreakDown.TitleInsuranceValue; }
            catch { TitleInsuranceValue = 0; }

            try { AppraisalFeeValue = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialSellerClosingCostBreakDown.AppraisalFeeValue; }
            catch { AppraisalFeeValue = 0; }

            try { CreditReportValue = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialSellerClosingCostBreakDown.CreditReportValue; }
            catch { CreditReportValue = 0; }

            try { RecordingFeeValue = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialSellerClosingCostBreakDown.RecordingFeeValue; }
            catch { RecordingFeeValue = 0; }

            try { HoaTransferFeeValue = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialSellerClosingCostBreakDown.HoaTransferFeeValue; }
            catch { HoaTransferFeeValue = 0; }

            try { Others1Name = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialSellerClosingCostBreakDown.Others1Name; }
            catch { Others1Name = string.Empty; }

            try { Others1Value = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialSellerClosingCostBreakDown.Others1Value; }
            catch { Others1Value = 0; }

            try { Others2Name = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialSellerClosingCostBreakDown.Others2Name; }
            catch { Others2Name = string.Empty; }

            try { Others2Value = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialSellerClosingCostBreakDown.Others2Value; }
            catch { Others2Value = 0; }

            try { Others3Name = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialSellerClosingCostBreakDown.Others3Name; }
            catch { Others3Name = string.Empty; }

            try { Others3Value = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialSellerClosingCostBreakDown.Others3Value; }
            catch { Others3Value = 0; }

            try { Others4Name = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialSellerClosingCostBreakDown.Others4Name; }
            catch { Others4Name = string.Empty; }

            try { Others4Value = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialSellerClosingCostBreakDown.Others4Value; }
            catch { Others4Value = 0; }

            try { Others5Name = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialSellerClosingCostBreakDown.Others5Name; }
            catch { Others5Name = string.Empty; }

            try { Others5Value = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialSellerClosingCostBreakDown.Others5Value; }
            catch { Others5Value = 0; }

            try { Others6Name = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialSellerClosingCostBreakDown.Others6Name; }
            catch { Others6Name = string.Empty; }

            try { Others6Value = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialSellerClosingCostBreakDown.Others6Value; }
            catch { Others6Value = 0; }

            hud.SetSellerClosingCostValues(
                EscrowFeeValue,
                TitleInsuranceValue,
                AppraisalFeeValue,
                CreditReportValue,
                RecordingFeeValue,
                "HOA Transferr Fee",
                HoaTransferFeeValue,
                Others1Name,
                Others1Value,
                Others2Name,
                Others2Value,
                Others3Name,
                Others3Value,
                Others4Name,
                Others4Value,
                Others5Name,
                Others5Value,
                Others6Name,
                Others6Value);

            //for 3rd  lien – the payoff for 3rd should go into 506 and it should say “Payoff of 3rd Loan” and in the column next to it should be the $ amount.
            if (2 == _lienIdx)
            {
                decimal thirdPayOff = 0;

                try { thirdPayOff = CalculateOfferManager.Instance.P_CalculateOfferData.NextLienData[_lienIdx - 1].InitialOfferToThirdLender;}
                catch { thirdPayOff = 0; }

                hud.Set3rdPayOff(thirdPayOff);
            }

            hud.ShowDialog();
        }
        #endregion
        

        

    }
}
