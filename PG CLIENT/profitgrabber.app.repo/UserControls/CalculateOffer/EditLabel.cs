﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace DealMaker.UserControls.CalculateOffer
{
    public partial class EditLabel : Form
    {
        public EditLabel()
        {
            InitializeComponent();
            AddEventHandlers();
        }

        public EditLabel(string labelName)
        {
            InitializeComponent();
            _labelName.Text = labelName;
            AddEventHandlers();
        }

        void AddEventHandlers()
        {
            _labelName.KeyDown += new KeyEventHandler(On_labelName_KeyDown);
        }

        void On_labelName_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                DialogResult = DialogResult.OK;
                this.Close();
            }
        }

        public string LabelName
        {
            get { return _labelName.Text; }
            set { _labelName.Text = value; }
        }
    }
}
