﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

//Added
using DealMaker.ShortSale;
using DealMaker.ShortSale.Utils;
using DealMaker.PlainClasses.CalculateOffer;


namespace DealMaker.UserControls.CalculateOffer
{
    public partial class AForm : Form
    {
        const string Formatter = "#,0.00";
        decimal _totalValue = 0;

        public AForm()
        {
            InitializeComponent();
            AddEventHandlers();
        }

        public AForm(decimal total)
        {
            InitializeComponent();
            AddEventHandlers();

            _total.Text = total.ToString(Formatter);
            _totalValue = total;
        }

        public AForm(string listingAgencyName, string sellingAgencyName, decimal laValue, decimal saValue, decimal total)
        {
            _listingAgencyName.Text = listingAgencyName;
            _sellingAgencyName.Text = sellingAgencyName;
            _laValue.Text = laValue.ToString(Formatter);
            _saValue.Text = saValue.ToString(Formatter);
            _total.Text = total.ToString(Formatter);
            _totalValue = total;
            
            InitializeComponent();
            AddEventHandlers();
        }

        void AddEventHandlers()
        {
            _saveAsDefaultValues.Click += new EventHandler(On_SaveAsDefaultValues_Click);
            _loadDefaultValues.Click += new EventHandler(On_LoadDefaultValues_Click);

            _listingAgencyName.Click += new EventHandler(On_listingAgencyName_Click);
            _sellingAgencyName.Click += new EventHandler(On_sellingAgencyName_Click);

            _laValue.Leave += new EventHandler(On_laValue_Leave);
            _laValue.KeyDown += new KeyEventHandler(On_laValue_KeyDown);

            _saValue.Leave += new EventHandler(On_saValue_Leave);
            _saValue.KeyDown += new KeyEventHandler(On_saValue_KeyDown);
        }

        void On_LoadDefaultValues_Click(object sender, EventArgs e)
        {
            CalculateOfferManager.Instance.LoadCODefaultValues();

            if (null != CalculateOfferManager.Instance.P_CO_DefaultValues)
            {
                _listingAgencyName.Text = CalculateOfferManager.Instance.P_CO_DefaultValues.RealEstateCommission.ListingAgencyName;
                _sellingAgencyName.Text = CalculateOfferManager.Instance.P_CO_DefaultValues.RealEstateCommission.SellingAgencyName;

                _laValue.Text = CalculateOfferManager.Instance.P_CO_DefaultValues.RealEstateCommission.ListingAgencyPerc;
                _saValue.Text = CalculateOfferManager.Instance.P_CO_DefaultValues.RealEstateCommission.SellingAgencyPerc;
            }
            else
            {
                _laValue.Text = "3";
                _saValue.Text = "3";
            }

            Total = Util.GetDecimalValue(_laValue.Text) + Util.GetDecimalValue(_saValue.Text);
            
        }

        void On_SaveAsDefaultValues_Click(object sender, EventArgs e)
        {
            CalculateOfferManager.Instance.LoadCODefaultValues();

            if (null == CalculateOfferManager.Instance.P_CO_DefaultValues)
                CalculateOfferManager.Instance.P_CO_DefaultValues = new CO_DefaultValues();

            if (null == CalculateOfferManager.Instance.P_CO_DefaultValues.RealEstateCommission)
                CalculateOfferManager.Instance.P_CO_DefaultValues.RealEstateCommission = new CO_DefaultValuesRealEstateCommission();

            CalculateOfferManager.Instance.P_CO_DefaultValues.RealEstateCommission = new CO_DefaultValuesRealEstateCommission();
            CalculateOfferManager.Instance.P_CO_DefaultValues.RealEstateCommission.ListingAgencyName = _listingAgencyName.Text;
            CalculateOfferManager.Instance.P_CO_DefaultValues.RealEstateCommission.ListingAgencyPerc = _laValue.Text;
            CalculateOfferManager.Instance.P_CO_DefaultValues.RealEstateCommission.SellingAgencyName = _sellingAgencyName.Text;
            CalculateOfferManager.Instance.P_CO_DefaultValues.RealEstateCommission.SellingAgencyPerc = _saValue.Text;
            
            CalculateOfferManager.Instance.SaveCODefaultValues();
        }

        #region PROPERTIES
        public decimal Total
        {
            get { return _totalValue; }
            set 
            { 
                _totalValue = value;
                _total.Text = value.ToString(Formatter);
            }
        }

        public string ListingAgencyName
        {
            get { return _listingAgencyName.Text; }            
            set { _listingAgencyName.Text = value; }            
        }

        public string SellingAgencyName
        {
            get { return _sellingAgencyName.Text; }            
            set { _sellingAgencyName.Text = value; }            
        }

        public decimal ListingAgencyValue
        {
            get { return Util.GetDecimalValue(_laValue.Text) ; }            
            set { _laValue.Text = value.ToString(Formatter) ; }            
        }

        public decimal SellingAgencyValue
        {
            get { return Util.GetDecimalValue(_saValue.Text); }
            set { _saValue.Text = value.ToString(Formatter); }            
        }

        public RealEstateCommisionBreakDownData BreakDownData
        {
            get 
            {
                RealEstateCommisionBreakDownData data = new RealEstateCommisionBreakDownData();
                data.NameOfListingAgency = ListingAgencyName;
                data.ListingAgencyValue = ListingAgencyValue;
                data.NameOfSellingAgency = SellingAgencyName;
                data.SellingAgencyValue = SellingAgencyValue;
                data.Total = Total;

                return data;
            }
            set 
            {
                if (null != value)
                {
                    ListingAgencyName = value.NameOfListingAgency;
                    ListingAgencyValue = value.ListingAgencyValue;
                    SellingAgencyName = value.NameOfSellingAgency;
                    SellingAgencyValue = value.SellingAgencyValue;
                    Total = value.Total;
                }
            }
        }
        #endregion
        
        #region EVENT HANDLERS

        void On_saValue_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                HandleSellingAgencyValues();
        }

        void On_saValue_Leave(object sender, EventArgs e)
        {
            HandleSellingAgencyValues();
        }

        void On_laValue_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                HandleListingAgencyValues();
        }

        void On_laValue_Leave(object sender, EventArgs e)
        {
            HandleListingAgencyValues();
        }

        
        void On_sellingAgencyName_Click(object sender, EventArgs e)
        {
            //EditLabel el = new EditLabel(_sellingAgencyName.Text);
            //if (DialogResult.OK == el.ShowDialog())
            //    _sellingAgencyName.Text = el.LabelName;

            _sellingAgencyName.SelectAll();
        }        

        void On_listingAgencyName_Click(object sender, EventArgs e)
        {
            //EditLabel el = new EditLabel(_listingAgencyName.Text);
            //if (DialogResult.OK == el.ShowDialog())
            //    _listingAgencyName.Text = el.LabelName;
            _listingAgencyName.SelectAll();
        }

        void HandleListingAgencyValues()
        {
            decimal temp = _totalValue - Util.GetDecimalValue(_laValue.Text);
            _saValue.Text = temp.ToString(Formatter);
            _laValue.Text = Util.GetDecimalValue(_laValue.Text).ToString(Formatter);
        }

        void HandleSellingAgencyValues()
        {
            decimal temp = _totalValue - Util.GetDecimalValue(_saValue.Text);
            _laValue.Text = temp.ToString(Formatter);
            _saValue.Text = Util.GetDecimalValue(_saValue.Text).ToString(Formatter);
        }
        #endregion
    }
}
