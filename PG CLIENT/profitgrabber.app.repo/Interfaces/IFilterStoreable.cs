#region � Using �
using System;
using System.IO;
#endregion � Using �
#region � Namespace DealMaker �
namespace DealMaker
{
	#region � 0-- Interface IFilterStoreable �
	/// <summary>
	/// IFilterStoreable interface must implement each and every filter that will get persisted
	/// </summary>
	public interface IFilterStoreable
	{
		/// <summary>
		/// For storing object
		/// </summary>
		/// <param name="sw"></param>
		void StoreObject (StreamWriter sw);
	}
	#endregion � 0-- Interface IFilterStoreable �
}
#endregion � Namespace DealMaker �