using System;

namespace DealMaker.Interfaces
{
	/// <summary>
	/// Summary description for IDisplayControlObject.
	/// </summary>
	public interface IDisplayControlObject
	{
		void RefreshGUIOnChange();
	}
}
