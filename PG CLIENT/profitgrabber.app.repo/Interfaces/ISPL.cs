#region � Using �
using System;
using System.Data.SqlClient;
#endregion � Using �
#region � Namespace DealMaker �
namespace DealMaker
{
	#region � 0-- Interface ISPL �
	/// <summary>
	/// Interface for persisting objects via SPL
	/// </summary>
	public interface ISPL 
	{
		#region � ExecuteSPL �
		void ExecuteSPL(SqlConnection dbConn);		
		#endregion � ExecuteSPL �
	}
	#endregion � Interface ISPL �
}
#endregion � 0-- Namespace DealMaker �