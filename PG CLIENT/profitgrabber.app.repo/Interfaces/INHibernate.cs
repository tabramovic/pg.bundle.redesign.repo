#region � Using �
using System;
using NHibernate;
using NHibernate.Cfg;
#endregion � Using �
#region � Namespace DealMaker �
namespace DealMaker
{
	#region � 0-- Interface INHibernate �
	/// <summary>
	/// INHibernate - for saving objects using nHibernate
	/// </summary>
	public interface INHibernate
	{
		#region � NhSave �
		void NhSave (ISession session);
		#endregion � NhSave �
		#region � NhSaveOrUpdate �
		void NhSaveOrUpdate (ISession session);
		#endregion � NhSaveOrUpdate �
	}
	#endregion � 0-- Interface INHibernate �
}
#endregion � Namespace DealMaker �