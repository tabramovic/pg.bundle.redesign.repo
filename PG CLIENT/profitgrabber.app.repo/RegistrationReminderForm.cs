using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

//Added
using System.Threading;

namespace DealMaker
{
	/// <summary>
	/// Summary description for RegistrationReminderForm.
	/// </summary>
	public class RegistrationReminderForm : System.Windows.Forms.Form
	{
		private int waitingTime = 10;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Button bOK;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public RegistrationReminderForm()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			this.bOK.Click += new EventHandler(OK_EH);
			Globals.registrationReminder = new Thread(new ThreadStart(TimerWorker));
			Globals.registrationReminder.Start();
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}		

		private void TimerWorker()
		{
			for (int i = this.waitingTime; i >= 0; i--)
			{
				this.bOK.Text = i.ToString();
				System.Threading.Thread.Sleep(1000);
			}
			this.bOK.Text = "Close";
			this.bOK.Enabled = true;
		}


		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.bOK = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(16, 16);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(336, 23);
			this.label1.TabIndex = 0;
			this.label1.Text = "Please connect to the Internet to proof Your registration validation!";
			// 
			// bOK
			// 
			this.bOK.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(210)), ((System.Byte)(216)), ((System.Byte)(240)));
			this.bOK.Enabled = false;
			this.bOK.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.bOK.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.bOK.Location = new System.Drawing.Point(152, 64);
			this.bOK.Name = "bOK";
			this.bOK.TabIndex = 1;
			this.bOK.Text = "OK";
			// 
			// RegistrationReminderForm
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(210)), ((System.Byte)(216)), ((System.Byte)(240)));
			this.ClientSize = new System.Drawing.Size(354, 104);
			this.Controls.Add(this.bOK);
			this.Controls.Add(this.label1);
			this.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
			this.Name = "RegistrationReminderForm";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Registration Reminder";
			this.TopMost = true;
			this.ResumeLayout(false);

		}
		#endregion

		private void OK_EH(object sender, EventArgs e)
		{
			this.Close();
		}
	}
}
