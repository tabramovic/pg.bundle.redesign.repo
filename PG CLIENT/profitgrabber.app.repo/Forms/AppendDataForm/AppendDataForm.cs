﻿using System;
using System.Windows.Forms;
using DealMaker.PlainClasses.AppendData;
using System.Diagnostics;
using DataAppendService.Lib.Models.IDI;

namespace DealMaker.Forms.AppendDataForm
{
    public interface IAppendDataForm
    {
        void AppendDataWorker_OnCellRetrieved(bool success, string fullName, string address, string zip, string phoneNr);
        void AppendDataWorker_OnLandLineRetrieved(bool success, string fullName, string address, string zip, string phoneNr);
        void AppendDataWorker_OnEmailRetrieved(bool success, string fullName, string address, string zip, string email);
        void AppendDataWorker_OnSkipTraceDataRetrieved(SkiptraceClientResultset stcrs);
        void AppendDataWorker_OnProcessFinished(bool success, string error, int matchedCnt, int totalProcessCnt);        
    }

    public partial class AppendDataForm : Form
    {        
        AppendDataWorker appendDataWorker;
        int nodeId;
        AppendDataType appendType;
        int nrOfItems;
        bool finished = false;
        IAppendDataForm externalForm;
        IUpdateTreeViewAsync externalTreeView;

        Guid propertyItemId { get; set; }

        public AppendDataForm()
        {
            InitializeComponent();
        }

        public AppendDataForm(int nodeId, Guid propertyItemId, IAppendDataForm iAppendForm)
        {
            this.nodeId = nodeId;
            this.propertyItemId = propertyItemId;
            externalForm = iAppendForm;
            externalTreeView = Globals.customizedTreeViewUserControl;

            InitializeComponent();
            
            appendDataWorker = new AppendDataWorker(nodeId, propertyItemId);
            AssignExternalEHs();
            AssignTreeViewEHs();
            Init();
            
            ProcessSingleData();
        }

        public AppendDataForm(int nodeId, AppendDataType appendType, IUpdateTreeViewAsync treeView)
        {
            this.nodeId = nodeId;
            this.appendType = appendType;
            externalTreeView = treeView;

            InitializeComponent();
            
            appendDataWorker = new AppendDataWorker(nodeId, appendType);
            AssignTreeViewEHs();

            Init();            
            GetData();
        }

        

        void Init()
        {
            appendDataWorker.NotEnoughCredits += AppendDataWorker_OnNotEnoughCredits;

            progressBar.Minimum = 0;            
            appendDataWorker.OnItemIDsLoaded += AppendDataWorker_OnItemIDsLoaded;
            appendDataWorker.OnItemLoaded += AppendDataWorker_OnItemLoaded;
            appendDataWorker.OnItemSkipped += AppendDataWorker_OnItemSkipped;
            appendDataWorker.OnCellRetrieved += AppendDataWorker_OnCellRetrieved;
            appendDataWorker.OnEmailRetrieved += AppendDataWorker_OnEmailRetrieved;
            appendDataWorker.OnLandLineRetrieved += AppendDataWorker_OnLandLineRetrieved;
            appendDataWorker.OnItemProcessed += AppendDataWorker_OnItemProcessed;
            appendDataWorker.OnProcessFinished += AppendDataWorker_OnProcessFinished;            
        }
        
        void UnAssignEventHandlers()
        {
            appendDataWorker.NotEnoughCredits -= AppendDataWorker_OnNotEnoughCredits;

            appendDataWorker.OnItemIDsLoaded -= AppendDataWorker_OnItemIDsLoaded;
            appendDataWorker.OnItemLoaded -= AppendDataWorker_OnItemLoaded;
            appendDataWorker.OnItemSkipped -= AppendDataWorker_OnItemSkipped;
            appendDataWorker.OnCellRetrieved -= AppendDataWorker_OnCellRetrieved;
            appendDataWorker.OnEmailRetrieved -= AppendDataWorker_OnEmailRetrieved;
            appendDataWorker.OnLandLineRetrieved -= AppendDataWorker_OnLandLineRetrieved;
            appendDataWorker.OnItemProcessed -= AppendDataWorker_OnItemProcessed;
            appendDataWorker.OnProcessFinished -= AppendDataWorker_OnProcessFinished;
        }

        void AssignExternalEHs()
        {
            appendDataWorker.OnEmailRetrieved += externalForm.AppendDataWorker_OnEmailRetrieved;
            appendDataWorker.OnCellRetrieved += externalForm.AppendDataWorker_OnCellRetrieved;
            appendDataWorker.OnLandLineRetrieved += externalForm.AppendDataWorker_OnLandLineRetrieved;
            appendDataWorker.OnProcessFinished += externalForm.AppendDataWorker_OnProcessFinished;
            appendDataWorker.OnSkipTraceDataRetrieved += externalForm.AppendDataWorker_OnSkipTraceDataRetrieved;
        }
        

        void UnAssignExternalEHs()
        {
            if (null != externalForm)
            {
                appendDataWorker.OnEmailRetrieved -= externalForm.AppendDataWorker_OnEmailRetrieved;
                appendDataWorker.OnCellRetrieved -= externalForm.AppendDataWorker_OnCellRetrieved;
                appendDataWorker.OnLandLineRetrieved -= externalForm.AppendDataWorker_OnLandLineRetrieved;
                appendDataWorker.OnProcessFinished -= externalForm.AppendDataWorker_OnProcessFinished;
                appendDataWorker.OnSkipTraceDataRetrieved -= externalForm.AppendDataWorker_OnSkipTraceDataRetrieved;
            }

            if (null != externalTreeView)
                appendDataWorker.OnSubGroupCreated -= externalTreeView.UpdateTreeViewAsync;
        }

        void AssignTreeViewEHs()
        {
            if (null != externalTreeView)
                appendDataWorker.OnSubGroupCreated += externalTreeView.UpdateTreeViewAsync;
        }
        
        private void AppendDataWorker_OnLandLineRetrieved(bool success, string fullName, string address, string zip, string phoneNr)
        {
            UpdateAppendedLandLineResult(success, fullName, address, zip, "landline", phoneNr);
        }

        private void AppendDataWorker_OnEmailRetrieved(bool success, string fullName, string address, string zip, string email)
        {
            UpdateAppendedEmailResult(success, fullName, address, zip, "email", email);
        }

        private void AppendDataWorker_OnCellRetrieved(bool success, string fullName, string address, string zip, string phoneNr)
        {
            UpdateAppendedCellResult(success, fullName, address, zip, "cell phone", phoneNr);
        }

        private void AppendDataWorker_OnItemLoaded(string fullName, string address, string zip)
        {
            UpdateLoadedItem(fullName, address, zip);
        }

        private void AppendDataWorker_OnItemSkipped(bool name, bool address, bool zip)
        {
            UpdateSkippedItem(name, address, zip);
        }

        private void AppendDataWorker_OnItemIDsLoaded(int numberOfItems)
        {            
            if (IsHandleCreated)
            {
                this.BeginInvoke(new UpdateLabelAsync(UpdateLabel), new object[] { numberOfItems });
                this.BeginInvoke(new UpdatePBMaxValueAsync(UpdatePBMaxValue), new object[] { numberOfItems });
            }  
            else
            {
                System.Diagnostics.Debug.WriteLine("IsHandleCreated = false");
            }
        }

        private void AppendDataWorker_OnItemProcessed()
        {
            IncreasePBValue();
        }

        private void AppendDataWorker_OnNotEnoughCredits()
        {
            if (InvokeRequired)
                BeginInvoke((Action)( () =>  { _warning.Text = "NOT ENOUGH CREDITS, PLEASE CLICK HERE TO TOP-UP"; } ));
        }

        private void close_Click(object sender, EventArgs e)
        {
            if (!finished)
            {
                if (DialogResult.Yes == MessageBox.Show($"Are you sure you want to stop processing records?", "Append Records", MessageBoxButtons.YesNo, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1))
                {
                    appendDataWorker.StopProcessing();
                    UnAssignEventHandlers();
                    UnAssignExternalEHs();
                    this.Close();
                }
            }
            else
            {
                UnAssignEventHandlers();
                UnAssignExternalEHs();
                this.Close();
            }
        }

        private void GetData()
        {
            System.Threading.Tasks.Task.Run(async () => await appendDataWorker.StartProcessing());
        }

        private void ProcessSingleData()
        {                        
            System.Threading.Tasks.Task.Run(() => appendDataWorker.ProcessSingle());
        }

        delegate void UpdateLabelAsync(int nrOfItems);

        private void UpdateLabel(int nrOfItems)
        {
            if (this.operation.InvokeRequired)
            {
                this.BeginInvoke(new UpdateLabelAsync(UpdateLabel), new object[] { nrOfItems });
            }
            else
            {
                if (1 == nrOfItems)
                    this.operation.Text = $"{nrOfItems} contact will be evaluated for skip tracing.";
                else
                    this.operation.Text = $"{nrOfItems} contacts will be evaluated for skip tracing.";

                this.nrOfItems = nrOfItems;
            }
        }

        delegate void UpdateProgressAsync(int cnt, int nrOfItems);

        private void UpdateProgress(int cnt, int nrOfItems)
        {
            if (this.InvokeRequired)
            {
                this.BeginInvoke(new UpdateProgressAsync(UpdateProgress), new object[] { cnt, nrOfItems });
            }
            else
                this.operation.Text = $"{cnt}/{nrOfItems} contacts matched";
        }


        delegate void UpdateSkippedItemAsync(bool name, bool address, bool zip);
        private void UpdateSkippedItem(bool name, bool address, bool zip)
        {
            if (this.InvokeRequired)
            {
                this.BeginInvoke(new UpdateSkippedItemAsync(UpdateSkippedItem), new object[] { name, address, zip });
            }
            else
            {
                if (name)
                    lastName.Text = "MISSING";

                if (address || zip)
                    this.address.Text = "MISSING";

                IncreasePBValue();
            }
        }

        delegate void UpdateLoadedItemAsync(string fullName, string address, string zip);

        private void UpdateLoadedItem(string fullName, string address, string zip)
        {
            if (this.InvokeRequired)
            {
                this.BeginInvoke(new UpdateLoadedItemAsync(UpdateLoadedItem), new object[] { fullName, address, zip });
            }
            else
            {
                this.lastName.Text = $"{fullName.ToUpper()}";
                this.address.Text = $"{address.ToUpper()}, {zip}";
                this.cellphone.Text = "-";
                this.landline.Text = "-";
                this.email.Text = "-";
            }
        }


        delegate void UpdateCellResultAsync(bool success, string fullName, string address, string zip, string label, string value);
        private void UpdateAppendedCellResult(bool success, string fullName, string address, string zip, string label, string value)
        {
            if (this.InvokeRequired)
            {
                this.BeginInvoke(new UpdateCellResultAsync(UpdateAppendedCellResult), new object[] { success, fullName, address, zip, label, value });
            }
            else
            {
                this.cellphone.Text = $"{(success ? value : "NOT FOUND")}";
            }
        }

        delegate void UpdateLandLineResultAsync(bool success, string fullName, string address, string zip, string label, string value);
        private void UpdateAppendedLandLineResult(bool success, string fullName, string address, string zip, string label, string value)
        {
            if (this.InvokeRequired)
            {
                this.BeginInvoke(new UpdateLandLineResultAsync(UpdateAppendedLandLineResult), new object[] { success, fullName, address, zip, label, value });
            }
            else
            {
                this.landline.Text = $"{(success ? value : "NOT FOUND")}";
            }
        }

        delegate void UpdateEmailResultAsync(bool success, string fullName, string address, string zip, string label, string value);
        private void UpdateAppendedEmailResult(bool success, string fullName, string address, string zip, string label, string value)
        {
            if (this.InvokeRequired)
            {
                this.BeginInvoke(new UpdateEmailResultAsync(UpdateAppendedEmailResult), new object[] { success, fullName, address, zip, label, value });
            }
            else
            {
                this.email.Text = $"{(success ? value : "NOT FOUND")}";
            }
        }
       
        private delegate void UpdatePBMaxValueAsync(int maxValue);
        private void UpdatePBMaxValue(int maxValue)
        {
            if (this.InvokeRequired)
            {
                this.BeginInvoke(new UpdatePBMaxValueAsync(UpdatePBMaxValue), new object[] { maxValue });
            }
            else
            {
                progressBar.Maximum = maxValue;
            }
        }

        private delegate void IncreasePBValueAsync();
        private void IncreasePBValue()
        {
            if (this.InvokeRequired)
            {
                this.BeginInvoke(new IncreasePBValueAsync(IncreasePBValue));
            }
            else
            {
                if (progressBar.Value < progressBar.Maximum)
                    ++progressBar.Value;                 
            }
        }

        delegate void UpdateButtonLabelAsync();

        private void UpdateButtonLabel()
        {
            if (this.InvokeRequired)
            {
                this.BeginInvoke(new UpdateButtonLabelAsync(UpdateButtonLabel));
            }
            else
            {
                this.close.Text = "Close";                
            }
        }

        private void AppendDataWorker_OnProcessFinished(bool success, string error, int matchedCnt, int totalProcessCnt)
        {            
            finished = true;
            UpdateButtonLabel();

            if (!success)            
                UpdateInfoLabel(error);            
            else
                UpdateInfoLabel($"{totalProcessCnt} contacts have beed skip traced. {matchedCnt} contacts matched.");
        }        

        delegate void UpdateInfoLabelDelegate(string err);

        private void UpdateInfoLabel(string msg)
        {
            if (this.InvokeRequired)
            {
                this.BeginInvoke(new UpdateInfoLabelDelegate(UpdateInfoLabel), new object[] { msg });
            }
            else
            {
                this.infoLabel.Text = msg;
            }
        }

        private void _warning_Click(object sender, EventArgs e)
        {
            if (null != Globals.UserRegistration)
            {
                Process.Start("https://www.ProfitGrabber.com/SkipTracing/plans.php?pgid={" + Globals.UserRegistration.IDUser.ToString() + "}");                
            }
        }        
    }
}
