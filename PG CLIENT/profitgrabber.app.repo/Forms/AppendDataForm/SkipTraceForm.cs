﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DealMaker.Forms.AppendDataForm
{
    public partial class SkipTraceForm : Form
    {
        public SkipTraceForm()
        {
            InitializeComponent();
        }

        private void _btnOK_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void viewHistory_Click(object sender, EventArgs e)
        {
            if (null != Globals.UserRegistration)
            {
                Process.Start("https://www.ProfitGrabber.com/SkipTracing/?pgid={" + Globals.UserRegistration.IDUser.ToString() + "}");                
            }
        }

        private void viewHistory_MouseHover(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.Hand;
        }

        private void selectPlan_Click(object sender, EventArgs e)
        {
            if (null != Globals.UserRegistration)
            {
                Process.Start("https://www.profitgrabber.com/SkipTracing/plans.php?pgid={" + Globals.UserRegistration.IDUser.ToString() + "}");                
            }
        }
    }
}
