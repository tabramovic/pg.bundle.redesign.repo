﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DealMaker
{
    public enum UserEntries
    {
        MediaList, 
        OfficeStaffList,
        LeadSourceForSellingList,
        EmailList,
        NotSet,
    }
}
