using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;

//Added
using DealMaker.Forms.Task2Email;

namespace DealMaker
{
	/// <summary>
	/// Summary description for UserInformationForm.
	/// </summary>
	public class UserInformationForm : System.Windows.Forms.Form
    {
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.ColumnHeader media;
		private System.Windows.Forms.TextBox tbFirstName;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.Button bOK;
		private System.Windows.Forms.TextBox tbLastName;
		private System.Windows.Forms.TextBox tbCompanyName;
		private System.Windows.Forms.TextBox tbAddress;
		private System.Windows.Forms.TextBox tbPhoneNumber;
		private System.Windows.Forms.TextBox tbCity;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.TextBox tbState;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.TextBox tbZIP;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.TextBox tbFax;
		private System.Windows.Forms.ListView lvMedia;
		private System.Windows.Forms.Button bAdd;
		private System.Windows.Forms.Button bDelete;
        private Button _deleteOfficeStaff;
        private Button _addOfficeStaff;
        private ListView _officeStaffList;
        private ColumnHeader columnHeader1;
        private Button _deleteLeadSource;
        private Button _addLeadSource;
        private ListView _leadSouceList;
        private ColumnHeader columnHeader2;
        private GroupBox groupBox1;
        private GroupBox groupBox2;
        private GroupBox groupBox3;
        private GroupBox groupBox4;
        private TextBox tbEmail;
        private Label label1;
        private ContextMenuStrip _officeStaffCtxMenuStrip;
        private ToolStripMenuItem _updateEmailAddresses;
        private ToolStripMenuItem _clearEmailAddresses;
        private ToolStripMenuItem _taskToEmailSetup;
        private Label label12;
        private TextBox _foreclosureWebsite;
        private Label label15;
        private TextBox _buyingWebsite;
        private Label label16;
        private TextBox _email2;
        private Label label14;
        private Label label13;
        private TextBox _directNr;
        private PictureBox pbEmail1;
        private PictureBox pbEmail2;
        private PictureBox pbOfficeStaffList;
        private PictureBox pbAdvertisingMedia;
        private PictureBox pbLeadsourceForSellingList;
        private IContainer components;

		public UserInformationForm()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

            _addOfficeStaff.Click += new EventHandler(On_AddOfficeStaff);
            _addLeadSource.Click += new EventHandler(On_AddLeadSource);

            _deleteLeadSource.Click += new EventHandler(On_DeleteLeadSource);
            _deleteOfficeStaff.Click += new EventHandler(On_DeleteOfficeStaff);
            
            _officeStaffList.SelectedIndexChanged += new EventHandler(On_OfficeStaffList_SelectedIndexChanged);
            _officeStaffCtxMenuStrip.Opening += new CancelEventHandler(On_OfficeStaffCtxMenuStrip_Opening);

            _clearEmailAddresses.Click += new EventHandler(On_ClearEmailAddresses_Click);
            _updateEmailAddresses.Click += new EventHandler(On_UpdateEmailAddresses_Click);
            _taskToEmailSetup.Click += new EventHandler(On_TaskToEmailSetup_Click);
		}                                       

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UserInformationForm));
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lvMedia = new System.Windows.Forms.ListView();
            this.media = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.tbFirstName = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.tbLastName = new System.Windows.Forms.TextBox();
            this.tbCompanyName = new System.Windows.Forms.TextBox();
            this.tbAddress = new System.Windows.Forms.TextBox();
            this.tbPhoneNumber = new System.Windows.Forms.TextBox();
            this.bOK = new System.Windows.Forms.Button();
            this.tbCity = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.tbState = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.tbZIP = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.tbFax = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.bAdd = new System.Windows.Forms.Button();
            this.bDelete = new System.Windows.Forms.Button();
            this._deleteOfficeStaff = new System.Windows.Forms.Button();
            this._addOfficeStaff = new System.Windows.Forms.Button();
            this._officeStaffList = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this._officeStaffCtxMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this._updateEmailAddresses = new System.Windows.Forms.ToolStripMenuItem();
            this._clearEmailAddresses = new System.Windows.Forms.ToolStripMenuItem();
            this._taskToEmailSetup = new System.Windows.Forms.ToolStripMenuItem();
            this._deleteLeadSource = new System.Windows.Forms.Button();
            this._addLeadSource = new System.Windows.Forms.Button();
            this._leadSouceList = new System.Windows.Forms.ListView();
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.pbEmail2 = new System.Windows.Forms.PictureBox();
            this.pbEmail1 = new System.Windows.Forms.PictureBox();
            this._foreclosureWebsite = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this._buyingWebsite = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this._email2 = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this._directNr = new System.Windows.Forms.TextBox();
            this.tbEmail = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.pbOfficeStaffList = new System.Windows.Forms.PictureBox();
            this.label12 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.pbAdvertisingMedia = new System.Windows.Forms.PictureBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.pbLeadsourceForSellingList = new System.Windows.Forms.PictureBox();
            this._officeStaffCtxMenuStrip.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbEmail2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbEmail1)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbOfficeStaffList)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbAdvertisingMedia)).BeginInit();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbLeadsourceForSellingList)).BeginInit();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(13, 18);
            this.label2.Name = "label2";
            this.label2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label2.Size = new System.Drawing.Size(100, 23);
            this.label2.TabIndex = 1;
            this.label2.Text = ":First Name";
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(13, 72);
            this.label3.Name = "label3";
            this.label3.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label3.Size = new System.Drawing.Size(100, 23);
            this.label3.TabIndex = 2;
            this.label3.Text = ":Company Name";
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(13, 99);
            this.label4.Name = "label4";
            this.label4.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label4.Size = new System.Drawing.Size(100, 23);
            this.label4.TabIndex = 3;
            this.label4.Text = ":Company Address";
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(13, 151);
            this.label5.Name = "label5";
            this.label5.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label5.Size = new System.Drawing.Size(104, 23);
            this.label5.TabIndex = 4;
            this.label5.Text = ":.Marketing Nr";
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(0, 429);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(161, 23);
            this.label6.TabIndex = 5;
            this.label6.Text = "Advertising Media";
            this.label6.Visible = false;
            // 
            // lvMedia
            // 
            this.lvMedia.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.media});
            this.lvMedia.FullRowSelect = true;
            this.lvMedia.GridLines = true;
            this.lvMedia.Location = new System.Drawing.Point(9, 28);
            this.lvMedia.Name = "lvMedia";
            this.lvMedia.Size = new System.Drawing.Size(449, 152);
            this.lvMedia.TabIndex = 23;
            this.lvMedia.UseCompatibleStateImageBehavior = false;
            this.lvMedia.View = System.Windows.Forms.View.Details;
            // 
            // media
            // 
            this.media.Text = "Media";
            this.media.Width = 420;
            // 
            // tbFirstName
            // 
            this.tbFirstName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbFirstName.Location = new System.Drawing.Point(117, 15);
            this.tbFirstName.Name = "tbFirstName";
            this.tbFirstName.Size = new System.Drawing.Size(338, 20);
            this.tbFirstName.TabIndex = 7;
            this.tbFirstName.Enter += new System.EventHandler(this.textBox_Enter);
            this.tbFirstName.Leave += new System.EventHandler(this.textBox_Leave);
            // 
            // label7
            // 
            this.label7.Location = new System.Drawing.Point(11, 45);
            this.label7.Name = "label7";
            this.label7.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label7.Size = new System.Drawing.Size(100, 23);
            this.label7.TabIndex = 8;
            this.label7.Text = ":Last Name";
            // 
            // tbLastName
            // 
            this.tbLastName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbLastName.Location = new System.Drawing.Point(117, 42);
            this.tbLastName.Name = "tbLastName";
            this.tbLastName.Size = new System.Drawing.Size(338, 20);
            this.tbLastName.TabIndex = 9;
            this.tbLastName.Enter += new System.EventHandler(this.textBox_Enter);
            this.tbLastName.Leave += new System.EventHandler(this.textBox_Leave);
            // 
            // tbCompanyName
            // 
            this.tbCompanyName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbCompanyName.Location = new System.Drawing.Point(117, 69);
            this.tbCompanyName.Name = "tbCompanyName";
            this.tbCompanyName.Size = new System.Drawing.Size(338, 20);
            this.tbCompanyName.TabIndex = 10;
            this.tbCompanyName.Enter += new System.EventHandler(this.textBox_Enter);
            this.tbCompanyName.Leave += new System.EventHandler(this.textBox_Leave);
            // 
            // tbAddress
            // 
            this.tbAddress.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbAddress.Location = new System.Drawing.Point(117, 96);
            this.tbAddress.Name = "tbAddress";
            this.tbAddress.Size = new System.Drawing.Size(338, 20);
            this.tbAddress.TabIndex = 11;
            this.tbAddress.Enter += new System.EventHandler(this.textBox_Enter);
            this.tbAddress.Leave += new System.EventHandler(this.textBox_Leave);
            // 
            // tbPhoneNumber
            // 
            this.tbPhoneNumber.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbPhoneNumber.Location = new System.Drawing.Point(117, 149);
            this.tbPhoneNumber.Name = "tbPhoneNumber";
            this.tbPhoneNumber.Size = new System.Drawing.Size(128, 20);
            this.tbPhoneNumber.TabIndex = 19;
            this.tbPhoneNumber.Enter += new System.EventHandler(this.textBox_Enter);
            this.tbPhoneNumber.Leave += new System.EventHandler(this.textBox_Leave);
            // 
            // bOK
            // 
            this.bOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.bOK.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.bOK.Location = new System.Drawing.Point(827, 595);
            this.bOK.Name = "bOK";
            this.bOK.Size = new System.Drawing.Size(120, 24);
            this.bOK.TabIndex = 25;
            this.bOK.Text = "&Save and Close";
            this.bOK.Click += new System.EventHandler(this.bOK_Click);
            // 
            // tbCity
            // 
            this.tbCity.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbCity.Location = new System.Drawing.Point(117, 123);
            this.tbCity.Name = "tbCity";
            this.tbCity.Size = new System.Drawing.Size(147, 20);
            this.tbCity.TabIndex = 13;
            this.tbCity.Enter += new System.EventHandler(this.textBox_Enter);
            this.tbCity.Leave += new System.EventHandler(this.textBox_Leave);
            // 
            // label8
            // 
            this.label8.Location = new System.Drawing.Point(13, 126);
            this.label8.Name = "label8";
            this.label8.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label8.Size = new System.Drawing.Size(100, 23);
            this.label8.TabIndex = 14;
            this.label8.Text = ":City";
            // 
            // tbState
            // 
            this.tbState.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbState.Location = new System.Drawing.Point(309, 123);
            this.tbState.Name = "tbState";
            this.tbState.Size = new System.Drawing.Size(23, 20);
            this.tbState.TabIndex = 15;
            this.tbState.Enter += new System.EventHandler(this.textBox_Enter);
            this.tbState.Leave += new System.EventHandler(this.textBox_Leave);
            // 
            // label9
            // 
            this.label9.Location = new System.Drawing.Point(255, 126);
            this.label9.Name = "label9";
            this.label9.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label9.Size = new System.Drawing.Size(51, 23);
            this.label9.TabIndex = 16;
            this.label9.Text = ":State";
            // 
            // tbZIP
            // 
            this.tbZIP.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbZIP.Location = new System.Drawing.Point(393, 123);
            this.tbZIP.Name = "tbZIP";
            this.tbZIP.Size = new System.Drawing.Size(62, 20);
            this.tbZIP.TabIndex = 17;
            this.tbZIP.Enter += new System.EventHandler(this.textBox_Enter);
            this.tbZIP.Leave += new System.EventHandler(this.textBox_Leave);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(360, 125);
            this.label10.Name = "label10";
            this.label10.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label10.Size = new System.Drawing.Size(27, 13);
            this.label10.TabIndex = 18;
            this.label10.Text = ":ZIP";
            // 
            // tbFax
            // 
            this.tbFax.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbFax.Location = new System.Drawing.Point(117, 175);
            this.tbFax.Name = "tbFax";
            this.tbFax.Size = new System.Drawing.Size(128, 20);
            this.tbFax.TabIndex = 21;
            this.tbFax.Enter += new System.EventHandler(this.textBox_Enter);
            this.tbFax.Leave += new System.EventHandler(this.textBox_Leave);
            // 
            // label11
            // 
            this.label11.Location = new System.Drawing.Point(71, 178);
            this.label11.Name = "label11";
            this.label11.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label11.Size = new System.Drawing.Size(43, 23);
            this.label11.TabIndex = 20;
            this.label11.Text = ":Fax";
            // 
            // bAdd
            // 
            this.bAdd.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.bAdd.Location = new System.Drawing.Point(338, 186);
            this.bAdd.Name = "bAdd";
            this.bAdd.Size = new System.Drawing.Size(120, 24);
            this.bAdd.TabIndex = 26;
            this.bAdd.Text = "&Add Media";
            this.bAdd.Click += new System.EventHandler(this.bAdd_Click);
            // 
            // bDelete
            // 
            this.bDelete.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.bDelete.Location = new System.Drawing.Point(212, 186);
            this.bDelete.Name = "bDelete";
            this.bDelete.Size = new System.Drawing.Size(120, 24);
            this.bDelete.TabIndex = 27;
            this.bDelete.Text = "&Delete Media";
            this.bDelete.Click += new System.EventHandler(this.bDelete_Click);
            // 
            // _deleteOfficeStaff
            // 
            this._deleteOfficeStaff.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._deleteOfficeStaff.Location = new System.Drawing.Point(212, 287);
            this._deleteOfficeStaff.Name = "_deleteOfficeStaff";
            this._deleteOfficeStaff.Size = new System.Drawing.Size(120, 24);
            this._deleteOfficeStaff.TabIndex = 31;
            this._deleteOfficeStaff.Text = "Delete &Office Staff";
            // 
            // _addOfficeStaff
            // 
            this._addOfficeStaff.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._addOfficeStaff.Location = new System.Drawing.Point(338, 287);
            this._addOfficeStaff.Name = "_addOfficeStaff";
            this._addOfficeStaff.Size = new System.Drawing.Size(120, 24);
            this._addOfficeStaff.TabIndex = 30;
            this._addOfficeStaff.Text = "Add &Office Staff";
            // 
            // _officeStaffList
            // 
            this._officeStaffList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1});
            this._officeStaffList.ContextMenuStrip = this._officeStaffCtxMenuStrip;
            this._officeStaffList.FullRowSelect = true;
            this._officeStaffList.GridLines = true;
            this._officeStaffList.Location = new System.Drawing.Point(9, 75);
            this._officeStaffList.Name = "_officeStaffList";
            this._officeStaffList.Size = new System.Drawing.Size(449, 206);
            this._officeStaffList.TabIndex = 29;
            this._officeStaffList.UseCompatibleStateImageBehavior = false;
            this._officeStaffList.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Office Staff List";
            this.columnHeader1.Width = 420;
            // 
            // _officeStaffCtxMenuStrip
            // 
            this._officeStaffCtxMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._updateEmailAddresses,
            this._clearEmailAddresses,
            this._taskToEmailSetup});
            this._officeStaffCtxMenuStrip.Name = "_officeStaffCtxMenuStrip";
            this._officeStaffCtxMenuStrip.Size = new System.Drawing.Size(238, 70);
            // 
            // _updateEmailAddresses
            // 
            this._updateEmailAddresses.Enabled = false;
            this._updateEmailAddresses.Name = "_updateEmailAddresses";
            this._updateEmailAddresses.Size = new System.Drawing.Size(237, 22);
            this._updateEmailAddresses.Text = "Update Email Addresses";
            // 
            // _clearEmailAddresses
            // 
            this._clearEmailAddresses.Enabled = false;
            this._clearEmailAddresses.Name = "_clearEmailAddresses";
            this._clearEmailAddresses.Size = new System.Drawing.Size(237, 22);
            this._clearEmailAddresses.Text = "Clear Email Addresses ";
            // 
            // _taskToEmailSetup
            // 
            this._taskToEmailSetup.Enabled = false;
            this._taskToEmailSetup.Name = "_taskToEmailSetup";
            this._taskToEmailSetup.Size = new System.Drawing.Size(237, 22);
            this._taskToEmailSetup.Text = "Set Automatic Daily Send Time";
            // 
            // _deleteLeadSource
            // 
            this._deleteLeadSource.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._deleteLeadSource.Location = new System.Drawing.Point(212, 186);
            this._deleteLeadSource.Name = "_deleteLeadSource";
            this._deleteLeadSource.Size = new System.Drawing.Size(120, 24);
            this._deleteLeadSource.TabIndex = 35;
            this._deleteLeadSource.Text = "Delete &Leadsource";
            // 
            // _addLeadSource
            // 
            this._addLeadSource.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._addLeadSource.Location = new System.Drawing.Point(338, 186);
            this._addLeadSource.Name = "_addLeadSource";
            this._addLeadSource.Size = new System.Drawing.Size(120, 24);
            this._addLeadSource.TabIndex = 34;
            this._addLeadSource.Text = "Add &Leadsource";
            // 
            // _leadSouceList
            // 
            this._leadSouceList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader2});
            this._leadSouceList.FullRowSelect = true;
            this._leadSouceList.GridLines = true;
            this._leadSouceList.Location = new System.Drawing.Point(11, 28);
            this._leadSouceList.Name = "_leadSouceList";
            this._leadSouceList.Size = new System.Drawing.Size(449, 152);
            this._leadSouceList.TabIndex = 33;
            this._leadSouceList.UseCompatibleStateImageBehavior = false;
            this._leadSouceList.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Leadsource for Selling List";
            this.columnHeader2.Width = 420;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.pbEmail2);
            this.groupBox1.Controls.Add(this.pbEmail1);
            this.groupBox1.Controls.Add(this._foreclosureWebsite);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this._buyingWebsite);
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Controls.Add(this._email2);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this._directNr);
            this.groupBox1.Controls.Add(this.tbEmail);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.tbFirstName);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.tbLastName);
            this.groupBox1.Controls.Add(this.tbCompanyName);
            this.groupBox1.Controls.Add(this.tbAddress);
            this.groupBox1.Controls.Add(this.tbPhoneNumber);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.tbFax);
            this.groupBox1.Controls.Add(this.tbCity);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.tbZIP);
            this.groupBox1.Controls.Add(this.tbState);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Location = new System.Drawing.Point(3, 7);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(466, 317);
            this.groupBox1.TabIndex = 36;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "User Information";
            // 
            // pbEmail2
            // 
            this.pbEmail2.Image = ((System.Drawing.Image)(resources.GetObject("pbEmail2.Image")));
            this.pbEmail2.Location = new System.Drawing.Point(98, 227);
            this.pbEmail2.Name = "pbEmail2";
            this.pbEmail2.Size = new System.Drawing.Size(16, 16);
            this.pbEmail2.TabIndex = 32;
            this.pbEmail2.TabStop = false;
            this.pbEmail2.MouseHover += new System.EventHandler(this.pbEmail2_MouseHover);
            // 
            // pbEmail1
            // 
            this.pbEmail1.Image = ((System.Drawing.Image)(resources.GetObject("pbEmail1.Image")));
            this.pbEmail1.Location = new System.Drawing.Point(98, 201);
            this.pbEmail1.Name = "pbEmail1";
            this.pbEmail1.Size = new System.Drawing.Size(16, 16);
            this.pbEmail1.TabIndex = 31;
            this.pbEmail1.TabStop = false;
            this.pbEmail1.MouseHover += new System.EventHandler(this.pbEmail1_MouseHover);
            // 
            // _foreclosureWebsite
            // 
            this._foreclosureWebsite.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._foreclosureWebsite.Location = new System.Drawing.Point(117, 279);
            this._foreclosureWebsite.Name = "_foreclosureWebsite";
            this._foreclosureWebsite.Size = new System.Drawing.Size(338, 20);
            this._foreclosureWebsite.TabIndex = 26;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(6, 281);
            this.label15.Name = "label15";
            this.label15.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label15.Size = new System.Drawing.Size(107, 13);
            this.label15.TabIndex = 30;
            this.label15.Text = ":Foreclosure Website";
            // 
            // _buyingWebsite
            // 
            this._buyingWebsite.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._buyingWebsite.Location = new System.Drawing.Point(117, 253);
            this._buyingWebsite.Name = "_buyingWebsite";
            this._buyingWebsite.Size = new System.Drawing.Size(338, 20);
            this._buyingWebsite.TabIndex = 25;
            // 
            // label16
            // 
            this.label16.Location = new System.Drawing.Point(13, 256);
            this.label16.Name = "label16";
            this.label16.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label16.Size = new System.Drawing.Size(100, 23);
            this.label16.TabIndex = 28;
            this.label16.Text = ":Buying Website";
            // 
            // _email2
            // 
            this._email2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._email2.Location = new System.Drawing.Point(117, 227);
            this._email2.Name = "_email2";
            this._email2.Size = new System.Drawing.Size(338, 20);
            this._email2.TabIndex = 24;
            // 
            // label14
            // 
            this.label14.Location = new System.Drawing.Point(13, 230);
            this.label14.Name = "label14";
            this.label14.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label14.Size = new System.Drawing.Size(76, 23);
            this.label14.TabIndex = 26;
            this.label14.Text = ":Email 2";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(251, 151);
            this.label13.Name = "label13";
            this.label13.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label13.Size = new System.Drawing.Size(55, 13);
            this.label13.TabIndex = 24;
            this.label13.Text = ":.Direct Nr";
            // 
            // _directNr
            // 
            this._directNr.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._directNr.Location = new System.Drawing.Point(309, 149);
            this._directNr.Name = "_directNr";
            this._directNr.Size = new System.Drawing.Size(149, 20);
            this._directNr.TabIndex = 20;
            // 
            // tbEmail
            // 
            this.tbEmail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbEmail.Location = new System.Drawing.Point(117, 201);
            this.tbEmail.Name = "tbEmail";
            this.tbEmail.Size = new System.Drawing.Size(338, 20);
            this.tbEmail.TabIndex = 23;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(10, 204);
            this.label1.Name = "label1";
            this.label1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label1.Size = new System.Drawing.Size(79, 23);
            this.label1.TabIndex = 22;
            this.label1.Text = ":Email 1";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.pbOfficeStaffList);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this._addOfficeStaff);
            this.groupBox2.Controls.Add(this._deleteOfficeStaff);
            this.groupBox2.Controls.Add(this._officeStaffList);
            this.groupBox2.Location = new System.Drawing.Point(485, 7);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(466, 317);
            this.groupBox2.TabIndex = 37;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Office Staff List";
            // 
            // pbOfficeStaffList
            // 
            this.pbOfficeStaffList.Image = ((System.Drawing.Image)(resources.GetObject("pbOfficeStaffList.Image")));
            this.pbOfficeStaffList.Location = new System.Drawing.Point(442, 5);
            this.pbOfficeStaffList.Name = "pbOfficeStaffList";
            this.pbOfficeStaffList.Size = new System.Drawing.Size(16, 16);
            this.pbOfficeStaffList.TabIndex = 34;
            this.pbOfficeStaffList.TabStop = false;
            this.pbOfficeStaffList.MouseHover += new System.EventHandler(this.pbOfficeStaffList_MouseHover);
            // 
            // label12
            // 
            this.label12.Location = new System.Drawing.Point(11, 21);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(447, 47);
            this.label12.TabIndex = 32;
            this.label12.Text = "To enter, edit or delete email or text information, please right click on an Offi" +
    "ce Staff Member for menu options. You may also setup a daily time to send task r" +
    "eminders for each Member.";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.pbAdvertisingMedia);
            this.groupBox3.Controls.Add(this.lvMedia);
            this.groupBox3.Controls.Add(this.bAdd);
            this.groupBox3.Controls.Add(this.bDelete);
            this.groupBox3.Location = new System.Drawing.Point(16, 336);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(466, 216);
            this.groupBox3.TabIndex = 38;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Advertising Media";
            // 
            // pbAdvertisingMedia
            // 
            this.pbAdvertisingMedia.Image = ((System.Drawing.Image)(resources.GetObject("pbAdvertisingMedia.Image")));
            this.pbAdvertisingMedia.Location = new System.Drawing.Point(437, 6);
            this.pbAdvertisingMedia.Name = "pbAdvertisingMedia";
            this.pbAdvertisingMedia.Size = new System.Drawing.Size(16, 16);
            this.pbAdvertisingMedia.TabIndex = 33;
            this.pbAdvertisingMedia.TabStop = false;
            this.pbAdvertisingMedia.MouseHover += new System.EventHandler(this.pbAdvertisingMedia_MouseHover);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.pbLeadsourceForSellingList);
            this.groupBox4.Controls.Add(this._leadSouceList);
            this.groupBox4.Controls.Add(this._addLeadSource);
            this.groupBox4.Controls.Add(this._deleteLeadSource);
            this.groupBox4.Location = new System.Drawing.Point(488, 336);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(466, 216);
            this.groupBox4.TabIndex = 39;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Leadsource for Selling List";
            // 
            // pbLeadsourceForSellingList
            // 
            this.pbLeadsourceForSellingList.Image = ((System.Drawing.Image)(resources.GetObject("pbLeadsourceForSellingList.Image")));
            this.pbLeadsourceForSellingList.Location = new System.Drawing.Point(439, 6);
            this.pbLeadsourceForSellingList.Name = "pbLeadsourceForSellingList";
            this.pbLeadsourceForSellingList.Size = new System.Drawing.Size(16, 16);
            this.pbLeadsourceForSellingList.TabIndex = 35;
            this.pbLeadsourceForSellingList.TabStop = false;
            this.pbLeadsourceForSellingList.MouseHover += new System.EventHandler(this.pbLeadsourceForSellingList_MouseHover);
            // 
            // UserInformationForm
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.ClientSize = new System.Drawing.Size(959, 631);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.bOK);
            this.Controls.Add(this.label6);
            this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "UserInformationForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "User Information / Advertising Media / Officestaff List / Leadsource for Selling " +
    "List";
            this.Load += new System.EventHandler(this.UserInformationForm_Load);
            this._officeStaffCtxMenuStrip.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbEmail2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbEmail1)).EndInit();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbOfficeStaffList)).EndInit();
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbAdvertisingMedia)).EndInit();
            this.groupBox4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbLeadsourceForSellingList)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private void textBox_Enter(object sender, System.EventArgs e)
		{
			TextBox tb = (TextBox)sender;
			tb.BackColor = Color.LightBlue;			
		}

		private void textBox_Leave(object sender, System.EventArgs e)
		{
			TextBox tb = (TextBox)sender;
			tb.BackColor = Color.White;			
		}

		private void bOK_Click(object sender, System.EventArgs e)
		{
			User u = new User();

			u.FirstName		= this.tbFirstName.Text;
			u.LastName		= this.tbLastName.Text;
			u.PhoneNr		= this.tbPhoneNumber.Text;
			u.Address		= this.tbAddress.Text;
			u.City			= this.tbCity.Text;
			u.Fax			= this.tbFax.Text;
			u.State			= this.tbState.Text;
			u.ZIP			= this.tbZIP.Text;
			u.CompanyName	= this.tbCompanyName.Text;
            u.Email         = this.tbEmail.Text;

            u.Email2 = _email2.Text;
            u.DirectNumber = _directNr.Text;
            u.ForeclosureWebsite = _foreclosureWebsite.Text;
            u.BuyingWebsite = _buyingWebsite.Text;

            ApplicationHelper.SaveUser(u);
			this.Close();
		}

		private void UserInformationForm_Load(object sender, System.EventArgs e)
		{
			this.FillDialog(true);
            InitOfficeStaffList();
            InitLeadSourceForSellingList();
		}

		private void bAdd_Click(object sender, System.EventArgs e)
		{
            AddEntryForm amf = new AddEntryForm(UserEntries.MediaList);
			amf.ShowDialog();
			if (true == amf.Valid)
			{
				Media m = new Media();
				m.MediaName = amf.EntryName;
				ApplicationHelper.SaveMedia(m, eSaveOrUpdate.Save);

                MediaObject.Instance.ScanMedia();
				this.FillDialog(false);                
			}
		}


		private void bDelete_Click(object sender, System.EventArgs e)
		{
			
			ListView.SelectedListViewItemCollection lviColl = this.lvMedia.SelectedItems;			
			foreach (ListViewItem lvi in lviColl)
			{
				if (null != lvi)
				{
					Guid idMedia = (Guid)lvi.Tag;

                    string value = lvi.SubItems[0].Text;
                    if (DialogResult.Yes == MessageBox.Show("Are you sure you want to delete " + value + " from Advertising Media list?", "Delete Entry from Advertising Media list", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2))
                    {
                        ApplicationHelper.DeleteMedia(idMedia);
                        MediaObject.Instance.ScanMedia();
                        this.FillDialog(false);                        
                    }
				}		
			}			
		}

		private void FillDialog(bool loadUser)
		{
			this.lvMedia.Items.Clear();
			if (true == loadUser)
			{
				User u = ApplicationHelper.LoadUser();
				if (null != u)
				{
					this.tbFirstName.Text = u.FirstName;
					this.tbLastName.Text = u.LastName;
					this.tbAddress.Text = u.Address;
					this.tbCity.Text = u.City;
					this.tbCompanyName.Text = u.CompanyName;
					this.tbFax.Text = u.Fax;
					this.tbPhoneNumber.Text = u.PhoneNr;
					this.tbState.Text = u.State;
					this.tbZIP.Text = u.ZIP;
                    this.tbEmail.Text = u.Email;
                    _email2.Text = u.Email2;
                    _directNr.Text = u.DirectNumber;
                    _foreclosureWebsite.Text = u.ForeclosureWebsite;
                    _buyingWebsite.Text = u.BuyingWebsite;
				}
				else
				{	
					if (null != Globals.UserRegistration)
					{
						this.tbFirstName.Text = Globals.UserRegistration.FirstName;
						this.tbLastName.Text = Globals.UserRegistration.LastName;
						this.tbFax.Text = string.Empty;

						if (null != Globals.UserRegistration.Address)
						{
							this.tbAddress.Text = Globals.UserRegistration.Address.FullSiteStreetNumber;
							this.tbCity.Text = Globals.UserRegistration.Address.SiteCity;
							this.tbCompanyName.Text = Globals.UserRegistration.Address.Company;					
							this.tbPhoneNumber.Text = Globals.UserRegistration.Address.PhoneCell;
							this.tbState.Text = Globals.UserRegistration.Address.SiteState;
							this.tbZIP.Text = Globals.UserRegistration.Address.SiteZIP;				
						}
					}
				}
			}
            //ArrayList alMedia = ApplicationHelper.GetMediaList();
            //foreach (Guid mediaId in alMedia)
            //{
            //    Media m = ApplicationHelper.LoadMedia(mediaId);
            //    ListViewItem lvi = new ListViewItem(m.MediaName);				
            //    lvi.Tag = mediaId;
            //    this.lvMedia.Items.Add(lvi);
            //}

            foreach (Media media in MediaObject.Instance.MediaList)
                if (null != media && string.Empty != media.MediaName)
                {
                    ListViewItem lvi = new ListViewItem(media.MediaName);
                    lvi.Tag = media.IdMedia;
                    this.lvMedia.Items.Add(lvi);
                }
		}

        void InitOfficeStaffList()
        {
            Dictionary<Guid, string> dict = UserInformationManager.Instance.GetValues(UserEntries.OfficeStaffList, true);
            Dictionary<Guid, string>.Enumerator enumerator = dict.GetEnumerator();

            _officeStaffList.Items.Clear();

            while (enumerator.MoveNext())
            {
                ListViewItem lvi = new ListViewItem(enumerator.Current.Value);
                lvi.Tag = enumerator.Current.Key;

                _officeStaffList.Items.Add(lvi);
            }

        }

        void InitLeadSourceForSellingList()
        {
            Dictionary<Guid, string> dict = UserInformationManager.Instance.GetValues(UserEntries.LeadSourceForSellingList);
            Dictionary<Guid, string>.Enumerator enumerator = dict.GetEnumerator();

            _leadSouceList.Items.Clear();

            while (enumerator.MoveNext())
            {
                ListViewItem lvi = new ListViewItem(enumerator.Current.Value);
                lvi.Tag = enumerator.Current.Key;

                _leadSouceList.Items.Add(lvi);
            }
        }

        void On_AddLeadSource(object sender, EventArgs e)
        {
            AddEntryForm amf = new AddEntryForm(UserEntries.LeadSourceForSellingList);
            amf.ShowDialog();
            if (true == amf.Valid)
            {
                UserInformationManager.Instance.AddEntry(UserEntries.LeadSourceForSellingList, amf.EntryName);        
            }

            InitLeadSourceForSellingList();
        }

        void On_AddOfficeStaff(object sender, EventArgs e)
        {
            AddEntryForm amf = new AddEntryForm(UserEntries.OfficeStaffList);
            amf.ShowDialog();
            if (true == amf.Valid)
            {
                UserInformationManager.Instance.AddEntry(UserEntries.OfficeStaffList, amf.EntryName);
            }

            InitOfficeStaffList();
        }

        void On_DeleteOfficeStaff(object sender, EventArgs e)
        {
            ListView.SelectedListViewItemCollection lviColl = _officeStaffList.SelectedItems;
            foreach (ListViewItem lvi in lviColl)
            {
                if (null != lvi)
                {
                    Guid id = (Guid)lvi.Tag;
                    string value = lvi.SubItems[0].Text;

                    if (DialogResult.Yes == MessageBox.Show("Are you sure you want to delete " + value + " from Office Staff list?", "Delete Entry from Office Staff list", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2))
                    {
                        UserInformationManager.Instance.DeleteEntry(UserEntries.OfficeStaffList, id);
                        InitOfficeStaffList();
                    }
                }
            }            
        }

        void On_DeleteLeadSource(object sender, EventArgs e)
        {
            ListView.SelectedListViewItemCollection lviColl = _leadSouceList.SelectedItems;
            foreach (ListViewItem lvi in lviColl)
            {
                if (null != lvi)
                {
                    Guid id = (Guid)lvi.Tag;
                    string value = lvi.SubItems[0].Text;

                    if (DialogResult.Yes == MessageBox.Show("Are you sure you want to delete " + value + " from Lead Source for Selling list?", "Delete Entry from Lead Source for Selling list", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2 ))
                    {
                        UserInformationManager.Instance.DeleteEntry(UserEntries.LeadSourceForSellingList, id);
                        InitLeadSourceForSellingList();
                    }
                }
            }            
        }

        void On_OfficeStaffCtxMenuStrip_Opening(object sender, CancelEventArgs e)
        {
            ContextMenuStrip cxms = (ContextMenuStrip)sender;
            bool stopHere = true;

            if (null == _officeStaffList.SelectedItems || 0 == _officeStaffList.SelectedItems.Count)
                EnableOfficeStaffCtxMenu(false);
        }

        void On_OfficeStaffList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if ((null == ((ListView)sender).SelectedIndices) || (0 == ((ListView)sender).SelectedIndices.Count))            
                EnableOfficeStaffCtxMenu(false);            
            else
            {
               EnableOfficeStaffCtxMenu(true);
               _updateEmailAddresses.Text = "Update Email Address for " + ((ListView)sender).SelectedItems[0].Text;
               _clearEmailAddresses.Text = "Clear Email Address for " + ((ListView)sender).SelectedItems[0].Text;
            }
        }

        void On_UpdateEmailAddresses_Click(object sender, EventArgs e)
        {
            Guid id = (Guid)_officeStaffList.SelectedItems[0].Tag;
            string emailAddresses = UserInformationManager.Instance.GetEmailAddresses(id);

            AddEntryForm addEntryForm = new AddEntryForm(UserEntries.EmailList);
            addEntryForm.EntryName = emailAddresses;
            addEntryForm.ShowDialog();

            
            if (addEntryForm.Valid)
            {
                //call sp for update email adrress                
                UserInformationManager.Instance.UpdateEntry(id, addEntryForm.EntryName);
                InitOfficeStaffList();
            }
        }

        void On_ClearEmailAddresses_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show("Are you sure you want to clear all email addresses assigned to the selected user?", "Confirm Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);

            if (DialogResult.Yes == dr)
            {
                //call sp for update email address with empty param for emails
                Guid id = (Guid)_officeStaffList.SelectedItems[0].Tag;
                UserInformationManager.Instance.UpdateEntry(id, "");
                InitOfficeStaffList();
            }
        }

        void EnableOfficeStaffCtxMenu(bool enabled)
        {
            _clearEmailAddresses.Enabled = _updateEmailAddresses.Enabled = _taskToEmailSetup.Enabled = enabled;
        }

        void On_TaskToEmailSetup_Click(object sender, EventArgs e)
        {
            Guid id = (Guid)_officeStaffList.SelectedItems[0].Tag;

            Task2EmailSetup task2EmailSetupForm = new Task2EmailSetup(id);
            task2EmailSetupForm.ShowDialog();
        }

        private void pbEmail1_MouseHover(object sender, EventArgs e)
        {
            var tt = new ToolTip();            
            tt.SetToolTip((PictureBox)sender, "Typically the email address you use for your real estate business and will have on your printed marketing material.");            
        }

        private void pbEmail2_MouseHover(object sender, EventArgs e)
        {
            var tt = new ToolTip();
            tt.SetToolTip((PictureBox)sender, "Typically used for email marketing only. This is the email you set up to use for Email Blast. It is HIGHLY recommended that you have this separate email for this purpose only.");
        }

        private void pbAdvertisingMedia_MouseHover(object sender, EventArgs e)
        {
            var tt = new ToolTip();
            tt.SetToolTip((PictureBox)sender, "You can add or remove marketing types so you can deep track your results of different marketing pieces. This will appear in the Deal Info tab of of the Contact View, helping you track where your leads come from.");
        }

        private void pbOfficeStaffList_MouseHover(object sender, EventArgs e)
        {
            var tt = new ToolTip();
            tt.SetToolTip((PictureBox)sender, "The Office Staff List is used so you can assign tasks to specific people and send email and text reminders to specific people in your organization.");
        }

        private void pbLeadsourceForSellingList_MouseHover(object sender, EventArgs e)
        {
            var tt = new ToolTip();
            tt.SetToolTip((PictureBox)sender, "This is used to keep track of where your buyer leads come from when selling a house. It is used in the Sell tab of the Contact View.");
        }
    }
}
