using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace DealMaker
{
	/// <summary>
	/// Summary description for AddMediaForm.
	/// </summary>
    public class AddEntryForm : System.Windows.Forms.Form
	{
        UserEntries _ue = UserEntries.NotSet;
		private bool bValid = false;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Button bOK;
		private System.Windows.Forms.TextBox tbEntryName;
		private System.Windows.Forms.Button bCancel;
        private Label _addInfo;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public bool Valid
		{
			get {return this.bValid;}
		}

		public string EntryName
		{
            get { return this.tbEntryName.Text; }
            set { this.tbEntryName.Text = value; }
		}


        public AddEntryForm(UserEntries ue)
		{
            _ue = ue;
			InitializeComponent();
            SetText(ue);
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.label1 = new System.Windows.Forms.Label();
            this.bOK = new System.Windows.Forms.Button();
            this.tbEntryName = new System.Windows.Forms.TextBox();
            this.bCancel = new System.Windows.Forms.Button();
            this._addInfo = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(11, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(70, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Media Name:";
            // 
            // bOK
            // 
            this.bOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.bOK.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.bOK.Location = new System.Drawing.Point(421, 64);
            this.bOK.Name = "bOK";
            this.bOK.Size = new System.Drawing.Size(75, 23);
            this.bOK.TabIndex = 1;
            this.bOK.Text = "OK";
            this.bOK.Click += new System.EventHandler(this.bOK_Click);
            // 
            // tbEntryName
            // 
            this.tbEntryName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tbEntryName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbEntryName.Location = new System.Drawing.Point(14, 32);
            this.tbEntryName.Name = "tbEntryName";
            this.tbEntryName.Size = new System.Drawing.Size(563, 20);
            this.tbEntryName.TabIndex = 0;
            this.tbEntryName.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbEntryName_KeyPress);
            // 
            // bCancel
            // 
            this.bCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.bCancel.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.bCancel.Location = new System.Drawing.Point(502, 64);
            this.bCancel.Name = "bCancel";
            this.bCancel.Size = new System.Drawing.Size(75, 23);
            this.bCancel.TabIndex = 3;
            this.bCancel.Text = "Cancel";
            this.bCancel.Click += new System.EventHandler(this.bCancel_Click);
            // 
            // _addInfo
            // 
            this._addInfo.AutoSize = true;
            this._addInfo.Location = new System.Drawing.Point(12, 67);
            this._addInfo.Name = "_addInfo";
            this._addInfo.Size = new System.Drawing.Size(0, 13);
            this._addInfo.TabIndex = 4;
            // 
            // AddEntryForm
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.ClientSize = new System.Drawing.Size(589, 99);
            this.Controls.Add(this._addInfo);
            this.Controls.Add(this.bCancel);
            this.Controls.Add(this.tbEntryName);
            this.Controls.Add(this.bOK);
            this.Controls.Add(this.label1);
            this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "AddEntryForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Add Media Form";
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		#endregion

        void SetText(UserEntries ue)
        {
            switch (ue)
            {
                case UserEntries.MediaList:
                    this.Text = "Add Media";
                    this.label1.Text = "Media Name:";
                    break;

                case UserEntries.LeadSourceForSellingList:
                    this.Text = "Add Lead Source for Selling";
                    this.label1.Text = "Lead Source for Selling Name:";
                    break;

                case UserEntries.OfficeStaffList:
                    this.Text = "Add Office Staff";
                    this.label1.Text = "Office Staff Name:";
                    break;

                case UserEntries.EmailList:
                    this.Text = "Add or update email addresses";
                    this.label1.Text = "Insert single or multiple email addresses separated with comma. (i.e. john@company.com, jane@company.com):";
                    this._addInfo.Text = "If you want to send via text to a cell phone these are common formats: " + Environment.NewLine + Environment.NewLine + "Verizon: number@vtext.com" + Environment.NewLine + "AT&T: number@txt.att.net" + Environment.NewLine + "Qwest: number@qwestmp.com" + Environment.NewLine + "T-Mobile: number@tmomail.net" + Environment.NewLine + "Sprint: number@messaging.sprintpcs.com or number@pm.sprint.com" + Environment.NewLine + "Metro PCS: number@mymetropcs.com";
                    this.Height = 250;
                    break;

                case UserEntries.NotSet:
                    this.Text = "### NOT SET ###";
                    this.label1.Text = "### NOT SET ###";
                    break;
            }
        }
        
        private void bOK_Click(object sender, System.EventArgs e)
		{
			if (string.Empty == this.tbEntryName.Text)
			{

                string text = string.Empty;
                switch (_ue)
                {
                    case UserEntries.MediaList:
                        text = "Add media name!";                        
                        break;

                    case UserEntries.LeadSourceForSellingList:
                        text = "Add Lead Source for Selling Name!";
                        break;

                    case UserEntries.OfficeStaffList:
                        text = "Add Office Staff Name!";
                        break;

                    case UserEntries.EmailList:
                        text = "Add Email List!" + Environment.NewLine + Environment.NewLine + "i.e.: john@company.com or john@company.com, jane@company.com";
                        break;  
                  
                    default:
                        break;                    
                }

                if (string.Empty != text)
                    MessageBox.Show(this, text, "Warning", MessageBoxButtons.OK, MessageBoxIcon.Information);

				return;
			}
			this.bValid = true;
			this.Close();
		}

        private void tbEntryName_KeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
		{
			if (e.KeyChar == 13)
			{
				this.bOK_Click(sender, null);
			}	
		}

		private void bCancel_Click(object sender, System.EventArgs e)
		{
			this.Close();
		}
	}
}
