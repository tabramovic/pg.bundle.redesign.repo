using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace DealMaker
{
	/// <summary>
	/// Summary description for DeleteContactsQuestionForm.
	/// </summary>
	public class DeleteContactsQuestionForm : System.Windows.Forms.Form
	{		
		private bool bDelete = false;
		private const string msgPrefix = "Do you want to delete the tagged contact(s) from ";
		private const string msgSuffix = " group?";
		private string groupName = "current";
		private System.Windows.Forms.Button bYes;
		private System.Windows.Forms.Button bNo;
		private System.Windows.Forms.CheckBox cbDeleteAll;
		private System.Windows.Forms.Label lMsg;
		private System.Windows.Forms.Label label1;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		private string UserMsg
		{
			get { return msgPrefix + groupName + msgSuffix; }			
		}

		public bool DeleteContacts 
		{
			get { return this.bDelete; }
		}

		public bool PermanentDelete
		{
			get { return this.cbDeleteAll.Checked; }
		}

		public DeleteContactsQuestionForm(string groupName)
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			this.groupName = groupName;
			this.lMsg.Text = this.UserMsg;

			this.bYes.Click += new EventHandler(Yes_EH);
			this.bNo.Click += new EventHandler(No_EH);			
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.bYes = new System.Windows.Forms.Button();
			this.bNo = new System.Windows.Forms.Button();
			this.cbDeleteAll = new System.Windows.Forms.CheckBox();
			this.lMsg = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// bYes
			// 
			this.bYes.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.bYes.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.bYes.Location = new System.Drawing.Point(312, 101);
			this.bYes.Name = "bYes";
			this.bYes.TabIndex = 0;
			this.bYes.Text = "Yes";
			// 
			// bNo
			// 
			this.bNo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.bNo.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.bNo.Location = new System.Drawing.Point(392, 101);
			this.bNo.Name = "bNo";
			this.bNo.TabIndex = 1;
			this.bNo.Text = "No";
			// 
			// cbDeleteAll
			// 
			this.cbDeleteAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.cbDeleteAll.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.cbDeleteAll.Location = new System.Drawing.Point(16, 64);
			this.cbDeleteAll.Name = "cbDeleteAll";
			this.cbDeleteAll.Size = new System.Drawing.Size(16, 24);
			this.cbDeleteAll.TabIndex = 2;
			// 
			// lMsg
			// 
			this.lMsg.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.lMsg.Location = new System.Drawing.Point(16, 16);
			this.lMsg.Name = "lMsg";
			this.lMsg.Size = new System.Drawing.Size(456, 32);
			this.lMsg.TabIndex = 3;
			this.lMsg.Text = "Question";
			// 
			// label1
			// 
			this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.label1.Location = new System.Drawing.Point(40, 68);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(432, 23);
			this.label1.TabIndex = 4;
			this.label1.Text = "Check if you want to delete from ALL groups and PERMANENTLY from the program";
			// 
			// DeleteContactsQuestionForm
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(210)), ((System.Byte)(216)), ((System.Byte)(240)));
			this.ClientSize = new System.Drawing.Size(482, 136);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.lMsg);
			this.Controls.Add(this.cbDeleteAll);
			this.Controls.Add(this.bNo);
			this.Controls.Add(this.bYes);
			this.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "DeleteContactsQuestionForm";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Delete Contacts";
			this.TopMost = true;
			this.ResumeLayout(false);

		}
		#endregion

		private void Yes_EH(object sender, EventArgs e)
		{
			this.bDelete = true;
			this.Close();
		}

		private void No_EH(object sender, EventArgs e)
		{			
			this.Close();
		}		
	}
}
