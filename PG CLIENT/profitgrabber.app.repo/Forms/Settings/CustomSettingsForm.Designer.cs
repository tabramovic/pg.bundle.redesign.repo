﻿namespace DealMaker
{
    partial class CustomSettingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._quickLaunch = new System.Windows.Forms.CheckBox();
            this._ok = new System.Windows.Forms.Button();
            this._customSSXSettings = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // _quickLaunch
            // 
            this._quickLaunch.AutoSize = true;
            this._quickLaunch.Location = new System.Drawing.Point(12, 12);
            this._quickLaunch.Name = "_quickLaunch";
            this._quickLaunch.Size = new System.Drawing.Size(129, 17);
            this._quickLaunch.TabIndex = 0;
            this._quickLaunch.Text = "Enable Quick Launch";
            this._quickLaunch.UseVisualStyleBackColor = true;
            // 
            // _ok
            // 
            this._ok.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._ok.Location = new System.Drawing.Point(186, 86);
            this._ok.Name = "_ok";
            this._ok.Size = new System.Drawing.Size(75, 23);
            this._ok.TabIndex = 1;
            this._ok.Text = "OK";
            this._ok.UseVisualStyleBackColor = true;
            // 
            // _customSSXSettings
            // 
            this._customSSXSettings.AutoSize = true;
            this._customSSXSettings.Location = new System.Drawing.Point(12, 35);
            this._customSSXSettings.Name = "_customSSXSettings";
            this._customSSXSettings.Size = new System.Drawing.Size(154, 17);
            this._customSSXSettings.TabIndex = 2;
            this._customSSXSettings.Text = "Custom Short Sale Settings";
            this._customSSXSettings.UseVisualStyleBackColor = true;
            // 
            // CustomSettingsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.ClientSize = new System.Drawing.Size(273, 121);
            this.Controls.Add(this._customSSXSettings);
            this.Controls.Add(this._ok);
            this.Controls.Add(this._quickLaunch);
            this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "CustomSettingsForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Custom Application Settings";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox _quickLaunch;
        private System.Windows.Forms.Button _ok;
        private System.Windows.Forms.CheckBox _customSSXSettings;
    }
}