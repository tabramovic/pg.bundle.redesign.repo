﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace DealMaker
{
    public partial class CustomSettingsForm : Form
    {
        CustomSettingsManager _csm = new CustomSettingsManager();

        public CustomSettingsForm()
        {
            InitializeComponent();

            AddEventHandlers();
            DisplayCustomSettings();
        }

        void AddEventHandlers()
        {
            _ok.Click += new EventHandler(On_Ok_Click);
        }

        void On_Ok_Click(object sender, EventArgs e)
        {
            SaveCustomSettings();
            this.Close();
        }

        void DisplayCustomSettings()
        {
            string error = string.Empty;

            CustomSettings cs = _csm.LoadCustomSettings(out error);

            if (null != cs)
            {
                _quickLaunch.Checked = (1 == cs.QuickStart ? true : false);
                _customSSXSettings.Checked = (1 == cs.SpecialSSX ? true : false);

            }

            if (null != error && string.Empty != error)
                MessageBox.Show(error, "Profitgrabber Pro", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        void SaveCustomSettings()
        {
            string error = string.Empty;
            CustomSettings cs = new CustomSettings();
            cs.QuickStart = (true == _quickLaunch.Checked ? (byte)1 : (byte)0);
            cs.SpecialSSX = (true == _customSSXSettings.Checked ? (byte)1 : (byte)0);

            if (!_csm.SaveCustomSettings(cs, out error))
                MessageBox.Show(error, "Profitgrabber Pro", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
    }
}
