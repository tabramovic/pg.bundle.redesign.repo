﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml.Serialization;

namespace DealMaker
{
    public class CustomSettingsManager
    {
        const string FileName = "CustomSettings.xml";
        public CustomSettings LoadCustomSettings(out string error)
        {
            error = string.Empty;

            if (!TestForFile())
                return null;

            StreamReader reader = null;

            CustomSettings cs;
            try	// Deserialize
            {
                XmlSerializer ser = new XmlSerializer(typeof(CustomSettings));
                reader = new StreamReader(OSFolderManager.Instance.GetPlainLocalUserAppDataPath() + @"\" + FileName);
                cs = (CustomSettings)ser.Deserialize(reader);
            }
            catch (Exception ex)
            {
                cs = null;
                error = ex.Message;
            }
            finally
            {
                if (reader != null)
                    reader.Close();
                reader = null;
            }
            return cs;
        }

        public bool SaveCustomSettings(CustomSettings cs, out string error)
        {            
			bool returnValue = true;
            error = string.Empty;
			StreamWriter writer = null;

            if (!TestForFolder())
                Directory.CreateDirectory(OSFolderManager.Instance.GetPlainLocalUserAppDataPath());

			try //Serialize
			{
				XmlSerializer ser = new XmlSerializer(typeof(CustomSettings));
                writer = new StreamWriter(OSFolderManager.Instance.GetPlainLocalUserAppDataPath() + @"\" + FileName);
				ser.Serialize(writer, cs);				
			} 
			catch (Exception ex)
			{
				returnValue = false;
                error = ex.Message;
			} 
			finally
			{
				if (writer != null) 
					writer.Close();
				writer = null;
			} 

			return returnValue;
        }

        bool TestForFolder()
        {
            if (!Directory.Exists(OSFolderManager.Instance.GetPlainLocalUserAppDataPath()))
                return false;

            return true;
        }

        bool TestForFile()
        {
            if (!TestForFolder())
                return false;
            if (!File.Exists(OSFolderManager.Instance.GetPlainLocalUserAppDataPath() + @"\" + FileName))
                return false;

            return true;
        }
    }
}
