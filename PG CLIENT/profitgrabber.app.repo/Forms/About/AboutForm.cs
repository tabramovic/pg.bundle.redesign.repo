using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Diagnostics;
using Microsoft.Win32;

using System.Reflection;

namespace DealMaker
{
	/// <summary>
	/// Summary description for AboutForm.
	/// </summary>
	public class AboutForm : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Button bOK;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.LinkLabel linkLabel1;
		private System.Windows.Forms.LinkLabel linkLabel2;
		private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label _licensedTo;
		private System.Windows.Forms.PictureBox pbAbout;
		private System.Windows.Forms.Label _pgLabel;
        private Label _serialNr;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public AboutForm()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AboutForm));
            this.bOK = new System.Windows.Forms.Button();
            this._pgLabel = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.linkLabel2 = new System.Windows.Forms.LinkLabel();
            this.label4 = new System.Windows.Forms.Label();
            this._licensedTo = new System.Windows.Forms.Label();
            this.pbAbout = new System.Windows.Forms.PictureBox();
            this._serialNr = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pbAbout)).BeginInit();
            this.SuspendLayout();
            // 
            // bOK
            // 
            this.bOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.bOK.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.bOK.Location = new System.Drawing.Point(644, 620);
            this.bOK.Name = "bOK";
            this.bOK.Size = new System.Drawing.Size(90, 27);
            this.bOK.TabIndex = 0;
            this.bOK.Text = "OK";
            this.bOK.Click += new System.EventHandler(this.bOK_Click);
            // 
            // _pgLabel
            // 
            this._pgLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._pgLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._pgLabel.Location = new System.Drawing.Point(12, 423);
            this._pgLabel.Name = "_pgLabel";
            this._pgLabel.Size = new System.Drawing.Size(720, 36);
            this._pgLabel.TabIndex = 2;
            this._pgLabel.Text = "ProfitGrabber Pro";
            this._pgLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(19, 498);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(360, 17);
            this.label2.TabIndex = 3;
            this.label2.Text = "Copyright Real Estate Money, LLC. All Rights Reserved.";
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(19, 600);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(144, 27);
            this.label3.TabIndex = 4;
            this.label3.Text = "For more info contact:  ";
            // 
            // linkLabel1
            // 
            this.linkLabel1.Location = new System.Drawing.Point(217, 600);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(182, 27);
            this.linkLabel1.TabIndex = 5;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "support@profitgrabber.com";
            // 
            // linkLabel2
            // 
            this.linkLabel2.Location = new System.Drawing.Point(217, 628);
            this.linkLabel2.Name = "linkLabel2";
            this.linkLabel2.Size = new System.Drawing.Size(211, 26);
            this.linkLabel2.TabIndex = 7;
            this.linkLabel2.TabStop = true;
            this.linkLabel2.Text = "http://www.markorubel.com";
            this.linkLabel2.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel2_LinkClicked);
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(19, 628);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(192, 26);
            this.label4.TabIndex = 6;
            this.label4.Text = "For Real Estate training go to:";
            // 
            // _licensedTo
            // 
            this._licensedTo.AutoSize = true;
            this._licensedTo.Location = new System.Drawing.Point(19, 525);
            this._licensedTo.Name = "_licensedTo";
            this._licensedTo.Size = new System.Drawing.Size(177, 17);
            this._licensedTo.TabIndex = 8;
            this._licensedTo.Text = "This product is licensed to:";
            // 
            // pbAbout
            // 
            this.pbAbout.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pbAbout.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.pbAbout.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.pbAbout.Image = ((System.Drawing.Image)(resources.GetObject("pbAbout.Image")));
            this.pbAbout.Location = new System.Drawing.Point(10, 12);
            this.pbAbout.Name = "pbAbout";
            this.pbAbout.Size = new System.Drawing.Size(722, 408);
            this.pbAbout.TabIndex = 1;
            this.pbAbout.TabStop = false;
            // 
            // _serialNr
            // 
            this._serialNr.AutoSize = true;
            this._serialNr.Location = new System.Drawing.Point(19, 552);
            this._serialNr.Name = "_serialNr";
            this._serialNr.Size = new System.Drawing.Size(100, 17);
            this._serialNr.TabIndex = 10;
            this._serialNr.Text = "Serial number:";
            // 
            // AboutForm
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(6, 15);
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.ClientSize = new System.Drawing.Size(744, 658);
            this.Controls.Add(this._serialNr);
            this.Controls.Add(this._licensedTo);
            this.Controls.Add(this.linkLabel2);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.linkLabel1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this._pgLabel);
            this.Controls.Add(this.pbAbout);
            this.Controls.Add(this.bOK);
            this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "AboutForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "ProfitGrabber Pro - Server 8";
            this.Load += new System.EventHandler(this.AboutForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pbAbout)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		#endregion

		private void AboutForm_Load(object sender, System.EventArgs e)
		{
			try
			{
				Assembly assembly = Assembly.GetExecutingAssembly(); 
				System.Reflection.AssemblyName assemblyName = assembly.GetName();
				Version version = assemblyName.Version;
				
                //if (!Globals.NwVerEnabled)
                //{
                //    _pgLabel.Text += ", single user version.";
                //}
                //else
                //{                    
                //    if (null != Globals.activeModules)
                //    {
                //        if (Globals.activeModules.MOD_6)
                //            _pgLabel.Text += " - Client";
                //        else
                //            _pgLabel.Text += " - Server";
                //    }
                //    else
                //        _pgLabel.Text += ".";
                //}

                _pgLabel.Text += " Server 8 (Version " + version.ToString() + ")";
			}
			catch{}


            try
            {
                string registeredUser = string.Empty;
                if (null != Globals.UserRegistration)
                {
                    if (string.Empty != Globals.UserRegistration.FirstName)
                        registeredUser += Globals.UserRegistration.FirstName;
                    if (string.Empty != Globals.UserRegistration.MiddleName)
                        registeredUser += " " + Globals.UserRegistration.MiddleName;
                    if (string.Empty != Globals.UserRegistration.LastName)
                        registeredUser += " " + Globals.UserRegistration.LastName;
                }
                _licensedTo.Text = "This product is licensed to " + registeredUser;
            }
            catch { }

            try
            {
                _serialNr.Text = "Serial number: " + Globals.RegistrationRegistration.KeyCode;
            }
            catch { _serialNr.Text = string.Empty; }
		}

		private void bOK_Click(object sender, System.EventArgs e)
		{
			this.Close();
		}

		private void linkLabel2_LinkClicked(object sender, System.Windows.Forms.LinkLabelLinkClickedEventArgs e)
		{
            Process.Start("http://www.profitgrabber.com/account/subscribe_renew.cfm?IDUser={" + Globals.UserRegistration.IDUser.ToString() + "}");            
		}        
	}
}
