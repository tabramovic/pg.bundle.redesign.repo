using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

using DealMaker.PlainClasses.Fax;

namespace DealMaker
{
	/// <summary>
	/// Summary description for SetupInterfaxAccountForm.
	/// </summary>
	public class SetupInterfaxAccountForm : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Button _save;
		private System.Windows.Forms.TextBox _login;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox _password;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Button _cancel;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.Button _getAccount;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public SetupInterfaxAccountForm()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			_cancel.Click += new EventHandler(On_Cancel);
			_save.Click += new EventHandler(On_Save);
			
			_getAccount.Click += new EventHandler(On_GetAccount_Click);
			this.Load += new EventHandler(On_Load);

		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this._save = new System.Windows.Forms.Button();
			this._login = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this._password = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this._cancel = new System.Windows.Forms.Button();
			this.label10 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.label7 = new System.Windows.Forms.Label();
			this._getAccount = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// _save
			// 
			this._save.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this._save.Location = new System.Drawing.Point(376, 200);
			this._save.Name = "_save";
			this._save.TabIndex = 5;
			this._save.Text = "Save";
			// 
			// _login
			// 
			this._login.Location = new System.Drawing.Point(104, 152);
			this._login.Name = "_login";
			this._login.Size = new System.Drawing.Size(88, 20);
			this._login.TabIndex = 2;
			this._login.Text = "";
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(40, 152);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(56, 23);
			this.label1.TabIndex = 2;
			this.label1.Text = "Login";
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(40, 176);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(56, 23);
			this.label2.TabIndex = 3;
			this.label2.Text = "Password";
			// 
			// _password
			// 
			this._password.Location = new System.Drawing.Point(104, 176);
			this._password.Name = "_password";
			this._password.Size = new System.Drawing.Size(88, 20);
			this._password.TabIndex = 3;
			this._password.Text = "";
			// 
			// label3
			// 
			this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label3.Location = new System.Drawing.Point(24, 128);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(160, 23);
			this.label3.TabIndex = 5;
			this.label3.Text = "Interfax Account Credentials";
			// 
			// _cancel
			// 
			this._cancel.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this._cancel.Location = new System.Drawing.Point(296, 200);
			this._cancel.Name = "_cancel";
			this._cancel.TabIndex = 4;
			this._cancel.Text = "Cancel";
			// 
			// label10
			// 
			this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label10.Location = new System.Drawing.Point(24, 8);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(128, 23);
			this.label10.TabIndex = 48;
			this.label10.Text = "Get Interfax Account:";
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(40, 32);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(264, 23);
			this.label4.TabIndex = 49;
			this.label4.Text = "Interfax Service is webbased faxing service.";
			// 
			// label5
			// 
			this.label5.Location = new System.Drawing.Point(40, 56);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(416, 32);
			this.label5.TabIndex = 50;
			this.label5.Text = "Your PG system can be integrated with Interfax, which is an online faxing service" +
				".";
			// 
			// label6
			// 
			this.label6.Location = new System.Drawing.Point(40, 72);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(360, 16);
			this.label6.TabIndex = 51;
			this.label6.Text = "This requires that you setup an account with Interfax first!";
			// 
			// label7
			// 
			this.label7.Location = new System.Drawing.Point(40, 96);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(208, 16);
			this.label7.TabIndex = 52;
			this.label7.Text = "Click on Get Account Button to proceed.";
			// 
			// _getAccount
			// 
			this._getAccount.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this._getAccount.Location = new System.Drawing.Point(248, 92);
			this._getAccount.Name = "_getAccount";
			this._getAccount.Size = new System.Drawing.Size(80, 23);
			this._getAccount.TabIndex = 53;
			this._getAccount.Text = "Get Account";
			// 
			// SetupInterfaxAccountForm
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(210)), ((System.Byte)(216)), ((System.Byte)(240)));
			this.ClientSize = new System.Drawing.Size(458, 232);
			this.Controls.Add(this._getAccount);
			this.Controls.Add(this.label7);
			this.Controls.Add(this.label6);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.label10);
			this.Controls.Add(this._cancel);
			this.Controls.Add(this.label3);
			this.Controls.Add(this._password);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Controls.Add(this._login);
			this.Controls.Add(this._save);
			this.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
			this.Name = "SetupInterfaxAccountForm";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Interfax Account Setup";
			this.ResumeLayout(false);

		}
		#endregion

		private void On_Cancel(object sender, EventArgs e)
		{
			this.Close();
		}

		private void On_Save(object sender, EventArgs e)
		{
			InterfaxWebServiceSupport.Instance.SaveIfxData(_login.Text, _password.Text);
			this.Close();
		}

		private void On_Load(object sender, EventArgs e)
		{
			string userName;
			string password;

			InterfaxWebServiceSupport.Instance.LoadIfxData(out userName, out password);
			if (null != userName && null != password)
			{
				_login.Text = userName;
				_password.Text = password;
			}

		}

		private void On_GetAccount_Click(object sender, EventArgs e)
		{
			System.Diagnostics.Process.Start("www.interfax.net");
		}
	}
}
