using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

using DealMaker.Faxing;
using DealMaker.PlainClasses.Fax;

namespace DealMaker.Forms.Fax
{
	/// <summary>
	/// Summary description for InterfaxConsoleWindow.
	/// </summary>
	public class InterfaxConsoleWindow : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Button _Close;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label _faxNumber;
		private System.Windows.Forms.Label _DocumentName;
		private System.Windows.Forms.Label _status;
		private System.Windows.Forms.ColumnHeader columnHeader1;
		private System.Windows.Forms.ColumnHeader columnHeader2;
		private System.Windows.Forms.ListView _failedList;
		private System.Windows.Forms.Button _tagAll;
		private System.Windows.Forms.Button _unTagAll;
		private System.Windows.Forms.Button _resend;
		private System.Windows.Forms.LinkLabel _interfaxLabel;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label _StatusLabel;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public InterfaxConsoleWindow()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			AddEventHandlers();			
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this._Close = new System.Windows.Forms.Button();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this._faxNumber = new System.Windows.Forms.Label();
			this._DocumentName = new System.Windows.Forms.Label();
			this._status = new System.Windows.Forms.Label();
			this._failedList = new System.Windows.Forms.ListView();
			this.columnHeader1 = new System.Windows.Forms.ColumnHeader();
			this.columnHeader2 = new System.Windows.Forms.ColumnHeader();
			this._tagAll = new System.Windows.Forms.Button();
			this._unTagAll = new System.Windows.Forms.Button();
			this._resend = new System.Windows.Forms.Button();
			this._interfaxLabel = new System.Windows.Forms.LinkLabel();
			this.label4 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this._StatusLabel = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// _Close
			// 
			this._Close.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this._Close.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this._Close.Location = new System.Drawing.Point(496, 8);
			this._Close.Name = "_Close";
			this._Close.TabIndex = 0;
			this._Close.Text = "Close";
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(16, 88);
			this.label1.Name = "label1";
			this.label1.TabIndex = 1;
			this.label1.Text = "Fax Number";
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(16, 112);
			this.label2.Name = "label2";
			this.label2.TabIndex = 2;
			this.label2.Text = "Document name";
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(16, 168);
			this.label3.Name = "label3";
			this.label3.TabIndex = 3;
			this.label3.Text = "Status";
			// 
			// _faxNumber
			// 
			this._faxNumber.Location = new System.Drawing.Point(120, 88);
			this._faxNumber.Name = "_faxNumber";
			this._faxNumber.Size = new System.Drawing.Size(376, 23);
			this._faxNumber.TabIndex = 4;
			// 
			// _DocumentName
			// 
			this._DocumentName.Location = new System.Drawing.Point(120, 112);
			this._DocumentName.Name = "_DocumentName";
			this._DocumentName.Size = new System.Drawing.Size(376, 48);
			this._DocumentName.TabIndex = 5;
			// 
			// _status
			// 
			this._status.Location = new System.Drawing.Point(120, 168);
			this._status.Name = "_status";
			this._status.Size = new System.Drawing.Size(376, 40);
			this._status.TabIndex = 6;
			// 
			// _failedList
			// 
			this._failedList.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this._failedList.CheckBoxes = true;
			this._failedList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
																						  this.columnHeader1,
																						  this.columnHeader2});
			this._failedList.FullRowSelect = true;
			this._failedList.GridLines = true;
			this._failedList.Location = new System.Drawing.Point(19, 250);
			this._failedList.MultiSelect = false;
			this._failedList.Name = "_failedList";
			this._failedList.Size = new System.Drawing.Size(552, 192);
			this._failedList.TabIndex = 7;
			this._failedList.View = System.Windows.Forms.View.Details;
			// 
			// columnHeader1
			// 
			this.columnHeader1.Text = "FaxNr";
			this.columnHeader1.Width = 97;
			// 
			// columnHeader2
			// 
			this.columnHeader2.Text = "Document Name";
			this.columnHeader2.Width = 432;
			// 
			// _tagAll
			// 
			this._tagAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this._tagAll.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this._tagAll.Location = new System.Drawing.Point(16, 450);
			this._tagAll.Name = "_tagAll";
			this._tagAll.TabIndex = 8;
			this._tagAll.Text = "Tag All";
			// 
			// _unTagAll
			// 
			this._unTagAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this._unTagAll.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this._unTagAll.Location = new System.Drawing.Point(104, 450);
			this._unTagAll.Name = "_unTagAll";
			this._unTagAll.TabIndex = 9;
			this._unTagAll.Text = "UnTag All";
			// 
			// _resend
			// 
			this._resend.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this._resend.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this._resend.Location = new System.Drawing.Point(475, 450);
			this._resend.Name = "_resend";
			this._resend.Size = new System.Drawing.Size(96, 23);
			this._resend.TabIndex = 10;
			this._resend.Text = "Resend Tagged";
			// 
			// _interfaxLabel
			// 
			this._interfaxLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this._interfaxLabel.Location = new System.Drawing.Point(16, 482);
			this._interfaxLabel.Name = "_interfaxLabel";
			this._interfaxLabel.TabIndex = 11;
			this._interfaxLabel.TabStop = true;
			this._interfaxLabel.Text = "www.interfax.net";
			this._interfaxLabel.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this._interfaxLabel_LinkClicked);
			// 
			// label4
			// 
			this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label4.Location = new System.Drawing.Point(8, 224);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(144, 23);
			this.label4.TabIndex = 12;
			this.label4.Text = "Documents failed to send";
			// 
			// label5
			// 
			this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label5.Location = new System.Drawing.Point(8, 48);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(480, 23);
			this.label5.TabIndex = 13;
			this.label5.Text = "Current Document";
			// 
			// _StatusLabel
			// 
			this._StatusLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this._StatusLabel.Location = new System.Drawing.Point(8, 8);
			this._StatusLabel.Name = "_StatusLabel";
			this._StatusLabel.Size = new System.Drawing.Size(480, 32);
			this._StatusLabel.TabIndex = 14;
			this._StatusLabel.Text = "Fax Documents are being created...";
			// 
			// InterfaxConsoleWindow
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(210)), ((System.Byte)(216)), ((System.Byte)(240)));
			this.ClientSize = new System.Drawing.Size(578, 512);
			this.Controls.Add(this._StatusLabel);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.label4);
			this.Controls.Add(this._interfaxLabel);
			this.Controls.Add(this._resend);
			this.Controls.Add(this._unTagAll);
			this.Controls.Add(this._tagAll);
			this.Controls.Add(this._failedList);
			this.Controls.Add(this._status);
			this.Controls.Add(this._DocumentName);
			this.Controls.Add(this._faxNumber);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Controls.Add(this._Close);
			this.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
			this.Name = "InterfaxConsoleWindow";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Interfax Console";
			this.ResumeLayout(false);

		}
		#endregion

		void AddEventHandlers()
		{
			InterfaxWebServiceSupport.Instance.IfxStarted += new DealMaker.PlainClasses.Fax.InterfaxWebServiceSupport.IfxStartEventHandler(On_IfxStarted);			
			InterfaxWebServiceSupport.Instance.IfxEnded += new DealMaker.PlainClasses.Fax.InterfaxWebServiceSupport.IfxEndEventHandler(On_IfxEnded);
			InterfaxWebServiceSupport.Instance.IfxStatus += new DealMaker.PlainClasses.Fax.InterfaxWebServiceSupport.IfxStatusEventHandler(On_IfxStatus);
			_Close.Click += new EventHandler(On_Close);
			_tagAll.Click += new EventHandler(On_TagAll);
			_unTagAll.Click += new EventHandler(On_UnTagAll);
			_resend.Click += new EventHandler(On_Resend);
		}

		void RemoveEventHandlers()
		{
			InterfaxWebServiceSupport.Instance.IfxStarted -= new DealMaker.PlainClasses.Fax.InterfaxWebServiceSupport.IfxStartEventHandler(On_IfxStarted);			
			InterfaxWebServiceSupport.Instance.IfxEnded -= new DealMaker.PlainClasses.Fax.InterfaxWebServiceSupport.IfxEndEventHandler(On_IfxEnded);
		}

		private void On_IfxStatus(string msg, bool valid)
		{
			UpdateStatusLabelAsync(msg, valid);
		}

		private void On_IfxStarted(string faxNumber, string fileName)
		{
			UpdateLablesAsync(faxNumber, fileName, true, "Started");
		}

		private void On_IfxEnded(string faxNumber, string fileName, bool success, string desc)
		{
			if (success)
				UpdateLablesAsync(faxNumber, fileName, true, "Ended Successfully!");
			else
				UpdateLablesAsync(faxNumber, fileName, false, "Error: " + desc);
		}

		delegate void UpdateStatusLabelAsyncDelegate(string msg, bool valid);
		void UpdateStatusLabelAsync(string msg, bool valid)
		{
			if (InvokeRequired)
				BeginInvoke(new UpdateStatusLabelAsyncDelegate(UpdateStatusLabelAsync), new object[] {msg, valid});
			else
			{
				if (!valid)
					_StatusLabel.ForeColor = Color.Red;
				else
					_StatusLabel.ForeColor = this.ForeColor;

				_StatusLabel.Text = msg;
			}

		}



		delegate void UpdateLablesAsyncDelegate(string faxNr, string fileName, bool succ, string status);
		void UpdateLablesAsync(string faxNr, string fileName, bool succ, string status)
		{
			if (InvokeRequired)
				BeginInvoke(new UpdateLablesAsyncDelegate(UpdateLablesAsync), new object[] {faxNr, fileName, succ, status });
			else
			{
				_faxNumber.Text = faxNr;
				_DocumentName.Text = fileName;
				_status.Text = status;
				
				if (!succ)
				{
					FaxItem fi = new FaxItem(fileName, fileName, string.Empty, faxNr);
					
					ListViewItem lvi = new ListViewItem(faxNr);
					string plainFileName = fileName.Substring(Application.StartupPath.Length, fileName.Length - Application.StartupPath.Length);
					lvi.SubItems.Add(plainFileName);					
					lvi.Tag = fi;
					_failedList.Items.Add(lvi);
				}

				Refresh();
			}
		}

		private void On_Close(object sender, EventArgs e)
		{
			RemoveEventHandlers();
			Close();
		}

		private void On_TagAll(object sender, EventArgs e)
		{
			foreach (ListViewItem lvi in _failedList.Items)
				lvi.Checked = true;
		}

		private void On_UnTagAll(object sender, EventArgs e)
		{
			foreach (ListViewItem lvi in _failedList.Items)
				lvi.Checked = false;
		}

		private void On_Resend(object sender, EventArgs e)
		{
			ArrayList alDocsToResend = new ArrayList();
			foreach (ListViewItem lvi in _failedList.Items)
			{
				if (lvi.Checked)
				{
					alDocsToResend.Add((FaxItem)lvi.Tag);
					lvi.Remove();
				}
			}

			InterfaxWebServiceSupport.Instance.SendFaxes(alDocsToResend);
		}

		private void _interfaxLabel_LinkClicked(object sender, System.Windows.Forms.LinkLabelLinkClickedEventArgs e)
		{
			System.Diagnostics.Process.Start("www.interfax.net");
		}

		
	}
}
