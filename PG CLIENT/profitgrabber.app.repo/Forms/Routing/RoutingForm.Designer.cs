﻿namespace DealMaker
{
    partial class RoutingForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this._wayPoints = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this._ctxMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.setAsStartAddressToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.setAsEndAddressToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this._startAddress = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this._startCity = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this._startState = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this._endState = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this._endCity = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this._endAddress = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this._startZIP = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this._endZIP = new System.Windows.Forms.TextBox();
            this._cancel = new System.Windows.Forms.Button();
            this._getRoute = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.useCompanyAddress = new System.Windows.Forms.Button();
            this._useStartAddressAsEndAddress = new System.Windows.Forms.Button();
            this._showWPsInFile = new System.Windows.Forms.CheckBox();
            this._ctxMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // _wayPoints
            // 
            this._wayPoints.BackColor = System.Drawing.SystemColors.Window;
            this._wayPoints.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._wayPoints.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4});
            this._wayPoints.ContextMenuStrip = this._ctxMenuStrip;
            this._wayPoints.FullRowSelect = true;
            this._wayPoints.GridLines = true;
            this._wayPoints.HideSelection = false;
            this._wayPoints.Location = new System.Drawing.Point(32, 147);
            this._wayPoints.Margin = new System.Windows.Forms.Padding(1);
            this._wayPoints.MultiSelect = false;
            this._wayPoints.Name = "_wayPoints";
            this._wayPoints.Size = new System.Drawing.Size(416, 170);
            this._wayPoints.TabIndex = 10;
            this._wayPoints.TabStop = false;
            this._wayPoints.UseCompatibleStateImageBehavior = false;
            this._wayPoints.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Address";
            this.columnHeader1.Width = 221;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "City";
            this.columnHeader2.Width = 90;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "State";
            this.columnHeader3.Width = 44;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "ZIP";
            this.columnHeader4.Width = 111;
            // 
            // _ctxMenuStrip
            // 
            this._ctxMenuStrip.ImageScalingSize = new System.Drawing.Size(40, 40);
            this._ctxMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.setAsStartAddressToolStripMenuItem,
            this.setAsEndAddressToolStripMenuItem});
            this._ctxMenuStrip.Name = "_ctxMenuStrip";
            this._ctxMenuStrip.Size = new System.Drawing.Size(177, 48);
            // 
            // setAsStartAddressToolStripMenuItem
            // 
            this.setAsStartAddressToolStripMenuItem.Name = "setAsStartAddressToolStripMenuItem";
            this.setAsStartAddressToolStripMenuItem.Size = new System.Drawing.Size(176, 22);
            this.setAsStartAddressToolStripMenuItem.Text = "Set as Start Address";
            this.setAsStartAddressToolStripMenuItem.Click += new System.EventHandler(this.setAsStartAddressToolStripMenuItem_Click);
            // 
            // setAsEndAddressToolStripMenuItem
            // 
            this.setAsEndAddressToolStripMenuItem.Name = "setAsEndAddressToolStripMenuItem";
            this.setAsEndAddressToolStripMenuItem.Size = new System.Drawing.Size(176, 22);
            this.setAsEndAddressToolStripMenuItem.Text = "Set as End Address";
            this.setAsEndAddressToolStripMenuItem.Click += new System.EventHandler(this.setAsEndAddressToolStripMenuItem_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 10);
            this.label1.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(266, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Start Address (type in or right click the desired Address)";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(10, 337);
            this.label2.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(263, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "End Address (type in or right click the desired Address)";
            // 
            // _startAddress
            // 
            this._startAddress.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._startAddress.Location = new System.Drawing.Point(32, 50);
            this._startAddress.Margin = new System.Windows.Forms.Padding(1);
            this._startAddress.Name = "_startAddress";
            this._startAddress.Size = new System.Drawing.Size(221, 20);
            this._startAddress.TabIndex = 3;
            this._startAddress.WordWrap = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(252, 31);
            this.label4.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(24, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "City";
            // 
            // _startCity
            // 
            this._startCity.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._startCity.Location = new System.Drawing.Point(254, 50);
            this._startCity.Margin = new System.Windows.Forms.Padding(1);
            this._startCity.Name = "_startCity";
            this._startCity.Size = new System.Drawing.Size(95, 20);
            this._startCity.TabIndex = 5;
            this._startCity.WordWrap = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(348, 31);
            this.label5.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(32, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "State";
            // 
            // _startState
            // 
            this._startState.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._startState.Location = new System.Drawing.Point(350, 50);
            this._startState.Margin = new System.Windows.Forms.Padding(1);
            this._startState.Name = "_startState";
            this._startState.Size = new System.Drawing.Size(39, 20);
            this._startState.TabIndex = 7;
            this._startState.WordWrap = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(345, 359);
            this.label6.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(32, 13);
            this.label6.TabIndex = 14;
            this.label6.Text = "State";
            // 
            // _endState
            // 
            this._endState.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._endState.Location = new System.Drawing.Point(350, 377);
            this._endState.Margin = new System.Windows.Forms.Padding(1);
            this._endState.Name = "_endState";
            this._endState.Size = new System.Drawing.Size(39, 20);
            this._endState.TabIndex = 16;
            this._endState.WordWrap = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(249, 359);
            this.label7.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(24, 13);
            this.label7.TabIndex = 12;
            this.label7.Text = "City";
            // 
            // _endCity
            // 
            this._endCity.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._endCity.Location = new System.Drawing.Point(254, 377);
            this._endCity.Margin = new System.Windows.Forms.Padding(1);
            this._endCity.Name = "_endCity";
            this._endCity.Size = new System.Drawing.Size(95, 20);
            this._endCity.TabIndex = 14;
            this._endCity.WordWrap = false;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(30, 359);
            this.label8.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(67, 13);
            this.label8.TabIndex = 10;
            this.label8.Text = "End Address";
            // 
            // _endAddress
            // 
            this._endAddress.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._endAddress.Location = new System.Drawing.Point(32, 377);
            this._endAddress.Margin = new System.Windows.Forms.Padding(1);
            this._endAddress.Name = "_endAddress";
            this._endAddress.Size = new System.Drawing.Size(221, 20);
            this._endAddress.TabIndex = 12;
            this._endAddress.WordWrap = false;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(9, 126);
            this.label9.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(57, 13);
            this.label9.TabIndex = 15;
            this.label9.Text = "Waypoints";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(388, 31);
            this.label10.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(24, 13);
            this.label10.TabIndex = 17;
            this.label10.Text = "ZIP";
            // 
            // _startZIP
            // 
            this._startZIP.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._startZIP.Location = new System.Drawing.Point(390, 50);
            this._startZIP.Margin = new System.Windows.Forms.Padding(1);
            this._startZIP.Name = "_startZIP";
            this._startZIP.Size = new System.Drawing.Size(58, 20);
            this._startZIP.TabIndex = 8;
            this._startZIP.WordWrap = false;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(385, 359);
            this.label11.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(24, 13);
            this.label11.TabIndex = 19;
            this.label11.Text = "ZIP";
            // 
            // _endZIP
            // 
            this._endZIP.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._endZIP.Location = new System.Drawing.Point(390, 377);
            this._endZIP.Margin = new System.Windows.Forms.Padding(1);
            this._endZIP.Name = "_endZIP";
            this._endZIP.Size = new System.Drawing.Size(58, 20);
            this._endZIP.TabIndex = 18;
            this._endZIP.WordWrap = false;
            // 
            // _cancel
            // 
            this._cancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this._cancel.Location = new System.Drawing.Point(32, 465);
            this._cancel.Margin = new System.Windows.Forms.Padding(1);
            this._cancel.Name = "_cancel";
            this._cancel.Size = new System.Drawing.Size(66, 21);
            this._cancel.TabIndex = 22;
            this._cancel.Text = "Cancel";
            this._cancel.UseVisualStyleBackColor = true;
            // 
            // _getRoute
            // 
            this._getRoute.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._getRoute.Location = new System.Drawing.Point(313, 465);
            this._getRoute.Margin = new System.Windows.Forms.Padding(1);
            this._getRoute.Name = "_getRoute";
            this._getRoute.Size = new System.Drawing.Size(135, 21);
            this._getRoute.TabIndex = 24;
            this._getRoute.Text = "Get Route";
            this._getRoute.UseVisualStyleBackColor = true;
            this._getRoute.Click += new System.EventHandler(this._getRoute_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(30, 31);
            this.label3.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(70, 13);
            this.label3.TabIndex = 22;
            this.label3.Text = "Start Address";
            // 
            // useCompanyAddress
            // 
            this.useCompanyAddress.AutoSize = true;
            this.useCompanyAddress.Location = new System.Drawing.Point(32, 81);
            this.useCompanyAddress.Margin = new System.Windows.Forms.Padding(1);
            this.useCompanyAddress.Name = "useCompanyAddress";
            this.useCompanyAddress.Size = new System.Drawing.Size(221, 23);
            this.useCompanyAddress.TabIndex = 9;
            this.useCompanyAddress.Text = "Use Company Address as Starting Address";
            this.useCompanyAddress.UseVisualStyleBackColor = true;
            this.useCompanyAddress.Click += new System.EventHandler(this.useCompanyAddress_Click);
            // 
            // _useStartAddressAsEndAddress
            // 
            this._useStartAddressAsEndAddress.AutoSize = true;
            this._useStartAddressAsEndAddress.Location = new System.Drawing.Point(32, 413);
            this._useStartAddressAsEndAddress.Margin = new System.Windows.Forms.Padding(1);
            this._useStartAddressAsEndAddress.Name = "_useStartAddressAsEndAddress";
            this._useStartAddressAsEndAddress.Size = new System.Drawing.Size(221, 23);
            this._useStartAddressAsEndAddress.TabIndex = 20;
            this._useStartAddressAsEndAddress.Text = "Use Start Address as End Address";
            this._useStartAddressAsEndAddress.UseVisualStyleBackColor = true;
            this._useStartAddressAsEndAddress.Click += new System.EventHandler(this._useStartAddressAsEndAddress_Click);
            // 
            // _showWPsInFile
            // 
            this._showWPsInFile.AutoSize = true;
            this._showWPsInFile.Location = new System.Drawing.Point(313, 442);
            this._showWPsInFile.Name = "_showWPsInFile";
            this._showWPsInFile.Size = new System.Drawing.Size(135, 17);
            this._showWPsInFile.TabIndex = 25;
            this._showWPsInFile.Text = "Show Addresses in File";
            this._showWPsInFile.UseVisualStyleBackColor = true;
            // 
            // RoutingForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.ClientSize = new System.Drawing.Size(466, 496);
            this.Controls.Add(this._showWPsInFile);
            this.Controls.Add(this._useStartAddressAsEndAddress);
            this.Controls.Add(this.useCompanyAddress);
            this.Controls.Add(this.label3);
            this.Controls.Add(this._getRoute);
            this.Controls.Add(this._cancel);
            this.Controls.Add(this.label11);
            this.Controls.Add(this._endZIP);
            this.Controls.Add(this.label10);
            this.Controls.Add(this._startZIP);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label6);
            this.Controls.Add(this._endState);
            this.Controls.Add(this.label7);
            this.Controls.Add(this._endCity);
            this.Controls.Add(this.label8);
            this.Controls.Add(this._endAddress);
            this.Controls.Add(this.label5);
            this.Controls.Add(this._startState);
            this.Controls.Add(this.label4);
            this.Controls.Add(this._startCity);
            this.Controls.Add(this._startAddress);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this._wayPoints);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Margin = new System.Windows.Forms.Padding(1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "RoutingForm";
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Enter Start / End Points";
            this._ctxMenuStrip.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView _wayPoints;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox _startAddress;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox _startCity;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox _startState;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox _endState;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox _endCity;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox _endAddress;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox _startZIP;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox _endZIP;
        private System.Windows.Forms.Button _cancel;
        private System.Windows.Forms.Button _getRoute;
        private System.Windows.Forms.ContextMenuStrip _ctxMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem setAsStartAddressToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem setAsEndAddressToolStripMenuItem;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button useCompanyAddress;
        private System.Windows.Forms.Button _useStartAddressAsEndAddress;
        private System.Windows.Forms.CheckBox _showWPsInFile;
    }
}