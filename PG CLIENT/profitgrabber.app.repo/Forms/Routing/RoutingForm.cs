﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DealMaker.MapServiceReference;
using System.Diagnostics;
using System.Net;
using System.Collections.Specialized;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;

namespace DealMaker
{
    public partial class RoutingForm : Form
    {
        string APIKEY = "AIzaSyD0vKd-BPqEVg__NU-OXjrt6TZctUFbtf8";
        string APIKEY_OFFICIAL = "AIzaSyBTrMl9pm8SpuODHQ87KSpCy3HBS7LsjV0";

        ArrayList waypoints = new ArrayList();
        const int Start = 0;
        const int End = 1;
        const int NotSet = -1;
        bool newVersion = false;

        public RoutingForm(ArrayList taggedItems, bool newVersion)
        {
            InitializeComponent();

            waypoints = taggedItems;
            this.newVersion = newVersion;
        }        

        protected override void OnLoad(EventArgs e)
        {
            ShowWayPoints();
            base.OnLoad(e);
        }

        void ShowWayPoints()
        {
            if (!newVersion)
            {
                foreach (ListViewItem lvi in waypoints)
                {
                    ListViewItem item = new ListViewItem(lvi.SubItems[2].Text);
                    item.SubItems.Add(lvi.SubItems[3].Text.Trim());
                    item.SubItems.Add(lvi.SubItems[4].Text.Trim());
                    item.SubItems.Add(lvi.SubItems[6].Text.Trim());
                    item.Tag = NotSet;

                    _wayPoints.Items.Add(item);
                }
            }
            else
            {
                foreach (ListViewItem lvi in waypoints)
                {
                    ListViewItem item = new ListViewItem(lvi.SubItems[4].Text);
                    item.SubItems.Add(lvi.SubItems[5].Text.Trim());
                    item.SubItems.Add(lvi.SubItems[6].Text.Trim());
                    item.SubItems.Add(lvi.SubItems[3].Text.Trim());
                    item.Tag = NotSet;

                    _wayPoints.Items.Add(item);
                }
            }
        }

        void SetStartAsNotSet()
        {
            foreach (ListViewItem lvi in _wayPoints.Items)
                if ((int)lvi.Tag == Start)
                    lvi.Tag = NotSet;
        }

        void SetEndAsNotSet()
        {
            foreach (ListViewItem lvi in _wayPoints.Items)
                if ((int)lvi.Tag == End)
                    lvi.Tag = NotSet;
        }

        private void setAsStartAddressToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SetStartAsNotSet();

            if (null != _wayPoints.SelectedItems && 1 == _wayPoints.SelectedItems.Count)
            {
                _wayPoints.SelectedItems[0].Tag = Start;

                _startAddress.Text = _wayPoints.SelectedItems[0].Text;
                _startCity.Text = _wayPoints.SelectedItems[0].SubItems[1].Text;
                _startState.Text = _wayPoints.SelectedItems[0].SubItems[2].Text;
                _startZIP.Text = _wayPoints.SelectedItems[0].SubItems[3].Text;
            }
            else
                MessageBox.Show("Select Waypoint as starting Address", Globals.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void setAsEndAddressToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SetEndAsNotSet();

            if (null != _wayPoints.SelectedItems && 1 == _wayPoints.SelectedItems.Count)
            {
                _wayPoints.SelectedItems[0].Tag = End;

                _endAddress.Text = _wayPoints.SelectedItems[0].Text;
                _endCity.Text = _wayPoints.SelectedItems[0].SubItems[1].Text;
                _endState.Text = _wayPoints.SelectedItems[0].SubItems[2].Text;
                _endZIP.Text = _wayPoints.SelectedItems[0].SubItems[3].Text;
            }
            else
                MessageBox.Show("Select Waypoint as ending Address", Globals.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void GoogleMapWaypoints()
        {
            try
            {
                
                var lIdx = 1;

                System.Collections.Generic.List<string> locations = new System.Collections.Generic.List<string>();
                foreach (ListViewItem lvi in _wayPoints.Items)
                {
                    if (Start != (int)lvi.Tag && End != (int)lvi.Tag)
                    {
                        string location = string.Format("{0} {1}, {2} {3}", lvi.SubItems[0].Text, lvi.SubItems[1].Text, lvi.SubItems[2].Text, lvi.SubItems[3].Text);
                        locations.Add($"wp{lIdx}={Uri.EscapeDataString(location)}");
                        lIdx++;
                    }
                }

                var startLocation = string.Format("{0} {1}, {2} {3}", _startAddress.Text, _startCity.Text, _startState.Text, _startZIP.Text);                
                var endLocation = string.Format("{0} {1}, {2} {3}", _endAddress.Text, _endCity.Text, _endState.Text, _endZIP.Text);
                

                var payLoad = string.Empty;
                var start = $"start={Uri.EscapeDataString(startLocation)}";
                var end = $"end={Uri.EscapeDataString(endLocation)}";
                var waypoints = string.Join("&", locations);


                Process.Start($"https://www.profitgrabber.com/DoorKnocker/DoorKnockingModule.html?{start}&{end}&{waypoints}");                
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }
        }

        //private void _getRoutes_Click(object sender, EventArgs e)
        //{
        //    GoogleMapWaypoints_V2(null);
        //    return;
        //}

        private void _getRoute_Click(object sender, EventArgs e)
        {
            MessageBox.Show($"NOTE: Google Maps limitation is 10 addresses per map.{Environment.NewLine}{Environment.NewLine}Therefore ProfitGrabber will open MULTIPLE WINDOWS.{Environment.NewLine}{Environment.NewLine}You can send the routing info to your phone for each window separately.", "ProfitGrabber Pro", MessageBoxButtons.OK, MessageBoxIcon.Information);
            GoogleMapWaypoints_V2(10, _showWPsInFile.Checked);
            return;


            //MapServiceClient msc = new MapServiceClient();
            try
            {
                //string testIfAlive = msc.TestIfAlive();

                //MessageBox.Show(string.Format("testIfAlive: {0}", testIfAlive));

                System.Collections.Generic.List<string> locations = new System.Collections.Generic.List<string>();
                foreach (ListViewItem lvi in _wayPoints.Items)
                {
                    if (Start != (int)lvi.Tag && End != (int)lvi.Tag)
                    {
                        string location = string.Format("{0} {1}, {2} {3}", lvi.SubItems[0].Text, lvi.SubItems[1].Text, lvi.SubItems[2].Text, lvi.SubItems[3].Text);
                        locations.Add(location);
                    }                    
                }

                string startLocation = string.Format("{0} {1}, {2} {3}", _startAddress.Text, _startCity.Text, _startState.Text, _startZIP.Text);
                locations.Insert(0, startLocation);

                string endLocation = string.Format("{0} {1}, {2} {3}", _endAddress.Text, _endCity.Text, _endState.Text, _endZIP.Text);
                locations.Add(endLocation);

                string requestId = Guid.NewGuid().ToString();

                using (var wb = new WebClient())
                {
                    var data = new NameValueCollection();
                    data["keyCode"] = Globals.RegistrationRegistration.KeyCode;
                    data["id"] = requestId;
                    data["locations"] = JsonConvert.SerializeObject(locations);
                                        
                    string url = "https://www.profitgrabber.com/DoorKnockingModule/api/route/getdirections";

                    var response = wb.UploadValues(url, "POST", data);
                    string responseInString = Encoding.UTF8.GetString(response);                    
                }


                Process p = new Process();
                p.StartInfo.FileName = DefaultBrowserSupport.getDefaultBrowser();
                p.StartInfo.Arguments = "https://www.profitgrabber.com/WS-RoutingService/optimized.html?id=" + requestId;
                p.Start();
                
                //string res = msc.GetDirections(Globals.RegistrationRegistration.KeyCode, requestId, locations.ToArray());
                
                bool stopHere = true;
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }
        }

        private string PurgeAddress(string address)
        {
            address = address.Replace("#", "APT");
            //remove non-letter, non-digit characters and non-space characters
            char[] ldLocationArray = address.Where(c => (char.IsLetterOrDigit(c) || char.IsWhiteSpace(c) || c == ',' || c == '.' )).ToArray();
            return new string(ldLocationArray);
        }
        
        private void GoogleMapWaypoints_V2(int chunkSize, bool saveWayPointsToFile)
        {
            try
            {
                var unescapedLocations = new List<string>();
                var locations = new List<string>();
                foreach (ListViewItem lvi in _wayPoints.Items)
                {
                    if (Start != (int)lvi.Tag && End != (int)lvi.Tag)
                    {
                        string location = string.Format("{0} {1}, {2} {3}", lvi.SubItems[0].Text, lvi.SubItems[1].Text, lvi.SubItems[2].Text, lvi.SubItems[3].Text);

                        location = PurgeAddress(location);
                        unescapedLocations.Add(location);
                        locations.Add($"{Uri.EscapeUriString(location)}");                        
                    }
                }

                var startLocation = string.Format("{0} {1}, {2} {3}", _startAddress.Text, _startCity.Text, _startState.Text, _startZIP.Text);
                var endLocation = string.Format("{0} {1}, {2} {3}", _endAddress.Text, _endCity.Text, _endState.Text, _endZIP.Text);


                var payLoad = string.Empty;
                var origin = $"origin={Uri.EscapeUriString(PurgeAddress(startLocation))}";
                var destination = $"destination={Uri.EscapeUriString(PurgeAddress(endLocation))}";
                var waypoints = "waypoints=optimize:true|" + string.Join("|", locations);


                //var url = $"https://maps.googleapis.com/maps/api/directions/json?{origin}&{destination}&{waypoints}&key={APIKEY_OFFICIAL}";
                var url = $"https://maps.googleapis.com/maps/api/directions/json?key={APIKEY_OFFICIAL}&{origin}&{destination}&{waypoints}";

                try
                {
                    WebRequest request = (HttpWebRequest)WebRequest.Create(url);
                    WebResponse response = request.GetResponse();
                    Stream data = response.GetResponseStream();
                    StreamReader reader = new StreamReader(data);

                    string responseFromSrv = reader.ReadToEnd();
                    
                    response.Close();

                    dynamic obj = JsonConvert.DeserializeObject<object>(responseFromSrv);

                    var name = ((Newtonsoft.Json.Linq.JProperty)((Newtonsoft.Json.Linq.JContainer)((Newtonsoft.Json.Linq.JContainer)((Newtonsoft.Json.Linq.JContainer)((Newtonsoft.Json.Linq.JContainer)obj).First.Next).First).First).Last).Name;
                    if ("waypoint_order" == name)
                    {
                        var wpList = new List<int>();
                        var waypointsOrder = ((Newtonsoft.Json.Linq.JProperty)((Newtonsoft.Json.Linq.JContainer)((Newtonsoft.Json.Linq.JContainer)((Newtonsoft.Json.Linq.JContainer)((Newtonsoft.Json.Linq.JContainer)obj).First.Next).First).First).Last).Value;
                        for (int i = 0; i < waypointsOrder.Count(); i++)
                        {
                            var val = ((long)((Newtonsoft.Json.Linq.JValue)(waypointsOrder[i])).Value);                            
                            wpList.Add((int)val);                            
                        }
                        
                        var sortedwps = new List<string>();
                        wpList.ForEach(o => sortedwps.Add(unescapedLocations[o]));

                        sortedwps.Insert(0, startLocation);
                        sortedwps.Add(endLocation);

                        var unresolvedAddresses = unescapedLocations.Where(l => !sortedwps.Contains(l)).ToList();
                        if (null != unresolvedAddresses && unresolvedAddresses.Any())
                        {
                            var unresolvedAddressesMsg = $"Following addresses could not get resolved:{Environment.NewLine}{string.Join(Environment.NewLine, unresolvedAddresses)}";
                            MessageBox.Show(unresolvedAddressesMsg, "Warning", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }

                        if (saveWayPointsToFile)
                        {
                            if (!Directory.Exists($"{Application.StartupPath}{Globals.ExportedRoutes}"))
                                Directory.CreateDirectory($"{Application.StartupPath}{Globals.ExportedRoutes}");

                            var fileHasEntries = false;
                            var fileName = $"{Application.StartupPath}{Globals.ExportedRoutes}{DateTime.Now.ToString("yyyy-MM-dd hh-mm-ss")}.txt";
                            using (var sw = new StreamWriter(fileName))
                            {
                                int cnt = 1;
                                foreach (var wp in sortedwps)
                                {
                                    sw.WriteLine($"{cnt++}. {wp}");
                                    fileHasEntries = true;
                                }
                            }

                            if (fileHasEntries)
                            {
                                var p = new Process();
                                p.StartInfo.FileName = "notepad.exe";
                                p.StartInfo.Arguments = fileName;
                                p.Start();
                            }
                        }

                        var numberOfUniqueLocations = sortedwps.Count();
                                                                        
                        var chunks = GetChunks(numberOfUniqueLocations, chunkSize);
                        
                        var pcs = new List<Process>();
                        foreach (var kvp in chunks)
                        {
                            origin = $"origin={Uri.EscapeUriString(sortedwps[kvp.Key])}"; 
                            destination = $"destination={Uri.EscapeUriString(sortedwps[kvp.Value])}"; 
                            var wpListAsString = string.Join("%7c", sortedwps.GetRange(kvp.Key + 1, kvp.Value - kvp.Key - 1).ToArray());

                            //Process.Start($"https://www.google.com/maps/dir/?api=1&{origin}&{destination}&travelmode=driving&waypoints={wpListAsString}");

                            var p = new Process();
                            p.StartInfo.FileName = DefaultBrowserSupport.getDefaultBrowser();
                            p.StartInfo.Arguments = $"https://www.google.com/maps/dir/?api=1&{origin}&{destination}&travelmode=driving&waypoints={wpListAsString}";
                            pcs.Add(p);
                        }

                        pcs.ForEach(_ => { _.Start(); System.Threading.Thread.Sleep(500); });
                        

                    }
                }
                catch (Exception e)
                {
                    MessageBox.Show("Google Maps are not available at the moment", "ProfitGrabber Pro", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    //MessageBox.Show($"{DefaultBrowserSupport.getDefaultBrowser()}{Environment.NewLine}{e.Message}{Environment.NewLine}{e.InnerException}{Environment.NewLine}{e.StackTrace}");
                }                
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }
        }

        private void useCompanyAddress_Click(object sender, EventArgs e)
        {
            User u = ApplicationHelper.LoadUser();

            if (null == u)
                return;

            _startAddress.Text = !string.IsNullOrEmpty(u.Address) ? u.Address : string.Empty;
            _startCity.Text = !string.IsNullOrEmpty(u.City) ? u.City : string.Empty;
            _startState.Text = !string.IsNullOrEmpty(u.State) ? u.State : string.Empty;
            _startZIP.Text = !string.IsNullOrEmpty(u.ZIP) ? u.ZIP : string.Empty;
        }

        private List<KeyValuePair<int, int>> GetChunks(int length, int chunkSize)
        {
            var chunks = new List<KeyValuePair<int, int>>();

            
            var run = 0;
            var iCnt = 0;
            var rangeLists = new List<List<int>>();
            var range = new List<int>();
            var idxList = new List<int>();

            int chunkSizeCnt = 0;
            while (iCnt < length)
            {
                if (0 != chunkSizeCnt && 0 == chunkSizeCnt % chunkSize)
                {
                    var last = idxList[idxList.Count - 1];
                    idxList.Add(last);

                    chunkSizeCnt = 1;
                }

                idxList.Add(iCnt);

                chunkSizeCnt++;
                iCnt++;
            }


            for (int i = 0; i < Math.Ceiling((decimal)idxList.Count / chunkSize); i++)
            {
                var lowerIdx = i * chunkSize;
                var upperIdx = lowerIdx + chunkSize - 1 > idxList.Count - 1
                                    ? idxList.Count - 1
                                    : lowerIdx + chunkSize - 1;

                chunks.Add(new KeyValuePair<int, int>(idxList[lowerIdx], idxList[upperIdx]));
            }
                
                
                //var nrOfChunks = (int)Math.Ceiling((decimal)length / chunkSize.Value);

                //int start = 0, end = 0;
                //for (int i = 0; i < nrOfChunks; i++)
                //{
                //    start = end;
                //    end += chunkSize.Value - 1;

                //    end = (end > length - 1) ? length - 1 : end;
                //    chunks.Add(new KeyValuePair<int, int>(start, end));
                //}
            

            return chunks;
        }

        private void _useStartAddressAsEndAddress_Click(object sender, EventArgs e)
        {
            _endAddress.Text = _startAddress.Text;
            _endCity.Text = _startCity.Text;
            _endState.Text = _startState.Text;
            _endZIP.Text = _startZIP.Text;
        }        
    }    
}
