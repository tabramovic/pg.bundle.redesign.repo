using System;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace DealMaker.Forms.Floaters
{
    public class ActivitiesHistory : FloaterBaseForm
    {
        
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.label7 = new System.Windows.Forms.Label();
			this.label8 = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(21, 21);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(416, 38);
			this.label1.TabIndex = 0;
			this.label1.Text = "The Activities History tab shows all the tasks that had been assigned to that con" +
				"tact. ";
			// 
			// label2
			// 
			this.label2.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(192)), ((System.Byte)(0)), ((System.Byte)(0)));
			this.label2.Location = new System.Drawing.Point(22, 56);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(424, 57);
			this.label2.TabIndex = 1;
			this.label2.Text = @"The Task List will show tasks that have been assigned to the contact both individually (from this tab), or have been assigned to the contact via task that was assigned to a group the contact is a member of.  For each Task you can see the event date, days from the event date, and task description. ";
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(21, 296);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(425, 77);
			this.label3.TabIndex = 2;
			this.label3.Text = @"The HISTORY LIST will list all the past tasks, with the status, task type, done date, and task description. You can ADD a past task to that list to make a note of it even if that task was never scheduled for that contact. For example, you ordered the payoff for one of the loans. Now you can make a note of that using �Record Past Action� button.";
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(21, 360);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(425, 49);
			this.label4.TabIndex = 3;
			this.label4.Text = "You can DOUBLE CLICK on a task and the detail window will open. From that window " +
				"you can update or edit that task. For the past tasks, the detail window is graye" +
				"d out, and the changes are not allowed.";
			// 
			// label5
			// 
			this.label5.Location = new System.Drawing.Point(21, 408);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(358, 27);
			this.label5.TabIndex = 4;
			this.label5.Text = "You can change the column width by putting your cursor on the header border and m" +
				"oving it.";
			// 
			// label6
			// 
			this.label6.Location = new System.Drawing.Point(23, 120);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(423, 59);
			this.label6.TabIndex = 5;
			this.label6.Text = @">> The EVENT DATE is a date that the task was scheduled, and the task campaign starts counting from that date on. In most cases, the Event Date is a date when the seller called in, or when a notice of default was filled, or when your mailing campaign has started, etc.";
			// 
			// label7
			// 
			this.label7.ForeColor = System.Drawing.Color.Green;
			this.label7.Location = new System.Drawing.Point(23, 176);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(423, 63);
			this.label7.TabIndex = 6;
			this.label7.Text = @">> What are Tasks or Activities?   TASKS are things you want to do for a contact or a group to find, initiate, or complete a deal. Assigning a task creates a reminder for you to complete the task and provides a way to track what has been done and what needs to be done for a contact or a group. ";
			// 
			// label8
			// 
			this.label8.ForeColor = System.Drawing.Color.Green;
			this.label8.Location = new System.Drawing.Point(22, 232);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(415, 54);
			this.label8.TabIndex = 7;
			this.label8.Text = @"An ACTIVITY is a series of tasks. The advantage of assigning an activity to simply assigning tasks, is that you can save the Activity and reuse it later for more contacts and groups. This is a huge time saver and makes it very difficult to miss a task because you will be using time tested and proven series of tasks.";
			// 
			// ActivitiesHistory
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.BackColor = System.Drawing.Color.LightYellow;
			this.ClientSize = new System.Drawing.Size(466, 450);
			this.Controls.Add(this.label8);
			this.Controls.Add(this.label7);
			this.Controls.Add(this.label6);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.Name = "ActivitiesHistory";
			this.Opacity = 0.95;
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
			this.Text = "ActivitiesHistory";
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.Label label8;

		public ActivitiesHistory()
        {
            InitializeComponent();
			this.Opacity = 0;			
        }

		protected override void OnPaint(PaintEventArgs e)
		{
			GfxDrawer.DrawCurveFromOffsets(3, 130, 3, 3, 5, 3, "INTERACTIVE HELP", 2, 5, this, e.Graphics, Brushes.LightYellow, Pens.Black);
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}
    }
}