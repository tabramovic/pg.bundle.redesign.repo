using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using DealMaker;

namespace DealMaker.Forms.Floaters
{
	/// <summary>
	/// Summary description for SelectGroup.
	/// </summary>
	public class SelectGroup : FloaterBaseForm
	{
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label6;
        private Label label7;
        private Label label8;
        private Label label9;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public SelectGroup()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			this.Opacity = 0;			
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		protected override void OnPaint(PaintEventArgs e)
		{			
			GfxDrawer.DrawCurveFromOffsets(3, 130, 3, 3, 5, 3, "INTERACTIVE HELP", 2, 5, this, e.Graphics, Brushes.LightYellow, Pens.Black);
		}


		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.label7 = new System.Windows.Forms.Label();
			this.label8 = new System.Windows.Forms.Label();
			this.label9 = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.BackColor = System.Drawing.Color.Transparent;
			this.label1.Location = new System.Drawing.Point(21, 16);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(580, 56);
			this.label1.TabIndex = 0;
			this.label1.Text = @"With the TREE VIEW list of all the groups in Profitgrabber, you can organize, rename, delete, and create any number of groups and up to 100 subgroups under any of the groups. For example, you can create a Foreclosure Group, with 50 subgroups where each subgroup will contain all the defaults for that current week. ";
			// 
			// label2
			// 
			this.label2.BackColor = System.Drawing.Color.Transparent;
			this.label2.Location = new System.Drawing.Point(22, 72);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(586, 32);
			this.label2.TabIndex = 1;
			this.label2.Text = "The STATUS GROUPS are used to manage �the work flow� of your leads, and are expla" +
				"ined in the contact view section. They can not be renamed or deleted.";
			// 
			// label3
			// 
			this.label3.BackColor = System.Drawing.Color.Transparent;
			this.label3.Location = new System.Drawing.Point(22, 112);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(584, 32);
			this.label3.TabIndex = 2;
			this.label3.Text = "The MARKETING GROUPS are used to manage your lead generation effort. They can not" +
				" be deleted, but can be renamed.";
			// 
			// label4
			// 
			this.label4.BackColor = System.Drawing.Color.Transparent;
			this.label4.Location = new System.Drawing.Point(21, 160);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(584, 19);
			this.label4.TabIndex = 3;
			this.label4.Text = "The MY GROUPS are intended for you to easier manage your properties and contacts." +
				"";
			// 
			// label5
			// 
			this.label5.BackColor = System.Drawing.Color.Transparent;
			this.label5.ForeColor = System.Drawing.Color.Green;
			this.label5.Location = new System.Drawing.Point(21, 208);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(584, 50);
			this.label5.TabIndex = 4;
			this.label5.Text = @"With the Task List view, you can see all the tasks that are assigned for the Group currently selected. You can create, edit, and delete tasks and activities using the command buttons.  The tasks shown and created here are monitored and controlled by Profit Grabber�s Task Manager (the lowest button on the left), explained elsewhere in this manual.";
			// 
			// label6
			// 
			this.label6.BackColor = System.Drawing.Color.Transparent;
			this.label6.Location = new System.Drawing.Point(17, 368);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(584, 64);
			this.label6.TabIndex = 5;
			this.label6.Text = @"In History List you will keep the history of all the tasks that were scheduled for that group � you can see what kind of marketing was done and the dates that particular campaign was executed. You can also add/delete past actions that were NEVER part of any scheduled activity or a task, so not only that you can have a history of scheduled tasks for a selected group, but you can record any other actions that were taken or have happened relating to that group. ";
			// 
			// label7
			// 
			this.label7.Location = new System.Drawing.Point(21, 184);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(459, 16);
			this.label7.TabIndex = 6;
			this.label7.Text = "The RETURN MAIL GROUP is where your skip-traced records will be stored.";
			// 
			// label8
			// 
			this.label8.Location = new System.Drawing.Point(21, 264);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(587, 52);
			this.label8.TabIndex = 7;
			this.label8.Text = @">> What are Tasks or Activities?  TASKS are things you want to do for a contact or a group to find, initiate, or complete a deal. Assigning a task creates a reminder for you to complete the task and provides a way to track what has been done and what needs to be done for a contact or a group. ";
			// 
			// label9
			// 
			this.label9.Location = new System.Drawing.Point(20, 312);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(588, 52);
			this.label9.TabIndex = 8;
			this.label9.Text = @"An ACTIVITY is a series of tasks. The advantage of assigning an activity to simply assigning tasks, is that you can save the Activity and reuse it later for more contacts and groups. This is a huge time saver and makes it very difficult to miss a task because you will be using time tested and proven series of tasks.";
			// 
			// SelectGroup
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.BackColor = System.Drawing.Color.LightYellow;
			this.ClientSize = new System.Drawing.Size(624, 456);
			this.Controls.Add(this.label9);
			this.Controls.Add(this.label8);
			this.Controls.Add(this.label7);
			this.Controls.Add(this.label6);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.Name = "SelectGroup";
			this.Opacity = 0.95;
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
			this.Text = "SelectGroup";
			this.ResumeLayout(false);

		}
		#endregion
	}
}
