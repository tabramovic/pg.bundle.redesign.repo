using System; 
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace DealMaker.Forms.Floaters
{
    public class DealInfo : FloaterBaseForm
    {
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.label7 = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(13, 24);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(476, 33);
			this.label1.TabIndex = 0;
			this.label1.Text = "Here�s where you keep all the pertinent info about your deal, such as the propert" +
				"y details, the seller�s reason for selling and his/her motivation, the marketing" +
				" source of that lead, etc. ";
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(13, 66);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(312, 23);
			this.label2.TabIndex = 1;
			this.label2.Text = "The APN stands for Tax Assessor Parcel Number.";
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(13, 89);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(496, 47);
			this.label3.TabIndex = 2;
			this.label3.Text = "�TOTAL OWNED� field is the read-only field that gives you the addition of the all" +
				" three Loan Balances, plus the �Other Liens� amount. The Other Liens field is wh" +
				"ere you can enter any back taxes, liens or judgments, etc.";
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(13, 136);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(496, 52);
			this.label4.TabIndex = 3;
			this.label4.Text = @"�TOTAL MONTHLY� field is the read-only field that gives you the addition of all the three loan Monthly Payments, plus Home Owners� Association dues per month, and plus Ins&Tax/mo in case the taxes and insurance payment is not included in the loan payment. ";
			// 
			// label5
			// 
			this.label5.Location = new System.Drawing.Point(12, 188);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(488, 44);
			this.label5.TabIndex = 4;
			this.label5.Text = "The LEAD SOURCE drop down menu contains all the marketing you are currently doing" +
				", and this menu is populated by clicking on File > User Information > Add Media " +
				"on the main command bar.";
			// 
			// label6
			// 
			this.label6.Location = new System.Drawing.Point(14, 240);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(495, 34);
			this.label6.TabIndex = 5;
			this.label6.Text = "�ARV� (After Repaired Value) field is merged with data from COMPS tab > Estimated" +
				" Value field. However, the program will allow you to enter and save your own val" +
				"ue.";
			// 
			// label7
			// 
			this.label7.ForeColor = System.Drawing.Color.Green;
			this.label7.Location = new System.Drawing.Point(14, 272);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(495, 54);
			this.label7.TabIndex = 6;
			this.label7.Text = @"To facilitate the deal analysis using Deal IQ module, for your convenience the following data is also shown/transferred in the Deal IQ:  ARV (worth by comps), Worth By Seller, Total Owed, Total Monthly, Foreclosing (Y/N), Reinstate Amount, Asking Cash and Asking Price.";
			// 
			// DealInfo
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.BackColor = System.Drawing.Color.LightYellow;
			this.ClientSize = new System.Drawing.Size(521, 344);
			this.Controls.Add(this.label7);
			this.Controls.Add(this.label6);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.Name = "DealInfo";
			this.Opacity = 0.95;
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
			this.Text = "Form1";
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label7;
		
		public DealInfo()
        {
            InitializeComponent();
			this.Opacity = 0;
        }

		protected override void OnPaint(PaintEventArgs e)
		{
			GfxDrawer.DrawCurveFromOffsets(3, 130, 3, 3, 5, 3, "INTERACTIVE HELP", 2, 5, this, e.Graphics, Brushes.LightYellow, Pens.Black);
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}
    }
}