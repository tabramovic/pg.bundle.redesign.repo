using System;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace DealMaker.Forms.Floaters
{
    public class NewContact_ContactView : FloaterBaseForm
    {

		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.label7 = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(12, 25);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(487, 61);
			this.label1.TabIndex = 0;
			this.label1.Text = @"You will notice three different set of fields for the Mailing Address: Last Known, Previous, and Old. This is for you to keep track of all the old mailing addresses you have tried for that contact. The Return Mail wizard will update these Mailing Address fields with the new address every time the skip-tracing update is performed, moving automatically the old address in the row below.";
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(12, 95);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(487, 35);
			this.label2.TabIndex = 1;
			this.label2.Text = "The Last Known address is the one used for your mailings. If that field is absent" +
				", the Site Address will be used instead. ";
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(13, 134);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(486, 49);
			this.label3.TabIndex = 2;
			this.label3.Text = "NEW CONTACT function is the way you will get new contacts that are not on an impo" +
				"rtable list or file, into ProfitGrabber. You are provided with this blank worksh" +
				"eet version of the View Contacts screen and tabs. ";
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(14, 187);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(486, 43);
			this.label4.TabIndex = 3;
			this.label4.Text = @"You will want to fill out as many of the fields as possible to have the most complete and usable record of the Contact.  See Contact View section of this manual for a detailed description of the screen, tabs, and information stored/entered within this worksheet.";
			// 
			// label5
			// 
			this.label5.Location = new System.Drawing.Point(15, 234);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(484, 30);
			this.label5.TabIndex = 4;
			this.label5.Text = "You will notice three different set of fields for the Mailing Address: Last Known" +
				", Previous, and Old. This is for you to keep track of all the old mailing addres" +
				"ses you have tried for that contact. ";
			// 
			// label6
			// 
			this.label6.Location = new System.Drawing.Point(15, 312);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(485, 29);
			this.label6.TabIndex = 5;
			this.label6.Text = "The Last Known address is the one used for your mailings. If that field is absent" +
				", the Site Address will be used instead. ";
			// 
			// label7
			// 
			this.label7.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(192)), ((System.Byte)(0)), ((System.Byte)(0)));
			this.label7.Location = new System.Drawing.Point(16, 268);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(483, 36);
			this.label7.TabIndex = 6;
			this.label7.Text = "The Return Mail wizard will update these Mailing Address fields with the new addr" +
				"ess every time the skip-tracing update is performed, moving automatically the ol" +
				"d address in the row below.";
			// 
			// NewContact_ContactView
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.BackColor = System.Drawing.Color.LightYellow;
			this.ClientSize = new System.Drawing.Size(512, 360);
			this.Controls.Add(this.label7);
			this.Controls.Add(this.label6);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.Name = "NewContact_ContactView";
			this.Opacity = 0.95;
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
			this.Text = "ContactView";
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label7;

        public NewContact_ContactView()
        {
            InitializeComponent();
			this.Opacity = 0;
        }

		protected override void OnPaint(PaintEventArgs e)
		{
			GfxDrawer.DrawCurveFromOffsets(3, 130, 3, 3, 5, 3, "INTERACTIVE HELP", 2, 5, this, e.Graphics, Brushes.LightYellow, Pens.Black);
		}
    }
}