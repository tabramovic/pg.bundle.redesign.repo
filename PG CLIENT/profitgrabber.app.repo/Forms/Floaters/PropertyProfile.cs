using System;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace DealMaker.Forms.Floaters
{
    public class PropertyProfile : FloaterBaseForm
    {
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.label7 = new System.Windows.Forms.Label();
			this.label8 = new System.Windows.Forms.Label();
			this.label9 = new System.Windows.Forms.Label();
			this.label10 = new System.Windows.Forms.Label();
			this.label11 = new System.Windows.Forms.Label();
			this.label12 = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(21, 20);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(591, 74);
			this.label1.TabIndex = 0;
			this.label1.Text = @"The PROPERTY PROFILE report is an EXTENSIVE DATA PROFILE of your subject property. This report contains over 30 (sometimes over 40) data elements, giving you the full picture of the property you are considering buying. This report is available only with the current data subscription. If you desire to subscribe to �Ultimate Data Source� that covers over 1,000 counties, click on the �Property Data� on the command bar.";
			// 
			// label2
			// 
			this.label2.ForeColor = System.Drawing.Color.Green;
			this.label2.Location = new System.Drawing.Point(22, 85);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(590, 37);
			this.label2.TabIndex = 1;
			this.label2.Text = "You can search for the Property Profile for the subject property by supplying its" +
				" address, its parcel number or the owner�s name. The preferred way to search tha" +
				"t has the highest chance of finding your property is by using the ADDRESS SEARCH" +
				".";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(192)), ((System.Byte)(0)), ((System.Byte)(0)));
			this.label3.Location = new System.Drawing.Point(22, 122);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(98, 16);
			this.label3.TabIndex = 2;
			this.label3.Text = "IMPORTANT TIP: ";
			// 
			// label4
			// 
			this.label4.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(192)), ((System.Byte)(0)), ((System.Byte)(0)));
			this.label4.Location = new System.Drawing.Point(20, 135);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(576, 48);
			this.label4.TabIndex = 3;
			this.label4.Text = @"�The less is Better!� as far as searching goes - supplying less (but the required) information will INCREASE YOUR CHANCES of finding that property. If the information you entered is insufficient, you will get back multiple properties, so you can choose the target one from the list";
			// 
			// label5
			// 
			this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label5.Location = new System.Drawing.Point(21, 183);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(577, 38);
			this.label5.TabIndex = 4;
			this.label5.Text = ">> For example, the address search needs only the STREET NUMBER, STREET NAME, and" +
				" ZIP. Also use the Unit Number where applicable.";
			// 
			// label6
			// 
			this.label6.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(192)), ((System.Byte)(0)), ((System.Byte)(0)));
			this.label6.Location = new System.Drawing.Point(21, 221);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(588, 62);
			this.label6.TabIndex = 5;
			this.label6.Text = @"> The STREET NAME should not include any prefixes or suffixes � no East, West, Street, Avenue, Blvd, etc. � only the name (i.e. �E 5th Avenue � wrong; �5th� is correct)! If the street name is an ordinal number (i.e. 5th) than type it in as the ordinal number (5 is wrong, 5th is correct).";
			// 
			// label7
			// 
			this.label7.Location = new System.Drawing.Point(22, 272);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(575, 24);
			this.label7.TabIndex = 6;
			this.label7.Text = "> You can search by either the ZIP or the County and State � you don�t need ZIP a" +
				"nd County/State.";
			// 
			// label8
			// 
			this.label8.Location = new System.Drawing.Point(21, 296);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(565, 30);
			this.label8.TabIndex = 7;
			this.label8.Text = "NOTE: the pre-directional (East, West, etc.), post-directional, or suffix (Street" +
				", Circle, Ave., etc.) is not needed. If the ProfitGrabber finds �multiple proper" +
				"ties� you can select the right one from the list.";
			// 
			// label9
			// 
			this.label9.ForeColor = System.Drawing.Color.Green;
			this.label9.Location = new System.Drawing.Point(21, 337);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(563, 39);
			this.label9.TabIndex = 8;
			this.label9.Text = "If you get an error message �No Records Found For Search Criteria Submitted� you " +
				"need to change your search criteria � try putting less or more in the name field" +
				".";
			// 
			// label10
			// 
			this.label10.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(192)), ((System.Byte)(0)), ((System.Byte)(0)));
			this.label10.Location = new System.Drawing.Point(20, 376);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(577, 30);
			this.label10.TabIndex = 9;
			this.label10.Text = "You are NOT CHARGED if the search finds �Multiple Properties� and returns a list " +
				"of properties, or does not find any. You are charged only when the data is retur" +
				"ned.";
			// 
			// label11
			// 
			this.label11.Location = new System.Drawing.Point(19, 406);
			this.label11.Name = "label11";
			this.label11.Size = new System.Drawing.Size(590, 14);
			this.label11.TabIndex = 10;
			this.label11.Text = "You can print all the property data as a Report by clicking on Print Report butto" +
				"n.";
			// 
			// label12
			// 
			this.label12.Location = new System.Drawing.Point(19, 430);
			this.label12.Name = "label12";
			this.label12.Size = new System.Drawing.Size(577, 31);
			this.label12.TabIndex = 11;
			this.label12.Text = "The ProfitGrabber, LLC. does not guarantee the accuracy of data, since the data i" +
				"s sourced from different counties and sometimes via third party providers.";
			// 
			// PropertyProfile
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.BackColor = System.Drawing.Color.LightYellow;
			this.ClientSize = new System.Drawing.Size(623, 480);
			this.Controls.Add(this.label12);
			this.Controls.Add(this.label11);
			this.Controls.Add(this.label10);
			this.Controls.Add(this.label9);
			this.Controls.Add(this.label8);
			this.Controls.Add(this.label7);
			this.Controls.Add(this.label6);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.Name = "PropertyProfile";
			this.Opacity = 0.95;
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
			this.Text = "Form1";
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.Label label12;

        public PropertyProfile()
        {
            InitializeComponent();
			this.Opacity = 0;
        }

		protected override void OnPaint(PaintEventArgs e)
		{
			GfxDrawer.DrawCurveFromOffsets(3, 130, 3, 3, 5, 3, "INTERACTIVE HELP", 2, 5, this, e.Graphics, Brushes.LightYellow, Pens.Black);
		}
    }
}