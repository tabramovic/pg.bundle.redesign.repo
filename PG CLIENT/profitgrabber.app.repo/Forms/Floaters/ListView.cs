using System;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace DealMaker.Forms.Floaters
{
    public class ListView : FloaterBaseForm
    {
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(22, 21);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(519, 34);
			this.label1.TabIndex = 0;
			this.label1.Text = "The LIST VIEW is provided as a way of MANAGING CONTACTS in a group with as many v" +
				"isible on the screen as possible, along with a scroll bar for access to even mor" +
				"e.";
			// 
			// label2
			// 
			this.label2.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(192)), ((System.Byte)(0)), ((System.Byte)(0)));
			this.label2.Location = new System.Drawing.Point(23, 59);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(518, 69);
			this.label2.TabIndex = 1;
			this.label2.Text = @"DELETE TAGGED �  this is the button to use if you wanted to get rid of contacts in a Group, while still keeping the Group intact. In fact, this is the only place in ProfitGrabber where you can delete individual contacts without deleting a whole group. As with all Deletions in ProfitGrabber, a warning will pop up that you need to OK before deletion. You�ll be able to select to delete the tagged contacts from the currently selected group in List View only, or delete them permanently from the whole program.";
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(22, 138);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(517, 35);
			this.label3.TabIndex = 2;
			this.label3.Text = "LOOKUP TAGGED/UNTAGGED button will bring all of the contacts you have tagged/unta" +
				"gged into CONTACT VIEW to work with. ";
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(23, 173);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(505, 51);
			this.label4.TabIndex = 3;
			this.label4.Text = @"ASSIGN TO GROUP button will bring up the familiar Assign Group window where you can select a group or create a new one for the tagged contact(s) to go into. You may want to add Contacts to more than one group. You can use the Assign group over and over to assign contact to AS MANY GROUPS AS YOU NEED to.";
			// 
			// label5
			// 
			this.label5.Location = new System.Drawing.Point(22, 240);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(540, 39);
			this.label5.TabIndex = 4;
			this.label5.Text = "EXPORT TAGGED � this button opens up the Export Wizard, which will allow you to e" +
				"xport the tagged contacts to a file. ";
			// 
			// ListView
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.BackColor = System.Drawing.Color.LightYellow;
			this.ClientSize = new System.Drawing.Size(565, 288);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.Name = "ListView";
			this.Opacity = 0.95;
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
			this.Text = "ListView";
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label5;
		public ListView()
        {
            InitializeComponent();
			this.Opacity = 0;
        }

		protected override void OnPaint(PaintEventArgs e)
		{
			GfxDrawer.DrawCurveFromOffsets(3, 130, 3, 3, 5, 3, "INTERACTIVE HELP", 2, 5, this, e.Graphics, Brushes.LightYellow, Pens.Black);
		}
    }
}