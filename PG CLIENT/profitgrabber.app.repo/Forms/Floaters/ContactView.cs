using System;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace DealMaker.Forms.Floaters
{
    public class ContactView : FloaterBaseForm
    {
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(12, 25);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(487, 66);
			this.label1.TabIndex = 0;
			this.label1.Text = @"You will notice three different set of fields for the Mailing Address: Last Known, Previous, and Old. This is for you to keep track of all the OLD mailing addresses you have tried for that contact. The Return Mail wizard WILL UPDATE these Mailing Address fields with the new address every time the skip-tracing update is performed, MOVING AUTOMATICALLY THE OLD ADDRESS IN THE ROW BELOW.";
			// 
			// label2
			// 
			this.label2.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(192)), ((System.Byte)(0)), ((System.Byte)(0)));
			this.label2.Location = new System.Drawing.Point(12, 102);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(487, 35);
			this.label2.TabIndex = 1;
			this.label2.Text = "The Last Known address is the one used for your mailings. If that field is absent" +
				", the Site Address will be used instead. ";
			// 
			// ContactView
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.BackColor = System.Drawing.Color.LightYellow;
			this.ClientSize = new System.Drawing.Size(512, 160);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.Name = "ContactView";
			this.Opacity = 0.95;
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
			this.Text = "ContactView";
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		
		public ContactView()
        {
            InitializeComponent();
			this.Opacity = 0;
        }

		protected override void OnPaint(PaintEventArgs e)
		{
			GfxDrawer.DrawCurveFromOffsets(3, 130, 3, 3, 5, 3, "INTERACTIVE HELP", 2, 5, this, e.Graphics, Brushes.LightYellow, Pens.Black);
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>		
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}
    }
}