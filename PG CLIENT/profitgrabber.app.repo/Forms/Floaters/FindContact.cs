using System;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace DealMaker.Forms.Floaters
{
    public class FindContact : FloaterBaseForm
    {

		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.label7 = new System.Windows.Forms.Label();
			this.label8 = new System.Windows.Forms.Label();
			this.label9 = new System.Windows.Forms.Label();
			this.label10 = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(22, 20);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(379, 44);
			this.label1.TabIndex = 0;
			this.label1.Text = "The Find is a search function to locate specific contacts. Searches can be run in" +
				" All Groups, specified individual or multiple groups and their subgroups. ";
			// 
			// label2
			// 
			this.label2.ForeColor = System.Drawing.Color.Green;
			this.label2.Location = new System.Drawing.Point(23, 61);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(370, 33);
			this.label2.TabIndex = 1;
			this.label2.Text = "First Step: Choose the groups affected by the search (and if you want to search s" +
				"ubgroups).";
			// 
			// label3
			// 
			this.label3.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(192)), ((System.Byte)(0)), ((System.Byte)(0)));
			this.label3.Location = new System.Drawing.Point(22, 141);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(369, 72);
			this.label3.TabIndex = 2;
			this.label3.Text = @"Tip: use more specific search or narrow it when prompted! If the result of your search is greater than 20 records, you will be prompted to narrow your search. If you decide to continue without narrowing your search, the result will be still displayed, however it may take over 30 seconds for number of records exceeding 1,000.";
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(21, 213);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(370, 25);
			this.label4.TabIndex = 3;
			this.label4.Text = "TIP: to view a desired record in its Contact View, DOUBLE-CLICK on its row.";
			// 
			// label5
			// 
			this.label5.Location = new System.Drawing.Point(21, 238);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(376, 26);
			this.label5.TabIndex = 4;
			this.label5.Text = "Function buttons are provided at the bottom to manipulate and view the results:";
			// 
			// label6
			// 
			this.label6.Location = new System.Drawing.Point(23, 272);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(376, 45);
			this.label6.TabIndex = 5;
			this.label6.Text = "DELETE TAGGED �  will delete tagged contacts permanently from the whole program. " +
				"If you want to delete them ONLY from their groups, use delete function under Lis" +
				"t View.";
			// 
			// label7
			// 
			this.label7.Location = new System.Drawing.Point(24, 320);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(375, 32);
			this.label7.TabIndex = 6;
			this.label7.Text = "LOOKUP TAGGED/UNTAGGED button will bring all of the contacts you have tagged/unta" +
				"gged into Contact View to work with. ";
			// 
			// label8
			// 
			this.label8.Location = new System.Drawing.Point(24, 360);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(378, 70);
			this.label8.TabIndex = 7;
			this.label8.Text = @"ASSIGN TO GROUP button will bring up the familiar Assign Group window where you can select a group or create a new one for the tagged contact(s) to go into. You may want to add Contacts to more than one group. You can use the Assign group over and over to assign contact to as many groups as you need too.";
			// 
			// label9
			// 
			this.label9.Location = new System.Drawing.Point(24, 440);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(378, 36);
			this.label9.TabIndex = 8;
			this.label9.Text = "EXPORT TAGGED � this button opens up the Export Wizard, which will allow you to e" +
				"xport the tagged contacts to a file. ";
			// 
			// label10
			// 
			this.label10.ForeColor = System.Drawing.Color.Green;
			this.label10.Location = new System.Drawing.Point(24, 94);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(369, 47);
			this.label10.TabIndex = 9;
			this.label10.Text = "Second Step: Choose a search string you want use and mark the field that string w" +
				"ould be found in. The system will search the database and fill the results area " +
				"in with contacts matching your criteria.  ";
			// 
			// FindContact
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.BackColor = System.Drawing.Color.LightYellow;
			this.ClientSize = new System.Drawing.Size(419, 488);
			this.Controls.Add(this.label10);
			this.Controls.Add(this.label9);
			this.Controls.Add(this.label8);
			this.Controls.Add(this.label7);
			this.Controls.Add(this.label6);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.Name = "FindContact";
			this.Opacity = 0.95;
			this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.Label label10;

        public FindContact()
        {
            InitializeComponent();
			this.Opacity = 0;
        }
        
		protected override void OnPaint(PaintEventArgs e)
		{
			GfxDrawer.DrawCurveFromOffsets(3, 130, 3, 3, 5, 3, "INTERACTIVE HELP", 2, 5, this, e.Graphics, Brushes.LightYellow, Pens.Black);
		}
    }
}