using System;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace DealMaker.Forms.Floaters
{
    public class MFWSelectMarketingPiece : FloaterBaseForm
    {
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.label7 = new System.Windows.Forms.Label();
			this.label8 = new System.Windows.Forms.Label();
			this.label9 = new System.Windows.Forms.Label();
			this.label10 = new System.Windows.Forms.Label();
			this.label11 = new System.Windows.Forms.Label();
			this.label12 = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(24, 21);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(592, 28);
			this.label1.TabIndex = 0;
			this.label1.Text = "You have three sets of mail or fax documents to choose from: Mailing Labels, Dire" +
				"ct Response Letters or Fax documents.";
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(24, 48);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(585, 91);
			this.label2.TabIndex = 1;
			this.label2.Text = @"MAILING LABELS � you can select one of the three types as described below. The labels WITH BARCODE are to be used on the mailing piece going to prospects that you intend to skip trace if not reached. This will usually be your foreclosure or vacant house mailings. The barcode will be printed on the labels, so when the mail gets returned you will not need to manually re-enter it. You will use the scanner to quickly get them in the system. ";
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(24, 112);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(553, 37);
			this.label3.TabIndex = 2;
			this.label3.Text = "a) Label with Barcode � is to be used on postcards that you are planning on skip-" +
				"tracing if returned. ";
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(24, 128);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(537, 17);
			this.label4.TabIndex = 3;
			this.label4.Text = "b) Label without Barcode � it is to be used on postcards that you are not plannin" +
				"g to skip-trace.";
			// 
			// label5
			// 
			this.label5.Location = new System.Drawing.Point(24, 144);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(585, 57);
			this.label5.TabIndex = 4;
			this.label5.Text = @"c) Small Label with Barcode � is for letters if you want to skip trace ones that get undelivered and returned back to you. NOTE: these labels are meant to be used only for the barcode part, the address part will need to be hand-addressed. To avoid �junk mail� appearance and to increase chances of getting it opened, it is highly recommended that you never use labels for addressing your letters!";
			// 
			// label6
			// 
			this.label6.ForeColor = System.Drawing.Color.Maroon;
			this.label6.Location = new System.Drawing.Point(24, 213);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(585, 43);
			this.label6.TabIndex = 5;
			this.label6.Text = "IMPORTANT:  each barcode corresponds to that contact�s ID within the ProfitGrabbe" +
				"r, so it is important that you do not mix the barcodes and addresses. That is re" +
				"ason why the small labels are printed with their corresponding address on the si" +
				"de.";
			// 
			// label7
			// 
			this.label7.Location = new System.Drawing.Point(24, 264);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(585, 64);
			this.label7.TabIndex = 6;
			this.label7.Text = "LETTERS - clicking on this tab will reveal the various letters you have stored in" +
				" ProfitGrabber for mail merging. Once you have selected a letter, you click next" +
				" to move to the screen where the letter can be previewed and printed.";
			// 
			// label8
			// 
			this.label8.Location = new System.Drawing.Point(24, 296);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(585, 48);
			this.label8.TabIndex = 7;
			this.label8.Text = @"TIP: you can import in ProfitGrabber any number of letters or fax documents to be used here, or you can change/edit the existing letters. To import or add your own letters, or to edit existing letters, use Text Editor by clicking on �Tools� on the command bar, and selecting �Edit Mail/Fax Template�, then use the right-click menu.";
			// 
			// label9
			// 
			this.label9.Location = new System.Drawing.Point(24, 344);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(585, 55);
			this.label9.TabIndex = 8;
			this.label9.Text = "FAX DOCUMENTS � this tab has two selections. One is the Letter Of Intent and the " +
				"other is All Other Fax Documents. The Letter of Intent is a primary function of " +
				"ProfitGrabber�s faxing feature.";
			// 
			// label10
			// 
			this.label10.Location = new System.Drawing.Point(24, 384);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(585, 50);
			this.label10.TabIndex = 9;
			this.label10.Text = "The LETTER OF INTENT is for you to make mass �low-ball� offers on the active list" +
				"ings. You need to check your local real estate rules in order to comply with leg" +
				"alities in your area.";
			// 
			// label11
			// 
			this.label11.Location = new System.Drawing.Point(24, 424);
			this.label11.Name = "label11";
			this.label11.Size = new System.Drawing.Size(585, 93);
			this.label11.TabIndex = 10;
			this.label11.Text = @"The idea of �low-ball� (a.k.a. Blind Offers) is to �weed out� any motivated sellers without much expense at all. You are sending a low offer and if it gets countered, you may have a �catch�. It is a numbers game and it takes a lot of offers to get a deal. That�s why the ProfitGrabber will do it all automatically for you. It will calculate the Offer Price based on the percentage you select, it will personalize each Letter of Intent with the property and its listing agent information, and fax it all automatically.";
			// 
			// label12
			// 
			this.label12.ForeColor = System.Drawing.Color.Maroon;
			this.label12.Location = new System.Drawing.Point(24, 496);
			this.label12.Name = "label12";
			this.label12.Size = new System.Drawing.Size(585, 48);
			this.label12.TabIndex = 11;
			this.label12.Text = "NOTE: fields needed in Letter of Intent (to fax offers) are: LISTING AGENT NAME, " +
				"LISTING AGENT FAX NUMBER, PROPERTY ADDRESS, MLS NUMBER, LISTING PRICE. The syste" +
				"m will calculate the Offer Price from the Listing Price.";
			// 
			// MFWSelectMarketingPiece
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.BackColor = System.Drawing.Color.LightYellow;
			this.ClientSize = new System.Drawing.Size(630, 560);
			this.Controls.Add(this.label12);
			this.Controls.Add(this.label11);
			this.Controls.Add(this.label10);
			this.Controls.Add(this.label9);
			this.Controls.Add(this.label8);
			this.Controls.Add(this.label7);
			this.Controls.Add(this.label6);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.Name = "MFWSelectMarketingPiece";
			this.Opacity = 0.95;
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
			this.Text = "MFWSelectMarketingPiece";
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.Label label12;
		
		public MFWSelectMarketingPiece()
        {
            InitializeComponent();
			this.Opacity = 0;
        }

		protected override void OnPaint(PaintEventArgs e)
		{
			GfxDrawer.DrawCurveFromOffsets(3, 130, 3, 3, 5, 3, "INTERACTIVE HELP", 2, 5, this, e.Graphics, Brushes.LightYellow, Pens.Black);
		}
    }
}