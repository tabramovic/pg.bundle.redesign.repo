using System;

using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace DealMaker.Forms.Floaters
{
    public class Comps : FloaterBaseForm
    {
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

	

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.label7 = new System.Windows.Forms.Label();
			this.label8 = new System.Windows.Forms.Label();
			this.label10 = new System.Windows.Forms.Label();
			this.label11 = new System.Windows.Forms.Label();
			this.label12 = new System.Windows.Forms.Label();
			this.label13 = new System.Windows.Forms.Label();
			this.label14 = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(21, 19);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(561, 37);
			this.label1.TabIndex = 0;
			this.label1.Text = "The COMPS tab allows you to search for Comparable Sales properties in order to de" +
				"termine the Fair Market Value of your subject property. ";
			// 
			// label2
			// 
			this.label2.ForeColor = System.Drawing.Color.Black;
			this.label2.Location = new System.Drawing.Point(21, 56);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(561, 85);
			this.label2.TabIndex = 1;
			this.label2.Text = @"The COMPS Report will return up to 50 comparable sales in the vicinity of your subject property. Then you can sort those properties by Distance, by Subdivision, or any other way you want to analyze them. You will tag the properties that you determine are the most comparable to your property. Once tagged, you can click Recalculate button, and the Estimated Value will be calculated. This value is calculated by dividing each property�s Sales Price by its Square Footage, to obtain $/sq.ft. Then, that number is averaged and multiplied by your Subject Property square footage, to give you the Estimated Value. ";
			// 
			// label3
			// 
			this.label3.ForeColor = System.Drawing.Color.Green;
			this.label3.Location = new System.Drawing.Point(21, 141);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(561, 47);
			this.label3.TabIndex = 2;
			this.label3.Text = "If you want any additional information about any of the comparable properties, yo" +
				"u can double-click on its address (or its row) and a new window will pop-up, giv" +
				"ing you the complete Property Profile report for that �comparable sales� propert" +
				"y. ";
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(21, 188);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(549, 33);
			this.label4.TabIndex = 3;
			this.label4.Text = "Tip: since up to 50 profiles are returned, it is always better to NOT narrow your" +
				" search criteria (i.e. use search radius of 1 or 2 miles, instead of 0.5 miles)." +
				"";
			// 
			// label5
			// 
			this.label5.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(192)), ((System.Byte)(0)), ((System.Byte)(0)));
			this.label5.Location = new System.Drawing.Point(21, 221);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(379, 18);
			this.label5.TabIndex = 4;
			this.label5.Text = "IMPORTANT TIP (the same as with the Property Profile search): ";
			// 
			// label6
			// 
			this.label6.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(192)), ((System.Byte)(0)), ((System.Byte)(0)));
			this.label6.Location = new System.Drawing.Point(21, 239);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(549, 55);
			this.label6.TabIndex = 5;
			this.label6.Text = @"�The less is Better!� as far as searching goes - supplying less (but the required) information will increase your chances of finding that property. If the information you entered is insufficient, you will get back multiple properties, so you can choose the target one. ";
			// 
			// label7
			// 
			this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label7.Location = new System.Drawing.Point(22, 284);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(548, 29);
			this.label7.TabIndex = 6;
			this.label7.Text = ">> For example, the address search needs only the STREET NUMBER, STREET NAME, and" +
				" ZIP. Also use the Unit Number where applicable.";
			// 
			// label8
			// 
			this.label8.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(192)), ((System.Byte)(0)), ((System.Byte)(0)));
			this.label8.Location = new System.Drawing.Point(22, 322);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(548, 43);
			this.label8.TabIndex = 7;
			this.label8.Text = @"> The STREET NAME should not include any prefixes or suffixes � no East, West, Street, Avenue, Blvd, etc. � only the name (i.e. �E 5th Avenue � wrong; �5th� is correct)! If the street name is an ordinal number (i.e. 5th) than type it in as the ordinal number (5 is wrong, 5th is correct).";
			// 
			// label10
			// 
			this.label10.Location = new System.Drawing.Point(22, 380);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(549, 36);
			this.label10.TabIndex = 9;
			this.label10.Text = "NOTE: the pre-directional (East, West, etc.), post-directional, or suffix (Street" +
				", Circle, Ave., etc.) is not needed. If the ProfitGrabber finds �multiple proper" +
				"ties� you can select the right one from the list.";
			// 
			// label11
			// 
			this.label11.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(192)), ((System.Byte)(0)), ((System.Byte)(0)));
			this.label11.Location = new System.Drawing.Point(21, 416);
			this.label11.Name = "label11";
			this.label11.Size = new System.Drawing.Size(549, 49);
			this.label11.TabIndex = 10;
			this.label11.Text = "You are NOT CHARGED if the search finds �Multiple Properties� and returns a list " +
				"of properties, or does not find any. You are charged only when the data is retur" +
				"ned � only one charge per search regardless if 50 or 1 comparable property is re" +
				"turned. ";
			// 
			// label12
			// 
			this.label12.Location = new System.Drawing.Point(21, 476);
			this.label12.Name = "label12";
			this.label12.Size = new System.Drawing.Size(549, 29);
			this.label12.TabIndex = 11;
			this.label12.Text = "If you get an error message �No Records Found For Search Criteria Submitted� you " +
				"need to change your search criteria � try putting less or more in the name field" +
				".";
			// 
			// label13
			// 
			this.label13.Location = new System.Drawing.Point(22, 509);
			this.label13.Name = "label13";
			this.label13.Size = new System.Drawing.Size(548, 26);
			this.label13.TabIndex = 12;
			this.label13.Text = "You can print all the property data as a COMPS Report by clicking on Print Report" +
				" button. NOTE: the Report will include only tagged records (comps).";
			// 
			// label14
			// 
			this.label14.Location = new System.Drawing.Point(22, 544);
			this.label14.Name = "label14";
			this.label14.Size = new System.Drawing.Size(549, 26);
			this.label14.TabIndex = 13;
			this.label14.Text = "The ProfitGrabber, LLC. does not guarantee the accuracy of data, since the data i" +
				"s sourced from different counties and sometimes via third party providers.";
			// 
			// Comps
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.BackColor = System.Drawing.Color.LightYellow;
			this.ClientSize = new System.Drawing.Size(594, 591);
			this.Controls.Add(this.label14);
			this.Controls.Add(this.label13);
			this.Controls.Add(this.label12);
			this.Controls.Add(this.label11);
			this.Controls.Add(this.label10);
			this.Controls.Add(this.label8);
			this.Controls.Add(this.label7);
			this.Controls.Add(this.label6);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.Name = "Comps";
			this.Opacity = 0.95;
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
			this.Text = "Comps";
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.Label label13;
		private System.Windows.Forms.Label label14;
        
		public Comps()
        {
            InitializeComponent();
			this.Opacity = 0;
        }

		protected override void OnPaint(PaintEventArgs e)
		{
			GfxDrawer.DrawCurveFromOffsets(3, 130, 3, 3, 5, 3, "INTERACTIVE HELP", 2, 5, this, e.Graphics, Brushes.LightYellow, Pens.Black);
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}
    }
}