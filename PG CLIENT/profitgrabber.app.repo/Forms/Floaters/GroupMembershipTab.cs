using System;

using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace DealMaker.Forms.Floaters
{
    public class GroupMembershipTab : FloaterBaseForm
    {
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(12, 18);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(340, 30);
			this.label1.TabIndex = 0;
			this.label1.Text = "This is where you would MANAGE your �work flow� for that contact";
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(16, 48);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(379, 45);
			this.label2.TabIndex = 1;
			this.label2.Text = "You can use the �SHORTCUT BUTTONS� to assign to your contact to one of the Status" +
				" Groups. You choose the STATUS GROUP by clicking on one of the shortcut buttons " +
				"that fits the status of the contact you are viewing.";
			// 
			// label3
			// 
			this.label3.ForeColor = System.Drawing.Color.Green;
			this.label3.Location = new System.Drawing.Point(16, 104);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(379, 76);
			this.label3.TabIndex = 2;
			this.label3.Text = @"For example, if you signed a contract on that property, you can assign that contact to �Pending� group, and this group will show in the Status field that is viewable at the top of the Contact View screen. Later on, you can select the �Pending� group from the Select Group window, and see all of your pending contacts at one place.";
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(16, 192);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(378, 39);
			this.label4.TabIndex = 3;
			this.label4.Text = "Any leads that you decide to be of no value (Dead Leads), you can also assign to " +
				"Do Not Mail group, and later on you can take those out of your mailings.";
			// 
			// label5
			// 
			this.label5.Location = new System.Drawing.Point(16, 240);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(378, 73);
			this.label5.TabIndex = 4;
			this.label5.Text = @"You can use �Assign� and �Remove� buttons to manage the Other Group Membership. For example, if you want to see how many leads you are getting from referrals, you can create a group i.e. �Referral� and assign that contact to that group. Later on, you can see the number of records in that group from the Select Group window.";
			// 
			// GroupMembershipTab
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.BackColor = System.Drawing.Color.LightYellow;
			this.ClientSize = new System.Drawing.Size(403, 336);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.Name = "GroupMembershipTab";
			this.Opacity = 0.95;
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
			this.Text = "Form1";
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label5;
		
		public GroupMembershipTab()
        {
            InitializeComponent();
			this.Opacity = 0;
        }

		protected override void OnPaint(PaintEventArgs e)
		{
			GfxDrawer.DrawCurveFromOffsets(3, 130, 3, 3, 5, 3, "INTERACTIVE HELP", 2, 5, this, e.Graphics, Brushes.LightYellow, Pens.Black);
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}
    }
}