using System;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace DealMaker.Forms.Floaters
{
    public class MFWViewandTagRecipients : FloaterBaseForm
    {
        
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(25, 19);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(420, 53);
			this.label1.TabIndex = 0;
			this.label1.Text = @"This screen appears after the data is loaded into Mail Fax Wizard (after the �progress bar� window with the green status bar that goes across it is closed). If it is a very large group it can take a small period to load, but smaller groups will load right up. ";
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(24, 88);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(419, 50);
			this.label2.TabIndex = 1;
			this.label2.Text = "The contacts are displayed in the �View and Tag Your Recipients� list box. The fi" +
				"rst column of the list box is a Tag check box for each contact. You need to tag " +
				"all of the contacts you want to mail or fax to.";
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(24, 152);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(418, 39);
			this.label3.TabIndex = 2;
			this.label3.Text = "To TAG a record, click in its check box, or DOUBLE-CLICK on its row. You can quic" +
				"ken the process by using the TAG ALL and UNTAG ALL shortcut command buttons.  ";
			// 
			// label4
			// 
			this.label4.ForeColor = System.Drawing.Color.Green;
			this.label4.Location = new System.Drawing.Point(26, 208);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(419, 56);
			this.label4.TabIndex = 3;
			this.label4.Text = @"The field called �Number of Currently Tagged Records� shows you how many contacts you have tagged. This is useful to give you an overview of how many letters or mailing labels you are going to print. Also as precaution, the �Next� button is grayed out until you have tagged at least one record.";
			// 
			// MFWViewandTagRecipients
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.BackColor = System.Drawing.Color.LightYellow;
			this.ClientSize = new System.Drawing.Size(464, 280);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.Name = "MFWViewandTagRecipients";
			this.Opacity = 0.95;
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
			this.Text = "MFWViewandTagRecipients";
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		
		public MFWViewandTagRecipients()
        {
            InitializeComponent();
			this.Opacity = 0;
        }

		protected override void OnPaint(PaintEventArgs e)
		{
			GfxDrawer.DrawCurveFromOffsets(3, 130, 3, 3, 5, 3, "INTERACTIVE HELP", 2, 5, this, e.Graphics, Brushes.LightYellow, Pens.Black);
		}
    }
}