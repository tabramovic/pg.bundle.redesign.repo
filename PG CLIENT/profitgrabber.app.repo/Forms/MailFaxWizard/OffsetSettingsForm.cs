using System;
using System.Drawing;
using System.Drawing.Printing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;

//Added
using ProfitGrabber.Common;


namespace DealMaker
{
	public enum eLabel
	{
		eSmall = 0, 
		eMedium, 
		eLarge
	}
	
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class offsetSettingsForm : System.Windows.Forms.Form
	{
		private int loadedHorOffset = 0;
		private int loadedVertOffset = 0;

		ArrayList alExtendedRecords = new ArrayList();
		private MFW_PrintDescriptor pd = null;
		private ArrayList alselectedPropertyItems = null;
		private eLabel selectedLabel = eLabel.eSmall;
		private bool legalExit = false;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Button btnOK;
		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.Button btnTest;
		private System.Windows.Forms.Label label3;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
		private System.Windows.Forms.NumericUpDown nudHorizontal;
		private System.Windows.Forms.NumericUpDown nudVertical;
		private System.Windows.Forms.PictureBox pictureBox1;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.RadioButton rbSmall;
		private System.Windows.Forms.RadioButton rbMedium;
		private System.Windows.Forms.RadioButton rbLarge;

		public OffsetData offsetData;

		public ArrayList SelectedPropertyItems
		{
			get { return this.alselectedPropertyItems; }
			set {this.alselectedPropertyItems = value; }
		}

		public offsetSettingsForm(MFW_PrintDescriptor pd)
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			offsetData = new OffsetData();
			this.Closing += new CancelEventHandler(Form_Closing);

			this.rbLarge.Enter += new EventHandler(rbLarge_Enter);
			this.rbMedium.Enter += new EventHandler(rbMedium_Enter);
			this.rbSmall.Enter += new EventHandler(rbSmall_Enter);

			//Add Picture from Resource!
			System.IO.Stream  stream;
			System.Reflection.Assembly assembly;

			assembly = System.Reflection.Assembly.GetExecutingAssembly();
			stream = assembly.GetManifestResourceStream("DealMaker.res.paper.bmp");
			if (null != stream)
			{
				this.pictureBox1.Image = System.Drawing.Image.FromStream(stream);
			}

			this.pd = pd;

		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.btnOK = new System.Windows.Forms.Button();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.pictureBox1 = new System.Windows.Forms.PictureBox();
			this.nudVertical = new System.Windows.Forms.NumericUpDown();
			this.nudHorizontal = new System.Windows.Forms.NumericUpDown();
			this.label2 = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.btnCancel = new System.Windows.Forms.Button();
			this.btnTest = new System.Windows.Forms.Button();
			this.label3 = new System.Windows.Forms.Label();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.rbLarge = new System.Windows.Forms.RadioButton();
			this.rbMedium = new System.Windows.Forms.RadioButton();
			this.rbSmall = new System.Windows.Forms.RadioButton();
			this.groupBox1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.nudVertical)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.nudHorizontal)).BeginInit();
			this.groupBox2.SuspendLayout();
			this.SuspendLayout();
			// 
			// btnOK
			// 
			this.btnOK.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnOK.Location = new System.Drawing.Point(256, 16);
			this.btnOK.Name = "btnOK";
			this.btnOK.Size = new System.Drawing.Size(88, 23);
			this.btnOK.TabIndex = 0;
			this.btnOK.Text = "OK";
			this.btnOK.Click += new System.EventHandler(this.button1_Click);
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.pictureBox1);
			this.groupBox1.Controls.Add(this.nudVertical);
			this.groupBox1.Controls.Add(this.nudHorizontal);
			this.groupBox1.Controls.Add(this.label2);
			this.groupBox1.Controls.Add(this.label1);
			this.groupBox1.Location = new System.Drawing.Point(8, 8);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(240, 240);
			this.groupBox1.TabIndex = 1;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Printer Page Offset";
			// 
			// pictureBox1
			// 
			this.pictureBox1.Location = new System.Drawing.Point(32, 40);
			this.pictureBox1.Name = "pictureBox1";
			this.pictureBox1.Size = new System.Drawing.Size(96, 136);
			this.pictureBox1.TabIndex = 4;
			this.pictureBox1.TabStop = false;
			// 
			// nudVertical
			// 
			this.nudVertical.Location = new System.Drawing.Point(144, 96);
			this.nudVertical.Maximum = new System.Decimal(new int[] {
																		10,
																		0,
																		0,
																		0});
			this.nudVertical.Minimum = new System.Decimal(new int[] {
																		10,
																		0,
																		0,
																		-2147483648});
			this.nudVertical.Name = "nudVertical";
			this.nudVertical.Size = new System.Drawing.Size(72, 20);
			this.nudVertical.TabIndex = 1;
			this.nudVertical.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.nudVertical.ValueChanged += new System.EventHandler(this.nudVertical_ValueChanged);
			this.nudVertical.Leave += new System.EventHandler(this.nudVertical_ValueChanged);
			// 
			// nudHorizontal
			// 
			this.nudHorizontal.Location = new System.Drawing.Point(40, 192);
			this.nudHorizontal.Maximum = new System.Decimal(new int[] {
																		  10,
																		  0,
																		  0,
																		  0});
			this.nudHorizontal.Minimum = new System.Decimal(new int[] {
																		  10,
																		  0,
																		  0,
																		  -2147483648});
			this.nudHorizontal.Name = "nudHorizontal";
			this.nudHorizontal.Size = new System.Drawing.Size(72, 20);
			this.nudHorizontal.TabIndex = 0;
			this.nudHorizontal.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.nudHorizontal.ValueChanged += new System.EventHandler(this.nudHorizontal_ValueChanged);
			this.nudHorizontal.Leave += new System.EventHandler(this.nudHorizontal_ValueChanged);
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(40, 176);
			this.label2.Name = "label2";
			this.label2.TabIndex = 3;
			this.label2.Text = "Horizontal Offset";
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(144, 80);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(80, 23);
			this.label1.TabIndex = 2;
			this.label1.Text = "Vertical Offset";
			// 
			// btnCancel
			// 
			this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnCancel.Location = new System.Drawing.Point(256, 48);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.Size = new System.Drawing.Size(88, 23);
			this.btnCancel.TabIndex = 2;
			this.btnCancel.Text = "Cancel";
			this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
			// 
			// btnTest
			// 
			this.btnTest.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnTest.Location = new System.Drawing.Point(8, 112);
			this.btnTest.Name = "btnTest";
			this.btnTest.Size = new System.Drawing.Size(88, 23);
			this.btnTest.TabIndex = 3;
			this.btnTest.Text = "Test";
			this.btnTest.Click += new System.EventHandler(this.btnTest_Click);
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(24, 256);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(320, 23);
			this.label3.TabIndex = 4;
			this.label3.Text = "Note: the offset you set here will apply to all label formats!";
			// 
			// groupBox2
			// 
			this.groupBox2.Controls.Add(this.rbLarge);
			this.groupBox2.Controls.Add(this.rbMedium);
			this.groupBox2.Controls.Add(this.rbSmall);
			this.groupBox2.Controls.Add(this.btnTest);
			this.groupBox2.Location = new System.Drawing.Point(256, 104);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(136, 144);
			this.groupBox2.TabIndex = 5;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "Test Settings";
			// 
			// rbLarge
			// 
			this.rbLarge.Location = new System.Drawing.Point(8, 72);
			this.rbLarge.Name = "rbLarge";
			this.rbLarge.TabIndex = 6;
			this.rbLarge.Text = "Large Labels";
			// 
			// rbMedium
			// 
			this.rbMedium.Location = new System.Drawing.Point(8, 48);
			this.rbMedium.Name = "rbMedium";
			this.rbMedium.TabIndex = 5;
			this.rbMedium.Text = "Medium Labels";
			// 
			// rbSmall
			// 
			this.rbSmall.Checked = true;
			this.rbSmall.Location = new System.Drawing.Point(8, 24);
			this.rbSmall.Name = "rbSmall";
			this.rbSmall.TabIndex = 4;
			this.rbSmall.TabStop = true;
			this.rbSmall.Text = "Small Labels";
			// 
			// offsetSettingsForm
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(210)), ((System.Byte)(216)), ((System.Byte)(240)));
			this.CancelButton = this.btnCancel;
			this.ClientSize = new System.Drawing.Size(402, 280);
			this.Controls.Add(this.groupBox2);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.btnCancel);
			this.Controls.Add(this.groupBox1);
			this.Controls.Add(this.btnOK);
			this.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
			this.Name = "offsetSettingsForm";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Page Offset Settings";
			this.Load += new System.EventHandler(this.offsetSettings_Load);
			this.groupBox1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.nudVertical)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.nudHorizontal)).EndInit();
			this.groupBox2.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		

		private void button1_Click(object sender, System.EventArgs e)
		{
			
			if(DialogResult.Yes == MessageBox.Show("Are you sure that you want to replace the old Label Alignment settings", "Replace Settings", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2))
			{
				this.legalExit = true;
				offsetData.horizontalOffset = (int)nudHorizontal.Value;
				offsetData.verticalOffset = (int)nudVertical.Value;
				offsetData.StoreOffsets();
				this.Close();
			}
		}

		private void offsetSettings_Load(object sender, System.EventArgs e)
		{
			offsetData.LoadOffsets();
			nudVertical.Value = (decimal)offsetData.verticalOffset;
			nudHorizontal.Value = (decimal)offsetData.horizontalOffset;

			this.loadedHorOffset = offsetData.horizontalOffset;
			this.loadedVertOffset = offsetData.verticalOffset;
		}

		private void btnCancel_Click(object sender, System.EventArgs e)
		{			
			if(DialogResult.Yes == MessageBox.Show("Are you sure you want to cancel and not save these settings?", "Note", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2))
			{
				if (
						this.loadedVertOffset != this.offsetData.verticalOffset ||
						this.loadedHorOffset != this.offsetData.horizontalOffset)
				{
					offsetData.horizontalOffset = loadedHorOffset;
					offsetData.verticalOffset   = loadedVertOffset;
					offsetData.StoreOffsets();					
				}
				this.legalExit = true;
				this.Close();
			}	
		}

		private void nudVertical_ValueChanged(object sender, System.EventArgs e)
		{
			if (nudVertical.Value > 10) nudVertical.Value = 10;
			if (nudVertical.Value < -10) nudVertical.Value = -10;
		}

		private void nudHorizontal_ValueChanged(object sender, System.EventArgs e)
		{
			if (nudHorizontal.Value > 10) nudHorizontal.Value = 10;
			if (nudHorizontal.Value < -10) nudHorizontal.Value = -10;
		}

		private void btnTest_Click(object sender, System.EventArgs e)
		{
			switch (this.selectedLabel)
			{
				case eLabel.eLarge:
					ExtendListWithDummyData();
					break;

				case eLabel.eMedium:
					ExtendListWithDummyData();
					break;

				case eLabel.eSmall:
					ExtendListWithDummyData();
					break;
			}
			
			// RESET
			Globals.smallLabelsCount = 0;
			Globals.mediumLabelsCount = 0;
			Globals.largeLabelsCount = 0;
			
			offsetData.horizontalOffset = (int)nudHorizontal.Value;
			offsetData.verticalOffset = (int)nudVertical.Value;

			offsetData.StoreOffsets();
			
			try 
			{
				PrintDocument tmpprndoc = new PrintDocument();
				tmpprndoc.PrintPage += new PrintPageEventHandler(toPrinter);
				PrintPreviewDialogEx tmpprdiag = new PrintPreviewDialogEx();								
				tmpprdiag.Document = tmpprndoc;
			
				tmpprdiag.ShowDialog();

			}
			catch (Exception)
			{
				//silent catch (not a mistake)
				//when no printer installed -> Print -> Cancel print -> Cancel would crash app.
			}	

		}

		private void ExtendListWithDummyData()
		{
			this.alExtendedRecords = new ArrayList();
			if (null == this.alselectedPropertyItems)
				return;

			this.alExtendedRecords = (ArrayList)this.alselectedPropertyItems.Clone();

			switch (this.selectedLabel)
			{
				case eLabel.eLarge:
					for (int i = 0; i < this.pd.StartRow - 1; i++)
					{
						for (int iColNr = 0; iColNr < 3; iColNr++)
						{
							this.alExtendedRecords.Insert(0, null);
						}
					}
					break;
				case eLabel.eMedium:
					for (int i = 0; i < this.pd.StartRow - 1; i++)
					{
						for (int iColNr = 0; iColNr < 3; iColNr++)
						{
							this.alExtendedRecords.Insert(0, null);
						}
					}
					break;
				case eLabel.eSmall:
					for (int i = 0; i < this.pd.StartRow - 1; i++)
					{
						for (int iColNr = 0; iColNr < 2; iColNr++)
						{
							this.alExtendedRecords.Insert(0, null);
						}
					}
					break;
			}
		}


		private void toPrinter(Object sender, PrintPageEventArgs e)
		{
			OffsetData offsetData = new OffsetData();
			offsetData.LoadOffsets();

			if (null == alselectedPropertyItems)
			{
				//test for dummy user with Guid.Empty as key
			
				PropertyItem dummyPi = new PropertyItem();
				dummyPi.Owner = new Contact();
				dummyPi.Owner.FullName = "John Motivated Doe";
				dummyPi.Owner.SiteAddress = new Address();
				dummyPi.Owner.SiteAddress.FullSiteStreetAddress = "123456 Great House Drive, Unit 222";
				dummyPi.Owner.SiteAddress.SiteCity = "Any City";
				dummyPi.Owner.SiteAddress.SiteZIP = "88888";
				dummyPi.Owner.SiteAddress.SiteState = "AA";
					
				switch (this.selectedLabel)
				{
					case eLabel.eLarge:
						Graphics g = ApplicationHelper.GetLargeLabelGraphics(e, dummyPi, null, offsetData.horizontalOffset, offsetData.verticalOffset, pd);
						break;

					case eLabel.eSmall:
						ApplicationHelper.GetSmallLabelGraphics(e, dummyPi, null,  offsetData.horizontalOffset, offsetData.verticalOffset, pd);
						break;

					case eLabel.eMedium:
						ApplicationHelper.GetMediumLabelGraphics(e, dummyPi, null, offsetData.horizontalOffset, offsetData.verticalOffset, pd);
						break;
				}
			}
			else
			{
				switch (this.selectedLabel)
				{
					case eLabel.eLarge:
						//Graphics g = ApplicationHelper.GetLargeLabelGraphics(e, null, alselectedPropertyItems, offsetData.horizontalOffset, offsetData.verticalOffset, pd);
						Graphics g = ApplicationHelper.GetLargeLabelGraphics(e, null, this.alExtendedRecords, offsetData.horizontalOffset, offsetData.verticalOffset, pd);
						break;

					case eLabel.eSmall:
						//ApplicationHelper.GetSmallLabelGraphics(e, null, alselectedPropertyItems,  offsetData.horizontalOffset, offsetData.verticalOffset, pd);
						ApplicationHelper.GetSmallLabelGraphics(e, null, this.alExtendedRecords,  offsetData.horizontalOffset, offsetData.verticalOffset, pd);
						break;

					case eLabel.eMedium:
						//ApplicationHelper.GetMediumLabelGraphics(e, null, alselectedPropertyItems, offsetData.horizontalOffset, offsetData.verticalOffset, pd);
						ApplicationHelper.GetMediumLabelGraphics(e, null, this.alExtendedRecords, offsetData.horizontalOffset, offsetData.verticalOffset, pd);
						break;
				}			
			}

			
		}

		private void Form_Closing(object sender, CancelEventArgs e)
		{
			if (this.legalExit != true)
			{
				if(DialogResult.Yes == MessageBox.Show("Are you sure you want to cancel and not save these settings?", "Note", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2))
				{
					if (
						this.loadedVertOffset != this.offsetData.verticalOffset ||
						this.loadedHorOffset != this.offsetData.horizontalOffset)
					{
						offsetData.horizontalOffset = loadedHorOffset;
						offsetData.verticalOffset   = loadedVertOffset;
						offsetData.StoreOffsets();					
					}
				}	
				else
				{
					e.Cancel = true;				
				}
				
			}
		}

		private void rbLarge_Enter(object sender, EventArgs e)
		{
			this.selectedLabel = eLabel.eLarge;
		}
		private void rbMedium_Enter(object sender, EventArgs e)
		{
			this.selectedLabel = eLabel.eMedium;
		}
		private void rbSmall_Enter(object sender, EventArgs e)
		{										
			this.selectedLabel = eLabel.eSmall;
		}
	}
	public class OffsetData
	{
		int _horizontalOffset = 0;
		int _verticalOffset = 0;

		/// <summary>
		/// Note: Allowed values are between -10 and 10 !
		/// </summary>
		public int horizontalOffset
		{
			get{return _horizontalOffset;}
			set
			{
				if (value >= -10 && value <= 10)
					_horizontalOffset = value;
				else
					throw new Exception("Offset Settings: Setting out of range");
			}
		}
		
		/// <summary>
		/// Note: Allowed values are between -10 and 10 !
		/// </summary>
		public int verticalOffset
		{
			get{return _verticalOffset;}
			set
			{
				if (value >= -10 && value <= 10)
					_verticalOffset = value;
				else
					throw new Exception("Offset Settings: Setting out of range");
			}
		}

		public void StoreOffsets()
		{
			System.Xml.Serialization.XmlSerializer xmlser = new System.Xml.Serialization.XmlSerializer(typeof(OffsetData));
			System.IO.FileStream fs = new System.IO.FileStream(OSFolderManager.Instance.GetPlainLocalUserAppDataPath() + @"\offsets.xml", System.IO.FileMode.Create);
			xmlser.Serialize(fs, this);
			fs.Flush();
			fs.Close();
		}
		
		public void LoadOffsets()
		{
			System.IO.FileStream fs = null;
			OffsetData storredOffset = null;;
			System.Xml.Serialization.XmlSerializer xmlser = new System.Xml.Serialization.XmlSerializer(typeof(OffsetData));
			try
			{
                fs = new System.IO.FileStream(OSFolderManager.Instance.GetPlainLocalUserAppDataPath() + @"\offsets.xml", System.IO.FileMode.Open);
				storredOffset = (OffsetData)xmlser.Deserialize(fs);

				horizontalOffset = storredOffset.horizontalOffset;
				verticalOffset = storredOffset.verticalOffset;
			}
			catch (System.IO.FileNotFoundException)
			{
				// File does not exist! this is not a problem! Default values (0,0) will be used 
			}
			catch (Exception x)
			{
				throw new Exception("Opening Print Offset File" + "\n" + x.Message + "\n" + x.StackTrace);
			}
			finally
			{
				try
				{
					fs.Close();
				}
				catch
				{
					// file not opened !
				}
			}
		}
	}
}
