using System;

namespace DealMaker
{
	/// <summary>
	/// Summary description for MFW_PrintDescriptor.
	/// </summary>
	public class MFW_PrintDescriptor
	{
		private int from = -1;
		private int to = -1;
		private int startRow = -1;

		public MFW_PrintDescriptor()
		{
		}

		public MFW_PrintDescriptor(int from, int to, int startRow)
		{
			this.from = from;
			this.to = to;
			this.startRow = startRow;	
			
			if (this.to < this.from)
			{
				int temp = this.to;
				this.to = this.from;
				this.from = temp;
			}
		}

		public int From
		{
			get {return this.from;}
		}

		public int To
		{
			get {return this.to;}
		}

		public int StartRow
		{
			get {return startRow;}
		}
	}
}
