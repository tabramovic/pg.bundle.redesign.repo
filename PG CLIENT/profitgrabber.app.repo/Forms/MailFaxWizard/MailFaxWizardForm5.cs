#region � Using �
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Printing;
using System.Data;
using System.Windows.Forms;
using System.Linq;

using DealMaker.PlainClasses.Fax;
using System.Threading;
using System.Threading.Tasks;

//Added
using ProfitGrabber.Common;
using DealMaker.PlainClasses.EmailSender;
#endregion � Using �

using DealMaker.Forms.Fax;
using System.Net;
using System.Net.Mail;
using google.oauth.lib;

#region � Namespace DealMaker �
namespace DealMaker
{
	/// <summary>
	/// Summary description for MailFaxWizardForm5.
	/// </summary>
	public class MailFaxWizardForm5 : System.Windows.Forms.UserControl
	{
		#region � Data �
		
		private DealMaker.Forms.Floaters.MFWPrintorFax floater = null;
		
		ArrayList alExtendedRecords = new ArrayList();
		MFW_PrintDescriptor pd = null;
		private ArrayList cachedOriginalSelectedItems = new ArrayList();
		bool preview = false;
		bool print = false;
		private MailFaxWizard mfwObject;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Button bPreview;
		private System.Windows.Forms.Button bPrint;
		private System.Windows.Forms.Button bFax;
		private System.Windows.Forms.TextBox tbNumberOfSelectedRecipients;
		private System.Windows.Forms.TextBox tbMarketingPiece;
		private System.Windows.Forms.Button bSetOffsets;
		private DealMaker.ReadMe rm;
		private System.Windows.Forms.CheckBox _cbUseInterfax;
		private System.Windows.Forms.Button _setupInterfaxAccount;
        private Button _exportToPDF;
        private GroupBox printingRangeGroupBox;
        private Label label9;
        private Label lNrTagRec;
        private Label label8;
        private TextBox tbTo;
        private TextBox tbFrom;
        private Label label6;
        private Label label5;
        private ComboBox cbStartRow;
        private Label label4;
        private Button _sendEmails;
        private WindowsApplication.AcquisitionCalculator.TextBox _uploading;
        private Label emailAccountWarningLabel;
        private GroupBox groupBox1;
        private Label label10;
        private Label label11;
        private Label _testEmailStatusLabel;
        private Button button1;
        private Label _responseLabel;

        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.Container components = null;
        private Label label7;
        private TextBox _testEmailToAddress;
        #endregion � Data �

        #region � Properties �

        public ArrayList   CachedOriginalSelectedItems
		{
			get {return this.cachedOriginalSelectedItems;}
		}


		public MailFaxWizard MailFaxWizardObject
		{
			get { return this.mfwObject; }
			set { this.mfwObject = value; }
		}
        #endregion � Properties �

        SMTPObject smtpSettings;

        #region � CTR && Dispose �
        public MailFaxWizardForm5(MailFaxWizard mfwObject)
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

			this.mfwObject = mfwObject;

			if (
				mfwObject.LabelDocSel == eLabelDocumentSelection.faxDocument ||
				mfwObject.LabelDocSel == eLabelDocumentSelection.mailDocument
				)
			{
				this.HidePaperFeatures();                
			}
            else
                _exportToPDF.Enabled = false;   //disable export to PDF feature.

			this.cachedOriginalSelectedItems = this.mfwObject.SelectedRecords;
			this.lNrTagRec.Text = mfwObject.SelectedRecords.Count.ToString();

			Globals.largeLabelsCount = 0;
			Globals.mediumLabelsCount = 0;
			Globals.smallLabelsCount = 0;

			this.bSetOffsets.Click += new EventHandler(bSetOffsets_EH);
			this.tbFrom.Text = "1";
			this.tbTo.Text = this.mfwObject.SelectedRecords.Count.ToString();

			this.rm.MouseEnter += new EventHandler(rm_MouseEnter);
			this.rm.MouseLeave += new EventHandler(rm_MouseLeave);

			//Interfax
			_cbUseInterfax.CheckedChanged += new EventHandler(On_UseInterfax_CheckedChanged);

			if (!InterfaxWebServiceSupport.Instance.IsInitialized)
				InterfaxWebServiceSupport.Instance.LoadIfxData();

			_cbUseInterfax.Enabled = InterfaxWebServiceSupport.Instance.IsInitialized;
            _exportToPDF.Click += new EventHandler(On_ExportToPDF_Click);
            //_sendBlindOffers.Click += _sendBlindOffers_Click;

            smtpSettings = new SMTPObject().LoadSettigns();
            _testEmailToAddress.Text = smtpSettings.TestRecipient;
        }

        
        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}
		#endregion � CTR && Dispose �

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MailFaxWizardForm5));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.bPreview = new System.Windows.Forms.Button();
            this.bPrint = new System.Windows.Forms.Button();
            this.bFax = new System.Windows.Forms.Button();
            this.tbNumberOfSelectedRecipients = new System.Windows.Forms.TextBox();
            this.tbMarketingPiece = new System.Windows.Forms.TextBox();
            this.bSetOffsets = new System.Windows.Forms.Button();
            this._cbUseInterfax = new System.Windows.Forms.CheckBox();
            this._setupInterfaxAccount = new System.Windows.Forms.Button();
            this._exportToPDF = new System.Windows.Forms.Button();
            this.printingRangeGroupBox = new System.Windows.Forms.GroupBox();
            this.label9 = new System.Windows.Forms.Label();
            this.lNrTagRec = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.tbTo = new System.Windows.Forms.TextBox();
            this.tbFrom = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.cbStartRow = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this._sendEmails = new System.Windows.Forms.Button();
            this._uploading = new WindowsApplication.AcquisitionCalculator.TextBox();
            this.emailAccountWarningLabel = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this._responseLabel = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this._testEmailStatusLabel = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.rm = new DealMaker.ReadMe();
            this._testEmailToAddress = new System.Windows.Forms.TextBox();
            this.printingRangeGroupBox.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label1.Location = new System.Drawing.Point(3, 3);
            this.label1.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(128, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Print, Fax or Email:";
            // 
            // label2
            // 
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label2.Location = new System.Drawing.Point(12, 24);
            this.label2.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(165, 16);
            this.label2.TabIndex = 1;
            this.label2.Text = "Number of selected recipients:";
            // 
            // label3
            // 
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label3.Location = new System.Drawing.Point(12, 46);
            this.label3.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(165, 16);
            this.label3.TabIndex = 2;
            this.label3.Text = "Selected Marketing Piece:";
            // 
            // bPreview
            // 
            this.bPreview.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.bPreview.Location = new System.Drawing.Point(4, 76);
            this.bPreview.Margin = new System.Windows.Forms.Padding(1);
            this.bPreview.Name = "bPreview";
            this.bPreview.Size = new System.Drawing.Size(76, 23);
            this.bPreview.TabIndex = 3;
            this.bPreview.Text = "Preview";
            this.bPreview.Click += new System.EventHandler(this.bPreview_Click);
            // 
            // bPrint
            // 
            this.bPrint.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.bPrint.Location = new System.Drawing.Point(84, 76);
            this.bPrint.Margin = new System.Windows.Forms.Padding(1);
            this.bPrint.Name = "bPrint";
            this.bPrint.Size = new System.Drawing.Size(76, 23);
            this.bPrint.TabIndex = 4;
            this.bPrint.Text = "Print Letters";
            this.bPrint.Click += new System.EventHandler(this.bPrint_Click);
            // 
            // bFax
            // 
            this.bFax.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.bFax.Location = new System.Drawing.Point(163, 76);
            this.bFax.Margin = new System.Windows.Forms.Padding(1);
            this.bFax.Name = "bFax";
            this.bFax.Size = new System.Drawing.Size(76, 23);
            this.bFax.TabIndex = 5;
            this.bFax.Text = "Fax";
            this.bFax.Click += new System.EventHandler(this.bPrint_Click);
            // 
            // tbNumberOfSelectedRecipients
            // 
            this.tbNumberOfSelectedRecipients.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbNumberOfSelectedRecipients.Location = new System.Drawing.Point(170, 24);
            this.tbNumberOfSelectedRecipients.Margin = new System.Windows.Forms.Padding(1);
            this.tbNumberOfSelectedRecipients.Name = "tbNumberOfSelectedRecipients";
            this.tbNumberOfSelectedRecipients.ReadOnly = true;
            this.tbNumberOfSelectedRecipients.Size = new System.Drawing.Size(39, 20);
            this.tbNumberOfSelectedRecipients.TabIndex = 6;
            this.tbNumberOfSelectedRecipients.TabStop = false;
            // 
            // tbMarketingPiece
            // 
            this.tbMarketingPiece.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbMarketingPiece.Location = new System.Drawing.Point(170, 45);
            this.tbMarketingPiece.Margin = new System.Windows.Forms.Padding(1);
            this.tbMarketingPiece.Name = "tbMarketingPiece";
            this.tbMarketingPiece.ReadOnly = true;
            this.tbMarketingPiece.Size = new System.Drawing.Size(246, 20);
            this.tbMarketingPiece.TabIndex = 7;
            this.tbMarketingPiece.TabStop = false;
            // 
            // bSetOffsets
            // 
            this.bSetOffsets.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.bSetOffsets.Location = new System.Drawing.Point(5, 120);
            this.bSetOffsets.Margin = new System.Windows.Forms.Padding(1);
            this.bSetOffsets.Name = "bSetOffsets";
            this.bSetOffsets.Size = new System.Drawing.Size(154, 23);
            this.bSetOffsets.TabIndex = 9;
            this.bSetOffsets.Text = "Label Alignment";
            // 
            // _cbUseInterfax
            // 
            this._cbUseInterfax.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._cbUseInterfax.Location = new System.Drawing.Point(164, 100);
            this._cbUseInterfax.Margin = new System.Windows.Forms.Padding(1);
            this._cbUseInterfax.Name = "_cbUseInterfax";
            this._cbUseInterfax.Size = new System.Drawing.Size(117, 18);
            this._cbUseInterfax.TabIndex = 44;
            this._cbUseInterfax.Text = "Use Interfax account";
            // 
            // _setupInterfaxAccount
            // 
            this._setupInterfaxAccount.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._setupInterfaxAccount.Location = new System.Drawing.Point(164, 120);
            this._setupInterfaxAccount.Margin = new System.Windows.Forms.Padding(1);
            this._setupInterfaxAccount.Name = "_setupInterfaxAccount";
            this._setupInterfaxAccount.Size = new System.Drawing.Size(154, 23);
            this._setupInterfaxAccount.TabIndex = 47;
            this._setupInterfaxAccount.Text = "Setup Interfax account";
            this._setupInterfaxAccount.Click += new System.EventHandler(this._setupInterfaxAccount_Click);
            // 
            // _exportToPDF
            // 
            this._exportToPDF.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._exportToPDF.Location = new System.Drawing.Point(243, 77);
            this._exportToPDF.Margin = new System.Windows.Forms.Padding(1);
            this._exportToPDF.Name = "_exportToPDF";
            this._exportToPDF.Size = new System.Drawing.Size(76, 23);
            this._exportToPDF.TabIndex = 48;
            this._exportToPDF.Text = "Export to PDF";
            // 
            // printingRangeGroupBox
            // 
            this.printingRangeGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.printingRangeGroupBox.Controls.Add(this.label9);
            this.printingRangeGroupBox.Controls.Add(this.lNrTagRec);
            this.printingRangeGroupBox.Controls.Add(this.label8);
            this.printingRangeGroupBox.Controls.Add(this.tbTo);
            this.printingRangeGroupBox.Controls.Add(this.tbFrom);
            this.printingRangeGroupBox.Controls.Add(this.label6);
            this.printingRangeGroupBox.Controls.Add(this.label5);
            this.printingRangeGroupBox.Controls.Add(this.cbStartRow);
            this.printingRangeGroupBox.Controls.Add(this.label4);
            this.printingRangeGroupBox.Location = new System.Drawing.Point(6, 169);
            this.printingRangeGroupBox.Margin = new System.Windows.Forms.Padding(1);
            this.printingRangeGroupBox.Name = "printingRangeGroupBox";
            this.printingRangeGroupBox.Padding = new System.Windows.Forms.Padding(1);
            this.printingRangeGroupBox.Size = new System.Drawing.Size(553, 81);
            this.printingRangeGroupBox.TabIndex = 52;
            this.printingRangeGroupBox.TabStop = false;
            this.printingRangeGroupBox.Text = "Select Printing Range";
            // 
            // label9
            // 
            this.label9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label9.Location = new System.Drawing.Point(210, 55);
            this.label9.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(124, 22);
            this.label9.TabIndex = 58;
            this.label9.Text = "(Label Saving Feature)";
            // 
            // lNrTagRec
            // 
            this.lNrTagRec.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lNrTagRec.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.lNrTagRec.Location = new System.Drawing.Point(288, 29);
            this.lNrTagRec.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.lNrTagRec.Name = "lNrTagRec";
            this.lNrTagRec.Size = new System.Drawing.Size(33, 20);
            this.lNrTagRec.TabIndex = 57;
            this.lNrTagRec.Text = "_";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label8.Location = new System.Drawing.Point(261, 31);
            this.label8.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(19, 13);
            this.label8.TabIndex = 56;
            this.label8.Text = "of ";
            // 
            // tbTo
            // 
            this.tbTo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbTo.Location = new System.Drawing.Point(220, 29);
            this.tbTo.Margin = new System.Windows.Forms.Padding(1);
            this.tbTo.Name = "tbTo";
            this.tbTo.Size = new System.Drawing.Size(33, 20);
            this.tbTo.TabIndex = 55;
            // 
            // tbFrom
            // 
            this.tbFrom.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbFrom.Location = new System.Drawing.Point(150, 29);
            this.tbFrom.Margin = new System.Windows.Forms.Padding(1);
            this.tbFrom.Name = "tbFrom";
            this.tbFrom.Size = new System.Drawing.Size(33, 20);
            this.tbFrom.TabIndex = 54;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label6.Location = new System.Drawing.Point(194, 31);
            this.label6.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(23, 13);
            this.label6.TabIndex = 53;
            this.label6.Text = "To:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label5.Location = new System.Drawing.Point(18, 31);
            this.label5.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(96, 13);
            this.label5.TabIndex = 52;
            this.label5.Text = "Start From Record:";
            // 
            // cbStartRow
            // 
            this.cbStartRow.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10"});
            this.cbStartRow.Location = new System.Drawing.Point(150, 54);
            this.cbStartRow.Margin = new System.Windows.Forms.Padding(1);
            this.cbStartRow.Name = "cbStartRow";
            this.cbStartRow.Size = new System.Drawing.Size(55, 21);
            this.cbStartRow.TabIndex = 51;
            this.cbStartRow.Text = "1";
            // 
            // label4
            // 
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label4.Location = new System.Drawing.Point(18, 55);
            this.label4.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(84, 15);
            this.label4.TabIndex = 50;
            this.label4.Text = "Starting Row:";
            // 
            // _sendEmails
            // 
            this._sendEmails.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._sendEmails.AutoSize = true;
            this._sendEmails.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._sendEmails.Location = new System.Drawing.Point(473, 376);
            this._sendEmails.Margin = new System.Windows.Forms.Padding(1);
            this._sendEmails.Name = "_sendEmails";
            this._sendEmails.Size = new System.Drawing.Size(76, 23);
            this._sendEmails.TabIndex = 56;
            this._sendEmails.Text = "Send";
            this._sendEmails.Click += new System.EventHandler(this._sendEmails_Click);
            // 
            // _uploading
            // 
            this._uploading.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._uploading.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this._uploading.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this._uploading.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._uploading.Location = new System.Drawing.Point(272, 400);
            this._uploading.Multiline = true;
            this._uploading.Name = "_uploading";
            this._uploading.Size = new System.Drawing.Size(281, 0);
            this._uploading.TabIndex = 58;
            // 
            // emailAccountWarningLabel
            // 
            this.emailAccountWarningLabel.Enabled = false;
            this.emailAccountWarningLabel.ForeColor = System.Drawing.Color.Red;
            this.emailAccountWarningLabel.Location = new System.Drawing.Point(12, 385);
            this.emailAccountWarningLabel.Name = "emailAccountWarningLabel";
            this.emailAccountWarningLabel.Size = new System.Drawing.Size(60, 15);
            this.emailAccountWarningLabel.TabIndex = 59;
            this.emailAccountWarningLabel.Text = "Click to read this WARNING before sending!";
            this.emailAccountWarningLabel.Visible = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this._testEmailToAddress);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this._responseLabel);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this._testEmailStatusLabel);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Location = new System.Drawing.Point(9, 256);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(544, 116);
            this.groupBox1.TabIndex = 60;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Send test email to verify your account";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.ForeColor = System.Drawing.Color.Red;
            this.label7.Location = new System.Drawing.Point(5, 96);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(521, 13);
            this.label7.TabIndex = 63;
            this.label7.Text = " If you are having difficulty authenticating your account, please view the Video " +
    "Training under the Help menu.";
            // 
            // _responseLabel
            // 
            this._responseLabel.AutoSize = true;
            this._responseLabel.Location = new System.Drawing.Point(105, 42);
            this._responseLabel.Name = "_responseLabel";
            this._responseLabel.Size = new System.Drawing.Size(10, 13);
            this._responseLabel.TabIndex = 62;
            this._responseLabel.Text = "-";
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.AutoSize = true;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.button1.Location = new System.Drawing.Point(220, 16);
            this.button1.Margin = new System.Windows.Forms.Padding(1);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(98, 23);
            this.button1.TabIndex = 61;
            this.button1.Text = "Send Test Email";
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // _testEmailStatusLabel
            // 
            this._testEmailStatusLabel.AutoSize = true;
            this._testEmailStatusLabel.Location = new System.Drawing.Point(7, 42);
            this._testEmailStatusLabel.Name = "_testEmailStatusLabel";
            this._testEmailStatusLabel.Size = new System.Drawing.Size(92, 13);
            this._testEmailStatusLabel.TabIndex = 61;
            this._testEmailStatusLabel.Text = "Test Email Status:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(7, 61);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(496, 13);
            this.label10.TabIndex = 2;
            this.label10.Text = "If email needs to be authenticated, your browser will open and you must follow th" +
    "e steps in your browser.";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(7, 78);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(407, 13);
            this.label11.TabIndex = 3;
            this.label11.Text = "If Test Email Status is Email Sent, you may continue to Send Emails To Homeowners" +
    ".";
            // 
            // rm
            // 
            this.rm.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.rm.AutoSize = true;
            this.rm.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.rm.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("rm.BackgroundImage")));
            this.rm.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.rm.Location = new System.Drawing.Point(540, 3);
            this.rm.Margin = new System.Windows.Forms.Padding(1);
            this.rm.Name = "rm";
            this.rm.Size = new System.Drawing.Size(19, 20);
            this.rm.TabIndex = 40;
            // 
            // _testEmailToAddress
            // 
            this._testEmailToAddress.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._testEmailToAddress.Location = new System.Drawing.Point(6, 18);
            this._testEmailToAddress.Margin = new System.Windows.Forms.Padding(1);
            this._testEmailToAddress.Name = "_testEmailToAddress";
            this._testEmailToAddress.Size = new System.Drawing.Size(208, 20);
            this._testEmailToAddress.TabIndex = 59;
            // 
            // MailFaxWizardForm5
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.emailAccountWarningLabel);
            this.Controls.Add(this._sendEmails);
            this.Controls.Add(this.printingRangeGroupBox);
            this.Controls.Add(this._exportToPDF);
            this.Controls.Add(this._setupInterfaxAccount);
            this.Controls.Add(this._cbUseInterfax);
            this.Controls.Add(this.rm);
            this.Controls.Add(this.tbMarketingPiece);
            this.Controls.Add(this.tbNumberOfSelectedRecipients);
            this.Controls.Add(this.bFax);
            this.Controls.Add(this.bSetOffsets);
            this.Controls.Add(this.bPrint);
            this.Controls.Add(this.bPreview);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this._uploading);
            this.Margin = new System.Windows.Forms.Padding(1);
            this.Name = "MailFaxWizardForm5";
            this.Size = new System.Drawing.Size(563, 409);
            this.Load += new System.EventHandler(this.MailFaxWizardForm5_Load);
            this.printingRangeGroupBox.ResumeLayout(false);
            this.printingRangeGroupBox.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		#endregion

		#region � Methods �
		#region � MailFaxWizardForm5_Load �
		private void MailFaxWizardForm5_Load(object sender, System.EventArgs e)
		{
			this.tbNumberOfSelectedRecipients.Text = this.mfwObject.SelectedRecords.Count.ToString();
			this.tbMarketingPiece.Text = System.IO.Path.GetFileName(this.mfwObject.DocumentName);
			
			switch (this.mfwObject.LabelDocSel)
			{
				case eLabelDocumentSelection.labelLarge:
					this.bFax.Enabled = false;
					this.bPrint.Enabled	= true;
                    _sendEmails.Visible = false;
                    //emailAccountWarningLabel.Visible = false;
                    printingRangeGroupBox.Text = "Select Printing Range";
                    break;

				case eLabelDocumentSelection.labelMedium:
					this.bFax.Enabled = false;
					this.bPrint.Enabled	= true;
                    _sendEmails.Visible = false;
                    //emailAccountWarningLabel.Visible = false;
                    printingRangeGroupBox.Text = "Select Printing Range";
                    break;

				case eLabelDocumentSelection.labelSmall:
					this.bFax.Enabled = false;
					this.bPrint.Enabled	= true;
                    _sendEmails.Visible = false;
                    //emailAccountWarningLabel.Visible = false;
                    printingRangeGroupBox.Text = "Select Printing Range";
                    break;

				case eLabelDocumentSelection.mailDocument:
					this.bFax.Enabled = false;
					this.bPrint.Enabled	= true;
                    _sendEmails.Visible = false;
                    //emailAccountWarningLabel.Visible = false;
                    printingRangeGroupBox.Text = "Select Printing Range";
                    break;

				case eLabelDocumentSelection.faxDocument:
					this.bFax.Enabled = true;
					this.bPrint.Enabled	= false;
                    _sendEmails.Visible = false;
                    //emailAccountWarningLabel.Visible = false;
                    printingRangeGroupBox.Text = "Select Printing Range";
                    break;

                case eLabelDocumentSelection.emailLOI:
                    //bPreview.Enabled = true;
                    bPrint.Enabled = false;
                    bFax.Enabled = false;
                    _exportToPDF.Enabled = false;
                    _cbUseInterfax.Enabled = false;
                    bSetOffsets.Enabled = false;
                    _setupInterfaxAccount.Enabled = false;
                    //printingRangeGroupBox.Visible = false;
                    printingRangeGroupBox.Text = "Select Recipient Range";
                    HidePaperFeatures();
                    _sendEmails.Text = "Send Emails To Agents";
                    //emailAccountWarningLabel.Visible = true;
                    break;

                case eLabelDocumentSelection.emailBlast:
                    bPreview.Enabled = false;
                    bPrint.Enabled = false;
                    bFax.Enabled = false;
                    _exportToPDF.Enabled = false;
                    _cbUseInterfax.Enabled = false;
                    bSetOffsets.Enabled = false;
                    _setupInterfaxAccount.Enabled = false;
                    //printingRangeGroupBox.Visible = false;
                    printingRangeGroupBox.Text = "Select Recipient Range";
                    HidePaperFeatures();
                    _sendEmails.Text = "Send Emails To Homeowners";
                    //emailAccountWarningLabel.Visible = true;
                    break;
                
			}
		}
		#endregion � MailFaxWizardForm5_Load �
		#region � bPreviewAndPrint �
		private void bPreviewAndPrint(object sender, System.EventArgs e)
		{
			try 
			{
				switch (this.mfwObject.LabelDocSel)
				{
				   case eLabelDocumentSelection.labelLarge:
						ExtendListWithDummyData();
						break;

					case eLabelDocumentSelection.labelMedium:
						ExtendListWithDummyData();
						break;

					case eLabelDocumentSelection.labelSmall:
						ExtendListWithDummyData();
						break;
				}

				PrintDocument tmpprndoc = new PrintDocument();
				tmpprndoc.PrintPage += new PrintPageEventHandler(toPrinter);

				// For debugging purposes, only print to the PrintPreview 

				if (true == this.preview)
				{
					PrintPreviewDialogEx tmpprdiag = new PrintPreviewDialogEx();								
					tmpprdiag.Document = tmpprndoc;
				
					tmpprdiag.ShowDialog();
				}
				if (true == this.print && false == this.preview)
				{					
                    PrintDialog printDialog = new PrintDialog();
                    printDialog.Document = tmpprndoc;                    
                    if (DialogResult.OK == printDialog.ShowDialog())                    
                        tmpprndoc.Print();                    
				}
			}
			catch (Exception)
			{
				//silent catch (not a mistake)
				//when no printer installed -> Print -> Cancel print -> Cancel would crash app.
			}			
		}
		#endregion � bPreviewAndPrint �
		
		#region � OLD toPrinter �
		/*
		#region � toPrinter �
		private void toPrinter(Object sender, PrintPageEventArgs e)
		{
			switch (this.mfwObject.LabelDocSel)
			{
				case eLabelDocumentSelection.labelLarge:
					Graphics g = ApplicationHelper.GetLargeLabelGraphics(e, this.mfwObject.SelectedRecords);
					break;

				case eLabelDocumentSelection.labelMedium:
					ApplicationHelper.GetMediumLabelGraphics(e, this.mfwObject.SelectedRecords);
					break;

				case eLabelDocumentSelection.labelSmall:
					ApplicationHelper.GetSmallLabelGraphics(e, this.mfwObject.SelectedRecords);
					break;

				case eLabelDocumentSelection.mailDocument:
					break;

				case eLabelDocumentSelection.faxDocument:
					break;
			}												
		}
		#endregion � toPrinter �
		*/
		#endregion � OLD toPrinter �

		private void ExtendListWithDummyData()
		{
			this.alExtendedRecords = new ArrayList();
			this.alExtendedRecords = this.mfwObject.SelectedRecords;

			switch (this.mfwObject.LabelDocSel)
			{
				case eLabelDocumentSelection.labelLarge:
					for (int i = 0; i < this.pd.StartRow - 1; i++)
					{
						for (int iColNr = 0; iColNr < 3; iColNr++)
						{
							this.alExtendedRecords.Insert(0, null);
						}
					}
					break;
				case eLabelDocumentSelection.labelMedium:
					for (int i = 0; i < this.pd.StartRow - 1; i++)
					{
						for (int iColNr = 0; iColNr < 3; iColNr++)
						{
							this.alExtendedRecords.Insert(0, null);
						}
					}
					break;
				case eLabelDocumentSelection.labelSmall:
					for (int i = 0; i < this.pd.StartRow - 1; i++)
					{
						for (int iColNr = 0; iColNr < 2; iColNr++)
						{
							this.alExtendedRecords.Insert(0, null);
						}
					}
					break;
			}
		}

        

		#region � toPrinter �
		private void toPrinter(Object sender, PrintPageEventArgs e)
		{
			OffsetData offsetData = new OffsetData();
			offsetData.LoadOffsets();
			
			switch (this.mfwObject.LabelDocSel)
			{
				case eLabelDocumentSelection.labelLarge:					
					//Graphics g = ApplicationHelper.GetLargeLabelGraphics(e, null, this.mfwObject.SelectedRecords, offsetData.horizontalOffset, offsetData.verticalOffset, pd);					
					Graphics g = ApplicationHelper.GetLargeLabelGraphics(e, null, this.alExtendedRecords, offsetData.horizontalOffset, offsetData.verticalOffset, pd);
					break;

				case eLabelDocumentSelection.labelMedium:
					//ApplicationHelper.GetMediumLabelGraphics(e, null, this.mfwObject.SelectedRecords, offsetData.horizontalOffset, offsetData.verticalOffset, pd);					
					ApplicationHelper.GetMediumLabelGraphics(e, null, this.alExtendedRecords, offsetData.horizontalOffset, offsetData.verticalOffset, pd);
					break;

				case eLabelDocumentSelection.labelSmall:
					//ApplicationHelper.GetSmallLabelGraphics(e, null, this.mfwObject.SelectedRecords, offsetData.horizontalOffset, offsetData.verticalOffset, pd);					
					ApplicationHelper.GetSmallLabelGraphics(e, null, this.alExtendedRecords, offsetData.horizontalOffset, offsetData.verticalOffset, pd);
					break;

				case eLabelDocumentSelection.mailDocument:
					break;

				case eLabelDocumentSelection.faxDocument:
					break;
			}
											
		}
		#endregion � toPrinter �

		#region � bPreview_Click �
		private void bPreview_Click(object sender, System.EventArgs e)
		{
			// RESET
			Globals.smallLabelsCount = 0;
			Globals.mediumLabelsCount = 0;
			Globals.largeLabelsCount = 0;	

			this.pd = this.FillPrintDescriptor();
			if (null == pd)
				return;

			this.FilterItems();
			
			Globals.largeLabelsCount = 0;
			Globals.mediumLabelsCount = 0;
			Globals.smallLabelsCount = 0;
			
			switch (this.mfwObject.LabelDocSel)
			{
				case eLabelDocumentSelection.mailDocument:
                case eLabelDocumentSelection.emailBlast:
                case eLabelDocumentSelection.emailLOI:
                    //call txTextControl
                    MailFaxWizardPrintFaxForm mfwpff = new MailFaxWizardPrintFaxForm();
					mfwpff.MailFaxWizardObject = this.MailFaxWizardObject;
					mfwpff.Preview = true;
					mfwpff.ShowDialog();
					break;
				case  eLabelDocumentSelection.faxDocument:
					//call txTextControl
					mfwpff = new MailFaxWizardPrintFaxForm();
					mfwpff.MailFaxWizardObject = this.MailFaxWizardObject;
					mfwpff.Preview = true;
					mfwpff.Fax = true;
					mfwpff.ShowDialog();
					break;

				default:
					this.print= false;
					this.preview= true;
					this.bPreviewAndPrint(sender, e);
					return;

			}
		}
		#endregion � bPreview_Click �
		#region � bPrint_Click �
		private void bPrint_Click(object sender, System.EventArgs e)
		{
			//Interfax console
			if (InterfaxWebServiceSupport.Instance.UseIfxInterface)
			{
				//ShowConsole
				InterfaxConsoleWindow ifcw = new InterfaxConsoleWindow();
				ifcw.TopMost = true;
				ifcw.Show();					
			}
			
			// RESET
			Globals.smallLabelsCount = 0;
			Globals.mediumLabelsCount = 0;
			Globals.largeLabelsCount = 0;	

			this.pd = this.FillPrintDescriptor();
			if (null == pd)
				return;			

			this.FilterItems();
			
			Globals.largeLabelsCount = 0;
			Globals.mediumLabelsCount = 0;
			Globals.smallLabelsCount = 0;

			switch (this.mfwObject.LabelDocSel)
			{
				case eLabelDocumentSelection.mailDocument:					
					//call txTextControl
					MailFaxWizardPrintFaxForm mfwpff = new MailFaxWizardPrintFaxForm();
					mfwpff.MailFaxWizardObject = this.MailFaxWizardObject;
					mfwpff.ShowDialog();
					break;
				case  eLabelDocumentSelection.faxDocument:
					//call txTextControl
					mfwpff = new MailFaxWizardPrintFaxForm();
					mfwpff.MailFaxWizardObject = this.MailFaxWizardObject;
					mfwpff.Fax = true;
					mfwpff.ShowDialog();
					break;

				default:
					this.print= true;
					this.preview= false;
					this.bPreviewAndPrint(sender, e);
					return;
			}
		}
		#endregion � bPrint_Click �

		
		#endregion � Methods �

		private void bSetOffsets_EH(object sender, EventArgs e)
		{
			// RESET
			Globals.smallLabelsCount = 0;
			Globals.mediumLabelsCount = 0;
			Globals.largeLabelsCount = 0;	
			
			this.pd = this.FillPrintDescriptor();
			if (null == pd)
				return;

			this.FilterItems();

			offsetSettingsForm osf = new offsetSettingsForm(this.pd);
			osf.SelectedPropertyItems = this.mfwObject.SelectedRecords;
			osf.ShowDialog();
		}		

		private void tbTo_Leave(object sender, System.EventArgs e)
		{
			try
			{
				Int32 val = Convert.ToInt32(this.tbTo.Text);
				if (val > this.cachedOriginalSelectedItems.Count)
				{
					MessageBox.Show("Please select a value in range from 1 to " + this.cachedOriginalSelectedItems.Count.ToString(), "Value out of range", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}			
			}
			catch(Exception)
			{
				MessageBox.Show("Please select a value in range from 1 to " + this.cachedOriginalSelectedItems.Count.ToString(), "Value out of range", MessageBoxButtons.OK, MessageBoxIcon.Information);			
				this.tbTo.Focus();
				this.tbTo.SelectAll();
			}			
		}

		private void tbFrom_Leave(object sender, System.EventArgs e)
		{
			try
			{
				Int32 val = Convert.ToInt32(this.tbFrom.Text);
				if (val > this.cachedOriginalSelectedItems.Count)
				{
					MessageBox.Show("Please select a value in range from 1 to " + this.cachedOriginalSelectedItems.Count.ToString(), "Value out of range", MessageBoxButtons.OK, MessageBoxIcon.Information);
					return;
				}				
			}
			catch(Exception)
			{
				MessageBox.Show("Please select a value in range from 1 to " + this.cachedOriginalSelectedItems.Count, "Value out of range", MessageBoxButtons.OK, MessageBoxIcon.Information);			
				this.tbFrom.Focus();
				this.tbFrom.SelectAll();
			}			
		}

		private MFW_PrintDescriptor FillPrintDescriptor()
		{
			MFW_PrintDescriptor pd = null;
			
			try
			{
				int fromVal = Convert.ToInt32(this.tbFrom.Text);
				int toVal = Convert.ToInt32(this.tbTo.Text);

				if (fromVal > toVal)
				{
					MessageBox.Show(this, "From Value is larger than To Value!", "Value out of range");
					return null;
				}

				pd = new MFW_PrintDescriptor(
					Convert.ToInt32(this.tbFrom.Text), 
					Convert.ToInt32(this.tbTo.Text),
					Convert.ToInt32((string)this.cbStartRow.SelectedItem)
					)  ;
				}
			catch (Exception)
			{
				MessageBox.Show("Please select a value in range from 1 to " + this.mfwObject.SelectedRecords.Count, "Value out of range", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
			return pd;
		}
		
		private void FilterItems()
		{
			ArrayList alFilteredItems = new ArrayList();
			for (int i = this.pd.From - 1; i < this.pd.To; i++)
			{
				alFilteredItems.Add(this.cachedOriginalSelectedItems[i]);
			}
			this.mfwObject.SelectedRecords = alFilteredItems;
		}

		private void HidePaperFeatures()
		{
			//this.label7.Visible = false;
			//this.label5.Visible = false;
			//this.label6.Visible = false;
			//this.label8.Visible = false;
			this.label4.Visible = false;
			this.label9.Visible = false;
			//this.lNrTagRec.Visible = false;
			//this.tbFrom.Visible = false;
			//this.tbTo.Visible = false;
			this.cbStartRow.Visible = false;

			this.bSetOffsets.Visible = false;
		}

		private void rm_MouseEnter(object sender, EventArgs e)
		{
			this.floater = new DealMaker.Forms.Floaters.MFWPrintorFax();


			Point pReadMe = this.rm.Location;
			Size fSize = this.floater.Size;
						
			//this.floater.Location = new Point(pReadMe.X - fSize.Width, pReadMe.Y + this.readMe1.Height);
			this.floater.Location = PointToScreen(new Point (pReadMe.X - fSize.Width, pReadMe.Y) );

			this.floater.Show();
		}

		private void rm_MouseLeave(object sender, EventArgs e)
		{
			this.floater.Hide();		
		}

		//Interfax
		private void On_UseInterfax_CheckedChanged(object sender, EventArgs e)
		{
			if (_cbUseInterfax.Checked)
			{
				if (InterfaxWebServiceSupport.Instance.IsInitialized)
					InterfaxWebServiceSupport.Instance.UseIfxInterface = true;
			}
			else			
				InterfaxWebServiceSupport.Instance.UseIfxInterface = false;
			
		}

		private void _setupInterfaxAccount_Click(object sender, System.EventArgs e)
		{
			SetupInterfaxAccountForm form = new SetupInterfaxAccountForm();
			form.ShowDialog();

			if (InterfaxWebServiceSupport.Instance.IsInitialized)
				_cbUseInterfax.Enabled = true;
		}

        void ExportToPDF(bool autoClose, bool appendPGID)
        {
            string staticAttach = null;
            this.pd = this.FillPrintDescriptor();
            if (null == pd)
                return;

            this.FilterItems();

            if (!autoClose && !appendPGID)
            {
                MailFaxWizardPrintFaxForm mfwpff2 = new MailFaxWizardPrintFaxForm();
                mfwpff2.MailFaxWizardObject = this.MailFaxWizardObject;

                FolderBrowserDialog fbd = new FolderBrowserDialog();
                fbd.Description = "Please select or make a new folder to store the PDF export files";

                if (DialogResult.OK == fbd.ShowDialog())
                {
                    fbd.Description = "Set Export Directory";
                    fbd.ShowNewFolderButton = true;
                    mfwpff2.PDFPath = fbd.SelectedPath;

                    mfwpff2.ShowDialog();
                }
                return;
            }

            MailFaxWizardPrintFaxForm mfwpff = null;
            if (string.IsNullOrEmpty(this.MailFaxWizardObject.DocumentName) && this.MailFaxWizardObject.LabelDocSel == eLabelDocumentSelection.emailBlast)
                goto PlainTextEmail;

            var fileExt = System.IO.Path.GetExtension(this.MailFaxWizardObject.DocumentName);
            List<string> supportedFileTypes = new List<string>() { ".pdf", ".doc", ".docx", ".xls", ".xlsx", ".jpg", ".png" };
            if (supportedFileTypes.Contains(fileExt) && this.MailFaxWizardObject.LabelDocSel == eLabelDocumentSelection.emailBlast)
            {
                staticAttach = this.MailFaxWizardObject.DocumentName;
                goto PlainTextEmailWithStaticAttach;
            }

            mfwpff = new MailFaxWizardPrintFaxForm();
            mfwpff.MailFaxWizardObject = this.MailFaxWizardObject;

            mfwpff.PDFPath = $"{Application.StartupPath}" + @"\DocsToEmail\" + $"{DateTime.Now.ToString("yyyy-MM-dd hh-mm-ss")}";
            mfwpff.AutoClose = autoClose;
            mfwpff.AppendPGID = appendPGID;

            mfwpff.ShowDialog();

            PlainTextEmailWithStaticAttach:            
            PlainTextEmail:
            var smtpObj = new SMTPObject().LoadSettigns();

            int portAsInt;
            //if (!int.TryParse(smtpObj.Port, out portAsInt))
            //    portAsInt = 0;

            //AccountClient ac = new AccountClient(Globals.RegistrationRegistration.KeyCode, Globals.UserRegistration.IDUser, 
            //                                        smtpObj.OutgoingSMTPServer, portAsInt, smtpObj.EnableSSL, smtpObj.DisplayName,             
            //                                        smtpObj.ReplyTo, smtpObj.AccountName, smtpObj.EmailAddress, smtpObj.Password);
            try
            {
                //if (!ac.Upload())
                //{
                //    MessageBox.Show($"Sending account details failed, please try later.");
                //    return;
                //}

                string selectedTemplate;
                string displayName;
                string replyTo;

                selectedTemplate = MailFaxWizardObject.DocumentName;
                displayName = !string.IsNullOrEmpty(mfwObject.DisplayName) ? mfwObject.DisplayName : smtpObj.DisplayName;
                replyTo = !string.IsNullOrEmpty(mfwObject.ReplyTo) ? mfwObject.ReplyTo : smtpObj.ReplyTo;

                //if still empty, default to email address
                if (string.Empty == replyTo)
                    replyTo = smtpObj.EmailAddress;

                BlastMailTo blastTo = (eLabelDocumentSelection.emailLOI == mfwObject.LabelDocSel) ? BlastMailTo.eAgent : BlastMailTo.eOwner;

                totalRecords = MailFaxWizardObject.SelectedRecords.Count;

                List<KeyValuePair<Guid, Tuple<string, string>>> FilteredEmailRecords = new List<KeyValuePair<Guid, Tuple<string, string>>>();

                //begin - ugggg, ugly hack to support simulated load from TaskManager's click on Mail / Email / Fax
                if (null == MailFaxWizardObject.EmailRecords || 0 == MailFaxWizardObject.EmailRecords.Count)
                {
                    Dictionary<Guid, string> addresses;
                    var selectedRecords = MailFaxWizardObject.SelectedRecords.OfType<Guid>().ToList();
                    MailFaxWizardObject.EmailRecords = PlainClasses.Utils.EmailBlastSupport.GetAssignedEmails(selectedRecords, out addresses);

                    MailFaxWizardObject.AddressRecords = addresses;
                }
                //end

                foreach (var sr in MailFaxWizardObject.SelectedRecords)
                {
                    var filteredEmailRecords = MailFaxWizardObject.EmailRecords.Where(o => o.Key.Equals(sr));
                    if (filteredEmailRecords.Any())
                    {
                        var val = filteredEmailRecords.Select(er => er).Single();
                        FilteredEmailRecords.Add(val);
                    }                    
                }

                //if ("own" == (string)DealMaker.Properties.Settings.Default["BlindOffersEmailAccount"])
                {
                    DealMaker.Forms.Task2Email.EmailProcessorConsole console = new Forms.Task2Email.EmailProcessorConsole();
                    console.Show();
                }

                System.Threading.Tasks.Task.Factory.StartNew(() =>
                {
                    EmailSenderClient esw = new EmailSenderClient(staticAttach, mfwpff?.PDFPath, FilteredEmailRecords, MailFaxWizardObject.AddressRecords, displayName, replyTo, MailFaxWizardObject.SubjectPrefix,
                        MailFaxWizardObject.AppendAddressToSubject, MailFaxWizardObject.Body, smtpObj, blastTo);

                    esw.OnSendRequestUploaded += Esw_OnSendRequestUploaded;

                    esw.ProcessRequests();
                });
                
                
            }
            catch (WebException we)
            {
                MessageBox.Show($"Unable to send account details: {we.Message}");
                return;
            }
            catch (Exception e)
            {                
                MessageBox.Show($"Unable to process account details: {e.Message}");
                return;
            }
            

        }

        private void Esw_OnSendRequestUploaded(string to, string subject, string file, bool success)
        {
            SetUploadStatus(to, subject, file, success);
        }

        void On_ExportToPDF_Click(object sender, EventArgs e)
        {
            ExportToPDF(false, false);
        }
               

        private void _sendEmails_Click(object sender, EventArgs e)
        {
            if (DialogResult.OK != ShowWarning())
                return;

            ExportToPDF(true, true);
        }

        int totalRecords;
        int currentlyUploadedRecords;
        private delegate void SetUploadStatusDelegate(string to, string subject, string file, bool success);
        void SetUploadStatus(string to, string subject, string file, bool success)
        {            
            if (_uploading.InvokeRequired)
            {
                Console.WriteLine($"Before Invoke - Uploading:{file} - {(success ? "OK" : "failed")}");
                this.BeginInvoke(new SetUploadStatusDelegate(SetUploadStatus), new object[] {to, subject, file, success });
            }
            else
            {
                currentlyUploadedRecords++;
                Console.WriteLine($"Uploading:{to} {subject} {file} - {(success ? "OK" : "failed")}.");

                string subj = string.Empty;
                string attach = string.Empty;

                if (!string.IsNullOrEmpty(subject))
                    subj = subject.Length > 35 ? $"{subject.Substring(0, 35)}..." : subject;

                if (!string.IsNullOrEmpty(file))
                    attach = file.Length > 35 ? $"{file.Substring(0, 35)}..." : file;

                _uploading.Text = $"Uploading:{Environment.NewLine}{Environment.NewLine}To: {to}" 
                                                + $"{Environment.NewLine}Subject: {subj}"
                                                + $"{ Environment.NewLine}Attached: {attach}"
                                                + $"{ Environment.NewLine}{Environment.NewLine}Success: {(success ? "OK" : "Failed")}.";

                if (currentlyUploadedRecords == totalRecords)
                    _uploading.Text += $"{Environment.NewLine}{Environment.NewLine}Upload Finished.";
            }
        }
        
        private DialogResult ShowWarning()
        {
            string msg = $"If you have a list of more than 50 contacts you are sending to, you should send them in batches of 50 or less.  Your email provider may block your account for spamming if you send too much from one account in a short period of time.{Environment.NewLine}{Environment.NewLine}";
            msg += $"In addition, your email storage may become full so you may need to buy extra storage or delete items from your sent mail to keep your storage low.{Environment.NewLine}{Environment.NewLine}";
            msg += $"You should never use your regular personal or business email address for sending marketing email.  You should always create an account specifically for this purpose in case it gets blocked for spam.{Environment.NewLine}{Environment.NewLine}";
            msg += $"If sending stops suddenly, it is most likely that Google has turned the account off for 24 hours because of SPAM.  Wait 24 hours and try again.";
            return MessageBox.Show(msg, Globals.ProductName, MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
        }

        private async void button1_Click(object sender, EventArgs e)
        {            
            using (var mailMsg = new MailMessage { Subject = "Email from ProfitGrabber", Body = "This is test email from ProfitGrabber", From = new MailAddress(smtpSettings.EmailAddress, smtpSettings.DisplayName) })
            {
                mailMsg.To.Add(_testEmailToAddress.Text);
                mailMsg.ReplyToList.Add(mailMsg.From);

                try
                {
                    using (var emailService = new EmailService())
                    {
                        await emailService.InitializeGmailAccountService(Globals.ProductName, Globals.ClientId, Globals.ClientSecret, Globals.GetUserToAuthorize(smtpSettings?.EmailAddress));
                        var response = emailService.SendEmail(mailMsg);

                        _responseLabel.Text = $"Successfully Sent at {DateTime.Now.ToLongTimeString()}";
                    }
                }
                catch (Exception exc)
                {
                    MessageBox.Show($"Please click OK and try to send email again and Profit Grabber will try to authenticate the Email again.{Environment.NewLine}{Environment.NewLine}{exc.Message}");
                }
            }
        }        
    }
}
#endregion � Namespace DealMaker �