using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace DealMaker
{
	/// <summary>
	/// Summary description for MailFaxWizrd.
	/// </summary>
	public class MailFaxWizardForm : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Panel upperPanel;
		private System.Windows.Forms.Panel lowerPanel;
		private System.Windows.Forms.Button bCancel;
		private System.Windows.Forms.Button bPrevious;
		internal System.Windows.Forms.Button bNext;
		private MailFaxWizard mailFaxWizardObject = new MailFaxWizard();
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public MailFaxWizardForm(MailFaxWizard mailFaxWizardObject)
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			this.mailFaxWizardObject = mailFaxWizardObject;
			
			MailFaxWizardForm1 mfwf1 = new MailFaxWizardForm1(mailFaxWizardObject);
			mfwf1.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top;
			mfwf1.Dock = DockStyle.Fill;

			this.upperPanel.Controls.Add(mfwf1);
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.upperPanel = new System.Windows.Forms.Panel();
            this.lowerPanel = new System.Windows.Forms.Panel();
            this.bPrevious = new System.Windows.Forms.Button();
            this.bNext = new System.Windows.Forms.Button();
            this.bCancel = new System.Windows.Forms.Button();
            this.lowerPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // upperPanel
            // 
            this.upperPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.upperPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.upperPanel.Location = new System.Drawing.Point(0, 0);
            this.upperPanel.Name = "upperPanel";
            this.upperPanel.Size = new System.Drawing.Size(578, 408);
            this.upperPanel.TabIndex = 0;
            // 
            // lowerPanel
            // 
            this.lowerPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.lowerPanel.Controls.Add(this.bPrevious);
            this.lowerPanel.Controls.Add(this.bNext);
            this.lowerPanel.Controls.Add(this.bCancel);
            this.lowerPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lowerPanel.Location = new System.Drawing.Point(0, 408);
            this.lowerPanel.Name = "lowerPanel";
            this.lowerPanel.Size = new System.Drawing.Size(578, 32);
            this.lowerPanel.TabIndex = 1;
            // 
            // bPrevious
            // 
            this.bPrevious.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.bPrevious.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.bPrevious.Location = new System.Drawing.Point(411, 3);
            this.bPrevious.Name = "bPrevious";
            this.bPrevious.Size = new System.Drawing.Size(75, 23);
            this.bPrevious.TabIndex = 202;
            this.bPrevious.TabStop = false;
            this.bPrevious.Text = "Previous";
            this.bPrevious.Click += new System.EventHandler(this.bPrevious_Click);
            // 
            // bNext
            // 
            this.bNext.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.bNext.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.bNext.Location = new System.Drawing.Point(499, 3);
            this.bNext.Name = "bNext";
            this.bNext.Size = new System.Drawing.Size(75, 23);
            this.bNext.TabIndex = 203;
            this.bNext.TabStop = false;
            this.bNext.Text = "Next";
            this.bNext.Click += new System.EventHandler(this.bNext_Click);
            // 
            // bCancel
            // 
            this.bCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.bCancel.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.bCancel.Location = new System.Drawing.Point(8, 3);
            this.bCancel.Name = "bCancel";
            this.bCancel.Size = new System.Drawing.Size(75, 23);
            this.bCancel.TabIndex = 201;
            this.bCancel.TabStop = false;
            this.bCancel.Text = "Cancel";
            this.bCancel.Click += new System.EventHandler(this.bCancel_Click);
            // 
            // MailFaxWizardForm
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(578, 440);
            this.Controls.Add(this.lowerPanel);
            this.Controls.Add(this.upperPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "MailFaxWizardForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Mail / Email / Fax Wizard";
            this.lowerPanel.ResumeLayout(false);
            this.ResumeLayout(false);

		}
		#endregion

		private void bCancel_Click(object sender, System.EventArgs e)
		{
			if (DialogResult.Yes == MessageBox.Show(this, UserMessages.CancelMailFaxWizardText, UserMessages.CancelMailFaxWizard, MessageBoxButtons.YesNo, MessageBoxIcon.Question))
			{
				this.Close();
			}
		}

		private void bNext_Click(object sender, System.EventArgs e)
		{
			IEnumerator ie = this.upperPanel.Controls.GetEnumerator();			
			while (ie.MoveNext())
			{
				#region � MailFaxWizardForm1 �
				if (ie.Current is MailFaxWizardForm1)
				{
					MailFaxWizardForm1 mfwf1 = (MailFaxWizardForm1)ie.Current;
					MailFaxWizard mfwObject = mfwf1.MailFaxWizardObject;

					if (null == mfwObject.SelectedGroupIds || 0 == mfwObject.SelectedGroupIds.Length)
					{
						MessageBox.Show(this, "Select Group by clicking on browse button!", "Select Group", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
						return;
					}

					int iTotalPI = 0;
					foreach (int selectedGruopId in mfwObject.SelectedGroupIds)
					{
						iTotalPI += ApplicationHelper.GetPropertyCountInNode(selectedGruopId);
					}
					if (iTotalPI >= 5000)
					{
						MessageBox.Show(this, "Due to performance reasons Mail Fax Wizard can operate only on groups which contain less than 5000 contacts!", "Large Group!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
						return;
					}

					/*if (ApplicationHelper.GetPropertyCountInNode(mfwObject.SelectedGroupId) >= 5000)
					{
						MessageBox.Show(this, "Due to performance reasons Mail Fax Wizard can operate only on groups which contain less than 5000 contacts!", "Large Group!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
						return;
					}*/

					if (true == mfwObject.ExcludeDoNotMailRecords)
					{
						MailFaxWizardForm2 mfwf2 = new MailFaxWizardForm2(mfwObject);
						mfwf2.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top;
						mfwf2.Dock = DockStyle.Fill;

						this.upperPanel.Controls.RemoveAt(0);
						this.upperPanel.Controls.Add(mfwf2);
						this.upperPanel.Controls[0].Location = new Point(0,0);


					}
					else
					{											
						MailFaxWizardForm3 mfwf3 = new MailFaxWizardForm3(mfwObject);
						mfwf3.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top;
						mfwf3.Dock = DockStyle.Fill;

						this.upperPanel.Controls.RemoveAt(0);
						this.upperPanel.Controls.Add(mfwf3);
						this.upperPanel.Controls[0].Location = new Point(0,0);

                        mfwf3.AddEventHandler();
					}					
					break;
				}
				#endregion � MailFaxWizardForm1 �			
				#region � MailFaxWizardForm2 �
				if (ie.Current is MailFaxWizardForm2)
				{
					MailFaxWizardForm2 mfwf2 = (MailFaxWizardForm2)ie.Current;
					mfwf2.Next();
					MailFaxWizard mfwObject = mfwf2.MailFaxWizardObject;

					
					MailFaxWizardForm3 mfwf3 = new MailFaxWizardForm3(mfwObject);
					mfwf3.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top;
					mfwf3.Dock = DockStyle.Fill;

					this.upperPanel.Controls.RemoveAt(0);
					this.upperPanel.Controls.Add(mfwf3);
					this.upperPanel.Controls[0].Location = new Point(0,0);
									
					break;
				}
				#endregion � MailFaxWizardForm2 �			
				#region � MailFaxWizardForm3 �
				if (ie.Current is MailFaxWizardForm3)
				{
					MailFaxWizardForm3 mfwf3 = (MailFaxWizardForm3)ie.Current;
					mfwf3.Next();
					MailFaxWizard mfwObject = mfwf3.MailFaxWizardObject;

					
					MailFaxWizardForm4 mfwf4 = new MailFaxWizardForm4(mfwObject);					
					mfwf4.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top;
					mfwf4.Dock = DockStyle.Fill;

					this.upperPanel.Controls.RemoveAt(0);
					this.upperPanel.Controls.Add(mfwf4);
					this.upperPanel.Controls[0].Location = new Point(0,0);
									
					break;
				}
				#endregion � MailFaxWizardForm3 �			
				#region � MailFaxWizardForm4 �
				if (ie.Current is MailFaxWizardForm4)
				{
					MailFaxWizardForm4 mfwf4 = (MailFaxWizardForm4)ie.Current;
					MailFaxWizard mfwObject = mfwf4.MailFaxWizardObject;

                    if (eLabelDocumentSelection.notSet == mfwObject.LabelDocSel)
                    {
                        MessageBox.Show("Please select document");
                        return;
                    }

                    //old working commented out. New > new test - send only text
                    //if (eLabelDocumentSelection.emailBlast == mfwObject.LabelDocSel && string.IsNullOrEmpty(mfwObject.DocumentName))
                    //{
                    //    MessageBox.Show("Please select document");
                    //    return;
                    //}

                    if (eLabelDocumentSelection.emailBlast == mailFaxWizardObject.LabelDocSel)
                    {
                        mfwObject.DisplayName = mfwf4._email_Blast_DisplayName.Text;
                        mfwObject.ReplyTo = mfwf4._email_Blast_ReplyTo.Text;
                        mfwObject.SubjectPrefix = mfwf4._email_Blast_SubjectPrefix.Text;
                        mfwObject.AppendAddressToSubject = mfwf4._email_Blast_AppendAddress.Checked;
                        mfwObject.Body = mfwf4._email_Blast_EmailBody.Text;
                    }
                    if (eLabelDocumentSelection.emailLOI == mailFaxWizardObject.LabelDocSel)
                    {
                        mfwObject.DisplayName = mfwf4._email_LOI_DisplayName.Text;
                        mfwObject.ReplyTo = mfwf4._email_LOI_ReplyTo.Text;
                        mfwObject.SubjectPrefix = mfwf4._email_LOI_SubjectPrefix.Text;
                        mfwObject.AppendAddressToSubject = mfwf4._email_LOI_AppendAddress.Checked;
                        mfwObject.Body = mfwf4._email_LOI_EmailBody.Text;
                    }

					
					MailFaxWizardForm5 mfwf5 = new MailFaxWizardForm5(mfwObject);
					mfwf5.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top;
					mfwf5.Dock = DockStyle.Fill;

					this.upperPanel.Controls.RemoveAt(0);
					this.upperPanel.Controls.Add(mfwf5);
					this.upperPanel.Controls[0].Location = new Point(0,0);

					this.bNext.Text = "Finish";
									
					break;
				}
				#endregion � MailFaxWizardForm3 �			
				#region � MailFaxWizardForm5 �
				if (ie.Current is MailFaxWizardForm5)
				{
					this.Close();					
				}
				#endregion � MailFaxWizardForm5 �			
			}
		}

		private void bPrevious_Click(object sender, System.EventArgs e)
		{
			IEnumerator ie = this.upperPanel.Controls.GetEnumerator();			
			while (ie.MoveNext())
			{
				#region � MailFaxWizardForm2 �
				if (ie.Current is MailFaxWizardForm2)
				{
					MailFaxWizardForm2 mfwf2 = (MailFaxWizardForm2)ie.Current;					
					MailFaxWizard mfwObject = mfwf2.MailFaxWizardObject;
					
					MailFaxWizardForm1 mfwf1 = new MailFaxWizardForm1(mfwObject);
					mfwf1.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top;
					mfwf1.Dock = DockStyle.Fill;

					this.upperPanel.Controls.RemoveAt(0);
					this.upperPanel.Controls.Add(mfwf1);
					this.upperPanel.Controls[0].Location = new Point(0,0);
									
					break;
				}
				#endregion � MailFaxWizardForm2 �			
				#region � MailFaxWizardForm3 �
				if (ie.Current is MailFaxWizardForm3)
				{
					MailFaxWizardForm3 mfwf3 = (MailFaxWizardForm3)ie.Current;					
					MailFaxWizard mfwObject = mfwf3.MailFaxWizardObject;					
					
					MailFaxWizardForm1 mfwf1 = new MailFaxWizardForm1(mfwObject);
					mfwf1.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top;
					mfwf1.Dock = DockStyle.Fill;
					
					this.bNext.Enabled = true;

					this.upperPanel.Controls.RemoveAt(0);
					this.upperPanel.Controls.Add(mfwf1);
					this.upperPanel.Controls[0].Location = new Point(0,0);					
                                        				
					break;
				}
				#endregion � MailFaxWizardForm3 �			
				#region � MailFaxWizardForm4 �
				if (ie.Current is MailFaxWizardForm4)
				{
					MailFaxWizardForm4 mfwf4 = (MailFaxWizardForm4)ie.Current;
					MailFaxWizard mfwObject = mfwf4.MailFaxWizardObject;

					
					MailFaxWizardForm3 mfwf3 = new MailFaxWizardForm3(mfwObject);
					mfwf3.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top;
					mfwf3.Dock = DockStyle.Fill;

					this.upperPanel.Controls.RemoveAt(0);
					this.upperPanel.Controls.Add(mfwf3);
					this.upperPanel.Controls[0].Location = new Point(0,0);
                    mfwf3.AddEventHandler();
									
					break;
				}
				#endregion � MailFaxWizardForm3 �			
				#region � MailFaxWizardForm5 �
				if (ie.Current is MailFaxWizardForm5)
				{
					MailFaxWizardForm5 mfwf5 = (MailFaxWizardForm5)ie.Current;
					MailFaxWizard mfwObject = mfwf5.MailFaxWizardObject;
					mfwObject.SelectedRecords = mfwf5.CachedOriginalSelectedItems;
					
					MailFaxWizardForm4 mfwf4 = new MailFaxWizardForm4(mfwObject);
					mfwf4.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top;
					mfwf4.Dock = DockStyle.Fill;

					this.upperPanel.Controls.RemoveAt(0);
					this.upperPanel.Controls.Add(mfwf4);
					this.upperPanel.Controls[0].Location = new Point(0,0);


                    if (eLabelDocumentSelection.labelLarge == mfwObject.LabelDocSel || eLabelDocumentSelection.labelMedium == mfwObject.LabelDocSel || eLabelDocumentSelection.labelSmall == mfwObject.LabelDocSel)
                    {
                        mfwf4.RemoveTabCtrlEHs();
                        mfwf4.Tab_Control.SelectedIndex = 0;
                        mfwf4.AddTabCtrlEHs();

                        if (eLabelDocumentSelection.labelLarge == mfwObject.LabelDocSel)
                        {
                            mfwf4.rbLarge.Checked = true;
                            mfwObject.LabelDocSel = eLabelDocumentSelection.labelLarge;
                        }

                        if (eLabelDocumentSelection.labelMedium == mfwObject.LabelDocSel)
                        {
                            mfwf4.rbMedium.Checked = true;
                            mfwObject.LabelDocSel = eLabelDocumentSelection.labelMedium;
                        }

                        if (eLabelDocumentSelection.labelSmall == mfwObject.LabelDocSel)
                        {
                            mfwf4.rbSmall.Checked = true;
                            mfwObject.LabelDocSel = eLabelDocumentSelection.labelSmall;
                        }
                    }

                    else if (eLabelDocumentSelection.emailBlast == mfwObject.LabelDocSel)
                    {
                        mfwf4.RemoveTabCtrlEHs();
                        mfwf4.Tab_Control.SelectedIndex = 3;
                        mfwf4.AddTabCtrlEHs();

                        mfwObject.LabelDocSel = eLabelDocumentSelection.emailBlast;

                        mfwObject.DisplayName = mfwf4._email_Blast_DisplayName.Text;
                        mfwf4._email_Blast_ReplyTo.Text = mfwObject.ReplyTo;
                        mfwf4._email_Blast_SubjectPrefix.Text = mfwObject.SubjectPrefix;
                        mfwf4._email_Blast_AppendAddress.Checked = mfwObject.AppendAddressToSubject;
                        mfwf4._email_Blast_EmailBody.Text = mfwObject.Body;

                        mfwf4._email_Blast_SelectedTemplate.Text = System.IO.Path.GetFileName(mfwObject.DocumentName);
                        mfwf4.tbSelectedDocument.Text = $"Email Blast:{mfwf4._email_Blast_SelectedTemplate.Text}";
                    }
                    else if (eLabelDocumentSelection.emailLOI == mfwObject.LabelDocSel)
                    {
                        mfwf4.RemoveTabCtrlEHs();
                        mfwf4.Tab_Control.SelectedIndex = 2;
                        mfwf4.AddTabCtrlEHs();

                        mfwObject.LabelDocSel = eLabelDocumentSelection.emailLOI;

                        mfwf4._email_LOI_DisplayName.Text = mfwObject.DisplayName;
                        mfwf4._email_LOI_ReplyTo.Text = mfwObject.ReplyTo;
                        mfwf4._email_LOI_SubjectPrefix.Text = mfwObject.SubjectPrefix;
                        mfwf4._email_LOI_AppendAddress.Checked = mfwObject.AppendAddressToSubject;
                        mfwf4._email_LOI_EmailBody.Text = mfwObject.Body;

                        mfwf4._email_LOI_Perc.Text = mfwObject.Percentage.ToString();
                        mfwf4.tbSelectedDocument.Text = $"Email LOI, percentage at {mfwObject.Percentage.ToString()}%.";

                    }
                    else if (eLabelDocumentSelection.faxDocument == mfwObject.LabelDocSel)
                    {
                        var perc = mfwObject.Percentage;
                        var doc = mfwObject.DocumentName;

                        mfwf4.RemoveTabCtrlEHs();
                        mfwf4.Tab_Control.SelectedIndex = 4;
                        mfwf4.AddTabCtrlEHs();

                        mfwf4.RemoveRBEventHandlers();

                        mfwObject.Percentage = perc;
                        mfwObject.DocumentName = doc;

                        mfwObject.LabelDocSel = eLabelDocumentSelection.faxDocument;

                        if (mfwObject.DocumentName == "LOI")
                        {
                            mfwf4.rbLetterOfIntent.Checked = true;
                            mfwObject.Percentage = perc;
                            mfwf4.cbPercentage.Text = mfwObject.Percentage.ToString();
                            
                            mfwf4.tbSelectedDocument.Text = "Letter Of Intent at " + mfwObject.Percentage.ToString() +"%.";
                        }
                        else
                        {
                            mfwf4.rbOtherFaxDocuments.Checked = true;
                            mfwObject.DocumentName = doc;
                            mfwf4.lvOtherFaxDocs.Enabled = true;                            
                            mfwf4.tbSelectedDocument.Text = mfwObject.DocumentName;
                        }

                        mfwf4.AddRBEventHandlers();
                    }
                    else if (eLabelDocumentSelection.mailDocument == mfwObject.LabelDocSel)
                    {
                        var docName = mfwObject.DocumentName;
                        mfwf4.RemoveTabCtrlEHs();
                        mfwf4.Tab_Control.SelectedIndex = 1;
                        mfwf4.AddTabCtrlEHs();

                        mfwObject.LabelDocSel = eLabelDocumentSelection.mailDocument;

                        mfwObject.DocumentName = docName;
                        mfwf4.tbSelectedDocument.Text = mfwObject.DocumentName;
                    }

                    this.bNext.Text = "Next";
									
					break;
				}
				#endregion � MailFaxWizardForm5 �			
			}
		}				
	}
}
