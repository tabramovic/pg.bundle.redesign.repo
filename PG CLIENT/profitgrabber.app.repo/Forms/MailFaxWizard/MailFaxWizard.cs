#region � Using �
using System;
using System.Collections;
using System.Collections.Generic;
#endregion � Using �
#region � Namespace DealMaker � 
namespace DealMaker
{
	#region � Class MailFaxWizard �
	/// <summary>
	/// Summary description for MailFaxWizard.
	/// </summary>
	public class MailFaxWizard
	{
		#region � Data �
		private Guid idMailFaxWizard;
		private double percentage = 0;
		private bool loiSelected = false;
		private int[] selectedGroupIds;
		private string selectedGroupName;
		private eSelection selection = eSelection.selectedGroup;
		private eLabelDocumentSelection labelDocSel = eLabelDocumentSelection.notSet;
		private string	documentName = string.Empty;
		private bool bExcludeDoNotMailRecords = false;
		private ArrayList expelledRecords = new ArrayList();
		private ArrayList selectedRecords = new ArrayList();
		#endregion � Data �

		#region � Properties �
		#region � IdMailFaxWizard  �
		public Guid IdMailFaxWizard
		{
			get {return this.idMailFaxWizard;}
			set {this.idMailFaxWizard = value;}
		}
		#endregion � IdMailFaxWizard �
		
		#region � eLabelDocumentSelection  �
		public eLabelDocumentSelection LabelDocSel
		{
			get {return this.labelDocSel;}
			set {this.labelDocSel = value;}
		}
		#endregion � eLabelDocumentSelection  �
		#region � DocumentName �
		public string DocumentName
		{
			get {return this.documentName;}
			set {this.documentName = value;}
		}
		#endregion � DocumentName �
		#region � LoiSelected �
		public bool LoiSelected 
		{
			get { return this.loiSelected; }
			set { this.loiSelected = value; }
		}
		#endregion � LoiSelected �
		#region � Percentage �
		public double Percentage
		{
			get {return this.percentage;}
			set {this.percentage = value;}
		}
		#endregion � Percentage �
		#region � SelectedGroupId �
		public int[] SelectedGroupIds
		{
			get {return this.selectedGroupIds;}
			set {this.selectedGroupIds = value;}
		}
		#endregion � SelectedGroupId �
		#region � SelectedGroupName �
		public string SelectedGroupName
		{
			get {return this.selectedGroupName;}
			set {this.selectedGroupName = value;}
		}
		#endregion � SelectedGroupName �
		#region � Selection �
		public eSelection Selection
		{
			get {return this.selection;}
			set {this.selection = value;}
		}
		#endregion � Selection �
		#region � ExcludeDoNotMailRecords �
		public bool ExcludeDoNotMailRecords 
		{
			get {return this.bExcludeDoNotMailRecords;}
			set {this.bExcludeDoNotMailRecords = value;}
		}
		#endregion � ExcludeDoNotMailRecords �
		#region � ExpelledRecords �
		public ArrayList ExpelledRecords
		{
			get {return this.expelledRecords;}
			set {this.expelledRecords = value;}
		}
		#endregion � ExpelledRecords �
		#region � SelectedRecords �
		public ArrayList SelectedRecords
		{
			get {return this.selectedRecords;}
			set {this.selectedRecords = value;}
		}

        List<KeyValuePair<Guid, Tuple<string, string>>> emailRecords = new List<KeyValuePair<Guid, Tuple<string, string>>>();
        public List<KeyValuePair<Guid, Tuple<string, string>>> EmailRecords { get { return emailRecords; } set { emailRecords = value; } }

        public string DisplayName { get; set; }
        public string ReplyTo { get; set; }
        public string SubjectPrefix { get; set; }
        public bool   AppendAddressToSubject { get; set; }
        public string Body { get; set; }

        public Dictionary<Guid, string> addressRecords = new Dictionary<Guid, string>();
        public Dictionary<Guid, string> AddressRecords { get { return addressRecords; } set { addressRecords = value; } }
		#endregion � SelectedRecords �
		#endregion � Properties �
		#region � CTR �
		public MailFaxWizard()
		{

		}
		#endregion � CTR �
	}
	#endregion � Class MailFaxWizard �

	#region � Enum eSelection �
	/// <summary>
	/// Current Selection
	/// </summary>
	[Serializable]
	public enum eSelection
	{
		currentLookup = 0, 

		selectedGroup,
	}
	#endregion � Enum eSelection �

	#region � eLabelDocumentSelection �
	/// <summary>
	/// Mail merge will be performed on labels or documents
	/// </summary>
	[Serializable]
	public enum eLabelDocumentSelection
	{
		labelSmall = 0, 

		labelMedium, 

		labelLarge,

		mailDocument,

		faxDocument, 

        emailLOI, 

        emailBlast,

        notSet
	}
	#endregion � eLabelDocumentSelection �
}
#endregion � Namespace DealMaker �