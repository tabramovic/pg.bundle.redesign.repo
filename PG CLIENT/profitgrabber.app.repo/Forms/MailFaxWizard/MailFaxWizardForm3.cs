using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Collections.Generic;

namespace DealMaker
{
	/// <summary>
	/// Summary description for MailFaxWizardForm3.
	/// </summary>
	public class MailFaxWizardForm3 : System.Windows.Forms.UserControl
	{
		private DealMaker.Forms.Floaters.MFWViewandTagRecipients floater = null;
		ArrayList doNotMailData = null;
		ArrayList doNotMailContacts = null;
		
		const string YES = "Yes";
		const string NO = "No";
		private int numberOfTaggedRecords = 0;
		private bool asc = false;
		private System.Windows.Forms.ListView lvRecipients;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.ColumnHeader tag;
		private System.Windows.Forms.ColumnHeader fullName;
		private System.Windows.Forms.ColumnHeader siteStreetAddress;
		private System.Windows.Forms.ColumnHeader statusGroup;
		private System.Windows.Forms.Button bTagAll;
		private System.Windows.Forms.Button bUnTagAll;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox tbNumberOfRecords;
		private System.Windows.Forms.Label label3;
		private MailFaxWizard mfwObject;
		private System.Windows.Forms.ColumnHeader siteZIP;
		private System.Windows.Forms.ColumnHeader siteCity;
		private System.Windows.Forms.ColumnHeader siteState;
		private System.Windows.Forms.ColumnHeader listingAgentName;
		private System.Windows.Forms.ColumnHeader listingPrice;
		private System.Windows.Forms.ColumnHeader listingAgentFax;
		private System.Windows.Forms.ColumnHeader foreclosurefiledate;
		private System.Windows.Forms.ColumnHeader foreclosuresaledate;
		private System.Windows.Forms.ColumnHeader mailStreetAddress;
		private System.Windows.Forms.ColumnHeader mailCity;
		private System.Windows.Forms.ColumnHeader mailState;
		private System.Windows.Forms.ColumnHeader mailZIP;
		private DealMaker.ReadMe readMe1;
		private System.Windows.Forms.ColumnHeader doNotMailGroupMemb;
		private System.Windows.Forms.CheckBox cbDoNotMail;
		private System.Windows.Forms.Label label4;

        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.Container components = null;

		public MailFaxWizardForm3(MailFaxWizard mfwObject)
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();			

			this.mfwObject = mfwObject;				

			Hashtable htPropertyItems = null;
			if (this.mfwObject.Selection == eSelection.currentLookup)
			{
				if (null != Globals.oldListView)
				{
					PropertyItemListViewControl pilvc = (PropertyItemListViewControl)Globals.oldListView;
					htPropertyItems = pilvc.GetSelectedPropertyItems;					
				}
				
			}
			else
			{
				if (null != this.mfwObject.SelectedGroupIds && 0 != this.mfwObject.SelectedGroupIds.Length)
				{
					htPropertyItems = FillItems(this.mfwObject.SelectedGroupIds); //ApplicationHelper.GetPropertyItemsInNode(this.mfwObject.SelectedGroupId);
					if (0 == htPropertyItems.Count)
					{
						MessageBox.Show(this, "Group you selected does not contain any records!", "Empty group!", MessageBoxButtons.OK,MessageBoxIcon.Exclamation);
						return;
					}
				}
				else
				{
					MessageBox.Show(this, "Select group!", "Select group!", MessageBoxButtons.OK,MessageBoxIcon.Exclamation);
					return;
				}
			}

			if (null != htPropertyItems)
			{
				this.FillListView(htPropertyItems);
			}
			
			this.readMe1.MouseEnter += new EventHandler(readMe1_MouseEnter);
			this.readMe1.MouseLeave += new EventHandler(readMe1_MouseLeave);
		}

		Hashtable FillItems(int[] selectedGroupIds)
		{
			IDictionaryEnumerator ide;
			Hashtable ht = new Hashtable();
			foreach (int groupId in selectedGroupIds)
			{
				Hashtable htItemsInGroup = ApplicationHelper.GetPropertyItemsInNode(groupId);
				ide = htItemsInGroup.GetEnumerator();
				while (ide.MoveNext())
				{
					if (!ht.ContainsValue(ide.Value))
					{
                        //OLD, BugFix for multiple group selection
                        //if (ht.ContainsKey(ide.Key))
                        //    ht.Add((int)ide.Key + 10000, ide.Value);
                        //else
                        //    ht.Add(ide.Key, ide.Value);

                        //NEW
                        ht.Add(ht.Count, ide.Value);
					}
				}
			}
			return ht;
		}

		

		private void FillListView (Hashtable htPropertyItems)
		{			
			SqlDataReader reader = null;
			IDictionaryEnumerator ide = htPropertyItems.GetEnumerator();
			ArrayList alSelectedPropertyItems = new ArrayList();
            List<ListViewItem> lviList = new List<ListViewItem>();
			
			//Event trigger
			EventTrigger et = new EventTrigger();
			PBForm pbf = new PBForm(et);
			pbf.Capacity = htPropertyItems.Count;
			if (null != this.mfwObject && null != this.mfwObject.ExpelledRecords)
				pbf.Capacity = htPropertyItems.Count - this.mfwObject.ExpelledRecords.Count;

			pbf.WindowName = "Loading Data...";			
			pbf.Show();

			doNotMailData = ApplicationHelper.GetDoNotMailData();
			doNotMailContacts = new ArrayList();
			foreach (Guid g in doNotMailData)
			{
				doNotMailContacts.Add(PropertyItem.LoadPropertyItem(g));
			}

			SqlCommand sp_GetMFWPropertyItemDetails = new SqlCommand("GetMFWPropertyItemDetails", Connection.getInstance().HatchConn);
			sp_GetMFWPropertyItemDetails.CommandType = CommandType.StoredProcedure;

            this.mfwObject.EmailRecords = new List<KeyValuePair<Guid, Tuple<string, string>>>();
            this.mfwObject.AddressRecords = new Dictionary<Guid, string>();

            int iCnt = 0;
			while (ide.MoveNext())
			{	
				iCnt++;
				Guid propertyItemId = (Guid)ide.Value;
				if (false == this.mfwObject.ExpelledRecords.Contains(propertyItemId))
				{
					ListViewItem lvi = null;

					string statusGroup = ApplicationHelper.GetStatusGroupMembership(propertyItemId);

					try
					{									
						Cursor.Current = Cursors.WaitCursor;
						sp_GetMFWPropertyItemDetails.Parameters.Clear();		
						sp_GetMFWPropertyItemDetails.Parameters.Add("@propertyItemId", propertyItemId);
									
						if (ConnectionState.Open != Connection.getInstance().HatchConn.State)
							Connection.getInstance().HatchConn.Open();

						//sp_GetMFWPropertyItemDetails.ExecuteNonQuery();
						reader = sp_GetMFWPropertyItemDetails.ExecuteReader();

						
						while (reader.Read())
						{										
							string fssa = string.Empty;
							lvi = new ListViewItem();
							//Contact.FullName
							if (DBNull.Value != reader[0])
							{
								string contactName = reader.GetString(0);
								lvi.SubItems.Add(reader.GetString(0));
							}
							else
								lvi.SubItems.Add(string.Empty);
					
							//SiteAddress.FullSiteStreetAddress
							if (DBNull.Value != reader[1])
							{
								fssa = reader.GetString(1);
								lvi.SubItems.Add(reader.GetString(1));
							}
							else
							{
								lvi.SubItems.Add(string.Empty);
							}

							//SiteAddress.SiteCity
							if (DBNull.Value  != reader[2])
							{
								string siteCity = reader.GetString(2);
								lvi.SubItems.Add(reader.GetString(2));
							}
							else
							{
								lvi.SubItems.Add(string.Empty);
							}

							//SiteAddress.SiteState
							if (DBNull.Value  != reader[3])
							{
								string siteState = reader.GetString(3);
								lvi.SubItems.Add(reader.GetString(3));
							}
							else
							{
								lvi.SubItems.Add(string.Empty);
							}																	
							
							//SiteAddress.SiteZIP
							if (DBNull.Value  != reader[4])
							{
								string siteZIP = reader.GetString(4);
								lvi.SubItems.Add(reader.GetString(4));
							}
							else
							{
								lvi.SubItems.Add(string.Empty);
							}	
							
							//ListingAgent.ListingAgentFullName
							if (DBNull.Value  != reader[10])
							{
								string lafn = reader.GetString(10);
								lvi.SubItems.Add(reader.GetString(10));
							}
							else
							{
								lvi.SubItems.Add(string.Empty);
							}	

							//ListingAgent.ListingPrice
							if (DBNull.Value  != reader[11])
							{
								string lap = reader.GetDecimal(11).ToString();
								lvi.SubItems.Add(reader.GetDecimal(11).ToString());
							}
							else
							{
								lvi.SubItems.Add(string.Empty);
							}	

							//ListingAgent.ListingAgentFaxNumber
							if (DBNull.Value  != reader[12])
							{
								string lafaxNr = reader.GetString(12);
								lvi.SubItems.Add(reader.GetString(12));
							}
							else
							{
								lvi.SubItems.Add(string.Empty);
							}	

							//ForeClosureProperties.ForeClosureFileDate
							if (DBNull.Value  != reader[13])
							{
								string fcfn = reader.GetString(13);
								lvi.SubItems.Add(reader.GetString(13));
							}
							else
							{
								lvi.SubItems.Add(string.Empty);
							}	

							//ForeClosureProperties.ForeClosureSaleDate
							if (DBNull.Value  != reader[14])
							{
								string fcsd = reader.GetString(14);
								lvi.SubItems.Add(reader.GetString(14));
							}
							else
							{
								lvi.SubItems.Add(string.Empty);
							}

							//MailAddress.FullSiteStreetAddress
							if (DBNull.Value  != reader[5])
							{
								string mailAddr = reader.GetString(5);
								lvi.SubItems.Add(reader.GetString(5));
							}
							else
							{
								lvi.SubItems.Add(string.Empty);
							}
	
							//MailAddress.SiteCity
							if (DBNull.Value  != reader[6])
							{
								string mailCity = reader.GetString(6);
								lvi.SubItems.Add(reader.GetString(6));
							}
							else
							{
								lvi.SubItems.Add(string.Empty);
							}	

							//MailAddress.SiteState
							if (DBNull.Value  != reader[7])
							{
								string mailState = reader.GetString(7);
								lvi.SubItems.Add(reader.GetString(7));
							}
							else
							{
								lvi.SubItems.Add(string.Empty);
							}	

							//MailAddress.SiteZIP,
							if (DBNull.Value  != reader[8])
							{
								string mailZIP = reader.GetString(8);
								lvi.SubItems.Add(reader.GetString(8));
							}
							else
							{
								lvi.SubItems.Add(string.Empty);
							}

                            
                            // status group
                            lvi.SubItems.Add(statusGroup);

                            
                            //Do Not Mail Memebership
                            /*
							if (null != doNotMailData && 0 < doNotMailData.Count)
							{
								if (doNotMailData.Contains(propertyItemId))
									lvi.SubItems.Add(YES);
								else
									lvi.SubItems.Add(NO);
							}
							else
							{
								lvi.SubItems.Add(NO);
							}
							*/
                            if (TestForDoNotMail(propertyItemId, fssa))
								lvi.SubItems.Add(YES);
							else
								lvi.SubItems.Add(NO);

                            KeyValuePair<Guid, Tuple<string, string>> assignedEmailRecords = new KeyValuePair<Guid, Tuple<string, string>>();
                            string skipTracedContactEmail = string.Empty;
                            string contactEmail = string.Empty;
                            string listingAgentEmail = string.Empty;

                            //Contact.eMail
                            if (DBNull.Value != reader[15])
                            {
                                contactEmail = reader.GetString(15);                                
                                //Tuple<string, string> emailTuple = new Tuple<string, string>(emailAddress, string.Empty);
                                //this.mfwObject.EmailRecords.Add(record);
                            }

                            //ListingAgent eMail
                            if (DBNull.Value != reader[16])
                            {
                                listingAgentEmail = reader.GetString(16);                                
                            }

                            //SkipTraced eMail
                            if (DBNull.Value != reader[17])
                            {
                                skipTracedContactEmail = reader.GetString(17);
                            }

                            //use first choice email email. if empty then use skip trace email address.
                            if (string.IsNullOrEmpty(contactEmail))
                                contactEmail = skipTracedContactEmail;

                            Tuple<string, string> emailRecords = new Tuple<string, string>(contactEmail, listingAgentEmail);
                            assignedEmailRecords = new KeyValuePair<Guid, Tuple<string, string>>(propertyItemId, emailRecords);

                            mfwObject.EmailRecords.Add(assignedEmailRecords);

                            mfwObject.AddressRecords.Add(propertyItemId, fssa);
                            
                        }						
					}
					catch (Exception exc)
					{
						MessageBox.Show(exc.Message);						
					}
					finally
					{
                        if (null != reader)
						    reader.Close();

						Cursor.Current = Cursors.Arrow;
					}
					
					if (null != lvi)
					{
						lvi.SubItems.Add(ApplicationHelper.GetStatusGroupMembership(propertyItemId));
						lvi.Tag = (Guid)propertyItemId;                        
						//this.lvRecipients.Items.Add(lvi);
                        lviList.Add(lvi);
                        alSelectedPropertyItems.Add(propertyItemId);
					}
					//fire event
					et.TriggerChange();
				}
			}
			this.mfwObject.SelectedRecords = alSelectedPropertyItems;
            this.lvRecipients.Items.AddRange(lviList.ToArray());
		}

		public MailFaxWizard MailFaxWizardObject
		{
			get {return this.mfwObject;}
			set {this.mfwObject = value;}
		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MailFaxWizardForm3));
            this.lvRecipients = new System.Windows.Forms.ListView();
            this.tag = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.fullName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.siteStreetAddress = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.siteCity = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.siteState = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.siteZIP = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.listingAgentName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.listingPrice = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.listingAgentFax = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.foreclosurefiledate = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.foreclosuresaledate = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.mailStreetAddress = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.mailCity = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.mailState = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.mailZIP = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.statusGroup = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.doNotMailGroupMemb = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.label1 = new System.Windows.Forms.Label();
            this.bTagAll = new System.Windows.Forms.Button();
            this.bUnTagAll = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.tbNumberOfRecords = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cbDoNotMail = new System.Windows.Forms.CheckBox();
            this.label4 = new System.Windows.Forms.Label();
            this.readMe1 = new DealMaker.ReadMe();
            this.SuspendLayout();
            // 
            // lvRecipients
            // 
            this.lvRecipients.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lvRecipients.CheckBoxes = true;
            this.lvRecipients.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.tag,
            this.fullName,
            this.siteStreetAddress,
            this.siteCity,
            this.siteState,
            this.siteZIP,
            this.listingAgentName,
            this.listingPrice,
            this.listingAgentFax,
            this.foreclosurefiledate,
            this.foreclosuresaledate,
            this.mailStreetAddress,
            this.mailCity,
            this.mailState,
            this.mailZIP,
            this.statusGroup,
            this.doNotMailGroupMemb});
            this.lvRecipients.FullRowSelect = true;
            this.lvRecipients.GridLines = true;
            this.lvRecipients.Location = new System.Drawing.Point(24, 48);
            this.lvRecipients.Name = "lvRecipients";
            this.lvRecipients.Size = new System.Drawing.Size(969, 200);
            this.lvRecipients.TabIndex = 0;
            this.lvRecipients.UseCompatibleStateImageBehavior = false;
            this.lvRecipients.View = System.Windows.Forms.View.Details;
            this.lvRecipients.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.lvRecipients_ColumnClick);
            //this.lvRecipients.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.lvRecipients_ItemChecked);
            // 
            // tag
            // 
            this.tag.Text = "Tag";
            this.tag.Width = 49;
            // 
            // fullName
            // 
            this.fullName.Text = "Full Name";
            this.fullName.Width = 120;
            // 
            // siteStreetAddress
            // 
            this.siteStreetAddress.Text = "Site Street Address";
            this.siteStreetAddress.Width = 174;
            // 
            // siteCity
            // 
            this.siteCity.Text = "Site City";
            this.siteCity.Width = 106;
            // 
            // siteState
            // 
            this.siteState.Text = "Site State";
            // 
            // siteZIP
            // 
            this.siteZIP.Text = "Site ZIP";
            // 
            // listingAgentName
            // 
            this.listingAgentName.Text = "Listing Agent Name";
            // 
            // listingPrice
            // 
            this.listingPrice.Text = "Listing Price";
            // 
            // listingAgentFax
            // 
            this.listingAgentFax.Text = "Listing Agent Fax";
            // 
            // foreclosurefiledate
            // 
            this.foreclosurefiledate.Text = "Foreclosure file date";
            // 
            // foreclosuresaledate
            // 
            this.foreclosuresaledate.Text = "Foreclosure Sale Date";
            // 
            // mailStreetAddress
            // 
            this.mailStreetAddress.Text = "Mail Street Address";
            // 
            // mailCity
            // 
            this.mailCity.Text = "Mail City";
            // 
            // mailState
            // 
            this.mailState.Text = "Mail State";
            // 
            // mailZIP
            // 
            this.mailZIP.Text = "Mail ZIP";
            // 
            // statusGroup
            // 
            this.statusGroup.Text = "Status Group";
            this.statusGroup.Width = 102;
            // 
            // doNotMailGroupMemb
            // 
            this.doNotMailGroupMemb.Text = "Do Not Mail Group Member";
            this.doNotMailGroupMemb.Width = 160;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label1.Location = new System.Drawing.Point(8, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(304, 23);
            this.label1.TabIndex = 1;
            this.label1.Text = "View and Tag Your Recipients";
            // 
            // bTagAll
            // 
            this.bTagAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.bTagAll.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.bTagAll.Location = new System.Drawing.Point(838, 291);
            this.bTagAll.Name = "bTagAll";
            this.bTagAll.Size = new System.Drawing.Size(75, 23);
            this.bTagAll.TabIndex = 2;
            this.bTagAll.Text = "Tag All";
            this.bTagAll.Click += new System.EventHandler(this.bTagAll_Click);
            // 
            // bUnTagAll
            // 
            this.bUnTagAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.bUnTagAll.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.bUnTagAll.Location = new System.Drawing.Point(918, 291);
            this.bUnTagAll.Name = "bUnTagAll";
            this.bUnTagAll.Size = new System.Drawing.Size(75, 23);
            this.bUnTagAll.TabIndex = 3;
            this.bUnTagAll.Text = "UnTag All";
            this.bUnTagAll.Click += new System.EventHandler(this.bUnTagAll_Click);
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label2.Location = new System.Drawing.Point(24, 296);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(200, 23);
            this.label2.TabIndex = 4;
            this.label2.Text = "Number Of Currently Tagged Records:";
            // 
            // tbNumberOfRecords
            // 
            this.tbNumberOfRecords.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.tbNumberOfRecords.Location = new System.Drawing.Point(224, 293);
            this.tbNumberOfRecords.Name = "tbNumberOfRecords";
            this.tbNumberOfRecords.ReadOnly = true;
            this.tbNumberOfRecords.Size = new System.Drawing.Size(32, 20);
            this.tbNumberOfRecords.TabIndex = 5;
            this.tbNumberOfRecords.TabStop = false;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label3.Location = new System.Drawing.Point(24, 328);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(640, 23);
            this.label3.TabIndex = 6;
            this.label3.Text = "WARNING: Only the Tagged Records will be used in your Mail/ Fax Merging !";
            // 
            // cbDoNotMail
            // 
            this.cbDoNotMail.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cbDoNotMail.Checked = true;
            this.cbDoNotMail.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbDoNotMail.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.cbDoNotMail.Location = new System.Drawing.Point(27, 261);
            this.cbDoNotMail.Name = "cbDoNotMail";
            this.cbDoNotMail.Size = new System.Drawing.Size(24, 24);
            this.cbDoNotMail.TabIndex = 13;
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label4.Location = new System.Drawing.Point(44, 261);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(400, 23);
            this.label4.TabIndex = 14;
            this.label4.Text = "Check to eliminate contacts assigned to \"Do Not Mail\" Group during Tag All";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // readMe1
            // 
            this.readMe1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.readMe1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.readMe1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("readMe1.BackgroundImage")));
            this.readMe1.Location = new System.Drawing.Point(987, 8);
            this.readMe1.Name = "readMe1";
            this.readMe1.Size = new System.Drawing.Size(16, 16);
            this.readMe1.TabIndex = 12;
            // 
            // MailFaxWizardForm3
            // 
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.Controls.Add(this.label4);
            this.Controls.Add(this.cbDoNotMail);
            this.Controls.Add(this.readMe1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.tbNumberOfRecords);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.bUnTagAll);
            this.Controls.Add(this.bTagAll);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lvRecipients);
            this.Name = "MailFaxWizardForm3";
            this.Size = new System.Drawing.Size(1011, 352);
            this.Load += new System.EventHandler(this.MailFaxWizardForm3_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

		}
        #endregion

        public void AddEventHandler()
        {
            this.lvRecipients.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.lvRecipients_ItemChecked);
        }

        public void RemoveEventHandler()
        {
            this.lvRecipients.ItemChecked -= new System.Windows.Forms.ItemCheckedEventHandler(this.lvRecipients_ItemChecked);
        }

        private void _tagEmail_Click(object sender, EventArgs e)
        {
            RemoveEventHandler();
            this.TagListView(true, true);
            AddEventHandler();
            this.numberOfTaggedRecords = this.GetNumberOfCheckedItems();
            this.tbNumberOfRecords.Text = this.numberOfTaggedRecords.ToString();
            this.CheckForNextVisibility();
        }

        private void bTagAll_Click(object sender, System.EventArgs e)
		{
            RemoveEventHandler();
			this.TagListView(true, false);
            AddEventHandler();
			this.numberOfTaggedRecords = this.GetNumberOfCheckedItems();
			this.tbNumberOfRecords.Text = this.numberOfTaggedRecords.ToString();
			this.CheckForNextVisibility();

		}
		#region � TagListView �
		private void TagListView (bool ckhValue, bool forceEmail)
		{
			for (int i = 0; i < this.lvRecipients.Items.Count; i++)
			{
				ListViewItem lvi = (ListViewItem)this.lvRecipients.Items[i];

                if (ckhValue && cbDoNotMail.Checked)
                {
                    Guid propertyItemId = (Guid)lvi.Tag;
                    string fssa = lvi.SubItems[2].Text;

                    /*
					if (null != doNotMailData && 0 < doNotMailData.Count)
					{
						if (!doNotMailData.Contains(propertyItemId))						
							lvi.Checked = ckhValue;
					}
					else				
						lvi.SubItems.Add(NO);					
					*/
                    if (!TestForDoNotMail(propertyItemId, fssa))
                    {
                        lvi.Checked = ckhValue;

                        if (forceEmail)
                            if (string.Empty == lvi.SubItems[17].Text)
                                lvi.Checked = false;
                    }
                }
                else
                {
                    lvi.Checked = ckhValue;

                    if (forceEmail)
                        if (string.Empty == lvi.SubItems[17].Text)
                            lvi.Checked = false;
                }
                        
			}
		}
		#endregion � TagListView �

		private int GetNumberOfCheckedItems ()
		{
			int iCnt = 0;
			for (int i = 0; i < this.lvRecipients.Items.Count; i++)
			{
				ListViewItem lvi = (ListViewItem)this.lvRecipients.Items[i];
                if (null != lvi)
                {
                    if (true == lvi.Checked)
                        iCnt++;
                }
			}
			return iCnt;
		}

		private void bUnTagAll_Click(object sender, System.EventArgs e)
		{
            RemoveEventHandler();
			this.TagListView(false, false);
            AddEventHandler();
			this.numberOfTaggedRecords = this.GetNumberOfCheckedItems();
			this.tbNumberOfRecords.Text = this.numberOfTaggedRecords.ToString();
			this.CheckForNextVisibility();
		}

		

        //private void lvRecipients_MouseUp(object sender, System.Windows.Forms.MouseEventArgs e)
        //{
            //this.numberOfTaggedRecords = this.GetNumberOfCheckedItems();
            //this.tbNumberOfRecords.Text = this.numberOfTaggedRecords.ToString();
            //this.CheckForNextVisibility();
        //}

        private void lvRecipients_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            this.numberOfTaggedRecords = this.GetNumberOfCheckedItems();
            this.tbNumberOfRecords.Text = this.numberOfTaggedRecords.ToString();
            this.CheckForNextVisibility();
        }

        
		private void lvRecipients_ColumnClick(object sender, System.Windows.Forms.ColumnClickEventArgs e)
		{
			// Set the ListViewItemSorter property to a new ListViewItemComparer 
			// object. Setting this property immediately sorts the 
			// ListView using the ListViewItemComparer object.			
			this.asc = !this.asc;
			if (true == this.asc)
				this.lvRecipients.ListViewItemSorter = new ListViewItemComparer(e.Column);
			else
				this.lvRecipients.ListViewItemSorter = new ListViewItemComparerDesc(e.Column);
		}
		private void CheckForNextVisibility ()
		{
			if (0 == this.numberOfTaggedRecords)
			{
				this.SetNextState(false);
			}
			else
			{
				this.SetNextState(true);
			}
		}
		private void SetNextState(bool enabled)
		{
            try
            {
                Panel upperPanel = (Panel)this.Parent;
                MailFaxWizardForm mfwf = (MailFaxWizardForm)upperPanel.Parent;
                mfwf.bNext.Enabled = enabled;
            }
            catch 
            {
                bool stopHere = true;
            }
		}

		public void Next()
		{
			ArrayList alSelectedItems = new	ArrayList();
			for (int i = 0; i < this.lvRecipients.Items.Count; i++)
			{
				ListViewItem lvi = (ListViewItem)this.lvRecipients.Items[i];
				if (true == lvi.Checked)
				{
					alSelectedItems.Add((Guid)lvi.Tag);
				}
			}
			this.mfwObject.SelectedRecords = alSelectedItems;
		}

		private void MailFaxWizardForm3_Load(object sender, System.EventArgs e)
		{
			this.CheckForNextVisibility();
		}

		private void readMe1_MouseEnter(object sender, EventArgs e)
		{
			this.floater = new DealMaker.Forms.Floaters.MFWViewandTagRecipients();


			Point pReadMe = this.readMe1.Location;
			Size fSize = this.floater.Size;
						
			//this.floater.Location = new Point(pReadMe.X - fSize.Width, pReadMe.Y + this.readMe1.Height);
			this.floater.Location = PointToScreen(new Point (pReadMe.X - fSize.Width, pReadMe.Y) );

			this.floater.Show();
		}

		private void readMe1_MouseLeave(object sender, EventArgs e)
		{
			this.floater.Hide();
		}

		bool TestForDoNotMail(Guid propertyItemId, string fullSiteStreetAddress)
		{
			if (null == doNotMailData || 0 == doNotMailData.Count)
				return false;
			
			if (doNotMailData.Contains(propertyItemId))
				return true;
			else
			{
				foreach (PropertyItem pi in doNotMailContacts)
				{
					if (null != pi.Owner && null != pi.Owner.SiteAddress)
					{
						if (pi.Owner.SiteAddress.FullSiteStreetAddress.Equals(fullSiteStreetAddress))
							return true;
					}
				}
				return false;
			}
		}

       
    }	
}
