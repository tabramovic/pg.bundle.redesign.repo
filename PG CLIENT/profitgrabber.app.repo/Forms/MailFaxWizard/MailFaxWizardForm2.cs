using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;

namespace DealMaker
{
	/// <summary>
	/// Summary description for MailFaxWizardForm2.
	/// </summary>
	public class MailFaxWizardForm2 : System.Windows.Forms.UserControl
	{
		private System.Windows.Forms.ColumnHeader tag;
		private System.Windows.Forms.ColumnHeader fullName;
		private System.Windows.Forms.ColumnHeader mailMergeStreetAddress;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private MailFaxWizard mailFaxWizardObject = null;
		private System.Windows.Forms.ListView lvDoNotMail;
		private System.Windows.Forms.Button _TagAll;
		private System.Windows.Forms.Button _UnTagAll;
        private ColumnHeader emailAddress;

        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.Container components = null;

		public MailFaxWizardForm2(MailFaxWizard mfwObject)
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();
			_TagAll.Click += new EventHandler(OnTagAllClick);
			_UnTagAll.Click += new EventHandler(OnUnTagAllClick);

			this.mailFaxWizardObject = mfwObject;

			Hashtable htDoNotMailItems = ApplicationHelper.GetPropertyItemsInNode(21);
			Hashtable htItemsInGroup =  FillItems(mfwObject.SelectedGroupIds);// = ApplicationHelper.GetPropertyItemsInNode(mfwObject.SelectedGroupId);			
			
			//this.FillListView(htDoNotMailItems);
			this.FillListView(MatchContacts(htDoNotMailItems, htItemsInGroup));
		}

		Hashtable FillItems(int[] selectedGroupIds)
		{
			IDictionaryEnumerator ide;
			Hashtable ht = new Hashtable();
			foreach (int groupId in selectedGroupIds)
			{
				Hashtable htItemsInGroup = ApplicationHelper.GetPropertyItemsInNode(groupId);
				ide = htItemsInGroup.GetEnumerator();
				while (ide.MoveNext())
				{
					if (!ht.ContainsValue(ide.Value))
					{
						if (ht.ContainsKey(ide.Key))
							ht.Add((int)ide.Key + 10000, ide.Value);
						else
							ht.Add(ide.Key, ide.Value);
					}
				}
			}
			return ht;
		}

		Hashtable MatchContacts(Hashtable doNotMailItems, Hashtable itemsInGroups)
		{
			Hashtable res = new Hashtable();
			IDictionaryEnumerator ide = doNotMailItems.GetEnumerator();
			while (ide.MoveNext())
			{
				if (itemsInGroups.ContainsValue((Guid)ide.Value))
					res.Add((int)ide.Key, (Guid)ide.Value);
			}
			return res;
		}

		private void FillListView(Hashtable htDoNotMailItems)
		{
			IDictionaryEnumerator ide = htDoNotMailItems.GetEnumerator();
			while (ide.MoveNext())
			{
				PropertyItem pi = ApplicationHelper.LoadPropertyItem((Guid)ide.Value);
				ListViewItem lvi = new ListViewItem();
				lvi.SubItems.Add(pi.Owner.FullName);
				string address = string.Empty;
				if (null != pi && null != pi.Owner && null != pi.Owner.MailAddress)
				{
					if (string.Empty != pi.Owner.MailAddress.FullSiteStreetAddress.Trim())
					{
						address = pi.Owner.MailAddress.FullSiteStreetAddress;
						address += ", ";
					}
					else
						address += pi.Owner.MailAddress.SiteCitySiteStateSiteZIP;
				}
				
				lvi.SubItems.Add(address);

                var emailAddress = null != pi?.Owner?.EMail ? pi.Owner.EMail : "Not Present";
                lvi.SubItems.Add(emailAddress);

                lvi.Tag = (Guid)ide.Value;
				this.lvDoNotMail.Items.Add(lvi);
			}
		}

		public MailFaxWizard MailFaxWizardObject
		{
			get {return this.mailFaxWizardObject;}
			set {this.mailFaxWizardObject = value;}
		}
		

		public void Next()
		{
			ArrayList alExpelledItems = new ArrayList();
			for (int i = 0; i < this.lvDoNotMail.Items.Count; i++)
			{
				ListViewItem lvi = (ListViewItem)this.lvDoNotMail.Items[i];
				if (true == lvi.Checked)
				{
					alExpelledItems.Add((Guid)lvi.Tag);
				}
			}
			this.mailFaxWizardObject.ExpelledRecords = alExpelledItems;
		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.lvDoNotMail = new System.Windows.Forms.ListView();
            this.tag = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.fullName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.mailMergeStreetAddress = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this._TagAll = new System.Windows.Forms.Button();
            this._UnTagAll = new System.Windows.Forms.Button();
            this.emailAddress = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.SuspendLayout();
            // 
            // lvDoNotMail
            // 
            this.lvDoNotMail.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lvDoNotMail.CheckBoxes = true;
            this.lvDoNotMail.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.tag,
            this.fullName,
            this.mailMergeStreetAddress,
            this.emailAddress});
            this.lvDoNotMail.FullRowSelect = true;
            this.lvDoNotMail.GridLines = true;
            this.lvDoNotMail.Location = new System.Drawing.Point(24, 80);
            this.lvDoNotMail.Name = "lvDoNotMail";
            this.lvDoNotMail.Size = new System.Drawing.Size(488, 200);
            this.lvDoNotMail.TabIndex = 0;
            this.lvDoNotMail.UseCompatibleStateImageBehavior = false;
            this.lvDoNotMail.View = System.Windows.Forms.View.Details;
            // 
            // tag
            // 
            this.tag.Text = "Tag";
            // 
            // fullName
            // 
            this.fullName.Text = "Full Name";
            this.fullName.Width = 200;
            // 
            // mailMergeStreetAddress
            // 
            this.mailMergeStreetAddress.Text = "Mail Merge Street Address";
            this.mailMergeStreetAddress.Width = 351;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label1.Location = new System.Drawing.Point(24, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(344, 23);
            this.label1.TabIndex = 1;
            this.label1.Text = "DO NOT MAIL Screening:";
            // 
            // label2
            // 
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label2.Location = new System.Drawing.Point(40, 40);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(320, 23);
            this.label2.TabIndex = 2;
            this.label2.Text = "Tag records that you want to eliminate from your Mail Merge!";
            // 
            // _TagAll
            // 
            this._TagAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._TagAll.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._TagAll.Location = new System.Drawing.Point(440, 304);
            this._TagAll.Name = "_TagAll";
            this._TagAll.Size = new System.Drawing.Size(75, 23);
            this._TagAll.TabIndex = 3;
            this._TagAll.Text = "Tag All";
            // 
            // _UnTagAll
            // 
            this._UnTagAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._UnTagAll.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._UnTagAll.Location = new System.Drawing.Point(352, 304);
            this._UnTagAll.Name = "_UnTagAll";
            this._UnTagAll.Size = new System.Drawing.Size(75, 23);
            this._UnTagAll.TabIndex = 4;
            this._UnTagAll.Text = "Untag All";
            // 
            // emailAddress
            // 
            this.emailAddress.Text = "Email Address";
            // 
            // MailFaxWizardForm2
            // 
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.Controls.Add(this._UnTagAll);
            this.Controls.Add(this._TagAll);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lvDoNotMail);
            this.Name = "MailFaxWizardForm2";
            this.Size = new System.Drawing.Size(528, 344);
            this.Load += new System.EventHandler(this.MailFaxWizardForm2_Load);
            this.ResumeLayout(false);

		}
		#endregion

		private void MailFaxWizardForm2_Load(object sender, System.EventArgs e)
		{
		
		}

		private void OnTagAllClick(object sender, EventArgs e)
		{
			CheckAllLVIs(true);
		}

		private void OnUnTagAllClick(object sender, EventArgs e)
		{
			CheckAllLVIs(false);
		}

		void CheckAllLVIs(bool val)
		{
			foreach (ListViewItem lvi in this.lvDoNotMail.Items)
			{
				lvi.Checked = val;
			}
		}
	}
}
