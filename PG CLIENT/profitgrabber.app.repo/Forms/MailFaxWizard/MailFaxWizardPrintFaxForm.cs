using System;
using System.IO;
using System.Data;
using System.Drawing;
using System.Threading;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data.SqlTypes;
using System.Data.SqlClient;
using NHibernate;
using NHibernate.Cfg;

using DealMaker.Faxing;
using DealMaker.PlainClasses.Fax;

namespace DealMaker
{
	/// <summary>
	/// Summary description for MailFaxWizardPrintFaxForm.
	/// </summary>
	public class MailFaxWizardPrintFaxForm : System.Windows.Forms.Form
	{
		bool cancelThreads = false;
		bool fax = false;
		bool preview = false;
		bool print = false;
        string pdfPath = null;

		private MailFaxWizard mfwObject;
		private EditDocumentTemplateUserControl edtuc = null;
		private System.Windows.Forms.Panel upperPanel;
		private System.Windows.Forms.Panel lowerPanel;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox tbDocName;
		private System.Windows.Forms.TextBox tbCurrent;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TextBox tbMaxCount;
		private System.Windows.Forms.Button bClose;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public MailFaxWizardPrintFaxForm()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			this.edtuc.txTextControl1.ContextMenu = null;	//removing ctxMenu
			this.edtuc.txTextControl1.EditMode = TXTextControl.EditMode.ReadOnly;	//read only enabled
			this.edtuc.DoNotStore = true;
            AutoClose = false;
            AppendPGID = false;

            this.OnUpdateHeader += MailFaxWizardPrintFaxForm_OnUpdateHeader;
            this.OnUpdatePreviewLables += MailFaxWizardPrintFaxForm_OnUpdatePreviewLables;
        }
        
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		protected override void OnClosing(CancelEventArgs e)
		{
			this.cancelThreads = true;
			base.OnClosing (e);
		}


		#region � Properties �
		public MailFaxWizard MailFaxWizardObject
		{
			get { return this.mfwObject; }
			set { this.mfwObject = value; }
		}
		public bool Fax
		{
			get {return this.fax;}
			set {this.fax = value;}
		}
		public bool Preview
		{
			get {return this.preview;}
			set {this.preview = value;}
		}

		public bool Print
		{
			get {return this.print;}
			set {this.print = value;}
		}

        public bool SaveToPDF
        {
            get { return (null != pdfPath ? true : false); }            
        }

        public string PDFPath
        {
            get { return pdfPath; }
            set 
            { 
                pdfPath = value;

                if (!Directory.Exists(pdfPath))
                    Directory.CreateDirectory(pdfPath);
            }
        }

        public bool AutoClose { get; set; }
        public bool AppendPGID { get; set; }
        #endregion � Properties �

        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MailFaxWizardPrintFaxForm));
            this.upperPanel = new System.Windows.Forms.Panel();
            this.bClose = new System.Windows.Forms.Button();
            this.tbMaxCount = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tbCurrent = new System.Windows.Forms.TextBox();
            this.tbDocName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lowerPanel = new System.Windows.Forms.Panel();
            this.edtuc = new DealMaker.EditDocumentTemplateUserControl();
            this.upperPanel.SuspendLayout();
            this.lowerPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // upperPanel
            // 
            this.upperPanel.Controls.Add(this.bClose);
            this.upperPanel.Controls.Add(this.tbMaxCount);
            this.upperPanel.Controls.Add(this.label3);
            this.upperPanel.Controls.Add(this.tbCurrent);
            this.upperPanel.Controls.Add(this.tbDocName);
            this.upperPanel.Controls.Add(this.label2);
            this.upperPanel.Controls.Add(this.label1);
            this.upperPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.upperPanel.Location = new System.Drawing.Point(0, 0);
            this.upperPanel.Name = "upperPanel";
            this.upperPanel.Size = new System.Drawing.Size(712, 72);
            this.upperPanel.TabIndex = 0;
            // 
            // bClose
            // 
            this.bClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.bClose.Enabled = false;
            this.bClose.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.bClose.Location = new System.Drawing.Point(632, 35);
            this.bClose.Name = "bClose";
            this.bClose.Size = new System.Drawing.Size(75, 23);
            this.bClose.TabIndex = 6;
            this.bClose.Text = "Close";
            this.bClose.Click += new System.EventHandler(this.bClose_Click);
            // 
            // tbMaxCount
            // 
            this.tbMaxCount.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbMaxCount.Location = new System.Drawing.Point(130, 40);
            this.tbMaxCount.Name = "tbMaxCount";
            this.tbMaxCount.ReadOnly = true;
            this.tbMaxCount.Size = new System.Drawing.Size(37, 13);
            this.tbMaxCount.TabIndex = 5;
            this.tbMaxCount.TabStop = false;
            this.tbMaxCount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.Location = new System.Drawing.Point(108, 40);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(16, 23);
            this.label3.TabIndex = 4;
            this.label3.Text = "of";
            // 
            // tbCurrent
            // 
            this.tbCurrent.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbCurrent.Location = new System.Drawing.Point(66, 40);
            this.tbCurrent.Name = "tbCurrent";
            this.tbCurrent.ReadOnly = true;
            this.tbCurrent.Size = new System.Drawing.Size(37, 13);
            this.tbCurrent.TabIndex = 3;
            this.tbCurrent.TabStop = false;
            this.tbCurrent.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // tbDocName
            // 
            this.tbDocName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbDocName.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbDocName.Location = new System.Drawing.Point(108, 16);
            this.tbDocName.Name = "tbDocName";
            this.tbDocName.ReadOnly = true;
            this.tbDocName.Size = new System.Drawing.Size(542, 13);
            this.tbDocName.TabIndex = 2;
            this.tbDocName.TabStop = false;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(16, 40);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 23);
            this.label2.TabIndex = 1;
            this.label2.Text = "Printing";
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(16, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 23);
            this.label1.TabIndex = 0;
            this.label1.Text = "Print Status for:";
            // 
            // lowerPanel
            // 
            this.lowerPanel.Controls.Add(this.edtuc);
            this.lowerPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lowerPanel.Location = new System.Drawing.Point(0, 72);
            this.lowerPanel.Name = "lowerPanel";
            this.lowerPanel.Size = new System.Drawing.Size(712, 494);
            this.lowerPanel.TabIndex = 1;
            // 
            // edtuc
            // 
            this.edtuc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.edtuc.DoNotStore = false;
            this.edtuc.Location = new System.Drawing.Point(0, 0);
            this.edtuc.Name = "edtuc";
            this.edtuc.OfferedPrice = "";
            this.edtuc.Size = new System.Drawing.Size(712, 494);
            this.edtuc.TabIndex = 0;
            // 
            // MailFaxWizardPrintFaxForm
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.ClientSize = new System.Drawing.Size(712, 566);
            this.Controls.Add(this.lowerPanel);
            this.Controls.Add(this.upperPanel);
            this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MailFaxWizardPrintFaxForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Mail / Fax Wizard - Print / Fax Form";
            this.Load += new System.EventHandler(this.MailFaxWizardPrintFaxForm_Load);
            this.upperPanel.ResumeLayout(false);
            this.upperPanel.PerformLayout();
            this.lowerPanel.ResumeLayout(false);
            this.ResumeLayout(false);

		}
		#endregion

		private void MailFaxWizardPrintFaxForm_Load(object sender, System.EventArgs e)
		{
			if (false == this.Fax)
			{
				Thread printWorker = new Thread(new ThreadStart(this.PrintWorker));
				printWorker.Start();
			}
			else
			{				
				Thread faxWorker = new Thread(new ThreadStart(this.FaxWorker));
				faxWorker.Start();
			}
		}

		private void SaveTask(Task t)
		{
			ISession session = null;
			ITransaction transaction = null;
			try
			{				
				Connection conn = Connection.getInstance();     
				SqlConnection sqlConn = conn.HatchConn;
				IDbConnection iSqlConn = sqlConn as IDbConnection;

				Configuration cfg = Globals.cfg;				
				ISessionFactory factory = Globals.factory;
				
				
				if (null != iSqlConn) 
				{
					session = factory.OpenSession(iSqlConn);
					if (ConnectionState.Closed == iSqlConn.State) 
					{
						iSqlConn.Open();
					}
				}
				else 
				{
					session = factory.OpenSession();
				}				
				transaction = session.BeginTransaction();					
					
				session.Save(t);					
				transaction.Commit();										
						
			}
			catch (Exception exc)
			{
				MessageBox.Show(exc.ToString());
				transaction.Rollback();
			}				
			finally
			{				
				session.Close();					
			}			
		}

        private void MailFaxWizardPrintFaxForm_OnUpdatePreviewLables(bool tbCurrentVisible, bool tbMaxCountVisible, bool label3Visible, string label1Text, string label2Text, bool bCloseEnabled)
        {
            UpdatePreviewLabels(tbCurrentVisible, tbMaxCountVisible, label3Visible, label1Text, label2Text, bCloseEnabled);
        }

        private void MailFaxWizardPrintFaxForm_OnUpdateHeader(int current, int total, string documentName)
        {
            UpdateHeader(current, total, documentName);
        }

        event UpdateHeaderEventHandler OnUpdateHeader;
        delegate void UpdateHeaderEventHandler(int current, int total, string documentName);

        void UpdateHeader(int current, int total, string documentName)
        {
            if (this.InvokeRequired)
                this.BeginInvoke(new UpdateHeaderEventHandler(UpdateHeader), new object[] { current, total, documentName });
            else
            {
                tbCurrent.Text = current.ToString();
                tbMaxCount.Text = total.ToString();
                tbDocName.Text = documentName;

                //tbCurrent.Refresh();
                //tbMaxCount.Refresh();
                //tbDocName.Refresh();

                Application.DoEvents();
            }
        }

        event UpdatePreviewLabelsEventHandler OnUpdatePreviewLables;
        delegate void UpdatePreviewLabelsEventHandler(bool tbCurrentVisible, bool tbMaxCountVisible, bool label3Visible, string label1Text, string label2Text, bool bCloseEnabled);
        void UpdatePreviewLabels(bool tbCurrentVisible, bool tbMaxCountVisible, bool label3Visible, string label1Text, string label2Text, bool bCloseEnabled)
        {
            if (this.InvokeRequired)
                this.BeginInvoke(new UpdatePreviewLabelsEventHandler(UpdatePreviewLabels), new object[] { tbCurrentVisible, tbMaxCountVisible, label3Visible, label1Text, label2Text, bCloseEnabled });
            else
            {
                this.tbCurrent.Visible = tbCurrentVisible;
                this.tbMaxCount.Visible = tbMaxCountVisible;
                this.label3.Visible = label3Visible;
                this.label1.Text = label1Text;
                this.label2.Text = label2Text;
                this.bClose.Enabled = bCloseEnabled;
            }

        }

        delegate void PrintWorkerDelegate();
		private void PrintWorker()
		{
            if (InvokeRequired)
                this.BeginInvoke(new PrintWorkerDelegate(PrintWorker));
            else
            {

                ArrayList alSelectedrecords = this.MailFaxWizardObject.SelectedRecords;
                if (0 == alSelectedrecords.Count)
                {
                    return;
                }
                bool isEmailBlast = eLabelDocumentSelection.emailBlast == MailFaxWizardObject.LabelDocSel || eLabelDocumentSelection.emailLOI == MailFaxWizardObject.LabelDocSel ? true : false;

                //this.tbDocName.Text = this.MailFaxWizardObject.DocumentName;
                var shortDocName = Path.GetFileName(this.MailFaxWizardObject.DocumentName);
                string docName = string.Empty; //(Globals.LOI == this.MailFaxWizardObject.DocumentName ? Application.StartupPath + Globals.FaxPath + this.MailFaxWizardObject.DocumentName + ".tx" : this.MailFaxWizardObject.DocumentName);

                if (Globals.LOI == this.MailFaxWizardObject.DocumentName)
                    docName = Application.StartupPath + Globals.FaxPath + this.MailFaxWizardObject.DocumentName + ".tx";

                if (eLabelDocumentSelection.mailDocument == MailFaxWizardObject.LabelDocSel)
                    docName = Application.StartupPath + Globals.MailPath + this.MailFaxWizardObject.DocumentName + ".tx";

                if (isEmailBlast)
                    docName = this.MailFaxWizardObject.DocumentName;


                bool usePercentage = (Globals.LOI == this.MailFaxWizardObject.DocumentName || eLabelDocumentSelection.emailLOI == MailFaxWizardObject.LabelDocSel ? true : false);
                

                this.edtuc.LoadDoc(docName);

                if (true == this.Preview)
                {
                    if (null != OnUpdatePreviewLables)
                        OnUpdatePreviewLables(false, false, false, "Print Preview", "Preview", true);
                    /*this.tbCurrent.Visible = false;
                    this.tbMaxCount.Visible = false;
                    this.label3.Visible = false;
                    this.label1.Text = "Print Preview";
                    this.label2.Text = "Preview";
                    this.bClose.Enabled = true;*/

                    PropertyItem pi = PropertyItem.LoadPropertyItem((Guid)alSelectedrecords[0]);
                    MergeMappingFields(pi, usePercentage, this.MailFaxWizardObject.Percentage);
                }
                else
                {

                    //this.tbMaxCount.Text = alSelectedrecords.Count.ToString();
                    
                    for (int i = 0; i < alSelectedrecords.Count; i++)
                    {
                        if (null != OnUpdateHeader)
                            OnUpdateHeader(i + 1, alSelectedrecords.Count, shortDocName);
                                                
                        //TA++:Abort Thread
                        if (this.cancelThreads == true)
                        {
                            Thread.CurrentThread.Abort();
                        }
                        //TA--:Abort Thread

                        //this.tbCurrent.Text = (i + 1).ToString();

                        //TA++ 17.05.2005
                        //Bug Nr.:RQ_0001
                        //OLD++
                        /*
                            PropertyItem pi = PropertyItem.LoadPropertyItem((Guid)alSelectedrecords[0]);
                        */
                        //OLD--

                        try
                        {
                            //NEW++
                            PropertyItem pi = PropertyItem.LoadPropertyItem((Guid)alSelectedrecords[i]);
                            //NEW--
                            //TA-- 17.05.2005
                            MergeMappingFields(pi, usePercentage, this.MailFaxWizardObject.Percentage);

                            if (SaveToPDF)
                            {
                                string fileName = Path.GetFileNameWithoutExtension(this.MailFaxWizardObject.DocumentName) + " for " + pi.Owner?.SiteAddress?.FullSiteStreetAddress;

                                if (AppendPGID)
                                    fileName = $"{fileName}{"."}{pi.IdPropertyItem}";

                                int idx = fileName.IndexOfAny(Path.GetInvalidFileNameChars());
                                if (-1 != idx)
                                    fileName = fileName.Substring(0, idx);

                                fileName += ".pdf";
                                //fileName += ".html";

                                this.edtuc.txTextControl1.Save(pdfPath + @"\" + fileName, TXTextControl.StreamType.AdobePDF);
                                //this.edtuc.txTextControl1.Save(pdfPath + @"\" + fileName, TXTextControl.StreamType.HTMLFormat);
                            }
                            else
                                this.edtuc.PrintDirect(this.MailFaxWizardObject.DocumentName + " for " + pi.Owner.FullName != string.Empty ? pi.Owner.FullName : "Unknown");

                            Task t = new Task();
                            t.IdPropertyItem = pi.IdPropertyItem;
                            t.Status = (int)Status.Done;
                            t.Type = (int)TaskType.Mail;
                            t.TaskDate = DateTime.Today;
                            t.Description = this.MailFaxWizardObject.DocumentName;
                            this.SaveTask(t);
                            this.EnableCloseButton(i + 1, alSelectedrecords.Count);

                            if (i + 1 == alSelectedrecords.Count)
                                if (AutoClose)
                                    this.CloseFormAsync();
                        }
                        catch (Exception exc)
                        {
#if DEBUG
                            MessageBox.Show(exc.Message);
#endif
                        }
                    }
                }
            }
		}

        delegate void FaxWorkerDelegate();
		private void FaxWorker()
		{
            if (InvokeRequired)
            {
                BeginInvoke(new FaxWorkerDelegate(FaxWorker));
            }
            else
            {
                ArrayList alSelectedrecords = this.MailFaxWizardObject.SelectedRecords;
                ArrayList alDocsToFax = new ArrayList();
                if (0 == alSelectedrecords.Count)
                {
                    return;
                }
                this.tbDocName.Text = this.MailFaxWizardObject.DocumentName;

                this.edtuc.LoadDoc(Application.StartupPath + Globals.FaxPath + this.MailFaxWizardObject.DocumentName + ".tx");

                if (true == this.Preview)
                {
                    this.tbCurrent.Visible = false;
                    this.tbMaxCount.Visible = false;
                    this.label3.Visible = false;
                    this.label1.Text = "Fax Preview";
                    this.label2.Text = "Preview";
                    this.bClose.Enabled = true;


                    PropertyItem pi = PropertyItem.LoadPropertyItem((Guid)alSelectedrecords[0]);

                    if (this.MailFaxWizardObject.DocumentName == Globals.LOI)
                    {
                        MergeMappingFields(pi, true, this.MailFaxWizardObject.Percentage);
                    }
                    else
                    {
                        MergeMappingFields(pi, false, 0);
                    }
                }
                else
                {
                    this.tbMaxCount.Text = alSelectedrecords.Count.ToString();
                    for (int i = 0; i < alSelectedrecords.Count; i++)
                    {
                        //TA++:Abort Thread 
                        if (this.cancelThreads == true)
                        {
                            Thread.CurrentThread.Abort();
                        }
                        //TA--

                        this.tbCurrent.Text = (i + 1).ToString();
                        PropertyItem pi = PropertyItem.LoadPropertyItem((Guid)alSelectedrecords[i]);
                        if (this.MailFaxWizardObject.DocumentName == Globals.LOI)
                        {
                            MergeMappingFields(pi, true, this.MailFaxWizardObject.Percentage);
                        }
                        else
                        {
                            MergeMappingFields(pi, false, 0);
                        }
                        string fileName = this.MailFaxWizardObject.DocumentName + " for ";
                        if (pi.Owner.FullName.Trim() != string.Empty)
                        {
                            fileName += pi.Owner.FullName;
                        }
                        else
                        {
                            fileName += pi.IdPropertyItem.ToString();
                        }

                        bool _saveTask = false;
                        if (null != pi.ListingAgent && pi.ListingAgent.ListingAgentFaxNumber != string.Empty)
                        {
                            string faxNumber = pi.ListingAgent.ListingAgentFaxNumber;
                            string pureFaxNumber = string.Empty;
                            for (int iCnt = 0; iCnt < faxNumber.Length; iCnt++)
                            {
                                if (InterfaxWebServiceSupport.Instance.UseIfxInterface)
                                {
                                    if (true == (Char.IsNumber((char)faxNumber[iCnt])) || '+' == faxNumber[iCnt])
                                        pureFaxNumber += faxNumber[iCnt];
                                }
                                else
                                {
                                    if (true == (Char.IsNumber((char)faxNumber[iCnt])))
                                        pureFaxNumber += faxNumber[iCnt];
                                }
                            }

                            if (string.Empty != pureFaxNumber)
                            {
                                if (-1 != fileName.IndexOf("/"))
                                    fileName = fileName.Replace("/", "-");

                                string fn = Application.StartupPath + @"\DocsToFax\" + fileName + ".rtf";
                                FaxItem fi = new FaxItem(this.MailFaxWizardObject.DocumentName, fn, pi.Owner.FullName, pureFaxNumber);
                                alDocsToFax.Add(fi);
                                _saveTask = true;
                            }
                        }

                        this.edtuc.SaveFile_RTF(fileName);                        

                        if (_saveTask)
                        {
                            Task t = new Task();
                            t.IdPropertyItem = pi.IdPropertyItem;
                            t.Status = (int)Status.Done;
                            t.Type = (int)TaskType.Fax;
                            t.TaskDate = DateTime.Today;
                            t.Description = this.MailFaxWizardObject.DocumentName;
                            this.SaveTask(t);
                        }

                        this.EnableCloseButton(i + 1, alSelectedrecords.Count);
                    }
                    CloseFormAsync();	//FOR INTERFAX CONSOLE
                }


                try
                {

                    if (InterfaxWebServiceSupport.Instance.UseIfxInterface)
                    {
                        InterfaxWebServiceSupport.Instance.SendFaxes(alDocsToFax);
                    }
                    else
                    {
                        if (0 != alDocsToFax.Count)
                        {                            
                            BatchFax bf = new BatchFax();
                            bf.Send(alDocsToFax);                            
                        }
                    }
                }
                catch (Exception exc)
                {
                    MessageBox.Show(this, exc.Message, "Faxing Error", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
            }
		}

        delegate void EnableCloseButtonDelegate(int currentCount, int maxCount);
        private void EnableCloseButton(int currentCount, int maxCount)
		{
            if (InvokeRequired)
                BeginInvoke(new EnableCloseButtonDelegate(EnableCloseButton), new object[] { currentCount, maxCount });
            else
            {
                if (maxCount == currentCount)
                    this.bClose.Enabled = true;
            }
		}


		private void MergeMappingFields(PropertyItem pi, bool usePercentage, double perc)
		{
			edtuc.MergeMappingFields(pi, usePercentage, perc);
            //Thread.Sleep(200);
            Thread.Sleep(1);
        }

		private void bClose_Click(object sender, System.EventArgs e)
		{
            //this.Close();
            CloseFormAsync();
		}

		delegate void CloseFormAsyncDelegate();
		void CloseFormAsync()
		{
			if (InvokeRequired)
				BeginInvoke(new CloseFormAsyncDelegate(CloseFormAsync), null);
			else
				this.Close();
		}        
    }
}
