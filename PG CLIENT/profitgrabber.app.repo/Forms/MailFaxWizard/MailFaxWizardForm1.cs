using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;

namespace DealMaker
{
	/// <summary>
	/// Summary description for MailFaxWizardForm1.
	/// </summary>
	public class MailFaxWizardForm1 : System.Windows.Forms.UserControl
	{
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.RadioButton rbCurrentLookup;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TextBox tbSelection;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.CheckBox cbEliminate;
		private MailFaxWizard mfwObject;
		private System.Windows.Forms.RadioButton rbGroup;
		private System.Windows.Forms.Button bBrowse;
		private System.Windows.Forms.PictureBox pbPicure;
		private System.Windows.Forms.Button _bClear;
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public MailFaxWizardForm1(MailFaxWizard mfwObject)
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();
			
			this.cbEliminate.Checked = mfwObject.ExcludeDoNotMailRecords;
			if (eSelection.currentLookup == mfwObject.Selection)
			{
				this.rbCurrentLookup.Checked = true;
			}
			else if (eSelection.selectedGroup == mfwObject.Selection)
			{
				this.rbGroup.Checked = true;
				this.tbSelection.Text = mfwObject.SelectedGroupName;
			}
			//this.bBrowse.Enabled = CheckBrowseVisibility();
			
			this.mfwObject = mfwObject;			
		}

		public bool CheckBrowseVisibility()
		{			
				return this.rbGroup.Checked;
		}

		public void Next()
		{
			//if ()
		}

		public MailFaxWizard MailFaxWizardObject
		{
			get 
			{
				if (true == this.rbGroup.Checked)
					this.mfwObject.Selection = eSelection.selectedGroup;
				else if (true == this.rbCurrentLookup.Checked)
					this.mfwObject.Selection = eSelection.currentLookup;

				return this.mfwObject;
			}
			set {this.mfwObject = value;}
		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(MailFaxWizardForm1));
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.rbCurrentLookup = new System.Windows.Forms.RadioButton();
			this.rbGroup = new System.Windows.Forms.RadioButton();
			this.label3 = new System.Windows.Forms.Label();
			this.tbSelection = new System.Windows.Forms.TextBox();
			this.label4 = new System.Windows.Forms.Label();
			this.cbEliminate = new System.Windows.Forms.CheckBox();
			this.bBrowse = new System.Windows.Forms.Button();
			this.pbPicure = new System.Windows.Forms.PictureBox();
			this._bClear = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label1.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.label1.Location = new System.Drawing.Point(16, 16);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(256, 23);
			this.label1.TabIndex = 0;
			this.label1.Text = "Select Recipients";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label2
			// 
			this.label2.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.label2.Location = new System.Drawing.Point(32, 56);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(296, 23);
			this.label2.TabIndex = 1;
			this.label2.Text = "Select a group or a lookup that you want to mail or fax to:";
			// 
			// rbCurrentLookup
			// 
			this.rbCurrentLookup.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.rbCurrentLookup.Location = new System.Drawing.Point(272, 344);
			this.rbCurrentLookup.Name = "rbCurrentLookup";
			this.rbCurrentLookup.TabIndex = 2;
			this.rbCurrentLookup.TabStop = true;
			this.rbCurrentLookup.Text = "Current Lookup";
			this.rbCurrentLookup.Visible = false;
			this.rbCurrentLookup.CheckedChanged += new System.EventHandler(this.radioBoxChecedChanged);
			// 
			// rbGroup
			// 
			this.rbGroup.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.rbGroup.Location = new System.Drawing.Point(384, 344);
			this.rbGroup.Name = "rbGroup";
			this.rbGroup.TabIndex = 3;
			this.rbGroup.Text = "Group";
			this.rbGroup.Visible = false;
			this.rbGroup.CheckedChanged += new System.EventHandler(this.radioBoxChecedChanged);
			// 
			// label3
			// 
			this.label3.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.label3.Location = new System.Drawing.Point(32, 88);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(296, 23);
			this.label3.TabIndex = 4;
			this.label3.Text = "Current Selection For Mail Merge:";
			// 
			// tbSelection
			// 
			this.tbSelection.Location = new System.Drawing.Point(336, 88);
			this.tbSelection.Name = "tbSelection";
			this.tbSelection.ReadOnly = true;
			this.tbSelection.Size = new System.Drawing.Size(232, 20);
			this.tbSelection.TabIndex = 5;
			this.tbSelection.TabStop = false;
			this.tbSelection.Text = "";
			// 
			// label4
			// 
			this.label4.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.label4.Location = new System.Drawing.Point(8, 312);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(328, 23);
			this.label4.TabIndex = 6;
			this.label4.Text = "Eliminate \"Do Not Mail\" Records out of your selected recipients?";
			this.label4.Visible = false;
			// 
			// cbEliminate
			// 
			this.cbEliminate.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.cbEliminate.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.cbEliminate.Location = new System.Drawing.Point(336, 308);
			this.cbEliminate.Name = "cbEliminate";
			this.cbEliminate.TabIndex = 7;
			this.cbEliminate.Text = "Eliminate";
			this.cbEliminate.Visible = false;
			this.cbEliminate.CheckedChanged += new System.EventHandler(this.cbEliminate_CheckedChanged);
			// 
			// bBrowse
			// 
			this.bBrowse.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.bBrowse.Location = new System.Drawing.Point(336, 56);
			this.bBrowse.Name = "bBrowse";
			this.bBrowse.Size = new System.Drawing.Size(40, 23);
			this.bBrowse.TabIndex = 8;
			this.bBrowse.Text = "...";
			this.bBrowse.Click += new System.EventHandler(this.bBrowse_Click);
			// 
			// pbPicure
			// 
			this.pbPicure.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pbPicure.BackgroundImage")));
			this.pbPicure.Location = new System.Drawing.Point(104, 144);
			this.pbPicure.Name = "pbPicure";
			this.pbPicure.Size = new System.Drawing.Size(376, 100);
			this.pbPicure.TabIndex = 9;
			this.pbPicure.TabStop = false;
			// 
			// _bClear
			// 
			this._bClear.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this._bClear.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this._bClear.Location = new System.Drawing.Point(384, 56);
			this._bClear.Name = "_bClear";
			this._bClear.Size = new System.Drawing.Size(96, 23);
			this._bClear.TabIndex = 10;
			this._bClear.Text = "Clear Selection";
			this._bClear.Click += new System.EventHandler(this._bClear_Click);
			// 
			// MailFaxWizardForm1
			// 
			this.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(210)), ((System.Byte)(216)), ((System.Byte)(240)));
			this.Controls.Add(this._bClear);
			this.Controls.Add(this.pbPicure);
			this.Controls.Add(this.bBrowse);
			this.Controls.Add(this.cbEliminate);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.tbSelection);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.rbGroup);
			this.Controls.Add(this.rbCurrentLookup);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Name = "MailFaxWizardForm1";
			this.Size = new System.Drawing.Size(584, 384);
			this.ResumeLayout(false);

		}
		#endregion

		private void bBrowse_Click(object sender, System.EventArgs e)
		{
			SelectGroupForm sgf = new SelectGroupForm();
			sgf.ShowDialog();
			
			//this.mfwObject.SelectedGroupId = sgf.customizedTreeViewUserControl1.SelectedNodeId;			
			if (null == this.mfwObject.SelectedGroupIds)
			{
				this.mfwObject.SelectedGroupIds = new int[1];
				this.mfwObject.SelectedGroupIds[0] = sgf.customizedTreeViewUserControl1.SelectedNodeId;
			}
			else
			{
				foreach (int i in this.mfwObject.SelectedGroupIds)
				{
					if (i == sgf.customizedTreeViewUserControl1.SelectedNodeId)
					{
						MessageBox.Show("This group is already selected.", "Already existing", MessageBoxButtons.OK, MessageBoxIcon.Information);
						return;
					}
				}

				int[] newIds = new int[this.mfwObject.SelectedGroupIds.Length + 1];
				Array.Copy(this.mfwObject.SelectedGroupIds, 0, newIds, 0, this.mfwObject.SelectedGroupIds.Length);
				newIds[this.mfwObject.SelectedGroupIds.Length] = sgf.customizedTreeViewUserControl1.SelectedNodeId;
				this.mfwObject.SelectedGroupIds = newIds;
			}
			
			if (1 == this.mfwObject.SelectedGroupIds.Length)
				this.mfwObject.SelectedGroupName = sgf.customizedTreeViewUserControl1.SelectedNodeName;
			else
				this.mfwObject.SelectedGroupName =  this.mfwObject.SelectedGroupIds.Length.ToString() + " groups selected!";

			if (1 == this.mfwObject.SelectedGroupIds.Length)
				this.tbSelection.Text = sgf.customizedTreeViewUserControl1.SelectedNodeName;			
			else
				this.tbSelection.Text = this.mfwObject.SelectedGroupIds.Length.ToString() + " groups selected!";
		}

		private void radioBoxChecedChanged(object sender, System.EventArgs e)
		{
			//this.bBrowse.Enabled = this.CheckBrowseVisibility();			
		}

		private void cbEliminate_CheckedChanged(object sender, System.EventArgs e)
		{
			if (null != this.mfwObject)
				this.mfwObject.ExcludeDoNotMailRecords = this.cbEliminate.Checked;
		}

		private void _bClear_Click(object sender, System.EventArgs e)
		{
			this.tbSelection.Text = string.Empty;
			this.mfwObject.SelectedGroupName = string.Empty;
			this.mfwObject.SelectedGroupIds = null;
		}
	}
}
