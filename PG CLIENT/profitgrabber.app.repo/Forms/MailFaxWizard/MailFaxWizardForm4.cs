#region � Using �
using System;
using System.IO;
using System.Reflection;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;
using System.Linq;
#endregion � Using �
#region � Namespace DealMaker �
namespace DealMaker
{
	#region � Class MailFaxWizardForm4 �
	/// <summary>
	/// Summary description for MailFaxWizardForm4.
	/// </summary>
	public class MailFaxWizardForm4 : System.Windows.Forms.UserControl
	{
		#region � Data �
		private DealMaker.Forms.Floaters.MFWSelectMarketingPiece floater = null;
		
		private MailFaxWizard mfwObject;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TabControl tabControl1;
		private System.Windows.Forms.Label label3;
        public TextBox tbSelectedDocument;
        private System.Windows.Forms.TabPage tpMailingLabels;
		private System.Windows.Forms.TabPage tpLetters;
		private System.Windows.Forms.TabPage tpFaxDocuments;
		internal System.Windows.Forms.RadioButton rbLarge;
        internal System.Windows.Forms.RadioButton rbMedium;
        internal System.Windows.Forms.RadioButton rbSmall;
		private System.Windows.Forms.ListView lvLetters;
		internal System.Windows.Forms.ComboBox cbPercentage;
		private System.Windows.Forms.Label Percentage;
        internal RadioButton rbOtherFaxDocuments;
        internal System.Windows.Forms.ListView lvOtherFaxDocs;
		private System.Windows.Forms.ColumnHeader documentName;
		private System.Windows.Forms.ColumnHeader faxDocumentName;
        internal RadioButton rbLetterOfIntent;
        private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label4;
		private DealMaker.ReadMe readMe1;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.Label label8;
        private TabPage tpEmailTemplates;
        internal ComboBox _email_LOI_Perc;
        private Label label9;
        private Label label10;
        private Label label13;
        private Label label12;
        private GroupBox groupBox2;
        internal TextBox _email_LOI_ReplyTo;
        private Label label14;
        internal CheckBox _email_LOI_AppendAddress;
        internal TextBox _email_LOI_SubjectPrefix;
        internal TextBox _email_LOI_EmailBody;
        private Label label15;
        private Label label16;
        private Label label17;
        internal TextBox _email_LOI_DisplayName;
        private Label label18;
        private TabPage tpEmailBlast;
        private GroupBox groupBox1;
        public TextBox _email_Blast_DisplayName;
        private Label label20;
        public TextBox _email_Blast_ReplyTo;
        private Label label21;
        public CheckBox _email_Blast_AppendAddress;
        public TextBox _email_Blast_SubjectPrefix;
        public TextBox _email_Blast_EmailBody;
        private Label label22;
        private Label label23;
        private Label label19;
        private Label label26;
        private Button _email_Blast_ClearSelectedTemplate;
        internal TextBox _email_Blast_SelectedTemplate;
        private Button _email_Blast_browseForAttachment;
        private Label label11;
        private Label label24;
        private ComboBox comboBox1;
        private Label label25;
        public CheckBox _email_LOI_AppendFooter;
        public CheckBox _email_Blast_AppendFooter;

        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.Container components = null;
		#endregion � Data �
		#region � Properties �
		public MailFaxWizard MailFaxWizardObject
		{
			get { return this.mfwObject; }
			set { this.mfwObject = value; }
		}

		public TabControl Tab_Control
		{
			get {return this.tabControl1;}
		}

		public TabPage TP_FaxDocuments
		{
			get {return this.tpFaxDocuments;}
		}
	
		public TabPage TP_Letters
		{
			get {return this.tpLetters;}
		}

		public TabPage TP_MailingLabels
		{
			get {return this.tpMailingLabels;}
		}
		#endregion � Properties �
		#region � CTR && Dispose �
		public MailFaxWizardForm4(MailFaxWizard mfwObject)
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

			this.mfwObject = mfwObject;	
			//this.rbLarge.Checked = true;
			//this.radioBoxCheckedChanged(this.rbLarge, null);						

			this.readMe1.MouseEnter += new EventHandler(readMe1_MouseEnter);
			this.readMe1.MouseLeave += new EventHandler(readMe1_MouseLeave);

            //Enable_EmailLOI(false);
            //Enable_EmailBlast(false);

            SMTPObject smtpObj = new SMTPObject().LoadSettigns();

            _email_Blast_DisplayName.Text = smtpObj.DisplayName;
            _email_Blast_ReplyTo.Text = smtpObj.ReplyTo;

            _email_LOI_DisplayName.Text = smtpObj.DisplayName;
            _email_LOI_ReplyTo.Text = smtpObj.ReplyTo;

            AddRBEventHandlers();

            _email_LOI_DisplayName.TextChanged += _email_LOI_TextChanged;
            _email_LOI_ReplyTo.TextChanged += _email_LOI_TextChanged;
            _email_LOI_SubjectPrefix.TextChanged += _email_LOI_TextChanged;
            _email_LOI_AppendAddress.CheckedChanged += _email_LOI_CheckedChanged;
            _email_LOI_EmailBody.TextChanged += _email_LOI_TextChanged;

            _email_Blast_DisplayName.TextChanged += _email_Blast_TextChanged;
            _email_Blast_ReplyTo.TextChanged += _email_Blast_TextChanged;
            _email_Blast_SubjectPrefix.TextChanged += _email_Blast_TextChanged;
            _email_Blast_AppendAddress.CheckedChanged += _email_Blast_CheckedChanged; 
            _email_Blast_EmailBody.TextChanged += _email_Blast_TextChanged;

            _email_Blast_AppendFooter_CheckedChanged(this, null);
            _email_LOI_AppendFooter_CheckedChanged(this, null);

        }

        private void _email_Blast_CheckedChanged(object sender, EventArgs e)
        {
            SetEmailBlastDataToObject();
        }

        private void _email_Blast_TextChanged(object sender, EventArgs e)
        {
            SetEmailBlastDataToObject();
        }

        private void _email_LOI_CheckedChanged(object sender, EventArgs e)
        {
            SetEmailLOIDataToObject();
        }

        private void _email_LOI_TextChanged(object sender, EventArgs e)
        {
            SetEmailLOIDataToObject();
        }
        
        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}
		#endregion � CTR && Dispose  �

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MailFaxWizardForm4));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tpMailingLabels = new System.Windows.Forms.TabPage();
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.rbSmall = new System.Windows.Forms.RadioButton();
            this.rbMedium = new System.Windows.Forms.RadioButton();
            this.rbLarge = new System.Windows.Forms.RadioButton();
            this.label5 = new System.Windows.Forms.Label();
            this.tpLetters = new System.Windows.Forms.TabPage();
            this.lvLetters = new System.Windows.Forms.ListView();
            this.documentName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.tpEmailTemplates = new System.Windows.Forms.TabPage();
            this._email_LOI_AppendFooter = new System.Windows.Forms.CheckBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this._email_LOI_DisplayName = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this._email_LOI_ReplyTo = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this._email_LOI_AppendAddress = new System.Windows.Forms.CheckBox();
            this._email_LOI_SubjectPrefix = new System.Windows.Forms.TextBox();
            this._email_LOI_EmailBody = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this._email_LOI_Perc = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.tpEmailBlast = new System.Windows.Forms.TabPage();
            this.label24 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this._email_Blast_ClearSelectedTemplate = new System.Windows.Forms.Button();
            this._email_Blast_SelectedTemplate = new System.Windows.Forms.TextBox();
            this._email_Blast_browseForAttachment = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this._email_Blast_AppendFooter = new System.Windows.Forms.CheckBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label25 = new System.Windows.Forms.Label();
            this._email_Blast_DisplayName = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this._email_Blast_ReplyTo = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this._email_Blast_AppendAddress = new System.Windows.Forms.CheckBox();
            this._email_Blast_SubjectPrefix = new System.Windows.Forms.TextBox();
            this._email_Blast_EmailBody = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.tpFaxDocuments = new System.Windows.Forms.TabPage();
            this.cbPercentage = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.lvOtherFaxDocs = new System.Windows.Forms.ListView();
            this.faxDocumentName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.rbOtherFaxDocuments = new System.Windows.Forms.RadioButton();
            this.Percentage = new System.Windows.Forms.Label();
            this.rbLetterOfIntent = new System.Windows.Forms.RadioButton();
            this.label3 = new System.Windows.Forms.Label();
            this.tbSelectedDocument = new System.Windows.Forms.TextBox();
            this.readMe1 = new DealMaker.ReadMe();
            this.tabControl1.SuspendLayout();
            this.tpMailingLabels.SuspendLayout();
            this.tpLetters.SuspendLayout();
            this.tpEmailTemplates.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.tpEmailBlast.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tpFaxDocuments.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label1.Location = new System.Drawing.Point(3, 3);
            this.label1.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(159, 19);
            this.label1.TabIndex = 0;
            this.label1.Text = "Select Marketing Piece";
            // 
            // label2
            // 
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label2.Location = new System.Drawing.Point(8, 22);
            this.label2.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(322, 19);
            this.label2.TabIndex = 1;
            this.label2.Text = "Select a document below to use it in your Mail / Fax Merge:";
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tpMailingLabels);
            this.tabControl1.Controls.Add(this.tpLetters);
            this.tabControl1.Controls.Add(this.tpEmailTemplates);
            this.tabControl1.Controls.Add(this.tpEmailBlast);
            this.tabControl1.Controls.Add(this.tpFaxDocuments);
            this.tabControl1.Location = new System.Drawing.Point(12, 46);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(1);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(526, 358);
            this.tabControl1.TabIndex = 2;
            this.tabControl1.TabStop = false;
            this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            // 
            // tpMailingLabels
            // 
            this.tpMailingLabels.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.tpMailingLabels.Controls.Add(this.label4);
            this.tpMailingLabels.Controls.Add(this.label6);
            this.tpMailingLabels.Controls.Add(this.rbSmall);
            this.tpMailingLabels.Controls.Add(this.rbMedium);
            this.tpMailingLabels.Controls.Add(this.rbLarge);
            this.tpMailingLabels.Controls.Add(this.label5);
            this.tpMailingLabels.ForeColor = System.Drawing.SystemColors.ControlText;
            this.tpMailingLabels.Location = new System.Drawing.Point(4, 22);
            this.tpMailingLabels.Margin = new System.Windows.Forms.Padding(1);
            this.tpMailingLabels.Name = "tpMailingLabels";
            this.tpMailingLabels.Size = new System.Drawing.Size(518, 332);
            this.tpMailingLabels.TabIndex = 0;
            this.tpMailingLabels.Text = "Mailing Labels";
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label4.Location = new System.Drawing.Point(159, 25);
            this.label4.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(287, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "(1� x 2 5/8 Address Label with barcode, used on postcards)";
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.AutoSize = true;
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label6.Location = new System.Drawing.Point(159, 91);
            this.label6.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(313, 13);
            this.label6.TabIndex = 7;
            this.label6.Text = "(1/2� x 1 �� Label with barcode, used on hand addressed letters)";
            // 
            // rbSmall
            // 
            this.rbSmall.AutoSize = true;
            this.rbSmall.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.rbSmall.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.rbSmall.Location = new System.Drawing.Point(6, 89);
            this.rbSmall.Margin = new System.Windows.Forms.Padding(1);
            this.rbSmall.Name = "rbSmall";
            this.rbSmall.Size = new System.Drawing.Size(150, 18);
            this.rbSmall.TabIndex = 3;
            this.rbSmall.Text = "Small Label with Barcode";
            this.rbSmall.CheckedChanged += new System.EventHandler(this.radioBoxCheckedChanged);
            // 
            // rbMedium
            // 
            this.rbMedium.AutoSize = true;
            this.rbMedium.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.rbMedium.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.rbMedium.Location = new System.Drawing.Point(6, 56);
            this.rbMedium.Margin = new System.Windows.Forms.Padding(1);
            this.rbMedium.Name = "rbMedium";
            this.rbMedium.Size = new System.Drawing.Size(137, 18);
            this.rbMedium.TabIndex = 2;
            this.rbMedium.Text = "Label without Barcode";
            this.rbMedium.CheckedChanged += new System.EventHandler(this.radioBoxCheckedChanged);
            // 
            // rbLarge
            // 
            this.rbLarge.AutoSize = true;
            this.rbLarge.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.rbLarge.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.rbLarge.Location = new System.Drawing.Point(6, 23);
            this.rbLarge.Margin = new System.Windows.Forms.Padding(1);
            this.rbLarge.Name = "rbLarge";
            this.rbLarge.Size = new System.Drawing.Size(122, 18);
            this.rbLarge.TabIndex = 1;
            this.rbLarge.Text = "Label with Barcode";
            this.rbLarge.CheckedChanged += new System.EventHandler(this.radioBoxCheckedChanged);
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label5.Location = new System.Drawing.Point(159, 58);
            this.label5.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(302, 13);
            this.label5.TabIndex = 6;
            this.label5.Text = "(1� x 2 5/8 Address Label without barcode, used on postcards)";
            // 
            // tpLetters
            // 
            this.tpLetters.Controls.Add(this.lvLetters);
            this.tpLetters.Location = new System.Drawing.Point(4, 22);
            this.tpLetters.Margin = new System.Windows.Forms.Padding(1);
            this.tpLetters.Name = "tpLetters";
            this.tpLetters.Size = new System.Drawing.Size(518, 332);
            this.tpLetters.TabIndex = 1;
            this.tpLetters.Text = "Letter Templates";
            // 
            // lvLetters
            // 
            this.lvLetters.Activation = System.Windows.Forms.ItemActivation.OneClick;
            this.lvLetters.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.documentName});
            this.lvLetters.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvLetters.FullRowSelect = true;
            this.lvLetters.GridLines = true;
            this.lvLetters.HideSelection = false;
            this.lvLetters.Location = new System.Drawing.Point(0, 0);
            this.lvLetters.Margin = new System.Windows.Forms.Padding(1);
            this.lvLetters.MultiSelect = false;
            this.lvLetters.Name = "lvLetters";
            this.lvLetters.Size = new System.Drawing.Size(518, 332);
            this.lvLetters.TabIndex = 11;
            this.lvLetters.UseCompatibleStateImageBehavior = false;
            this.lvLetters.View = System.Windows.Forms.View.Details;
            this.lvLetters.SelectedIndexChanged += new System.EventHandler(this.lvLetters_SelectedIndexChanged);
            // 
            // documentName
            // 
            this.documentName.Text = "Document Name";
            this.documentName.Width = 486;
            // 
            // tpEmailTemplates
            // 
            this.tpEmailTemplates.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.tpEmailTemplates.Controls.Add(this._email_LOI_AppendFooter);
            this.tpEmailTemplates.Controls.Add(this.label11);
            this.tpEmailTemplates.Controls.Add(this.label17);
            this.tpEmailTemplates.Controls.Add(this.groupBox2);
            this.tpEmailTemplates.Controls.Add(this.label13);
            this.tpEmailTemplates.Controls.Add(this.label12);
            this.tpEmailTemplates.Controls.Add(this._email_LOI_Perc);
            this.tpEmailTemplates.Controls.Add(this.label9);
            this.tpEmailTemplates.Controls.Add(this.label10);
            this.tpEmailTemplates.Location = new System.Drawing.Point(4, 22);
            this.tpEmailTemplates.Margin = new System.Windows.Forms.Padding(1);
            this.tpEmailTemplates.Name = "tpEmailTemplates";
            this.tpEmailTemplates.Size = new System.Drawing.Size(518, 332);
            this.tpEmailTemplates.TabIndex = 3;
            this.tpEmailTemplates.Text = "Email - LOI";
            // 
            // _email_LOI_AppendFooter
            // 
            this._email_LOI_AppendFooter.AutoSize = true;
            this._email_LOI_AppendFooter.Checked = true;
            this._email_LOI_AppendFooter.CheckState = System.Windows.Forms.CheckState.Checked;
            this._email_LOI_AppendFooter.ForeColor = System.Drawing.Color.Black;
            this._email_LOI_AppendFooter.Location = new System.Drawing.Point(112, 275);
            this._email_LOI_AppendFooter.Margin = new System.Windows.Forms.Padding(1);
            this._email_LOI_AppendFooter.Name = "_email_LOI_AppendFooter";
            this._email_LOI_AppendFooter.Size = new System.Drawing.Size(159, 17);
            this._email_LOI_AppendFooter.TabIndex = 84;
            this._email_LOI_AppendFooter.Text = "Append Email LOI Signature";
            this._email_LOI_AppendFooter.UseVisualStyleBackColor = true;
            this._email_LOI_AppendFooter.CheckedChanged += new System.EventHandler(this._email_LOI_AppendFooter_CheckedChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label11.Location = new System.Drawing.Point(6, 44);
            this.label11.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(197, 13);
            this.label11.TabIndex = 63;
            this.label11.Text = "Select % for your \"Letter of Intent\" Offer:";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label17.Location = new System.Drawing.Point(6, 292);
            this.label17.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(294, 13);
            this.label17.TabIndex = 55;
            this.label17.Text = "* Personalized LOI PDF document is attached to every email.";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this._email_LOI_DisplayName);
            this.groupBox2.Controls.Add(this.label18);
            this.groupBox2.Controls.Add(this._email_LOI_ReplyTo);
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Controls.Add(this._email_LOI_AppendAddress);
            this.groupBox2.Controls.Add(this._email_LOI_SubjectPrefix);
            this.groupBox2.Controls.Add(this._email_LOI_EmailBody);
            this.groupBox2.Controls.Add(this.label15);
            this.groupBox2.Controls.Add(this.label16);
            this.groupBox2.Location = new System.Drawing.Point(8, 88);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(1);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(1);
            this.groupBox2.Size = new System.Drawing.Size(495, 191);
            this.groupBox2.TabIndex = 54;
            this.groupBox2.TabStop = false;
            // 
            // _email_LOI_DisplayName
            // 
            this._email_LOI_DisplayName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._email_LOI_DisplayName.Enabled = false;
            this._email_LOI_DisplayName.Location = new System.Drawing.Point(104, 14);
            this._email_LOI_DisplayName.Margin = new System.Windows.Forms.Padding(1);
            this._email_LOI_DisplayName.Name = "_email_LOI_DisplayName";
            this._email_LOI_DisplayName.Size = new System.Drawing.Size(235, 20);
            this._email_LOI_DisplayName.TabIndex = 58;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label18.Location = new System.Drawing.Point(14, 14);
            this.label18.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(75, 13);
            this.label18.TabIndex = 10;
            this.label18.Text = "Display Name:";
            // 
            // _email_LOI_ReplyTo
            // 
            this._email_LOI_ReplyTo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._email_LOI_ReplyTo.Enabled = false;
            this._email_LOI_ReplyTo.Location = new System.Drawing.Point(104, 36);
            this._email_LOI_ReplyTo.Margin = new System.Windows.Forms.Padding(1);
            this._email_LOI_ReplyTo.Name = "_email_LOI_ReplyTo";
            this._email_LOI_ReplyTo.Size = new System.Drawing.Size(235, 20);
            this._email_LOI_ReplyTo.TabIndex = 59;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label14.Location = new System.Drawing.Point(14, 36);
            this.label14.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(53, 13);
            this.label14.TabIndex = 8;
            this.label14.Text = "Reply To:";
            // 
            // _email_LOI_AppendAddress
            // 
            this._email_LOI_AppendAddress.AutoSize = true;
            this._email_LOI_AppendAddress.Checked = true;
            this._email_LOI_AppendAddress.CheckState = System.Windows.Forms.CheckState.Checked;
            this._email_LOI_AppendAddress.Location = new System.Drawing.Point(104, 81);
            this._email_LOI_AppendAddress.Margin = new System.Windows.Forms.Padding(1);
            this._email_LOI_AppendAddress.Name = "_email_LOI_AppendAddress";
            this._email_LOI_AppendAddress.Size = new System.Drawing.Size(398, 17);
            this._email_LOI_AppendAddress.TabIndex = 61;
            this._email_LOI_AppendAddress.Text = "Append Site Street Address to the end of Email Subject line (in the above field)." +
    "";
            this._email_LOI_AppendAddress.UseVisualStyleBackColor = true;
            // 
            // _email_LOI_SubjectPrefix
            // 
            this._email_LOI_SubjectPrefix.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._email_LOI_SubjectPrefix.Location = new System.Drawing.Point(104, 58);
            this._email_LOI_SubjectPrefix.Margin = new System.Windows.Forms.Padding(1);
            this._email_LOI_SubjectPrefix.Name = "_email_LOI_SubjectPrefix";
            this._email_LOI_SubjectPrefix.Size = new System.Drawing.Size(384, 20);
            this._email_LOI_SubjectPrefix.TabIndex = 60;
            this._email_LOI_SubjectPrefix.Text = "Offer On ";
            // 
            // _email_LOI_EmailBody
            // 
            this._email_LOI_EmailBody.AcceptsReturn = true;
            this._email_LOI_EmailBody.AcceptsTab = true;
            this._email_LOI_EmailBody.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._email_LOI_EmailBody.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._email_LOI_EmailBody.Location = new System.Drawing.Point(104, 105);
            this._email_LOI_EmailBody.Margin = new System.Windows.Forms.Padding(1);
            this._email_LOI_EmailBody.Multiline = true;
            this._email_LOI_EmailBody.Name = "_email_LOI_EmailBody";
            this._email_LOI_EmailBody.Size = new System.Drawing.Size(384, 80);
            this._email_LOI_EmailBody.TabIndex = 62;
            this._email_LOI_EmailBody.Text = "Please find the attached offer.\r\nThank you and have a wonderful day.";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label15.Location = new System.Drawing.Point(14, 105);
            this.label15.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(62, 13);
            this.label15.TabIndex = 4;
            this.label15.Text = "Email Body:";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label16.Location = new System.Drawing.Point(14, 59);
            this.label16.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(75, 13);
            this.label16.TabIndex = 3;
            this.label16.Text = "Subject Prefix:";
            // 
            // label13
            // 
            this.label13.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label13.AutoSize = true;
            this.label13.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label13.Location = new System.Drawing.Point(43, 63);
            this.label13.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(184, 13);
            this.label13.TabIndex = 15;
            this.label13.Text = "(% is calculated from the Listing Price)";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label12.Location = new System.Drawing.Point(6, 13);
            this.label12.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(265, 13);
            this.label12.TabIndex = 14;
            this.label12.Text = "Blind Offer System - Mass Emails to AGENTS:";
            // 
            // _email_LOI_Perc
            // 
            this._email_LOI_Perc.Items.AddRange(new object[] {
            "36%",
            "42.5%",
            "44.5%",
            "46.5%",
            "48.5%",
            "50.5%",
            "52.5%",
            "54.5%",
            "56.5%",
            "58.5%",
            "60.5%",
            "62.5%",
            "64.5%",
            "66.5%",
            "68.5%",
            "70.5%",
            "72.5%",
            "74.5%",
            "76.5%",
            "78.5%",
            "80.5%",
            "82.5%"});
            this._email_LOI_Perc.Location = new System.Drawing.Point(213, 41);
            this._email_LOI_Perc.Margin = new System.Windows.Forms.Padding(1);
            this._email_LOI_Perc.Name = "_email_LOI_Perc";
            this._email_LOI_Perc.Size = new System.Drawing.Size(58, 21);
            this._email_LOI_Perc.TabIndex = 57;
            this._email_LOI_Perc.SelectedIndexChanged += new System.EventHandler(this._email_LOI_Perc_selectedIndexChanged);
            // 
            // label9
            // 
            this.label9.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label9.AutoSize = true;
            this.label9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label9.Location = new System.Drawing.Point(301, 63);
            this.label9.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(147, 13);
            this.label9.TabIndex = 12;
            this.label9.Text = "i.e. 325,561.33 to 325.560.00";
            // 
            // label10
            // 
            this.label10.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label10.AutoSize = true;
            this.label10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label10.Location = new System.Drawing.Point(294, 44);
            this.label10.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(220, 13);
            this.label10.TabIndex = 11;
            this.label10.Text = "The calculated offer price will be rounded up.";
            // 
            // tpEmailBlast
            // 
            this.tpEmailBlast.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.tpEmailBlast.Controls.Add(this.label24);
            this.tpEmailBlast.Controls.Add(this.label26);
            this.tpEmailBlast.Controls.Add(this._email_Blast_ClearSelectedTemplate);
            this.tpEmailBlast.Controls.Add(this._email_Blast_SelectedTemplate);
            this.tpEmailBlast.Controls.Add(this._email_Blast_browseForAttachment);
            this.tpEmailBlast.Controls.Add(this.groupBox1);
            this.tpEmailBlast.Controls.Add(this.label19);
            this.tpEmailBlast.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.tpEmailBlast.Location = new System.Drawing.Point(4, 22);
            this.tpEmailBlast.Name = "tpEmailBlast";
            this.tpEmailBlast.Size = new System.Drawing.Size(518, 332);
            this.tpEmailBlast.TabIndex = 4;
            this.tpEmailBlast.Text = "Email Blast";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(4, 256);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(385, 13);
            this.label24.TabIndex = 76;
            this.label24.Text = "Select the attachment or leave empty if you don\'t want to send any attachments:";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(4, 284);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(109, 13);
            this.label26.TabIndex = 61;
            this.label26.Text = "Selected Attachment:";
            // 
            // _email_Blast_ClearSelectedTemplate
            // 
            this._email_Blast_ClearSelectedTemplate.AutoSize = true;
            this._email_Blast_ClearSelectedTemplate.Location = new System.Drawing.Point(427, 279);
            this._email_Blast_ClearSelectedTemplate.Name = "_email_Blast_ClearSelectedTemplate";
            this._email_Blast_ClearSelectedTemplate.Size = new System.Drawing.Size(73, 23);
            this._email_Blast_ClearSelectedTemplate.TabIndex = 75;
            this._email_Blast_ClearSelectedTemplate.Text = "Clear";
            this._email_Blast_ClearSelectedTemplate.UseVisualStyleBackColor = true;
            this._email_Blast_ClearSelectedTemplate.Click += new System.EventHandler(this._email_Blast_ClearSelectedTemplate_Click);
            // 
            // _email_Blast_SelectedTemplate
            // 
            this._email_Blast_SelectedTemplate.Location = new System.Drawing.Point(116, 281);
            this._email_Blast_SelectedTemplate.Margin = new System.Windows.Forms.Padding(1);
            this._email_Blast_SelectedTemplate.Name = "_email_Blast_SelectedTemplate";
            this._email_Blast_SelectedTemplate.ReadOnly = true;
            this._email_Blast_SelectedTemplate.Size = new System.Drawing.Size(235, 20);
            this._email_Blast_SelectedTemplate.TabIndex = 74;
            this._email_Blast_SelectedTemplate.TabStop = false;
            // 
            // _email_Blast_browseForAttachment
            // 
            this._email_Blast_browseForAttachment.AutoSize = true;
            this._email_Blast_browseForAttachment.Location = new System.Drawing.Point(427, 251);
            this._email_Blast_browseForAttachment.Name = "_email_Blast_browseForAttachment";
            this._email_Blast_browseForAttachment.Size = new System.Drawing.Size(31, 23);
            this._email_Blast_browseForAttachment.TabIndex = 73;
            this._email_Blast_browseForAttachment.Text = "...";
            this._email_Blast_browseForAttachment.UseVisualStyleBackColor = true;
            this._email_Blast_browseForAttachment.Click += new System.EventHandler(this._email_Blast_browseForAttachment_OnClick);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this._email_Blast_AppendFooter);
            this.groupBox1.Controls.Add(this.comboBox1);
            this.groupBox1.Controls.Add(this.label25);
            this.groupBox1.Controls.Add(this._email_Blast_DisplayName);
            this.groupBox1.Controls.Add(this.label20);
            this.groupBox1.Controls.Add(this._email_Blast_ReplyTo);
            this.groupBox1.Controls.Add(this.label21);
            this.groupBox1.Controls.Add(this._email_Blast_AppendAddress);
            this.groupBox1.Controls.Add(this._email_Blast_SubjectPrefix);
            this.groupBox1.Controls.Add(this._email_Blast_EmailBody);
            this.groupBox1.Controls.Add(this.label22);
            this.groupBox1.Controls.Add(this.label23);
            this.groupBox1.Location = new System.Drawing.Point(7, 30);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(1);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(1);
            this.groupBox1.Size = new System.Drawing.Size(495, 222);
            this.groupBox1.TabIndex = 55;
            this.groupBox1.TabStop = false;
            // 
            // _email_Blast_AppendFooter
            // 
            this._email_Blast_AppendFooter.AutoSize = true;
            this._email_Blast_AppendFooter.Checked = true;
            this._email_Blast_AppendFooter.CheckState = System.Windows.Forms.CheckState.Checked;
            this._email_Blast_AppendFooter.ForeColor = System.Drawing.Color.Black;
            this._email_Blast_AppendFooter.Location = new System.Drawing.Point(17, 202);
            this._email_Blast_AppendFooter.Margin = new System.Windows.Forms.Padding(1);
            this._email_Blast_AppendFooter.Name = "_email_Blast_AppendFooter";
            this._email_Blast_AppendFooter.Size = new System.Drawing.Size(165, 17);
            this._email_Blast_AppendFooter.TabIndex = 83;
            this._email_Blast_AppendFooter.Text = "Append Email Blast Signature";
            this._email_Blast_AppendFooter.UseVisualStyleBackColor = true;
            this._email_Blast_AppendFooter.CheckedChanged += new System.EventHandler(this._email_Blast_AppendFooter_CheckedChanged);
            // 
            // comboBox1
            // 
            this.comboBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "Templ. #1",
            "Templ. #2",
            "Templ. #3"});
            this.comboBox1.Location = new System.Drawing.Point(410, 100);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(77, 21);
            this.comboBox1.TabIndex = 82;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label25.Location = new System.Drawing.Point(14, 103);
            this.label25.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(385, 13);
            this.label25.TabIndex = 81;
            this.label25.Text = "Select the template for Email Body or leave it blank to use the Email Body below:" +
    "";
            // 
            // _email_Blast_DisplayName
            // 
            this._email_Blast_DisplayName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._email_Blast_DisplayName.Enabled = false;
            this._email_Blast_DisplayName.Location = new System.Drawing.Point(104, 12);
            this._email_Blast_DisplayName.Margin = new System.Windows.Forms.Padding(1);
            this._email_Blast_DisplayName.Name = "_email_Blast_DisplayName";
            this._email_Blast_DisplayName.Size = new System.Drawing.Size(235, 20);
            this._email_Blast_DisplayName.TabIndex = 76;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label20.Location = new System.Drawing.Point(14, 12);
            this.label20.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(75, 13);
            this.label20.TabIndex = 10;
            this.label20.Text = "Display Name:";
            // 
            // _email_Blast_ReplyTo
            // 
            this._email_Blast_ReplyTo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._email_Blast_ReplyTo.Enabled = false;
            this._email_Blast_ReplyTo.Location = new System.Drawing.Point(104, 34);
            this._email_Blast_ReplyTo.Margin = new System.Windows.Forms.Padding(1);
            this._email_Blast_ReplyTo.Name = "_email_Blast_ReplyTo";
            this._email_Blast_ReplyTo.Size = new System.Drawing.Size(235, 20);
            this._email_Blast_ReplyTo.TabIndex = 77;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label21.Location = new System.Drawing.Point(14, 34);
            this.label21.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(53, 13);
            this.label21.TabIndex = 8;
            this.label21.Text = "Reply To:";
            // 
            // _email_Blast_AppendAddress
            // 
            this._email_Blast_AppendAddress.AutoSize = true;
            this._email_Blast_AppendAddress.Checked = true;
            this._email_Blast_AppendAddress.CheckState = System.Windows.Forms.CheckState.Checked;
            this._email_Blast_AppendAddress.ForeColor = System.Drawing.Color.Black;
            this._email_Blast_AppendAddress.Location = new System.Drawing.Point(104, 79);
            this._email_Blast_AppendAddress.Margin = new System.Windows.Forms.Padding(1);
            this._email_Blast_AppendAddress.Name = "_email_Blast_AppendAddress";
            this._email_Blast_AppendAddress.Size = new System.Drawing.Size(398, 17);
            this._email_Blast_AppendAddress.TabIndex = 79;
            this._email_Blast_AppendAddress.Text = "Append Site Street Address to the end of Email Subject line (in the above field)." +
    "";
            this._email_Blast_AppendAddress.UseVisualStyleBackColor = true;
            // 
            // _email_Blast_SubjectPrefix
            // 
            this._email_Blast_SubjectPrefix.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._email_Blast_SubjectPrefix.Location = new System.Drawing.Point(104, 56);
            this._email_Blast_SubjectPrefix.Margin = new System.Windows.Forms.Padding(1);
            this._email_Blast_SubjectPrefix.Name = "_email_Blast_SubjectPrefix";
            this._email_Blast_SubjectPrefix.Size = new System.Drawing.Size(384, 20);
            this._email_Blast_SubjectPrefix.TabIndex = 78;
            this._email_Blast_SubjectPrefix.Text = "Your Home on";
            // 
            // _email_Blast_EmailBody
            // 
            this._email_Blast_EmailBody.AcceptsReturn = true;
            this._email_Blast_EmailBody.AcceptsTab = true;
            this._email_Blast_EmailBody.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._email_Blast_EmailBody.Location = new System.Drawing.Point(17, 134);
            this._email_Blast_EmailBody.Margin = new System.Windows.Forms.Padding(1);
            this._email_Blast_EmailBody.Multiline = true;
            this._email_Blast_EmailBody.Name = "_email_Blast_EmailBody";
            this._email_Blast_EmailBody.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._email_Blast_EmailBody.Size = new System.Drawing.Size(471, 66);
            this._email_Blast_EmailBody.TabIndex = 80;
            this._email_Blast_EmailBody.Text = "Please see the attached documentation.";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label22.Location = new System.Drawing.Point(14, 120);
            this.label22.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(107, 13);
            this.label22.TabIndex = 4;
            this.label22.Text = "Selected Email Body:";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label23.Location = new System.Drawing.Point(14, 57);
            this.label23.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(75, 13);
            this.label23.TabIndex = 3;
            this.label23.Text = "Subject Prefix:";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label19.Location = new System.Drawing.Point(6, 13);
            this.label19.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(183, 13);
            this.label19.TabIndex = 15;
            this.label19.Text = "Email Blast to HOMEOWNERS:";
            // 
            // tpFaxDocuments
            // 
            this.tpFaxDocuments.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.tpFaxDocuments.Controls.Add(this.cbPercentage);
            this.tpFaxDocuments.Controls.Add(this.label8);
            this.tpFaxDocuments.Controls.Add(this.label7);
            this.tpFaxDocuments.Controls.Add(this.lvOtherFaxDocs);
            this.tpFaxDocuments.Controls.Add(this.rbOtherFaxDocuments);
            this.tpFaxDocuments.Controls.Add(this.Percentage);
            this.tpFaxDocuments.Controls.Add(this.rbLetterOfIntent);
            this.tpFaxDocuments.Location = new System.Drawing.Point(4, 22);
            this.tpFaxDocuments.Margin = new System.Windows.Forms.Padding(1);
            this.tpFaxDocuments.Name = "tpFaxDocuments";
            this.tpFaxDocuments.Size = new System.Drawing.Size(518, 332);
            this.tpFaxDocuments.TabIndex = 2;
            this.tpFaxDocuments.Text = "Fax - LOI";
            // 
            // cbPercentage
            // 
            this.cbPercentage.Enabled = false;
            this.cbPercentage.Items.AddRange(new object[] {
            "36%",
            "42.5%",
            "44.5%",
            "46.5%",
            "48.5%",
            "50.5%",
            "52.5%",
            "54.5%",
            "56.5%",
            "58.5%",
            "60.5%",
            "62.5%",
            "64.5%",
            "66.5%",
            "68.5%",
            "70.5%",
            "72.5%",
            "74.5%",
            "76.5%",
            "78.5%",
            "80.5%",
            "82.5%"});
            this.cbPercentage.Location = new System.Drawing.Point(189, 21);
            this.cbPercentage.Margin = new System.Windows.Forms.Padding(1);
            this.cbPercentage.Name = "cbPercentage";
            this.cbPercentage.Size = new System.Drawing.Size(56, 21);
            this.cbPercentage.TabIndex = 22;
            this.cbPercentage.SelectedIndexChanged += new System.EventHandler(this.cbPercentage_SelectedIndexChanged);
            // 
            // label8
            // 
            this.label8.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label8.AutoSize = true;
            this.label8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label8.Location = new System.Drawing.Point(256, 43);
            this.label8.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(147, 13);
            this.label8.TabIndex = 6;
            this.label8.Text = "i.e. 325,561.33 to 325.560.00";
            // 
            // label7
            // 
            this.label7.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label7.AutoSize = true;
            this.label7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label7.Location = new System.Drawing.Point(256, 25);
            this.label7.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(220, 13);
            this.label7.TabIndex = 5;
            this.label7.Text = "The calculated offer price will be rounded up.";
            // 
            // lvOtherFaxDocs
            // 
            this.lvOtherFaxDocs.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lvOtherFaxDocs.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.faxDocumentName});
            this.lvOtherFaxDocs.Enabled = false;
            this.lvOtherFaxDocs.FullRowSelect = true;
            this.lvOtherFaxDocs.GridLines = true;
            this.lvOtherFaxDocs.HideSelection = false;
            this.lvOtherFaxDocs.HoverSelection = true;
            this.lvOtherFaxDocs.Location = new System.Drawing.Point(6, 79);
            this.lvOtherFaxDocs.Margin = new System.Windows.Forms.Padding(1);
            this.lvOtherFaxDocs.Name = "lvOtherFaxDocs";
            this.lvOtherFaxDocs.Size = new System.Drawing.Size(495, 226);
            this.lvOtherFaxDocs.TabIndex = 24;
            this.lvOtherFaxDocs.UseCompatibleStateImageBehavior = false;
            this.lvOtherFaxDocs.View = System.Windows.Forms.View.Details;
            this.lvOtherFaxDocs.SelectedIndexChanged += new System.EventHandler(this.lvOtherFaxDocs_SelectedIndexChanged);
            // 
            // faxDocumentName
            // 
            this.faxDocumentName.Text = "Document Name";
            this.faxDocumentName.Width = 435;
            // 
            // rbOtherFaxDocuments
            // 
            this.rbOtherFaxDocuments.AutoSize = true;
            this.rbOtherFaxDocuments.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.rbOtherFaxDocuments.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.rbOtherFaxDocuments.Location = new System.Drawing.Point(6, 47);
            this.rbOtherFaxDocuments.Margin = new System.Windows.Forms.Padding(1);
            this.rbOtherFaxDocuments.Name = "rbOtherFaxDocuments";
            this.rbOtherFaxDocuments.Size = new System.Drawing.Size(129, 18);
            this.rbOtherFaxDocuments.TabIndex = 23;
            this.rbOtherFaxDocuments.Text = "Other fax documents";
            // 
            // Percentage
            // 
            this.Percentage.AutoSize = true;
            this.Percentage.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.Percentage.Location = new System.Drawing.Point(120, 24);
            this.Percentage.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.Percentage.Name = "Percentage";
            this.Percentage.Size = new System.Drawing.Size(65, 13);
            this.Percentage.TabIndex = 2;
            this.Percentage.Text = "Percentage:";
            this.Percentage.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // rbLetterOfIntent
            // 
            this.rbLetterOfIntent.AutoSize = true;
            this.rbLetterOfIntent.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.rbLetterOfIntent.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.rbLetterOfIntent.Location = new System.Drawing.Point(6, 23);
            this.rbLetterOfIntent.Margin = new System.Windows.Forms.Padding(1);
            this.rbLetterOfIntent.Name = "rbLetterOfIntent";
            this.rbLetterOfIntent.Size = new System.Drawing.Size(102, 18);
            this.rbLetterOfIntent.TabIndex = 21;
            this.rbLetterOfIntent.Text = "Letter Of Intent";
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label3.Location = new System.Drawing.Point(13, 408);
            this.label3.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(104, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Selected Document:";
            // 
            // tbSelectedDocument
            // 
            this.tbSelectedDocument.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbSelectedDocument.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.tbSelectedDocument.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbSelectedDocument.Cursor = System.Windows.Forms.Cursors.Default;
            this.tbSelectedDocument.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbSelectedDocument.ForeColor = System.Drawing.Color.Red;
            this.tbSelectedDocument.Location = new System.Drawing.Point(121, 405);
            this.tbSelectedDocument.Margin = new System.Windows.Forms.Padding(1);
            this.tbSelectedDocument.Name = "tbSelectedDocument";
            this.tbSelectedDocument.ReadOnly = true;
            this.tbSelectedDocument.Size = new System.Drawing.Size(416, 22);
            this.tbSelectedDocument.TabIndex = 4;
            this.tbSelectedDocument.TabStop = false;
            // 
            // readMe1
            // 
            this.readMe1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.readMe1.AutoSize = true;
            this.readMe1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.readMe1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("readMe1.BackgroundImage")));
            this.readMe1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.readMe1.Location = new System.Drawing.Point(516, 3);
            this.readMe1.Margin = new System.Windows.Forms.Padding(1);
            this.readMe1.Name = "readMe1";
            this.readMe1.Size = new System.Drawing.Size(19, 19);
            this.readMe1.TabIndex = 13;
            // 
            // MailFaxWizardForm4
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.Controls.Add(this.readMe1);
            this.Controls.Add(this.tbSelectedDocument);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Margin = new System.Windows.Forms.Padding(1);
            this.Name = "MailFaxWizardForm4";
            this.Size = new System.Drawing.Size(538, 432);
            this.Load += new System.EventHandler(this.MailFaxWizardForm4_Load);
            this.tabControl1.ResumeLayout(false);
            this.tpMailingLabels.ResumeLayout(false);
            this.tpMailingLabels.PerformLayout();
            this.tpLetters.ResumeLayout(false);
            this.tpEmailTemplates.ResumeLayout(false);
            this.tpEmailTemplates.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.tpEmailBlast.ResumeLayout(false);
            this.tpEmailBlast.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tpFaxDocuments.ResumeLayout(false);
            this.tpFaxDocuments.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

		}
        #endregion

        public void AddRBEventHandlers()
        {
            this.rbOtherFaxDocuments.CheckedChanged += new System.EventHandler(this.rbFaxDocumentCheckedChanged);
            this.rbLetterOfIntent.CheckedChanged += new System.EventHandler(this.rbFaxDocumentCheckedChanged);
        }

        public void RemoveRBEventHandlers()
        {
            this.rbOtherFaxDocuments.CheckedChanged -= new System.EventHandler(this.rbFaxDocumentCheckedChanged);
            this.rbLetterOfIntent.CheckedChanged -= new System.EventHandler(this.rbFaxDocumentCheckedChanged);
        }

        public void RemoveTabCtrlEHs()
        {
            this.tabControl1.SelectedIndexChanged -= new System.EventHandler(this.tabControl1_SelectedIndexChanged);
        }

        public void AddTabCtrlEHs()
        {
            this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
        }

        #region � Methods �

        private void ClearLabelRadioBoxes()
		{			
			//RQ_0012++
			//if (this.tabControl1.SelectedIndex != 0) //CLEAR ONLY IF NOT ACTIVE
			////RQ_0012--
			//{
			//	this.rbLarge.Checked	= false;
			//	this.rbMedium.Checked	= false;
			//	this.rbSmall.Checked	= false;
			//}
		}
		#region � rbFaxDocumentCheckedChanged �
		private void rbFaxDocumentCheckedChanged(object sender, System.EventArgs e)
		{
            if (((RadioButton)sender).Name == "rbLetterOfIntent" && ((RadioButton)sender).Checked)
            {
                this.cbPercentage.Enabled = this.rbLetterOfIntent.Checked;
                this.lvOtherFaxDocs.Enabled = !this.rbLetterOfIntent.Checked;
            }

            if (((RadioButton)sender).Name == "rbOtherFaxDocuments" && ((RadioButton)sender).Checked)
            {
                this.cbPercentage.Enabled = this.rbLetterOfIntent.Checked;
                this.lvOtherFaxDocs.Enabled = !this.rbLetterOfIntent.Checked;
            }            
        }
		#endregion � rbFaxDocumentCheckedChanged �
		
        private void ClearMailingLabelsRadioBoxes()
        {
            this.rbLarge.Checked = false;
            this.rbMedium.Checked = false;
            this.rbSmall.Checked = false;
        }
		private void ClearFaxDocRadioBoxes()
		{
			//RQ_0012++
			if (this.tabControl1.SelectedIndex != 4)	//ONLY IF NOT ACTIVE
			//RQ_0012--
			{
				this.rbLetterOfIntent.Checked = false;
				this.rbOtherFaxDocuments.Checked = false;
			}
		}
		#region � MailFaxWizardForm4_Load �
		private void MailFaxWizardForm4_Load(object sender, System.EventArgs e)
		{
			this.ScanDirectories();
		}
		#endregion � MailFaxWizardForm4_Load �
		#region � ScanDirectories �
		private void ScanDirectories()
		{
			try
			{
				DirectoryInfo di = new DirectoryInfo(Application.StartupPath + Globals.MailPath);
			
				//TA++ added search pattern "*.tx"
				//2006, April 15
				FileInfo[] fi = di.GetFiles("*.tx");						
				//TA-- added search pattern "*.tx"
				foreach (FileInfo fiTemp in fi)
				{
					string templateName = fiTemp.Name;
					int extLen = fiTemp.Extension.Length;
					templateName = templateName.Substring(0, templateName.Length - extLen);
					this.lvLetters.Items.Add(templateName);
				}

				di = new DirectoryInfo(Application.StartupPath + Globals.FaxPath);
				//TA++ added search pattern "*.tx"
				//2006, April 15
				fi = di.GetFiles("*.tx");		
				//TA-- added search pattern "*.tx"
				foreach (FileInfo fiTemp in fi)
				{
					string templateName = fiTemp.Name;
					int extLen = fiTemp.Extension.Length;
					templateName = templateName.Substring(0, templateName.Length - extLen);
					if (templateName != Globals.LOI)
					{
						this.lvOtherFaxDocs.Items.Add(templateName);
					}
				}

                di = new DirectoryInfo(Application.StartupPath + Globals.EmailPath);
                //TA++ added search pattern "*.tx"
                //2006, April 15
                fi = di.GetFiles("*.tx");
                //TA-- added search pattern "*.tx"
                foreach (FileInfo fiTemp in fi)
                {
                    string templateName = fiTemp.Name;
                    int extLen = fiTemp.Extension.Length;
                    templateName = templateName.Substring(0, templateName.Length - extLen);
                    if (templateName != Globals.LOI)
                    {
                        //this.lvOtherEmailDocs.Items.Add(templateName);
                    }
                }
            }
			catch (Exception)
			{
				//do nothing
			}
		}
		#endregion � ScanDirectories �
		#region � lvOtherFaxDocs_SelectedIndexChanged �
		private void lvOtherFaxDocs_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (null != this.lvOtherFaxDocs.SelectedItems && 0 != this.lvOtherFaxDocs.SelectedItems.Count)
			{
				this.tbSelectedDocument.Text = this.lvOtherFaxDocs.SelectedItems[0].Text;
				this.mfwObject.LabelDocSel = eLabelDocumentSelection.faxDocument;
				this.mfwObject.DocumentName = this.lvOtherFaxDocs.SelectedItems[0].Text;					
			}
		}
		#endregion � lvOtherFaxDocs_SelectedIndexChanged �
		#region � cbPercentage_SelectedIndexChanged �
		private void cbPercentage_SelectedIndexChanged(object sender, System.EventArgs e)
		{			
			if (null == this.cbPercentage.SelectedItem)
				return;

			this.tbSelectedDocument.Text = "Letter Of Intent at " + this.cbPercentage.SelectedItem.ToString() + ".";
			this.mfwObject.LabelDocSel = eLabelDocumentSelection.faxDocument;
			this.mfwObject.DocumentName = Globals.LOI;
			
			string selectedPercentage = this.cbPercentage.SelectedItem.ToString();
			selectedPercentage = selectedPercentage.Replace("%", string.Empty);
			//selectedPercentage = selectedPercentage.Replace(".", ",");
			selectedPercentage = selectedPercentage.Trim();
			
			double perc = 0;
			try
			{
				System.IFormatProvider format =	new System.Globalization.CultureInfo("en-US", true);
				perc = Convert.ToDouble(selectedPercentage, format);
			}
			catch (Exception)
			{
				perc = 0;
			}
			this.mfwObject.Percentage = perc;
		}
		#endregion � cbPercentage_SelectedIndexChanged �
		#region � lvLetters_SelectedIndexChanged �
		private void lvLetters_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (null != this.lvLetters.SelectedItems && 0 != this.lvLetters.SelectedItems.Count)
			{
				this.tbSelectedDocument.Text = this.lvLetters.SelectedItems[0].Text;
				this.mfwObject.LabelDocSel = eLabelDocumentSelection.mailDocument;
				this.mfwObject.DocumentName = this.lvLetters.SelectedItems[0].Text;                
            }
		}
		#endregion � lvLetters_SelectedIndexChanged �

		private void radioBoxCheckedChanged(object sender, System.EventArgs e)
		{
			RadioButton rb = (RadioButton)sender;
			//RQ_0012++
			if (false == rb.Checked)
			{
				return;
			}
			//RQ_0012--
			
			TabPage tp = (TabPage)rb.Parent;
			
			
			this.tbSelectedDocument.Text = tp.Text + ": " + rb.Text;
			
			switch (rb.Text)
			{
				case "Label with Barcode":
					this.mfwObject.LabelDocSel = eLabelDocumentSelection.labelLarge;
					this.mfwObject.DocumentName = Globals.LargeLabel;
					break;
				case "Label without Barcode":
					this.mfwObject.LabelDocSel = eLabelDocumentSelection.labelMedium;
					this.mfwObject.DocumentName = Globals.MediumLabel;
					break;
				case "Small Label with Barcode":
					this.mfwObject.LabelDocSel = eLabelDocumentSelection.labelSmall;
					this.mfwObject.DocumentName = Globals.SmallLabel;
					break;
			}									
        }
		#endregion � Methods �

		private void readMe1_MouseEnter(object sender, EventArgs e)
		{
			this.floater = new DealMaker.Forms.Floaters.MFWSelectMarketingPiece();


			Point pReadMe = this.readMe1.Location;
			Size fSize = this.floater.Size;
									
			this.floater.Location = PointToScreen(new Point (pReadMe.X - fSize.Width, pReadMe.Y) );

			this.floater.Show();
		}

		private void readMe1_MouseLeave(object sender, EventArgs e)
		{
			this.floater.Hide();		
		}

        private void Check_EmailLOI(bool state)
        {
            
            //Enable_EmailLOI(state);
        }
        
        private void _email_Blast_checkedChanged(object sender, EventArgs e)
        {
            RadioButton rb = (RadioButton)sender;
            if (rb.Checked)
            {                
                this.tbSelectedDocument.Text = $"Email Blast - Select Document!";                
            }
        }

        private void _email_LOI_checkedChanged(object sender, EventArgs e)
        {
            RadioButton rb = (RadioButton)sender;
            if (rb.Checked)
            {                
                this.mfwObject.LabelDocSel = eLabelDocumentSelection.emailLOI;
                this.mfwObject.LoiSelected = true;

                this.mfwObject.DocumentName = Application.StartupPath + Globals.EmailPath + "LOI-ForEmail.tx";
                double perc = GetPercentage(this._email_LOI_Perc.SelectedItem.ToString());
                this.mfwObject.Percentage = perc;

                this.tbSelectedDocument.Text = $"Email LOI, percentage at {perc}%.";                
            }
        }

        

        private void _email_Blast_browseForAttachment_OnClick(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.InitialDirectory = Application.StartupPath + Globals.EmailPath;
            
            ofd.Filter = "Tx Template Files (*.tx)|*.tx|PDF files (*.pdf)|*.pdf|MS Word files (*.docx)|*.docx|MS Word files (*.doc)|*.doc|MS Excel files (*.xls)|*.xls|MS Excel files (*.xlsx)|*.xlsx|JPG files (*.jpg)|*.jpg|PNG files (*.png)|*.png";
            ofd.FilterIndex = 1;
            ofd.RestoreDirectory = true;

            if (ofd.ShowDialog() == DialogResult.OK)
            {
                _email_Blast_SelectedTemplate.Text = Path.GetFileName(ofd.FileName);

                this.tbSelectedDocument.Text = $"Email Blast:{_email_Blast_SelectedTemplate.Text}";
                                    
                this.mfwObject.LabelDocSel = eLabelDocumentSelection.emailBlast;
                this.mfwObject.DocumentName = ofd.FileName;                
            }            
        }

        private void _email_Blast_ClearSelectedTemplate_Click(object sender, EventArgs e)
        {
            _email_Blast_SelectedTemplate.Text = string.Empty;
            this.mfwObject.DocumentName = string.Empty;
            this.tbSelectedDocument.Text = $"Email Blast - Select Document!";            
        }

        private void _email_LOI_Perc_selectedIndexChanged(object sender, EventArgs e)
        {
            mfwObject.LabelDocSel = eLabelDocumentSelection.emailLOI;
            mfwObject.LoiSelected = true;

            mfwObject.DocumentName = Application.StartupPath + Globals.EmailPath + "LOI-ForEmail.tx";
                        
            this.mfwObject.Percentage = GetPercentage(this._email_LOI_Perc.SelectedItem.ToString());
            this.tbSelectedDocument.Text = $"Email LOI, percentage at {this.mfwObject.Percentage}%.";
            
            SetEmailLOIDataToObject();            
        }

        private void SetEmailLOIDataToObject()
        {
            mfwObject.DisplayName = _email_LOI_DisplayName.Text;
            mfwObject.ReplyTo = _email_LOI_ReplyTo.Text;
            mfwObject.SubjectPrefix = _email_LOI_SubjectPrefix.Text;
            mfwObject.AppendAddressToSubject = _email_LOI_AppendAddress.Checked;
            mfwObject.Body = _email_LOI_EmailBody.Text;
        }

        private void SetEmailBlastDataToObject()
        {
            mfwObject.DisplayName = _email_Blast_DisplayName.Text;
            mfwObject.ReplyTo = _email_Blast_ReplyTo.Text;
            mfwObject.SubjectPrefix = _email_Blast_SubjectPrefix.Text;
            mfwObject.AppendAddressToSubject = _email_Blast_AppendAddress.Checked;
            mfwObject.Body = _email_Blast_EmailBody.Text;
        }

        private double GetPercentage(string selectedPercentage)
        {
            double perc = 0;
            try
            {
                selectedPercentage = selectedPercentage.Replace("%", string.Empty);
                selectedPercentage = selectedPercentage.Trim();

                IFormatProvider format = new System.Globalization.CultureInfo("en-US", true);
                perc = Convert.ToDouble(selectedPercentage, format);
            }
            catch (Exception)
            {
                perc = 0;
            }

            return perc;
        }

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            var selectedTabIdx = ((TabControl)sender).SelectedIndex;

            if (0 == selectedTabIdx)
            {                
                if (rbLarge.Checked)
                {
                   tbSelectedDocument.Text = "Label with Barcode";
                   mfwObject.LabelDocSel = eLabelDocumentSelection.labelLarge;
                   mfwObject.DocumentName = Globals.LargeLabel;
                    
                }
                else if (rbMedium.Checked)
                {
                   tbSelectedDocument.Text = "Label without Barcode";
                   mfwObject.LabelDocSel = eLabelDocumentSelection.labelMedium;
                   mfwObject.DocumentName = Globals.MediumLabel;
                    
                }
                else if (rbSmall.Checked)
                {
                   tbSelectedDocument.Text = "Small Label with Barcode";
                   mfwObject.LabelDocSel = eLabelDocumentSelection.labelSmall;
                   mfwObject.DocumentName = Globals.SmallLabel;                    
                }
            }
            if (1 == selectedTabIdx)
            {
                tbSelectedDocument.Text = string.Empty;
                mfwObject.LabelDocSel = eLabelDocumentSelection.notSet;
                mfwObject.DocumentName = string.Empty;

            }
            if (2 == selectedTabIdx)
            {
                if (null != this._email_LOI_Perc.SelectedItem)
                {
                    mfwObject.LabelDocSel = eLabelDocumentSelection.emailLOI;
                    mfwObject.LoiSelected = true;

                    mfwObject.DocumentName = Application.StartupPath + Globals.EmailPath + "LOI-ForEmail.tx";

                    double perc = GetPercentage(this._email_LOI_Perc.SelectedItem.ToString());
                    mfwObject.Percentage = perc;

                    tbSelectedDocument.Text = $"Email LOI, percentage at {perc}%.";
                }
                else
                {
                    tbSelectedDocument.Text = $"Select LOI Percentage.";
                    mfwObject.LabelDocSel = eLabelDocumentSelection.notSet;
                }
            }
            if (3 == selectedTabIdx)
            {
                if (string.Empty != _email_Blast_SelectedTemplate.Text)
                {
                    tbSelectedDocument.Text = $"Email Blast:{_email_Blast_SelectedTemplate.Text}";

                    mfwObject.LabelDocSel = eLabelDocumentSelection.emailBlast;
                    mfwObject.DocumentName = _email_Blast_SelectedTemplate.Text;
                }
                else
                {
                    //new test - send only text
                    tbSelectedDocument.Text = $"Select Email Blast Template or send custom email.";
                    mfwObject.LabelDocSel = eLabelDocumentSelection.emailBlast;

                    //old working
                    //tbSelectedDocument.Text = $"Select Email Blast Template.";
                    //mfwObject.LabelDocSel = eLabelDocumentSelection.notSet;
                }
            }
            if (4 == selectedTabIdx)
            {
                if (rbLetterOfIntent.Checked)
                {
                    tbSelectedDocument.Text = "Letter Of Intent at " + this.cbPercentage.SelectedItem.ToString();
                    mfwObject.LabelDocSel = eLabelDocumentSelection.faxDocument;
                    mfwObject.DocumentName = Globals.LOI;

                    string selectedPercentage = this.cbPercentage.SelectedItem.ToString();
                    selectedPercentage = selectedPercentage.Replace("%", string.Empty);
                    selectedPercentage = selectedPercentage.Trim();

                    double perc = 0;
                    try
                    {
                        System.IFormatProvider format = new System.Globalization.CultureInfo("en-US", true);
                        perc = Convert.ToDouble(selectedPercentage, format);
                    }
                    catch (Exception)
                    {
                        perc = 0;
                    }
                    mfwObject.Percentage = perc;
                }
                else
                {
                    tbSelectedDocument.Text = $"Select Letter of Intent or Other Fax Document.";
                    mfwObject.LabelDocSel = eLabelDocumentSelection.notSet;
                }
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            _email_Blast_AppendFooter.Checked = true;

            var cb = (ComboBox)sender;

            if (((string)cb.SelectedItem).Contains("#1"))
            {
                _email_Blast_EmailBody.Text = ReadTemplate(1);  //$"{Environment.NewLine}TEMPLATE #1";
                _email_Blast_AppendFooter_CheckedChanged(this, null);
                return;
            }
            if (((string)cb.SelectedItem).Contains("#2"))
            {
                _email_Blast_EmailBody.Text = ReadTemplate(2); //$"{Environment.NewLine}TEMPLATE #2";
                _email_Blast_AppendFooter_CheckedChanged(this, null);
                return;
            }
            if (((string)cb.SelectedItem).Contains("#3"))
            {
                _email_Blast_EmailBody.Text = ReadTemplate(3); //$"{Environment.NewLine}TEMPLATE #3";
                _email_Blast_AppendFooter_CheckedChanged(this, null);
                return;
            }
            else
                _email_Blast_EmailBody.Text = string.Empty;

            _email_Blast_AppendFooter_CheckedChanged(this, null);
        }

        private string ReadTemplate(int templateId)
        {
            var assembly = Assembly.GetExecutingAssembly();
            var resourceName = string.Empty;
            var outputText = string.Empty;

            if (1 == templateId)
            {
                resourceName = assembly.GetManifestResourceNames().Single(str => str.EndsWith("MFWT1.txt"));                
            }
            else if (2 == templateId)
            {
                resourceName = assembly.GetManifestResourceNames().Single(str => str.EndsWith("MFWT2.txt"));                
            }
            else if (3 == templateId)
            {
                resourceName = assembly.GetManifestResourceNames().Single(str => str.EndsWith("MFWT3.txt"));
            }
            
            try
            {
                using (var stream = assembly.GetManifestResourceStream(resourceName))
                {
                    if (null != stream)
                        using (var streamReader = new StreamReader(stream))
                            outputText = streamReader.ReadToEnd();
                }
            }
            catch (Exception e)
            {
                outputText = string.Empty;
            }
            
            return outputText;
        }

        private void _email_Blast_AppendFooter_CheckedChanged(object sender, EventArgs e)
        {
            if (_email_Blast_AppendFooter.Checked)
                _email_Blast_EmailBody.Text += $"{Environment.NewLine}{Globals.EmailBlastSig}";
            else
            {
                _email_Blast_EmailBody.Text = _email_Blast_EmailBody.Text.Replace($"{Environment.NewLine}{Globals.EmailBlastSig}", string.Empty);
            }
        }

        private void _email_LOI_AppendFooter_CheckedChanged(object sender, EventArgs e)
        {
            if (_email_LOI_AppendFooter.Checked)
                _email_LOI_EmailBody.Text += $"{Environment.NewLine}{Globals.EmailLOISig}";
            else
            {
                _email_LOI_EmailBody.Text = _email_LOI_EmailBody.Text.Replace($"{Environment.NewLine}{Globals.EmailLOISig}", string.Empty);
            }
        }
    }
	#endregion � Class MailFaxWizardForm4 �
}
#endregion � Namespace DealMaker �