using System;
using System.Drawing;
using System.Collections;
using System.Windows.Forms;
using System.ComponentModel;
using DealMaker;
using DealMaker.DALC;
using DealMaker.Filters;
using DealMaker.Classes;
using DealMaker.Forms.Wizard;


namespace DealMaker.Forms.Export
{
	public class SelectContactsPage : DealMaker.Forms.Wizard.WizardPage
	{ 
		private PropertyItem[] contacts = null;

		private System.Windows.Forms.TreeView groupsTreeView;
		private System.Windows.Forms.Label groupLabel;
		private System.Windows.Forms.CheckBox eliminateDoNotMailcheckBox;
		private System.Windows.Forms.Label selectedGroupLabel;
		private System.Windows.Forms.PictureBox pbImage;
		private System.ComponentModel.IContainer components = null;

		public SelectContactsPage()
		{
			// This call is required by the Windows Form Designer.
			InitializeComponent();
		}

		public SelectContactsPage(WizardForm wizardForm): 
			base(wizardForm)
		{
			// This call is required by the Windows Form Designer.
			InitializeComponent();

			FirstEntered += new EventHandler(FirstEnteredEventHandler);
			CanExit += new CanExitEventHandler(CanExitEventHandler);
			
			groupsTreeView.AfterSelect += new TreeViewEventHandler(AfterSelectEventHandler);
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Shitty Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(SelectContactsPage));
			this.groupsTreeView = new System.Windows.Forms.TreeView();
			this.groupLabel = new System.Windows.Forms.Label();
			this.eliminateDoNotMailcheckBox = new System.Windows.Forms.CheckBox();
			this.selectedGroupLabel = new System.Windows.Forms.Label();
			this.pbImage = new System.Windows.Forms.PictureBox();
			this.SuspendLayout();
			// 
			// groupsTreeView
			// 
			this.groupsTreeView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.groupsTreeView.HideSelection = false;
			this.groupsTreeView.ImageIndex = -1;
			this.groupsTreeView.Location = new System.Drawing.Point(0, 18);
			this.groupsTreeView.Name = "groupsTreeView";
			this.groupsTreeView.SelectedImageIndex = -1;
			this.groupsTreeView.Size = new System.Drawing.Size(400, 112);
			this.groupsTreeView.TabIndex = 0;
			// 
			// groupLabel
			// 
			this.groupLabel.Location = new System.Drawing.Point(0, 0);
			this.groupLabel.Name = "groupLabel";
			this.groupLabel.Size = new System.Drawing.Size(136, 16);
			this.groupLabel.TabIndex = 1;
			this.groupLabel.Text = "Select a group to export";
			// 
			// eliminateDoNotMailcheckBox
			// 
			this.eliminateDoNotMailcheckBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.eliminateDoNotMailcheckBox.Location = new System.Drawing.Point(272, 291);
			this.eliminateDoNotMailcheckBox.Name = "eliminateDoNotMailcheckBox";
			this.eliminateDoNotMailcheckBox.Size = new System.Drawing.Size(128, 16);
			this.eliminateDoNotMailcheckBox.TabIndex = 3;
			this.eliminateDoNotMailcheckBox.Text = "Eliminate \"Do Not Mail\" entries";
			this.eliminateDoNotMailcheckBox.Visible = false;
			// 
			// selectedGroupLabel
			// 
			this.selectedGroupLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.selectedGroupLabel.Location = new System.Drawing.Point(0, 290);
			this.selectedGroupLabel.Name = "selectedGroupLabel";
			this.selectedGroupLabel.Size = new System.Drawing.Size(244, 16);
			this.selectedGroupLabel.TabIndex = 4;
			this.selectedGroupLabel.Text = "Selected group:";
			// 
			// pbImage
			// 
			this.pbImage.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.pbImage.Image = ((System.Drawing.Image)(resources.GetObject("pbImage.Image")));
			this.pbImage.Location = new System.Drawing.Point(56, 138);
			this.pbImage.Name = "pbImage";
			this.pbImage.Size = new System.Drawing.Size(288, 144);
			this.pbImage.TabIndex = 5;
			this.pbImage.TabStop = false;
			// 
			// SelectContactsPage
			// 
			this.Controls.Add(this.pbImage);
			this.Controls.Add(this.selectedGroupLabel);
			this.Controls.Add(this.eliminateDoNotMailcheckBox);
			this.Controls.Add(this.groupLabel);
			this.Controls.Add(this.groupsTreeView);
			this.Name = "SelectContactsPage";
			this.Size = new System.Drawing.Size(408, 312);
			this.Title = "Select Contacts To Export";
			this.ResumeLayout(false);

		}
		#endregion

		//sd: fix, added "All Groups" for root node, fixed group order (now consistent with other group listings)
		private void EnumerateGroup(TreeView treeView, Hashtable groups, Group root, TreeNode rootNode)
		{			
			ArrayList keys = new ArrayList(groups.Keys);

			for(int i=keys.Count-1;i>=0;i--)
			{
				Group parentGroup = null;
				Group group = groups[keys[i]] as Group;

				if (null == group)
					continue;

				if (groups.Contains(group.ParentGroupId))
					parentGroup = groups[group.ParentGroupId] as Group;

				if (root == parentGroup)
				{
					TreeNode node = new TreeNode();

					node.Text = group.Name;
					node.Tag = group;

					if (null == rootNode)
					{
						node.Text = "All Groups";
						treeView.Nodes.Add(node);
						node.Expand();
					}
					else
					{
						rootNode.Nodes.Add(node);
					}

					EnumerateGroup(treeView, groups, group, node);
				}
			}
		}

		void EnumerateNode(TreeNode node, Hashtable contacts)
		{
			PropertyItem[] groupItems;
			Group g = node.Tag as Group;
			if (null == g)
				return;

			groupItems = g.PropertyItems;
			foreach (PropertyItem item in groupItems)
				contacts[item.IdPropertyItem] = item;

			foreach (TreeNode subNode in node.Nodes)
				EnumerateNode(subNode, contacts);
		}

		void FirstEnteredEventHandler(object sender, EventArgs e)
		{
			Hashtable nodes = new Hashtable();
			Hashtable groups = new Hashtable();

//			ArrayList groupList = new ArrayList(GroupFactory.GetInstance().GetGroups());
			
//			foreach (Group g in GroupFactory.GetInstance().GetGroups())
//			for(int i=groupList.Count-1;i>=0;i--)
//				groups.Add(((Group)groupList[i]).GroupId, (Group)groupList[i]);

			foreach (Group g in GroupFactory.GetInstance().GetGroups())
				groups.Add(g.GroupId, g);

			groupsTreeView.BeginUpdate();
			EnumerateGroup(groupsTreeView, groups, null, null);
			//EnumerateGroup(groupsTreeView, groupList, null, null);
			groupsTreeView.EndUpdate();
		}

		bool CanExitEventHandler(object sender, EventArgs e)
		{
			ArrayList resultList = new ArrayList();
			Hashtable contactsList = new Hashtable();

			if (null == groupsTreeView.SelectedNode)
			{
				MessageBox.Show(this, "Select group for export!", "Select group", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				return false;
			}

			EnumerateNode(groupsTreeView.SelectedNode, contactsList);

			resultList.AddRange(contactsList.Values);
			contacts = (PropertyItem[])resultList.ToArray(typeof(PropertyItem));

			return contacts != null && contacts.Length > 0;	   
		}

		void AfterSelectEventHandler(object sender, TreeViewEventArgs e)
		{
			selectedGroupLabel.Text = e.Node.Text;
		}

		#region Properties
		public PropertyItem[] Contacts
		{
			get 
			{
				return contacts;
			}
			set
			{
				contacts = value;
			}
		}

		public bool EliminateDoNotMailEntries
		{
			get { return eliminateDoNotMailcheckBox.Checked; }
		}
		#endregion
	}
}

