using System;
using System.IO;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using DealMaker.Mapi;
using DealMaker.Export;
using DealMaker.Filters;
using DealMaker.Forms.Wizard;
using ProfitGrabber.Sheets.Repo;
using System.Linq;

namespace DealMaker.Forms.Export
{
	public class ExportCompletedPage : DealMaker.Forms.Wizard.WizardPage
	{
		private System.Windows.Forms.Label exportedCountLabel;
		private System.Windows.Forms.Label totalExportedLabel;
		private System.Windows.Forms.Label locationInfoLabel;
		private System.Windows.Forms.Label locationLabel;
		private System.Windows.Forms.Label label1;
		private System.ComponentModel.IContainer components = null;

		public ExportCompletedPage()
		{
			// This call is required by the Windows Form Designer.
			InitializeComponent();
		}

		public ExportCompletedPage(WizardForm wizardForm):
			base(wizardForm)
		{
			// This call is required by the Windows Form Designer.
			InitializeComponent();

			Entered += new EventHandler(EnteredEventHandler);
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		private async void EnteredEventHandler(object sender, EventArgs e)
		{
			PropertyItemsExport export;
			SelectContactsPage csp = WizardForm[0] as SelectContactsPage;
			CreateExportProfilePage cepp = WizardForm[2] as CreateExportProfilePage;
			CreateExportFilterPage cefp = WizardForm[3] as CreateExportFilterPage;
			SelectExportLocationPage selp = WizardForm[6] as SelectExportLocationPage;

			exportedCountLabel.Text = "exporting...";

			WizardForm.ButtonStyles = WizardFormButtonStyles.HelpButton |
									  WizardFormButtonStyles.FinishButton |
									  WizardFormButtonStyles.NextButton |
									  WizardFormButtonStyles.BackButton;

			locationLabel.Text = selp.ExportFileName;

			export = new PropertyItemsExport(csp.Contacts);

			foreach (ExportAttributeMapping mapping in cepp.SelectedFields)
				export.AddMapping(mapping);

			foreach (IPropertyItemFilter filter in cefp.Filters)
				export.AddFilter(filter);

			if (selp.SaveToGoogleDrive)
			{
				locationLabel.Visible = label1.Visible = locationInfoLabel.Visible  = false;
				this.Title = string.Empty;				
			}
			else
			{
				export.ExportFileName = selp.ExportFileName;
				export.ExportWriter = selp.ExportWriter;
			}
						
			exportedCountLabel.Text = export.Execute().ToString();			

			if (selp.AttachToEmail)
			{
				SimpleMapi.SendDocuments(
					new String[] {selp.ExportFileName},
					new String[] {selp.ExportFileDocument});
			}

			if (selp.SaveToGoogleDrive)
			{
				exportedCountLabel.Text = $"Uploading...";
				try
				{										
					GoogleSheetsClient client = new GoogleSheetsClient();
					var res = await client.CreateDocument(Globals.ClientId, Globals.ClientSecret, selp.SpreadSheetTitle, selp.DataSheetTitle, export.SpreadSheetData, selp.GoogleAccount);
					
					if (res.Updates.UpdatedRows > 0)
						exportedCountLabel.Text = $"Upload Finished for {res.Updates.UpdatedRows-1} records.";
					else
						exportedCountLabel.Text = $"Uploaded Empty Document.";
				}
				catch (Exception exc)
                {
					MessageBox.Show(exc.Message, "Error in Upload", MessageBoxButtons.OK);
                }								
			}
		}

		#region Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.exportedCountLabel = new System.Windows.Forms.Label();
            this.totalExportedLabel = new System.Windows.Forms.Label();
            this.locationInfoLabel = new System.Windows.Forms.Label();
            this.locationLabel = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // exportedCountLabel
            // 
            this.exportedCountLabel.AutoSize = true;
            this.exportedCountLabel.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.exportedCountLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.exportedCountLabel.Location = new System.Drawing.Point(184, 120);
            this.exportedCountLabel.Name = "exportedCountLabel";
            this.exportedCountLabel.Size = new System.Drawing.Size(71, 13);
            this.exportedCountLabel.TabIndex = 9;
            this.exportedCountLabel.Text = "exporting...";
            // 
            // totalExportedLabel
            // 
            this.totalExportedLabel.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.totalExportedLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.totalExportedLabel.Location = new System.Drawing.Point(0, 120);
            this.totalExportedLabel.Name = "totalExportedLabel";
            this.totalExportedLabel.Size = new System.Drawing.Size(184, 16);
            this.totalExportedLabel.TabIndex = 8;
            this.totalExportedLabel.Text = "TOTAL # of Records exported:";
            // 
            // locationInfoLabel
            // 
            this.locationInfoLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.locationInfoLabel.Location = new System.Drawing.Point(0, 0);
            this.locationInfoLabel.Name = "locationInfoLabel";
            this.locationInfoLabel.Size = new System.Drawing.Size(312, 16);
            this.locationInfoLabel.TabIndex = 11;
            this.locationInfoLabel.Text = "Your export file is saved in the  following location:";
            // 
            // locationLabel
            // 
            this.locationLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.locationLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.locationLabel.Location = new System.Drawing.Point(48, 24);
            this.locationLabel.Name = "locationLabel";
            this.locationLabel.Size = new System.Drawing.Size(216, 40);
            this.locationLabel.TabIndex = 12;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.Location = new System.Drawing.Point(0, 72);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(312, 16);
            this.label1.TabIndex = 13;
            this.label1.Text = "Please, write down the location of your exported file!";
            // 
            // ExportCompletedPage
            // 
            this.Controls.Add(this.label1);
            this.Controls.Add(this.locationLabel);
            this.Controls.Add(this.locationInfoLabel);
            this.Controls.Add(this.exportedCountLabel);
            this.Controls.Add(this.totalExportedLabel);
            this.Name = "ExportCompletedPage";
            this.Size = new System.Drawing.Size(312, 160);
            this.Title = "Export Completed";
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		#endregion
	}
}

