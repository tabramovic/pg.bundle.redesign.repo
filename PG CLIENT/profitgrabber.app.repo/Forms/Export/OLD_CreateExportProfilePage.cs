using System;
using System.Collections;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using DealMaker;
using DealMaker.Export;
using DealMaker.PlainClasses;
using DealMaker.Forms.Wizard;
using System.Reflection;

namespace DealMaker.Forms.Export
{
	public class CreateExportProfilePage : DealMaker.Forms.Wizard.WizardPage
	{
		private System.Windows.Forms.Label availableFieldsLabel;
		private System.Windows.Forms.Label selectedFieldsLabel;
		private System.Windows.Forms.Button removeButton;
		private System.Windows.Forms.Button addEmptyButton;
		private System.Windows.Forms.Button upButton;
		private System.Windows.Forms.Button downButton;
		private System.Windows.Forms.ListBox selectedListBox;
		private System.Windows.Forms.Button addButton;
		private System.Windows.Forms.Button addAllButton;
		private System.Windows.Forms.ListBox availableListBox1;
		private System.Windows.Forms.ListView availableListBox;
		private System.ComponentModel.IContainer components = null;

		public CreateExportProfilePage(WizardForm wizardForm): base(wizardForm)
		{
			// This call is required by the Windows Form Designer.
			InitializeComponent();

			FirstEntered += new EventHandler(FirstEnteredEventHandler);
			CanExit += new CanExitEventHandler(CanExitEventHandler);

			addButton.Click += new EventHandler(AddButtonClickedEventHandler);
			removeButton.Click += new EventHandler(RemoveButtonClickedEventHandler);
			addEmptyButton.Click += new EventHandler(AddEmptyButtonClickedEventHandler);
			addAllButton.Click += new EventHandler(AddAllButonClickedEventHandler);
			upButton.Click += new EventHandler(UpButtonClickedEventHandler);
			downButton.Click += new EventHandler(DownButtonClickEventHandler);
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		private void SelectedListBoxShiftSelectedItem(int offset)
		{
			object o;
			int selectedIndex = selectedListBox.SelectedIndex;
			object selectedItem = selectedListBox.SelectedItem;
			ListBox.ObjectCollection items = selectedListBox.Items;

			if (null == selectedItem || 
				selectedIndex + offset < 0 ||
				selectedIndex + offset >= selectedListBox.Items.Count )
				return;
			
			o = items[selectedIndex + offset];
			items[selectedIndex + offset] = selectedItem;
			items[selectedIndex] = o;

			selectedListBox.SetSelected(selectedIndex, false);
			selectedListBox.SetSelected(selectedIndex + offset, true);
		}

		private class ItemAttribute : IComparable
		{
			public ItemAttribute()
			{
			}

			public ItemAttribute(String description, int position)
			{
				this.Description = description;
				this.Position = position;
			}

			public ItemAttribute(PropertyItemAttribute attribute, int position)
			{
				this.PropertyItemAttribute = attribute;
				this.Position = position;
			}
/*
			public ItemAttribute(Color color, int position)
			{
				this.color = color;
				this.Position = position;
			}
*/
			public ItemAttribute(bool alternate_color, int position)
			{
				this.AlternateColor = alternate_color;
				this.Position = position;
			}

			//public ExportAttributeMapping ExportAttributeMapping;

			public bool AlternateColor = false;
			public string Description;
//			public Color color = Color.AliceBlue;
			public int Position;
			public PropertyItemAttribute PropertyItemAttribute;
			#region IComparable Members

			public int CompareTo(object obj)
			{
				return this.Position - ((ItemAttribute)obj).Position;
			}

			#endregion
		}

		private void FirstEnteredEventHandler(object sender, EventArgs e)
		{
			//sasa, 20.09.05: redesign needed, again
			/*
			SelectContactsPage contactsPage = WizardForm[0] as SelectContactsPage;

			//sd: note, in original version this selects only attributes from first object
			PropertyItem item = contactsPage.Contacts[0];

			//sd: note, here we could put foreach all objects in list, and return get object
			//attributes to original which returns only used properties
			//in that case, in list then should appear only propertyes used in that group
			//for now, this is considered performance penality and not time worth for implementation
			//(if proceaded with implementation, should check architecture, because risk of duplicates)
			foreach (PropertyItemAttribute attribute in item.PropertyItemAttributes)
				availableListBox.Items.Add(attribute);
			*/

			//sasa, 20.09.05: here we go...
			
			// !!!!!!!!!!! HOROR CODE, ABANDON ALL HOPE !!!!!!!!!!!!!!
			// due to insane requests, timeline and nonconsistent property arhitecture,
			// this looks like it looks

			try
			{
				SelectContactsPage contactsPage = WizardForm[0] as SelectContactsPage;

				PropertyItem item = contactsPage.Contacts[0];

				//ObjectPresentation.CollectionKeyObject keyObject = new ObjectPresentation.CollectionKeyObject();

				Hashtable htItemPositions = new Hashtable();
				ImportIntermedeateObject iio = new ImportIntermedeateObject();		
				PropertyInfo[] pi =  iio.GetType().GetProperties();			
				for (int i = 0; i < pi.Length; i++)
				{
					object[] descAttr = pi[i].GetCustomAttributes(typeof(DescriptionAttribute), true);
					object[] posAttr = pi[i].GetCustomAttributes(typeof(ItemPositionAttribute), true);
					if (0 != descAttr.Length && 0 != posAttr.Length)
					{      
						//DescriptionAttribute da = (DescriptionAttribute)descAttr[0];
						//DescriptionAttribute da = (DescriptionAttribute)descAttr[0];
						ItemPositionAttribute ita = (ItemPositionAttribute)posAttr[0];

						if (false == htItemPositions.Contains(pi[i].Name))
						{
							int iPos = 1;
							try
							{
								iPos = Convert.ToInt16(ita.Desc);
							}
							catch (Exception)
							{
								iPos = 1;
							}
							htItemPositions.Add(pi[i].Name, iPos);
						}
					}
				}



				ArrayList attributeList = new ArrayList();

				//tst
//				attributeList.Add(new ItemAttribute("Site Street Address",101));
//				attributeList.Add(new ItemAttribute("Site Street Address",101));

				attributeList.Add(new ItemAttribute(true, 1009));

				attributeList.Add(new ItemAttribute("Site Street Address",1010));
				attributeList.Add(new ItemAttribute("Site Street Number",1020));
				attributeList.Add(new ItemAttribute("Site Steet PreDirectional",1030));
				attributeList.Add(new ItemAttribute("Site Street Name",1040));
				attributeList.Add(new ItemAttribute("Site Street PostDirectional",1050));
				attributeList.Add(new ItemAttribute("Site Street Suffix",1060));
				attributeList.Add(new ItemAttribute("Site Street Unit Number",1070));
				attributeList.Add(new ItemAttribute("Site City",1080));
				attributeList.Add(new ItemAttribute("Site State",1090));
				attributeList.Add(new ItemAttribute("Site ZIP",1100));
				attributeList.Add(new ItemAttribute("Site City, State",1110));

				//Mail Address

				attributeList.Add(new ItemAttribute(true, 1119));
				attributeList.Add(new ItemAttribute("Last Known Mailing Street Address",1120));
				attributeList.Add(new ItemAttribute("Last Known Mailing City, State",1130));
				attributeList.Add(new ItemAttribute("Last Known Mailing City",1140));
				attributeList.Add(new ItemAttribute("Last Known Mailing State",1150));
				attributeList.Add(new ItemAttribute("Last Known Mailing ZIP",1160));

				attributeList.Add(new ItemAttribute(true, 1169));
				attributeList.Add(new ItemAttribute("Previously Known Mailing Street Address",1170));
				attributeList.Add(new ItemAttribute("Previously Known Mailing City, State",1180));
				attributeList.Add(new ItemAttribute("Previously Known Mailing City",1190));
				attributeList.Add(new ItemAttribute("Previously Known Mailing State",1200));
				attributeList.Add(new ItemAttribute("Previously Known Mailing ZIP",1210));


				//add colos for imported from intermediete object
				int attributeOffset = 4999;
				attributeList.Add(new ItemAttribute(true, attributeOffset + 10*28));
				attributeList.Add(new ItemAttribute(true, attributeOffset + 10*37));
				attributeList.Add(new ItemAttribute(true, attributeOffset + 10*51));
//				attributeList.Add(new ItemAttribute(true, attributeOffset + 10*66));
				attributeList.Add(new ItemAttribute(true, attributeOffset + 10*77));
				attributeList.Add(new ItemAttribute(true, attributeOffset + 10*98));
				
				attributeList.Add(new ItemAttribute(true, 109969));
				attributeList.Add(new ItemAttribute(true, 109999));
				attributeList.Add(new ItemAttribute(true, 110109));
//				attributeList.Add(new ItemAttribute(true, 110169));

				attributeList.Add(new ItemAttribute(true, 119));
				

				foreach (PropertyItemAttribute attribute in item.PropertyItemAttributes)
				{
					ItemAttribute ia = new ItemAttribute();
					ia.PropertyItemAttribute = attribute;
					ia.Position = 100000;

					if(htItemPositions.ContainsKey(attribute.AttributeProperty.Name))
					{
						ia.Position = ((int)htItemPositions[attribute.AttributeProperty.Name])*10+5000;
					}
	/*				foreach(object str in htItemPositions.Keys)
					{
						if(((string)str).Trim() == attribute.AttributeProperty.Name.Trim())
						{
							ia.Position = (int)htItemPositions[str];
							break;
						}
					}
	*/

/*
 * IGNORE
*/
					//ignore list
					if(ia.PropertyItemAttribute.AttributeDescription == "Trustee Phone Nr") continue;
					if(ia.PropertyItemAttribute.AttributeDescription == "eMail") continue;
					if(ia.PropertyItemAttribute.AttributeDescription == "Date Created") continue;
					if(ia.PropertyItemAttribute.AttributeDescription == "Date Last Modified") continue;
					if(ia.PropertyItemAttribute.AttributeDescription == "Fax Number") continue;
					if(ia.PropertyItemAttribute.AttributeDescription == "Barcode End Number") continue;
					if(ia.PropertyItemAttribute.AttributeDescription == "Date Entered") continue;
					if(ia.PropertyItemAttribute.AttributeDescription == "Lead Source") continue;
					if(ia.PropertyItemAttribute.AttributeDescription == "City Code") continue;
					if(ia.PropertyItemAttribute.AttributeDescription == "Trustee Zip") continue;
					if(ia.PropertyItemAttribute.AttributeDescription == "Max Offset") continue;
					if(ia.PropertyItemAttribute.AttributeDescription == "Bar Code Start number") continue;
					if(ia.PropertyItemAttribute.AttributeDescription == "Map Number Part of APN") continue;
					if(ia.PropertyItemAttribute.AttributeDescription == "Date Edited") continue;
					if(ia.PropertyItemAttribute.AttributeDescription == "Book Number Part of APN") continue;
					if(ia.PropertyItemAttribute.AttributeDescription == "Owner Absent Occupied") continue;
					if(ia.PropertyItemAttribute.AttributeDescription == "School District") continue;


					//PRVO
					if(ia.PropertyItemAttribute.AttributeDescription == "PropertyItem Id") ia.Position = 100;
					if(ia.PropertyItemAttribute.AttributeDescription == "Barcode Id") ia.Position = 110;
					if(ia.PropertyItemAttribute.AttributeDescription == "Full Name") ia.Position = 120;
					if(ia.PropertyItemAttribute.AttributeDescription == "First Name") ia.Position = 130;
					if(ia.PropertyItemAttribute.AttributeDescription == "Middle Name") ia.Position = 140;
					if(ia.PropertyItemAttribute.AttributeDescription == "Last Name") ia.Position = 150;
					if(ia.PropertyItemAttribute.AttributeDescription == "Salutation Name") ia.Position = 160;
					if(ia.PropertyItemAttribute.AttributeDescription == "Go By") ia.Position = 170;

					if(ia.PropertyItemAttribute.AttributeDescription == "Full Cash Value") ia.Position = attributeOffset + 10*50;

					//PRED PREDZADNJE
					if(ia.PropertyItemAttribute.AttributeDescription == "Subdivision") ia.Position = 109970;
					if(ia.PropertyItemAttribute.AttributeDescription == "Fireplace") ia.Position = 109980;
					if(ia.PropertyItemAttribute.AttributeDescription == "Story") ia.Position = 109990;

					if(ia.PropertyItemAttribute.AttributeDescription == "First Loan Balance") ia.Position = 110000;
					if(ia.PropertyItemAttribute.AttributeDescription == "Second Loan Balance") ia.Position = 110010;
					if(ia.PropertyItemAttribute.AttributeDescription == "Third Loan Balance") ia.Position = 110020;
					if(ia.PropertyItemAttribute.AttributeDescription == "First Loan Monthly") ia.Position = 110030;
					if(ia.PropertyItemAttribute.AttributeDescription == "Second Loan Monthly") ia.Position = 110040;
					if(ia.PropertyItemAttribute.AttributeDescription == "Third Loan Monthly") ia.Position = 110050;
					if(ia.PropertyItemAttribute.AttributeDescription == "Total Loans") ia.Position = 110060;
					if(ia.PropertyItemAttribute.AttributeDescription == "Reinstatement amount") ia.Position = 110070;
					if(ia.PropertyItemAttribute.AttributeDescription == "HOA per month") ia.Position = 110080;
					if(ia.PropertyItemAttribute.AttributeDescription == "Unpaid Balance") ia.Position = 110090;
					if(ia.PropertyItemAttribute.AttributeDescription == "Amnt of Other Liens") ia.Position = 110100;

					//PRED ZADNJE
					if(ia.PropertyItemAttribute.AttributeDescription == "Foreclosure File Number") ia.Position = 110110;
					if(ia.PropertyItemAttribute.AttributeDescription == "Foreclosure Status") ia.Position = 110120;
					if(ia.PropertyItemAttribute.AttributeDescription == "Foreclosure Deed Date") ia.Position = 110128;
					if(ia.PropertyItemAttribute.AttributeDescription == "Foreclosure Sale Date") ia.Position = 110130;
					if(ia.PropertyItemAttribute.AttributeDescription == "Foreclosure File Date") ia.Position = 110140;
					if(ia.PropertyItemAttribute.AttributeDescription == "Foreclosure Sale Time") ia.Position = 110141;
					if(ia.PropertyItemAttribute.AttributeDescription == "Foreclosure Sale Place") ia.Position = 110141;
					if(ia.PropertyItemAttribute.AttributeDescription == "Original File Number") ia.Position = 110150;
					if(ia.PropertyItemAttribute.AttributeDescription == "Original Date") ia.Position = 110160;

					//ZADNJE
					if(ia.PropertyItemAttribute.AttributeDescription == "Parcel Number of APN") ia.Position = 110170;
					if(ia.PropertyItemAttribute.AttributeDescription == "Parcel Num. Suffix Part Of APN") ia.Position = 110180;

					if(ia.PropertyItemAttribute.AttributeDescription == "Status") ia.Position = 110190;

					attributeList.Add(ia);
				}

				attributeList.Sort();

				availableListBox.Columns.Add(new ColumnHeader());
				//availableListBox.Columns[0].Text = "Property";
				availableListBox.HeaderStyle = ColumnHeaderStyle.None;
				availableListBox.BackColor = Color.White;
				availableListBox.Columns[0].Width = availableListBox.Width - 25;
				//availableListBox
				
				Color currentColor = Color.White;
				foreach (ItemAttribute attribute in attributeList)
				{
					if(attribute.AlternateColor == true)
					{
						if(currentColor == Color.White)
						{
							currentColor = Color.LightGray;
						}
						else
						{
							currentColor = Color.White;
						}
						continue;
					}
/*					if(attribute.color != Color.AliceBlue)
					{
						currentColor = attribute.color;
						continue;
					}
*/
					//if null add description, mapping will be extracted later
					if(attribute.PropertyItemAttribute == null)
					{
						attribute.PropertyItemAttribute = new PropertyItemAttribute(attribute.Description, null, attribute.Description, null);

//						attribute.ExportAttributeMapping = new ExportAttributeMapping(attr.attributeDescription);

//						item.NextRebuildProperties = true;
//						attribute.PropertyItemAttribute = item[attribute.Description];
						//attribute.PropertyItemAttribute.AttributeDescription = attribute.Description;
//						attribute.PropertyItemAttribute = new PropertyItemAttribute(attribute.Description, null, attribute.Description, null);
					}

					availableListBox.BackColor = currentColor;
					//availableListBox.Items.Add(attribute.PropertyItemAttribute);

					ListViewItem lvi = new ListViewItem(attribute.PropertyItemAttribute.AttributeDescription);
					lvi.Tag = attribute.PropertyItemAttribute;
					lvi.BackColor = currentColor;
					availableListBox.Items.Add(lvi);
				}
				availableListBox.BackColor = Color.White;
			}
			catch(Exception ee)
			{
				MessageBox.Show(ee.ToString());
			}


		}

		private bool CanExitEventHandler(object sender, EventArgs e)
		{
			return selectedListBox.Items.Count > 0;
		}

		private void AddButtonClickedEventHandler(object sender, EventArgs e)
		{
			if (null == availableListBox.SelectedItems)
				return;

			//sasa, 22.09.05: now list view
			//foreach(object o in availableListBox.SelectedItems)
			foreach(ListViewItem lvi in availableListBox.SelectedItems)
			{
				object o = lvi.Tag;
				PropertyItemAttribute attribute;
				ExportAttributeMapping mapping;

				attribute = o as PropertyItemAttribute;
				if (null == o)
					continue;
				try
				{
				if(attribute.AttributeProperty != null)
				{
					mapping = new ExportAttributeMapping(
						attribute.AttributeDescription,
						attribute.AttributeDescription,
						attribute.AttributeProperty.PropertyType.ToString());
				}
				else
				{
					mapping = new ExportAttributeMapping(attribute.AttributeDescription);
				}
				selectedListBox.Items.Add(mapping);
				}
				catch(Exception 
#if DEBUG
					ee
#endif
					){
#if DEBUG
					MessageBox.Show(ee.ToString());
#endif
				}
			}
		}

		private void RemoveButtonClickedEventHandler(object sender, EventArgs e)
		{
			int i;

			if (null == selectedListBox.SelectedItems)
				return;

			for (i = selectedListBox.Items.Count; i > 0; i--)
			{
				if (selectedListBox.GetSelected(i - 1))
					selectedListBox.Items.Remove(selectedListBox.Items[i - 1]);
			}
		}

		private void AddEmptyButtonClickedEventHandler(object sender, EventArgs e)
		{
			selectedListBox.Items.Add(new ExportAttributeMapping(ExportAttributeMapping.EmptyColumn, "", "System.String"));
		}

		private void AddAllButonClickedEventHandler(object sender, EventArgs e)
		{
			try
			{
				//sasa, 22.09.05: now list view
				//foreach(object o in availableListBox.Items)
				foreach(ListViewItem lvi in availableListBox.Items)
				{
					object o = lvi.Tag;
					//sasa: 03.09.05, copied from add button clicked event handler
					//this solves bug in page after
					//no comment on why this is not the same procedure with list parameter
					
					/*
					PropertyItemAttribute attribute;
					ExportAttributeMapping mapping;

					attribute = o as PropertyItemAttribute;
					if (null == o)
						continue;

					mapping = new ExportAttributeMapping(
						attribute.attributeDescription);

					selectedListBox.Items.Add(mapping);
					*/

					PropertyItemAttribute attribute;
					ExportAttributeMapping mapping;

					attribute = o as PropertyItemAttribute;
					if (null == o)
						continue;


					if(attribute.AttributeProperty != null)
					{
						mapping = new ExportAttributeMapping(
							attribute.AttributeDescription,
							attribute.AttributeDescription,
							attribute.AttributeProperty.PropertyType.ToString());
					}
					else
					{
						mapping = new ExportAttributeMapping(attribute.AttributeDescription);
					}

					selectedListBox.Items.Add(mapping);

					/*
									mapping = new ExportAttributeMapping(
										attribute.AttributeDescription,
										attribute.AttributeDescription,
										attribute.AttributeProperty.PropertyType.ToString());

									selectedListBox.Items.Add(mapping);
					*/
				}
			}
			catch(Exception
#if DEBUG
				ee
#endif
				)
			{
#if DEBUG
			MessageBox.Show(ee.ToString());
#endif
			}

		}

		private void UpButtonClickedEventHandler(object sender, EventArgs e)
		{
			SelectedListBoxShiftSelectedItem(-1);
		}

		private void DownButtonClickEventHandler(object sender, EventArgs e)
		{
			SelectedListBoxShiftSelectedItem(1);
		}

		#region Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.availableFieldsLabel = new System.Windows.Forms.Label();
			this.availableListBox1 = new System.Windows.Forms.ListBox();
			this.selectedListBox = new System.Windows.Forms.ListBox();
			this.addButton = new System.Windows.Forms.Button();
			this.removeButton = new System.Windows.Forms.Button();
			this.addEmptyButton = new System.Windows.Forms.Button();
			this.upButton = new System.Windows.Forms.Button();
			this.downButton = new System.Windows.Forms.Button();
			this.selectedFieldsLabel = new System.Windows.Forms.Label();
			this.addAllButton = new System.Windows.Forms.Button();
			this.availableListBox = new System.Windows.Forms.ListView();
			this.SuspendLayout();
			// 
			// availableFieldsLabel
			// 
			this.availableFieldsLabel.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.availableFieldsLabel.Location = new System.Drawing.Point(0, 0);
			this.availableFieldsLabel.Name = "availableFieldsLabel";
			this.availableFieldsLabel.Size = new System.Drawing.Size(88, 16);
			this.availableFieldsLabel.TabIndex = 0;
			this.availableFieldsLabel.Text = "Available Fields:";
			// 
			// availableListBox1
			// 
			this.availableListBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left)));
			this.availableListBox1.Location = new System.Drawing.Point(0, 16);
			this.availableListBox1.Name = "availableListBox1";
			this.availableListBox1.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
			this.availableListBox1.Size = new System.Drawing.Size(208, 238);
			this.availableListBox1.TabIndex = 1;
			// 
			// selectedListBox
			// 
			this.selectedListBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.selectedListBox.Location = new System.Drawing.Point(288, 16);
			this.selectedListBox.Name = "selectedListBox";
			this.selectedListBox.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
			this.selectedListBox.Size = new System.Drawing.Size(208, 238);
			this.selectedListBox.TabIndex = 2;
			// 
			// addButton
			// 
			this.addButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.addButton.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.addButton.Location = new System.Drawing.Point(216, 16);
			this.addButton.Name = "addButton";
			this.addButton.Size = new System.Drawing.Size(64, 23);
			this.addButton.TabIndex = 3;
			this.addButton.Text = "&Add >>";
			// 
			// removeButton
			// 
			this.removeButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.removeButton.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.removeButton.Location = new System.Drawing.Point(216, 48);
			this.removeButton.Name = "removeButton";
			this.removeButton.Size = new System.Drawing.Size(64, 23);
			this.removeButton.TabIndex = 4;
			this.removeButton.Text = "<< &Remove";
			// 
			// addEmptyButton
			// 
			this.addEmptyButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.addEmptyButton.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.addEmptyButton.Location = new System.Drawing.Point(216, 88);
			this.addEmptyButton.Name = "addEmptyButton";
			this.addEmptyButton.Size = new System.Drawing.Size(64, 32);
			this.addEmptyButton.TabIndex = 5;
			this.addEmptyButton.Text = "Add Empty Column";
			// 
			// upButton
			// 
			this.upButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.upButton.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.upButton.Location = new System.Drawing.Point(288, 264);
			this.upButton.Name = "upButton";
			this.upButton.TabIndex = 6;
			this.upButton.Text = "Move Up";
			// 
			// downButton
			// 
			this.downButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.downButton.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.downButton.Location = new System.Drawing.Point(424, 264);
			this.downButton.Name = "downButton";
			this.downButton.Size = new System.Drawing.Size(72, 23);
			this.downButton.TabIndex = 7;
			this.downButton.Text = "Move Down";
			// 
			// selectedFieldsLabel
			// 
			this.selectedFieldsLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.selectedFieldsLabel.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.selectedFieldsLabel.Location = new System.Drawing.Point(288, 0);
			this.selectedFieldsLabel.Name = "selectedFieldsLabel";
			this.selectedFieldsLabel.Size = new System.Drawing.Size(176, 16);
			this.selectedFieldsLabel.TabIndex = 8;
			this.selectedFieldsLabel.Text = "Selected Fields for Export:";
			// 
			// addAllButton
			// 
			this.addAllButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.addAllButton.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.addAllButton.Location = new System.Drawing.Point(216, 128);
			this.addAllButton.Name = "addAllButton";
			this.addAllButton.Size = new System.Drawing.Size(64, 32);
			this.addAllButton.TabIndex = 9;
			this.addAllButton.Text = "&Add all Columns";
			// 
			// availableListBox
			// 
			this.availableListBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left)));
			this.availableListBox.FullRowSelect = true;
			this.availableListBox.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
			this.availableListBox.Location = new System.Drawing.Point(0, 16);
			this.availableListBox.Name = "availableListBox";
			this.availableListBox.Size = new System.Drawing.Size(208, 240);
			this.availableListBox.TabIndex = 10;
			this.availableListBox.View = System.Windows.Forms.View.Details;
			// 
			// CreateExportProfilePage
			// 
			this.Controls.Add(this.availableListBox);
			this.Controls.Add(this.addAllButton);
			this.Controls.Add(this.selectedFieldsLabel);
			this.Controls.Add(this.downButton);
			this.Controls.Add(this.upButton);
			this.Controls.Add(this.addEmptyButton);
			this.Controls.Add(this.removeButton);
			this.Controls.Add(this.addButton);
			this.Controls.Add(this.selectedListBox);
			this.Controls.Add(this.availableListBox1);
			this.Controls.Add(this.availableFieldsLabel);
			this.Name = "CreateExportProfilePage";
			this.Size = new System.Drawing.Size(496, 296);
			this.Title = "Create Export Profile";
			this.ResumeLayout(false);

		}
		#endregion

		#region Properties
		public ExportAttributeMapping[] SelectedFields
		{
			get
			{
				ArrayList items = new ArrayList();

				foreach(object o in selectedListBox.Items)
					items.Add(o);

				return (ExportAttributeMapping[])items.ToArray(typeof(ExportAttributeMapping));
			}
			set
			{
				selectedListBox.Items.Clear();

				if (null != value)
					selectedListBox.Items.AddRange(value);
			}
		}
		#endregion
	}
}


