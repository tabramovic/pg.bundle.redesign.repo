﻿using DealMaker.Export;
using System;
using System.Linq;
using System.Windows.Forms;

namespace DealMaker.Forms.Export
{
    public partial class LoadAccounts : Form
    {
        public string SelectedAddress { get; set; }
        public ExportAccounts Accounts { get; }

        IGoogleAccountsHandler _handler = new GoogleAccountsHandler();
        public LoadAccounts()
        {            
            Accounts = new GoogleAccountsHandler().GetGoogleAccounts();
            Init();
        }

        public LoadAccounts(ExportAccounts accounts)
        {            
            Accounts = accounts;
            Init();
        }

        private void Init()
        {
            InitializeComponent();
            AddEventHandlers();
            InitForm();
        }

        private void AddEventHandlers()
        {
            _useFirst.Click += _useFirst_Click;
            _useSecond.Click += _useSecond_Click;
            _useThird.Click += _useThird_Click;
            _save.Click += _save_Click;
            _dontSave.Click += _dontSave_Click;
        }

        private void _dontSave_Click(object sender, EventArgs e)
        {
            SelectedAddress = Accounts.LastUsedAccountName;
            this.Close();
        }

        private void _save_Click(object sender, EventArgs e)
        {
            SelectedAddress = Accounts.LastUsedAccountName;
            Save(SelectedAddress);
            this.CloseWithOK();
        }
        
        private void _useThird_Click(object sender, EventArgs e)
        {
            SelectedAddress = GetEmailAddressBasedOnIdx(3);
            Save(SelectedAddress);
            CloseWithOK();            
        }

        private void _useSecond_Click(object sender, EventArgs e)
        {
            SelectedAddress = GetEmailAddressBasedOnIdx(2);
            Save(SelectedAddress);
            CloseWithOK();
        }

        private void _useFirst_Click(object sender, EventArgs e)
        {
            SelectedAddress = GetEmailAddressBasedOnIdx(1);
            Save(SelectedAddress);
            CloseWithOK();
        }

        
        private void Save(string lastUsedEmailAddress)
        {
            _handler.Save(new string[] { _firstEmail.Text, _secondEmail.Text, _thirdEmail.Text }, lastUsedEmailAddress);
        }

        private void CloseWithOK()
        {            
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private string GetEmailAddressBasedOnIdx(int status)
        {
            switch (status)
            {
                case 1:
                    return _firstEmail.Text;                    

                case 2:
                    return _secondEmail.Text;                    

                default:
                    return _thirdEmail.Text;                    
            }            
        }

        private void InitForm()
        {                        
            if (Accounts?.AccountNames?.Any() ?? false)
            {
                if (Accounts.AccountNames.Count() > 2)
                    _thirdEmail.Text = Accounts.AccountNames[2];
                if (Accounts.AccountNames.Count() > 1)
                    _secondEmail.Text = Accounts.AccountNames[1];

                _firstEmail.Text = Accounts.AccountNames[0];
            }
        }
    }
}
