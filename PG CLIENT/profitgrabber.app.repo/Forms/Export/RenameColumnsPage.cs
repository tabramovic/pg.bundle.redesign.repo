using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using DealMaker.Export;
using DealMaker.Filters;
using DealMaker.Forms.Wizard;
using DealMaker.PlainClasses;

namespace DealMaker.Forms.Export
{
	public class RenameColumnsPage : DealMaker.Forms.Wizard.WizardPage
	{
		ListViewItem renameRow = null;
		TextBox columnEdit = new TextBox();
		private System.Windows.Forms.Label renameInfoLabel;
		private System.Windows.Forms.ListView renameListView;
		private System.Windows.Forms.Label label1;
		private System.ComponentModel.IContainer components = null;

		[StructLayout(LayoutKind.Sequential)]
		struct DumbPInvokeRect
		{
			public System.Int32 left;
			public System.Int32 top;
			public System.Int32 right;
			public System.Int32 bottom;
		};
 
		[DllImport("user32.dll")]
		private static extern System.Int32 SendMessage(System.IntPtr hWnd, System.UInt32 Msg, System.Int32 wParam, ref DumbPInvokeRect rect);


		public RenameColumnsPage()
		{
			// This call is required by the Windows Form Designer.
			InitializeComponent();
		}

		public RenameColumnsPage(WizardForm wizardForm): base(wizardForm)
		{
			// This call is required by the Windows Form Designer.
			InitializeComponent();

			Entered += new EventHandler(EnteredEventHandler);

			columnEdit.Visible = false;
			renameListView.Controls.Add(columnEdit);			
			
			columnEdit.Leave += new EventHandler(ColumnEditLeaveEventHandler);
			columnEdit.KeyDown += new KeyEventHandler(ColumnEditKeyDown);
			renameListView.ColumnClick += new ColumnClickEventHandler(ColumnClickedEventHandler);
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		private void CreateHeader()
		{
			int width;
			ExportAttributeMapping[] mappings;
			CreateExportProfilePage cepp = WizardForm[2] as CreateExportProfilePage;

			renameListView.Columns.Clear();

			width = 100;
			mappings = cepp.SelectedFields;

			// Create header for ListView control
			renameRow = new ListViewItem();
			renameRow.BackColor = Color.Yellow;
			foreach (ExportAttributeMapping mapping in mappings)
			{
				ColumnHeader header = new ColumnHeader();

				header.Width = width;
				header.Text = mapping.ColumnName;
				renameListView.Columns.Add(header);

				// Create yellow rename row
				if (String.Empty == renameRow.Text)
					renameRow.Text = mapping.ColumnName;
				else
					renameRow.SubItems.Add(mapping.ColumnName);
			}

			renameListView.Items.Add(renameRow);
			renameListView.Refresh();
		}

		private void PopulateList()
		{
			int i;
			int sample = 10;
			PropertyItem[] contacts;
			ExportAttributeMapping[] mappings;
			SelectContactsPage scp = WizardForm[0] as SelectContactsPage;
			CreateExportFilterPage cefp = WizardForm[3] as CreateExportFilterPage;
			CreateExportProfilePage cepp = WizardForm[2] as CreateExportProfilePage;

			mappings = cepp.SelectedFields;
			contacts = new PropertyItemCriteria(scp.Contacts, cefp.Filters).List();

			// Populate list view with sample data
			if (contacts.Length < sample)
				sample = contacts.Length;

			for (i = 0; i < sample; i++)
			{
				ListViewItem item = new ListViewItem();
				if (null != mappings)
				{
					//Sasa 23.09.2005.
					/* COMMENTED OUT
					foreach (ExportAttributeMapping mapping in mappings)
					{
						string text = String.Empty;

					
						
						if (String.Empty != mapping.AttributeName &&
							ExportAttributeMapping.EmptyColumn != mapping.AttributeName)
							try
							{
								text = contacts[i][mapping.AttributeName].AttributeValue.ToString();
							}
							catch (Exception
	#if DEBUG
								exc
	#endif
								)
							{
	#if DEBUG
								MessageBox.Show(exc.ToString());
	#endif
								text = string.Empty;
							}
					
						if (String.Empty == item.Text)
							item.Text = text;
						else
							item.SubItems.Add(text);

					
					}
					*/

					foreach (ExportAttributeMapping mapping in mappings)
					{
						string text = String.Empty;

						if (String.Empty != mapping.AttributeName &&
							ExportAttributeMapping.EmptyColumn != mapping.AttributeName)
						{		
							//TA++: 02.04.2005.
							//Dirty hack to read manually added properties in propertyItemAttribute hashtable added to propertyItem object							
							if (null != contacts[i][mapping.AttributeName])
							{
								try
								{
                                    if (null != contacts[i][mapping.AttributeName].AttributeValue)
                                        text = contacts[i][mapping.AttributeName].AttributeValue.ToString();
                                    else
                                    {
                                        PropertyItemAttribute pia = (PropertyItemAttribute)contacts[i][mapping.AttributeName];
                                        if (null != pia)
                                        {
                                            text = pia.AttributeName;
                                        }
                                    }
								}
								catch (Exception e)								
								{								
									PropertyItemAttribute pia = (PropertyItemAttribute)contacts[i][mapping.AttributeName];
									if (null != pia)
									{
										text = pia.AttributeName;
									}
								}
							}
							else
							{
								text = string.Empty;
							}
							//TA--: 02.04.2005.
						}
					
						if (String.Empty == item.Text)
							item.Text = text;
						else
							item.SubItems.Add(text);
					}
				

					renameListView.Items.Add(item);
				}
			}
		}

		private void EnteredEventHandler(object sender , EventArgs e)
		{
			renameListView.SuspendLayout();

			renameListView.Items.Clear();

			CreateHeader();
			PopulateList();

			renameListView.ResumeLayout();
			renameListView.Refresh();
		}

		private void ColumnClickedEventHandler(object sender, ColumnClickEventArgs e)
		{
			DumbPInvokeRect rect = new DumbPInvokeRect();
			CreateExportProfilePage cepp = WizardForm[2] as CreateExportProfilePage;

			rect.top = e.Column;
			rect.left = 0;

			/* Do the dumb pinvoke just to get subitem rectangle */
			SendMessage(renameListView.Handle, 0x1000 + 56, 0, ref rect);

			columnEdit.Left = rect.left;
			columnEdit.Top = renameRow.Bounds.Top;
			columnEdit.Height = renameRow.Bounds.Height;
			columnEdit.Width = renameListView.Columns[e.Column].Width;
			columnEdit.Text = cepp.SelectedFields[e.Column].ColumnName;
			columnEdit.Tag = e.Column;
			columnEdit.Visible = true;
			columnEdit.SelectAll();
			columnEdit.Focus();
		}

		private void ColumnEditLeaveEventHandler(object sender, EventArgs e)
		{
			int column = (int)columnEdit.Tag;
			CreateExportProfilePage cepp = WizardForm[2] as CreateExportProfilePage;

			cepp.SelectedFields[column].ColumnName = columnEdit.Text;
			if (0 == column)
				renameRow.Text = columnEdit.Text;
			else
				renameRow.SubItems[column].Text = columnEdit.Text;

			columnEdit.Visible = false;
		}

		public void ColumnEditKeyDown(object sender, KeyEventArgs e)
		{
			switch (e.KeyCode)
			{
				case Keys.Enter:
					columnEdit.Visible = false;
					break;

				case Keys.Escape:
					columnEdit.Text = renameListView.Columns[(int)columnEdit.Tag].Text;
					columnEdit.Visible = false;
					break;
			}
		}
		#region Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.renameInfoLabel = new System.Windows.Forms.Label();
			this.renameListView = new System.Windows.Forms.ListView();
			this.label1 = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// renameInfoLabel
			// 
			this.renameInfoLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.renameInfoLabel.Location = new System.Drawing.Point(0, 0);
			this.renameInfoLabel.Name = "renameInfoLabel";
			this.renameInfoLabel.Size = new System.Drawing.Size(400, 40);
			this.renameInfoLabel.TabIndex = 0;
			this.renameInfoLabel.Text = "This screen allows you to rename the columns accordingly to your export file Rece" +
				"iver requirements. In most cases there will be no special requirements, so you c" +
				"an just click \"next\" to skip this screen.";
			// 
			// renameListView
			// 
			this.renameListView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.renameListView.FullRowSelect = true;
			this.renameListView.HideSelection = false;
			this.renameListView.LabelEdit = true;
			this.renameListView.Location = new System.Drawing.Point(0, 48);
			this.renameListView.MultiSelect = false;
			this.renameListView.Name = "renameListView";
			this.renameListView.Size = new System.Drawing.Size(400, 152);
			this.renameListView.TabIndex = 1;
			this.renameListView.View = System.Windows.Forms.View.Details;
			// 
			// label1
			// 
			this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.label1.Location = new System.Drawing.Point(0, 208);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(416, 16);
			this.label1.TabIndex = 2;
			this.label1.Text = "*Note: the records displayed above are only a sample of records from your group.";
			// 
			// RenameColumnsPage
			// 
			this.Controls.Add(this.label1);
			this.Controls.Add(this.renameListView);
			this.Controls.Add(this.renameInfoLabel);
			this.Name = "RenameColumnsPage";
			this.Size = new System.Drawing.Size(400, 224);
			this.Title = "Rename Column Headers";
			this.ResumeLayout(false);

		}
		#endregion
	}
}
