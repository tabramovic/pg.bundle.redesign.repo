using System;
using System.IO;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using DealMaker.Export;
using DealMaker.Forms.Wizard;

namespace DealMaker.Forms.Export
{
	public class SelectExportLocationPage : DealMaker.Forms.Wizard.WizardPage
	{
		private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog;
		private System.Windows.Forms.Label selectFileLabel;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label nameLabel;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TextBox locationTextBox;
		private System.Windows.Forms.TextBox nameTextBox;
		private System.Windows.Forms.ComboBox typeComboBox;
		private System.Windows.Forms.Button browseButton;
		private System.Windows.Forms.CheckBox emailCheckBox;
		private System.ComponentModel.IContainer components = null;
        private CheckBox _googleDrive;
        private TextBox _spreadSheetTitle;
        private Label label2;
        private TextBox _dataSheetTitle;
        private Label label4;
        private Label label5;
        private TextBox _googleAccount;
        private Label label8;
        private Button _loadGoogleAccounts;
        private CheckBox _localDrive;

        ExportAccounts _accounts;

        //TA++ RQ_0069
        private string originalName = string.Empty;
		//TA-- RQ_0069

		public SelectExportLocationPage()
		{
			// This call is required by the Windows Form Designer.
			InitializeComponent();
		}

		public SelectExportLocationPage(WizardForm wizardForm):
			base(wizardForm)
		{
			// This call is required by the Windows Form Designer.
			InitializeComponent();

			nameTextBox.Leave += new EventHandler(NameTextBoxLeaveEventHandler);
			Entered += new EventHandler(EnteredEventHandler);
			FirstEntered += new EventHandler(FirstEnteredEventHandler);
			CanExit += new CanExitEventHandler(CanExitEventHandler);

			browseButton.Click += new EventHandler(BrowseButtonClickedEventHandler);
            _googleDrive.CheckedChanged += _googleDrive_CheckedChanged;
            _localDrive.CheckedChanged += _localDrive_CheckedChanged;
            _loadGoogleAccounts.Click += _loadGoogleAccounts_Click;
        }

       

        private void _loadGoogleAccounts_Click(object sender, EventArgs e)
        {
            var laForm = new LoadAccounts(_accounts);
            var dr = laForm.ShowDialog();

            if (dr == DialogResult.OK)            
                _googleAccount.Text = laForm.SelectedAddress;
                            
        }

        private void _localDrive_CheckedChanged(object sender, EventArgs e)
        {
            _googleDrive.Checked = !_localDrive.Checked;
            InitSaveLocations();
        }

        private void _googleDrive_CheckedChanged(object sender, EventArgs e)
        {
            _localDrive.Checked = !_googleDrive.Checked;
            InitSaveLocations();
        }

        private void InitSaveLocations()
        {
            InitExportToGoogleDriveOption();
            InitSaveToLocalDriveOption();
        }

        private void InitExportToGoogleDriveOption()
        {
            _googleAccount.Enabled = _dataSheetTitle.Enabled = _spreadSheetTitle.Enabled = _googleDrive.Checked;

            
            if (_googleDrive.Checked)
            {
                IGoogleAccountsHandler googleAccountsHandler = new GoogleAccountsHandler();
                _accounts = googleAccountsHandler.GetGoogleAccounts();
                if (null != _accounts && !string.IsNullOrWhiteSpace(_accounts.LastUsedAccountName))
                {
                    _googleAccount.Text = _accounts.LastUsedAccountName;
                }
                else
                {
                    var smtpSettings = new SMTPObject().LoadSettigns();
                    _googleAccount.Text = smtpSettings?.EmailAddress;
                }
            }
        }

        private void InitSaveToLocalDriveOption()
        {
            locationTextBox.Enabled = browseButton.Enabled = nameTextBox.Enabled = typeComboBox.Enabled  = emailCheckBox.Enabled = _localDrive.Checked;
        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		private void FirstEnteredEventHandler(object sender, EventArgs e)
		{            
			string exportPath = Application.StartupPath + "\\Export";

			if (!Directory.Exists(exportPath))
				Directory.CreateDirectory(exportPath);

			//locationTextBox.Text = exportPath;

			typeComboBox.Items.AddRange(new object[]
				{ new CSVExportWriter(),
			      new TabDelimitedExportWriter()});
			typeComboBox.SelectedIndex = 0;
		}

		private void EnteredEventHandler(object sender, EventArgs e)
		{
			WizardForm.ButtonStyles = WizardFormButtonStyles.HelpButton |
									  WizardFormButtonStyles.BackButton |
									  WizardFormButtonStyles.NextButton |
									  WizardFormButtonStyles.CancelButton;

			//TA++ RQ_0069
			this.nameTextBox.Text = this.originalName;
            //TA-- RQ_0069

            _spreadSheetTitle.Text = String.Empty;
            InitSaveLocations();
        }

		private void NameTextBoxLeaveEventHandler(object sender, EventArgs e)
		{
			//TA++ RQ_0069
			//if (String.Empty == Path.GetExtension(nameTextBox.Text) &&
			//	ExportWriter != null)
			//	nameTextBox.Text += ExportWriter.GetDefaultExtension();
			this.originalName = nameTextBox.Text;
			//TA-- RQ_0069
		}

		private bool CanExitEventHandler(object sender, EventArgs e)
		{
            if (_googleDrive.Checked)
            {
                if (string.IsNullOrWhiteSpace(_googleAccount.Text))
                {
                    MessageBox.Show("Please select Google Account.", Globals.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return false;
                }

                if (string.IsNullOrWhiteSpace(_spreadSheetTitle.Text))
                {
                    MessageBox.Show("Please enter Google Spreadsheet title.", Globals.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return false;
                }

                return true;
            }

			//sasa, 23.09.05: we need warning message if can't exit
/*			return Directory.Exists(locationTextBox.Text) &&
				   nameTextBox.Text != String.Empty &&
				   typeComboBox.Text != String.Empty;
*/
			if(Directory.Exists(locationTextBox.Text) &&
				nameTextBox.Text != String.Empty &&
				typeComboBox.Text != String.Empty)
			{				

				//TA++ RQ_0069				
				nameTextBox.Text = this.originalName + ExportWriter.GetDefaultExtension();
				//TA-- RQ_0069
				return true;
			}
			else
			{
				MessageBox.Show("Name field is empty!");
				return false;
			}
		}

		private void BrowseButtonClickedEventHandler(object sender, EventArgs e)
		{
			folderBrowserDialog.SelectedPath = locationTextBox.Text;

			if (DialogResult.OK == folderBrowserDialog.ShowDialog())
				locationTextBox.Text = folderBrowserDialog.SelectedPath;
		}

		#region Properties

        public string SpreadSheetTitle { get { return !string.IsNullOrEmpty(_spreadSheetTitle.Text) ? _spreadSheetTitle.Text : "Export"; } }
        public string DataSheetTitle { get { return !string.IsNullOrEmpty(_dataSheetTitle.Text) ? _dataSheetTitle.Text : "Sheet1"; } }
		public string ExportFileName
		{
			get
			{
				return locationTextBox.Text + "\\" +
					   nameTextBox.Text;
			}
		}

		public string ExportFileDocument
		{
			get
			{
				return nameTextBox.Text;
			}
		}

		public IExportWriter ExportWriter
		{
			get
			{
				return typeComboBox.SelectedItem as IExportWriter;
			}
		}
		#endregion

		public bool AttachToEmail
		{
			get
			{
				return emailCheckBox.Checked;
			}

		}

		public bool SaveToGoogleDrive => _googleDrive.Checked;

        public string GoogleAccount => _googleAccount.Text;
        

		#region Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.folderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.selectFileLabel = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.locationTextBox = new System.Windows.Forms.TextBox();
            this.nameTextBox = new System.Windows.Forms.TextBox();
            this.nameLabel = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.typeComboBox = new System.Windows.Forms.ComboBox();
            this.browseButton = new System.Windows.Forms.Button();
            this.emailCheckBox = new System.Windows.Forms.CheckBox();
            this._googleDrive = new System.Windows.Forms.CheckBox();
            this._spreadSheetTitle = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this._dataSheetTitle = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this._googleAccount = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this._loadGoogleAccounts = new System.Windows.Forms.Button();
            this._localDrive = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // selectFileLabel
            // 
            this.selectFileLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.selectFileLabel.Location = new System.Drawing.Point(32, 151);
            this.selectFileLabel.Name = "selectFileLabel";
            this.selectFileLabel.Size = new System.Drawing.Size(487, 18);
            this.selectFileLabel.TabIndex = 3;
            this.selectFileLabel.Text = "Please select the location on your computer and name under which you want to save" +
    " your export.";
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(32, 172);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 20);
            this.label1.TabIndex = 1;
            this.label1.Text = "Location:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // locationTextBox
            // 
            this.locationTextBox.Location = new System.Drawing.Point(96, 172);
            this.locationTextBox.Name = "locationTextBox";
            this.locationTextBox.Size = new System.Drawing.Size(258, 20);
            this.locationTextBox.TabIndex = 22;
            // 
            // nameTextBox
            // 
            this.nameTextBox.Location = new System.Drawing.Point(96, 204);
            this.nameTextBox.Name = "nameTextBox";
            this.nameTextBox.Size = new System.Drawing.Size(258, 20);
            this.nameTextBox.TabIndex = 26;
            // 
            // nameLabel
            // 
            this.nameLabel.Location = new System.Drawing.Point(32, 204);
            this.nameLabel.Name = "nameLabel";
            this.nameLabel.Size = new System.Drawing.Size(64, 20);
            this.nameLabel.TabIndex = 4;
            this.nameLabel.Text = "Name:";
            this.nameLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(32, 236);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(64, 20);
            this.label3.TabIndex = 6;
            this.label3.Text = "Type:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // typeComboBox
            // 
            this.typeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.typeComboBox.Location = new System.Drawing.Point(96, 236);
            this.typeComboBox.Name = "typeComboBox";
            this.typeComboBox.Size = new System.Drawing.Size(168, 21);
            this.typeComboBox.TabIndex = 28;
            // 
            // browseButton
            // 
            this.browseButton.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.browseButton.Location = new System.Drawing.Point(360, 172);
            this.browseButton.Name = "browseButton";
            this.browseButton.Size = new System.Drawing.Size(24, 20);
            this.browseButton.TabIndex = 24;
            this.browseButton.Text = "...";
            // 
            // emailCheckBox
            // 
            this.emailCheckBox.Location = new System.Drawing.Point(96, 268);
            this.emailCheckBox.Name = "emailCheckBox";
            this.emailCheckBox.Size = new System.Drawing.Size(104, 24);
            this.emailCheckBox.TabIndex = 30;
            this.emailCheckBox.Text = "E-mail";
            // 
            // _googleDrive
            // 
            this._googleDrive.Checked = true;
            this._googleDrive.CheckState = System.Windows.Forms.CheckState.Checked;
            this._googleDrive.Location = new System.Drawing.Point(7, 3);
            this._googleDrive.Name = "_googleDrive";
            this._googleDrive.Size = new System.Drawing.Size(131, 24);
            this._googleDrive.TabIndex = 10;
            this._googleDrive.Text = "Save to Google Drive";
            // 
            // _spreadSheetTitle
            // 
            this._spreadSheetTitle.Location = new System.Drawing.Point(138, 57);
            this._spreadSheetTitle.Name = "_spreadSheetTitle";
            this._spreadSheetTitle.Size = new System.Drawing.Size(216, 20);
            this._spreadSheetTitle.TabIndex = 14;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(32, 57);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(106, 20);
            this.label2.TabIndex = 11;
            this.label2.Text = "Spreadsheet Title:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // _dataSheetTitle
            // 
            this._dataSheetTitle.Location = new System.Drawing.Point(443, 80);
            this._dataSheetTitle.Name = "_dataSheetTitle";
            this._dataSheetTitle.ReadOnly = true;
            this._dataSheetTitle.Size = new System.Drawing.Size(61, 20);
            this._dataSheetTitle.TabIndex = 16;
            this._dataSheetTitle.Text = "Sheet1";
            this._dataSheetTitle.Visible = false;
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(400, 54);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(104, 20);
            this.label4.TabIndex = 13;
            this.label4.Text = "Datasheet Title:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label4.Visible = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(32, 87);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(405, 13);
            this.label5.TabIndex = 15;
            this.label5.Text = "Save to Google Drive is generally used for using a 3rd party email tool like Gmas" +
    "s.co.";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // _googleAccount
            // 
            this._googleAccount.Location = new System.Drawing.Point(138, 31);
            this._googleAccount.Name = "_googleAccount";
            this._googleAccount.ReadOnly = true;
            this._googleAccount.Size = new System.Drawing.Size(216, 20);
            this._googleAccount.TabIndex = 12;
            // 
            // label8
            // 
            this.label8.Location = new System.Drawing.Point(32, 30);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(106, 20);
            this.label8.TabIndex = 19;
            this.label8.Text = "Google Account;";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // _loadGoogleAccounts
            // 
            this._loadGoogleAccounts.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._loadGoogleAccounts.Location = new System.Drawing.Point(360, 31);
            this._loadGoogleAccounts.Name = "_loadGoogleAccounts";
            this._loadGoogleAccounts.Size = new System.Drawing.Size(144, 20);
            this._loadGoogleAccounts.TabIndex = 13;
            this._loadGoogleAccounts.Text = "Select a Google Account";
            // 
            // _localDrive
            // 
            this._localDrive.Location = new System.Drawing.Point(7, 124);
            this._localDrive.Name = "_localDrive";
            this._localDrive.Size = new System.Drawing.Size(193, 24);
            this._localDrive.TabIndex = 20;
            this._localDrive.Text = "Save to Local Drive and Email";
            // 
            // SelectExportLocationPage
            // 
            this.Controls.Add(this._localDrive);
            this.Controls.Add(this._loadGoogleAccounts);
            this.Controls.Add(this._googleAccount);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label5);
            this.Controls.Add(this._dataSheetTitle);
            this.Controls.Add(this.label4);
            this.Controls.Add(this._spreadSheetTitle);
            this.Controls.Add(this.label2);
            this.Controls.Add(this._googleDrive);
            this.Controls.Add(this.emailCheckBox);
            this.Controls.Add(this.browseButton);
            this.Controls.Add(this.typeComboBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.nameTextBox);
            this.Controls.Add(this.nameLabel);
            this.Controls.Add(this.locationTextBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.selectFileLabel);
            this.Name = "SelectExportLocationPage";
            this.Size = new System.Drawing.Size(525, 300);
            this.Title = "Select Export Location";
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		#endregion
	}
}

