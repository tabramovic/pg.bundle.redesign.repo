using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using DealMaker;
using DealMaker.Export;
using DealMaker.Filters;
using DealMaker.Forms.Wizard;

namespace DealMaker.Forms.Export
{
	public class CreateExportFilterPage : DealMaker.Forms.Wizard.WizardPage
	{
		SimpleFilter[] simpleFilters = null;
		MathematicalFilter[] mathFilters = null; 
		ArrayList mathFilterViews = new ArrayList();
		ArrayList simpleFilterViews = new ArrayList();

		private System.Windows.Forms.Label skipInfoLabel;
		private System.Windows.Forms.Label simpleFilterLabel;
		private System.Windows.Forms.Label simpleFilterInstructionsLabel;
		private System.Windows.Forms.Label dataColumnLabel;
		private System.Windows.Forms.ComboBox dataColumnComboBox0;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.ComboBox opComboBox;
		private System.Windows.Forms.ComboBox dataColumnComboBox1;
		private System.Windows.Forms.ComboBox dataColumnComboBox2;
		private System.Windows.Forms.ComboBox dataColumnComboBox3;
		private System.Windows.Forms.ComboBox conditionComboBox3;
		private System.Windows.Forms.ComboBox conditionComboBox2;
		private System.Windows.Forms.ComboBox conditionComboBox1;
		private System.Windows.Forms.ComboBox conditionComboBox0;
		private System.Windows.Forms.Label simpleFilterConditionLabel;
		private System.Windows.Forms.Label simpleFilterValueLabel;
		private System.Windows.Forms.Label dataColumn2Label;
		private System.Windows.Forms.Label dataColumn1Label;
		private System.Windows.Forms.Label mathematicalFilterInfoLabel;
		private System.Windows.Forms.Label mathematicalFilterLabel;
		private System.Windows.Forms.Label mathematicalFilterValueLabel;
		private System.Windows.Forms.Label mathematicalFilterConditionLabel;
		private System.Windows.Forms.ComboBox mathDataColumnComboBox1;
		private System.Windows.Forms.ComboBox mathDataColumnComboBox0;
		private System.Windows.Forms.ComboBox mathConditionComboBox;
		private System.Windows.Forms.TextBox valueTextBox0;
		private System.Windows.Forms.TextBox valueTextBox1;
		private System.Windows.Forms.TextBox valueTextBox2;
		private System.Windows.Forms.TextBox valueTextBox3;
		private System.Windows.Forms.TextBox mathValueTextBox;
		private System.ComponentModel.IContainer components = null;

		internal class SimpleFilterView
		{
			ComboBox dataColumnComboBox;
			ComboBox conditionComboBox;
			TextBox valueTextBox;

			public SimpleFilterView(
				ComboBox dataColumnComboBox,
				ComboBox conditionComboBox,
				TextBox valueTextBox)
			{
				this.dataColumnComboBox = dataColumnComboBox;
				this.conditionComboBox = conditionComboBox;
				this.valueTextBox = valueTextBox;
			}

			#region Properties
			public SimpleFilter Filter
			{
				get
				{
					if (dataColumnComboBox.Text == String.Empty ||
						conditionComboBox.Text == String.Empty ||
						valueTextBox.Text == String.Empty)
						return null;
					
					try
					{
						return new SimpleFilter(
							dataColumnComboBox.Text,
							conditionComboBox.Text,
							Double.Parse(valueTextBox.Text));
					}
					catch
					{
						return null;
					}	
				}
			}
			public ComboBox DataColumnComboBox
			{
				get { return dataColumnComboBox; }
			}

			public ComboBox ConditionComboBox
			{
				get { return conditionComboBox; }
			}

			public TextBox ValueTextBox
			{
				get { return valueTextBox; }
			}
			#endregion
		}

		internal class MathFilterView
		{
			ComboBox opComboBox;
			TextBox valueTextBox;
			ComboBox conditionComboBox;
			ComboBox dataColumn1ComboBox;
			ComboBox dataColumn2ComboBox;

			public MathFilterView(
				ComboBox dataColumn1ComboBox,				
				ComboBox opComboBox, 
				ComboBox dataColumn2ComboBox,
				ComboBox conditionComboBox,
				TextBox valueTextBox)
			{
				this.opComboBox = opComboBox;
				this.valueTextBox = valueTextBox;
				this.conditionComboBox = conditionComboBox;
				this.dataColumn1ComboBox = dataColumn1ComboBox;
				this.dataColumn2ComboBox = dataColumn2ComboBox;
			}

			#region Properties
			public ComboBox OpComboBox
			{
				get { return opComboBox; }
			}

			public TextBox ValueTextBox
			{
				get { return valueTextBox; }
			}

			public ComboBox ConditionComboBox
			{
				get { return conditionComboBox; }
			}

			public ComboBox DataColumn1ComboBox
			{
				get { return dataColumn1ComboBox; }
			}

			public ComboBox DataColumn2ComboBox
			{
				get { return dataColumn2ComboBox; }
			}

			public MathematicalFilter Filter
			{
				get 
				{
					if (DataColumn1ComboBox.Text == String.Empty ||
						OpComboBox.Text == String.Empty ||
						DataColumn2ComboBox.Text == String.Empty ||
						ConditionComboBox.Text == String.Empty ||
						valueTextBox.Text == String.Empty)
					{
						return null;
					}

					try
					{
						return new MathematicalFilter(
							DataColumn1ComboBox.Text,
							DataColumn2ComboBox.Text,
							OpComboBox.Text,
							ConditionComboBox.Text,
							Double.Parse(ValueTextBox.Text));
					}
					catch
					{
						return null;
					}
				}
			}
			#endregion 
				
		}

		public CreateExportFilterPage()
		{
			// This call is required by the Windows Form Designer.
			InitializeComponent();
		}

		public CreateExportFilterPage(WizardForm wizardForm): base(wizardForm)
		{
			// This call is required by the Windows Form Designer.
			InitializeComponent();

			simpleFilterViews.Add(
				new SimpleFilterView(dataColumnComboBox0, conditionComboBox0, valueTextBox0));
			simpleFilterViews.Add(
				new SimpleFilterView(dataColumnComboBox1, conditionComboBox1, valueTextBox1));
			simpleFilterViews.Add(
				new SimpleFilterView(dataColumnComboBox2, conditionComboBox2, valueTextBox2));
			simpleFilterViews.Add(
				new SimpleFilterView(dataColumnComboBox3, conditionComboBox3, valueTextBox3));
			
			mathFilterViews.Add(
				new MathFilterView(mathDataColumnComboBox0, opComboBox, mathDataColumnComboBox1, mathConditionComboBox, mathValueTextBox));

			Entered += new EventHandler(EnteredEventHandler);
			Exited += new EventHandler(ExitedEventHandler);
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		private void EnteredEventHandler(object sender, EventArgs e)
		{
			CreateExportProfilePage cepp = WizardForm[2] as CreateExportProfilePage;

			ArrayList selectedNumericalFields = new ArrayList();

			//sasa, 24.08.2005: filter properties, for now add only value types
			//should be only numerical, first to see is this good enough
			//sasa, 26.08.2005: changed selection to int, float i double
			foreach (ExportAttributeMapping mapping in cepp.SelectedFields)
			{
				try
				{
					if(mapping.AttributeType.Length == 0) continue;
					Type type = Type.GetType(mapping.AttributeType);
					//if(Type.GetType(mapping.AttributeType).IsValueType)
					if(type == typeof(int) || type == typeof(float) || type == typeof(double))
					{
						selectedNumericalFields.Add(mapping);
					}
				}
				catch (Exception 
#if DEBUG 
					exc
#endif
					)
				{
#if DEBUG 
					MessageBox.Show(exc.Message + System.Environment.NewLine + exc.InnerException);
#endif

				}
			}

			foreach (SimpleFilterView sfv in simpleFilterViews)
			{
//				cepp.SelectedFields[0].
				sfv.DataColumnComboBox.Items.Clear();
				//sfv.DataColumnComboBox.Items.AddRange(cepp.SelectedFields);

				sfv.DataColumnComboBox.Items.AddRange(selectedNumericalFields.ToArray());

				sfv.ConditionComboBox.Items.Clear();
				sfv.ConditionComboBox.Items.AddRange(Globals.FilterConditions);
			}

			if (simpleFilters != null)
				SimpleFilters = simpleFilters;
			
			// ihuk, 06.01.2005
			// note that for now there is only one math filter but 
			// maybe one day there will be more of them
			foreach (MathFilterView mfv in mathFilterViews)
			{
				mfv.DataColumn1ComboBox.Items.Clear();
				mfv.DataColumn2ComboBox.Items.Clear();
				mfv.ConditionComboBox.Items.Clear();
				mfv.ConditionComboBox.Items.AddRange(Globals.FilterConditions);
				mfv.OpComboBox.Items.Clear();
				mfv.OpComboBox.Items.AddRange(Globals.MathematicalFilterOperations);

				foreach (ExportAttributeMapping mapping in cepp.SelectedFields)
				{
				
					if (mapping.AttributeType == String.Empty ||
						mapping.AttributeType == typeof(System.UInt16).ToString() ||
						mapping.AttributeType == typeof(System.UInt32).ToString() ||
						mapping.AttributeType == typeof(System.Double).ToString())
					{
					
						mfv.DataColumn1ComboBox.Items.Add(mapping);
						mfv.DataColumn2ComboBox.Items.Add(mapping);
					}
				}				
			}

			if (mathFilters != null)
				MathFilters = mathFilters;
		}

		private void ExitedEventHandler(object sender, EventArgs e)
		{
			ArrayList sfs = new ArrayList();
			ArrayList mfs = new ArrayList();

			foreach (SimpleFilterView sfv in simpleFilterViews)
			{
				if (null != sfv.Filter)
					sfs.Add(sfv.Filter);
			}

			foreach (MathFilterView mfv in mathFilterViews)
			{
				if (null != mfv.Filter)
					mfs.Add(mfv.Filter);
			}

			simpleFilters = (SimpleFilter[])sfs.ToArray(typeof(SimpleFilter));
			mathFilters = (MathematicalFilter[])mfs.ToArray(typeof(MathematicalFilter));
		}

		#region Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.skipInfoLabel = new System.Windows.Forms.Label();
			this.simpleFilterLabel = new System.Windows.Forms.Label();
			this.simpleFilterInstructionsLabel = new System.Windows.Forms.Label();
			this.dataColumnLabel = new System.Windows.Forms.Label();
			this.dataColumnComboBox0 = new System.Windows.Forms.ComboBox();
			this.dataColumnComboBox1 = new System.Windows.Forms.ComboBox();
			this.dataColumnComboBox2 = new System.Windows.Forms.ComboBox();
			this.dataColumnComboBox3 = new System.Windows.Forms.ComboBox();
			this.conditionComboBox3 = new System.Windows.Forms.ComboBox();
			this.conditionComboBox2 = new System.Windows.Forms.ComboBox();
			this.conditionComboBox1 = new System.Windows.Forms.ComboBox();
			this.conditionComboBox0 = new System.Windows.Forms.ComboBox();
			this.simpleFilterConditionLabel = new System.Windows.Forms.Label();
			this.simpleFilterValueLabel = new System.Windows.Forms.Label();
			this.valueTextBox0 = new System.Windows.Forms.TextBox();
			this.valueTextBox1 = new System.Windows.Forms.TextBox();
			this.valueTextBox2 = new System.Windows.Forms.TextBox();
			this.valueTextBox3 = new System.Windows.Forms.TextBox();
			this.mathValueTextBox = new System.Windows.Forms.TextBox();
			this.mathDataColumnComboBox1 = new System.Windows.Forms.ComboBox();
			this.dataColumn2Label = new System.Windows.Forms.Label();
			this.mathDataColumnComboBox0 = new System.Windows.Forms.ComboBox();
			this.dataColumn1Label = new System.Windows.Forms.Label();
			this.mathematicalFilterInfoLabel = new System.Windows.Forms.Label();
			this.mathematicalFilterLabel = new System.Windows.Forms.Label();
			this.label8 = new System.Windows.Forms.Label();
			this.mathConditionComboBox = new System.Windows.Forms.ComboBox();
			this.opComboBox = new System.Windows.Forms.ComboBox();
			this.label3 = new System.Windows.Forms.Label();
			this.mathematicalFilterValueLabel = new System.Windows.Forms.Label();
			this.mathematicalFilterConditionLabel = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// skipInfoLabel
			// 
			this.skipInfoLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.skipInfoLabel.Location = new System.Drawing.Point(0, 0);
			this.skipInfoLabel.Name = "skipInfoLabel";
			this.skipInfoLabel.Size = new System.Drawing.Size(528, 16);
			this.skipInfoLabel.TabIndex = 0;
			this.skipInfoLabel.Text = "Skip this step if you want to export all the selected records!";
			// 
			// simpleFilterLabel
			// 
			this.simpleFilterLabel.Location = new System.Drawing.Point(0, 24);
			this.simpleFilterLabel.Name = "simpleFilterLabel";
			this.simpleFilterLabel.Size = new System.Drawing.Size(72, 16);
			this.simpleFilterLabel.TabIndex = 1;
			this.simpleFilterLabel.Text = "Simple filter:";
			// 
			// simpleFilterInstructionsLabel
			// 
			this.simpleFilterInstructionsLabel.Location = new System.Drawing.Point(16, 40);
			this.simpleFilterInstructionsLabel.Name = "simpleFilterInstructionsLabel";
			this.simpleFilterInstructionsLabel.Size = new System.Drawing.Size(368, 24);
			this.simpleFilterInstructionsLabel.TabIndex = 2;
			this.simpleFilterInstructionsLabel.Text = "Choose the data from the drop down menu that you want the condition to apply to, " +
				"then choose the condition and its value.";
			// 
			// dataColumnLabel
			// 
			this.dataColumnLabel.Location = new System.Drawing.Point(16, 72);
			this.dataColumnLabel.Name = "dataColumnLabel";
			this.dataColumnLabel.Size = new System.Drawing.Size(72, 16);
			this.dataColumnLabel.TabIndex = 3;
			this.dataColumnLabel.Text = "Data Column";
			// 
			// dataColumnComboBox0
			// 
			this.dataColumnComboBox0.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.dataColumnComboBox0.Location = new System.Drawing.Point(16, 88);
			this.dataColumnComboBox0.Name = "dataColumnComboBox0";
			this.dataColumnComboBox0.Size = new System.Drawing.Size(121, 21);
			this.dataColumnComboBox0.TabIndex = 6;
			// 
			// dataColumnComboBox1
			// 
			this.dataColumnComboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.dataColumnComboBox1.Location = new System.Drawing.Point(16, 112);
			this.dataColumnComboBox1.Name = "dataColumnComboBox1";
			this.dataColumnComboBox1.Size = new System.Drawing.Size(121, 21);
			this.dataColumnComboBox1.TabIndex = 9;
			// 
			// dataColumnComboBox2
			// 
			this.dataColumnComboBox2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.dataColumnComboBox2.Location = new System.Drawing.Point(16, 136);
			this.dataColumnComboBox2.Name = "dataColumnComboBox2";
			this.dataColumnComboBox2.Size = new System.Drawing.Size(121, 21);
			this.dataColumnComboBox2.TabIndex = 12;
			// 
			// dataColumnComboBox3
			// 
			this.dataColumnComboBox3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.dataColumnComboBox3.Location = new System.Drawing.Point(16, 160);
			this.dataColumnComboBox3.Name = "dataColumnComboBox3";
			this.dataColumnComboBox3.Size = new System.Drawing.Size(121, 21);
			this.dataColumnComboBox3.TabIndex = 15;
			// 
			// conditionComboBox3
			// 
			this.conditionComboBox3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.conditionComboBox3.Location = new System.Drawing.Point(144, 160);
			this.conditionComboBox3.Name = "conditionComboBox3";
			this.conditionComboBox3.Size = new System.Drawing.Size(121, 21);
			this.conditionComboBox3.TabIndex = 16;
			// 
			// conditionComboBox2
			// 
			this.conditionComboBox2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.conditionComboBox2.Location = new System.Drawing.Point(144, 136);
			this.conditionComboBox2.Name = "conditionComboBox2";
			this.conditionComboBox2.Size = new System.Drawing.Size(121, 21);
			this.conditionComboBox2.TabIndex = 13;
			// 
			// conditionComboBox1
			// 
			this.conditionComboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.conditionComboBox1.Location = new System.Drawing.Point(144, 112);
			this.conditionComboBox1.Name = "conditionComboBox1";
			this.conditionComboBox1.Size = new System.Drawing.Size(121, 21);
			this.conditionComboBox1.TabIndex = 10;
			// 
			// conditionComboBox0
			// 
			this.conditionComboBox0.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.conditionComboBox0.Location = new System.Drawing.Point(144, 88);
			this.conditionComboBox0.Name = "conditionComboBox0";
			this.conditionComboBox0.Size = new System.Drawing.Size(121, 21);
			this.conditionComboBox0.TabIndex = 7;
			// 
			// simpleFilterConditionLabel
			// 
			this.simpleFilterConditionLabel.Location = new System.Drawing.Point(144, 72);
			this.simpleFilterConditionLabel.Name = "simpleFilterConditionLabel";
			this.simpleFilterConditionLabel.Size = new System.Drawing.Size(72, 16);
			this.simpleFilterConditionLabel.TabIndex = 4;
			this.simpleFilterConditionLabel.Text = "Condition";
			// 
			// simpleFilterValueLabel
			// 
			this.simpleFilterValueLabel.Location = new System.Drawing.Point(272, 72);
			this.simpleFilterValueLabel.Name = "simpleFilterValueLabel";
			this.simpleFilterValueLabel.Size = new System.Drawing.Size(72, 16);
			this.simpleFilterValueLabel.TabIndex = 5;
			this.simpleFilterValueLabel.Text = "Value";
			// 
			// valueTextBox0
			// 
			this.valueTextBox0.Location = new System.Drawing.Point(272, 88);
			this.valueTextBox0.Name = "valueTextBox0";
			this.valueTextBox0.TabIndex = 8;
			this.valueTextBox0.Text = "";
			this.valueTextBox0.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// valueTextBox1
			// 
			this.valueTextBox1.Location = new System.Drawing.Point(272, 112);
			this.valueTextBox1.Name = "valueTextBox1";
			this.valueTextBox1.TabIndex = 11;
			this.valueTextBox1.Text = "";
			this.valueTextBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// valueTextBox2
			// 
			this.valueTextBox2.Location = new System.Drawing.Point(272, 136);
			this.valueTextBox2.Name = "valueTextBox2";
			this.valueTextBox2.TabIndex = 14;
			this.valueTextBox2.Text = "";
			this.valueTextBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// valueTextBox3
			// 
			this.valueTextBox3.Location = new System.Drawing.Point(272, 160);
			this.valueTextBox3.Name = "valueTextBox3";
			this.valueTextBox3.TabIndex = 17;
			this.valueTextBox3.Text = "";
			this.valueTextBox3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// mathValueTextBox
			// 
			this.mathValueTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.mathValueTextBox.Location = new System.Drawing.Point(480, 272);
			this.mathValueTextBox.Name = "mathValueTextBox";
			this.mathValueTextBox.Size = new System.Drawing.Size(48, 20);
			this.mathValueTextBox.TabIndex = 29;
			this.mathValueTextBox.Text = "";
			this.mathValueTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// mathDataColumnComboBox1
			// 
			this.mathDataColumnComboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.mathDataColumnComboBox1.Location = new System.Drawing.Point(200, 272);
			this.mathDataColumnComboBox1.Name = "mathDataColumnComboBox1";
			this.mathDataColumnComboBox1.Size = new System.Drawing.Size(112, 21);
			this.mathDataColumnComboBox1.TabIndex = 26;
			// 
			// dataColumn2Label
			// 
			this.dataColumn2Label.Location = new System.Drawing.Point(200, 256);
			this.dataColumn2Label.Name = "dataColumn2Label";
			this.dataColumn2Label.Size = new System.Drawing.Size(88, 16);
			this.dataColumn2Label.TabIndex = 21;
			this.dataColumn2Label.Text = "Data Column #2";
			// 
			// mathDataColumnComboBox0
			// 
			this.mathDataColumnComboBox0.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.mathDataColumnComboBox0.Location = new System.Drawing.Point(16, 272);
			this.mathDataColumnComboBox0.Name = "mathDataColumnComboBox0";
			this.mathDataColumnComboBox0.Size = new System.Drawing.Size(112, 21);
			this.mathDataColumnComboBox0.TabIndex = 24;
			// 
			// dataColumn1Label
			// 
			this.dataColumn1Label.Location = new System.Drawing.Point(16, 256);
			this.dataColumn1Label.Name = "dataColumn1Label";
			this.dataColumn1Label.Size = new System.Drawing.Size(88, 16);
			this.dataColumn1Label.TabIndex = 20;
			this.dataColumn1Label.Text = "Data Column #1";
			// 
			// mathematicalFilterInfoLabel
			// 
			this.mathematicalFilterInfoLabel.Location = new System.Drawing.Point(16, 208);
			this.mathematicalFilterInfoLabel.Name = "mathematicalFilterInfoLabel";
			this.mathematicalFilterInfoLabel.Size = new System.Drawing.Size(368, 40);
			this.mathematicalFilterInfoLabel.TabIndex = 19;
			this.mathematicalFilterInfoLabel.Text = "Mathematical Filter allows you to apply \"add\" or \"substract\" function to 2 column" +
				"s of your choice, and then put  a condition on the result of the applied mathema" +
				"tical function.";
			// 
			// mathematicalFilterLabel
			// 
			this.mathematicalFilterLabel.Location = new System.Drawing.Point(0, 192);
			this.mathematicalFilterLabel.Name = "mathematicalFilterLabel";
			this.mathematicalFilterLabel.Size = new System.Drawing.Size(136, 16);
			this.mathematicalFilterLabel.TabIndex = 18;
			this.mathematicalFilterLabel.Text = "Mathematical Filter:";
			// 
			// label8
			// 
			this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(238)));
			this.label8.Location = new System.Drawing.Point(0, 304);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(528, 32);
			this.label8.TabIndex = 30;
			this.label8.Text = "ATTENTION: only data that meet ALL the above conditions will be exported!";
			// 
			// mathConditionComboBox
			// 
			this.mathConditionComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.mathConditionComboBox.Location = new System.Drawing.Point(360, 272);
			this.mathConditionComboBox.Name = "mathConditionComboBox";
			this.mathConditionComboBox.Size = new System.Drawing.Size(112, 21);
			this.mathConditionComboBox.TabIndex = 28;
			// 
			// opComboBox
			// 
			this.opComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.opComboBox.Location = new System.Drawing.Point(136, 272);
			this.opComboBox.Name = "opComboBox";
			this.opComboBox.Size = new System.Drawing.Size(56, 21);
			this.opComboBox.TabIndex = 25;
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(320, 272);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(40, 11);
			this.label3.TabIndex = 27;
			this.label3.Text = "Result:";
			// 
			// mathematicalFilterValueLabel
			// 
			this.mathematicalFilterValueLabel.Location = new System.Drawing.Point(480, 256);
			this.mathematicalFilterValueLabel.Name = "mathematicalFilterValueLabel";
			this.mathematicalFilterValueLabel.Size = new System.Drawing.Size(40, 16);
			this.mathematicalFilterValueLabel.TabIndex = 23;
			this.mathematicalFilterValueLabel.Text = "Value";
			// 
			// mathematicalFilterConditionLabel
			// 
			this.mathematicalFilterConditionLabel.Location = new System.Drawing.Point(360, 256);
			this.mathematicalFilterConditionLabel.Name = "mathematicalFilterConditionLabel";
			this.mathematicalFilterConditionLabel.Size = new System.Drawing.Size(72, 16);
			this.mathematicalFilterConditionLabel.TabIndex = 22;
			this.mathematicalFilterConditionLabel.Text = "Condition";
			// 
			// CreateExportFilterPage
			// 
			this.Controls.Add(this.mathematicalFilterConditionLabel);
			this.Controls.Add(this.mathematicalFilterValueLabel);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.opComboBox);
			this.Controls.Add(this.mathConditionComboBox);
			this.Controls.Add(this.label8);
			this.Controls.Add(this.mathValueTextBox);
			this.Controls.Add(this.mathDataColumnComboBox1);
			this.Controls.Add(this.dataColumn2Label);
			this.Controls.Add(this.mathDataColumnComboBox0);
			this.Controls.Add(this.dataColumn1Label);
			this.Controls.Add(this.mathematicalFilterInfoLabel);
			this.Controls.Add(this.mathematicalFilterLabel);
			this.Controls.Add(this.valueTextBox3);
			this.Controls.Add(this.valueTextBox2);
			this.Controls.Add(this.valueTextBox1);
			this.Controls.Add(this.valueTextBox0);
			this.Controls.Add(this.simpleFilterValueLabel);
			this.Controls.Add(this.conditionComboBox3);
			this.Controls.Add(this.conditionComboBox2);
			this.Controls.Add(this.conditionComboBox1);
			this.Controls.Add(this.conditionComboBox0);
			this.Controls.Add(this.simpleFilterConditionLabel);
			this.Controls.Add(this.dataColumnComboBox3);
			this.Controls.Add(this.dataColumnComboBox2);
			this.Controls.Add(this.dataColumnComboBox1);
			this.Controls.Add(this.dataColumnComboBox0);
			this.Controls.Add(this.dataColumnLabel);
			this.Controls.Add(this.simpleFilterInstructionsLabel);
			this.Controls.Add(this.simpleFilterLabel);
			this.Controls.Add(this.skipInfoLabel);
			this.Name = "CreateExportFilterPage";
			this.Size = new System.Drawing.Size(528, 320);
			this.Title = "Create Export Filter";
			this.ResumeLayout(false);

		}
		#endregion

		#region Properties
		public SimpleFilter[] SimpleFilters
		{
			get 
			{ 
				return simpleFilters; 
			}
			set
			{
				int i;
				int count = simpleFilterViews.Count;

				if (value == null)
					return;

				if (value.Length < count)
					count = value.Length;

				for (i = 0; i < count; i++)
				{
					SimpleFilterView sfv = simpleFilterViews[i] as SimpleFilterView;

					sfv.DataColumnComboBox.Text = value[i].DataColumn;
					sfv.ConditionComboBox.Text = value[i].Condition;
					sfv.ValueTextBox.Text = value[i].ValueDefined.ToString();
				}

				simpleFilters = value;
			}
		}

		public MathematicalFilter[] MathFilters
		{
			get 
			{ 
				return mathFilters; 
			}
			set
			{
				int i;
				int count = mathFilterViews.Count;

				if (value == null)
					return;

				if (value.Length < count)
					count = value.Length;

				for (i = 0; i < count; i++)
				{
					MathFilterView mfv = mathFilterViews[i] as MathFilterView;

					mfv.OpComboBox.Text = value[i].DefinedOperator;
					mfv.DataColumn1ComboBox.Text = value[i].DataColumn1;
					mfv.DataColumn2ComboBox.Text = value[i].DataColumn2;
					mfv.ConditionComboBox.Text = value[i].ResultCondition;
					mfv.ValueTextBox.Text = value[i].DefinedValue.ToString();
				}

				mathFilters = value;
			}
		}

		public IPropertyItemFilter[] Filters
		{
			get
			{
				ArrayList filters = new ArrayList();
				SelectContactsPage scp = WizardForm[0] as SelectContactsPage;

				filters.AddRange(simpleFilters);
				filters.AddRange(mathFilters);

				if (scp.EliminateDoNotMailEntries)
					filters.Add(new GroupMembershipFilter(21, false));

				return (IPropertyItemFilter[])filters.ToArray(typeof(IPropertyItemFilter));
			}
		}
		#endregion
	}
}


