﻿namespace DealMaker.Forms.Export
{
    partial class LoadAccounts
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._firstEmail = new WindowsApplication.AcquisitionCalculator.TextBox();
            this._secondEmail = new WindowsApplication.AcquisitionCalculator.TextBox();
            this._thirdEmail = new WindowsApplication.AcquisitionCalculator.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this._save = new System.Windows.Forms.Button();
            this._useFirst = new System.Windows.Forms.Button();
            this._useSecond = new System.Windows.Forms.Button();
            this._useThird = new System.Windows.Forms.Button();
            this._dontSave = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // _firstEmail
            // 
            this._firstEmail.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._firstEmail.Location = new System.Drawing.Point(23, 92);
            this._firstEmail.Name = "_firstEmail";
            this._firstEmail.Size = new System.Drawing.Size(319, 20);
            this._firstEmail.TabIndex = 0;
            // 
            // _secondEmail
            // 
            this._secondEmail.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._secondEmail.Location = new System.Drawing.Point(23, 157);
            this._secondEmail.Name = "_secondEmail";
            this._secondEmail.Size = new System.Drawing.Size(319, 20);
            this._secondEmail.TabIndex = 2;
            // 
            // _thirdEmail
            // 
            this._thirdEmail.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._thirdEmail.Location = new System.Drawing.Point(23, 224);
            this._thirdEmail.Name = "_thirdEmail";
            this._thirdEmail.Size = new System.Drawing.Size(319, 20);
            this._thirdEmail.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 61);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(93, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Google Account 1";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(11, 132);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(93, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Google Account 2";
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(11, 196);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(93, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Google Account 3";
            // 
            // _save
            // 
            this._save.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._save.DialogResult = System.Windows.Forms.DialogResult.OK;
            this._save.Location = new System.Drawing.Point(348, 271);
            this._save.Name = "_save";
            this._save.Size = new System.Drawing.Size(75, 23);
            this._save.TabIndex = 6;
            this._save.Text = "Save";
            this._save.UseVisualStyleBackColor = true;
            // 
            // _useFirst
            // 
            this._useFirst.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._useFirst.Location = new System.Drawing.Point(348, 90);
            this._useFirst.Name = "_useFirst";
            this._useFirst.Size = new System.Drawing.Size(75, 23);
            this._useFirst.TabIndex = 1;
            this._useFirst.Text = "Select";
            this._useFirst.UseVisualStyleBackColor = true;
            // 
            // _useSecond
            // 
            this._useSecond.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._useSecond.Location = new System.Drawing.Point(348, 155);
            this._useSecond.Name = "_useSecond";
            this._useSecond.Size = new System.Drawing.Size(75, 23);
            this._useSecond.TabIndex = 3;
            this._useSecond.Text = "Select";
            this._useSecond.UseVisualStyleBackColor = true;
            // 
            // _useThird
            // 
            this._useThird.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._useThird.Location = new System.Drawing.Point(348, 222);
            this._useThird.Name = "_useThird";
            this._useThird.Size = new System.Drawing.Size(75, 23);
            this._useThird.TabIndex = 5;
            this._useThird.Text = "Select";
            this._useThird.UseVisualStyleBackColor = true;
            // 
            // _dontSave
            // 
            this._dontSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._dontSave.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this._dontSave.Location = new System.Drawing.Point(267, 271);
            this._dontSave.Name = "_dontSave";
            this._dontSave.Size = new System.Drawing.Size(75, 23);
            this._dontSave.TabIndex = 7;
            this._dontSave.Text = "Don\'t Save";
            this._dontSave.UseVisualStyleBackColor = true;
            this._dontSave.Visible = false;
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 21);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(403, 13);
            this.label4.TabIndex = 12;
            this.label4.Text = "Enter up to 3 Google accounts and click Select to choose the account to upload to" +
    ".";
            // 
            // LoadAccounts
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.ClientSize = new System.Drawing.Size(436, 306);
            this.Controls.Add(this.label4);
            this.Controls.Add(this._dontSave);
            this.Controls.Add(this._useThird);
            this.Controls.Add(this._useSecond);
            this.Controls.Add(this._useFirst);
            this.Controls.Add(this._save);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this._thirdEmail);
            this.Controls.Add(this._secondEmail);
            this.Controls.Add(this._firstEmail);
            this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "LoadAccounts";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Select Google Account";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private WindowsApplication.AcquisitionCalculator.TextBox _firstEmail;
        private WindowsApplication.AcquisitionCalculator.TextBox _secondEmail;
        private WindowsApplication.AcquisitionCalculator.TextBox _thirdEmail;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button _save;
        private System.Windows.Forms.Button _useFirst;
        private System.Windows.Forms.Button _useSecond;
        private System.Windows.Forms.Button _useThird;
        private System.Windows.Forms.Button _dontSave;
        private System.Windows.Forms.Label label4;
    }
}