using System;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using DealMaker.Export;
using DealMaker.Forms.Wizard;
using DealMaker.PlainClasses.Utils;

//PROFILE MANAGER

namespace DealMaker.Forms.Export
{
	public class SelectExportProfilePage : DealMaker.Forms.Wizard.WizardPage
	{
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.ColumnHeader profileColumnHeader;
		private System.Windows.Forms.ColumnHeader descriptionColumnHeader;
		private System.Windows.Forms.CheckBox createNewCheckBox;
		private System.Windows.Forms.ListView profilesListView;
		private System.Windows.Forms.ContextMenu contextMenu1;
		private System.Windows.Forms.MenuItem _DeleteProfile;
		private System.ComponentModel.IContainer components = null;

		public SelectExportProfilePage(WizardForm wizardForm): base(wizardForm)
		{
			// This call is required by the Windows Form Designer.
			InitializeComponent();

			//TA++, 28.03.2005. new features required:
			this.createNewCheckBox.CheckedChanged += new EventHandler(createNewProfile_EH);

			FirstEntered += new EventHandler(FirstEnteredEventHandler);
			Exited += new EventHandler(ExitedEventHandler);
			CanExit += new CanExitEventHandler(CanExitEventHandler);
			//TA--, 28.03.2005. new features required:

			//PROFILE MANAGER
			_DeleteProfile.Click += new EventHandler(OnDeleteProfileClick);
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.profilesListView = new System.Windows.Forms.ListView();
			this.profileColumnHeader = new System.Windows.Forms.ColumnHeader();
			this.descriptionColumnHeader = new System.Windows.Forms.ColumnHeader();
			this.createNewCheckBox = new System.Windows.Forms.CheckBox();
			this.contextMenu1 = new System.Windows.Forms.ContextMenu();
			this._DeleteProfile = new System.Windows.Forms.MenuItem();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.label1.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.label1.Location = new System.Drawing.Point(0, 0);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(280, 32);
			this.label1.TabIndex = 0;
			this.label1.Text = "Select export profile or click \"Create new profile\" to create new export profile";
			// 
			// label3
			// 
			this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.label3.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.label3.Location = new System.Drawing.Point(0, 48);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(280, 16);
			this.label3.TabIndex = 2;
			this.label3.Text = "Profiles";
			// 
			// profilesListView
			// 
			this.profilesListView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.profilesListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
																							   this.profileColumnHeader,
																							   this.descriptionColumnHeader});
			this.profilesListView.ContextMenu = this.contextMenu1;
			this.profilesListView.FullRowSelect = true;
			this.profilesListView.GridLines = true;
			this.profilesListView.HideSelection = false;
			this.profilesListView.Location = new System.Drawing.Point(0, 64);
			this.profilesListView.MultiSelect = false;
			this.profilesListView.Name = "profilesListView";
			this.profilesListView.Size = new System.Drawing.Size(280, 112);
			this.profilesListView.TabIndex = 3;
			this.profilesListView.View = System.Windows.Forms.View.Details;
			// 
			// profileColumnHeader
			// 
			this.profileColumnHeader.Text = "Profile";
			this.profileColumnHeader.Width = 100;
			// 
			// descriptionColumnHeader
			// 
			this.descriptionColumnHeader.Text = "Description";
			this.descriptionColumnHeader.Width = 165;
			// 
			// createNewCheckBox
			// 
			this.createNewCheckBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.createNewCheckBox.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.createNewCheckBox.Location = new System.Drawing.Point(168, 192);
			this.createNewCheckBox.Name = "createNewCheckBox";
			this.createNewCheckBox.Size = new System.Drawing.Size(106, 16);
			this.createNewCheckBox.TabIndex = 4;
			this.createNewCheckBox.Text = "Create new profile";
			// 
			// contextMenu1
			// 
			this.contextMenu1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																						 this._DeleteProfile});
			// 
			// _DeleteProfile
			// 
			this._DeleteProfile.Index = 0;
			this._DeleteProfile.Text = "Delete Profile";
			// 
			// SelectExportProfilePage
			// 
			this.Controls.Add(this.createNewCheckBox);
			this.Controls.Add(this.profilesListView);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label1);
			this.Name = "SelectExportProfilePage";
			this.Size = new System.Drawing.Size(280, 216);
			this.Title = "Select Export Profile";
			this.ResumeLayout(false);

		}
		#endregion

		private void FirstEnteredEventHandler(object sender, EventArgs e)
		{
			string path = Application.StartupPath + Globals.ExportProfilesPath;

			profilesListView.Items.Clear();

			if (!Directory.Exists(path))
				return;

			foreach (string file in Directory.GetFiles(path, "*.xml"))
			{
				FileStream fs;
				XmlSerializer serializer;
				ExportMappingProfile profile;
				ListViewItem item = new ListViewItem();

				try
				{
					fs = new FileStream(file, FileMode.Open);
					serializer = new XmlSerializer(typeof(ExportMappingProfile));

					profile = (ExportMappingProfile)serializer.Deserialize(fs);

					fs.Close();
				}
				catch(Exception 
#if DEBUG
					ex
#endif
					)
				{
					//sasa, 24.08.2005: this silent catch coused a lot of difficoult in bug chase, if debug show exception
					#if DEBUG
						MessageBox.Show(ex.ToString());
					#endif
					
					continue;
				}

				item.Text = profile.Name;
				item.SubItems.Add(profile.Description);
				item.Tag = profile;

				profilesListView.Items.Add(item);
			}
		}

		private void ExitedEventHandler(object sender, EventArgs e)
		{
			CreateExportProfilePage cepp = WizardForm[2] as CreateExportProfilePage;
			CreateExportFilterPage cefp = WizardForm[3] as CreateExportFilterPage;

			if (null == SelectedProfile || CreateNewProfile)
				return;

			cepp.SelectedFields = SelectedProfile.Mappings;
			cefp.SimpleFilters = SelectedProfile.SimpleFilters;
			cefp.MathFilters = SelectedProfile.MathFilters;

			//WizardForm.ButtonStyles |= WizardFormButtonStyles.FinishButton;
			//WizardForm.FinishButton.Text = "&Export";
		}

		private bool CanExitEventHandler(object sender, EventArgs e)
		{
			bool retVal = SelectedProfile != null || CreateNewProfile;
			if (false == retVal)
			{
				MessageBox.Show(this, "Please select Existing Profile from the list, or check \"Create New Profile!\"", "Select Profile", MessageBoxButtons.OK, MessageBoxIcon.Stop);
			}
			return retVal;
		}
		#region Properties
		public bool CreateNewProfile
		{
			get
			{
				return createNewCheckBox.Checked;
			}
		}

		public ExportMappingProfile SelectedProfile
		{
			get
			{
				if (null != profilesListView.FocusedItem)
					return profilesListView.FocusedItem.Tag as ExportMappingProfile;
				else
					return null;
			}
		}
		#endregion

		//TA++, 28.03.2005. new features required:
		private void createNewProfile_EH(object sender, EventArgs e)
		{
			CheckBox cb = (CheckBox)sender;
			if (true == cb.Checked)
			{
				this.profilesListView.Enabled = false;
			}
			else
			{
				this.profilesListView.Enabled = true;
			}
		}
		//TA--, 28.03.2005. new features required:

		private void OnDeleteProfileClick(object sender, EventArgs e)
		{			
			if (null != this.profilesListView.SelectedItems && 1 == this.profilesListView.SelectedItems.Count)
			{
				string s = Application.StartupPath + Globals.ExportProfilesPath + @"\" + (string)this.profilesListView.SelectedItems[0].Text + ".profile.xml";
				ProfileManager pm = new ProfileManager(s, true);
				
				string outMessage;

				if (false == pm.DeleteProfile(out outMessage))
					MessageBox.Show(outMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);	
				else
					FirstEnteredEventHandler(sender, e);
			}
		}
	}
}

