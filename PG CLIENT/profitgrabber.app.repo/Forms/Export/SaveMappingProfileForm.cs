using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace DealMaker.Forms.Export
{
	/// <summary>
	/// Summary description for SaveMappingProfile.
	/// </summary>
	public class SaveMappingProfileForm : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Label profileNameLabel;
		private System.Windows.Forms.TextBox profileNameTextBox;
		private System.Windows.Forms.Button saveButton;
		private System.Windows.Forms.Button cancelButton;
		private System.Windows.Forms.Label descriptionLabel;
		private System.Windows.Forms.TextBox profileDescriptionTextBox;

		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public SaveMappingProfileForm()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			profileNameTextBox.TextChanged += new EventHandler(TextChangedEventHandler);
			profileDescriptionTextBox.TextChanged += new EventHandler(TextChangedEventHandler);
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		private void TextChangedEventHandler(object sender, EventArgs e)
		{
			saveButton.Enabled = profileNameTextBox.Text != String.Empty &&
								 profileDescriptionTextBox.Text != String.Empty;
								
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.saveButton = new System.Windows.Forms.Button();
			this.cancelButton = new System.Windows.Forms.Button();
			this.profileNameLabel = new System.Windows.Forms.Label();
			this.descriptionLabel = new System.Windows.Forms.Label();
			this.profileNameTextBox = new System.Windows.Forms.TextBox();
			this.profileDescriptionTextBox = new System.Windows.Forms.TextBox();
			this.SuspendLayout();
			// 
			// saveButton
			// 
			this.saveButton.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.saveButton.Enabled = false;
			this.saveButton.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.saveButton.Location = new System.Drawing.Point(144, 104);
			this.saveButton.Name = "saveButton";
			this.saveButton.TabIndex = 5;
			this.saveButton.Text = "&Save Profile";
			// 
			// cancelButton
			// 
			this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.cancelButton.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.cancelButton.Location = new System.Drawing.Point(64, 104);
			this.cancelButton.Name = "cancelButton";
			this.cancelButton.TabIndex = 4;
			this.cancelButton.Text = "&Cancel";
			// 
			// profileNameLabel
			// 
			this.profileNameLabel.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.profileNameLabel.Location = new System.Drawing.Point(8, 8);
			this.profileNameLabel.Name = "profileNameLabel";
			this.profileNameLabel.Size = new System.Drawing.Size(100, 16);
			this.profileNameLabel.TabIndex = 0;
			this.profileNameLabel.Text = "Profile name:";
			// 
			// descriptionLabel
			// 
			this.descriptionLabel.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.descriptionLabel.Location = new System.Drawing.Point(8, 56);
			this.descriptionLabel.Name = "descriptionLabel";
			this.descriptionLabel.Size = new System.Drawing.Size(100, 16);
			this.descriptionLabel.TabIndex = 2;
			this.descriptionLabel.Text = "Description:";
			// 
			// profileNameTextBox
			// 
			this.profileNameTextBox.Location = new System.Drawing.Point(8, 24);
			this.profileNameTextBox.Name = "profileNameTextBox";
			this.profileNameTextBox.Size = new System.Drawing.Size(128, 20);
			this.profileNameTextBox.TabIndex = 1;
			this.profileNameTextBox.Text = "";
			// 
			// profileDescriptionTextBox
			// 
			this.profileDescriptionTextBox.Location = new System.Drawing.Point(8, 72);
			this.profileDescriptionTextBox.Name = "profileDescriptionTextBox";
			this.profileDescriptionTextBox.Size = new System.Drawing.Size(208, 20);
			this.profileDescriptionTextBox.TabIndex = 3;
			this.profileDescriptionTextBox.Text = "";
			// 
			// SaveMappingProfileForm
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(210)), ((System.Byte)(216)), ((System.Byte)(240)));
			this.ClientSize = new System.Drawing.Size(224, 134);
			this.Controls.Add(this.profileDescriptionTextBox);
			this.Controls.Add(this.profileNameTextBox);
			this.Controls.Add(this.descriptionLabel);
			this.Controls.Add(this.profileNameLabel);
			this.Controls.Add(this.cancelButton);
			this.Controls.Add(this.saveButton);
			this.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "SaveMappingProfileForm";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Save map";
			this.TopMost = true;
			this.ResumeLayout(false);

		}
		#endregion

		public bool Execute()
		{
			return DialogResult.OK == this.ShowDialog();
		}

		#region Properties
		public string ProfileName
		{
			get
			{
				return profileNameTextBox.Text;
			}
		}

		public string ProfileDescription
		{
			get
			{
				return profileDescriptionTextBox.Text;
			}
		}
		#endregion
	}
}
