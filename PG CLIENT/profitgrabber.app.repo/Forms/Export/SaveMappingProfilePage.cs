using System;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using DealMaker.Export;
using DealMaker.Filters;
using DealMaker.Forms.Wizard;

namespace DealMaker.Forms.Export
{
	public class SaveMappingProfilePage : DealMaker.Forms.Wizard.WizardPage
	{
		private System.Windows.Forms.Label infoLabel0;
		private System.Windows.Forms.Label infoLabel1;
		private System.Windows.Forms.Label skipInfoLabel;
		private System.Windows.Forms.Label warningLabel;
		private System.Windows.Forms.Label torelRecordsLabel;
		private System.Windows.Forms.Label totelFilteredLabel;
		private System.Windows.Forms.Label totalCountLabel;
		private System.Windows.Forms.Label filteredCountLabel;
		private System.Windows.Forms.Button saveMapButton;
		private System.ComponentModel.IContainer components = null;

		public SaveMappingProfilePage()
		{
			// This call is required by the Windows Form Designer.
			InitializeComponent();
		}

		public SaveMappingProfilePage(WizardForm wizard): base(wizard)
		{
			// This call is required by the Windows Form Designer.
			InitializeComponent();

			Entered += new EventHandler(EnteredEventHandler);
			saveMapButton.Click += new EventHandler(SaveMapButtonClickedEventHandler);
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		private void EnteredEventHandler(object sender, EventArgs e)
		{
			int filteredCount = 0;
			SelectContactsPage scp = WizardForm[0] as SelectContactsPage;
			CreateExportFilterPage cefp = WizardForm[3] as CreateExportFilterPage;

			filteredCountLabel.Text = "calculating...";

			totalCountLabel.Text = scp.Contacts.Length.ToString();
			filteredCount = new PropertyItemCriteria(scp.Contacts, cefp.Filters).List().Length;

			filteredCountLabel.Text = filteredCount.ToString();
		}

		private void SaveMapButtonClickedEventHandler(object sender, EventArgs e)
		{
			XmlSerializer serializer;
			ExportMappingProfile profile;
			string path = Application.StartupPath + Globals.ExportProfilesPath;
			CreateExportProfilePage cepp = WizardForm[2] as CreateExportProfilePage;
			CreateExportFilterPage cefp = WizardForm[3] as CreateExportFilterPage;
			
			do
			{
				string profileFileName;
				SaveMappingProfileForm saveDialog = new SaveMappingProfileForm();

				if (!saveDialog.Execute())
					break;

				profileFileName = path + "\\" + saveDialog.ProfileName + ".profile.xml";

				if (File.Exists(profileFileName))
				{
					if (DialogResult.No == MessageBox.Show(
						"Profile under that name already exists\n" +
						"Do you want to replace old profile with this profile?",
						"Replace profile",
						MessageBoxButtons.YesNo))
					{
						continue;
					}
				}
						
				/* Check if directory exists and create it if not */
				if (!Directory.Exists(path))
					Directory.CreateDirectory(path);

				serializer = new XmlSerializer(typeof(ExportMappingProfile));
				profile = new ExportMappingProfile(
					saveDialog.ProfileName, saveDialog.ProfileDescription,
					cefp.SimpleFilters, cefp.MathFilters, cepp.SelectedFields);

				try
				{
					StreamWriter writer = new StreamWriter(profileFileName);
					serializer.Serialize(writer, profile);
					writer.Close();
				}
				catch (System.Exception ex)
				{
					MessageBox.Show(ex.ToString());
				}

				break;

			} while (true);
		}

		#region Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.infoLabel0 = new System.Windows.Forms.Label();
			this.infoLabel1 = new System.Windows.Forms.Label();
			this.skipInfoLabel = new System.Windows.Forms.Label();
			this.warningLabel = new System.Windows.Forms.Label();
			this.torelRecordsLabel = new System.Windows.Forms.Label();
			this.totelFilteredLabel = new System.Windows.Forms.Label();
			this.totalCountLabel = new System.Windows.Forms.Label();
			this.filteredCountLabel = new System.Windows.Forms.Label();
			this.saveMapButton = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// infoLabel0
			// 
			this.infoLabel0.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.infoLabel0.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.infoLabel0.Location = new System.Drawing.Point(0, 0);
			this.infoLabel0.Name = "infoLabel0";
			this.infoLabel0.Size = new System.Drawing.Size(432, 32);
			this.infoLabel0.TabIndex = 0;
			this.infoLabel0.Text = "Most of the time you will be exporting files to the same sources, therefore using" +
				" the same mappng profile.";
			// 
			// infoLabel1
			// 
			this.infoLabel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.infoLabel1.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.infoLabel1.Location = new System.Drawing.Point(0, 40);
			this.infoLabel1.Name = "infoLabel1";
			this.infoLabel1.Size = new System.Drawing.Size(432, 32);
			this.infoLabel1.TabIndex = 1;
			this.infoLabel1.Text = "If you foresee using Mapping Profile that you just created, clisk SAVE MAP below!" +
				" This will save the Export Mapping Profile together with the Filter (if you crea" +
				"ted one).";
			// 
			// skipInfoLabel
			// 
			this.skipInfoLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.skipInfoLabel.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.skipInfoLabel.Location = new System.Drawing.Point(0, 144);
			this.skipInfoLabel.Name = "skipInfoLabel";
			this.skipInfoLabel.Size = new System.Drawing.Size(432, 16);
			this.skipInfoLabel.TabIndex = 2;
			this.skipInfoLabel.Text = "To SKIP saving Mapping Profile click Next!";
			// 
			// warningLabel
			// 
			this.warningLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.warningLabel.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.warningLabel.Location = new System.Drawing.Point(0, 160);
			this.warningLabel.Name = "warningLabel";
			this.warningLabel.Size = new System.Drawing.Size(432, 16);
			this.warningLabel.TabIndex = 3;
			this.warningLabel.Text = "Warning! If you skip saving this profile, you will need to recreate it again next" +
				" time!";
			// 
			// torelRecordsLabel
			// 
			this.torelRecordsLabel.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.torelRecordsLabel.Location = new System.Drawing.Point(0, 80);
			this.torelRecordsLabel.Name = "torelRecordsLabel";
			this.torelRecordsLabel.Size = new System.Drawing.Size(272, 16);
			this.torelRecordsLabel.TabIndex = 4;
			this.torelRecordsLabel.Text = "TOTAL # of Records BEFORE the filter was applied:";
			// 
			// totelFilteredLabel
			// 
			this.totelFilteredLabel.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.totelFilteredLabel.Location = new System.Drawing.Point(0, 96);
			this.totelFilteredLabel.Name = "totelFilteredLabel";
			this.totelFilteredLabel.Size = new System.Drawing.Size(272, 16);
			this.totelFilteredLabel.TabIndex = 5;
			this.totelFilteredLabel.Text = "TOTAL # of Records AFTER the filter was applied:";
			// 
			// totalCountLabel
			// 
			this.totalCountLabel.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.totalCountLabel.Location = new System.Drawing.Point(264, 80);
			this.totalCountLabel.Name = "totalCountLabel";
			this.totalCountLabel.Size = new System.Drawing.Size(72, 16);
			this.totalCountLabel.TabIndex = 6;
			this.totalCountLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
			// 
			// filteredCountLabel
			// 
			this.filteredCountLabel.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.filteredCountLabel.Location = new System.Drawing.Point(264, 96);
			this.filteredCountLabel.Name = "filteredCountLabel";
			this.filteredCountLabel.Size = new System.Drawing.Size(72, 16);
			this.filteredCountLabel.TabIndex = 7;
			this.filteredCountLabel.Text = "calculating...";
			this.filteredCountLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
			// 
			// saveMapButton
			// 
			this.saveMapButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.saveMapButton.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.saveMapButton.Location = new System.Drawing.Point(352, 80);
			this.saveMapButton.Name = "saveMapButton";
			this.saveMapButton.Size = new System.Drawing.Size(72, 28);
			this.saveMapButton.TabIndex = 8;
			this.saveMapButton.Text = "SAVE MAP";
			// 
			// SaveMappingProfilePage
			// 
			this.Controls.Add(this.saveMapButton);
			this.Controls.Add(this.filteredCountLabel);
			this.Controls.Add(this.totalCountLabel);
			this.Controls.Add(this.totelFilteredLabel);
			this.Controls.Add(this.torelRecordsLabel);
			this.Controls.Add(this.warningLabel);
			this.Controls.Add(this.skipInfoLabel);
			this.Controls.Add(this.infoLabel1);
			this.Controls.Add(this.infoLabel0);
			this.Name = "SaveMappingProfilePage";
			this.Size = new System.Drawing.Size(432, 176);
			this.Title = "Save Export Mapping Profile";
			this.ResumeLayout(false);

		}
		#endregion
	}
}

