using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using Microsoft.Win32;

using System.Diagnostics;

namespace DealMaker
{
	/// <summary>
	/// Summary description for UserCreditForm.
	/// </summary>
	public class UserCreditForm : System.Windows.Forms.Form
	{
		Guid _userId = Guid.Empty;

		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.LinkLabel _renewTopUpCredits;
		private System.Windows.Forms.LinkLabel _renewSubscription;
		private System.Windows.Forms.Button _ok;
		private System.Windows.Forms.Label _availableMonthlySunscription;
		private System.Windows.Forms.Label _availableTopUps;
		private System.Windows.Forms.Label _totalCreditsAvailable;
		private System.Windows.Forms.Label _typeOfSearch;
		private System.Windows.Forms.Label _searchAmount;
		private System.Windows.Forms.Label _totalSearchCost;
		private System.Windows.Forms.LinkLabel _viewMyAccountDetails;
		private System.Windows.Forms.Label label9;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public UserCreditForm(Guid UserId, int availableSubscription, int availableTopUpCredits, int totalAvailableCredits, TypeOfCharge typeOfCharge, int chargeAmount, int totalChargeAmount)
		{			
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();			

			_ok.Click +=new EventHandler(On_Ok_Click);

			_userId = UserId;
			_availableMonthlySunscription.Text = availableSubscription.ToString();
			_availableTopUps.Text = availableTopUpCredits.ToString();
			_totalCreditsAvailable.Text = totalAvailableCredits.ToString();

			switch (typeOfCharge)
			{
				case TypeOfCharge.COMPS:
					_typeOfSearch.Text = "COMPS";
					break;

				case TypeOfCharge.PP:
					_typeOfSearch.Text = "Property profile";
					break;

				case TypeOfCharge.MM:
					_typeOfSearch.Text = "Name matching";
					break;

				default:
					_typeOfSearch.Text = string.Empty;
					break;
			}
			
			_searchAmount.Text = chargeAmount.ToString();
			_totalSearchCost.Text = totalChargeAmount.ToString();

			_renewSubscription.Click += new EventHandler(On_renewSubscription_Click);
			_renewTopUpCredits.Click += new EventHandler(On_renewTopUpCredits_Click);
			_viewMyAccountDetails.Click += new EventHandler(On_viewMyAccountDetails_Click);
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.label7 = new System.Windows.Forms.Label();
			this.label8 = new System.Windows.Forms.Label();
			this._renewTopUpCredits = new System.Windows.Forms.LinkLabel();
			this._renewSubscription = new System.Windows.Forms.LinkLabel();
			this._ok = new System.Windows.Forms.Button();
			this._availableMonthlySunscription = new System.Windows.Forms.Label();
			this._availableTopUps = new System.Windows.Forms.Label();
			this._totalCreditsAvailable = new System.Windows.Forms.Label();
			this._typeOfSearch = new System.Windows.Forms.Label();
			this._searchAmount = new System.Windows.Forms.Label();
			this._totalSearchCost = new System.Windows.Forms.Label();
			this._viewMyAccountDetails = new System.Windows.Forms.LinkLabel();
			this.label9 = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label1.Location = new System.Drawing.Point(8, 136);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(176, 23);
			this.label1.TabIndex = 0;
			this.label1.Text = "Total Cost (in terms of credits):";
			// 
			// label2
			// 
			this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label2.Location = new System.Drawing.Point(8, 24);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(128, 23);
			this.label2.TabIndex = 1;
			this.label2.Text = "Available Credits";
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(32, 48);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(120, 23);
			this.label3.TabIndex = 2;
			this.label3.Text = "Monthly Subscription:";
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(32, 72);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(120, 23);
			this.label4.TabIndex = 3;
			this.label4.Text = "Top Up Credits:";
			// 
			// label5
			// 
			this.label5.Location = new System.Drawing.Point(112, 104);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(40, 23);
			this.label5.TabIndex = 4;
			this.label5.Text = "Total:";
			// 
			// label6
			// 
			this.label6.Location = new System.Drawing.Point(16, 160);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(85, 23);
			this.label6.TabIndex = 5;
			this.label6.Text = "Type of Search:";
			// 
			// label7
			// 
			this.label7.Location = new System.Drawing.Point(211, 160);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(56, 23);
			this.label7.TabIndex = 6;
			this.label7.Text = "Amount:";
			// 
			// label8
			// 
			this.label8.Location = new System.Drawing.Point(312, 160);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(40, 23);
			this.label8.TabIndex = 7;
			this.label8.Text = "Total:";
			// 
			// _renewTopUpCredits
			// 
			this._renewTopUpCredits.Location = new System.Drawing.Point(240, 72);
			this._renewTopUpCredits.Name = "_renewTopUpCredits";
			this._renewTopUpCredits.Size = new System.Drawing.Size(128, 23);
			this._renewTopUpCredits.TabIndex = 8;
			this._renewTopUpCredits.TabStop = true;
			this._renewTopUpCredits.Text = "Renew TopUp Credits";
			// 
			// _renewSubscription
			// 
			this._renewSubscription.Location = new System.Drawing.Point(240, 48);
			this._renewSubscription.Name = "_renewSubscription";
			this._renewSubscription.Size = new System.Drawing.Size(128, 23);
			this._renewSubscription.TabIndex = 9;
			this._renewSubscription.TabStop = true;
			this._renewSubscription.Text = "Renew Subscription";
			// 
			// _ok
			// 
			this._ok.Location = new System.Drawing.Point(184, 208);
			this._ok.Name = "_ok";
			this._ok.TabIndex = 10;
			this._ok.Text = "OK";
			// 
			// _availableMonthlySunscription
			// 
			this._availableMonthlySunscription.Location = new System.Drawing.Point(160, 48);
			this._availableMonthlySunscription.Name = "_availableMonthlySunscription";
			this._availableMonthlySunscription.Size = new System.Drawing.Size(72, 23);
			this._availableMonthlySunscription.TabIndex = 11;
			// 
			// _availableTopUps
			// 
			this._availableTopUps.Location = new System.Drawing.Point(160, 72);
			this._availableTopUps.Name = "_availableTopUps";
			this._availableTopUps.Size = new System.Drawing.Size(72, 23);
			this._availableTopUps.TabIndex = 12;
			// 
			// _totalCreditsAvailable
			// 
			this._totalCreditsAvailable.Location = new System.Drawing.Point(160, 104);
			this._totalCreditsAvailable.Name = "_totalCreditsAvailable";
			this._totalCreditsAvailable.Size = new System.Drawing.Size(72, 23);
			this._totalCreditsAvailable.TabIndex = 13;
			// 
			// _typeOfSearch
			// 
			this._typeOfSearch.Location = new System.Drawing.Point(104, 160);
			this._typeOfSearch.Name = "_typeOfSearch";
			this._typeOfSearch.Size = new System.Drawing.Size(96, 23);
			this._typeOfSearch.TabIndex = 14;
			// 
			// _searchAmount
			// 
			this._searchAmount.Location = new System.Drawing.Point(265, 160);
			this._searchAmount.Name = "_searchAmount";
			this._searchAmount.Size = new System.Drawing.Size(29, 23);
			this._searchAmount.TabIndex = 15;
			// 
			// _totalSearchCost
			// 
			this._totalSearchCost.Location = new System.Drawing.Point(357, 160);
			this._totalSearchCost.Name = "_totalSearchCost";
			this._totalSearchCost.Size = new System.Drawing.Size(29, 23);
			this._totalSearchCost.TabIndex = 16;
			// 
			// _viewMyAccountDetails
			// 
			this._viewMyAccountDetails.Location = new System.Drawing.Point(240, 104);
			this._viewMyAccountDetails.Name = "_viewMyAccountDetails";
			this._viewMyAccountDetails.Size = new System.Drawing.Size(136, 23);
			this._viewMyAccountDetails.TabIndex = 17;
			this._viewMyAccountDetails.TabStop = true;
			this._viewMyAccountDetails.Text = "View My Account Details";
			// 
			// label9
			// 
			this.label9.Location = new System.Drawing.Point(392, 160);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(40, 23);
			this.label9.TabIndex = 18;
			this.label9.Text = "credits";
			// 
			// UserCreditForm
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(210)), ((System.Byte)(216)), ((System.Byte)(240)));
			this.ClientSize = new System.Drawing.Size(442, 248);
			this.Controls.Add(this.label9);
			this.Controls.Add(this._viewMyAccountDetails);
			this.Controls.Add(this._totalSearchCost);
			this.Controls.Add(this._searchAmount);
			this.Controls.Add(this._typeOfSearch);
			this.Controls.Add(this._totalCreditsAvailable);
			this.Controls.Add(this._availableTopUps);
			this.Controls.Add(this._availableMonthlySunscription);
			this.Controls.Add(this._ok);
			this.Controls.Add(this._renewSubscription);
			this.Controls.Add(this._renewTopUpCredits);
			this.Controls.Add(this.label8);
			this.Controls.Add(this.label7);
			this.Controls.Add(this.label6);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "UserCreditForm";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "ProfitGrabber Pro - User Credits";
			this.ResumeLayout(false);

		}
		#endregion

		private void On_renewSubscription_Click(object sender, EventArgs e)
		{
            if (Guid.Empty != _userId)
            {
                Process.Start("http://www.profitgrabber.com/account/subscribe_renew.cfm?IDUser={" + _userId.ToString() + "}");                
            }
		}

		private void On_renewTopUpCredits_Click(object sender, EventArgs e)
		{
            if (Guid.Empty != _userId)
            {				
				Process.Start("http://www.profitgrabber.com/Account/TopUp.cfm?GUID={" + _userId.ToString() + "}");                
            }
			
		}

		private void On_viewMyAccountDetails_Click(object sender, EventArgs e)
		{
            if (Guid.Empty != _userId)
            {
				Process.Start("http://www.ProfitGrabber.com/Account/Home.cfm?GUID={" + _userId.ToString() + "}");                
            }
		}        

		private void On_Ok_Click(object sender, EventArgs e)
		{
			Close();
		}
	}
}
