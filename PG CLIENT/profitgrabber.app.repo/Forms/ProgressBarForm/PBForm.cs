#region � Using �
using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
#endregion � Using �
#region � Namespace �
namespace DealMaker
{
	/// <summary>
	/// Summary description for PBForm.
	/// </summary>
	public class PBForm : System.Windows.Forms.Form
	{
		private EventTrigger eventTrigger = null;
		private System.Windows.Forms.ProgressBar pb;
		private int  capacity = 0;
		private int cnt = 0;

		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public PBForm(EventTrigger et)
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			this.eventTrigger = et;
			this.eventTrigger.LineReadingFinished += new FillProgressBarEventHandler(FillProgressBar);
			

		}

		public int Capacity
		{
			get {return this.capacity;}
			set 
			{
				this.capacity = value;
				this.pb.Maximum = value;				
			}
		}

		public string WindowName
		{
			get { return this.Text; }
			set { this.Text = value; }
		}

		private void FillProgressBar(object sender, EventArgs e)
		{						
			if (cnt <= this.pb.Maximum)
			{
				this.pb.Value = cnt;			
				cnt ++;
			}


			if (cnt == capacity)
			{
				this.Close();
			}
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.pb = new System.Windows.Forms.ProgressBar();
			this.SuspendLayout();
			// 
			// pb
			// 
			this.pb.Location = new System.Drawing.Point(8, 16);
			this.pb.Name = "pb";
			this.pb.Size = new System.Drawing.Size(280, 16);
			this.pb.TabIndex = 0;
			// 
			// PBForm
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(210)), ((System.Byte)(216)), ((System.Byte)(240)));
			this.ClientSize = new System.Drawing.Size(298, 48);
			this.Controls.Add(this.pb);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
			this.Name = "PBForm";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "PBForm";
			this.ResumeLayout(false);

		}
		#endregion

		
	}
}
#endregion � Namespace �