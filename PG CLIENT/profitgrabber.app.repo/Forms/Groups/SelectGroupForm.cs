using System;
using System.Data;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data.SqlClient; 
using System.Data.SqlTypes;
using DataAppendService.Lib.Models.IDI;
using DealMaker.Conversion.ShortSale;

//Added
using DealMaker.PlainClasses.ContactTypes;

namespace DealMaker
{
	/// <summary>
	/// Summary description for SelectGroupForm.
	/// </summary>
	public class SelectGroupForm : System.Windows.Forms.Form
	{
		private bool bValid = false;
		ArrayList alPropertyItemsId = new ArrayList();
		Guid propertyItemId = Guid.Empty;
		public DealMaker.CustomizedTreeViewUserControl customizedTreeViewUserControl1;
		private System.Windows.Forms.Button bOK;
		private System.Windows.Forms.Button bCancel;

        ContactTypes _contactType = ContactTypes.Seller;

		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public bool Valid 
		{
			get {return this.bValid;}
		}

        public ContactTypes ContactType 
        {
            get { return _contactType; }
            set { _contactType = value; }
        }        

		public SelectGroupForm(ArrayList alPropertyItemId)
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			this.alPropertyItemsId = alPropertyItemId;
		}
		public SelectGroupForm(Guid propertyItemId)
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			this.propertyItemId = propertyItemId;
			this.alPropertyItemsId.Add(propertyItemId);
		}
		public SelectGroupForm()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
		}

        public int SelectedNodeId
        {
            get 
            {
                if (null != this.customizedTreeViewUserControl1)
                    return this.customizedTreeViewUserControl1.SelectedNodeId;
                else
                    return -1;
            }
        }

        public TreeNode SelectedNode => this.customizedTreeViewUserControl1.SelectedTreeNode;

        public string SelectedNodeName
        {
            get 
            {
                if (null != this.customizedTreeViewUserControl1 && null != this.customizedTreeViewUserControl1.SelectedNodeName)
                    return this.customizedTreeViewUserControl1.SelectedNodeName;
                else
                    return string.Empty;
            }
        }

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.customizedTreeViewUserControl1 = new DealMaker.CustomizedTreeViewUserControl();
            this.bOK = new System.Windows.Forms.Button();
            this.bCancel = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // customizedTreeViewUserControl1
            // 
            this.customizedTreeViewUserControl1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.customizedTreeViewUserControl1.Location = new System.Drawing.Point(8, 8);
            this.customizedTreeViewUserControl1.Name = "customizedTreeViewUserControl1";
            this.customizedTreeViewUserControl1.SelectedTreeNode = null;
            this.customizedTreeViewUserControl1.Size = new System.Drawing.Size(408, 519);
            this.customizedTreeViewUserControl1.TabIndex = 0;
            this.customizedTreeViewUserControl1.UpdateGlobals = false;
            // 
            // bOK
            // 
            this.bOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.bOK.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.bOK.Location = new System.Drawing.Point(416, 16);
            this.bOK.Name = "bOK";
            this.bOK.Size = new System.Drawing.Size(75, 23);
            this.bOK.TabIndex = 1;
            this.bOK.Text = "OK";
            this.bOK.Click += new System.EventHandler(this.bOK_Click);
            // 
            // bCancel
            // 
            this.bCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.bCancel.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.bCancel.Location = new System.Drawing.Point(416, 48);
            this.bCancel.Name = "bCancel";
            this.bCancel.Size = new System.Drawing.Size(75, 23);
            this.bCancel.TabIndex = 2;
            this.bCancel.Text = "Cancel";
            this.bCancel.Click += new System.EventHandler(this.bCancel_Click);
            // 
            // SelectGroupForm
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.ClientSize = new System.Drawing.Size(498, 520);
            this.Controls.Add(this.bCancel);
            this.Controls.Add(this.bOK);
            this.Controls.Add(this.customizedTreeViewUserControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "SelectGroupForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Select Group";
            this.ResumeLayout(false);

		}
		#endregion

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            switch (_contactType)
            {
                case ContactTypes.Buyer:
                    if (null != this.customizedTreeViewUserControl1.BuyerNode)
                        this.customizedTreeViewUserControl1.SelectedTreeNode = this.customizedTreeViewUserControl1.BuyerNode; 
                    break;

                case ContactTypes.Lender:
                    if (null != this.customizedTreeViewUserControl1.LenderNode)
                        this.customizedTreeViewUserControl1.SelectedTreeNode = this.customizedTreeViewUserControl1.LenderNode; 
                    break;
            }            
        }

        bool AllowImport(ContactTypes selectedContactType, int nodeId)
        {
            return ContactTypesManager.Instance.AllowImport(selectedContactType, nodeId);
        }

		private void bOK_Click(object sender, System.EventArgs e)
		{
			SqlDataReader groupMembershipReader = null;
			if (Guid.Empty != propertyItemId)
			{				
				//if it doesn�t belong to it - assign it !
				try
				{	
					int nodeId = this.customizedTreeViewUserControl1.SelectedNodeId;

					if (1 == nodeId)
					{
						MessageBox.Show(this, "Please select a group where to save a contact!\nContact can not be saved in a root group!", "Select group!", MessageBoxButtons.OK, MessageBoxIcon.Information);
						return;
					}

                    if (!AllowImport(_contactType, nodeId))
                    {
                        if (ContactTypes.Buyer == _contactType)
                        {
                            MessageBox.Show("Buyers can get assigned only to " + ContactTypesManager.CashBuyersGroup + " and its subgroups.", "New Contact", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            return;
                        }

                        if (ContactTypes.Lender == _contactType)
                        {
                            MessageBox.Show("Lenders can get assigned only to " + ContactTypesManager.PrivateLenderGroup + " and its subgroups.", "New Contact", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            return;
                        }

                        if (ContactTypes.Seller == _contactType)
                        {
                            MessageBox.Show("Sellers can't get assigned to " + ContactTypesManager.PrivateLenderGroup + " or " + ContactTypesManager.CashBuyersGroup + " or their subgroups.", "New Contact", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            return;
                        }
                    }
						
					SqlCommand sp_TestGroupMembership = new SqlCommand("TestGroupMembership", Connection.getInstance().HatchConn);

					sp_TestGroupMembership.CommandType = CommandType.StoredProcedure;					
					sp_TestGroupMembership.Parameters.Add("@propertyItemId", this.propertyItemId);
					sp_TestGroupMembership.Parameters.Add("@nodeId", nodeId);
													
									
					if (ConnectionState.Open != Connection.getInstance().HatchConn.State)
						Connection.getInstance().HatchConn.Open();
				
					groupMembershipReader = sp_TestGroupMembership.ExecuteReader();
					if (groupMembershipReader.HasRows)				
					{
						MessageBox.Show("Property Item is already assigned to this group !");					
						return;

					}
					else
					{
						//insert it !!!
						try
						{	
							groupMembershipReader.Close();
							SqlCommand sp_InsertGroupMembership = new SqlCommand("InsertGroupMembership", Connection.getInstance().HatchConn);

							sp_InsertGroupMembership.CommandType = CommandType.StoredProcedure;					
							sp_InsertGroupMembership.Parameters.Add("@propertyItemId", this.propertyItemId);
							sp_InsertGroupMembership.Parameters.Add("@nodeId", nodeId);
													
									
							if (ConnectionState.Open != Connection.getInstance().HatchConn.State)
								Connection.getInstance().HatchConn.Open();
				
							sp_InsertGroupMembership.ExecuteNonQuery();				
					
						}
						catch( Exception ex)
						{					
							MessageBox.Show(ex.ToString());				
						}					
					}
				
				}
				catch( Exception ex)
				{				
					MessageBox.Show(ex.ToString());				
				}
				finally 
				{				
					if (null != groupMembershipReader && !groupMembershipReader.IsClosed)
					{
						groupMembershipReader.Close();
					}
				}
			}
			if (this.alPropertyItemsId.Count > 0)
			{
				foreach (Guid id in alPropertyItemsId)
				{
					try
					{	
						int nodeId = this.customizedTreeViewUserControl1.SelectedNodeId;
						
						SqlCommand sp_TestGroupMembership = new SqlCommand("TestGroupMembership", Connection.getInstance().HatchConn);

						sp_TestGroupMembership.CommandType = CommandType.StoredProcedure;					
						sp_TestGroupMembership.Parameters.Add("@propertyItemId", id);
						sp_TestGroupMembership.Parameters.Add("@nodeId", nodeId);
													
									
						if (ConnectionState.Open != Connection.getInstance().HatchConn.State)
							Connection.getInstance().HatchConn.Open();
				
						groupMembershipReader = sp_TestGroupMembership.ExecuteReader();
						if (groupMembershipReader.HasRows)				
						{
							continue;

						}
						else
						{
							//insert it !!!
							try
							{	
								groupMembershipReader.Close();
								SqlCommand sp_InsertGroupMembership = new SqlCommand("InsertGroupMembership", Connection.getInstance().HatchConn);

								sp_InsertGroupMembership.CommandType = CommandType.StoredProcedure;					
								sp_InsertGroupMembership.Parameters.Add("@propertyItemId", id);
								sp_InsertGroupMembership.Parameters.Add("@nodeId", nodeId);
													
									
								if (ConnectionState.Open != Connection.getInstance().HatchConn.State)
									Connection.getInstance().HatchConn.Open();
				
								sp_InsertGroupMembership.ExecuteNonQuery();				
					
							}
							catch( Exception ex)
							{					
								MessageBox.Show(ex.ToString());				
							}					
						}
				
					}
					catch( Exception ex)
					{				
						MessageBox.Show(ex.ToString());				
					}
					finally 
					{				
						if (false == groupMembershipReader.IsClosed)
						{
							groupMembershipReader.Close();
						}
					}
				}
			}
			this.bValid = true;
			this.Close();
		}

		private void bCancel_Click(object sender, System.EventArgs e)
		{
			this.Close();
		}
	}
}
