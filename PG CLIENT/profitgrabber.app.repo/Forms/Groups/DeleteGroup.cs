using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace DealMaker
{
	/// <summary>
	/// Summary description for DeleteGroup.
	/// </summary>
	public class DeleteGroup : System.Windows.Forms.Form
	{
		private bool bBrisi = false;
		private bool bValid = false;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label lText;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Button button2;
		private System.Windows.Forms.CheckBox cbDestroy;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public DeleteGroup()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.lText = new System.Windows.Forms.Label();
			this.button1 = new System.Windows.Forms.Button();
			this.button2 = new System.Windows.Forms.Button();
			this.cbDestroy = new System.Windows.Forms.CheckBox();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label1.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.label1.Location = new System.Drawing.Point(24, 24);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(256, 16);
			this.label1.TabIndex = 0;
			this.label1.Text = "Are you sure you want to delete the selected Group?";
			// 
			// lText
			// 
			this.lText.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.lText.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lText.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.lText.Location = new System.Drawing.Point(64, 56);
			this.lText.Name = "lText";
			this.lText.Size = new System.Drawing.Size(352, 16);
			this.lText.TabIndex = 1;
			// 
			// button1
			// 
			this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.button1.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.button1.Location = new System.Drawing.Point(248, 136);
			this.button1.Name = "button1";
			this.button1.TabIndex = 3;
			this.button1.Text = "Yes";
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// button2
			// 
			this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.button2.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.button2.Location = new System.Drawing.Point(336, 136);
			this.button2.Name = "button2";
			this.button2.TabIndex = 2;
			this.button2.Text = "No";
			this.button2.Click += new System.EventHandler(this.button2_Click);
			// 
			// cbDestroy
			// 
			this.cbDestroy.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.cbDestroy.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.cbDestroy.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.cbDestroy.Location = new System.Drawing.Point(32, 80);
			this.cbDestroy.Name = "cbDestroy";
			this.cbDestroy.Size = new System.Drawing.Size(376, 32);
			this.cbDestroy.TabIndex = 4;
			this.cbDestroy.Text = "Delete contacts permanently from all other groups they belong to!";
			// 
			// DeleteGroup
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 14);
			this.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(210)), ((System.Byte)(216)), ((System.Byte)(240)));
			this.ClientSize = new System.Drawing.Size(426, 168);
			this.ControlBox = false;
			this.Controls.Add(this.cbDestroy);
			this.Controls.Add(this.button2);
			this.Controls.Add(this.button1);
			this.Controls.Add(this.lText);
			this.Controls.Add(this.label1);
			this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "DeleteGroup";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Delete Group";
			this.ResumeLayout(false);

		}
		#endregion

		//OK
		private void button1_Click(object sender, System.EventArgs e)
		{
			this.bBrisi = true;
			bValid = true;
			this.Close();
		}
		
		//Cancel
		private void button2_Click(object sender, System.EventArgs e)
		{
			this.bBrisi = false;
			bValid = true;
			this.Close();
		}

		public bool Valid
		{
			get{return this.bValid;}
		}

		public bool Brisi
		{
			get{return this.bBrisi;}
		}

		public string Description
		{
			get {return this.lText.Text;}
			set {this.lText.Text = value;}
		}

		public bool Destroy
		{
			get {return this.cbDestroy.Checked;}
		}
	}
}
