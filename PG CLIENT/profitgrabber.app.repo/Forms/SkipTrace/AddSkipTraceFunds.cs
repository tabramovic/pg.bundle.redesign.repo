﻿using System;
using System.Windows.Forms;

namespace DealMaker.Forms.SkipTrace
{
    public partial class AddSkipTraceFunds : Form
    {
        public AddSkipTraceFunds(decimal estimated, decimal available)
        {
            InitializeComponent();
            Estimated = estimated;
            Available = available;
        }

        public decimal Estimated { get; }
        public decimal Available { get; }

        public bool SkipTraceAllRecordsIncludingPreviouslySkipTraced => _skipTraceAll.Checked;

        protected override void OnLoad(EventArgs e)
        {
            _estimated.Text += $"{string.Format("{0:n}", Estimated)}.";
            _balance.Text += $"{string.Format("{0:n}", Available)}.";

            if (Estimated > Available)
                _skipTrace.Enabled = false;

            base.OnLoad(e);
        }
    }
}
