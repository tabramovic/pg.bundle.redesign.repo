﻿namespace DealMaker.Forms.SkipTrace
{
    partial class AddSkipTraceFunds
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._estimated = new System.Windows.Forms.Label();
            this._balance = new System.Windows.Forms.Label();
            this._cancel = new System.Windows.Forms.Button();
            this._skipTrace = new System.Windows.Forms.Button();
            this._addFunds = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this._warningLabel = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this._skipTraceAll = new System.Windows.Forms.CheckBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // _estimated
            // 
            this._estimated.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._estimated.AutoSize = true;
            this._estimated.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._estimated.Location = new System.Drawing.Point(36, 134);
            this._estimated.Name = "_estimated";
            this._estimated.Size = new System.Drawing.Size(242, 13);
            this._estimated.TabIndex = 2;
            this._estimated.Text = "Potential charge for this search (100% hit rate) is $";
            // 
            // _balance
            // 
            this._balance.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._balance.AutoSize = true;
            this._balance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._balance.Location = new System.Drawing.Point(36, 156);
            this._balance.Name = "_balance";
            this._balance.Size = new System.Drawing.Size(131, 13);
            this._balance.TabIndex = 3;
            this._balance.Text = "Your account balance is $";
            // 
            // _cancel
            // 
            this._cancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this._cancel.Location = new System.Drawing.Point(545, 418);
            this._cancel.Name = "_cancel";
            this._cancel.Size = new System.Drawing.Size(75, 23);
            this._cancel.TabIndex = 5;
            this._cancel.Text = "Cancel";
            this._cancel.UseVisualStyleBackColor = true;
            // 
            // _skipTrace
            // 
            this._skipTrace.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._skipTrace.DialogResult = System.Windows.Forms.DialogResult.OK;
            this._skipTrace.Location = new System.Drawing.Point(464, 418);
            this._skipTrace.Name = "_skipTrace";
            this._skipTrace.Size = new System.Drawing.Size(75, 23);
            this._skipTrace.TabIndex = 6;
            this._skipTrace.Text = "Skip Trace";
            this._skipTrace.UseVisualStyleBackColor = true;
            // 
            // _addFunds
            // 
            this._addFunds.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._addFunds.DialogResult = System.Windows.Forms.DialogResult.Abort;
            this._addFunds.Location = new System.Drawing.Point(383, 418);
            this._addFunds.Name = "_addFunds";
            this._addFunds.Size = new System.Drawing.Size(75, 23);
            this._addFunds.TabIndex = 7;
            this._addFunds.Text = "Add Funds";
            this._addFunds.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(490, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Before starting, you should make sure you have a name and address for each of the" +
    " contacts in the list";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(20, 34);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(337, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "so this service will have the best chance of finding correct information.";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(12, 65);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(604, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "If you do not have names, you should run the Matching Module under the Tools menu" +
    " to find names before starting this search.";
            // 
            // _warningLabel
            // 
            this._warningLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._warningLabel.AutoSize = true;
            this._warningLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._warningLabel.ForeColor = System.Drawing.Color.Red;
            this._warningLabel.Location = new System.Drawing.Point(36, 229);
            this._warningLabel.Name = "_warningLabel";
            this._warningLabel.Size = new System.Drawing.Size(461, 13);
            this._warningLabel.TabIndex = 11;
            this._warningLabel.Text = "You must have an account balance larger than the potential charge before you can " +
    "Skip Trace. ";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(12, 206);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(309, 13);
            this.label2.TabIndex = 13;
            this.label2.Text = "Your account will be charged for each contact that returns data.";
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(36, 252);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(379, 13);
            this.label3.TabIndex = 14;
            this.label3.Text = "If you do not, please Add Funds first and the Skip Trace button will be enabled.";
            // 
            // _skipTraceAll
            // 
            this._skipTraceAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._skipTraceAll.AutoSize = true;
            this._skipTraceAll.Location = new System.Drawing.Point(39, 377);
            this._skipTraceAll.Name = "_skipTraceAll";
            this._skipTraceAll.Size = new System.Drawing.Size(316, 17);
            this._skipTraceAll.TabIndex = 15;
            this._skipTraceAll.Text = "Skip trace all contacts, including those previously skip traced.";
            this._skipTraceAll.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(12, 87);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(533, 13);
            this.label6.TabIndex = 16;
            this.label6.Text = "You should also reduce the First Name field to a single name.  You should not hav" +
    "e \"Bob && Mary\" as a first name.";
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(12, 286);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(484, 13);
            this.label7.TabIndex = 17;
            this.label7.Text = "Profit Grabber will only attempt to skip trace those contacts that have not been " +
    "previously skip traced.";
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(12, 325);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(469, 13);
            this.label8.TabIndex = 18;
            this.label8.Text = " If a contact has been previously skip traced or has skip traced data merged from" +
    " another contact,";
            // 
            // label9
            // 
            this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(36, 344);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(347, 13);
            this.label9.TabIndex = 19;
            this.label9.Text = "the contact will NOT be skip traced unless the following box is checked.";
            // 
            // AddSkipTraceFunds
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.ClientSize = new System.Drawing.Size(634, 455);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this._skipTraceAll);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this._warningLabel);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label1);
            this.Controls.Add(this._addFunds);
            this.Controls.Add(this._skipTrace);
            this.Controls.Add(this._cancel);
            this.Controls.Add(this._balance);
            this.Controls.Add(this._estimated);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "AddSkipTraceFunds";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Skip Trace";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label _estimated;
        private System.Windows.Forms.Label _balance;
        private System.Windows.Forms.Button _cancel;
        private System.Windows.Forms.Button _skipTrace;
        private System.Windows.Forms.Button _addFunds;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label _warningLabel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox _skipTraceAll;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
    }
}