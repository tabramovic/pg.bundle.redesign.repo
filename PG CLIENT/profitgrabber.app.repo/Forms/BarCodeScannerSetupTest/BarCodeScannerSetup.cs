using System;
using System.Drawing;
using System.Drawing.Printing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

//Added
using ProfitGrabber.Common;

namespace DealMaker
{
	/// <summary>
	/// Summary description for BarCodeScannerSetup.
	/// </summary>
	public class BarCodeScannerSetup : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Button bPrint;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.TextBox tbText;
		private System.Windows.Forms.Button bClose;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public BarCodeScannerSetup()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			this.tbText.Focus();
			this.tbText.Select();
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.bPrint = new System.Windows.Forms.Button();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.tbText = new System.Windows.Forms.TextBox();
			this.bClose = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label1.Location = new System.Drawing.Point(16, 16);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(208, 23);
			this.label1.TabIndex = 0;
			this.label1.Text = "Bar Code Scanner Setup";
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(32, 48);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(560, 40);
			this.label2.TabIndex = 1;
			this.label2.Text = "1.) Connect your Bar Code Scanner properly into the computer. Please refer to the" +
				" Manual that came with it.";
			// 
			// bPrint
			// 
			this.bPrint.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.bPrint.Location = new System.Drawing.Point(56, 120);
			this.bPrint.Name = "bPrint";
			this.bPrint.TabIndex = 2;
			this.bPrint.Text = "Print";
			this.bPrint.Click += new System.EventHandler(this.bPrint_Click);
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(32, 88);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(560, 32);
			this.label3.TabIndex = 3;
			this.label3.Text = "2.) Press Print Button which will print Bar Code encrypted string on to the print" +
				"er of Your choice.";
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(32, 152);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(560, 32);
			this.label4.TabIndex = 4;
			this.label4.Text = "3.) Use Bar Code Scanner to scan all printed strings.";
			// 
			// label5
			// 
			this.label5.Location = new System.Drawing.Point(32, 232);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(560, 32);
			this.label5.TabIndex = 5;
			this.label5.Text = "4.) If scanning went OK, you will see the message \"Bar Code Scan Test Passed\".";
			// 
			// tbText
			// 
			this.tbText.Location = new System.Drawing.Point(56, 192);
			this.tbText.Name = "tbText";
			this.tbText.Size = new System.Drawing.Size(208, 20);
			this.tbText.TabIndex = 6;
			this.tbText.Text = "";
			// 
			// bClose
			// 
			this.bClose.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.bClose.Location = new System.Drawing.Point(520, 296);
			this.bClose.Name = "bClose";
			this.bClose.TabIndex = 7;
			this.bClose.Text = "&Close";
			this.bClose.Click += new System.EventHandler(this.bClose_Click);
			// 
			// BarCodeScannerSetup
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(210)), ((System.Byte)(216)), ((System.Byte)(240)));
			this.ClientSize = new System.Drawing.Size(618, 344);
			this.Controls.Add(this.bClose);
			this.Controls.Add(this.tbText);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.bPrint);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
			this.Name = "BarCodeScannerSetup";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Bar Code Scanner Setup";
			this.ResumeLayout(false);

		}
		#endregion

		private void bClose_Click(object sender, System.EventArgs e)
		{
			this.Close();
		}

		private void bPrint_Click(object sender, System.EventArgs e)
		{
			
			PrintDialog pd = new PrintDialog();

			PrintDocument tmpprndoc = new PrintDocument();
			tmpprndoc.PrintPage += new PrintPageEventHandler(toPrinter);

			pd.Document = tmpprndoc;

			PrintPreviewDialogEx tmpprdiag = new PrintPreviewDialogEx();
				
			tmpprdiag.Document = tmpprndoc;
			tmpprdiag.ShowDialog();
		}

		private void toPrinter(Object sender, PrintPageEventArgs e)
		{			
			Font barcodeFont_8  = new Font("IDAutomationHC39M", 8);
			Font barcodeFont_10 = new Font("IDAutomationHC39M", 10);
			Font barcodeFont_12 = new Font("IDAutomationHC39M", 12);
			Font barcodeFont_14 = new Font("IDAutomationHC39M", 14);
			Graphics g = e.Graphics;			

			Brush br = new SolidBrush(Color.Black);				
			g.DrawString("*Passed*", barcodeFont_8, br, 40, 50); 
			g.DrawString("*Passed*", barcodeFont_10, br, 40, 100); 
			g.DrawString("*Passed*", barcodeFont_12, br, 40, 200); 
			g.DrawString("*Passed*", barcodeFont_14, br, 40, 400); 
		}		
	}
}
