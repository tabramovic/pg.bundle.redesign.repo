using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace DealMaker
{
	/// <summary>
	/// Summary description for LockingForm.
	/// </summary>
	public class LockingForm : System.Windows.Forms.Form
	{
		private System.Windows.Forms.CheckBox cbCOMPS;
		private System.Windows.Forms.CheckBox cbGFR;
		private System.Windows.Forms.CheckBox cbMergeModule;
		private System.Windows.Forms.CheckBox cbAll;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Button bOK;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public LockingForm()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();			
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		public bool EnableCOMPS
		{
			get {return this.cbCOMPS.Checked;}
		}

		public bool EnableGFR
		{
			get {return this.cbGFR.Checked;}
		}

		public bool EnableMergeModule
		{
			get {return this.cbMergeModule.Checked;}
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.cbCOMPS = new System.Windows.Forms.CheckBox();
			this.cbGFR = new System.Windows.Forms.CheckBox();
			this.cbMergeModule = new System.Windows.Forms.CheckBox();
			this.cbAll = new System.Windows.Forms.CheckBox();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.bOK = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// cbCOMPS
			// 
			this.cbCOMPS.Checked = true;
			this.cbCOMPS.CheckState = System.Windows.Forms.CheckState.Checked;
			this.cbCOMPS.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.cbCOMPS.Location = new System.Drawing.Point(32, 48);
			this.cbCOMPS.Name = "cbCOMPS";
			this.cbCOMPS.TabIndex = 0;
			this.cbCOMPS.Text = "COMPS";
			this.cbCOMPS.Click += new System.EventHandler(this.cbCOMPS_CheckedChanged);
			// 
			// cbGFR
			// 
			this.cbGFR.Checked = true;
			this.cbGFR.CheckState = System.Windows.Forms.CheckState.Checked;
			this.cbGFR.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.cbGFR.Location = new System.Drawing.Point(32, 80);
			this.cbGFR.Name = "cbGFR";
			this.cbGFR.Size = new System.Drawing.Size(120, 24);
			this.cbGFR.TabIndex = 1;
			this.cbGFR.Text = "Get Forms Ready";
			this.cbGFR.Click += new System.EventHandler(this.cbGFR_CheckedChanged);
			// 
			// cbMergeModule
			// 
			this.cbMergeModule.Checked = true;
			this.cbMergeModule.CheckState = System.Windows.Forms.CheckState.Checked;
			this.cbMergeModule.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.cbMergeModule.Location = new System.Drawing.Point(32, 112);
			this.cbMergeModule.Name = "cbMergeModule";
			this.cbMergeModule.TabIndex = 2;
			this.cbMergeModule.Text = "Merge Module";
			this.cbMergeModule.Click += new System.EventHandler(this.cbMergeModule_CheckedChanged);
			// 
			// cbAll
			// 
			this.cbAll.Checked = true;
			this.cbAll.CheckState = System.Windows.Forms.CheckState.Checked;
			this.cbAll.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.cbAll.Location = new System.Drawing.Point(32, 160);
			this.cbAll.Name = "cbAll";
			this.cbAll.TabIndex = 3;
			this.cbAll.Text = "All";
			this.cbAll.Click += new System.EventHandler(this.cbAll_CheckedChanged);
			// 
			// label1
			// 
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label1.Location = new System.Drawing.Point(8, 16);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(208, 23);
			this.label1.TabIndex = 4;
			this.label1.Text = "Check the checkbox to unlock feature";
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(8, 192);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(224, 40);
			this.label2.TabIndex = 5;
			this.label2.Text = "Note: Merge Module is not integrated yet       and marketing module is always ON." +
				"";
			this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// bOK
			// 
			this.bOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.bOK.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.bOK.Location = new System.Drawing.Point(146, 246);
			this.bOK.Name = "bOK";
			this.bOK.TabIndex = 6;
			this.bOK.Text = "OK";
			this.bOK.Click += new System.EventHandler(this.bOK_Click);
			// 
			// LockingForm
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(234, 288);
			this.Controls.Add(this.bOK);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.cbAll);
			this.Controls.Add(this.cbMergeModule);
			this.Controls.Add(this.cbGFR);
			this.Controls.Add(this.cbCOMPS);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "LockingForm";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "UnLock Feature";
			this.TopMost = true;
			this.ResumeLayout(false);

		}
		#endregion

		private void cbCOMPS_CheckedChanged(object sender, System.EventArgs e)
		{
			this.cbAll.Checked = false;
		}

		private void cbGFR_CheckedChanged(object sender, System.EventArgs e)
		{
			this.cbAll.Checked = false;
		}

		private void cbMergeModule_CheckedChanged(object sender, System.EventArgs e)
		{
			this.cbAll.Checked = false;			
		}

		private void cbAll_CheckedChanged(object sender, System.EventArgs e)
		{
			this.cbCOMPS.Checked = true;
			this.cbGFR.Checked = true;
			this.cbMergeModule.Checked = true;
		}

		private void bOK_Click(object sender, System.EventArgs e)
		{
			this.Close();
		}
	}
}
