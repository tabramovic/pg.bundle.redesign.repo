﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;

//Added
using DealMaker.PlainClasses.Utils;

namespace DealMaker
{
    public partial class ApplyFixes : Form
    {
        ScriptedFixes fixes = new ScriptedFixes();
        string txt = string.Empty;

        public ApplyFixes()
        {
            InitializeComponent();
            fixes.ScfStatus += new ScriptedFixes.ScriptedFixesStatusEventHandler(On_fixes_ScfStatus);
            _close.Click += new EventHandler(On_Close_Click);
        }

        void On_Close_Click(object sender, EventArgs e)
        {            
            this.Close();
        }        

        void  On_fixes_ScfStatus(string msg, bool valid)
        {
            UpdateLabel(msg, valid);
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            Thread t = new Thread(new ThreadStart(fixes.ApplyFixes));
            t.Start();
        }


        delegate void UpdateLabelDelegate(string msg, bool valid);
        void UpdateLabel(string msg, bool valid)
        {
            if (InvokeRequired)
                Invoke(new UpdateLabelDelegate(UpdateLabel), new object[] { msg, valid });
            else
                _label.Text = msg + " - " + (valid ? "Success" : "False") + ".";
        }
    }
}
