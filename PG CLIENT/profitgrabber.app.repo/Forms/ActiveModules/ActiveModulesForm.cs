using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace DealMaker
{
	/// <summary>
	/// Summary description for ActiveModulesForm.
	/// </summary>
	public class ActiveModulesForm : System.Windows.Forms.Form
	{
		private const string FeatureEnabled = "Enabled";
		private const string Disabled = "Disabled";
		private const string Active = "Active";
		private const string NotActivated = "Not Activated";
		private const string Unknown = "Unknown";

		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.Label lRegistration;
		private System.Windows.Forms.Label lPP_COMPS;
		private System.Windows.Forms.Label lGFR;
		private System.Windows.Forms.Label lDealIQ;
		private System.Windows.Forms.Label lMatchingModule;
		private System.Windows.Forms.Label lNetworkMode;
		private System.Windows.Forms.Label lAAHUbtegration;
		private System.Windows.Forms.Label lStatus;
        private Label label8;
        private Label label9;
        private Label lComm;
        private Label lSS;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public ActiveModulesForm()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();						
			InitControl();			
			
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ActiveModulesForm));
            this.button1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.lRegistration = new System.Windows.Forms.Label();
            this.lPP_COMPS = new System.Windows.Forms.Label();
            this.lGFR = new System.Windows.Forms.Label();
            this.lDealIQ = new System.Windows.Forms.Label();
            this.lMatchingModule = new System.Windows.Forms.Label();
            this.lNetworkMode = new System.Windows.Forms.Label();
            this.lAAHUbtegration = new System.Windows.Forms.Label();
            this.lStatus = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.lComm = new System.Windows.Forms.Label();
            this.lSS = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.button1.Location = new System.Drawing.Point(280, 8);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "Close";
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label1
            // 
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label1.Location = new System.Drawing.Point(16, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(152, 23);
            this.label1.TabIndex = 1;
            this.label1.Text = "Registration:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label2
            // 
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label2.Location = new System.Drawing.Point(16, 47);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(152, 23);
            this.label2.TabIndex = 2;
            this.label2.Text = "Property Profile and COMPS:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label3
            // 
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label3.Location = new System.Drawing.Point(16, 78);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(152, 23);
            this.label3.TabIndex = 3;
            this.label3.Text = "Get Forms Ready:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label4
            // 
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label4.Location = new System.Drawing.Point(16, 109);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(152, 23);
            this.label4.TabIndex = 4;
            this.label4.Text = "Deal IQ:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label5
            // 
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label5.Location = new System.Drawing.Point(16, 140);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(152, 23);
            this.label5.TabIndex = 5;
            this.label5.Text = "Matching Module:";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label6
            // 
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label6.Location = new System.Drawing.Point(16, 171);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(152, 23);
            this.label6.TabIndex = 6;
            this.label6.Text = "Network Mode:";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label7
            // 
            this.label7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label7.Location = new System.Drawing.Point(16, 202);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(152, 23);
            this.label7.TabIndex = 7;
            this.label7.Text = "Website Integration:";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lRegistration
            // 
            this.lRegistration.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.lRegistration.Location = new System.Drawing.Point(168, 16);
            this.lRegistration.Name = "lRegistration";
            this.lRegistration.Size = new System.Drawing.Size(72, 23);
            this.lRegistration.TabIndex = 8;
            this.lRegistration.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lPP_COMPS
            // 
            this.lPP_COMPS.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.lPP_COMPS.Location = new System.Drawing.Point(168, 48);
            this.lPP_COMPS.Name = "lPP_COMPS";
            this.lPP_COMPS.Size = new System.Drawing.Size(72, 23);
            this.lPP_COMPS.TabIndex = 9;
            this.lPP_COMPS.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lGFR
            // 
            this.lGFR.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.lGFR.Location = new System.Drawing.Point(168, 80);
            this.lGFR.Name = "lGFR";
            this.lGFR.Size = new System.Drawing.Size(72, 23);
            this.lGFR.TabIndex = 10;
            this.lGFR.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lDealIQ
            // 
            this.lDealIQ.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.lDealIQ.Location = new System.Drawing.Point(168, 112);
            this.lDealIQ.Name = "lDealIQ";
            this.lDealIQ.Size = new System.Drawing.Size(72, 23);
            this.lDealIQ.TabIndex = 11;
            this.lDealIQ.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lMatchingModule
            // 
            this.lMatchingModule.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.lMatchingModule.Location = new System.Drawing.Point(168, 144);
            this.lMatchingModule.Name = "lMatchingModule";
            this.lMatchingModule.Size = new System.Drawing.Size(72, 23);
            this.lMatchingModule.TabIndex = 12;
            this.lMatchingModule.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lNetworkMode
            // 
            this.lNetworkMode.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.lNetworkMode.Location = new System.Drawing.Point(168, 176);
            this.lNetworkMode.Name = "lNetworkMode";
            this.lNetworkMode.Size = new System.Drawing.Size(72, 23);
            this.lNetworkMode.TabIndex = 13;
            this.lNetworkMode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lAAHUbtegration
            // 
            this.lAAHUbtegration.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.lAAHUbtegration.Location = new System.Drawing.Point(168, 208);
            this.lAAHUbtegration.Name = "lAAHUbtegration";
            this.lAAHUbtegration.Size = new System.Drawing.Size(72, 23);
            this.lAAHUbtegration.TabIndex = 14;
            this.lAAHUbtegration.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lStatus
            // 
            this.lStatus.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lStatus.ForeColor = System.Drawing.Color.Red;
            this.lStatus.Location = new System.Drawing.Point(16, 289);
            this.lStatus.Name = "lStatus";
            this.lStatus.Size = new System.Drawing.Size(344, 23);
            this.lStatus.TabIndex = 15;
            this.lStatus.Text = "In order to Unlock new Features, please restart ProfitGrabber Pro";
            this.lStatus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label8
            // 
            this.label8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label8.Location = new System.Drawing.Point(16, 264);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(152, 23);
            this.label8.TabIndex = 17;
            this.label8.Text = "Short Sale Module:";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label9
            // 
            this.label9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label9.Location = new System.Drawing.Point(16, 233);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(152, 23);
            this.label9.TabIndex = 16;
            this.label9.Text = "Commercials Module:";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lComm
            // 
            this.lComm.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.lComm.Location = new System.Drawing.Point(168, 233);
            this.lComm.Name = "lComm";
            this.lComm.Size = new System.Drawing.Size(72, 23);
            this.lComm.TabIndex = 18;
            this.lComm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lSS
            // 
            this.lSS.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.lSS.Location = new System.Drawing.Point(168, 264);
            this.lSS.Name = "lSS";
            this.lSS.Size = new System.Drawing.Size(72, 23);
            this.lSS.TabIndex = 19;
            this.lSS.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ActiveModulesForm
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.ClientSize = new System.Drawing.Size(362, 321);
            this.Controls.Add(this.lSS);
            this.Controls.Add(this.lComm);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.lStatus);
            this.Controls.Add(this.lAAHUbtegration);
            this.Controls.Add(this.lNetworkMode);
            this.Controls.Add(this.lMatchingModule);
            this.Controls.Add(this.lDealIQ);
            this.Controls.Add(this.lGFR);
            this.Controls.Add(this.lPP_COMPS);
            this.Controls.Add(this.lRegistration);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ActiveModulesForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Active Modules Overview";
            this.ResumeLayout(false);

		}
		#endregion

		private void InitControl()
		{
			bool bRes = this.GetData();
			
			
			
			if (null != Globals.activeModules)
			{
				if (Globals.activeModules.MOD_0)
					this.lRegistration.Text = FeatureEnabled;
				else
					this.lRegistration.Text = Disabled;

				if (Globals.activeModules.MOD_1 && Globals.activeModules.MOD_2)
					this.lPP_COMPS.Text = Active;
				else
					this.lPP_COMPS.Text = NotActivated;

				if (Globals.activeModules.MOD_3)
					this.lGFR.Text = Active;
				else
					this.lGFR.Text = NotActivated;

				if (Globals.activeModules.MOD_4)
					this.lDealIQ.Text = Active;
				else
					this.lDealIQ.Text = NotActivated;

				if (Globals.activeModules.MOD_5)
					this.lMatchingModule.Text = Active;
				else
					this.lMatchingModule.Text = NotActivated;

				if (Globals.activeModules.MOD_6)
					this.lNetworkMode.Text = Active;
				else
					this.lNetworkMode.Text = NotActivated;

				if (Globals.activeModules.MOD_7)
					this.lAAHUbtegration.Text = Active;
				else
					this.lAAHUbtegration.Text = NotActivated;

                if (Globals.activeModules.MOD_8)
                    this.lSS.Text = Active;
                else
                    this.lSS.Text = NotActivated;

                if (Globals.activeModules.MOD_9)
                    this.lComm.Text = Active;
                else
                    this.lComm.Text = NotActivated;	
			
				if (true == Globals.activeModules.MOD_0 && true == bRes)
				{
					this.lStatus.Text = "In Order to Unlock new features, please restart ProfitGrabber Pro!";
				}
				else
				{
					this.lStatus.Text = "You do not have any new features installed!";
				}
				
			}
			else
			{
				this.lRegistration.Text = Unknown;
				this.lPP_COMPS.Text = Unknown;
				this.lGFR.Text = Unknown;
				this.lDealIQ.Text = Unknown;
				this.lMatchingModule.Text = Unknown;
				this.lNetworkMode.Text = Unknown;
				this.lAAHUbtegration.Text = Unknown;
				this.lStatus.Text = string.Empty;
			}
		}

		private bool GetData()
		{
			bool bRes = false;
			string msg = string.Empty;
			DealMaker.Registration.ActiveModules am = ApplicationHelper.GetActiveModulesFromWS(Globals.UserRegistration.IDUser, out msg);
			if (null != am && Globals.activeModules != am)
			{
				
				ApplicationHelper.SaveActiveModules(am);	
			
				if (
					am.MOD_0 != Globals.activeModules.MOD_0 ||
					am.MOD_1 != Globals.activeModules.MOD_1 ||
					am.MOD_2 != Globals.activeModules.MOD_2 ||
					am.MOD_3 != Globals.activeModules.MOD_3 ||
					am.MOD_4 != Globals.activeModules.MOD_4 ||
					am.MOD_5 != Globals.activeModules.MOD_5 ||
					am.MOD_6 != Globals.activeModules.MOD_6 ||
					am.MOD_7 != Globals.activeModules.MOD_7 
					)
				{
					bRes = true;
				}	
				Globals.activeModules = am;
			}
			return bRes;
		}

		private void button1_Click(object sender, System.EventArgs e)
		{
			this.Close();
		}
	}
}
