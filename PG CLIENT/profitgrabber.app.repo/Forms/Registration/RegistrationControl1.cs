using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;

namespace DealMaker
{
	/// <summary>
	/// Summary description for RegistrationControl1.
	/// </summary>
	public class RegistrationControl1 : System.Windows.Forms.UserControl
	{
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.RichTextBox rtbEula;
		private System.Windows.Forms.CheckBox cbAgreeToEULA;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public RegistrationControl1()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();
			this.cbAgreeToEULA.CheckStateChanged += new EventHandler(AgreeToEula);
			try
			{
				System.IO.StreamReader sr = new System.IO.StreamReader(Application.StartupPath + @"\PG EULA.txt");
				if (null != sr)
				{
					string line = string.Empty;
					while (sr.Peek() >= 0)
					{
						this.rtbEula.Text += sr.ReadLine() + System.Environment.NewLine;
					}
				
				}
			}
			catch (Exception exc)
			{
				this.rtbEula.Text = string.Empty;
				throw exc;	//TA:11.04.2005 - For saving exceptions that may come...
			}
		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.rtbEula = new System.Windows.Forms.RichTextBox();
			this.cbAgreeToEULA = new System.Windows.Forms.CheckBox();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label1.Location = new System.Drawing.Point(8, 8);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(360, 23);
			this.label1.TabIndex = 0;
			this.label1.Text = "Welcome to big Profits with ProfitGrabber Pro";
			// 
			// rtbEula
			// 
			this.rtbEula.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.rtbEula.BackColor = System.Drawing.SystemColors.InactiveBorder;
			this.rtbEula.ForeColor = System.Drawing.Color.Black;
			this.rtbEula.Location = new System.Drawing.Point(8, 72);
			this.rtbEula.Name = "rtbEula";
			this.rtbEula.ReadOnly = true;
			this.rtbEula.Size = new System.Drawing.Size(360, 128);
			this.rtbEula.TabIndex = 1;
			this.rtbEula.TabStop = false;
			this.rtbEula.Text = "";
			// 
			// cbAgreeToEULA
			// 
			this.cbAgreeToEULA.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.cbAgreeToEULA.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.cbAgreeToEULA.Location = new System.Drawing.Point(8, 208);
			this.cbAgreeToEULA.Name = "cbAgreeToEULA";
			this.cbAgreeToEULA.Size = new System.Drawing.Size(192, 24);
			this.cbAgreeToEULA.TabIndex = 3;
			this.cbAgreeToEULA.Text = "I Agree to Licence Agreement";
			// 
			// label2
			// 
			this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label2.Location = new System.Drawing.Point(8, 40);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(360, 32);
			this.label2.TabIndex = 4;
			this.label2.Text = "The registration process will take 5-10 minutes, and you need to be connected to " +
				"the Internet during the entire process!";
			// 
			// label3
			// 
			this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.label3.Location = new System.Drawing.Point(8, 248);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(352, 32);
			this.label3.TabIndex = 5;
			this.label3.Text = "By checking Yes, you confirm that you have read the agreement and fully understan" +
				"d and agree to its content.";
			// 
			// RegistrationControl1
			// 
			this.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(210)), ((System.Byte)(216)), ((System.Byte)(240)));
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.cbAgreeToEULA);
			this.Controls.Add(this.rtbEula);
			this.Controls.Add(this.label1);
			this.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.Name = "RegistrationControl1";
			this.Size = new System.Drawing.Size(376, 288);
			this.ResumeLayout(false);

		}
		#endregion
		
		private void AgreeToEula(object sender, EventArgs e)
		{
			CheckBox cb = (CheckBox)sender;
			if (true == cb.Checked)
			{
				Panel upperPanel = (Panel)this.Parent;
				RegistrationForm rf = (RegistrationForm)upperPanel.Parent;
				//rf.NextEnabled = true;
                rf.EnableNextAsync(true);
			}
			else
			{
				Panel upperPanel = (Panel)this.Parent;
				RegistrationForm rf = (RegistrationForm)upperPanel.Parent;
				//rf.NextEnabled = false;
                rf.EnableNextAsync(false);
			}
		}
	}
}
