using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using DealMaker.Registration;


namespace DealMaker
{
	/// <summary>
	/// Summary description for RegistrationForm.
	/// </summary>
	public class RegistrationForm : System.Windows.Forms.Form
	{
		private bool successFullRegistration = false;
		private System.Windows.Forms.Panel upperPanel;
		private System.Windows.Forms.Panel lowerPanel;
		private System.Windows.Forms.Button bNext;
		private System.Windows.Forms.Button bCancel;
		private System.Windows.Forms.Button bPrev;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public bool SuccessFullRegistration
		{
			get {return this.successFullRegistration; }
			set { this.successFullRegistration = value; }
		}

        delegate void EnableNextAsyncDelegate(bool value);
        public void EnableNextAsync(bool value)
        {
            if (InvokeRequired)
                BeginInvoke(new EnableNextAsyncDelegate(EnableNextAsync), new object[] { value });
            else
                this.bNext.Enabled = value;
        }

        //public bool NextEnabled 
        //{
        //    get {return this.bNext.Enabled;}
        //    set {this.bNext.Enabled = value;}
        //}

        delegate void SetNextStringAsyncDelegate(string text);
        public void SetNextStringAsync(string text)
        {
            if (InvokeRequired)
                BeginInvoke(new SetNextStringAsyncDelegate(SetNextStringAsync), new object[] { text });
            else
                this.bNext.Text = text;
        }

        //public string NextString
        //{
        //    get {return this.bNext.Text;}
        //    set {this.bNext.Text = value; }
        //}

        delegate void EnableCancelAsyncDelegate(bool value);
        public void EnableCancelAsync(bool value)
        {
            if (InvokeRequired)
                BeginInvoke(new EnableCancelAsyncDelegate(EnableCancelAsync), new object[] { value });
            else
                this.bCancel.Enabled = value;
        }

        //public bool CancelEnabled
        //{
        //    get {return this.bCancel.Enabled;}
        //    set { this.bCancel.Enabled = value; }
        //}

        delegate void EnablePrevAsyncDelegate(bool value);
        public void EnablePrevAsync(bool value)
        {
            if (InvokeRequired)
                BeginInvoke(new EnablePrevAsyncDelegate(EnablePrevAsync), new object[] { value });
            else
                this.bPrev.Enabled = value;
        }

        //public bool PrevEnabled
        //{
        //    get {return this.bPrev.Enabled; }
        //    set { this.bPrev.Enabled = value; }
        //}

		public RegistrationForm()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			if (true == ApplicationHelper.LoadRegistrationData())
				Globals.Registered = true;
			else
				Globals.Registered = false;
	
			this.bPrev.Enabled = false;
		
			this.bCancel.Click	+= new EventHandler(CancelWizard);
			this.bNext.Click	+= new EventHandler(NextStep);
			this.bPrev.Click	+= new EventHandler(PrevStep);

			RegistrationControl1 rc1 = new RegistrationControl1();
			rc1.Dock = DockStyle.Fill;
			this.upperPanel.Controls.Add(rc1);			
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(RegistrationForm));
			this.upperPanel = new System.Windows.Forms.Panel();
			this.lowerPanel = new System.Windows.Forms.Panel();
			this.bPrev = new System.Windows.Forms.Button();
			this.bCancel = new System.Windows.Forms.Button();
			this.bNext = new System.Windows.Forms.Button();
			this.lowerPanel.SuspendLayout();
			this.SuspendLayout();
			// 
			// upperPanel
			// 
			this.upperPanel.Dock = System.Windows.Forms.DockStyle.Top;
			this.upperPanel.Location = new System.Drawing.Point(0, 0);
			this.upperPanel.Name = "upperPanel";
			this.upperPanel.Size = new System.Drawing.Size(602, 328);
			this.upperPanel.TabIndex = 0;
			// 
			// lowerPanel
			// 
			this.lowerPanel.Controls.Add(this.bPrev);
			this.lowerPanel.Controls.Add(this.bCancel);
			this.lowerPanel.Controls.Add(this.bNext);
			this.lowerPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.lowerPanel.Location = new System.Drawing.Point(0, 328);
			this.lowerPanel.Name = "lowerPanel";
			this.lowerPanel.Size = new System.Drawing.Size(602, 48);
			this.lowerPanel.TabIndex = 1;
			// 
			// bPrev
			// 
			this.bPrev.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.bPrev.Enabled = false;
			this.bPrev.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.bPrev.Location = new System.Drawing.Point(424, 8);
			this.bPrev.Name = "bPrev";
			this.bPrev.TabIndex = 3;
			this.bPrev.Text = "Previous";
			// 
			// bCancel
			// 
			this.bCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.bCancel.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.bCancel.Location = new System.Drawing.Point(16, 8);
			this.bCancel.Name = "bCancel";
			this.bCancel.TabIndex = 2;
			this.bCancel.Text = "Cancel";
			// 
			// bNext
			// 
			this.bNext.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.bNext.Enabled = false;
			this.bNext.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.bNext.Location = new System.Drawing.Point(512, 8);
			this.bNext.Name = "bNext";
			this.bNext.TabIndex = 0;
			this.bNext.Text = "Next";
			// 
			// RegistrationForm
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(210)), ((System.Byte)(216)), ((System.Byte)(240)));
			this.ClientSize = new System.Drawing.Size(602, 376);
			this.Controls.Add(this.lowerPanel);
			this.Controls.Add(this.upperPanel);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "RegistrationForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Registration Form";
			this.Closing += new System.ComponentModel.CancelEventHandler(this.RegistrationForm_Closing);
			this.lowerPanel.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		private void CancelWizard(object sender, System.EventArgs e)
		{
			if (DialogResult.Yes == MessageBox.Show(this, "Are You sure you want to exit?", "Exit Registration",  MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2))
			{
				this.Close();
			}
			else
			{
				this.successFullRegistration = false;
				return;
			}
		}

		private void PrevStep(object sender, EventArgs e)
		{
			if (null == this.upperPanel || null == this.upperPanel.Controls || 0 == this.upperPanel.Controls.Count)
				return;
			if (this.upperPanel.Controls[0] is RegistrationControl1)
			{
				return;
			}

			if (this.upperPanel.Controls[0] is RegistrationControl2)
			{
				this.upperPanel.Controls.RemoveAt(0);
				RegistrationControl2 rc2 = new RegistrationControl2();
				rc2.Save();

				RegistrationControl1 rc1 = new RegistrationControl1();
				rc1.Dock = DockStyle.Fill;
				this.upperPanel.Controls.Add(rc1);
				this.bPrev.Enabled = false;
				return;
			}

			if (this.upperPanel.Controls[0] is RegistrationControl3)
			{
				this.upperPanel.Controls.RemoveAt(0);
				RegistrationControl2 rc2 = new RegistrationControl2();								
				rc2.Dock = DockStyle.Fill;
				this.upperPanel.Controls.Add(rc2);
				this.bPrev.Enabled = true;
				this.bNext.Enabled = true;
				this.bNext.Text = "Next";
				return;
			}	
		}


		private void NextStep(object sender, EventArgs e)
		{
			if (null == this.upperPanel || null == this.upperPanel.Controls || 0 == this.upperPanel.Controls.Count)
				return;
			if (this.upperPanel.Controls[0] is RegistrationControl1)
			{
				this.upperPanel.Controls.RemoveAt(0);
				RegistrationControl2 rc2 = new RegistrationControl2();
				rc2.Dock = DockStyle.Fill;
				this.upperPanel.Controls.Add(rc2);
				this.bPrev.Enabled = true;
				return;
			}
			if (this.upperPanel.Controls[0] is RegistrationControl2)
			{
				RegistrationControl2 rc2 = (RegistrationControl2)this.upperPanel.Controls[0];
				if (false == rc2.ValidEntries)
				{
					MessageBox.Show(this, "Please fill in the data in all fields", "Missing data", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
					return;
				}
				rc2.Save();
				this.upperPanel.Controls.RemoveAt(0);

				this.bNext.Enabled = false;
                
				DealMaker.Registration.User user = new DealMaker.Registration.User();
				user.FirstName		= rc2.FirstName;
				user.LastName		= rc2.LastName;				
				user.MiddleName		= string.Empty;

				DealMaker.Registration.Address address = new DealMaker.Registration.Address();
				//SoftwareRegistration.DataClasses.Address address = new SoftwareRegistration.DataClasses.Address();
				address.Company 					= rc2.UserCompanyName;
				address.EMail						= rc2.Email;
				address.FullSiteStreetNumber		= rc2.FullStreetMailingAddress;
				//address.IDAddress					
				address.PhoneBusiness				= rc2.BusinessPhone;
				address.PhoneCell					= rc2.CellNumber;
				address.SiteCity					= rc2.City;
				address.SiteCitySiteState			= string.Empty;
				address.SiteCitySiteStateSiteZIP	= rc2.HomePhone; // TA: 22.06.2005. overridden mapping *new user requirements, old DB design*
				address.SiteCountry					= string.Empty;
				address.SiteState					= rc2.State;
				address.SiteZIP						= rc2.ZIP;
				address.SiteStreetName				= string.Empty;
				address.SiteStreetNumber			= string.Empty;
				address.SiteStreetPostDirectional	= string.Empty;
				address.SiteStreetPreDirectional	= string.Empty;
				address.SiteStreetSuffix			= string.Empty;
				address.SiteStreetUnitNumber		= string.Empty;				

				user.Address = address;
												
				DealMaker.Registration.Registration reg = new DealMaker.Registration.Registration(); 
				reg.User = user;				
				reg.KeyCode = rc2.Serial1 + "-" + rc2.Serial2 + "-"+ rc2.Serial3 + "-" + rc2.Serial4;
				reg.DateActivation = DateTime.Now;			
				//reg.LicenceType
				//reg.Product
				//reg.VolumeNumber


				//reg.KeyCode
				//reg.User																				
				

				//u = new SoftwareRegistration.DataClasses.User();

				//SoftwareRegistration.ClassLibrary.WebServiceCore wsc;

				//wsc = new SoftwareRegistration.ClassLibrary.WebServiceCore();

				//IList ilRes =  wsc.RegisterUser(u);

				RegistrationControl3 rc3 = new RegistrationControl3(reg);
				rc3.Dock = DockStyle.Fill;
				this.upperPanel.Controls.Add(rc3);
				this.bPrev.Enabled = true;				
				return;

			}

			if (this.upperPanel.Controls[0] is RegistrationControl3)
			{
				this.Close();
				return;
			}
		}

		private void RegistrationForm_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			e.Cancel = false;
			if (this.upperPanel.Controls[0] is RegistrationControl3)
			{
				RegistrationControl3 rc3 = (RegistrationControl3)this.upperPanel.Controls[0];
				if (System.Threading.ThreadState.Stopped != rc3.Worker.ThreadState)
					e.Cancel = true;
				else
					e.Cancel = false;
			} 			
		}
	}
}
