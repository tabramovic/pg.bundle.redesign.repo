using System;
using System.IO;
using System.Reflection;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;
using System.Runtime.Serialization.Formatters.Binary;

namespace DealMaker
{
	/// <summary>
	/// Summary description for RegistrationControl2.
	/// </summary>
	public class RegistrationControl2 : System.Windows.Forms.UserControl
	{
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.Label label13;
		private System.Windows.Forms.Label label14;
		private System.Windows.Forms.Label label15;
		private System.Windows.Forms.Label label16;
		private System.Windows.Forms.Label label17;
		private System.Windows.Forms.Label label18;
		private System.Windows.Forms.Label label19;
		private System.Windows.Forms.TextBox tbFirstName;
		private System.Windows.Forms.TextBox tbLastName;
		private System.Windows.Forms.TextBox tbCompanyName;
		private System.Windows.Forms.TextBox tbFullStreetMailingAddress;
		private System.Windows.Forms.TextBox tbCity;
		private System.Windows.Forms.TextBox tbState;
		private System.Windows.Forms.TextBox tbHomePhone;
		private System.Windows.Forms.TextBox tbBusinessPhone;
		private System.Windows.Forms.TextBox tbEmail;
		private System.Windows.Forms.TextBox tbSerial1;
		private System.Windows.Forms.TextBox tbSerial2;
		private System.Windows.Forms.TextBox tbSerial3;
		private System.Windows.Forms.TextBox tbSerial4;
		private System.Windows.Forms.TextBox tbCellNumber;
		private System.Windows.Forms.Label label20;
		private System.Windows.Forms.TextBox tbZIP;
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public RegistrationControl2()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

			Hashtable htLoadedData = this.LoadData();
			if (0 != htLoadedData.Count)
			this.SetLoadedData(htLoadedData);

			this.tbFirstName.Enter					+= new EventHandler(EnterTextBox);
			this.tbLastName.Enter					+= new EventHandler(EnterTextBox);
			this.tbCompanyName.Enter				+= new EventHandler(EnterTextBox);
			this.tbFullStreetMailingAddress.Enter	+= new EventHandler(EnterTextBox);
			this.tbCity.Enter						+= new EventHandler(EnterTextBox);
			this.tbState.Enter 						+= new EventHandler(EnterTextBox);
			this.tbZIP.Enter						+= new EventHandler(EnterTextBox);
			this.tbHomePhone.Enter 					+= new EventHandler(EnterTextBox);
			this.tbBusinessPhone.Enter 				+= new EventHandler(EnterTextBox);
			this.tbCellNumber.Enter 				+= new EventHandler(EnterTextBox);
			this.tbEmail.Enter 						+= new EventHandler(EnterTextBox);
			this.tbSerial1.Enter 					+= new EventHandler(EnterTextBox);
			this.tbSerial2.Enter 					+= new EventHandler(EnterTextBox);
			this.tbSerial3.Enter 					+= new EventHandler(EnterTextBox);
			this.tbSerial4.Enter 					+= new EventHandler(EnterTextBox);
			
			
			this.tbFirstName.Leave					+= new EventHandler(LeaveTextBox);						
			this.tbLastName.Leave					+= new EventHandler(LeaveTextBox);						
			this.tbCompanyName.Leave				+= new EventHandler(LeaveTextBox);						
			this.tbFullStreetMailingAddress.Leave	+= new EventHandler(LeaveTextBox);						
			this.tbCity.Leave						+= new EventHandler(LeaveTextBox);						
			this.tbState.Leave						+= new EventHandler(LeaveTextBox);
			this.tbZIP.Leave						+= new EventHandler(LeaveTextBox);
			this.tbHomePhone.Leave					+= new EventHandler(LeaveTextBox);						
			this.tbBusinessPhone.Leave				+= new EventHandler(LeaveTextBox);						
			this.tbCellNumber.Leave					+= new EventHandler(LeaveTextBox);						
			this.tbEmail.Leave						+= new EventHandler(LeaveTextBox);						
			this.tbSerial1.Leave 					+= new EventHandler(LeaveTextBox);						
			this.tbSerial2.Leave 					+= new EventHandler(LeaveTextBox);						
			this.tbSerial3.Leave 					+= new EventHandler(LeaveTextBox);						
			this.tbSerial4.Leave 					+= new EventHandler(LeaveTextBox);
		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.tbFirstName = new System.Windows.Forms.TextBox();
            this.tbLastName = new System.Windows.Forms.TextBox();
            this.tbCompanyName = new System.Windows.Forms.TextBox();
            this.tbFullStreetMailingAddress = new System.Windows.Forms.TextBox();
            this.tbCity = new System.Windows.Forms.TextBox();
            this.tbState = new System.Windows.Forms.TextBox();
            this.tbHomePhone = new System.Windows.Forms.TextBox();
            this.tbBusinessPhone = new System.Windows.Forms.TextBox();
            this.tbEmail = new System.Windows.Forms.TextBox();
            this.tbSerial1 = new System.Windows.Forms.TextBox();
            this.tbSerial2 = new System.Windows.Forms.TextBox();
            this.tbSerial3 = new System.Windows.Forms.TextBox();
            this.tbSerial4 = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.tbCellNumber = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.tbZIP = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(8, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(136, 23);
            this.label1.TabIndex = 0;
            this.label1.Text = "Step 1: Registration";
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(8, 32);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(160, 23);
            this.label2.TabIndex = 1;
            this.label2.Text = "Enter your contact information:";
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(8, 56);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(472, 23);
            this.label3.TabIndex = 2;
            this.label3.Text = "Accuracy of your contact information is extremely important in order to take";
            this.label3.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(8, 80);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(472, 23);
            this.label4.TabIndex = 3;
            this.label4.Text = "the full advantage of all the ProfitGrabber-Pro features! ";
            this.label4.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(8, 104);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(472, 23);
            this.label5.TabIndex = 4;
            this.label5.Text = "Your information will be kept strictly confidential!";
            this.label5.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(8, 130);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(64, 23);
            this.label6.TabIndex = 5;
            this.label6.Text = "First Name:";
            // 
            // label7
            // 
            this.label7.Location = new System.Drawing.Point(264, 130);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(64, 23);
            this.label7.TabIndex = 6;
            this.label7.Text = "Last Name:";
            // 
            // label8
            // 
            this.label8.Location = new System.Drawing.Point(8, 154);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(96, 23);
            this.label8.TabIndex = 7;
            this.label8.Text = "Company Name:";
            // 
            // label9
            // 
            this.label9.Location = new System.Drawing.Point(8, 184);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(144, 23);
            this.label9.TabIndex = 8;
            this.label9.Text = "Full Street Mailing Address:";
            // 
            // label10
            // 
            this.label10.Location = new System.Drawing.Point(112, 208);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(32, 23);
            this.label10.TabIndex = 9;
            this.label10.Text = "City:";
            // 
            // label11
            // 
            this.label11.Location = new System.Drawing.Point(256, 208);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(32, 23);
            this.label11.TabIndex = 10;
            this.label11.Text = "State:";
            // 
            // label12
            // 
            this.label12.Location = new System.Drawing.Point(8, 240);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(120, 23);
            this.label12.TabIndex = 11;
            this.label12.Text = "Home Phone Number:";
            // 
            // label13
            // 
            this.label13.Location = new System.Drawing.Point(232, 240);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(136, 23);
            this.label13.TabIndex = 12;
            this.label13.Text = "Business Phone Number:";
            // 
            // label14
            // 
            this.label14.Location = new System.Drawing.Point(8, 264);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(100, 23);
            this.label14.TabIndex = 13;
            this.label14.Text = "Cell Number:";
            // 
            // label15
            // 
            this.label15.Location = new System.Drawing.Point(232, 264);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(40, 23);
            this.label15.TabIndex = 14;
            this.label15.Text = "Email:";
            // 
            // label16
            // 
            this.label16.Location = new System.Drawing.Point(8, 296);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(176, 23);
            this.label16.TabIndex = 15;
            this.label16.Text = "ProfitGrabber-Pro Serial Number:";
            // 
            // tbFirstName
            // 
            this.tbFirstName.Location = new System.Drawing.Point(104, 128);
            this.tbFirstName.MaxLength = 32;
            this.tbFirstName.Name = "tbFirstName";
            this.tbFirstName.Size = new System.Drawing.Size(152, 20);
            this.tbFirstName.TabIndex = 16;
            // 
            // tbLastName
            // 
            this.tbLastName.Location = new System.Drawing.Point(328, 128);
            this.tbLastName.MaxLength = 32;
            this.tbLastName.Name = "tbLastName";
            this.tbLastName.Size = new System.Drawing.Size(152, 20);
            this.tbLastName.TabIndex = 17;
            // 
            // tbCompanyName
            // 
            this.tbCompanyName.Location = new System.Drawing.Point(104, 152);
            this.tbCompanyName.MaxLength = 32;
            this.tbCompanyName.Name = "tbCompanyName";
            this.tbCompanyName.Size = new System.Drawing.Size(152, 20);
            this.tbCompanyName.TabIndex = 18;
            // 
            // tbFullStreetMailingAddress
            // 
            this.tbFullStreetMailingAddress.Location = new System.Drawing.Point(152, 182);
            this.tbFullStreetMailingAddress.MaxLength = 32;
            this.tbFullStreetMailingAddress.Name = "tbFullStreetMailingAddress";
            this.tbFullStreetMailingAddress.Size = new System.Drawing.Size(328, 20);
            this.tbFullStreetMailingAddress.TabIndex = 19;
            // 
            // tbCity
            // 
            this.tbCity.Location = new System.Drawing.Point(152, 208);
            this.tbCity.MaxLength = 32;
            this.tbCity.Name = "tbCity";
            this.tbCity.Size = new System.Drawing.Size(96, 20);
            this.tbCity.TabIndex = 20;
            // 
            // tbState
            // 
            this.tbState.Location = new System.Drawing.Point(296, 206);
            this.tbState.MaxLength = 32;
            this.tbState.Name = "tbState";
            this.tbState.Size = new System.Drawing.Size(80, 20);
            this.tbState.TabIndex = 21;
            // 
            // tbHomePhone
            // 
            this.tbHomePhone.Location = new System.Drawing.Point(128, 236);
            this.tbHomePhone.MaxLength = 32;
            this.tbHomePhone.Name = "tbHomePhone";
            this.tbHomePhone.Size = new System.Drawing.Size(104, 20);
            this.tbHomePhone.TabIndex = 23;
            // 
            // tbBusinessPhone
            // 
            this.tbBusinessPhone.Location = new System.Drawing.Point(376, 240);
            this.tbBusinessPhone.MaxLength = 32;
            this.tbBusinessPhone.Name = "tbBusinessPhone";
            this.tbBusinessPhone.Size = new System.Drawing.Size(104, 20);
            this.tbBusinessPhone.TabIndex = 24;
            // 
            // tbEmail
            // 
            this.tbEmail.Location = new System.Drawing.Point(272, 264);
            this.tbEmail.MaxLength = 64;
            this.tbEmail.Name = "tbEmail";
            this.tbEmail.Size = new System.Drawing.Size(208, 20);
            this.tbEmail.TabIndex = 26;
            // 
            // tbSerial1
            // 
            this.tbSerial1.Location = new System.Drawing.Point(184, 293);
            this.tbSerial1.MaxLength = 4;
            this.tbSerial1.Name = "tbSerial1";
            this.tbSerial1.Size = new System.Drawing.Size(56, 20);
            this.tbSerial1.TabIndex = 27;
            this.tbSerial1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbSerial2
            // 
            this.tbSerial2.Location = new System.Drawing.Point(264, 293);
            this.tbSerial2.MaxLength = 4;
            this.tbSerial2.Name = "tbSerial2";
            this.tbSerial2.Size = new System.Drawing.Size(56, 20);
            this.tbSerial2.TabIndex = 28;
            this.tbSerial2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbSerial3
            // 
            this.tbSerial3.Location = new System.Drawing.Point(344, 293);
            this.tbSerial3.MaxLength = 4;
            this.tbSerial3.Name = "tbSerial3";
            this.tbSerial3.Size = new System.Drawing.Size(56, 20);
            this.tbSerial3.TabIndex = 29;
            this.tbSerial3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbSerial4
            // 
            this.tbSerial4.Location = new System.Drawing.Point(424, 293);
            this.tbSerial4.MaxLength = 4;
            this.tbSerial4.Name = "tbSerial4";
            this.tbSerial4.Size = new System.Drawing.Size(56, 20);
            this.tbSerial4.TabIndex = 30;
            this.tbSerial4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label17
            // 
            this.label17.Location = new System.Drawing.Point(328, 295);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(8, 23);
            this.label17.TabIndex = 29;
            this.label17.Text = "-";
            // 
            // label18
            // 
            this.label18.Location = new System.Drawing.Point(248, 295);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(8, 23);
            this.label18.TabIndex = 30;
            this.label18.Text = "-";
            // 
            // label19
            // 
            this.label19.Location = new System.Drawing.Point(408, 295);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(8, 23);
            this.label19.TabIndex = 31;
            this.label19.Text = "-";
            // 
            // tbCellNumber
            // 
            this.tbCellNumber.Location = new System.Drawing.Point(128, 264);
            this.tbCellNumber.MaxLength = 32;
            this.tbCellNumber.Name = "tbCellNumber";
            this.tbCellNumber.Size = new System.Drawing.Size(104, 20);
            this.tbCellNumber.TabIndex = 25;
            // 
            // label20
            // 
            this.label20.Location = new System.Drawing.Point(384, 208);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(32, 23);
            this.label20.TabIndex = 32;
            this.label20.Text = "ZIP:";
            // 
            // tbZIP
            // 
            this.tbZIP.Location = new System.Drawing.Point(416, 208);
            this.tbZIP.MaxLength = 32;
            this.tbZIP.Name = "tbZIP";
            this.tbZIP.Size = new System.Drawing.Size(64, 20);
            this.tbZIP.TabIndex = 22;
            // 
            // RegistrationControl2
            // 
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.Controls.Add(this.tbZIP);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.tbCellNumber);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.tbSerial4);
            this.Controls.Add(this.tbSerial3);
            this.Controls.Add(this.tbSerial2);
            this.Controls.Add(this.tbSerial1);
            this.Controls.Add(this.tbEmail);
            this.Controls.Add(this.tbBusinessPhone);
            this.Controls.Add(this.tbHomePhone);
            this.Controls.Add(this.tbState);
            this.Controls.Add(this.tbCity);
            this.Controls.Add(this.tbFullStreetMailingAddress);
            this.Controls.Add(this.tbCompanyName);
            this.Controls.Add(this.tbLastName);
            this.Controls.Add(this.tbFirstName);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.Name = "RegistrationControl2";
            this.Size = new System.Drawing.Size(488, 328);
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		#endregion


		public string FirstName
		{
			get {return this.tbFirstName.Text;}
			set {this.tbFirstName.Text = value;}
		}

		public string LastName
		{
			get {return this.tbLastName.Text;}
			set {this.tbLastName.Text = value;}
		}

		public string UserCompanyName
		{
			get {return this.tbCompanyName.Text;}
			set {this.tbCompanyName.Text = value;}
		}

		public string FullStreetMailingAddress
		{
			get {return this.tbFullStreetMailingAddress.Text;}
			set {this.tbFullStreetMailingAddress.Text = value;}
		}

		public string City
		{
			get {return this.tbCity.Text;}
			set {this.tbCity.Text = value;}
		}

		public string State
		{
			get {return this.tbState.Text;}
			set {this.tbState.Text = value;}
		}

		public string ZIP
		{
			get {return this.tbZIP.Text;}
			set {this.tbZIP.Text = value;}
		}

		public string HomePhone
		{
			get {return this.tbHomePhone.Text;}
			set {this.tbHomePhone.Text = value;}
		}

		public string BusinessPhone
		{
			get {return this.tbBusinessPhone.Text;}
			set {this.tbBusinessPhone.Text = value;}
		}

		public string CellNumber
		{
			get {return this.tbCellNumber.Text;}
			set {this.tbCellNumber.Text = value;}
		}

		public string Email
		{
			get {return this.tbEmail.Text;}
			set {this.tbEmail.Text = value;}
		}

		public string Serial1
		{
			get {return this.tbSerial1.Text;}
			set {this.tbSerial1.Text = value;}
		}

		public string Serial2
		{
			get {return this.tbSerial2.Text;}
			set {this.tbSerial2.Text = value;}
		}

		public string Serial3
		{
			get {return this.tbSerial3.Text;}
			set {this.tbSerial3.Text = value;}
		}

		public string Serial4
		{
			get {return this.tbSerial4.Text;}
			set {this.tbSerial4.Text = value;}
		}

		public bool ValidEntries
		{
			get
			{
				if (
						string.Empty != FirstName
					&&	string.Empty != LastName
					&&  string.Empty != UserCompanyName
					&&  string.Empty != FullStreetMailingAddress
					&&  string.Empty != City
					&&  string.Empty != State
					&&  string.Empty != ZIP
					&&  string.Empty != HomePhone
					&&  string.Empty != BusinessPhone
					&&  string.Empty != CellNumber
					&&  string.Empty != Email
					&&  string.Empty != Serial1
					&&  string.Empty != Serial2
					&&  string.Empty != Serial3
					&&  string.Empty != Serial4
					)
					return true;
				else
					return false;
			}
		}

		
		
		#region � FillHashtable �
		private Hashtable FillHashtable()
		{
			Hashtable htItems = new Hashtable();			
			PropertyInfo[] pi = this.GetType().GetProperties();
			int i = 0;
			try
			{				
				for (i = 0; i < pi.Length; i++)			
				{
					PropertyInfo propInfo = (PropertyInfo)pi[i];
					Type t = pi[i].PropertyType;
					if (t == typeof(System.String))
					{
						if (false == htItems.Contains(propInfo.Name))
						{
							if (propInfo.MemberType == MemberTypes.Property)
								htItems.Add(propInfo.Name, (string)propInfo.GetValue(this, null));						
						}						
					}
				}
			}
			catch (Exception exc)
			{
				MessageBox.Show(exc.Message + ", Inner Exc: "  + exc.InnerException + ", Source: " + exc.Source + " icnt = " + i.ToString());
				throw exc;	//TA:11.04.2005 - For saving exceptions that may come...
			}
			return htItems;
		}
		#endregion � FillHashtable �

		public void Save()
		{
			Hashtable htToSave =  this.FillHashtable();
            if (false == Directory.Exists(OSFolderManager.Instance.GetPlainLocalUserAppDataPath()))
			{
                Directory.CreateDirectory(OSFolderManager.Instance.GetPlainLocalUserAppDataPath());
			}
            FileStream fs = new FileStream(OSFolderManager.Instance.GetPlainLocalUserAppDataPath() + @"\user.dat", FileMode.OpenOrCreate, FileAccess.Write);
			try
			{				
				BinaryFormatter bf=new BinaryFormatter ();
				bf.Serialize (fs, htToSave);
			}
			catch (Exception exc)
			{
				MessageBox.Show(exc.Message);
				throw exc;	//TA:11.04.2005 - For saving exceptions that may come...
			}
			finally
			{
				fs.Close ();
			}
		}
		
		
		public Hashtable LoadData()
		{
			Hashtable htLoadedData = new Hashtable();
			FileStream fs = null;
            if (false == Directory.Exists(OSFolderManager.Instance.GetPlainLocalUserAppDataPath()))
			{
				return htLoadedData;
			}
			try
			{
                fs = new FileStream(OSFolderManager.Instance.GetPlainLocalUserAppDataPath() + @"\user.dat", FileMode.Open, FileAccess.Read);
			}
			catch (Exception)
			{
				return htLoadedData;				
			}
			if (null != fs)
			{
				try
				{					
					BinaryFormatter bf=new BinaryFormatter ();
					htLoadedData = (Hashtable)bf.Deserialize(fs);					
				}
				catch(Exception exc)				
				{
					MessageBox.Show(exc.ToString());
					throw exc;	//TA:11.04.2005 - For saving exceptions that may come...
				}
				finally
				{
					fs.Close ();
				}									
			}
			return htLoadedData;
		}
		
		
		private void SetLoadedData(Hashtable htLoadedData)
		{					
			int i = 0;
			PropertyInfo[] pi = this.GetType().GetProperties();
			
			for (i = 0; i < pi.Length; i++)			
			{
				try
				{
					PropertyInfo propInfo = (PropertyInfo)pi[i];
					Type t = pi[i].PropertyType;
					if (t == typeof(System.String))
					{
						string savedData = (string)htLoadedData[propInfo.Name];	
						if (null == savedData)
							savedData = string.Empty;
						if (true == propInfo.CanWrite)
						{
							propInfo.SetValue(this, savedData, null);					
						}
					}
				}
				catch(Exception exc)
				{
					MessageBox.Show(i.ToString() + " "  +exc.ToString());					
					continue;
				}
			}
		}		
		


		private void EnterTextBox(object sender, EventArgs e)
		{
			TextBox tb = (TextBox)sender;
			tb.BackColor = Color.LightBlue;
		}

		private void LeaveTextBox(object sender, EventArgs e)
		{
			TextBox tb = (TextBox)sender;
			tb.BackColor = Color.White;
		}
	}
}
