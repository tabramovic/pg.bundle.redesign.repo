using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;
using System.Threading;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Collections.Specialized;
using System.Configuration;
using System.Diagnostics;
using System.ServiceProcess;
using Microsoft.Win32;

//Added
using DealMaker.Registration;

namespace DealMaker
{
	/// <summary>
	/// Summary description for RegistrationControl3.
	/// </summary>
	public class RegistrationControl3 : System.Windows.Forms.UserControl
	{
        bool _loaded = false;
        string message = string.Empty;
		private Thread worker = null;
		private DealMaker.Registration.Registration registration = null;
		private System.Windows.Forms.PictureBox pbAnimated;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TextBox tbResult;
		private System.Windows.Forms.PictureBox pictureBox1;
		private System.Windows.Forms.TextBox textBox1;
		private System.Windows.Forms.TextBox textBox2;
		private System.Windows.Forms.TextBox textBox3;
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public Thread Worker
		{
			get { return this.worker; }
			set { this.worker = value; }
		}


		public RegistrationControl3(DealMaker.Registration.Registration registration)
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

			this.registration = registration;	
			
			this.worker = new Thread(new ThreadStart(WorkerThread));			
			worker.Start();			
		}

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            _loaded = true;
        }

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RegistrationControl3));
            this.pbAnimated = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.tbResult = new System.Windows.Forms.TextBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.pbAnimated)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // pbAnimated
            // 
            this.pbAnimated.Image = ((System.Drawing.Image)(resources.GetObject("pbAnimated.Image")));
            this.pbAnimated.Location = new System.Drawing.Point(8, 88);
            this.pbAnimated.Name = "pbAnimated";
            this.pbAnimated.Size = new System.Drawing.Size(96, 96);
            this.pbAnimated.TabIndex = 0;
            this.pbAnimated.TabStop = false;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(8, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(288, 23);
            this.label1.TabIndex = 1;
            this.label1.Text = "Step 1: Waiting for Confirmation from Server";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.Location = new System.Drawing.Point(8, 40);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(400, 40);
            this.label2.TabIndex = 2;
            this.label2.Text = "A timeout to server is set to 90 secs. If server does NOT respond - registration " +
                "is not successful.";
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(112, 88);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(48, 23);
            this.label3.TabIndex = 4;
            this.label3.Text = "Result:";
            // 
            // tbResult
            // 
            this.tbResult.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tbResult.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.tbResult.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbResult.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.tbResult.Location = new System.Drawing.Point(160, 88);
            this.tbResult.Multiline = true;
            this.tbResult.Name = "tbResult";
            this.tbResult.ReadOnly = true;
            this.tbResult.Size = new System.Drawing.Size(240, 64);
            this.tbResult.TabIndex = 5;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.BackgroundImage")));
            this.pictureBox1.Location = new System.Drawing.Point(336, 216);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(72, 72);
            this.pictureBox1.TabIndex = 6;
            this.pictureBox1.TabStop = false;
            // 
            // textBox1
            // 
            this.textBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.textBox1.Location = new System.Drawing.Point(8, 211);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(216, 16);
            this.textBox1.TabIndex = 7;
            this.textBox1.Text = "PROFITGRABBER SUPPORT INFO:";
            this.textBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox2
            // 
            this.textBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.textBox2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.textBox2.Location = new System.Drawing.Point(8, 240);
            this.textBox2.Multiline = true;
            this.textBox2.Name = "textBox2";
            this.textBox2.ReadOnly = true;
            this.textBox2.Size = new System.Drawing.Size(216, 16);
            this.textBox2.TabIndex = 8;
            this.textBox2.Text = "support@profitgrabber.com";
            this.textBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox3
            // 
            this.textBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.textBox3.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.textBox3.Location = new System.Drawing.Point(8, 258);
            this.textBox3.Multiline = true;
            this.textBox3.Name = "textBox3";
            this.textBox3.ReadOnly = true;
            this.textBox3.Size = new System.Drawing.Size(216, 16);
            this.textBox3.TabIndex = 9;
            this.textBox3.Text = "858-633-8223 (Monday - Friday)";
            this.textBox3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // RegistrationControl3
            // 
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.Controls.Add(this.textBox3);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.tbResult);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pbAnimated);
            this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.Name = "RegistrationControl3";
            this.Size = new System.Drawing.Size(416, 296);
            ((System.ComponentModel.ISupportInitialize)(this.pbAnimated)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		#endregion

        delegate void WorkerThreadDelegate();
		public void WorkerThread()
		{
            while (!_loaded)
                Thread.Sleep(1000);

            if (InvokeRequired)
            {
                BeginInvoke(new WorkerThreadDelegate(WorkerThread), null);
            }
            else            
            {
                RegistrationForm rf = null;
			    try
			    {
				    Panel p = this.Parent as Panel;
                    
                    if (null != p)
				        rf = (RegistrationForm)p.Parent;

                    if (null != rf)
                    {                        
                        rf.EnableNextAsync(false);
                        rf.EnablePrevAsync(false);
                        rf.EnableCancelAsync(false); 
                    }
			    }
			    catch (Exception exc)
			    {
				    throw new Exception("Panel2RegForm", exc);
			    }

			    if (null == rf)
			    {
				    throw new Exception("RegistrationForm is null");
			    }
    			
			    DealMaker.Registration.Service1 service = null;
			    try
			    {
				    service = new DealMaker.Registration.Service1();
			    }
			    catch (Exception exc)
			    {
    #if DEBUG
				    MessageBox.Show("Message: " + exc.ToString() + "\nInner Exception: " + exc.InnerException);
    #endif
				    throw new Exception("service = new DealMaker.Registration.Service1()",  exc);	//TA:11.04.2005 - For saving exceptions that may come...
			    }
			    service.Timeout = 90000;
    			
			    //read from app.config			
			    try
			    {
				    NameValueCollection nvc = (NameValueCollection)ConfigurationSettings.GetConfig("ProfitGrabberWebSupport");
				    if (null != nvc && nvc.Count != 0)
				    {
					    Globals.ProfitGrabberWebSupportURL = (string)nvc["URL"];
					    service.Url = (string)nvc["URL"];				
				    }
			    }
			    catch (Exception exc)
			    {
				    throw new Exception("Reading from app.Config",  exc);	//TA:11.04.2005 - For saving exceptions that may come...
			    }
    			
    						
			    IList ilRes = null;
			    try
			    {
				    ilRes =  service.RegisterUser(this.registration.User, this.registration, out message);
				    this.tbResult.Text = message;
			    }
			    catch (Exception exc)
			    {
				    this.tbResult.Text = "Registration Server did not respond. Check Your connection!";
    #if DEBUG
				    string s = exc.Message;
				    MessageBox.Show("Exc.: \n\t" + exc.Message + "\nInner exc: \n\t" + exc.InnerException);
    #endif
				    throw new Exception("Register User: ", exc);	//TA:11.04.2005 - For saving exceptions that may come...
			    }
			    finally
			    {
                    rf.EnableNextAsync(true);
                    rf.SetNextStringAsync("Finish");
                    //rf.NextEnabled = true;
				    //rf.NextString = "Finish";

                    rf.EnablePrevAsync(true);
				    //rf.PrevEnabled = true;
                    rf.EnableCancelAsync(true);
				    //rf.CancelEnabled = true;
			    }


			    if (null != ilRes && 0 != ilRes.Count)
			    {
				    //success
				    //
				    string strResult = ilRes[0] as string;
				    if (null != strResult && "Successfull!" == strResult)
				    {
					    rf.SuccessFullRegistration = true;
					    Globals.Registered = true;
    					
					    this.tbResult.Text = "Registration Successful!" + System.Environment.NewLine + System.Environment.NewLine + 
						    "Click on Finish and then when ProfitGrabber starts, go to Help menu and watch the Quick Start Webinars."						
						    ;

					    //start iexplore
					    DealMaker.Registration.User user = null;
					    if (null != ilRes[1])
					    {
						    user = ilRes[1] as DealMaker.Registration.User;
						    if (null != user)
						    {	
							    ApplicationHelper.SaveRegistrationData(rf.SuccessFullRegistration, user, this.registration);
							    string idUser = user.IDUser.ToString();							
							    //Process.Start("IExplore.exe", "http://www.profitgrabber.com/account/subscribe_new.cfm?IDUser={" + idUser + "}");
						    }
					    }

					    //TA++ 05.03.2006. (active modules)
					    string msg = string.Empty;
					    ActiveModules am = ApplicationHelper.GetActiveModulesFromWS(user.IDUser, service, out msg);
					    if (null == am)
					    {
						    //CREATE DEFAULT AM !!!
						    am = new ActiveModules();
						    am.IDRegistration = this.registration.IDRegistration;
						    am.MOD_0 = true;
						    am.MOD_1 = am.MOD_2 = am.MOD_3 = am.MOD_4 = am.MOD_5 = am.MOD_6 = am.MOD_7  = am.MOD_8 = am.MOD_9 = am.MOD_10 = false;
						    am.MOD_11 = am.MOD_12 = am.MOD_13 = am.MOD_14 = am.MOD_15 = am.MOD_16 = am.MOD_17 = am.MOD_18 = am.MOD_19 = am.MOD_20 = false;																		
					    }
					    //SET THEM MEMORY RESIDENT
					    Globals.activeModules = am;
					    //SAVE TO FILE
					    ApplicationHelper.SaveActiveModules(am);
    									
    					
					    // we do not start IE if binary is a NW CLI
					    if (null != am && false == am.MOD_6)
					    {
						    string idUser = user.IDUser.ToString();							
							Process.Start("http://www.profitgrabber.com/account/subscribe_new.cfm?IDUser={" + idUser + "}");                            
					    }

    					

					    //***Network Version Client***
    											
					    //PGDatabaseSelectService
					    ServiceControllerEx scePGDBSelectService = null;
					    try
					    {
						    //Disable PGDatabaseSelectService (not started anyway by the installer)
						    scePGDBSelectService = new ServiceControllerEx(Globals.PGDatabaseSelectService);

						    if (true == am.MOD_6)	//CLIENT
						    {
							    //stop if running
							    if (scePGDBSelectService.Status.Equals(ServiceControllerStatus.Running))
								    scePGDBSelectService.Stop();
							    //Change startup type to disabled
							    if (scePGDBSelectService.StartupType != "Disabled")						
								    scePGDBSelectService.StartupType = "Disabled";
						    }
						    else					//SERVER
						    {
							    //Change startup type to automatic
							    if (scePGDBSelectService.StartupType != "Automatic")						
								    scePGDBSelectService.StartupType = "Automatic";

							    //start if not running
							    if (!scePGDBSelectService.Status.Equals(ServiceControllerStatus.Running))
								    scePGDBSelectService.Start();

						    }
    					
					    }
					    catch 
					    {}
					    finally
					    {
						    scePGDBSelectService.Dispose();
					    }
    										
    					
					    //MSSQL$PG_DB2
					    ServiceControllerEx sceMSDE = null;
					    try
					    {
						    sceMSDE = new ServiceControllerEx(Globals.MSDE);
						    if (true == am.MOD_6)		//CLIENT
						    {
							    ////Stop MSSQL$PG_DB2 if running
							    if (sceMSDE.Status.Equals(ServiceControllerStatus.Running))
								    sceMSDE.Stop();
							    //Change startup type to disabled
							    if (sceMSDE.StartupType != "Disabled")						
								    sceMSDE.StartupType = "Disabled";
						    }
						    else				//SERVER
						    {
							    //Change startup type to Automatic
							    if (sceMSDE.StartupType != "Automatic")						
								    sceMSDE.StartupType = "Automatic";

							    ////start if not running
							    if (!sceMSDE.Status.Equals(ServiceControllerStatus.Running))
								    sceMSDE.Start();
    							
						    }
					    }
					    catch
					    {}
					    finally
					    {
						    sceMSDE.Dispose();												
					    }
					    //TA-- 05.03.2006. (active modules)
				    }
				    else
				    {
					    rf.SuccessFullRegistration = false;
					    Globals.Registered = false;
					    this.tbResult.Text = "Registration is NOT successful!" + 
						    Environment.NewLine + 
						    Environment.NewLine + 
						    "Please do the following and try again:" +
						    Environment.NewLine +
						    "a) disable your Firewall, so the registration can get to our server" + 
						    Environment.NewLine + 
						    "b) verify that you entered your serial number correctly" + 
						    Environment.NewLine +
						    "c) if you tried a & b, and it is still not going through, contact the support at their email or number below."						
						    ;
				    }
			    }
			    else
			    {
				    rf.SuccessFullRegistration = false;
				    Globals.Registered = false;
			    }
            }
		}        
    
	}
}
