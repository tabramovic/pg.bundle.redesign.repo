using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

using DealMaker.PlainClasses.Duplicates;
using DealMaker.PlainClasses.Utils;

namespace DealMaker.Forms.Duplicates
{
	/// <summary>
	/// Summary description for Progress.
	/// </summary>
	public class Progress : System.Windows.Forms.Form
	{
		bool _closeForm = false;
		eProgressBarTypes _progressBarType;
		bool _forcedStop = false;
		bool _finished = false;
		int _max;

		private System.Windows.Forms.Button _close;
		private System.Windows.Forms.ProgressBar _progressBar;
		private System.Windows.Forms.Label _Status;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public Progress(int numberOfRecords, eProgressBarTypes type)
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();						
			_progressBar.Maximum =_max = numberOfRecords;	
			
			_closeForm = false;
		 	
			_forcedStop = false;
			_finished = false;		

			_progressBarType = type;

			switch (type)
			{
				case eProgressBarTypes.Load:
					this.Text = "Loading...";
					DuplicateChecker.Instance.OnPropertyItemLoaded += new DuplicateChecker.PropertyItemLoadedHandler(On_OnPropertyItemLoaded);
					break;

				case eProgressBarTypes.Match:
					this.Text = "Matching...";
					DuplicateChecker.Instance.OnContact_A_Browsed += new DealMaker.PlainClasses.Utils.DuplicateChecker.Contact_A_BrowsedHandler(On_OnContact_A_Browsed);
					break;

				case eProgressBarTypes.Delete:
					this.Text = "Permanent delete";
					DuplicateChecker.Instance.OnPropertyItemDeleted +=new DuplicateChecker.PropertyItemDeletedHandler(On_OnPropertyItemDeleted);
					break;

				case eProgressBarTypes.Merge:
					this.Text = "Merging...";					
					DuplicateChecker.Instance.OnPropertyItemMerged +=new DealMaker.PlainClasses.Utils.DuplicateChecker.PropertyItemMergedHandler(On_OnPropertyItemMerged);
					break;
			}
																							
			_close.Click += new EventHandler(On_Close_Click);
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this._progressBar = new System.Windows.Forms.ProgressBar();
			this._close = new System.Windows.Forms.Button();
			this._Status = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// _progressBar
			// 
			this._progressBar.Location = new System.Drawing.Point(8, 16);
			this._progressBar.Name = "_progressBar";
			this._progressBar.Size = new System.Drawing.Size(312, 23);
			this._progressBar.TabIndex = 0;
			// 
			// _close
			// 
			this._close.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this._close.Location = new System.Drawing.Point(248, 48);
			this._close.Name = "_close";
			this._close.TabIndex = 1;
			this._close.Text = "Cancel";
			// 
			// _Status
			// 
			this._Status.Location = new System.Drawing.Point(8, 48);
			this._Status.Name = "_Status";
			this._Status.TabIndex = 2;
			// 
			// Progress
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(328, 80);
			this.Controls.Add(this._Status);
			this.Controls.Add(this._close);
			this.Controls.Add(this._progressBar);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "Progress";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.ResumeLayout(false);

		}
		#endregion

		private void On_OnPropertyItemDeleted(int current, int total, bool forcedStop)
		{
			HandleEvent(current, total, forcedStop);
		}

		private void On_OnPropertyItemLoaded(int current, int total, bool forcedStop)
		{
			HandleEvent(current, total, forcedStop);
		}

		private void On_OnContact_A_Browsed(int current, int total, bool forcedStop)
		{
			HandleEvent(current, total, forcedStop);
		}

		private void On_OnPropertyItemMerged(int current, int total, bool forcedStop)
		{
			HandleEvent(current, total, forcedStop);
		}

        delegate void HandleEventDelegate(int current, int total, bool forcedStop);
        void HandleEvent(int current, int total, bool forcedStop)
		{
            if (this.InvokeRequired)
                this.BeginInvoke(new HandleEventDelegate(HandleEvent), new object[] { current, total, forcedStop });
            else
            {
                //if (_progressBar.Maximum < total) //OLD
                if (_progressBar.Maximum != total)  //NEW
                    _progressBar.Maximum = total;

                _progressBar.Value = current;
                _Status.Text = current.ToString() + " / " + total.ToString();

                if (forcedStop)
                {
                    _forcedStop = forcedStop;
                    _close.Text = "Close";
                }

                if (current == _progressBar.Maximum)
                {
                    _finished = true;
                    _close.Text = "Close";
                    DetachAndClose();
                }
            }
		}

		private void On_Close_Click(object sender, EventArgs e)
		{
			if (_finished || _forcedStop)
			{
				DetachAndClose();
			}
			else
			{
				switch (_progressBarType)
				{
					case eProgressBarTypes.Delete:
						DuplicateChecker.Instance.ForcedStopDeleting = true;
						break;

					case eProgressBarTypes.Load:
						DuplicateChecker.Instance.ForcedStopLoading = true;
						break;

					case eProgressBarTypes.Match:
						DuplicateChecker.Instance.StopMatching = true;
						break;

					case eProgressBarTypes.Merge:
						DuplicateChecker.Instance.ForcedStopMerging = true;
						break;
				}
				_forcedStop = true;
				_close.Text = "Close";												
			}
		}
		
		void DetachAndClose()
		{
			//DETACH
			switch (_progressBarType)
			{
				case eProgressBarTypes.Delete:
					DuplicateChecker.Instance.OnPropertyItemDeleted -=new DuplicateChecker.PropertyItemDeletedHandler(On_OnPropertyItemDeleted);				
					//DuplicateChecker.Instance.ForcedStopDeleting = false;
					break;

				case eProgressBarTypes.Load:
					DuplicateChecker.Instance.OnPropertyItemLoaded -= new DuplicateChecker.PropertyItemLoadedHandler(On_OnPropertyItemLoaded);
					//DuplicateChecker.Instance.ForcedStopLoading = false;
					break;

				case eProgressBarTypes.Match:
					DuplicateChecker.Instance.OnContact_A_Browsed -= new DealMaker.PlainClasses.Utils.DuplicateChecker.Contact_A_BrowsedHandler(On_OnContact_A_Browsed);
					//DuplicateChecker.Instance.StopMatching = false;
					break;

				case eProgressBarTypes.Merge:
					DuplicateChecker.Instance.OnPropertyItemMerged -= new DealMaker.PlainClasses.Utils.DuplicateChecker.PropertyItemMergedHandler(On_OnPropertyItemMerged);
					//DuplicateChecker.Instance.ForcedStopMerging = false;
					break;
			}			
								
			_closeForm = true;
			try
			{
				this.Close();
			}
			catch {}
		}

		protected override void OnVisibleChanged(EventArgs e)
		{			
			base.OnVisibleChanged (e);

			if (_closeForm)
				this.Close();
		}
	}
}
