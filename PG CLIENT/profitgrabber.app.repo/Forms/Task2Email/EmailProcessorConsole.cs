﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

//Added
using System.Net.Mail;

namespace DealMaker.Forms.Task2Email
{
    public partial class EmailProcessorConsole : Form, IDisposable
    {
        int remaining = -1;
        const string Separator = "--------------------------------------";
        public EmailProcessorConsole()
        {
            InitializeComponent();

            EmailSenderWorker.Instance.StartSendingEmail += new EmailSenderWorker.StartSendingDelegate(On_StartSendingEmail);
            EmailSenderWorker.Instance.EmailSent += new EmailSenderWorker.SuccessSendingDelegate(On_EmailSent);
            EmailSenderWorker.Instance.EmailFailed += new EmailSenderWorker.FailedSendingDelegate(On_EmailFailed);
        }

        void On_EmailFailed(MailMessage mm, string msg)
        {
            UpdateLogAsync(GetTimeStamp() + "Sending failed, message is requeued for sending." + Environment.NewLine); // + Environment.NewLine + GetMailMessageText(mm) + Environment.NewLine + msg + Environment.NewLine + Environment.NewLine);
        }

        void On_EmailSent(MailMessage mm)
        {
            var msg = GetTimeStamp() + "Sent!" + Environment.NewLine;

            if (0 == remaining)
                msg += $"{Environment.NewLine}{Environment.NewLine}All emails have been sent, you may close the window now.";

            UpdateLogAsync(msg); // + Environment.NewLine + GetMailMessageText(mm) + Environment.NewLine + Environment.NewLine);
        }

        void On_StartSendingEmail(MailMessage mm, int remaining)
        {
            this.remaining = remaining;
            var msg = Separator + Environment.NewLine + GetTimeStamp() + "Remaining: " + remaining.ToString() + Environment.NewLine + "Sending: " + Environment.NewLine + GetMailMessageText(mm) + Environment.NewLine;            
            UpdateLogAsync(msg);
        }

        delegate void UpdateLogAsyncDelegate(string text);
        void UpdateLogAsync(string text)
        {
            if (_logger.InvokeRequired)
                this.BeginInvoke(new UpdateLogAsyncDelegate(UpdateLogAsync), new object[] { text });
            else
            {
                if (_logger.Text.Length + text.Length > 32000)
                    _logger.Text = "CLEARING SCREEN" + Environment.NewLine;

                _logger.Text += text + Environment.NewLine;
                _logger.SelectionStart = _logger.Text.Length;
                _logger.ScrollToCaret();
            }
        }

        string GetTimeStamp()
        {
            DateTime now = DateTime.Now;
            return now.ToLongTimeString() + ":"+ Environment.NewLine;
        }

        string GetMailMessageText(MailMessage mm)
        {
            var txt = $"\tTo: {mm.To}{Environment.NewLine}\tSubject: {mm.Subject}{Environment.NewLine}";

            if (null != mm.Attachments)
                foreach (var att in mm.Attachments)
                    txt += $"\tAttachment: {att?.ContentDisposition?.FileName}{Environment.NewLine}";

            txt += $"\tMessage: {mm.Body}";

            return txt;
        }

        
    }
}
