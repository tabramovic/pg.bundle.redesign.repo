﻿namespace DealMaker.Forms.Task2Email
{
    partial class Task2EmailSetup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this._serviceEnabled = new System.Windows.Forms.CheckBox();
            this._OK = new System.Windows.Forms.Button();
            this._cancel = new System.Windows.Forms.Button();
            this._timeControl = new System.Windows.Forms.DateTimePicker();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 49);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Send out emails at: ";
            // 
            // _serviceEnabled
            // 
            this._serviceEnabled.AutoSize = true;
            this._serviceEnabled.Location = new System.Drawing.Point(12, 12);
            this._serviceEnabled.Name = "_serviceEnabled";
            this._serviceEnabled.Size = new System.Drawing.Size(103, 17);
            this._serviceEnabled.TabIndex = 1;
            this._serviceEnabled.Text = "Send tasks daily";
            this._serviceEnabled.UseVisualStyleBackColor = true;
            // 
            // _OK
            // 
            this._OK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._OK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this._OK.Location = new System.Drawing.Point(136, 93);
            this._OK.Name = "_OK";
            this._OK.Size = new System.Drawing.Size(75, 23);
            this._OK.TabIndex = 6;
            this._OK.Text = "OK";
            this._OK.UseVisualStyleBackColor = true;
            // 
            // _cancel
            // 
            this._cancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this._cancel.Location = new System.Drawing.Point(217, 93);
            this._cancel.Name = "_cancel";
            this._cancel.Size = new System.Drawing.Size(75, 23);
            this._cancel.TabIndex = 7;
            this._cancel.Text = "Cancel";
            this._cancel.UseVisualStyleBackColor = true;
            // 
            // _timeControl
            // 
            this._timeControl.CustomFormat = "hh:mm tt";
            this._timeControl.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this._timeControl.Location = new System.Drawing.Point(110, 46);
            this._timeControl.Name = "_timeControl";
            this._timeControl.ShowUpDown = true;
            this._timeControl.Size = new System.Drawing.Size(75, 20);
            this._timeControl.TabIndex = 8;
            // 
            // Task2EmailSetup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.ClientSize = new System.Drawing.Size(301, 124);
            this.Controls.Add(this._timeControl);
            this.Controls.Add(this._cancel);
            this.Controls.Add(this._OK);
            this.Controls.Add(this._serviceEnabled);
            this.Controls.Add(this.label1);
            this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "Task2EmailSetup";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Task to Email Service Setup";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox _serviceEnabled;
        private System.Windows.Forms.Button _OK;
        private System.Windows.Forms.Button _cancel;
        private System.Windows.Forms.DateTimePicker _timeControl;
    }
}