﻿namespace DealMaker.Forms.Task2Email
{
    partial class EmailProcessorConsole
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                EmailSenderWorker.Instance.StartSendingEmail -= new EmailSenderWorker.StartSendingDelegate(On_StartSendingEmail);
                EmailSenderWorker.Instance.EmailSent -= new EmailSenderWorker.SuccessSendingDelegate(On_EmailSent);
                EmailSenderWorker.Instance.EmailFailed -= new EmailSenderWorker.FailedSendingDelegate(On_EmailFailed);

                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._logger = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // _logger
            // 
            this._logger.Dock = System.Windows.Forms.DockStyle.Fill;
            this._logger.Location = new System.Drawing.Point(0, 0);
            this._logger.Multiline = true;
            this._logger.Name = "_logger";
            this._logger.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this._logger.Size = new System.Drawing.Size(537, 209);
            this._logger.TabIndex = 0;
            // 
            // EmailProcessorConsole
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(537, 209);
            this.Controls.Add(this._logger);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Name = "EmailProcessorConsole";
            this.Text = "Email Processor Console Window";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox _logger;
    }
}