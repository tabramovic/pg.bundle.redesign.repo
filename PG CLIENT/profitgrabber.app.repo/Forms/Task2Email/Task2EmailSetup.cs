﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

//Added
using System.Configuration;

namespace DealMaker.Forms.Task2Email
{
    public partial class Task2EmailSetup : Form
    {
        Guid _officeStaffId = Guid.Empty;
        bool _task2emailServiceEnabled = false;
        int _task2emailHrs = 0;
        int _task2emailMins = 0;

        public bool Task2emailServiceEnabled
        {
            get { return _task2emailServiceEnabled; }
        }

        public int Task2emailHrs
        {
            get { return _task2emailHrs; }
        }

        public int Task2emailMins
        {
            get { return _task2emailMins; }
        }

        public Task2EmailSetup(Guid officeStaffId)
        {
            InitializeComponent();
            AddEventHandlers();

            _officeStaffId = officeStaffId;
            _timeControl.Enabled = false;
            LoadSettings(officeStaffId);
        }

        void AddEventHandlers()
        {            
            _OK.Click += new EventHandler(On_OK_Click);
            _serviceEnabled.CheckedChanged += new EventHandler(On_ServiceEnabled_CheckedChanged);
        }        

        void SaveSettings()
        {
            string err = string.Empty;
            Task2EmailSettingsManager manager = new Task2EmailSettingsManager();
            task2emailsettings settings = manager.LoadCustomSettings(out err);

            if (null == settings)
                settings = new task2emailsettings();

            if (null == settings.data)
                settings.data = new task2emailsettingsData[0];

            bool updated = false;
            foreach (task2emailsettingsData data in settings.data)
            {
                if (data.officeStaffId == _officeStaffId.ToString())
                {
                    data.serviceEnabled = _timeControl.Enabled ? 1 : 0;
                    data.hour = (int)_timeControl.Value.Hour;
                    data.minute = (int)_timeControl.Value.Minute;

                    updated = true;
                }
            }

            if (!updated)
            {
                task2emailsettingsData newData = new task2emailsettingsData();

                newData.officeStaffId = _officeStaffId.ToString();
                newData.serviceEnabled = _timeControl.Enabled ? 1 : 0;
                newData.hour = (int)_timeControl.Value.Hour;
                newData.minute = (int)_timeControl.Value.Minute;

                task2emailsettingsData[] newDataArray = new task2emailsettingsData[settings.data.Length + 1];
                Array.Copy(settings.data, newDataArray, settings.data.Length);

                newDataArray[newDataArray.Length - 1] = newData;

                settings.data = newDataArray;
            }

            manager.SaveCustomSettings(settings, out err);
        }

        public void LoadSettings(Guid officeStaffId)
        {
            try
            {
                string err = string.Empty;
                Task2EmailSettingsManager manager = new Task2EmailSettingsManager();
                task2emailsettings settings = manager.LoadCustomSettings(out err);

                if (null == settings || null == settings.data)
                    return;

                foreach (task2emailsettingsData data in settings.data)
                {
                    if (data.officeStaffId == officeStaffId.ToString())
                    {                        
                        _task2emailServiceEnabled = (1 == data.serviceEnabled ? true : false);                        
                        _task2emailHrs = data.hour;
                        _task2emailMins = data.minute;

                        _serviceEnabled.Checked = _task2emailServiceEnabled;
                        if (_serviceEnabled.Checked)
                        {
                            DateTime dt = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, _task2emailHrs, _task2emailMins, 0);

                            _timeControl.Value = dt;                            
                        }
                    }
                }                
            }
            catch
            {
                _task2emailServiceEnabled = false;
            }

            _serviceEnabled.Checked = _task2emailServiceEnabled;
        }

        void On_OK_Click(object sender, EventArgs e)
        {
            SaveSettings();
            this.Close();
        }

        void On_ServiceEnabled_CheckedChanged(object sender, EventArgs e)
        {            
            _timeControl.Enabled = _serviceEnabled.Checked;            
        }
    }
}
