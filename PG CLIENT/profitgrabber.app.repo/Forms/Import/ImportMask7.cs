using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace DealMaker
{
	/// <summary>
	/// Summary description for ImportMask7.
	/// </summary>
	public class ImportMask7 : System.Windows.Forms.Form
	{		
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Button bOK;
		private System.Windows.Forms.TextBox tbImported;
		private System.Windows.Forms.TextBox tbRecordsInGroup;
		private System.Windows.Forms.TextBox tbExcluded;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;		

		public ImportMask7(string recordsImported, string recordsInGroup)
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			this.tbImported.Text = recordsImported;
			this.tbRecordsInGroup.Text	= recordsInGroup;
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.bOK = new System.Windows.Forms.Button();
			this.tbImported = new System.Windows.Forms.TextBox();
			this.tbRecordsInGroup = new System.Windows.Forms.TextBox();
			this.tbExcluded = new System.Windows.Forms.TextBox();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label1.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.label1.Location = new System.Drawing.Point(16, 16);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(112, 23);
			this.label1.TabIndex = 0;
			this.label1.Text = "Import Completed";
			// 
			// label2
			// 
			this.label2.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.label2.Location = new System.Drawing.Point(48, 48);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(176, 23);
			this.label2.TabIndex = 1;
			this.label2.Text = "Records Imported:";
			// 
			// label3
			// 
			this.label3.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.label3.Location = new System.Drawing.Point(48, 72);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(176, 23);
			this.label3.TabIndex = 2;
			this.label3.Text = "Total number of records in group:";
			// 
			// label4
			// 
			this.label4.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.label4.Location = new System.Drawing.Point(8, 96);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(216, 23);
			this.label4.TabIndex = 3;
			this.label4.Text = "Number of Duplicates that were excluded during the import:";
			this.label4.Visible = false;
			// 
			// bOK
			// 
			this.bOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.bOK.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.bOK.Location = new System.Drawing.Point(248, 112);
			this.bOK.Name = "bOK";
			this.bOK.TabIndex = 4;
			this.bOK.Text = "OK";
			this.bOK.Click += new System.EventHandler(this.bOK_Click);
			// 
			// tbImported
			// 
			this.tbImported.Location = new System.Drawing.Point(224, 48);
			this.tbImported.Name = "tbImported";
			this.tbImported.ReadOnly = true;
			this.tbImported.TabIndex = 5;
			this.tbImported.Text = "";
			// 
			// tbRecordsInGroup
			// 
			this.tbRecordsInGroup.Location = new System.Drawing.Point(224, 72);
			this.tbRecordsInGroup.Name = "tbRecordsInGroup";
			this.tbRecordsInGroup.ReadOnly = true;
			this.tbRecordsInGroup.TabIndex = 6;
			this.tbRecordsInGroup.Text = "";
			// 
			// tbExcluded
			// 
			this.tbExcluded.Location = new System.Drawing.Point(184, 112);
			this.tbExcluded.Name = "tbExcluded";
			this.tbExcluded.ReadOnly = true;
			this.tbExcluded.Size = new System.Drawing.Size(32, 20);
			this.tbExcluded.TabIndex = 7;
			this.tbExcluded.Text = "";
			this.tbExcluded.Visible = false;
			// 
			// ImportMask7
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(210)), ((System.Byte)(216)), ((System.Byte)(240)));
			this.ClientSize = new System.Drawing.Size(338, 152);
			this.Controls.Add(this.tbExcluded);
			this.Controls.Add(this.tbRecordsInGroup);
			this.Controls.Add(this.tbImported);
			this.Controls.Add(this.bOK);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
			this.Name = "ImportMask7";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Import Wizard";
			this.Closing += new System.ComponentModel.CancelEventHandler(this.ImportMask7_Closing);
			this.Load += new System.EventHandler(this.ImportMask7_Load);
			this.ResumeLayout(false);

		}
		#endregion

		private void bOK_Click(object sender, System.EventArgs e)
		{
			Globals.importWizardStarted = false;
			this.Close();
		}

		private void ImportMask7_Load(object sender, System.EventArgs e)
		{
			//this.Location = Globals.importWindowPos;
		}

		private void ImportMask7_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (true == e.Cancel)
			{
				if (DialogResult.Yes == MessageBox.Show(this, UserMessages.CancelImportText, UserMessages.CancelImport, MessageBoxButtons.YesNo, MessageBoxIcon.Question))
				{
					Globals.importWizardStarted = false;
					this.Close();
				}
			}
		}
	}
}
