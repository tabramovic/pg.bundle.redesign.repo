#region � Using �
using System;
using System.IO;
using System.Data;
using System.Drawing;
using System.Reflection;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data.SqlClient; 
using NHibernate;
using NHibernate.Cfg;
#endregion � Using �
#region � Namespace DealMaker �
namespace DealMaker
{
	#region � Class ImportMask5 �
	/// <summary>
	/// Class ImportMask5 - implements requirements defined in preRequisites
	/// </summary>
	public class ImportMask5 : System.Windows.Forms.Form
	{
		#region � Data �
		private ArrayList alAllowedZIPs = null;
		private Delimiter inputDelimiter;
		private string profilesPath = string.Empty;
		private string selectedProfile = string.Empty;
		private string selectedFile;
		private bool bChecked;
		private bool bCreateNewProfile = false;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.Button bNext;
		private System.Windows.Forms.Button bPrev;
		private System.Windows.Forms.Button bCancel;
		private System.Windows.Forms.Button bSaveMap;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
		#endregion � Data �
		#region � Properties �
		#region � InputDelimiter �
		public Delimiter InputDelimiter
		{
			get {return this.inputDelimiter;}
			set {this.inputDelimiter = value;}
		}
		#endregion � InputDelimiter �
		#region � SelectedFile �
		public string SelectedFile
		{
			get {return this.selectedFile;}
			set {this.selectedFile = value;}
		}
		#endregion � SelectedFile �
		#region � Checked �
		public bool Checked
		{
			get {return this.bChecked;}
			set {this.bChecked = value;}
		}
		#endregion � Checked �
		#region � CreateNewProfile �
		public bool CreateNewProfile
		{
			get {return this.bCreateNewProfile;}
			set {this.bCreateNewProfile = value;}
		}
		#endregion � CreateNewProfile �
		#region � SelectedProfile �
		public string SelectedProfile
		{
			get {return this.selectedProfile; }
			set {this.selectedProfile = value;}
		}		
		#endregion � SelectedProfile �

		public ArrayList AllowedZIPs
		{
			get {return this.alAllowedZIPs;}
			set {this.alAllowedZIPs = value;}
		}

		#endregion � Properties �	
		#region � CTR && Dispose �
		public ImportMask5()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			this.Location = Globals.importWindowPos;
			
		}

		public ImportMask5(string profilesPath)
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			this.Location = Globals.importWindowPos;
			this.profilesPath = profilesPath;
			
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}
		#endregion � CTR && Dispose �
		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.label7 = new System.Windows.Forms.Label();
			this.bNext = new System.Windows.Forms.Button();
			this.bPrev = new System.Windows.Forms.Button();
			this.bCancel = new System.Windows.Forms.Button();
			this.bSaveMap = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label1.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.label1.Location = new System.Drawing.Point(24, 24);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(160, 23);
			this.label1.TabIndex = 0;
			this.label1.Text = "Save Mapping Profile:";
			// 
			// label2
			// 
			this.label2.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.label2.Location = new System.Drawing.Point(64, 56);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(488, 40);
			this.label2.TabIndex = 1;
			this.label2.Text = "Most of the times you will be importing files from the same sources, therefore us" +
				"ing the same mapping profile.";
			// 
			// label3
			// 
			this.label3.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.label3.Location = new System.Drawing.Point(64, 104);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(488, 40);
			this.label3.TabIndex = 2;
			this.label3.Text = "If you forsee using the Mapping Profile that you just created in the future, clic" +
				"k SAVE MAP below! This will save the Mapping Profile together with its Filter.";
			// 
			// label6
			// 
			this.label6.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.label6.Location = new System.Drawing.Point(64, 216);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(304, 23);
			this.label6.TabIndex = 5;
			this.label6.Text = "To SKIP saving the Mapping Profile click Next!";
			// 
			// label7
			// 
			this.label7.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.label7.Location = new System.Drawing.Point(64, 240);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(392, 23);
			this.label7.TabIndex = 6;
			this.label7.Text = "Warning! If you skip saving this profile, you will need to recreate it next time!" +
				"";
			// 
			// bNext
			// 
			this.bNext.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.bNext.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.bNext.Location = new System.Drawing.Point(392, 288);
			this.bNext.Name = "bNext";
			this.bNext.TabIndex = 15;
			this.bNext.Text = "Next";
			this.bNext.Visible = false;
			this.bNext.Click += new System.EventHandler(this.bNext_Click);
			// 
			// bPrev
			// 
			this.bPrev.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.bPrev.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.bPrev.Location = new System.Drawing.Point(304, 288);
			this.bPrev.Name = "bPrev";
			this.bPrev.TabIndex = 14;
			this.bPrev.Text = "Previous";
			this.bPrev.Visible = false;
			this.bPrev.Click += new System.EventHandler(this.bPrev_Click);
			// 
			// bCancel
			// 
			this.bCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.bCancel.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.bCancel.Location = new System.Drawing.Point(480, 288);
			this.bCancel.Name = "bCancel";
			this.bCancel.TabIndex = 16;
			this.bCancel.Text = "Close";
			this.bCancel.Click += new System.EventHandler(this.bCancel_Click);
			// 
			// bSaveMap
			// 
			this.bSaveMap.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.bSaveMap.Location = new System.Drawing.Point(64, 152);
			this.bSaveMap.Name = "bSaveMap";
			this.bSaveMap.TabIndex = 13;
			this.bSaveMap.Text = "SAVE MAP";
			this.bSaveMap.Click += new System.EventHandler(this.bSaveMap_Click);
			// 
			// ImportMask5
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(210)), ((System.Byte)(216)), ((System.Byte)(240)));
			this.ClientSize = new System.Drawing.Size(570, 328);
			this.Controls.Add(this.bSaveMap);
			this.Controls.Add(this.bCancel);
			this.Controls.Add(this.bPrev);
			this.Controls.Add(this.bNext);
			this.Controls.Add(this.label7);
			this.Controls.Add(this.label6);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
			this.Name = "ImportMask5";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.ResumeLayout(false);

		}
		#endregion
		#region � Methods �
		#region � bSaveMap_Click �
		private void bSaveMap_Click(object sender, System.EventArgs e)
		{
			SaveProfileForm spf = null;
			if (string.Empty != this.profilesPath)
			{
				spf = new SaveProfileForm(this.profilesPath);
			}
			else
			{
				spf = new SaveProfileForm();
			}
			spf.ShowDialog();
			this.Close();
		}
		#endregion � bSaveMap_Click �
		#region � bNext_Click �
		private void bNext_Click(object sender, System.EventArgs e)
		{
			//this.ImportData();
						
			Globals.importWindowPos = this.Location;
			//this.Close();
			ImportMask6 im6 = new ImportMask6();
			im6.InputDelimiter = this.InputDelimiter;
			im6.SelectedFile = this.SelectedFile;
			im6.SelectedProfile = this.SelectedProfile;
			im6.CreateNewProfile = this.CreateNewProfile;
			im6.Checked = this.Checked;
			//im6.alImportIntermediatedObjects = im6.ImportData();						
			//im6.alFilteredImportIntermediateObjects = im6.FilterImportIntermediateObjects(im6.alImportIntermediatedObjects);
			//im6.BeforeFilter = im6.alImportIntermediatedObjects.Count.ToString();									
			//im6.AfterFilter = im6.alFilteredImportIntermediateObjects.Count.ToString();

			im6.Show();			
		}
		#endregion � bNext_Click �
		#region � bPrev_Click �
		private void bPrev_Click(object sender, System.EventArgs e)
		{
			Globals.importWindowPos = this.Location;
			ImportMask4	im4 = new ImportMask4();
			im4.SelectedFile = this.SelectedFile;
			im4.Checked = this.Checked;
			im4.CreateNewProfile = this.CreateNewProfile;
			im4.InputDelimiter = this.InputDelimiter;
			im4.SelectedProfile = this.SelectedProfile;
			//this.Close();
			im4.Show();
		}
		#endregion � bPrev_Click �
		#region � bCancel_Click �
		private void bCancel_Click(object sender, System.EventArgs e)
		{
			//if (DialogResult.Yes == MessageBox.Show(this, UserMessages.CancelImportText, UserMessages.CancelImport, MessageBoxButtons.YesNo, MessageBoxIcon.Question))
			//{
				//Globals.importWizardStarted = false;
				this.Close();
			//}
		}
		#endregion � bCancel_Click �		

		private void ImportMask5_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (true == e.Cancel)
			{
				if (DialogResult.Yes == MessageBox.Show(this, UserMessages.CancelImportText, UserMessages.CancelImport, MessageBoxButtons.YesNo, MessageBoxIcon.Question))
				{
					Globals.importWizardStarted = false;
					//this.Close();
				}
			}
		}
		#endregion � Methods �	
	
		
	}
	#endregion � Class ImportMask5 �
}
#endregion � Namespace DealMaker �