﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using DealMaker.PlainClasses.RealEFlowImport;
using DealMaker.PlainClasses.Utils;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Linq;

namespace DealMaker.Forms.Import.RealEFlow
{
    public partial class RealEFlowImportMask : Form
    {
        const string DealFinderGroupName = "DealFinder360";
        const string Caption = "DealFinder360";
        private readonly CustomizedTreeViewUserControl _groups;
        private string _selectedFile;
        private int _maxContactsPerGroupValue = 100;
        private int _userSelectedParentImportGroupId;

        private string ImportSubGroupName => $"{_subgroupName.Text}";
        private string ImportGroupName => $"{_subgroupName.Text}";
        public RealEFlowImportMask(CustomizedTreeViewUserControl groups)
        {
            InitializeComponent();
            AddEventHandlers();            
            _groups = groups;
        }

        void AddEventHandlers()
        {
            _bBrowse.Click += BBrowse_Click;
            _btnSelectGroup.Click += _btnSelectGroup_Click;
            _cancel.Click += _cancel_Click;
            _import.Click += _import_Click;
        }

        private void _btnSelectGroup_Click(object sender, EventArgs e)
        {
            var sgf = new SelectGroupForm();

            sgf.ShowDialog();

            if (DialogResult.OK != sgf.DialogResult)
                return;

            _userSelectedParentImportGroupId = sgf.SelectedNodeId;

            _importGroupName.Text = sgf.SelectedNodeName;

            _subgroupName.ReadOnly = false;
        }

        //yes async void for EH is valid (as long as all the logic is factored out for unit tests)
        private async void _import_Click(object sender, EventArgs e)
        {
            try
            {
                _maxContactsPerGroupValue = (int)_maxContactsPerGroup.Value;

                if (!CheckGroupName(ImportSubGroupName))
                {
                    MessageBox.Show($"There is a problem with the name of your subgroup.{Environment.NewLine}{Environment.NewLine}Allowed characters are letters and numbers and _.  No spaces or special characters are allowed. Maximum group name length is 32 characters total.{Environment.NewLine}{Environment.NewLine}If the subgroup name has any spaces or special character, please rename the subgroup and try again.", Caption);
                    return;
                }

                var recordsImported = await Import(ImportGroupName, _maxContactsPerGroupValue, _userSelectedParentImportGroupId);
                if (-1 != recordsImported)
                {
                    MessageBox.Show($"System imported {recordsImported} records.", Caption);
                    this.Close();
                }
            }
            catch (Exception exc)
            {
                MessageBox.Show($"{exc.Message}{Environment.NewLine}{exc.InnerException}");
            }
            
        }

        private async System.Threading.Tasks.Task<int> Import(string groupName, int maxContactsPerGroup, int userSelectedParentImportGroupId)
        {
            var recordsImported = 0;
                        
            if (!CompressedFileHelper.ExtractFile(_selectedFile, out var csvDataFile, out var error))
                MessageBox.Show(error, Caption);

            var recordList = await new RealEFlowFileReader(csvDataFile).ReadAndMap();
            var propertyItemList = recordList.ToPropertyItemList();

            int numberOfGroupsNeeded = 
                propertyItemList.Count / maxContactsPerGroup + 
                (propertyItemList.Count % maxContactsPerGroup > 0 ? 1 : 0);

            using (var dbConnection = new SqlConnection(Network.Util.GetConnectionStringFromServerName(Globals.DatabaseServer)))
            {
                dbConnection.Open();

                if (ConnectionState.Open == dbConnection.State)
                {
                    int dealFinderGroupId;

                    if (default == userSelectedParentImportGroupId)
                    {
                        if (!ApplicationHelper.FindGroup(DealFinderGroupName, out dealFinderGroupId, dbConnection))
                            ApplicationHelper.CreateNode(1, DealFinderGroupName, DealFinderGroupName, out dealFinderGroupId, dbConnection);
                    }
                    else
                        dealFinderGroupId = userSelectedParentImportGroupId;

                    var subGroupDict = ApplicationHelper.GetSubGroups(dealFinderGroupId);

                    if (!CheckGroupNameAvailability(groupName, subGroupDict))
                    {
                        MessageBox.Show($"Group not created.{Environment.NewLine}{Environment.NewLine}The group name must be at least one character and different than other DealFinder360 groups.", Caption);
                        return -1;
                    }

                    var createdGroupIds = new List<int>();
                    for (var groupSuffix = 1; groupSuffix <= numberOfGroupsNeeded; groupSuffix++)
                    {
                        if (!ApplicationHelper.FindGroup($"{groupName}-{groupSuffix}", out int groupId, dbConnection))
                            ApplicationHelper.CreateNode(dealFinderGroupId, $"{groupName}-{groupSuffix}", $"{groupName}-{groupSuffix}", out groupId, dbConnection);

                        createdGroupIds.Add(groupId);

                        if (null != _groups)
                            _groups.OnNewGroup();
                    }
                    
                    var createdGroupsCount = createdGroupIds.Count;
                    propertyItemList.ForEach(pi =>
                    {
                        pi.ExecuteSPL(dbConnection);
                        ApplicationHelper.AssignPropertyItemToGroup(createdGroupIds[recordsImported / maxContactsPerGroup], pi.IdPropertyItem);
                        recordsImported++;
                    });
                }
            }

            return recordsImported;
        }

        private void _cancel_Click(object sender, EventArgs e)
        {
            if (DialogResult.Yes == MessageBox.Show("Are you sure you want to exit DealFinder360?", Caption, MessageBoxButtons.YesNo, MessageBoxIcon.Question))
                this.Close();
        }

        private void BBrowse_Click(object sender, EventArgs e)
        {
            var ofd = new OpenFileDialog
            {
                InitialDirectory = KnownFolders.GetPath(KnownFolder.Downloads),
                Filter = "zip files (*.zip)|*.zip"
            };

            if (DialogResult.OK == ofd.ShowDialog())            
                _selectedFile = tbSelectedFile.Text = ofd.FileName;                            
        }

        private bool CheckGroupName(string groupName)
        {
            if (groupName.Length > 32)
                return false;

            Regex regex = new Regex("^([_a-zA-Z0-9]+)$");
            return regex.IsMatch(groupName);            
        }

        private bool CheckGroupNameAvailability(string groupName, Dictionary<int, string> groups)
        {
            var groupNames = groups.Select(kvp => 
                kvp.Value.Contains("-") 
                ? kvp.Value.Substring(0, kvp.Value.IndexOf("-")) 
                : kvp.Value)
                .ToList();

            return !groupNames.Contains(groupName);
        }
    }
}
