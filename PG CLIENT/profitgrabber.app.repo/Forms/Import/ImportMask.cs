using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace DealMaker
{
	/// <summary>
	/// Summary description for ImportMask.
	/// </summary>
	public class ImportMask : System.Windows.Forms.Form
	{
		private bool importedData = false;
		private System.Windows.Forms.Panel upperPanel;
		private System.Windows.Forms.Panel lowerPanel;
		public System.Windows.Forms.Button bNext;
		private System.Windows.Forms.Button bPrevious;
		private System.Windows.Forms.Button bCancel;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public ImportMask()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();						
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.upperPanel = new System.Windows.Forms.Panel();
			this.lowerPanel = new System.Windows.Forms.Panel();
			this.bCancel = new System.Windows.Forms.Button();
			this.bPrevious = new System.Windows.Forms.Button();
			this.bNext = new System.Windows.Forms.Button();
			this.lowerPanel.SuspendLayout();
			this.SuspendLayout();
			// 
			// upperPanel
			// 
			this.upperPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.upperPanel.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(210)), ((System.Byte)(216)), ((System.Byte)(240)));
			this.upperPanel.Location = new System.Drawing.Point(0, 0);
			this.upperPanel.Name = "upperPanel";
			this.upperPanel.Size = new System.Drawing.Size(578, 385);
			this.upperPanel.TabIndex = 0;
			// 
			// lowerPanel
			// 
			this.lowerPanel.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(210)), ((System.Byte)(216)), ((System.Byte)(240)));
			this.lowerPanel.Controls.Add(this.bCancel);
			this.lowerPanel.Controls.Add(this.bPrevious);
			this.lowerPanel.Controls.Add(this.bNext);
			this.lowerPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.lowerPanel.Location = new System.Drawing.Point(0, 392);
			this.lowerPanel.Name = "lowerPanel";
			this.lowerPanel.Size = new System.Drawing.Size(578, 48);
			this.lowerPanel.TabIndex = 1;
			// 
			// bCancel
			// 
			this.bCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.bCancel.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.bCancel.Location = new System.Drawing.Point(8, 16);
			this.bCancel.Name = "bCancel";
			this.bCancel.TabIndex = 2;
			this.bCancel.Text = "Cancel";
			this.bCancel.Click += new System.EventHandler(this.bCancel_Click);
			// 
			// bPrevious
			// 
			this.bPrevious.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.bPrevious.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.bPrevious.Location = new System.Drawing.Point(408, 16);
			this.bPrevious.Name = "bPrevious";
			this.bPrevious.TabIndex = 1;
			this.bPrevious.Text = "Previous";
			this.bPrevious.Click += new System.EventHandler(this.bPrevious_Click);
			// 
			// bNext
			// 
			this.bNext.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.bNext.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.bNext.Location = new System.Drawing.Point(496, 16);
			this.bNext.Name = "bNext";
			this.bNext.TabIndex = 0;
			this.bNext.Text = "Next";
			this.bNext.Click += new System.EventHandler(this.bNext_Click);
			// 
			// ImportMask
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(210)), ((System.Byte)(216)), ((System.Byte)(240)));
			this.ClientSize = new System.Drawing.Size(578, 440);
			this.Controls.Add(this.lowerPanel);
			this.Controls.Add(this.upperPanel);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
			this.Name = "ImportMask";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Import Wizard";
			this.Load += new System.EventHandler(this.ImportMask_Load);
			this.lowerPanel.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		
		public bool ImportedData 
		{
			get { return this.importedData; }
			set { this.importedData = value; }
		}
		
		
		private void ImportMask_Load(object sender, System.EventArgs e)
		{
			ImportMask1 im1 = new ImportMask1();					
			im1.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top;
			im1.Dock = DockStyle.Fill;
			
			this.upperPanel.Controls.Add(im1);
			this.upperPanel.Controls[0].Location = new Point(0,0);
			
			
			this.CheckPreviousVisibility();
		}

		private void CheckPreviousVisibility()
		{
			IEnumerator ie = this.upperPanel.Controls.GetEnumerator();
			while (ie.MoveNext())
			{
				if (ie.Current is ImportMask1)
				{
					this.bPrevious.Visible = false;
				}
				else
				{
					this.bPrevious.Visible = true;
				}
			}
		}

		private void bNext_Click(object sender, System.EventArgs e)
		{
			IEnumerator ie = this.upperPanel.Controls.GetEnumerator();			
			while (ie.MoveNext())
			{
				#region � ImportMask1 �
				if (ie.Current is ImportMask1)
				{
					ImportMask1 im1 = (ImportMask1)ie.Current;
					im1.bNext_Click(sender, e);

					if (true == im1.Valid)
					{
						ImportMask2 im2 = new ImportMask2();					
						im2.Checked = im1.Checked;
						im2.SelectedFile = im1.SelectedFile;
						im2.InputDelimiter = im1.InputDelimiter;
						im2.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top;
						im2.Dock = DockStyle.Fill;

						this.upperPanel.Controls.RemoveAt(0);
						this.upperPanel.Controls.Add(im2);
						this.upperPanel.Controls[0].Location = new Point(0,0);
						this.CheckPreviousVisibility();
					}
				break;
				}
				#endregion � ImportMask1 �
				#region � ImportMask2 �
				if (ie.Current is ImportMask2)
				{
					ImportMask2 im2 = (ImportMask2)ie.Current;

					ImportMask3 im3 = new ImportMask3();
					im3.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top;
					im3.Dock = DockStyle.Fill;
					im3.SelectedFile = im2.SelectedFile;
					im3.Checked = im2.Checked;
					im3.CreateNewProfile = im2.CreateNewProfile;
					im3.InputDelimiter = im2.InputDelimiter;
					im3.SelectedProfile = im2.SelectedProfile;
					
					this.upperPanel.Controls.RemoveAt(0);
					this.upperPanel.Controls.Add(im3);
					this.upperPanel.Controls[0].Location = new Point(0,0);
					this.CheckPreviousVisibility();
					break;
					
				}
				#endregion � ImportMask2 �
				#region � ImportMask3 �
				if (ie.Current is ImportMask3)
				{
					ImportMask3 im3 = (ImportMask3)ie.Current;
					im3.bNext_Click(sender, e);


					ImportMask4 im4 = new ImportMask4();
					im4.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top;
					im4.Dock = DockStyle.Fill;
					im4.SelectedFile = im3.SelectedFile;
					im4.Checked = im3.Checked;
					im4.CreateNewProfile = im3.CreateNewProfile;
					im4.InputDelimiter = im3.InputDelimiter;
					im4.SelectedProfile = im3.SelectedProfile;
					im4.SelectedState = im3.SelectedState;
					im4.TrimZIP = im3.TrimZIP;
                    im4.SeparateFirstName = im3.SeparateFirstName;
                    im4.LastNameFirst = im3.LastNameFirst;
					
					this.upperPanel.Controls.RemoveAt(0);
					this.upperPanel.Controls.Add(im4);
					this.upperPanel.Controls[0].Location = new Point(0,0);
					this.CheckPreviousVisibility();
					break;
				}
				#endregion � ImportMask3 �
				#region � ImportMask4 �
				if (ie.Current is ImportMask4)
				{
					ImportMask4 im4 = (ImportMask4)ie.Current;
					im4.bNext_Click(sender, e);
					im4.UpdateZIPFilter();


					ImportMask6 im6 = new ImportMask6();
					im6.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top;
					im6.Dock = DockStyle.Fill;
					im6.SelectedFile = im4.SelectedFile;
					im6.Checked = im4.Checked;
					im6.CreateNewProfile = im4.CreateNewProfile;						
					im6.SelectedProfile = im4.SelectedProfile;
					im6.InputDelimiter = im4.InputDelimiter;
					im6.AllowedZIPs = Globals.zipFilter;// im4.AllowedZIPs;
					im6.TrimZIP = im4.TrimZIP;                    

                    im6.SeparateFirstName = im4.SeparateFirstName;
                    im6.LastNameFirst = im4.LastNameFirst;

					im6.alImportIntermediatedObjects = im6.ImportData();						
					im6.alFilteredImportIntermediateObjects = im6.FilterImportIntermediateObjects(im6.alImportIntermediatedObjects);
					im6.BeforeFilter = im6.alImportIntermediatedObjects.Count.ToString();									
					im6.AfterFilter = im6.alFilteredImportIntermediateObjects.Count.ToString();
					im6.SelectedState = im4.SelectedState;
					
					

					
					this.upperPanel.Controls.RemoveAt(0);
					this.upperPanel.Controls.Add(im6);
					this.upperPanel.Controls[0].Location = new Point(0,0);
					this.CheckPreviousVisibility();
					this.bNext.Text = "Close";
					break;
				}
				#endregion � ImportMask4 �
				#region � ImportMask5 �
				if (ie.Current is ImportMask5)
				{
					ImportMask5 im5 = (ImportMask5)ie.Current;
					
					ImportMask6 im6 = new ImportMask6();
					im6.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top;
					im6.Dock = DockStyle.Fill;
					im6.InputDelimiter = im5.InputDelimiter;
					im6.SelectedFile = im5.SelectedFile;
					im6.SelectedProfile = im5.SelectedProfile;
					im6.CreateNewProfile = im5.CreateNewProfile;
					im6.Checked = im5.Checked;
					
					
					this.upperPanel.Controls.RemoveAt(0);
					this.upperPanel.Controls.Add(im6);
					this.upperPanel.Controls[0].Location = new Point(0,0);
					this.CheckPreviousVisibility();
					this.bNext.Text = "Close";
					break;
				}
				#endregion � ImportMask5 �
				#region � ImportMask6 �
				if (ie.Current is ImportMask6)
				{	
					if (DialogResult.Yes == MessageBox.Show(this, "Are you sure you want to exit import?", "Exit Import", MessageBoxButtons.YesNo, MessageBoxIcon.Question) )
					{
						ApplicationHelper.ResetFilters();			
						this.Close();
						break;
					}
					else
					{
						break;
					}
				}
				#endregion � ImportMask6 �
			}
		}

		private void bCancel_Click(object sender, System.EventArgs e)
		{
			if (DialogResult.Yes == MessageBox.Show(this, UserMessages.CancelImportText, UserMessages.CancelImport, MessageBoxButtons.YesNo, MessageBoxIcon.Question))
			{
				this.Close();
				ApplicationHelper.ResetFilters();
			}
		}

		internal void bPrevious_Click(object sender, System.EventArgs e)
		{
			IEnumerator ie = this.upperPanel.Controls.GetEnumerator();			
			while (ie.MoveNext())
			{
				#region � ImportMask2 �
				if (ie.Current is ImportMask2)
				{
					ImportMask2 im2 = (ImportMask2)ie.Current;
					ImportMask1 im1 = new ImportMask1(im2.SelectedFile, im2.Checked, im2.InputDelimiter);
					im1.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top;
					im1.Dock = DockStyle.Fill;
					this.upperPanel.Controls.RemoveAt(0);
					this.upperPanel.Controls.Add(im1);
					this.upperPanel.Controls[0].Location = new Point(0,0);
					this.CheckPreviousVisibility();
					this.bNext.Enabled = true;
					break;
				}
				#endregion � ImportMask2 �
				#region � ImportMask3 �
				if (ie.Current is ImportMask3)
				{
					ImportMask3 im3 = (ImportMask3)ie.Current;
					ImportMask2 im2 = new ImportMask2();
					im2.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top;
					im2.Dock = DockStyle.Fill;

					im2.SelectedFile = im3.SelectedFile;
					im2.Checked = im3.Checked;
					im2.CreateNewProfile = im3.CreateNewProfile;
					im2.InputDelimiter = im3.InputDelimiter;
					im2.SelectedProfile = im3.SelectedProfile;                    

					this.upperPanel.Controls.RemoveAt(0);
					this.upperPanel.Controls.Add(im2);
					this.upperPanel.Controls[0].Location = new Point(0,0);
					this.CheckPreviousVisibility();
					this.bNext.Enabled = true;
					break;
				}
				#endregion � ImportMask3 �
				#region � ImportMask4 �
				if (ie.Current is ImportMask4)
				{
					ImportMask4 im4 = (ImportMask4)ie.Current;
					ImportMask3 im3 = new ImportMask3();
					im3.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top;
					im3.Dock = DockStyle.Fill;
					im3.SelectedFile = im4.SelectedFile;					
					im3.Checked = im4.Checked;
					im3.CreateNewProfile = im4.CreateNewProfile;
					im3.SelectedProfile = im4.SelectedProfile;
					im3.InputDelimiter = im4.InputDelimiter;
					im3.UseMappingTable = true;
					im3.SelectedState = im4.SelectedState;
                    im3.SeparateFirstName = im4.SeparateFirstName;
                    im3.LastNameFirst = im4.LastNameFirst;

					this.upperPanel.Controls.RemoveAt(0);
					this.upperPanel.Controls.Add(im3);
					this.upperPanel.Controls[0].Location = new Point(0,0);
					this.CheckPreviousVisibility();
					this.bNext.Enabled = true;
					break;
				}
				#endregion � ImportMask4 �
				#region � ImportMask5 �
				if (ie.Current is ImportMask5)
				{
					ImportMask5 im5 = (ImportMask5)ie.Current;
					ImportMask4 im4 = new ImportMask4();
					im4.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top;
					im4.Dock = DockStyle.Fill;				
					im4.SelectedFile = im5.SelectedFile;
					im4.Checked = im5.Checked;
					im4.CreateNewProfile = im5.CreateNewProfile;
					im4.InputDelimiter = im5.InputDelimiter;
					im4.SelectedProfile = im5.SelectedProfile;				

					this.upperPanel.Controls.RemoveAt(0);
					this.upperPanel.Controls.Add(im4);
					this.upperPanel.Controls[0].Location = new Point(0,0);
					this.CheckPreviousVisibility();
					this.bNext.Enabled = true;
					break;
				}
				#endregion � ImportMask5 �
				#region � ImportMask6 �
				if (ie.Current is ImportMask6)
				{
					ImportMask6 im6 = (ImportMask6)ie.Current;					
									
					ImportMask4 im4 = new ImportMask4();
					im4.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top;
					im4.Dock = DockStyle.Fill;	
					im4.InputDelimiter = im6.InputDelimiter;
					im4.SelectedFile = im6.SelectedFile;
					im4.SelectedProfile = im6.SelectedProfile;
					im4.CreateNewProfile = im6.CreateNewProfile;
					im4.Checked = im6.Checked;
					im4.SelectedState = im6.SelectedState;
                    im4.SeparateFirstName = im6.SeparateFirstName;
                    im4.LastNameFirst = im6.LastNameFirst;
			

					this.upperPanel.Controls.RemoveAt(0);
					this.upperPanel.Controls.Add(im4);
					this.upperPanel.Controls[0].Location = new Point(0,0);
					this.CheckPreviousVisibility();
					this.bNext.Enabled = true;
					this.bNext.Text = "Next";
					break;
				}
				#endregion � ImportMask6 �
				
			}
		}		
	}
}
