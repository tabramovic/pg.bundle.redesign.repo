#region � Using �
using System;
using System.IO;
using System.Threading;
using System.Diagnostics;
using System.Drawing;
using System.Data;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data.SqlClient; 
using System.Data.SqlTypes;
using DataAppendService.Lib.Models.IDI;
using NHibernate;
using NHibernate.Cfg;

//Added
using DealMaker.PlainClasses.ContactTypes;
using Newtonsoft.Json;

#endregion � Using �
#region � Namespace DealMaker �
namespace DealMaker
{
	#region � COMMENTED OUT �
	//this.tv_Nodes.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tv_Nodes_KeyDown);
	//this.tv_Nodes.MouseDown += new System.Windows.Forms.MouseEventHandler(this.tv_Nodes_MouseDown);
	//this.tv_Nodes.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.tv_Nodes_AfterSelect);
	#endregion � COMMENTED OUT �

	#region � Class ImportMask6 �
	/// <summary>
	/// Summary description for ImportMask6.
	/// </summary>
	public class ImportMask6 : System.Windows.Forms.UserControl
	{
		#region � Data �
		//private bool validIIO = true;
		bool _trimZIP = true;
		private ArrayList alZIPs = null;
		private string selectedState = string.Empty;
		private TreeNode selectedNode	= null;
		public ArrayList alImportIntermediatedObjects = null;
		public ArrayList alFilteredImportIntermediateObjects = null;
		private Delimiter inputDelimiter;
		private string selectedProfile = string.Empty;
		private string selectedFile;
		private bool bChecked;
		private bool bCreateNewProfile = false;
        bool separateFirstName = false;
        bool lastNameFirst = false;

		private System.Windows.Forms.Label label1;
		private DealMaker.CustomizedTreeViewUserControl customizedTreeViewUserControl1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox tbExistingEntries;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.CheckBox cbExcludeDuplicates;
		private System.Windows.Forms.Button bBack;
		private System.Windows.Forms.Button bCancel;
		private System.Windows.Forms.Button bImport;
		private System.Windows.Forms.TextBox tbAfterFilter;
		private System.Windows.Forms.TextBox tbBeforeFilter;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Button bSaveMap;
        private GroupBox groupBox1;
        private RadioButton _lender;
        private RadioButton _buyer;
        private RadioButton _seller;

        ContactTypes _selectedContactType;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
		#endregion � Data �
		#region � Properties �
		#region � InputDelimiter �
		public Delimiter InputDelimiter
		{
			get {return this.inputDelimiter;}
			set {this.inputDelimiter = value;}
		}
		#endregion � InputDelimiter �
		#region � SelectedFile �
		public string SelectedFile
		{
			get {return this.selectedFile;}
			set {this.selectedFile = value;}
		}
		#endregion � SelectedFile �
		#region � Checked �
		public bool Checked
		{
			get {return this.bChecked;}
			set {this.bChecked = value;}
		}
		#endregion � Checked �
		#region � CreateNewProfile �
		public bool CreateNewProfile
		{
			get {return this.bCreateNewProfile;}
			set {this.bCreateNewProfile = value;}
		}
		#endregion � CreateNewProfile �
		#region � SelectedProfile �
		public string SelectedProfile
		{
			get {return this.selectedProfile; }
			set {this.selectedProfile = value;}
		}		
		#endregion � SelectedProfile �
		public string SelectedState
		{
			get {return this.selectedState;}
			set {this.selectedState = value; }
		}
		public bool TrimZIP
		{
			get {return _trimZIP;}
			set {_trimZIP = value;}
		}
		public ArrayList AllowedZIPs
		{
			get {return this.alZIPs;}
			set {this.alZIPs = value;}
		}
        public bool SeparateFirstName
        {
            get { return separateFirstName; }
            set { separateFirstName = value; }
        }

        public bool LastNameFirst
        {
            get { return lastNameFirst; }
            set { lastNameFirst = value; }
        }

        public ContactTypes SelectedContacyType
        {
            get { return _selectedContactType; } 
        }

		#endregion � Properties �	
		#region � CTR && Dispose �
		public ImportMask6()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			this.customizedTreeViewUserControl1.tv_Nodes.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tv_Nodes_KeyDown);
			this.customizedTreeViewUserControl1.tv_Nodes.MouseDown += new System.Windows.Forms.MouseEventHandler(this.tv_Nodes_MouseDown);
			this.customizedTreeViewUserControl1.tv_Nodes.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.tv_Nodes_AfterSelect);

            _seller.Click += new EventHandler(On_Seller_Click);
            _buyer.Click += new EventHandler(On_Buyer_Click);
            _lender.Click += new EventHandler(On_Lender_Click);

			this.Location = Globals.importWindowPos; 	
		}        

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}
		#endregion � CTR && Dispose �

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.label1 = new System.Windows.Forms.Label();
            this.customizedTreeViewUserControl1 = new DealMaker.CustomizedTreeViewUserControl();
            this.label2 = new System.Windows.Forms.Label();
            this.tbExistingEntries = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cbExcludeDuplicates = new System.Windows.Forms.CheckBox();
            this.bBack = new System.Windows.Forms.Button();
            this.bCancel = new System.Windows.Forms.Button();
            this.bImport = new System.Windows.Forms.Button();
            this.tbAfterFilter = new System.Windows.Forms.TextBox();
            this.tbBeforeFilter = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.bSaveMap = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this._seller = new System.Windows.Forms.RadioButton();
            this._buyer = new System.Windows.Forms.RadioButton();
            this._lender = new System.Windows.Forms.RadioButton();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label1.Location = new System.Drawing.Point(8, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(152, 23);
            this.label1.TabIndex = 0;
            this.label1.Text = "Import File Destination";
            // 
            // customizedTreeViewUserControl1
            // 
            this.customizedTreeViewUserControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.customizedTreeViewUserControl1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.customizedTreeViewUserControl1.Location = new System.Drawing.Point(8, 40);
            this.customizedTreeViewUserControl1.Name = "customizedTreeViewUserControl1";
            this.customizedTreeViewUserControl1.SelectedTreeNode = null;
            this.customizedTreeViewUserControl1.Size = new System.Drawing.Size(296, 125);
            this.customizedTreeViewUserControl1.TabIndex = 1;
            this.customizedTreeViewUserControl1.UpdateGlobals = false;
            // 
            // label2
            // 
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label2.Location = new System.Drawing.Point(320, 24);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(216, 40);
            this.label2.TabIndex = 2;
            this.label2.Text = "TOTAL number of Records already EXISTING in the selected group:";
            // 
            // tbExistingEntries
            // 
            this.tbExistingEntries.Location = new System.Drawing.Point(320, 64);
            this.tbExistingEntries.Name = "tbExistingEntries";
            this.tbExistingEntries.ReadOnly = true;
            this.tbExistingEntries.Size = new System.Drawing.Size(100, 20);
            this.tbExistingEntries.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label3.Location = new System.Drawing.Point(320, 96);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(232, 56);
            this.label3.TabIndex = 4;
            this.label3.Text = "If there are existing records in Destination Group, the imported records will be " +
                "added to those records. If you don�t want that, create a new group.";
            // 
            // cbExcludeDuplicates
            // 
            this.cbExcludeDuplicates.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.cbExcludeDuplicates.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.cbExcludeDuplicates.Location = new System.Drawing.Point(320, 151);
            this.cbExcludeDuplicates.Name = "cbExcludeDuplicates";
            this.cbExcludeDuplicates.Size = new System.Drawing.Size(232, 56);
            this.cbExcludeDuplicates.TabIndex = 5;
            this.cbExcludeDuplicates.Text = "Check this box if you want to exclude duplicates during import";
            this.cbExcludeDuplicates.Visible = false;
            // 
            // bBack
            // 
            this.bBack.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.bBack.Location = new System.Drawing.Point(312, 348);
            this.bBack.Name = "bBack";
            this.bBack.Size = new System.Drawing.Size(32, 23);
            this.bBack.TabIndex = 6;
            this.bBack.Text = "Previous";
            this.bBack.Visible = false;
            this.bBack.Click += new System.EventHandler(this.bBack_Click);
            // 
            // bCancel
            // 
            this.bCancel.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.bCancel.Location = new System.Drawing.Point(352, 348);
            this.bCancel.Name = "bCancel";
            this.bCancel.Size = new System.Drawing.Size(32, 23);
            this.bCancel.TabIndex = 7;
            this.bCancel.Text = "Cancel";
            this.bCancel.Visible = false;
            this.bCancel.Click += new System.EventHandler(this.bCancel_Click);
            // 
            // bImport
            // 
            this.bImport.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.bImport.Location = new System.Drawing.Point(488, 348);
            this.bImport.Name = "bImport";
            this.bImport.Size = new System.Drawing.Size(75, 23);
            this.bImport.TabIndex = 8;
            this.bImport.Text = "Import";
            this.bImport.Click += new System.EventHandler(this.bImport_Click);
            // 
            // tbAfterFilter
            // 
            this.tbAfterFilter.Location = new System.Drawing.Point(488, 271);
            this.tbAfterFilter.Name = "tbAfterFilter";
            this.tbAfterFilter.ReadOnly = true;
            this.tbAfterFilter.Size = new System.Drawing.Size(75, 20);
            this.tbAfterFilter.TabIndex = 16;
            this.tbAfterFilter.TabStop = false;
            // 
            // tbBeforeFilter
            // 
            this.tbBeforeFilter.Location = new System.Drawing.Point(488, 231);
            this.tbBeforeFilter.Name = "tbBeforeFilter";
            this.tbBeforeFilter.ReadOnly = true;
            this.tbBeforeFilter.Size = new System.Drawing.Size(75, 20);
            this.tbBeforeFilter.TabIndex = 15;
            this.tbBeforeFilter.TabStop = false;
            // 
            // label5
            // 
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label5.Location = new System.Drawing.Point(320, 263);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(160, 32);
            this.label5.TabIndex = 14;
            this.label5.Text = "TOTAL number of Records AFTER the filter was applied:";
            // 
            // label4
            // 
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label4.Location = new System.Drawing.Point(320, 223);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(168, 32);
            this.label4.TabIndex = 13;
            this.label4.Text = "TOTAL number of Records BEFORE the filter was applied:";
            // 
            // bSaveMap
            // 
            this.bSaveMap.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.bSaveMap.Location = new System.Drawing.Point(400, 348);
            this.bSaveMap.Name = "bSaveMap";
            this.bSaveMap.Size = new System.Drawing.Size(75, 23);
            this.bSaveMap.TabIndex = 17;
            this.bSaveMap.Text = "Save Map";
            this.bSaveMap.Click += new System.EventHandler(this.bSaveMap_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this._lender);
            this.groupBox1.Controls.Add(this._buyer);
            this.groupBox1.Controls.Add(this._seller);
            this.groupBox1.Location = new System.Drawing.Point(323, 299);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(240, 43);
            this.groupBox1.TabIndex = 18;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Import as";
            // 
            // _seller
            // 
            this._seller.AutoSize = true;
            this._seller.Checked = true;
            this._seller.Location = new System.Drawing.Point(10, 19);
            this._seller.Name = "_seller";
            this._seller.Size = new System.Drawing.Size(51, 17);
            this._seller.TabIndex = 0;
            this._seller.TabStop = true;
            this._seller.Text = "Seller";
            this._seller.UseVisualStyleBackColor = true;
            // 
            // _buyer
            // 
            this._buyer.AutoSize = true;
            this._buyer.Location = new System.Drawing.Point(77, 19);
            this._buyer.Name = "_buyer";
            this._buyer.Size = new System.Drawing.Size(52, 17);
            this._buyer.TabIndex = 1;
            this._buyer.TabStop = true;
            this._buyer.Text = "Buyer";
            this._buyer.UseVisualStyleBackColor = true;
            // 
            // _lender
            // 
            this._lender.AutoSize = true;
            this._lender.Location = new System.Drawing.Point(144, 19);
            this._lender.Name = "_lender";
            this._lender.Size = new System.Drawing.Size(58, 17);
            this._lender.TabIndex = 2;
            this._lender.TabStop = true;
            this._lender.Text = "Lender";
            this._lender.UseVisualStyleBackColor = true;
            // 
            // ImportMask6
            // 
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.bSaveMap);
            this.Controls.Add(this.tbAfterFilter);
            this.Controls.Add(this.tbBeforeFilter);
            this.Controls.Add(this.tbExistingEntries);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.bImport);
            this.Controls.Add(this.bCancel);
            this.Controls.Add(this.bBack);
            this.Controls.Add(this.cbExcludeDuplicates);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.customizedTreeViewUserControl1);
            this.Controls.Add(this.label1);
            this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Name = "ImportMask6";
            this.Size = new System.Drawing.Size(576, 384);
            this.Load += new System.EventHandler(this.ImportMask6_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		#endregion

		#region � Properties �
		#region � BeforeFilter �
		public string BeforeFilter
		{
			get {return this.tbBeforeFilter.Text;}
			set {this.tbBeforeFilter.Text = value;}
		}
		#endregion � BeforeFilter �
		#region � AfterFilter �
		public string AfterFilter
		{
			get {return this.tbAfterFilter.Text;}
			set {this.tbAfterFilter.Text = value;}
		}
		#endregion � AfterFilter �
		#endregion � Properties �

		#region � Methods �
		#region � ImportMask6_Load �
		private void ImportMask6_Load(object sender, System.EventArgs e)
		{			
			//this.alImportIntermediatedObjects = this.ImportData();						
			//this.alFilteredImportIntermediateObjects = this.FilterImportIntermediateObjects(alImportIntermediatedObjects);
			//this.tbBeforeFilter.Text = alImportIntermediatedObjects.Count.ToString();									
			//this.tbAfterFilter.Text = alFilteredImportIntermediateObjects.Count.ToString();
			//apply filters !!!
			
		}
		#endregion � ImportMask6_Load �
		
		#region � TestImportIntermediateObject �
		private bool TestImportIntermediateObject(ImportIntermedeateObject iio, out ArrayList alOutErrors)
		{
			ArrayList alErrors = new ArrayList();
			//test for ALL filters
			IDictionaryEnumerator ide = Globals.htFilters.GetEnumerator();
			while (ide.MoveNext())
			{
				#region � SimpleFilter �
				if (ide.Value is SimpleFilter)
				{
					SimpleFilter sf = (SimpleFilter)ide.Value;

					Type iioType = iio.GetType();			
					PropertyInfo[] pi =  iioType.GetProperties();			
					for (int i = 0; i < pi.Length; i++)
					{
						object[] attr = pi[i].GetCustomAttributes(typeof(DescriptionAttribute), true);
						if (0 != attr.Length)
						{   
							DescriptionAttribute da = (DescriptionAttribute)attr[0];		
							string iioPropertyName = pi[i].Name;
							if (da.Desc == sf.DataColumn)
							{
								Type t = pi[i].PropertyType;																				
								if (t == typeof(System.Int32))
								{
									int iPropVal = 0;
									double dValue = 0;
									try
									{
										iPropVal = (int) pi[i].GetValue(iio, null);
										dValue = Convert.ToDouble(sf.ValueDefined);
										if (false == ApplicationHelper.SimpleFilterResult((double)iPropVal, dValue, sf.Condition))
										{
											alOutErrors = alErrors;
											return false;
										}
									}
									catch (Exception exc)
									{
										alErrors.Add("SIMPLE FILTER: Filtering [to INTEGER value] FAILED for " + sf.DataColumn + " , value" + sf.ValueDefined + " could not be casted! ->" + exc.Message);
										alOutErrors = alErrors;
										return false;
									}

								}
								if (t == typeof(System.Double))
								{
									double dPropVal = 0;
									double dValue = 0;
									try
									{
										dPropVal = (double) pi[i].GetValue(iio, null);
										dValue = Convert.ToDouble(sf.ValueDefined);
										if (false == ApplicationHelper.SimpleFilterResult((double)dPropVal, dValue, sf.Condition))
										{
											alOutErrors = alErrors;
											return false;
										}
									}
									catch (Exception exc)
									{
										alErrors.Add("SIMPLE FILTER: Filtering [to DECIMAL value] FAILED for " + sf.DataColumn + " , value" + sf.ValueDefined + " could not be casted! ->" + exc.Message);
										alOutErrors = alErrors;
										return false;
									}
								}
								break;									
							}
						}
					}					
				}
					#endregion � SimpleFilter �
					#region � MathFilter �
				else if (ide.Value is MathematicalFilter)//MathFilter
				{
					MathematicalFilter mf = ide.Value as MathematicalFilter;
					if (null != mf)
					{
						double leftValue = 0, rightValue = 0;
						int iLeftValue = 0, iRightValue = 0;
						bool leftValueAssigned = false, rightValueAssigned = false;

						Type iioType = iio.GetType();			
						PropertyInfo[] pi =  iioType.GetProperties();			
						for (int i = 0; i < pi.Length; i++)
						{
							object[] attr = pi[i].GetCustomAttributes(typeof(DescriptionAttribute), true);
							if (0 != attr.Length)
							{   
								DescriptionAttribute da = (DescriptionAttribute)attr[0];		
								string iioPropertyName = pi[i].Name;
								if (da.Desc == mf.DataColumn1)
								{																											
									try
									{
										Type t = pi[i].PropertyType;																				
										if (t == typeof(System.Int32))
										{
											iLeftValue = (int) pi[i].GetValue(iio, null);
											leftValue = (double)iLeftValue;
										}
										else if (t == typeof(System.Double))
										{
											leftValue = (double) pi[i].GetValue(iio, null);
										}
										leftValueAssigned = true;
									}
									catch (Exception exc)
									{
										alErrors.Add("MATHEMATICAL FILTER: Filtering FAILED for " + mf.DataColumn1 + "! ->" + exc.Message);
										alOutErrors = alErrors;
										return false;
									}
								}
								else if (da.Desc == mf.DataColumn2)
								{
									try
									{
										Type t = pi[i].PropertyType;
										if (t == typeof(System.Int32))
										{
											iRightValue = (int) pi[i].GetValue(iio, null);
											rightValue = (double)iLeftValue;
										}
										else if (t == typeof(System.Double))
										{
											rightValue = (double) pi[i].GetValue(iio, null);
										}
										rightValueAssigned = true;
									}
									catch (Exception exc)
									{
										alErrors.Add("MATHEMATICAL FILTER: Filtering FAILED for " + mf.DataColumn1 + "! ->" + exc.Message);
										alOutErrors = alErrors;
										return false;
									}

								}
								if (true == leftValueAssigned && true == rightValueAssigned)
								{
									int iResult = 0;
									double dResult = 0;
									try
									{
										Type t = pi[i].PropertyType;
										if (t == typeof(System.Int32))
										{
											iResult = Convert.ToInt32(mf.DefinedValue);
											dResult = (double)iResult;
										}
										else if (t == typeof(System.Double))
										{
											dResult = Convert.ToDouble(mf.DefinedValue);
										}


									}
									catch (Exception exc)
									{
										alErrors.Add("MATHEMATICAL FILTER: Filtering FAILED for " + mf.DataColumn1 + "! ->" + exc.Message);
										alOutErrors = alErrors;
										return false;
									}
									if (false == ApplicationHelper.MathematicalFilterResult((double)leftValue, mf.DefinedOperator, (double)rightValue, mf.ResultCondition, (double)dResult))
									{
										alOutErrors = alErrors;
										return false;
									}
								}
							}
						}
					}
				}
					#endregion � MathFilter �

				else if (ide.Value is ZIPFilter)
				{
					ZIPFilter zf = (ZIPFilter)ide.Value;
					Type iioType = iio.GetType();			
					PropertyInfo[] pi =  iioType.GetProperties();			
					for (int i = 0; i < pi.Length; i++)
					{
						object[] attr = pi[i].GetCustomAttributes(typeof(DescriptionAttribute), true);
						if (0 != attr.Length)
						{   
							DescriptionAttribute da = (DescriptionAttribute)attr[0];		
							string iioPropertyName = pi[i].Name;

							if (da.Desc.Equals(zf.PropertyName))
							{
								string zip = (string) pi[i].GetValue(iio, null);
								if (false == zf.TestForValue(zip, true))	//FORCE COMPARISMENT UPON FIRST 5 CHARACTERS
								{
									alOutErrors = alErrors;
									return false;
								}
							}
						}
					}
				}

				else if (ide.Value is DateFilter)
				{
					DateFilter df = (DateFilter)ide.Value;
					Type iioType = iio.GetType();			
					PropertyInfo[] pi =  iioType.GetProperties();			
					for (int i = 0; i < pi.Length; i++)
					{
						object[] attr = pi[i].GetCustomAttributes(typeof(DescriptionAttribute), true);
						if (0 != attr.Length)
						{   
							DescriptionAttribute da = (DescriptionAttribute)attr[0];		
							string iioPropertyName = pi[i].Name;

							if (da.Desc.Equals(df.PropertyName))
							{								
								DateTime dDate = DateTime.MinValue;
								string sDate = string.Empty;
								if ("Deed Date" != da.Desc && "Loan Recording Date" != da.Desc)
								{
									dDate = (DateTime) pi[i].GetValue(iio, null);
									if (false == df.TestForValue(dDate.ToShortDateString()))
									{
										alOutErrors = alErrors;
										return false;
									}
								}
								else
								{
									sDate = (string) pi[i].GetValue(iio, null);
									if (false == df.TestForValue(sDate))
									{
										alOutErrors = alErrors;
										return false;
									}
								}

							}
						}
					}
				}
			}
			alOutErrors = alErrors;						

			return true;
		}
		#endregion � TestImportIntermediateObject �

		#region � FilterImportIntermediateObjects �
		public ArrayList FilterImportIntermediateObjects(ArrayList alImportIntermediatedObjects)
		{
			//Event trigger
			EventTrigger et = new EventTrigger();
			PBForm pbf = new PBForm(et);
			pbf.Capacity = alImportIntermediatedObjects.Count;
			pbf.WindowName = "Applying Filters...";			
			pbf.Show();

			ArrayList alFilteredImportIntermediateObjects = new ArrayList();
			ArrayList alFilterErrors = new ArrayList();
			foreach (ImportIntermedeateObject iio in alImportIntermediatedObjects)
			{
				ArrayList alErrors = new ArrayList();
				if (true == this.TestImportIntermediateObject(iio, out alErrors))
				{
					alFilteredImportIntermediateObjects.Add(iio);					
				}				
				//fire event
				et.TriggerChange();
			}
			return alFilteredImportIntermediateObjects;
		}
		#endregion � FilterImportIntermediateObjects �

		#region � bBack_Click �
		private void bBack_Click(object sender, System.EventArgs e)
		{
			Globals.importWindowPos = this.Location;
			//this.Close();
			ImportMask5 im5 = new ImportMask5();
			im5.InputDelimiter = this.InputDelimiter;
			im5.SelectedFile = this.SelectedFile;
			im5.SelectedProfile = this.SelectedProfile;
			im5.CreateNewProfile = this.CreateNewProfile;
			im5.Checked = this.Checked;
			im5.Show();	
		}
		#endregion � bBack_Click �
		#region � bCancel_Click �
		private void bCancel_Click(object sender, System.EventArgs e)
		{
			if (DialogResult.Yes == MessageBox.Show(this, UserMessages.CancelImportText, UserMessages.CancelImport, MessageBoxButtons.YesNo, MessageBoxIcon.Question))
			{
				Globals.importWizardStarted = false;
				//this.Close();
			}
		}
		#endregion � bCancel_Click �
		#region � bImport_Click �
		private void bImport_Click(object sender, System.EventArgs e)
		{
			if (null == this.selectedNode)
			{
				MessageBox.Show("Please select group where to import data!");
				return;
			}

			if (1 == ((Node)this.selectedNode.Tag).nodeId)
			{
				MessageBox.Show("Please select or create the import destination group first!");
				return;
			}

            if (!AllowImport(_selectedContactType, ((Node)this.selectedNode.Tag).nodeId))
            {
                if (ContactTypes.Buyer == _selectedContactType)
                {
                    MessageBox.Show("Buyers can get assigned only to " + ContactTypesManager.CashBuyersGroup + " and its subgroups.", "Import records", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                if (ContactTypes.Lender == _selectedContactType)
                {
                    MessageBox.Show("Lenders can get assigned only to " + ContactTypesManager.PrivateLenderGroup + " and its subgroups.", "Import records", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                if (ContactTypes.Seller == _selectedContactType)
                {
                    MessageBox.Show("Sellers can't get assigned to " + ContactTypesManager.PrivateLenderGroup + " or " + ContactTypesManager.CashBuyersGroup + " or their subgroups.", "Import records", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
            }

			Panel p = (Panel)this.Parent;
			ImportMask im = (ImportMask)p.Parent;
			if (true == im.ImportedData)
			{
				if (DialogResult.Yes != MessageBox.Show(this, "Do you want to import SAME data again?", "You have already imported those records", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation))
					return;
			}

			Globals.importWindowPos = this.Location;
			int nodeId = ((Node)this.selectedNode.Tag).nodeId;
			if (null != this.alFilteredImportIntermediateObjects && 0 != this.alFilteredImportIntermediateObjects.Count)
			{
				ArrayList alPropertyItemIds = MapToPropertyItem(this.alFilteredImportIntermediateObjects);	
				if (null != alPropertyItemIds)
				{
					this.AssignToNode(alPropertyItemIds, nodeId);
				}
			}
			this.GetPropertyCountInNode(nodeId);

			ImportMask7 im7 = new ImportMask7(this.tbAfterFilter.Text, this.tbExistingEntries.Text);
			im7.ShowDialog();
			im.ImportedData = true;
			//this.Close();
		}
		#endregion � bImport_Click �
		#region � ImportData �
		public ArrayList ImportData ()
		{
			//Event trigger
			EventTrigger et = new EventTrigger();
			PBForm pbf = new PBForm(et);
			pbf.Capacity = Globals.Capacity;
			if (true == this.Checked)
				pbf.Capacity -= 1;
			pbf.WindowName = "Importing Data...";			
			pbf.Show();

			ArrayList alImportIntermediatedObjects = new ArrayList();
			ArrayList alErrors = new ArrayList();
			using (StreamReader sr = new StreamReader(this.SelectedFile)) 
			{
				char delimiter = ApplicationHelper.GetDelimiter(this.InputDelimiter);
				string line = string.Empty;
				int iLineCnt = 0;
				bool filters = false;
				while ((line = sr.ReadLine()) != null) 
				{
					iLineCnt++;					
					if (1 == iLineCnt && true == this.Checked)
					{
						continue;	//first row contains header names, no real data yet!
					}

					if (string.Empty != line && false == filters)
					{
						Hashtable htDelimiterPositions = this.GetDelimiterPositions(line, delimiter);
						ImportIntermedeateObject iio = new ImportIntermedeateObject();
						//this.validIIO = true;
						IDictionaryEnumerator ide = Globals.htMappingInfo.GetEnumerator();
						while (ide.MoveNext())
						{
							MapperSourceInfo msi = (MapperSourceInfo)ide.Value;
							if (false == msi.IsInitialized())
							{
								continue;
							}
							else//handle it
							{								
								string valueFromFile = string.Empty;
								int columnIndex = msi.ColumnIndex;
								MapperObjectInfo moi = (MapperObjectInfo)ide.Key;
								string propertyName = moi.PropertyName;																

								//find value
								if (0 == columnIndex)
								{
									int endDelimiterPosition = (int)htDelimiterPositions[columnIndex];
									valueFromFile = line.Substring(0, endDelimiterPosition);									
								}
								else
								{
									/*search from columnIndex-th delimiter until you either:
									 *															find  nextOne or
									 *															reach end of line
									 */
									int endDelimiterPosition = 0;
									int prevDelimiterPosition = 0;
									try
									{
										endDelimiterPosition = (int)htDelimiterPositions[columnIndex];
										prevDelimiterPosition = (int)htDelimiterPositions[columnIndex - 1];
									}
									catch (Exception /*exc*/)
									{
#if DEBUG
										//MessageBox.Show("Exception: " + exc.Message + "\nInner Message: " + exc.InnerException + "\nStackTrace: " + exc.StackTrace + "\nAt Line: " + iLineCnt.ToString());
#endif
										MessageBox.Show(this, "Possible corrupted File! Take a look at the row: " + iLineCnt.ToString(), "Error while reading Import File", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
										continue;
									}
																		
									valueFromFile = line.Substring(prevDelimiterPosition + 1, endDelimiterPosition - prevDelimiterPosition - 1);									
								}
								
								Type iioType = iio.GetType();			
								PropertyInfo[] pi =  iioType.GetProperties();			
								for (int i = 0; i < pi.Length; i++)
								{
									object[] attr = pi[i].GetCustomAttributes(typeof(DescriptionAttribute), true);
									if (0 != attr.Length)
									{      										
										string iioPropertyName = pi[i].Name;
										if (iioPropertyName == propertyName)
										{
											Type t = pi[i].PropertyType;
											
											#region � System.Int32 �
											if (t == typeof(System.Int32))
											{													
												try
												{													
													//valueFromFile = ApplicationHelper.PurifyStringToIntOrDoubleCastable(valueFromFile);
													//valueFromFile = valueFromFile.Replace("$", string.Empty);
													//valueFromFile = valueFromFile.Replace("\"", string.Empty);
													System.Int32 intValue = 0;
													if (string.Empty != valueFromFile)
													{
														intValue = Convert.ToInt32(valueFromFile);
													}
													pi[i].SetValue(iio, intValue, null);
												
												}
												catch(Exception exc)
												{
													System.Int32 intValue = 0;
													string s = "Row: " + iLineCnt +", Property: " + propertyName+ ", Message: Could not cast to Integer " + valueFromFile + " " + t.ToString() + " " + exc.Message;
													alErrors.Add(s);
													pi[i].SetValue(iio, intValue, null);
												}
											}														
											#endregion � System.Int32 �
											#region � System.Double �
											if (t == typeof(System.Double))
											{
												try
												{
													valueFromFile = ApplicationHelper.PurifyStringToIntOrDoubleCastable(valueFromFile);
													//valueFromFile = valueFromFile.Replace("$", string.Empty);
													//valueFromFile = valueFromFile.Replace("\"", string.Empty);
													
													System.Double doubleValue = 0;
													if (string.Empty != valueFromFile)
													{
														//doubleValue = Convert.ToDouble(valueFromFile);
														System.IFormatProvider format =	new System.Globalization.CultureInfo("en-US", true);
														doubleValue = System.Double.Parse(valueFromFile, format);
													}
													pi[i].SetValue(iio, doubleValue, null);
													
												}
												catch(Exception exc)
												{
													System.Double doubleValue = 0;
													string s = "Row: " + iLineCnt +", Property: " + propertyName+ ", Message: Could not cast to Double " + valueFromFile + " " + t.ToString() + " " + exc.Message;
													alErrors.Add(s);
													pi[i].SetValue(iio, doubleValue, null);
												}														
											}
											#endregion � System.Double �
											#region � System.DateTime �
											if (t == typeof(System.DateTime))
											{
												try
												{
													System.DateTime dateTime = new DateTime(1, 1, 1);
													
													if (string.Empty != valueFromFile)
													{
														//dateTime = Convert.ToDateTime(valueFromFile);
														System.IFormatProvider format =	new System.Globalization.CultureInfo("en-US", true);														
														dateTime = System.DateTime.Parse(valueFromFile, format,	System.Globalization.DateTimeStyles.NoCurrentDateDefault);
													}													
													pi[i].SetValue(iio, dateTime, null);	
												}
												catch(Exception exc)
												{
													System.DateTime dateTime = new DateTime(1, 1, 1);
													string s = "Row: " + iLineCnt +", Property: " + propertyName+ ", Message: Could not cast to DateTime " + valueFromFile + " " + t.ToString() + " " + exc.Message;
													alErrors.Add(s);
													pi[i].SetValue(iio, dateTime, null);
												}
											}
											#endregion � System.DateTime �
											#region � System.String �
											if (t == typeof(System.String))
											{
												try
												{
													//TA++ advanced ZIP filter 23.Jan.2006
													//if (iioPropertyName == "SiteZIP")
													//{
													//	if ((null != this.AllowedZIPs) && (false == this.AllowedZIPs.Contains(valueFromFile)))
													//	{
													//		this.validIIO = false;
													//	}
													//}
													string strippedValueFromFile = valueFromFile.Replace("\"", string.Empty);
													
													//TA++: removed 2007, Jan. 16th
													//
													//if ((this.TrimZIP) && (iioPropertyName == "SiteZIP" || iioPropertyName == "MailingZIP"))
													//{
													//	strippedValueFromFile = strippedValueFromFile.Substring(0, 5);
													//}
													//TA--

													pi[i].SetValue(iio, strippedValueFromFile, null);
																										
												}
												catch(Exception exc)
												{
													string stringValue = string.Empty;
													string s = "Row: " + iLineCnt +", Property: " + propertyName+ ", Message: Problems with string " + valueFromFile + " " + t.ToString() + " " + exc.Message;
													alErrors.Add(s);
													pi[i].SetValue(iio, stringValue, null);
												}
											}
											#endregion � System.String �
										}
									}
								}									

							}
						}
						//if (true == this.validIIO)
						//{
							alImportIntermediatedObjects.Add(iio);
						//}
					}
					//fire event
					et.TriggerChange();
				}
			}
			if (alErrors.Count > 0)
			{
				this.SaveErrorLog(alErrors);
			}
			return alImportIntermediatedObjects;			
		}
		#endregion � ImportData �
		#region � SaveErrorLog �
		private void SaveErrorLog(ArrayList alErrors)
		{
			string errorLogName = this.SelectedFile.Substring(0, this.SelectedFile.Length - 4);
			errorLogName += "_ERROR_LOG.txt";
			FileInfo fi = new FileInfo(errorLogName);						
			StreamWriter sw = fi.CreateText();
			for (int i = 0; i < alErrors.Count; i++)
			{
				sw.WriteLine((string)alErrors[i]);
			}
			sw.Flush();
			sw.Close();
			if (DialogResult.Yes == MessageBox.Show(this, UserMessages.ProblemsWithImportFileDesc, UserMessages.ProblemsWithImportFile, MessageBoxButtons.YesNo, MessageBoxIcon.Information))
			{
				Process.Start("notepad.exe", errorLogName);
			}			
		}
		#endregion � SaveErrorLog �
		#region � GetDelimiterPositions �		
		private Hashtable GetDelimiterPositions(string line, char delimiter)
		{
			Hashtable	htDelimiterPositions = new Hashtable();
			if (delimiter != ',')
			{
				int startIndex = -1;
				int pos = 0;
			
				while( true )
				{
		
					//search from the beginning of line until you reach first delimiter
					startIndex = line.IndexOf(delimiter, startIndex + 1, 	line.Length - startIndex - 1 );

					// Exit the loop if the target is not found.
					if( startIndex < 0 )
					{
						break;
					}
					else
					{
						htDelimiterPositions.Add(pos, startIndex);
					}
					pos++;
				}
				htDelimiterPositions.Add(pos++, line.Length);	//last entry!
				return htDelimiterPositions;
			}
			else
			{
				//take care of commas as value !!!
				ArrayList result = new ArrayList();
				this.ParseCSVFields(result, line, htDelimiterPositions);
				return htDelimiterPositions;
			}
		}
		#endregion � GetDelimiterPositions �

        void GetNames(string fullName, bool lastNameFirst, out string firstName, out string lastName)
        {
            firstName = string.Empty;
            lastName = string.Empty;

            try
            {
                fullName = fullName.Trim();
                int blankPos1 = fullName.IndexOf(" ");

                if (-1 == blankPos1) //single word
                {
                    if (lastNameFirst)
                        lastName = fullName;
                    else
                        firstName = fullName;

                    return;
                }

                lastName = fullName.Substring(0, blankPos1).Trim();
                int blankPos2 = fullName.IndexOf(" ", blankPos1 + 1);

                if (-1 == blankPos2)
                    firstName = fullName.Substring(blankPos1 + 1, fullName.Length - (blankPos1) - 1);
                else
                    firstName = fullName.Substring(blankPos1 + 1, blankPos2 - (blankPos1) - 1);
            }
            catch
            {
                firstName = string.Empty;
                lastName = string.Empty;
            }

            if (!lastNameFirst)
            {
                string temp = firstName;
                firstName = lastName;
                lastName = temp;
            }
        }

		#region � MapToPropertyItem �
		private ArrayList MapToPropertyItem ( ArrayList alImportIntermedeateObjects)
		{
			
			try
			{
				//Event trigger
				EventTrigger et = new EventTrigger();
				PBForm pbf = new PBForm(et);
				try
				{
					pbf.Capacity = Convert.ToInt32(this.AfterFilter);//alImportIntermediatedObjects.Count;
				}
				catch
				{
					pbf.Capacity = alImportIntermediatedObjects.Count;
				}
				pbf.WindowName = "Importing Data...";			
				pbf.Show();

				ArrayList alPropertyItemIds = new ArrayList();
				Connection conn = Connection.getInstance();     
				SqlConnection sqlConn = conn.HatchConn;
				IDbConnection iSqlConn = sqlConn as IDbConnection;

				//NHibernateUpdate
				/*
				Configuration cfg = new Configuration();								

				cfg.AddAssembly("DealMaker");                  

				ISessionFactory factory = cfg.BuildSessionFactory();*/
				Configuration cfg = Globals.cfg;
				//ISessionFactory factory = Globals.factory;
				
				//ISession session = null;
				if (null != iSqlConn) 
				{
					//session = factory.OpenSession(iSqlConn);
					//if (ConnectionState.Closed == iSqlConn.State) 
					{
						//iSqlConn.Open();
					}
				}
				else 
				{
					//session = factory.OpenSession();
				}

				//ArrayList alPropertyItems = new ArrayList();
				foreach (ImportIntermedeateObject iio in alImportIntermedeateObjects)
				{
					PropertyItem pi = new PropertyItem();
					alPropertyItemIds.Add(pi.IdPropertyItem);
				
					#region � Owner �
					Contact owner = new Contact();

					owner.STCellphone = iio.SkipTracedCellPhone1.Trim();
					owner.STLandline = iio.SkipTracedLandLine1.Trim();
					owner.STEmail = iio.SkipTracedEmail1.Trim();

                    var skipTracedData = new SkiptraceClientResultset
                    {
                        MobilePhones = new List<string>(3),
                        ResidentialPhones = new List<string>(2),
                        Emails = new List<string>(3)
				    };

                    skipTracedData.MobilePhones.AddRange(new [] { iio.SkipTracedCellPhone1, iio.SkipTracedCellPhone2, iio.SkipTracedCellPhone3 });
                    skipTracedData.ResidentialPhones.AddRange(new [] {iio.SkipTracedLandLine1, iio.SkipTracedLandLine2});
                    skipTracedData.Emails.AddRange(new [] {iio.SkipTracedEmail1, iio.SkipTracedEmail2, iio.SkipTracedEmail3 });
                    owner.ST_P1_RESPONSE = JsonConvert.SerializeObject(skipTracedData);

					owner.WorkPhone = new Phone(iio.OwnerPhoneWork.Trim(), string.Empty);
					owner.CellPhone = new Phone(iio.OwnerMobile.Trim(), string.Empty);

					#region � Name (First, Middle, Last, Full, GoBy) �
					//TA, May, 01. 2006 (added trimends)
					owner.FirstName = iio.FirstOwnerName.Trim();//.TrimEnd(' ');
					owner.MiddleName = iio.MiddleOwnerName.Trim();//.TrimEnd(' ');
					owner.LastName = iio.LastOwnerName.Trim();//.TrimEnd(' ');
					owner.FullName = iio.FullOwnerName.Trim();//.TrimEnd(' ');

                    if (this.separateFirstName)
                    {
                        string firstName = string.Empty;
                        string lastName = string.Empty;

                        GetNames(owner.FullName, this.lastNameFirst, out firstName, out lastName);
                        owner.FirstName = firstName;
                        owner.LastName = lastName;
                    }

					//owner.GoBy			= iio.SalutationOwnerName;				
					#endregion � Name (First, Middle, Last, Full, GoBy) �								
					#region � Phone (Home, Cell, Work), Fax, EMail �					
					owner.HomePhone = new Phone(iio.OwnerPhoneHome, string.Empty);
					//owner.CellPhone = new Phone(iio.OwnerMobile, string.Empty);
					//owner.WorkPhone = new Phone(iio.OwnerPhoneWork, string.Empty);				
					owner.EMail		= iio.OwnerEmail;				
					owner.FaxNumber = string.Empty;					
					#endregion � Phone (Home, Cell, Work), Fax, EMail �				
					#region � SiteAddress �						
					if 
						(
						string.Empty != iio.FullSiteStreetAddress ||
						string.Empty != iio.SiteStreetNumber ||
						string.Empty != iio.SiteStreetPreDirectional ||
						string.Empty != iio.SiteStreetName ||
						string.Empty != iio.SiteStreetPostDirectional ||
						string.Empty != iio.SiteSteeetSuffix ||
						string.Empty != iio.SiteStreetUnitNumber ||
						string.Empty != iio.SiteCity ||
						string.Empty != iio.SiteState ||
						string.Empty != iio.SiteZIP ||
						string.Empty != iio.SiteCitySiteState ||
						//string.Empty != iio.SiteCitySiteStateSiteZIP ||
						string.Empty != iio.SiteCountry	||
						string.Empty != this.SelectedState
						)
					{
						DealMaker.Address siteAddress = new Address();								
						siteAddress.FullSiteStreetAddress		= iio.FullSiteStreetAddress;
						siteAddress.SiteStreetNumber			= iio.SiteStreetNumber;
						siteAddress.SiteStreetPreDirectional	= iio.SiteStreetPreDirectional;
						siteAddress.SiteStreetName				= iio.SiteStreetName;								
						siteAddress.SiteStreetPostDirectional	= iio.SiteStreetPostDirectional;				
						siteAddress.SiteStreetSuffix			= iio.SiteSteeetSuffix;
						siteAddress.SiteStreetUnitNumber		= iio.SiteStreetUnitNumber;								
						siteAddress.SiteCity					= iio.SiteCity;
						if (string.Empty != this.SelectedState)
						{
							siteAddress.SiteState = this.SelectedState;
						}
						else
						{
							siteAddress.SiteState					= iio.SiteState;
						}
						siteAddress.SiteZIP						= iio.SiteZIP;
						siteAddress.SiteCitySiteState			= iio.SiteCitySiteState;
						//siteAddress.SiteCitySiteStateSiteZIP	= iio.SiteCitySiteStateSiteZIP;
						siteAddress.SiteCountry = iio.SiteCountry;
						owner.SiteAddress = siteAddress;
					}
					else
					{
						owner.SiteAddress = null;
					}
					#endregion � SiteAddress �
					#region � MailAddress �					
					if 
						(
						string.Empty !=  iio.FullMailingStreetAddress || 
						string.Empty !=  iio.MailingStreetNumber || 
						string.Empty !=  iio.MailingStreetPreDirectional || 
						string.Empty !=  iio.MailingStreetName || 
						string.Empty !=  iio.MailingStreetPostDirectional || 
						string.Empty !=  iio.MailingSteeetSuffix || 
						string.Empty !=  iio.MailingStreetUnitNumber || 
						string.Empty !=  iio.MailingCity || 
						string.Empty !=  iio.MailingState || 
						string.Empty !=  iio.MailingZIP || 
						string.Empty !=  iio.MailingCityMailingState //|| 
						//string.Empty !=  iio.MailingCityMailingStateMailingZIP
						)
					{
						Address mailAddress	= new Address();
						mailAddress.FullSiteStreetAddress		= iio.FullMailingStreetAddress;
						mailAddress.SiteStreetNumber			= iio.MailingStreetNumber;
						mailAddress.SiteStreetPreDirectional	= iio.MailingStreetPreDirectional;
						mailAddress.SiteStreetName				= iio.MailingStreetName;
						mailAddress.SiteStreetPostDirectional	= iio.MailingStreetPostDirectional;				
						mailAddress.SiteStreetSuffix			= iio.MailingSteeetSuffix;
						mailAddress.SiteStreetUnitNumber		= iio.MailingStreetUnitNumber;								
						mailAddress.SiteCity					= iio.MailingCity;
						mailAddress.SiteState					= iio.MailingState;
						mailAddress.SiteZIP						= iio.MailingZIP;
						mailAddress.SiteCitySiteState			= iio.MailingCityMailingState;
						//mailAddress.SiteCitySiteStateSiteZIP	= iio.MailingCityMailingStateMailingZIP;
						mailAddress.SiteCountry					= string.Empty;
						owner.MailAddress = mailAddress;
					}
					else
					{
						owner.MailAddress = null;
					}
					#endregion � MailAddress �
					pi.Owner = owner;
					#endregion � Owner �
					#region � ListingAgent �
					if 
						(
						string.Empty !=	iio.ListingAgentFullNane || 
						string.Empty !=	iio.ListingAgentFirstName || 
						string.Empty !=	iio.ListingAgentLastName || 
						string.Empty !=	iio.ListingAgentPhoneNumber || 
						string.Empty !=	iio.ListingAgentFaxNumber || 
						string.Empty !=	iio.ListingAgentOffice || 
						string.Empty !=	iio.ListingAgentCode || 
						string.Empty !=	iio.ListingMLSNr || 
						0			 !=	iio.ListingPrice || 
						string.Empty !=	iio.ListingAgentEmail
						)
					{
						ListingAgent la = new ListingAgent();
						la.ListingAgentFullName			= iio.ListingAgentFullNane;
						la.ListingAgentFirstName		= iio.ListingAgentFirstName;
						la.ListingAgentLastName			= iio.ListingAgentLastName;
						la.ListingAgentPhoneNumber		= iio.ListingAgentPhoneNumber;
						la.ListingAgentFaxNumber		= iio.ListingAgentFaxNumber;
						la.ListingAgentOffice			= iio.ListingAgentOffice;
						la.ListingAgentCode				= iio.ListingAgentCode;
						la.ListingAgentMLSNumber		= iio.ListingMLSNr;
						la.ListingPrice					= iio.ListingPrice;
						la.ListingAgentEMail			= iio.ListingAgentEmail;				
						pi.ListingAgent = la;
					}
					else
					{
						pi.ListingAgent = null;
					}
					#endregion � ListingAgent �
					#region � DateCreated �
					pi.DateCreated = DateTime.Now;
					#endregion � DateCreated �
					#region � DateLastModified �
					pi.DateLastModified = DateTime.Now;
					#endregion � DateLastModified �
					#region � DealInfo �
					DealInfo di = new DealInfo();
					#region � FinancialProperties �
					if 
						(
							string.Empty != iio.Status || 
							string.Empty != iio.ParcelNumber || 
							new DateTime(1, 1, 1) != iio.LastSaleDate || 
							string.Empty != iio.SaleDocumentNumber || 
							0			 != iio.CostPerSquareFoot || 
							0			 != iio.FirstLoan || 
							0			 != iio.SecondLoan || 
							string.Empty != iio.FirstLoanType || 
							0			 != iio.FirstLoanInterestRate || 
							string.Empty != iio.Lender || 
							0			 != iio.Loans || 
							0			 != iio.Equity || 
							0			 != iio.CashDownPayment
						)
					{
						FinancialProperties fp = new FinancialProperties();
						fp.Status				= iio.Status;
						fp.ParcelNumber			= iio.ParcelNumber;
						fp.LastSaleDate			= iio.LastSaleDate;
						fp.LastSaleValue		= iio.LastSaleValue;
						fp.SaleDocumentNumber	= iio.SaleDocumentNumber;
						fp.CostPerSquareFoot	= iio.CostPerSquareFoot;
						fp.FirstLoan			= iio.FirstLoan;
						fp.SecondLoan			= iio.SecondLoan;
						fp.FirstLoanType		= iio.FirstLoanType;
						fp.FirstLoanInterestRate = iio.FirstLoanInterestRate.ToString();
						fp.Lender				= iio.Lender;
						fp.Loans				= iio.Loans;
						fp.Equity				= iio.Equity;
						fp.CashDownPayment		= iio.CashDownPayment;
						fp.DeedDate				= iio.DeedDate;
						fp.BookAPN				= iio.BookNumberPartOfAPN;
						fp.MapAPN				= iio.MapNumberPartOfAPN;
						fp.ParcelAPN			= iio.ParcelNumerPartOfAPN;
						fp.ParcelNumAPN			= iio.ParcelNumSuffixPartOfAPN;
						fp.ParcelNumber			= iio.ParcelNumber;
						fp.EstimatedHomeValue	= iio.EstimatedHomeValue;
						fp.CostPerSquareFoot    = iio.CostPerSquareFoot;
						di.FinancialProperty = fp;
					}
					else
					{
						di.FinancialProperty = null;
					}
					#endregion � FinancialProperties �
					#region � TaxProperties �
					if 
						(
							0			 != iio.TaxAssessedValue || 
							0			 != iio.FullCashValue || 
							string.Empty != iio.TaxStatus || 
							0			 != iio.TaxDeliquentYear || 
							0			 != iio.TaxAmount || 
							string.Empty != iio.TaxRateArea || 
							string.Empty != iio.TaxExemption || 
							0			 != iio.PercentImprovement || 
							string.Empty != iio.UseCode || 
							string.Empty != iio.OwnerOccupiedAbsent || 
							0			 != iio.LandValue

						)
					{
						TaxProperties tp = new TaxProperties();
						tp.TaxAssessedValue			= iio.TaxAssessedValue;
						tp.FullCashValue			= iio.FullCashValue.ToString();
						tp.TaxStatus				= iio.TaxStatus;
						tp.TaxDeliquentYear			= iio.TaxDeliquentYear.ToString();
						tp.TaxAmount				= iio.TaxAmount.ToString();
						tp.TaxRateArea				= iio.TaxRateArea;
						tp.TaxExemption				= iio.TaxExemption;
						tp.PercentImprovement		= iio.PercentImprovement.ToString();
						tp.UseCode					= iio.UseCode;
						tp.OwnerAbsentOccupied		= iio.OwnerOccupiedAbsent.ToString();
						tp.LandValue				= iio.LandValue.ToString();
						di.TaxProperty = tp;
					}
					else 
					{
						di.TaxProperty = null;
					}
					#endregion � TaxProperties �
					#region � ForeClosureProperties �
					if 
						(
							string.Empty != iio.ForeClosureStatus || 
							string.Empty != iio.ForeClosureFileNumber || 
							new DateTime(1, 1, 1) != iio.ForeClosureFileDate || 
							0 != iio.OriginalMortgageBalance || 							
							new DateTime(1, 1, 1) != iio.ForeClosureSaleDate ||							
							0 != iio.OpenBidAmount ||
							string.Empty != iio.UnpayedBalance ||
							string.Empty != iio.loanRecordingNumber ||
							string.Empty != iio.LoanRecordingDate ||
							string.Empty != iio.OriginalTrustor ||
							string.Empty != iio.forecClosureSaleTime ||
							string.Empty != iio.ForeclosureSalePlace ||
							string.Empty != iio.TrusteeSaleNumber ||
							string.Empty != iio.TrusteeName ||
							string.Empty != iio.TrusteePhoneNumber ||
							string.Empty != iio.TrusteeFullStreetAddress ||
							string.Empty != iio.TrusteeCity ||
							string.Empty != iio.TrusteeState ||
							string.Empty != iio.TrusteeZIP ||
							string.Empty != iio.MortgageBeneficiaryFirst ||
							string.Empty != iio.MortgageBeneficiaryLast ||
							string.Empty != iio.UnpayedBalance	||
							0 != iio.OpenBidAmount
						)
					{
						ForeClosureProperties fcp = new ForeClosureProperties();
						fcp.ForeclosureStatus		= iio.ForeClosureStatus;
						fcp.ForeclosureFileNumber	= iio.ForeClosureFileNumber;
						fcp.ForeclosureFileDate		= iio.ForeClosureFileDate.ToShortDateString();
						fcp.OriginalMortgageBalance = iio.OriginalMortgageBalance.ToString();
						//fcp.ReinstatementAmount		= iio.ReInstatementAmount.ToString();
						fcp.ForeclosureSaleDate		= iio.ForeClosureSaleDate.ToShortDateString();
						fcp.OpenBidAmount =			iio.OpenBidAmount.ToString();
						fcp.UnpaidBalance			= iio.UnpayedBalance;
						fcp.LoanRecordingNumber		= iio.loanRecordingNumber;
						fcp.LoanRecordingDate		= iio.LoanRecordingDate;
						fcp.OriginalTrustor			= iio.OriginalTrustor;
						fcp.ForeclosureSaleTime		= iio.forecClosureSaleTime;
						fcp.ForeclosureSalePlace	= iio.ForeclosureSalePlace;
						fcp.TrusteeSaleNumber		= iio.TrusteeSaleNumber;
						fcp.TrusteeName				= iio.TrusteeName;
						fcp.TrusteePhoneNr			= iio.TrusteePhoneNumber;
						fcp.TrusteeFullStreetAddress	= iio.TrusteeFullStreetAddress;
						fcp.TrusteeCity				= iio.TrusteeCity;
						fcp.TrusteeState			= iio.TrusteeState;
						fcp.TrusteeZip				= iio.TrusteeZIP;
						fcp.MortgageBeneficiaryFirst = iio.MortgageBeneficiaryFirst;
						fcp.MortgageBeneficiaryLast	 = iio.MortgageBeneficiaryLast;
						fcp.UnpaidBalance			= iio.UnpayedBalance;
						fcp.OpenBidAmount			= iio.OpenBidAmount.ToString("c");
						di.ForeClosureProperty = fcp;
					}
					else
					{
						di.ForeClosureProperty = null;
					}
					#endregion � ForeClosureProperties �
					#region � HouseProperties �
					if 
						(
							0 != iio.Bedrooms || 
							0 != iio.Bathrooms || 
							string.Empty != iio.Garage || 
							string.Empty !=  iio.Pool || 
							0 != iio.HomeSqFt || 
							0 != iio.LotSqFt || 
							0 != iio.YearBuilt //|| 
							//0 != iio.Story //|| 
							//string.Empty != iio.FirePlace							
						)
					{
						HouseProperties hp = new HouseProperties();
						hp.Bedrooms		= iio.Bedrooms;
						hp.Bathrooms	= iio.Bathrooms;
						hp.Garage		= iio.Garage;
						hp.Pool			= iio.Pool;
						hp.HomeSqFt		= iio.HomeSqFt;
						hp.LotSqFt		= iio.LotSqFt;
						hp.YearBuilt	= iio.YearBuilt;
						//hp.Story		= iio.Story;
						//hp.Fireplace	= iio.FirePlace;
						hp.PropertyType	= iio.PropertyType;
						di.HouseProperty = hp;
					}
					else
					{
						di.HouseProperty = null;
					}
					#endregion � HouseProperties �
					#region � LoanProperties �									
					if (0 != iio.Loans)
					{
						LoanProperties lp = new LoanProperties();	
						lp.TotalLoans = iio.Loans;
						di.LoanProperty = lp;
					}
					else
					{
						di.LoanProperty = null;
					}
					
					#endregion � LoanProperties �
					#region � DistrictProperties �
					DistrictProperties dp = new DistrictProperties();
					if 
						(
						string.Empty != iio.SubDivision ||
						string.Empty !=  iio.LegalDescription ||
						string.Empty != iio.Vesting
						)
					{
						dp.Subdivision		= iio.SubDivision;
						dp.LegalDescription = iio.LegalDescription;
						dp.Vesting			= iio.Vesting;
						dp.MapArea			= iio.MapArea;
						dp.MapGrid			= iio.MapGrid;
						di.DistrictProperty = dp;
					}
					else
					{
						di.DistrictProperty = null;
					}
					#endregion � DistrictProperties �
					#region � LeadSourceProperties �
					//LeadSourceProperties lsp = new LeadSourceProperties();
					di.LeadSourceProperty = null;
					#endregion � LeadSourceProperties �
					#region � DivorceProperties �
					if 
						(
							string.Empty != iio.DivorceStatus ||
							new DateTime(1, 1, 1) != iio.DivorceDate

						)
					{
						DivorceProperties dvp = new DivorceProperties();
						dvp.DivorceStatus	= iio.DivorceStatus;
						dvp.DivorceDate		= iio.DivorceDate;
						di.DivorceProperty = dvp;
					}
					else
					{
						di.DivorceProperty = null;
					}
					#endregion � DivorceProperties �				
					#region � OtherProperties �										
					if (string.Empty != iio.MailCRRT || string.Empty != iio.MailScore || string.Empty != iio.PrivacyFlag || string.Empty != iio.DoNotCallFlag)
					{
						OtherProperties op = new OtherProperties();
						op.MailCRRT = iio.MailCRRT;
						op.MailScore = iio.MailScore;
						op.PrivacyFlag = iio.PrivacyFlag;
						op.DoNotCallFlag = iio.DoNotCallFlag;
						di.OtherProperty = op;
					}
					else
					{
						di.OtherProperty = null;
					}

					#endregion � OtherProperties �
					if 
						(
							null != di.FinancialProperty ||
							null != di.TaxProperty ||
							null != di.ForeClosureProperty ||
							null != di.HouseProperty ||
							null != di.LoanProperty ||
							null != di.DistrictProperty ||
							null != di.LeadSourceProperty ||
							null != di.DivorceProperty ||
							null != di.OtherProperty
						)
					{
						pi.DealInfo = di;
					}
					else
					{
						pi.DealInfo = null;
					}
					#endregion � DealInfo �												

					//ITransaction transaction = session.BeginTransaction();
			
					// Tell NHibernate that this object should be saved
					//pi.NhSave(session);
					pi.ExecuteSPL(sqlConn);
					
					// commit all of the changes to the DB and close the ISession
					//transaction.Commit();

					//session.Flush();

					//Fire event
					et.TriggerChange();
			
				}												
				//session.Flush();								
				//session.Close();
				return alPropertyItemIds;
			}
			catch (Exception exc)
			{
				MessageBox.Show(exc.ToString());
				return null;
			}			
		}
		#endregion � MapToPropertyItem �	
		#region � AssignToNode �
		private void AssignToNode(ArrayList alPropertyItemIds, int nodeId)
		{
			try
			{
				foreach (Guid id in alPropertyItemIds)
				{
					SqlCommand sp_InsertPropertyItemInNode = new SqlCommand("InsertPropertyItemInNode", Connection.getInstance().HatchConn);

					sp_InsertPropertyItemInNode.CommandType = CommandType.StoredProcedure;
					
					sp_InsertPropertyItemInNode.Parameters.Add("@NodeId", nodeId);
					sp_InsertPropertyItemInNode.Parameters.Add("@IdPropertyItem", id);				
																	
					if (ConnectionState.Open != Connection.getInstance().HatchConn.State)
						Connection.getInstance().HatchConn.Open();

					sp_InsertPropertyItemInNode.ExecuteNonQuery();
				}
				
			}
			catch( Exception ex)
			{
				MessageBox.Show(ex.ToString());
				return; 
			}
		}
		#endregion � AssignToNode �
		#region � ParseCSVFields �
		// Parses the CSV fields and pushes the fields into the result arraylist
		private void ParseCSVFields(ArrayList result, string data, Hashtable htDelimiterPositions) 
		{

			int pos = -1;
			int columnNr = 0;
			while (pos < data.Length)
			{
				result.Add(ParseCSVField(data, ref pos));
				htDelimiterPositions.Add(columnNr, pos);
				columnNr++;				
			}
		}
		#endregion � ParseCSVFields �
		#region � ParseCSVField �
		// Parses the field at the given position of the data, modified pos to match
		// the first unparsed position and returns the parsed field
		private string ParseCSVField(string data, ref int startSeparatorPosition) 
		{

			if (startSeparatorPosition == data.Length-1) 
			{
				startSeparatorPosition++;
				// The last field is empty
				return "";
			}

			int fromPos = startSeparatorPosition + 1;

			// Determine if this is a quoted field
			if (data[fromPos] == '"') 
			{
				// If we're at the end of the string, let's consider this a field that
				// only contains the quote
				if (fromPos == data.Length-1) 
				{
					fromPos++;
					return "\"";
				}

				// Otherwise, return a string of appropriate length with double quotes collapsed
				// Note that FSQ returns data.Length if no single quote was found
				int nextSingleQuote = FindSingleQuote(data, fromPos+1);
				startSeparatorPosition = nextSingleQuote+1;				
				string retVal = data.Substring(fromPos+1, nextSingleQuote-fromPos-1).Replace("\"\"", "\"");
				return retVal;
			}

			// The field ends in the next comma or EOL
			int nextComma = data.IndexOf(',', fromPos);
			if (nextComma == -1) 
			{
				startSeparatorPosition = data.Length;
				string retVal = data.Substring(fromPos);
				return retVal;
			}
			else 
			{
				startSeparatorPosition = nextComma;				
				string retVal = data.Substring(fromPos, nextComma-fromPos);
				return retVal;
			}
		}
		#endregion � ParseCSVField �
		#region � FindSingleQuote �
		// Returns the index of the next single quote mark in the string 
		// (starting from startFrom)
		private int FindSingleQuote(string data, int startFrom) 
		{

			int i = startFrom-1;
			while (++i < data.Length)
				if (data[i] == '"') 
				{
					// If this is a double quote, bypass the chars
					if (i < data.Length-1 && data[i+1] == '"') 
					{
						i++;
						continue;
					}
					else
						return i;
				}
			// If no quote found, return the end value of i (data.Length)
			return i;
		}
		#endregion � FindSingleQuote �
		#region � tv_Nodes_KeyDown �
		private void tv_Nodes_KeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
		{
			if (null != ((TreeView)sender).SelectedNode)
			{
				this.selectedNode = ((TreeView)sender).SelectedNode;
			}
		}
		#endregion � tv_Nodes_KeyDown �
		#region � tv_Nodes_MouseDown �
		private void tv_Nodes_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			if (null != ((TreeView)sender).SelectedNode)
			{
				this.selectedNode = ((TreeView)sender).SelectedNode;
			}
		}
		#endregion � tv_Nodes_MouseDown �
		#region � tv_Nodes_AfterSelect �
		private void tv_Nodes_AfterSelect(object sender, System.Windows.Forms.TreeViewEventArgs e)
		{
			if (null != ((TreeView)sender).SelectedNode)
			{
				this.selectedNode = ((TreeView)sender).SelectedNode;
				Node n = (Node)this.selectedNode.Tag;
				this.GetPropertyCountInNode(n.nodeId);
			}
		}
		#endregion � tv_Nodes_AfterSelect �
		#region � GetPropertyCountInNode �
		private void GetPropertyCountInNode (int nodeId)
		{
			SqlDataReader nodeReader = null;
			try
			{
				SqlCommand sp_GetPropertyCount = new SqlCommand("GetPropertyCount", Connection.getInstance().HatchConn);
				sp_GetPropertyCount.CommandType = CommandType.StoredProcedure;								
				sp_GetPropertyCount.Parameters.Add("@NodeId", nodeId);
									
				if (ConnectionState.Open != Connection.getInstance().HatchConn.State)
					Connection.getInstance().HatchConn.Open();

				sp_GetPropertyCount.ExecuteNonQuery();

				nodeReader = sp_GetPropertyCount.ExecuteReader();

				while (nodeReader.Read())
				{
					Int32	count				= nodeReader.GetInt32(0);
					this.tbExistingEntries.Text = count.ToString();
				}
			}
			catch	(Exception exc)
			{
				MessageBox.Show(exc.Message);
			}
			finally
			{
				nodeReader.Close();
			}
		}
		#endregion � GetPropertyCountInNode �
		#region � ImportMask6_Closing �
		private void ImportMask6_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (true == e.Cancel)
			{
				if (DialogResult.Yes == MessageBox.Show(this, UserMessages.CancelImportText, UserMessages.CancelImport, MessageBoxButtons.YesNo, MessageBoxIcon.Question))
				{
					Globals.importWizardStarted = false;
					//this.Close();
				}
			}
		}
		#endregion � ImportMask6_Closing �

		private void bSaveMap_Click(object sender, System.EventArgs e)
		{
			ImportMask5 im5 = new ImportMask5();
			im5.ShowDialog();
		}
		#endregion � Methods �

        void On_Lender_Click(object sender, EventArgs e)
        {
            _selectedContactType = ContactTypes.Lender;

            if (null != this.customizedTreeViewUserControl1.LenderNode)
                this.customizedTreeViewUserControl1.SelectedTreeNode = this.customizedTreeViewUserControl1.LenderNode; 
        }

        void On_Buyer_Click(object sender, EventArgs e)
        {
            _selectedContactType = ContactTypes.Buyer;

            if (null != this.customizedTreeViewUserControl1.BuyerNode)
                this.customizedTreeViewUserControl1.SelectedTreeNode = this.customizedTreeViewUserControl1.BuyerNode; 
        }

        void On_Seller_Click(object sender, EventArgs e)
        {
            _selectedContactType = ContactTypes.Seller;

            if (null != this.customizedTreeViewUserControl1.RootSellerNode)
                this.customizedTreeViewUserControl1.SelectedTreeNode = this.customizedTreeViewUserControl1.RootSellerNode; 
        }

        bool AllowImport(ContactTypes selectedContactType, int nodeId)
        {
            return ContactTypesManager.Instance.AllowImport(selectedContactType, nodeId);
        }        		
	}
	#endregion � Class ImportMask6 �
}
#endregion � Namespace DealMaker �