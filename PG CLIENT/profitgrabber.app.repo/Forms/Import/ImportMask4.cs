#region � Using �
using System;
using System.Drawing;
using System.Reflection;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
#endregion � Using �
#region � Namespace DealMaker �
namespace DealMaker
{
	#region � Class ImportMask4 �
	/// <summary>
	/// ImportMask4 Class implements features defined in preRequisites
	/// </summary>
	public class ImportMask4 : System.Windows.Forms.UserControl
	{
		#region � Data �
		bool _trimZIP = true;
		private ArrayList alZIPs = null;
		private string selectedState = string.Empty;
		private string selectedProfile = string.Empty;
		private Delimiter inputDelimiter;
		private string selectedFile;
		private bool bChecked;
		private bool bCreateNewProfile = false;
        bool separateFirstName = false;
        bool lastNameFirst = false;

		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.ComboBox cbDataColumn1;
		private System.Windows.Forms.ComboBox cbDataColumn2;
		private System.Windows.Forms.ComboBox cbDataColumn3;
		private System.Windows.Forms.ComboBox cbDataColumn4;
		private System.Windows.Forms.ComboBox cbCondition1;
		private System.Windows.Forms.ComboBox cbCondition2;
		private System.Windows.Forms.ComboBox cbCondition3;
		private System.Windows.Forms.ComboBox cbCondition4;
		private System.Windows.Forms.TextBox tbValue1;
		private System.Windows.Forms.TextBox tbValue2;
		private System.Windows.Forms.TextBox tbValue3;
		private System.Windows.Forms.TextBox tbValue4;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.ComboBox cbDataColumn5;
		private System.Windows.Forms.ComboBox cbCondition5;
		private System.Windows.Forms.ComboBox cbDataColumn6;
		private System.Windows.Forms.ComboBox cbCondition6;
		private System.Windows.Forms.TextBox tbValue5;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.Label label13;
		private System.Windows.Forms.Label label14;
		private System.Windows.Forms.Label label15;
		private System.Windows.Forms.Button bNext;
		private System.Windows.Forms.Button bPrevious;
		private System.Windows.Forms.Button bCancel;
		private DealMaker.ReadMe rm;
		private System.Windows.Forms.Button bReset;
		private System.Windows.Forms.Label label16;
		private System.Windows.Forms.Button bZIPFilter;
		private System.Windows.Forms.TextBox tbZips;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.ComboBox cbAdvZIP;
		private System.Windows.Forms.TextBox tbDateFilterValue1;
		private System.Windows.Forms.ComboBox cbDateFilter1Cond;
		private System.Windows.Forms.ComboBox cbDateFilterLeftSide1;
		private System.Windows.Forms.TextBox tbDateFilterValue2;
		private System.Windows.Forms.ComboBox cbDateFilter2Cond;
		private System.Windows.Forms.ComboBox cbDateFilterLeftSide2;
		private System.Windows.Forms.ComboBox cbZIPCondition;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
		#endregion � Data �
		#region � Properties �
		#region � InputDelimiter �
		public Delimiter InputDelimiter
		{
			get {return this.inputDelimiter;}
			set {this.inputDelimiter = value;}
		}
		#endregion � InputDelimiter �
		#region � SelectedFile �
		public string SelectedFile
		{
			get {return this.selectedFile;}
			set {this.selectedFile = value;}
		}
		#endregion � SelectedFile �
		#region � Checked �
		public bool Checked
		{
			get {return this.bChecked;}
			set {this.bChecked = value;}
		}
		#endregion � Checked �
		#region � CreateNewProfile �
		public bool CreateNewProfile
		{
			get {return this.bCreateNewProfile;}
			set {this.bCreateNewProfile = value;}
		}
		#endregion � CreateNewProfile �
		#region � SelectedProfile �
		public string SelectedProfile
		{
			get {return this.selectedProfile; }
			set {this.selectedProfile = value;}
		}		
		#endregion � SelectedProfile �
		public string SelectedState
		{
			get {return this.selectedState;}
			set {this.selectedState = value; }
		}

		public bool TrimZIP
		{
			get {return _trimZIP;}
			set {_trimZIP = value;}
		}

		public ArrayList AllowedZIPs
		{
			get {return this.alZIPs;}
			set {this.alZIPs = value;}
		}
        public bool SeparateFirstName
        {
            get { return separateFirstName; }
            set { separateFirstName = value; }
        }

        public bool LastNameFirst
        {
            get { return lastNameFirst; }
            set { lastNameFirst = value; }
        }
		#endregion � Properties �
		#region � CTR && Dispose �
		public ImportMask4()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			this.cbCondition1.Items.AddRange(
				new object[] {Globals.IsEqual, Globals.DoesNotEqual, Globals.IsGreaterThan, Globals.IsGreaterThanOrEqualTo,
								 Globals.IsLessThan,  Globals.IsLessThanOrEqualTo});

			this.cbCondition2.Items.AddRange(
				new object[] {Globals.IsEqual, Globals.DoesNotEqual, Globals.IsGreaterThan, Globals.IsGreaterThanOrEqualTo,
							  Globals.IsLessThan,  Globals.IsLessThanOrEqualTo});

			this.cbCondition3.Items.AddRange(
				new object[] {Globals.IsEqual, Globals.DoesNotEqual, Globals.IsGreaterThan, Globals.IsGreaterThanOrEqualTo,
								 Globals.IsLessThan,  Globals.IsLessThanOrEqualTo});

			this.cbCondition4.Items.AddRange(
				new object[] {Globals.IsEqual, Globals.DoesNotEqual, Globals.IsGreaterThan, Globals.IsGreaterThanOrEqualTo,
								 Globals.IsLessThan,  Globals.IsLessThanOrEqualTo});

			this.cbCondition6.Items.AddRange(
				new object[] {Globals.IsEqual, Globals.DoesNotEqual, Globals.IsGreaterThan, Globals.IsGreaterThanOrEqualTo,
								 Globals.IsLessThan,  Globals.IsLessThanOrEqualTo});

			this.cbCondition5.Items.AddRange(
				new object[] {Globals.Plus, Globals.Minus});

			this.cbDateFilter1Cond.Items.AddRange(
				new object[] {Globals.IsEqual, Globals.DoesNotEqual, Globals.IsGreaterThan, Globals.IsGreaterThanOrEqualTo,
								 Globals.IsLessThan,  Globals.IsLessThanOrEqualTo});

			this.cbDateFilter2Cond.Items.AddRange(
				new object[] {Globals.IsEqual, Globals.DoesNotEqual, Globals.IsGreaterThan, Globals.IsGreaterThanOrEqualTo,
								 Globals.IsLessThan,  Globals.IsLessThanOrEqualTo});

			this.tbDateFilterValue1.MouseDown += new MouseEventHandler(tbDateFilterValue_MouseDown1);
			this.tbDateFilterValue2.MouseDown += new MouseEventHandler(tbDateFilterValue_MouseDown2);
			this.tbDateFilterValue1.KeyDown += new KeyEventHandler(tbDateFilterValue_KeyDown1);
			this.tbDateFilterValue2.KeyDown += new KeyEventHandler(tbDateFilterValue_KeyDown2);
			

			this.Location = Globals.importWindowPos;

			for (int i = 0; i < Globals.zipFilter.Count; i++)
			{
				if (0 != i)
				{
					this.tbZips.Text += ", ";
				}
				this.tbZips.Text += (string)Globals.zipFilter[i];
			}				
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}
		#endregion � CTR && Dispose �
		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(ImportMask4));
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.label7 = new System.Windows.Forms.Label();
			this.cbDataColumn1 = new System.Windows.Forms.ComboBox();
			this.cbDataColumn2 = new System.Windows.Forms.ComboBox();
			this.cbDataColumn3 = new System.Windows.Forms.ComboBox();
			this.cbDataColumn4 = new System.Windows.Forms.ComboBox();
			this.cbCondition1 = new System.Windows.Forms.ComboBox();
			this.cbCondition2 = new System.Windows.Forms.ComboBox();
			this.cbCondition3 = new System.Windows.Forms.ComboBox();
			this.cbCondition4 = new System.Windows.Forms.ComboBox();
			this.tbValue1 = new System.Windows.Forms.TextBox();
			this.tbValue2 = new System.Windows.Forms.TextBox();
			this.tbValue3 = new System.Windows.Forms.TextBox();
			this.tbValue4 = new System.Windows.Forms.TextBox();
			this.label8 = new System.Windows.Forms.Label();
			this.label9 = new System.Windows.Forms.Label();
			this.label10 = new System.Windows.Forms.Label();
			this.cbDataColumn5 = new System.Windows.Forms.ComboBox();
			this.cbCondition5 = new System.Windows.Forms.ComboBox();
			this.cbDataColumn6 = new System.Windows.Forms.ComboBox();
			this.cbCondition6 = new System.Windows.Forms.ComboBox();
			this.tbValue5 = new System.Windows.Forms.TextBox();
			this.label11 = new System.Windows.Forms.Label();
			this.label12 = new System.Windows.Forms.Label();
			this.label13 = new System.Windows.Forms.Label();
			this.label14 = new System.Windows.Forms.Label();
			this.label15 = new System.Windows.Forms.Label();
			this.bNext = new System.Windows.Forms.Button();
			this.bPrevious = new System.Windows.Forms.Button();
			this.bCancel = new System.Windows.Forms.Button();
			this.rm = new DealMaker.ReadMe();
			this.bReset = new System.Windows.Forms.Button();
			this.label16 = new System.Windows.Forms.Label();
			this.bZIPFilter = new System.Windows.Forms.Button();
			this.tbZips = new System.Windows.Forms.TextBox();
			this.tbDateFilterValue1 = new System.Windows.Forms.TextBox();
			this.cbDateFilter1Cond = new System.Windows.Forms.ComboBox();
			this.cbDateFilterLeftSide1 = new System.Windows.Forms.ComboBox();
			this.tbDateFilterValue2 = new System.Windows.Forms.TextBox();
			this.cbDateFilter2Cond = new System.Windows.Forms.ComboBox();
			this.cbDateFilterLeftSide2 = new System.Windows.Forms.ComboBox();
			this.cbAdvZIP = new System.Windows.Forms.ComboBox();
			this.cbZIPCondition = new System.Windows.Forms.ComboBox();
			this.label3 = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label1.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.label1.Location = new System.Drawing.Point(16, 8);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(120, 23);
			this.label1.TabIndex = 0;
			this.label1.Text = "Create Import Filter:";
			// 
			// label2
			// 
			this.label2.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.label2.Location = new System.Drawing.Point(144, 8);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(408, 23);
			this.label2.TabIndex = 1;
			this.label2.Text = "Skip this step if you want to import all the records from your import file!";
			// 
			// label4
			// 
			this.label4.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.label4.Location = new System.Drawing.Point(16, 26);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(552, 32);
			this.label4.TabIndex = 3;
			this.label4.Text = "Simple Filter: Choose the data from the drop down menu that you want the conditio" +
				"n to apply to, then choose the condition and its value.";
			// 
			// label5
			// 
			this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label5.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.label5.Location = new System.Drawing.Point(32, 62);
			this.label5.Name = "label5";
			this.label5.TabIndex = 4;
			this.label5.Text = "Data Column";
			// 
			// label6
			// 
			this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label6.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.label6.Location = new System.Drawing.Point(152, 62);
			this.label6.Name = "label6";
			this.label6.TabIndex = 5;
			this.label6.Text = "Condition";
			// 
			// label7
			// 
			this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label7.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.label7.Location = new System.Drawing.Point(288, 62);
			this.label7.Name = "label7";
			this.label7.TabIndex = 6;
			this.label7.Text = "Value";
			// 
			// cbDataColumn1
			// 
			this.cbDataColumn1.Location = new System.Drawing.Point(32, 78);
			this.cbDataColumn1.Name = "cbDataColumn1";
			this.cbDataColumn1.Size = new System.Drawing.Size(120, 21);
			this.cbDataColumn1.TabIndex = 7;
			this.cbDataColumn1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.comboBox_KeyPressed);
			// 
			// cbDataColumn2
			// 
			this.cbDataColumn2.Location = new System.Drawing.Point(32, 102);
			this.cbDataColumn2.Name = "cbDataColumn2";
			this.cbDataColumn2.Size = new System.Drawing.Size(120, 21);
			this.cbDataColumn2.TabIndex = 8;
			this.cbDataColumn2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.comboBox_KeyPressed);
			// 
			// cbDataColumn3
			// 
			this.cbDataColumn3.Location = new System.Drawing.Point(32, 126);
			this.cbDataColumn3.Name = "cbDataColumn3";
			this.cbDataColumn3.Size = new System.Drawing.Size(120, 21);
			this.cbDataColumn3.TabIndex = 9;
			this.cbDataColumn3.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.comboBox_KeyPressed);
			// 
			// cbDataColumn4
			// 
			this.cbDataColumn4.Location = new System.Drawing.Point(32, 150);
			this.cbDataColumn4.Name = "cbDataColumn4";
			this.cbDataColumn4.Size = new System.Drawing.Size(120, 21);
			this.cbDataColumn4.TabIndex = 10;
			this.cbDataColumn4.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.comboBox_KeyPressed);
			// 
			// cbCondition1
			// 
			this.cbCondition1.Location = new System.Drawing.Point(152, 78);
			this.cbCondition1.Name = "cbCondition1";
			this.cbCondition1.Size = new System.Drawing.Size(136, 21);
			this.cbCondition1.TabIndex = 11;
			this.cbCondition1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.comboBox_KeyPressed);
			// 
			// cbCondition2
			// 
			this.cbCondition2.Location = new System.Drawing.Point(152, 102);
			this.cbCondition2.Name = "cbCondition2";
			this.cbCondition2.Size = new System.Drawing.Size(136, 21);
			this.cbCondition2.TabIndex = 12;
			this.cbCondition2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.comboBox_KeyPressed);
			// 
			// cbCondition3
			// 
			this.cbCondition3.Location = new System.Drawing.Point(152, 126);
			this.cbCondition3.Name = "cbCondition3";
			this.cbCondition3.Size = new System.Drawing.Size(136, 21);
			this.cbCondition3.TabIndex = 13;
			this.cbCondition3.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.comboBox_KeyPressed);
			// 
			// cbCondition4
			// 
			this.cbCondition4.Location = new System.Drawing.Point(152, 150);
			this.cbCondition4.Name = "cbCondition4";
			this.cbCondition4.Size = new System.Drawing.Size(136, 21);
			this.cbCondition4.TabIndex = 14;
			this.cbCondition4.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.comboBox_KeyPressed);
			// 
			// tbValue1
			// 
			this.tbValue1.Location = new System.Drawing.Point(288, 78);
			this.tbValue1.Name = "tbValue1";
			this.tbValue1.Size = new System.Drawing.Size(118, 20);
			this.tbValue1.TabIndex = 15;
			this.tbValue1.Text = "";
			// 
			// tbValue2
			// 
			this.tbValue2.Location = new System.Drawing.Point(288, 102);
			this.tbValue2.Name = "tbValue2";
			this.tbValue2.Size = new System.Drawing.Size(118, 20);
			this.tbValue2.TabIndex = 16;
			this.tbValue2.Text = "";
			// 
			// tbValue3
			// 
			this.tbValue3.Location = new System.Drawing.Point(288, 126);
			this.tbValue3.Name = "tbValue3";
			this.tbValue3.Size = new System.Drawing.Size(118, 20);
			this.tbValue3.TabIndex = 17;
			this.tbValue3.Text = "";
			// 
			// tbValue4
			// 
			this.tbValue4.Location = new System.Drawing.Point(288, 150);
			this.tbValue4.Name = "tbValue4";
			this.tbValue4.Size = new System.Drawing.Size(118, 20);
			this.tbValue4.TabIndex = 18;
			this.tbValue4.Text = "";
			// 
			// label8
			// 
			this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label8.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.label8.Location = new System.Drawing.Point(16, 298);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(112, 23);
			this.label8.TabIndex = 19;
			this.label8.Text = "Mathematical Filter:";
			// 
			// label9
			// 
			this.label9.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.label9.Location = new System.Drawing.Point(112, 298);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(456, 32);
			this.label9.TabIndex = 20;
			this.label9.Text = "Mathematical Filter allows you to apply \"add\" or \"substract\" functions to 2 colum" +
				"ns of your choice, and then put a condition on the result of the applied mathema" +
				"tical function.";
			// 
			// label10
			// 
			this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label10.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.label10.Location = new System.Drawing.Point(32, 328);
			this.label10.Name = "label10";
			this.label10.TabIndex = 21;
			this.label10.Text = "Data Column # 1";
			// 
			// cbDataColumn5
			// 
			this.cbDataColumn5.Location = new System.Drawing.Point(32, 341);
			this.cbDataColumn5.Name = "cbDataColumn5";
			this.cbDataColumn5.Size = new System.Drawing.Size(120, 21);
			this.cbDataColumn5.TabIndex = 22;
			this.cbDataColumn5.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.comboBox_KeyPressed);
			// 
			// cbCondition5
			// 
			this.cbCondition5.Location = new System.Drawing.Point(152, 341);
			this.cbCondition5.Name = "cbCondition5";
			this.cbCondition5.Size = new System.Drawing.Size(96, 21);
			this.cbCondition5.TabIndex = 23;
			this.cbCondition5.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.comboBox_KeyPressed);
			// 
			// cbDataColumn6
			// 
			this.cbDataColumn6.Location = new System.Drawing.Point(248, 341);
			this.cbDataColumn6.Name = "cbDataColumn6";
			this.cbDataColumn6.Size = new System.Drawing.Size(104, 21);
			this.cbDataColumn6.TabIndex = 24;
			this.cbDataColumn6.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.comboBox_KeyPressed);
			// 
			// cbCondition6
			// 
			this.cbCondition6.Location = new System.Drawing.Point(392, 341);
			this.cbCondition6.Name = "cbCondition6";
			this.cbCondition6.Size = new System.Drawing.Size(96, 21);
			this.cbCondition6.TabIndex = 25;
			this.cbCondition6.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.comboBox_KeyPressed);
			// 
			// tbValue5
			// 
			this.tbValue5.Location = new System.Drawing.Point(488, 341);
			this.tbValue5.Name = "tbValue5";
			this.tbValue5.Size = new System.Drawing.Size(80, 20);
			this.tbValue5.TabIndex = 26;
			this.tbValue5.Text = "";
			// 
			// label11
			// 
			this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label11.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.label11.Location = new System.Drawing.Point(248, 328);
			this.label11.Name = "label11";
			this.label11.TabIndex = 27;
			this.label11.Text = "Data Column # 2";
			// 
			// label12
			// 
			this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label12.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.label12.Location = new System.Drawing.Point(392, 328);
			this.label12.Name = "label12";
			this.label12.TabIndex = 28;
			this.label12.Text = "Condition";
			// 
			// label13
			// 
			this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label13.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.label13.Location = new System.Drawing.Point(488, 328);
			this.label13.Name = "label13";
			this.label13.Size = new System.Drawing.Size(72, 23);
			this.label13.TabIndex = 29;
			this.label13.Text = "Value";
			// 
			// label14
			// 
			this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label14.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.label14.Location = new System.Drawing.Point(352, 341);
			this.label14.Name = "label14";
			this.label14.Size = new System.Drawing.Size(40, 23);
			this.label14.TabIndex = 30;
			this.label14.Text = "Result";
			this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label15
			// 
			this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label15.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.label15.Location = new System.Drawing.Point(16, 368);
			this.label15.Name = "label15";
			this.label15.Size = new System.Drawing.Size(520, 23);
			this.label15.TabIndex = 31;
			this.label15.Text = ">> ATTENTION: only data that meet ALL the above conditions will be imported!";
			// 
			// bNext
			// 
			this.bNext.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.bNext.Location = new System.Drawing.Point(536, 128);
			this.bNext.Name = "bNext";
			this.bNext.Size = new System.Drawing.Size(24, 16);
			this.bNext.TabIndex = 32;
			this.bNext.Text = "Next";
			this.bNext.Visible = false;
			this.bNext.Click += new System.EventHandler(this.bNext_Click);
			// 
			// bPrevious
			// 
			this.bPrevious.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.bPrevious.Location = new System.Drawing.Point(536, 104);
			this.bPrevious.Name = "bPrevious";
			this.bPrevious.Size = new System.Drawing.Size(24, 16);
			this.bPrevious.TabIndex = 33;
			this.bPrevious.Text = "Previous";
			this.bPrevious.Visible = false;
			this.bPrevious.Click += new System.EventHandler(this.bPrevious_Click);
			// 
			// bCancel
			// 
			this.bCancel.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.bCancel.Location = new System.Drawing.Point(536, 152);
			this.bCancel.Name = "bCancel";
			this.bCancel.Size = new System.Drawing.Size(24, 16);
			this.bCancel.TabIndex = 38;
			this.bCancel.Text = "Cancel";
			this.bCancel.Visible = false;
			this.bCancel.Click += new System.EventHandler(this.bCancel_Click);
			// 
			// rm
			// 
			this.rm.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(210)), ((System.Byte)(216)), ((System.Byte)(240)));
			this.rm.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("rm.BackgroundImage")));
			this.rm.Location = new System.Drawing.Point(560, 0);
			this.rm.Name = "rm";
			this.rm.Size = new System.Drawing.Size(16, 16);
			this.rm.TabIndex = 39;
			// 
			// bReset
			// 
			this.bReset.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.bReset.Location = new System.Drawing.Point(536, 80);
			this.bReset.Name = "bReset";
			this.bReset.Size = new System.Drawing.Size(24, 16);
			this.bReset.TabIndex = 40;
			this.bReset.Text = "Reset";
			this.bReset.Visible = false;
			this.bReset.Click += new System.EventHandler(this.bReset_Click);
			// 
			// label16
			// 
			this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label16.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.label16.Location = new System.Drawing.Point(16, 254);
			this.label16.Name = "label16";
			this.label16.Size = new System.Drawing.Size(552, 23);
			this.label16.TabIndex = 41;
			this.label16.Text = "Advanced ZIP Filter:  Enter any number of ZIP codes, separated by comma (i.e. 850" +
				"44, 85008, 92005).";
			// 
			// bZIPFilter
			// 
			this.bZIPFilter.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.bZIPFilter.Location = new System.Drawing.Point(528, 368);
			this.bZIPFilter.Name = "bZIPFilter";
			this.bZIPFilter.Size = new System.Drawing.Size(40, 23);
			this.bZIPFilter.TabIndex = 42;
			this.bZIPFilter.Text = "...";
			this.bZIPFilter.Visible = false;
			this.bZIPFilter.Click += new System.EventHandler(this.bZIPFilter_Click);
			// 
			// tbZips
			// 
			this.tbZips.Location = new System.Drawing.Point(256, 270);
			this.tbZips.Name = "tbZips";
			this.tbZips.Size = new System.Drawing.Size(312, 20);
			this.tbZips.TabIndex = 43;
			this.tbZips.Text = "";
			// 
			// tbDateFilterValue1
			// 
			this.tbDateFilterValue1.Location = new System.Drawing.Point(288, 197);
			this.tbDateFilterValue1.Name = "tbDateFilterValue1";
			this.tbDateFilterValue1.Size = new System.Drawing.Size(118, 20);
			this.tbDateFilterValue1.TabIndex = 46;
			this.tbDateFilterValue1.Text = "";
			// 
			// cbDateFilter1Cond
			// 
			this.cbDateFilter1Cond.Location = new System.Drawing.Point(152, 197);
			this.cbDateFilter1Cond.Name = "cbDateFilter1Cond";
			this.cbDateFilter1Cond.Size = new System.Drawing.Size(136, 21);
			this.cbDateFilter1Cond.TabIndex = 45;
			// 
			// cbDateFilterLeftSide1
			// 
			this.cbDateFilterLeftSide1.Location = new System.Drawing.Point(32, 197);
			this.cbDateFilterLeftSide1.Name = "cbDateFilterLeftSide1";
			this.cbDateFilterLeftSide1.Size = new System.Drawing.Size(120, 21);
			this.cbDateFilterLeftSide1.TabIndex = 44;
			// 
			// tbDateFilterValue2
			// 
			this.tbDateFilterValue2.Location = new System.Drawing.Point(288, 221);
			this.tbDateFilterValue2.Name = "tbDateFilterValue2";
			this.tbDateFilterValue2.Size = new System.Drawing.Size(118, 20);
			this.tbDateFilterValue2.TabIndex = 49;
			this.tbDateFilterValue2.Text = "";
			// 
			// cbDateFilter2Cond
			// 
			this.cbDateFilter2Cond.Location = new System.Drawing.Point(152, 221);
			this.cbDateFilter2Cond.Name = "cbDateFilter2Cond";
			this.cbDateFilter2Cond.Size = new System.Drawing.Size(136, 21);
			this.cbDateFilter2Cond.TabIndex = 48;
			// 
			// cbDateFilterLeftSide2
			// 
			this.cbDateFilterLeftSide2.Location = new System.Drawing.Point(32, 221);
			this.cbDateFilterLeftSide2.Name = "cbDateFilterLeftSide2";
			this.cbDateFilterLeftSide2.Size = new System.Drawing.Size(120, 21);
			this.cbDateFilterLeftSide2.TabIndex = 47;
			// 
			// cbAdvZIP
			// 
			this.cbAdvZIP.Location = new System.Drawing.Point(32, 270);
			this.cbAdvZIP.Name = "cbAdvZIP";
			this.cbAdvZIP.Size = new System.Drawing.Size(120, 21);
			this.cbAdvZIP.TabIndex = 50;
			// 
			// cbZIPCondition
			// 
			this.cbZIPCondition.Items.AddRange(new object[] {
																"Equals",
																"Does Not Equal"});
			this.cbZIPCondition.Location = new System.Drawing.Point(152, 270);
			this.cbZIPCondition.Name = "cbZIPCondition";
			this.cbZIPCondition.Size = new System.Drawing.Size(104, 21);
			this.cbZIPCondition.TabIndex = 51;
			// 
			// label3
			// 
			this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label3.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.label3.Location = new System.Drawing.Point(16, 181);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(112, 23);
			this.label3.TabIndex = 52;
			this.label3.Text = "Date Filter:";
			// 
			// ImportMask4
			// 
			this.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(210)), ((System.Byte)(216)), ((System.Byte)(240)));
			this.Controls.Add(this.label14);
			this.Controls.Add(this.tbValue5);
			this.Controls.Add(this.cbCondition6);
			this.Controls.Add(this.cbDataColumn6);
			this.Controls.Add(this.cbCondition5);
			this.Controls.Add(this.cbDataColumn5);
			this.Controls.Add(this.cbDataColumn4);
			this.Controls.Add(this.cbDateFilterLeftSide1);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.cbZIPCondition);
			this.Controls.Add(this.cbAdvZIP);
			this.Controls.Add(this.tbDateFilterValue2);
			this.Controls.Add(this.cbDateFilter2Cond);
			this.Controls.Add(this.cbDateFilterLeftSide2);
			this.Controls.Add(this.tbDateFilterValue1);
			this.Controls.Add(this.cbDateFilter1Cond);
			this.Controls.Add(this.tbValue1);
			this.Controls.Add(this.cbCondition1);
			this.Controls.Add(this.cbDataColumn1);
			this.Controls.Add(this.label15);
			this.Controls.Add(this.tbZips);
			this.Controls.Add(this.bZIPFilter);
			this.Controls.Add(this.label16);
			this.Controls.Add(this.label9);
			this.Controls.Add(this.label8);
			this.Controls.Add(this.bReset);
			this.Controls.Add(this.rm);
			this.Controls.Add(this.bCancel);
			this.Controls.Add(this.bPrevious);
			this.Controls.Add(this.bNext);
			this.Controls.Add(this.label13);
			this.Controls.Add(this.label12);
			this.Controls.Add(this.label11);
			this.Controls.Add(this.tbValue4);
			this.Controls.Add(this.tbValue3);
			this.Controls.Add(this.tbValue2);
			this.Controls.Add(this.label10);
			this.Controls.Add(this.cbCondition4);
			this.Controls.Add(this.cbCondition3);
			this.Controls.Add(this.cbCondition2);
			this.Controls.Add(this.cbDataColumn3);
			this.Controls.Add(this.cbDataColumn2);
			this.Controls.Add(this.label7);
			this.Controls.Add(this.label6);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Name = "ImportMask4";
			this.Size = new System.Drawing.Size(578, 385);
			this.Load += new System.EventHandler(this.ImportMask4_Load);
			this.ResumeLayout(false);

		}
		#endregion
		#region � Methods �
		#region � bPrevious_Click �
		private void bPrevious_Click(object sender, System.EventArgs e)
		{
			Globals.importWindowPos = this.Location;
			//this.Close();
			ImportMask3 im3 = new ImportMask3();
			im3.SelectedFile = this.SelectedFile;
			im3.Checked = this.Checked;
			im3.CreateNewProfile = this.CreateNewProfile;
			im3.SelectedProfile = this.SelectedProfile;
			im3.InputDelimiter = this.InputDelimiter;
			im3.UseMappingTable = true;
			im3.Show();
		}
		#endregion � bPrevious_Click �
		#region � ImportMask4_Load �
		private void ImportMask4_Load(object sender, System.EventArgs e)
		{
			this.PopulateDataColumns();
		}
		#endregion � ImportMask4_Load �
		#region � PopulateDataColumns �
		private void PopulateDataColumns()
		{
			ImportIntermedeateObject iio = new ImportIntermedeateObject();
			
			Type iioType = iio.GetType();			
			PropertyInfo[] pi =  iioType.GetProperties();			
			IDictionaryEnumerator ide = Globals.htMappingInfo.GetEnumerator();			


			for (int i = 0; i < pi.Length; i++)
			{
				Type t = pi[i].PropertyType;
				object[] attr = pi[i].GetCustomAttributes(typeof(DescriptionAttribute), true);
				if (0 == attr.Length)
					return;
				DescriptionAttribute da = (DescriptionAttribute)attr[0];
				
					if ((t == typeof(System.Int32)) ||  (t == typeof(System.Double)))
					{	
						ide = Globals.htMappingInfo.GetEnumerator();
						//if (0 != attr.Length)
						{      
							//DescriptionAttribute da = (DescriptionAttribute)attr[0];
							try
							{
								while (ide.MoveNext())
								{								
									MapperObjectInfo moi = (MapperObjectInfo)ide.Key;
									MapperSourceInfo msi = (MapperSourceInfo)ide.Value;
									if (da.Desc == moi.PropertyDesc && -1 != msi.ColumnIndex)
									{
										this.cbDataColumn1.Items.Add(da.Desc);
										this.cbDataColumn2.Items.Add(da.Desc);
										this.cbDataColumn3.Items.Add(da.Desc);
										this.cbDataColumn4.Items.Add(da.Desc);
										this.cbDataColumn5.Items.Add(da.Desc);
										this.cbDataColumn6.Items.Add(da.Desc);
									}
								}
							}
							catch (Exception
#if DEBUG
								e
#endif
								)
							{
#if DEBUG
								MessageBox.Show(e.ToString());
#endif
							}
						}						
					}
			

				//Added filter
				ide = Globals.htMappingInfo.GetEnumerator();
				if (t == typeof(System.DateTime))
				{
					while (ide.MoveNext())
					{
						MapperObjectInfo moi = (MapperObjectInfo)ide.Key;
						MapperSourceInfo msi = (MapperSourceInfo)ide.Value;
						if (da.Desc == moi.PropertyDesc && -1 != msi.ColumnIndex)
						{
							this.cbDateFilterLeftSide1.Items.Add(da.Desc);
							this.cbDateFilterLeftSide2.Items.Add(da.Desc);
						}		
					}
				}

				if (t == typeof(string))
				{
					ide = Globals.htMappingInfo.GetEnumerator();
					while (ide.MoveNext())
					{
						MapperObjectInfo moi = (MapperObjectInfo)ide.Key;
						MapperSourceInfo msi = (MapperSourceInfo)ide.Value;
						if (da.Desc == moi.PropertyDesc && -1 != msi.ColumnIndex)
						{
							if (da.Desc == "Deed Date" || da.Desc == "Loan Recording Date")
							{
								this.cbDateFilterLeftSide1.Items.Add(da.Desc);
								this.cbDateFilterLeftSide2.Items.Add(da.Desc);
							}
							if (da.Desc == "Mail ZIP")
							{
								this.cbAdvZIP.Items.Add(da.Desc);								
							}
							if (da.Desc == "Site ZIP")
							{
								this.cbAdvZIP.Items.Add(da.Desc);								
							}
						}		
					}
				}
			}	

			if (null != Globals.htFilters && 0 != Globals.htFilters.Count)
			{
				//show saved Filters!
				int iCnt = 1;
				bool dateFilterOneFilled = false;				
				//IDictionaryEnumerator ide = Globals.htFilters.GetEnumerator();
				ide = Globals.htFilters.GetEnumerator();
				while (ide.MoveNext())
				{
					if (ide.Value is MathematicalFilter)
					{
						MathematicalFilter mf = (MathematicalFilter)ide.Value;
						this.cbDataColumn5.SelectedItem = mf.DataColumn1;
						this.cbCondition5.SelectedItem = mf.DefinedOperator;
						this.cbDataColumn6.SelectedItem = mf.DataColumn2;
						this.cbCondition6.SelectedItem = mf.ResultCondition;
						this.tbValue5.Text = mf.DefinedValue.ToString();
					}
					if (ide.Value is SimpleFilter)
					{
						SimpleFilter sf = (SimpleFilter)ide.Value;
						if (1 == iCnt)
						{
							this.cbDataColumn1.SelectedItem = sf.DataColumn;
							this.cbCondition1.SelectedItem  = sf.Condition;
							this.tbValue1.Text				= sf.ValueDefined.ToString();
						}
						if (2 == iCnt)
						{
							this.cbDataColumn2.SelectedItem = sf.DataColumn;
							this.cbCondition2.SelectedItem  = sf.Condition;
							this.tbValue2.Text				= sf.ValueDefined.ToString();
						}
						if (3 == iCnt)
						{
							this.cbDataColumn3.SelectedItem = sf.DataColumn;
							this.cbCondition3.SelectedItem  = sf.Condition;
							this.tbValue3.Text				= sf.ValueDefined.ToString();
						}
						if (4 == iCnt)
						{
							this.cbDataColumn4.SelectedItem = sf.DataColumn;
							this.cbCondition4.SelectedItem  = sf.Condition;
							this.tbValue4.Text				= sf.ValueDefined.ToString();
						}
						iCnt++;
					}
					if (ide.Value is ZIPFilter)
					{
						ZIPFilter zf = (ZIPFilter)ide.Value;
						this.cbAdvZIP.SelectedItem = zf.PropertyName;
						this.cbZIPCondition.SelectedItem = zf.Condition;
						this.tbZips.Text = zf.Zips;
					}
					if (ide.Value is DateFilter)
					{
						DateFilter df = (DateFilter)ide.Value;
						if (false == dateFilterOneFilled)
						{
							//Fill 1st filter
							cbDateFilterLeftSide1.SelectedItem = df.PropertyName;
							cbDateFilter1Cond.SelectedItem = df.Condition;
							tbDateFilterValue1.Text = df.Dates;
							dateFilterOneFilled = true;
						}
						else
						{
							//Fill 2nd filter
							cbDateFilterLeftSide2.SelectedItem = df.PropertyName;
							cbDateFilter2Cond.SelectedItem = df.Condition;
							tbDateFilterValue2.Text = df.Dates;
						}

					}
				}
			}
			
		}
		#endregion � PopulateDataColumns �
		#region � CheckMapping �
		private void CheckMapping()
		{
			
		}
		#endregion � CheckMapping �
		#region � bNext_Click �
		public void bNext_Click(object sender, System.EventArgs e)
		{
			Globals.importWindowPos = this.Location;
			this.SaveProfile();
			//this.Close();
			/*
			ImportMask5 im5 = new ImportMask5();			
			im5.SelectedFile = this.SelectedFile;
			im5.Checked = this.Checked;
			im5.CreateNewProfile = this.CreateNewProfile;						
			im5.SelectedProfile = this.SelectedProfile;
			im5.InputDelimiter = this.InputDelimiter;

			im5.Show();*/
		}
		#endregion � bNext_Click �
		#region � SaveProfile �
		private void SaveProfile()
		{			
			//TA++ 26.03.2005
			//We'll clear all filters and apply them once again...
			Globals.htFilters.Clear();
			//TA-- 26.03.2005
			
			#region � Simple Filter 1 �
			if	(
				string.Empty != (string)this.cbDataColumn1.SelectedItem		&&
				string.Empty != (string)this.cbCondition1.SelectedItem		&&
				string.Empty != this.tbValue1.Text									
				)
			{
				try
				{
					double value1 = Convert.ToDouble(this.tbValue1.Text);
					SimpleFilter sf = new SimpleFilter(
						(string)this.cbDataColumn1.SelectedItem, 
						(string)this.cbCondition1.SelectedItem, 
						value1);
					if (Globals.htFilters.ContainsKey("sf1"))
					{
						Globals.htFilters["sf1"] = sf;
					}
					else
					{
						Globals.htFilters.Add("sf1", sf);
					}
				}
				catch (Exception exc)
				{
					MessageBox.Show("Could not convert to value 1 to number!");
					string s = exc.Message;
					return;
				}
			}
			/*else
			{
					if (Globals.htFilters.ContainsKey("sf1"))
						Globals.htFilters.Remove("sf1");
			}*/
			#endregion � Simple Filter 1 �
			#region � Simple Filter 2 �
			if	(
				string.Empty != (string)this.cbDataColumn2.SelectedItem		&&
				string.Empty != (string)this.cbCondition2.SelectedItem		&&
				string.Empty != this.tbValue2.Text									
				)
			{
				try
				{
					double value2 = Convert.ToDouble(this.tbValue2.Text);
					SimpleFilter sf = new SimpleFilter(
						(string)this.cbDataColumn2.SelectedItem, 
						(string)this.cbCondition2.SelectedItem, 
						value2);
					if (Globals.htFilters.ContainsKey("sf2"))
					{
						Globals.htFilters["sf2"] = sf;
					}
					else
					{
						Globals.htFilters.Add("sf2", sf);
					}

				}
				catch (Exception exc)
				{
					MessageBox.Show("Could not convert to value 2 to number!");
					string s = exc.Message;
					return;
				}
			}
			/*else
			{
				if (Globals.htFilters.ContainsKey("sf2"))
					Globals.htFilters.Remove("sf2");
			}*/
			#endregion � Simple Filter 2 �
			#region � Simple Filter 3 �
			if	(
				string.Empty != (string)this.cbDataColumn3.SelectedItem		&&
				string.Empty != (string)this.cbCondition3.SelectedItem		&&
				string.Empty != this.tbValue3.Text									
				)
			{
				try
				{
					double value3 = Convert.ToDouble(this.tbValue3.Text);
					SimpleFilter sf = new SimpleFilter(
						(string)this.cbDataColumn3.SelectedItem, 
						(string)this.cbCondition3.SelectedItem, 
						value3);
					if (Globals.htFilters.ContainsKey("sf3"))
					{
						Globals.htFilters["sf3"] = sf;
					}
					else
					{
						Globals.htFilters.Add("sf3", sf);
					}

				}
				catch (Exception exc)
				{
					MessageBox.Show("Could not convert to value 3 to number!");
					string s = exc.Message;
					return;
				}
			}
			/*else
			{
				if (Globals.htFilters.ContainsKey("sf3"))
					Globals.htFilters.Remove("sf3");
			}*/
			#endregion � Simple Filter 3 �
			#region � Simple Filter 4 �
			if	(
				string.Empty != (string)this.cbDataColumn4.SelectedItem		&&
				string.Empty != (string)this.cbCondition4.SelectedItem		&&
				string.Empty != this.tbValue4.Text									
				)
			{
				try
				{
					double value4 = Convert.ToDouble(this.tbValue4.Text);
					SimpleFilter sf = new SimpleFilter(
						(string)this.cbDataColumn4.SelectedItem, 
						(string)this.cbCondition4.SelectedItem, 
						value4);
					if (Globals.htFilters.ContainsKey("sf4"))
					{
						Globals.htFilters["sf4"] = sf;
					}
					else
					{
						Globals.htFilters.Add("sf4", sf);
					}

				}
				catch (Exception exc)
				{
					MessageBox.Show("Could not convert to value 4 to number!");
					string s = exc.Message;
					return;
				}
			}
			/*else
			{
				if (Globals.htFilters.ContainsKey("sf4"))
					Globals.htFilters.Remove("sf4");
			}*/
			#endregion � Simple Filter 4 �
			#region � Mathematical Filter �
			if	(
				string.Empty != (string)this.cbDataColumn5.SelectedItem		&&
				string.Empty != (string)this.cbCondition5.SelectedItem		&&
				string.Empty != (string)this.cbDataColumn6.SelectedItem		&&
				string.Empty != (string)this.cbCondition6.SelectedItem		&&
				string.Empty != this.tbValue5.Text									
				)
			{
				try
				{
					double value5 = Convert.ToDouble(this.tbValue5.Text);
					MathematicalFilter mf = new MathematicalFilter(
						(string)this.cbDataColumn5.SelectedItem,
						(string)this.cbDataColumn6.SelectedItem,
						(string)this.cbCondition5.SelectedItem,
						
						(string)this.cbCondition6.SelectedItem,
						value5);
					if (Globals.htFilters.ContainsKey("mf"))
					{
						Globals.htFilters["mf"] = mf;
					}
					else
					{
						Globals.htFilters.Add("mf", mf);
					}
				}
				catch (Exception exc)
				{
					MessageBox.Show("Could not convert to value 5 to number!");
					string s = exc.Message;
					return;
				}
			}
			/*else
			{
				if (Globals.htFilters.ContainsKey("mf"))
					Globals.htFilters.Remove("mf");
			}*/
			#endregion � Mathematical Filter �

			if (
				string.Empty != (string)cbAdvZIP.SelectedItem &&
				string.Empty != (string)cbZIPCondition.SelectedItem &&
				string.Empty != tbZips.Text
				)
			{
				ZIPFilter zf = new ZIPFilter(tbZips.Text, (string)cbAdvZIP.SelectedItem, (string)cbZIPCondition.SelectedItem);
				if (false == Globals.htFilters.Contains("zf"))
				{
					Globals.htFilters.Add("zf", zf);
				}
				else
				{
					Globals.htFilters["zf"] = zf;
				}

			}
			/*else
			{
				if (Globals.htFilters.ContainsKey("zf"))
					Globals.htFilters.Remove("zf");
			}*/



			if (
				string.Empty != (string)cbDateFilterLeftSide1.SelectedItem &&
				string.Empty != (string)cbDateFilter1Cond.SelectedItem &&
				string.Empty != tbDateFilterValue1.Text
				)
			{
				DateFilter df = new DateFilter(tbDateFilterValue1.Text, (string)cbDateFilterLeftSide1.SelectedItem, (string)cbDateFilter1Cond.SelectedItem);
				if (false == Globals.htFilters.Contains("df_" + df.PropertyName))
				{
					Globals.htFilters.Add("df_" + df.PropertyName, df);
				}
				else			
				{
					Globals.htFilters["df_" + df.PropertyName] = df;
				}
			}

			if (
				string.Empty != (string)cbDateFilterLeftSide2.SelectedItem &&
				string.Empty != (string)cbDateFilter2Cond.SelectedItem &&
				string.Empty != tbDateFilterValue2.Text
				)
			{
				DateFilter df = new DateFilter(tbDateFilterValue2.Text, (string)cbDateFilterLeftSide2.SelectedItem, (string)cbDateFilter2Cond.SelectedItem);
				if (false == Globals.htFilters.Contains("df_" + df.PropertyName))
				{
					Globals.htFilters.Add("df_" + df.PropertyName, df);
				}
				else			
				{
					Globals.htFilters["df_" + df.PropertyName] = df;
				}
			}			
		}
		#endregion � SaveProfile �

		private void bCancel_Click(object sender, System.EventArgs e)
		{
			if (DialogResult.Yes == MessageBox.Show(this, UserMessages.CancelImportText, UserMessages.CancelImport, MessageBoxButtons.YesNo, MessageBoxIcon.Question))
			{
				Globals.importWizardStarted = false;
				//this.Close();
			}
		}

		public void UpdateZIPFilter()
		{
			Globals.zipFilter = new ArrayList();
			string temp = this.tbZips.Text;
			temp = temp.Replace(" ", "");
			string zip = string.Empty;
			foreach (char ch in temp)
			{
				
				if (ch != ',')
				{
					zip += ch;
				}
				else
				{
					Globals.zipFilter.Add(zip);
					zip = string.Empty;
				}
			}
			if (string.Empty != zip)
			{
				Globals.zipFilter.Add(zip);
			}
		}
		#endregion � Methods �

		private void ImportMask4_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (true == e.Cancel)
			{
				if (DialogResult.Yes == MessageBox.Show(this, UserMessages.CancelImportText, UserMessages.CancelImport, MessageBoxButtons.YesNo, MessageBoxIcon.Question))
				{
					Globals.importWizardStarted = false;
					//this.Close();
				}
			}
		}

		private void comboBox_KeyPressed(object sender, System.Windows.Forms.KeyPressEventArgs e)
		{
			//to DISALLOW manual entries !!!
			e.Handled = true;
		}

		private void bReset_Click(object sender, System.EventArgs e)
		{
			ApplicationHelper.ResetFilters();
		}

		private void bZIPFilter_Click(object sender, System.EventArgs e)
		{
			AdvancedZIPFilterForm azff = new AdvancedZIPFilterForm();
			azff.ShowDialog();

			this.alZIPs = azff.ZIPs;
		}

		private void tbDateFilterValue_MouseDown1(object sender, MouseEventArgs e)
		{
			this.HandleDateFilter(1);
		}

		private void tbDateFilterValue_KeyDown1(object sender, KeyEventArgs e)
		{
			this.HandleDateFilter(1);
		}

		private void tbDateFilterValue_MouseDown2(object sender, MouseEventArgs e)
		{
			this.HandleDateFilter(2);			
		}

		private void tbDateFilterValue_KeyDown2(object sender, KeyEventArgs e)
		{
			this.HandleDateFilter(2);			
		}

		private void HandleDateFilter(int filterIdx)
		{
			bool cleared;
			DateTime dt = DateTime.MinValue;
			if (true == this.ShowDateForm(out dt, out cleared))
			{
				//update val
				if (1 == filterIdx)
				{
					this.tbDateFilterValue1.Text = dt.ToShortDateString();
				}
				else if (2 == filterIdx)
				{
					this.tbDateFilterValue2.Text = dt.ToShortDateString();
				}
			}
			else
			{
				if (true == cleared)
				{
					//update val
					if (1 == filterIdx)
					{
						this.tbDateFilterValue1.Text = string.Empty;
					}
					else if (2 == filterIdx)
					{
						this.tbDateFilterValue2.Text = string.Empty;
					}
				}
			}
		}
		

		private bool ShowDateForm(out DateTime selectedDate, out bool cleared)
		{
			DateTimePickerForm dtpf = new DateTimePickerForm();
			dtpf.ShowDialog();

			cleared = false;
			selectedDate = DateTime.MinValue;
			if (true == dtpf.Valid)
			{
				selectedDate = dtpf.SelectedDate;
				return true;
			}
			else
			{
				if (true == dtpf.ClearDate)
				{
					cleared = true;
				}
				return false;
			}
		}
	}
	#endregion � Class ImportMask4 �
}
#endregion � Namespace DealMaker �