#region � Using �
using System;
using System.IO;
using System.Text;
using System.Drawing;
using System.Reflection;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
#endregion � Using �
#region � Namespace DealMaker �
namespace DealMaker
{
	delegate void ShowProgressDelegate(int totalLines, int linesSoFar);
	#region � Class ImportMask3 �
	/// <summary>
	/// Class ImportMask3 - implements requirements defined in preRequisites
	/// </summary>
	public class ImportMask3 : System.Windows.Forms.UserControl
	{
		#region � Data �
		bool _trimZIP = true;
		private string selectedState = string.Empty;
		private Delimiter inputDelimiter;
		private bool useMappingTable = false;
		private string columnName = string.Empty;
		private int columnClickedIndex = -1;
        bool separateFirstName = false;
        bool lastNameFirst = false;
		//private int prevColumnClickedIndex = -1;
		//private Color prevColumnColor;
		private string selectedFile;
		private bool bChecked;
		private bool bCreateNewProfile = false;
		private string selectedProfile = string.Empty;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Button bNext;
		private System.Windows.Forms.Button bPrev;
		private System.Windows.Forms.ContextMenu ctxMenu;
		private System.Windows.Forms.ListView lvData;
		private System.Windows.Forms.MenuItem menuItem1;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox tbSelectedColumn;
		private System.Windows.Forms.Button bAssign;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Button bUnAssign;
		private System.Windows.Forms.Button bCancel;
		private DealMaker.ReadMe rm;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
		#endregion � Data �
		#region � Properties �
		#region � UseMappingTable �
		public bool UseMappingTable
		{
			get {return this.useMappingTable;}
			set {this.useMappingTable = value;}
		}
		#endregion � UseMappingTable �
		#region � InputDelimiter �
		public Delimiter InputDelimiter
		{
			get {return this.inputDelimiter;}
			set {this.inputDelimiter = value;}
		}
		#endregion � InputDelimiter �
		#region � SelectedFile �
		public string SelectedFile
		{
			get {return this.selectedFile;}
			set {this.selectedFile = value;}
		}
		#endregion � SelectedFile �
		#region � Checked �
		public bool Checked
		{
			get {return this.bChecked;}
			set {this.bChecked = value;}
		}
		#endregion � Checked �
		#region � CreateNewProfile �
		public bool CreateNewProfile
		{
			get {return this.bCreateNewProfile;}
			set {this.bCreateNewProfile = value;}
		}
		#endregion � CreateNewProfile �
		#region � SelectedProfile �
		public string SelectedProfile
		{
			get {return this.selectedProfile; }
			set {this.selectedProfile = value;}
		}		
		#endregion � SelectedProfile �
		public string SelectedState
		{
			get {return this.selectedState;}
			set {this.selectedState = value; }
		}

		public bool TrimZIP
		{
			get {return _trimZIP;}
			set {_trimZIP = value;}
		}

        public bool SeparateFirstName
        {
            get { return separateFirstName; }
            set { separateFirstName = value; }
        }

        public bool LastNameFirst
        {
            get { return lastNameFirst; }
            set { lastNameFirst = value; }
        }
		#endregion � Properties �
		#region � CTR && Dispose �
		public ImportMask3()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			this.Location = Globals.importWindowPos;
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}
		#endregion � CTR && Dispose �
		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(ImportMask3));
			this.lvData = new System.Windows.Forms.ListView();
			this.ctxMenu = new System.Windows.Forms.ContextMenu();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.label2 = new System.Windows.Forms.Label();
			this.bNext = new System.Windows.Forms.Button();
			this.bPrev = new System.Windows.Forms.Button();
			this.bCancel = new System.Windows.Forms.Button();
			this.menuItem1 = new System.Windows.Forms.MenuItem();
			this.label1 = new System.Windows.Forms.Label();
			this.tbSelectedColumn = new System.Windows.Forms.TextBox();
			this.bAssign = new System.Windows.Forms.Button();
			this.bUnAssign = new System.Windows.Forms.Button();
			this.rm = new DealMaker.ReadMe();
			this.groupBox1.SuspendLayout();
			this.SuspendLayout();
			// 
			// lvData
			// 
			this.lvData.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.lvData.BackColor = System.Drawing.SystemColors.Window;
			this.lvData.ContextMenu = this.ctxMenu;
			this.lvData.FullRowSelect = false;
			this.lvData.GridLines = true;
			this.lvData.Location = new System.Drawing.Point(16, 72);
			this.lvData.Name = "lvData";
			this.lvData.Size = new System.Drawing.Size(504, 216);
			this.lvData.TabIndex = 0;
			this.lvData.View = System.Windows.Forms.View.Details;
			this.lvData.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.lvData_ColumnClick);
			// 
			// groupBox1
			// 
			this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox1.Controls.Add(this.label2);
			this.groupBox1.Controls.Add(this.lvData);
			this.groupBox1.Controls.Add(this.bNext);
			this.groupBox1.Controls.Add(this.bPrev);
			this.groupBox1.Controls.Add(this.bCancel);
			this.groupBox1.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.groupBox1.Location = new System.Drawing.Point(16, 24);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(536, 304);
			this.groupBox1.TabIndex = 1;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Field Mapping / Assigning";
			// 
			// label2
			// 
			this.label2.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.label2.Location = new System.Drawing.Point(16, 32);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(200, 23);
			this.label2.TabIndex = 1;
			this.label2.Text = "Click on Column Header to select it";
			// 
			// bNext
			// 
			this.bNext.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.bNext.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.bNext.Location = new System.Drawing.Point(440, 16);
			this.bNext.Name = "bNext";
			this.bNext.TabIndex = 2;
			this.bNext.Text = "Next";
			this.bNext.Visible = false;
			this.bNext.Click += new System.EventHandler(this.bNext_Click);
			// 
			// bPrev
			// 
			this.bPrev.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.bPrev.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.bPrev.Location = new System.Drawing.Point(360, 20);
			this.bPrev.Name = "bPrev";
			this.bPrev.TabIndex = 3;
			this.bPrev.Text = "Previous";
			this.bPrev.Visible = false;
			this.bPrev.Click += new System.EventHandler(this.bPrev_Click);
			// 
			// bCancel
			// 
			this.bCancel.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.bCancel.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.bCancel.Location = new System.Drawing.Point(280, 20);
			this.bCancel.Name = "bCancel";
			this.bCancel.TabIndex = 9;
			this.bCancel.Text = "Cancel";
			this.bCancel.Visible = false;
			this.bCancel.Click += new System.EventHandler(this.bCancel_Click);
			// 
			// menuItem1
			// 
			this.menuItem1.Index = -1;
			this.menuItem1.Text = "";
			// 
			// label1
			// 
			this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.label1.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.label1.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.label1.Location = new System.Drawing.Point(16, 348);
			this.label1.Name = "label1";
			this.label1.TabIndex = 5;
			this.label1.Text = "Selected Column:";
			this.label1.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
			// 
			// tbSelectedColumn
			// 
			this.tbSelectedColumn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.tbSelectedColumn.Location = new System.Drawing.Point(120, 344);
			this.tbSelectedColumn.Name = "tbSelectedColumn";
			this.tbSelectedColumn.ReadOnly = true;
			this.tbSelectedColumn.Size = new System.Drawing.Size(184, 20);
			this.tbSelectedColumn.TabIndex = 6;
			this.tbSelectedColumn.Text = "";
			// 
			// bAssign
			// 
			this.bAssign.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.bAssign.Enabled = false;
			this.bAssign.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.bAssign.Location = new System.Drawing.Point(392, 342);
			this.bAssign.Name = "bAssign";
			this.bAssign.TabIndex = 7;
			this.bAssign.Text = "Assign";
			this.bAssign.Click += new System.EventHandler(this.bAssign_Click);
			// 
			// bUnAssign
			// 
			this.bUnAssign.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.bUnAssign.Enabled = false;
			this.bUnAssign.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.bUnAssign.Location = new System.Drawing.Point(313, 342);
			this.bUnAssign.Name = "bUnAssign";
			this.bUnAssign.TabIndex = 8;
			this.bUnAssign.Text = "Un-Assign";
			this.bUnAssign.Click += new System.EventHandler(this.bUnAssign_Click);
			// 
			// rm
			// 
			this.rm.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(210)), ((System.Byte)(216)), ((System.Byte)(240)));
			//this.rm.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("rm.BackgroundImage")));			
			this.rm.Location = new System.Drawing.Point(8, 8);
			this.rm.Name = "rm";
			this.rm.Size = new System.Drawing.Size(16, 16);
			this.rm.TabIndex = 9;
			// 
			// ImportMask3
			// 
			this.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(210)), ((System.Byte)(216)), ((System.Byte)(240)));
			this.Controls.Add(this.rm);
			this.Controls.Add(this.bUnAssign);
			this.Controls.Add(this.bAssign);
			this.Controls.Add(this.tbSelectedColumn);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.groupBox1);
			this.Name = "ImportMask3";
			this.Size = new System.Drawing.Size(576, 384);
			this.Load += new System.EventHandler(this.ImportMask3_Load);
			this.groupBox1.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion		
		#region � Methods �
		#region � bNext_Click �
		public void bNext_Click(object sender, System.EventArgs e)
		{
			this.CheckOwnerMapping();
			//TA++: 28.03.2005. New features added
			/*
			if (false == this.CheckOwnerMapping())
			{
				SpecifyOwnerName son = new SpecifyOwnerName();
				son.ShowDialog();
			}
			*/
			//TA--: 28.03.2005. New features added
			Globals.importWindowPos = this.Location;
			MessageBox.Show(this, UserMessages.Warning, "Warning!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
			//this.Close();
			/*ImportMask4 im4 = new ImportMask4();
			im4.SelectedFile = this.SelectedFile;
			im4.Checked = this.Checked;
			im4.CreateNewProfile = this.CreateNewProfile;
			im4.InputDelimiter = this.InputDelimiter;
			im4.SelectedProfile = this.SelectedProfile;
			im4.Show();*/
			this.CheckSiteStateMapping();
			this.CheckZIPMapping();
		}
		#endregion � bNext_Click �		
		#region � bPrev_Click �
		private void bPrev_Click(object sender, System.EventArgs e)
		{
			Globals.importWindowPos = this.Location;
			//this.Close();
			ImportMask2 im2 = new ImportMask2();
			im2.SelectedFile = this.SelectedFile;
			im2.Checked = this.Checked;
			im2.CreateNewProfile = this.CreateNewProfile;
			im2.InputDelimiter = this.InputDelimiter;
			im2.SelectedProfile = this.SelectedProfile;
			im2.Show();
		}
		#endregion � bPrev_Click �		
		#region � ImportMask3_Load �
		private void ImportMask3_Load(object sender, System.EventArgs e)
		{			
			if (true == this.UseMappingTable)
			{
				//this.useMappingTable = false;
			}
			else
			{
				//CLEAR TABLES !!!
				Globals.htMappingInfo.Clear();
				Globals.htFilters.Clear();
			}

			char delimiter = ',';
			switch (this.InputDelimiter)
			{
				case Delimiter.comma:
					delimiter = ',';
					break;
				case Delimiter.hash:
					delimiter = '#';
					break;
				case Delimiter.semiComma:
					delimiter = ';';
					break;
				case Delimiter.tab:
					delimiter = '\t';
					break;
			}
 
			this.PopulateListView(this.SelectedFile, this.Checked, delimiter, true);
			this.BuildMappingInfoTable();
			
			if (true == this.bChecked && this.lvData.Items.Count > 0)
			{
				//ListViewItem lvi = this.lvData.Items[0];
				//lvi.ForeColor = Color.Red;
				//lvi.BackColor = Color.LightGray;				
			}
			
			if (false == this.CreateNewProfile && false == this.UseMappingTable)
			{
				//load profile
				this.FillHashtables();				

			}
			if (false == this.CreateNewProfile && false == this.UseMappingTable)
			{				
				this.LoadFilters();
			}
			
			this.UpdateColumnNames();
			
			if (true == this.UseMappingTable)//reset it
			{
				this.useMappingTable = false;
			}
		}
		#endregion � ImportMask3_Load �		
		#region � BuildMappingInfoTable �
		private void BuildMappingInfoTable()
		{
			ImportIntermedeateObject iio = new ImportIntermedeateObject();
			Type iioType = iio.GetType();			
			PropertyInfo[] pi =  iioType.GetProperties();			
			for (int i = 0; i < pi.Length; i++)
			{
				object[] attr = pi[i].GetCustomAttributes(typeof(DescriptionAttribute), true);
				if (0 != attr.Length)
				{      
					DescriptionAttribute da = (DescriptionAttribute)attr[0];
					MapperObjectInfo moi = new MapperObjectInfo(pi[i].Name, da.Desc);
					if (false == Globals.htMappingInfo.ContainsKey(moi))
					{
						Globals.htMappingInfo.Add(moi, new MapperSourceInfo());
					}
				}
			}	
		}
		#endregion � BuildMappingInfoTable �
		#region � PopulateContextMenu �
		private void PopulateContextMenu()
		{
			ImportIntermedeateObject iio = new ImportIntermedeateObject();
			Type iioType = iio.GetType();			
			PropertyInfo[] pi =  iioType.GetProperties();			
			for (int i = 0; i < pi.Length; i++)
			{
				//int fieldvalue = (Int32)fields[i].GetValue(null);				
				MenuItem	mi = new MenuItem(pi[i].Name);				
				//pi[i].Attributes
				mi.Click += new System.EventHandler(this.menuItem_Click);
				this.ctxMenu.MenuItems.Add(mi);
			}			
		}
		#endregion � PopulateContextMenu �
		
		

		#region � PopulateListView  �
		private void PopulateListView (string selectedFile, bool firstRowContainsHeaders, char separator, bool createNewProfile)
		{			
			int lnCnt = 0;
			using (StreamReader sr = new StreamReader(selectedFile))
			{
				/*
				string completeFile = sr.ReadToEnd();
				for (int i = 0; i < completeFile.Length; i++)
				{
					if (completeFile[i] == '\n')
					{
						lnCnt++;
					}
				}				
				*/
				
				//int iReadLineCnt = 0;
				while (null != sr.ReadLine())
				{
					lnCnt++;
				}
				
			}
			Globals.Capacity = lnCnt;

			//Event trigger
			EventTrigger et = new EventTrigger();
			PBForm pbf = new PBForm(et);
			pbf.Capacity = lnCnt;
			pbf.WindowName = "Loading Data...";			
			pbf.Show();
			

			ArrayList alItems = new ArrayList();
			
			if (this.InputDelimiter == Delimiter.comma)
			{
				using (CSVReader csv = new CSVReader(this.SelectedFile)) 
				{	
			
					
					string[] fields;
					while ((fields = csv.GetCSVLine()) != null) 
					{						
						ArrayList alRow = new ArrayList();		
						foreach (string field in fields) 
						{
							alRow.Add(field);
						}
						alItems.Add(alRow);
						
						//fire event
						et.TriggerChange();
					}
				}
			}
			else
			{
				using (StreamReader sr = new StreamReader(selectedFile)) 
				{	
								
					String line = string.Empty;
					while ((line = sr.ReadLine()) != null)
					{
						if (line == string.Empty)
						{
							//fire event
							et.TriggerChange();	//to close pb form when last rows are empty !

							continue;
						}
															
						ArrayList alRow = new ArrayList();
						if (separator != ',')
						{
							while (-1 != line.IndexOf(separator))
							{
								string columnItem = line.Substring(0, line.IndexOf(separator));						
								line = line.Substring(line.IndexOf(separator) + 1);
								alRow.Add(columnItem);
							}
							//Add last element
							if (0 != line.Length)
							{
								line = line.Trim();
								if (0 != line.Length)
								{
									alRow.Add(line);
								}
							}
							alItems.Add(alRow);
						}
						
						
						//fire event
						et.TriggerChange();
					}
					sr.Close();
				}			
			}
			
			#region � Display  Header Names �
			if (true == createNewProfile && alItems.Count > 0)
			{
				//set UnAssiged
				ArrayList alRow = (ArrayList)alItems[0];
				for (int iCnt = 0; iCnt < alRow.Count; iCnt++)
				{
					ColumnHeader ch = new ColumnHeader();
					ch.Text = UserMessages.UnAssigned;
					ch.Width = 100;
					this.lvData.Columns.Add(ch);
				}
			}
			#endregion � Display  Header Names �
			

			int listViewMaxCapacity = (alItems.Count > 100) ? 100 : alItems.Count;
			for (int i = 0; i < listViewMaxCapacity; i++)
			{
				ArrayList alRow = (ArrayList)alItems[i];
				ListViewItem lvi = null;
				for (int j = 0; j < alRow.Count; j++)
				{
					if (0 == j)
					{
						lvi = new ListViewItem((string)alRow[j]);
					}
					else
					{				
						lvi.SubItems.Add((string)alRow[j]);
					}					
				}
				if (null != lvi)
				{
					this.lvData.Items.Add(lvi);										
				}
			}
			
		}
		#endregion � PopulateListView  �
		#region � menuItem_Click �
		private void menuItem_Click(object sender, System.EventArgs e)
		{
			//MessageBox.Show(sender.ToString() + " " + e.ToString());
		}
		#endregion � menuItem_Click �
		#region � SetHeaderColors �
		private void SetHeaderColors(ListViewItem lvi)
		{
			
			//set all to white
			for (int i = 0; i < this.lvData.Columns.Count; i++)
			{
				lvi.SubItems[i].BackColor = Color.White;
			}
			
			//set assiged to yellow
			IDictionaryEnumerator ide = Globals.htMappingInfo.GetEnumerator();
			while (ide.MoveNext())
			{
				MapperSourceInfo msi = (MapperSourceInfo)ide.Value;
				if (-1 != msi.ColumnIndex)
				{
					MapperObjectInfo moi = (MapperObjectInfo)ide.Key;					
					lvi.SubItems[msi.ColumnIndex].BackColor = Color.Yellow;
				}					
			}

			//set selected to lightblue
			lvi.SubItems[this.columnClickedIndex].BackColor = Color.LightBlue;			
			this.lvData.EndUpdate();
		}
		#endregion � SetHeaderColors �
		#region � lvData_ColumnClick �
		private void lvData_ColumnClick(object sender, System.Windows.Forms.ColumnClickEventArgs e)
		{	
			this.lvData.BeginUpdate();		
			if (false == this.bAssign.Enabled)
				this.bAssign.Enabled = true;

			ListView lv = (ListView)sender;
			ListViewItem lvi = this.lvData.Items[0];			
			/*
			if (-1 != this.prevColumnClickedIndex)
			{
				lvi.SubItems[this.prevColumnClickedIndex].BackColor = prevColumnColor;
			}*/

			this.columnClickedIndex = e.Column;
			//this.prevColumnClickedIndex = this.columnClickedIndex;
			//this.prevColumnColor = lvi.SubItems[this.columnClickedIndex].BackColor;

			//lvi.SubItems[this.columnClickedIndex].BackColor = Color.Blue;
			this.columnName = lvi.SubItems[e.Column].Text;

			if (this.bChecked)
				this.tbSelectedColumn.Text = "'" + this.columnName + "', Column: " + this.columnClickedIndex.ToString();
			else
				this.tbSelectedColumn.Text = UserMessages.UnKnown + ", Column: " + this.columnClickedIndex.ToString();		
			
			this.bUnAssign.Enabled = this.CheckIfAssigned(this.columnClickedIndex);				
			this.SetHeaderColors(lvi);			
		}
		#endregion � lvData_ColumnClick �						
		#region � CheckIfAssigned �
		private bool CheckIfAssigned(int columnIndex)
		{			
			IDictionaryEnumerator ide = Globals.htMappingInfo.GetEnumerator();
			while (ide.MoveNext())
			{
				MapperSourceInfo msi = (MapperSourceInfo)ide.Value;
				if (msi.ColumnIndex == columnIndex)
				{
					MapperObjectInfo moi = (MapperObjectInfo)ide.Key;					
					return true;
				}
			}
			return false;
		}
		#endregion � CheckIfAssigned �			
		#region � bAssign_Click �
		private void bAssign_Click(object sender, System.EventArgs e)
		{
			using (MapperForm1 mf1 = new MapperForm1()) 
			{
				try
				{	
					//check if already assigned
					if (false == this.CheckIfAssigned(this.columnClickedIndex))
					{
						mf1.ShowDialog();
					
						if (null != mf1.MappedObject)
						{
							MapperObjectInfo mio = (MapperObjectInfo)mf1.MappedObject;
							MapperSourceInfo msi = new MapperSourceInfo(this.columnClickedIndex, this.columnName);

							//update mapping info
							if (Globals.htMappingInfo.ContainsKey(mio))
							{
								Globals.htMappingInfo[mio] = msi;						
							}
							//update columnName
							this.UpdateColumnNames();
						}
					}
					else
					{
						MessageBox.Show(this, UserMessages.AlreadyAssignedDesc, UserMessages.AlreadyAssigned, MessageBoxButtons.OK, MessageBoxIcon.Stop);
					}
				}
				catch(System.Runtime.InteropServices.SEHException sehe)
				{
					MessageBox.Show(sehe.Message);
				}
				catch(Exception exc)
				{					
					MessageBox.Show(exc.Message);
				}
				finally
				{
					mf1.Dispose();					
				}
			}		
		}
		#endregion � bAssign_Click �			
		#region � UpdateColumnNames �
		private void UpdateColumnNames()
		{
			try
			{
				//this.lvData.SuspendLayout();
				this.lvData.BeginUpdate();
				ListViewItem lvi = this.lvData.Items[0];
				lvi.UseItemStyleForSubItems = false;

				IDictionaryEnumerator ide = Globals.htMappingInfo.GetEnumerator();
				while (ide.MoveNext())
				{
					MapperSourceInfo msi = (MapperSourceInfo)ide.Value;
					if (-1 != msi.ColumnIndex)
					{
						MapperObjectInfo moi = (MapperObjectInfo)ide.Key;					
						ColumnHeader chOriginal = (ColumnHeader)this.lvData.Columns[msi.ColumnIndex];
						chOriginal.Text = moi.PropertyDesc;
						chOriginal.Width = moi.PropertyDesc.Length * 9;
						
						//ListViewItem.ListViewSubItem subItem = lvi.SubItems[msi.ColumnIndex];
						//subItem.BackColor = Color.Yellow;
						lvi.SubItems[msi.ColumnIndex].BackColor = Color.Yellow;
						/*for (int i = 0; i < this.lvData.Items.Count; i++)
						{							
							lvi = this.lvData.Items[i];	
							lvi.UseItemStyleForSubItems = false;
							lvi.SubItems[msi.ColumnIndex].BackColor = Color.Yellow;				
						}*/						
					}					
				}
				this.lvData.EndUpdate();
				//this.lvData.ResumeLayout();
			}
			catch(Exception)
			{
				MessageBox.Show(this, UserMessages.WrongBinding, UserMessages.WrongBindingHeader, MessageBoxButtons.OK, MessageBoxIcon.Error);
				Panel p = (Panel)this.Parent;
				ImportMask im = (ImportMask)p.Parent;

				im.bPrevious_Click (this, null);
			
				//return;
			}
		}
		#endregion � UpdateColumnNames �
		#region � CheckOwnerMapping �
		private bool CheckOwnerMapping()
		{			 
			bool firstNameAssigned = false;
			bool fullNameAssigned = false;
			IDictionaryEnumerator ide = Globals.htMappingInfo.GetEnumerator();
			while (ide.MoveNext())
			{
				MapperObjectInfo moi = (MapperObjectInfo)ide.Key;				
				
				//TA++: 28.03.2005: New features added ?!?!?!?!
				if (moi.PropertyName == "FirstOwnerName")
				{
					MapperSourceInfo msi = (MapperSourceInfo)ide.Value;
					if (-1 != msi.ColumnIndex)
					{
						firstNameAssigned = true;				
					}
				}
				if (moi.PropertyName == "FullOwnerName")
				{
					MapperSourceInfo msi = (MapperSourceInfo)ide.Value;
					if (-1 != msi.ColumnIndex)
					{
						fullNameAssigned = true;										
					}
				}				
				
				/*if (moi.PropertyName == "FirstOwnerName" || moi.PropertyName == "LastOwnerName" || moi.PropertyName == "MiddleOwnerName" || moi.PropertyName == "FullOwnerName")
				{
					MapperSourceInfo msi = (MapperSourceInfo)ide.Value;
					if (-1 != msi.ColumnIndex)
						return true;
				}
				*/
				//TA--: 28.03.2005: New features added ?!?!?!?!
			}
			//TA++: 28.03.2005: New features added ?!?!?!?!
			if (false == firstNameAssigned && true == fullNameAssigned)
			{
				//MessageBox.Show(this, "First Owner name is not assigned or present in your import file,\ntherefore the program will use \"Dear homeowner\" on your letters.", "Mapping", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                if (DialogResult.Yes == MessageBox.Show(this, "ProfitGrabber has detected that your list does NOT have the First & Last names separated. " + Environment.NewLine + "Your marketing letters will NOT be automatically personalized, i.e. \"Dear John\" because of that reason." + Environment.NewLine + Environment.NewLine + "Do you want the system to automatically try to separate First & Last names?", "Alert", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
                {
                    ResolveNameForm rnf = new ResolveNameForm();
                    if (DialogResult.OK == rnf.ShowDialog())
                    {
                        this.separateFirstName = true;
                        this.lastNameFirst = rnf.LastNameFirst;
                    }
                }
			}
				
			if (false == firstNameAssigned && false == fullNameAssigned)
			{
				MessageBox.Show(this, "The Owner Name fields are not assigned or present in your import file, \ntherefore the program will use \"Dear homeowner\" on your letters, and \"Current resident\" on your mailing labels.", "Mapping", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
			}
			//TA--: 28.03.2005: New features added ?!?!?!?!

			return false;
		}
		#endregion � CheckOwnerMapping �

		public bool CheckSiteStateMapping()
		{
			bool siteStateAssigned = false;
			IDictionaryEnumerator ide = Globals.htMappingInfo.GetEnumerator();
			while (ide.MoveNext())
			{
				MapperObjectInfo moi = (MapperObjectInfo)ide.Key;				
				
				//TA++: 04.09.2005: New features added SiteState MUST be assigned
				if (moi.PropertyName == "SiteState")
				{
					MapperSourceInfo msi = (MapperSourceInfo)ide.Value;
					if (-1 != msi.ColumnIndex)
					{
						siteStateAssigned = true;
						break;
					}
				}
			}

			if (false == siteStateAssigned)
			{
				SpecifyStateForm ssf = new SpecifyStateForm();
				ssf.ShowDialog();

				this.SelectedState = ssf.SelecedState;

			}
			return siteStateAssigned;
		}

		bool CheckZIPMapping()
		{
			bool zipAssigned = false;
			IDictionaryEnumerator ide = Globals.htMappingInfo.GetEnumerator();
			while (ide.MoveNext())
			{
				MapperObjectInfo moi = (MapperObjectInfo)ide.Key;				
				
				//TA++: 04.09.2005: New features added SiteState MUST be assigned
				if (moi.PropertyName == "SiteZIP" || moi.PropertyName == "MailingZIP")
				{
					MapperSourceInfo msi = (MapperSourceInfo)ide.Value;
					if (-1 != msi.ColumnIndex)
					{
						zipAssigned = true;
						break;
					}
				}
			}

			/*
			if (true == zipAssigned)
			{
				TrimZIPForm tzf = new TrimZIPForm();
				tzf.ShowDialog();

				_trimZIP = tzf.Trim;
			}
			*/
			return zipAssigned;
		}

		#region � bUnAssign_Click �
		private void bUnAssign_Click(object sender, System.EventArgs e)
		{
			MapperSourceInfo msi = new MapperSourceInfo(this.columnClickedIndex, this.columnName);
			IDictionaryEnumerator ide = Globals.htMappingInfo.GetEnumerator();
			while (ide.MoveNext())
			{
				MapperSourceInfo msiFound = (MapperSourceInfo)ide.Value;
				if (msiFound.ColumnIndex == msi.ColumnIndex)
				{
					MapperSourceInfo newMsi = new MapperSourceInfo();
					Globals.htMappingInfo[(MapperObjectInfo)ide.Key] = newMsi;
					break;
				}
			}
			this.bUnAssign.Enabled = false;
			
			ColumnHeader chOriginal = (ColumnHeader)this.lvData.Columns[this.columnClickedIndex];
			chOriginal.Text = UserMessages.UnAssigned;
			chOriginal.Width = 100;
			
		}
		#endregion � bUnAssign_Click �				
		#region � FillFilters �		
		private void FillFilters(ref StreamReader sr)
		{
			string line = string.Empty;
			while ((line = sr.ReadLine()) != null) 
			{
				if (Globals.SimpleFilterStarter == line)
				{
					//SimpleFilter
					if ((line = sr.ReadLine()) != null)
					{

					}
				}
				if (Globals.MathFilterStarter == line)
				{
					if ((line = sr.ReadLine()) != null)
					{

					}
				}
			}
		}
		#endregion � FillFilters �		
		#region � LoadFilters �
		private void LoadFilters ()
		{
			Globals.htFilters.Clear();
			bool mathFilter = false;
			bool simpleFilter = false;
			bool zipFilter = false;
			bool dateFilter = false;
			
			using (StreamReader sr1 = new StreamReader(this.SelectedProfile)) 
			{
				char separator = '\t';
				string line = string.Empty;
				int iLineCnt = 0;
								
				while ((line = sr1.ReadLine()) != null)
				{					
					if (line == Globals.MathFilterStarter)
					{
						mathFilter = true;
						continue;
					}
					if (line == Globals.MathFilterEnder)
					{
						mathFilter = false;
						continue;
					}
					if (line == Globals.SimpleFilterStarter)
					{
						simpleFilter = true;
						continue;
					}
					if (line == Globals.SimpleFilterEnder)					
					{
						simpleFilter = false;
						continue;
					}
					if (line == Globals.ZIPFilterStarter)
					{						
						zipFilter=true;
						continue;
					}
					if (line == Globals.ZIPFilterEnder)
					{
						zipFilter=false;
						continue;
					}
					if (line == Globals.DateFilterStarter)
					{						
						dateFilter=true;
						continue;
					}
					if (line == Globals.DateFilterEnder)
					{
						dateFilter=false;
						continue;
					}

					if (true == simpleFilter)
					{
						iLineCnt++;
						int iSep1 = 0, iSep2 = 0;
						iSep1 = line.IndexOf(separator);
						iSep2 = line.IndexOf(separator, iSep1 + 1);
						string leftPropertyName = line.Substring(0, iSep1);
						string oper	= line.Substring(iSep1 + 1, iSep2 - iSep1 - 1);
						string val  = line.Substring(iSep2 + 1);
						try
						{
							double dVal = Convert.ToDouble(val);							
							SimpleFilter sf = new SimpleFilter();
							sf.DataColumn = leftPropertyName;
							sf.Condition = oper;
							sf.ValueDefined = dVal;
							Globals.htFilters.Add("sf" + iLineCnt.ToString(), sf);

						}
						catch (Exception exc)
						{
							MessageBox.Show("SimpleFilter failed to load! " + exc.Message);
						}
										
					}
					else if (true == mathFilter)
					{
						int iSep1 = 0, iSep2 = 0, iSep3 = 0, iSep4 = 0;
						iSep1 = line.IndexOf(separator);
						iSep2 = line.IndexOf(separator, iSep1 + 1);
						iSep3 = line.IndexOf(separator, iSep2 + 1);
						iSep4 = line.IndexOf(separator, iSep3 + 1);
						string leftPropertyName = line.Substring(0, iSep1);
						string rightPropertyName = line.Substring(iSep1 + 1, iSep2 - iSep1 - 1);
						string oper1 = line.Substring(iSep2 + 1, iSep3 - iSep2 - 1);
						string oper2  = line.Substring(iSep3 + 1, iSep4 - iSep3 - 1);
						string val =  line.Substring(iSep4 + 1);
						try
						{
							double dVal = Convert.ToDouble(val);
							MathematicalFilter mf = new MathematicalFilter(leftPropertyName, rightPropertyName, oper1, oper2, dVal);
							Globals.htFilters.Add("mf", mf);
						}
						catch (Exception exc)
						{
							MessageBox.Show("MathFiler failed to load! " + exc.Message);
						}					
					}	
					else if (true == zipFilter)
					{
						string propertyName = line;
						string zips = string.Empty;
						string condition = string.Empty;
						if ( (line = sr1.ReadLine()) != null)
						{
							condition = line;
						}

						if ( (line = sr1.ReadLine()) != null)
						{
							zips = line;
						}

						if (false == Globals.htFilters.Contains("zf_" + propertyName))
						{
							Globals.htFilters.Add("zf_" + propertyName, new ZIPFilter(zips, propertyName, condition));
						}
					}

					else if (true == dateFilter)
					{
						string propertyName = line;
						string dates = string.Empty;
						string condition = string.Empty;

						if ( (line = sr1.ReadLine()) != null)
						{
							condition = line;
						}

						if ( (line = sr1.ReadLine()) != null)
						{
							dates = line;
						}

						if (false == Globals.htFilters.Contains("df_" + propertyName))
						{
							Globals.htFilters.Add("df_" + propertyName, new DateFilter(dates, propertyName, condition));
						}
					}
				}
				sr1.Close();			
			}
		}
		#endregion � LoadFilters �
		#region � FillHashtables �
		private void FillHashtables ()
		{			
			using (StreamReader sr = new StreamReader(this.SelectedProfile)) 
			{
				char separator = '\t';
				string line = string.Empty;
				int iLineCnt = 0;
				while ((line = sr.ReadLine()) != null) 
				{
					iLineCnt++;
					if (1 == iLineCnt)
					{
						continue;	//profile desc.
					}
					
					
					if (line == Globals.MapperDelimiter)
					{
						break;						
					}
					
					int iPos1 = 0, iPos2 = 0, iPos3 = 0;
					iPos1 = line.IndexOf(separator);
					iPos2 = line.IndexOf(separator, iPos1 + 1);
					iPos3 = line.IndexOf(separator, iPos2 + 1);
					
					string propertyName = line.Substring(0, iPos1);
					propertyName = propertyName.TrimEnd(' ');
					
					string propertyDesc = line.Substring(iPos1 + 1, iPos2 - iPos1 - 1);
					propertyDesc = propertyDesc.TrimEnd(' ');
					
					string columnIndex = line.Substring(iPos2 + 1, iPos3 - iPos2 - 1);
					columnIndex = columnIndex.TrimEnd(' ');
					int iColumnIndex = -1;

					string columnDesc = line.Substring(iPos3 + 1);
					columnDesc = columnDesc.TrimEnd(' ');

					try
					{
						iColumnIndex = Convert.ToInt32(columnIndex);
					}
					catch(Exception)
					{
						MessageBox.Show(UserMessages.CouldNotCastToInteger);
					}
					
					MapperObjectInfo moi = new MapperObjectInfo(propertyName, propertyDesc);
					MapperSourceInfo msi = new MapperSourceInfo(iColumnIndex, columnDesc);
					
					if (false == Globals.htMappingInfo.ContainsKey(moi))
					{
						Globals.htMappingInfo.Add(moi, msi);
					}
					else
					{
						//update
						Globals.htMappingInfo[moi] = msi;
					}
				}
				sr.Close();
			}
		}
		#endregion � FillHashtables �
		#region � bCancel_Click �
		private void bCancel_Click(object sender, System.EventArgs e)
		{
			if (DialogResult.Yes == MessageBox.Show(this, UserMessages.CancelImportText, UserMessages.CancelImport, MessageBoxButtons.YesNo, MessageBoxIcon.Question))
			{
				Globals.importWizardStarted = false;
				//this.Close();
			}
		}
		#endregion � bCancel_Click �
		#region � ImportMask3_Closing �
		private void ImportMask3_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (true == e.Cancel)
			{
				if (DialogResult.Yes == MessageBox.Show(this, UserMessages.CancelImportText, UserMessages.CancelImport, MessageBoxButtons.YesNo, MessageBoxIcon.Question))
				{
					Globals.importWizardStarted = false;
					//this.Close();
				}
			}
		}
		#endregion � ImportMask3_Closing �
		#endregion � Methods �						
	}
		#endregion � Class ImportMask3 �	
}
#endregion � Namespace DealMaker �