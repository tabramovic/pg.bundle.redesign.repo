using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using ObjectPresentation;
using NHibernate;

namespace DealMaker.Forms.SchedulingActicities
{
	/// <summary>
	/// Summary description for AssignActivity.
	/// </summary>
	public class AssignActivity : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Label assignActivityLabel;
		private System.Windows.Forms.Label selectEventDateLabel;
		private System.Windows.Forms.Label activityCreatedForLabel;
		private System.Windows.Forms.Label activityCreatedByLabel;
		private System.Windows.Forms.Button cancelButton;
		private System.Windows.Forms.Button saveAsButton;
		private System.Windows.Forms.Button assignButton;
		private System.Windows.Forms.MainMenu mainMenu1;
		private System.Windows.Forms.Label buildActivityLabel;
		private System.Windows.Forms.Label taskListLable;
		private System.Windows.Forms.Label taskListPosition;
		private System.Windows.Forms.Button editTaskButton;
		private System.Windows.Forms.Button addTaskButton;
        private System.Windows.Forms.Button deleteTaskButton;
        private IContainer components;

		ArrayList tasks = new ArrayList();
		private System.Windows.Forms.ComboBox CreatedFor;
		private System.Windows.Forms.ComboBox CreatedBy;
		private System.Windows.Forms.DateTimePicker EventDate;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Button selectExistingActivity;
		private System.Windows.Forms.Button ClearAll;
		GridEditObjects taskListGrid;

		private bool askToSave = false;

		Activity activity;
		public Activity Activity
		{
			set
			{
				activity = value;
			}
			get
			{
				return activity;
			}
		}

		private PropertyItem user;
		private System.Windows.Forms.TextBox assignTo;
		private System.Windows.Forms.TextBox ActivityName;
	
		public PropertyItem User
		{
			set
			{
				user = value;
				group = null;

				if(user != null && user.Owner !=null)
					assignTo.Text = user.Owner.FullName;
			}
			get
			{
				return user;
			}
		}

		private Node group;
		public Node Group
		{
			set
			{
				group = value;
				user = null;

				assignTo.Text = group.nodeName;
			}
			get
			{
				return group;
			}
		}

		public AssignActivity()
		{

//			tasks.Add(new Task());
//			tasks.Add(new Task());
//			tasks.Add(new Task());

			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			Static.FillCreatedCombo(CreatedFor);
			Static.FillCreatedCombo(CreatedBy);

			taskListGrid = new GridEditObjects(tasks, typeof(Task), "Task.xml");
			taskListGrid.grid.BackColor =  System.Drawing.Color.FromArgb(((System.Byte)(210)), ((System.Byte)(216)), ((System.Byte)(240)));

			Static.FillTaskListWithTaskTypes(taskListGrid);
/*			ArrayList list = new ArrayList();

			//MessageBox.Show(((int)TaskType.Mail).ToString());

			for(int i=0;i<10;i++) list.Add("");

			list[0] = "Undefined";
			list[(int)TaskType.Call] = "Call";
			list[(int)TaskType.Mail] = "Mail";
			list[(int)TaskType.ToDo] = "ToDo";
			list[(int)TaskType.Visit] = "Visit";
			list[(int)TaskType.Fax] = "Fax";

			taskListGrid.AddIntList("Type", list);
*/
			taskListGrid.Location = taskListPosition.Location;
			taskListGrid.Size = taskListPosition.Size;

			taskListGrid.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top;
			
			taskListGrid.Editable = false;

			taskListGrid.ShowGrid();
			this.Controls.Add(taskListGrid);
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.assignActivityLabel = new System.Windows.Forms.Label();
            this.assignTo = new System.Windows.Forms.TextBox();
            this.selectEventDateLabel = new System.Windows.Forms.Label();
            this.EventDate = new System.Windows.Forms.DateTimePicker();
            this.activityCreatedForLabel = new System.Windows.Forms.Label();
            this.activityCreatedByLabel = new System.Windows.Forms.Label();
            this.cancelButton = new System.Windows.Forms.Button();
            this.saveAsButton = new System.Windows.Forms.Button();
            this.assignButton = new System.Windows.Forms.Button();
            this.CreatedFor = new System.Windows.Forms.ComboBox();
            this.CreatedBy = new System.Windows.Forms.ComboBox();
            this.mainMenu1 = new System.Windows.Forms.MainMenu(this.components);
            this.buildActivityLabel = new System.Windows.Forms.Label();
            this.taskListLable = new System.Windows.Forms.Label();
            this.taskListPosition = new System.Windows.Forms.Label();
            this.editTaskButton = new System.Windows.Forms.Button();
            this.addTaskButton = new System.Windows.Forms.Button();
            this.deleteTaskButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.ActivityName = new System.Windows.Forms.TextBox();
            this.selectExistingActivity = new System.Windows.Forms.Button();
            this.ClearAll = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // assignActivityLabel
            // 
            this.assignActivityLabel.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.assignActivityLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.assignActivityLabel.Location = new System.Drawing.Point(16, 16);
            this.assignActivityLabel.Name = "assignActivityLabel";
            this.assignActivityLabel.Size = new System.Drawing.Size(104, 24);
            this.assignActivityLabel.TabIndex = 0;
            this.assignActivityLabel.Text = "Assign activity to:";
            this.assignActivityLabel.Click += new System.EventHandler(this.assignActivityLabel_Click);
            // 
            // assignTo
            // 
            this.assignTo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.assignTo.Location = new System.Drawing.Point(128, 16);
            this.assignTo.Name = "assignTo";
            this.assignTo.ReadOnly = true;
            this.assignTo.Size = new System.Drawing.Size(336, 20);
            this.assignTo.TabIndex = 1;
            this.assignTo.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // selectEventDateLabel
            // 
            this.selectEventDateLabel.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.selectEventDateLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.selectEventDateLabel.Location = new System.Drawing.Point(16, 120);
            this.selectEventDateLabel.Name = "selectEventDateLabel";
            this.selectEventDateLabel.Size = new System.Drawing.Size(168, 23);
            this.selectEventDateLabel.TabIndex = 2;
            this.selectEventDateLabel.Text = "Select YOUR EVENT DATE:";
            this.selectEventDateLabel.Click += new System.EventHandler(this.label2_Click);
            // 
            // EventDate
            // 
            this.EventDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.EventDate.Location = new System.Drawing.Point(176, 120);
            this.EventDate.Name = "EventDate";
            this.EventDate.Size = new System.Drawing.Size(192, 20);
            this.EventDate.TabIndex = 3;
            // 
            // activityCreatedForLabel
            // 
            this.activityCreatedForLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.activityCreatedForLabel.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.activityCreatedForLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.activityCreatedForLabel.Location = new System.Drawing.Point(16, 413);
            this.activityCreatedForLabel.Name = "activityCreatedForLabel";
            this.activityCreatedForLabel.Size = new System.Drawing.Size(112, 24);
            this.activityCreatedForLabel.TabIndex = 4;
            this.activityCreatedForLabel.Text = "Activity Created For:";
            // 
            // activityCreatedByLabel
            // 
            this.activityCreatedByLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.activityCreatedByLabel.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.activityCreatedByLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.activityCreatedByLabel.Location = new System.Drawing.Point(16, 445);
            this.activityCreatedByLabel.Name = "activityCreatedByLabel";
            this.activityCreatedByLabel.Size = new System.Drawing.Size(112, 24);
            this.activityCreatedByLabel.TabIndex = 5;
            this.activityCreatedByLabel.Text = "Activity Created By:";
            // 
            // cancelButton
            // 
            this.cancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.cancelButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cancelButton.Location = new System.Drawing.Point(16, 477);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(72, 24);
            this.cancelButton.TabIndex = 6;
            this.cancelButton.Text = "Cancel";
            // 
            // saveAsButton
            // 
            this.saveAsButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.saveAsButton.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.saveAsButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.saveAsButton.Location = new System.Drawing.Point(432, 373);
            this.saveAsButton.Name = "saveAsButton";
            this.saveAsButton.Size = new System.Drawing.Size(72, 24);
            this.saveAsButton.TabIndex = 7;
            this.saveAsButton.Text = "Save As";
            this.saveAsButton.Click += new System.EventHandler(this.saveAsButton_Click);
            // 
            // assignButton
            // 
            this.assignButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.assignButton.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.assignButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.assignButton.Location = new System.Drawing.Point(432, 477);
            this.assignButton.Name = "assignButton";
            this.assignButton.Size = new System.Drawing.Size(72, 24);
            this.assignButton.TabIndex = 8;
            this.assignButton.Text = "Assign";
            this.assignButton.Click += new System.EventHandler(this.assignButton_Click);
            // 
            // CreatedFor
            // 
            this.CreatedFor.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.CreatedFor.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CreatedFor.Location = new System.Drawing.Point(136, 413);
            this.CreatedFor.Name = "CreatedFor";
            this.CreatedFor.Size = new System.Drawing.Size(152, 21);
            this.CreatedFor.TabIndex = 9;
            // 
            // CreatedBy
            // 
            this.CreatedBy.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.CreatedBy.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CreatedBy.Location = new System.Drawing.Point(136, 445);
            this.CreatedBy.Name = "CreatedBy";
            this.CreatedBy.Size = new System.Drawing.Size(152, 21);
            this.CreatedBy.TabIndex = 10;
            // 
            // buildActivityLabel
            // 
            this.buildActivityLabel.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.buildActivityLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buildActivityLabel.Location = new System.Drawing.Point(80, 160);
            this.buildActivityLabel.Name = "buildActivityLabel";
            this.buildActivityLabel.Size = new System.Drawing.Size(336, 24);
            this.buildActivityLabel.TabIndex = 11;
            this.buildActivityLabel.Text = "Build Your Activity by using buttons below list.";
            // 
            // taskListLable
            // 
            this.taskListLable.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.taskListLable.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.taskListLable.Location = new System.Drawing.Point(8, 160);
            this.taskListLable.Name = "taskListLable";
            this.taskListLable.Size = new System.Drawing.Size(72, 16);
            this.taskListLable.TabIndex = 12;
            this.taskListLable.Text = "TASK LIST:";
            // 
            // taskListPosition
            // 
            this.taskListPosition.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.taskListPosition.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.taskListPosition.Location = new System.Drawing.Point(16, 192);
            this.taskListPosition.Name = "taskListPosition";
            this.taskListPosition.Size = new System.Drawing.Size(496, 160);
            this.taskListPosition.TabIndex = 13;
            this.taskListPosition.Text = "task list position (inivisible)";
            this.taskListPosition.Visible = false;
            // 
            // editTaskButton
            // 
            this.editTaskButton.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.editTaskButton.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.editTaskButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.editTaskButton.Location = new System.Drawing.Point(112, 373);
            this.editTaskButton.Name = "editTaskButton";
            this.editTaskButton.Size = new System.Drawing.Size(72, 24);
            this.editTaskButton.TabIndex = 15;
            this.editTaskButton.Text = "Edit Task";
            this.editTaskButton.Click += new System.EventHandler(this.editTaskButton_Click);
            // 
            // addTaskButton
            // 
            this.addTaskButton.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.addTaskButton.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.addTaskButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addTaskButton.Location = new System.Drawing.Point(16, 373);
            this.addTaskButton.Name = "addTaskButton";
            this.addTaskButton.Size = new System.Drawing.Size(72, 24);
            this.addTaskButton.TabIndex = 14;
            this.addTaskButton.Text = "Add Task";
            this.addTaskButton.Click += new System.EventHandler(this.addTaskButton_Click);
            // 
            // deleteTaskButton
            // 
            this.deleteTaskButton.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.deleteTaskButton.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.deleteTaskButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deleteTaskButton.Location = new System.Drawing.Point(208, 373);
            this.deleteTaskButton.Name = "deleteTaskButton";
            this.deleteTaskButton.Size = new System.Drawing.Size(72, 24);
            this.deleteTaskButton.TabIndex = 16;
            this.deleteTaskButton.Text = "Delete Task";
            this.deleteTaskButton.Click += new System.EventHandler(this.deleteTaskButton_Click);
            // 
            // label1
            // 
            this.label1.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(8, 48);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(416, 23);
            this.label1.TabIndex = 17;
            this.label1.Text = "Create a NEW activity below, or Load an EXISTING ACTIVITY:";
            // 
            // ActivityName
            // 
            this.ActivityName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.ActivityName.Enabled = false;
            this.ActivityName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ActivityName.Location = new System.Drawing.Point(96, 80);
            this.ActivityName.Name = "ActivityName";
            this.ActivityName.Size = new System.Drawing.Size(272, 20);
            this.ActivityName.TabIndex = 18;
            // 
            // selectExistingActivity
            // 
            this.selectExistingActivity.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.selectExistingActivity.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.selectExistingActivity.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.selectExistingActivity.Location = new System.Drawing.Point(384, 80);
            this.selectExistingActivity.Name = "selectExistingActivity";
            this.selectExistingActivity.Size = new System.Drawing.Size(48, 24);
            this.selectExistingActivity.TabIndex = 19;
            this.selectExistingActivity.Text = "...";
            this.selectExistingActivity.Click += new System.EventHandler(this.selectExistingActivity_Click);
            // 
            // ClearAll
            // 
            this.ClearAll.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.ClearAll.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.ClearAll.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ClearAll.Location = new System.Drawing.Point(344, 373);
            this.ClearAll.Name = "ClearAll";
            this.ClearAll.Size = new System.Drawing.Size(72, 24);
            this.ClearAll.TabIndex = 20;
            this.ClearAll.Text = "Clear All";
            this.ClearAll.Click += new System.EventHandler(this.ClearAll_Click);
            // 
            // AssignActivity
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.ClientSize = new System.Drawing.Size(522, 525);
            this.Controls.Add(this.ClearAll);
            this.Controls.Add(this.selectExistingActivity);
            this.Controls.Add(this.ActivityName);
            this.Controls.Add(this.assignTo);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.deleteTaskButton);
            this.Controls.Add(this.editTaskButton);
            this.Controls.Add(this.addTaskButton);
            this.Controls.Add(this.taskListPosition);
            this.Controls.Add(this.taskListLable);
            this.Controls.Add(this.buildActivityLabel);
            this.Controls.Add(this.CreatedBy);
            this.Controls.Add(this.CreatedFor);
            this.Controls.Add(this.assignButton);
            this.Controls.Add(this.saveAsButton);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.activityCreatedByLabel);
            this.Controls.Add(this.activityCreatedForLabel);
            this.Controls.Add(this.EventDate);
            this.Controls.Add(this.selectEventDateLabel);
            this.Controls.Add(this.assignActivityLabel);
            this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Menu = this.mainMenu1;
            this.MinimumSize = new System.Drawing.Size(528, 528);
            this.Name = "AssignActivity";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Assign Activity";
            this.Load += new System.EventHandler(this.AssignActivity_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		#endregion

		private void label2_Click(object sender, System.EventArgs e)
		{
		
		}

		private void assignActivityLabel_Click(object sender, System.EventArgs e)
		{
		
		}

		private void textBox1_TextChanged(object sender, System.EventArgs e)
		{
		
		}

		private void addTaskButton_Click(object sender, System.EventArgs e)
		{
			Task task = new Task();

			task.EventDate = this.EventDate.Value;
			task.CreatedBy = this.CreatedBy.Text;
			task.CreatedFor = this.CreatedFor.Text;

			task.MoveToBuisnessDay = true;

			task.Status = (int)Status.Activity;

//			if((task = Static.InputTask(task, false)) != null)

			AssignTaskStep1 form = new AssignTaskStep1();

			form.task = task;
			form.ShowDate = false;
			
			if(group != null) form.Group = group;
			if(user != null) form.User = User;

			form.ShowDialog();

			task.IdNode = 0;
			task.IdPropertyItem = Guid.Empty;

			if(form.DialogResult == DialogResult.OK)
			{
				askToSave = true;
				tasks.Add(task);
				taskListGrid.ShowGrid();
			}
		}

		private void deleteTaskButton_Click(object sender, System.EventArgs e)
		{
			askToSave = true;
			taskListGrid.deleteCell();
		}

		private void editTaskButton_Click(object sender, System.EventArgs e)
		{
			//set event date to selected
			foreach(Task t in tasks)
			{
				t.EventDate = EventDate.Value.Date;
			}

			Task task = (Task)taskListGrid.SelectedObject;
			if(task != null)
			{
				AssignTaskStep2 form2 = new AssignTaskStep2();

				if(group != null) form2.Group = group;
				if(user != null) form2.User = User;

				form2.TaskType = (TaskType)task.Type;
				form2.Task = task;
				form2.ShowDate = false;

				form2.ShowDialog();

				taskListGrid.ShowGrid();

				askToSave = true;
			}
			else
			{
				MessageBox.Show("Please select task first!");
			}
		}

		private void AssignActivity_Load(object sender, System.EventArgs e)
		{
		
		}

		private void selectExistingActivity_Click(object sender, System.EventArgs e)
		{
/*			NHibernate.ISession session = Globals.factory.OpenSession();
//			NHibernate.ITransaction = session.BeginTransaction();

			IList activitiyList =  session.CreateCriteria(typeof(Activity)).List();

			FormSelectObject formSelectObject = new FormSelectObject(activitiyList, typeof(Activity), "Activity.xml");

			formSelectObject.Text = "Select activity to assign";

			formSelectObject.ShowDialog();


			session.Close();
*/
			if(tasks.Count > 0)
			{
				if(MessageBox.Show("You are about to add another activity to your Task List. Do you want to proceed?", "Attention", MessageBoxButtons.YesNo) == DialogResult.No)
				{
					return;
				}
			}

			SelectActivity selectActivity = new SelectActivity();

			selectActivity.SetLoadActivity();

			selectActivity.ShowDialog();

			if(selectActivity.DialogResult == DialogResult.OK)
			{
				Activity activity;

				activity = selectActivity.Activity;

//				tasks.Clear();
				tasks.AddRange(activity.Task);
                foreach (Task t in tasks)
                    t.TaskDate = DateTime.Now.Date;

				taskListGrid.ShowGrid();

				this.CreatedBy.Text = activity.CreatedBy;
				this.CreatedFor.Text = activity.CreatedFor;
				//				this.EventDate.Value = activity.EventDate;

				this.ActivityName.Text = activity.Name;
			}
		}
/*
		private void deleteExistingActivity_Click(object sender, System.EventArgs e)
		{
			NHibernate.ISession session = Globals.factory.OpenSession();
			NHibernate.ITransaction transaction = session.BeginTransaction();

			IList activitiyList =  session.CreateCriteria(typeof(Activity)).List();

			FormSelectObject formSelectObject = new FormSelectObject(activitiyList, typeof(Activity), "Activity.xml");

			formSelectObject.Text = "Select activity to delete";

			formSelectObject.ShowDialog();

			if(formSelectObject.DialogResult == DialogResult.OK)
			{
				session.Delete(formSelectObject.SelectedObject);
			}

			transaction.Commit();
			session.Close();
		}
*/
		private void saveAsButton_Click(object sender, System.EventArgs e)
		{
/*			QueryBox queryBox = new QueryBox();

			queryBox.Text = "Enter Activity Name";

			queryBox.ShowDialog();

			if(queryBox.DialogResult == DialogResult.OK)
			{
				Activity activity = new Activity();

				activity.IdActivity = Guid.Empty;
				activity.Name = queryBox.Input;

				activity.CreatedBy = this.CreatedBy.Text;
				activity.CreatedFor = this.CreatedFor.Text;
//				activity.EventDate = this.EventDate.Value;

				foreach(Task task in tasks)
				{
					task.IdTask = Guid.Empty;
				}

				activity.Task = tasks;

				NHibernate.ISession session = Globals.factory.OpenSession();
				NHibernate.ITransaction transaction = session.BeginTransaction();

				session.Save(activity);

				transaction.Commit();
				session.Close();
			}
*/
			prepareActivity();

			SelectActivity selectActivity = new SelectActivity();

			selectActivity.Activity = activity;

			selectActivity.SetSaveActivity();

			selectActivity.ShowDialog();

			if(selectActivity.DialogResult == DialogResult.OK)
			{
				this.ActivityName.Text = activity.Name;
				askToSave = false;
			}
		}

		private void prepareActivity()
		{
			activity = new Activity();

			activity.IdActivity = Guid.Empty;

			activity.CreatedBy = this.CreatedBy.Text;
			activity.CreatedFor = this.CreatedFor.Text;
			//				activity.EventDate = this.EventDate.Value;


			activity.EventDate = EventDate.Value.Date;

			foreach(Task task in tasks)
			{
				task.IdTask = Guid.Empty;
				task.TaskDate = task.EventDate = activity.EventDate.Date;
				task.TaskDate.AddDays(task.DatesFrom);

				if(group != null)
				{
					task.IdNode = group.nodeId;
					task.IdPropertyItem = Guid.Empty;
				}

				if(user != null)
				{
					task.IdPropertyItem = user.IdPropertyItem;
					task.IdNode = 0;
				}

				//sasa 12.12.05: bugfix, from date added only if moved to buisness day = true 
				//new, moved up, resolved problem when date incremented if moved to buisness day = true, but datesFrom moved it from weekday
				task.TaskDate = task.EventDate.Date.AddDays(task.DatesFrom);

				if(task.MoveToBuisnessDay)
				{
					if(task.TaskDate.DayOfWeek == System.DayOfWeek.Saturday)
					{
						//move forward 2 days if saturday
//						task.DatesFrom += 2;
						task.TaskDate = task.TaskDate.Date.AddDays(2);
					}

					if(task.TaskDate.DayOfWeek == System.DayOfWeek.Sunday)
					{
						//move forward 1 day if sunday
//						task.DatesFrom += 1;
						task.TaskDate = task.TaskDate.Date.AddDays(1);
					}

				}

				task.TaskDate = task.TaskDate.Date;
			}


			activity.Task = tasks;
		}

		private void assignButton_Click(object sender, System.EventArgs e)
		{
			if(tasks.Count > 0)
			{
				if(EventDate.Value < DateTime.Today)
				{
					MessageBox.Show("Event Date cannot be in past! Please enter correct Event Date.");
					this.DialogResult = DialogResult.None;
				}

				prepareActivity();

				if(askToSave)
					if(MessageBox.Show("Do you want to save this activity you just created?", "Attention", MessageBoxButtons.YesNo) == DialogResult.Yes)
					{
						saveAsButton_Click(null, null);
					}						

				this.DialogResult = DialogResult.OK;
				this.Close();
			}
			else
			{
				MessageBox.Show("Your Task List is empty. Add a task first!");
			}
		}

		private void ClearAll_Click(object sender, System.EventArgs e)
		{
			askToSave = false;
			tasks.Clear();
			taskListGrid.ShowGrid();
			this.ActivityName.Text = "";
		}
	}
}
