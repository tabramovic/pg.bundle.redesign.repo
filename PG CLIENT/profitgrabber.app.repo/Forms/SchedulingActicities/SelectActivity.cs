using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using NHibernate;
using NHibernate.Expression;

namespace DealMaker.Forms.SchedulingActicities
{
	/// <summary>
	/// Summary description for SelectActivity.
	/// </summary>
	public class SelectActivity : System.Windows.Forms.Form
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		ObjectPresentation.GridEditObjects objectList;
		private System.Windows.Forms.Button Cancel;
		private System.Windows.Forms.Button Delete;
		private System.Windows.Forms.Button Action;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox Description;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox ActivityName;
		private System.Windows.Forms.Panel namePanel;

		private Activity activity;
		private System.Windows.Forms.Label gridLoaction;

		ArrayList activitiyList = new ArrayList();

		public Activity Activity
		{
			get
			{
				return activity;
			}
			set
			{
				activity = value;
			}
		}

		private bool saveActivity = false;
		public void SetSaveActivity()
		{
			saveActivity = true;
			this.Action.Text = "Save";
			objectList.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top;
		}

		private bool loadActivity = false;
		public void SetLoadActivity()
		{
			loadActivity = true;
			objectList.Anchor = AnchorStyles.Top | AnchorStyles.Left;
			this.Height -= this.namePanel.Height;
			this.namePanel.Visible = false;
			this.Action.Text = "Load";
			objectList.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top;
		}

		public SelectActivity()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			loadActivityList();

			objectList = new ObjectPresentation.GridEditObjects(activitiyList, typeof(Activity), "ActivitySelect.xml");

			objectList.grid.BackColor =  System.Drawing.Color.FromArgb(((System.Byte)(210)), ((System.Byte)(216)), ((System.Byte)(240)));

			objectList.SelectionChanged += new ObjectPresentation.GridEditObjects.SelectionChangedEventHandler(selectionChange);

			objectList.Location = gridLoaction.Location;
			objectList.Size = gridLoaction.Size;

			objectList.Editable = false;

			loadActivityList();
			objectList.ShowGrid();

			this.Controls.Add(objectList);

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		private void loadActivityList()
		{
			NHibernate.ISession session = Globals.factory.OpenSession();

			try
			{
				IList list = session.CreateCriteria(typeof(Activity)).List();
				activitiyList.Clear();

				if(list != null)
					activitiyList.AddRange(list);
			}
			catch(Exception ee)
			{
				MessageBox.Show("Unable to load objects!");
				throw ee;
			}
			finally
			{
				session.Close();
			}
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Cancel = new System.Windows.Forms.Button();
			this.Delete = new System.Windows.Forms.Button();
			this.Action = new System.Windows.Forms.Button();
			this.namePanel = new System.Windows.Forms.Panel();
			this.label2 = new System.Windows.Forms.Label();
			this.Description = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.ActivityName = new System.Windows.Forms.TextBox();
			this.gridLoaction = new System.Windows.Forms.Label();
			this.namePanel.SuspendLayout();
			this.SuspendLayout();
			// 
			// Cancel
			// 
			this.Cancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.Cancel.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.Cancel.Location = new System.Drawing.Point(8, 344);
			this.Cancel.Name = "Cancel";
			this.Cancel.TabIndex = 4;
			this.Cancel.Text = "Cancel";
			// 
			// Delete
			// 
			this.Delete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.Delete.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.Delete.Location = new System.Drawing.Point(96, 344);
			this.Delete.Name = "Delete";
			this.Delete.TabIndex = 5;
			this.Delete.Text = "Delete";
			this.Delete.Click += new System.EventHandler(this.Delete_Click);
			// 
			// Action
			// 
			this.Action.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.Action.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.Action.Location = new System.Drawing.Point(448, 344);
			this.Action.Name = "Action";
			this.Action.TabIndex = 6;
			this.Action.Text = "Action";
			this.Action.Click += new System.EventHandler(this.Action_Click);
			// 
			// namePanel
			// 
			this.namePanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.namePanel.Controls.Add(this.label2);
			this.namePanel.Controls.Add(this.Description);
			this.namePanel.Controls.Add(this.label1);
			this.namePanel.Controls.Add(this.ActivityName);
			this.namePanel.Location = new System.Drawing.Point(8, 264);
			this.namePanel.Name = "namePanel";
			this.namePanel.Size = new System.Drawing.Size(520, 72);
			this.namePanel.TabIndex = 8;
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(8, 48);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(112, 23);
			this.label2.TabIndex = 7;
			this.label2.Text = "Activity Description:";
			// 
			// Description
			// 
			this.Description.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.Description.Location = new System.Drawing.Point(120, 40);
			this.Description.Name = "Description";
			this.Description.Size = new System.Drawing.Size(384, 20);
			this.Description.TabIndex = 6;
			this.Description.Text = "";
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(8, 9);
			this.label1.Name = "label1";
			this.label1.TabIndex = 5;
			this.label1.Text = "Activity Name:";
			// 
			// ActivityName
			// 
			this.ActivityName.Location = new System.Drawing.Point(120, 9);
			this.ActivityName.Name = "ActivityName";
			this.ActivityName.Size = new System.Drawing.Size(152, 20);
			this.ActivityName.TabIndex = 4;
			this.ActivityName.Text = "";
			// 
			// gridLoaction
			// 
			this.gridLoaction.Location = new System.Drawing.Point(8, 8);
			this.gridLoaction.Name = "gridLoaction";
			this.gridLoaction.Size = new System.Drawing.Size(520, 256);
			this.gridLoaction.TabIndex = 9;
			this.gridLoaction.Text = "gridLoaction";
			this.gridLoaction.Visible = false;
			// 
			// SelectActivity
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(210)), ((System.Byte)(216)), ((System.Byte)(240)));
			this.ClientSize = new System.Drawing.Size(536, 373);
			this.Controls.Add(this.gridLoaction);
			this.Controls.Add(this.namePanel);
			this.Controls.Add(this.Action);
			this.Controls.Add(this.Delete);
			this.Controls.Add(this.Cancel);
			this.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
			this.Name = "SelectActivity";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Select Activity";
			this.namePanel.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

        bool TestForExistingActivity(string activityName)
        {
            bool retVal = false;
            NHibernate.ISession session = Globals.factory.OpenSession();            

            try
            {
                IList existingActivity = session.CreateCriteria(typeof(Activity)).Add(Expression.Eq("Name", activityName)).List();
                if (existingActivity.Count > 0)
                {
                    foreach (object obj in existingActivity)
                    {
                        session.Delete(obj);
                    }
                    retVal = true;
                }
            }
            catch (Exception exc)
            {
                retVal = false;
            }
            finally
            {
                session.Flush();
                session.Close();
            }

            return retVal;

        }

		private void Action_Click(object sender, System.EventArgs e)
		{
			if(loadActivity)
			{
				activity = (Activity)objectList.SelectedObject;
			}

			if(saveActivity)
			{

                bool alreadyExistingActivity = TestForExistingActivity(ActivityName.Text);

				if(ActivityName.Text.Length > 0)
				{
					NHibernate.ISession session = Globals.factory.OpenSession();
					NHibernate.ITransaction transaction = session.BeginTransaction();

					try
					{
						activity.Name = ActivityName.Text;
						activity.Description = Description.Text;                        

                        /* -- OLD CAUSING UNABLE TO SAVE OBJECT ERROR
						IList existingActivity = session.CreateCriteria(typeof(Activity)).Add(Expression.Eq("Name", activity.Name)).List();
                                                
						if(existingActivity.Count > 0)
						{
							if(MessageBox.Show("Are you sure you want to replace an existing activity?", "Replace", MessageBoxButtons.OKCancel) == DialogResult.OK)
							{
								foreach(object obj in existingActivity)
								{
                                    session.Delete(obj);                                    
								}
							}
							else
							{
								session.Close();
								
								return;
							}
						}   
                        */

                        //NEW (BUGFIX)
                        if (alreadyExistingActivity)
                        {
                            if (MessageBox.Show("Are you sure you want to replace an existing activity?", "Replace", MessageBoxButtons.OKCancel) == DialogResult.Cancel)
                            {                                
                                return; //finally block will close the session
                            }
                        }


                        session.Evict(activity);
                        session.Save(activity);

						transaction.Commit();
					}
					catch(Exception ee)
					{
						transaction.Rollback();
						MessageBox.Show("Unable to save object!");
						throw ee;
					}
					finally
					{
						session.Close();
					}
				}
				else
				{
					MessageBox.Show("Please type in the name for this activity!");
					return;
				}
			}
		
			this.DialogResult = DialogResult.OK;
			this.Close();
		}

		private void selectionChange(object sender, System.EventArgs e)
		{
			Activity activity = (Activity)objectList.SelectedObject;

			if(activity != null)
			{
				this.ActivityName.Text = activity.Name;
				this.Description.Text = activity.Description;
			}
		}

		private void Delete_Click(object sender, System.EventArgs e)
		{
			if(MessageBox.Show("Are you sure you want to delete activity?", "Delete", MessageBoxButtons.YesNo) == DialogResult.Yes)
			{

				NHibernate.ISession session = Globals.factory.OpenSession();
				NHibernate.ITransaction transaction = session.BeginTransaction();

				if(objectList.SelectedObject != null)
				{
					try
					{
						DealMaker.Activity ac = (DealMaker.Activity)objectList.SelectedObject;

						session.Delete(ac);

						transaction.Commit();

//						activitiyList.Clear();

//						loadActivityList();

//						MessageBox.Show(activitiyList.Equals(objectList).ToString());

						activitiyList.Remove(ac);
//						objectList.loadedObjects.Remove(ac);

						objectList.ShowGrid();
					}
					catch(Exception ee)
					{
						MessageBox.Show("Unable to delete object!");
						throw ee;
					}
					finally
					{
						session.Close();
					}
				}
				else
				{
					MessageBox.Show("Please select object first!");
				}
			}
		}
	}
}
