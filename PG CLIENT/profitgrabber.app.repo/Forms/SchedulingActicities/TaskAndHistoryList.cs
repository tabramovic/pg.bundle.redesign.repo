using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;
using ObjectPresentation;

//Floaters
using DealMaker.Forms.Floaters;
using DealMaker.PlainClasses.ReturnMailWizard;

//ShortSale Module
//using ShortSaleModule.Common.Adapters;

namespace DealMaker.Forms.SchedulingActicities
{
	/// <summary>
	/// Summary description for TaskAndHistoryList.
	/// </summary>
	public class TaskAndHistoryList : System.Windows.Forms.UserControl
	{
		private DealMaker.Forms.Floaters.ActivitiesHistory activitiesFloater = null;
		
		private System.Windows.Forms.Label taskListLocation;
		private System.Windows.Forms.Label historyListLocation;
		private System.Windows.Forms.Label taskListFor;
		private System.Windows.Forms.Label historyListFor;
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		private ArrayList fullList = new ArrayList();
		private ArrayList taskList = new ArrayList();
		private ArrayList historyList = new ArrayList();

		private GridEditObjects taskListGrid;
		private GridEditObjects historyListGrid;

		private PropertyItem user;
		private System.Windows.Forms.Button AssignTask;
		private System.Windows.Forms.Button AssignActivity;
		private System.Windows.Forms.Button DeleteTask;
		private System.Windows.Forms.Button EditTask;
		private System.Windows.Forms.Button DeletePastAction;
		private System.Windows.Forms.Button RecordPastAction;
		private DealMaker.ReadMe readMe3;

        bool _ssxAssigned = false;
        private Button DoneTask;
	
		public bool IconVisible
		{
			get {return this.readMe3.Visible;}
			set {this.readMe3.Visible = value;}
		}
		
		public PropertyItem User
		{
			set
			{
				user = value;
				group = null;

				taskListFor.Text = "Task List (PENDING):";
				historyListFor.Text = "History list:";

				FillTaskList();
			}
			get
			{
				return user;
			}
		}

		private Node group;
		public Node Group
		{
			set
			{
				group = value;
				user = null;

				taskListFor.Text = "TASK LIST for Selected Group: " + group.nodeName;
				historyListFor.Text = "HISTORY LIST for Selected Group: " + group.nodeName;
				
				FillTaskList();
			}
			get
			{
				return group;
			}
		}

		public void SetEventHandler()
		{
		    Globals.customizedTreeViewUserControl.NodeChanged += new CustomizedTreeViewUserControl.NodeChangedEventHandler(nodeChanged);            
		}

        //ShortSale todo
        void SetSSXEventHandler()
        {
            if (!_ssxAssigned)
            {
                //DataManager.Instance.AssignedTask += new DataManager.AssignedTaskToSSX(On_SSXInstance_AssignedTask);
                _ssxAssigned = true;
            }
        }

        void On_SSXInstance_AssignedTask()
        {            
            FillTaskList();
        }

		private void nodeChanged(object sender, System.EventArgs e)
		{
			user = null;
			Node loadedGroup = new Node();

			loadedGroup.nodeId = Globals.selectedNodeId;
			loadedGroup.nodeName = Globals.selectedNodeName;

			Group = loadedGroup;

			FillTaskList();
		}


		#region � Network UDP �
		//sasa, 20.11.05: nw version, invoke required (refresh thread in different task)
		private delegate void UpdateDelegate();

		private void UpdateDelegateFunc()
		{
			FillTaskList();
		}
		
		private void NetworkUDP_ChangedTask(Network.NetworkUDP.Packet p)
		{
			Globals.MainForm.BeginInvoke(new UpdateDelegate(UpdateDelegateFunc));
		}
		#endregion � Network UDP �



//		//sasa, 13.11.05: nw version, invoke required (refresh thread in different task)
//		private delegate void UpdateDelegate();
		
		/// <summary>
		/// refreshes task and history list from database
		/// </summary>
		//sasa, 30.01.06: bugfix, task list was not refreshed on page reload, we need public access
		public void FillTaskList()
		{
//			//sasa, 13.11.05: nw version, invoke required (refresh thread in different task)
//			//			if (true == this.InvokeRequired)
//			if (true == this.taskListGrid.grid.InvokeRequired)
//			{
//				this.BeginInvoke(new UpdateDelegate(FillTaskList));
//				return;
//			}

            SetSSXEventHandler();

			fullList.Clear();
			taskList.Clear();
			historyList.Clear();

			if(group != null)
			{
				fullList.AddRange(Static.GetGroupTasks(group));
			}

			if(user != null)
			{
				fullList.AddRange(Static.GetUserTasks(user));
			}

			foreach(Task task in fullList)
			{
				if(task.Status == (int)Status.Pending)
				{
					taskList.Add(task);
				}
				else
				{
					if(task.Status != (int)Status.Activity)
					{
						historyList.Add(task);
					}
				}
			}

			taskListGrid.ShowGrid();
			historyListGrid.ShowGrid();
		}

		public TaskAndHistoryList()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

			this.readMe3.MouseEnter += new EventHandler(activitiesFloater_MouseEnter);
			this.readMe3.MouseLeave += new EventHandler(activitiesFloater_MouseLeave);

			//init task grid
			taskListGrid = new ObjectPresentation.GridEditObjects(taskList, typeof(Task), "TaskPending.xml");

			taskListGrid.Location = taskListLocation.Location;
			taskListGrid.Size = taskListLocation.Size;

			taskListGrid.grid.BackColor =  System.Drawing.Color.FromArgb(((System.Byte)(210)), ((System.Byte)(216)), ((System.Byte)(240)));

			taskListGrid.Editable = false;

			taskListGrid.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top;
			//taskListGrid.Anchor = AnchorStyles.Left | AnchorStyles.Right;

			Static.FillTaskListWithTaskTypes(taskListGrid);

			taskListGrid.doubleClickEventHandler = new EventHandler(taskListDoubleClick);

			taskListGrid.ShowGrid();

			this.Controls.Add(taskListGrid);

			//init history grid
			historyListGrid = new ObjectPresentation.GridEditObjects(historyList, typeof(Task), "TaskHistory.xml");

			historyListGrid.Location = historyListLocation.Location;
			historyListGrid.Size = historyListLocation.Size;

			historyListGrid.grid.BackColor =  System.Drawing.Color.FromArgb(((System.Byte)(210)), ((System.Byte)(216)), ((System.Byte)(240)));

			historyListGrid.Editable = false;

//			historyListGrid.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top;
			historyListGrid.Anchor = AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Bottom;

			Static.FillTaskListWithTaskTypes(historyListGrid);

			historyListGrid.doubleClickEventHandler = new EventHandler(historyListDoubleClick);

			historyListGrid.ShowGrid();

			this.Controls.Add(historyListGrid);

			//sasa, 20.11.05: nw, event handler
			Network.NetworkUDP.ChangedTask += new DealMaker.Network.NetworkChangedEventHandler(NetworkUDP_ChangedTask);

			//ta, 07.Aug.2008: Apply/Force Marketing Campaign
			SkipTracedContactAssistant.ACTIVITY_ASSIGNED_BY_SKIPTRACED_CONTACT_ASSISTANT += new DealMaker.PlainClasses.ReturnMailWizard.SkipTracedContactAssistant.ActivityAssignedBySkipTracedContactAssistant(On_ACTIVITY_ASSIGNED_BY_SKIPTRACED_CONTACT_ASSISTANT);
		}

		private void historyListDoubleClick(object sender, System.EventArgs e)
		{
			Task task = (Task)historyListGrid.SelectedObject;
			
			if(task != null)
			{
				AssignTaskStep2 form2 = new AssignTaskStep2();

				form2.TaskType = (TaskType)task.Type;
				form2.Task = task;
				form2.ShowDate = true;

				//todo make not editable
				foreach(Control control in form2.Controls)
				{
					if(!(control is Label || control.Name == "cancelButton"))
						control.Enabled = false;
/*					{
						if(control is TextBox)
						{
							((TextBox)control).ReadOnly = true;
						}
						else
						{
							control.Enabled = false;
						}
					}
*/				}

				form2.ShowDialog();
			}
		}

		private void taskListDoubleClick(object sender, System.EventArgs e)
		{
			this.EditTask_Click(this, null);
		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TaskAndHistoryList));
            this.taskListLocation = new System.Windows.Forms.Label();
            this.historyListLocation = new System.Windows.Forms.Label();
            this.AssignTask = new System.Windows.Forms.Button();
            this.AssignActivity = new System.Windows.Forms.Button();
            this.DeleteTask = new System.Windows.Forms.Button();
            this.EditTask = new System.Windows.Forms.Button();
            this.DeletePastAction = new System.Windows.Forms.Button();
            this.RecordPastAction = new System.Windows.Forms.Button();
            this.taskListFor = new System.Windows.Forms.Label();
            this.historyListFor = new System.Windows.Forms.Label();
            this.readMe3 = new DealMaker.ReadMe();
            this.DoneTask = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // taskListLocation
            // 
            this.taskListLocation.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.taskListLocation.Location = new System.Drawing.Point(8, 16);
            this.taskListLocation.Name = "taskListLocation";
            this.taskListLocation.Size = new System.Drawing.Size(328, 128);
            this.taskListLocation.TabIndex = 0;
            this.taskListLocation.Text = "taskListLocation";
            this.taskListLocation.Visible = false;
            // 
            // historyListLocation
            // 
            this.historyListLocation.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.historyListLocation.Location = new System.Drawing.Point(8, 194);
            this.historyListLocation.Name = "historyListLocation";
            this.historyListLocation.Size = new System.Drawing.Size(344, 116);
            this.historyListLocation.TabIndex = 1;
            this.historyListLocation.Text = "historyListLocation";
            this.historyListLocation.Visible = false;
            // 
            // AssignTask
            // 
            this.AssignTask.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.AssignTask.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.AssignTask.Location = new System.Drawing.Point(4, 149);
            this.AssignTask.Name = "AssignTask";
            this.AssignTask.Size = new System.Drawing.Size(64, 32);
            this.AssignTask.TabIndex = 2;
            this.AssignTask.Text = "Assign Task";
            this.AssignTask.Click += new System.EventHandler(this.AssignTask_Click);
            // 
            // AssignActivity
            // 
            this.AssignActivity.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.AssignActivity.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.AssignActivity.Location = new System.Drawing.Point(76, 149);
            this.AssignActivity.Name = "AssignActivity";
            this.AssignActivity.Size = new System.Drawing.Size(64, 32);
            this.AssignActivity.TabIndex = 3;
            this.AssignActivity.Text = "Assign Activity";
            this.AssignActivity.Click += new System.EventHandler(this.AssignActivity_Click);
            // 
            // DeleteTask
            // 
            this.DeleteTask.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.DeleteTask.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.DeleteTask.Location = new System.Drawing.Point(148, 149);
            this.DeleteTask.Name = "DeleteTask";
            this.DeleteTask.Size = new System.Drawing.Size(64, 32);
            this.DeleteTask.TabIndex = 4;
            this.DeleteTask.Text = "Delete Task";
            this.DeleteTask.Click += new System.EventHandler(this.DeleteTask_Click);
            // 
            // EditTask
            // 
            this.EditTask.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.EditTask.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.EditTask.Location = new System.Drawing.Point(220, 149);
            this.EditTask.Name = "EditTask";
            this.EditTask.Size = new System.Drawing.Size(64, 32);
            this.EditTask.TabIndex = 5;
            this.EditTask.Text = "Edit Task";
            this.EditTask.Click += new System.EventHandler(this.EditTask_Click);
            // 
            // DeletePastAction
            // 
            this.DeletePastAction.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.DeletePastAction.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.DeletePastAction.Location = new System.Drawing.Point(184, 314);
            this.DeletePastAction.Name = "DeletePastAction";
            this.DeletePastAction.Size = new System.Drawing.Size(84, 32);
            this.DeletePastAction.TabIndex = 7;
            this.DeletePastAction.Text = "Delete Past Action";
            this.DeletePastAction.Click += new System.EventHandler(this.DeletePastAction_Click);
            // 
            // RecordPastAction
            // 
            this.RecordPastAction.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.RecordPastAction.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.RecordPastAction.Location = new System.Drawing.Point(80, 314);
            this.RecordPastAction.Name = "RecordPastAction";
            this.RecordPastAction.Size = new System.Drawing.Size(84, 32);
            this.RecordPastAction.TabIndex = 6;
            this.RecordPastAction.Text = "Record Past Action";
            this.RecordPastAction.Click += new System.EventHandler(this.RecordPastAction_Click);
            // 
            // taskListFor
            // 
            this.taskListFor.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.taskListFor.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.taskListFor.Location = new System.Drawing.Point(8, 15);
            this.taskListFor.Name = "taskListFor";
            this.taskListFor.Size = new System.Drawing.Size(328, 32);
            this.taskListFor.TabIndex = 8;
            this.taskListFor.Text = "Task list for:";
            // 
            // historyListFor
            // 
            this.historyListFor.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.historyListFor.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.historyListFor.Location = new System.Drawing.Point(8, 195);
            this.historyListFor.Name = "historyListFor";
            this.historyListFor.Size = new System.Drawing.Size(344, 32);
            this.historyListFor.TabIndex = 9;
            this.historyListFor.Text = "History list for:";
            // 
            // readMe3
            // 
            this.readMe3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.readMe3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.readMe3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("readMe3.BackgroundImage")));
            this.readMe3.Location = new System.Drawing.Point(336, 8);
            this.readMe3.Name = "readMe3";
            this.readMe3.Size = new System.Drawing.Size(16, 16);
            this.readMe3.TabIndex = 79;
            // 
            // DoneTask
            // 
            this.DoneTask.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.DoneTask.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.DoneTask.Location = new System.Drawing.Point(291, 149);
            this.DoneTask.Name = "DoneTask";
            this.DoneTask.Size = new System.Drawing.Size(64, 32);
            this.DoneTask.TabIndex = 80;
            this.DoneTask.Text = "Done Task";
            this.DoneTask.Click += new System.EventHandler(this.DoneTask_Click);
            // 
            // TaskAndHistoryList
            // 
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.Controls.Add(this.DoneTask);
            this.Controls.Add(this.readMe3);
            this.Controls.Add(this.historyListFor);
            this.Controls.Add(this.taskListFor);
            this.Controls.Add(this.DeletePastAction);
            this.Controls.Add(this.RecordPastAction);
            this.Controls.Add(this.EditTask);
            this.Controls.Add(this.DeleteTask);
            this.Controls.Add(this.AssignActivity);
            this.Controls.Add(this.AssignTask);
            this.Controls.Add(this.historyListLocation);
            this.Controls.Add(this.taskListLocation);
            this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.Name = "TaskAndHistoryList";
            this.Size = new System.Drawing.Size(360, 352);
            this.Load += new System.EventHandler(this.TaskAndHistoryList_Load);
            this.ResumeLayout(false);

		}
		#endregion

		private void TaskAndHistoryList_Load(object sender, System.EventArgs e)
		{
		
		}

		private bool showAttention()
		{
			if((group == null && user == null) || (group != null && group.nodeId == 1))
			{
				MessageBox.Show("You are not allowed to Assign an Activity or Task to ALL GROUPS!\n                     Select one of the other groups!","Not Allowed!");
				return false;
			}

			if(group != null)
			{
				int numberOfContacts = ApplicationHelper.GetPropertyCountInNode(group.nodeId);
	
/*				string assignError = 
				"                                You are assigning an activity/task to a Group of "+numberOfContacts.ToString()+" contacts!\n"+
				"                                                 Do you want to proceed?\n\n"+
				//				"Yes    No\n"+
				"Warning! The activity/task will NOT be assigned to any Subgroup(s), if your Group has Subgroup(s)!\n";
*/
				string assignError = "You are assigning an Activity/Task to a group of "+numberOfContacts.ToString()+" contacts!\n\nAre you sure you want to proceed?\n\n(An Activity or a Task will not be assigned to any Subgroups!)";
	
				if(MessageBox.Show(assignError, "Attention!", MessageBoxButtons.YesNo) == DialogResult.No)
				{
					return false;
				}
			}

			return true;
		}

		private void AssignTask_Click(object sender, System.EventArgs e)
		{
			if(!this.showAttention()) return;

			Task task = new Task();

			task.Status = (int)Status.NotAssigned;

			task.MoveToBuisnessDay = true;

			AssignTaskStep1 form = new AssignTaskStep1(true);
			form.task = task;

			task.EventDate = DateTime.Today;
			
			if(group != null) form.Group = group;
			if(user != null) form.User = User;

			form.ShowDialog();

			if(form.DialogResult == DialogResult.OK)
			{
				task.Status = (int)Status.Pending;

				if(group != null)
				{
					task.Status = (int)Status.Pending;
					Static.AssignTaskToGroup(task, group);
				}

				if(user != null)
				{
					task.Status = (int)Status.Pending;
					Static.AssignTaskToUser(task, user);
				}
			}
			FillTaskList();
		}

		private void AssignActivity_Click(object sender, System.EventArgs e)
		{
			if(!this.showAttention()) return;

			AssignActivity form = new AssignActivity();
			
			if(group != null) form.Group = group;
			if(user != null) form.User = user;

			form.ShowDialog();

			if(form.DialogResult == DialogResult.OK)
			{
				//todo: make activity entry function to lower database roundtrips
				if(group != null)
				{
					foreach(Task task in form.Activity.Task)
					{
						task.Status = (int)Status.Pending;
						Static.AssignTaskToGroup(task, group);
					}
				}

				if(user != null)
				{
					foreach(Task task in form.Activity.Task)
					{
						task.Status = (int)Status.Pending;
						Static.AssignTaskToUser(task, user);
					}
				}

			}
			FillTaskList();		
		}

		private void DeleteTask_Click(object sender, System.EventArgs e)
		{
			if(taskListGrid.Selection.Count > 0)
			//if(taskListGrid.SelectedObject != null)
			{
				if(MessageBox.Show("Are you sure you want to delete "+taskListGrid.Selection.Count.ToString()+" task(s)?", "Attention", MessageBoxButtons.YesNo) == DialogResult.Yes)
				{
					if(group != null)
					{
						foreach(Task task in taskListGrid.Selection)
						{
							//Static.DeleteTask((Task)(taskListGrid.SelectedObject));
							Static.DeleteTask((Task)task);
						}
					}

					if(user != null)
					{
//						Static.DeleteUserTask((Task)(taskListGrid.SelectedObject), user);
						foreach(Task task in taskListGrid.Selection)
						{
							Static.DeleteUserTask((Task)task, user);
						}
					}

					FillTaskList();
				}
			}
			else
			{
				MessageBox.Show("Select the Task you want to Delete!");
			}
		}

		private void EditTask_Click(object sender, System.EventArgs e)
		{
			Task task = (Task)taskListGrid.SelectedObject;
			
			if(task != null)
			{
				AssignTaskStep2 form2 = new AssignTaskStep2();
                
				form2.TaskType = (TaskType)task.Type;
				form2.Task = task;
                
				//TA++ 21.07.2005
				//RQ
				if (Guid.Empty != task.IdPropertyItem)
				{
					form2.User = PropertyItem.LoadPropertyItem(task.IdPropertyItem);				
				}
				form2.PhoneNumber = task.PhoneNumber;
				//TA--

				form2.ShowDate = true;
				form2.AssignButtonText = "Update";

                form2.SetEditMode();
                form2.SetEditFlag();
				form2.ShowDialog();

				if(form2.DialogResult == DialogResult.OK)
				{
					if(group != null)
					{
						Static.UpdateGroupTask(task);
					}

					if(user != null)
					{
						Static.UpdateUserTask(task, user);
					}
				}
				form2.Dispose();
			}
			else
			{
				MessageBox.Show("Select the Task you want to Edit!");
			}		
			FillTaskList();
		}

		private void RecordPastAction_Click(object sender, System.EventArgs e)
		{
			if(!this.showAttention()) return;
//			QueryBox query = new QueryBox();

//			query.Text = "Enter Past Action Description:";

//			query.ShowDialog();

			RecordPastAction query = new RecordPastAction();
			query.ShowDialog();

			if(query.DialogResult == DialogResult.OK)
			{
				Task task = new Task();

				//				task.Description = query.Input;

				FormPropertyObject.FillObject(query, task);

				task.EventDate = task.TaskDate = task.TaskDate.Date;
				
				task.Status = (int)Status.Done;
				task.Type = (int)TaskType.PastAction;

				if(group != null)
				{
					Static.AssignTaskToGroup(task, group);
				}

				if(user != null)
				{
					Static.AssignTaskToUser(task, user);
				}
			}
			FillTaskList();
		}

		private void DeletePastAction_Click(object sender, System.EventArgs e)
		{
			Task task = (Task)historyListGrid.SelectedObject;

			if(task == null)
			{
				MessageBox.Show("Please select past action first!");
				return;
			}

			if(task.Type != (int)TaskType.PastAction)
			{
				MessageBox.Show("Only past action can be deleted!");
				return;
			}

			if(MessageBox.Show("Are you sure you want to delete past action?", "Attention", MessageBoxButtons.YesNo) == DialogResult.Yes)
			{
				if(task != null)
				{
					if(group != null)
					{
						Static.DeleteTask(task);
					}

					if(user != null)
					{
						Static.DeleteUserTask(task, user);
					}

					FillTaskList();
				}
				else
				{
					MessageBox.Show("Select the Task you want to Delete!");
				}
			}
			FillTaskList();
		}

		private void activitiesFloater_MouseEnter(object sender, EventArgs e)
		{
			this.activitiesFloater = new DealMaker.Forms.Floaters.ActivitiesHistory();


			Point pReadMe = this.readMe3.Location;
			Size fSize = this.activitiesFloater.Size;
						
			//this.floater.Location = new Point(pReadMe.X - fSize.Width, pReadMe.Y + this.readMe1.Height);
			this.activitiesFloater.Location = PointToScreen(new Point (pReadMe.X - fSize.Width, pReadMe.Y - 30) );

			this.activitiesFloater.Show();
		}

		private void activitiesFloater_MouseLeave(object sender, EventArgs e)
		{
			this.activitiesFloater.Hide();
		}

		private void On_ACTIVITY_ASSIGNED_BY_SKIPTRACED_CONTACT_ASSISTANT()
		{
			FillTaskList();
		}

        private void DoneTask_Click(object sender, EventArgs e)
        {
            bool doneShown = false;

            if (taskListGrid.Selection.Count > 0)
            {
                if (MessageBox.Show("Are you sure you want to permanently DONE selected Task(s)?", "Done Task?", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    foreach (Task task in taskListGrid.Selection)
                    {
                        if (task.Status == (int)Status.Canceled)
                        {
                            if (!doneShown)
                            {
                                MessageBox.Show("Canceled task can not be done!");
                                doneShown = true;
                            }
                        }
                        else
                        {
                            task.TaskDate = DateTime.Today;
                            task.Status = (int)Status.Done;

                            Static.UpdateTask(task);
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Please select task(s) first!");
                }

                FillTaskList();
            }
        }
	}
}
