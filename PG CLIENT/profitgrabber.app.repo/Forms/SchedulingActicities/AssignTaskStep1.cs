using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace DealMaker.Forms.SchedulingActicities
{
	/// <summary>
	/// Summary description for AssignTaskStep1.
	/// </summary>
	public class AssignTaskStep1 : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Label assignTaskLabel;
		private System.Windows.Forms.Label taskForLabel;
		private System.Windows.Forms.TextBox taskFor;
		private System.Windows.Forms.Label selectTaskLabel;
		private System.Windows.Forms.Button cancelButton;
		private System.Windows.Forms.Button nextButton;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
		private System.Windows.Forms.RadioButton Type1;
		private System.Windows.Forms.RadioButton Type2;
		private System.Windows.Forms.RadioButton Type3;
		private System.Windows.Forms.RadioButton Type4;

		public Task task;
		private bool showDate = true;
        bool _displayTaskDate = false;

		private PropertyItem user;
		public PropertyItem User
		{
			set
			{
				user = value;
				group = null;

				if(user != null && user.Owner !=null)
					taskFor.Text = user.Owner.FullName;
			}
			get
			{
				return user;
			}
		}

		private Node group;
		public Node Group
		{
			set
			{
				group = value;
				user = null;

				taskFor.Text = group.nodeName;
			}
			get
			{
				return group;
			}
		}

		public bool ShowDate
		{
			set
			{
				showDate = value;
			}
			get
			{
				return showDate;
			}
		}

		public AssignTaskStep1()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

        public AssignTaskStep1(bool displayTaskDate)
        {
            
            InitializeComponent();
            _displayTaskDate = displayTaskDate;
        }

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.assignTaskLabel = new System.Windows.Forms.Label();
            this.taskForLabel = new System.Windows.Forms.Label();
            this.taskFor = new System.Windows.Forms.TextBox();
            this.selectTaskLabel = new System.Windows.Forms.Label();
            this.Type1 = new System.Windows.Forms.RadioButton();
            this.Type2 = new System.Windows.Forms.RadioButton();
            this.Type3 = new System.Windows.Forms.RadioButton();
            this.cancelButton = new System.Windows.Forms.Button();
            this.nextButton = new System.Windows.Forms.Button();
            this.Type4 = new System.Windows.Forms.RadioButton();
            this.SuspendLayout();
            // 
            // assignTaskLabel
            // 
            this.assignTaskLabel.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.assignTaskLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.assignTaskLabel.Location = new System.Drawing.Point(16, 16);
            this.assignTaskLabel.Name = "assignTaskLabel";
            this.assignTaskLabel.Size = new System.Drawing.Size(120, 23);
            this.assignTaskLabel.TabIndex = 0;
            this.assignTaskLabel.Text = "Select TASK:";
            // 
            // taskForLabel
            // 
            this.taskForLabel.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.taskForLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.taskForLabel.Location = new System.Drawing.Point(24, 56);
            this.taskForLabel.Name = "taskForLabel";
            this.taskForLabel.Size = new System.Drawing.Size(64, 23);
            this.taskForLabel.TabIndex = 1;
            this.taskForLabel.Text = "TASK For:";
            // 
            // taskFor
            // 
            this.taskFor.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.taskFor.Location = new System.Drawing.Point(96, 56);
            this.taskFor.Name = "taskFor";
            this.taskFor.ReadOnly = true;
            this.taskFor.Size = new System.Drawing.Size(376, 20);
            this.taskFor.TabIndex = 2;
            // 
            // selectTaskLabel
            // 
            this.selectTaskLabel.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.selectTaskLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.selectTaskLabel.Location = new System.Drawing.Point(24, 104);
            this.selectTaskLabel.Name = "selectTaskLabel";
            this.selectTaskLabel.Size = new System.Drawing.Size(424, 23);
            this.selectTaskLabel.TabIndex = 3;
            this.selectTaskLabel.Text = "Select a TASK below that you want to schedule/assign for that contact/group:";
            // 
            // Type1
            // 
            this.Type1.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.Type1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Type1.Location = new System.Drawing.Point(32, 136);
            this.Type1.Name = "Type1";
            this.Type1.Size = new System.Drawing.Size(78, 24);
            this.Type1.TabIndex = 4;
            this.Type1.Text = "To Do";
            // 
            // Type2
            // 
            this.Type2.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.Type2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Type2.Location = new System.Drawing.Point(133, 136);
            this.Type2.Name = "Type2";
            this.Type2.Size = new System.Drawing.Size(86, 24);
            this.Type2.TabIndex = 5;
            this.Type2.Text = "Call";
            // 
            // Type3
            // 
            this.Type3.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.Type3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Type3.Location = new System.Drawing.Point(225, 136);
            this.Type3.Name = "Type3";
            this.Type3.Size = new System.Drawing.Size(104, 24);
            this.Type3.TabIndex = 6;
            this.Type3.Text = "Mail / Email / Fax";
            // 
            // cancelButton
            // 
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.cancelButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cancelButton.Location = new System.Drawing.Point(16, 176);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 23);
            this.cancelButton.TabIndex = 7;
            this.cancelButton.Text = "Cancel";
            // 
            // nextButton
            // 
            this.nextButton.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.nextButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nextButton.Location = new System.Drawing.Point(392, 176);
            this.nextButton.Name = "nextButton";
            this.nextButton.Size = new System.Drawing.Size(75, 23);
            this.nextButton.TabIndex = 8;
            this.nextButton.Text = "Next";
            this.nextButton.Click += new System.EventHandler(this.nextButton_Click);
            // 
            // Type4
            // 
            this.Type4.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.Type4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Type4.Location = new System.Drawing.Point(369, 136);
            this.Type4.Name = "Type4";
            this.Type4.Size = new System.Drawing.Size(88, 24);
            this.Type4.TabIndex = 9;
            this.Type4.Text = "Visit";
            // 
            // AssignTaskStep1
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.ClientSize = new System.Drawing.Size(480, 215);
            this.Controls.Add(this.Type4);
            this.Controls.Add(this.nextButton);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.Type3);
            this.Controls.Add(this.Type2);
            this.Controls.Add(this.Type1);
            this.Controls.Add(this.selectTaskLabel);
            this.Controls.Add(this.taskFor);
            this.Controls.Add(this.taskForLabel);
            this.Controls.Add(this.assignTaskLabel);
            this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Location = new System.Drawing.Point(110, 60);
            this.MaximizeBox = false;
            this.Name = "AssignTaskStep1";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Assign Task - Step 1";
            this.Load += new System.EventHandler(this.AssignTaskStep1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		#endregion

		private void AssignTaskStep1_Load(object sender, System.EventArgs e)
		{
			if(task != null)
				ObjectPresentation.FormPropertyObject.FillForm(this, task);
		}

		private void nextButton_Click(object sender, System.EventArgs e)
		{
			if(!(Type1.Checked | Type2.Checked | Type3.Checked | Type4.Checked))
			{
				MessageBox.Show("Please select Task type!");
				return;
			}

			if(task == null)
			{
				task = new Task();
				task.MoveToBuisnessDay = true;
			}

			AssignTaskStep2 form2 = new AssignTaskStep2();

			ObjectPresentation.FormPropertyObject.FillObject(this, task);

			form2.TaskType = (TaskType)task.Type;
			form2.Task = task;

			if(group != null) form2.Group = group;
			if(user != null) form2.User = User;

			form2.ShowDate = showDate;

			//form2.parent = this;

			if(task.Status != (int)Status.Activity)
				form2.ShowOutlookTaskEntryCheckBox = true;
			
			//to prevent flicker
			form2.Visible = false;

            if (_displayTaskDate)
                form2.SetEditMode();

			form2.ShowDialog();

			this.DialogResult = form2.DialogResult; 

			form2.Dispose();

			this.Close();
		}
	}
}
