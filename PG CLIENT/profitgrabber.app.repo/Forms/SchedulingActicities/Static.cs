using System;
using System.Data;
using System.Windows.Forms;
using NHibernate;
using NHibernate.Expression;
using System.Collections;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Data.SqlClient;
using System.Threading;

namespace DealMaker.Forms.SchedulingActicities
{
	/// <summary>
	/// Summary description for Static.
	/// </summary>
	public class Static
	{
		public Static()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		static public void TaskAteredAction()
		{
			pendingTasks = CheckPendingTasks();
		}

		static public void FillTaskListWithContactInfo(IList taskList)
		{
			NHibernate.ISession session = Globals.factory.OpenSession();

			try
			{
				if(taskList.Count > 0)
					foreach(Task task in taskList)
					{
						//add user info
						if(task.IdPropertyItem != Guid.Empty)
						{
							//todo add info
							//						session.CreateCriteria(Typeof(Contact)).Add(Expression.Eq("IdPropertyItem", task.IdPropertyItem)
							IList propertyItems = session.CreateCriteria(typeof(PropertyItem)).Add(Expression.Eq("IdPropertyItem", task.IdPropertyItem)).List();

							if(propertyItems != null && propertyItems.Count > 0)
							{
								PropertyItem user = (PropertyItem)propertyItems[0];

								if(user != null && user.Owner!=null && user.Owner.SiteAddress !=null)
									task.SiteAddress = user.Owner.SiteAddress.FullSiteStreetAddress;

								if(user != null && user.Owner!=null)
									task.ContactOrGroup = user.Owner.FullName.TrimEnd();
							}
						}

						//add group info
						if(task.IdNode != 0)
						{
							IList nodes = session.CreateCriteria(typeof(NodeNhibernete)).Add(Expression.Eq("nodeId", task.IdNode)).List();
					
							if(nodes != null && nodes.Count > 0)
							{
								NodeNhibernete group = (NodeNhibernete)nodes[0];

								task.ContactOrGroup = group.nodeName;
								task.SiteAddress = "Group";
							}
						}
					}
			}
			catch(Exception e)
			{
				MessageBox.Show("Unable to fill tasks!");
				throw e;
			}
			finally
			{
				session.Close();
			}
		}

		public static void AssignTaskToGroup(Task task, Node group)
		{
			task.IdTask = Guid.NewGuid();
			task.IdNode = group.nodeId;
			task.IdPropertyItem = Guid.Empty;

			NHibernate.ISession session = Globals.factory.OpenSession();

			try
			{

				NHibernate.ITransaction transaction = session.BeginTransaction();

				session.Save(task);

				transaction.Commit();

				//sasa, 13.11.05: nw broadcast udp packet changed pi
				Network.NetworkUDP.Broadcast(task);

			}
			catch(Exception e)
			{
				MessageBox.Show("Unable to persist object!");
				throw e;
			}
			finally
			{
				session.Close();
			}
			TaskAteredAction();
		}

		public static void AssignTaskToUser(Task task, PropertyItem item)
		{
			task.IdTask = Guid.NewGuid();
			task.IdNode = 0;
			task.IdPropertyItem = item.IdPropertyItem;

			NHibernate.ISession session = Globals.factory.OpenSession();

			try
			{
				NHibernate.ITransaction transaction = session.BeginTransaction();

				session.Save(task);

				transaction.Commit();

				//sasa, 13.11.05: nw broadcast udp packet changed pi
				Network.NetworkUDP.Broadcast(task);
			}
			catch(Exception e)
			{
				MessageBox.Show("Unable to persist object!");
				throw e;
			}
			finally
			{
				session.Close();
			}
			TaskAteredAction();
		}

		public static void UpdateTask(Task task)
		{
			NHibernate.ISession session = Globals.factory.OpenSession();

			try
			{
				NHibernate.ITransaction transaction = session.BeginTransaction();

				session.Update(task);

				transaction.Commit();

				//sasa, 13.11.05: nw broadcast udp packet changed pi
				Network.NetworkUDP.Broadcast(task);
			}
			catch(Exception e)
			{
				MessageBox.Show("Unable to update task!");
				throw e;
			}
			finally
			{
				session.Close();
			}
			TaskAteredAction();
		}

		public static void FillTaskListWithTaskTypes(ObjectPresentation.GridEditObjects taskListGrid)
		{
			ArrayList list = new ArrayList();

			for(int i=0;i<10;i++) list.Add("");

			list[0] = "Undefined";
			list[(int)TaskType.Call] = "Call";
			list[(int)TaskType.Mail] = "Mail / Fax";
			list[(int)TaskType.ToDo] = "ToDo";
			list[(int)TaskType.Visit] = "Visit";
			//			list[(int)TaskType.Fax] = "Fax";
			list[(int)TaskType.PastAction] = "PastAction";

			taskListGrid.AddIntList("Type", list);

			ArrayList status = new ArrayList();

			for(int i=0;i<10;i++) status.Add("");

			status[(int)Status.Pending] = "Pending";
			status[(int)Status.Done] = "Done";
			status[(int)Status.Canceled] = "Canceled";
			status[(int)Status.PastDo] = "Past Due";

			taskListGrid.AddIntList("Status", status);
		}

		public static IList GetTaskUsers(Task task)
		{
			ArrayList list = new ArrayList();

			if(task.IdPropertyItem != Guid.Empty)
			{
				list.Add(task.IdPropertyItem);
				return list;
			}

			if(task.IdNode != 0)
			{
				SqlDataReader nodeReader = null;
				try
				{
					//					SqlCommand cmd = new SqlCommand("GetTaskPropertyItems", Connection.getInstance().HatchConn);
					//					cmd.CommandType = CommandType.StoredProcedure;								
					//					cmd.Parameters.Add("@IdNode", task.IdNode);

					//Console.WriteLine(task.IdTask.ToString());

					//					String guid = " '{"+task.IdTask.ToString()+"}' ";

					string sqlQuery = 
					
						"SELECT     NodePropertyItems.IdPropertyItem " +
						"	FROM         NodePropertyItems INNER JOIN Task ON NodePropertyItems.NodeId = Task.IdNode " +
						"	WHERE     (Task.IdTask = '"+ task.IdTask.ToString() +"') AND (NodePropertyItems.IdPropertyItem NOT IN " +
						"			(SELECT     TaskExcludedPropertyItems.IdPropertyItem	FROM TaskExcludedPropertyItems " +
						"										                            WHERE TaskExcludedPropertyItems.IdTask = Task.IdTask)) ";

					SqlCommand cmd = new SqlCommand(sqlQuery, Connection.getInstance().HatchConn);

					if (ConnectionState.Open != Connection.getInstance().HatchConn.State)
						Connection.getInstance().HatchConn.Open();

					cmd.ExecuteNonQuery();

					nodeReader = cmd.ExecuteReader();

					while (nodeReader.Read())
					{
						Guid	propertyItemId		= nodeReader.GetGuid(0);
						list.Add(propertyItemId);	
					}
				}
				catch	(Exception exc)
				{
					MessageBox.Show(exc.Message);
				}
				finally
				{
					nodeReader.Close();				
				}
			}

			return list;
		}

		public static IList GetUserTasks(PropertyItem item)
		{
			IList taskList = null;

			NHibernate.ISession session = Globals.factory.OpenSession();
			
			try
			{
				//horror query to get task associated with user

				String guid = " '{"+item.IdPropertyItem.ToString()+"}' ";
			
				NHibernate.SqlCommand.SqlString queryString = new NHibernate.SqlCommand.SqlString(
					/*
										"	($alias.IdPropertyItem = "+guid+ " and $alias.IdNode = 0)" +
										"	or	" + 
										"	(	" + 
										"		$alias.IdNode in	" + 
										"			(select NodePropertyItems.NodeId from NodePropertyItems where	" + 
										"				NodePropertyItems.IdPropertyItem = " + guid +
										"			)	" + 
										"		and	" + 
										"		$alias.IdTask not in	" + 
										"			(select TaskExcludedPropertyItems.IdTask from TaskExcludedPropertyItems where	" + 
										"				TaskExcludedPropertyItems.IdTask = $alias.IdTask	" + 
										"				and	" + 
										"				TaskExcludedPropertyItems.IdPropertyItem = " + guid + 
										"			)	" + 
										"	 and $alias.IdNode <> 0 and $alias.IdProperty <> {"+Guid.Empty.ToString()+"})	"
					*/					
					"	($alias.IdPropertyItem = "+guid+ " and $alias.IdNode = 0)" +
					"	or	" + 
					"	(	" + 
					"		$alias.IdNode in	" + 
					"			(select NodePropertyItems.NodeId from NodePropertyItems where	" + 
					"				NodePropertyItems.IdPropertyItem = " + guid +
					"			)	" + 
					"		and	" + 
					"		$alias.IdTask not in	" + 
					"			(select TaskExcludedPropertyItems.IdTask from TaskExcludedPropertyItems where	" + 
					"				TaskExcludedPropertyItems.IdTask = $alias.IdTask	" + 
					"				and	" + 
					"				TaskExcludedPropertyItems.IdPropertyItem = " + guid + 
					"			)	" + 
					"	 and $alias.IdNode <> 0 )"
					);

				taskList = session.CreateCriteria(typeof(Task)).Add(Expression.Sql(queryString)).List();
			}
			catch(Exception e)
			{
				MessageBox.Show("Unable to load objects!");
				throw e;
			}
			finally
			{
				session.Close();
			}

			return taskList;
		}

		public static IList GetGroupTasks(Node node)
		{
			IList taskList = null;

			NHibernate.ISession session = Globals.factory.OpenSession();

			try
			{
				taskList = session.CreateCriteria(typeof(Task)).Add(Expression.Eq("IdNode", node.nodeId )).List();

			}
			catch(Exception e)
			{
				MessageBox.Show("Unable to load objects!");
				throw e;
			}
			finally
			{
				session.Close();
			}

			return taskList;
		}

		//sasa, 03.02.2006: purge old tasks new feature
		public static void PurgeOldTasks(DateTime date)
		{
			IList taskList = null;

			NHibernate.ISession session = Globals.factory.OpenSession();

			NHibernate.ITransaction transaction = session.BeginTransaction();
			try
			{
				taskList = session.CreateCriteria(typeof(Task)).Add(Expression.Le("TaskDate",date.AddDays(-1).Date)).Add(Expression.Or(Expression.Eq("Status",Status.Done),Expression.Eq("Status",Status.Canceled))).List();

				foreach(Task t in taskList)
				{
					session.Delete(t);
				}

				transaction.Commit();

				if(taskList.Count > 0)				
					Network.NetworkUDP.Broadcast((Task)taskList[0]); //hack, broadcast first task, just to avoid null in task id (which mean that packet is property id)
			}
			catch(Exception 
#if DEBUG
				e
#endif
				)
			{
				MessageBox.Show("Unable to load objects!");
#if DEBUG
				MessageBox.Show(e.ToString());
#endif
				transaction.Rollback();
//				throw e;
			}
			finally
			{
				session.Close();
			}
		}


		//assign new and add to excluded if both group and user id exists and task with that id non existant
		public static void UpdateUserTask(Task task, PropertyItem user)
		{

			NHibernate.ISession session = Globals.factory.OpenSession();

			try
			{
				NHibernate.ITransaction transaction = session.BeginTransaction();

				//check is task assigned to group
				if(task.IdNode != 0)
				{
					//					task.ExcludedPropertyItems.Add(user);
					//					IList tasksToAddEvictedUser = session.CreateCriteria(typeof(Task)).Add(Expression.Eq("IdTask", task.IdTask)).List();

					Task taskToAddEvictedUser = new Task();
					session.Load(taskToAddEvictedUser, task.IdTask); //= (Task)tasksToAddEvictedUser[0];

					taskToAddEvictedUser.ExcludedPropertyItems.Add(user);

					session.Update(taskToAddEvictedUser);
					
					session.Evict(task);
					//if(task.MailFaxWizard != null)
					//	session.Evict(task.MailFaxWizard);

					//					int IdNode = task.IdNode;

					//assign new task to individual user

					task.IdTask = Guid.Empty;
					task.IdNode = 0;
					task.ExcludedPropertyItems = null;
					task.IdPropertyItem = user.IdPropertyItem;
					if(task.MailFaxWizard != null)
					{
						session.Evict(task.MailFaxWizard);
						task.MailFaxWizard = (MailFaxWizard)session.Load(typeof(MailFaxWizard), task.MailFaxWizard.IdMailFaxWizard);
					}

					session.Save(task);

					//add old task to evict list
					/*
										IList evictedTasks = session.CreateCriteria(typeof(Task)).Add(Expression.Eq("IdNode", IdNode)).List();
					
										Task evictedTask = (Task)evictedTasks[0];

										//shoud be only one
										evictedTask.ExcludedPropertyItems.Add(user);

										session.Update(evictedTask);
					*/				}
				else
				{
					//if task is assigned only tu user, update it normally
					session.Update(task);
				}

				transaction.Commit();

				//sasa, 13.11.05: nw broadcast udp packet changed pi
				Network.NetworkUDP.Broadcast(task);
			}
			catch(Exception e)
			{
				MessageBox.Show("Unable to update object!");
				throw e;
			}
			finally
			{
				session.Close();
			}
			TaskAteredAction();
		}

		public static void UpdateGroupTask(Task task)
		{
			NHibernate.ISession session = Globals.factory.OpenSession();

			try
			{
				NHibernate.ITransaction transaction = session.BeginTransaction();

				session.Update(task);

				transaction.Commit();

				//sasa, 13.11.05: nw broadcast udp packet changed pi
				Network.NetworkUDP.Broadcast(task);
			}
			catch(Exception e)
			{
				MessageBox.Show("Unable to update object!");
				throw e;
			}
			finally
			{
				session.Close();
			}
			TaskAteredAction();
		}

		public static void DeleteUserTask(Task task, PropertyItem user)
		{
			if(task.IdNode == 0)
			{
				//individual task
				DeleteTask(task);
			}
			else
			{
				//group task, add use
				
				NHibernate.ISession session = Globals.factory.OpenSession();

				try
				{
					NHibernate.ITransaction transaction = session.BeginTransaction();

					session.Load(task, task.IdTask);

					task.ExcludedPropertyItems.Add(user);

					session.Update(task);

					transaction.Commit();

					//sasa, 13.11.05: nw broadcast udp packet changed pi
					Network.NetworkUDP.Broadcast(task);
				}
				catch(Exception e)
				{
					MessageBox.Show("Unable to delete object!");
					throw e;
				}
				finally
				{
					session.Close();
				}
			}
			TaskAteredAction();
		}

		public static void DeleteTask(Task task)
		{
			NHibernate.ISession session = Globals.factory.OpenSession();

			try
			{
				NHibernate.ITransaction transaction = session.BeginTransaction();

				session.Delete(task);

				transaction.Commit();

			}
			catch(Exception e)
			{
				MessageBox.Show("Unable to delete object!");
				throw e;
			}
			finally
			{
				session.Close();
			}
			TaskAteredAction();
		}

		public static void FillCreatedCombo(ComboBox combo)
		{
			//User u = ApplicationHelper.LoadUser();

            //if (null != u)
            {
                //string userName = u.FirstName + " " + u.LastName;

                //combo.Items.Add(userName);
                //combo.Items.Add("Buying Assistant");
                //combo.Items.Add("Pro Buyer");
                //combo.Items.Add("Selling Assistant");

                Dictionary<Guid, string> dict = UserInformationManager.Instance.GetValues(UserEntries.OfficeStaffList);
                if (null != dict)
                {
                    Dictionary<Guid, string>.Enumerator enumerator = dict.GetEnumerator();
                    while (enumerator.MoveNext())
                    {
                        combo.Items.Add(enumerator.Current.Value);
                    }
                }
            }
            //else
            //{
                //MessageBox.Show("Please go to File -> User Information and confirm Your user information data!", "User Information Confirmation needed!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            //}            			
		}

        public static void FillTask2EmailCombo(ComboBox combo)
        {
            try
            {
                combo.Items.Clear();
                combo.Text = string.Empty;
            }
            catch { }

            //User u = ApplicationHelper.LoadUser();

            //if (null != u)
            {
                //string userName = u.FirstName + " " + u.LastName;                

                Dictionary<Guid, string> dict = UserInformationManager.Instance.GetOfficeStaffWithEmailAddress();
                if (null != dict)
                {
                    if (dict.Count > 0)
                    {
                        //combo.Items.Add("-SELECT-");                        
                    }

                    Dictionary<Guid, string>.Enumerator enumerator = dict.GetEnumerator();
                    while (enumerator.MoveNext())
                    {
                        combo.Items.Add(enumerator.Current.Value);
                    }

                    //if (null != combo.Items && combo.Items.Count > 0)
                    //    combo.SelectedIndex = 0;
                }
            }
            //else
            //{
                //MessageBox.Show("Please go to File -> User Information and confirm Your user information data!", "User Information Confirmation needed!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            //}
        }

        public static Expression taskFilterAll = (Expression.Or(Expression.IsNull("CreatedFor"), Expression.IsNotNull("CreatedFor"))); //tautology, missing from expression
        public static Expression taskFilter = taskFilterAll;

		public static bool CheckPendingTasks()
		{
			NHibernate.ISession session = Globals.factory.OpenSession();

			bool result = 
				session.CreateCriteria(typeof(Task)).
				Add(Expression.Between("TaskDate", System.Data.SqlTypes.SqlDateTime.MinValue.Value, DateTime.Today.Date)).SetMaxResults(1).
				Add(Expression.Eq("Status",(int)Status.Pending)).
                Add(taskFilter).
				List().Count > 0;			

			session.Close();

			return result;
		}

		public static bool PendingTasks
		{
			get
			{
				return pendingTasks;
			}
		}

		static bool pendingTasks;

		public static void CheckPendingTasksThread_ThreadProc()
		{

			while(true)
			{
				try
				{
					pendingTasks = CheckPendingTasks();
					Thread.Sleep(Globals.CheckPendingTasksThread_MsecTimeout);
				}
				catch (System.Threading.ThreadAbortException)
				{
					// do nothing - we are terminating the thread because of app exit !
				}
				catch(Exception 
#if DEBUG
					ee
#endif
					)
				{
#if DEBUG
					MessageBox.Show(ee.ToString());
#endif
				}
			}
		}
		
		public static void StartCheckPendingTasksThread()
		{
			//Thread t = new Thread(new ThreadStart(CheckPendingTasksThread_ThreadProc));
			//t.Start();

			Globals.taskManagerChecker = new Thread(new ThreadStart(CheckPendingTasksThread_ThreadProc));
			Globals.taskManagerChecker.Start();
		}

		/*
				public static Task InputTask(Task task, bool ShowDate)
				{
					AssignTaskStep1 form = new AssignTaskStep1();
					form.task = task;
					form.ShowDialog();
					if(form.DialogResult == DialogResult.OK)
					{
						return form.task;
					}
					else
					{
						return null;
					}
				}
		*/	}
}
