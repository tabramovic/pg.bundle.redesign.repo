using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;
using NHibernate;
using NHibernate.Expression;

namespace DealMaker.Forms.SchedulingActicities
{
	/// <summary>
	/// Summary description for TaskManager.
	/// </summary>
	public class TaskManager : System.Windows.Forms.UserControl
	{
		private System.Windows.Forms.Button selectToday;
		private System.Windows.Forms.Button selectLast7;
		private System.Windows.Forms.MonthCalendar calendar;
		private System.Windows.Forms.CheckBox pending;
		private System.Windows.Forms.CheckBox done;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.CheckBox canceled;
		private System.Windows.Forms.Label taskListPosition;
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		private ObjectPresentation.GridEditObjects taskListGrid;
		private System.Windows.Forms.Button cancelTask;
		private System.Windows.Forms.Button doneTask;
		private System.Windows.Forms.Button selectAll;
		private System.Windows.Forms.GroupBox groupBox2;
		private ArrayList taskList = new ArrayList();

		private bool selectAllDates = false;

		private bool ignoreCalendarChange = false;
		private System.Windows.Forms.Button printFax;
		private System.Windows.Forms.CheckBox pastDo;
		private System.Windows.Forms.Button reactivateTask;
        private GroupBox groupBox3;
        private ComboBox showStaff3;
        private ComboBox showStaff2;
        private ComboBox showStaff1;
        private RadioButton showSelected;
        private RadioButton showAll;
        private Button _openContact;
        private GroupBox groupBox4;
        private Button _sendTasks;
        private ComboBox _staffWithEmail;
        private Label label1;
		private bool calendarClicked = false;

		#region � Network UDP �
		//sasa, 20.11.05: nw version, invoke required (refresh thread in different task)
		private delegate void UpdateDelegate();

		private void UpdateDelegateFunc()
		{
			loadTasks();
		}
		
		private void NetworkUDP_ChangedTask(Network.NetworkUDP.Packet p)
		{
			Globals.MainForm.BeginInvoke(new UpdateDelegate(UpdateDelegateFunc));
		}
		#endregion � Network UDP �

        public void ReloadAssignees()
        {
            Static.FillTask2EmailCombo(_staffWithEmail);
        }

		public void loadTasks()
		{
			IList list = null;

			DateTime minDate = calendar.SelectionStart;
			DateTime maxDate = calendar.SelectionEnd;

			if(selectAllDates)
			{
				minDate = calendar.MinDate;
				maxDate = calendar.MaxDate;
			}

			NHibernate.ISession session = Globals.factory.OpenSession();

			try
			{
				//list = (IList)session.CreateCriteria(typeof(Task))./*Add(Expression.Eq("IdNode", node.nodeId )).*/List();

				taskList.Clear();

                Expression filterTask =
                    Expression.Or(Expression.Or(
                                    Expression.Eq("CreatedFor", showStaff1.Text),
                                    Expression.Eq("CreatedFor", showStaff2.Text)),
                                  Expression.Or(
                                    Expression.Eq("CreatedFor", showStaff3.Text),
                                    Expression.Eq("CreatedFor", "")
                                    ));

                Expression filter = ShowTasksForAll ? (Static.taskFilter = Static.taskFilterAll):
                                                      (Static.taskFilter = filterTask);

				if(canceled.Checked)
				{
                    list = (IList)session.CreateCriteria(typeof(Task)).
                        Add(Expression.Between("TaskDate", minDate, maxDate)).
                        Add(Expression.Eq("Status", (int)Status.Canceled)).
                        Add(filter).
                        List();

					if(list != null && list.Count > 0)
						taskList.AddRange(list);
				}

				if(done.Checked)
				{
					list = (IList)session.CreateCriteria(typeof(Task)).
						Add(Expression.Between("TaskDate", minDate, maxDate)).
						Add(Expression.Eq("Status",(int)Status.Done)).
                        Add(filter).
						List();

					if(list != null && list.Count > 0)
						taskList.AddRange(list);
				}

				if(pastDo.Checked || pending.Checked)
				{                    
					list = (IList)session.CreateCriteria(typeof(Task)).
						Add(Expression.Between("TaskDate", minDate, maxDate)).
						Add(Expression.Eq("Status",(int)Status.Pending)).
                        Add(filter).
						List();
                    
                    /*
                    list = (IList)session.CreateCriteria(typeof(Task)).
                        Add(Expression.Between("TaskDate", minDate, maxDate)).
                        Add(Expression.Or(Expression.Eq("Status", (int)Status.Pending), Expression.Eq("Status", (int)Status.PastDo))).                        
                        Add(filter).
                        List();
                    */

					if(list != null && list.Count > 0)
					{
						foreach(object obj in list)
						{
							session.Evict(obj);
						}

					again:
						foreach(Task task in list)
						{
							if(task.TaskDate < DateTime.Today)
							{
								task.Status = (int)Status.PastDo;
							}

							if(!pastDo.Checked && task.Status == (int)Status.PastDo)
							{
								list.Remove(task);
								goto again;
							}

							if(!pending.Checked && task.Status == (int)Status.Pending)
							{
								list.Remove(task);
								goto again;
							}
						}

						if(list.Count > 0)
							taskList.AddRange(list);
					}
				}

				/*
				if(taskList.Count > 0)
					foreach(Task task in taskList)
					{
						//add user info
						if(task.IdPropertyItem != Guid.Empty)
						{
							//todo add info
							//						session.CreateCriteria(Typeof(Contact)).Add(Expression.Eq("IdPropertyItem", task.IdPropertyItem)
							IList propertyItems = session.CreateCriteria(typeof(PropertyItem)).Add(Expression.Eq("IdPropertyItem", task.IdPropertyItem)).List();

							if(propertyItems != null && propertyItems.Count > 0)
							{
								PropertyItem user = (PropertyItem)propertyItems[0];

								if(user != null && user.Owner!=null && user.Owner.SiteAddress !=null)
									task.SiteAddress = user.Owner.SiteAddress.FullSiteStreetAddress;

								if(user != null && user.Owner!=null)
									task.ContactOrGroup = user.Owner.FullName;
							}
						}

						//add group info
						if(task.IdNode != 0)
						{
							IList nodes = session.CreateCriteria(typeof(NodeNhibernete)).Add(Expression.Eq("nodeId", task.IdNode)).List();
							
							if(nodes != null && nodes.Count > 0)
							{
								NodeNhibernete group = (NodeNhibernete)nodes[0];

								task.ContactOrGroup = group.nodeName;
								task.SiteAddress = "Group";
							}
						}
					}
*/
				Static.FillTaskListWithContactInfo(taskList);			
			}
			catch(Exception e)
			{
				MessageBox.Show("Unable to load tasks!");
				throw e;
			}
			finally
			{
				session.Close();
			}
				//Selection[0].VisualProperties.BackColor = Color.

			taskListGrid.ShowGrid();

			//			taskListGrid.grid.w
			for(int y=1;y<taskListGrid.grid.Rows;y++)
				for(int x=1;x<taskListGrid.grid.Cols;x++)
				{
					Color color = Color.White;

					ObjectPresentation.GridEditObjects.StoredObject so = ((ObjectPresentation.GridEditObjects.StoredObject)(taskListGrid.grid[y, 4].Tag));

					if(so != null)
					{
						Task task = (Task)so.Object;
						if(task != null)
						{
							if(task.Status == (int)Status.Pending) color = Color.LightYellow;//Color.Yellow;
							if(task.Status == (int)Status.PastDo) color = Color.OrangeRed;//Color.Red;
							if(task.Status == (int)Status.Done) color = Color.SeaGreen;//Color.Green;
							if(task.Status == (int)Status.Canceled) color = Color.DarkGray;//Color.Gray;
/*							if(task.Status == (int)Status.Pending) color = Color.Yellow;
							if(task.Status == (int)Status.PastDo) color = Color.Red;
							if(task.Status == (int)Status.Done) color = Color.Green;
							if(task.Status == (int)Status.Canceled) color = Color.Gray;
*/						}
					}

					taskListGrid.grid[y,x].VisualProperties.BackColor = color;
				}

			if(selectAllDates)
			{
				DateTime cminDate = DateTime.Today;
				DateTime cmaxDate = DateTime.Today;

				if(taskList != null && taskList.Count > 0)
					foreach(Task task in taskList)
					{
						if(task.TaskDate > cmaxDate) cmaxDate = task.TaskDate;
						if(task.TaskDate < cminDate) cminDate = task.TaskDate;
					}

				calendar.SelectionStart = cminDate;
				calendar.SelectionEnd = cmaxDate;
			}
		}

        public IList loadTasksForEmail(string taskOwner)
        {
            IList list = null;
                        
            DateTime minDate = calendar.SelectionStart;
            DateTime maxDate = calendar.SelectionEnd;
            
            NHibernate.ISession session = Globals.factory.OpenSession();

            try
            {                
                taskList.Clear();
                Expression filterTask = Expression.Eq("CreatedFor", taskOwner);                                    
                
                list = (IList)session.CreateCriteria(typeof(Task)).
                    Add(Expression.Eq("Status", (int)Status.Pending)).                    
                    //Add(Expression.Gt("Status", (int)Status.Activity)).
                    //Add(Expression.Lt("Status", (int)Status.PastDo)).                    
                    Add(filterTask).
                    List();

                if (list != null && list.Count > 0)
                {
                    foreach (Task task in list)
                    {
                        if (task.Status == (int)Status.Done)
                            continue;

                        if (task.Status == (int)Status.Canceled)
                            continue;

                        if (task.TaskDate < DateTime.Today)                        
                            task.Status = (int)Status.PastDo;                        

                        if (task.Status == (int)Status.PastDo)
                            taskList.Add(task);

                        else if ((task.Status == (int)Status.Pending) && (task.TaskDate == DateTime.Now.Date))
                            taskList.Add(task);
                    }
                }
                                                                
                Static.FillTaskListWithContactInfo(taskList);
            }
            catch (Exception e)
            {
                MessageBox.Show("Unable to load tasks!");
                throw e;
            }
            finally
            {
                session.Close();
            }
            
            taskListGrid.ShowGrid();
            
            for (int y = 1; y < taskListGrid.grid.Rows; y++)
                for (int x = 1; x < taskListGrid.grid.Cols; x++)
                {
                    Color color = Color.White;

                    ObjectPresentation.GridEditObjects.StoredObject so = ((ObjectPresentation.GridEditObjects.StoredObject)(taskListGrid.grid[y, 4].Tag));

                    if (so != null)
                    {
                        Task task = (Task)so.Object;
                        if (task != null)
                        {
                            if (task.Status == (int)Status.Pending) color = Color.LightYellow;
                            if (task.Status == (int)Status.PastDo) color = Color.OrangeRed;
                            if (task.Status == (int)Status.Done) color = Color.SeaGreen;
                            if (task.Status == (int)Status.Canceled) color = Color.DarkGray;                            
                        }
                    }

                    taskListGrid.grid[y, x].VisualProperties.BackColor = color;
                }

            //return list;
            return taskList;
        }

		public TaskManager()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();
			
			taskListGrid = new ObjectPresentation.GridEditObjects(taskList, typeof(Task), "TaskTaskManager.xml");

			taskListGrid.grid.BackColor =  System.Drawing.Color.FromArgb(((System.Byte)(210)), ((System.Byte)(216)), ((System.Byte)(240)));

			taskListGrid.Editable = false;

			Static.FillTaskListWithTaskTypes(taskListGrid);

			taskListGrid.Location = taskListPosition.Location;
			taskListGrid.Size = taskListPosition.Size;

			taskListGrid.Anchor = AnchorStyles.Top | AnchorStyles.Right | AnchorStyles.Left | AnchorStyles.Bottom;

			taskListGrid.doubleClickEventHandler = new EventHandler(taskListDoubleClick);

//			taskListGrid.ShowGrid();
			loadTasks();

			this.Controls.Add(taskListGrid);

			//sasa, 20.11.05: nw, event handler
			Network.NetworkUDP.ChangedTask += new DealMaker.Network.NetworkChangedEventHandler(NetworkUDP_ChangedTask);

            _openContact.Click += new EventHandler(On_OpenContact_Click);
            _staffWithEmail.SelectedIndexChanged += new EventHandler(On_StaffWithEmail_SelectedIndexChanged);
            _sendTasks.Click += new EventHandler(On_SendTasks_Click);
		}                

        void On_OpenContact_Click(object sender, EventArgs e)
        {
            Hashtable htPropertyItems = new Hashtable();
            bool groupTaskFound = false;
            bool contactTaskFound = false;
    
            if (taskListGrid.Selection.Count > 0)
            {                                
                int i = 1;
                foreach (Task task in taskListGrid.Selection)
                {
                    if (Guid.Empty != task.IdPropertyItem && !htPropertyItems.ContainsValue(task.IdPropertyItem))
                    {
                        htPropertyItems.Add(i, task.IdPropertyItem);
                        contactTaskFound = true;
                    }

                    if (0 != task.IdNode)
                        groupTaskFound = true;
                }                
            }
            else
            {
                MessageBox.Show("Please select task(s) first!");
            }

            if (groupTaskFound && !contactTaskFound)
            {
                MessageBox.Show("Some of selected tasks is associated with a group." + Environment.NewLine + " To view Contact, please select a task associated with an individual contact.", "View Contact", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else if (groupTaskFound && contactTaskFound)
            {
                MessageBox.Show("Some of selected tasks is associated with a group. " + Environment.NewLine + "Contact view will display only contacts having assigned task associated with an individual contact.", "View Contact", MessageBoxButtons.OK, MessageBoxIcon.Information); 
            }


            if (htPropertyItems.Count > 0)
            {
                //sasa, 2006.08.27.: bugfix nw
                Globals.ContactScreenOnMainForm = true;

                if (null != Globals.oldContactScreen)
                {
                    //sasa, 20.11.05: hack, aaaghr, we must remove event handler from former contact screen
                    Network.NetworkUDP.ChangedPropertyItem -= Globals.oldContactScreen.networkEventHandler;
                    //sasa, 24.01.06: new nw lock
                    Network.NetworkLock.StatusChanged -= Globals.oldContactScreen.networkEventHandler2;
                }
                //~sasa

                ContactScreen cs = new ContactScreen(htPropertyItems);
                cs.Size = Globals.applicationSize;
                cs.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top;
                cs.Dock = DockStyle.Fill;
                Panel panel = (Panel)this.Parent;
                MainForm mainForm = (MainForm)panel.Parent;

                mainForm.appBodyPanel.Controls.RemoveAt(0);
                mainForm.appBodyPanel.Controls.Add(cs);

                //2006.08.27, sasa: nw lock, reload contact and acquire new lock if cs already on screen
                cs.ReloadContact();
            }
        }

		private void taskListDoubleClick(object sender, System.EventArgs e)
		{
			Task task = (Task)(taskListGrid.SelectedObject);

			if(task != null)
				if(task.Status == (int)Status.Pending || task.Status == (int)Status.PastDo)
			{
				AssignTaskStep2 form2 = new AssignTaskStep2();                

				form2.TaskType = (TaskType)task.Type;
				form2.Task = task;
				form2.ShowDate = true;
                form2.Priority = task.Priority;
                form2.SetEditFlag();
                form2.SetEditMode();

				//TA++ 21.07.2005
				//RQ
				if (Guid.Empty != task.IdPropertyItem)
				{
					form2.User = PropertyItem.LoadPropertyItem(task.IdPropertyItem);
				}
				form2.PhoneNumber = task.PhoneNumber;
				//TA--

				form2.done.Visible = true;

				form2.ShowDialog();

				if(form2.DialogResult == DialogResult.OK)
				{
					Static.UpdateTask(task);
					select_CheckedChanged(null, null);
//					taskListGrid.ShowGrid();
/*
					if(task.IdNode != 0)
					{
						Static.UpdateGroupTask(task);
						taskListGrid.ShowGrid();
					}

					if(task.IdPropertyItem != Guid.Empty)
					{
						PropertyItem pi = new PropertyItem();
						pi.IdPropertyItem = task.IdPropertyItem;

						Static.UpdateUserTask(task, pi);
						taskListGrid.ShowGrid();
					}
*/				}
			}
		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.selectLast7 = new System.Windows.Forms.Button();
            this.selectToday = new System.Windows.Forms.Button();
            this.calendar = new System.Windows.Forms.MonthCalendar();
            this.pending = new System.Windows.Forms.CheckBox();
            this.pastDo = new System.Windows.Forms.CheckBox();
            this.done = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.canceled = new System.Windows.Forms.CheckBox();
            this.taskListPosition = new System.Windows.Forms.Label();
            this.cancelTask = new System.Windows.Forms.Button();
            this.doneTask = new System.Windows.Forms.Button();
            this.printFax = new System.Windows.Forms.Button();
            this.selectAll = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.reactivateTask = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.showStaff3 = new System.Windows.Forms.ComboBox();
            this.showStaff2 = new System.Windows.Forms.ComboBox();
            this.showStaff1 = new System.Windows.Forms.ComboBox();
            this.showSelected = new System.Windows.Forms.RadioButton();
            this.showAll = new System.Windows.Forms.RadioButton();
            this._openContact = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this._sendTasks = new System.Windows.Forms.Button();
            this._staffWithEmail = new System.Windows.Forms.ComboBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // selectLast7
            // 
            this.selectLast7.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.selectLast7.Location = new System.Drawing.Point(15, 64);
            this.selectLast7.Name = "selectLast7";
            this.selectLast7.Size = new System.Drawing.Size(121, 32);
            this.selectLast7.TabIndex = 1;
            this.selectLast7.Text = "Select Last 7 Days";
            this.selectLast7.Click += new System.EventHandler(this.selectLast7_Click);
            // 
            // selectToday
            // 
            this.selectToday.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.selectToday.Location = new System.Drawing.Point(15, 104);
            this.selectToday.Name = "selectToday";
            this.selectToday.Size = new System.Drawing.Size(121, 32);
            this.selectToday.TabIndex = 2;
            this.selectToday.Text = "Select Today";
            this.selectToday.Click += new System.EventHandler(this.selectToday_Click);
            // 
            // calendar
            // 
            this.calendar.ForeColor = System.Drawing.Color.Black;
            this.calendar.Location = new System.Drawing.Point(8, 8);
            this.calendar.MaxDate = new System.DateTime(2030, 12, 31, 0, 0, 0, 0);
            this.calendar.MaxSelectionCount = 2000000000;
            this.calendar.MinDate = new System.DateTime(2019, 1, 1, 0, 0, 0, 0);
            this.calendar.Name = "calendar";
            this.calendar.TabIndex = 3;
            this.calendar.TitleBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.calendar.TitleForeColor = System.Drawing.Color.DeepSkyBlue;
            this.calendar.TrailingForeColor = System.Drawing.Color.Gray;
            this.calendar.DateChanged += new System.Windows.Forms.DateRangeEventHandler(this.calendar_DateChanged);
            this.calendar.MouseDown += new System.Windows.Forms.MouseEventHandler(this.calendar_MouseDown);
            // 
            // pending
            // 
            this.pending.Checked = true;
            this.pending.CheckState = System.Windows.Forms.CheckState.Checked;
            this.pending.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.pending.Location = new System.Drawing.Point(16, 16);
            this.pending.Name = "pending";
            this.pending.Size = new System.Drawing.Size(104, 32);
            this.pending.TabIndex = 4;
            this.pending.Text = "Pending";
            this.pending.CheckedChanged += new System.EventHandler(this.select_CheckedChanged);
            // 
            // pastDo
            // 
            this.pastDo.Checked = true;
            this.pastDo.CheckState = System.Windows.Forms.CheckState.Checked;
            this.pastDo.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.pastDo.Location = new System.Drawing.Point(16, 48);
            this.pastDo.Name = "pastDo";
            this.pastDo.Size = new System.Drawing.Size(104, 32);
            this.pastDo.TabIndex = 5;
            this.pastDo.Text = "Past Due";
            this.pastDo.CheckedChanged += new System.EventHandler(this.select_CheckedChanged);
            // 
            // done
            // 
            this.done.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.done.Location = new System.Drawing.Point(16, 80);
            this.done.Name = "done";
            this.done.Size = new System.Drawing.Size(104, 32);
            this.done.TabIndex = 6;
            this.done.Text = "Done";
            this.done.CheckedChanged += new System.EventHandler(this.select_CheckedChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.canceled);
            this.groupBox1.Controls.Add(this.pending);
            this.groupBox1.Controls.Add(this.pastDo);
            this.groupBox1.Controls.Add(this.done);
            this.groupBox1.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.groupBox1.Location = new System.Drawing.Point(444, 16);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(136, 152);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Show:";
            // 
            // canceled
            // 
            this.canceled.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.canceled.Location = new System.Drawing.Point(16, 112);
            this.canceled.Name = "canceled";
            this.canceled.Size = new System.Drawing.Size(104, 32);
            this.canceled.TabIndex = 7;
            this.canceled.Text = "Canceled";
            this.canceled.CheckedChanged += new System.EventHandler(this.select_CheckedChanged);
            // 
            // taskListPosition
            // 
            this.taskListPosition.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.taskListPosition.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.taskListPosition.Location = new System.Drawing.Point(8, 208);
            this.taskListPosition.Name = "taskListPosition";
            this.taskListPosition.Size = new System.Drawing.Size(902, 432);
            this.taskListPosition.TabIndex = 8;
            this.taskListPosition.Text = "taskListPosition";
            this.taskListPosition.Visible = false;
            // 
            // cancelTask
            // 
            this.cancelTask.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cancelTask.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.cancelTask.Location = new System.Drawing.Point(8, 651);
            this.cancelTask.Name = "cancelTask";
            this.cancelTask.Size = new System.Drawing.Size(104, 37);
            this.cancelTask.TabIndex = 9;
            this.cancelTask.Text = "Cancel Task";
            this.cancelTask.Click += new System.EventHandler(this.cancelTask_Click);
            // 
            // doneTask
            // 
            this.doneTask.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.doneTask.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.doneTask.Location = new System.Drawing.Point(120, 651);
            this.doneTask.Name = "doneTask";
            this.doneTask.Size = new System.Drawing.Size(104, 37);
            this.doneTask.TabIndex = 10;
            this.doneTask.Text = "Done Task";
            this.doneTask.Click += new System.EventHandler(this.doneTask_Click);
            // 
            // printFax
            // 
            this.printFax.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.printFax.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.printFax.Location = new System.Drawing.Point(808, 651);
            this.printFax.Name = "printFax";
            this.printFax.Size = new System.Drawing.Size(104, 37);
            this.printFax.TabIndex = 12;
            this.printFax.Text = "Print / Email / Fax";
            this.printFax.Click += new System.EventHandler(this.printFax_Click);
            // 
            // selectAll
            // 
            this.selectAll.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.selectAll.Location = new System.Drawing.Point(15, 24);
            this.selectAll.Name = "selectAll";
            this.selectAll.Size = new System.Drawing.Size(121, 32);
            this.selectAll.TabIndex = 13;
            this.selectAll.Text = "Select All";
            this.selectAll.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.selectToday);
            this.groupBox2.Controls.Add(this.selectAll);
            this.groupBox2.Controls.Add(this.selectLast7);
            this.groupBox2.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.groupBox2.Location = new System.Drawing.Point(290, 16);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(152, 152);
            this.groupBox2.TabIndex = 14;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Select:";
            // 
            // reactivateTask
            // 
            this.reactivateTask.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.reactivateTask.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.reactivateTask.Location = new System.Drawing.Point(232, 651);
            this.reactivateTask.Name = "reactivateTask";
            this.reactivateTask.Size = new System.Drawing.Size(104, 37);
            this.reactivateTask.TabIndex = 15;
            this.reactivateTask.Text = "Re-Activate Task";
            this.reactivateTask.Click += new System.EventHandler(this.reactivateTask_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.showStaff3);
            this.groupBox3.Controls.Add(this.showStaff2);
            this.groupBox3.Controls.Add(this.showStaff1);
            this.groupBox3.Controls.Add(this.showSelected);
            this.groupBox3.Controls.Add(this.showAll);
            this.groupBox3.Location = new System.Drawing.Point(582, 16);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(160, 152);
            this.groupBox3.TabIndex = 16;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Show Tasks For:";
            // 
            // showStaff3
            // 
            this.showStaff3.Enabled = false;
            this.showStaff3.FormattingEnabled = true;
            this.showStaff3.Location = new System.Drawing.Point(15, 118);
            this.showStaff3.Name = "showStaff3";
            this.showStaff3.Size = new System.Drawing.Size(129, 21);
            this.showStaff3.TabIndex = 4;
            this.showStaff3.SelectedIndexChanged += new System.EventHandler(this.showStaff3_SelectedIndexChanged);
            // 
            // showStaff2
            // 
            this.showStaff2.Enabled = false;
            this.showStaff2.FormattingEnabled = true;
            this.showStaff2.Location = new System.Drawing.Point(15, 86);
            this.showStaff2.Name = "showStaff2";
            this.showStaff2.Size = new System.Drawing.Size(129, 21);
            this.showStaff2.TabIndex = 3;
            this.showStaff2.SelectedIndexChanged += new System.EventHandler(this.showStaff2_SelectedIndexChanged);
            // 
            // showStaff1
            // 
            this.showStaff1.Enabled = false;
            this.showStaff1.FormattingEnabled = true;
            this.showStaff1.Location = new System.Drawing.Point(15, 54);
            this.showStaff1.Name = "showStaff1";
            this.showStaff1.Size = new System.Drawing.Size(129, 21);
            this.showStaff1.TabIndex = 2;
            this.showStaff1.SelectedIndexChanged += new System.EventHandler(this.showStaff1_SelectedIndexChanged);
            // 
            // showSelected
            // 
            this.showSelected.AutoSize = true;
            this.showSelected.Location = new System.Drawing.Point(85, 23);
            this.showSelected.Name = "showSelected";
            this.showSelected.Size = new System.Drawing.Size(67, 17);
            this.showSelected.TabIndex = 1;
            this.showSelected.Text = "Selected";
            this.showSelected.UseVisualStyleBackColor = true;
            this.showSelected.CheckedChanged += new System.EventHandler(this.showSelected_CheckedChanged);
            // 
            // showAll
            // 
            this.showAll.AutoSize = true;
            this.showAll.Checked = true;
            this.showAll.Location = new System.Drawing.Point(24, 24);
            this.showAll.Name = "showAll";
            this.showAll.Size = new System.Drawing.Size(36, 17);
            this.showAll.TabIndex = 0;
            this.showAll.TabStop = true;
            this.showAll.Text = "All";
            this.showAll.UseVisualStyleBackColor = true;
            this.showAll.CheckedChanged += new System.EventHandler(this.showAll_CheckedChanged);
            // 
            // _openContact
            // 
            this._openContact.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._openContact.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._openContact.Location = new System.Drawing.Point(698, 651);
            this._openContact.Name = "_openContact";
            this._openContact.Size = new System.Drawing.Size(104, 37);
            this._openContact.TabIndex = 17;
            this._openContact.Text = "Open Contact";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label1);
            this.groupBox4.Controls.Add(this._sendTasks);
            this.groupBox4.Controls.Add(this._staffWithEmail);
            this.groupBox4.Location = new System.Drawing.Point(745, 16);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(155, 152);
            this.groupBox4.TabIndex = 18;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "SendTasks via Email/Text";
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(8, 36);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(141, 43);
            this.label1.TabIndex = 5;
            this.label1.Text = "Send Todays and Past Due tasks to:";
            // 
            // _sendTasks
            // 
            this._sendTasks.Enabled = false;
            this._sendTasks.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._sendTasks.ForeColor = System.Drawing.Color.Black;
            this._sendTasks.Location = new System.Drawing.Point(11, 116);
            this._sendTasks.Name = "_sendTasks";
            this._sendTasks.Size = new System.Drawing.Size(129, 23);
            this._sendTasks.TabIndex = 4;
            this._sendTasks.Text = "Send Tasks";
            this._sendTasks.UseVisualStyleBackColor = true;
            // 
            // _staffWithEmail
            // 
            this._staffWithEmail.FormattingEnabled = true;
            this._staffWithEmail.Location = new System.Drawing.Point(11, 86);
            this._staffWithEmail.Name = "_staffWithEmail";
            this._staffWithEmail.Size = new System.Drawing.Size(129, 21);
            this._staffWithEmail.TabIndex = 3;
            // 
            // TaskManager
            // 
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this._openContact);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.reactivateTask);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.printFax);
            this.Controls.Add(this.doneTask);
            this.Controls.Add(this.cancelTask);
            this.Controls.Add(this.taskListPosition);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.calendar);
            this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.Name = "TaskManager";
            this.Size = new System.Drawing.Size(918, 704);
            this.Load += new System.EventHandler(this.TaskManager_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.ResumeLayout(false);

		}
		#endregion

		private void selectToday_Click(object sender, System.EventArgs e)
		{
            ResetEmailTasksGroupBox();
			selectAllDates = false;

            //BEGIN - force event handler in case when table shows tasks to send for specific user
            if (DateTime.Today == calendar.SelectionStart && DateTime.Today == calendar.SelectionEnd)
                calendar_DateChanged(null, null);
            //END
			
			calendar.SelectionStart = DateTime.Today;
			calendar.SelectionEnd = DateTime.Today;
		}

		private void selectLast7_Click(object sender, System.EventArgs e)
		{
            ResetEmailTasksGroupBox();
			selectAllDates = false;

            //BEGIN - force event handler in case when table shows tasks to send for specific user
            if (DateTime.Today.AddDays(-7) == calendar.SelectionStart && DateTime.Today == calendar.SelectionEnd)
                calendar_DateChanged(null, null);
            //END

			calendar.SelectionStart = DateTime.Today.AddDays(-7);
			calendar.SelectionEnd = DateTime.Today;

		}

		private void select_CheckedChanged(object sender, System.EventArgs e)
		{
            ResetEmailTasksGroupBox();
			//			MessageBox.Show(pending.Checked.ToString());
			if(!pending.Checked && !pastDo.Checked && !done.Checked && !canceled.Checked)
			{
				((CheckBox)sender).Checked = true;
			}
			loadTasks();
		}

		private void canceled_CheckedChanged(object sender, System.EventArgs e)
		{
		
		}

		private void TaskManager_Load(object sender, System.EventArgs e)
		{
            Static.FillCreatedCombo(showStaff1);
            Static.FillCreatedCombo(showStaff2);
            Static.FillCreatedCombo(showStaff3);

            Static.FillTask2EmailCombo(_staffWithEmail);
        }

		private void button1_Click(object sender, System.EventArgs e)
		{
		}

		private void calendar_DateChanged(object sender, System.Windows.Forms.DateRangeEventArgs e)
		{
			if(!ignoreCalendarChange)
			{
                ResetEmailTasksGroupBox();

				if(calendarClicked && selectAllDates)
				{
					selectAllDates = false;
					calendarClicked = false;

					ignoreCalendarChange = true;
					calendar.SelectionStart = calendar.SelectionEnd;
					Application.DoEvents();
					calendar.SelectionEnd = calendar.SelectionStart;
					ignoreCalendarChange = false;
				}

				loadTasks();
			}
		}

		private void cancelTask_Click(object sender, System.EventArgs e)
		{
            ResetEmailTasksGroupBox();

				bool doneShown = false;
				//			MessageBoxOptions.
				//Task task = (Task)taskListGrid.SelectedObject;

				//			if(task != null)
				if(taskListGrid.Selection.Count > 0)
				{
					if(MessageBox.Show("Are you sure you want to permanently CANCEL selected Task(s)?","Cancel Task?", MessageBoxButtons.YesNo) == DialogResult.Yes)
					{
						foreach(Task task in taskListGrid.Selection)
						{
							if(task.Status == (int)Status.Done)
							{
								//MessageBox.Show("This is done task and It can not be canceled!");
								if(!doneShown)
								{
									MessageBox.Show("Done task can not be canceled!");
									doneShown = true;
								}
							}
							else
							{
								task.Status = (int)Status.Canceled;
								Static.UpdateTask(task);
							}
						}
					}
				}
				else
				{
					MessageBox.Show("Please select task(s) first!");
				}

				loadTasks();
		}

		private void doneTask_Click(object sender, System.EventArgs e)
		{
            ResetEmailTasksGroupBox();

//			Task task = (Task)taskListGrid.SelectedObject;

//			if(task != null)
			bool doneShown = false;

			if(taskListGrid.Selection.Count > 0)
			{
				if(MessageBox.Show("Are you sure you want to permanently DONE selected Task(s)?","Done Task?", MessageBoxButtons.YesNo) == DialogResult.Yes)
				{
					foreach(Task task in taskListGrid.Selection)
					{
						if(task.Status == (int)Status.Canceled)
						{
							if(!doneShown)
							{
								MessageBox.Show("Canceled task can not be done!");
								doneShown = true;
							}
						}
						else
						{
							task.TaskDate = DateTime.Today;
							task.Status = (int)Status.Done;

							Static.UpdateTask(task);
						}
					}
				}
				else
				{
					MessageBox.Show("Please select task(s) first!");
				}

				loadTasks();
			}
		}

		private void button1_Click_1(object sender, System.EventArgs e)
		{
            ResetEmailTasksGroupBox();

			/* original
			calendar.SelectionStart = calendar.MinDate;
			calendar.SelectionEnd = calendar.MaxDate;

			loadTasks();
			*/

			selectAllDates = true;
			loadTasks();

		}

		private void calendar_MouseUp(object sender, System.Windows.Forms.MouseEventArgs e)
		{

//			selectAllDates = false;

		}

		private void calendar_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
		{
/*
 			if(selectAllDates)
			{

				selectAllDates = false;

				ignoreCalendarChange = true;
				calendar.SelectionStart = calendar.SelectionEnd;
				Application.DoEvents();
				ignoreCalendarChange = false;
				calendar.SelectionEnd = calendar.SelectionStart;
			}
*/
			if(selectAllDates)
				calendarClicked = true;
		}

		private void printFax_Click(object sender, System.EventArgs e)
		{
			bool oneDone = false;

			if(taskListGrid.Selection.Count > 0)
			{
				foreach(Task task in taskListGrid.Selection)
				{
					if(task.MailFaxWizard != null)
					{
					ShowTask:
						ArrayList tasks = (ArrayList)Static.GetTaskUsers(task);
						ArrayList doNotMail = ApplicationHelper.GetDoNotMailData();						
						ArrayList remove = new ArrayList();
						
						//remove dnm
						foreach(Guid r in tasks)
							foreach(Guid d in doNotMail)
								if(r == d) remove.Add(r);

						foreach(Guid r in remove)
							tasks.Remove(r);

						if(tasks.Count == 0)
						{
							MessageBox.Show("No Mail / Fax pieces to merge!");
							continue;
						}

						task.MailFaxWizard.SelectedRecords = tasks;
						task.MailFaxWizard.ExcludeDoNotMailRecords = true;

						ExecuteMailFax executeMailFax = new ExecuteMailFax(task.MailFaxWizard);
						executeMailFax.ShowDialog();

						oneDone = true;

						if(executeMailFax.DialogResult == DialogResult.OK)
						{
							DoneQuery dq = new DoneQuery();

							dq.ShowDialog();

							if(dq.DialogResult == DialogResult.OK)
							{
								task.TaskDate = DateTime.Today;
								task.Status = (int)Status.Done;

								Static.UpdateTask(task);
							}
						}
						else
						{
							CancelQuery cq = new CancelQuery();
							cq.ShowDialog();
							if(cq.DialogResult == DialogResult.Cancel) goto ShowTask;
						}

					}
				}

				if(oneDone)
				{
                    ResetEmailTasksGroupBox();
					loadTasks();
				}
				else
				{
					MessageBox.Show("No Mail / Fax pieces selected to merge!");
				}
/*				foreach(Task task in taskListGrid.Selection)
				{
					task.TaskDate = DateTime.Today;
					task.Status = (int)Status.Done;

					Static.UpdateTask(task);
				}
*/			}
			else
			{
				MessageBox.Show("Please select task(s) first!");
			}
		}

		private void reactivateTask_Click(object sender, System.EventArgs e)
		{
            ResetEmailTasksGroupBox();
			bool doneShown = false;

			if(taskListGrid.Selection.Count > 0)
			{
				if(MessageBox.Show("Are you sure you want to Re-Activate selected Task(s)?","Re-Activate Task?", MessageBoxButtons.YesNo) == DialogResult.Yes)
				{
					foreach(Task task in taskListGrid.Selection)
					{
						if(task.Status != (int)Status.Canceled && task.Status != (int)Status.Done)
						{
							if(!doneShown)
							{
								MessageBox.Show("Pending task can not be reactivated!");
								doneShown = true;
							}
						}
						else
						{
							//task.TaskDate = DateTime.Today;
							task.Status = (int)Status.Pending;

							Static.UpdateTask(task);
							loadTasks();
						}
					}
				}
			}
			else
			{
				MessageBox.Show("Please select task(s) first!");
			}

		}

        private void enableShowStaff(bool enabled)
        {
            showStaff1.Enabled = enabled;
            showStaff2.Enabled = enabled;
            showStaff3.Enabled = enabled;
        }

        private void showAll_CheckedChanged(object sender, EventArgs e)
        {
            ResetEmailTasksGroupBox();
            enableShowStaff(false);
            loadTasks();
        }

        private void showSelected_CheckedChanged(object sender, EventArgs e)
        {
            ResetEmailTasksGroupBox();
            enableShowStaff(true);
            loadTasks();
        }

        public bool ShowTasksForAll
        {
            get { return showAll.Checked; }
            set { showAll.Checked = value; }
        }

        private void showStaff1_SelectedIndexChanged(object sender, EventArgs e)
        {
            ResetEmailTasksGroupBox();
            loadTasks();
        }

        private void showStaff2_SelectedIndexChanged(object sender, EventArgs e)
        {
            ResetEmailTasksGroupBox();
            loadTasks();
        }

        private void showStaff3_SelectedIndexChanged(object sender, EventArgs e)
        {
            ResetEmailTasksGroupBox();
            loadTasks();
        }

        void On_StaffWithEmail_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (0 <= _staffWithEmail.SelectedIndex)
            {
                IList tasks = loadTasksForEmail(_staffWithEmail.Text);
                _sendTasks.Enabled = true;
                _sendTasks.Tag = tasks;                
            }
            else
            {
                _sendTasks.Enabled = false;
                _sendTasks.Tag = null;
            }
        }

        void ResetEmailTasksGroupBox()
        {
            if (null != _staffWithEmail.Items && 0 < _staffWithEmail.Items.Count)
            {
                //_staffWithEmail.SelectedIndex = 0;
                _staffWithEmail.SelectedIndex = -1;
                _staffWithEmail.Text = string.Empty;
            }
        }

        async void On_SendTasks_Click(object sender, EventArgs e)
        {
            if (null != _sendTasks.Tag)
            {
                await Task2EmailWorker.Instance.SendTasksNow(_staffWithEmail.Text, (IList)(_sendTasks.Tag));
            }
        }
    }  
}
