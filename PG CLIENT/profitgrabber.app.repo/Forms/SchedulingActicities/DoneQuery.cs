using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace DealMaker.Forms.SchedulingActicities
{
	/// <summary>
	/// Summary description for DoneQuery.
	/// </summary>
	public class DoneQuery : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Button doneTask;
		private System.Windows.Forms.Button cancel;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label3;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public DoneQuery()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.doneTask = new System.Windows.Forms.Button();
			this.cancel = new System.Windows.Forms.Button();
			this.label2 = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// doneTask
			// 
			this.doneTask.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.doneTask.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.doneTask.Location = new System.Drawing.Point(368, 160);
			this.doneTask.Name = "doneTask";
			this.doneTask.Size = new System.Drawing.Size(88, 32);
			this.doneTask.TabIndex = 0;
			this.doneTask.Text = "Done Task";
			// 
			// cancel
			// 
			this.cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.cancel.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.cancel.Location = new System.Drawing.Point(272, 160);
			this.cancel.Name = "cancel";
			this.cancel.Size = new System.Drawing.Size(88, 32);
			this.cancel.TabIndex = 1;
			this.cancel.Text = "Cancel";
			// 
			// label2
			// 
			this.label2.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label2.Location = new System.Drawing.Point(8, 8);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(448, 53);
			this.label2.TabIndex = 3;
			this.label2.Text = "Are you sure you want to mark this task as �Done Task� and remove it from the Tas" +
				"k List?";
			this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label1
			// 
			this.label1.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.label1.Location = new System.Drawing.Point(8, 72);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(448, 32);
			this.label1.TabIndex = 4;
			this.label1.Text = "You should click �Cancel� if your mailing pieces from this Task did NOT print out" +
				" correctly! Please verify.";
			// 
			// label3
			// 
			this.label3.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.label3.Location = new System.Drawing.Point(8, 112);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(448, 40);
			this.label3.TabIndex = 5;
			this.label3.Text = "You should click �Done Task� if everything printed out to your satisfaction, sinc" +
				"e that completes the task.";
			// 
			// DoneQuery
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(210)), ((System.Byte)(216)), ((System.Byte)(240)));
			this.ClientSize = new System.Drawing.Size(466, 199);
			this.ControlBox = false;
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.cancel);
			this.Controls.Add(this.doneTask);
			this.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "DoneQuery";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "ATTENTION";
			this.ResumeLayout(false);

		}
		#endregion
	}
}
