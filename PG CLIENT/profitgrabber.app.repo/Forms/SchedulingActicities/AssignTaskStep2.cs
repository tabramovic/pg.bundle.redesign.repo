using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
//using Outlook = Microsoft.Office.Interop.Outlook;

namespace DealMaker.Forms.SchedulingActicities
{
	/// <summary>
	/// Summary description for AssignTaskStep2.
	/// </summary>
	public class AssignTaskStep2 : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Label assignTaskLabel;
		private System.Windows.Forms.TextBox taskFor;
		private System.Windows.Forms.Label taskForLabel;
		private System.Windows.Forms.Label scheduleTaskDateLabel;
		private System.Windows.Forms.Label daysLabel;
		private System.Windows.Forms.Label yourSelectedLabel;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Panel mailPanel;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Button selectMailingPieceButton;
		private System.Windows.Forms.TextBox mailingPiece;
		private System.Windows.Forms.Panel callPanel;
		private System.Windows.Forms.TextBox phoneNumber;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.Label toDoTaskLabel;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.TextBox homePhoneNumber;
		private System.Windows.Forms.TextBox workPhoneNumber;
		private System.Windows.Forms.TextBox cellPhoneNumber;
		private System.Windows.Forms.TextBox bestTimeHome;
		private System.Windows.Forms.TextBox bestTimeWork;
		private System.Windows.Forms.TextBox bestTimeCell;
		private System.Windows.Forms.Label label13;
		private System.Windows.Forms.Button cancelButton;
		private System.Windows.Forms.Button assignButton;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
		private System.Windows.Forms.ComboBox dateDirection;

		private TaskType taskType;
		private System.Windows.Forms.RadioButton firstBuisnessDayTrue;
		private System.Windows.Forms.RadioButton firstBuisnessDayFalse;
		private System.Windows.Forms.ComboBox CreatedFor;
		private System.Windows.Forms.ComboBox CreatedBy;
		private System.Windows.Forms.TextBox Description;
		private System.Windows.Forms.TextBox Notes;
		private System.Windows.Forms.TextBox date;
		private System.Windows.Forms.Label selectEventDateLabel;
		private System.Windows.Forms.DateTimePicker EventDate;

		public Form parent;
		bool doNotCheckForPast = false;
		private System.Windows.Forms.CheckBox exportTaskInOutlook;
        private NumericUpDown _priority;
        private Label label10;
        private Label _taskDateLabel;
        private DateTimePicker _taskDate;
        bool _editMode = false;
        private CheckBox _sendPerEmail;
        private DateTimePicker _sendOutTimeStamp;
        CustomSettings _customSettings = null;

        

		#region � Network UDP �

		public Network.NetworkChangedEventHandler networkEventHandler;

		//sasa, 20.11.05: nw version, invoke required (refresh thread in different task)
		private delegate void UpdateDelegate();

		private void UpdateDelegateFunc()
		{                        
            if (MessageBox.Show(this, "Other application has changed task that you are currently working on." + System.Environment.NewLine + "Would you like to continue work on the old task?", "Attention", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                //				Network.NetworkUDP.ChangedTask -= new DealMaker.Network.NetworkChangedEventHandler(NetworkUDP_ChangedTask);
                this.DialogResult = DialogResult.Cancel;
                this.Close();
            }            
		}
		
		private void NetworkUDP_ChangedTask(Network.NetworkUDP.Packet p)
		{
			if(p.TaskID == this.task.IdTask)
			{
				Globals.MainForm.BeginInvoke(new UpdateDelegate(UpdateDelegateFunc));
			}
		}
		#endregion � Network UDP �

        
		public string PhoneNumber
		{
			get {return this.phoneNumber.Text;}
			set {this.phoneNumber.Text = value;}
		}

		public string AssignButtonText
		{
			set
			{
				assignButton.Text = value;
			}
		}

		bool showOutlookTaskEntryCheckBox = false;
		public bool ShowOutlookTaskEntryCheckBox
		{
			set
			{
                //sasa - privremeno zakomentirano
                //showOutlookTaskEntryCheckBox = value;
                //exportTaskInOutlook.Visible = value;
			}
		}

		private PropertyItem user;
		public PropertyItem User
		{
			set
			{
				user = value;
				group = null;

				if(user != null && user.Owner != null)
					taskFor.Text = user.Owner.FullName;
				
				if(user != null && user.Owner != null)
				{
					if (null != user.Owner.HomePhone)
					{
						homePhoneNumber.Text = user.Owner.HomePhone.PhoneNr;
						bestTimeHome.Text = user.Owner.HomePhone.BestTimeToCall;
					}

					if (null != user.Owner.WorkPhone)
					{
						workPhoneNumber.Text = user.Owner.WorkPhone.PhoneNr;
						bestTimeWork.Text = user.Owner.WorkPhone.BestTimeToCall;
					}

					if (null != user.Owner.CellPhone)
					{
						cellPhoneNumber.Text = user.Owner.CellPhone.PhoneNr;										
						bestTimeCell.Text = user.Owner.CellPhone.BestTimeToCall;
					}										
				}
			}
			get
			{
				return user;
			}
		}

		private Node group;
		public Node Group
		{
			set
			{
				group = value;
				user = null;

				taskFor.Text = group.nodeName;
			}
			get
			{
				return group;
			}
		}
		
		private Task task;
		public System.Windows.Forms.Button done;
		private bool showDate;

		public bool ShowDate
		{
			set
			{
				showDate = value;
				EventDate.Enabled = showDate;
				if(showDate)
				{
					selectEventDateLabel.Text = "Select YOUR EVENT DATE:";
				}
				else
				{
					selectEventDateLabel.Text = "EVENT DATE:";
				}
			}
			get
			{
				return showDate;
			}
		}

        public int Priority
        {
            get { return (int)_priority.Value; }
            set { _priority.Value = value; }
        }

        /* //Fills event date, days from event date and move to business day
		public Task Task
		{
			set
			{
				task = value;

				if(task != null)
				{
					ObjectPresentation.FormPropertyObject.FillForm(this, task);
				
					ArrayList list = new ArrayList();
					list.Add(task);
					Static.FillTaskListWithContactInfo(list);			
					taskFor.Text = task.ContactOrGroup;

					if(task.MailFaxWizard != null)
					{
						mailingPiece.Text = task.MailFaxWizard.DocumentName;
					}

					if(task.DatesFrom >= 0)
					{
						dateDirection.SelectedIndex = (int)TaskDateType.From;
						date.Text = task.DatesFrom.ToString();
					}
					else
					{
						dateDirection.SelectedIndex = (int)TaskDateType.PriorTo;
						date.Text = Convert.ToString(0 - task.DatesFrom);
					}

					if(task.MoveToBuisnessDay)
					{
						firstBuisnessDayTrue.Checked = true;
					}
					else
					{
						firstBuisnessDayFalse.Checked = true;
					}
				}
			}
			get
			{
				return task;
			}
		}
*/
        public void SetEditFlag()
        {
            this.Text = "Edit Task";
        }
        public void SetEditMode()
        {
            _editMode = true;            
            _taskDateLabel.Visible = _taskDate.Visible = true;

            selectEventDateLabel.Enabled = false;
            EventDate.Enabled = false;
            scheduleTaskDateLabel.Enabled = false;
            date.Enabled = false;
            daysLabel.Enabled = false;
            dateDirection.Enabled = false;
            yourSelectedLabel.Enabled = false;
            label1.Enabled = false;
            firstBuisnessDayTrue.Enabled = false;
            firstBuisnessDayFalse.Enabled = false;

            if (task.TaskDate <= DateTime.MinValue.Date)
                _taskDate.Value = DateTime.Now.Date;
            else
                _taskDate.Value = task.TaskDate;
        }

        public Task Task
        {
            set
            {
                task = value;

                if (task != null)
                {
                    

                    ObjectPresentation.FormPropertyObject.FillForm(this, task);

                    ArrayList list = new ArrayList();
                    list.Add(task);
                    Static.FillTaskListWithContactInfo(list);

                    if (null != task.ContactOrGroup)
                        taskFor.Text = task.ContactOrGroup.TrimEnd();                    

                    if (task.MailFaxWizard != null)
                    {
                        mailingPiece.Text = System.IO.Path.GetFileName(task.MailFaxWizard.DocumentName);                       
                    }

                    if (task.DatesFrom >= 0)
                    {
                        dateDirection.SelectedIndex = (int)TaskDateType.From;
                        date.Text = "0";//task.DatesFrom.ToString();
                    }
                    else
                    {
                        dateDirection.SelectedIndex = (int)TaskDateType.PriorTo;
                        date.Text = "0";//Convert.ToString(0 - task.DatesFrom);
                    }


                    if (task.MoveToBuisnessDay)
                    {
                        firstBuisnessDayTrue.Checked = true;
                    }
                    else
                    {
                        firstBuisnessDayFalse.Checked = true;
                    }

                    //OLD
                    //this.EventDate.Value = task.TaskDate <= DateTimePicker.MinimumDateTime ? DateTime.Now : task.TaskDate;

                    //NEW (custom fix for Kit)
                    this.EventDate.Value = task.EventDate <= DateTimePicker.MinimumDateTime ? DateTime.Now : task.EventDate;
                    this.date.Text = task.DatesFrom.ToString();
                    
                    //PRIORITY
                    this.Priority = task.Priority;

                    if (DateTime.MinValue != task.CheckOut)
                    {
                        _sendPerEmail.Checked = true;
                        _sendOutTimeStamp.Value = task.CheckOut;
                    }
                    
                }
            }
            get
            {
                return task;
            }
        }


		public TaskType TaskType
		{ 
			get
			{
				return taskType;
			}
			set
			{
				taskType = value;

				string taskName = "";

				if(taskType == TaskType.Call)
				{
					taskName = "Call";
					this.Height += callPanel.Height;
					callPanel.Visible = true;
				}

				if(taskType == TaskType.Mail)
				{
					taskName = "Mail / Fax";
					this.Height += mailPanel.Height;
					mailPanel.Visible = true;
				}

				if(taskType == TaskType.Visit)
				{
					taskName = "Visit";
				}

				if(taskType == TaskType.ToDo)
				{
					taskName = "ToDo";
				}

				if(taskType == TaskType.PastAction)
				{
					taskName = "PastAction";
				}

				toDoTaskLabel.Text = "Enter Short Description of your " + taskName + " task:";
				assignTaskLabel.Text = "Define \"" + taskName + "\" Task:";
			}
		}

		public AssignTaskStep2()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			exportTaskInOutlook.Visible = showOutlookTaskEntryCheckBox;

			//add combo items
			Static.FillCreatedCombo(CreatedFor);
			Static.FillCreatedCombo(CreatedBy);

            
			//sasa, 20.11.05: nw, event handler
			Network.NetworkUDP.ChangedTask += networkEventHandler = new DealMaker.Network.NetworkChangedEventHandler(NetworkUDP_ChangedTask);
            _sendPerEmail.CheckedChanged += new EventHandler(On_SendPerEmail_CheckedChanged);

            string customSettingsError = string.Empty;
            _customSettings = new CustomSettingsManager().LoadCustomSettings(out customSettingsError);
		}

        protected override void OnClosing(CancelEventArgs e)
        {
            Network.NetworkUDP.ChangedTask -= networkEventHandler = new DealMaker.Network.NetworkChangedEventHandler(NetworkUDP_ChangedTask);
            base.OnClosing(e);
        }

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				//sasa 20.11.05: hack, we must remove event handler, there should be smarter way for this, dispose doesn't work
                Network.NetworkUDP.ChangedTask -= this.networkEventHandler = new DealMaker.Network.NetworkChangedEventHandler(NetworkUDP_ChangedTask);

				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.assignTaskLabel = new System.Windows.Forms.Label();
            this.taskFor = new System.Windows.Forms.TextBox();
            this.taskForLabel = new System.Windows.Forms.Label();
            this.scheduleTaskDateLabel = new System.Windows.Forms.Label();
            this.date = new System.Windows.Forms.TextBox();
            this.daysLabel = new System.Windows.Forms.Label();
            this.dateDirection = new System.Windows.Forms.ComboBox();
            this.yourSelectedLabel = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.firstBuisnessDayTrue = new System.Windows.Forms.RadioButton();
            this.firstBuisnessDayFalse = new System.Windows.Forms.RadioButton();
            this.mailPanel = new System.Windows.Forms.Panel();
            this.selectMailingPieceButton = new System.Windows.Forms.Button();
            this.mailingPiece = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.callPanel = new System.Windows.Forms.Panel();
            this.bestTimeCell = new System.Windows.Forms.TextBox();
            this.bestTimeWork = new System.Windows.Forms.TextBox();
            this.bestTimeHome = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.cellPhoneNumber = new System.Windows.Forms.TextBox();
            this.workPhoneNumber = new System.Windows.Forms.TextBox();
            this.homePhoneNumber = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.phoneNumber = new System.Windows.Forms.TextBox();
            this.toDoTaskLabel = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.CreatedFor = new System.Windows.Forms.ComboBox();
            this.CreatedBy = new System.Windows.Forms.ComboBox();
            this.Description = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.Notes = new System.Windows.Forms.TextBox();
            this.cancelButton = new System.Windows.Forms.Button();
            this.assignButton = new System.Windows.Forms.Button();
            this.EventDate = new System.Windows.Forms.DateTimePicker();
            this.selectEventDateLabel = new System.Windows.Forms.Label();
            this.done = new System.Windows.Forms.Button();
            this.exportTaskInOutlook = new System.Windows.Forms.CheckBox();
            this._priority = new System.Windows.Forms.NumericUpDown();
            this.label10 = new System.Windows.Forms.Label();
            this._taskDateLabel = new System.Windows.Forms.Label();
            this._taskDate = new System.Windows.Forms.DateTimePicker();
            this._sendPerEmail = new System.Windows.Forms.CheckBox();
            this._sendOutTimeStamp = new System.Windows.Forms.DateTimePicker();
            this.mailPanel.SuspendLayout();
            this.callPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._priority)).BeginInit();
            this.SuspendLayout();
            // 
            // assignTaskLabel
            // 
            this.assignTaskLabel.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.assignTaskLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.assignTaskLabel.Location = new System.Drawing.Point(9, 10);
            this.assignTaskLabel.Name = "assignTaskLabel";
            this.assignTaskLabel.Size = new System.Drawing.Size(343, 22);
            this.assignTaskLabel.TabIndex = 1;
            this.assignTaskLabel.Text = "defineTask";
            // 
            // taskFor
            // 
            this.taskFor.Location = new System.Drawing.Point(94, 35);
            this.taskFor.Name = "taskFor";
            this.taskFor.ReadOnly = true;
            this.taskFor.Size = new System.Drawing.Size(279, 20);
            this.taskFor.TabIndex = 4;
            // 
            // taskForLabel
            // 
            this.taskForLabel.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.taskForLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.taskForLabel.Location = new System.Drawing.Point(24, 38);
            this.taskForLabel.Name = "taskForLabel";
            this.taskForLabel.Size = new System.Drawing.Size(64, 23);
            this.taskForLabel.TabIndex = 3;
            this.taskForLabel.Text = "TASK For:";
            // 
            // scheduleTaskDateLabel
            // 
            this.scheduleTaskDateLabel.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.scheduleTaskDateLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.scheduleTaskDateLabel.Location = new System.Drawing.Point(24, 88);
            this.scheduleTaskDateLabel.Name = "scheduleTaskDateLabel";
            this.scheduleTaskDateLabel.Size = new System.Drawing.Size(120, 23);
            this.scheduleTaskDateLabel.TabIndex = 5;
            this.scheduleTaskDateLabel.Text = "Schedule TASK DATE ";
            // 
            // date
            // 
            this.date.Location = new System.Drawing.Point(143, 85);
            this.date.Name = "date";
            this.date.Size = new System.Drawing.Size(40, 20);
            this.date.TabIndex = 6;
            this.date.Validating += new System.ComponentModel.CancelEventHandler(this.date_Validating);
            // 
            // daysLabel
            // 
            this.daysLabel.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.daysLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.daysLabel.Location = new System.Drawing.Point(191, 88);
            this.daysLabel.Name = "daysLabel";
            this.daysLabel.Size = new System.Drawing.Size(32, 23);
            this.daysLabel.TabIndex = 7;
            this.daysLabel.Text = "days";
            // 
            // dateDirection
            // 
            this.dateDirection.Items.AddRange(new object[] {
            "From",
            "Prior To"});
            this.dateDirection.Location = new System.Drawing.Point(227, 85);
            this.dateDirection.Name = "dateDirection";
            this.dateDirection.Size = new System.Drawing.Size(67, 21);
            this.dateDirection.TabIndex = 8;
            this.dateDirection.Text = "From";
            // 
            // yourSelectedLabel
            // 
            this.yourSelectedLabel.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.yourSelectedLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.yourSelectedLabel.Location = new System.Drawing.Point(300, 88);
            this.yourSelectedLabel.Name = "yourSelectedLabel";
            this.yourSelectedLabel.Size = new System.Drawing.Size(172, 23);
            this.yourSelectedLabel.TabIndex = 9;
            this.yourSelectedLabel.Text = "your SELECTED EVENT DATE.";
            // 
            // label1
            // 
            this.label1.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(43, 111);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(456, 23);
            this.label1.TabIndex = 10;
            this.label1.Text = "If your TASK DATE falls on Weekend, do you want to move it to the first Buisness " +
    "Day?";
            // 
            // firstBuisnessDayTrue
            // 
            this.firstBuisnessDayTrue.Checked = true;
            this.firstBuisnessDayTrue.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.firstBuisnessDayTrue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.firstBuisnessDayTrue.Location = new System.Drawing.Point(172, 130);
            this.firstBuisnessDayTrue.Name = "firstBuisnessDayTrue";
            this.firstBuisnessDayTrue.Size = new System.Drawing.Size(56, 16);
            this.firstBuisnessDayTrue.TabIndex = 11;
            this.firstBuisnessDayTrue.TabStop = true;
            this.firstBuisnessDayTrue.Text = "Yes";
            // 
            // firstBuisnessDayFalse
            // 
            this.firstBuisnessDayFalse.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.firstBuisnessDayFalse.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.firstBuisnessDayFalse.Location = new System.Drawing.Point(292, 130);
            this.firstBuisnessDayFalse.Name = "firstBuisnessDayFalse";
            this.firstBuisnessDayFalse.Size = new System.Drawing.Size(64, 16);
            this.firstBuisnessDayFalse.TabIndex = 12;
            this.firstBuisnessDayFalse.Text = "No";
            // 
            // mailPanel
            // 
            this.mailPanel.Controls.Add(this.selectMailingPieceButton);
            this.mailPanel.Controls.Add(this.mailingPiece);
            this.mailPanel.Controls.Add(this.label3);
            this.mailPanel.Location = new System.Drawing.Point(12, 225);
            this.mailPanel.Name = "mailPanel";
            this.mailPanel.Size = new System.Drawing.Size(457, 88);
            this.mailPanel.TabIndex = 14;
            this.mailPanel.Visible = false;
            // 
            // selectMailingPieceButton
            // 
            this.selectMailingPieceButton.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.selectMailingPieceButton.Location = new System.Drawing.Point(420, 40);
            this.selectMailingPieceButton.Name = "selectMailingPieceButton";
            this.selectMailingPieceButton.Size = new System.Drawing.Size(32, 22);
            this.selectMailingPieceButton.TabIndex = 2;
            this.selectMailingPieceButton.Text = "...";
            this.selectMailingPieceButton.Click += new System.EventHandler(this.selectMailingPieceButton_Click);
            // 
            // mailingPiece
            // 
            this.mailingPiece.Location = new System.Drawing.Point(10, 41);
            this.mailingPiece.Name = "mailingPiece";
            this.mailingPiece.ReadOnly = true;
            this.mailingPiece.Size = new System.Drawing.Size(403, 20);
            this.mailingPiece.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.label3.Location = new System.Drawing.Point(12, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(432, 23);
            this.label3.TabIndex = 0;
            this.label3.Text = "Select a MAILING PIECE to use in this TASK:";
            // 
            // callPanel
            // 
            this.callPanel.Controls.Add(this.bestTimeCell);
            this.callPanel.Controls.Add(this.bestTimeWork);
            this.callPanel.Controls.Add(this.bestTimeHome);
            this.callPanel.Controls.Add(this.label9);
            this.callPanel.Controls.Add(this.cellPhoneNumber);
            this.callPanel.Controls.Add(this.workPhoneNumber);
            this.callPanel.Controls.Add(this.homePhoneNumber);
            this.callPanel.Controls.Add(this.label8);
            this.callPanel.Controls.Add(this.label7);
            this.callPanel.Controls.Add(this.label6);
            this.callPanel.Controls.Add(this.label5);
            this.callPanel.Controls.Add(this.label4);
            this.callPanel.Controls.Add(this.phoneNumber);
            this.callPanel.Location = new System.Drawing.Point(12, 225);
            this.callPanel.Name = "callPanel";
            this.callPanel.Size = new System.Drawing.Size(457, 145);
            this.callPanel.TabIndex = 15;
            this.callPanel.Visible = false;
            // 
            // bestTimeCell
            // 
            this.bestTimeCell.Location = new System.Drawing.Point(392, 114);
            this.bestTimeCell.Name = "bestTimeCell";
            this.bestTimeCell.ReadOnly = true;
            this.bestTimeCell.Size = new System.Drawing.Size(48, 20);
            this.bestTimeCell.TabIndex = 14;
            // 
            // bestTimeWork
            // 
            this.bestTimeWork.Location = new System.Drawing.Point(392, 82);
            this.bestTimeWork.Name = "bestTimeWork";
            this.bestTimeWork.ReadOnly = true;
            this.bestTimeWork.Size = new System.Drawing.Size(48, 20);
            this.bestTimeWork.TabIndex = 12;
            // 
            // bestTimeHome
            // 
            this.bestTimeHome.Location = new System.Drawing.Point(392, 50);
            this.bestTimeHome.Name = "bestTimeHome";
            this.bestTimeHome.ReadOnly = true;
            this.bestTimeHome.Size = new System.Drawing.Size(48, 20);
            this.bestTimeHome.TabIndex = 10;
            // 
            // label9
            // 
            this.label9.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.label9.Location = new System.Drawing.Point(392, 26);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(56, 16);
            this.label9.TabIndex = 9;
            this.label9.Text = "Best time:";
            // 
            // cellPhoneNumber
            // 
            this.cellPhoneNumber.Location = new System.Drawing.Point(224, 114);
            this.cellPhoneNumber.Name = "cellPhoneNumber";
            this.cellPhoneNumber.ReadOnly = true;
            this.cellPhoneNumber.Size = new System.Drawing.Size(152, 20);
            this.cellPhoneNumber.TabIndex = 8;
            // 
            // workPhoneNumber
            // 
            this.workPhoneNumber.Location = new System.Drawing.Point(224, 82);
            this.workPhoneNumber.Name = "workPhoneNumber";
            this.workPhoneNumber.ReadOnly = true;
            this.workPhoneNumber.Size = new System.Drawing.Size(152, 20);
            this.workPhoneNumber.TabIndex = 7;
            // 
            // homePhoneNumber
            // 
            this.homePhoneNumber.Location = new System.Drawing.Point(224, 50);
            this.homePhoneNumber.Name = "homePhoneNumber";
            this.homePhoneNumber.ReadOnly = true;
            this.homePhoneNumber.Size = new System.Drawing.Size(152, 20);
            this.homePhoneNumber.TabIndex = 6;
            // 
            // label8
            // 
            this.label8.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.label8.Location = new System.Drawing.Point(144, 114);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(72, 16);
            this.label8.TabIndex = 5;
            this.label8.Text = "Cell Phone";
            // 
            // label7
            // 
            this.label7.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.label7.Location = new System.Drawing.Point(144, 82);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(72, 23);
            this.label7.TabIndex = 4;
            this.label7.Text = "Work Phone";
            // 
            // label6
            // 
            this.label6.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.label6.Location = new System.Drawing.Point(144, 50);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(72, 23);
            this.label6.TabIndex = 3;
            this.label6.Text = "Home Phone";
            // 
            // label5
            // 
            this.label5.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.label5.Location = new System.Drawing.Point(8, 49);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(144, 23);
            this.label5.TabIndex = 2;
            this.label5.Text = "Available Phone Numbers:";
            // 
            // label4
            // 
            this.label4.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.label4.Location = new System.Drawing.Point(8, 20);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(208, 23);
            this.label4.TabIndex = 1;
            this.label4.Text = "Enter Phone Number to Call (not required):";
            // 
            // phoneNumber
            // 
            this.phoneNumber.Location = new System.Drawing.Point(224, 18);
            this.phoneNumber.Name = "phoneNumber";
            this.phoneNumber.Size = new System.Drawing.Size(152, 20);
            this.phoneNumber.TabIndex = 0;
            // 
            // toDoTaskLabel
            // 
            this.toDoTaskLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.toDoTaskLabel.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.toDoTaskLabel.Location = new System.Drawing.Point(16, 226);
            this.toDoTaskLabel.Name = "toDoTaskLabel";
            this.toDoTaskLabel.Size = new System.Drawing.Size(280, 23);
            this.toDoTaskLabel.TabIndex = 16;
            this.toDoTaskLabel.Text = "Enter Short Description of your >>xxxx<< task:";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label2.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.label2.Location = new System.Drawing.Point(16, 314);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(104, 23);
            this.label2.TabIndex = 17;
            this.label2.Text = "Task Created For:";
            // 
            // label12
            // 
            this.label12.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label12.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.label12.Location = new System.Drawing.Point(16, 346);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(104, 23);
            this.label12.TabIndex = 18;
            this.label12.Text = "Task Created By:";
            // 
            // CreatedFor
            // 
            this.CreatedFor.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.CreatedFor.Location = new System.Drawing.Point(120, 314);
            this.CreatedFor.Name = "CreatedFor";
            this.CreatedFor.Size = new System.Drawing.Size(141, 21);
            this.CreatedFor.TabIndex = 19;
            // 
            // CreatedBy
            // 
            this.CreatedBy.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.CreatedBy.Location = new System.Drawing.Point(120, 346);
            this.CreatedBy.Name = "CreatedBy";
            this.CreatedBy.Size = new System.Drawing.Size(141, 21);
            this.CreatedBy.TabIndex = 20;
            // 
            // Description
            // 
            this.Description.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.Description.Location = new System.Drawing.Point(240, 226);
            this.Description.Name = "Description";
            this.Description.Size = new System.Drawing.Size(224, 20);
            this.Description.TabIndex = 21;
            // 
            // label13
            // 
            this.label13.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label13.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.label13.Location = new System.Drawing.Point(16, 250);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(40, 23);
            this.label13.TabIndex = 22;
            this.label13.Text = "Notes:";
            // 
            // Notes
            // 
            this.Notes.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.Notes.Location = new System.Drawing.Point(56, 250);
            this.Notes.MaxLength = 2047;
            this.Notes.Multiline = true;
            this.Notes.Name = "Notes";
            this.Notes.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Notes.Size = new System.Drawing.Size(408, 51);
            this.Notes.TabIndex = 23;
            // 
            // cancelButton
            // 
            this.cancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.cancelButton.Location = new System.Drawing.Point(341, 346);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(64, 23);
            this.cancelButton.TabIndex = 24;
            this.cancelButton.Text = "Cancel";
            // 
            // assignButton
            // 
            this.assignButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.assignButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.assignButton.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.assignButton.Location = new System.Drawing.Point(413, 346);
            this.assignButton.Name = "assignButton";
            this.assignButton.Size = new System.Drawing.Size(64, 23);
            this.assignButton.TabIndex = 25;
            this.assignButton.Text = "Assign";
            this.assignButton.Click += new System.EventHandler(this.assignButton_Click);
            // 
            // EventDate
            // 
            this.EventDate.Location = new System.Drawing.Point(172, 58);
            this.EventDate.Name = "EventDate";
            this.EventDate.Size = new System.Drawing.Size(201, 20);
            this.EventDate.TabIndex = 27;
            this.EventDate.Validating += new System.ComponentModel.CancelEventHandler(this.EventDate_Validating);
            // 
            // selectEventDateLabel
            // 
            this.selectEventDateLabel.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.selectEventDateLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.selectEventDateLabel.Location = new System.Drawing.Point(12, 61);
            this.selectEventDateLabel.Name = "selectEventDateLabel";
            this.selectEventDateLabel.Size = new System.Drawing.Size(160, 23);
            this.selectEventDateLabel.TabIndex = 26;
            this.selectEventDateLabel.Text = "Select YOUR EVENT DATE:";
            this.selectEventDateLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // done
            // 
            this.done.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.done.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.done.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.done.Location = new System.Drawing.Point(269, 346);
            this.done.Name = "done";
            this.done.Size = new System.Drawing.Size(64, 23);
            this.done.TabIndex = 28;
            this.done.Text = "Done";
            this.done.Visible = false;
            this.done.Click += new System.EventHandler(this.done_Click);
            // 
            // exportTaskInOutlook
            // 
            this.exportTaskInOutlook.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.exportTaskInOutlook.Location = new System.Drawing.Point(344, 319);
            this.exportTaskInOutlook.Name = "exportTaskInOutlook";
            this.exportTaskInOutlook.Size = new System.Drawing.Size(144, 16);
            this.exportTaskInOutlook.TabIndex = 29;
            this.exportTaskInOutlook.Text = "Export Task in Outlook";
            this.exportTaskInOutlook.Visible = false;
            // 
            // _priority
            // 
            this._priority.Location = new System.Drawing.Point(432, 35);
            this._priority.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this._priority.Name = "_priority";
            this._priority.Size = new System.Drawing.Size(45, 20);
            this._priority.TabIndex = 30;
            // 
            // label10
            // 
            this.label10.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(379, 35);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(47, 23);
            this.label10.TabIndex = 31;
            this.label10.Text = "Priority:";
            // 
            // _taskDateLabel
            // 
            this._taskDateLabel.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._taskDateLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._taskDateLabel.Location = new System.Drawing.Point(91, 157);
            this._taskDateLabel.Name = "_taskDateLabel";
            this._taskDateLabel.Size = new System.Drawing.Size(77, 23);
            this._taskDateLabel.TabIndex = 32;
            this._taskDateLabel.Text = "TASK DATE:";
            this._taskDateLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this._taskDateLabel.Visible = false;
            // 
            // _taskDate
            // 
            this._taskDate.Location = new System.Drawing.Point(170, 157);
            this._taskDate.Name = "_taskDate";
            this._taskDate.Size = new System.Drawing.Size(256, 20);
            this._taskDate.TabIndex = 33;
            this._taskDate.Visible = false;
            // 
            // _sendPerEmail
            // 
            this._sendPerEmail.AutoSize = true;
            this._sendPerEmail.Location = new System.Drawing.Point(194, 186);
            this._sendPerEmail.Name = "_sendPerEmail";
            this._sendPerEmail.Size = new System.Drawing.Size(141, 17);
            this._sendPerEmail.TabIndex = 34;
            this._sendPerEmail.Text = "Send Task via E-mail at:";
            this._sendPerEmail.UseVisualStyleBackColor = true;
            // 
            // _sendOutTimeStamp
            // 
            this._sendOutTimeStamp.CustomFormat = "hh:mm tt";
            this._sendOutTimeStamp.Enabled = false;
            this._sendOutTimeStamp.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this._sendOutTimeStamp.Location = new System.Drawing.Point(341, 183);
            this._sendOutTimeStamp.Name = "_sendOutTimeStamp";
            this._sendOutTimeStamp.ShowUpDown = true;
            this._sendOutTimeStamp.Size = new System.Drawing.Size(84, 20);
            this._sendOutTimeStamp.TabIndex = 35;
            // 
            // AssignTaskStep2
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.ClientSize = new System.Drawing.Size(482, 377);
            this.Controls.Add(this.mailPanel);
            this.Controls.Add(this._sendOutTimeStamp);
            this.Controls.Add(this._taskDate);
            this.Controls.Add(this._sendPerEmail);
            this.Controls.Add(this.date);
            this.Controls.Add(this._taskDateLabel);
            this.Controls.Add(this.label10);
            this.Controls.Add(this._priority);
            this.Controls.Add(this.EventDate);
            this.Controls.Add(this.selectEventDateLabel);
            this.Controls.Add(this.assignButton);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.Notes);
            this.Controls.Add(this.Description);
            this.Controls.Add(this.taskFor);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.CreatedBy);
            this.Controls.Add(this.CreatedFor);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.toDoTaskLabel);
            this.Controls.Add(this.firstBuisnessDayFalse);
            this.Controls.Add(this.firstBuisnessDayTrue);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.yourSelectedLabel);
            this.Controls.Add(this.dateDirection);
            this.Controls.Add(this.daysLabel);
            this.Controls.Add(this.scheduleTaskDateLabel);
            this.Controls.Add(this.taskForLabel);
            this.Controls.Add(this.assignTaskLabel);
            this.Controls.Add(this.done);
            this.Controls.Add(this.exportTaskInOutlook);
            this.Controls.Add(this.callPanel);
            this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.Name = "AssignTaskStep2";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Assign Task - Step 2";
            this.Load += new System.EventHandler(this.AssignTaskStep2_Load);
            this.mailPanel.ResumeLayout(false);
            this.mailPanel.PerformLayout();
            this.callPanel.ResumeLayout(false);
            this.callPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._priority)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		#endregion

        

		private void assignButton_Click(object sender, System.EventArgs e)
		{
			ObjectPresentation.FormPropertyObject.FillObject(this, task);
			task.PhoneNumber = this.phoneNumber.Text;
            task.Priority = (int)this._priority.Value;   
            
		
			if(this.TaskType == TaskType.Mail && task.MailFaxWizard == null)
			{
				MessageBox.Show("Please select MAILING PIECE to be used in this task!");
				this.DialogResult = DialogResult.None;
				return;
			}

            if (!_editMode)
            {

                task.MoveToBuisnessDay = firstBuisnessDayTrue.Checked;

                if (dateDirection.SelectedIndex == (int)TaskDateType.PriorTo)
                {
                    task.DatesFrom = 0 - Convert.ToInt32(date.Text);
                }
                else
                {
                    task.DatesFrom = Convert.ToInt32(date.Text);
                }

                //sasa 14.12.05: additional date skew, bug/feature fix
                task.TaskDate = task.EventDate.Date.AddDays(task.DatesFrom);

                if (task.MoveToBuisnessDay)
                {
                    if (task.TaskDate.DayOfWeek == System.DayOfWeek.Saturday)
                    {
                        //move forward 2 days if saturday
                        //					task.DatesFrom += 2;
                        task.TaskDate = task.TaskDate.Date.AddDays(2);
                    }

                    if (task.TaskDate.DayOfWeek == System.DayOfWeek.Sunday)
                    {
                        //move forward 1 day if sunday
                        //					task.DatesFrom += 1;
                        task.TaskDate = task.TaskDate.Date.AddDays(1);
                    }

                    //				task.TaskDate = task.EventDate.AddDays(task.DatesFrom);
                }

                task.TaskDate = task.TaskDate.Date;
            }
            else
            {
                task.TaskDate = _taskDate.Value.Date;
            }

			if(!doNotCheckForPast)
				if(task.TaskDate < DateTime.Today)
				{
					MessageBox.Show("Task Date cannot be in past! Please enter correct Task Date.");
					this.DialogResult = DialogResult.None;
				}

			//TODO - vidi sta je sa assigmentom iz activitya
			if(exportTaskInOutlook.Checked)
			{
				/*
				Outlook.ApplicationClass app = new Outlook.ApplicationClass();
				Outlook.TaskItem tsk = (Outlook.TaskItem) 
					app.CreateItem(Outlook.OlItemType.olTaskItem);

				tsk.StartDate = task.EventDate;
				tsk.DueDate = task.EventDate;
				tsk.Subject = "ProfitGrabber Task Entry " + task.Type.ToString() + " for:" + task.ContactOrGroup;
				tsk.Body = "Task description: "+ task.Description + "\n"+ "Taks Notes: " +task.Notes;
				tsk.Owner = task.CreatedFor;
				tsk.Assign();
				tsk.Save();
				*/
			}

            if (task.Status == (int)Status.PastDo) task.Status = (int)Status.Pending;//fix pastdo dissapearing,sasa 19.11.09

            //CHECKOUT
            task.CheckOut = _sendPerEmail.Checked ? _sendOutTimeStamp.Value : DateTime.MinValue;
            if (_sendPerEmail.Checked && task.CheckOut.Date != task.TaskDate.Date)
            {
                DateTime newTimeStamp = task.TaskDate.Date;
                newTimeStamp = newTimeStamp.AddHours(task.CheckOut.Hour);
                newTimeStamp = newTimeStamp.AddMinutes(task.CheckOut.Minute);

                task.CheckOut = newTimeStamp;
            }
		}

		private void date_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			try
			{
				int i = Convert.ToInt32(date.Text);
			}
			catch
			{
				e.Cancel = true;
				MessageBox.Show("Enter Numerical Value in your �days� field!");
				date.Select(0, date.Text.Length);
			}		
		}

		private void AssignTaskStep2_Load(object sender, System.EventArgs e)
		{
			if(parent!= null)
			{
				this.Location = parent.Location;
				this.Width = parent.Width;
			}

			this.Visible = true;
			
			if(parent!= null)
			{
				parent.Visible = false;
			}
		}

		private void EventDate_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
/*			if(EventDate.Value.Date < DateTime.Today)
			{
//				MessageBox.Show("
			}
*/		}

		private void done_Click(object sender, System.EventArgs e)
		{
			task.Status = (int)Status.Done;
			doNotCheckForPast = true;
			assignButton_Click(null, null);		
		}

		private void selectMailingPieceButton_Click(object sender, System.EventArgs e)
		{
            try
            {
                MailFaxWizard mailFaxWizard = task?.MailFaxWizard;

                if (mailFaxWizard == null)
                {
                    mailFaxWizard = new MailFaxWizard();
                }

                SelectMailFax selectMailFax = new SelectMailFax(mailFaxWizard);
                selectMailFax.BringToFront();

                selectMailFax.ShowDialog();

                if (selectMailFax.DialogResult == DialogResult.OK)
                {
                    if (mailFaxWizard.LabelDocSel == eLabelDocumentSelection.emailBlast || mailFaxWizard.LabelDocSel == eLabelDocumentSelection.emailLOI)
                        mailFaxWizard.DocumentName = mailFaxWizard.DocumentName;

                    if (mailFaxWizard.LabelDocSel == eLabelDocumentSelection.emailBlast)
                        mailingPiece.Text = $"Email Blast - {System.IO.Path.GetFileName(mailFaxWizard.DocumentName)}";

                    else if (mailFaxWizard.LabelDocSel == eLabelDocumentSelection.emailLOI)
                        mailingPiece.Text = $"Email LOI - {System.IO.Path.GetFileName(mailFaxWizard.DocumentName)}";
                    else
                        mailingPiece.Text = mailFaxWizard.DocumentName;

                    task.MailFaxWizard = mailFaxWizard;
                }
            }
            catch (Exception exc)
            {
                MessageBox.Show($"{exc.Message}{Environment.NewLine}{exc.InnerException}{Environment.NewLine}{exc.StackTrace}");
            }
		}

        void On_SendPerEmail_CheckedChanged(object sender, EventArgs e)
        {            
            _sendOutTimeStamp.Enabled = _sendPerEmail.Checked;            
        }
	}
}
