using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace DealMaker.Forms.SchedulingActicities
{
	/// <summary>
	/// Summary description for ActivitiesList.
	/// </summary>
	public class ActivitiesList : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Label activityListPosition;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public ActivitiesList()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.activityListPosition = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// activityListPosition
			// 
			this.activityListPosition.Location = new System.Drawing.Point(8, 8);
			this.activityListPosition.Name = "activityListPosition";
			this.activityListPosition.Size = new System.Drawing.Size(384, 136);
			this.activityListPosition.TabIndex = 14;
			this.activityListPosition.Text = "activity list position (inivisible)";
			this.activityListPosition.Visible = false;
			// 
			// ActivitiesList
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(210)), ((System.Byte)(216)), ((System.Byte)(240)));
			this.ClientSize = new System.Drawing.Size(400, 181);
			this.Controls.Add(this.activityListPosition);
			this.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
			this.Name = "ActivitiesList";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Activities List";
			this.Load += new System.EventHandler(this.ActivitiesList_Load);
			this.ResumeLayout(false);

		}
		#endregion

		private void ActivitiesList_Load(object sender, System.EventArgs e)
		{
		
		}
	}
}
