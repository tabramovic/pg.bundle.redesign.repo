using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace DealMaker
{
	/// <summary>
	/// Summary description for OnwTwoThreeSoldNotification.
	/// </summary>
	public class OnwTwoThreeSoldNotification : System.Windows.Forms.Form
	{		
		private System.Windows.Forms.Button bOK;
		private System.Windows.Forms.Label lText;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label lMsg;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public OnwTwoThreeSoldNotification(int numberOfLeads)
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();			

			this.lMsg.Text = "You have just received " + numberOfLeads.ToString() + " new Lead(s)!";
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(OnwTwoThreeSoldNotification));
			this.lText = new System.Windows.Forms.Label();
			this.bOK = new System.Windows.Forms.Button();
			this.lMsg = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// lText
			// 
			this.lText.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lText.Location = new System.Drawing.Point(16, 80);
			this.lText.Name = "lText";
			this.lText.Size = new System.Drawing.Size(448, 112);
			this.lText.TabIndex = 0;
			this.lText.Text = "...";
			// 
			// bOK
			// 
			this.bOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.bOK.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.bOK.Location = new System.Drawing.Point(200, 206);
			this.bOK.Name = "bOK";
			this.bOK.TabIndex = 1;
			this.bOK.Text = "OK";
			this.bOK.Click += new System.EventHandler(this.bOK_Click);
			// 
			// lMsg
			// 
			this.lMsg.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.lMsg.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lMsg.Location = new System.Drawing.Point(8, 16);
			this.lMsg.Name = "lMsg";
			this.lMsg.Size = new System.Drawing.Size(464, 23);
			this.lMsg.TabIndex = 2;
			this.lMsg.Text = "You have just received _ new Lead(s)!";
			this.lMsg.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(8, 40);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(456, 23);
			this.label2.TabIndex = 3;
			this.label2.Text = "__________________________________________________________________";
			this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// OnwTwoThreeSoldNotification
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.BackColor = System.Drawing.Color.Red;
			this.ClientSize = new System.Drawing.Size(474, 248);
			this.Controls.Add(this.lMsg);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.bOK);
			this.Controls.Add(this.lText);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "OnwTwoThreeSoldNotification";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "ATTENTION";
			this.TopMost = true;
			this.ResumeLayout(false);

		}
		#endregion

		private void bOK_Click(object sender, System.EventArgs e)
		{
			this.Close();
		}

		public string Message
		{
			get {return this.lText.Text;}
			set {this.lText.Text = value;}
		}
	}
}
