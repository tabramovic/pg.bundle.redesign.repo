using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;
using System.Globalization;

//Added
//using ShortSaleModule.Common.RepairsControl;
using DealMaker.Repairs;

namespace DealMaker.Forms.BreakDown
{
	/// <summary>
	/// Summary description for UserControl.
	/// </summary>
	public class UserControl_BreakDown : System.Windows.Forms.UserControl
	{
		private DataTable	datatableCosts;
		private DataView	dataviewCosts;
		private DataColumn	dataColumnCostDescription;
		private DataColumn	dataColumnCost;
		private ArrayList	dataCollection;
        decimal sum = 0.0M;

		private System.Windows.Forms.Label labelTotalCost;
		TextBox textBoxTotalCost;
		private System.Windows.Forms.DataGrid dataGridCosts;
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public UserControl_BreakDown()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

			// TODO: Add any initialization after the InitializeComponent call
			datatableCosts = new DataTable("Costs");

			dataColumnCostDescription = new DataColumn("Cost Description", typeof(System.String));
			dataColumnCost = new DataColumn("Cost", typeof(System.String));            

			dataGridCosts_Resize(this, new EventArgs());
			dataGridCosts.PreferredColumnWidth = Convert.ToInt32(dataGridCosts.Width * 0.98 / 2);
			dataGridCosts.BackColor = Color.White;

			datatableCosts.Columns.Add(dataColumnCostDescription);
			datatableCosts.Columns.Add(dataColumnCost);


			dataviewCosts = new DataView(datatableCosts);
			dataviewCosts.AllowNew = false;

			dataGridCosts.DataSource = datatableCosts;
			textBoxTotalCost.Width = dataGridCosts.TableStyles["Costs"].GridColumnStyles["Cost"].Width;            

			dataCollection = new ArrayList();
		}

        

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.labelTotalCost = new System.Windows.Forms.Label();
            this.textBoxTotalCost = new System.Windows.Forms.TextBox();
            this.dataGridCosts = new System.Windows.Forms.DataGrid();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridCosts)).BeginInit();
            this.SuspendLayout();
            // 
            // labelTotalCost
            // 
            this.labelTotalCost.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.labelTotalCost.AutoSize = true;
            this.labelTotalCost.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTotalCost.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.labelTotalCost.Location = new System.Drawing.Point(92, 279);
            this.labelTotalCost.Name = "labelTotalCost";
            this.labelTotalCost.Size = new System.Drawing.Size(58, 13);
            this.labelTotalCost.TabIndex = 4;
            this.labelTotalCost.Text = "Total Cost:";
            // 
            // textBoxTotalCost
            // 
            this.textBoxTotalCost.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxTotalCost.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxTotalCost.Location = new System.Drawing.Point(159, 277);
            this.textBoxTotalCost.Name = "textBoxTotalCost";
            this.textBoxTotalCost.Size = new System.Drawing.Size(80, 20);
            this.textBoxTotalCost.TabIndex = 5;
            this.textBoxTotalCost.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // dataGridCosts
            // 
            this.dataGridCosts.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridCosts.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dataGridCosts.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.dataGridCosts.CaptionText = "Cost Breakdown:";
            this.dataGridCosts.DataMember = "";
            this.dataGridCosts.HeaderForeColor = System.Drawing.SystemColors.ControlText;
            this.dataGridCosts.Location = new System.Drawing.Point(0, 0);
            this.dataGridCosts.Name = "dataGridCosts";
            this.dataGridCosts.PreferredColumnWidth = 125;
            this.dataGridCosts.RowHeadersVisible = false;
            this.dataGridCosts.Size = new System.Drawing.Size(248, 271);
            this.dataGridCosts.TabIndex = 1;
            this.dataGridCosts.Resize += new System.EventHandler(this.dataGridCosts_Resize);
            this.dataGridCosts.CurrentCellChanged += new System.EventHandler(this.dataGridCosts_CurrentCellChanged);            
            // 
            // UserControl_BreakDown
            // 
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.Controls.Add(this.dataGridCosts);
            this.Controls.Add(this.textBoxTotalCost);
            this.Controls.Add(this.labelTotalCost);
            this.Name = "UserControl_BreakDown";
            this.Size = new System.Drawing.Size(248, 301);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridCosts)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		#endregion

        public decimal Total
        {
            get 
            {                
                return sum; 
            }
        }

        public string TotalLabel
        {
            get { return labelTotalCost.Text; }
            set { labelTotalCost.Text = value; }
        }

		private void dataGridCosts_Resize(object sender, System.EventArgs e)
		{
			DataGridTextBoxColumn columnStyleCost = new DataGridTextBoxColumn();
			columnStyleCost.TextBox.Enabled = true;
			columnStyleCost.HeaderText = dataColumnCost.ColumnName;
			columnStyleCost.MappingName = dataColumnCost.ColumnName;
			columnStyleCost.Width = textBoxTotalCost.Width - 2;
			columnStyleCost.Format = "c";            

            //columnStyleCost.Alignment = HorizontalAlignment.Right;
			//columnStyleCost.Width = dataGridCosts.Width / 4 - 2;
            

			DataGridTextBoxColumn columnStyleCostDescription = new DataGridTextBoxColumn();
			columnStyleCostDescription.TextBox.Enabled = true;
			columnStyleCostDescription.HeaderText = dataColumnCostDescription.ColumnName;
			columnStyleCostDescription.MappingName = dataColumnCostDescription.ColumnName;
			columnStyleCostDescription.Width = dataGridCosts.Width -  textBoxTotalCost.Width - 2;            
			
            //DataGridTextBoxColumn dummy = new DataGridTextBoxColumn();
            //columnStyleCostDescription.TextBox.Enabled = false;
            //columnStyleCostDescription.HeaderText = string.Empty;
            //columnStyleCostDescription.MappingName = string.Empty;
            //columnStyleCostDescription.Width = 5;            
			
			DataGridTableStyle tableStyle = new DataGridTableStyle();
			// Add the new column styles to the table style.
			// beware order!!
			tableStyle.GridColumnStyles.Add(columnStyleCostDescription);
			tableStyle.GridColumnStyles.Add(columnStyleCost);
            //tableStyle.GridColumnStyles.Add(dummy);
			tableStyle.RowHeadersVisible = false;
			// must have for Mapping 8))
			tableStyle.MappingName = datatableCosts.TableName;
		
			dataGridCosts.TableStyles.Clear();
			// Add the new table style to the data grid.
			dataGridCosts.TableStyles.Add(tableStyle);
		}                
                                
		public void dataGridCosts_CurrentCellChanged(object sender, System.EventArgs e)
		{
			decimal cell;
            sum = 0.0M;

            try
            {
                DataGrid snder = sender as DataGrid;
                if (0 != snder.CurrentCell.ColumnNumber)                                    
                    return;                
            }
            catch { }

			dataCollection.Clear();

			foreach(DataRow dr in datatableCosts.Rows)
			{
				try
				{
					cell = Decimal.Parse(dr["Cost"].ToString(), NumberStyles.Currency);
					dr["Cost"] = cell.ToString("c");
					sum += cell;
					CostItem ci = new CostItem();
					ci.CostDescription = dr["Cost Description"].ToString();
					ci.Cost = cell;
					dataCollection.Add(ci);			
				}
				catch (FormatException)
				{
					//throw fex;
					dr["Cost"] = (0.0M).ToString("c");
				}
			}
		
			textBoxTotalCost.Text = sum.ToString("c");

		}        

		public DealMaker.Repairs.CostItem[] DataArrayCostItems
		{
			set
			{
				sum = 0.0M;
				dataCollection.Clear();
                if (null != value)
                {
                    for (int i = 0; i < value.Length; i++)
                    {
                        dataCollection.Add(value[i]);
                        datatableCosts.Rows.Add(new object[] { value[i].CostDescription, value[i].Cost });
                        sum += value[i].Cost;
                    }
                }

				textBoxTotalCost.Text = sum.ToString("c");
			}

			get
			{
				CostItem[] arr_dataCollection = new CostItem[dataCollection.Count];
				dataCollection.CopyTo(arr_dataCollection);

				return arr_dataCollection;
			}
	    }                                        
	}
}
