using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;

//Added
using DealMaker.Repairs;

namespace DealMaker.Forms.BreakDown
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class FormBreakDown : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Button buttonClose;
		public UserControl_BreakDown userControl_BreakDown;
        private Label label1;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public FormBreakDown()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

        public FormBreakDown(string formLabel, string totalLabel)
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();

            this.Text = formLabel;
            this.userControl_BreakDown.TotalLabel = totalLabel;
        }

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormBreakDown));
            this.buttonClose = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.userControl_BreakDown = new UserControl_BreakDown();
            this.SuspendLayout();
            // 
            // buttonClose
            // 
            this.buttonClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonClose.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.buttonClose.Location = new System.Drawing.Point(201, 304);
            this.buttonClose.Name = "buttonClose";
            this.buttonClose.Size = new System.Drawing.Size(80, 23);
            this.buttonClose.TabIndex = 1;
            this.buttonClose.Text = "Close";
            this.buttonClose.Click += new System.EventHandler(this.buttonClose_Click);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 309);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(183, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "To add an item, click on a line below.";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // userControl_BreakDown
            // 
            this.userControl_BreakDown.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.userControl_BreakDown.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.userControl_BreakDown.DataArrayCostItems = new CostItem[0];
            this.userControl_BreakDown.Location = new System.Drawing.Point(-1, 2);
            this.userControl_BreakDown.Name = "userControl_BreakDown";
            this.userControl_BreakDown.Size = new System.Drawing.Size(292, 301);
            this.userControl_BreakDown.TabIndex = 2;
            this.userControl_BreakDown.TotalLabel = "Total Cost:";
            // 
            // FormBreakDown
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.ClientSize = new System.Drawing.Size(290, 339);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.userControl_BreakDown);
            this.Controls.Add(this.buttonClose);
            this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FormBreakDown";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "BreakDown";
            this.Closing += new System.ComponentModel.CancelEventHandler(this.FormBreakDown_Closing);
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		#endregion

		

		private void buttonClose_Click(object sender, System.EventArgs e)
		{
			this.Close();
		}

		private void FormBreakDown_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			userControl_BreakDown.dataGridCosts_CurrentCellChanged(this, new EventArgs());
			return;
		}

        private void label1_Click(object sender, EventArgs e)
        {

        }
	}
}
