﻿namespace DealMaker.ShortSale.Forms
{
    partial class SubfolderStructure
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SubfolderStructure));
            this._existingSubFolders = new System.Windows.Forms.ListView();
            this.columnHeader1 = new System.Windows.Forms.ColumnHeader();
            this._add = new System.Windows.Forms.Button();
            this._delete = new System.Windows.Forms.Button();
            this._close = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this._userDefinedSubFolders = new System.Windows.Forms.ListView();
            this.columnHeader2 = new System.Windows.Forms.ColumnHeader();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this._cancel = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // _existingSubFolders
            // 
            this._existingSubFolders.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1});
            this._existingSubFolders.GridLines = true;
            this._existingSubFolders.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this._existingSubFolders.Location = new System.Drawing.Point(6, 19);
            this._existingSubFolders.Name = "_existingSubFolders";
            this._existingSubFolders.Size = new System.Drawing.Size(252, 280);
            this._existingSubFolders.TabIndex = 0;
            this._existingSubFolders.UseCompatibleStateImageBehavior = false;
            this._existingSubFolders.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Subfolders";
            this.columnHeader1.Width = 216;
            // 
            // _add
            // 
            this._add.Location = new System.Drawing.Point(6, 276);
            this._add.Name = "_add";
            this._add.Size = new System.Drawing.Size(123, 23);
            this._add.TabIndex = 1;
            this._add.Text = "Add Subfolder";
            this._add.UseVisualStyleBackColor = true;
            // 
            // _delete
            // 
            this._delete.Location = new System.Drawing.Point(135, 276);
            this._delete.Name = "_delete";
            this._delete.Size = new System.Drawing.Size(123, 23);
            this._delete.TabIndex = 2;
            this._delete.Text = "Delete Subfolder";
            this._delete.UseVisualStyleBackColor = true;
            // 
            // _close
            // 
            this._close.DialogResult = System.Windows.Forms.DialogResult.OK;
            this._close.Location = new System.Drawing.Point(385, 426);
            this._close.Name = "_close";
            this._close.Size = new System.Drawing.Size(75, 23);
            this._close.TabIndex = 3;
            this._close.Text = "Close";
            this._close.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this._existingSubFolders);
            this.groupBox1.Location = new System.Drawing.Point(9, 113);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(265, 307);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Program defined subfolders";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this._userDefinedSubFolders);
            this.groupBox2.Controls.Add(this._add);
            this.groupBox2.Controls.Add(this._delete);
            this.groupBox2.Location = new System.Drawing.Point(283, 113);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(265, 307);
            this.groupBox2.TabIndex = 5;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "User defined subfolders";
            // 
            // _userDefinedSubFolders
            // 
            this._userDefinedSubFolders.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader2});
            this._userDefinedSubFolders.GridLines = true;
            this._userDefinedSubFolders.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this._userDefinedSubFolders.Location = new System.Drawing.Point(6, 19);
            this._userDefinedSubFolders.Name = "_userDefinedSubFolders";
            this._userDefinedSubFolders.Size = new System.Drawing.Size(252, 251);
            this._userDefinedSubFolders.TabIndex = 1;
            this._userDefinedSubFolders.UseCompatibleStateImageBehavior = false;
            this._userDefinedSubFolders.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Subfolders";
            this.columnHeader2.Width = 216;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(9, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(539, 58);
            this.label1.TabIndex = 6;
            this.label1.Text = resources.GetString("label1.Text");
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(9, 75);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(539, 34);
            this.label2.TabIndex = 7;
            this.label2.Text = "The program will automatically create “Program Defined Subfolders” and any “User " +
                "Defined Subfolder” you add for every new property.";
            // 
            // _cancel
            // 
            this._cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this._cancel.Location = new System.Drawing.Point(466, 426);
            this._cancel.Name = "_cancel";
            this._cancel.Size = new System.Drawing.Size(75, 23);
            this._cancel.TabIndex = 8;
            this._cancel.Text = "Cancel";
            this._cancel.UseVisualStyleBackColor = true;
            // 
            // SubfolderStructure
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.ClientSize = new System.Drawing.Size(557, 461);
            this.Controls.Add(this._cancel);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this._close);
            this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "SubfolderStructure";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Define the Sub-folders for each property.";
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView _existingSubFolders;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.Button _add;
        private System.Windows.Forms.Button _delete;
        private System.Windows.Forms.Button _close;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ListView _userDefinedSubFolders;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button _cancel;
    }
}