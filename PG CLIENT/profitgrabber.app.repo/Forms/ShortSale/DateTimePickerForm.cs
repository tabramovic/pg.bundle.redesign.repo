using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

//Added
using DealMaker.ShortSale.Utils;

namespace DealMaker.Forms.ShortSale
{
	/// <summary>
	/// Summary description for DateTimePickerForm.
	/// </summary>
	public class DateTimePickerForm : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Button _ok;
		private System.Windows.Forms.Button _cancel;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
		private System.Windows.Forms.MonthCalendar _monthCalendar;
		private System.Windows.Forms.Button _clear;
		private System.Windows.Forms.Button _keepCurrent;
		
		eDatePickerOptions _datePickerOption = eDatePickerOptions.NotSet;

		public DateTimePickerForm()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			_ok.Click += new EventHandler(On_Ok_Click);
			_cancel.Click += new EventHandler(On_Cancel_Click);
			_keepCurrent.Click += new EventHandler(On_KeepCurrent_Click);
			_clear.Click += new EventHandler(On_Clear_Click);
			
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this._monthCalendar = new System.Windows.Forms.MonthCalendar();
			this._ok = new System.Windows.Forms.Button();
			this._cancel = new System.Windows.Forms.Button();
			this._keepCurrent = new System.Windows.Forms.Button();
			this._clear = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// _monthCalendar
			// 
			this._monthCalendar.Location = new System.Drawing.Point(8, 8);
			this._monthCalendar.MaxSelectionCount = 1;
			this._monthCalendar.Name = "_monthCalendar";
			this._monthCalendar.TabIndex = 0;
			// 
			// _ok
			// 
			this._ok.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this._ok.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this._ok.Location = new System.Drawing.Point(248, 8);
			this._ok.Name = "_ok";
			this._ok.Size = new System.Drawing.Size(120, 23);
			this._ok.TabIndex = 1;
			this._ok.Text = "Select";
			// 
			// _cancel
			// 
			this._cancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this._cancel.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this._cancel.Location = new System.Drawing.Point(248, 128);
			this._cancel.Name = "_cancel";
			this._cancel.Size = new System.Drawing.Size(120, 23);
			this._cancel.TabIndex = 2;
			this._cancel.Text = "Cancel";
			// 
			// _keepCurrent
			// 
			this._keepCurrent.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this._keepCurrent.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this._keepCurrent.Location = new System.Drawing.Point(248, 48);
			this._keepCurrent.Name = "_keepCurrent";
			this._keepCurrent.Size = new System.Drawing.Size(120, 23);
			this._keepCurrent.TabIndex = 3;
			this._keepCurrent.Text = "Keep Current";
			// 
			// _clear
			// 
			this._clear.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this._clear.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this._clear.Location = new System.Drawing.Point(248, 88);
			this._clear.Name = "_clear";
			this._clear.Size = new System.Drawing.Size(120, 23);
			this._clear.TabIndex = 4;
			this._clear.Text = "Clear";
			// 
			// DateTimePickerForm
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(210)), ((System.Byte)(216)), ((System.Byte)(240)));
			this.ClientSize = new System.Drawing.Size(386, 176);
			this.Controls.Add(this._clear);
			this.Controls.Add(this._keepCurrent);
			this.Controls.Add(this._cancel);
			this.Controls.Add(this._ok);
			this.Controls.Add(this._monthCalendar);
			this.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
			this.Name = "DateTimePickerForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Pick a date";
			this.ResumeLayout(false);

		}
		#endregion

		public eDatePickerOptions DatePickerOption
		{
			get {return _datePickerOption;}
		}

		public string SelectedDateAsString
		{
			get { return _monthCalendar.SelectionStart.ToShortDateString(); }
		}

		public DateTime SelectedDate
		{
			get { return _monthCalendar.SelectionStart; }
		}		

		private void On_Ok_Click(object sender, EventArgs e)
		{
			_datePickerOption = eDatePickerOptions.Select;
			this.Close();
		}

		private void On_Cancel_Click(object sender, EventArgs e)
		{
			_datePickerOption = eDatePickerOptions.KeepCurrent;
			this.Close();
		}		

		private void On_KeepCurrent_Click(object sender, EventArgs e)
		{
			_datePickerOption = eDatePickerOptions.KeepCurrent;
			this.Close();
		}

		private void On_Clear_Click(object sender, EventArgs e)
		{
			_datePickerOption = eDatePickerOptions.Clear;
			this.Close();
		}

		protected override void OnClosing(CancelEventArgs e)
		{
			base.OnClosing (e);
		}
	}
}
