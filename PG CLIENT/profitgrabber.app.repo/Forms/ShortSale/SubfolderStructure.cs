﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

//Added
using DealMaker.ShortSale.Utils;
using DealMaker.PlainClasses.ShortSale.Adapters;

namespace DealMaker.ShortSale.Forms
{
    public partial class SubfolderStructure : Form
    {
        DefaultInfoValues _div = null;
        string _error = string.Empty;

        public SubfolderStructure()
        {
            InitializeComponent();

            _add.Click += new EventHandler(On_Add_Click);
            _delete.Click += new EventHandler(On_Delete_Click);
            _close.Click += new EventHandler(On_Close_Click);
                        
            DataManager.Instance.LoadDefaultInfoValues(out _div, out _error);
        }

        public DefaultInfoValues div
        {
            get { return _div; }
        }

        void On_Close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        void On_Delete_Click(object sender, EventArgs e)
        {
            if (null == _userDefinedSubFolders.SelectedItems || 0 == _userDefinedSubFolders.SelectedItems.Count)
            {
                MessageBox.Show("Select subfolder to delete", StringStore.AppName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            try { _userDefinedSubFolders.Items.Remove(_userDefinedSubFolders.SelectedItems[0]); }
            catch { }

            UpdateDefaultInfoValues();
        }

        void On_Add_Click(object sender, EventArgs e)
        {
            SetFolderName sfn = new SetFolderName();
            if (DialogResult.OK == sfn.ShowDialog())
            {
                ListViewItem lvi = new ListViewItem(sfn.FolderName);
                lvi.Tag = sfn.FolderName;
                _userDefinedSubFolders.Items.Add(lvi);
            }

            UpdateDefaultInfoValues();
        }

        void UpdateDefaultInfoValues()
        {
            List<string> newSubFolders = new List<string>();

            if (null != _userDefinedSubFolders.SelectedItems)
            {
                foreach (ListViewItem lvi in _userDefinedSubFolders.Items)
                {
                    newSubFolders.Add((string)lvi.Tag);
                }
            }

            _div.UserSubFolderStructField = newSubFolders.ToArray();
            DataManager.Instance.SaveDefaultInfoValues(_div, out _error);

        }

        protected override void OnLoad(EventArgs e)
        {
            _userDefinedSubFolders.Items.Clear();

            foreach (string customFolder in StringStore.PredefinedFolders)
            {
                ListViewItem lvi = new ListViewItem(customFolder);
                lvi.Tag = customFolder;

                _existingSubFolders.Items.Add(lvi);
            }

            if (null != _div && null != _div.UserSubFolderStructField)
            {
                foreach (string customFolder in _div.UserSubFolderStructField)
                {
                    ListViewItem lvi = new ListViewItem(customFolder);
                    lvi.Tag = customFolder;

                    _userDefinedSubFolders.Items.Add(lvi);
                }
            }

            base.OnLoad(e);
        }
    }
}
