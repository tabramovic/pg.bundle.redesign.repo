﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

//Added
using DealMaker.ShortSale.Utils;

namespace DealMaker.ShortSale.Forms
{
    public partial class CustomPopUpForm : Form
    {
        public CustomPopUpForm()
        {
            InitializeComponent();
        }

        protected override void OnLoad(EventArgs e)
        {
            this.Text = StringStore.AppName;
            base.OnLoad(e);
        }
    }
}
