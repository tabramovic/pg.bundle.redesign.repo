using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

//Added
using DealMaker.ShortSale.Utils;
using DealMaker.UserControls.ShortSale;

namespace DealMaker.Forms.ShortSale
{
	/// <summary>
	/// Summary description for TemplateEditor.
	/// </summary>
	public class TemplateEditor : System.Windows.Forms.Form
	{
        const int TimerInterval = 1000;
        private System.Windows.Forms.TabPage _DocumentView;

		LienSet _lienSet;
		SelectedDocuments _sd;
		private System.Windows.Forms.TabControl _formTabControl;
		private System.Windows.Forms.TabControl _documentTabControl;
        string _userAppFolder = string.Empty;
        Timer _timer = new Timer();
        

		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;


		public TemplateEditor()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
            _timer.Interval = TimerInterval;
            AddEventHandlers();
		}
        

        public TemplateEditor(LienSet lienSet, SelectedDocuments sd)
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();
            _lienSet = lienSet;
            _sd = sd;
            _timer.Interval = TimerInterval;


            UpdateGUI();
            AddDocuments(_sd);
            AddEventHandlers();
        }
                
		public void SetDocumentSet(LienSet lienSet, SelectedDocuments sd)
		{
			_lienSet = lienSet;
			_sd = sd;


			UpdateGUI();
			AddDocuments(_sd);
            AddEventHandlers();
		}

        void AddEventHandlers()
        {
            _documentTabControl.SelectedIndexChanged += new EventHandler(On_documentTabControl_SelectedIndexChanged);
            _timer.Tick += new EventHandler(On_Timer_Tick);
            this.Load += new EventHandler(On_TemplateEditor_Load);            
        }

        void On_Timer_Tick(object sender, EventArgs e)
        {
            _timer.Stop();
            OpenSelectedTemplate();
        }
        
        void On_TemplateEditor_Load(object sender, EventArgs e)
        {
            //OpenSelectedTemplate();
            _timer.Enabled = true;            
            _timer.Start();
        }

        void On_documentTabControl_SelectedIndexChanged(object sender, EventArgs e)
        {
            //OpenSelectedTemplate();
            _timer.Enabled = true;            
            _timer.Start();
        }

        public string UserAppFolder
        {
            get { return _userAppFolder; }
            set { _userAppFolder = value; }
        }

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TemplateEditor));
            this._formTabControl = new System.Windows.Forms.TabControl();
            this._DocumentView = new System.Windows.Forms.TabPage();
            this._documentTabControl = new System.Windows.Forms.TabControl();
            this._formTabControl.SuspendLayout();
            this._DocumentView.SuspendLayout();
            this.SuspendLayout();
            // 
            // _formTabControl
            // 
            this._formTabControl.Controls.Add(this._DocumentView);
            this._formTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this._formTabControl.Location = new System.Drawing.Point(0, 0);
            this._formTabControl.Name = "_formTabControl";
            this._formTabControl.SelectedIndex = 0;
            this._formTabControl.Size = new System.Drawing.Size(1008, 732);
            this._formTabControl.TabIndex = 0;
            // 
            // _DocumentView
            // 
            this._DocumentView.Controls.Add(this._documentTabControl);
            this._DocumentView.Location = new System.Drawing.Point(4, 22);
            this._DocumentView.Name = "_DocumentView";
            this._DocumentView.Size = new System.Drawing.Size(1000, 706);
            this._DocumentView.TabIndex = 0;
            this._DocumentView.Text = "Document View";
            // 
            // _documentTabControl
            // 
            this._documentTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this._documentTabControl.Location = new System.Drawing.Point(0, 0);
            this._documentTabControl.Name = "_documentTabControl";
            this._documentTabControl.SelectedIndex = 0;
            this._documentTabControl.Size = new System.Drawing.Size(1000, 706);
            this._documentTabControl.TabIndex = 0;
            // 
            // TemplateEditor
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.ClientSize = new System.Drawing.Size(1008, 732);
            this.Controls.Add(this._formTabControl);
            this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "TemplateEditor";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SSX Template Editor � use Right Click!";
            this._formTabControl.ResumeLayout(false);
            this._DocumentView.ResumeLayout(false);
            this.ResumeLayout(false);

		}
		#endregion

		void UpdateGUI()
		{
			switch (_lienSet)
			{
				case LienSet.FirstLienSet:
					_DocumentView.Text += " for 1st Lien Documents";
					break;

				case LienSet.SecondLienSet:
					_DocumentView.Text += " for 2nd Lien Documents";
					break;

				case LienSet.ThirdLienSet:
					_DocumentView.Text += " for 3rd Lien Documents";
					break;

				case LienSet.Generic:
					_DocumentView.Text += " for Generic Documents";
					break;
			}						
		}

		void AddDocuments(SelectedDocuments documents)
		{
            if (documents._faxCoverAuthorization)
                AddDocument(DocumentSet.FaxCoverAuthorization);
            if (documents._authorizationToRelease)
                AddDocument(DocumentSet.AuthorizationToRelease);

            if (documents._standardPacketChecklist)
                AddDocument(DocumentSet.StandardPacketChecklist);
            if (documents._faxCoverGeneral)
                AddDocument(DocumentSet.FaxCoverGeneral);

            if (documents._packetCoverLetter_Him)
                AddDocument(DocumentSet.PacketCoverLetterHim);
            if (documents._packetCoverLetter_Her)
                AddDocument(DocumentSet.PacketCoverLetterHer);
            if (documents._packetCoverLetter_They)
                AddDocument(DocumentSet.PacketCoverLetterThey);	            														
			if (documents._handshipLetter)
				AddDocument(DocumentSet.HardshipLetter);			

			if (documents._lienSpecificUserDocument1)
				AddDocument(DocumentSet.LienSpecificUserDocument1);
			if (documents._lienSpecificUserDocument2)
				AddDocument(DocumentSet.LienSpecificUserDocument2);
			if (documents._lienSpecificUserDocument3)
				AddDocument(DocumentSet.LienSpecificUserDocument3);
            if (documents._lienSpecificUserDocument4)
                AddDocument(DocumentSet.LienSpecificUserDocument4);
            if (documents._lienSpecificUserDocument5)
                AddDocument(DocumentSet.LienSpecificUserDocument5);

		}
        
		void AddDocument(DocumentSet document)
		{
			DocumentTemplate dt = new DocumentTemplate();
            dt = new DocumentTemplate();
            dt.UserAppFolder = _userAppFolder;
			
			dt.Dock = DockStyle.Fill;
            

			TabPage tp = new TabPage(SelectedDocuments.GetTabNameForDocument(document));			
			tp.Controls.Add(dt);            
			_documentTabControl.Controls.Add(tp);		
	
			switch (_lienSet)
			{
				case LienSet.FirstLienSet:
					dt.SetMailMergeTemplateReadyForOpen(_lienSet, @"\1st Lien\" + document.ToString() + ".tx");
					break;

				case LienSet.SecondLienSet:
					dt.SetMailMergeTemplateReadyForOpen(_lienSet, @"\2nd Lien\" + document.ToString() + ".tx");
					break;

				case LienSet.ThirdLienSet:
					dt.SetMailMergeTemplateReadyForOpen(_lienSet, @"\3rd Lien\" + document.ToString() + ".tx");
					break;

				case LienSet.Generic:
					dt.SetMailMergeTemplateReadyForOpen(_lienSet, @"\Generic\" + document.ToString() + ".tx");
					break;
			}

            dt.CreateControl();            
		}

        public void OpenSelectedTemplate()
        {
            DocumentTemplate dt = (DocumentTemplate)_documentTabControl.SelectedTab.Controls[0];
            dt.OpenTemplate();
        }
	}
}
