using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

//Added
using DealMaker.ShortSale.Utils;


namespace DealMaker.ShortSale.Forms
{
	/// <summary>
	/// Summary description for SaveForm.
	/// </summary>
	public class SetFolderName : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox _name;
		private System.Windows.Forms.Button _ok;
		private System.Windows.Forms.Button _cancel;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

        public SetFolderName()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
            AddEventHandlers();			
		}

        public SetFolderName(string preselectedFolder)
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();
            AddEventHandlers();

            _name.Text = preselectedFolder;
        }

        void AddEventHandlers()
        {
            _ok.Click += new EventHandler(On_ok_Click);
            _cancel.Click += new EventHandler(On_Cancel_Click);            
        }

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.label1 = new System.Windows.Forms.Label();
            this._name = new System.Windows.Forms.TextBox();
            this._ok = new System.Windows.Forms.Button();
            this._cancel = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(14, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 23);
            this.label1.TabIndex = 0;
            this.label1.Text = "Name:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // _name
            // 
            this._name.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._name.Location = new System.Drawing.Point(16, 32);
            this._name.Name = "_name";
            this._name.Size = new System.Drawing.Size(376, 20);
            this._name.TabIndex = 0;
            // 
            // _ok
            // 
            this._ok.DialogResult = System.Windows.Forms.DialogResult.OK;
            this._ok.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._ok.Location = new System.Drawing.Point(232, 64);
            this._ok.Name = "_ok";
            this._ok.Size = new System.Drawing.Size(75, 23);
            this._ok.TabIndex = 1;
            this._ok.Text = "OK";
            // 
            // _cancel
            // 
            this._cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this._cancel.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._cancel.Location = new System.Drawing.Point(320, 64);
            this._cancel.Name = "_cancel";
            this._cancel.Size = new System.Drawing.Size(75, 23);
            this._cancel.TabIndex = 2;
            this._cancel.Text = "Cancel";
            // 
            // SetFolderName
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.ClientSize = new System.Drawing.Size(410, 104);
            this.Controls.Add(this._cancel);
            this.Controls.Add(this._ok);
            this.Controls.Add(this._name);
            this.Controls.Add(this.label1);
            this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "SetFolderName";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Set subfolder name";
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		#endregion

        public string FolderName
        {
            get { return _name.Text; }
        }

		private void On_ok_Click(object sender, EventArgs e)
		{
            string regex = @"^[^ \\/:*?""<>|]+([ ]+[^ \\/:*?""<>|]+)*$";

            Match match = Regex.Match(_name.Text, regex, RegexOptions.IgnoreCase);
            if (match.Success)
                this.Close();
            else
            {
                MessageBox.Show("Selected subfolder name cannot be applied to windows folder!", StringStore.AppName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.DialogResult = DialogResult.None;
            }
		}

		private void On_Cancel_Click(object sender, EventArgs e)
		{
			this.Close();
		}
	}
}
