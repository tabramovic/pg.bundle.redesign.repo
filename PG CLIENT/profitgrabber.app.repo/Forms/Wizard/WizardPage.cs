using System;
using System.Windows.Forms;

namespace DealMaker.Forms.Wizard
{
	public delegate bool CanExitEventHandler(object sender, EventArgs e);

	public class WizardPage: UserControl
	{
		private bool entered = false;
		private string title = "Wizard page";
		private WizardForm wizardForm = null;

		public EventHandler Exited;
		public EventHandler Entered;
		public EventHandler FirstEntered;
		public CanExitEventHandler CanExit;

		public WizardPage()
		{
		}

		public WizardPage(WizardForm wizardForm)
		{
			this.wizardForm = wizardForm;
		}

		protected virtual void OnExit()
		{
			if (null != Exited)
				Exited(this, EventArgs.Empty);
		}

		protected virtual bool OnCanExit()
		{
			if (null != CanExit)
				return CanExit(this, EventArgs.Empty);
			else
				return true;
		}

		protected virtual void OnEnter()
		{
			if (!entered)
			{
				if (null != FirstEntered)
					FirstEntered(this, EventArgs.Empty);

				entered = true;
			}

			if (null != Entered)
				Entered(this, EventArgs.Empty);
		}

		public void EnterPage()
		{
			OnEnter();
		}

		public void ExitPage()
		{
			OnExit();
		}

		public bool CanExitPage()
		{
			return OnCanExit();
		}

		#region Properties
		public WizardForm WizardForm
		{
			get { return wizardForm; }
		}

		public string Title
		{
			get
			{
				return title;
			}
			set
			{
				title = value;

				if (null != wizardForm)
					wizardForm.PageTitle = title;
			}
		}
		#endregion 
	}
}