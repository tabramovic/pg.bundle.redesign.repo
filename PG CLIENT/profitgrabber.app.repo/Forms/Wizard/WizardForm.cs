using System;
using System.Drawing;
using System.Collections;
using System.Windows.Forms;
using System.ComponentModel;

namespace DealMaker.Forms.Wizard
{
	public class WizardForm: Form
	{
		private string title; 
		private int firstPageIndex = -1;
		private int currentPageIndex = -1;
		private Hashtable pages = new Hashtable();
		private WizardFormButtonStyles buttonStyles = WizardFormButtonStyles.BackButton |
			WizardFormButtonStyles.NextButton |
			WizardFormButtonStyles.CancelButton;
		private System.Windows.Forms.Button backButton;
		private System.Windows.Forms.Button nextButton;
		private System.Windows.Forms.Button cancelButton;
		private System.Windows.Forms.Button finishButton;
		private System.Windows.Forms.Panel pageContainer;
		private System.Windows.Forms.PictureBox pictureBox;
		private System.Windows.Forms.Panel titlePanel;
		private System.Windows.Forms.Label titleLabel;
		private System.Windows.Forms.GroupBox separatorBevel;

		public WizardForm()
		{
			InitializeComponent();

			Closing += new CancelEventHandler(FormClosingEventHandler);
			nextButton.Click += new EventHandler(NextButtonClick);
			backButton.Click += new EventHandler(BackButtonClick);
			cancelButton.Click += new EventHandler(CancelButtonClick);
			finishButton.Click += new EventHandler(FinishButtonClick);
		}

		#region Shitty Form Designer Code 
		private void InitializeComponent()
		{
			this.pictureBox = new System.Windows.Forms.PictureBox();
			this.finishButton = new System.Windows.Forms.Button();
			this.nextButton = new System.Windows.Forms.Button();
			this.backButton = new System.Windows.Forms.Button();
			this.separatorBevel = new System.Windows.Forms.GroupBox();
			this.pageContainer = new System.Windows.Forms.Panel();
			this.cancelButton = new System.Windows.Forms.Button();
			this.titlePanel = new System.Windows.Forms.Panel();
			this.titleLabel = new System.Windows.Forms.Label();
			this.titlePanel.SuspendLayout();
			this.SuspendLayout();
			// 
			// pictureBox
			// 
			this.pictureBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left)));
			this.pictureBox.Location = new System.Drawing.Point(8, 8);
			this.pictureBox.Name = "pictureBox";
			this.pictureBox.Size = new System.Drawing.Size(96, 160);
			this.pictureBox.TabIndex = 0;
			this.pictureBox.TabStop = false;
			// 
			// finishButton
			// 
			this.finishButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.finishButton.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.finishButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(238)));
			this.finishButton.Location = new System.Drawing.Point(336, 192);
			this.finishButton.Name = "finishButton";
			this.finishButton.Size = new System.Drawing.Size(75, 24);
			this.finishButton.TabIndex = 1;
			this.finishButton.Text = "&Finish";
			// 
			// nextButton
			// 
			this.nextButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.nextButton.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.nextButton.Location = new System.Drawing.Point(256, 192);
			this.nextButton.Name = "nextButton";
			this.nextButton.Size = new System.Drawing.Size(75, 24);
			this.nextButton.TabIndex = 2;
			this.nextButton.Text = "&Next >";
			// 
			// backButton
			// 
			this.backButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.backButton.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.backButton.Location = new System.Drawing.Point(176, 192);
			this.backButton.Name = "backButton";
			this.backButton.Size = new System.Drawing.Size(75, 24);
			this.backButton.TabIndex = 3;
			this.backButton.Text = "< &Back";
			// 
			// separatorBevel
			// 
			this.separatorBevel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.separatorBevel.Location = new System.Drawing.Point(11, 176);
			this.separatorBevel.Name = "separatorBevel";
			this.separatorBevel.Size = new System.Drawing.Size(397, 4);
			this.separatorBevel.TabIndex = 5;
			this.separatorBevel.TabStop = false;
			// 
			// pageContainer
			// 
			this.pageContainer.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.pageContainer.Location = new System.Drawing.Point(112, 48);
			this.pageContainer.Name = "pageContainer";
			this.pageContainer.Size = new System.Drawing.Size(296, 120);
			this.pageContainer.TabIndex = 6;
			// 
			// cancelButton
			// 
			this.cancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.cancelButton.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.cancelButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(238)));
			this.cancelButton.Location = new System.Drawing.Point(8, 192);
			this.cancelButton.Name = "cancelButton";
			this.cancelButton.Size = new System.Drawing.Size(75, 24);
			this.cancelButton.TabIndex = 7;
			this.cancelButton.Text = "&Cancel";
			// 
			// titlePanel
			// 
			this.titlePanel.Controls.Add(this.titleLabel);
			this.titlePanel.Location = new System.Drawing.Point(112, 8);
			this.titlePanel.Name = "titlePanel";
			this.titlePanel.Size = new System.Drawing.Size(296, 32);
			this.titlePanel.TabIndex = 8;
			// 
			// titleLabel
			// 
			this.titleLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.titleLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(238)));
			this.titleLabel.Location = new System.Drawing.Point(0, 8);
			this.titleLabel.Name = "titleLabel";
			this.titleLabel.Size = new System.Drawing.Size(280, 16);
			this.titleLabel.TabIndex = 0;
			this.titleLabel.Text = "Page Title";
			// 
			// WizardForm
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(418, 230);
			this.Controls.Add(this.titlePanel);
			this.Controls.Add(this.cancelButton);
			this.Controls.Add(this.pageContainer);
			this.Controls.Add(this.backButton);
			this.Controls.Add(this.nextButton);
			this.Controls.Add(this.finishButton);
			this.Controls.Add(this.pictureBox);
			this.Controls.Add(this.separatorBevel);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "WizardForm";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Wizard";
			this.titlePanel.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion 

		private void SetCurrentPage(int pageIndex)
		{
			WizardPage newPage = pages[pageIndex] as WizardPage;
			if (null == newPage)
				return;

			if (null != CurrentPage && pageIndex > currentPageIndex)
			{
				this.Cursor = Cursors.WaitCursor;

				if (!CurrentPage.CanExitPage())
				{
					this.Cursor = Cursors.Default;
					return;
				}

				this.Cursor = Cursors.WaitCursor;

				CurrentPage.ExitPage();

				this.Cursor = Cursors.Default;
			}

			pageContainer.SuspendLayout();

			if (pageIndex == firstPageIndex)
			{
				int left = 112;
				int width = ClientRectangle.Width - left - 8;

				pageContainer.Left = titlePanel.Left = left;
				pageContainer.Width = titlePanel.Width = width;
				pictureBox.Visible = true;
			}
			else
			{
				int left = 8;
				int width = ClientRectangle.Width - left - 8;

				pageContainer.Left = titlePanel.Left = left;
				pageContainer.Width = titlePanel.Width = width;
				pictureBox.Visible = false;
			}

			pageContainer.Controls.Clear();
			pageContainer.Controls.Add(newPage);

			newPage.Dock = DockStyle.Fill;
			PageTitle = newPage.Title;

			pageContainer.ResumeLayout(true);

			currentPageIndex = pageIndex;

			backButton.Enabled = currentPageIndex > firstPageIndex;
			nextButton.Enabled = currentPageIndex < pages.Count - 1;

			this.Refresh();

			this.Cursor = Cursors.WaitCursor;

			newPage.EnterPage();

			this.Cursor = Cursors.Default;
		}

		private void UpdateButtonPositions()
		{
			int iLocation;
			Button[] buttons = new Button[] {backButton, nextButton, finishButton};

			if (!this.Visible)
				return;

			backButton.Visible = (buttonStyles & WizardFormButtonStyles.BackButton) > 0;
			nextButton.Visible = (buttonStyles & WizardFormButtonStyles.NextButton) > 0;
			cancelButton.Visible = (buttonStyles & WizardFormButtonStyles.CancelButton) > 0;
			finishButton.Visible = (buttonStyles & WizardFormButtonStyles.FinishButton) > 0;

			iLocation = finishButton.Left;
			for (int iButton = buttons.Length - 1; iButton >= 0; iButton--)
			{
				if (!buttons[iButton].Visible)
					continue;
					
				buttons[iButton].Left = iLocation;

				iLocation -= finishButton.Width + 4;
			}
		}

		private bool ConfirmClose()
		{
			return DialogResult.Yes == 
				MessageBox.Show("Are you sure you want to exit Export Wizard?",
				this.Title, 
				MessageBoxButtons.YesNo,
				MessageBoxIcon.Question);
		}

		private void FormClosingEventHandler(object sender, CancelEventArgs e)
		{
			e.Cancel = !ConfirmClose();
		}
		private void NextButtonClick(object sender, EventArgs e)
		{
			CurrentPageIndex += 1;
		}

		private void BackButtonClick(object sender, EventArgs e)
		{
			CurrentPageIndex -= 1;
		}

		private void CancelButtonClick(object sender, EventArgs e)
		{
			this.DialogResult = DialogResult.Cancel;
		}


		private void FinishButtonClick(object sender, EventArgs e)
		{
			this.DialogResult = DialogResult.OK;
		}

		public void AddPage(WizardPage page)
		{
			pages[pages.Count] = page;
			if (-1 == firstPageIndex)
				firstPageIndex = pages.Count - 1;
		}

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);

			UpdateButtonPositions();
		}

		public DialogResult Execute()
		{
			CurrentPageIndex = FirstPageIndex;

			return this.ShowDialog();
		}

		#region Properties
		public Image Image
		{
			get
			{
				return pictureBox.Image;
			}
			set
			{
				pictureBox.Image = value;
			}
		}
		public WizardPage this [int page]
		{
			get
			{
				return pages[page] as WizardPage;
			}
		}

		public WizardPage CurrentPage
		{
			get
			{
				return pages[currentPageIndex] as WizardPage;
			}
		}

		public int CurrentPageIndex
		{
			get
			{
				return currentPageIndex;
			}
			set
			{
				SetCurrentPage(value);
			}
		}

		public int FirstPageIndex
		{
			get
			{
				return firstPageIndex;
			}
			set
			{
				if (value < 0 || value >= pages.Count)
					throw new System.Exception("Invalid value for page index");

				firstPageIndex = value;
			}
		}

		public string Title
		{
			get 
			{ 
				return title;
			}
			set 
			{
				title = value;
				this.Text = title;
			}
		}

		public WizardFormButtonStyles ButtonStyles
		{
			get
			{
				return buttonStyles;
			}
			set
			{
				buttonStyles = value;
				UpdateButtonPositions();
			}
		}

		public string PageTitle
		{
			get 
			{
				return titleLabel.Text;
			}
			set
			{
				titleLabel.Text = value;
			}
		}

		public Button BackButton
		{
			get { return backButton; }
		}

		public Button NextButton
		{
			get { return nextButton; }
		}

		public new Button CancelButton
		{
			get { return cancelButton; }
		}

		public Button FinishButton
		{
			get { return finishButton; }
		}
		#endregion
	}
}