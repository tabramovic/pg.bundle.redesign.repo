using System;

namespace DealMaker.Forms.Wizard
{
	[FlagsAttribute]
	public enum WizardFormButtonStyles
	{
		HelpButton = 1, 
		BackButton = 2, 
		NextButton = 4, 
		CancelButton = 8, 
		FinishButton = 16
	}
}