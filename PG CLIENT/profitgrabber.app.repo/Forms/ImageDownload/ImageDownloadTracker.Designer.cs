﻿namespace DealMaker
{
    partial class ImageDownloadTracker
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._CancelClose = new System.Windows.Forms.Button();
            this._DownloadLabel = new System.Windows.Forms.Label();
            this._pb = new System.Windows.Forms.ProgressBar();
            this.SuspendLayout();
            // 
            // _CancelClose
            // 
            this._CancelClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._CancelClose.Location = new System.Drawing.Point(417, 76);
            this._CancelClose.Name = "_CancelClose";
            this._CancelClose.Size = new System.Drawing.Size(75, 23);
            this._CancelClose.TabIndex = 0;
            this._CancelClose.Text = "Cancel";
            this._CancelClose.UseVisualStyleBackColor = true;
            // 
            // _DownloadLabel
            // 
            this._DownloadLabel.AutoSize = true;
            this._DownloadLabel.Location = new System.Drawing.Point(12, 9);
            this._DownloadLabel.Name = "_DownloadLabel";
            this._DownloadLabel.Size = new System.Drawing.Size(106, 13);
            this._DownloadLabel.TabIndex = 1;
            this._DownloadLabel.Text = "Downloading Images";
            // 
            // _pb
            // 
            this._pb.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this._pb.Location = new System.Drawing.Point(15, 35);
            this._pb.Name = "_pb";
            this._pb.Size = new System.Drawing.Size(477, 23);
            this._pb.TabIndex = 2;
            // 
            // ImageDownloadTracker
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.ClientSize = new System.Drawing.Size(504, 111);
            this.Controls.Add(this._pb);
            this.Controls.Add(this._DownloadLabel);
            this.Controls.Add(this._CancelClose);
            this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Name = "ImageDownloadTracker";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Downloading Images";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button _CancelClose;
        private System.Windows.Forms.Label _DownloadLabel;
        private System.Windows.Forms.ProgressBar _pb;
    }
}