﻿using System;
using System.Collections.Generic;
using System.Text;

//Added
using System.IO;
using System.Drawing;
using System.Threading;
using DealMaker.ImagingProxyService;

namespace DealMaker
{
    public class ImageDownloader : IImageDownloader
    {
        public delegate void ImageDownloadedHandler(int iPos, string address, bool success);
        public static event ImageDownloadedHandler OnImageDownloaded;
        
        bool _continueDownload = true;
        List<string> _addresses = null;
        List<Image> _images = new List<Image>();
        MapType _mapType = MapType.StreetView;
        int _width;
        int _height;
        int _zoom = 18;
        int _heading = 0;

        public List<Image> Images
        {
            get { return _images; }
        }

        public ImageDownloader(List<string> addresses)
        {
            _addresses = addresses;
            _width = 150;
            _height = 125;
        }

        public void StartDownload()
        {
            Thread th = new Thread(DownloadThread);
            th.Start(_addresses);
            
        }

        protected virtual void FireEvent(int iPos, string address, bool success)
        {
            if (OnImageDownloaded != null)
                OnImageDownloaded(iPos, address, success);
        }

        void DownloadThread(object data)
        {
            List<string> addresses = (List<string>)data;

            bool correctAddress;
            double longtitude;
            double latitude;
            string err;
            bool res;            

            if (null == addresses || 0 == addresses.Count)
                return;
                        
            GoogleImagingProxyService gIPS = new GoogleImagingProxyService();

            if (_continueDownload)
            {
                bool anyAddressFound;
                byte[] imgArr = gIPS.GetImageWithMarkers(MapType.satellite, 16/*_zoom*/, 750, 260, addresses.ToArray(), out anyAddressFound);

                if (null != imgArr)
                {
                    MemoryStream ms = new MemoryStream(imgArr);
                    Image img = Image.FromStream(ms);

                    _images.Add(img);
                }
            }
                        
            for (int i = 0; i < addresses.Count; i++)
            {
                if (!_continueDownload)
                    break;

                string addr = addresses[i];
                if (null != addr && string.Empty != addr)
                {
                    try
                    {
                        res = gIPS.GetGeoCode(addresses[i], out correctAddress, out err, out longtitude, out latitude);

                        if (res)
                        {
                            byte[] imgArr = gIPS.GetImage(_mapType, _zoom, _width, _height, longtitude, latitude, _heading);

                            MemoryStream ms = new MemoryStream(imgArr);
                            Image img = Image.FromStream(ms);

                            _images.Add(img);
                            FireEvent(i, addresses[i], true);
                        }
                        else
                        {
                            _images.Add(null);
                            FireEvent(i, addresses[i], false);
                        }
                    }
                    catch
                    {
                        _images.Add(null);
                        FireEvent(i, addresses[i], false);
                    }
                }
                else
                {
                    _images.Add(null);
                    FireEvent(i, addresses[i], false);
                }                
            }
            
        }
        
        
        #region IImageDownloader Members
        public void StopDownload()
        {
            _continueDownload = false;
        }
        #endregion


    }
}
