﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace DealMaker
{
    public partial class ImageDownloadTracker : Form
    {
        int _cnt = 1;
        int _nrOfImagesToDownload;

        public IImageDownloader ImgDownloader { get; set; }

        public ImageDownloadTracker(int nrOfImagesToDownload)
        {            
            InitializeComponent();
            AddEventHandlers();

            _nrOfImagesToDownload = nrOfImagesToDownload;
            _pb.Maximum = nrOfImagesToDownload;
            _cnt = 1;
        }

        void AddEventHandlers()
        {
            _CancelClose.Click += new EventHandler(On_CancelClose_Click);
            ImageDownloader.OnImageDownloaded += new ImageDownloader.ImageDownloadedHandler(On_ImageDownloader_OnImageDownloaded);

        }
        
        void On_CancelClose_Click(object sender, EventArgs e)
        {
            ImgDownloader.StopDownload();
            CloseForm();
        }
                

        void On_ImageDownloader_OnImageDownloaded(int iPos, string address, bool success)
        {
            FillProgressBar(address, success);
        }

        void CloseForm()
        {            
            ImageDownloader.OnImageDownloaded -= new ImageDownloader.ImageDownloadedHandler(On_ImageDownloader_OnImageDownloaded);
            this.Close();
        }

        delegate void FillProgressBarDelegate(string address, bool success);
        private void FillProgressBar(string address, bool success)
        {
            if (InvokeRequired)
                BeginInvoke(new FillProgressBarDelegate(FillProgressBar), new object[] { address, success });

            else
            {
                if (_cnt <= _pb.Maximum)
                {
                    _pb.Value = _cnt;
                    _cnt++;
                }

                if (success)
                    _DownloadLabel.Text = address + ": " + _cnt.ToString() + "/" + _nrOfImagesToDownload.ToString() + " downloaded successfully.";
                else
                    _DownloadLabel.Text = address + ": " + _cnt.ToString() + "/" + _nrOfImagesToDownload.ToString() + " download failed.";

                if (_cnt == _nrOfImagesToDownload)
                    CloseForm();
            }
        }
    }
}
