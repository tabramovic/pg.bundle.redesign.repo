#region � Using �
using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
#endregion � Using �
#region � Namespace DealMaker �
namespace DealMaker
{
	#region � Class RetMailProcMask2 �
	/// <summary>
	/// Summary description for ReturnMailWizardForm2.
	/// </summary>
	public class RetMailProcMask2 : System.Windows.Forms.UserControl
	{
		#region � Data �
		private ReturnMailWizardObject rmwo = null;
		ArrayList alPropertyItemIds = new ArrayList();
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox tbScan;
		private System.Windows.Forms.ListView lvPropertyItems;
		private System.Windows.Forms.ColumnHeader chTag;
		private System.Windows.Forms.ColumnHeader chFirstEntered;
		private System.Windows.Forms.ColumnHeader chGroup;
		private System.Windows.Forms.ColumnHeader chFullName;
		private System.Windows.Forms.ColumnHeader chSiteStreetAddress;
		private System.Windows.Forms.ColumnHeader chSiteCityState;
		private System.Windows.Forms.ColumnHeader chSiteZIP;
		private System.Windows.Forms.ColumnHeader chLKMailingStreetAddress;
		private System.Windows.Forms.ColumnHeader chLKMailingCityState;
		private System.Windows.Forms.ColumnHeader chLKMailingZIP;
		private System.Windows.Forms.ColumnHeader chPKMailingStreetAddress;
		private System.Windows.Forms.ColumnHeader chPKMailingCityState;
		private System.Windows.Forms.ColumnHeader chPKMailingZIP;
		private System.Windows.Forms.ColumnHeader chStatusGroup;
		private System.Windows.Forms.Button bDeleteTagged;
		private System.Windows.Forms.Button bTag;
		private System.Windows.Forms.Button uUnTag;
		private System.Windows.Forms.Button bUntagAll;
		private System.Windows.Forms.Button bTagAll;
		private System.Windows.Forms.Label label2;
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
		#endregion � Data �
		#region � CTR �
		public RetMailProcMask2(ReturnMailWizardObject rmwo)
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();
			
			this.GetPropertyItemIds();
			this.rmwo = rmwo;

			RemoveAllPropertyItemsFromRMWO();

			this.lvPropertyItems.Items.Clear();
			if (null != this.RMWO.OriginalSelectionPropertyItemIds && 0 != this.RMWO.OriginalSelectionPropertyItemIds.Count)
			{
				foreach (Guid propertyItemId in this.RMWO.OriginalSelectionPropertyItemIds)
				{
					this.FillRow(propertyItemId.ToString(), false);
				}
			}

			this.tbScan.Focus();
			this.tbScan.Select();			
		}
		#endregion � CTR �
		#region � Properties �
		#region � RMWO �
		public ReturnMailWizardObject RMWO
		{
			get { return this.rmwo; }
			set { this.rmwo = value; }
		}
		#endregion � RMWO �

		public TextBox TB_Scan
		{
			get {return this.tbScan;}
			set {this.tbScan = value;}
		}
		#endregion � Properties �
		#region � Dispose �
		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}
		#endregion � Dispose �
		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.tbScan = new System.Windows.Forms.TextBox();
			this.lvPropertyItems = new System.Windows.Forms.ListView();
			this.chTag = new System.Windows.Forms.ColumnHeader();
			this.chFirstEntered = new System.Windows.Forms.ColumnHeader();
			this.chGroup = new System.Windows.Forms.ColumnHeader();
			this.chFullName = new System.Windows.Forms.ColumnHeader();
			this.chSiteStreetAddress = new System.Windows.Forms.ColumnHeader();
			this.chSiteCityState = new System.Windows.Forms.ColumnHeader();
			this.chSiteZIP = new System.Windows.Forms.ColumnHeader();
			this.chLKMailingStreetAddress = new System.Windows.Forms.ColumnHeader();
			this.chLKMailingCityState = new System.Windows.Forms.ColumnHeader();
			this.chLKMailingZIP = new System.Windows.Forms.ColumnHeader();
			this.chPKMailingStreetAddress = new System.Windows.Forms.ColumnHeader();
			this.chPKMailingCityState = new System.Windows.Forms.ColumnHeader();
			this.chPKMailingZIP = new System.Windows.Forms.ColumnHeader();
			this.chStatusGroup = new System.Windows.Forms.ColumnHeader();
			this.bDeleteTagged = new System.Windows.Forms.Button();
			this.bTag = new System.Windows.Forms.Button();
			this.uUnTag = new System.Windows.Forms.Button();
			this.bUntagAll = new System.Windows.Forms.Button();
			this.bTagAll = new System.Windows.Forms.Button();
			this.label2 = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(8, 16);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(144, 23);
			this.label1.TabIndex = 0;
			this.label1.Text = "Scan-In the Returned Mail:";
			// 
			// tbScan
			// 
			this.tbScan.Location = new System.Drawing.Point(141, 12);
			this.tbScan.Name = "tbScan";
			this.tbScan.Size = new System.Drawing.Size(144, 20);
			this.tbScan.TabIndex = 1;
			this.tbScan.Text = "";
			this.tbScan.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbScan_KeyPress);
			// 
			// lvPropertyItems
			// 
			this.lvPropertyItems.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.lvPropertyItems.CheckBoxes = true;
			this.lvPropertyItems.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
																							  this.chTag,
																							  this.chFirstEntered,
																							  this.chGroup,
																							  this.chFullName,
																							  this.chSiteStreetAddress,
																							  this.chSiteCityState,
																							  this.chSiteZIP,
																							  this.chLKMailingStreetAddress,
																							  this.chLKMailingCityState,
																							  this.chLKMailingZIP,
																							  this.chPKMailingStreetAddress,
																							  this.chPKMailingCityState,
																							  this.chPKMailingZIP,
																							  this.chStatusGroup});
			this.lvPropertyItems.FullRowSelect = true;
			this.lvPropertyItems.GridLines = true;
			this.lvPropertyItems.HoverSelection = true;
			this.lvPropertyItems.Location = new System.Drawing.Point(8, 56);
			this.lvPropertyItems.MultiSelect = false;
			this.lvPropertyItems.Name = "lvPropertyItems";
			this.lvPropertyItems.Size = new System.Drawing.Size(560, 192);
			this.lvPropertyItems.TabIndex = 2;
			this.lvPropertyItems.View = System.Windows.Forms.View.Details;
			// 
			// chTag
			// 
			this.chTag.Text = "Tag";
			this.chTag.Width = 48;
			// 
			// chFirstEntered
			// 
			this.chFirstEntered.Text = "First Entered";
			this.chFirstEntered.Width = 82;
			// 
			// chGroup
			// 
			this.chGroup.Text = "Group";
			this.chGroup.Width = 100;
			// 
			// chFullName
			// 
			this.chFullName.Text = "Full Name";
			this.chFullName.Width = 100;
			// 
			// chSiteStreetAddress
			// 
			this.chSiteStreetAddress.Text = "Site Street Address";
			this.chSiteStreetAddress.Width = 100;
			// 
			// chSiteCityState
			// 
			this.chSiteCityState.Text = "Site City, State";
			this.chSiteCityState.Width = 100;
			// 
			// chSiteZIP
			// 
			this.chSiteZIP.Text = "Site ZIP";
			this.chSiteZIP.Width = 100;
			// 
			// chLKMailingStreetAddress
			// 
			this.chLKMailingStreetAddress.Text = "LK Mailing Street Address";
			this.chLKMailingStreetAddress.Width = 100;
			// 
			// chLKMailingCityState
			// 
			this.chLKMailingCityState.Text = "LK Mailing City, State";
			this.chLKMailingCityState.Width = 100;
			// 
			// chLKMailingZIP
			// 
			this.chLKMailingZIP.Text = "LK Mailing ZIP";
			this.chLKMailingZIP.Width = 100;
			// 
			// chPKMailingStreetAddress
			// 
			this.chPKMailingStreetAddress.Text = "PK Mailing Street Address";
			this.chPKMailingStreetAddress.Width = 100;
			// 
			// chPKMailingCityState
			// 
			this.chPKMailingCityState.Text = "PK Mailing City, State";
			this.chPKMailingCityState.Width = 100;
			// 
			// chPKMailingZIP
			// 
			this.chPKMailingZIP.Text = "PK Mailing ZIP";
			this.chPKMailingZIP.Width = 100;
			// 
			// chStatusGroup
			// 
			this.chStatusGroup.Text = "Status Group ";
			this.chStatusGroup.Width = 100;
			// 
			// bDeleteTagged
			// 
			this.bDeleteTagged.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.bDeleteTagged.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.bDeleteTagged.Location = new System.Drawing.Point(464, 256);
			this.bDeleteTagged.Name = "bDeleteTagged";
			this.bDeleteTagged.Size = new System.Drawing.Size(104, 23);
			this.bDeleteTagged.TabIndex = 3;
			this.bDeleteTagged.Text = "Delete Tagged";
			this.bDeleteTagged.Click += new System.EventHandler(this.bDeleteTagged_Click);
			// 
			// bTag
			// 
			this.bTag.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.bTag.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.bTag.Location = new System.Drawing.Point(8, 256);
			this.bTag.Name = "bTag";
			this.bTag.Size = new System.Drawing.Size(104, 23);
			this.bTag.TabIndex = 4;
			this.bTag.Text = "Tag";
			this.bTag.Click += new System.EventHandler(this.bTag_Click);
			// 
			// uUnTag
			// 
			this.uUnTag.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.uUnTag.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.uUnTag.Location = new System.Drawing.Point(128, 256);
			this.uUnTag.Name = "uUnTag";
			this.uUnTag.Size = new System.Drawing.Size(104, 23);
			this.uUnTag.TabIndex = 5;
			this.uUnTag.Text = "Untag";
			this.uUnTag.Click += new System.EventHandler(this.uUnTag_Click);
			// 
			// bUntagAll
			// 
			this.bUntagAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.bUntagAll.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.bUntagAll.Location = new System.Drawing.Point(352, 256);
			this.bUntagAll.Name = "bUntagAll";
			this.bUntagAll.Size = new System.Drawing.Size(104, 23);
			this.bUntagAll.TabIndex = 7;
			this.bUntagAll.Text = "Untag All";
			this.bUntagAll.Click += new System.EventHandler(this.bUntagAll_Click);
			// 
			// bTagAll
			// 
			this.bTagAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.bTagAll.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.bTagAll.Location = new System.Drawing.Point(240, 256);
			this.bTagAll.Name = "bTagAll";
			this.bTagAll.Size = new System.Drawing.Size(104, 23);
			this.bTagAll.TabIndex = 6;
			this.bTagAll.Text = "Tag All";
			this.bTagAll.Click += new System.EventHandler(this.bTagAll_Click);
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(290, 16);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(296, 32);
			this.label2.TabIndex = 8;
			this.label2.Text = "* Please click in the white field on the left before scanning!";
			// 
			// RetMailProcMask2
			// 
			this.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(210)), ((System.Byte)(216)), ((System.Byte)(240)));
			this.Controls.Add(this.tbScan);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.bUntagAll);
			this.Controls.Add(this.bTagAll);
			this.Controls.Add(this.uUnTag);
			this.Controls.Add(this.bTag);
			this.Controls.Add(this.bDeleteTagged);
			this.Controls.Add(this.lvPropertyItems);
			this.Controls.Add(this.label1);
			this.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.Name = "RetMailProcMask2";
			this.Size = new System.Drawing.Size(592, 288);
			this.ResumeLayout(false);

		}
		#endregion
		#region � Methods �
		private void FillRow(string propertyItemId, bool appendData)
		{
            if (IsAlreadyPresent(new Guid((string)propertyItemId)))
            {
                MessageBox.Show("Record has already been scanned!", "ProfitGrabber Pro", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

			ArrayList alGroupMembership = ApplicationHelper.GetGroupMembershipData(new Guid((string) propertyItemId));
			string statusGroupMembership =  ApplicationHelper.GetStatusGroupMembership(new Guid((string) propertyItemId));
			PropertyItem pi = PropertyItem.LoadPropertyItem(new Guid((string) propertyItemId));
			ListViewItem lvi = new ListViewItem(string.Empty);
			lvi.SubItems.Add(pi.DateCreated.ToShortDateString());
					
			if (alGroupMembership.Count == 0)
				lvi.SubItems.Add(string.Empty); //group
			else
			{
				int i = 0;
				string groups = string.Empty;
				foreach (string s in alGroupMembership)
				{
					if (0 == i)
						groups += s;
					else if (i == alGroupMembership.Count - 1)
						groups += s;
					else
						groups += s + ", ";
					i++;
				}
				lvi.SubItems.Add(groups); //group
			}

			if (null != pi.Owner)
				lvi.SubItems.Add(pi.Owner.FullName); //4.	Full Name � contact full name
			else
				lvi.SubItems.Add(string.Empty); //4.	Full Name � contact full name
					
			if (null != pi.Owner && null != pi.Owner.SiteAddress)
			{
				lvi.SubItems.Add(pi.Owner.SiteAddress.FullSiteStreetAddress); //5.	Site Street Address
				lvi.SubItems.Add(pi.Owner.SiteAddress.SiteCitySiteState); //6.	Site City, State � like:  Los Angeles, CA
				lvi.SubItems.Add(pi.Owner.SiteAddress.SiteZIP); //7.	Site ZIP
			}
			else
			{
				lvi.SubItems.Add(string.Empty); //5.	Site Street Address
				lvi.SubItems.Add(string.Empty); //6.	Site City, State � like:  Los Angeles, CA
				lvi.SubItems.Add(string.Empty); //7.	Site ZIP
			}

			if (null != pi.Owner && null != pi.Owner.MailAddress)
			{
				lvi.SubItems.Add(pi.Owner.MailAddress.FullSiteStreetAddress); //11.	PK Mailing Street Address
				lvi.SubItems.Add(pi.Owner.MailAddress.SiteCitySiteState); //12.	PK Mailing City, State
				lvi.SubItems.Add(pi.Owner.MailAddress.SiteZIP); //13.	PK Mailing ZIP
			}
			else
			{
				lvi.SubItems.Add(string.Empty); //11.	PK Mailing Street Address
				lvi.SubItems.Add(string.Empty); //12.	PK Mailing City, State
				lvi.SubItems.Add(string.Empty); //13.	PK Mailing ZIP
			}

			if (null != pi.Owner && null != pi.Owner.SecondAddress)
			{
				lvi.SubItems.Add(pi.Owner.SecondAddress.FullSiteStreetAddress); //8.	LK Mailing Street Address
				lvi.SubItems.Add(pi.Owner.SecondAddress.SiteCitySiteState); //9.	LK Mailing City, State
				lvi.SubItems.Add(pi.Owner.SecondAddress.SiteZIP); //10.	LK Mailing ZIP
			}
			else
			{
				lvi.SubItems.Add(string.Empty); //8.	LK Mailing Street Address
				lvi.SubItems.Add(string.Empty); //9.	LK Mailing City, State
				lvi.SubItems.Add(string.Empty); //10.	LK Mailing ZIP
			}
					
			lvi.SubItems.Add(statusGroupMembership); //14.	Status Group � if contact is not assigned to any status group, it should say �not assigned�					
			lvi.Tag = new Guid((string) propertyItemId);
			
			if (true == appendData)
			{			
				//this.RMWO.OriginalSelectionPropertyItems.Add(pi);
				//this.RMWO.OriginalSelectionPropertyItemIds.Add((Guid)pi.IdPropertyItem);
				lvi.Checked = true;
			}

			this.lvPropertyItems.Items.Add(lvi);
		}

        bool IsAlreadyPresent(Guid piId)
        {
            foreach (ListViewItem lvi in lvPropertyItems.Items)
            {
                if ((Guid)lvi.Tag == piId)
                    return true;
            }

            return false;
        }

		#region � tbScan_KeyPress �
		private void tbScan_KeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
		{
			TextBox tb = (TextBox)sender;
			string partialId = tb.Text;
			
			ArrayList alFoundItemId = this.GetPropertyItemId(partialId);
			
			if ((char)Keys.Enter == e.KeyChar)
			{
				if (0 == alFoundItemId.Count)
				{
					MessageBox.Show(this, "Record does not exist in ProfitGrabber Pro Database!", "Record does not exist!", MessageBoxButtons.OK, MessageBoxIcon.Information);
					return;
				}
				else if (1 == alFoundItemId.Count)
				{
					this.FillRow((string)alFoundItemId[0], true);
				}
				else
				{
					ReturnMailWizardResolveAmb rmwra = new ReturnMailWizardResolveAmb();
					rmwra.PropertyItems = alFoundItemId;
					rmwra.ShowDialog();
					if (true == rmwra.Valid)
					{
						this.FillRow(rmwra.SelectedGuid.ToString(), true);
					}
				}
				tb.Text = string.Empty;
			}
		}
		#endregion � tbScan_KeyPress �
		#region � GetPropertyItemId �
		private ArrayList GetPropertyItemId(string partialId)
		{
			ArrayList alFound = new ArrayList();
			foreach (string s in this.alPropertyItemIds)
			{
				string originator = s.Substring(0, 6);
				if (originator.ToUpper() == partialId.ToUpper())
					alFound.Add(s);				
			}
			return alFound;
		}
		#endregion � GetPropertyItemId �
		#region � GetPropertyItemIds �
		private IList GetPropertyItemIds()
		{
			this.Cursor = Cursors.WaitCursor;
			alPropertyItemIds.Clear();
			SqlDataReader reader = null;
			try
			{						
				SqlCommand sp_GetPropertyItemIds = new SqlCommand("GetPropertyItemIds", Connection.getInstance().HatchConn);
				sp_GetPropertyItemIds.CommandType = CommandType.StoredProcedure;	

				if (ConnectionState.Open != Connection.getInstance().HatchConn.State)
					Connection.getInstance().HatchConn.Open();

				reader = sp_GetPropertyItemIds.ExecuteReader();
				while (reader.Read())
				{
					Guid propertyItemId = Guid.Empty;
					if (DBNull.Value != reader[0])
						propertyItemId = reader.GetGuid(0);
					alPropertyItemIds.Add(propertyItemId.ToString());
				}
			}
			catch (Exception exc)
			{
				MessageBox.Show(exc.Message);
			}
			reader.Close();
			this.Cursor = Cursors.Default;
			//MessageBox.Show(alPropertyItemIds.Count.ToString());
			return alPropertyItemIds;			
		}
		#endregion � GetPropertyItemIds �
		#region � bDeleteTagged_Click �
		private void bDeleteTagged_Click(object sender, System.EventArgs e)
		{
			ArrayList alPropertyItemsToDelete = new ArrayList();
			foreach (ListViewItem lvi in this.lvPropertyItems.Items)
			{
				if (true == lvi.Checked)
				{
					alPropertyItemsToDelete.Add((Guid)lvi.Tag);
				}
			}
			bool bUpdate = false;
			ApplicationHelper.DeleteTaggedPropertyItems(alPropertyItemsToDelete, out bUpdate);
			if (true == bUpdate)
			{				
				foreach (Guid piId in alPropertyItemsToDelete)
				{					
					foreach (ListViewItem lvi in this.lvPropertyItems.Items)
					{
						if ((Guid)lvi.Tag == piId)
						{
							lvi.Remove();
						}
					}
				}

				/*ArrayList alPropertItems = (ArrayList)this.RMWO.PropertyItems.Clone();
				foreach (ListViewItem lvi in this.lvPropertyItems.Items)
				{
					if (true == lvi.Checked)
					{
						Guid propertyItemId = (Guid)lvi.Tag;
						foreach (PropertyItem pi in this.RMWO.PropertyItems)
						{
							if (pi.IdPropertyItem == propertyItemId)
								alPropertItems.Remove(pi);
						}
						lvi.Remove();
					}
				}
				this.RMWO.PropertyItems = alPropertItems;*/
			}
		}
		#endregion � bDeleteTagged_Click �
		#region � bTag_Click �
		private void bTag_Click(object sender, System.EventArgs e)
		{
			if (null != this.lvPropertyItems.SelectedItems)
			{
				foreach (ListViewItem lvi in this.lvPropertyItems.SelectedItems)
				{
					lvi.Checked = true;
				}
			}
		}
		#endregion � bTag_Click �
		#region � uUnTag_Click �
		private void uUnTag_Click(object sender, System.EventArgs e)
		{
			if (null != this.lvPropertyItems.Items)
			{
				foreach (ListViewItem lvi in this.lvPropertyItems.SelectedItems)
				{
					lvi.Checked = false;
				}
			}
		}
		#endregion � uUnTag_Click �
		#region � bTagAll_Click �
		private void bTagAll_Click(object sender, System.EventArgs e)
		{			
			foreach (ListViewItem lvi in this.lvPropertyItems.Items)
			{
				lvi.Checked = true;								
			}			
		}
		#endregion � bTagAll_Click �
		#region � bUntagAll_Click �
		private void bUntagAll_Click(object sender, System.EventArgs e)
		{
			foreach (ListViewItem lvi in this.lvPropertyItems.Items)
			{
				lvi.Checked = false;
			}			
		}
		#endregion � bUntagAll_Click �

		private void RemoveAllPropertyItemsFromRMWO()
		{
			if (null != this.rmwo)
			{
				if (null != this.rmwo.PropertyItemIds)
					this.rmwo.PropertyItems.Clear();

				if (null != this.rmwo.PropertyItemIds)
					this.rmwo.PropertyItemIds.Clear();                
			}
		}
		
		
		
		public void AddTaggedContactsToRMWO()
		{
			foreach (ListViewItem lvi in this.lvPropertyItems.Items)
			{
				Guid piId = (Guid)lvi.Tag;
				PropertyItem pi = ApplicationHelper.LoadPropertyItem(piId);

				if (lvi.Checked)
				{						
				    this.RMWO.PropertyItemIds.Add((Guid)piId);										                                    
				    this.RMWO.PropertyItems.Add(pi);					
				}

                if (!this.RMWO.OriginalSelectionPropertyItemIds.Contains((Guid)piId))
                {
                    this.RMWO.OriginalSelectionPropertyItemIds.Add((Guid)piId);
                    this.RMWO.OriginalSelectionPropertyItems.Add(pi);
                }
			}
		}
		#endregion � Methods �		
	}
	#endregion � Class RetMailProcMask2 �
}
#endregion � Namespace DealMaker �
