using System;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using DealMaker.Export;
using DealMaker.Forms.Wizard;

namespace DealMaker
{
	public class RetMailProcMask3 : System.Windows.Forms.UserControl
	{
		private ReturnMailWizardObject rmwo = null;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.ColumnHeader profileColumnHeader;
		private System.Windows.Forms.ColumnHeader descriptionColumnHeader;
		private System.Windows.Forms.CheckBox createNewCheckBox;
		private System.Windows.Forms.ListView profilesListView;
		private System.ComponentModel.IContainer components = null;

		public RetMailProcMask3(ReturnMailWizardObject rmwo)
		{
			// This call is required by the Windows Form Designer.
			InitializeComponent();

			this.rmwo = rmwo;
			this.createNewCheckBox.CheckedChanged += new EventHandler(createNewCheckBox_EH);
		}

		public ReturnMailWizardObject RMWO
		{
			get { return this.rmwo; }
			set { this.rmwo = value; }
		}


		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.profilesListView = new System.Windows.Forms.ListView();
			this.profileColumnHeader = new System.Windows.Forms.ColumnHeader();
			this.descriptionColumnHeader = new System.Windows.Forms.ColumnHeader();
			this.createNewCheckBox = new System.Windows.Forms.CheckBox();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.label1.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.label1.Location = new System.Drawing.Point(8, 8);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(360, 32);
			this.label1.TabIndex = 0;
			this.label1.Text = "Select export profile or click \"Create new profile\" to create new export profile";
			this.label1.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
			// 
			// profilesListView
			// 
			this.profilesListView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.profilesListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
																							   this.profileColumnHeader,
																							   this.descriptionColumnHeader});
			this.profilesListView.FullRowSelect = true;
			this.profilesListView.GridLines = true;
			this.profilesListView.HideSelection = false;
			this.profilesListView.Location = new System.Drawing.Point(8, 48);
			this.profilesListView.MultiSelect = false;
			this.profilesListView.Name = "profilesListView";
			this.profilesListView.Size = new System.Drawing.Size(392, 208);
			this.profilesListView.TabIndex = 3;
			this.profilesListView.View = System.Windows.Forms.View.Details;
			// 
			// profileColumnHeader
			// 
			this.profileColumnHeader.Text = "Profile";
			this.profileColumnHeader.Width = 239;
			// 
			// descriptionColumnHeader
			// 
			this.descriptionColumnHeader.Text = "Description";
			this.descriptionColumnHeader.Width = 165;
			// 
			// createNewCheckBox
			// 
			this.createNewCheckBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.createNewCheckBox.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.createNewCheckBox.Location = new System.Drawing.Point(288, 272);
			this.createNewCheckBox.Name = "createNewCheckBox";
			this.createNewCheckBox.Size = new System.Drawing.Size(112, 16);
			this.createNewCheckBox.TabIndex = 4;
			this.createNewCheckBox.Text = "Create new profile";
			// 
			// RetMailProcMask3
			// 
			this.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(210)), ((System.Byte)(216)), ((System.Byte)(240)));
			this.Controls.Add(this.createNewCheckBox);
			this.Controls.Add(this.profilesListView);
			this.Controls.Add(this.label1);
			this.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.Name = "RetMailProcMask3";
			this.Size = new System.Drawing.Size(408, 296);
			this.ResumeLayout(false);

		}
		#endregion

		public void Init()
		{
			string path = Application.StartupPath + @"\"+ Globals.ReturnedMailProfilePath;

			if (!Directory.Exists(path))
				return;

			foreach (string file in Directory.GetFiles(path, "*.xml"))
			{
				FileStream fs;
				XmlSerializer serializer;
				ExportMappingProfile profile;
				ListViewItem item = new ListViewItem();

				try
				{
					fs = new FileStream(file, FileMode.Open);
					serializer = new XmlSerializer(typeof(ExportMappingProfile));

					profile = (ExportMappingProfile)serializer.Deserialize(fs);

					fs.Close();
				}
				catch
				{
					continue;
				}

				item.Text = profile.Name;
				item.SubItems.Add(profile.Description);
				item.Tag = profile;

				profilesListView.Items.Add(item);
			}
		}

		public bool ExitPage()
		{
			//CreateExportProfilePage cepp = WizardForm[2] as CreateExportProfilePage;
			//CreateExportFilterPage cefp = WizardForm[3] as CreateExportFilterPage;

			if (null == SelectedProfile && false == CreateNewProfile)
				return false;

			if (null != SelectedProfile)
				this.RMWO.SelectedFields = SelectedProfile.Mappings;
			return true;

			//WizardForm.ButtonStyles |= WizardFormButtonStyles.FinishButton;
			//WizardForm.FinishButton.Text = "&Export";
		}



		public bool CanExitEventHandler()
		{
			return SelectedProfile != null || CreateNewProfile;
		}

		
		#region Properties
		public bool CreateNewProfile
		{
			get
			{
				return createNewCheckBox.Checked;
			}
		}

		public ExportMappingProfile SelectedProfile
		{
			get
			{
				if (null != profilesListView.FocusedItem)
					return profilesListView.FocusedItem.Tag as ExportMappingProfile;
				else
					return null;
			}
		}
		#endregion

		private void createNewCheckBox_EH(object sender, EventArgs e)
		{
			CheckBox cb = (CheckBox)sender;
			if (false == cb.Checked)
			{
				this.profilesListView.Enabled = true;				
			}
			else
				this.profilesListView.Enabled = false;				
		}
	}
}

