using System;
using System.Drawing;
using System.Collections;
using DealMaker.Export;

namespace DealMaker
{
	public enum eDirection
	{
		eScanIn = 0, 
		eUpdate,
	}

	/// <summary>
	/// Summary description for ReturnMailWizardObject.
	/// </summary>
	public class ReturnMailWizardObject
	{		
		private ArrayList					alOriginalSelectionPropertyItemIds = new ArrayList();
		private ArrayList					alOriginalSelectionPropertyItems = new ArrayList();
		private ArrayList					alPropertyItemIds	= new ArrayList();
		private ArrayList					alPropertyItems		= new ArrayList();
		private Image						image				= null;
		private eDirection					direction			= eDirection.eScanIn;
		private ExportAttributeMapping[]	expAttributeMapping	= null;
		private ExportMappingProfile		expMappingProfile	= null;
		private bool						eMail				= false;
		private string						exportFileName		= string.Empty;
		private string						exportFileDocument	= string.Empty;
		private string						originalFileName	= string.Empty;

		
		public ReturnMailWizardObject()
		{
			
		}

		public ArrayList OriginalSelectionPropertyItemIds
		{
			get {return alOriginalSelectionPropertyItemIds;}
			set {alOriginalSelectionPropertyItemIds = value;}
		}

		public ArrayList OriginalSelectionPropertyItems
		{
			get {return alOriginalSelectionPropertyItems;}
			set {alOriginalSelectionPropertyItems = value;}
		}

		public string OriginalFileName
		{
			get {return this.originalFileName;}
			set {this.originalFileName = value;}
		}

		public ExportMappingProfile	ExpMappingProfile
		{
			get { return this.expMappingProfile; }
			set { this.expMappingProfile = value; }
		}

		public string ExportFileName
		{
			get { return this.exportFileName; }
			set { this.exportFileName = value; }
		}

		public string ExportFileDocument
		{
			get { return this.exportFileDocument; }
			set { this.exportFileDocument = value; }
		}


		public bool EMail
		{
			get {return this.eMail;}
			set {this.eMail = value;}
		}

		public ExportAttributeMapping[] ExpAttributeMapping
		{
			get {return this.expAttributeMapping; }
			set { this.expAttributeMapping = value; }
		}

		public eDirection Direction 
		{
			get { return this.direction; }
			set { this.direction = value; }
		}

		public ArrayList PropertyItemIds
		{
			get { return this.alPropertyItemIds; }
			set { this.alPropertyItemIds = value; }
		}

		public ArrayList PropertyItems
		{
			get {return this.alPropertyItems;}
			set {this.alPropertyItems = value;}
		}

		public Image ScreenImage
		{
			get { return this.image; }
			set {this.image = value;}
		}

		public ExportAttributeMapping[] SelectedFields
		{
			get
			{
				ArrayList items = new ArrayList();
				
				if (null != this.expAttributeMapping)
				{
					foreach(object o in this.expAttributeMapping)
						items.Add(o);
				}

				return (ExportAttributeMapping[])items.ToArray(typeof(ExportAttributeMapping));
			}
			set
			{
				this.ExpAttributeMapping = value;
			}
		}

	}
}
