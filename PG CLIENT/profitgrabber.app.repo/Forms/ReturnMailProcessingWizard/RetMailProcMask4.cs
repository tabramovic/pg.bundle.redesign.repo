using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using DealMaker;
using DealMaker.Export;
using DealMaker.PlainClasses;
using DealMaker.Forms.Wizard;

namespace DealMaker
{
	public class RetMailProcMask4 : System.Windows.Forms.UserControl
	{
		private ReturnMailWizardObject rmwo = null;
		private System.Windows.Forms.Label availableFieldsLabel;
		private System.Windows.Forms.Label selectedFieldsLabel;
		private System.Windows.Forms.Button removeButton;
		private System.Windows.Forms.Button addEmptyButton;
		private System.Windows.Forms.Button upButton;
		private System.Windows.Forms.Button downButton;
		private System.Windows.Forms.ListBox availableListBox;
		private System.Windows.Forms.ListBox selectedListBox;
		private System.Windows.Forms.Button addButton;
		private System.Windows.Forms.Button addAllButton;
		private System.ComponentModel.IContainer components = null;

		public RetMailProcMask4(ReturnMailWizardObject rmwo)
		{
			// This call is required by the Windows Form Designer.
			InitializeComponent();

			this.rmwo = rmwo;

			if (null != this.RMWO.SelectedFields && 0 != this.RMWO.SelectedFields.Length)
			{
				this.SelectedFields = this.RMWO.SelectedFields;
			}
			
			//CanExit += new CanExitEventHandler(CanExitEventHandler);
			
			this.Leave += new EventHandler(AssignMappingsToRMWO);
			addButton.Click += new EventHandler(AddButtonClickedEventHandler);
			removeButton.Click += new EventHandler(RemoveButtonClickedEventHandler);
			addEmptyButton.Click += new EventHandler(AddEmptyButtonClickedEventHandler);
			addAllButton.Click += new EventHandler(AddAllButonClickedEventHandler);
			upButton.Click += new EventHandler(UpButtonClickedEventHandler);
			downButton.Click += new EventHandler(DownButtonClickEventHandler);
		}

		public ReturnMailWizardObject RMWO
		{
			get { return this.rmwo; }
			set { this.rmwo = value; }
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		private void SelectedListBoxShiftSelectedItem(int offset)
		{
			object o;
			int selectedIndex = selectedListBox.SelectedIndex;
			object selectedItem = selectedListBox.SelectedItem;
			ListBox.ObjectCollection items = selectedListBox.Items;

			if (null == selectedItem || 
				selectedIndex + offset < 0 ||
				selectedIndex + offset >= selectedListBox.Items.Count )
				return;
			
			o = items[selectedIndex + offset];
			items[selectedIndex + offset] = selectedItem;
			items[selectedIndex] = o;

			selectedListBox.SetSelected(selectedIndex, false);
			selectedListBox.SetSelected(selectedIndex + offset, true);
		}		

		private void FirstEnteredEventHandler(object sender, EventArgs e)
		{
			/*DealMaker.Forms.Export.SelectContactsPage contactsPage = WizardForm[0] as DealMaker.Forms.Export.SelectContactsPage;
			PropertyItem item = contactsPage.Contacts[0];

			foreach (PropertyItemAttribute attribute in item.PropertyItemAttributes)
				availableListBox.Items.Add(attribute);*/
			
			PropertyItemAttribute propertyItemId						= new PropertyItemAttribute("PropertyItem Id", null, "PropertyItem Id", null);
			PropertyItemAttribute firstName 							= new PropertyItemAttribute("First Name", null, "First Name", null);
			PropertyItemAttribute midName								= new PropertyItemAttribute("Middle Name", null, "Middle Name", null);
			PropertyItemAttribute lastName								= new PropertyItemAttribute("Last Name", null, "Last Name", null);
			PropertyItemAttribute fullName								= new PropertyItemAttribute("Full Name", null, "Full Name", null);
			PropertyItemAttribute siteStreetAddress 					= new PropertyItemAttribute("Site Street Address", null, "Site Street Address", null);
			PropertyItemAttribute siteCity								= new PropertyItemAttribute("Site City", null, "Site City", null);
			PropertyItemAttribute siteState								= new PropertyItemAttribute("Site State", null, "Site State", null);
			PropertyItemAttribute siteCityState							= new PropertyItemAttribute("Site City, State", null, "Site City, State", null);
			PropertyItemAttribute siteZIP								= new PropertyItemAttribute("Site ZIP", null, "Site ZIP", null);
			PropertyItemAttribute lastKnownMailingStreetAddress			= new PropertyItemAttribute("Last Known Mailing Street Address", null, "Last Known Mailing Street Address", null);
			PropertyItemAttribute lastKnownMailingCityState				= new PropertyItemAttribute("Last Known Mailing City, State", null, "Last Known Mailing City, State", null);
			PropertyItemAttribute lastKnownMailingCity					= new PropertyItemAttribute("Last Known Mailing City", null, "Last Known Mailing City", null);
			PropertyItemAttribute lastKnownMailingState					= new PropertyItemAttribute("Last Known Mailing State", null, "Last Known Mailing State", null);
			PropertyItemAttribute lastKnownMailingZIP					= new PropertyItemAttribute("Last Known Mailing ZIP", null, "Last Known Mailing ZIP", null);
			PropertyItemAttribute previouslyKnownMailingStreetAddress	= new PropertyItemAttribute("Previously Known Mailing Street Address", null, "Previously Known Mailing Street Address", null);
			PropertyItemAttribute previouslyKnownMailingCityState		= new PropertyItemAttribute("Previously Known Mailing City, State", null, "Previously Known Mailing City, State", null);
			PropertyItemAttribute previouslyKnownMailingCity			= new PropertyItemAttribute("Previously Known Mailing City", null, "Previously Known Mailing City", null);
			PropertyItemAttribute previouslyKnownMailingState			= new PropertyItemAttribute("Previously Known Mailing State", null, "Previously Known Mailing State", null);
			PropertyItemAttribute previouslyKnownMailingZIP				= new PropertyItemAttribute("Previously Known Mailing ZIP", null, "Previously Known Mailing ZIP", null);
			PropertyItemAttribute firstEntered							= new PropertyItemAttribute("First Entered ", null, "First Entered ", null);
			PropertyItemAttribute group 								= new PropertyItemAttribute("Group", null, "Group", null);
			PropertyItemAttribute statusGroup  							= new PropertyItemAttribute("Status Group", null, "Status Group", null);
						
			availableListBox.Items.AddRange(
				new object[] {
								propertyItemId,
								firstName, 
								midName, 
								lastName, 
								fullName, 
								siteStreetAddress, 
								siteCity, 
								siteState, 
								siteZIP, 
								lastKnownMailingStreetAddress, 
								lastKnownMailingCityState, 
								lastKnownMailingCity, 
								lastKnownMailingState, 
								lastKnownMailingZIP, 
								previouslyKnownMailingStreetAddress, 
								previouslyKnownMailingCityState, 
								previouslyKnownMailingCity, 
								previouslyKnownMailingState, 
								previouslyKnownMailingZIP, 
								firstEntered, 
								group, 
								statusGroup
							  }
				);
		}

		public  bool CanExitEventHandler(object sender, EventArgs e)
		{
			return selectedListBox.Items.Count > 0;
		}

		private void AddButtonClickedEventHandler(object sender, EventArgs e)
		{
			if (null == availableListBox.SelectedItems)
				return;

			foreach(object o in availableListBox.SelectedItems)
			{
				PropertyItemAttribute attribute;
				ExportAttributeMapping mapping;

				attribute = o as PropertyItemAttribute;
				if (null == o)
					continue;

				mapping = new ExportAttributeMapping(
					attribute.attributeDescription);

				selectedListBox.Items.Add(mapping);
			}													
			
			bool bFound = false;
			IEnumerator ie = selectedListBox.Items.GetEnumerator();
			while (ie.MoveNext())
			{
				ExportAttributeMapping eam = (ExportAttributeMapping)ie.Current;
				if (eam.AttributeName == "PropertyItem Id")
				{
					bFound = true;
					break;
				}
			}

			if (false == bFound)
			{
				ExportAttributeMapping propertyItemMapping = new ExportAttributeMapping("PropertyItem Id");
				selectedListBox.Items.Insert(0, propertyItemMapping);
			}
		}

        bool HasForMultiplePIs()
        {
            int iCnt = 0;
            IEnumerator ie = selectedListBox.Items.GetEnumerator();
            while (ie.MoveNext())
            {
                ExportAttributeMapping eam = (ExportAttributeMapping)ie.Current;
                if (eam.AttributeName == "PropertyItem Id")
                {
                    iCnt++;
                }
            }

            if (iCnt >= 2)
                return true;
            else
                return false;
 
        }

		private void RemoveButtonClickedEventHandler(object sender, EventArgs e)
		{
			int i;

			if (null == selectedListBox.SelectedItems)
				return;

			for (i = selectedListBox.Items.Count; i > 0; i--)
			{
                if (selectedListBox.GetSelected(i - 1))
				{
					ExportAttributeMapping eam = (ExportAttributeMapping)selectedListBox.Items[i - 1];

                    if ((eam.AttributeName != "PropertyItem Id") || (eam.AttributeName == "PropertyItem Id" && HasForMultiplePIs()))
					    selectedListBox.Items.Remove(selectedListBox.Items[i - 1]);
				}
			}
		}

		private void AddEmptyButtonClickedEventHandler(object sender, EventArgs e)
		{
			selectedListBox.Items.Add(new ExportAttributeMapping("Empty Column"));
		}

		private void AddAllButonClickedEventHandler(object sender, EventArgs e)
		{
			foreach (object o in availableListBox.Items)
			{
				PropertyItemAttribute attribute;
				ExportAttributeMapping mapping;

				attribute = o as PropertyItemAttribute;
				if (null == o)
					continue;

				mapping = new ExportAttributeMapping(
					attribute.attributeDescription);

				selectedListBox.Items.Add(mapping);
			}
		}

		private void AssignMappingsToRMWO(object sender, EventArgs e)
		{
			ExportAttributeMapping[] eam = new ExportAttributeMapping[this.selectedListBox.Items.Count];
			for (int i = 0; i < this.selectedListBox.Items.Count; i++)
			{				
				eam[i] = (ExportAttributeMapping)this.selectedListBox.Items[i];
			}
			this.RMWO.ExpAttributeMapping = eam;
		}

		private void UpButtonClickedEventHandler(object sender, EventArgs e)
		{
			SelectedListBoxShiftSelectedItem(-1);
		}

		private void DownButtonClickEventHandler(object sender, EventArgs e)
		{
			SelectedListBoxShiftSelectedItem(1);
		}

		#region Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.availableFieldsLabel = new System.Windows.Forms.Label();
			this.availableListBox = new System.Windows.Forms.ListBox();
			this.selectedListBox = new System.Windows.Forms.ListBox();
			this.addButton = new System.Windows.Forms.Button();
			this.removeButton = new System.Windows.Forms.Button();
			this.addEmptyButton = new System.Windows.Forms.Button();
			this.upButton = new System.Windows.Forms.Button();
			this.downButton = new System.Windows.Forms.Button();
			this.selectedFieldsLabel = new System.Windows.Forms.Label();
			this.addAllButton = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// availableFieldsLabel
			// 
			this.availableFieldsLabel.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.availableFieldsLabel.Location = new System.Drawing.Point(8, 8);
			this.availableFieldsLabel.Name = "availableFieldsLabel";
			this.availableFieldsLabel.Size = new System.Drawing.Size(88, 16);
			this.availableFieldsLabel.TabIndex = 0;
			this.availableFieldsLabel.Text = "Available Fields:";
			// 
			// availableListBox
			// 
			this.availableListBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left)));
			this.availableListBox.Location = new System.Drawing.Point(8, 32);
			this.availableListBox.Name = "availableListBox";
			this.availableListBox.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
			this.availableListBox.Size = new System.Drawing.Size(200, 238);
			this.availableListBox.TabIndex = 1;
			// 
			// selectedListBox
			// 
			this.selectedListBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.selectedListBox.Location = new System.Drawing.Point(288, 32);
			this.selectedListBox.Name = "selectedListBox";
			this.selectedListBox.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
			this.selectedListBox.Size = new System.Drawing.Size(200, 238);
			this.selectedListBox.TabIndex = 2;
			// 
			// addButton
			// 
			this.addButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.addButton.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.addButton.Location = new System.Drawing.Point(216, 32);
			this.addButton.Name = "addButton";
			this.addButton.Size = new System.Drawing.Size(64, 23);
			this.addButton.TabIndex = 3;
			this.addButton.Text = "&Add >>";
			// 
			// removeButton
			// 
			this.removeButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.removeButton.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.removeButton.Location = new System.Drawing.Point(216, 64);
			this.removeButton.Name = "removeButton";
			this.removeButton.Size = new System.Drawing.Size(64, 23);
			this.removeButton.TabIndex = 4;
			this.removeButton.Text = "<< &Remove";
			// 
			// addEmptyButton
			// 
			this.addEmptyButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.addEmptyButton.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.addEmptyButton.Location = new System.Drawing.Point(216, 104);
			this.addEmptyButton.Name = "addEmptyButton";
			this.addEmptyButton.Size = new System.Drawing.Size(64, 32);
			this.addEmptyButton.TabIndex = 5;
			this.addEmptyButton.Text = "Add Empty Column";
			// 
			// upButton
			// 
			this.upButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.upButton.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.upButton.Location = new System.Drawing.Point(328, 288);
			this.upButton.Name = "upButton";
			this.upButton.TabIndex = 6;
			this.upButton.Text = "Move Up";
			// 
			// downButton
			// 
			this.downButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.downButton.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.downButton.Location = new System.Drawing.Point(416, 288);
			this.downButton.Name = "downButton";
			this.downButton.Size = new System.Drawing.Size(72, 23);
			this.downButton.TabIndex = 7;
			this.downButton.Text = "Move Down";
			// 
			// selectedFieldsLabel
			// 
			this.selectedFieldsLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.selectedFieldsLabel.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.selectedFieldsLabel.Location = new System.Drawing.Point(288, 8);
			this.selectedFieldsLabel.Name = "selectedFieldsLabel";
			this.selectedFieldsLabel.Size = new System.Drawing.Size(176, 16);
			this.selectedFieldsLabel.TabIndex = 8;
			this.selectedFieldsLabel.Text = "Selected Fields for Export:";
			// 
			// addAllButton
			// 
			this.addAllButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.addAllButton.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.addAllButton.Location = new System.Drawing.Point(216, 144);
			this.addAllButton.Name = "addAllButton";
			this.addAllButton.Size = new System.Drawing.Size(64, 32);
			this.addAllButton.TabIndex = 9;
			this.addAllButton.Text = "&Add all Columns";
			// 
			// RetMailProcMask4
			// 
			this.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(210)), ((System.Byte)(216)), ((System.Byte)(240)));
			this.Controls.Add(this.addAllButton);
			this.Controls.Add(this.selectedFieldsLabel);
			this.Controls.Add(this.downButton);
			this.Controls.Add(this.upButton);
			this.Controls.Add(this.addEmptyButton);
			this.Controls.Add(this.removeButton);
			this.Controls.Add(this.addButton);
			this.Controls.Add(this.selectedListBox);
			this.Controls.Add(this.availableListBox);
			this.Controls.Add(this.availableFieldsLabel);
			this.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.Name = "RetMailProcMask4";
			this.Size = new System.Drawing.Size(496, 328);
			this.Load += new System.EventHandler(this.FirstEnteredEventHandler);
			this.ResumeLayout(false);

		}
		#endregion



		#region Properties
		public ExportAttributeMapping[] SelectedFields
		{
			get
			{
				ArrayList items = new ArrayList();

				foreach(object o in selectedListBox.Items)
					items.Add(o);

				return (ExportAttributeMapping[])items.ToArray(typeof(ExportAttributeMapping));
			}
			set
			{
				selectedListBox.Items.Clear();

				if (null != value)
					selectedListBox.Items.AddRange(value);
			}
		}
		#endregion
	}
}


