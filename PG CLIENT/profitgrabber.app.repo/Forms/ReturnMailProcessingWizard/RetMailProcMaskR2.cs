using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;
using System.IO;
using DealMaker.PlainClasses.Utils;

namespace DealMaker
{
	/// <summary>
	/// Summary description for RetMailProcMaskR2.
	/// </summary>
	public class RetMailProcMaskR2 : System.Windows.Forms.UserControl
	{
		#region � Data �
		private string profilesPath = Application.StartupPath + Globals.ReturnedMailUpdateDataProfilePath;
		private Delimiter inputDelimiter;
		private string selectedFile;
		private string selectedProfile = string.Empty;
		private bool bChecked;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.ListView lvImportProfile;
		private System.Windows.Forms.ColumnHeader profileName;
		private System.Windows.Forms.ColumnHeader profileDescription;
		private System.Windows.Forms.CheckBox cbCreateNewProfile;
		private System.Windows.Forms.ContextMenu contextMenu1;
		private System.Windows.Forms.MenuItem _DeleteProfile;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
		#endregion � Data �
		#region � Properties �
		#region � InputDelimiter �
		public Delimiter InputDelimiter
		{
			get {return this.inputDelimiter;}
			set {this.inputDelimiter = value;}
		}
		#endregion � InputDelimiter �
		#region � SelectedFile �
		public string SelectedFile
		{
			get {return this.selectedFile;}
			set {this.selectedFile = value;}
		}
		#endregion � SelectedFile �
		#region � Checked �
		public bool Checked
		{
			get {return this.bChecked;}
			set {this.bChecked = value;}
		}
		#endregion � Checked �
		#region � CreateNewProfile �
		public bool CreateNewProfile
		{
			get {return this.cbCreateNewProfile.Checked;}
			set {this.cbCreateNewProfile.Checked = value;}
		}
		#endregion � CreateNewProfile �
		#region � SelectedProfile �
		public string SelectedProfile
		{
			get {return this.selectedProfile; }
			set {this.selectedProfile = value;}
		}		
		#endregion � SelectedProfile �
		#endregion � Properties �

		public RetMailProcMaskR2()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

			this.cbCreateNewProfile.CheckedChanged += new EventHandler(cbCreateNewProfile_EH);
            //PROFILE MANAGER
			_DeleteProfile.Click += new EventHandler(OnDeleteProfileClick);
		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.lvImportProfile = new System.Windows.Forms.ListView();
			this.profileName = new System.Windows.Forms.ColumnHeader();
			this.profileDescription = new System.Windows.Forms.ColumnHeader();
			this.cbCreateNewProfile = new System.Windows.Forms.CheckBox();
			this.contextMenu1 = new System.Windows.Forms.ContextMenu();
			this._DeleteProfile = new System.Windows.Forms.MenuItem();
			this.groupBox1.SuspendLayout();
			this.SuspendLayout();
			// 
			// groupBox1
			// 
			this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox1.Controls.Add(this.lvImportProfile);
			this.groupBox1.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.groupBox1.Location = new System.Drawing.Point(16, 24);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(534, 312);
			this.groupBox1.TabIndex = 0;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Select Import Profile";
			// 
			// lvImportProfile
			// 
			this.lvImportProfile.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.lvImportProfile.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
																							  this.profileName,
																							  this.profileDescription});
			this.lvImportProfile.ContextMenu = this.contextMenu1;
			this.lvImportProfile.FullRowSelect = true;
			this.lvImportProfile.GridLines = true;
			this.lvImportProfile.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
			this.lvImportProfile.Location = new System.Drawing.Point(16, 24);
			this.lvImportProfile.Name = "lvImportProfile";
			this.lvImportProfile.Size = new System.Drawing.Size(502, 272);
			this.lvImportProfile.TabIndex = 0;
			this.lvImportProfile.View = System.Windows.Forms.View.Details;
			this.lvImportProfile.SelectedIndexChanged += new System.EventHandler(this.lvImportProfile_SelectedIndexChanged);
			// 
			// profileName
			// 
			this.profileName.Text = "Mapping Profile Name";
			this.profileName.Width = 138;
			// 
			// profileDescription
			// 
			this.profileDescription.Text = "Description";
			this.profileDescription.Width = 359;
			// 
			// cbCreateNewProfile
			// 
			this.cbCreateNewProfile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.cbCreateNewProfile.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.cbCreateNewProfile.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.cbCreateNewProfile.Location = new System.Drawing.Point(424, 352);
			this.cbCreateNewProfile.Name = "cbCreateNewProfile";
			this.cbCreateNewProfile.Size = new System.Drawing.Size(128, 24);
			this.cbCreateNewProfile.TabIndex = 1;
			this.cbCreateNewProfile.Text = "Create New Profile";
			this.cbCreateNewProfile.CheckedChanged += new System.EventHandler(this.cbCreateNewProfile_CheckedChanged);
			// 
			// contextMenu1
			// 
			this.contextMenu1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																						 this._DeleteProfile});
			// 
			// _DeleteProfile
			// 
			this._DeleteProfile.Index = 0;
			this._DeleteProfile.Text = "Delete Profile";
			// 
			// RetMailProcMaskR2
			// 
			this.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(210)), ((System.Byte)(216)), ((System.Byte)(240)));
			this.Controls.Add(this.cbCreateNewProfile);
			this.Controls.Add(this.groupBox1);
			this.Name = "RetMailProcMaskR2";
			this.Size = new System.Drawing.Size(576, 384);
			this.Load += new System.EventHandler(this.ImportMask2_Load);
			this.groupBox1.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion
		#region � Methods �
		#region � SetNextVisibility �
		private void SetNextVisibility()
		{
			if (true == this.cbCreateNewProfile.Checked || (this.lvImportProfile.Items.Count > 0 && null != this.lvImportProfile.SelectedItems && 0 != this.lvImportProfile.SelectedItems.Count))
			{
				
				if (null != this.Parent)
				{
					Panel panel = (Panel)this.Parent;
					ReturnMailWizardForm rmwf = (ReturnMailWizardForm)panel.Parent;
					rmwf.NextEnabled = true;
				}
				if ((this.lvImportProfile.Items.Count > 0 && null != this.lvImportProfile.SelectedItems && 0 != this.lvImportProfile.SelectedItems.Count))
				{
					this.selectedProfile = profilesPath + @"\" + this.lvImportProfile.SelectedItems[0].SubItems[0].Text + Globals.ProfileExtention;
				}
			}			
		}
		#endregion � SetNextVisibility �
		#region � bPrev_Click �
		public void bPrev_Click(object sender, System.EventArgs e)
		{
			Globals.importWindowPos = this.Location;
			//this.Close();			
			ImportMask1 im1 = new ImportMask1(this.selectedFile, this.Checked, this.InputDelimiter);			
			im1.Show();
		}
		#endregion � bPrev_Click �
		#region � cbCreateNewProfile_CheckedChanged �
		private void cbCreateNewProfile_CheckedChanged(object sender, System.EventArgs e)
		{
			this.SetNextVisibility();
		}
		#endregion � cbCreateNewProfile_CheckedChanged �
		#region � lvImportProfile_SelectedIndexChanged �
		private void lvImportProfile_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			this.SetNextVisibility();
		}
		#endregion � lvImportProfile_SelectedIndexChanged �
		#region � bNext_Click �
		private void bNext_Click(object sender, System.EventArgs e)
		{
			/*try
			{
				Globals.importWindowPos = this.Location;
				//this.Close();			
				ImportMask3 im3 = new ImportMask3();
				im3.SelectedFile = this.SelectedFile;
				im3.Checked = this.Checked;
				im3.CreateNewProfile = this.CreateNewProfile;
				im3.InputDelimiter = this.InputDelimiter;
				im3.SelectedProfile = this.SelectedProfile;
				im3.Show();
			}
			catch (System.Runtime.InteropServices.SEHException exc)
			{
				MessageBox.Show(exc.Message);
			}*/
		}
		#endregion � bNext_Click �

		private void ImportMask2_Load(object sender, System.EventArgs e)
		{			
			Panel panel = (Panel)this.Parent;
			
			int columnNumber = this.lvImportProfile.Columns.Count;
			double dColumnWidht = this.lvImportProfile.Width / columnNumber;
			int columnWidth = (int)dColumnWidht;
			foreach (ColumnHeader ch in this.lvImportProfile.Columns)
			{
				ch.Width = columnWidth;
			}
			this.UpdateListView();
		}
	
		#region � UpdateListView �
		private void UpdateListView()
		{			
			this.lvImportProfile.Items.Clear();
			if (false == Directory.Exists(this.profilesPath))
			{
				return;
			}
			string[] files = Directory.GetFiles(this.profilesPath);
			foreach (string s in files)
			{
				string desc = string.Empty;
				FileInfo fi = new FileInfo(s);

				using (StreamReader sr = fi.OpenText()) 
				{
					string line = string.Empty;
					while ((line = sr.ReadLine()) != null) 
					{						
						desc = line;						
						break;
					}
					sr.Close();
				}

				string name = string.Empty;
				int indexOfDir = s.LastIndexOf(@"\");
				if (indexOfDir > 0)
				{
					name = s.Substring(indexOfDir + 1);
				}
				name = name.Replace(Globals.ProfileExtention, string.Empty);


				ListViewItem lvi = new ListViewItem(name);
				lvi.SubItems[0].Text = name;
				lvi.SubItems.Add(desc);
				lvi.Tag = s;
				this.lvImportProfile.Items.Add(lvi);
			}
		}
		#endregion � UpdateListView �

		private void bCancel_Click(object sender, System.EventArgs e)
		{
			if (DialogResult.Yes == MessageBox.Show(this, UserMessages.CancelImportText, UserMessages.CancelImport, MessageBoxButtons.YesNo, MessageBoxIcon.Question))
			{
				Globals.importWizardStarted = false;
				//this.Close();
			}
		}
		#endregion � Methods �

		private void ImportMask2_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (true == e.Cancel)
			{
				if (DialogResult.Yes == MessageBox.Show(this, UserMessages.CancelImportText, UserMessages.CancelImport, MessageBoxButtons.YesNo, MessageBoxIcon.Question))
				{
					Globals.importWizardStarted = false;
					//this.Close();
				}
			}
		}

		private void cbCreateNewProfile_EH(object sender, EventArgs e)
		{
			CheckBox cb = (CheckBox)sender;
			if (true == cb.Checked)
			{
				this.lvImportProfile.Enabled = false;
			}
			else
			{
				this.lvImportProfile.Enabled = true;
			}
		}

		private void OnDeleteProfileClick(object sender, EventArgs e)
		{
			if (null != this.lvImportProfile.SelectedItems && 1 == this.lvImportProfile.SelectedItems.Count)
			{
				string s = (string)this.lvImportProfile.SelectedItems[0].Tag;
				ProfileManager pm = new ProfileManager(s, true);
				
				string outMessage;

				if (false == pm.DeleteProfile(out outMessage))
					MessageBox.Show(outMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);	
				else
					UpdateListView();
			}
		}
	}
}
