using System;
using System.Reflection;
using System.Diagnostics;
using System.IO;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace DealMaker
{
	/// <summary>
	/// Summary description for RetMailProcMaskR1.
	/// </summary>
	public class RetMailProcMaskR1 : System.Windows.Forms.UserControl
	{
		#region � Data �
		private Delimiter inputDelimiter;
		private int nextScreen = -1;
		private Excel.Application _mExcelApp = null;
		Excel.Worksheet worksheet = null;
		Excel.Sheets sheets = null;		
		private bool bValid	= false;
		private bool bChecked = false;
		private string selectedFile = string.Empty;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Button bBrowse;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.TextBox tbSelectedFile;
		private System.Windows.Forms.CheckBox cbFirstRow;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
		#endregion � Data �
		#region � Properties �
		public int NextScreen
		{
			get {return this.nextScreen;}
		}
		#region � Valid �
		public bool Valid 
		{
			get { return this.bValid; }		
		}
		#endregion � Valid �
		#region � SelectedFile �
		public string SelectedFile
		{
			get {return this.tbSelectedFile.Text;}
			set {this.tbSelectedFile.Text = value;}
		}
		#endregion � SelectedFile �
		#region � Checked �
		public bool Checked
		{
			get {return this.cbFirstRow.Checked;}
			set {this.cbFirstRow.Checked = value;}
		}
		#endregion � Checked �
		#region � Delimiter �
		public Delimiter InputDelimiter
		{
			get 
			{					
				return this.inputDelimiter;
			}
			set 
			{
				this.inputDelimiter = value;
			}
		}
		#endregion � Delimiter �
		#endregion � Properties �

		public RetMailProcMaskR1()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

			this.Location = Globals.importWindowPos;
			Globals.importWizardStarted = true;			

		}

		public RetMailProcMaskR1(string selectedFile, bool Checked, Delimiter inputDelimiter)
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

			this.tbSelectedFile.Text = selectedFile;
			this.cbFirstRow.Checked = Checked;
			this.InputDelimiter = inputDelimiter;
		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.tbSelectedFile = new System.Windows.Forms.TextBox();
			this.bBrowse = new System.Windows.Forms.Button();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.cbFirstRow = new System.Windows.Forms.CheckBox();
			this.groupBox1.SuspendLayout();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.label1.Location = new System.Drawing.Point(16, 29);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(56, 23);
			this.label1.TabIndex = 0;
			this.label1.Text = "Data File:";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// tbSelectedFile
			// 
			this.tbSelectedFile.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.tbSelectedFile.Location = new System.Drawing.Point(88, 32);
			this.tbSelectedFile.Name = "tbSelectedFile";
			this.tbSelectedFile.Size = new System.Drawing.Size(392, 20);
			this.tbSelectedFile.TabIndex = 1;
			this.tbSelectedFile.Text = "";
			// 
			// bBrowse
			// 
			this.bBrowse.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.bBrowse.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.bBrowse.Location = new System.Drawing.Point(496, 32);
			this.bBrowse.Name = "bBrowse";
			this.bBrowse.Size = new System.Drawing.Size(32, 23);
			this.bBrowse.TabIndex = 2;
			this.bBrowse.Text = "...";
			this.bBrowse.Click += new System.EventHandler(this.bBrowse_Click);
			// 
			// groupBox1
			// 
			this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox1.Controls.Add(this.label1);
			this.groupBox1.Controls.Add(this.tbSelectedFile);
			this.groupBox1.Controls.Add(this.bBrowse);
			this.groupBox1.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.groupBox1.Location = new System.Drawing.Point(16, 24);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(544, 72);
			this.groupBox1.TabIndex = 3;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Select Skip Traced Import Data File";
			// 
			// cbFirstRow
			// 
			this.cbFirstRow.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.cbFirstRow.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.cbFirstRow.Location = new System.Drawing.Point(18, 104);
			this.cbFirstRow.Name = "cbFirstRow";
			this.cbFirstRow.Size = new System.Drawing.Size(264, 24);
			this.cbFirstRow.TabIndex = 4;
			this.cbFirstRow.Text = "First row contains field names";
			// 
			// RetMailProcMaskR1
			// 
			this.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(210)), ((System.Byte)(216)), ((System.Byte)(240)));
			this.Controls.Add(this.cbFirstRow);
			this.Controls.Add(this.groupBox1);
			this.Name = "RetMailProcMaskR1";
			this.Size = new System.Drawing.Size(576, 384);
			this.Load += new System.EventHandler(this.ImportMask1_Load);
			this.groupBox1.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion
		#region � Methods �
		#region � bBrowse_Click �
		private void bBrowse_Click(object sender, System.EventArgs e)
		{			
			string lastPath = string.Empty;
			try
			{				
				lastPath = Application.CommonAppDataRegistry.GetValue("LastRetMailProcPath").ToString();
			}
			catch (Exception)
			{
				lastPath = @"c:\";
			}

			OpenFileDialog ofd = new OpenFileDialog();
			ofd.InitialDirectory = lastPath; //@"c:\" ;
			//Image Files(*.BMP;*.JPG;*.GIF)|*.BMP;*.JPG;*.GIF|All files (*.*)|*.*"

			ofd.Filter = "Import Files (*.txt;*.csv)|*.txt;*.csv|Text Files (*.txt)|*.txt|Comma Separated Values files (*.csv)|*.csv" ;
			ofd.FilterIndex = 1 ;
			ofd.RestoreDirectory = true ;

			if(ofd.ShowDialog() == DialogResult.OK)
			{					
				this.tbSelectedFile.Text = ofd.FileName;
				string extention = this.tbSelectedFile.Text.Substring(this.tbSelectedFile.Text.Length - 3);

				Globals.selectedExcelFile = string.Empty;
				Globals.selectedExcelIndex = -1;
				this._mExcelApp = null;

				
				int lastPos = ofd.FileName.LastIndexOf(@"\");
				lastPath = ofd.FileName.Substring(0, lastPos);
				Application.CommonAppDataRegistry.SetValue("LastRetMailProcPath", lastPath);

			}			
		}
		#endregion � bBrowse_Click �
		#region � bNext_Click �
		public void bNext_Click(object sender, System.EventArgs e)
		{															
			this.selectedFile = this.tbSelectedFile.Text;
			this.bChecked = this.cbFirstRow.Checked;
		
			if (false == File.Exists(this.selectedFile))
			{
				MessageBox.Show(UserMessages.IncorrectFileName);
				return;
			}			


			this.InputDelimiter = this.FindDelimiter(this.tbSelectedFile.Text);
			
			Globals.importWindowPos = this.Location;
			this.bValid = true;
			//this.Close();
									
			/*ImportMask2 im2 = new ImportMask2();
			im2.Checked = this.Checked;
			im2.SelectedFile = this.selectedFile;
			im2.InputDelimiter = this.InputDelimiter;
			im2.ShowDialog();*/	
		}
		#endregion � bNext_Click �
		#region � Find Delimiter �
		private Delimiter FindDelimiter(string file)
		{
			string ext = file.Substring(file.Length - 3);
			return FindDelimiterInsideFile (file);
			/*switch (ext)
			{
				case "csv":
					return Delimiter.comma;
				case "txt":
					return FindDelimiterInsideFile(file);
				default:
					return Delimiter.comma;
			}*/
		}

		private Delimiter FindDelimiterInsideFile (string file)
		{
			try
			{
				using (StreamReader sr = new StreamReader(file)) 
				{				
					String line = string.Empty;
					if ((line = sr.ReadLine()) != null)
					{
						string tempLine = line;
						int iPos = 0;
						int iCnt = 0;
						while ((iPos = tempLine.IndexOf("\t")) != -1)
						{
							tempLine = tempLine.Substring(iPos + 1);
							iCnt++;
						}
						if (iCnt >= 1)
							return Delimiter.tab;
					
						tempLine = line;
						iPos = 0;
						iCnt = 0;
						while ((iPos = tempLine.IndexOf(";")) != -1)
						{
							tempLine = tempLine.Substring(iPos + 1);
							iCnt++;
						}
						if (iCnt >= 1)
							return Delimiter.semiComma;				
					
						else
							return Delimiter.comma;
					}
					else 
					{
						MessageBox.Show(UserMessages.EmptyFile);
						return Delimiter.comma;
					}
				}
			}
			catch (Exception exc)
			{
				MessageBox.Show(exc.Message);
				return Delimiter.comma;
			}
		}
		#endregion � Find Delimiter �
		#region � bCancel_Click �
		private void bCancel_Click(object sender, System.EventArgs e)
		{
			Globals.importWindowPos = this.Location;
			if (DialogResult.Yes == MessageBox.Show(this, UserMessages.CancelImportText, UserMessages.CancelImport, MessageBoxButtons.YesNo, MessageBoxIcon.Question))
			{
				Globals.importWizardStarted = false;
				//this.Close();
			}
		}
		#endregion � bCancel_Click �
		#region � ImportMask1_Load �
		private void ImportMask1_Load(object sender, System.EventArgs e)
		{			

		}
		#endregion � ImportMask1_Load �
		#region � GetSheetNames �
		private void GetSheetNames()
		{
			/*try
			{
				
					//Dim oApp As New Excel.Application()
					//oApp.Visible = True
					//oApp.UserControl = True
					//Dim oldCI As System.Globalization.CultureInfo = System.Threading.Thread.CurrentThread.CurrentCulture
					//System.Threading.Thread.CurrentThread.CurrentCulture = New System.Globalization.CultureInfo("en-US")
					//oApp.Workbooks.Add()
					//System.Threading.Thread.CurrentThread.CurrentCulture = oldCI 
				 
				 

				//BUG "Old format or invalid type library" WorkAround ()
				System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
				
				this.cbSheetName.Items.Clear();
				if (null == _mExcelApp)
					_mExcelApp = new Excel.Application();
				//  See if the Excel Application Object was successfully constructed
				if (_mExcelApp == null) 
				{  
					MessageBox.Show("ERROR: EXCEL couldn't be started!");  				
				}
				_mExcelApp.DisplayAlerts = false;
				Excel.Workbook theWorkbook = _mExcelApp.Workbooks.Open(
					this.tbSelectedFile.Text, 0, true, 5,"", "", true, Excel.XlPlatform.xlWindows, "\t", false, 
					false, 0, true);    
				// get the collection of sheets in the workbook    
				sheets = theWorkbook.Worksheets;   
				Hashtable htSheets = new Hashtable();
				// get the first and only worksheet from the collection   
				// of worksheets    
				for (int i = 0; i < sheets.Count; i++)
				{
					Excel.Worksheet tempWorksheet = (Excel.Worksheet)sheets.get_Item(i + 1);
					htSheets.Add(i, tempWorksheet.Name);
					this.cbSheetName.Items.Add(tempWorksheet.Name);
				}

				//this.cbSheetName.SelectedIndex = 0;
			}
			catch (Exception exc)
			{
				MessageBox.Show(exc.Message);
			}*/
			
		}
		#endregion � GetSheetNames �
		#region � cbSheetName_SelectedIndexChanged �
		private void cbSheetName_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			Globals.selectedExcelIndex = ((ComboBox)sender).SelectedIndex + 1;			
			worksheet = (Excel.Worksheet)sheets.get_Item(Globals.selectedExcelIndex);

			string newName = this.tbSelectedFile.Text.Replace(".xls", string.Empty) + worksheet.Name + ".csv";
			if (File.Exists(newName))
			{
				File.Delete(newName);
			}
			//worksheet.SaveAs(newName, Excel.XlFileFormat.xlCSV, Type.Missing, Type.Missing, Excel.XlSaveAsAccessMode.xlNoChange /*Type.Missing*/, Type.Missing, Type.Missing, Type.Missing, Type.Missing);			
			
			Globals.selectedExcelFile = newName;
			this.tbSelectedFile.Text = newName;
		}
		#endregion � cbSheetName_SelectedIndexChanged �
		#region � CloseExcel �
		public void CloseExcel()
		{
			try
			{
				//
				// Close the Excel application. 
				// Tell the stupid interop layer to release the object. 
				//
				_mExcelApp.Quit();
				System.Runtime.InteropServices.Marshal.ReleaseComObject(_mExcelApp);
			}
			catch(Exception exc)
			{
				MessageBox.Show(exc.Message);
			}
		}
		#endregion � CloseExcel �

		private void ImportMask1_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (true == e.Cancel)
			{
				if (DialogResult.Yes == MessageBox.Show(this, UserMessages.CancelImportText, UserMessages.CancelImport, MessageBoxButtons.YesNo, MessageBoxIcon.Question))
				{
					Globals.importWizardStarted = false;
					//this.Close();
				}
			}
		}
		#endregion � Methods �		
	}
}
