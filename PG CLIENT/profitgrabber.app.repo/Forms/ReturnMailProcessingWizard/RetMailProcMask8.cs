using System;
using System.IO;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using DealMaker.Mapi;
using DealMaker.Export;
using DealMaker.Filters;
using DealMaker.Forms.Wizard;

using DealMaker.PlainClasses.ReturnMailWizard;

namespace DealMaker
{
	/// <summary>
	/// Summary description for RetMailProcMask8.
	/// </summary>
	public class RetMailProcMask8 : System.Windows.Forms.UserControl
	{
		private ReturnMailWizardObject rmwo = null;
		private System.Windows.Forms.Label exportedCountLabel;
		private System.Windows.Forms.Label totalExportedLabel;
		private System.Windows.Forms.Label locationInfoLabel;
		private System.Windows.Forms.Label locationLabel;
		private System.Windows.Forms.Label label1;
		private System.ComponentModel.Container components = null;

		public RetMailProcMask8(ReturnMailWizardObject rmwo)
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

			this.rmwo = rmwo;	

		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		public ReturnMailWizardObject RMWO
		{
			get { return this.rmwo; }
			set { this.rmwo = value; }
		}

		public void ReAssignContactsToNewGroups()
		{
			ArrayList propertyItemIds = new ArrayList();
			PropertyItem[] propertyItems = (PropertyItem[])this.RMWO.PropertyItems.ToArray(typeof(PropertyItem));
			
			
			foreach (PropertyItem pi in propertyItems)
				propertyItemIds.Add(pi.IdPropertyItem);

			SkipTracedContactAssistant.Instance.ProcessScannedRecords(propertyItemIds);
		}

		public void Init()
		{
			PropertyItemsExport export;
			//SelectContactsPage csp = WizardForm[0] as SelectContactsPage;
			//CreateExportProfilePage cepp = WizardForm[2] as CreateExportProfilePage;
			//CreateExportFilterPage cefp = WizardForm[3] as CreateExportFilterPage;
			//SelectExportLocationPage selp = WizardForm[6] as SelectExportLocationPage;

			exportedCountLabel.Text = "exporting...";
			
			locationLabel.Text = this.RMWO.ExportFileDocument;

			export = new PropertyItemsExport((PropertyItem[])this.RMWO.PropertyItems.ToArray(typeof(PropertyItem)));

			foreach (ExportAttributeMapping mapping in this.RMWO.SelectedFields)
				export.AddMapping(mapping);

			//foreach (IPropertyItemFilter filter in cefp.Filters)
			//	export.AddFilter(filter);	
	
			export.ExportFileName = this.RMWO.ExportFileDocument;
			//export.ExportWriter = selp.ExportWriter;

			exportedCountLabel.Text = export.Execute().ToString();

			if (this.RMWO.EMail)
			{
				SimpleMapi.SendDocuments(
					new String[] {this.RMWO.ExportFileDocument},
					new String[] {this.RMWO.ExportFileName}					
					);
			}						
		}

		#region Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.exportedCountLabel = new System.Windows.Forms.Label();
			this.totalExportedLabel = new System.Windows.Forms.Label();
			this.locationInfoLabel = new System.Windows.Forms.Label();
			this.locationLabel = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// exportedCountLabel
			// 
			this.exportedCountLabel.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.exportedCountLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(238)));
			this.exportedCountLabel.Location = new System.Drawing.Point(192, 120);
			this.exportedCountLabel.Name = "exportedCountLabel";
			this.exportedCountLabel.Size = new System.Drawing.Size(64, 16);
			this.exportedCountLabel.TabIndex = 9;
			this.exportedCountLabel.Text = "exporting...";
			// 
			// totalExportedLabel
			// 
			this.totalExportedLabel.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.totalExportedLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(238)));
			this.totalExportedLabel.Location = new System.Drawing.Point(8, 120);
			this.totalExportedLabel.Name = "totalExportedLabel";
			this.totalExportedLabel.Size = new System.Drawing.Size(184, 16);
			this.totalExportedLabel.TabIndex = 8;
			this.totalExportedLabel.Text = "TOTAL # of Records exported:";
			// 
			// locationInfoLabel
			// 
			this.locationInfoLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.locationInfoLabel.Location = new System.Drawing.Point(8, 8);
			this.locationInfoLabel.Name = "locationInfoLabel";
			this.locationInfoLabel.Size = new System.Drawing.Size(352, 16);
			this.locationInfoLabel.TabIndex = 11;
			this.locationInfoLabel.Text = "Your export file is saved in the  following location:";
			// 
			// locationLabel
			// 
			this.locationLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.locationLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(238)));
			this.locationLabel.Location = new System.Drawing.Point(48, 32);
			this.locationLabel.Name = "locationLabel";
			this.locationLabel.Size = new System.Drawing.Size(312, 40);
			this.locationLabel.TabIndex = 12;
			// 
			// label1
			// 
			this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.label1.Location = new System.Drawing.Point(16, 80);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(352, 16);
			this.label1.TabIndex = 13;
			this.label1.Text = "Please, write down the location of your exported file!";
			// 
			// RetMailProcMask8
			// 
			this.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(210)), ((System.Byte)(216)), ((System.Byte)(240)));
			this.Controls.Add(this.label1);
			this.Controls.Add(this.locationLabel);
			this.Controls.Add(this.locationInfoLabel);
			this.Controls.Add(this.exportedCountLabel);
			this.Controls.Add(this.totalExportedLabel);
			this.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.Name = "RetMailProcMask8";
			this.Size = new System.Drawing.Size(368, 176);
			this.ResumeLayout(false);

		}
		#endregion
	}
}
