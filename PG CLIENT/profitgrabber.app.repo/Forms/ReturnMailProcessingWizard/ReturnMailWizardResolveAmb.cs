using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace DealMaker
{
	/// <summary>
	/// Summary description for ReturnMailWizardResolveAmb.
	/// </summary>
	public class ReturnMailWizardResolveAmb : System.Windows.Forms.Form
	{
		bool bValid = false;
		private Guid selectedId = Guid.Empty;
		private ArrayList alPropertyItems = new ArrayList();
		private System.Windows.Forms.ListView lvItems;
		private System.Windows.Forms.ColumnHeader fullName;
		private System.Windows.Forms.ColumnHeader ssa;
		private System.Windows.Forms.ColumnHeader msa;
		private System.Windows.Forms.Button bOK;
		private System.Windows.Forms.Button bCancel;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public bool Valid
		{
			get {return this.bValid;}
		}

		public Guid SelectedGuid
		{
			get {return this.selectedId;}
		}
		
		public ArrayList PropertyItems
		{
			get {return this.alPropertyItems;}
			set {this.alPropertyItems = value;}
		}

		public ReturnMailWizardResolveAmb()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.lvItems = new System.Windows.Forms.ListView();
			this.fullName = new System.Windows.Forms.ColumnHeader();
			this.ssa = new System.Windows.Forms.ColumnHeader();
			this.msa = new System.Windows.Forms.ColumnHeader();
			this.bOK = new System.Windows.Forms.Button();
			this.bCancel = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// lvItems
			// 
			this.lvItems.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.lvItems.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
																					  this.fullName,
																					  this.ssa,
																					  this.msa});
			this.lvItems.FullRowSelect = true;
			this.lvItems.GridLines = true;
			this.lvItems.Location = new System.Drawing.Point(8, 8);
			this.lvItems.Name = "lvItems";
			this.lvItems.Size = new System.Drawing.Size(526, 128);
			this.lvItems.TabIndex = 0;
			this.lvItems.View = System.Windows.Forms.View.Details;
			// 
			// fullName
			// 
			this.fullName.Text = "Full Name";
			this.fullName.Width = 149;
			// 
			// ssa
			// 
			this.ssa.Text = "Site Street Address";
			this.ssa.Width = 182;
			// 
			// msa
			// 
			this.msa.Text = "Mail Street Address";
			this.msa.Width = 190;
			// 
			// bOK
			// 
			this.bOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.bOK.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.bOK.Location = new System.Drawing.Point(456, 152);
			this.bOK.Name = "bOK";
			this.bOK.TabIndex = 1;
			this.bOK.Text = "OK";
			this.bOK.Click += new System.EventHandler(this.bOK_Click);
			// 
			// bCancel
			// 
			this.bCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.bCancel.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.bCancel.Location = new System.Drawing.Point(368, 152);
			this.bCancel.Name = "bCancel";
			this.bCancel.TabIndex = 2;
			this.bCancel.Text = "Cancel";
			this.bCancel.Click += new System.EventHandler(this.bCancel_Click);
			// 
			// ReturnMailWizardResolveAmb
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(210)), ((System.Byte)(216)), ((System.Byte)(240)));
			this.ClientSize = new System.Drawing.Size(546, 192);
			this.Controls.Add(this.bCancel);
			this.Controls.Add(this.bOK);
			this.Controls.Add(this.lvItems);
			this.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
			this.Name = "ReturnMailWizardResolveAmb";
			this.ShowInTaskbar = false;
			this.Text = "Resolve Ambiguity";
			this.Load += new System.EventHandler(this.ReturnMailWizardResolveAmb_Load);
			this.ResumeLayout(false);

		}
		#endregion

		private void bOK_Click(object sender, System.EventArgs e)
		{
			if (null != this.lvItems.SelectedItems)
			{
				this.bValid = true;
				this.selectedId = (Guid)this.lvItems.SelectedItems[0].Tag;
				this.Close();
			}			
		}

		private void ReturnMailWizardResolveAmb_Load(object sender, System.EventArgs e)
		{
			foreach (string propItemId in this.alPropertyItems)
			{
				PropertyItem pi = PropertyItem.LoadPropertyItem(new Guid(propItemId));
				if (null != pi)
				{
					ListViewItem lvi = null;
					
					if (null != pi.Owner)
						lvi = new ListViewItem(pi.Owner.FullName);
					else
						lvi = new ListViewItem(string.Empty);	

					if (null != pi.Owner.SiteAddress)
						lvi.SubItems.Add(pi.Owner.SiteAddress.FullSiteStreetAddress);

					if (null != pi.Owner.MailAddress)
						lvi.SubItems.Add(pi.Owner.MailAddress.FullSiteStreetAddress);

					lvi.Tag = (Guid)pi.IdPropertyItem;
					this.lvItems.Items.Add(lvi);
				}
			}
		}

		private void bCancel_Click(object sender, System.EventArgs e)
		{
			this.bValid = false;
			this.Close();
		}
	}
}
