using System;
using System.IO;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using DealMaker.Export;
using DealMaker.Forms.Wizard;

namespace DealMaker
{
	/// <summary>
	/// Summary description for RetMailProcMask7.
	/// </summary>
	public class RetMailProcMask7 : System.Windows.Forms.UserControl
	{
		private ReturnMailWizardObject rmwo = null;
		private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog;
		private System.Windows.Forms.Label selectFileLabel;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label nameLabel;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TextBox locationTextBox;
		private System.Windows.Forms.TextBox nameTextBox;
		private System.Windows.Forms.ComboBox typeComboBox;
		private System.Windows.Forms.Button browseButton;
		private System.Windows.Forms.CheckBox emailCheckBox;
        private Label label2;
		private System.ComponentModel.Container components = null;		


		public RetMailProcMask7(ReturnMailWizardObject rmwo)
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

			this.rmwo = rmwo;	

			this.nameTextBox.Text = rmwo.OriginalFileName;

			nameTextBox.Leave += new EventHandler(NameTextBoxLeaveEventHandler);
			browseButton.Click += new EventHandler(BrowseButtonClickedEventHandler);
			this.emailCheckBox.Click += new EventHandler(EmailCheckStateChanged);
		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		public ReturnMailWizardObject RMWO
		{
			get { return this.rmwo; }
			set { this.rmwo = value; }
		}

		public string LocationTextBox
		{
			get { return this.locationTextBox.Text; }
			set { this.locationTextBox.Text = value; }
		}

		public string NameTextBox
		{
			get 
			{ 
				if (String.Empty == Path.GetExtension(nameTextBox.Text) && ExportWriter != null)
				{
					nameTextBox.Text = this.rmwo.OriginalFileName;// + ExportWriter.GetDefaultExtension();
				}
				return this.nameTextBox.Text; 
			}
			set { this.nameTextBox.Text = value; }
		}

		public string TypeSuffix
		{			
			get 
			{
				if (null != ExportWriter)
					return ExportWriter.GetDefaultExtension();
				else
					return string.Empty;
			}
		}

		public void Init()
		{
			string exportPath = Application.StartupPath + @"\" + Globals.ReturnedMailExportData;

			if (!Directory.Exists(exportPath))
				Directory.CreateDirectory(exportPath);

			locationTextBox.Text = exportPath;			
			if (null != this.rmwo)
			{
				this.emailCheckBox.Checked = this.rmwo.EMail;
			}

			typeComboBox.Items.AddRange(new object[]
				{
						new CSVExportWriter(),
					new TabDelimitedExportWriter()});
			typeComboBox.SelectedIndex = 0;
		}

		private void NameTextBoxLeaveEventHandler(object sender, EventArgs e)
		{
			//TA++ RQ_0069
			//if (String.Empty == Path.GetExtension(nameTextBox.Text) &&
			//	ExportWriter != null)
			//	nameTextBox.Text += ExportWriter.GetDefaultExtension();            

			this.rmwo.OriginalFileName = nameTextBox.Text;			
			//TA-- RQ_0069

		}

		private void EmailCheckStateChanged(object sender, EventArgs e)
		{
			this.RMWO.EMail = this.emailCheckBox.Checked;
		}
		

		private void BrowseButtonClickedEventHandler(object sender, EventArgs e)
		{
			folderBrowserDialog.SelectedPath = locationTextBox.Text;

			if (DialogResult.OK == folderBrowserDialog.ShowDialog())
				locationTextBox.Text = folderBrowserDialog.SelectedPath;
		}

		#region Properties
		public string ExportFileName
		{
			get
			{
				return locationTextBox.Text + "\\" +
					nameTextBox.Text;
			}
		}

		public string ExportFileDocument
		{
			get
			{
				return nameTextBox.Text;
			}
		}

		public IExportWriter ExportWriter
		{
			get
			{
				return typeComboBox.SelectedItem as IExportWriter;
			}
		}
		#endregion

		public bool AttachToEmail
		{
			get
			{
				return emailCheckBox.Checked;
			}

		}

		#region Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.folderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.selectFileLabel = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.locationTextBox = new System.Windows.Forms.TextBox();
            this.nameTextBox = new System.Windows.Forms.TextBox();
            this.nameLabel = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.typeComboBox = new System.Windows.Forms.ComboBox();
            this.browseButton = new System.Windows.Forms.Button();
            this.emailCheckBox = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // selectFileLabel
            // 
            this.selectFileLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.selectFileLabel.Location = new System.Drawing.Point(8, 8);
            this.selectFileLabel.Name = "selectFileLabel";
            this.selectFileLabel.Size = new System.Drawing.Size(360, 32);
            this.selectFileLabel.TabIndex = 0;
            this.selectFileLabel.Text = "Please select the location on your computer (local hard-disk) and name under whic" +
                "h you want to save your export.";
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(8, 48);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 20);
            this.label1.TabIndex = 1;
            this.label1.Text = "Location:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // locationTextBox
            // 
            this.locationTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.locationTextBox.Location = new System.Drawing.Point(72, 48);
            this.locationTextBox.Name = "locationTextBox";
            this.locationTextBox.Size = new System.Drawing.Size(248, 20);
            this.locationTextBox.TabIndex = 2;
            // 
            // nameTextBox
            // 
            this.nameTextBox.Location = new System.Drawing.Point(72, 80);
            this.nameTextBox.Name = "nameTextBox";
            this.nameTextBox.Size = new System.Drawing.Size(176, 20);
            this.nameTextBox.TabIndex = 5;
            // 
            // nameLabel
            // 
            this.nameLabel.Location = new System.Drawing.Point(8, 80);
            this.nameLabel.Name = "nameLabel";
            this.nameLabel.Size = new System.Drawing.Size(64, 20);
            this.nameLabel.TabIndex = 4;
            this.nameLabel.Text = "Name:";
            this.nameLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(8, 149);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(64, 20);
            this.label3.TabIndex = 6;
            this.label3.Text = "Type:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // typeComboBox
            // 
            this.typeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.typeComboBox.Location = new System.Drawing.Point(72, 149);
            this.typeComboBox.Name = "typeComboBox";
            this.typeComboBox.Size = new System.Drawing.Size(176, 21);
            this.typeComboBox.TabIndex = 7;
            // 
            // browseButton
            // 
            this.browseButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.browseButton.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.browseButton.Location = new System.Drawing.Point(328, 48);
            this.browseButton.Name = "browseButton";
            this.browseButton.Size = new System.Drawing.Size(24, 20);
            this.browseButton.TabIndex = 8;
            this.browseButton.Text = "...";
            // 
            // emailCheckBox
            // 
            this.emailCheckBox.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.emailCheckBox.Location = new System.Drawing.Point(72, 181);
            this.emailCheckBox.Name = "emailCheckBox";
            this.emailCheckBox.Size = new System.Drawing.Size(104, 24);
            this.emailCheckBox.TabIndex = 9;
            this.emailCheckBox.Text = "E-mail";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(69, 103);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(304, 20);
            this.label2.TabIndex = 10;
            this.label2.Text = "A file name cannot contain any of the following characters: \\ / : * ? \" < > |";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // RetMailProcMask7
            // 
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.Controls.Add(this.label2);
            this.Controls.Add(this.emailCheckBox);
            this.Controls.Add(this.browseButton);
            this.Controls.Add(this.typeComboBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.nameTextBox);
            this.Controls.Add(this.nameLabel);
            this.Controls.Add(this.locationTextBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.selectFileLabel);
            this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.Name = "RetMailProcMask7";
            this.Size = new System.Drawing.Size(376, 216);
            this.Load += new System.EventHandler(this.RetMailProcMask7_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		#endregion

		private void RetMailProcMask7_Load(object sender, System.EventArgs e)
		{
		
		}
		
	}
}
