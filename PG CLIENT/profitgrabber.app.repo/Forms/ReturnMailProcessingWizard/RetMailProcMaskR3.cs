#region � Using �
using System;
using System.IO;
using System.Text;
using System.Drawing;
using System.Reflection;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
#endregion � Using �

namespace DealMaker
{
	/// <summary>
	/// Summary description for RetMailProcMaskR3.
	/// </summary>
	public class RetMailProcMaskR3 : System.Windows.Forms.UserControl
	{
		#region � Data �
		private Delimiter inputDelimiter;
		private bool useMappingTable = false;
		private string columnName = string.Empty;
		private int columnClickedIndex = -1;
		//private int prevColumnClickedIndex = -1;
		//private Color prevColumnColor;
		private string selectedFile;
		private bool bChecked;
		private bool bCreateNewProfile = false;
		private string selectedProfile = string.Empty;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.ContextMenu ctxMenu;
		private System.Windows.Forms.ListView lvData;
		private System.Windows.Forms.MenuItem menuItem1;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox tbSelectedColumn;
		private System.Windows.Forms.Button bAssign;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Button bUnAssign;
		private System.Windows.Forms.Label label3;
        private Button _tagAll;
        private Button _unTagAll;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
		#endregion � Data �
		#region � Properties �
		#region � UseMappingTable �
		public bool UseMappingTable
		{
			get {return this.useMappingTable;}
			set {this.useMappingTable = value;}
		}
		#endregion � UseMappingTable �
		#region � InputDelimiter �
		public Delimiter InputDelimiter
		{
			get {return this.inputDelimiter;}
			set {this.inputDelimiter = value;}
		}
		#endregion � InputDelimiter �
		#region � SelectedFile �
		public string SelectedFile
		{
			get {return this.selectedFile;}
			set {this.selectedFile = value;}
		}
		#endregion � SelectedFile �
		#region � Checked �
		public bool Checked
		{
			get {return this.bChecked;}
			set {this.bChecked = value;}
		}
		#endregion � Checked �
		#region � CreateNewProfile �
		public bool CreateNewProfile
		{
			get {return this.bCreateNewProfile;}
			set {this.bCreateNewProfile = value;}
		}
		#endregion � CreateNewProfile �
		#region � SelectedProfile �
		public string SelectedProfile
		{
			get {return this.selectedProfile; }
			set {this.selectedProfile = value;}
		}		
		#endregion � SelectedProfile �
		#endregion � Properties �

		public RetMailProcMaskR3()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();
            AddEventHandlers();
		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.lvData = new System.Windows.Forms.ListView();
            this.ctxMenu = new System.Windows.Forms.ContextMenu();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.menuItem1 = new System.Windows.Forms.MenuItem();
            this.label1 = new System.Windows.Forms.Label();
            this.tbSelectedColumn = new System.Windows.Forms.TextBox();
            this.bAssign = new System.Windows.Forms.Button();
            this.bUnAssign = new System.Windows.Forms.Button();
            this._tagAll = new System.Windows.Forms.Button();
            this._unTagAll = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lvData
            // 
            this.lvData.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lvData.BackColor = System.Drawing.SystemColors.Window;
            this.lvData.CheckBoxes = true;
            this.lvData.ContextMenu = this.ctxMenu;
            this.lvData.FullRowSelect = true;
            this.lvData.GridLines = true;
            this.lvData.Location = new System.Drawing.Point(16, 72);
            this.lvData.MultiSelect = false;
            this.lvData.Name = "lvData";
            this.lvData.Size = new System.Drawing.Size(504, 240);
            this.lvData.TabIndex = 0;
            this.lvData.UseCompatibleStateImageBehavior = false;
            this.lvData.View = System.Windows.Forms.View.Details;
            this.lvData.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.lvData_ItemCheck);
            this.lvData.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.lvData_ColumnClick);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.lvData);
            this.groupBox1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.groupBox1.Location = new System.Drawing.Point(16, 8);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(536, 328);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Field Mapping / Assigning";
            // 
            // label3
            // 
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label3.Location = new System.Drawing.Point(16, 48);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(328, 23);
            this.label3.TabIndex = 2;
            this.label3.Text = "Step 2 - Tag only records that were successfully skiptraced.";
            // 
            // label2
            // 
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label2.Location = new System.Drawing.Point(16, 24);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(328, 23);
            this.label2.TabIndex = 1;
            this.label2.Text = "Step 1 - Click on Column Header to select and assign it";
            // 
            // menuItem1
            // 
            this.menuItem1.Index = -1;
            this.menuItem1.Text = "";
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label1.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.label1.Location = new System.Drawing.Point(16, 343);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 23);
            this.label1.TabIndex = 5;
            this.label1.Text = "Selected Column:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // tbSelectedColumn
            // 
            this.tbSelectedColumn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.tbSelectedColumn.Location = new System.Drawing.Point(120, 339);
            this.tbSelectedColumn.Name = "tbSelectedColumn";
            this.tbSelectedColumn.ReadOnly = true;
            this.tbSelectedColumn.Size = new System.Drawing.Size(184, 20);
            this.tbSelectedColumn.TabIndex = 6;
            // 
            // bAssign
            // 
            this.bAssign.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.bAssign.Enabled = false;
            this.bAssign.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.bAssign.Location = new System.Drawing.Point(392, 337);
            this.bAssign.Name = "bAssign";
            this.bAssign.Size = new System.Drawing.Size(75, 23);
            this.bAssign.TabIndex = 7;
            this.bAssign.Text = "Assign";
            this.bAssign.Click += new System.EventHandler(this.bAssign_Click);
            // 
            // bUnAssign
            // 
            this.bUnAssign.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.bUnAssign.Enabled = false;
            this.bUnAssign.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.bUnAssign.Location = new System.Drawing.Point(313, 337);
            this.bUnAssign.Name = "bUnAssign";
            this.bUnAssign.Size = new System.Drawing.Size(75, 23);
            this.bUnAssign.TabIndex = 8;
            this.bUnAssign.Text = "Un-Assign";
            this.bUnAssign.Click += new System.EventHandler(this.bUnAssign_Click);
            // 
            // _tagAll
            // 
            this._tagAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._tagAll.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._tagAll.Location = new System.Drawing.Point(392, 360);
            this._tagAll.Name = "_tagAll";
            this._tagAll.Size = new System.Drawing.Size(75, 23);
            this._tagAll.TabIndex = 9;
            this._tagAll.Text = "Tag All";
            // 
            // _unTagAll
            // 
            this._unTagAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._unTagAll.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._unTagAll.Location = new System.Drawing.Point(313, 360);
            this._unTagAll.Name = "_unTagAll";
            this._unTagAll.Size = new System.Drawing.Size(75, 23);
            this._unTagAll.TabIndex = 10;
            this._unTagAll.Text = "Un-Tag All";
            // 
            // RetMailProcMaskR3
            // 
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.Controls.Add(this._unTagAll);
            this.Controls.Add(this._tagAll);
            this.Controls.Add(this.bUnAssign);
            this.Controls.Add(this.bAssign);
            this.Controls.Add(this.tbSelectedColumn);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.groupBox1);
            this.Name = "RetMailProcMaskR3";
            this.Size = new System.Drawing.Size(576, 384);
            this.Load += new System.EventHandler(this.ImportMask3_Load);
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		#endregion		

        void AddEventHandlers()
        {
            _tagAll.Click += new EventHandler(On_TagAll_Click);
            _unTagAll.Click += new EventHandler(On_UnTagAll_Click);
        }        

		#region � Methods �
		#region � bNext_Click �
		public void bNext_Click(object sender, System.EventArgs e)
		{
			//if (false == this.CheckOwnerMapping())
			//{
			//	SpecifyOwnerName son = new SpecifyOwnerName();
			//	son.ShowDialog();
			//}
			//Globals.importWindowPos = this.Location;
			//MessageBox.Show(this, UserMessages.Warning, string.Empty, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
			//this.Close();
			/*ImportMask4 im4 = new ImportMask4();
			im4.SelectedFile = this.SelectedFile;
			im4.Checked = this.Checked;
			im4.CreateNewProfile = this.CreateNewProfile;
			im4.InputDelimiter = this.InputDelimiter;
			im4.SelectedProfile = this.SelectedProfile;
			im4.Show();*/
		}
		#endregion � bNext_Click �		
		#region � bPrev_Click �
		private void bPrev_Click(object sender, System.EventArgs e)
		{
			Globals.importWindowPos = this.Location;
			//this.Close();
			ImportMask2 im2 = new ImportMask2();
			im2.SelectedFile = this.SelectedFile;
			im2.Checked = this.Checked;
			im2.CreateNewProfile = this.CreateNewProfile;
			im2.InputDelimiter = this.InputDelimiter;
			im2.SelectedProfile = this.SelectedProfile;
			im2.Show();
		}
		#endregion � bPrev_Click �		
		#region � ImportMask3_Load �
		private void ImportMask3_Load(object sender, System.EventArgs e)
		{			
			if (true == this.UseMappingTable)
			{
				//this.useMappingTable = false;
			}
			else
			{
				//CLEAR TABLES !!!
				Globals.htMappingInfo.Clear();
				Globals.htFilters.Clear();
			}

			char delimiter = ',';
			switch (this.InputDelimiter)
			{
				case Delimiter.comma:
					delimiter = ',';
					break;
				case Delimiter.hash:
					delimiter = '#';
					break;
				case Delimiter.semiComma:
					delimiter = ';';
					break;
				case Delimiter.tab:
					delimiter = '\t';
					break;
			}
 
			this.PopulateListView(this.SelectedFile, this.Checked, delimiter, true);
			this.BuildMappingInfoTable();
			
			if (true == this.bChecked && this.lvData.Items.Count > 0)
			{
				//ListViewItem lvi = this.lvData.Items[0];
				//lvi.ForeColor = Color.Red;
				//lvi.BackColor = Color.LightGray;				
			}
			
			if (false == this.CreateNewProfile && false == this.UseMappingTable)
			{
				//load profile
				this.FillHashtables();				

			}
			if (false == this.CreateNewProfile && false == this.UseMappingTable)
			{				
				this.LoadFilters();
			}
			
			this.UpdateColumnNames();
			
			if (true == this.UseMappingTable)//reset it
			{
				this.useMappingTable = false;
			}

			if (true == this.Checked)
			{
				ListViewItem lvi = (ListViewItem)this.lvData.Items[0];
				lvi.Checked = false;
			}
		}
		#endregion � ImportMask3_Load �		
		#region � BuildMappingInfoTable �
		private void BuildMappingInfoTable()
		{
			//if (null == Globals.htRetMailWizardItemPosition)
			Globals.htRetMailWizardItemPosition = new Hashtable();
				
			
			Globals.htRetMailWizardItemPosition.Add(Globals.MRWContactId, 1);
			Globals.htRetMailWizardItemPosition.Add(Globals.MRWLNSAddress, 2);
			Globals.htRetMailWizardItemPosition.Add(Globals.MRWLNSNumber, 3);
			Globals.htRetMailWizardItemPosition.Add(Globals.MRWLNSPreDir, 4);
			Globals.htRetMailWizardItemPosition.Add(Globals.MRWLNSName, 5);
			Globals.htRetMailWizardItemPosition.Add(Globals.MRWLNSPostDir, 6);
			Globals.htRetMailWizardItemPosition.Add(Globals.MRWLNSSuffix, 7);
			Globals.htRetMailWizardItemPosition.Add(Globals.MRWLNSUnitNumber, 8);
			Globals.htRetMailWizardItemPosition.Add(Globals.MRWLNCity, 9);
			Globals.htRetMailWizardItemPosition.Add(Globals.MRWLNState, 10);
			Globals.htRetMailWizardItemPosition.Add(Globals.MRWLNZip, 11);
			Globals.htRetMailWizardItemPosition.Add(Globals.MRWSTPhoneNr1, 12);
			Globals.htRetMailWizardItemPosition.Add(Globals.MRWSTPhoneNr2, 13);

			MapperObjectInfo moiContactId	= new MapperObjectInfo(Globals.MRWContactId, "Contact Id");
			MapperObjectInfo moiLNSAddress	= new MapperObjectInfo(Globals.MRWLNSAddress, "Last Known Street Address");
			MapperObjectInfo moiLNSNumber	= new MapperObjectInfo(Globals.MRWLNSNumber, "Last Known Street Number");
			MapperObjectInfo moiLNSPreDir	= new MapperObjectInfo(Globals.MRWLNSPreDir, "Last Known Street PreDirectional");
			MapperObjectInfo moiLNSName		= new MapperObjectInfo(Globals.MRWLNSName, "Last Known Street Name");
			MapperObjectInfo moiLNSPostDir	= new MapperObjectInfo(Globals.MRWLNSPostDir, "Last Known Street PostDirectional");
			MapperObjectInfo moiLNSSuffix	= new MapperObjectInfo(Globals.MRWLNSSuffix, "Last Known Street Suffix");
			MapperObjectInfo moiLNSUnitNr	= new MapperObjectInfo(Globals.MRWLNSUnitNumber, "Last Known Street Unit Number");
			MapperObjectInfo moiLNCity	= new MapperObjectInfo(Globals.MRWLNCity, "Last Known City");
			MapperObjectInfo moiLNState	= new MapperObjectInfo(Globals.MRWLNState, "Last Known State");
			MapperObjectInfo moiLNZip	= new MapperObjectInfo(Globals.MRWLNZip, "Last Known ZIP");
			MapperObjectInfo moiSTPhoneNr1	= new MapperObjectInfo(Globals.MRWSTPhoneNr1, "Skip Traced Phone Number 1");
			MapperObjectInfo moiSTPhoneNr2	= new MapperObjectInfo(Globals.MRWSTPhoneNr2, "Skip Traced Phone Number 2");
				
			Globals.htMappingInfo.Add(moiContactId, new MapperSourceInfo());
			Globals.htMappingInfo.Add(moiLNSAddress, new MapperSourceInfo());
			Globals.htMappingInfo.Add(moiLNSNumber, new MapperSourceInfo());
			Globals.htMappingInfo.Add(moiLNSPreDir, new MapperSourceInfo());
			Globals.htMappingInfo.Add(moiLNSName, new MapperSourceInfo());
			Globals.htMappingInfo.Add(moiLNSPostDir, new MapperSourceInfo());
			Globals.htMappingInfo.Add(moiLNSSuffix, new MapperSourceInfo());
			Globals.htMappingInfo.Add(moiLNSUnitNr, new MapperSourceInfo());
			Globals.htMappingInfo.Add(moiLNCity, new MapperSourceInfo());
			Globals.htMappingInfo.Add(moiLNState, new MapperSourceInfo());
			Globals.htMappingInfo.Add(moiLNZip, new MapperSourceInfo());
			Globals.htMappingInfo.Add(moiSTPhoneNr1, new MapperSourceInfo());
			Globals.htMappingInfo.Add(moiSTPhoneNr2, new MapperSourceInfo());


			/*ImportIntermedeateObject iio = new ImportIntermedeateObject();
			Type iioType = iio.GetType();			
			PropertyInfo[] pi =  iioType.GetProperties();			
			for (int i = 0; i < pi.Length; i++)
			{


				object[] attr = pi[i].GetCustomAttributes(typeof(DescriptionAttribute), true);				
				if (0 != attr.Length)
				{      
					DescriptionAttribute da = (DescriptionAttribute)attr[0];
					MapperObjectInfo moi = new MapperObjectInfo(pi[i].Name, da.Desc);
					if (false == Globals.htMappingInfo.ContainsKey(moi))
					{
						Globals.htMappingInfo.Add(moi, new MapperSourceInfo());
					}
				}
			}
			*/
		}
		#endregion � BuildMappingInfoTable �
		#region � PopulateContextMenu �
		private void PopulateContextMenu()
		{
			ImportIntermedeateObject iio = new ImportIntermedeateObject();
			Type iioType = iio.GetType();			
			PropertyInfo[] pi =  iioType.GetProperties();			
			for (int i = 0; i < pi.Length; i++)
			{
				//int fieldvalue = (Int32)fields[i].GetValue(null);				
				MenuItem	mi = new MenuItem(pi[i].Name);				
				//pi[i].Attributes
				mi.Click += new System.EventHandler(this.menuItem_Click);
				this.ctxMenu.MenuItems.Add(mi);
			}			
		}
		#endregion � PopulateContextMenu �
		
		

		#region � PopulateListView  �
		private void PopulateListView (string selectedFile, bool firstRowContainsHeaders, char separator, bool createNewProfile)
		{			
			int lnCnt = 0;
			using (StreamReader sr = new StreamReader(selectedFile))
			{
				
				string completeFile = sr.ReadToEnd();
				for (int i = 0; i < completeFile.Length; i++)
				{
					if (completeFile[i] == '\n')
					{
						lnCnt++;
					}
				}
			}
			Globals.Capacity = lnCnt;

			//Event trigger
			EventTrigger et = new EventTrigger();
			PBForm pbf = new PBForm(et);
			pbf.Capacity = lnCnt;
			pbf.WindowName = "Loading Data...";			
			pbf.Show();
			

			ArrayList alItems = new ArrayList();
			
			if (this.InputDelimiter == Delimiter.comma)
			{
				using (CSVReader csv = new CSVReader(this.SelectedFile)) 
				{	
			
					
					string[] fields;
					while ((fields = csv.GetCSVLine()) != null) 
					{						
						ArrayList alRow = new ArrayList();		
						foreach (string field in fields) 
						{
							alRow.Add(field);
						}
						alItems.Add(alRow);
						
						//fire event
						et.TriggerChange();
					}
				}
			}
			else
			{
				using (StreamReader sr = new StreamReader(selectedFile)) 
				{	
								
					String line = string.Empty;
					while ((line = sr.ReadLine()) != null)
					{
						if (line == string.Empty)
						{
							continue;
						}
															
						ArrayList alRow = new ArrayList();
						if (separator != ',')
						{
							while (-1 != line.IndexOf(separator))
							{
								string columnItem = line.Substring(0, line.IndexOf(separator));						
								line = line.Substring(line.IndexOf(separator) + 1);
								alRow.Add(columnItem);
							}
							//Add last element
							if (0 != line.Length)
							{
								line = line.Trim();
								if (0 != line.Length)
								{
									alRow.Add(line);
								}
							}
							alItems.Add(alRow);
						}
						
						
						//fire event
						et.TriggerChange();
					}
					sr.Close();
				}			
			}
			
			#region � Display  Header Names �
			if (true == createNewProfile && alItems.Count > 0)
			{
				//set UnAssiged
				ArrayList alRow = (ArrayList)alItems[0];
				for (int iCnt = 0; iCnt < alRow.Count; iCnt++)
				{
					ColumnHeader ch = new ColumnHeader();
					ch.Text = UserMessages.UnAssigned;
					ch.Width = 100;
					this.lvData.Columns.Add(ch);
				}
			}
			#endregion � Display  Header Names �
			
				
			//TA++23.08.2005
			//OLD++
			//int listViewMaxCapacity = (alItems.Count > 100) ? 100 : alItems.Count;
			//OLD--
			//NEW++
			int listViewMaxCapacity = alItems.Count;
			//NEW--
			//TA--23.08.2005
			for (int i = 0; i < listViewMaxCapacity; i++)
			{
				ArrayList alRow = (ArrayList)alItems[i];
				ListViewItem lvi = null;
				for (int j = 0; j < alRow.Count; j++)
				{
					if (0 == j)
					{
						lvi = new ListViewItem((string)alRow[j]);
					}
					else
					{				
						lvi.SubItems.Add((string)alRow[j]);
					}					
				}
				if (null != lvi)
				{
					this.lvData.Items.Add(lvi);										
				}
			}
			
		}
		#endregion � PopulateListView  �
		#region � menuItem_Click �
		private void menuItem_Click(object sender, System.EventArgs e)
		{
			//MessageBox.Show(sender.ToString() + " " + e.ToString());
		}
		#endregion � menuItem_Click �
		#region � SetHeaderColors �
		private void SetHeaderColors(ListViewItem lvi)
		{
			
			//set all to white
			for (int i = 0; i < this.lvData.Columns.Count; i++)
			{
				lvi.SubItems[i].BackColor = Color.White;
			}
			
			//set assiged to yellow
			IDictionaryEnumerator ide = Globals.htMappingInfo.GetEnumerator();
			while (ide.MoveNext())
			{
				MapperSourceInfo msi = (MapperSourceInfo)ide.Value;
				if (-1 != msi.ColumnIndex)
				{
					MapperObjectInfo moi = (MapperObjectInfo)ide.Key;					
					lvi.SubItems[msi.ColumnIndex].BackColor = Color.Yellow;
				}					
			}

			//set selected to lightblue
			lvi.SubItems[this.columnClickedIndex].BackColor = Color.LightBlue;			
			this.lvData.EndUpdate();
		}
		#endregion � SetHeaderColors �
		#region � lvData_ColumnClick �
		private void lvData_ColumnClick(object sender, System.Windows.Forms.ColumnClickEventArgs e)
		{	
			this.lvData.BeginUpdate();		
			if (false == this.bAssign.Enabled)
				this.bAssign.Enabled = true;

			ListView lv = (ListView)sender;
			ListViewItem lvi = this.lvData.Items[0];			
			/*
			if (-1 != this.prevColumnClickedIndex)
			{
				lvi.SubItems[this.prevColumnClickedIndex].BackColor = prevColumnColor;
			}*/

			this.columnClickedIndex = e.Column;
			//this.prevColumnClickedIndex = this.columnClickedIndex;
			//this.prevColumnColor = lvi.SubItems[this.columnClickedIndex].BackColor;

			//lvi.SubItems[this.columnClickedIndex].BackColor = Color.Blue;
			this.columnName = lvi.SubItems[e.Column].Text;

			if (this.bChecked)
				this.tbSelectedColumn.Text = "'" + this.columnName + "', Column: " + this.columnClickedIndex.ToString();
			else
				this.tbSelectedColumn.Text = UserMessages.UnKnown + ", Column: " + this.columnClickedIndex.ToString();		
			
			this.bUnAssign.Enabled = this.CheckIfAssigned(this.columnClickedIndex);				
			this.SetHeaderColors(lvi);			
		}
		#endregion � lvData_ColumnClick �						
		#region � CheckIfAssigned �
		private bool CheckIfAssigned(int columnIndex)
		{			
			IDictionaryEnumerator ide = Globals.htMappingInfo.GetEnumerator();
			while (ide.MoveNext())
			{
				MapperSourceInfo msi = (MapperSourceInfo)ide.Value;
				if (msi.ColumnIndex == columnIndex)
				{
					MapperObjectInfo moi = (MapperObjectInfo)ide.Key;					
					return true;
				}
			}
			return false;
		}
		#endregion � CheckIfAssigned �			
		#region � bAssign_Click �
		private void bAssign_Click(object sender, System.EventArgs e)
		{
			using (RetMailMapperForm mf1 = new RetMailMapperForm()) 
			{
				try
				{	
					//check if already assigned
					if (false == this.CheckIfAssigned(this.columnClickedIndex))
					{
						mf1.ShowDialog();
					
						if (null != mf1.MappedObject)
						{
							MapperObjectInfo mio = (MapperObjectInfo)mf1.MappedObject;
							MapperSourceInfo msi = new MapperSourceInfo(this.columnClickedIndex, this.columnName);

							//update mapping info
							if (Globals.htMappingInfo.ContainsKey(mio))
							{
								Globals.htMappingInfo[mio] = msi;						
							}
							//update columnName
							this.UpdateColumnNames();
						}
					}
					else
					{
						MessageBox.Show(this, UserMessages.AlreadyAssignedDesc, UserMessages.AlreadyAssigned, MessageBoxButtons.OK, MessageBoxIcon.Stop);
					}
				}
				catch(System.Runtime.InteropServices.SEHException sehe)
				{
					MessageBox.Show(sehe.Message);
				}
				catch(Exception exc)
				{					
					MessageBox.Show(exc.Message);
				}
				finally
				{
					mf1.Dispose();					
				}
			}		
		}
		#endregion � bAssign_Click �			
		#region � UpdateColumnNames �
		private void UpdateColumnNames()
		{
			try
			{
				//this.lvData.SuspendLayout();
				this.lvData.BeginUpdate();
				ListViewItem lvi = this.lvData.Items[0];
				lvi.UseItemStyleForSubItems = false;

				IDictionaryEnumerator ide = Globals.htMappingInfo.GetEnumerator();
				while (ide.MoveNext())
				{
					MapperSourceInfo msi = (MapperSourceInfo)ide.Value;
					if (-1 != msi.ColumnIndex)
					{
						MapperObjectInfo moi = (MapperObjectInfo)ide.Key;					
						ColumnHeader chOriginal = (ColumnHeader)this.lvData.Columns[msi.ColumnIndex];
						chOriginal.Text = moi.PropertyDesc;
						chOriginal.Width = moi.PropertyDesc.Length * 9;
						
						//ListViewItem.ListViewSubItem subItem = lvi.SubItems[msi.ColumnIndex];
						//subItem.BackColor = Color.Yellow;
						lvi.SubItems[msi.ColumnIndex].BackColor = Color.Yellow;
						/*for (int i = 0; i < this.lvData.Items.Count; i++)
						{							
							lvi = this.lvData.Items[i];	
							lvi.UseItemStyleForSubItems = false;
							lvi.SubItems[msi.ColumnIndex].BackColor = Color.Yellow;				
						}*/						
					}					
				}
				this.lvData.EndUpdate();
				//this.lvData.ResumeLayout();
			}
			catch(Exception 
#if DEBUG
				exc
#endif
				)
			{
#if DEBUG
				MessageBox.Show("Exception\nMessage: " + exc.Message + "\nInner Exception: " + exc.InnerException);
#endif
				MessageBox.Show(this, UserMessages.WrongBinding, UserMessages.WrongBindingHeader, MessageBoxButtons.OK, MessageBoxIcon.Error);
				Panel p = (Panel)this.Parent;
				ImportMask im = (ImportMask)p.Parent;

				im.bPrevious_Click (this, null);
			
				//return;
			}
		}
		#endregion � UpdateColumnNames �
		#region � CheckPropertyItemIdMapping �
		public bool CheckPropertyItemIdMapping()
		{			
			IDictionaryEnumerator ide = Globals.htMappingInfo.GetEnumerator();
			while (ide.MoveNext())
			{
				MapperObjectInfo moi = (MapperObjectInfo)ide.Key;				
				if (moi.PropertyName == "ContactId")
				{
					MapperSourceInfo msi = (MapperSourceInfo)ide.Value;
					if (-1 != msi.ColumnIndex)
						return true;
				}
			}
			return false;
		}
		#endregion � CheckPropertyItemIdMapping �
		#region � bUnAssign_Click �
		private void bUnAssign_Click(object sender, System.EventArgs e)
		{
			MapperSourceInfo msi = new MapperSourceInfo(this.columnClickedIndex, this.columnName);
			IDictionaryEnumerator ide = Globals.htMappingInfo.GetEnumerator();
			while (ide.MoveNext())
			{
				MapperSourceInfo msiFound = (MapperSourceInfo)ide.Value;
				if (msiFound.ColumnIndex == msi.ColumnIndex)
				{
					MapperSourceInfo newMsi = new MapperSourceInfo();
					Globals.htMappingInfo[(MapperObjectInfo)ide.Key] = newMsi;
					break;
				}
			}
			this.bUnAssign.Enabled = false;
			
			ColumnHeader chOriginal = (ColumnHeader)this.lvData.Columns[this.columnClickedIndex];
			chOriginal.Text = UserMessages.UnAssigned;
			chOriginal.Width = 100;
			
		}
		#endregion � bUnAssign_Click �				
		#region � FillFilters �		
		private void FillFilters(ref StreamReader sr)
		{
			string line = string.Empty;
			while ((line = sr.ReadLine()) != null) 
			{
				if (Globals.SimpleFilterStarter == line)
				{
					//SimpleFilter
					if ((line = sr.ReadLine()) != null)
					{

					}
				}
				if (Globals.MathFilterStarter == line)
				{
					if ((line = sr.ReadLine()) != null)
					{

					}
				}
			}
		}
		#endregion � FillFilters �		
		#region � LoadFilters �
		private void LoadFilters ()
		{
			Globals.htFilters.Clear();
			bool mathFilter = false;
			bool simpleFilter = false;
			
			using (StreamReader sr1 = new StreamReader(this.SelectedProfile)) 
			{
				char separator = '\t';
				string line = string.Empty;
				int iLineCnt = 0;
								
				while ((line = sr1.ReadLine()) != null)
				{					
					if (line == Globals.MathFilterStarter)
					{
						mathFilter = true;
						continue;
					}
					if (line == Globals.MathFilterEnder)
					{
						mathFilter = false;
						continue;
					}
					if (line == Globals.SimpleFilterStarter)
					{
						simpleFilter = true;
						continue;
					}
					if (line == Globals.SimpleFilterEnder)					
					{
						simpleFilter = false;
						continue;
					}

					if (true == simpleFilter)
					{
						iLineCnt++;
						int iSep1 = 0, iSep2 = 0;
						iSep1 = line.IndexOf(separator);
						iSep2 = line.IndexOf(separator, iSep1 + 1);
						string leftPropertyName = line.Substring(0, iSep1);
						string oper	= line.Substring(iSep1 + 1, iSep2 - iSep1 - 1);
						string val  = line.Substring(iSep2 + 1);
						try
						{
							double dVal = Convert.ToDouble(val);							
							SimpleFilter sf = new SimpleFilter();
							sf.DataColumn = leftPropertyName;
							sf.Condition = oper;
							sf.ValueDefined = dVal;
							Globals.htFilters.Add("sf" + iLineCnt.ToString(), sf);

						}
						catch (Exception exc)
						{
							MessageBox.Show("SimpleFilter failed to load! " + exc.Message);
						}
										
					}
					else if (true == mathFilter)
					{
						int iSep1 = 0, iSep2 = 0, iSep3 = 0, iSep4 = 0;
						iSep1 = line.IndexOf(separator);
						iSep2 = line.IndexOf(separator, iSep1 + 1);
						iSep3 = line.IndexOf(separator, iSep2 + 1);
						iSep4 = line.IndexOf(separator, iSep3 + 1);
						string leftPropertyName = line.Substring(0, iSep1);
						string rightPropertyName = line.Substring(iSep1 + 1, iSep2 - iSep1 - 1);
						string oper1 = line.Substring(iSep2 + 1, iSep3 - iSep2 - 1);
						string oper2  = line.Substring(iSep3 + 1, iSep4 - iSep3 - 1);
						string val =  line.Substring(iSep4 + 1);
						try
						{
							double dVal = Convert.ToDouble(val);
							MathematicalFilter mf = new MathematicalFilter(leftPropertyName, rightPropertyName, oper1, oper2, dVal);
							Globals.htFilters.Add("mf", mf);
						}
						catch (Exception exc)
						{
							MessageBox.Show("MathFiler failed to load! " + exc.Message);
						}
					}				
				}
				sr1.Close();			
			}
		}
		#endregion � LoadFilters �
		#region � FillHashtables �
		private void FillHashtables ()
		{			
			using (StreamReader sr = new StreamReader(this.SelectedProfile)) 
			{
				char separator = '\t';
				string line = string.Empty;
				int iLineCnt = 0;
				while ((line = sr.ReadLine()) != null) 
				{
					iLineCnt++;
					if (1 == iLineCnt)
					{
						continue;	//profile desc.
					}
					
					
					if (line == Globals.MapperDelimiter)
					{
						break;						
					}
					
					int iPos1 = 0, iPos2 = 0, iPos3 = 0;
					iPos1 = line.IndexOf(separator);
					iPos2 = line.IndexOf(separator, iPos1 + 1);
					iPos3 = line.IndexOf(separator, iPos2 + 1);
					
					string propertyName = line.Substring(0, iPos1);
					propertyName = propertyName.TrimEnd(' ');
					
					string propertyDesc = line.Substring(iPos1 + 1, iPos2 - iPos1 - 1);
					propertyDesc = propertyDesc.TrimEnd(' ');
					
					string columnIndex = line.Substring(iPos2 + 1, iPos3 - iPos2 - 1);
					columnIndex = columnIndex.TrimEnd(' ');
					int iColumnIndex = -1;

					string columnDesc = line.Substring(iPos3 + 1);
					columnDesc = columnDesc.TrimEnd(' ');

					try
					{
						iColumnIndex = Convert.ToInt32(columnIndex);
					}
					catch(Exception)
					{
						MessageBox.Show(UserMessages.CouldNotCastToInteger);
					}
					
					MapperObjectInfo moi = new MapperObjectInfo(propertyName, propertyDesc);
					MapperSourceInfo msi = new MapperSourceInfo(iColumnIndex, columnDesc);
					
					if (false == Globals.htMappingInfo.ContainsKey(moi))
					{
						Globals.htMappingInfo.Add(moi, msi);
					}
					else
					{
						//update
						Globals.htMappingInfo[moi] = msi;
					}
				}
				sr.Close();
			}
		}
		#endregion � FillHashtables �
		#region � bCancel_Click �
		private void bCancel_Click(object sender, System.EventArgs e)
		{
			if (DialogResult.Yes == MessageBox.Show(this, UserMessages.CancelImportText, UserMessages.CancelImport, MessageBoxButtons.YesNo, MessageBoxIcon.Question))
			{
				Globals.importWizardStarted = false;
				//this.Close();
			}
		}
		#endregion � bCancel_Click �
		#region � ImportMask3_Closing �
		private void ImportMask3_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (true == e.Cancel)
			{
				if (DialogResult.Yes == MessageBox.Show(this, UserMessages.CancelImportText, UserMessages.CancelImport, MessageBoxButtons.YesNo, MessageBoxIcon.Question))
				{
					Globals.importWizardStarted = false;
					//this.Close();
				}
			}
		}
		#endregion � ImportMask3_Closing �
		#region lvData_ItemCheck 
		private void lvData_ItemCheck(object sender, System.Windows.Forms.ItemCheckEventArgs e)
		{			
			if (true == this.Checked)
			{
				if (0 == e.Index)
				{
					MessageBox.Show(this, "When input file contains header names, You can not set check state for 1st row!", "Wrong selection!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
					e.NewValue = CheckState.Unchecked;
				}
			}
		}
		#endregion lvData_ItemCheck 
		#region GetStats
		public void GetStats(out ArrayList succ, out ArrayList unsucc)
		{
			succ = new ArrayList();
			unsucc = new ArrayList();

			for (int i = 0; i < this.lvData.Items.Count; i++)
			{
				if (true == this.Checked && 0 == i)
					continue;

				ListViewItem lvi = (ListViewItem)this.lvData.Items[i];
				if (true == lvi.Checked)
					succ.Add(i);
				else
					unsucc.Add(i);
			}			
		}
		#endregion GetStats

        void On_UnTagAll_Click(object sender, EventArgs e)
        {
            SetItemCheckState(false);
        }

        void On_TagAll_Click(object sender, EventArgs e)
        {
            SetItemCheckState(true);
        }

        void SetItemCheckState(bool state)
        {
            for (int i = 0; i < this.lvData.Items.Count; i++)            
                this.lvData.Items[i].Checked = state;
            
        }
		#endregion � Methods �						

	}
}
