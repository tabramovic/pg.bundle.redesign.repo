#region � Using �
using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Reflection;
#endregion � Using �

namespace DealMaker
{
	/// <summary>
	/// Summary description for RetMailMapperForm.
	/// </summary>
	public class RetMailMapperForm : System.Windows.Forms.Form
	{
		#region � Data �
		private MapperObjectInfo moi = null;
		private System.Windows.Forms.Button bOK;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.ListView lvBindingInfo;
		private System.Windows.Forms.ColumnHeader propertyName;
		private System.Windows.Forms.ColumnHeader propertyDesc;
		private System.Windows.Forms.ColumnHeader sourceName;
		private System.Windows.Forms.Button bCancel;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
		#endregion � Data �

		public RetMailMapperForm()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region � Properties �
		#region � MappedObject �
		public MapperObjectInfo MappedObject
		{
			get {return this.moi;}
		}
		#endregion � MappedObject �
		#endregion � Properties �
		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.bOK = new System.Windows.Forms.Button();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.lvBindingInfo = new System.Windows.Forms.ListView();
			this.propertyName = new System.Windows.Forms.ColumnHeader();
			this.propertyDesc = new System.Windows.Forms.ColumnHeader();
			this.sourceName = new System.Windows.Forms.ColumnHeader();
			this.bCancel = new System.Windows.Forms.Button();
			this.groupBox1.SuspendLayout();
			this.SuspendLayout();
			// 
			// bOK
			// 
			this.bOK.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.bOK.Location = new System.Drawing.Point(576, 440);
			this.bOK.Name = "bOK";
			this.bOK.TabIndex = 0;
			this.bOK.Text = "OK";
			this.bOK.Click += new System.EventHandler(this.bOK_Click);
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.lvBindingInfo);
			this.groupBox1.Location = new System.Drawing.Point(16, 16);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(632, 408);
			this.groupBox1.TabIndex = 1;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Profit Grabber Fields";
			// 
			// lvBindingInfo
			// 
			this.lvBindingInfo.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
																							this.propertyName,
																							this.propertyDesc,
																							this.sourceName});
			this.lvBindingInfo.FullRowSelect = true;
			this.lvBindingInfo.GridLines = true;
			this.lvBindingInfo.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
			this.lvBindingInfo.HideSelection = false;
			this.lvBindingInfo.Location = new System.Drawing.Point(16, 24);
			this.lvBindingInfo.MultiSelect = false;
			this.lvBindingInfo.Name = "lvBindingInfo";
			this.lvBindingInfo.Size = new System.Drawing.Size(600, 376);
			this.lvBindingInfo.TabIndex = 0;
			this.lvBindingInfo.View = System.Windows.Forms.View.Details;
			// 
			// propertyName
			// 
			this.propertyName.Text = "Field Name";
			this.propertyName.Width = 152;
			// 
			// propertyDesc
			// 
			this.propertyDesc.Text = "Field Description";
			this.propertyDesc.Width = 206;
			// 
			// sourceName
			// 
			this.sourceName.Text = "Import Field Assigned To It";
			this.sourceName.Width = 238;
			// 
			// bCancel
			// 
			this.bCancel.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.bCancel.Location = new System.Drawing.Point(488, 440);
			this.bCancel.Name = "bCancel";
			this.bCancel.TabIndex = 2;
			this.bCancel.Text = "Cancel";
			this.bCancel.Click += new System.EventHandler(this.bCancel_Click);
			// 
			// RetMailMapperForm
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(210)), ((System.Byte)(216)), ((System.Byte)(240)));
			this.ClientSize = new System.Drawing.Size(674, 476);
			this.Controls.Add(this.bCancel);
			this.Controls.Add(this.groupBox1);
			this.Controls.Add(this.bOK);
			this.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
			this.Name = "RetMailMapperForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Map / Assign To";
			this.Load += new System.EventHandler(this.MapperForm1_Load);
			this.groupBox1.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion
		#region � Methods �
		#region � bOK_Click �
		private void bOK_Click(object sender, System.EventArgs e)
		{
			IDictionaryEnumerator ide = Globals.htMappingInfo.GetEnumerator();
			System.Windows.Forms.ListView.SelectedListViewItemCollection lvc = this.lvBindingInfo.SelectedItems;
			if (null != lvc && 0 != lvc.Count )
			{
				ListViewItem lvi = (ListViewItem)lvc[0];
				string key = lvi.SubItems[0].Text;

				while (ide.MoveNext())
				{
					MapperObjectInfo moi = (MapperObjectInfo)ide.Key;
					if (moi.PropertyName == key)
					{
						this.moi = (MapperObjectInfo)ide.Key;
						break;
					}
				}
			}
			this.Close();
		}
		#endregion � bOK_Click �
		#region � MapperForm1_Load �
		private void MapperForm1_Load(object sender, System.EventArgs e)
		{			
			Hashtable htItemPosition = this.ScanForPositions();
			SortedList alListViewItems = new SortedList(new ListViewItemTagComparer());
			int columnNumber = this.lvBindingInfo.Columns.Count;
			double dColumnWidht = this.lvBindingInfo.Width / columnNumber;
			int columnWidth = (int)dColumnWidht;
			foreach (ColumnHeader ch in this.lvBindingInfo.Columns)
			{
				ch.Width = columnWidth;
			}

			this.lvBindingInfo.Items.Clear();
			IDictionaryEnumerator ide = Globals.htMappingInfo.GetEnumerator();			
			
			int iPos = 0;
			while (ide.MoveNext())
			{		
				iPos = (int)Globals.htRetMailWizardItemPosition[((MapperObjectInfo)ide.Key).PropertyName];
				MapperObjectInfo moi = (MapperObjectInfo)ide.Key;
				MapperSourceInfo msi = (MapperSourceInfo)ide.Value;				

				ListViewItem lvi = null;
				if (-1 == msi.ColumnIndex)
				{
					lvi = new ListViewItem(new string[] {moi.PropertyName, moi.PropertyDesc, /*string.Empty,*/ string.Empty});					
				}
				else
				{
					lvi = new ListViewItem(new string[] {moi.PropertyName, moi.PropertyDesc, /*msi.ColumnIndex.ToString(),*/ msi.ColumnDesc});
				}
				lvi.Tag = iPos;
				alListViewItems.Add((int)lvi.Tag, lvi);												
			}	
			foreach (ListViewItem lviTemp in alListViewItems.Values)
				this.lvBindingInfo.Items.Insert(this.lvBindingInfo.Items.Count, lviTemp);		
		}
		#endregion � MapperForm1_Load �

		private Hashtable ScanForPositions()
		{
			Hashtable htItemPositions = new Hashtable();
			ImportIntermedeateObject iio = new ImportIntermedeateObject();		
			PropertyInfo[] pi =  iio.GetType().GetProperties();			
			for (int i = 0; i < pi.Length; i++)
			{
				object[] descAttr = pi[i].GetCustomAttributes(typeof(DescriptionAttribute), true);
				object[] posAttr = pi[i].GetCustomAttributes(typeof(ItemPositionAttribute), true);
				if (0 != descAttr.Length && 0 != posAttr.Length)
				{      
					DescriptionAttribute da = (DescriptionAttribute)descAttr[0];
					ItemPositionAttribute ita = (ItemPositionAttribute)posAttr[0];

					if (false == htItemPositions.Contains(da))
					{
						int iPos = 1;
						try
						{
							iPos = Convert.ToInt16(ita.Desc);
						}
						catch (Exception)
						{
							iPos = 1;
						}
						htItemPositions.Add(da.Desc, iPos);
					}
				}
			}
			return htItemPositions;
		}
		#endregion � Methods �

		private void bCancel_Click(object sender, System.EventArgs e)
		{
			this.Close();
		}
	}
}
