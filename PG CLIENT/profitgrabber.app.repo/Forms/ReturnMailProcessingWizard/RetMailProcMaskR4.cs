#region � Using �
using System;
using System.IO;
using System.Threading;
using System.Diagnostics;
using System.Drawing;
using System.Data;
using System.Reflection;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data.SqlClient; 
using System.Data.SqlTypes;
using NHibernate;
using NHibernate.Cfg;

//ADDED
using DealMaker.PlainClasses.ReturnMailWizard;
#endregion � Using �

namespace DealMaker
{
	/// <summary>
	/// Summary description for RetMailProcMaskR4.
	/// </summary>
	public class RetMailProcMaskR4 : System.Windows.Forms.UserControl
	{
		#region � Data �
		private int succNode = -1;
		private int unSuccNode = -1;
		private ArrayList alSuccessfuly = new ArrayList();
		private ArrayList alUnSuccessfuly = new ArrayList();
		//private bool bSucGroup = false;
		//private bool bUnSucGroup = false;
		private TreeNode selectedNode	= null;
		public ArrayList alImportIntermediatedObjects = null;
		public ArrayList alFilteredImportIntermediateObjects = null;
		private Delimiter inputDelimiter;
		private string selectedProfile = string.Empty;
		private string selectedFile;
		private bool bChecked;
		private bool bCreateNewProfile = false;
		private System.Windows.Forms.Label label1;
		private DealMaker.CustomizedTreeViewUserControl customizedTreeViewUserControl1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox tbExistingEntries;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.CheckBox cbExcludeDuplicates;
		private System.Windows.Forms.Button bBack;
		private System.Windows.Forms.Button bCancel;
		private System.Windows.Forms.Button bImport;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Button bSaveMap;
		private System.Windows.Forms.TextBox tbSuccessful;
		private System.Windows.Forms.Button bSelectSuccessfulGroup;
		private System.Windows.Forms.Button bSelectUnSuccessfulGroup;
		private System.Windows.Forms.TextBox tbUnsuccessful;
		private System.Windows.Forms.TextBox tbUnSuccCnt;
		private System.Windows.Forms.TextBox tbSuccCnt;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label7;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
		#endregion � Data �
		#region � Properties �
		#region � InputDelimiter �
		public Delimiter InputDelimiter
		{
			get {return this.inputDelimiter;}
			set {this.inputDelimiter = value;}
		}
		#endregion � InputDelimiter �
		#region � SelectedFile �
		public string SelectedFile
		{
			get {return this.selectedFile;}
			set {this.selectedFile = value;}
		}
		#endregion � SelectedFile �
		#region � Checked �
		public bool Checked
		{
			get {return this.bChecked;}
			set {this.bChecked = value;}
		}
		#endregion � Checked �
		#region � CreateNewProfile �
		public bool CreateNewProfile
		{
			get {return this.bCreateNewProfile;}
			set {this.bCreateNewProfile = value;}
		}
		#endregion � CreateNewProfile �
		#region � SelectedProfile �
		public string SelectedProfile
		{
			get {return this.selectedProfile; }
			set {this.selectedProfile = value;}
		}		
		#endregion � SelectedProfile �
		#endregion � Properties �	
		public RetMailProcMaskR4()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

			this.customizedTreeViewUserControl1.tv_Nodes.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tv_Nodes_KeyDown);
			this.customizedTreeViewUserControl1.tv_Nodes.MouseDown += new System.Windows.Forms.MouseEventHandler(this.tv_Nodes_MouseDown);
			this.customizedTreeViewUserControl1.tv_Nodes.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.tv_Nodes_AfterSelect);									

			CustomizedTreeViewUserControl.REFRESH_GROUPS += new DealMaker.CustomizedTreeViewUserControl.RefreshHandler(RefreshGroups);
		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}


		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.customizedTreeViewUserControl1 = new DealMaker.CustomizedTreeViewUserControl();
			this.label2 = new System.Windows.Forms.Label();
			this.tbExistingEntries = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.cbExcludeDuplicates = new System.Windows.Forms.CheckBox();
			this.bBack = new System.Windows.Forms.Button();
			this.bCancel = new System.Windows.Forms.Button();
			this.bImport = new System.Windows.Forms.Button();
			this.tbUnSuccCnt = new System.Windows.Forms.TextBox();
			this.tbSuccCnt = new System.Windows.Forms.TextBox();
			this.label5 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.bSaveMap = new System.Windows.Forms.Button();
			this.tbSuccessful = new System.Windows.Forms.TextBox();
			this.tbUnsuccessful = new System.Windows.Forms.TextBox();
			this.bSelectSuccessfulGroup = new System.Windows.Forms.Button();
			this.bSelectUnSuccessfulGroup = new System.Windows.Forms.Button();
			this.label6 = new System.Windows.Forms.Label();
			this.label7 = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label1.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.label1.Location = new System.Drawing.Point(8, 176);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(152, 23);
			this.label1.TabIndex = 0;
			this.label1.Text = "Import File Destination";
			this.label1.Visible = false;
			// 
			// customizedTreeViewUserControl1
			// 
			this.customizedTreeViewUserControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left)));
			this.customizedTreeViewUserControl1.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(210)), ((System.Byte)(216)), ((System.Byte)(240)));
			this.customizedTreeViewUserControl1.Location = new System.Drawing.Point(8, 208);
			this.customizedTreeViewUserControl1.Name = "customizedTreeViewUserControl1";
			this.customizedTreeViewUserControl1.SelectedTreeNode = null;
			this.customizedTreeViewUserControl1.Size = new System.Drawing.Size(296, 125);
			this.customizedTreeViewUserControl1.TabIndex = 1;
			this.customizedTreeViewUserControl1.UpdateGlobals = false;
			this.customizedTreeViewUserControl1.Visible = false;
			// 
			// label2
			// 
			this.label2.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.label2.Location = new System.Drawing.Point(320, 160);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(216, 40);
			this.label2.TabIndex = 2;
			this.label2.Text = "TOTAL number of Records already EXISTING in the selected group:";
			this.label2.Visible = false;
			// 
			// tbExistingEntries
			// 
			this.tbExistingEntries.Location = new System.Drawing.Point(168, 176);
			this.tbExistingEntries.Name = "tbExistingEntries";
			this.tbExistingEntries.ReadOnly = true;
			this.tbExistingEntries.TabIndex = 3;
			this.tbExistingEntries.Text = "";
			this.tbExistingEntries.Visible = false;
			// 
			// label3
			// 
			this.label3.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.label3.Location = new System.Drawing.Point(320, 200);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(232, 56);
			this.label3.TabIndex = 4;
			this.label3.Text = "If there are existing records in Destination Group, the imported records will be " +
				"added to those records. If you don�t want that, create a new group.";
			this.label3.Visible = false;
			// 
			// cbExcludeDuplicates
			// 
			this.cbExcludeDuplicates.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.cbExcludeDuplicates.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.cbExcludeDuplicates.Location = new System.Drawing.Point(16, 344);
			this.cbExcludeDuplicates.Name = "cbExcludeDuplicates";
			this.cbExcludeDuplicates.Size = new System.Drawing.Size(296, 32);
			this.cbExcludeDuplicates.TabIndex = 5;
			this.cbExcludeDuplicates.Text = "Check this box if you want to exclude duplicates during import";
			this.cbExcludeDuplicates.Visible = false;
			// 
			// bBack
			// 
			this.bBack.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.bBack.Location = new System.Drawing.Point(320, 336);
			this.bBack.Name = "bBack";
			this.bBack.Size = new System.Drawing.Size(32, 23);
			this.bBack.TabIndex = 6;
			this.bBack.Text = "Previous";
			this.bBack.Visible = false;
			this.bBack.Click += new System.EventHandler(this.bBack_Click);
			// 
			// bCancel
			// 
			this.bCancel.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.bCancel.Location = new System.Drawing.Point(360, 336);
			this.bCancel.Name = "bCancel";
			this.bCancel.Size = new System.Drawing.Size(32, 23);
			this.bCancel.TabIndex = 7;
			this.bCancel.Text = "Cancel";
			this.bCancel.Visible = false;
			this.bCancel.Click += new System.EventHandler(this.bCancel_Click);
			// 
			// bImport
			// 
			this.bImport.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.bImport.Location = new System.Drawing.Point(485, 336);
			this.bImport.Name = "bImport";
			this.bImport.TabIndex = 8;
			this.bImport.Text = "Import";
			this.bImport.Click += new System.EventHandler(this.bImport_Click);
			// 
			// tbUnSuccCnt
			// 
			this.tbUnSuccCnt.Location = new System.Drawing.Point(336, 48);
			this.tbUnSuccCnt.Name = "tbUnSuccCnt";
			this.tbUnSuccCnt.ReadOnly = true;
			this.tbUnSuccCnt.Size = new System.Drawing.Size(75, 20);
			this.tbUnSuccCnt.TabIndex = 16;
			this.tbUnSuccCnt.TabStop = false;
			this.tbUnSuccCnt.Text = "";
			// 
			// tbSuccCnt
			// 
			this.tbSuccCnt.Location = new System.Drawing.Point(336, 16);
			this.tbSuccCnt.Name = "tbSuccCnt";
			this.tbSuccCnt.ReadOnly = true;
			this.tbSuccCnt.Size = new System.Drawing.Size(75, 20);
			this.tbSuccCnt.TabIndex = 15;
			this.tbSuccCnt.TabStop = false;
			this.tbSuccCnt.Text = "";
			// 
			// label5
			// 
			this.label5.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.label5.Location = new System.Drawing.Point(16, 48);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(312, 24);
			this.label5.TabIndex = 14;
			this.label5.Text = "TOTAL number of Records UNSUCCESSFULLY skiptraced:";
			// 
			// label4
			// 
			this.label4.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.label4.Location = new System.Drawing.Point(16, 16);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(296, 24);
			this.label4.TabIndex = 13;
			this.label4.Text = "TOTAL number of Records SUCCESSFULLY skiptraced:";
			// 
			// bSaveMap
			// 
			this.bSaveMap.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.bSaveMap.Location = new System.Drawing.Point(400, 336);
			this.bSaveMap.Name = "bSaveMap";
			this.bSaveMap.TabIndex = 17;
			this.bSaveMap.Text = "Save Map";
			this.bSaveMap.Click += new System.EventHandler(this.bSaveMap_Click);
			// 
			// tbSuccessful
			// 
			this.tbSuccessful.Location = new System.Drawing.Point(320, 264);
			this.tbSuccessful.Name = "tbSuccessful";
			this.tbSuccessful.ReadOnly = true;
			this.tbSuccessful.Size = new System.Drawing.Size(200, 20);
			this.tbSuccessful.TabIndex = 18;
			this.tbSuccessful.Text = "Select Successful Group";
			this.tbSuccessful.Visible = false;
			// 
			// tbUnsuccessful
			// 
			this.tbUnsuccessful.Location = new System.Drawing.Point(320, 296);
			this.tbUnsuccessful.Name = "tbUnsuccessful";
			this.tbUnsuccessful.ReadOnly = true;
			this.tbUnsuccessful.Size = new System.Drawing.Size(200, 20);
			this.tbUnsuccessful.TabIndex = 19;
			this.tbUnsuccessful.Text = "Select UnSuccessful Group";
			this.tbUnsuccessful.Visible = false;
			// 
			// bSelectSuccessfulGroup
			// 
			this.bSelectSuccessfulGroup.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.bSelectSuccessfulGroup.Location = new System.Drawing.Point(528, 264);
			this.bSelectSuccessfulGroup.Name = "bSelectSuccessfulGroup";
			this.bSelectSuccessfulGroup.Size = new System.Drawing.Size(32, 23);
			this.bSelectSuccessfulGroup.TabIndex = 20;
			this.bSelectSuccessfulGroup.Text = "...";
			this.bSelectSuccessfulGroup.Visible = false;
			this.bSelectSuccessfulGroup.Click += new System.EventHandler(this.bSelectSuccessfulGroup_Click);
			// 
			// bSelectUnSuccessfulGroup
			// 
			this.bSelectUnSuccessfulGroup.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.bSelectUnSuccessfulGroup.Location = new System.Drawing.Point(528, 296);
			this.bSelectUnSuccessfulGroup.Name = "bSelectUnSuccessfulGroup";
			this.bSelectUnSuccessfulGroup.Size = new System.Drawing.Size(32, 23);
			this.bSelectUnSuccessfulGroup.TabIndex = 21;
			this.bSelectUnSuccessfulGroup.Text = "...";
			this.bSelectUnSuccessfulGroup.Visible = false;
			this.bSelectUnSuccessfulGroup.Click += new System.EventHandler(this.bSelectUnSuccessfulGroup_Click);
			// 
			// label6
			// 
			this.label6.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.label6.Location = new System.Drawing.Point(16, 80);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(456, 24);
			this.label6.TabIndex = 22;
			this.label6.Text = "ProfitGrabber Pro will place successfully skiptraced records into the new subgrou" +
				"p called ";
			// 
			// label7
			// 
			this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label7.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.label7.Location = new System.Drawing.Point(32, 104);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(128, 24);
			this.label7.TabIndex = 23;
			this.label7.Text = "SkipTracedRecords.";
			// 
			// RetMailProcMaskR4
			// 
			this.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(210)), ((System.Byte)(216)), ((System.Byte)(240)));
			this.Controls.Add(this.label7);
			this.Controls.Add(this.label6);
			this.Controls.Add(this.bSelectUnSuccessfulGroup);
			this.Controls.Add(this.bSelectSuccessfulGroup);
			this.Controls.Add(this.tbUnsuccessful);
			this.Controls.Add(this.tbSuccessful);
			this.Controls.Add(this.bSaveMap);
			this.Controls.Add(this.tbUnSuccCnt);
			this.Controls.Add(this.tbSuccCnt);
			this.Controls.Add(this.tbExistingEntries);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.bImport);
			this.Controls.Add(this.bCancel);
			this.Controls.Add(this.bBack);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.customizedTreeViewUserControl1);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.cbExcludeDuplicates);
			this.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(64)), ((System.Byte)(64)), ((System.Byte)(64)));
			this.Name = "RetMailProcMaskR4";
			this.Size = new System.Drawing.Size(576, 384);
			this.Load += new System.EventHandler(this.ImportMask6_Load);
			this.ResumeLayout(false);

		}
		#endregion

		
		public ArrayList SuccCount
		{
			get {return this.alSuccessfuly;}
			set 
			{
				this.alSuccessfuly = value;
				this.tbSuccCnt.Text = this.alSuccessfuly.Count.ToString();
			}
		}
		
		public ArrayList UnSuccCount
		{
			get {return this.alSuccessfuly;}
			set 
			{
				this.alUnSuccessfuly = value;
				this.tbUnSuccCnt.Text = this.alUnSuccessfuly.Count.ToString();
			}
		}
		
		
		
		private void ImportMask6_Load(object sender, System.EventArgs e)
		{						
			this.alImportIntermediatedObjects = this.ImportData();												
		}
								
		private void bBack_Click(object sender, System.EventArgs e)
		{
			Globals.importWindowPos = this.Location;			
			ImportMask5 im5 = new ImportMask5();
			im5.InputDelimiter = this.InputDelimiter;
			im5.SelectedFile = this.SelectedFile;
			im5.SelectedProfile = this.SelectedProfile;
			im5.CreateNewProfile = this.CreateNewProfile;
			im5.Checked = this.Checked;
			im5.Show();	
		}
				
		private void bCancel_Click(object sender, System.EventArgs e)
		{
			if (DialogResult.Yes == MessageBox.Show(this, UserMessages.CancelImportText, UserMessages.CancelImport, MessageBoxButtons.YesNo, MessageBoxIcon.Question))
			{
				Globals.importWizardStarted = false;				
			}
		}
		
		private bool UpdateSuccessfulySkipTracedContact(RMW_MapperObject rmwmo)
		{
			PropertyItem pi = null;
			try
			{
				DealMaker.Address mAddress = null;
				DealMaker.Address sAddress = null;				
				pi = PropertyItem.LoadPropertyItem(new Guid(rmwmo.MRWContactId));				
				if (null != pi)
				{
					if (null == pi.Owner)
					{
						pi.Owner = new Contact();
						pi.Owner.save = true;

						pi.Owner.MailAddress = new Address();
						pi.Owner.MailAddress.save = true;

						//update first address only - didn�t have anythig before
						pi.Owner.MailAddress.FullSiteStreetAddress		= rmwmo.MRWLNSAddress;
						pi.Owner.MailAddress.SiteStreetUnitNumber		= rmwmo.MRWLNSUnitNumber;
						pi.Owner.MailAddress.SiteStreetName				= rmwmo.MRWLNSName;
						pi.Owner.MailAddress.SiteStreetNumber			= rmwmo.MRWLNSNumber;
						pi.Owner.MailAddress.SiteStreetPostDirectional	= rmwmo.MRWLNSPostDir;
						pi.Owner.MailAddress.SiteStreetPreDirectional	= rmwmo.MRWLNSPreDir;
						pi.Owner.MailAddress.SiteStreetSuffix			= rmwmo.MRWLNSSuffix;
						pi.Owner.MailAddress.SiteCity					= rmwmo.MRWLNCity;
						pi.Owner.MailAddress.SiteState					= rmwmo.MRWLNState;
						pi.Owner.MailAddress.SiteZIP					= rmwmo.MRWLNZip;
						pi.Owner.MailAddress.SiteCitySiteState			= string.Empty;
						pi.Owner.MailAddress.SiteCitySiteStateSiteZIP	= string.Empty;
						
						pi.Owner.HomePhone = new Phone(rmwmo.MRWSTPhoneNr1, string.Empty);
						pi.Owner.HomePhone.save = true;

						pi.Owner.WorkPhone = new Phone(rmwmo.MRWSTPhoneNr2, string.Empty);
						pi.Owner.WorkPhone.save = true;

						return true;	//If OK
					}
					else
					{
						pi.Owner.save = false;						

						//Phone 1
						if (null == pi.Owner.HomePhone)
						{
							pi.Owner.HomePhone = new Phone();
							pi.Owner.HomePhone.save = true;
						}
						else
						{
							pi.Owner.HomePhone.save = false;
						}
						pi.Owner.HomePhone.PhoneNr = rmwmo.MRWSTPhoneNr1;						

						//Phone 2
						if (null == pi.Owner.WorkPhone)
						{
							pi.Owner.WorkPhone = new Phone();
							pi.Owner.WorkPhone.save = true;
						}
						else
						{
							pi.Owner.WorkPhone.save = false;
						}
						pi.Owner.WorkPhone.PhoneNr = rmwmo.MRWSTPhoneNr2;						
						

						if (null == pi.Owner.MailAddress)
						{
							pi.Owner.MailAddress = new Address();
							pi.Owner.MailAddress.save = true;
						}
						else
						{
							pi.Owner.MailAddress.save = false;
							mAddress = new Address(pi.Owner.MailAddress);
						}

						pi.Owner.MailAddress.FullSiteStreetAddress		= rmwmo.MRWLNSAddress;
						pi.Owner.MailAddress.SiteStreetUnitNumber		= rmwmo.MRWLNSUnitNumber;
						pi.Owner.MailAddress.SiteStreetName				= rmwmo.MRWLNSName;
						pi.Owner.MailAddress.SiteStreetNumber			= rmwmo.MRWLNSNumber;
						pi.Owner.MailAddress.SiteStreetPostDirectional	= rmwmo.MRWLNSPostDir;
						pi.Owner.MailAddress.SiteStreetPreDirectional	= rmwmo.MRWLNSPreDir;
						pi.Owner.MailAddress.SiteStreetSuffix			= rmwmo.MRWLNSSuffix;					
						pi.Owner.MailAddress.SiteCity					= rmwmo.MRWLNCity;
						pi.Owner.MailAddress.SiteState					= rmwmo.MRWLNState;
						pi.Owner.MailAddress.SiteZIP					= rmwmo.MRWLNZip;
						pi.Owner.MailAddress.SiteCitySiteState			= string.Empty;	
						pi.Owner.MailAddress.SiteCitySiteStateSiteZIP	= string.Empty;
						
						if (null != mAddress)
						{
							//update second address
							if (null == pi.Owner.SecondAddress)
							{
								pi.Owner.SecondAddress = new Address();
								pi.Owner.SecondAddress.save = true;
							}
							else
							{
								pi.Owner.SecondAddress.save = false;								
								sAddress = new Address(pi.Owner.SecondAddress);
							}
							pi.Owner.SecondAddress.FullSiteStreetAddress		= mAddress.FullSiteStreetAddress;
							pi.Owner.SecondAddress.SiteStreetUnitNumber			= mAddress.SiteStreetUnitNumber;
							pi.Owner.SecondAddress.SiteStreetName				= mAddress.SiteStreetName;
							pi.Owner.SecondAddress.SiteStreetNumber				= mAddress.SiteStreetNumber;
							pi.Owner.SecondAddress.SiteStreetPostDirectional	= mAddress.SiteStreetPostDirectional;
							pi.Owner.SecondAddress.SiteStreetPreDirectional		= mAddress.SiteStreetPreDirectional;
							pi.Owner.SecondAddress.SiteStreetSuffix				= mAddress.SiteStreetSuffix;					
							pi.Owner.SecondAddress.SiteCity						= mAddress.SiteCity;
							pi.Owner.SecondAddress.SiteState					= mAddress.SiteState;
							pi.Owner.SecondAddress.SiteZIP						= mAddress.SiteZIP;
							pi.Owner.SecondAddress.SiteCitySiteState			= mAddress.SiteCitySiteState;
							pi.Owner.SecondAddress.SiteCitySiteStateSiteZIP		= mAddress.SiteCitySiteStateSiteZIP;

						}						
						if (null != sAddress)
						{
							//update third address
							if (null != pi.Owner.ThirdAddress)
							{
								pi.Owner.ThirdAddress = new Address();
								pi.Owner.ThirdAddress.save = true;
							}
							else
							{
								pi.Owner.ThirdAddress.save = false;
							}
							pi.Owner.ThirdAddress.FullSiteStreetAddress			= sAddress.FullSiteStreetAddress;
							pi.Owner.ThirdAddress.SiteStreetUnitNumber			= sAddress.SiteStreetUnitNumber;
							pi.Owner.ThirdAddress.SiteStreetName				= sAddress.SiteStreetName;
							pi.Owner.ThirdAddress.SiteStreetNumber				= sAddress.SiteStreetNumber;
							pi.Owner.ThirdAddress.SiteStreetPostDirectional		= sAddress.SiteStreetPostDirectional;
							pi.Owner.ThirdAddress.SiteStreetPreDirectional		= sAddress.SiteStreetPreDirectional;
							pi.Owner.ThirdAddress.SiteStreetSuffix				= sAddress.SiteStreetSuffix;					
							pi.Owner.ThirdAddress.SiteCity						= sAddress.SiteCity;
							pi.Owner.ThirdAddress.SiteState						= sAddress.SiteState;
							pi.Owner.ThirdAddress.SiteZIP						= sAddress.SiteZIP;
							pi.Owner.ThirdAddress.SiteCitySiteState				= sAddress.SiteCitySiteState;	
							pi.Owner.ThirdAddress.SiteCitySiteStateSiteZIP		= sAddress.SiteCitySiteStateSiteZIP;

						}						
					}			
				}
			}
			catch
			{
				return false;
			}
			PropertyItem.SavePropertyItem(pi);
			return true;
		}
		
		private void bImport_Click(object sender, System.EventArgs e)
		{						
			ArrayList alSuccPropertyItemIds = new ArrayList();
			ArrayList alUnSuccPropertyItemIds = new ArrayList();

			for (int i = 0; i < alSuccessfuly.Count; i++)
			{				
				try
				{
					int iPos = (int)alSuccessfuly[i];
					if (true == this.Checked)
						iPos--;
					RMW_MapperObject rmwmo = (RMW_MapperObject)alImportIntermediatedObjects[iPos];								
					this.UpdateSuccessfulySkipTracedContact(rmwmo);
					alSuccPropertyItemIds.Add(new Guid(rmwmo.MRWContactId));												
				}
				catch (Exception
#if DEBUG
					exc
#endif
					)
				{
#if DEBUG
					MessageBox.Show("Succ PropertyItem, pos:" + i.ToString() + "\n\n" + exc.Message + "\n" + exc.InnerException);
#endif
				}
			}
			for (int i = 0; i < alUnSuccessfuly.Count; i++)
			{
				try
				{
					int iPos = (int)alUnSuccessfuly[i];
					if (true == this.Checked)
						iPos--;
					RMW_MapperObject rmwmo = (RMW_MapperObject)alImportIntermediatedObjects[iPos];
					alUnSuccPropertyItemIds.Add(new Guid(rmwmo.MRWContactId));
				}
				catch (Exception
#if DEBUG
					exc
#endif
					)
				{
#if DEBUG
					MessageBox.Show("Unsucc PropertyItem, pos:" + i.ToString() + "\n\n" + exc.Message + "\n" + exc.InnerException);
#endif
				}
			}
						
			SkipTracedContactAssistant.Instance.ProcessReImportedRecords(alSuccPropertyItemIds);
			MessageBox.Show(this, "Skiptraced records were imported and updated.", "Finished", MessageBoxButtons.OK, MessageBoxIcon.Information);
		}
		
		#region � ImportData �
		public ArrayList ImportData ()
		{			
			ArrayList alImportIntermediatedObjects = new ArrayList();
			ArrayList alErrors = new ArrayList();
			using (StreamReader sr = new StreamReader(this.SelectedFile)) 
			{
				RMW_MapperObject rmwmo = null;
				char delimiter = ApplicationHelper.GetDelimiter(this.InputDelimiter);
				string line = string.Empty;
				int iLineCnt = 0;
				bool filters = false;
				while ((line = sr.ReadLine()) != null) 
				{
					rmwmo = new RMW_MapperObject();					
					int iRes = this.alSuccessfuly.BinarySearch(iLineCnt);
					if (0 <= iRes)
						rmwmo.succTraced = true;					
						
					if (0 == iLineCnt && true == this.Checked)
					{
						iLineCnt++;
						continue;	//first row contains header names, no real data yet!
					}
										

					try
					{
						if (string.Empty != line && false == filters)
						{
							Hashtable htDelimiterPositions = this.GetDelimiterPositions(line, delimiter);						
							IDictionaryEnumerator ide = Globals.htMappingInfo.GetEnumerator();
							while (ide.MoveNext())
							{
								MapperSourceInfo msi = (MapperSourceInfo)ide.Value;
								if (false == msi.IsInitialized())
								{
									continue;
								}
								else//handle it
								{								
									string valueFromFile = string.Empty;
									int columnIndex = msi.ColumnIndex;
									MapperObjectInfo moi = (MapperObjectInfo)ide.Key;
									string propertyName = moi.PropertyName;																

									//find value
									if (0 == columnIndex)
									{
										int endDelimiterPosition = (int)htDelimiterPositions[columnIndex];
										valueFromFile = line.Substring(0, endDelimiterPosition);									
									}
									else
									{
										/*search from columnIndex-th delimiter until you either:
										 *															find  nextOne or
										 *															reach end of line
										 */
										int endDelimiterPosition = (int)htDelimiterPositions[columnIndex];
										int prevDelimiterPosition = (int)htDelimiterPositions[columnIndex - 1];
																		
										valueFromFile = line.Substring(prevDelimiterPosition + 1, endDelimiterPosition - prevDelimiterPosition - 1);									
									}
																															
									switch (propertyName)										
									{
										case Globals.MRWContactId:
											rmwmo.MRWContactId = valueFromFile;
											break;
										case Globals.MRWLNSAddress:
											rmwmo.MRWLNSAddress = valueFromFile;
											break;
										case Globals.MRWLNSName:
											rmwmo.MRWLNSName = valueFromFile;
											break;
										case Globals.MRWLNSNumber:
											rmwmo.MRWLNSNumber = valueFromFile;
											break;
										case Globals.MRWLNSPostDir:
											rmwmo.MRWLNSPostDir = valueFromFile;
											break;
										case Globals.MRWLNSPreDir:
											rmwmo.MRWLNSPreDir = valueFromFile;
											break;
										case Globals.MRWLNSSuffix:
											rmwmo.MRWLNSSuffix = valueFromFile;
											break;
										case Globals.MRWLNSUnitNumber:
											rmwmo.MRWLNSUnitNumber = valueFromFile;
											break;
										case Globals.MRWLNCity:
											rmwmo.MRWLNCity = valueFromFile;
											break;
										case Globals.MRWLNState:
											rmwmo.MRWLNState = valueFromFile;
											break;
										case Globals.MRWLNZip:
											rmwmo.MRWLNZip = valueFromFile;
											break;
										case Globals.MRWSTPhoneNr1:
											rmwmo.MRWSTPhoneNr1 = valueFromFile;
											break;
										case Globals.MRWSTPhoneNr2:
											rmwmo.MRWSTPhoneNr2 = valueFromFile;
											break;
										default:
											break;
									}																											
								}
							}						
							alImportIntermediatedObjects.Add(rmwmo);
						}
					}
					catch (Exception 
#if DEBUG
						exc
#endif
						)
					{
#if DEBUG
						MessageBox.Show("Import File @" + iLineCnt.ToString() + "\n\n" + exc.Message + exc.InnerException);
#endif
					}
					finally
					{						
						iLineCnt++;
					}
				}
			}
			if (alErrors.Count > 0)
			{
				this.SaveErrorLog(alErrors);
			}
			return alImportIntermediatedObjects;			
		}
		#endregion � ImportData �
		#region � SaveErrorLog �
		private void SaveErrorLog(ArrayList alErrors)
		{
			string errorLogName = this.SelectedFile.Substring(0, this.SelectedFile.Length - 4);
			errorLogName += "_ERROR_LOG.txt";
			FileInfo fi = new FileInfo(errorLogName);						
			StreamWriter sw = fi.CreateText();
			for (int i = 0; i < alErrors.Count; i++)
			{
				sw.WriteLine((string)alErrors[i]);
			}
			sw.Flush();
			sw.Close();
			if (DialogResult.Yes == MessageBox.Show(this, UserMessages.ProblemsWithImportFileDesc, UserMessages.ProblemsWithImportFile, MessageBoxButtons.YesNo, MessageBoxIcon.Information))
			{
				Process.Start("notepad.exe", errorLogName);
			}			
		}
		#endregion � SaveErrorLog �
		#region � GetDelimiterPositions �		
		private Hashtable GetDelimiterPositions(string line, char delimiter)
		{
			Hashtable	htDelimiterPositions = new Hashtable();
			if (delimiter != ',')
			{
				int startIndex = -1;
				int pos = 0;
			
				while( true )
				{
		
					//search from the beginning of line until you reach first delimiter
					startIndex = line.IndexOf(delimiter, startIndex + 1, 	line.Length - startIndex - 1 );

					// Exit the loop if the target is not found.
					if( startIndex < 0 )
					{
						break;
					}
					else
					{
						htDelimiterPositions.Add(pos, startIndex);
					}
					pos++;
				}
				htDelimiterPositions.Add(pos++, line.Length);	//last entry!
				return htDelimiterPositions;
			}
			else
			{
				//take care of commas as value !!!
				ArrayList result = new ArrayList();
				this.ParseCSVFields(result, line, htDelimiterPositions);
				return htDelimiterPositions;
			}
		}
		#endregion � GetDelimiterPositions �				
		#region � ParseCSVFields �
		// Parses the CSV fields and pushes the fields into the result arraylist
		private void ParseCSVFields(ArrayList result, string data, Hashtable htDelimiterPositions) 
		{

			int pos = -1;
			int columnNr = 0;
			while (pos < data.Length)
			{
				result.Add(ParseCSVField(data, ref pos));
				htDelimiterPositions.Add(columnNr, pos);
				columnNr++;				
			}
		}
		#endregion � ParseCSVFields �
		#region � ParseCSVField �
		// Parses the field at the given position of the data, modified pos to match
		// the first unparsed position and returns the parsed field
		private string ParseCSVField(string data, ref int startSeparatorPosition) 
		{

			if (startSeparatorPosition == data.Length-1) 
			{
				startSeparatorPosition++;
				// The last field is empty
				return "";
			}

			int fromPos = startSeparatorPosition + 1;

			// Determine if this is a quoted field
			if (data[fromPos] == '"') 
			{
				// If we're at the end of the string, let's consider this a field that
				// only contains the quote
				if (fromPos == data.Length-1) 
				{
					fromPos++;
					return "\"";
				}

				// Otherwise, return a string of appropriate length with double quotes collapsed
				// Note that FSQ returns data.Length if no single quote was found
				int nextSingleQuote = FindSingleQuote(data, fromPos+1);
				startSeparatorPosition = nextSingleQuote+1;				
				string retVal = data.Substring(fromPos+1, nextSingleQuote-fromPos-1).Replace("\"\"", "\"");
				return retVal;
			}

			// The field ends in the next comma or EOL
			int nextComma = data.IndexOf(',', fromPos);
			if (nextComma == -1) 
			{
				startSeparatorPosition = data.Length;
				string retVal = data.Substring(fromPos);
				return retVal;
			}
			else 
			{
				startSeparatorPosition = nextComma;				
				string retVal = data.Substring(fromPos, nextComma-fromPos);
				return retVal;
			}
		}
		#endregion � ParseCSVField �
		#region � FindSingleQuote �
		// Returns the index of the next single quote mark in the string 
		// (starting from startFrom)
		private int FindSingleQuote(string data, int startFrom) 
		{

			int i = startFrom-1;
			while (++i < data.Length)
				if (data[i] == '"') 
				{
					// If this is a double quote, bypass the chars
					if (i < data.Length-1 && data[i+1] == '"') 
					{
						i++;
						continue;
					}
					else
						return i;
				}
			// If no quote found, return the end value of i (data.Length)
			return i;
		}
		#endregion � FindSingleQuote �
		#region � tv_Nodes_KeyDown �
		private void tv_Nodes_KeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
		{
			if (null != ((TreeView)sender).SelectedNode)
			{
				this.selectedNode = ((TreeView)sender).SelectedNode;
			}
		}
		#endregion � tv_Nodes_KeyDown �
		#region � tv_Nodes_MouseDown �
		private void tv_Nodes_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			if (null != ((TreeView)sender).SelectedNode)
			{
				this.selectedNode = ((TreeView)sender).SelectedNode;
			}
		}
		#endregion � tv_Nodes_MouseDown �
		#region � tv_Nodes_AfterSelect �
		private void tv_Nodes_AfterSelect(object sender, System.Windows.Forms.TreeViewEventArgs e)
		{
			if (null != ((TreeView)sender).SelectedNode)
			{
				this.selectedNode = ((TreeView)sender).SelectedNode;
				Node n = (Node)this.selectedNode.Tag;
				this.GetPropertyCountInNode(n.nodeId);

				/*
				if (true == bSucGroup)
				{
					this.tbSuccessful.Text = n.nodeName;
					this.succNode = n.nodeId;
				}

				if (true == bUnSucGroup)
				{
					this.tbUnsuccessful.Text = n.nodeName;
					this.unSuccNode = n.nodeId;
				}
				*/

				//this.bSucGroup = false;
				//this.bUnSucGroup = false;
			}
		}
		#endregion � tv_Nodes_AfterSelect �
		#region � GetPropertyCountInNode �
		private void GetPropertyCountInNode (int nodeId)
		{
			SqlDataReader nodeReader = null;
			try
			{
				SqlCommand sp_GetPropertyCount = new SqlCommand("GetPropertyCount", Connection.getInstance().HatchConn);
				sp_GetPropertyCount.CommandType = CommandType.StoredProcedure;								
				sp_GetPropertyCount.Parameters.Add("@NodeId", nodeId);
									
				if (ConnectionState.Open != Connection.getInstance().HatchConn.State)
					Connection.getInstance().HatchConn.Open();

				sp_GetPropertyCount.ExecuteNonQuery();

				nodeReader = sp_GetPropertyCount.ExecuteReader();

				while (nodeReader.Read())
				{
					Int32	count				= nodeReader.GetInt32(0);
					this.tbExistingEntries.Text = count.ToString();
				}
			}
			catch	(Exception exc)
			{
				MessageBox.Show(exc.Message);
			}
			finally
			{
				nodeReader.Close();
			}
		}
		#endregion � GetPropertyCountInNode �
		#region � ImportMask6_Closing �
		private void ImportMask6_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (true == e.Cancel)
			{
				if (DialogResult.Yes == MessageBox.Show(this, UserMessages.CancelImportText, UserMessages.CancelImport, MessageBoxButtons.YesNo, MessageBoxIcon.Question))
				{
					Globals.importWizardStarted = false;
					//this.Close();
				}
			}
		}
		#endregion � ImportMask6_Closing �

		private void bSaveMap_Click(object sender, System.EventArgs e)
		{
			ImportMask5 im5 = new ImportMask5(Application.StartupPath + Globals.ReturnedMailUpdateDataProfilePath);
			im5.ShowDialog();
		}		

		private void bSelectSuccessfulGroup_Click(object sender, System.EventArgs e)
		{
			MessageBox.Show(this, "Select Group from Group treeview which will represent Your successfully skiptraced group!", "Select Group", MessageBoxButtons.OK, MessageBoxIcon.Information);
			SelectGroupForm sgf = new SelectGroupForm();
			sgf.ShowDialog();
			if (true == sgf.Valid)
			{
				this.tbSuccessful.Text = sgf.customizedTreeViewUserControl1.SelectedNodeName;
				this.succNode = sgf.customizedTreeViewUserControl1.SelectedNodeId;
			}
			//this.bSucGroup = true;
		}

		private void bSelectUnSuccessfulGroup_Click(object sender, System.EventArgs e)
		{
			MessageBox.Show(this, "Select Group from Group treeview which will represent Your un-successfully skiptraced group!", "Select Group", MessageBoxButtons.OK, MessageBoxIcon.Information);
			SelectGroupForm sgf = new SelectGroupForm();
			sgf.ShowDialog();
			if (true == sgf.Valid)
			{
				this.tbUnsuccessful.Text = sgf.customizedTreeViewUserControl1.SelectedNodeName;
				this.unSuccNode = sgf.customizedTreeViewUserControl1.SelectedNodeId;
			}
			//this.bUnSucGroup = true;
		}		

		
		private delegate void RefreshGroupsDelegate();
		private void RefreshGroups()
		{
			if (true == this.customizedTreeViewUserControl1.InvokeRequired)
			{
				this.Invoke(new RefreshGroupsDelegate(RefreshGroups));
			}
			TreeNode tnTemp = this.customizedTreeViewUserControl1.SelectedTreeNode;
			this.customizedTreeViewUserControl1.UpdateTreeView();
			if (null != tnTemp)
			{
				this.customizedTreeViewUserControl1.SelectedTreeNode = tnTemp;
			}
			//tnTemp.Expand();
		}		
	}

	internal sealed class RMW_MapperObject
	{
		public bool   succTraced 		= false;
		public string MRWContactId		= string.Empty;
		public string MRWLNSAddress		= string.Empty;	
		public string MRWLNSNumber		= string.Empty;	
		public string MRWLNSPreDir		= string.Empty;	
		public string MRWLNSName		= string.Empty;		
		public string MRWLNSPostDir		= string.Empty;	
		public string MRWLNSSuffix		= string.Empty;	
		public string MRWLNSUnitNumber	= string.Empty;
		public string MRWLNCity			= string.Empty;
		public string MRWLNState		= string.Empty;
		public string MRWLNZip			= string.Empty;	
		public string MRWSTPhoneNr1		= string.Empty;	
		public string MRWSTPhoneNr2		= string.Empty;			

		public RMW_MapperObject()
		{
		}
	}
}
