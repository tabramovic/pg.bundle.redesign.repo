using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace DealMaker
{
	/// <summary>
	/// Summary description for ReturnMailWizardForm.
	/// </summary>
	public class ReturnMailWizardForm : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Button bNext;
		private System.Windows.Forms.Button bPrevious;
		private System.Windows.Forms.Button bCancel;
		private System.Windows.Forms.Panel upperPanel;
		private System.Windows.Forms.Panel lowerPanel;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public ReturnMailWizardForm()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
	
			RetMailProcMask1 rmpm1 = new RetMailProcMask1(new ReturnMailWizardObject());
			rmpm1.Dock = DockStyle.Fill;
			this.upperPanel.Controls.Add(rmpm1);
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.upperPanel = new System.Windows.Forms.Panel();
			this.lowerPanel = new System.Windows.Forms.Panel();
			this.bNext = new System.Windows.Forms.Button();
			this.bPrevious = new System.Windows.Forms.Button();
			this.bCancel = new System.Windows.Forms.Button();
			this.lowerPanel.SuspendLayout();
			this.SuspendLayout();
			// 
			// upperPanel
			// 
			this.upperPanel.Dock = System.Windows.Forms.DockStyle.Top;
			this.upperPanel.Location = new System.Drawing.Point(0, 0);
			this.upperPanel.Name = "upperPanel";
			this.upperPanel.Size = new System.Drawing.Size(578, 384);
			this.upperPanel.TabIndex = 0;
			// 
			// lowerPanel
			// 
			this.lowerPanel.Controls.Add(this.bCancel);
			this.lowerPanel.Controls.Add(this.bPrevious);
			this.lowerPanel.Controls.Add(this.bNext);
			this.lowerPanel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lowerPanel.Location = new System.Drawing.Point(0, 384);
			this.lowerPanel.Name = "lowerPanel";
			this.lowerPanel.Size = new System.Drawing.Size(578, 56);
			this.lowerPanel.TabIndex = 1;
			// 
			// bNext
			// 
			this.bNext.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.bNext.Location = new System.Drawing.Point(488, 16);
			this.bNext.Name = "bNext";
			this.bNext.TabIndex = 0;
			this.bNext.Text = "Next";
			this.bNext.Click += new System.EventHandler(this.bNext_Click);
			// 
			// bPrevious
			// 
			this.bPrevious.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.bPrevious.Location = new System.Drawing.Point(400, 16);
			this.bPrevious.Name = "bPrevious";
			this.bPrevious.TabIndex = 1;
			this.bPrevious.Text = "Previous";
			this.bPrevious.Click += new System.EventHandler(this.bPrevious_Click);
			// 
			// bCancel
			// 
			this.bCancel.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.bCancel.Location = new System.Drawing.Point(16, 16);
			this.bCancel.Name = "bCancel";
			this.bCancel.TabIndex = 2;
			this.bCancel.Text = "Cancel";
			this.bCancel.Click += new System.EventHandler(this.bCancel_Click);
			// 
			// ReturnMailWizardForm
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(210)), ((System.Byte)(216)), ((System.Byte)(240)));
			this.ClientSize = new System.Drawing.Size(578, 440);
			this.Controls.Add(this.lowerPanel);
			this.Controls.Add(this.upperPanel);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
			this.Name = "ReturnMailWizardForm";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Return Mail Wizard";
			this.lowerPanel.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion


		public bool NextEnabled
		{
			get { return this.bNext.Enabled; }
			set { this.bNext.Enabled = value; }
		}

		private void bNext_Click(object sender, System.EventArgs e)
		{
			IEnumerator ie = this.upperPanel.Controls.GetEnumerator();			
			while (ie.MoveNext())
			{
				#region � RetMailProcMask1 �
				if (ie.Current is RetMailProcMask1)
				{
					RetMailProcMask1 rmpm1 = (RetMailProcMask1)ie.Current;
					if (eDirection.eScanIn == rmpm1.RMWO.Direction)
					{
						RetMailProcMask2 rmpm2 = new RetMailProcMask2(rmpm1.RMWO);										
						rmpm2.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top;
						rmpm2.Dock = DockStyle.Fill;

						this.upperPanel.Controls.RemoveAt(0);
						this.upperPanel.Controls.Add(rmpm2);
						this.upperPanel.Controls[0].Location = new Point(0,0);
						rmpm2.TB_Scan.Select();
						rmpm2.TB_Scan.Focus();
						break;
					}
					else
					{
						RetMailProcMaskR1 rmpmr1 = new RetMailProcMaskR1();
						rmpmr1.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top;
						rmpmr1.Dock = DockStyle.Fill;

						this.upperPanel.Controls.RemoveAt(0);
						this.upperPanel.Controls.Add(rmpmr1);
						this.upperPanel.Controls[0].Location = new Point(0,0);
						break;
					}
				}
				#endregion � RetMailProcMask1 �
				#region � RetMailProcMask2 �
				if (ie.Current is RetMailProcMask2)
				{
					RetMailProcMask2 rmpm2 = (RetMailProcMask2)ie.Current;
										
					rmpm2.AddTaggedContactsToRMWO();

					if (0 == rmpm2.RMWO.PropertyItems.Count)
					{
						MessageBox.Show(this, "Scan In return mail!", "No mail piece scanned!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
						return;
					}
					
					RetMailProcMask3 rmpm3 = new RetMailProcMask3(rmpm2.RMWO);	
					rmpm3.Init();
					rmpm3.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top;
					rmpm3.Dock = DockStyle.Fill;

					this.upperPanel.Controls.RemoveAt(0);
					this.upperPanel.Controls.Add(rmpm3);
					this.upperPanel.Controls[0].Location = new Point(0,0);
					break;
				}
				#endregion � RetMailProcMask2 �
				#region � RetMailProcMask3 �
				if (ie.Current is RetMailProcMask3)
				{
					RetMailProcMask3 rmpm3 = (RetMailProcMask3)ie.Current;
					if (false == rmpm3.ExitPage())
						return;

					RetMailProcMask4 rmpm4 = new RetMailProcMask4(rmpm3.RMWO);										
					//rmpm4.SelectedFields = rmpm3.RMWO.SelectedFields;
					rmpm4.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top;
					rmpm4.Dock = DockStyle.Fill;

					this.upperPanel.Controls.RemoveAt(0);
					this.upperPanel.Controls.Add(rmpm4);
					this.upperPanel.Controls[0].Location = new Point(0,0);
					break;
				}
				#endregion � RetMailProcMask3 �
				#region � RetMailProcMask4 �
				if (ie.Current is RetMailProcMask4)
				{
					
					RetMailProcMask4 rmpm4 = (RetMailProcMask4)ie.Current;

					if (rmpm4.CanExitEventHandler(this, null))
					{
						RetMailProcMask5 rmpm5 = new RetMailProcMask5(rmpm4.RMWO);										
						rmpm5.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top;
						rmpm5.Dock = DockStyle.Fill;

						this.upperPanel.Controls.RemoveAt(0);
						this.upperPanel.Controls.Add(rmpm5);
						this.upperPanel.Controls[0].Location = new Point(0,0);
						rmpm5.Init();
					}
					else
					{
						MessageBox.Show(this, "Add Fields for Export !", "Empty Return Mail Wizard Profile", MessageBoxButtons.OK, MessageBoxIcon.Information);
					}
					break;
				}
				#endregion � RetMailProcMask4 �
				#region � RetMailProcMask5 �
				if (ie.Current is RetMailProcMask5)
				{
					RetMailProcMask5 rmpm5 = (RetMailProcMask5)ie.Current;

					RetMailProcMask6 rmpm6 = new RetMailProcMask6(rmpm5.RMWO);										
					rmpm6.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top;
					rmpm6.Dock = DockStyle.Fill;

					this.upperPanel.Controls.RemoveAt(0);
					this.upperPanel.Controls.Add(rmpm6);
					this.upperPanel.Controls[0].Location = new Point(0,0);
					rmpm6.Init();
					break;
				}
				#endregion � RetMailProcMask5 �
				#region � RetMailProcMask6 �
				if (ie.Current is RetMailProcMask6)
				{
					RetMailProcMask6 rmpm6 = (RetMailProcMask6)ie.Current;

					RetMailProcMask7 rmpm7 = new RetMailProcMask7(rmpm6.RMWO);										
					rmpm7.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top;
					rmpm7.Dock = DockStyle.Fill;

					this.upperPanel.Controls.RemoveAt(0);
					this.upperPanel.Controls.Add(rmpm7);
					this.upperPanel.Controls[0].Location = new Point(0,0);
					rmpm7.Init();
					break;
				}
				#endregion � RetMailProcMask6 �
				#region � RetMailProcMask7 �
				if (ie.Current is RetMailProcMask7)
				{
					RetMailProcMask7 rmpm7 = (RetMailProcMask7)ie.Current;
					ReturnMailWizardObject rmwo = rmpm7.RMWO;
					rmwo.ExportFileName = rmpm7.NameTextBox;

                    if (rmpm7.NameTextBox.IndexOfAny(System.IO.Path.GetInvalidFileNameChars()) >= 0)
                    {
                        MessageBox.Show("A file name cannot contain any of the following characters: \\ / : * ? \" < > |", Globals.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        break;
                    }

					if (string.Empty == rmwo.OriginalFileName)
					{
						MessageBox.Show(this, "Enter file name", "File name missing", MessageBoxButtons.OK, MessageBoxIcon.Information);
						break;
					}                    
					rmwo.ExportFileDocument = rmpm7.LocationTextBox + @"\" + rmpm7.NameTextBox + rmpm7.TypeSuffix;
					RetMailProcMask8 rmpm8 = new RetMailProcMask8(rmwo);										
					rmpm8.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top;
					rmpm8.Dock = DockStyle.Fill;

					this.upperPanel.Controls.RemoveAt(0);
					this.upperPanel.Controls.Add(rmpm8);
					this.upperPanel.Controls[0].Location = new Point(0,0);

					rmpm8.ReAssignContactsToNewGroups();

					rmpm8.Init();

					this.bNext.Text = "Finish";
					break;
				}
				#endregion � RetMailProcMask7 �
				#region � RetMailProcMask8 �
				if (ie.Current is RetMailProcMask8)
				{
					this.Close();
				}
				#endregion � RetMailProcMask8 �
				#region * RetMailProcMaskR1 *
				if (ie.Current is RetMailProcMaskR1)
				{
					RetMailProcMaskR1 im1 = (RetMailProcMaskR1)ie.Current;
					im1.bNext_Click(sender, e);

					if (true == im1.Valid)
					{
						RetMailProcMaskR2 im2 = new RetMailProcMaskR2();					
						im2.Checked = im1.Checked;
						im2.SelectedFile = im1.SelectedFile;
						im2.InputDelimiter = im1.InputDelimiter;
						im2.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top;
						im2.Dock = DockStyle.Fill;

						this.upperPanel.Controls.RemoveAt(0);
						this.upperPanel.Controls.Add(im2);
						this.upperPanel.Controls[0].Location = new Point(0,0);						
					}
					break;
				}
				#endregion * RetMailProcMaskR1 *
				#region * RetMailProcMaskR2 *
				if (ie.Current is RetMailProcMaskR2)
				{
					RetMailProcMaskR2 rmpr2 = (RetMailProcMaskR2)ie.Current;

					if (false == rmpr2.CreateNewProfile && string.Empty == rmpr2.SelectedProfile )
					{
						MessageBox.Show(this, "Select existing profile or create a new one!", "Selection Needed!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
						return;
					}
					RetMailProcMaskR3 rmpr3 = new RetMailProcMaskR3();
					rmpr3.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top;
					rmpr3.Dock = DockStyle.Fill;
					rmpr3.SelectedFile = rmpr2.SelectedFile;
					rmpr3.Checked = rmpr2.Checked;
					rmpr3.CreateNewProfile = rmpr2.CreateNewProfile;
					rmpr3.InputDelimiter = rmpr2.InputDelimiter;
					rmpr3.SelectedProfile = rmpr2.SelectedProfile;
					
					this.upperPanel.Controls.RemoveAt(0);
					this.upperPanel.Controls.Add(rmpr3);
					this.upperPanel.Controls[0].Location = new Point(0,0);										
					break;
				}
				#endregion * RetMailProcMaskR2 *
				#region * RetMailProcMaskR3 *
				if (ie.Current is RetMailProcMaskR3)
				{
					RetMailProcMaskR3 rmpr3 = (RetMailProcMaskR3)ie.Current;
					
					if (false == rmpr3.CheckPropertyItemIdMapping())
					{
						MessageBox.Show(this, "Please map ContactId to appropriate column - usually \"PropertyItemId\"", "PropertyItemId is unassigned", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
						return;
					}

					ArrayList succRecords = new ArrayList();
					ArrayList unSuccRecords = new ArrayList();
					rmpr3.GetStats(out succRecords, out unSuccRecords);

					RetMailProcMaskR4 rmpr4 = new RetMailProcMaskR4();
					rmpr4.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top;
					rmpr4.Dock = DockStyle.Fill;
					rmpr4.InputDelimiter = rmpr3.InputDelimiter;
					rmpr4.SelectedFile = rmpr3.SelectedFile;
					rmpr4.SelectedProfile = rmpr3.SelectedProfile;
					rmpr4.CreateNewProfile = rmpr3.CreateNewProfile;
					rmpr4.Checked = rmpr3.Checked;
					rmpr4.SuccCount = succRecords;
					rmpr4.UnSuccCount = unSuccRecords;
					
					this.upperPanel.Controls.RemoveAt(0);
					this.upperPanel.Controls.Add(rmpr4);
					this.upperPanel.Controls[0].Location = new Point(0,0);	
					this.bNext.Text = "Finish";				
					break;
				}
				#endregion * RetMailProcMaskR3 *
				#region * RetMailProcMaskR4 *
				if (ie.Current is RetMailProcMaskR4)
				{
					this.Close();					
				}
				#endregion * RetMailProcMaskR4 *

			}
		}

		private void bPrevious_Click(object sender, System.EventArgs e)
		{
			IEnumerator ie = this.upperPanel.Controls.GetEnumerator();
			while (ie.MoveNext())
			{
				#region � RetMailProcMask2 �
				if (ie.Current is RetMailProcMask2)
				{
					RetMailProcMask2 rmpm2 = (RetMailProcMask2)ie.Current;

					RetMailProcMask1 rmpm1 = new RetMailProcMask1(rmpm2.RMWO);						
					rmpm1.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top;
					rmpm1.Dock = DockStyle.Fill;

					this.upperPanel.Controls.RemoveAt(0);
					this.upperPanel.Controls.Add(rmpm1);
					this.upperPanel.Controls[0].Location = new Point(0,0);
					break;
				}
				#endregion � RetMailProcMask2 �
				#region � RetMailProcMask3 �
				if (ie.Current is RetMailProcMask3)
				{
					RetMailProcMask3 rmpm3 = (RetMailProcMask3)ie.Current;

					RetMailProcMask2 rmpm2 = new RetMailProcMask2(rmpm3.RMWO);						
					rmpm2.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top;
					rmpm2.Dock = DockStyle.Fill;

					this.upperPanel.Controls.RemoveAt(0);
					this.upperPanel.Controls.Add(rmpm2);
					this.upperPanel.Controls[0].Location = new Point(0,0);
					break;
				}
				#endregion � RetMailProcMask3 �
				#region � RetMailProcMask4 �
				if (ie.Current is RetMailProcMask4)
				{
					RetMailProcMask4 rmpm4 = (RetMailProcMask4)ie.Current;

					RetMailProcMask3 rmpm3 = new RetMailProcMask3(rmpm4.RMWO);
					rmpm3.Init();	
					rmpm3.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top;
					rmpm3.Dock = DockStyle.Fill;

					this.upperPanel.Controls.RemoveAt(0);
					this.upperPanel.Controls.Add(rmpm3);
					this.upperPanel.Controls[0].Location = new Point(0,0);
					break;
				}
				#endregion � RetMailProcMask4 �
				#region � RetMailProcMask5 �
				if (ie.Current is RetMailProcMask5)
				{
					RetMailProcMask5 rmpm5 = (RetMailProcMask5)ie.Current;

					RetMailProcMask4 rmpm4 = new RetMailProcMask4(rmpm5.RMWO);					
					rmpm4.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top;
					rmpm4.Dock = DockStyle.Fill;

					this.upperPanel.Controls.RemoveAt(0);
					this.upperPanel.Controls.Add(rmpm4);
					this.upperPanel.Controls[0].Location = new Point(0,0);
					break;
				}
				#endregion � RetMailProcMask5 �
				#region � RetMailProcMask6 �
				if (ie.Current is RetMailProcMask6)
				{
					RetMailProcMask6 rmpm6 = (RetMailProcMask6)ie.Current;

					RetMailProcMask5 rmpm5 = new RetMailProcMask5(rmpm6.RMWO);					
					rmpm5.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top;
					rmpm5.Dock = DockStyle.Fill;
					rmpm5.Init();

					this.upperPanel.Controls.RemoveAt(0);
					this.upperPanel.Controls.Add(rmpm5);
					this.upperPanel.Controls[0].Location = new Point(0,0);
					break;
				}
				#endregion � RetMailProcMask6 �
				#region � RetMailProcMask7 �
				if (ie.Current is RetMailProcMask7)
				{
					RetMailProcMask7 rmpm7 = (RetMailProcMask7)ie.Current;

					RetMailProcMask6 rmpm6 = new RetMailProcMask6(rmpm7.RMWO);					
					rmpm6.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top;
					rmpm6.Dock = DockStyle.Fill;
					rmpm6.Init();

					this.upperPanel.Controls.RemoveAt(0);
					this.upperPanel.Controls.Add(rmpm6);
					this.upperPanel.Controls[0].Location = new Point(0,0);
					break;
				}
				#endregion � RetMailProcMask7 �
				#region � RetMailProcMask8 �
				if (ie.Current is RetMailProcMask8)
				{
					RetMailProcMask8 rmpm8 = (RetMailProcMask8)ie.Current;

					RetMailProcMask7 rmpm7 = new RetMailProcMask7(rmpm8.RMWO);					
					rmpm7.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top;
					rmpm7.Dock = DockStyle.Fill;
					rmpm7.Init();

					this.upperPanel.Controls.RemoveAt(0);
					this.upperPanel.Controls.Add(rmpm7);
					this.upperPanel.Controls[0].Location = new Point(0,0);
					this.bNext.Text = "Next";
					break;
				}
				#endregion � RetMailProcMask7 �
				#region * RetMailProcMaskR1 *
				if (ie.Current is RetMailProcMaskR1)
				{
					RetMailProcMaskR1 im1 = (RetMailProcMaskR1)ie.Current;					

					RetMailProcMask1 rmpm1 = new RetMailProcMask1(new ReturnMailWizardObject());	//since it`s other wizard						
					rmpm1.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top;
					rmpm1.Dock = DockStyle.Fill;

					this.upperPanel.Controls.RemoveAt(0);
					this.upperPanel.Controls.Add(rmpm1);
					this.upperPanel.Controls[0].Location = new Point(0,0);
					break;
				}
				#endregion * RetMailProcMaskR1 *
				#region * RetMailProcMaskR2 *
				if (ie.Current is RetMailProcMaskR2)
				{
					RetMailProcMaskR2 rmpr2 = (RetMailProcMaskR2)ie.Current;

					RetMailProcMaskR1 rmpr1 = new RetMailProcMaskR1();
					rmpr1.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top;
					rmpr1.Dock = DockStyle.Fill;
					rmpr1.SelectedFile = rmpr2.SelectedFile;
					rmpr1.Checked = rmpr2.Checked;					
					rmpr1.InputDelimiter = rmpr2.InputDelimiter;					
					
					this.upperPanel.Controls.RemoveAt(0);
					this.upperPanel.Controls.Add(rmpr1);
					this.upperPanel.Controls[0].Location = new Point(0,0);										
					break;
				}
				#endregion * RetMailProcMaskR2 *
				#region * RetMailProcMaskR3 *
				if (ie.Current is RetMailProcMaskR3)
				{
					RetMailProcMaskR3 rmpr3 = (RetMailProcMaskR3)ie.Current;

					RetMailProcMaskR2 rmpr2 = new RetMailProcMaskR2();
					rmpr2.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top;
					rmpr2.Dock = DockStyle.Fill;
					rmpr2.InputDelimiter = rmpr3.InputDelimiter;
					rmpr2.SelectedFile = rmpr3.SelectedFile;
					rmpr2.SelectedProfile = rmpr3.SelectedProfile;
					rmpr2.CreateNewProfile = rmpr3.CreateNewProfile;
					rmpr2.Checked = rmpr3.Checked;
					
					this.upperPanel.Controls.RemoveAt(0);
					this.upperPanel.Controls.Add(rmpr2);
					this.upperPanel.Controls[0].Location = new Point(0,0);										
					break;
				}
				#endregion * RetMailProcMaskR3 *
				#region * RetMailProcMaskR4 *
				if (ie.Current is RetMailProcMaskR4)
				{
					RetMailProcMaskR4 rmpr4 = (RetMailProcMaskR4)ie.Current;

					RetMailProcMaskR3 rmpr3 = new RetMailProcMaskR3();
					rmpr3.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top;
					rmpr3.Dock = DockStyle.Fill;
					rmpr3.InputDelimiter = rmpr4.InputDelimiter;
					rmpr3.SelectedFile = rmpr4.SelectedFile;
					rmpr3.SelectedProfile = rmpr4.SelectedProfile;
					rmpr3.CreateNewProfile = rmpr4.CreateNewProfile;
					rmpr3.Checked = rmpr4.Checked;
					
					this.upperPanel.Controls.RemoveAt(0);
					this.upperPanel.Controls.Add(rmpr3);
					this.upperPanel.Controls[0].Location = new Point(0,0);
					this.bNext.Text = "Next";									
					break;
				}
				#endregion * RetMailProcMaskR4 *
			}
		}

		private void bCancel_Click(object sender, System.EventArgs e)
		{
			if (DialogResult.Yes == MessageBox.Show(this, "Are you sure that you want to exit Return Mail Wizard?", "Quit Return Mail Wizard", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2))
			{
				this.Close();
			}
		}
	}
}
