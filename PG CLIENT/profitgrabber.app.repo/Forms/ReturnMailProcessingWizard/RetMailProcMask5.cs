using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using DealMaker.Export;
using DealMaker.Filters;
using DealMaker.Forms.Wizard;
using DealMaker.PlainClasses;

namespace DealMaker
{
	public class RetMailProcMask5 : System.Windows.Forms.UserControl
	{
		private ReturnMailWizardObject rmwo = null;
		ListViewItem renameRow = null;
		TextBox columnEdit = new TextBox();
		private System.Windows.Forms.Label renameInfoLabel;
		private System.Windows.Forms.ListView renameListView;
		private System.ComponentModel.IContainer components = null;

		[StructLayout(LayoutKind.Sequential)]
			struct DumbPInvokeRect
		{
			public System.Int32 left;
			public System.Int32 top;
			public System.Int32 right;
			public System.Int32 bottom;
		};
 
		[DllImport("user32.dll")]
		private static extern System.Int32 SendMessage(System.IntPtr hWnd, System.UInt32 Msg, System.Int32 wParam, ref DumbPInvokeRect rect);



		/*public RenameColumnsPage()
		{
			// This call is required by the Windows Form Designer.
			InitializeComponent();
		}*/

		public ReturnMailWizardObject RMWO
		{
			get { return this.rmwo; }
			set { this.rmwo = value; }
		}

		public RetMailProcMask5(ReturnMailWizardObject rmwo)
		{
			// This call is required by the Windows Form Designer.
			InitializeComponent();

			this.rmwo = rmwo;			

			//this.Enter += new EventHandler(EnteredEventHandler);

			columnEdit.Visible = false;
			renameListView.Controls.Add(columnEdit);
			
			columnEdit.Leave += new EventHandler(ColumnEditLeaveEventHandler);
			columnEdit.KeyDown += new KeyEventHandler(ColumnEditKeyDown);
			renameListView.ColumnClick += new ColumnClickEventHandler(ColumnClickedEventHandler);
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		private void CreateHeader()
		{
			int width;
			ExportAttributeMapping[] mappings = this.rmwo.ExpAttributeMapping;
			if (null == mappings)
				return;
			
			renameListView.Columns.Clear();
			width = 100;

			// Create header for ListView control
			renameRow = new ListViewItem();
			renameRow.BackColor = Color.Yellow;
			foreach (ExportAttributeMapping mapping in mappings)
			{
				ColumnHeader header = new ColumnHeader();

				header.Width = width;
				header.Text = mapping.ColumnName;
				renameListView.Columns.Add(header);

				// Create yellow rename row
				if (String.Empty == renameRow.Text)
					renameRow.Text = mapping.ColumnName;
				else
					renameRow.SubItems.Add(mapping.ColumnName);
			}

			renameListView.Items.Add(renameRow);
			renameListView.Refresh();
		}		

		private void PopulateList()
		{
			int i;
			int sample = 10;
			PropertyItem[] contacts;
			ExportAttributeMapping[] mappings;
			//SelectContactsPage scp = WizardForm[0] as SelectContactsPage;
			//CreateExportFilterPage cefp = WizardForm[3] as CreateExportFilterPage;
			//CreateExportProfilePage cepp = WizardForm[2] as CreateExportProfilePage;

			mappings = this.RMWO.ExpAttributeMapping;
			contacts = new PropertyItemCriteria((PropertyItem[])this.RMWO.PropertyItems.ToArray(typeof(PropertyItem)) , null).List();

			// Populate list view with sample data
			if (contacts.Length < sample)
				sample = contacts.Length;

			for (i = 0; i < sample; i++)
			{
				ListViewItem item = new ListViewItem();
				if (null != mappings)
				{
					foreach (ExportAttributeMapping mapping in mappings)
					{
						string text = String.Empty;

						if (String.Empty != mapping.AttributeName &&
							ExportAttributeMapping.EmptyColumn != mapping.AttributeName)
						{		
							//TA++: 02.04.2005.
							//Dirty hack to read manually added properties in propertyItemAttribute hashtable added to propertyItem object							
							if (null != contacts[i][mapping.AttributeName])
							{
								try
								{									
									text = contacts[i][mapping.AttributeName].AttributeValue.ToString();								
								}
								catch (Exception)								
								{								
									PropertyItemAttribute pia = (PropertyItemAttribute)contacts[i][mapping.AttributeName];
									if (null != pia)
									{
										text = pia.AttributeName;
									}
								}
							}
							else
							{
								text = string.Empty;
							}
							//TA--: 02.04.2005.
						}
					
						if (String.Empty == item.Text)
							item.Text = text;
						else
							item.SubItems.Add(text);
					}
				
				renameListView.Items.Add(item);
				}
			}
		}

		public void Init()
		{
			renameListView.SuspendLayout();

			renameListView.Items.Clear();

			CreateHeader();
			PopulateList();

			renameListView.ResumeLayout();
			renameListView.Refresh();
		}
		
		private void ColumnClickedEventHandler(object sender, ColumnClickEventArgs e)
		{
			DumbPInvokeRect rect = new DumbPInvokeRect();

			rect.top = e.Column;
			rect.left = 0;

			/* Do the dumb pinvoke just to get subitem rectangle */
			SendMessage(renameListView.Handle, 0x1000 + 56, 0, ref rect);

			columnEdit.Left = rect.left;
			columnEdit.Top = renameRow.Bounds.Top;
			columnEdit.Height = renameRow.Bounds.Height;
			columnEdit.Width = renameListView.Columns[e.Column].Width;
			columnEdit.Text = this.RMWO.SelectedFields[e.Column].ColumnName;
			columnEdit.Tag = e.Column;
			columnEdit.Visible = true;
			columnEdit.SelectAll();
			columnEdit.Focus();
		}

		private void ColumnEditLeaveEventHandler(object sender, EventArgs e)
		{
			int column = (int)columnEdit.Tag;
			

			this.RMWO.SelectedFields[column].ColumnName = columnEdit.Text;
			if (0 == column)
				renameRow.Text = columnEdit.Text;
			else
				renameRow.SubItems[column].Text = columnEdit.Text;

			columnEdit.Visible = false;
		}

		public void ColumnEditKeyDown(object sender, KeyEventArgs e)
		{
			switch (e.KeyCode)
			{
				case Keys.Enter:
					columnEdit.Visible = false;
					break;

				case Keys.Escape:
					columnEdit.Text = renameListView.Columns[(int)columnEdit.Tag].Text;
					columnEdit.Visible = false;
					break;
			}
		}

		#region Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.renameInfoLabel = new System.Windows.Forms.Label();
			this.renameListView = new System.Windows.Forms.ListView();
			this.SuspendLayout();
			// 
			// renameInfoLabel
			// 
			this.renameInfoLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.renameInfoLabel.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(210)), ((System.Byte)(216)), ((System.Byte)(240)));
			this.renameInfoLabel.Location = new System.Drawing.Point(8, 8);
			this.renameInfoLabel.Name = "renameInfoLabel";
			this.renameInfoLabel.Size = new System.Drawing.Size(400, 48);
			this.renameInfoLabel.TabIndex = 0;
			this.renameInfoLabel.Text = "This screen allows you to rename the columns accordingly to your export file Rece" +
				"iver requirements. To rename a column, click on its main header. In most cases t" +
				"here will be no special requirements, so you can just click \"next\" to skip this " +
				"screen.";
			// 
			// renameListView
			// 
			this.renameListView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.renameListView.FullRowSelect = true;
			this.renameListView.GridLines = true;
			this.renameListView.HideSelection = false;
			this.renameListView.LabelEdit = true;
			this.renameListView.Location = new System.Drawing.Point(8, 64);
			this.renameListView.MultiSelect = false;
			this.renameListView.Name = "renameListView";
			this.renameListView.Size = new System.Drawing.Size(400, 200);
			this.renameListView.TabIndex = 1;
			this.renameListView.View = System.Windows.Forms.View.Details;
			// 
			// RetMailProcMask5
			// 
			this.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(210)), ((System.Byte)(216)), ((System.Byte)(240)));
			this.Controls.Add(this.renameListView);
			this.Controls.Add(this.renameInfoLabel);
			this.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.Name = "RetMailProcMask5";
			this.Size = new System.Drawing.Size(416, 272);
			this.ResumeLayout(false);

		}
		#endregion
	}
}
