using System;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using DealMaker.Export;
using DealMaker.Filters;
using DealMaker.Forms.Wizard;
using DealMaker.Forms.Export;

namespace DealMaker
{
	/// <summary>
	/// Summary description for RetMailProcMask6.
	/// </summary>
	public class RetMailProcMask6 : System.Windows.Forms.UserControl
	{
		private ReturnMailWizardObject rmwo = null;
		private System.Windows.Forms.Label infoLabel0;
		private System.Windows.Forms.Label infoLabel1;
		private System.Windows.Forms.Label skipInfoLabel;
		private System.Windows.Forms.Label warningLabel;
		private System.Windows.Forms.Label torelRecordsLabel;
		private System.Windows.Forms.Label totelFilteredLabel;
		private System.Windows.Forms.Label totalCountLabel;
		private System.Windows.Forms.Label filteredCountLabel;
		private System.Windows.Forms.Button saveMapButton;
		private System.ComponentModel.Container components = null;

		public RetMailProcMask6(ReturnMailWizardObject rmwo)
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

			//Entered += new EventHandler(EnteredEventHandler);
			
			this.rmwo = rmwo;	
			saveMapButton.Click += new EventHandler(SaveMapButtonClickedEventHandler);

		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		public ReturnMailWizardObject RMWO
		{
			get { return this.rmwo; }
			set { this.rmwo = value; }
		}

		public void Init()
		{
			int filteredCount = 0;
			

			filteredCountLabel.Text = "calculating...";

			totalCountLabel.Text = this.RMWO.PropertyItems.Count.ToString();
			filteredCount = new PropertyItemCriteria((PropertyItem[])this.RMWO.PropertyItems.ToArray(typeof(PropertyItem)), null).List().Length;

			filteredCountLabel.Text = filteredCount.ToString();
		}

		private void SaveMapButtonClickedEventHandler(object sender, EventArgs e)
		{
			XmlSerializer serializer;
			ExportMappingProfile profile;
			string path = Application.StartupPath + @"\" +Globals.ReturnedMailProfilePath;
			
			
			do
			{
				string profileFileName;
				SaveMappingProfileForm saveDialog = new SaveMappingProfileForm();

				if (!saveDialog.Execute())
					break;

				profileFileName = path + "\\" + saveDialog.ProfileName + ".profile.xml";

				if (File.Exists(profileFileName))
				{
					if (DialogResult.No == MessageBox.Show(
						"Profile under that name already exists\n" +
						"Do you want to replace old profile with this profile?",
						"Replace profile",
						MessageBoxButtons.YesNo))
					{
						continue;
					}
				}
						
				/* Check if directory exists and create it if not */
				if (!Directory.Exists(path))
					Directory.CreateDirectory(path);

				serializer = new XmlSerializer(typeof(ExportMappingProfile));
				profile = new ExportMappingProfile(
					saveDialog.ProfileName, saveDialog.ProfileDescription,
					null, null, this.RMWO.SelectedFields);

				try
				{
					StreamWriter writer = new StreamWriter(profileFileName);
					serializer.Serialize(writer, profile);
					writer.Close();
				}
				catch (System.Exception ex)
				{
					MessageBox.Show(ex.ToString());
				}

				break;

			} while (true);
		}

		#region Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.infoLabel0 = new System.Windows.Forms.Label();
			this.infoLabel1 = new System.Windows.Forms.Label();
			this.skipInfoLabel = new System.Windows.Forms.Label();
			this.warningLabel = new System.Windows.Forms.Label();
			this.torelRecordsLabel = new System.Windows.Forms.Label();
			this.totelFilteredLabel = new System.Windows.Forms.Label();
			this.totalCountLabel = new System.Windows.Forms.Label();
			this.filteredCountLabel = new System.Windows.Forms.Label();
			this.saveMapButton = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// infoLabel0
			// 
			this.infoLabel0.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.infoLabel0.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(210)), ((System.Byte)(216)), ((System.Byte)(240)));
			this.infoLabel0.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.infoLabel0.Location = new System.Drawing.Point(8, 8);
			this.infoLabel0.Name = "infoLabel0";
			this.infoLabel0.Size = new System.Drawing.Size(512, 32);
			this.infoLabel0.TabIndex = 0;
			this.infoLabel0.Text = "Most of the time you will be exporting files to the same sources, therefore using" +
				" the same mappng profile.";
			// 
			// infoLabel1
			// 
			this.infoLabel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.infoLabel1.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.infoLabel1.Location = new System.Drawing.Point(8, 48);
			this.infoLabel1.Name = "infoLabel1";
			this.infoLabel1.Size = new System.Drawing.Size(512, 32);
			this.infoLabel1.TabIndex = 1;
			this.infoLabel1.Text = "If you foresee using Mapping Profile that you just created, click SAVE MAP below!" +
				" This will save the Export Mapping Profile.";
			// 
			// skipInfoLabel
			// 
			this.skipInfoLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.skipInfoLabel.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.skipInfoLabel.Location = new System.Drawing.Point(8, 208);
			this.skipInfoLabel.Name = "skipInfoLabel";
			this.skipInfoLabel.Size = new System.Drawing.Size(512, 16);
			this.skipInfoLabel.TabIndex = 2;
			this.skipInfoLabel.Text = "To SKIP saving Mapping Profile click Next!";
			// 
			// warningLabel
			// 
			this.warningLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.warningLabel.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.warningLabel.Location = new System.Drawing.Point(8, 224);
			this.warningLabel.Name = "warningLabel";
			this.warningLabel.Size = new System.Drawing.Size(512, 16);
			this.warningLabel.TabIndex = 3;
			this.warningLabel.Text = "Warning! If you skip saving this profile, you will need to recreate it again next" +
				" time!";
			// 
			// torelRecordsLabel
			// 
			this.torelRecordsLabel.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.torelRecordsLabel.Location = new System.Drawing.Point(8, 136);
			this.torelRecordsLabel.Name = "torelRecordsLabel";
			this.torelRecordsLabel.Size = new System.Drawing.Size(272, 16);
			this.torelRecordsLabel.TabIndex = 4;
			this.torelRecordsLabel.Text = "TOTAL # of Records BEFORE the filter was applied:";
			this.torelRecordsLabel.Visible = false;
			// 
			// totelFilteredLabel
			// 
			this.totelFilteredLabel.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.totelFilteredLabel.Location = new System.Drawing.Point(8, 152);
			this.totelFilteredLabel.Name = "totelFilteredLabel";
			this.totelFilteredLabel.Size = new System.Drawing.Size(272, 16);
			this.totelFilteredLabel.TabIndex = 5;
			this.totelFilteredLabel.Text = "TOTAL # of Records AFTER the filter was applied:";
			this.totelFilteredLabel.Visible = false;
			// 
			// totalCountLabel
			// 
			this.totalCountLabel.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.totalCountLabel.Location = new System.Drawing.Point(272, 136);
			this.totalCountLabel.Name = "totalCountLabel";
			this.totalCountLabel.Size = new System.Drawing.Size(72, 16);
			this.totalCountLabel.TabIndex = 6;
			this.totalCountLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
			this.totalCountLabel.Visible = false;
			// 
			// filteredCountLabel
			// 
			this.filteredCountLabel.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.filteredCountLabel.Location = new System.Drawing.Point(272, 152);
			this.filteredCountLabel.Name = "filteredCountLabel";
			this.filteredCountLabel.Size = new System.Drawing.Size(72, 16);
			this.filteredCountLabel.TabIndex = 7;
			this.filteredCountLabel.Text = "calculating...";
			this.filteredCountLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
			this.filteredCountLabel.Visible = false;
			// 
			// saveMapButton
			// 
			this.saveMapButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.saveMapButton.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.saveMapButton.Location = new System.Drawing.Point(280, 96);
			this.saveMapButton.Name = "saveMapButton";
			this.saveMapButton.Size = new System.Drawing.Size(72, 28);
			this.saveMapButton.TabIndex = 8;
			this.saveMapButton.Text = "SAVE MAP";
			// 
			// RetMailProcMask6
			// 
			this.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(210)), ((System.Byte)(216)), ((System.Byte)(240)));
			this.Controls.Add(this.saveMapButton);
			this.Controls.Add(this.filteredCountLabel);
			this.Controls.Add(this.totalCountLabel);
			this.Controls.Add(this.totelFilteredLabel);
			this.Controls.Add(this.torelRecordsLabel);
			this.Controls.Add(this.warningLabel);
			this.Controls.Add(this.skipInfoLabel);
			this.Controls.Add(this.infoLabel1);
			this.Controls.Add(this.infoLabel0);
			this.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(41)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.Name = "RetMailProcMask6";
			this.Size = new System.Drawing.Size(528, 248);
			this.Load += new System.EventHandler(this.RetMailProcMask6_Load);
			this.ResumeLayout(false);

		}
		#endregion

		private void RetMailProcMask6_Load(object sender, System.EventArgs e)
		{
		
		}
	}
}
