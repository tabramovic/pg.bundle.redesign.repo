using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;

namespace DealMaker
{
	/// <summary>
	/// Summary description for RetMailProcMask1.
	/// </summary>
	public class RetMailProcMask1 : System.Windows.Forms.UserControl
	{
		private ReturnMailWizardObject rmwo = null;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.RadioButton rbScan;
		private System.Windows.Forms.RadioButton rbUpdate;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.PictureBox pbPicture;
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public RetMailProcMask1(ReturnMailWizardObject rmwo)
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

			System.IO.Stream  stream = null;
			System.Reflection.Assembly assembly = System.Reflection.Assembly.GetExecutingAssembly();
											
			stream = assembly.GetManifestResourceStream("DealMaker.res.Scan_In_Step1.gif");
			if (null != stream)
				this.pbPicture.Image = System.Drawing.Image.FromStream(stream);			

			this.rmwo = rmwo;
		}

		public ReturnMailWizardObject RMWO
		{
			get { return this.rmwo; }
			set { this.rmwo = value; }
		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.rbScan = new System.Windows.Forms.RadioButton();
			this.rbUpdate = new System.Windows.Forms.RadioButton();
			this.label2 = new System.Windows.Forms.Label();
			this.pbPicture = new System.Windows.Forms.PictureBox();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label1.Location = new System.Drawing.Point(16, 16);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(352, 23);
			this.label1.TabIndex = 0;
			this.label1.Text = "Select below �to start� or �to complete� the Return Mail Processing:";
			// 
			// rbScan
			// 
			this.rbScan.Checked = true;
			this.rbScan.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.rbScan.Location = new System.Drawing.Point(160, 40);
			this.rbScan.Name = "rbScan";
			this.rbScan.Size = new System.Drawing.Size(296, 24);
			this.rbScan.TabIndex = 1;
			this.rbScan.TabStop = true;
			this.rbScan.Text = "Scan-in the returned mail && email it to a Skip-tracer";
			this.rbScan.CheckedChanged += new System.EventHandler(this.rbScan_CheckedChanged);
			// 
			// rbUpdate
			// 
			this.rbUpdate.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.rbUpdate.Location = new System.Drawing.Point(160, 64);
			this.rbUpdate.Name = "rbUpdate";
			this.rbUpdate.Size = new System.Drawing.Size(296, 24);
			this.rbUpdate.TabIndex = 2;
			this.rbUpdate.Text = "Update your contacts with the Skip-traced records";
			this.rbUpdate.CheckedChanged += new System.EventHandler(this.rbScan_CheckedChanged);
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(16, 136);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(280, 23);
			this.label2.TabIndex = 3;
			this.label2.Text = "The picture below describes the selected process:";
			// 
			// pbPicture
			// 
			this.pbPicture.Location = new System.Drawing.Point(104, 176);
			this.pbPicture.Name = "pbPicture";
			this.pbPicture.Size = new System.Drawing.Size(512, 112);
			this.pbPicture.TabIndex = 4;
			this.pbPicture.TabStop = false;
			// 
			// RetMailProcMask1
			// 
			this.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(210)), ((System.Byte)(216)), ((System.Byte)(240)));
			this.Controls.Add(this.pbPicture);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.rbUpdate);
			this.Controls.Add(this.rbScan);
			this.Controls.Add(this.label1);
			this.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.Name = "RetMailProcMask1";
			this.Size = new System.Drawing.Size(648, 320);
			this.ResumeLayout(false);

		}
		#endregion

		private void rbScan_CheckedChanged(object sender, System.EventArgs e)
		{
			System.IO.Stream  stream = null;
			System.Reflection.Assembly assembly = System.Reflection.Assembly.GetExecutingAssembly();

			if (true == this.rbScan.Checked)
			{								
				stream = assembly.GetManifestResourceStream("DealMaker.res.Scan_In_Step1.gif");
				if (null != stream)
					this.pbPicture.Image = System.Drawing.Image.FromStream(stream);
				this.RMWO.Direction = eDirection.eScanIn;
			}
			else
			{
				stream = assembly.GetManifestResourceStream("DealMaker.res.Return_Mail_Wizard_Step2.gif");
				if (null != stream)
					this.pbPicture.Image = System.Drawing.Image.FromStream(stream);
				this.RMWO.Direction = eDirection.eUpdate;
			}
		}
	}
}
