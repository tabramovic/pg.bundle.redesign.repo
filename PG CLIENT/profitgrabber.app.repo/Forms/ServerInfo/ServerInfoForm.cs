using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace DealMaker
{
	/// <summary>
	/// Summary description for ServerInfoForm.
	/// </summary>
	public class ServerInfoForm : System.Windows.Forms.Form
	{
		bool _validClose = false;
		private System.Windows.Forms.Button bOK;
		private System.Windows.Forms.Button bCancel;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox tb1;
		private System.Windows.Forms.TextBox tb2;
		private System.Windows.Forms.TextBox tb3;
		private System.Windows.Forms.TextBox tb4;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public ServerInfoForm()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			bOK.Click += new EventHandler(OnOKClick);
			bCancel.Click += new EventHandler(OnCancelClick);
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.bOK = new System.Windows.Forms.Button();
			this.bCancel = new System.Windows.Forms.Button();
			this.label1 = new System.Windows.Forms.Label();
			this.tb1 = new System.Windows.Forms.TextBox();
			this.tb2 = new System.Windows.Forms.TextBox();
			this.tb3 = new System.Windows.Forms.TextBox();
			this.tb4 = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// bOK
			// 
			this.bOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.bOK.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.bOK.Location = new System.Drawing.Point(146, 94);
			this.bOK.Name = "bOK";
			this.bOK.TabIndex = 0;
			this.bOK.Text = "OK";
			// 
			// bCancel
			// 
			this.bCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.bCancel.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.bCancel.Location = new System.Drawing.Point(232, 94);
			this.bCancel.Name = "bCancel";
			this.bCancel.TabIndex = 1;
			this.bCancel.Text = "Cancel";
			// 
			// label1
			// 
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label1.Location = new System.Drawing.Point(16, 16);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(248, 23);
			this.label1.TabIndex = 2;
			this.label1.Text = "Enter ProfitGrabber Pro Server Key Code:";
			// 
			// tb1
			// 
			this.tb1.Location = new System.Drawing.Point(32, 48);
			this.tb1.MaxLength = 4;
			this.tb1.Name = "tb1";
			this.tb1.Size = new System.Drawing.Size(56, 20);
			this.tb1.TabIndex = 3;
			this.tb1.Text = "";
			this.tb1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// tb2
			// 
			this.tb2.Location = new System.Drawing.Point(104, 48);
			this.tb2.MaxLength = 4;
			this.tb2.Name = "tb2";
			this.tb2.Size = new System.Drawing.Size(56, 20);
			this.tb2.TabIndex = 4;
			this.tb2.Text = "";
			this.tb2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// tb3
			// 
			this.tb3.Location = new System.Drawing.Point(176, 48);
			this.tb3.MaxLength = 4;
			this.tb3.Name = "tb3";
			this.tb3.Size = new System.Drawing.Size(56, 20);
			this.tb3.TabIndex = 5;
			this.tb3.Text = "";
			this.tb3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// tb4
			// 
			this.tb4.Location = new System.Drawing.Point(248, 48);
			this.tb4.MaxLength = 4;
			this.tb4.Name = "tb4";
			this.tb4.Size = new System.Drawing.Size(56, 20);
			this.tb4.TabIndex = 6;
			this.tb4.Text = "";
			this.tb4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(93, 51);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(8, 23);
			this.label2.TabIndex = 7;
			this.label2.Text = "-";
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(164, 51);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(8, 23);
			this.label3.TabIndex = 8;
			this.label3.Text = "-";
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(237, 51);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(8, 23);
			this.label4.TabIndex = 9;
			this.label4.Text = "-";
			// 
			// ServerInfoForm
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(210)), ((System.Byte)(216)), ((System.Byte)(240)));
			this.ClientSize = new System.Drawing.Size(322, 128);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.tb4);
			this.Controls.Add(this.tb3);
			this.Controls.Add(this.tb2);
			this.Controls.Add(this.tb1);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.bCancel);
			this.Controls.Add(this.bOK);
			this.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "ServerInfoForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Server Info";
			this.ResumeLayout(false);

		}
		#endregion

		public bool ValidClose
		{
			get 
			{
				return _validClose;
			}
		}

		public string Serial
		{
			get 
			{
				return tb1.Text + "-" + tb2.Text + "-" + tb3.Text + "-" + tb4.Text;
			}
		}

		private void OnOKClick(object sender, EventArgs e)
		{
			if (null != Globals.DataConsumptionReg)
				Globals.DataConsumptionReg.KeyCode = Serial.ToUpper();

			_validClose = true;
			this.Close();
		}

		private void OnCancelClick(object sender, EventArgs e)
		{			
			this.Close();
		}		
	}
}
