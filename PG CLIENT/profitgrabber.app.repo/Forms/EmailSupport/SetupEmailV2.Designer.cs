﻿namespace DealMaker.Forms.EmailSupport
{
    partial class SetupEmailV2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label22 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.replyTo = new System.Windows.Forms.Label();
            this._replyTo = new System.Windows.Forms.TextBox();
            this.displayName = new System.Windows.Forms.Label();
            this._displayName = new System.Windows.Forms.TextBox();
            this._response = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this._testSendingEmail = new System.Windows.Forms.Button();
            this.label14 = new System.Windows.Forms.Label();
            this._to = new System.Windows.Forms.TextBox();
            this._cancel = new System.Windows.Forms.Button();
            this._ok = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this._fromEmailAddress = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this._saveEmailSettings = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Red;
            this.label22.Location = new System.Drawing.Point(12, 102);
            this.label22.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(229, 13);
            this.label22.TabIndex = 37;
            this.label22.Text = "You must use a Gmail or Google Workspace email address.";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(12, 79);
            this.label17.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(372, 13);
            this.label17.TabIndex = 36;
            this.label17.Text = "To send Email Marketing, use the Mail/Email Wizard or Assign an Email Task.";
            // 
            // label18
            // 
            this.label18.Location = new System.Drawing.Point(12, 30);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(618, 35);
            this.label18.TabIndex = 35;
            this.label18.Text = "To setup the email and and text addresses for Office Staff to receive reminders, " +
    "please select [User Information] from the [File] menu and follow the instruction" +
    "s there.";
            // 
            // label16
            // 
            this.label16.Location = new System.Drawing.Point(12, 9);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(630, 33);
            this.label16.TabIndex = 34;
            this.label16.Text = "This is where you setup your email configuration for Profit Grabber Task Manager " +
    "and Email Marketing.";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.ForeColor = System.Drawing.Color.Gray;
            this.label19.Location = new System.Drawing.Point(383, 225);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(134, 13);
            this.label19.TabIndex = 42;
            this.label19.Text = "Your Real Name (i.e. John)";
            // 
            // replyTo
            // 
            this.replyTo.AutoSize = true;
            this.replyTo.Enabled = false;
            this.replyTo.Location = new System.Drawing.Point(12, 251);
            this.replyTo.Name = "replyTo";
            this.replyTo.Size = new System.Drawing.Size(53, 13);
            this.replyTo.TabIndex = 40;
            this.replyTo.Text = "Reply To:";
            this.replyTo.Visible = false;
            // 
            // _replyTo
            // 
            this._replyTo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._replyTo.Location = new System.Drawing.Point(171, 249);
            this._replyTo.Name = "_replyTo";
            this._replyTo.Size = new System.Drawing.Size(206, 20);
            this._replyTo.TabIndex = 3;
            this._replyTo.Visible = false;
            // 
            // displayName
            // 
            this.displayName.AutoSize = true;
            this.displayName.Location = new System.Drawing.Point(12, 225);
            this.displayName.Name = "displayName";
            this.displayName.Size = new System.Drawing.Size(75, 13);
            this.displayName.TabIndex = 38;
            this.displayName.Text = "Display Name:";
            // 
            // _displayName
            // 
            this._displayName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._displayName.Location = new System.Drawing.Point(171, 223);
            this._displayName.Name = "_displayName";
            this._displayName.Size = new System.Drawing.Size(206, 20);
            this._displayName.TabIndex = 2;
            // 
            // _response
            // 
            this._response.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._response.Location = new System.Drawing.Point(108, 430);
            this._response.Name = "_response";
            this._response.Size = new System.Drawing.Size(533, 56);
            this._response.TabIndex = 47;
            this._response.Text = "-";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(12, 338);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(330, 13);
            this.label15.TabIndex = 48;
            this.label15.Text = "Type-in Recipient\'s Email Address to test your current Gmail Settings.";
            // 
            // _testSendingEmail
            // 
            this._testSendingEmail.Location = new System.Drawing.Point(386, 359);
            this._testSendingEmail.Name = "_testSendingEmail";
            this._testSendingEmail.Size = new System.Drawing.Size(121, 23);
            this._testSendingEmail.TabIndex = 5;
            this._testSendingEmail.Text = "Send a Test Email";
            this._testSendingEmail.UseVisualStyleBackColor = true;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(12, 362);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(131, 13);
            this.label14.TabIndex = 44;
            this.label14.Text = "Recipient\'s Email Address:";
            // 
            // _to
            // 
            this._to.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._to.Location = new System.Drawing.Point(171, 360);
            this._to.Name = "_to";
            this._to.Size = new System.Drawing.Size(206, 20);
            this._to.TabIndex = 4;
            // 
            // _cancel
            // 
            this._cancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this._cancel.Location = new System.Drawing.Point(489, 491);
            this._cancel.Name = "_cancel";
            this._cancel.Size = new System.Drawing.Size(75, 23);
            this._cancel.TabIndex = 6;
            this._cancel.Text = "Cancel";
            this._cancel.UseVisualStyleBackColor = true;
            // 
            // _ok
            // 
            this._ok.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._ok.DialogResult = System.Windows.Forms.DialogResult.OK;
            this._ok.Location = new System.Drawing.Point(570, 491);
            this._ok.Name = "_ok";
            this._ok.Size = new System.Drawing.Size(75, 23);
            this._ok.TabIndex = 7;
            this._ok.Text = "OK";
            this._ok.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 198);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(102, 13);
            this.label4.TabIndex = 51;
            this.label4.Text = "From Email Address:";
            // 
            // _fromEmailAddress
            // 
            this._fromEmailAddress.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._fromEmailAddress.Location = new System.Drawing.Point(171, 196);
            this._fromEmailAddress.Name = "_fromEmailAddress";
            this._fromEmailAddress.Size = new System.Drawing.Size(206, 20);
            this._fromEmailAddress.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.Gray;
            this.label1.Location = new System.Drawing.Point(383, 198);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(149, 13);
            this.label1.TabIndex = 53;
            this.label1.Text = "Your Google Workspace or Gmail Address";
            // 
            // _saveEmailSettings
            // 
            this._saveEmailSettings.Location = new System.Drawing.Point(386, 388);
            this._saveEmailSettings.Name = "_saveEmailSettings";
            this._saveEmailSettings.Size = new System.Drawing.Size(121, 23);
            this._saveEmailSettings.TabIndex = 54;
            this._saveEmailSettings.Text = "Save Email Settings";
            this._saveEmailSettings.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(12, 123);
            this.label2.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(369, 13);
            this.label2.TabIndex = 55;
            this.label2.Text = "Profit Grabber can be used for Email Marketing up to 50 or so Emails per day.";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(12, 165);
            this.label3.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(499, 13);
            this.label3.TabIndex = 56;
            this.label3.Text = "Check the Video Training under the Help Menu to learn how to send larger quantiti" +
    "es of Email Marketing.";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Red;
            this.label5.Location = new System.Drawing.Point(12, 288);
            this.label5.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(439, 13);
            this.label5.TabIndex = 57;
            this.label5.Text = "You must Send a Test Email to authenticate your account before you send Email Mar" +
    "keting.";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Red;
            this.label6.Location = new System.Drawing.Point(12, 311);
            this.label6.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(487, 13);
            this.label6.TabIndex = 58;
            this.label6.Text = "Please watch the training video under the Help menu to learn how to authenticate " +
    "your email account.";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.ForeColor = System.Drawing.Color.Gray;
            this.label20.Location = new System.Drawing.Point(383, 251);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(161, 13);
            this.label20.TabIndex = 43;
            this.label20.Text = "Desired Reply To Email Address ";
            this.label20.Visible = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Red;
            this.label7.Location = new System.Drawing.Point(12, 144);
            this.label7.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(591, 13);
            this.label7.TabIndex = 59;
            this.label7.Text = "If you plan to do more volume than that you will likely experience difficulty wit" +
    "h google shutting down your account as SPAM.";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(12, 430);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(92, 13);
            this.label8.TabIndex = 60;
            this.label8.Text = "Test Email Status:";
            // 
            // SetupEmailV2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.ClientSize = new System.Drawing.Size(658, 526);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this._saveEmailSettings);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this._fromEmailAddress);
            this.Controls.Add(this._cancel);
            this.Controls.Add(this._ok);
            this.Controls.Add(this._response);
            this.Controls.Add(this.label15);
            this.Controls.Add(this._testSendingEmail);
            this.Controls.Add(this.label14);
            this.Controls.Add(this._to);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.replyTo);
            this.Controls.Add(this._replyTo);
            this.Controls.Add(this.displayName);
            this.Controls.Add(this._displayName);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label16);
            this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "SetupEmailV2";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Email Setup for Email Marketing and Task Reminders";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label replyTo;
        private System.Windows.Forms.TextBox _replyTo;
        private System.Windows.Forms.Label displayName;
        private System.Windows.Forms.TextBox _displayName;
        private System.Windows.Forms.Label _response;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Button _testSendingEmail;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox _to;
        private System.Windows.Forms.Button _cancel;
        private System.Windows.Forms.Button _ok;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox _fromEmailAddress;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button _saveEmailSettings;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
    }
}