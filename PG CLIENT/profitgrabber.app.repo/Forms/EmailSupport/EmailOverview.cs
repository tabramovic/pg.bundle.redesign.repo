﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using Newtonsoft.Json;

namespace DealMaker.Forms.EmailSupport
{
    public partial class Email_Overview : Form
    {
        private Guid pgId;
        private string url;

        public Email_Overview(Guid pgid)
        {
            pgId = pgid;

            //TODO: Set into Config
            url = $"https://www.profitgrabber.com/EmailSenderService/api/sendrequests/{pgId}";
            //url = $"http://localhost:54052/api/sendrequests/{pgId}";
            InitializeComponent();

            GetData();
        }

        private delegate void UpdateLabelDelegate(string newText);
        void UpdateLabel (string newText)
        {
            if (InvokeRequired)
                BeginInvoke(new UpdateLabelDelegate(UpdateLabel), new object[] { newText });
            else
                _downloadLabel.Text = newText;
        }

        private delegate void AddToListViewDelegate(ListViewItem[] lvis);
        void AddToListView(ListViewItem[] lvis)
        {
            if (InvokeRequired)
                BeginInvoke(new AddToListViewDelegate(AddToListView), new object[] { lvis });
            else
            {
                emails.Items.Clear();
                emails.Items.AddRange(lvis);
            }
        }

        private void GetData()
        {            
            System.Threading.Tasks.Task.Factory.StartNew(() => 
            {
                var receivedData = GetDataFromServer();
                RenderChanges(receivedData);
            });
        }

        dynamic GetDataFromServer()
        {
            try
            {
                using (var wc = new WebClient())
                {
                    var sendRequestsAsJSON = wc.DownloadString(url);                    
                    dynamic dO = JsonConvert.DeserializeObject(sendRequestsAsJSON);

                    return dO;                    
                }
            }            
            catch(Exception e)
            {
                MessageBox.Show($"Error in retrieve");
                return null;
            }
        }

        private void RenderChanges(dynamic receivedData)
        {
            List<ListViewItem> lvis = new List<ListViewItem>();
            foreach (var data in receivedData)
            {
                string receivedTs = data.ReceivedTS;
                string from = data.From;
                string to = data.To;
                string subject = data.Subject;
                string statusId = data.StatusID;
                string statusDesc = GetFullStatus(statusId);

                ListViewItem lvi = new ListViewItem(receivedTs);
                lvi.SubItems.Add(from);
                lvi.SubItems.Add(to);
                lvi.SubItems.Add(subject);
                lvi.SubItems.Add(statusDesc);

                lvis.Add(lvi);
            }

            AddToListView(lvis.ToArray());
            UpdateLabel("Ready");
        }

        private static string GetFullStatus(string statusId)
        {
            string fullStatus = string.Empty;
            switch (statusId)
            {
                case "WP":
                    fullStatus = "Not yet sent";
                    break;

                case "ST":
                    fullStatus = "Sent";
                    break;

                case "CN":
                    fullStatus = "Cancelled";
                    break;

                case "F":
                    fullStatus = "Failed";
                    break;

                default:
                    fullStatus = string.Empty;
                    break;
            }

            return fullStatus;
        }

        private void close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void _reload_Click(object sender, EventArgs e)
        {
            GetData();
        }
    }
}
