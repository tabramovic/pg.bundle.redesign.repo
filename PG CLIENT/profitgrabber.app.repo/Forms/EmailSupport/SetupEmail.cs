﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

//Added
using DealMaker.PlainClasses;

namespace DealMaker.Forms.EmailSupport
{
    public partial class SetupEmail : Form
    {
        public bool SaveSettings { get; set; }
        public SMTPObject SmtpSettings { get; set; }

        public SetupEmail()
        {
            InitializeComponent();
            AddEventHandlers();
        }

        public SetupEmail(SMTPObject smtpSettings)
        {
            InitializeComponent();
            AddEventHandlers();

            if (null != smtpSettings)
            {
                _smtpServer.Text = smtpSettings.OutgoingSMTPServer;
                _smtpPort.Text = smtpSettings.Port;
                _useSSL.Checked = smtpSettings.EnableSSL;
                _accountName.Text = smtpSettings.AccountName;
                _emailAddress.Text = smtpSettings.EmailAddress;
                _password.Text = smtpSettings.Password;
                _displayName.Text = smtpSettings.DisplayName;
                _replyTo.Text = smtpSettings.ReplyTo;
                
            }

            try
            {
                if ("own" == (string)DealMaker.Properties.Settings.Default["BlindOffersEmailAccount"])
                    _rbBlindOffersOwn.Checked = true;
                else if ("pg" == (string)DealMaker.Properties.Settings.Default["BlindOffersEmailAccount"])
                    _rbBlindOffersPG.Checked = true;                    
            }
            catch (Exception e)
            {
                bool stopHere = true;
            }            
        }
        
        void AddEventHandlers()
        {
            _showPwd.CheckedChanged += new EventHandler(On_ShowPwd_CheckedChanged);
            _ok.Click += new EventHandler(On_Ok_Click);
            _cancel.Click += new EventHandler(On_Cancel_Click);
            _testSMTPSettings.Click += new EventHandler(On_TestSMTPSettings_Click);
        }
        
        void On_Cancel_Click(object sender, EventArgs e)
        {
            SaveSettings = false;            
        }

        void On_Ok_Click(object sender, EventArgs e)
        {
            SaveSettings = true;

            if (_rbBlindOffersOwn.Checked)
                DealMaker.Properties.Settings.Default["BlindOffersEmailAccount"] = "own";
            else
                if (_rbBlindOffersPG.Checked)
                DealMaker.Properties.Settings.Default["BlindOffersEmailAccount"] = "pg";

            DealMaker.Properties.Settings.Default.Save();

            UpdateSmtpSettings();
        }

        void On_ShowPwd_CheckedChanged(object sender, EventArgs e)
        {
            _password.UseSystemPasswordChar = !_showPwd.Checked;
        }

        void On_TestSMTPSettings_Click(object sender, EventArgs e)
        {
            string errMsg = string.Empty;

            UpdateSmtpSettings();
            bool testRes = SmtpSettings.TestSettings(_recipient.Text, out errMsg);

            if (testRes)
                _succSendLabel.Text = "Email successfully sent!";
            else
                _succSendLabel.Text = "Error: " + errMsg;
        }

        void UpdateSmtpSettings()
        {
            SmtpSettings = new SMTPObject();

            SmtpSettings.OutgoingSMTPServer = _smtpServer.Text;
            SmtpSettings.Port = _smtpPort.Text;
            SmtpSettings.EnableSSL = _useSSL.Checked;
            SmtpSettings.AccountName = _accountName.Text;
            SmtpSettings.EmailAddress = _emailAddress.Text;
            SmtpSettings.Password = _password.Text;
            SmtpSettings.ReplyTo = _replyTo.Text;
            SmtpSettings.DisplayName = _displayName.Text;
        }

        private void SetupEmail_Load(object sender, EventArgs e)
        {

        }
    }
}
