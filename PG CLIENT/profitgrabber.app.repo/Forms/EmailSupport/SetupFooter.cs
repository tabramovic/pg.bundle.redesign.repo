﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DealMaker.Forms.EmailSupport
{
    public partial class SetupFooter : Form
    {
        public SetupFooter()
        {
            InitializeComponent();

            emailBlastSignature.Text = Globals.EmailBlastSig;
            emailLOISignature.Text = Globals.EmailLOISig;
        }

        private void save_Click(object sender, EventArgs e)
        {
            try
            {
                using (var sw = new StreamWriter(OSFolderManager.Instance.GetPlainLocalUserAppDataPath() + @"\" + Globals.EmailBlastFileName))
                {
                    sw.Write(emailBlastSignature.Text);
                    sw.Flush();

                    Globals.EmailBlastSig = emailBlastSignature.Text;
                }

                using (var sw = new StreamWriter(OSFolderManager.Instance.GetPlainLocalUserAppDataPath() + @"\" + Globals.EmailLOIFileName))
                {
                    sw.Write(emailLOISignature.Text);
                    sw.Flush();

                    Globals.EmailLOISig = emailLOISignature.Text;
                }
            }
            catch (Exception)
            {
                MessageBox.Show($"Error in save", Globals.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }            
        }
    }
}
