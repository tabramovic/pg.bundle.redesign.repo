﻿namespace DealMaker.Forms.EmailSupport
{
    partial class Email_Overview
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.header = new System.Windows.Forms.Panel();
            this._downloadLabel = new System.Windows.Forms.Label();
            this.footer = new System.Windows.Forms.Panel();
            this.close = new System.Windows.Forms.Button();
            this.body = new System.Windows.Forms.Panel();
            this.emails = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this._reload = new System.Windows.Forms.Button();
            this.header.SuspendLayout();
            this.footer.SuspendLayout();
            this.body.SuspendLayout();
            this.SuspendLayout();
            // 
            // header
            // 
            this.header.Controls.Add(this._downloadLabel);
            this.header.Dock = System.Windows.Forms.DockStyle.Top;
            this.header.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.header.Location = new System.Drawing.Point(0, 0);
            this.header.Name = "header";
            this.header.Size = new System.Drawing.Size(894, 43);
            this.header.TabIndex = 0;
            // 
            // _downloadLabel
            // 
            this._downloadLabel.AutoSize = true;
            this._downloadLabel.Location = new System.Drawing.Point(12, 9);
            this._downloadLabel.Name = "_downloadLabel";
            this._downloadLabel.Size = new System.Drawing.Size(102, 13);
            this._downloadLabel.TabIndex = 0;
            this._downloadLabel.Text = "Downloading data...";
            // 
            // footer
            // 
            this.footer.Controls.Add(this._reload);
            this.footer.Controls.Add(this.close);
            this.footer.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.footer.Location = new System.Drawing.Point(0, 439);
            this.footer.Name = "footer";
            this.footer.Size = new System.Drawing.Size(894, 48);
            this.footer.TabIndex = 1;
            // 
            // close
            // 
            this.close.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.close.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.close.Location = new System.Drawing.Point(807, 13);
            this.close.Name = "close";
            this.close.Size = new System.Drawing.Size(75, 23);
            this.close.TabIndex = 0;
            this.close.Text = "Close";
            this.close.UseVisualStyleBackColor = true;
            this.close.Click += new System.EventHandler(this.close_Click);
            // 
            // body
            // 
            this.body.Controls.Add(this.emails);
            this.body.Dock = System.Windows.Forms.DockStyle.Fill;
            this.body.Location = new System.Drawing.Point(0, 43);
            this.body.Name = "body";
            this.body.Size = new System.Drawing.Size(894, 396);
            this.body.TabIndex = 2;
            // 
            // emails
            // 
            this.emails.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader5,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4});
            this.emails.Dock = System.Windows.Forms.DockStyle.Fill;
            this.emails.FullRowSelect = true;
            this.emails.GridLines = true;
            this.emails.Location = new System.Drawing.Point(0, 0);
            this.emails.Name = "emails";
            this.emails.Size = new System.Drawing.Size(894, 396);
            this.emails.TabIndex = 0;
            this.emails.UseCompatibleStateImageBehavior = false;
            this.emails.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Sent";
            this.columnHeader1.Width = 136;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "From";
            this.columnHeader5.Width = 160;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "To";
            this.columnHeader2.Width = 160;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Subject";
            this.columnHeader3.Width = 300;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Status";
            this.columnHeader4.Width = 50;
            // 
            // _reload
            // 
            this._reload.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._reload.Location = new System.Drawing.Point(15, 13);
            this._reload.Name = "_reload";
            this._reload.Size = new System.Drawing.Size(75, 23);
            this._reload.TabIndex = 1;
            this._reload.Text = "Reload";
            this._reload.UseVisualStyleBackColor = true;
            this._reload.Click += new System.EventHandler(this._reload_Click);
            // 
            // Email_Overview
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.ClientSize = new System.Drawing.Size(894, 487);
            this.Controls.Add(this.body);
            this.Controls.Add(this.footer);
            this.Controls.Add(this.header);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "Email_Overview";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Overview Sent Emails";
            this.header.ResumeLayout(false);
            this.header.PerformLayout();
            this.footer.ResumeLayout(false);
            this.body.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel header;
        private System.Windows.Forms.Panel footer;
        private System.Windows.Forms.Panel body;
        private System.Windows.Forms.ListView emails;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.Button close;
        private System.Windows.Forms.Label _downloadLabel;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.Button _reload;
    }
}