﻿using google.oauth.lib;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DealMaker.Forms.EmailSupport
{
    public partial class SetupEmailV2 : Form
    {
        SMTPObject smtpSettings;

        public SetupEmailV2()
        {
            InitializeComponent();
            AddEventHandlers();

            smtpSettings = new SMTPObject();
            LoadSettings();
        }

        void AddEventHandlers()
        {
            _saveEmailSettings.Click += _saveEmailSettings_Click;
            _testSendingEmail.Click += _testSendingEmail_Click;            
            _ok.Click += _ok_Click;
        }        

        private void _saveEmailSettings_Click(object sender, EventArgs e)
        {
            SaveSettings();
        }

        private void _ok_Click(object sender, EventArgs e)
        {
            SaveSettings();
            Close();
        }

        void SaveSettings()
        {
            smtpSettings.EmailAddress = _fromEmailAddress.Text;
            smtpSettings.DisplayName = _displayName.Text;
            smtpSettings.ReplyTo = _fromEmailAddress.Text; //_replyTo.Text;            
            smtpSettings.TestRecipient = _to.Text;

            smtpSettings.PersistSettings();
        }

        void LoadSettings()
        {            
            smtpSettings = smtpSettings.LoadSettigns();

            _fromEmailAddress.Text = smtpSettings.EmailAddress;
            _displayName.Text = smtpSettings.DisplayName;
            _replyTo.Text = smtpSettings.ReplyTo;            
            _to.Text = smtpSettings.TestRecipient;
        }

        private async void _testSendingEmail_Click(object sender, EventArgs e)
        {
            using (var mailMsg = new MailMessage { Subject = "Email from ProfitGrabber", Body = "This is test email from ProfitGrabber", From = new MailAddress(_fromEmailAddress.Text, _displayName.Text) })
            {
                mailMsg.To.Add(_to.Text);
                mailMsg.ReplyToList.Add(mailMsg.From);
                
                try
                {
                    using (var emailService = new EmailService())
                    {
                        await emailService.InitializeGmailAccountService(Globals.ProductName, Globals.ClientId, Globals.ClientSecret, Globals.GetUserToAuthorize(smtpSettings?.EmailAddress));
                        var response = emailService.SendEmail(mailMsg);

                        _response.Text = $"Sent at {DateTime.Now.ToLongTimeString()}";
                    }
                }
                catch (Exception exc)
                {
                    MessageBox.Show($"Please click OK and try to send email again and Profit Grabber will try to authenticate the Email again.{Environment.NewLine}{Environment.NewLine}{exc.Message}");
                }
            }
        }
    }
}
