﻿namespace DealMaker.Forms.EmailSupport
{
    partial class SetupEmail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this._ok = new System.Windows.Forms.Button();
            this._cancel = new System.Windows.Forms.Button();
            this._smtpServer = new System.Windows.Forms.TextBox();
            this._accountName = new System.Windows.Forms.TextBox();
            this._emailAddress = new System.Windows.Forms.TextBox();
            this._password = new System.Windows.Forms.TextBox();
            this._showPwd = new System.Windows.Forms.CheckBox();
            this.label6 = new System.Windows.Forms.Label();
            this._smtpPort = new System.Windows.Forms.NumericUpDown();
            this._useSSL = new System.Windows.Forms.CheckBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.replyTo = new System.Windows.Forms.Label();
            this._replyTo = new System.Windows.Forms.TextBox();
            this.displayName = new System.Windows.Forms.Label();
            this._displayName = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this._succSendLabel = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this._testSMTPSettings = new System.Windows.Forms.Button();
            this.label14 = new System.Windows.Forms.Label();
            this._recipient = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this._rbBlindOffersPG = new System.Windows.Forms.RadioButton();
            this._rbBlindOffersOwn = new System.Windows.Forms.RadioButton();
            this.line1 = new DealMaker.UserControls.Line();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this._smtpPort)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 13);
            this.label1.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 166);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(148, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Outgoing Mail (SMTP) Server:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 242);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(81, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Account Name:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 269);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(76, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Email Address:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 295);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(56, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Password:";
            // 
            // _ok
            // 
            this._ok.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._ok.DialogResult = System.Windows.Forms.DialogResult.OK;
            this._ok.Location = new System.Drawing.Point(593, 674);
            this._ok.Name = "_ok";
            this._ok.Size = new System.Drawing.Size(75, 23);
            this._ok.TabIndex = 17;
            this._ok.Text = "OK";
            this._ok.UseVisualStyleBackColor = true;
            // 
            // _cancel
            // 
            this._cancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this._cancel.Location = new System.Drawing.Point(512, 674);
            this._cancel.Name = "_cancel";
            this._cancel.Size = new System.Drawing.Size(75, 23);
            this._cancel.TabIndex = 16;
            this._cancel.Text = "Cancel";
            this._cancel.UseVisualStyleBackColor = true;
            // 
            // _smtpServer
            // 
            this._smtpServer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._smtpServer.Location = new System.Drawing.Point(165, 164);
            this._smtpServer.Name = "_smtpServer";
            this._smtpServer.Size = new System.Drawing.Size(206, 20);
            this._smtpServer.TabIndex = 7;
            // 
            // _accountName
            // 
            this._accountName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._accountName.Location = new System.Drawing.Point(165, 240);
            this._accountName.Name = "_accountName";
            this._accountName.Size = new System.Drawing.Size(206, 20);
            this._accountName.TabIndex = 10;
            // 
            // _emailAddress
            // 
            this._emailAddress.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._emailAddress.Location = new System.Drawing.Point(165, 267);
            this._emailAddress.Name = "_emailAddress";
            this._emailAddress.Size = new System.Drawing.Size(206, 20);
            this._emailAddress.TabIndex = 11;
            // 
            // _password
            // 
            this._password.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._password.Location = new System.Drawing.Point(165, 293);
            this._password.Name = "_password";
            this._password.Size = new System.Drawing.Size(206, 20);
            this._password.TabIndex = 12;
            this._password.UseSystemPasswordChar = true;
            // 
            // _showPwd
            // 
            this._showPwd.AutoSize = true;
            this._showPwd.Location = new System.Drawing.Point(165, 319);
            this._showPwd.Name = "_showPwd";
            this._showPwd.Size = new System.Drawing.Size(137, 17);
            this._showPwd.TabIndex = 13;
            this._showPwd.Text = "Show Password Letters";
            this._showPwd.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(125, 192);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(29, 13);
            this.label6.TabIndex = 13;
            this.label6.Text = "Port:";
            // 
            // _smtpPort
            // 
            this._smtpPort.Location = new System.Drawing.Point(165, 192);
            this._smtpPort.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this._smtpPort.Name = "_smtpPort";
            this._smtpPort.Size = new System.Drawing.Size(73, 20);
            this._smtpPort.TabIndex = 8;
            // 
            // _useSSL
            // 
            this._useSSL.AutoSize = true;
            this._useSSL.Location = new System.Drawing.Point(244, 195);
            this._useSSL.Name = "_useSSL";
            this._useSSL.Size = new System.Drawing.Size(68, 17);
            this._useSSL.TabIndex = 9;
            this._useSSL.Text = "Use SSL";
            this._useSSL.UseVisualStyleBackColor = true;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.ForeColor = System.Drawing.Color.Gray;
            this.label7.Location = new System.Drawing.Point(6, 147);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(295, 13);
            this.label7.TabIndex = 16;
            this.label7.Text = "Gray Values show default values for Google\'s SMTP account";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.ForeColor = System.Drawing.Color.Gray;
            this.label8.Location = new System.Drawing.Point(377, 166);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(79, 13);
            this.label8.TabIndex = 17;
            this.label8.Text = "smtp.gmail.com";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.ForeColor = System.Drawing.Color.Gray;
            this.label9.Location = new System.Drawing.Point(162, 215);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(25, 13);
            this.label9.TabIndex = 18;
            this.label9.Text = "587";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.ForeColor = System.Drawing.Color.Gray;
            this.label10.Location = new System.Drawing.Point(241, 215);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(28, 13);
            this.label10.TabIndex = 19;
            this.label10.Text = "YES";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.ForeColor = System.Drawing.Color.Gray;
            this.label11.Location = new System.Drawing.Point(377, 242);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(124, 13);
            this.label11.TabIndex = 20;
            this.label11.Text = "youraccount@gmail.com";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.ForeColor = System.Drawing.Color.Gray;
            this.label12.Location = new System.Drawing.Point(377, 269);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(124, 13);
            this.label12.TabIndex = 21;
            this.label12.Text = "youraccount@gmail.com";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.ForeColor = System.Drawing.Color.Gray;
            this.label13.Location = new System.Drawing.Point(377, 295);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(72, 13);
            this.label13.TabIndex = 22;
            this.label13.Text = "yourpassword";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label22);
            this.groupBox1.Controls.Add(this.label20);
            this.groupBox1.Controls.Add(this.label19);
            this.groupBox1.Controls.Add(this.replyTo);
            this.groupBox1.Controls.Add(this._replyTo);
            this.groupBox1.Controls.Add(this.displayName);
            this.groupBox1.Controls.Add(this._displayName);
            this.groupBox1.Controls.Add(this.label17);
            this.groupBox1.Controls.Add(this.label18);
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this._smtpServer);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this._accountName);
            this.groupBox1.Controls.Add(this._emailAddress);
            this.groupBox1.Controls.Add(this._useSSL);
            this.groupBox1.Controls.Add(this._password);
            this.groupBox1.Controls.Add(this._smtpPort);
            this.groupBox1.Controls.Add(this._showPwd);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Location = new System.Drawing.Point(25, 15);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(642, 396);
            this.groupBox1.TabIndex = 23;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Simple Mail Transfer Protocol Settings";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.ForeColor = System.Drawing.Color.Gray;
            this.label20.Location = new System.Drawing.Point(377, 369);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(161, 13);
            this.label20.TabIndex = 32;
            this.label20.Text = "Desired Reply To Email Address ";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.ForeColor = System.Drawing.Color.Gray;
            this.label19.Location = new System.Drawing.Point(377, 343);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(134, 13);
            this.label19.TabIndex = 31;
            this.label19.Text = "Your Real Name (i.e. John)";
            // 
            // replyTo
            // 
            this.replyTo.AutoSize = true;
            this.replyTo.Location = new System.Drawing.Point(6, 369);
            this.replyTo.Name = "replyTo";
            this.replyTo.Size = new System.Drawing.Size(53, 13);
            this.replyTo.TabIndex = 29;
            this.replyTo.Text = "Reply To:";
            // 
            // _replyTo
            // 
            this._replyTo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._replyTo.Location = new System.Drawing.Point(165, 367);
            this._replyTo.Name = "_replyTo";
            this._replyTo.Size = new System.Drawing.Size(206, 20);
            this._replyTo.TabIndex = 30;
            // 
            // displayName
            // 
            this.displayName.AutoSize = true;
            this.displayName.Location = new System.Drawing.Point(6, 343);
            this.displayName.Name = "displayName";
            this.displayName.Size = new System.Drawing.Size(75, 13);
            this.displayName.TabIndex = 27;
            this.displayName.Text = "Display Name:";
            // 
            // _displayName
            // 
            this._displayName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._displayName.Location = new System.Drawing.Point(165, 341);
            this._displayName.Name = "_displayName";
            this._displayName.Size = new System.Drawing.Size(206, 20);
            this._displayName.TabIndex = 28;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(6, 92);
            this.label17.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(372, 13);
            this.label17.TabIndex = 26;
            this.label17.Text = "To send Email Marketing, use the Mail/Email Wizard or Assign an Email Task.";
            // 
            // label18
            // 
            this.label18.Location = new System.Drawing.Point(6, 43);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(618, 35);
            this.label18.TabIndex = 25;
            this.label18.Text = "To setup the email and and text addresses for Office Staff to receive reminders, " +
    "please select [User Information] from the [File] menu and follow the instruction" +
    "s there.";
            // 
            // label16
            // 
            this.label16.Location = new System.Drawing.Point(6, 22);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(630, 33);
            this.label16.TabIndex = 23;
            this.label16.Text = "This is where you setup your email configuration for Profit Grabber Task Manager " +
    "and Email Marketing.";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this._succSendLabel);
            this.groupBox2.Controls.Add(this.label15);
            this.groupBox2.Controls.Add(this._testSMTPSettings);
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Controls.Add(this._recipient);
            this.groupBox2.Location = new System.Drawing.Point(25, 417);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(642, 131);
            this.groupBox2.TabIndex = 24;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Test Settings";
            // 
            // _succSendLabel
            // 
            this._succSendLabel.Location = new System.Drawing.Point(10, 81);
            this._succSendLabel.Name = "_succSendLabel";
            this._succSendLabel.Size = new System.Drawing.Size(607, 51);
            this._succSendLabel.TabIndex = 23;
            this._succSendLabel.Text = "-";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(6, 28);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(334, 13);
            this.label15.TabIndex = 26;
            this.label15.Text = "Type-in Recipient\'s Email Address to test your current SMTP Settings.";
            // 
            // _testSMTPSettings
            // 
            this._testSMTPSettings.Location = new System.Drawing.Point(380, 48);
            this._testSMTPSettings.Name = "_testSMTPSettings";
            this._testSMTPSettings.Size = new System.Drawing.Size(121, 23);
            this._testSMTPSettings.TabIndex = 15;
            this._testSMTPSettings.Text = "Send a Test Email";
            this._testSMTPSettings.UseVisualStyleBackColor = true;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(6, 51);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(131, 13);
            this.label14.TabIndex = 10;
            this.label14.Text = "Recipient\'s Email Address:";
            // 
            // _recipient
            // 
            this._recipient.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._recipient.Location = new System.Drawing.Point(168, 49);
            this._recipient.Name = "_recipient";
            this._recipient.Size = new System.Drawing.Size(206, 20);
            this._recipient.TabIndex = 14;
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.groupBox3.Controls.Add(this.label21);
            this.groupBox3.Controls.Add(this._rbBlindOffersPG);
            this.groupBox3.Controls.Add(this._rbBlindOffersOwn);
            this.groupBox3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.groupBox3.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.groupBox3.Location = new System.Drawing.Point(25, 575);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(1);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(1);
            this.groupBox3.Size = new System.Drawing.Size(642, 95);
            this.groupBox3.TabIndex = 25;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "For Email Marketing";
            // 
            // _rbBlindOffersPG
            // 
            this._rbBlindOffersPG.AutoSize = true;
            this._rbBlindOffersPG.Location = new System.Drawing.Point(58, 45);
            this._rbBlindOffersPG.Margin = new System.Windows.Forms.Padding(1);
            this._rbBlindOffersPG.Name = "_rbBlindOffersPG";
            this._rbBlindOffersPG.Size = new System.Drawing.Size(337, 17);
            this._rbBlindOffersPG.TabIndex = 1;
            this._rbBlindOffersPG.Text = "Use the generic Profit Grabber account to send Email Marketing\". ";
            this._rbBlindOffersPG.UseVisualStyleBackColor = true;
            // 
            // _rbBlindOffersOwn
            // 
            this._rbBlindOffersOwn.AutoSize = true;
            this._rbBlindOffersOwn.Checked = true;
            this._rbBlindOffersOwn.Location = new System.Drawing.Point(58, 20);
            this._rbBlindOffersOwn.Margin = new System.Windows.Forms.Padding(1);
            this._rbBlindOffersOwn.Name = "_rbBlindOffersOwn";
            this._rbBlindOffersOwn.Size = new System.Drawing.Size(541, 17);
            this._rbBlindOffersOwn.TabIndex = 0;
            this._rbBlindOffersOwn.TabStop = true;
            this._rbBlindOffersOwn.Text = "Use your personal email account above to send Email Marketing (strongly preferred" +
    " due to better deliverability)";
            this._rbBlindOffersOwn.UseVisualStyleBackColor = true;
            // 
            // line1
            // 
            this.line1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.line1.Location = new System.Drawing.Point(15, 549);
            this.line1.Name = "line1";
            this.line1.Size = new System.Drawing.Size(648, 16);
            this.line1.TabIndex = 2;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(96, 67);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(305, 13);
            this.label21.TabIndex = 27;
            this.label21.Text = "(make sure you have a Reply To: email address filled in above.)";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Red;
            this.label22.Location = new System.Drawing.Point(6, 119);
            this.label22.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(553, 13);
            this.label22.TabIndex = 33;
            this.label22.Text = "We recommend creating a gmail account specifically for Email Marketing and not us" +
    "ing your personal gmail account.";
            // 
            // SetupEmail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.ClientSize = new System.Drawing.Size(674, 709);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.line1);
            this.Controls.Add(this._cancel);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this._ok);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label1);
            this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SetupEmail";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Email Setup for Email Marketing and Task Reminders";
            this.Load += new System.EventHandler(this.SetupEmail_Load);
            ((System.ComponentModel.ISupportInitialize)(this._smtpPort)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button _ok;
        private System.Windows.Forms.Button _cancel;
        private System.Windows.Forms.TextBox _smtpServer;
        private System.Windows.Forms.TextBox _accountName;
        private System.Windows.Forms.TextBox _emailAddress;
        private System.Windows.Forms.TextBox _password;
        private System.Windows.Forms.CheckBox _showPwd;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown _smtpPort;
        private System.Windows.Forms.CheckBox _useSSL;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Button _testSMTPSettings;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox _recipient;
        private System.Windows.Forms.Label _succSendLabel;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.RadioButton _rbBlindOffersPG;
        private System.Windows.Forms.RadioButton _rbBlindOffersOwn;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label replyTo;
        private System.Windows.Forms.TextBox _replyTo;
        private System.Windows.Forms.Label displayName;
        private System.Windows.Forms.TextBox _displayName;
        private UserControls.Line line1;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
    }
}