#region � Using �
using System;
using System.IO;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
#endregion � Using �
#region � namespace DealMaker �
namespace DealMaker
{
	#region � Class SaveProfileForm �
	/// <summary>
	/// SaveProfileForm Class - saves import profile + filters
	/// </summary>
	public class SaveProfileForm : System.Windows.Forms.Form
	{
		#region � Data �
		private string profilesPath = Application.StartupPath + Globals.ProfilePath;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TextBox tbProfileName;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Button bOK;
		private System.Windows.Forms.Button bCancel;
		private System.Windows.Forms.TextBox tbProfileDesc;
		private System.Windows.Forms.Button bSave;
		private System.Windows.Forms.ListView lvProfiles;
		private System.Windows.Forms.ColumnHeader rofileName;
		private System.Windows.Forms.ColumnHeader profileDesc;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
		#endregion � Data �
		#region � CTR && Dispose �
		public SaveProfileForm()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}
		public SaveProfileForm(string profilesPath)
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			this.profilesPath = profilesPath;
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}
		#endregion � CTR && Dispose �
		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.lvProfiles = new System.Windows.Forms.ListView();
			this.rofileName = new System.Windows.Forms.ColumnHeader();
			this.profileDesc = new System.Windows.Forms.ColumnHeader();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.tbProfileName = new System.Windows.Forms.TextBox();
			this.label4 = new System.Windows.Forms.Label();
			this.tbProfileDesc = new System.Windows.Forms.TextBox();
			this.bOK = new System.Windows.Forms.Button();
			this.bCancel = new System.Windows.Forms.Button();
			this.bSave = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label1.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.label1.Location = new System.Drawing.Point(24, 16);
			this.label1.Name = "label1";
			this.label1.TabIndex = 0;
			this.label1.Text = "Save Profile";
			// 
			// lvProfiles
			// 
			this.lvProfiles.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
																						 this.rofileName,
																						 this.profileDesc});
			this.lvProfiles.FullRowSelect = true;
			this.lvProfiles.GridLines = true;
			this.lvProfiles.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
			this.lvProfiles.HideSelection = false;
			this.lvProfiles.Location = new System.Drawing.Point(32, 80);
			this.lvProfiles.MultiSelect = false;
			this.lvProfiles.Name = "lvProfiles";
			this.lvProfiles.Size = new System.Drawing.Size(592, 192);
			this.lvProfiles.TabIndex = 1;
			this.lvProfiles.View = System.Windows.Forms.View.Details;
			this.lvProfiles.SelectedIndexChanged += new System.EventHandler(this.lvProfiles_SelectedIndexChanged);
			// 
			// rofileName
			// 
			this.rofileName.Text = "Profile Name";
			this.rofileName.Width = 154;
			// 
			// profileDesc
			// 
			this.profileDesc.Text = "Profile Description";
			this.profileDesc.Width = 434;
			// 
			// label2
			// 
			this.label2.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.label2.Location = new System.Drawing.Point(32, 48);
			this.label2.Name = "label2";
			this.label2.TabIndex = 2;
			this.label2.Text = "Exsisting profiles";
			// 
			// label3
			// 
			this.label3.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.label3.Location = new System.Drawing.Point(32, 288);
			this.label3.Name = "label3";
			this.label3.TabIndex = 3;
			this.label3.Text = "Profile Name:";
			// 
			// tbProfileName
			// 
			this.tbProfileName.Location = new System.Drawing.Point(144, 288);
			this.tbProfileName.Name = "tbProfileName";
			this.tbProfileName.Size = new System.Drawing.Size(152, 20);
			this.tbProfileName.TabIndex = 4;
			this.tbProfileName.Text = "";
			// 
			// label4
			// 
			this.label4.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.label4.Location = new System.Drawing.Point(32, 320);
			this.label4.Name = "label4";
			this.label4.TabIndex = 5;
			this.label4.Text = "Profile Description:";
			// 
			// tbProfileDesc
			// 
			this.tbProfileDesc.Location = new System.Drawing.Point(144, 320);
			this.tbProfileDesc.Name = "tbProfileDesc";
			this.tbProfileDesc.Size = new System.Drawing.Size(480, 20);
			this.tbProfileDesc.TabIndex = 6;
			this.tbProfileDesc.Text = "";
			// 
			// bOK
			// 
			this.bOK.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.bOK.Location = new System.Drawing.Point(376, 368);
			this.bOK.Name = "bOK";
			this.bOK.TabIndex = 8;
			this.bOK.Text = "OK";
			this.bOK.Visible = false;
			this.bOK.Click += new System.EventHandler(this.bOK_Click);
			// 
			// bCancel
			// 
			this.bCancel.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.bCancel.Location = new System.Drawing.Point(552, 368);
			this.bCancel.Name = "bCancel";
			this.bCancel.TabIndex = 9;
			this.bCancel.Text = "Cancel";
			this.bCancel.Click += new System.EventHandler(this.bCancel_Click);
			// 
			// bSave
			// 
			this.bSave.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.bSave.Location = new System.Drawing.Point(464, 368);
			this.bSave.Name = "bSave";
			this.bSave.TabIndex = 7;
			this.bSave.Text = "Save";
			this.bSave.Click += new System.EventHandler(this.bSave_Click);
			// 
			// SaveProfileForm
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(210)), ((System.Byte)(216)), ((System.Byte)(240)));
			this.ClientSize = new System.Drawing.Size(674, 424);
			this.Controls.Add(this.bSave);
			this.Controls.Add(this.bCancel);
			this.Controls.Add(this.bOK);
			this.Controls.Add(this.tbProfileDesc);
			this.Controls.Add(this.tbProfileName);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.lvProfiles);
			this.Controls.Add(this.label1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
			this.Name = "SaveProfileForm";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Import Wizard";
			this.Load += new System.EventHandler(this.SaveProfileForm_Load);
			this.ResumeLayout(false);

		}
		#endregion
		#region � Methods �
		#region � bOK_Click �
		private void bOK_Click(object sender, System.EventArgs e)
		{
			this.Close();
		}
		#endregion � bOK_Click �
		#region � bCancel_Click �
		private void bCancel_Click(object sender, System.EventArgs e)
		{
			this.Close();
		}
		#endregion � bCancel_Click �
		#region � SaveProfileForm_Load �
		private void SaveProfileForm_Load(object sender, System.EventArgs e)
		{
			int columnNumber = this.lvProfiles.Columns.Count;
			double dColumnWidht = this.lvProfiles.Width / columnNumber;
			int columnWidth = (int)dColumnWidht;
			foreach (ColumnHeader ch in this.lvProfiles.Columns)
			{
				ch.Width = columnWidth;
			}
			this.UpdateListView();
		}
		#endregion � SaveProfileForm_Load �
		#region � bSave_Click �
		private void bSave_Click(object sender, System.EventArgs e)
		{			
			string profileName = this.tbProfileName.Text;
			string profileDesc = this.tbProfileDesc.Text;

			if (0 == profileName.Length)
			{
				MessageBox.Show(UserMessages.EmptyProfileName);
				return;
			}
			
			string file = this.profilesPath + @"\" + profileName + Globals.ProfileExtention;
			if (File.Exists(file) )
			{
				if (DialogResult.Yes  != MessageBox.Show(this, UserMessages.OverwriteExistingFile, "Overwrite", MessageBoxButtons.YesNo, MessageBoxIcon.Question ))
				{
					this.tbProfileDesc.Text = string.Empty;
					this.tbProfileName.Text = string.Empty;
					return;				
				}
			}
			FileInfo fi = new FileInfo(file);
			
			// Create a writer, ready to add entries to the file.
			StreamWriter sw = fi.CreateText();
			sw.WriteLine(profileDesc);			
			
			IDictionaryEnumerator ide = Globals.htMappingInfo.GetEnumerator();
			while (ide.MoveNext())
			{
				MapperObjectInfo moi = (MapperObjectInfo)ide.Key;
				MapperSourceInfo msi = (MapperSourceInfo)ide.Value;

				sw.WriteLine(moi.PropertyName + Globals.TabSeparator + moi.PropertyDesc + Globals.TabSeparator + msi.ColumnIndex + Globals.TabSeparator + msi.ColumnDesc);
			}

			sw.WriteLine(Globals.MapperDelimiter);
			IDictionaryEnumerator idEnumerator = Globals.htFilters.GetEnumerator();
			while (idEnumerator.MoveNext())
			{
				IFilterStoreable ifs = idEnumerator.Value as IFilterStoreable;
				if (null != ifs)
				{
					ifs.StoreObject(sw);
				}
			}

			sw.Flush();			
			sw.Close();
			this.UpdateListView();
			this.Close();	
		}
		#endregion � bSave_Click �
		#region � UpdateListView �
		private void UpdateListView()
		{			
			this.lvProfiles.Items.Clear();
			if (false == Directory.Exists(this.profilesPath))
			{
				Directory.CreateDirectory(this.profilesPath);
			}
			string[] files = Directory.GetFiles(this.profilesPath);
			foreach (string s in files)
			{
				string desc = string.Empty;
				FileInfo fi = new FileInfo(s);

				using (StreamReader sr = fi.OpenText()) 
				{
					string line = string.Empty;
					while ((line = sr.ReadLine()) != null) 
					{						
						desc = line;						
						break;
					}
					sr.Close();
				}

				string name = string.Empty;
				int indexOfDir = s.LastIndexOf(@"\");
				if (indexOfDir > 0)
				{
					name = s.Substring(indexOfDir + 1);
				}
				name = name.Replace(Globals.ProfileExtention, string.Empty);


				ListViewItem lvi = new ListViewItem(name);
				lvi.SubItems[0].Text = name;
				lvi.SubItems.Add(desc);
				this.lvProfiles.Items.Add(lvi);
			}
		}
		#endregion � UpdateListView �
		#region � lvProfiles_SelectedIndexChanged �
		private void lvProfiles_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (null != this.lvProfiles.SelectedItems && this.lvProfiles.SelectedItems.Count > 0)
			{
				this.tbProfileName.Text = this.lvProfiles.SelectedItems[0].SubItems[0].Text;
				this.tbProfileDesc.Text = this.lvProfiles.SelectedItems[0].SubItems[1].Text;
			}
		}
		#endregion � lvProfiles_SelectedIndexChanged �
		#endregion � Methods �
	}
	#endregion � Class SaveProfileForm �
}
#endregion � namespace DealMaker �