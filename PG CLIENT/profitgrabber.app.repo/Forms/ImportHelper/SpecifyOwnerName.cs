#region � Using �
using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
#endregion � Using �
#region � Namespace DealMaker �
namespace DealMaker
{
	#region � Class SpecifyOwnerName �
	/// <summary>
	/// Summary description for SpecifyOwnerName.
	/// </summary>
	public class SpecifyOwnerName : System.Windows.Forms.Form
	{
		#region � Data �

		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.RadioButton rbNeighbor;
		private System.Windows.Forms.RadioButton rbHomeOwner;
		private System.Windows.Forms.Button bOK;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label3;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
		#endregion � Data �
		#region � CTR && Dispose �
		public SpecifyOwnerName()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}
		#endregion � CTR && Dispose �
		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label2 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.rbNeighbor = new System.Windows.Forms.RadioButton();
			this.rbHomeOwner = new System.Windows.Forms.RadioButton();
			this.bOK = new System.Windows.Forms.Button();
			this.label5 = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// label2
			// 
			this.label2.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.label2.Location = new System.Drawing.Point(16, 104);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(488, 23);
			this.label2.TabIndex = 1;
			this.label2.Text = "Instead program will automatically use \"dear homeowner\" on your MAILING LABELS.";
			// 
			// label4
			// 
			this.label4.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.label4.Location = new System.Drawing.Point(16, 128);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(304, 23);
			this.label4.TabIndex = 3;
			this.label4.Text = "Please check appropriate box below for your preference:";
			this.label4.Visible = false;
			// 
			// rbNeighbor
			// 
			this.rbNeighbor.Checked = true;
			this.rbNeighbor.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.rbNeighbor.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.rbNeighbor.Location = new System.Drawing.Point(80, 160);
			this.rbNeighbor.Name = "rbNeighbor";
			this.rbNeighbor.TabIndex = 4;
			this.rbNeighbor.TabStop = true;
			this.rbNeighbor.Text = "Dear neighbor";
			this.rbNeighbor.Visible = false;
			// 
			// rbHomeOwner
			// 
			this.rbHomeOwner.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.rbHomeOwner.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.rbHomeOwner.Location = new System.Drawing.Point(80, 192);
			this.rbHomeOwner.Name = "rbHomeOwner";
			this.rbHomeOwner.Size = new System.Drawing.Size(120, 24);
			this.rbHomeOwner.TabIndex = 5;
			this.rbHomeOwner.Text = "Dear homeowner";
			this.rbHomeOwner.Visible = false;
			// 
			// bOK
			// 
			this.bOK.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.bOK.Location = new System.Drawing.Point(485, 224);
			this.bOK.Name = "bOK";
			this.bOK.TabIndex = 6;
			this.bOK.Text = "OK";
			this.bOK.Click += new System.EventHandler(this.bOK_Click);
			// 
			// label5
			// 
			this.label5.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.label5.Location = new System.Drawing.Point(16, 40);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(304, 23);
			this.label5.TabIndex = 7;
			this.label5.Text = "Full Owner Name is not present in your mapping.";
			// 
			// label1
			// 
			this.label1.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.label1.Location = new System.Drawing.Point(40, 64);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(352, 23);
			this.label1.TabIndex = 8;
			this.label1.Text = "Also, there is neither First Name nor Last name present.";
			// 
			// label3
			// 
			this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label3.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.label3.Location = new System.Drawing.Point(16, 8);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(304, 23);
			this.label3.TabIndex = 9;
			this.label3.Text = "Full Owner Name can not be determined.";
			// 
			// SpecifyOwnerName
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(210)), ((System.Byte)(216)), ((System.Byte)(240)));
			this.ClientSize = new System.Drawing.Size(586, 272);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.bOK);
			this.Controls.Add(this.rbHomeOwner);
			this.Controls.Add(this.rbNeighbor);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.label2);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
			this.Name = "SpecifyOwnerName";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Specify Owner Name";
			this.ResumeLayout(false);

		}
		#endregion
		#region � Methods �
		#region � bOK_Click �
		private void bOK_Click(object sender, System.EventArgs e)
		{
			//if (true == this.rbHomeOwner.Checked)
				Globals.OwnerSpecification = OwnerName.Homeowner;
			//else
			//	Globals.OwnerSpecification = OwnerName.Neighbor;

			this.Close();
		}
		#endregion � bOK_Click �
		#endregion � Methods �
	}
	#endregion � Class SpecifyOwnerName �
}
#endregion � Namespace DealMaker �