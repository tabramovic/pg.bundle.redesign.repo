using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace DealMaker
{
	/// <summary>
	/// Summary description for SpecifyStateForm.
	/// </summary>
	public class SpecifyStateForm : System.Windows.Forms.Form
	{
		private bool bOKClosing = false;
		private string selectedState = string.Empty;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.ComboBox cbStates;
		private System.Windows.Forms.Button bOK;
		private System.Windows.Forms.Button bSkip;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public string SelecedState
		{
			get {return this.selectedState;}
			set {this.selectedState = value;}
		}
		
		public SpecifyStateForm()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			this.bOK.Click +=new EventHandler(bOK_Click);
			this.Closing += new CancelEventHandler(SpecifyStateForm_Closing);
			this.bSkip.Click += new EventHandler(bSkip_Click);
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.cbStates = new System.Windows.Forms.ComboBox();
			this.bOK = new System.Windows.Forms.Button();
			this.bSkip = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(8, 16);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(328, 23);
			this.label1.TabIndex = 0;
			this.label1.Text = "The STATE field is not assigned or present in your import file.";
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(8, 48);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(368, 32);
			this.label2.TabIndex = 1;
			this.label2.Text = "Select the STATE you want to use in your mailings for current import file:";
			// 
			// label3
			// 
			this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label3.Location = new System.Drawing.Point(8, 96);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(88, 23);
			this.label3.TabIndex = 2;
			this.label3.Text = ">>ATTENTION:";
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(104, 96);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(264, 40);
			this.label4.TabIndex = 3;
			this.label4.Text = "the State you select will be assigned to every record that is present in your imp" +
				"ort file!";
			// 
			// cbStates
			// 
			this.cbStates.Items.AddRange(new object[] {
														  "AL",
														  "AK",
														  "AZ",
														  "AR",
														  "CA",
														  "CO",
														  "CT",
														  "DE",
														  "FL",
														  "GA",
														  "HI",
														  "ID",
														  "IL",
														  "IN",
														  "IA",
														  "KS",
														  "KY",
														  "LA",
														  "ME",
														  "MD",
														  "MA",
														  "MI",
														  "MN",
														  "MS",
														  "MO",
														  "MT",
														  "NE",
														  "NV",
														  "NH",
														  "NJ",
														  "NM",
														  "NY",
														  "NC",
														  "ND",
														  "OH",
														  "OK",
														  "OR",
														  "PA",
														  "RI",
														  "SC",
														  "SD",
														  "TN",
														  "TX",
														  "UT",
														  "VT",
														  "VA",
														  "WA",
														  "WV",
														  "WI",
														  "WY"});
			this.cbStates.Location = new System.Drawing.Point(384, 48);
			this.cbStates.Name = "cbStates";
			this.cbStates.Size = new System.Drawing.Size(48, 21);
			this.cbStates.TabIndex = 4;
			this.cbStates.Text = "AL";
			// 
			// bOK
			// 
			this.bOK.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.bOK.Location = new System.Drawing.Point(360, 152);
			this.bOK.Name = "bOK";
			this.bOK.TabIndex = 5;
			this.bOK.Text = "OK";
			// 
			// bSkip
			// 
			this.bSkip.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.bSkip.Location = new System.Drawing.Point(272, 152);
			this.bSkip.Name = "bSkip";
			this.bSkip.TabIndex = 6;
			this.bSkip.Text = "Skip";
			// 
			// SpecifyStateForm
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(210)), ((System.Byte)(216)), ((System.Byte)(240)));
			this.ClientSize = new System.Drawing.Size(450, 184);
			this.Controls.Add(this.bSkip);
			this.Controls.Add(this.bOK);
			this.Controls.Add(this.cbStates);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
			this.Name = "SpecifyStateForm";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Info Missing";
			this.ResumeLayout(false);

		}
		#endregion

		private void SpecifyStateForm_Closing(object sender, CancelEventArgs e)
		{
			if (false == this.bOKClosing)
			{
				MessageBox.Show(this, "Please select state and exit mask with OK button!", "Select state", MessageBoxButtons.OK, MessageBoxIcon.Information);
				e.Cancel = true;
			}
		}

		private void bOK_Click(object sender, EventArgs e)
		{
			this.selectedState = this.cbStates.Text;
			this.bOKClosing = true;
			this.Close();
		}

		private void bSkip_Click(object sender, EventArgs e)
		{
			this.selectedState = string.Empty;
			this.bOKClosing = true;
			this.Close();
		}
	}
}
