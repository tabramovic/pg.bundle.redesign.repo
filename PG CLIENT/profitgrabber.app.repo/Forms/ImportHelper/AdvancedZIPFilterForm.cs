using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace DealMaker
{
	/// <summary>
	/// Summary description for AdvancedZIPFilterForm.
	/// </summary>
	public class AdvancedZIPFilterForm : System.Windows.Forms.Form
	{
		private ArrayList alZIPs = new ArrayList();
		private System.Windows.Forms.ColumnHeader columnHeader1;
		private System.Windows.Forms.Button bAddNew;
		private System.Windows.Forms.Button bRemove;
		private System.Windows.Forms.Button bRemoveAll;
		private System.Windows.Forms.Button bClose;
		private System.Windows.Forms.ListView lvZIP;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public AdvancedZIPFilterForm()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.lvZIP = new System.Windows.Forms.ListView();
			this.columnHeader1 = new System.Windows.Forms.ColumnHeader();
			this.bAddNew = new System.Windows.Forms.Button();
			this.bRemove = new System.Windows.Forms.Button();
			this.bRemoveAll = new System.Windows.Forms.Button();
			this.bClose = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// lvZIP
			// 
			this.lvZIP.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
																					this.columnHeader1});
			this.lvZIP.FullRowSelect = true;
			this.lvZIP.GridLines = true;
			this.lvZIP.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
			this.lvZIP.Location = new System.Drawing.Point(16, 16);
			this.lvZIP.MultiSelect = false;
			this.lvZIP.Name = "lvZIP";
			this.lvZIP.Size = new System.Drawing.Size(120, 176);
			this.lvZIP.TabIndex = 0;
			this.lvZIP.View = System.Windows.Forms.View.Details;
			// 
			// columnHeader1
			// 
			this.columnHeader1.Text = "ZIP";
			this.columnHeader1.Width = 115;
			// 
			// bAddNew
			// 
			this.bAddNew.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.bAddNew.Location = new System.Drawing.Point(152, 16);
			this.bAddNew.Name = "bAddNew";
			this.bAddNew.TabIndex = 1;
			this.bAddNew.Text = "Add New";
			this.bAddNew.Click += new System.EventHandler(this.bAddNew_Click);
			// 
			// bRemove
			// 
			this.bRemove.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.bRemove.Location = new System.Drawing.Point(152, 48);
			this.bRemove.Name = "bRemove";
			this.bRemove.TabIndex = 2;
			this.bRemove.Text = "Remove";
			this.bRemove.Click += new System.EventHandler(this.bRemove_Click);
			// 
			// bRemoveAll
			// 
			this.bRemoveAll.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.bRemoveAll.Location = new System.Drawing.Point(152, 80);
			this.bRemoveAll.Name = "bRemoveAll";
			this.bRemoveAll.TabIndex = 3;
			this.bRemoveAll.Text = "Remove All";
			this.bRemoveAll.Click += new System.EventHandler(this.bRemoveAll_Click);
			// 
			// bClose
			// 
			this.bClose.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.bClose.Location = new System.Drawing.Point(152, 168);
			this.bClose.Name = "bClose";
			this.bClose.TabIndex = 4;
			this.bClose.Text = "Close";
			this.bClose.Click += new System.EventHandler(this.bClose_Click);
			// 
			// AdvancedZIPFilterForm
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(210)), ((System.Byte)(216)), ((System.Byte)(240)));
			this.ClientSize = new System.Drawing.Size(242, 208);
			this.Controls.Add(this.bClose);
			this.Controls.Add(this.bRemoveAll);
			this.Controls.Add(this.bRemove);
			this.Controls.Add(this.bAddNew);
			this.Controls.Add(this.lvZIP);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
			this.Name = "AdvancedZIPFilterForm";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Advanced ZIP Filter";
			this.ResumeLayout(false);

		}
		#endregion

		public ArrayList ZIPs
		{			
			get 
			{
				return this.alZIPs;
			}
		}

		private void bClose_Click(object sender, System.EventArgs e)
		{
			this.alZIPs = new ArrayList();
			if (null != this.lvZIP.Items && 0 != this.lvZIP.Items.Count)
			{
				for (int i = 0; i < this.lvZIP.Items.Count; i++)
				{
					this.alZIPs.Add(this.lvZIP.Items[i].Text);
				}
			}

			this.Close();
		}

		private void bRemoveAll_Click(object sender, System.EventArgs e)
		{
			this.lvZIP.Items.Clear();
		}

		private void bRemove_Click(object sender, System.EventArgs e)
		{
			if (null != this.lvZIP.SelectedIndices && 0 != this.lvZIP.SelectedIndices.Count)
			{
				int idx = this.lvZIP.SelectedIndices[0];
				this.lvZIP.Items.RemoveAt(idx);
			}
		}

		private void bAddNew_Click(object sender, System.EventArgs e)
		{
			AddNewZIPForm anzf = new AddNewZIPForm();
			anzf.ShowDialog();

			if (true == anzf.Valid)
			{
				ListViewItem lvi = new ListViewItem(anzf.ZIP);
				this.lvZIP.Items.Add(lvi);
			}
		}
	}
}
