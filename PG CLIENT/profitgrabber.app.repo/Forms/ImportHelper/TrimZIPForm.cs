using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace DealMaker
{
	/// <summary>
	/// Summary description for TrimZIPForm.
	/// </summary>
	public class TrimZIPForm : System.Windows.Forms.Form
	{		
		private System.Windows.Forms.CheckBox _cbTrim;
		private System.Windows.Forms.Button _bOK;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public TrimZIPForm()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this._cbTrim = new System.Windows.Forms.CheckBox();
			this._bOK = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// _cbTrim
			// 
			this._cbTrim.Checked = true;
			this._cbTrim.CheckState = System.Windows.Forms.CheckState.Checked;
			this._cbTrim.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this._cbTrim.Location = new System.Drawing.Point(16, 16);
			this._cbTrim.Name = "_cbTrim";
			this._cbTrim.Size = new System.Drawing.Size(192, 24);
			this._cbTrim.TabIndex = 0;
			this._cbTrim.Text = "Trim ZIP code (use only 5 digits)";
			// 
			// _bOK
			// 
			this._bOK.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this._bOK.Location = new System.Drawing.Point(136, 56);
			this._bOK.Name = "_bOK";
			this._bOK.TabIndex = 1;
			this._bOK.Text = "OK";
			this._bOK.Click += new System.EventHandler(this._bOK_Click);
			// 
			// TrimZIPForm
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(210)), ((System.Byte)(216)), ((System.Byte)(240)));
			this.ClientSize = new System.Drawing.Size(226, 88);
			this.Controls.Add(this._bOK);
			this.Controls.Add(this._cbTrim);
			this.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
			this.Name = "TrimZIPForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "ZIP Code Additional Info";
			this.ResumeLayout(false);

		}
		#endregion

		public bool Trim 
		{
			get { return _cbTrim.Checked; }
		}

		private void _bOK_Click(object sender, System.EventArgs e)
		{
			this.Close();
		}
	}
}
