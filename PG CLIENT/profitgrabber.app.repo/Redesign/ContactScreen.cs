﻿using System;
using System.Drawing;

namespace DealMaker
{
    public partial class ContactScreen
    {
        private readonly Color BGColor = Color.FromArgb(210, 216, 240);
        //redesign        
        private void AddRedesignEventHandlers()
        {
            AddRedesignContactInfoEventHandlers();
        }

        private void AddRedesignContactInfoEventHandlers()
        {
            //_ci_lk_panel            
            tbMailLastKnownAdd.GotFocus += OnCiLKPanel_Activate;
            tbMailLastKnownCity.GotFocus += OnCiLKPanel_Activate;
            tbMailLastKnownState.GotFocus += OnCiLKPanel_Activate;
            tbMailLastKnownCityState.GotFocus += OnCiLKPanel_Activate;
            tbMailLastKnownZIP.GotFocus += OnCiLKPanel_Activate;

            //_ci_previous_panel
            tbMailPreviousAdd.GotFocus += OnCiPreviousPanel_Activate;
            tbMailPreviousCity.GotFocus += OnCiPreviousPanel_Activate;
            tbMailPreviousState.GotFocus += OnCiPreviousPanel_Activate;
            tbMailPreviousCityState.GotFocus += OnCiPreviousPanel_Activate;
            tbMailPreviousZIP.GotFocus += OnCiPreviousPanel_Activate;

            //_ci_old_panel
            tbMailOldAdd.GotFocus += OnCiOldPanel_Activate;
            tbMailOldCity.GotFocus += OnCiOldPanel_Activate;
            tbMailOldState.GotFocus += OnCiOldPanel_Activate;
            tbMailOldZIP.GotFocus += OnCiOldPanel_Activate;
        }

        private void TbMailLastKnownAdd_GotFocus(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        bool ciLKPanel_IsActive = false;
        private void OnCiLKPanel_Activate(object sender, EventArgs e)
        {
            ciLKPanel_IsActive = true;
            _ci_lk_panel.BackColor = tbMailLastKnownAdd.BackColor = tbMailLastKnownCity.BackColor = tbMailLastKnownState.BackColor = tbMailLastKnownZIP.BackColor = Color.White;
            OnCiPreviousPanel_DeActivate(sender, e);
            OnCiOldPanel_DeActivate(sender, e);            
        }

        private void OnCiLKPanel_DeActivate(object sender, EventArgs e)
        {
            if (!ciLKPanel_IsActive)
                return;

            ciLKPanel_IsActive = false;
            _ci_lk_panel.BackColor = tbMailLastKnownAdd.BackColor = tbMailLastKnownCity.BackColor = tbMailLastKnownState.BackColor = tbMailLastKnownZIP.BackColor = BGColor;
        }

        bool ciPreviousPanel_IsActive = false;
        private void OnCiPreviousPanel_Activate(object sender, EventArgs e)
        {
            ciPreviousPanel_IsActive = true;
            _ci_previous_panel.BackColor = tbMailPreviousAdd.BackColor = tbMailPreviousCity.BackColor = tbMailPreviousState.BackColor = tbMailPreviousZIP.BackColor = Color.White;

            OnCiLKPanel_DeActivate(sender, e);            
            OnCiOldPanel_DeActivate(sender, e);
        }

        private void OnCiPreviousPanel_DeActivate(object sender, EventArgs e)
        {
            if (!ciPreviousPanel_IsActive)
                return;

            ciPreviousPanel_IsActive = false;
            _ci_previous_panel.BackColor = tbMailPreviousAdd.BackColor = tbMailPreviousCity.BackColor = tbMailPreviousState.BackColor = tbMailPreviousZIP.BackColor = BGColor;
        }

        bool CiOldPanel_IsActive = false;
        private void OnCiOldPanel_Activate(object sender, EventArgs e)
        {
            CiOldPanel_IsActive = true;
            _ci_old_panel.BackColor = tbMailOldAdd.BackColor = tbMailOldCity.BackColor = tbMailOldState.BackColor = tbMailOldZIP.BackColor = Color.White;

            OnCiLKPanel_DeActivate(sender, e);
            OnCiPreviousPanel_DeActivate(sender, e);            
        }

        private void OnCiOldPanel_DeActivate(object sender, EventArgs e)
        {
            if (!CiOldPanel_IsActive)
                return;
            
            CiOldPanel_IsActive = false;
            _ci_old_panel.BackColor = tbMailOldAdd.BackColor = tbMailOldCity.BackColor = tbMailOldState.BackColor = tbMailOldZIP.BackColor = BGColor;
        }
    }
}
