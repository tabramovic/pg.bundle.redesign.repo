using System.Reflection;
using System.Runtime.CompilerServices;

//
// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
//
[assembly: AssemblyTitle("DealMaker")]
[assembly: AssemblyDescription("DealMaker")]
[assembly: AssemblyConfiguration("")]
//[assembly: AssemblyCompany("Turn Key Systems Inc")]
[assembly: AssemblyCompany("Real Estate Money, LLC.")]
//[assembly: AssemblyProduct("ProfitGrabber Pro")]
[assembly: AssemblyProduct("ProfitGrabber Pro - Network Version")]
[assembly: AssemblyCopyright("Real Estate Money LLC")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

//
// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Revision and Build Numbers 
// by using the '*' as shown below:

//master branch - 

[assembly: AssemblyVersion("8.5.0.5")]
//8.5.0.5
//              - shortened [Get Skip Traced Records] GUI form 
//8.5.0.4
//              - removed zip exclusion,
//              - fixed matching module [remove multiple names] feature,
//              - [delete tagged] button is always enabled (when checking force separation checkbox)
//              - fixed assign to group feature for non-matched records
//8.5.0.3
//              - Best time to call fix
//8.5.0.2
//              - FullName fix
//8.5.0.1
//              - Cosmetic fixes
//8.5.0.0
//              - Cosmetic fixes + Matching Module Step B automatic select all on tab change
//8.9.4.9
//              - post RC fixes (several emails, cosmetics mostly)
//8.4.9.8
//              - RC 3 2021 DealFinder cosmetics
//8.4.9.7
//              - RC2 2021 (DealFinder360 Import process added step)
//8.4.9.6
//              - RC1 2021
//8.4.9.5
//              - RC 2021
//8.4.9.4
//              - Append Data Worker and Append Data Form fixes
//8.4.9.3
//              - import and export supporting new SkipTrace lables, additional text changes
//8.4.9.2
//              - bugfixes
//8.4.9.1
//              - [Remove multiple first names] support in StepB Matching Module
//8.4.9.0
//              - Merge contact now works on Contact Info, Deal Info and SkipTraced Info
//              - Possibility to create skiptraced subgroups and assign data to Skiptraced child groups from parent group
//8.4.8.9
//              - Skip trace all records, including those previously skip traced.
//8.4.8.8
//              - Support for lastUsedGoogleAccount in Export to Google Drive feature.
//8.4.8.7
//              - Export feature and GUI updates
//8.4.8.6
//              - Enhanced [Export to Google Drive] features; added 3 email address to use for mail marketing
//8.4.8.5 
//              - always enabled add skip trace credit button;
//              - setup email - removed replyTo address;
//              - defauted to address-only in scan-for-duplicates
//              - added copy from ST address to ContactInfo + push down already existing data;
//8.4.8.4
//          - implemented all high priority features:
//              - Add 3 skip traced Mobile and 2 skip traced residential to Import and Export Functions
//              - Add 3 Skip Traced Email to import and Export functions
//              - Add text to Contact Info tab, Skip Traced Results. In Red text.  "This is just a partial list, to see the full skip traced info, go to the Skip Traced info Tab"
//              - Remove the time stamp in the name for the export to google drive.  Let the user define the name.
//              - Allow the user to choose a group location for Deal Finder Import
//              - Add a right click menu item on group.  Under Delete Group, called Move Group.  Allow the user to move the group to a new location.  It needs to maintain assinged tasks and task history.
//              - Add a right click menu under "Move Group" called "Combine Groups" to copy all contacts from this group to another group. Reassign all tasks using tasks assigned to the destination.  Add appropriate text description of what will happen.
//8.2.4.3
//          - Matching module upgrade: Match contact that already have names and mailing addresses filter added.
//8.4.8.2
//          - MatchingModule previous counters fix
//8.4.8.1
//          - Use skiptrace module as PG client with Server's keycode
//8.4.8.0
//          - Upload to DC: do now allow upload of empty cell phone numbers
//8.4.7.9
//          - Cleaning SkipTraced control content upon new contact load
//          - Hiding [SkipTrace] button upon insufficient funds
//          - Save skip traced info for records that do not have phone and emails mathed
//8.4.7.8
//          - Single append data request GUI fix
//8.4.7.7
//          - GFR support for new DF360 records (with missing data)
//          - Applied counter (match/total processed) to GUI and workers
//          - Altered GUI text per request
//          - client side change (skip trace match cost)
//          - Added "ST Contact Info Not Found" group as skip trace result subgroup
//8.4.7.6
//          - Kit's request GUI update, DealFinder360 Import model update
//8.4.7.5
//          - Upload to DropCowboy skip traced phone numbers
//8.4.7.4
//          - IDI GUI redesign
//8.4.7.3
//          - IDI Relatives tab
//8.4.7.2
//          - IDI ContactInfo and OtherProperties tabs
//8.4.7.1
//          - integratrion with IDI skip trace provider
//8.4.7.0
//          - google auth combined scopes (/auth/gmail.send, /auth/spreadsheets)
//8.4.6.9
//          - google certificates swap needed for PG certification
//8.4.6.8
//          - google auth fix
//8.4.6.7
//          - TaskManager Calendar support for 2030
//          - DoorKnocker bugfix for only 1 item in list
//          - Export to Google Sheets - integrated credentials into code
//8.4.6.6 -
//          - removed getDefaultBrowser usage. now using Process.Start(url)
//8.4.6.5 -
//          - DealMaker360 - Supported customer defined group names. Crossreferenced with already existing names and not allowing duplicates
//8.4.6.4 -
//          - Split DealMaker360 imported files into multiple groups, set per maxContactsPerGroup parameter
//          - additional cosmetics
//8.4.6.3 -
//          - Replacing RealEFlow with DealMaker360
//8.4.6.2 -
//          - Export to Google Sheets
//8.4.6.1 -
//          - DealFinder360 custom import from zip file
//8.4.6.0 -
//          - cosmetic improvements
//8.4.5.9 -
//          - cosmetic improvements
//          - altered MFW Step 5 GUI (send test email)
//          - altered EmailSetupV2 form
//8.4.5.8 -
//          - setup new .pfx certificate into build process
//          - improved case where sending failed due to gmail oauth fail - PG will display message and stop trying to send emails
//          - disabled [DisplayName] and [ReplyTo] fields in [Email�LOI] and [Email Blast] tabs. 
//          - enhanced MFW dialog - now it supports sending test email from last step:
//8.4.5.7 - 
//          - support for loading skiptraced email when sending emails from task manager
////          - cosmetic MFW improvemengmts
//8.4.5.6 -
//          - cosmetic improvements,
//          - fix for sending emails from task manager
//8.4.5.5 -
//          - google.oauth.lib integration support for changing sending accounts
//8.4.5.4 -
//          - google.oauth.lib integration for test sending emails, sending emails per request from task manager and scheduled sending emails related to tasks
//8.4.5.3 -
//          - DropCowboy GUI iteration
//8.4.5.2 - 
//          - DropCowboy GUI iteration
//8.4.5.1 -
//          - DropCowboy GUI iteration
//8.4.5.0 -
//          - DropCowboy GUI customizations
//          - Skip Trace warning customizations
//8.4.4.9 -
//          - DropCowboy refresh list count after contact uploadswaklfcjn;fc;o/qahgwf ihl 
//8.4.4.8 -
//          - DropCowboy total number of assigned contacts response upgrade
//          - Datazapp - prior evoking the service, another call is made to check balance and estimate funds needed.
//8.4.4.7 - 
//          - DropCowboy Ringless Voice Drop & Text Message Service provider integrated.
//8.4.4.6 -
//          - Datazapp response model update
//8.4.4.5 -
//          - Skip trace service provider ListGiant replaced with Datazapp
//8.4.4.4 -
//          - Improved "search by skiptraced phone numbers" feature.
//8.4.4.3 - 
//          - Added OwnerWorkPhone and OwnerCellPhone to Import
//8.4.4.2 -
//          - Ability to import all phone numbers and email (added Skip Traced Email, Skip Traced Cellpone, Skip Traced Landline to import mapping).
//          - Ability to export all phone numbers�and email (added Skip Traced Email, Skip Traced Cellphone, Skip Traced Lanline to export mapping).
//8.4.4.1-
//          - Find contact using skip traced land line and cell phone
//          - Find contact using email or skip traced email.
//8.4.4.0-
//          - Added address merge into Stratics SMS campaigns
//8.4.3.9 -
//          - SQL stored procedure fix for "Unable to Load objects!" message that appeared when user deleted group which had assigned tasks to it which were part of the activities that belonged to other groups.
//8.4.3.8 - 
//          - Improved door-knocking module (handled apartment units)
//          - Added Activity fix to list of fixes
//          - Added support for sending SMS over Stratics
//8.4.3.7 - 
//          - Stratics altered texts, added starting default browser pointing to stratics website
//8.4.3.6 -
//          - Stratics RingVoiceMessages
//8.4.3.5 -
//          - added total bedroom count into into COMPS with Maps reports
//8.4.3.4 -
//          - added total bedroom count into COMPS and PP reports
//8.4.3.3 -
//          - added additional check form before Skip Trace process takes place
//          - added Email Blast and Email LOI signatures (load, edit, save, apply)
//          - added script to alter column length Task.Body to max 4000 characters for longer document templates
//8.4.3.2 -
//          - added createion of "Email Found" and "Phone Found" subgroups when skip-tracing group or individual contact
//          - added Custom Email Footer - new Form to setup it and check boxes to both Email LOI and Email Blast tabs
//8.4.3.1 -
//          - added support for old .tx templates (Address to CompanyAddress merge field)
//8.4.3.0 -
//          - added support for downloading .tx template (LOI EMail .tx template)
//8.4.2.9 - 
//          - added fix for Mail-Email-Fax-Wizard when SMTP settings are blank (null)
//8.4.2.8 -
//          - cosmetic changes, bugfix for sending email when using PG AWS email service without attachment.
//8.4.2.7 -
//          - pure cosmetic changes - added hovers and labels with more explanation.
//8.4.2.6 -
//          - supported MFW email blast text templates as part of embedded resources
//          - fixed .xlsd content type
//8.4.2.5 -
//          - supported sending predefined text templates and static attachments (.pdf, .xls, .xlsx, .doc, .docx, .jpg, .png)
//          
//8.4.2.4 -
//          - supported send plain text in email blast (without attachement)
//8.4.2.3 -
//          - explicit bringToFront forced to AssignActivity Step3
//          - fixed tabs on DoorKnocker module
//8.4.2.2 - 
//          - fixed "Marko's activity bug"
//          - finalized SkipTrace links
//          - cosmetics - seminar RC 1    
//8.4.2.1 - Integrated Transaction Server on Client Side for Data-Append (email, landline, cell phone)
//8.4.2.0 -
//Added GetSkipTraced Record in Contact View - Adjusted Mask, BLL, base mask, on-the-fly populate, autosave
//8.4.1.9 -
//Added STLastModified polje (DB, DALC, GUI)
//Added SkipTraceForm
//cosmetics
//8.4.1.8 - 
//Added STEmail, STCellPhone and STLandline properties into Contact Table, DALC and integrated into ContactView as SkipTraced records
//Altered Data Append feature to use Contact email as first choice and if empty, then use Skip Trace Email (which mail fax wizard will use)
//Added OnSkipped event handler to data append service for records that are missing some crucial data (name or address or zip)
//Adjusted Duplicate Search so that it merges skiptraced data too
//Added from-to range to send email from MFW
//8.4.1.7 - 
//Updated MailFaxWizardPrintFaxForm (added DoEvents, form became responsive again)
//removed and added eventhandler on MailFaxWizard to speed-up long list loading    
//8.4.1.6 -
//Added 3 seconds delay time between sending emails
//Added "Insert Page Break"
//Integrated new LOI-ForEmail.tx template
//For both Email-LOI and Email Blast:
//altered prefix and body, append checkbox - checked by default
//Double checked spaces in email body(that is gmail processing, PG sends it out correctly)
//Door knocker - integrated google maps with optimized routing and continuous mode, 
//added filtering per ZIPs:
//added "Use Company Address as Starting Address":
//year built - now supports up to 2019
//data append service
//added marketing & direct number(now supports old.tx templates)
//added email1, email2, buying and foreclosure website - both fields and mapping
//google maps on Contact menu
//Get Instant Data(email, landline and cell phone) - menu item in ProfitBoosters menu(adds credits to data append service.)
//8.4.1.5 - Added following fields to UserData:
//          - altered table (added columns)
//          - added those fields into User Data Mapping Items in edit-document-template-user-control
//8.4.1.4 - Added google maps routing
//8.4.1.3 - Added Email to UserInformation, supported its merge in template editor
//8.4.1.2 - Integrated EMAIL blast AGENT execution into Task Manager
//          - cosmetic change (mail / fax >> mail / email / fax)
//          - supported preview button for email blast
//          - supported delete from group when belonging to status group only
//8.4.1.1 - Integrated EMAIL blast LOI execution into Task Manager:
//          - created script that expands DB for email related fields that need save 
//8.4.1.0 - Use Own Email (for Email Blast) will send email directly from PG (as for tasks)
//          - Assign Task & Activity now uses Email Blast tabs
//          - Assign Task - Step 2 form is now centered parent
//          - Appedn Data does NOT use first / last name due to better results
//          - File >> Email Setup >> Vies Sent Emails got Refresh button
//          - Updated current and added new EH for succ. and unsucc posting email to service 
//8.4.0.9 - Export wizard (fixed null reference exception bug), Routing Form (optimized listiview rendering)
//8.4.0.8 - door knocking module - stretched dropdown box
//8.4.0.7 - phone append - added altering label to close button (stop to close upon finished processing)
//8.4.0.6 - phone append - request all data append (cell, landline and email)
//8.4.0.5 - phone append + cell, landline and email data append
//8.4.0.4 - phone append
//8.4.0.3 - MFW removed checkboxes, now tab selection marks active selection. Added back feature (remembers previous selection).
//8.4.0.2 - MFW step4 - cosmetic changes
//8.4.0.1 - Contact Info - Imported Data - listing agent: following fields were read only, and now are editable + autosave: tb_II_LI_ListingMLSNumber, tb_II_LI_ListingOffice, tb_II_LI_LA_FirstName, tb_II_LI_LA_LastName, tb_II_LI_LA_AgentCode, tb_II_LI_LA_PhoneNumber
//8.4.0.0 - Email Blast feature
//8.3.6.5 - EditDocumentMailFaxTemplate - new cosmetic changes in ctx menu (added Cut feature and Ctrl + _ shortcuts, altered menu items order), added CheckForSave method for New and Exit ProfitgrabberFeatures.
//8.3.6.4 - EditDocumentMailFaxTemplate - cosmetic changes in ctx menu, prepopulate document name in open/save file dialogs, show document name in Form.Text (hide when control is not visible), support save if document is altered for both template and regular document.
//8.3.6.3 - bugfix (SSX Edit Document Template Context Menu support)
//8.3.6.2 - added reading from .config file for beta support
//8.3.6.1 - bugfix (property profile control clearForm didn't clear textboxes in groupbox)
//8.3.6.0 - added support for routing
//8.3.5.0 - dotNet Framework 4.0, new txTextControl
//        - added Globals.DatabasePwd 
//8.3.5.0 - Insert SqFt Manually into the COMPS
//8.3.4.0 - removed notification about previously loaded contact being changed :: added support for "other application has modified contact you are working on": removed event handler from AssignTaskStep2.cs, Dispose method so the messagebox doesn't show
//8.3.3.0 - added Find Duplicates support among old and new imported records (customer support request) 
//        - added Matching Module support for disconnect on DataProvider's end
//8.3.2.0 - GFR User Fields support
//8.3.1.0 - Print COMPS with Maps - saving to PDF or XPS support
//8.3.0.0 - EditDocumentTemplateUserControl bugfix (PropertyItems merge field), ContactView bugfix (when group does not contain any contacts)
//8.2.9.0 - Added support for LOI save to PDF
//8.2.8.0 - Fixed Import: IIO, ProperCase method - added TrimEnd(); COMPS radius - removed all entries greater than 10 miles; BuyerInfoForm - bugfix;
//8.2.7.0 - Merge Fields for CB/PL, Kit's duplicate search, Matching Module (separation updates full name), Print COMPS WITH MAPS, Upload Photo on Contact View
//8.2.6.0 - support for single running instance (if another instance is already running, bring it to front).
//8.2.5.0 - supporting lastName grouping for CB: i.e.fullName: JOHN and SAMANTHA Q MARSHALL, firstName: JOHN and SAMANTHA Q, lastName: MARSHALL
//8.2.4.0 - supported name splitting for CB / PL (*if 2 words and 2nd word is not (LLC or INC, TRUST or CORP) >> then split into firstName, lastName  || * if 3 words and 3rd word is not (LLC or INC) >> then split into firstName, middleName, lastName)
//8.2.3.0 - Auto Select Groups (Seller, Buyer, Lender)
//8.2.2.0 - Private Lenders Group (plural form)
//8.2.1.0 - Import negative data support
//8.2.0.0 - New ContactTypes (Buyer and Lender along the present Seller)
//8.1.8.2 - bugfix (multithreading)
//8.1.8.1 - added sending tasks to staff who doesn't have auto send time included, re-arranged task manager GUI for 125% font size
//8.1.8.0 - adding special handling for Cancelled tasks
//8.1.7.0 - sending task to email: introduced async sending mails from queue, added mail processor console, added caching user info data, 
//          altered taskManager behaviour related to sending task2email, altered user messages, removed certain features (per request),
//          removed seconds from daily send time
//8.1.6.0 - auto send bugfix
//8.1.5.0 - sending task to email: removed -SELECT- label, and added AM/PM back
//8.1.4.0 - sending task to email feature small cosmetic fixes + bugfix
//8.1.3.0 - sending task to email feature small cosmetic fixes
//8.1.2.0 - sending task to email feature now supports sending ownerName & siteAddress
//8.1.1.0 - sending task to email feature now supports sending past due tasks
//8.1.0.0 - Add dev for sending task to email feature
//8.0.0.0 - Wrapped up version, recompiled to .net framework 3.5
//6.6.0.0 - Finished additional requirements for Task2Email feature + bugfixes.
//6.5.5.0 - Finished Task2Mail feature. This contains altered NegotiationControl.cs - On_browseLogFile_Click && IGNORE_LOCKS feature specially for Kit.
//6.5.4.0 - Added System.Web.dll and supported Encoding URLs needed for YAHOO maps
//6.5.3.3 - Changed Video training link to http://www.profitgrabber.com/training
//6.5.3.2 - VCPL lead integration *VCPL = VoiceConnect & PatLive)
//6.5.3.1 - AAH, NPC and ACOK lead integration - tweaks and fixes
//6.5.3.0 - AAH, NPC and ACOK lead integration
//6.5.2.0 - AAH web service and AAH Sync full download introduced 
//6.5.1.0 - EditDocumentTemplateUserControl - PrintDirect optimization
//6.5.0.0 - ContactInfo - fixed saving image, Picture manager - changed image type to from gif to jpg for better quality
//6.4.9.9 - get default browser feature
//6.4.9.8 - ignore locks - feature requested by Kit, see NetworkLock.cs, #define at the top.
//6.4.9.7 - MM bugfix
//6.4.9.6 - NwConversionLog sometimes got placed onto desktop. Fix supplied.
//6.4.9.5 - TxTextTemplate control - fixes & improvements:
//              1. fixed pdf document naming, 
//              2. PageSetup displays correct paper size/orientation after you change it and show pagesettings again,
//              3. New document resets your current paper size, margins & landscape settings, creating new Letter - portrait with .79 margins for left, right, top and bottom          
//6.4.9.4 - TxTextTemplate contol - support various page formats
//6.4.9.3 - Added Kit's request - repositioning open and assing buttons in NegotiationControl (aspect of SSX)
//6.4.9.2 - Added custom merge fields (SaleDate, Date, Date (Rich Format))
//6.4.9.1 - Kit's SSX change requests incorporated into Custom SSX settings related to assign & open log folder
//6.4.9.0 - Kit's SSX change requests
//6.4.8.0 - SSX User Objects Count fix
//6.4.7.0 - Export to PDF bugfix - disabled for small, medium and large labels
//6.4.6.0 - Export to PDF integrated into MFW's last step
//6.4.5.0 - separate firstName bugfix for single word as fullName
//6.4.4.0 - separate firstName from fullName (integrated into import procedure and MM step2)
//6.2.3.0 - Repairs control bugfix
//6.4.2.0 - Update Kit's objects hidden, LeadSource bugfix
//6.4.1.0 - custom Kit's changes (upgrading old ssx model to latest one)
//6.4.0.0 - Duplicate checker: keep firstEntered vs. lastModified bugfix
//6.3.9.0 - export COMPS feature re-integrated
//6.3.8.0 - bugfixing, vol 2 (COMPS's dateOfRetrieve wasn't deserialized into object when loaded from DB, so sequent updates to the same object failed) File: comps.cs, Ln.:309, this.dateOfRetrieve = cps.dateOfRetrieve;
//6.3.7.0 - bugfixing, vol 1
//6.3.6.0 - Moved to alternative web services methods!
//6.3.5.0 - Added Address Matcher (St. = Str = Street, Boulevard, Avenue support)
//6.3.4.0 - Reiko Tamura support - multiple owners (splitted by / )
//6.3.3.0 - added MessageBox in select database form, added printerChooser in MFW (txTextControl)
//6.3.2.0 - unzip lib, automatic start of PGDatabaseSelectService
//6.3.1.0 - PrinterChooser (PrintDialog) in MFW
//6.3.0.0 - Automatic Conversion from PLAIN to NW
//6.2.0.0 - MM bugfixing (invokeRequired), separateFirstName (force acpect of stepB)
//6.1.0.0 - MOD10 - internal version (no zip filter)
//6.0.0.0 - RC1
//5.8.6.0 - cosmetics (About form, splash screen)
//5.8.5.0 - small fixes (RMW, DefaultInfoValues)
//5.8.4.0 - SSX enhancements as per Kit's document requested
//5.8.3.0 - fax debugging
//5.8.2.0 - fax debugging
//5.8.1.0 - minor bugfixes: tooltip on main form
//5.8.0.0 - Lien Control supports assigning tasks
//5.7.2.0 - PrintPreviewDialogEx supports printer choose
//5.7.1.0 - EditDocumentTemlateUserControl - save as PDF
//        - MatchingModule Step 2, force splitting names feature
//        - TaskManager Open Contact button regrouped
//        - CustomSettings - Quick Launch skips splashscreen
//5.7.0.0 - RC1
//5.6.6.0 - RMW fix for duplicate scanned records
//5.6.5.0 - BuyerInfoOverview fix - BuyerInfoOverview.cs, ln.174, 175, 
//          BuyerInfoForm: GetData() SelectedItem replaced with Text property
//                         SetData() _spokeWith.SelectedIndex = 0; replaced with _spokeWith.Text = bid.SpokeWith;
//5.5.0.0 - Auto upgrade database, RMW bugfix, PropertyProfile Sale date fix, Alpha Offer Price, OffePrice editable
//5.3.2.0 - support for DB fixes, dummy screen for ssx when disabled
//5.3.1.0 - Integration: a.) OwnerPropertyAddress in TxTemplates, 
//                       b.) Autosave for Evaluate tab, ssx and buyers tab
//                       c.) Comps search criteria save & reload
//                       d.) Custom HUD integration
//5.3.0.0 - TxTextControl Ver 15
//5.2.0.0 - Assign Task Step2 - EditMode - custom implementation for Kit
//5.1.1.0 - Create shortsale dalc upon start bugfix
//5.1.0.0 - Added Code for expanding TaskTable, into ApplicationHelper (void AlterTable_ExpandTaskTable())
//5.0.0.0 - HOTFIX FOR 123SOLD time zones. Applied new webservice http://www.profitgrabber.com/ws2Test/Service1.asmx and integrated Ticks into web service and OneTwoThreeSold.cs
//4.9.2.0 - Custom Fix for Kit - see AssignTaskStep2.cs, Ln. 300
//4.9.1.0 - Edit Grid Objects (//sasa quick fix - grid now uses IComparable for default sort 19.11.09) - wrapped with try catch since it fails
//4.9.0.0 - Task Priority (DB, DALC, Presentation Layer Change)
//4.8.1.0 - Open contact from task manager
//4.8.0.0 - Add Lien bugfix
//4.7.2.0 - Additional Impl.
//4.7.1.0 - BugFixing
//4.7.0.0 - SSX altered specs by Kit
//4.6.4.0 - SSX altered specs by Kit
//4.6.3.0 - Media bugfix
//4.6.2.0 - Commercials 
//4.6.1.1 - bugfixing
//4.6.1.0 - added datalayer creator for officestafflist and leadsource list
//4.6.0.2 - bugfixing
//4.6.0.1 - bugfixing
//4.6.0.0 - Integrated SSX along with scheduler features
//4.5.2.2 - UNABLE TO LOAD OBJECTS Bugfix (See selectactivity.cs, Ln. 314)
//4.5.2.1 - RELEASE LOCK TESTING
//4.5.2.0 - MFW - fixed bug on tag recipiens MailFaxWizardForm3.cs (added ItemChecked event handler)
//4.5.1.0 - MM: added parser for the MAIL CITY and MAIL STATE from returned MAIL_CITY_STATE value the dataprovider provided.
//        - Registration screen - added constraints: FN, LN, CN, FSMA, C, S, Z, HPN, BPN, CN, Email are restricted to 32 characters       
//        - Fixed couple of crossthreaded problems
//4.5.0.0 - Integrated SSX intot the PG ContactView tabs
//4.4.0.2 - Added listview column sorts and buyer info report (multiple pages, multiple rows)
//4.4.0.1 - Buyer Info RC1
//4.4.0.0 - LeadSource for selling / Office staff lists integrated (warning DB change)
//4.3.0.0 - MFW Landscape print support
//4.2.0.0 - EditDocumentTemplateUserControl - added Landscape print option (bugfix)
//4.1.0.0 - EditDocumentTemplateUserControl - added Landscape print option
//4.0.0.0 - integrated SSX, added Vista support
//3.7.0.1 - Added TxTextControl 14, ported to .NetFramework 2.0
//3.7.0.0 - COMMERCIALS OBJECTS RESET UPON NEW CONTACT
//3.6.0.0 - INTEGRATED COMMERCIALS PHASE 1
//3.5.3.3 - SKIP TRACE WIZARD - REFRESH & SELECT GROUP WHEN FINISHED SCAN-IN
//3.5.3.2 - MATCHING MODULE - GROUP VIEW REFRESH (AFTER FINISHED MATCHING)
//3.5.3.1 - SKIP TRACING - GROUP VIEW FLICKER REMOVED
//3.5.3.0 - SKIP TRACING - removed succ and unsucc group, additional logic with newly created groups, 
//		  - FIND DUPLICATES: added 4th checkbox (name or address) and retested		 
//3.5.2.0 - SKIP TRACING ENHANCEMENT (beta testing)
//3.5.1.2 - MERGE CONTACTS (beta testing)
//3.5.1.1 - MM - implemented USE APN checkbox
//3.5.1.0 - PWD CHANGE
//3.5.0.6 - Labels bugfix
//3.5.0.5 - MM APN search now takes both IMPORTED DATA APN and USER DATA APN. USER DATA will be considered first if they both exist
//3.5.0.4 - NW CLI query and charge account using SRV credentials (IMPLEMENTED)
//3.5.0.3 - Added APN criteria to MM
// Changed Purge database icons, changed labels
// changed lables in UserCredit Form
//bugfix - current resident
//3.5.0.2 - Removed dumpObjects
//3.5.0.1 - Removed DEBUG Connect to database dump
// - 123Sold DB connection reparied like main connection (different connection string for NW than PLAIN)
// - PurgeDatabase DB connection repaired like main connection (different connection string for NW than PLAIN)
//3.5.0.0 - new dtd
// - (No Mail) and N/AVAIL handlers integrated into MM
// - TopUp credits integrated into PP, COMPS and MM
// - Added account status pages
//3.4.1.2 - ReEnabled ZIP Filter for new zip codes, fixed Step2 disabled buttons
//3.4.1.1 - Removed ZIP Filter
//3.4.1.0 - Integrated Excel 2007, supported both 2003 and 2007 excel file formats
//3.4.0.9 - Integrated Excel 2003
//Advanced Lables (Small / Medium / Big) because of multiple owners introduced in MM
//Enabled clean database / find duplicates
//3.4.0.8 - Integrated Sasa's scheduler work
//Integrated txTextFrames into EditDocumentTemplate user control
//Integrated PhoneNumber and Mailing Address into the Matching Module
//3.4.0.7 - migraged to new .dtd and made a forced release. 
//COMPS work with new .dtd
//Purge database - disabled
//MM new features - disabled
//Labels - the biggest labels are taken care of, the rest is still in progress
//Top Up credits - not released yet
//3.4.0.6 - Purge database reengineered, ZIP filter bugfix
//3.4.0.5 - Purge database and find duplicates - still under construction
//3.4.0.4 - Added licenses.licx file back to the solution. TxTextConrol will not work if it isn't part of the solution
//3.4.0.3 - Integrated new MM First & Last Name Parser
//Service Worker.cs
//3.4.0.2 - (COMPS & PP Login) = PROFT1, (MM login) = PROFT2;
//MM Login- ServiceWorker.cs, Ln 219
//COMPS Login - UserControlPropertyProfileCompsListView.cs, Ln. 1101
//PP Login - UserControlPropertyProfileComps.cs, Ln. 903
//3.4.0.1 - started using svn
//3.4.0.0 - Merged PLAIN and NW branches
//3.3.1.9 - Integrated HUD (short sale module)
//3.3.1.9 - Added feature - Copy Import info to deal info
//3.3.1.8 - Group Membership - date last modified does not change when we assign / delete contact to another group!
//3.3.1.7 - AUTOSAVE FEATURE ADDED !
//3.3.1.6 - Save .rtf truly embedds image !!! Image can be viewed in MS WORD. 
//Still problem with OLE objects due to txTextControl!
//3.3.1.5 - Interfax support added

//
// In order to sign your assembly you must specify a key to use. Refer to the 
// Microsoft .NET Framework documentation for more information on assembly signing.
//
// Use the attributes below to control which key is used for signing. 
//
// Notes: 
//   (*) If no key is specified, the assembly is not signed.
//   (*) KeyName refers to a key that has been installed in the Crypto Service
//       Provider (CSP) on your machine. KeyFile refers to a file which contains
//       a key.
//   (*) If the KeyFile and the KeyName values are both specified, the 
//       following processing occurs:
//       (1) If the KeyName can be found in the CSP, that key is used.
//       (2) If the KeyName does not exist and the KeyFile does exist, the key 
//           in the KeyFile is installed into the CSP and used.
//   (*) In order to create a KeyFile, you can use the sn.exe (Strong Name) utility.
//       When specifying the KeyFile, the location of the KeyFile should be
//       relative to the project output directory which is
//       %Project Directory%\obj\<configuration>. For example, if your KeyFile is
//       located in the project directory, you would specify the AssemblyKeyFile 
//       attribute as [assembly: AssemblyKeyFile("..\\..\\mykey.snk")]
//   (*) Delay Signing is an advanced option - see the Microsoft .NET Framework
//       documentation for more information on this.
//
[assembly: AssemblyDelaySign(false)]
[assembly: AssemblyKeyFile("")]
[assembly: AssemblyKeyName("")]
