﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace DealMaker
{
    public sealed class OSFolderManager
    {
        static readonly OSFolderManager _instance = new OSFolderManager();

        OSFolderManager()
        { }


        static OSFolderManager()
        { }

        public static OSFolderManager Instance
        {
            get { return _instance; }
        }

        public string GetPlainAppDataPath()
        {
            string ver = Application.ProductVersion;
            int pVerIdx = GetAppDataPath().IndexOf(ver);
            return GetAppDataPath().Substring(0, pVerIdx);
        }

        public string GetAppDataPath()
        {
            return Application.UserAppDataPath;
        }

        public string GetPlainLocalUserAppDataPath()
        {
            string ver = Application.ProductVersion;
            int pVerIdx = GetLocalUserAppDataPath().IndexOf(ver);
            return GetLocalUserAppDataPath().Substring(0, pVerIdx);
        }

        public string GetLocalUserAppDataPath()
        {
            return Application.LocalUserAppDataPath;
        }

    }
}
