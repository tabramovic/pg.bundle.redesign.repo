﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Printing;

//added
using DTD.Response;

namespace DealMaker.PlainClasses.OnLineData.COMPS
{
    public class COMPSReportGenerator
    {
        public COMPSReportGenerator()
        { }

        public static Graphics PrintDataReport(PrintPageEventArgs e, PROPERTY property, int verticalOffset, int current, int total)
        {
            Graphics g = e.Graphics;
            Font font_16 = new Font("Arial", 16);
            Font font_14 = new Font("Arial", 13);
            Font font_12 = new Font("Arial", 11);
            Font font_10 = new Font("Arial", 10);
            Font font_10B = new Font("Arial", 10, FontStyle.Bold);
            Font font_12B = new Font("Arial", 11, FontStyle.Bold);
            Font font_9 = new Font("Arial", 8);
            Brush brush = new SolidBrush(Color.Black);
            Pen pen = new Pen(brush, 2);

            COMPSReportGenerator.PrintDataReportHeader(e, verticalOffset, current, total);

            COMPSReportGenerator.PrintReportFooter(e);

            //g.DrawLine(pen, new Point(25, 110 + verticalOffset), new Point(825, 110 + verticalOffset));

            //Subject Property
            string subjectProperty = "Subject Property: ";
            try
            {
                subjectProperty += property._PARSED_STREET_ADDRESS._HouseNumber + " " +
                                    property._PARSED_STREET_ADDRESS._StreetName + " " +
                                    property._City + ", " +
                                    property._State + " " +
                                    property._PostalCode + " ";
            }
            catch { }
            g.DrawString(subjectProperty, font_10B, brush, 50, 130 + verticalOffset);

            string subDivision = "Subdivision: ";
            try { subDivision += property._SubdivisionIdentifier; }
            catch { }
            g.DrawString(subDivision, font_10, brush, 75, 165 + verticalOffset);

            //g.DrawLine(pen, new Point(25, 180 + verticalOffset), new Point(825, 180 + verticalOffset));

            //Owner Name
            string ownerName = "Owner Name: ";
            try { ownerName += property.PROPERTY_OWNER[0]._OwnerName; }
            catch { }
            g.DrawString(ownerName, font_10B, brush, 50, 200 + verticalOffset);

            string phoneNumber = "Phone Number: ";
            try { phoneNumber += property.PROPERTY_OWNER[0]._PhoneNumber; }
            catch { }
            g.DrawString(phoneNumber, font_10, brush, 555, 200 + verticalOffset);

            string mailingAddress = "Mailing Address: ";
            try { mailingAddress += property.PROPERTY_OWNER[0]._MailingAddress + " " + property.PROPERTY_OWNER[0]._MailingCityAndState + ", " + property.PROPERTY_OWNER[0]._MailingPostalCode; }
            catch { }
            g.DrawString(mailingAddress, font_10, brush, 75, 235 + verticalOffset);

            //g.DrawLine(pen, new Point(25, 255 + verticalOffset), new Point(825, 255 + verticalOffset));

            //Property Characteristics
            g.DrawString("Property Characteristics:", font_12B, brush, 50, 285 + verticalOffset);

            string totalLivingArea = "Total Living Area: ";
            try
            {
                totalLivingArea += Convert.ToDouble(property._PROPERTY_CHARACTERISTICS._IMPROVEMENTS._ROOM_COUNT._TotalLivingAreaSquareFeetNumber).ToString("N");
                totalLivingArea = totalLivingArea.Substring(0, totalLivingArea.Length - 3);
            }
            catch { }
            g.DrawString(totalLivingArea, font_10, brush, 50, 305 + verticalOffset);

            string yearBuilt = "Year Built: ";
            try { yearBuilt += property._PROPERTY_CHARACTERISTICS._IMPROVEMENTS._GENERAL_DESCRIPTION._YearBuiltDateIdentifier; }
            catch { }
            g.DrawString(yearBuilt, font_10, brush, 300, 305 + verticalOffset);

            string totalRooms = "Total Rooms: ";
            try { totalRooms += property._PROPERTY_CHARACTERISTICS._IMPROVEMENTS._ROOM_COUNT._TotalRoomCount; }
            catch { }
            g.DrawString(totalRooms, font_10, brush, 555, 305 + verticalOffset);

            string lotArea = "Lot Area: ";
            try
            {
                lotArea += Convert.ToDouble(property._PROPERTY_CHARACTERISTICS._SITE._DIMENSIONS._LotAreaSquareFeetNumber).ToString("N");
                lotArea = lotArea.Substring(0, lotArea.Length - 3);
            }
            catch { }
            g.DrawString(lotArea, font_10, brush, 50, 325 + verticalOffset);

            string parkingType = "Parking Type: ";
            try { parkingType += property._PROPERTY_CHARACTERISTICS._IMPROVEMENTS._PARKING._TypeDescription; }
            catch { }
            g.DrawString(parkingType, font_10, brush, 300, 325 + verticalOffset);

            string totalBedrooms = "Total Bedrooms: ";
            try { totalBedrooms += property._PROPERTY_CHARACTERISTICS._IMPROVEMENTS._ROOM_COUNT._TotalBedroomsCount; } catch { }
            g.DrawString(totalBedrooms, font_10, brush, 555, 325 + verticalOffset);

            string pool = "Pool: ";
            try { pool += property._PROPERTY_CHARACTERISTICS._IMPROVEMENTS._FEATURES._POOL._HasFeatureIndicator; }
            catch { }
            g.DrawString(pool, font_10, brush, 50, 345 + verticalOffset);

            string garage = "Garage: ";
            try { garage += property._PROPERTY_CHARACTERISTICS._IMPROVEMENTS._PARKING._TypeDescription; }
            catch { }
            g.DrawString(garage, font_10, brush, 300, 345 + verticalOffset);

            string totalBaths = "Total Baths: ";
            try { totalBaths += property._PROPERTY_CHARACTERISTICS._IMPROVEMENTS._ROOM_COUNT._TotalBathsCount; }
            catch { }
            g.DrawString(totalBaths, font_10, brush, 555, 345 + verticalOffset);            

            string zoning = "Zoning: ";
            try { zoning += property._PROPERTY_CHARACTERISTICS._SITE._ZONING._ClassificationIdentifier; }
            catch { }
            g.DrawString(zoning, font_10, brush, 50, 365 + verticalOffset);

            string landUse = "Land Use: ";
            try { landUse += property._PROPERTY_CHARACTERISTICS._SITE._CHARACTERISTICS._LandUseDescription; }
            catch { }
            g.DrawString(landUse, font_10, brush, 300, 365 + verticalOffset);

            string fireplaces = "Fireplaces: ";
            try { fireplaces += property._PROPERTY_CHARACTERISTICS._IMPROVEMENTS._FEATURES._FIREPLACES._HasFeatureIndicator; }
            catch { }
            g.DrawString(fireplaces, font_10, brush, 555, 365 + verticalOffset);

            //g.DrawLine(pen, new Point(25, 385 + verticalOffset), new Point(825, 385 + verticalOffset));

            //Financials
            g.DrawString("Financials:", font_12B, brush, 50, 400 + verticalOffset);
            string lastSaleDate = "Last Sale Date: ";
            try
            {
                string lsd = property._PROPERTY_HISTORY._SALES_HISTORY[0]._LastSalesDate;
                DateTime d = new DateTime
                    (
                    (int.Parse(lsd.Substring(0, 4))),
                    (int.Parse(lsd.Substring(4, 2))),
                    (int.Parse(lsd.Substring(6, 2)))
                    );


                lastSaleDate += d.ToShortDateString();
            }
            catch { }
            g.DrawString(lastSaleDate, font_10, brush, 50, 420 + verticalOffset);

            string lastSalePrice = "Last Sale Price: ";
            try { lastSalePrice += Convert.ToDouble(property._PROPERTY_HISTORY._SALES_HISTORY[0]._LastSalesPriceAmount).ToString("C"); }
            catch { }
            g.DrawString(lastSalePrice, font_10, brush, 300, 420 + verticalOffset);

            string pricePerSqFt = "Price Per Sq.Ft.: ";
            try { pricePerSqFt += Convert.ToDouble(property._PROPERTY_HISTORY._SALES_HISTORY[0]._PricePerSquareFootAmount).ToString("C"); }
            catch { }
            g.DrawString(pricePerSqFt, font_10, brush, 555, 420 + verticalOffset);

            string firstLoan = "First Loan: ";
            //try {firstLoan += property._PROPERTY_HISTORY._MORTGAGE_HISTORY[0].;} catch {}
            g.DrawString(firstLoan, font_10, brush, 50, 440 + verticalOffset);

            string secondLoan = "Second Loan: ";
            //try {secondLoan += property._PROPERTY_HISTORY._SALES_HISTORY[0]._PricePerSquareFootAmount;} catch {}
            g.DrawString(secondLoan, font_10, brush, 300, 440 + verticalOffset);

            string cashDown = "Cash Down: ";
            try { cashDown += Convert.ToDouble(property._PROPERTY_HISTORY._SALES_HISTORY[0]._CashDownAmount).ToString("C"); }
            catch { }
            g.DrawString(cashDown, font_10, brush, 555, 440 + verticalOffset);

            string deedType = "Deed Type: ";
            try { deedType += property._PROPERTY_HISTORY._SALES_HISTORY[0]._DeedTypeDescription; }
            catch { }
            g.DrawString(deedType, font_10, brush, 50, 460 + verticalOffset);

            string recDate = "Rec. Date: ";
            try
            {
                string rd = property._PROPERTY_HISTORY._SALES_HISTORY[0]._LastRecordingDate;
                DateTime d = new DateTime
                    (
                    (int.Parse(rd.Substring(0, 4))),
                    (int.Parse(rd.Substring(4, 2))),
                    (int.Parse(rd.Substring(6, 2)))
                    );

                recDate += d.ToShortDateString();
            }
            catch { }
            g.DrawString(recDate, font_10, brush, 300, 460 + verticalOffset);

            string docNo = "Doc.No.: ";
            try
            { docNo += property._PROPERTY_HISTORY._SALES_HISTORY[0]._DocumentNumberIdentifier; }
            catch { }
            g.DrawString(docNo, font_10, brush, 555, 460 + verticalOffset);

            //string sellerName = "Seller Name: ";			
            //try {sellerName += property.PROPERTY_OWNER[0]._OwnerName;}  catch {}
            //g.DrawString(sellerName, font_12, brush, 600, 455 + verticalOffset);

            //g.DrawLine(pen, new Point(25, 470 + verticalOffset), new Point(825, 470 + verticalOffset));

            //Taxes
            g.DrawString("Taxes:", font_12B, brush, 50, 495 + verticalOffset);

            string apn = "APN: ";
            try { apn += property._AssessorsParcelIdentifier; }
            catch { }
            g.DrawString(apn, font_10, brush, 150, 495 + verticalOffset);

            string propTax = "Prop. Tax.: ";
            try { propTax += Convert.ToDouble(property._PROPERTY_TAX._RealEstateTotalTaxAmount).ToString("C"); }
            catch { }
            g.DrawString(propTax, font_10, brush, 50, 515 + verticalOffset);

            string assesmYear = "Assesm. Year: ";
            try { assesmYear += property._PROPERTY_TAX._AssessmentYear; }
            catch { }
            g.DrawString(assesmYear, font_10, brush, 300, 515 + verticalOffset);

            string deliqYear = "Deliq. Year: ";
            try { deliqYear += property._PROPERTY_TAX._DelinquentYear; }
            catch { }
            g.DrawString(deliqYear, font_10, brush, 555, 515 + verticalOffset);

            string taxArea = "Tax Area: ";
            try { deliqYear += property._PROPERTY_TAX._RateAreaIdentifier; }
            catch { }
            g.DrawString(taxArea, font_10, brush, 50, 535 + verticalOffset);

            string taxYear = "Tax Year: ";
            try { taxYear += property._PROPERTY_TAX._TaxYear; }
            catch { }
            g.DrawString(taxYear, font_10, brush, 300, 535 + verticalOffset);

            string exempType = "Exempt. Type: ";
            try { exempType += property._PROPERTY_TAX._ExemptionTypeDescription; }
            catch { }
            g.DrawString(exempType, font_10, brush, 555, 535 + verticalOffset);

            string totalAssessedValue = "Assessed Value: ";
            try { totalAssessedValue += Convert.ToDouble(property._PROPERTY_TAX._TotalAssessedValueAmount).ToString("C"); }
            catch { }
            g.DrawString(totalAssessedValue, font_10, brush, 50, 555 + verticalOffset);

            string landValue = "Land Use: ";
            try { landValue += property._PROPERTY_TAX._LandValueAmount; }
            catch { }
            g.DrawString(landValue, font_10, brush, 300, 555 + verticalOffset);

            string improvementValue = "Improvement Value: ";
            try { improvementValue += Convert.ToDouble(property._PROPERTY_TAX._ImprovementValueAmount).ToString("C"); }
            catch { }
            g.DrawString(improvementValue, font_10, brush, 555, 555 + verticalOffset);

            //g.DrawLine(pen, new Point(25, 565 + verticalOffset), new Point(825, 565 + verticalOffset));

            //Legal Description			
            string legalDesc = string.Empty;
            try { legalDesc += property._LEGAL_DESCRIPTION._TextDescription; }
            catch { }
            g.DrawString("Legal Description: ", font_12B, brush, 50, 590 + verticalOffset);
            g.DrawString(legalDesc, font_10, brush, 75, 610 + verticalOffset);


            //Additional
            g.DrawString("Additional:", font_12B, brush, 50, 640 + verticalOffset);
            string schoolDistrict = "School District: ";
            try { schoolDistrict += property._NEIGHBORHOOD_AREA_INFORMATION._SchoolDistrictName; }
            catch { }
            g.DrawString(schoolDistrict, font_10, brush, 50, 660 + verticalOffset);

            string mapRefOne = "Map Ref. One: ";
            try { mapRefOne += property._NEIGHBORHOOD_AREA_INFORMATION._MapReferenceOneIdentifier; }
            catch { }
            g.DrawString(mapRefOne, font_10, brush, 300, 660 + verticalOffset);

            string mapRefTwo = "Map Ref. Two:";
            try { mapRefTwo += property._NEIGHBORHOOD_AREA_INFORMATION._MapReferenceTwoIdentifier; }
            catch { }
            g.DrawString(mapRefTwo, font_10, brush, 600, 660 + verticalOffset);

            //g.DrawLine(pen, new Point(25, 620 + verticalOffset), new Point(825, 620 + verticalOffset));


            return g;

        }

        static string GetPropertyValueForKey(PROPERTY property, int propertyNameIdx)
        {
            try
            {
                switch (propertyNameIdx)
                {

                    //"Subject Property Addr.:",    //0
                    case 0:
                        return property._PARSED_STREET_ADDRESS._HouseNumber + " " + property._PARSED_STREET_ADDRESS._StreetName;

                    //"Subject Property City:", 	  //1
                    case 1:
                        return property._City;

                    //"Subject Property State:", 	  //2
                    case 2:
                        return property._State;

                    //"Subject Property ZIP:", 	      //3
                    case 3:
                        return property._PostalCode;

                    //"Subdivision:", 			      //4
                    case 4:
                        return property._SubdivisionIdentifier;

                    //"Owner Name:", 				  //5
                    case 5:
                        return property.PROPERTY_OWNER[0]._OwnerName;

                    //"Phone Number:", 			      //6
                    case 6:
                        return property.PROPERTY_OWNER[0]._PhoneNumber;

                    //"Mailing Address:", 		      //7
                    case 7:
                        return property.PROPERTY_OWNER[0]._MailingAddress;

                    //"Mailing City and State:",	  //8
                    case 8:
                        return property.PROPERTY_OWNER[0]._MailingCityAndState;

                    //"Mailing ZIP",                  //9
                    case 9:
                        return property.PROPERTY_OWNER[0]._MailingPostalCode;

                    //"Total Living Area:",           //10
                    case 10:
                        return Convert.ToDouble(property._PROPERTY_CHARACTERISTICS._IMPROVEMENTS._ROOM_COUNT._TotalLivingAreaSquareFeetNumber).ToString("N"); ;

                    //"Year Built:",				  //11
                    case 11:
                        return property._PROPERTY_CHARACTERISTICS._IMPROVEMENTS._GENERAL_DESCRIPTION._YearBuiltDateIdentifier;

                    //"Total Rooms:",				  //12
                    case 12:
                        return property._PROPERTY_CHARACTERISTICS._IMPROVEMENTS._ROOM_COUNT._TotalRoomCount;

                    //"Total Bedrooms",             //13
                    case 13:
                        return property._PROPERTY_CHARACTERISTICS._IMPROVEMENTS._ROOM_COUNT._TotalBedroomsCount;

                    //"Lot Area:",				      //14
                    case 14:
                        string lotArea = Convert.ToDouble(property._PROPERTY_CHARACTERISTICS._SITE._DIMENSIONS._LotAreaSquareFeetNumber).ToString("N");
                        return lotArea.Substring(0, lotArea.Length - 3);

                    //"Parking Type:",			      //15
                    case 15:
                        return property._PROPERTY_CHARACTERISTICS._IMPROVEMENTS._PARKING._TypeDescription;

                    //"Total Baths:",				  //16
                    case 16:
                        return property._PROPERTY_CHARACTERISTICS._IMPROVEMENTS._ROOM_COUNT._TotalBathsCount; ;

                    //"Pool:",					      //17
                    case 17:
                        return property._PROPERTY_CHARACTERISTICS._IMPROVEMENTS._FEATURES._POOL._HasFeatureIndicator; ;

                    //"Garage:",					  //18
                    case 18:
                        return property._PROPERTY_CHARACTERISTICS._IMPROVEMENTS._PARKING._TypeDescription; ;

                    //"Fireplaces:",				  //19
                    case 19:
                        return property._PROPERTY_CHARACTERISTICS._IMPROVEMENTS._FEATURES._FIREPLACES._HasFeatureIndicator; ;

                    //"Zoning:",					  //20
                    case 20:
                        return property._PROPERTY_CHARACTERISTICS._SITE._ZONING._ClassificationIdentifier; ;

                    //"Land Use:",				      //21
                    case 21:
                        return property._PROPERTY_CHARACTERISTICS._SITE._CHARACTERISTICS._LandUseDescription; ;

                    //"Last Sale Date:",			  //22
                    case 22:
                        string lsd = property._PROPERTY_HISTORY._SALES_HISTORY[0]._LastSalesDate;
                        DateTime d = new DateTime((int.Parse(lsd.Substring(0, 4))), (int.Parse(lsd.Substring(4, 2))), int.Parse(lsd.Substring(6, 2)));
                        return d.ToShortDateString();

                    //"Last Sale Price:",			  //23
                    case 23:
                        return Convert.ToDouble(property._PROPERTY_HISTORY._SALES_HISTORY[0]._LastSalesPriceAmount).ToString("C");

                    //"Price Per Sq.Ft.:",		      //24
                    case 24:
                        return Convert.ToDouble(property._PROPERTY_HISTORY._SALES_HISTORY[0]._PricePerSquareFootAmount).ToString("C");

                    ////"First Loan:",
                    ////"Second Loan:",

                    //"Cash Down:",				      //25
                    case 25:
                        return Convert.ToDouble(property._PROPERTY_HISTORY._SALES_HISTORY[0]._CashDownAmount).ToString("C");

                    //"Deed Type:",				      //26
                    case 26:
                        return property._PROPERTY_HISTORY._SALES_HISTORY[0]._DeedTypeDescription;

                    //"Rec. Date:",				      //27
                    case 27:
                        string rd = property._PROPERTY_HISTORY._SALES_HISTORY[0]._LastRecordingDate;
                        d = new DateTime((int.Parse(rd.Substring(0, 4))), (int.Parse(rd.Substring(4, 2))), (int.Parse(rd.Substring(6, 2))));
                        return d.ToShortDateString();

                    //"Doc.No.:",					  //28
                    case 28:
                        return property._PROPERTY_HISTORY._SALES_HISTORY[0]._DocumentNumberIdentifier; ;

                    //"APN:",						  //29
                    case 29:
                        return property._AssessorsParcelIdentifier;

                    //"Prop. Tax.:",				  //30
                    case 30:
                        return Convert.ToDouble(property._PROPERTY_TAX._RealEstateTotalTaxAmount).ToString("C");

                    //"Assesm. Year:",			      //31
                    case 31:
                        return property._PROPERTY_TAX._AssessmentYear;

                    //"Deliq. Year:",				  //32
                    case 32:
                        return property._PROPERTY_TAX._DelinquentYear;

                    //"Tax Area:",				      //33
                    case 33:
                        return property._PROPERTY_TAX._RateAreaIdentifier;

                    //"Tax Year:",				      //34
                    case 34:
                        return property._PROPERTY_TAX._TaxYear;

                    //"Exempt. Type:",			      //35
                    case 35:
                        return property._PROPERTY_TAX._ExemptionTypeDescription;

                    //"Assessed Value:",			  //36
                    case 36:
                        return Convert.ToDouble(property._PROPERTY_TAX._TotalAssessedValueAmount).ToString("C");

                    //"Land Use:",				      //37
                    case 37:
                        return property._PROPERTY_TAX._LandValueAmount;

                    //"Improvement Value:",		      //38
                    case 38:
                        return Convert.ToDouble(property._PROPERTY_TAX._ImprovementValueAmount).ToString("C");

                    //"Legal Description:",		      
                    //case 38:
                    //    return property._LEGAL_DESCRIPTION._TextDescription;

                    //"School District:",			  //39
                    case 39:
                        return property._NEIGHBORHOOD_AREA_INFORMATION._SchoolDistrictName;

                    //"Map Ref. One:",			      //40
                    case 40:
                        return property._NEIGHBORHOOD_AREA_INFORMATION._MapReferenceOneIdentifier;

                    //"Map Ref Two:"				  //41
                    case 41:
                        return property._NEIGHBORHOOD_AREA_INFORMATION._MapReferenceTwoIdentifier;

                    default:
                        return string.Empty;
                }
            }
            catch { return string.Empty; }            
        }

        public static Graphics PrintDataReportWithMaps(PrintPageEventArgs e, List<PROPERTY> properties, int verticalOffset, int current, int total, List<Image> images, DTD.Response.PROPERTY ppProperty)
        {
            Graphics g = e.Graphics;
            Font font_16 = new Font("Arial", 16);
            Font font_14 = new Font("Arial", 13);
            Font font_12 = new Font("Arial", 11);
            Font font_10 = new Font("Arial", 10);
            Font font_10B = new Font("Arial", 10, FontStyle.Bold);
            Font font_12B = new Font("Arial", 11, FontStyle.Bold);
            Font font_8 = new Font("Arial", 8);
            Font font_8B = new Font("Arial", 8, FontStyle.Bold);
            Font font_9 = new Font("Arial", 9);
            Font font_9B = new Font("Arial", 9, FontStyle.Bold);

            Brush brush = new SolidBrush(Color.Black);
            Pen pen = new Pen(brush, 2);
            int pictureOffset = 165;

            //pic size = 160x140

            g.DrawLine(Pens.Black, new Point(25, 50), new Point(825, 50));
            
            //COLUMN HEADER BEGIN
            g.DrawString("SUBJ. PROPERTY (A)", font_10B, Brushes.Black, new PointF(185, 20));

            if ((current - 1) * 3 + 1 <= properties.Count)
                g.DrawString("COMPS " + Number2String((current - 1) * 3 + 1 + 1, true), font_10B, Brushes.Black, new PointF(345, 20));  

            if ((current - 1) * 3 + 2 <= properties.Count)
                g.DrawString("COMPS " + Number2String((current - 1) * 3 + 2 + 1, true), font_10B, Brushes.Black, new PointF(505, 20));  

            if ((current - 1) * 3 + 3 <= properties.Count)
                g.DrawString("COMPS " + Number2String((current - 1) * 3 + 3 + 1, true), font_10B, Brushes.Black, new PointF(665, 20));  
            //COLUMN HEADER END

            string[] reportPropertyNames = new string[] 
            { 
                "Subject Property Addr.",    //0
                "Subject Property City", 	    //1
                "Subject Property State", 	    //2
                "Subject Property ZIP", 	    //3
                "Subdivision", 			    //4
                "Owner Name", 				    //5
                "Phone Number", 			    //6
                "Mailing Address", 		    //7
                "Mailing City and State",	    //8
                "Mailing ZIP",                  //9
                "Total Living Area",           //10
                "Year Built",				    //11
                "Total Rooms",				    //12
                "Total Bedrooms",               //13
                "Lot Area",				    //14
                "Parking Type",			    //15
                "Total Baths",				    //16
                "Pool",					    //17
                "Garage",					    //18
                "Fireplaces",				    //19
                "Zoning",					    //20
                "Land Use",				    //21
                "Last Sale Date",			    //22
                "Last Sale Price",			    //23
                "Price Per Sq.Ft.",		    //24
                //"First Loan:",
                //"Second Loan:",
                "Cash Down",				    //25
                "Deed Type",				    //26
                "Rec. Date",				    //27
                "Doc.No.",					    //28
                "APN",						    //29
                "Prop. Tax.",				    //30
                "Assesm. Year",			    //31
                "Deliq. Year",				    //32
                "Tax Area",				    //33
                "Tax Year",				    //34
                "Exempt. Type",			    //35
                "Assessed Value",			    //36
                "Land Use",				    //37
                "Improvement Value",		    //38
                "Legal Description",		    //39
                "School District",			    //40
                "Map Ref. One",			    //41
                "Map Ref Two"				    //42
            };

            float rowHeight = (g.MeasureString("X", font_10B)).Height;            
            int vertPos = pictureOffset;

            for (int i = 0; i < reportPropertyNames.Length; i++)
            {
                vertPos += (int)rowHeight;

                if (0 == i)                
                    g.DrawLine(Pens.LightGray, new Point(25, vertPos), new Point(825, vertPos));
                
                g.DrawString(reportPropertyNames[i], font_10B, brush, new PointF(25, vertPos));
                
                List<int> propertyStartVertPositions = new List<int>();
                for (int j = 0; j < properties.Count; j++)
                    propertyStartVertPositions.Add(vertPos);

                bool drawLine = true;
                //for (int j = 0; j < properties.Count; j++)
                int tempOffset = (current - 1) * 3;
                int maxJ = 3 + tempOffset;

                if (maxJ > properties.Count)
                    maxJ = properties.Count;
                
                for (int j = 0 + tempOffset; j < maxJ; j++)
                {
                    drawLine = true;

                    PROPERTY singleProp = properties[j];                    

                    if (null != singleProp)
                    {
                        string propertyValueForKey = GetPropertyValueForKey(singleProp, i);

                        //PP Data
                        string ppPropertyValueForKey = GetPropertyValueForKey(ppProperty, i);

                        SizeF stringSize = g.MeasureString(propertyValueForKey, font_10);

                        if (stringSize.Width < 160)
                        {
                            g.DrawString(propertyValueForKey, font_10, brush, new PointF(160 + 25 + (((j % 3) + 1) * 160), propertyStartVertPositions[j]));

                            //PP Data
                            if (0 == j % 3)
                                g.DrawString(ppPropertyValueForKey, font_10, brush, new PointF(160 + 25, propertyStartVertPositions[j]));                            
                        }
                        else
                        {
                            //get string array with maxwidth 160
                            if (propertyValueForKey.Length > 60)
                                propertyValueForKey = propertyValueForKey.Substring(0, 60) + "...";

                            List<string> lines = GetTexts(propertyValueForKey, g, font_10, 160);

                            int tempCnt = 0;
                            //render string array && adjust addedHeight;
                            foreach (string line in lines)
                            {
                                drawLine = false;
                                g.DrawString(line, font_10, brush, new PointF(160 + 25 + (((j % 3) + 1) * 160), propertyStartVertPositions[j]));

                                if (tempCnt < lines.Count - 1)
                                {
                                    propertyStartVertPositions[j] += (int)rowHeight;
                                    drawLine = true;
                                }

                                tempCnt++;
                            }
                        }

                        if (drawLine)
                            g.DrawLine(Pens.LightGray, new Point(25, propertyStartVertPositions[j]), new Point(825, propertyStartVertPositions[j]));
                    }
                }                                

                foreach (int propertyStartVertPos in propertyStartVertPositions)
                {
                    if (propertyStartVertPos > vertPos)
                        vertPos = propertyStartVertPos;                
                }                
            }

            //Add last graphics

            vertPos += (int)rowHeight;
            //Add Vert. lines
            for (int i = 0; i < 6; i++)
            {
                g.DrawLine(Pens.LightGray, new Point(25 + (i * 160), 50), new Point(25 + (i * 160), vertPos));                
            }

            //draw images
            //current = currentPage

            //draw always
            if (null != images)
            {
                try
                {
                    g.DrawImage(images[1], new Point(27 + 160, 52));

                    for (int i = 2; i < 5; i++)
                    {
                        int picOffset = ((current - 1) * 3) + i;
                        if (picOffset < images.Count)
                        {
                            if (2 == i)
                                g.DrawImage(images[picOffset], new Point(27 + 320, 52));
                            else if (3 == i)
                                g.DrawImage(images[picOffset], new Point(27 + 480, 52));
                            else if (4 == i)
                                g.DrawImage(images[picOffset], new Point(27 + 640, 52));
                        }
                    }
                }
                catch { }   //silent
            }



            //Add Horiz. line
            g.DrawLine(Pens.LightGray, new Point(25, vertPos), new Point(825, vertPos));

            COMPSReportGenerator.PrintReportFooter(e);
            
            return g;

        }

        public static List<string> GetTexts(string val, Graphics g, Font font, int maxWidth)
        {
            List<string> retVal = new List<string>();
            string[] input = val.Split(' ');
            string row = string.Empty;

            foreach (string word in input)
            {
                if (g.MeasureString(word, font).Width >= maxWidth)                
                    continue;

                if (g.MeasureString((row + " " + word), font).Width < maxWidth)
                {
                    if (string.Empty == row)
                        row = word;
                    else
                        row = row + " " + word;
                }
                else
                {
                    retVal.Add(row);
                    row = word;
                }
            }

            retVal.Add(row);
            return retVal;
        }

        public static Graphics PrintMultipleDataReportHeaderWithMaps
            (
            PrintPageEventArgs e,
            string header,
            string subject,
            string sFor,
            string subjLivingArea,
            string searchCriteria,
            string sqftRange,
            string allSalesInLast,
            string searchRadius,
            string estimatedValue,
            string compsNr,
            PROPERTY[] property,
            List<Image> images,
            DTD.Response.PROPERTY ppProperty
            )
        {
            Graphics g = e.Graphics;
            Font font_16 = new Font("Arial", 16);
            Font font_14 = new Font("Arial", 14);
            Font font_14B = new Font("Arial", 14, FontStyle.Bold);
            Font font_12 = new Font("Arial", 12);
            Font font_12B = new Font("Arial", 12, FontStyle.Bold);
            Font font_9 = new Font("Arial", 9);
            Font font_10 = new Font("Arial", 10);
            Font font_10B = new Font("Arial", 10, FontStyle.Bold);
            Brush brush = new SolidBrush(Color.Black);
            Pen pen = new Pen(brush, 2);

            SizeF headerSize = g.MeasureString(header, font_14B);
            int width = e.PageBounds.Size.Width;
            int xOffset = (int)((width - headerSize.Width) / 2);

            g.DrawString(sFor, font_14B, brush, 35, 50);
            g.DrawString(header, font_14B, brush, xOffset, 100);

            DateTime now = DateTime.Now;            
            StringFormat sf = new StringFormat(StringFormatFlags.DirectionRightToLeft);            
            g.DrawString(now.ToShortDateString(), font_10, brush, 825, 50, sf);

            g.DrawLine(Pens.Black, new Point(35, 75), new Point(825, 75));

            //PICTURE BOX
            g.DrawLine(Pens.Black, new Point(60, 140), new Point(800, 140));
            g.DrawLine(Pens.Black, new Point(60, 415), new Point(800, 415));

            if (null != images && images.Count > 0 && null != images[0])
            {                                
                g.DrawImage(images[0], new Point(95, 143));
            }

            g.DrawString("Subject Property:" + subject, font_10B, brush, 35, 440);
            g.DrawString("Site Address: " + sFor, font_10, brush, 75, 475);
            g.DrawString("Subject Living Area: " + subjLivingArea, font_10, brush, 75, 510);

            g.DrawString("Search Criteria: " + searchCriteria, font_10B, brush, 35, 545);
            g.DrawString("Sq.Ft. Range: " + sqftRange, font_10, brush, 75, 580);
            g.DrawString("All Sales In Last: " + allSalesInLast, font_10, brush, 300, 580);
            g.DrawString("Search Radius: " + searchRadius, font_10, brush, 500, 580);

            g.DrawString("Estimated Value: " + estimatedValue, font_10B, brush, 35, 625);
            g.DrawString("Number of COMPS found: " + compsNr, font_10B, brush, 500, 625);

            if (null == property)
                return g;

            g.DrawString("Summary Statistics", font_10B, brush, 35, 670);

            sf = new StringFormat(StringFormatFlags.DirectionRightToLeft);

            List<double> salePrices = new List<double>();
            List<double> totalLivingAreas = new List<double>();
            List<double> pricesPerSqFt = new List<double>();
            List<double> totalRooms = new List<double>();
            List<double> totalBedRooms = new List<double>();
            List<double> totalBaths = new List<double>();
            List<double> years = new List<double>();
            List<double> lotSqFts = new List<double>();
            List<double> distances = new List<double>();

            double lastSalePrice = 0;
            double totalLivingArea = 0;
            double pricePerSqFt = 0;
            double totalRoom = 0;
            double totalBedroom = 0;
            double totalBath = 0;
            double year = 0;
            double lotSqft = 0;
            double distance = 0;

            try { salePrices.Add(Convert.ToDouble(ppProperty._PROPERTY_HISTORY._SALES_HISTORY[0]._LastSalesPriceAmount)); }
            catch { }

            try { totalLivingAreas.Add(Convert.ToDouble(ppProperty._PROPERTY_CHARACTERISTICS._IMPROVEMENTS._ROOM_COUNT._TotalLivingAreaSquareFeetNumber)); }
            catch { }

            try { pricesPerSqFt.Add(Convert.ToDouble(ppProperty._PROPERTY_HISTORY._SALES_HISTORY[0]._PricePerSquareFootAmount)); }
            catch { }

            try { totalRooms.Add(Convert.ToDouble(ppProperty._PROPERTY_CHARACTERISTICS._IMPROVEMENTS._ROOM_COUNT._TotalRoomCount)); }
            catch { }

            try { totalBedRooms.Add(Convert.ToDouble(ppProperty._PROPERTY_CHARACTERISTICS._IMPROVEMENTS._ROOM_COUNT._TotalBedroomsCount)); }
            catch { }

            try { totalBaths.Add(Convert.ToDouble(ppProperty._PROPERTY_CHARACTERISTICS._IMPROVEMENTS._ROOM_COUNT._TotalBathsCount)); }
            catch { }

            try { years.Add(Convert.ToDouble(ppProperty._PROPERTY_CHARACTERISTICS._IMPROVEMENTS._GENERAL_DESCRIPTION._YearBuiltDateIdentifier)); }
            catch { }

            try { lotSqFts.Add(Convert.ToDouble(ppProperty._PROPERTY_CHARACTERISTICS._SITE._DIMENSIONS._LotAreaSquareFeetNumber)); }
            catch { }

            for (int i = 0; i < property.Length; i++)
            {                
                try { lastSalePrice = Convert.ToDouble(property[i]._PROPERTY_HISTORY._SALES_HISTORY[0]._LastSalesPriceAmount); }
                catch {lastSalePrice = 0;}
                salePrices.Add(lastSalePrice);

                try { totalLivingArea = Convert.ToDouble(property[i]._PROPERTY_CHARACTERISTICS._IMPROVEMENTS._ROOM_COUNT._TotalLivingAreaSquareFeetNumber); }
                catch { totalLivingArea = 0; }
                totalLivingAreas.Add(totalLivingArea);

                try { pricePerSqFt = Convert.ToDouble(property[i]._PROPERTY_HISTORY._SALES_HISTORY[0]._PricePerSquareFootAmount); }
                catch { pricePerSqFt = 0; }
                pricesPerSqFt.Add(pricePerSqFt);

                try { totalRoom = Convert.ToDouble(property[i]._PROPERTY_CHARACTERISTICS._IMPROVEMENTS._ROOM_COUNT._TotalRoomCount); }
                catch { totalRoom = 0; }
                totalRooms.Add(totalRoom);

                try { totalBedroom = Convert.ToDouble(property[i]._PROPERTY_CHARACTERISTICS._IMPROVEMENTS._ROOM_COUNT._TotalBedroomsCount); }
                catch { totalBedroom = 0; }
                totalBedRooms.Add(totalBedroom);

                try { totalBath = Convert.ToDouble(property[i]._PROPERTY_CHARACTERISTICS._IMPROVEMENTS._ROOM_COUNT._TotalBathsCount); }
                catch { totalBath = 0; }
                totalBaths.Add(totalBath);

                try { year = Convert.ToDouble(property[i]._PROPERTY_CHARACTERISTICS._IMPROVEMENTS._GENERAL_DESCRIPTION._YearBuiltDateIdentifier); }
                catch { year = 0; }
                years.Add(year);

                try { lotSqft = Convert.ToDouble(property[i]._PROPERTY_CHARACTERISTICS._SITE._DIMENSIONS._LotAreaSquareFeetNumber); }
                catch { lotSqft = 0; }
                lotSqFts.Add(lotSqft);

                try { distance = Convert.ToDouble(property[i]._PlatName); }
                catch { distance = 0; }
                distances.Add(distance);

            }

            string formatter = "#,##0.00";

            g.DrawString("Subject Property", font_10, brush, 300, 705, sf);
            g.DrawString("High", font_10, brush, 420, 705, sf);
            g.DrawString("Low", font_10, brush, 540, 705, sf);
            g.DrawString("Median", font_10, brush, 680, 705, sf);
            g.DrawString("Average", font_10, brush, 800, 705, sf);

            g.DrawString("Sale price", font_10, brush, 35, 740);
            try { lastSalePrice = Convert.ToDouble(ppProperty._PROPERTY_HISTORY._SALES_HISTORY[0]._LastSalesPriceAmount); }
            catch {lastSalePrice = 0;}

            g.DrawString(lastSalePrice.ToString("C"), font_10, brush, 300, 740, sf);
            g.DrawString(GetMaxValue(salePrices.ToArray()).ToString("C"), font_10, brush, 420, 740, sf);
            g.DrawString(GetMinValue(salePrices.ToArray()).ToString("C"), font_10, brush, 540, 740, sf);
            g.DrawString(GetMedianValue(salePrices.ToArray()).ToString("C"), font_10, brush, 680, 740, sf);
            g.DrawString(GetAvgValue(salePrices.ToArray()).ToString("C"), font_10, brush, 800, 740, sf);

            g.DrawString("Total Living Area (Sq.Ft.)", font_10, brush, 35, 775);
            try { totalLivingArea = Convert.ToDouble(ppProperty._PROPERTY_CHARACTERISTICS._IMPROVEMENTS._ROOM_COUNT._TotalLivingAreaSquareFeetNumber); }
            catch { totalLivingArea = 0; }
            g.DrawString(totalLivingArea.ToString("#.00"), font_10, brush, 300, 775, sf);
            g.DrawString(GetMaxValue(totalLivingAreas.ToArray()).ToString(formatter), font_10, brush, 420, 775, sf);
            g.DrawString(GetMinValue(totalLivingAreas.ToArray()).ToString(formatter), font_10, brush, 540, 775, sf);
            g.DrawString(GetMedianValue(totalLivingAreas.ToArray()).ToString(formatter), font_10, brush, 680, 775, sf);
            g.DrawString(GetAvgValue(totalLivingAreas.ToArray()).ToString(formatter), font_10, brush, 800, 775, sf);

            g.DrawString("Price Per Sq.Ft.", font_10, brush, 35, 810);
            try { pricePerSqFt = Convert.ToDouble(ppProperty._PROPERTY_HISTORY._SALES_HISTORY[0]._PricePerSquareFootAmount); }
            catch { pricePerSqFt = 0; }
            g.DrawString(pricePerSqFt.ToString("#.00"), font_10, brush, 300, 810, sf);
            g.DrawString(GetMaxValue(pricesPerSqFt.ToArray()).ToString(formatter), font_10, brush, 420, 810, sf);
            g.DrawString(GetMinValue(pricesPerSqFt.ToArray()).ToString(formatter), font_10, brush, 540, 810, sf);
            g.DrawString(GetMedianValue(pricesPerSqFt.ToArray()).ToString(formatter), font_10, brush, 680, 810, sf);
            g.DrawString(GetAvgValue(pricesPerSqFt.ToArray()).ToString(formatter), font_10, brush, 800, 810, sf);

            g.DrawString("Total Rooms", font_10, brush, 35, 845);
            try { totalRoom = Convert.ToDouble(ppProperty._PROPERTY_CHARACTERISTICS._IMPROVEMENTS._ROOM_COUNT._TotalRoomCount); }
            catch { totalRoom = 0; }
            g.DrawString(totalRoom.ToString("#.00"), font_10, brush, 300, 845, sf);
            g.DrawString(GetMaxValue(totalRooms.ToArray()).ToString(formatter), font_10, brush, 420, 845, sf);
            g.DrawString(GetMinValue(totalRooms.ToArray()).ToString(formatter), font_10, brush, 540, 845, sf);
            g.DrawString(GetMedianValue(totalRooms.ToArray()).ToString(formatter), font_10, brush, 680, 845, sf);
            g.DrawString(GetAvgValue(totalRooms.ToArray()).ToString(formatter), font_10, brush, 800, 845, sf);

            g.DrawString("Total Bedrooms", font_10, brush, 35, 880);
            try { totalBedroom = Convert.ToDouble(ppProperty._PROPERTY_CHARACTERISTICS._IMPROVEMENTS._ROOM_COUNT._TotalBedroomsCount); }
            catch { totalBedroom = 0; }
            g.DrawString(totalBedroom.ToString("#.00"), font_10, brush, 300, 880, sf);
            g.DrawString(GetMaxValue(totalBedRooms.ToArray()).ToString(formatter), font_10, brush, 420, 880, sf);
            g.DrawString(GetMinValue(totalBedRooms.ToArray()).ToString(formatter), font_10, brush, 540, 880, sf);
            g.DrawString(GetMedianValue(totalBedRooms.ToArray()).ToString(formatter), font_10, brush, 680, 880, sf);
            g.DrawString(GetAvgValue(totalBedRooms.ToArray()).ToString(formatter), font_10, brush, 800, 880, sf);

            g.DrawString("Total Baths", font_10, brush, 35, 915);
            try { totalBath = Convert.ToDouble(ppProperty._PROPERTY_CHARACTERISTICS._IMPROVEMENTS._ROOM_COUNT._TotalBathsCount); }
            catch { totalBath = 0; }
            g.DrawString(totalBath.ToString("#.00"), font_10, brush, 300, 915, sf);
            g.DrawString(GetMaxValue(totalBaths.ToArray()).ToString(formatter), font_10, brush, 420, 915, sf);
            g.DrawString(GetMinValue(totalBaths.ToArray()).ToString(formatter), font_10, brush, 540, 915, sf);
            g.DrawString(GetMedianValue(totalBaths.ToArray()).ToString(formatter), font_10, brush, 680, 915, sf);
            g.DrawString(GetAvgValue(totalBaths.ToArray()).ToString(formatter), font_10, brush, 800, 915, sf);

            g.DrawString("Year Built", font_10, brush, 35, 950);
            try { year = Convert.ToDouble(ppProperty._PROPERTY_CHARACTERISTICS._IMPROVEMENTS._GENERAL_DESCRIPTION._YearBuiltDateIdentifier); }
            catch { year = 0; }
            g.DrawString(year.ToString("#.00"), font_10, brush, 300, 950, sf);
            g.DrawString(GetMaxValue(years.ToArray()).ToString(formatter), font_10, brush, 420, 950, sf);
            g.DrawString(GetMinValue(years.ToArray()).ToString(formatter), font_10, brush, 540, 950, sf);
            g.DrawString(GetMedianValue(years.ToArray()).ToString(formatter), font_10, brush, 680, 950, sf);
            g.DrawString(GetAvgValue(years.ToArray()).ToString(formatter), font_10, brush, 800, 950, sf);

            g.DrawString("Lot Area", font_10, brush, 35, 985);
            try { lotSqft = Convert.ToDouble(ppProperty._PROPERTY_CHARACTERISTICS._SITE._DIMENSIONS._LotAreaSquareFeetNumber); }
            catch { lotSqft = 0; }
            g.DrawString(lotSqft.ToString("#.00"), font_10, brush, 300, 985, sf);
            g.DrawString(GetMaxValue(lotSqFts.ToArray()).ToString(formatter), font_10, brush, 420, 985, sf);
            g.DrawString(GetMinValue(lotSqFts.ToArray()).ToString(formatter), font_10, brush, 540, 985, sf);
            g.DrawString(GetMedianValue(lotSqFts.ToArray()).ToString(formatter), font_10, brush, 680, 985, sf);
            g.DrawString(GetAvgValue(lotSqFts.ToArray()).ToString(formatter), font_10, brush, 800, 985, sf);


            //g.DrawString("Stories", font_10, brush, 35, 985);
            g.DrawString("Distance (miles)", font_10, brush, 35, 1020);
            g.DrawString(GetMaxValue(distances.ToArray()).ToString(formatter), font_10, brush, 420, 1020, sf);
            g.DrawString(GetMinValue(distances.ToArray()).ToString(formatter), font_10, brush, 540, 1020, sf);
            g.DrawString(GetMedianValue(distances.ToArray()).ToString(formatter), font_10, brush, 680, 1020, sf);
            g.DrawString(GetAvgValue(distances.ToArray()).ToString(formatter), font_10, brush, 800, 1020, sf);

            COMPSReportGenerator.PrintReportFooter(e);

            return g;
        }

        public static Graphics PrintMultipleDataReportHeader
            (
            PrintPageEventArgs e,
            string header,
            string subject,
            string sFor,
            string subjLivingArea,
            string searchCriteria,
            string sqftRange,
            string allSalesInLast,
            string searchRadius,
            string estimatedValue,
            string compsNr
            )
        {
            Graphics g = e.Graphics;
            Font font_16 = new Font("Arial", 16);
            Font font_14 = new Font("Arial", 14);
            Font font_14B = new Font("Arial", 14, FontStyle.Bold);
            Font font_12 = new Font("Arial", 12);
            Font font_12B = new Font("Arial", 12, FontStyle.Bold);
            Font font_9 = new Font("Arial", 9);
            Font font_10 = new Font("Arial", 10);
            Font font_10B = new Font("Arial", 10, FontStyle.Bold);
            Brush brush = new SolidBrush(Color.Black);
            Pen pen = new Pen(brush, 2);

            SizeF headerSize = g.MeasureString(header, font_14B);
            int width = e.PageBounds.Size.Width;
            int xOffset = (int)((width - headerSize.Width) / 2);

            g.DrawString(header, font_14B, brush, xOffset, 50);
            g.DrawString(DateTime.Now.ToShortDateString(), font_10, brush, 700, 30);

            g.DrawString("Subject Property:" + subject, font_10B, brush, 35, 90);
            g.DrawString("Site Address: " + sFor, font_10, brush, 75, 125);
            g.DrawString("Subject Living Area: " + subjLivingArea, font_10, brush, 400, 125);

            g.DrawString("Search Criteria: " + searchCriteria, font_10B, brush, 35, 160);
            g.DrawString("Sq.Ft. Range: " + sqftRange, font_10, brush, 75, 195);
            g.DrawString("All Sales In Last: " + allSalesInLast, font_10, brush, 300, 195);
            g.DrawString("Search Radius: " + searchRadius, font_10, brush, 500, 195);

            g.DrawString("Estimated Value: " + estimatedValue, font_10B, brush, 35, 240);
            g.DrawString("Number of COMPS found: " + compsNr, font_10B, brush, 500, 240);

            return g;
        }

        public static Graphics PrintMultipleDataReportWithMaps
            (
                PrintPageEventArgs e,
                string header,
                PROPERTY[] property,
                int iPageCnt,
                string subject,
                string sFor,
                string subjLivingArea,
                string searchCriteria,
                string sqftRange,
                string allSalesInLast,
                string searchRadius,
                string estimatedValue,
                string compsNr, 
                List<Image> images,
                DTD.Response.PROPERTY ppProperty
            )
        {
            int verticalOffset = 0;
            if (iPageCnt == 0)
            {
                COMPSReportGenerator.PrintMultipleDataReportHeaderWithMaps
                    (e, header, subject, sFor, subjLivingArea, searchCriteria, sqftRange, allSalesInLast, searchRadius, estimatedValue, compsNr, property, images, ppProperty);

                e.HasMorePages = true;
                return e.Graphics;                
            }

            List<PROPERTY> props = new List<PROPERTY>();

            

            
            for (int iCnt = 0; iCnt < property.Length; iCnt++)
            {
                /*int mod = iCnt % 4;
                if (0 == mod)
                    props.Add(property[0]);

                if (0 < iCnt)
                    props.Add(property[iCnt]);*/

                props.Add(property[iCnt]);
            }                        

            COMPSReportGenerator.PrintDataReportWithMaps(e, props, verticalOffset, iPageCnt, property.Length, images, ppProperty);

            /*if (iPageCnt < (property.Length - 1))
                e.HasMorePages = true;*/

            //if (iPageCnt < (property.Length - ((iPageCnt * 3) + 1)))
            if (iPageCnt * 3 < property.Length)
                e.HasMorePages = true;

            return e.Graphics;
        }

        public static Graphics PrintMultipleDataReport
            (
                PrintPageEventArgs e,
                string header,
                PROPERTY[] property,
                int iPageCnt,
                string subject,
                string sFor,
                string subjLivingArea,
                string searchCriteria,
                string sqftRange,
                string allSalesInLast,
                string searchRadius,
                string estimatedValue,
                string compsNr
            )
        {
            int verticalOffset = 0;
            if (iPageCnt == 0)
            {
                COMPSReportGenerator.PrintMultipleDataReportHeader
                    (e, header, subject, sFor, subjLivingArea, searchCriteria, sqftRange, allSalesInLast, searchRadius, estimatedValue, compsNr);
                verticalOffset += 250;
            }

            COMPSReportGenerator.PrintDataReport(e, property[iPageCnt], verticalOffset, iPageCnt, property.Length);

            if (iPageCnt < (property.Length - 1))
                e.HasMorePages = true;

            return e.Graphics;
        }

        public static Graphics PrintDataReportHeader(PrintPageEventArgs e, int headerOffset, int current, int total)
        {
            Graphics g = e.Graphics;
            Font font_12 = new Font("Arial", 12);
            Font font_14 = new Font("Arial", 14);
            Font font_14B = new Font("Arial", 14, FontStyle.Bold);
            Font font_13B = new Font("Arial", 13, FontStyle.Bold);
            Brush brush = new SolidBrush(Color.Black);

            if (1 == total)
            {
                g.DrawString("SUBJECT PROPERTY PROFILE DATA REPORT", font_13B, brush, 230, 50 + headerOffset);
                //g.DrawString("DATA REPORT", font_13B, brush, 335, 75 + headerOffset);
            }
            else
            {
                g.DrawString("COMPARABLE " + (current + 1).ToString() + " PROPERTY PROFILE DATA REPORT", font_13B, brush, 195, 50 + headerOffset);
                //g.DrawString("DATA REPORT", font_13B, brush, 335, 75 + headerOffset);
            }

            if (0 == headerOffset)
                g.DrawString(DateTime.Now.ToShortDateString(), font_12, brush, 700, 30 + headerOffset);

            return g;
        }

        public static Graphics PrintReportFooter(PrintPageEventArgs e)
        {
            Graphics g = e.Graphics;

            Font font_10I = new Font("Arial", 10, FontStyle.Italic);
            Brush brush = new SolidBrush(Color.Black);

            g.DrawString("ULTIMATE DATA SOURCE REPORTS. Copyright TKS Inc. All rights reserved.", font_10I, brush, 50, 1050);

            return g;
        }

        public static double GetMaxValue(double[] sourceNumbers)
        {
            double temp = 0;

            if (sourceNumbers == null || sourceNumbers.Length == 0)
                return 0D;

            for (int i = 0; i < sourceNumbers.Length; i++)
                if (sourceNumbers[i] > temp)
                    temp = sourceNumbers[i];

            return temp;
        }

        public static double GetMinValue(double[] sourceNumbers)
        {
            double temp = 0;

            if (sourceNumbers == null || sourceNumbers.Length == 0)
                return 0D;

            temp = sourceNumbers[0];

            for (int i = 0; i < sourceNumbers.Length; i++)
                if (sourceNumbers[i] < temp && 0 != sourceNumbers[i])
                    temp = sourceNumbers[i];

            return temp;
        }

        public static double GetMedianValue(double[] sourceNumbers)
        {
            //Framework 2.0 version of this method. there is an easier way in F4        
            if (sourceNumbers == null || sourceNumbers.Length == 0)
                return 0D;

            //make sure the list is sorted, but use a new array
            double[] sortedPNumbers = (double[])sourceNumbers.Clone();
            sourceNumbers.CopyTo(sortedPNumbers, 0);
            Array.Sort(sortedPNumbers);

            //get the median
            int size = sortedPNumbers.Length;
            int mid = size / 2;
            double median = (size % 2 != 0) ? (double)sortedPNumbers[mid] : ((double)sortedPNumbers[mid] + (double)sortedPNumbers[mid - 1]) / 2;
            return median;
        }

        public static double GetAvgValue(double[] sourceNumbers)
        {
            double temp = 0;

            if (sourceNumbers == null || sourceNumbers.Length == 0)
                return 0D;

            for (int i = 0; i < sourceNumbers.Length; i++)
                temp += sourceNumbers[i];

            return temp / sourceNumbers.Length;
        }

        static String Number2String(int number, bool isCaps)
        {
            Char c = (Char)((isCaps ? 65 : 97) + (number - 1));
            return c.ToString();
        }
    }
}
