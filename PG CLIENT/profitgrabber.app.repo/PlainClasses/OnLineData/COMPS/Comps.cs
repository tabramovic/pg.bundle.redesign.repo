﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;

//Added
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;


namespace DealMaker.PlainClasses.OnLineData.COMPS
{
    [Serializable]
    public class comps
    {
        string _streetNo;
        string _preDir;
        string _streetName;
        string _postDir;
        string _suffix;
        string _unitNo;
        string _zip;
        string _county;
        string _state;
        string _apn;
        string _sqft;        
        string _sqFtRange;
        string _allSalesInLast;
        string _searchRadius;
     
        private Guid propertyItemId;
        private DateTime dateOfRetrieve;
        private Decimal conservativeValue;
        private Decimal realisticValue;
        private Byte[] xmlStream;
        private String taggedCSV;
        private String taggedRealisticCSV;
        private String taggedConservativeCSV;
     
        public comps()
        {
        }

        public comps(Guid propertyItemId, DateTime dateOfRetrieve,
            Decimal conservative, Decimal realistic, Byte[] xmlStream,
            String taggedCSV)
        {
            this.propertyItemId = propertyItemId;
            this.dateOfRetrieve = dateOfRetrieve;
            this.conservativeValue = conservative;
            this.realisticValue = realistic;
            this.xmlStream = xmlStream;
            this.taggedCSV = taggedCSV;
        }
     
        [Description("propertyItemId")]
        public Guid P_propertyItemId
        {
            get
            {
                return this.propertyItemId;
            }
            set
            {
                this.propertyItemId = value;
            }
        }
        
        [Description("dateOfRetrieve")]
        public DateTime P_dateOfRetrieve
        {
            get
            {
                return this.dateOfRetrieve;
            }
            set
            {
                this.dateOfRetrieve = value;
            }
        }
        
        [Description("conservativeValue")]
        public Decimal P_ConservativeValue
        {
            get
            {
                return this.conservativeValue;
            }
            set
            {
                this.conservativeValue = value;
            }
        }
        
        [Description("realisticValue")]
        public Decimal P_RealisticValue
        {
            get
            {
                return this.realisticValue;
            }
            set
            {
                this.realisticValue = value;
            }
        }
       
        [Description("xmlStream")]
        public Byte[] P_xmlStream
        {
            get
            {
                return this.xmlStream;
            }
            set
            {
                this.xmlStream = value;
            }
        }
        
        [Description("taggedCSV")]
        public String P_taggedCSV
        {
            get
            {
                return this.taggedCSV;
            }
            set
            {
                this.taggedCSV = value;
            }
        }

        [Description("taggedRealisticCSV")]
        public String P_taggedRealisticCSV
        {
            get
            {
                if (null != this.taggedRealisticCSV)
                    return this.taggedRealisticCSV;
                else
                {
                    string[] csvs = this.taggedCSV.Split('#');
                    return csvs[2];
                }
            }
            set
            {
                this.taggedRealisticCSV = value;
            }
        }

        [Description("taggedConservativeCSV")]
        public String P_taggedConservativeCSV
        {
            get
            {
                if (null != this.taggedConservativeCSV)
                    return this.taggedConservativeCSV;
                else
                {
                    string[] csvs = this.taggedCSV.Split('#');
                    return csvs[1];
                }
            }
            set
            {
                this.taggedConservativeCSV = value;
            }
        }

        [Description("_streetNo")]
        public string StreetNo
        {
            get { return _streetNo; }
            set { _streetNo = value; }
        }

        [Description("_preDir")]
        public string PreDir
        {
            get { return _preDir; }
            set { _preDir = value; }
        }

        [Description("_streetName")]
        public string StreetName
        {
            get { return _streetName; }
            set { _streetName = value; }
        }

        [Description("_postDir")]
        public string PostDir
        {
            get { return _postDir; }
            set { _postDir = value; }
        }

        [Description("_suffix")]
        public string Suffix
        {
            get { return _suffix; }
            set { _suffix = value; }
        }

        [Description("_unitNo")]
        public string UnitNo
        {
            get { return _unitNo; }
            set { _unitNo = value; }
        }

        [Description("_zip")]
        public string Zip
        {
            get { return _zip; }
            set { _zip = value; }
        }

        [Description("_county")]
        public string County
        {
            get { return _county; }
            set { _county = value; }
        }

        [Description("_state")]
        public string State
        {
            get { return _state; }
            set { _state = value; }
        }

        [Description("_apn")]
        public string Apn
        {
            get { return _apn; }
            set { _apn = value; }
        }

        [Description("_sqft")]
        public string Sqft
        {
            get { return _sqft; }
            set { _sqft = value; }
        }        

        [Description("_sqFtRange")]
        public string SqFtRange
        {
            get { return _sqFtRange; }
            set { _sqFtRange = value; }
        }

        [Description("_allSalesInLast")]
        public string AllSalesInLast
        {
            get { return _allSalesInLast; }
            set { _allSalesInLast = value; }
        }

        [Description("_searchRadius")]
        public string SearchRadius
        {
            get { return _searchRadius; }
            set { _searchRadius = value; }
        }

        public byte[] ToByteArray()
        {                        
            BinaryFormatter bf = new BinaryFormatter();
            MemoryStream ms = new MemoryStream();
            bf.Serialize(ms, this);
            return ms.ToArray();
        }

        public bool GetObjectFromByteArray(byte[] arrBytes)
        {
            try
            {
                MemoryStream memStream = new MemoryStream();
                BinaryFormatter binForm = new BinaryFormatter();
                memStream.Write(arrBytes, 0, arrBytes.Length);
                memStream.Seek(0, SeekOrigin.Begin);
                comps cps = (comps)binForm.Deserialize(memStream);

                this._allSalesInLast = cps._allSalesInLast;
                this._apn = cps._apn;
                this._county = cps._county;
                this._postDir = cps._postDir;
                this._preDir = cps._preDir;
                this._searchRadius = cps._searchRadius;
                this._sqft = cps._sqft;
                this._sqFtRange = cps._sqFtRange;
                this._state = cps._state;
                this._streetName = cps._streetName;
                this._streetNo = cps._streetNo;
                this._suffix = cps._suffix;
                this._unitNo = cps._unitNo;
                this._zip = cps._zip;
                this.conservativeValue = cps.conservativeValue;
                this.propertyItemId = cps.propertyItemId;
                this.realisticValue = cps.realisticValue;
                this.taggedConservativeCSV = cps.taggedConservativeCSV;
                this.taggedCSV = cps.taggedCSV;
                this.taggedRealisticCSV = cps.taggedRealisticCSV;
                this.xmlStream = cps.xmlStream;
                this.dateOfRetrieve = cps.dateOfRetrieve;

                return true;
            }
            catch
            { return false; }
        }
    }
}
