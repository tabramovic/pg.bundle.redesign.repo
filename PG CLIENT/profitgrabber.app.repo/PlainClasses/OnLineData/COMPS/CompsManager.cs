﻿using System;
using System.Collections;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Data;
using System.Data.SqlClient;


//Added
using DealMaker.Interfaces;

namespace DealMaker.PlainClasses.OnLineData.COMPS
{
    public sealed class CompsManager
    {
        static readonly CompsManager _instance = new CompsManager();

        static CompsManager()
        { }

        CompsManager()
        { }

        public static CompsManager Instance
        {
            get { return _instance; }
        }

        string _path = string.Empty;
        string _documentId = string.Empty;
        comps _comps = new comps();

        public comps P_comps
        {
            get { return _comps; }
            set { _comps = value; }
        }

        public void ResetObject()
        {
            _comps = new comps();
        }

        public string GetCOMPSAddress()
        {
            string sFor = string.Empty;
            sFor += P_comps.StreetNo;
            if (sFor != string.Empty)
                sFor += " ";

            if (P_comps.PreDir != string.Empty)
            {
                sFor += P_comps.PreDir + " ";
            }

            if (P_comps.StreetName != string.Empty)
            {
                sFor += P_comps.StreetName + " ";
            }

            if (P_comps.PostDir != string.Empty)
            {
                sFor += P_comps.PostDir + " ";
            }

            if (P_comps.Suffix != string.Empty)
            {
                sFor += P_comps.Suffix + " ";
            }

            if (P_comps.UnitNo != string.Empty)
            {
                sFor += P_comps.UnitNo;
            }

            return sFor;
        }        
                
        public bool SaveData(Guid propertyItemId, DateTime dateOfRetrieve, decimal estimatedValue, decimal privateEstimatedValue, byte[] xmlStream, string taggedCSV)
        {
            comps[] cps = Select(propertyItemId);

            if (null == cps || 0 == cps.Length)
                return Insert(propertyItemId, dateOfRetrieve, estimatedValue, privateEstimatedValue, xmlStream, taggedCSV);
            else
                return Update(propertyItemId, dateOfRetrieve, estimatedValue, privateEstimatedValue, xmlStream, taggedCSV);
        }
        
        bool Insert(Guid propertyItemId, DateTime dateOfRetrieve, decimal estimatedValue, decimal privateEstimatedValue, byte[] xmlStream, string taggedCSV)
        {
            bool retVal = false;

            try
            {
                SqlCommand sp_insert_comps = new SqlCommand("insert_comps", Connection.getInstance().HatchConn);
                sp_insert_comps.CommandType = CommandType.StoredProcedure;

                sp_insert_comps.Parameters.AddWithValue("@propertyItemId", propertyItemId);
                sp_insert_comps.Parameters.AddWithValue("@dateOfRetrieve", dateOfRetrieve);
                sp_insert_comps.Parameters.AddWithValue("@estimatedValue", estimatedValue);

                sp_insert_comps.Parameters.AddWithValue("@privateEstimatedValue", privateEstimatedValue);
                sp_insert_comps.Parameters.AddWithValue("@xmlStream", xmlStream);
                sp_insert_comps.Parameters.AddWithValue("@taggedCSV", taggedCSV);
                sp_insert_comps.ExecuteNonQuery();

                retVal = true;
            }
            catch (Exception exc)
            {
                retVal = false;
            }

            return retVal;
        }

        bool Update(Guid propertyItemId, DateTime dateOfRetrieve, decimal estimatedValue, decimal privateEstimatedValue, byte[] xmlStream, string taggedCSV)
        {
            bool retVal = false;

            try
            {
                SqlCommand sp_update_comps = new SqlCommand("update_comps", Connection.getInstance().HatchConn);
                sp_update_comps.CommandType = CommandType.StoredProcedure;

                sp_update_comps.Parameters.AddWithValue("@propertyItemId", propertyItemId);
                sp_update_comps.Parameters.AddWithValue("@dateOfRetrieve", dateOfRetrieve);
                sp_update_comps.Parameters.AddWithValue("@estimatedValue", estimatedValue);
                sp_update_comps.Parameters.AddWithValue("@privateEstimatedValue", privateEstimatedValue);
                sp_update_comps.Parameters.AddWithValue("@xmlStream", xmlStream);
                sp_update_comps.Parameters.AddWithValue("@taggedCSV", taggedCSV);
                sp_update_comps.ExecuteNonQuery();

                retVal = true;
            }
            catch (Exception exc)
            {
                retVal = false;
            }

            return retVal;
        }        

        public comps[] Select(params object[] list)
        {
            ArrayList alObjects = new ArrayList();
            SqlDataReader reader = null;
            comps tempcomps = null;
            try
            {
                SqlCommand sp_select_comps = new SqlCommand("select_comps", Connection.getInstance().HatchConn);

                sp_select_comps.CommandType = CommandType.StoredProcedure;

                if (null != list && 0 != list.Length)
                {
                    sp_select_comps.Parameters.AddWithValue("@propertyItemId", (System.Guid)list[0]);
                }
                else
                {
                    sp_select_comps.Parameters.AddWithValue("@propertyItemId", DBNull.Value);
                }
                reader = sp_select_comps.ExecuteReader();
                while (reader.Read())
                {
                    System.Data.SqlTypes.SqlBinary sqlBinary4 = reader.GetSqlBinary(4);

                    comps cps = new comps();
                    if (cps.GetObjectFromByteArray((byte[])sqlBinary4.Value))   //NEW object model
                        alObjects.Add(cps);
                    else  //OLD object model (just get xml data)
                        cps = new comps(reader.GetGuid(0), reader.GetDateTime(1), reader.GetDecimal(2), reader.GetDecimal(3), (byte[])sqlBinary4.Value, reader.GetString(5));
                                        
                    alObjects.Add(cps);
                }
            }
            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {
                reader.Close();
            }
            return (comps[])alObjects.ToArray(typeof(comps));
        }
        
    }
}
