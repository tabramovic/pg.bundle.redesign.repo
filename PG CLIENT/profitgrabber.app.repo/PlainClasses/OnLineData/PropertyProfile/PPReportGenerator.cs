﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Printing;

//added
using DTD.Response;

namespace DealMaker.PlainClasses.OnLineData.PropertyProfile
{
    public class PPReportGenerator
    {
        public static Graphics PrintDataReport(PrintPageEventArgs e, PROPERTY property, int verticalOffset, int current, int total)
        {
            Graphics g = e.Graphics;
            Font font_16 = new Font("Arial", 16);
            Font font_14 = new Font("Arial", 13);
            Font font_12 = new Font("Arial", 11);
            Font font_10 = new Font("Arial", 10);
            Font font_10B = new Font("Arial", 10, FontStyle.Bold);
            Font font_12B = new Font("Arial", 11, FontStyle.Bold);
            Font font_9 = new Font("Arial", 8);
            Brush brush = new SolidBrush(Color.Black);
            Pen pen = new Pen(brush, 2);

            PPReportGenerator.PrintDataReportHeader(e, verticalOffset, current, total);

            PPReportGenerator.PrintReportFooter(e);

            //g.DrawLine(pen, new Point(25, 110 + verticalOffset), new Point(825, 110 + verticalOffset));

            //Subject Property
            string subjectProperty = "Subject Property: ";
            try
            {
                subjectProperty += property._PARSED_STREET_ADDRESS._HouseNumber + " " +
                                    property._PARSED_STREET_ADDRESS._StreetName + " " +
                                    property._City + ", " +
                                    property._State + " " +
                                    property._PostalCode + " ";
            }
            catch { }
            g.DrawString(subjectProperty, font_10B, brush, 50, 130 + verticalOffset);

            string subDivision = "Subdivision: ";
            try { subDivision += property._SubdivisionIdentifier; }
            catch { }
            g.DrawString(subDivision, font_10, brush, 75, 165 + verticalOffset);

            //g.DrawLine(pen, new Point(25, 180 + verticalOffset), new Point(825, 180 + verticalOffset));

            //Owner Name
            string ownerName = "Owner Name: ";
            try { ownerName += property.PROPERTY_OWNER[0]._OwnerName; }
            catch { }
            g.DrawString(ownerName, font_10B, brush, 50, 200 + verticalOffset);

            string phoneNumber = "Phone Number: ";
            try { phoneNumber += property.PROPERTY_OWNER[0]._PhoneNumber; }
            catch { }
            g.DrawString(phoneNumber, font_10, brush, 555, 200 + verticalOffset);

            string mailingAddress = "Mailing Address: ";
            try { mailingAddress += property.PROPERTY_OWNER[0]._MailingAddress + " " + property.PROPERTY_OWNER[0]._MailingCityAndState + ", " + property.PROPERTY_OWNER[0]._MailingPostalCode; }
            catch { }
            g.DrawString(mailingAddress, font_10, brush, 75, 235 + verticalOffset);

            //g.DrawLine(pen, new Point(25, 255 + verticalOffset), new Point(825, 255 + verticalOffset));

            //Property Characteristics
            g.DrawString("Property Characteristics:", font_12B, brush, 50, 285 + verticalOffset);

            string totalLivingArea = "Total Living Area: ";
            try
            {
                totalLivingArea += Convert.ToDouble(property._PROPERTY_CHARACTERISTICS._IMPROVEMENTS._ROOM_COUNT._TotalLivingAreaSquareFeetNumber).ToString("N");
                totalLivingArea = totalLivingArea.Substring(0, totalLivingArea.Length - 3);
            }
            catch { }
            g.DrawString(totalLivingArea, font_10, brush, 50, 305 + verticalOffset);

            string yearBuilt = "Year Built: ";
            try { yearBuilt += property._PROPERTY_CHARACTERISTICS._IMPROVEMENTS._GENERAL_DESCRIPTION._YearBuiltDateIdentifier; }
            catch { }
            g.DrawString(yearBuilt, font_10, brush, 300, 305 + verticalOffset);

            string totalRooms = "Total Rooms: ";
            try { totalRooms += property._PROPERTY_CHARACTERISTICS._IMPROVEMENTS._ROOM_COUNT._TotalRoomCount; }
            catch { }
            g.DrawString(totalRooms, font_10, brush, 555, 305 + verticalOffset);

            string lotArea = "Lot Area: ";
            try
            {
                lotArea += Convert.ToDouble(property._PROPERTY_CHARACTERISTICS._SITE._DIMENSIONS._LotAreaSquareFeetNumber).ToString("N");
                lotArea = lotArea.Substring(0, lotArea.Length - 3);
            }
            catch { }
            g.DrawString(lotArea, font_10, brush, 50, 325 + verticalOffset);

            string parkingType = "Parking Type: ";
            try { parkingType += property._PROPERTY_CHARACTERISTICS._IMPROVEMENTS._PARKING._TypeDescription; }
            catch { }
            g.DrawString(parkingType, font_10, brush, 300, 325 + verticalOffset);

            string totalBedrooms = "Total Bedrooms: ";
            try { totalBedrooms += property._PROPERTY_CHARACTERISTICS._IMPROVEMENTS._ROOM_COUNT._TotalBedroomsCount; } catch { }
            g.DrawString(totalBedrooms, font_10, brush, 555, 325 + verticalOffset);            

            string pool = "Pool: ";
            try { pool += property._PROPERTY_CHARACTERISTICS._IMPROVEMENTS._FEATURES._POOL._HasFeatureIndicator; }
            catch { }
            g.DrawString(pool, font_10, brush, 50, 345 + verticalOffset);

            string garage = "Garage: ";
            try { garage += property._PROPERTY_CHARACTERISTICS._IMPROVEMENTS._PARKING._TypeDescription; }
            catch { }
            g.DrawString(garage, font_10, brush, 300, 345 + verticalOffset);

            string totalBaths = "Total Baths: ";
            try { totalBaths += property._PROPERTY_CHARACTERISTICS._IMPROVEMENTS._ROOM_COUNT._TotalBathsCount; }
            catch { }
            g.DrawString(totalBaths, font_10, brush, 555, 345 + verticalOffset);

            string fireplaces = "Fireplaces: ";
            try { fireplaces += property._PROPERTY_CHARACTERISTICS._IMPROVEMENTS._FEATURES._FIREPLACES._HasFeatureIndicator; }
            catch { }
            g.DrawString(fireplaces, font_10, brush, 555, 365 + verticalOffset);

            string zoning = "Zoning: ";
            try { zoning += property._PROPERTY_CHARACTERISTICS._SITE._ZONING._ClassificationIdentifier; }
            catch { }
            g.DrawString(zoning, font_10, brush, 50, 365 + verticalOffset);

            string landUse = "Land Use: ";
            try { landUse += property._PROPERTY_CHARACTERISTICS._SITE._CHARACTERISTICS._LandUseDescription; }
            catch { }
            g.DrawString(landUse, font_10, brush, 300, 365 + verticalOffset);

            //g.DrawLine(pen, new Point(25, 385 + verticalOffset), new Point(825, 385 + verticalOffset));

            //Financials
            g.DrawString("Financials:", font_12B, brush, 50, 400 + verticalOffset);
            string lastSaleDate = "Last Sale Date: ";
            try
            {
                string lsd = property._PROPERTY_HISTORY._SALES_HISTORY[0]._LastSalesDate;
                DateTime d = new DateTime
                    (
                    (int.Parse(lsd.Substring(0, 4))),
                    (int.Parse(lsd.Substring(4, 2))),
                    (int.Parse(lsd.Substring(6, 2)))
                    );


                lastSaleDate += d.ToShortDateString();
            }
            catch { }
            g.DrawString(lastSaleDate, font_10, brush, 50, 420 + verticalOffset);

            string lastSalePrice = "Last Sale Price: ";
            try { lastSalePrice += Convert.ToDouble(property._PROPERTY_HISTORY._SALES_HISTORY[0]._LastSalesPriceAmount).ToString("C"); }
            catch { }
            g.DrawString(lastSalePrice, font_10, brush, 300, 420 + verticalOffset);

            string pricePerSqFt = "Price Per Sq.Ft.: ";
            try { pricePerSqFt += Convert.ToDouble(property._PROPERTY_HISTORY._SALES_HISTORY[0]._PricePerSquareFootAmount).ToString("C"); }
            catch { }
            g.DrawString(pricePerSqFt, font_10, brush, 555, 420 + verticalOffset);

            string firstLoan = "First Loan: ";
            try {firstLoan += Convert.ToDouble(property._PROPERTY_HISTORY._MORTGAGE_HISTORY[0]._FirstMortgageAmount).ToString("C"); } catch {}
            g.DrawString(firstLoan, font_10, brush, 50, 440 + verticalOffset);

            string secondLoan = "Second Loan: ";
            try {secondLoan += Convert.ToDouble(property._PROPERTY_HISTORY._MORTGAGE_HISTORY[0]._SecondMortgageAmount).ToString("C"); } catch { }
            g.DrawString(secondLoan, font_10, brush, 300, 440 + verticalOffset);

            string cashDown = "Cash Down: ";
            try { cashDown += Convert.ToDouble(property._PROPERTY_HISTORY._SALES_HISTORY[0]._CashDownAmount).ToString("C"); }
            catch { }
            g.DrawString(cashDown, font_10, brush, 555, 440 + verticalOffset);

            string deedType = "Deed Type: ";
            try { deedType += property._PROPERTY_HISTORY._SALES_HISTORY[0]._DeedTypeDescription; }
            catch { }
            g.DrawString(deedType, font_10, brush, 50, 460 + verticalOffset);

            string recDate = "Rec. Date: ";
            try
            {
                string rd = property._PROPERTY_HISTORY._SALES_HISTORY[0]._LastRecordingDate;
                DateTime d = new DateTime
                    (
                    (int.Parse(rd.Substring(0, 4))),
                    (int.Parse(rd.Substring(4, 2))),
                    (int.Parse(rd.Substring(6, 2)))
                    );

                recDate += d.ToShortDateString();
            }
            catch { }
            g.DrawString(recDate, font_10, brush, 300, 460 + verticalOffset);

            string docNo = "Doc.No.: ";
            try
            { docNo += property._PROPERTY_HISTORY._SALES_HISTORY[0]._DocumentNumberIdentifier; }
            catch { }
            g.DrawString(docNo, font_10, brush, 555, 460 + verticalOffset);

            //string sellerName = "Seller Name: ";			
            //try {sellerName += property.PROPERTY_OWNER[0]._OwnerName;}  catch {}
            //g.DrawString(sellerName, font_12, brush, 600, 455 + verticalOffset);

            //g.DrawLine(pen, new Point(25, 470 + verticalOffset), new Point(825, 470 + verticalOffset));

            //Taxes
            g.DrawString("Taxes:", font_12B, brush, 50, 495 + verticalOffset);

            string apn = "APN: ";
            try { apn += property._AssessorsParcelIdentifier; }
            catch { }
            g.DrawString(apn, font_10, brush, 150, 495 + verticalOffset);

            string propTax = "Prop. Tax.: ";
            try { propTax += Convert.ToDouble(property._PROPERTY_TAX._RealEstateTotalTaxAmount).ToString("C"); }
            catch { }
            g.DrawString(propTax, font_10, brush, 50, 515 + verticalOffset);

            string assesmYear = "Assesm. Year: ";
            try { assesmYear += property._PROPERTY_TAX._AssessmentYear; }
            catch { }
            g.DrawString(assesmYear, font_10, brush, 300, 515 + verticalOffset);

            string deliqYear = "Deliq. Year: ";
            try { deliqYear += property._PROPERTY_TAX._DelinquentYear; }
            catch { }
            g.DrawString(deliqYear, font_10, brush, 555, 515 + verticalOffset);

            string taxArea = "Tax Area: ";
            try { deliqYear += property._PROPERTY_TAX._RateAreaIdentifier; }
            catch { }
            g.DrawString(taxArea, font_10, brush, 50, 535 + verticalOffset);

            string taxYear = "Tax Year: ";
            try { taxYear += property._PROPERTY_TAX._TaxYear; }
            catch { }
            g.DrawString(taxYear, font_10, brush, 300, 535 + verticalOffset);

            string exempType = "Exempt. Type: ";
            try { exempType += property._PROPERTY_TAX._ExemptionTypeDescription; }
            catch { }
            g.DrawString(exempType, font_10, brush, 555, 535 + verticalOffset);

            string totalAssessedValue = "Assessed Value: ";
            try { totalAssessedValue += Convert.ToDouble(property._PROPERTY_TAX._TotalAssessedValueAmount).ToString("C"); }
            catch { }
            g.DrawString(totalAssessedValue, font_10, brush, 50, 555 + verticalOffset);

            string landValue = "Land Use: ";
            try { landValue += property._PROPERTY_TAX._LandValueAmount; }
            catch { }
            g.DrawString(landValue, font_10, brush, 300, 555 + verticalOffset);

            string improvementValue = "Improvement Value: ";
            try { improvementValue += Convert.ToDouble(property._PROPERTY_TAX._ImprovementValueAmount).ToString("C"); }
            catch { }
            g.DrawString(improvementValue, font_10, brush, 555, 555 + verticalOffset);

            //g.DrawLine(pen, new Point(25, 565 + verticalOffset), new Point(825, 565 + verticalOffset));

            //Legal Description			
            string legalDesc = string.Empty;
            try { legalDesc += property._LEGAL_DESCRIPTION._TextDescription; }
            catch { }
            g.DrawString("Legal Description: ", font_12B, brush, 50, 590 + verticalOffset);
            g.DrawString(legalDesc, font_10, brush, 75, 610 + verticalOffset);


            //Additional
            g.DrawString("Additional:", font_12B, brush, 50, 640 + verticalOffset);
            string schoolDistrict = "School District: ";
            try { schoolDistrict += property._NEIGHBORHOOD_AREA_INFORMATION._SchoolDistrictName; }
            catch { }
            g.DrawString(schoolDistrict, font_10, brush, 50, 660 + verticalOffset);

            string mapRefOne = "Map Ref. One: ";
            try { mapRefOne += property._NEIGHBORHOOD_AREA_INFORMATION._MapReferenceOneIdentifier; }
            catch { }
            g.DrawString(mapRefOne, font_10, brush, 300, 660 + verticalOffset);

            string mapRefTwo = "Map Ref Two:";
            try { mapRefTwo += property._NEIGHBORHOOD_AREA_INFORMATION._MapReferenceTwoIdentifier; }
            catch { }
            g.DrawString(mapRefTwo, font_10, brush, 600, 660 + verticalOffset);

            //g.DrawLine(pen, new Point(25, 620 + verticalOffset), new Point(825, 620 + verticalOffset));


            return g;

        }

        public static Graphics PrintDataReportHeader(PrintPageEventArgs e, int headerOffset, int current, int total)
        {
            Graphics g = e.Graphics;
            Font font_12 = new Font("Arial", 12);
            Font font_14 = new Font("Arial", 14);
            Font font_14B = new Font("Arial", 14, FontStyle.Bold);
            Font font_13B = new Font("Arial", 13, FontStyle.Bold);
            Brush brush = new SolidBrush(Color.Black);

            if (1 == total)
            {
                g.DrawString("SUBJECT PROPERTY PROFILE DATA REPORT", font_13B, brush, 215, 50 + headerOffset);                
            }
            else
            {
                g.DrawString("COMPARABLE " + (current + 1).ToString() + " PROPERTY PROFILE", font_13B, brush, 230, 50 + headerOffset);
                g.DrawString("DATA REPORT", font_13B, brush, 335, 75 + headerOffset);
            }

            if (0 == headerOffset)
                g.DrawString(DateTime.Now.ToShortDateString(), font_12, brush, 700, 30 + headerOffset);

            return g;
        }

        public static Graphics PrintReportFooter(PrintPageEventArgs e)
        {
            Graphics g = e.Graphics;

            Font font_10I = new Font("Arial", 10, FontStyle.Italic);
            Brush brush = new SolidBrush(Color.Black);

            g.DrawString("ULTIMATE DATA SOURCE REPORTS. Copyright TKS Inc. All rights reserved.", font_10I, brush, 50, 975);

            return g;
        }
    }
}
