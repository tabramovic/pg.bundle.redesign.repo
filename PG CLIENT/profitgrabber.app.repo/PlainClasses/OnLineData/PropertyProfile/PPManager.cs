﻿using System;
using System.Collections;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Data;
using System.Data.SqlClient;


//Added
using DealMaker.Interfaces;


namespace DealMaker.PlainClasses.OnLineData.PropertyProfile
{
    public sealed class PPManager //: IExternalDocumentControl
    {
        static readonly PPManager _instance = new PPManager();

        static PPManager()
        {}

        PPManager()
        {}

        public static PPManager Instance
        {
            get {return _instance;}
        }

        string _path = string.Empty;
        string _documentId = string.Empty;
        PropertyProfile _propertyProfile = new PropertyProfile();

        public void ResetObject()
        {
            _propertyProfile = new PropertyProfile();
        }

        public PropertyProfile P_PropertyProfile
        {
            get { return _propertyProfile; }
            set { _propertyProfile = value; }
        }                        

        public bool LoadData()
        {
            return true;
        }

        public bool SaveData(Guid propertyItemId, byte[] xmlStream)
        {
            PropertyProfile[] ppFound = Select(new object[] { propertyItemId });

            if (null == ppFound || 0 == ppFound.Length)
                return Insert(propertyItemId, xmlStream);
            else
                return Update(propertyItemId, xmlStream);
        }        
        
        bool Insert(Guid propertyItemId, byte[] xmlStream)
        {
            bool retVal = false;

            try
            {
                SqlCommand sp_insert_PropertyProfile = new SqlCommand("insert_PropertyProfile", Connection.getInstance().HatchConn);
                sp_insert_PropertyProfile.CommandType = CommandType.StoredProcedure;
                sp_insert_PropertyProfile.Parameters.AddWithValue("@propertyItemId", propertyItemId);
                sp_insert_PropertyProfile.Parameters.AddWithValue("@xmlStream", xmlStream);
                sp_insert_PropertyProfile.ExecuteNonQuery();
                retVal = true;
            }
            catch (Exception exc)
            {
                retVal = false;
            }
            return retVal;
        }

        bool Update(Guid propertyItemId, byte[] xmlStream)
        {
            bool retVal = false;

            try
            {
                SqlCommand sp_update_PropertyProfile = new SqlCommand("update_PropertyProfile", Connection.getInstance().HatchConn);
                sp_update_PropertyProfile.CommandType = CommandType.StoredProcedure;

                sp_update_PropertyProfile.Parameters.AddWithValue("@propertyItemId", propertyItemId);
                sp_update_PropertyProfile.Parameters.AddWithValue("@xmlStream", xmlStream);
                sp_update_PropertyProfile.ExecuteNonQuery();
                retVal = true;
            }
            catch (Exception exc)
            {
                retVal = false;
            }
            return retVal;
        }

        public int Delete(Guid propertyItemId)
        {
            int retVal = 0;

            try
            {
                SqlCommand sp_delete_PropertyProfile = new SqlCommand("delete_PropertyProfile", Connection.getInstance().HatchConn);
                sp_delete_PropertyProfile.CommandType = CommandType.StoredProcedure;

                sp_delete_PropertyProfile.Parameters.AddWithValue("@propertyItemId", propertyItemId);
                retVal = sp_delete_PropertyProfile.ExecuteNonQuery();
            }
            catch (Exception exc)
            {
                throw exc;
            }
            return retVal;
        }

        public PropertyProfile[] Select(params object[] list)
        {
            ArrayList alObjects = new ArrayList();
            SqlDataReader reader = null;
            PropertyProfile tempPropertyProfile = null;
            try
            {
                SqlCommand sp_select_PropertyProfile = new SqlCommand("select_PropertyProfile", Connection.getInstance().HatchConn);

                sp_select_PropertyProfile.CommandType = CommandType.StoredProcedure;

                if (null != list && 0 != list.Length)
                {
                    sp_select_PropertyProfile.Parameters.AddWithValue("@propertyItemId", (System.Guid)list[0]);
                }
                else
                {
                    sp_select_PropertyProfile.Parameters.AddWithValue("@propertyItemId", DBNull.Value);
                }
                reader = sp_select_PropertyProfile.ExecuteReader();
                while (reader.Read())
                {
                    System.Data.SqlTypes.SqlBinary sqlBinary1 = reader.GetSqlBinary(1);
                    tempPropertyProfile = new PropertyProfile(reader.GetGuid(0), (byte[])sqlBinary1.Value);
                    alObjects.Add(tempPropertyProfile);
                }
            }
            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {
                reader.Close();
            }
            return (PropertyProfile[]) alObjects.ToArray(typeof(PropertyProfile));
        }        
    }
}
