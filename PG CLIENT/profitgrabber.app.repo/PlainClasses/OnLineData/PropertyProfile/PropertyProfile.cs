﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;

namespace DealMaker.PlainClasses.OnLineData.PropertyProfile
{
    public class PropertyProfile
    {
        
        private Guid propertyItemId;
        private Byte[] xmlStream;
        
        public PropertyProfile()
        {
        }

        public PropertyProfile(Guid propertyItemId, Byte[]
            xmlStream)
        {
            this.propertyItemId = propertyItemId;
            this.xmlStream = xmlStream;
        }
        
        [Description("propertyItemId")]
        public Guid P_propertyItemId
        {
            get
            {
                return this.propertyItemId;
            }
            set
            {
                this.propertyItemId = value;
            }
        }
        
        [Description("xmlStream")]
        public Byte[] P_xmlStream
        {
            get
            {
                return this.xmlStream;
            }
            set
            {
                this.xmlStream = value;
            }
        }                        
    }
}
