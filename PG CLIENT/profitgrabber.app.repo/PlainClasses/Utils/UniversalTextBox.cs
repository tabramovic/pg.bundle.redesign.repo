using System;
using System.Windows.Forms;
using System.Data;
using System.Drawing;
using System.Text;
using System.Text.RegularExpressions;


namespace DealMaker
{
	/// <summary>
	/// TextBox with extra functionality implemented:
	///   1. mask-matching with RegularExpressions
	///   2. keypress intercepting
	///   
	///   Example RegularExpressions
	///   <list type="">
	///     Character     /./ 
	///     Letter        /^$|[a-z]/i 
	///     Phone number  /^$|^\+?[0-9\/\s\\\/\-\.]+$/ 
	///     Member        /^\d{3}$/ 
	///     Zip =         /^$|^[A-Za-z]+[\-\s]{1}\d{4,5}$|^\d{4,5}$|^\d{5}[\-\s]?\d{4}$/ 
	///     EMail         /^$|^$|^[a-z][a-z_0-9\.\-]+@[a-z_0-9\.\-]+\.[a-z]{2,3}$/i 
	///     Pledge        /^\d*$|^\d*\.\d{2}$/ 
	///     Date          /^$|^[0-9]{1,2}\.[0-9]{1,2}\.[0-9]{4}$/ 
	///     Numeric       /^$|^[0-9]*$/    
	///     Numeric       /^$|(^[0-9]*\.[0-9]{3}(,[0-9]{0,2}){0,1}$)|(^[0-9]{1,3}(,[0-9]{0,2}){0,1}$)/ 
	///     Password      /.{8,8}/ 
	///   </list>
	/// </summary>
	public 
		class 
		NumericTextBox 
		: 
		System.Windows.Forms.TextBox
	{
		//========================================================================
		/// <summary>
		/// Regular expression for Mask of the TextBox
		/// </summary>
		private Regex   _regex_mask;

		//========================================================================
		/// <summary>
		/// string for Regex pattern (needed for designer)
		/// </summary>
		private string  _str_regex_mask;
		private string  _str_regex_err_msg = string.Empty;

		//////////////////////////////////////////////////////////////////////////
		public
			NumericTextBox
			(
			)
		{
			//this.KeyPress += new KeyPressEventHandler(Function2HandleKeyPressEvent);			
			this.RegularExpressionMask = @"^([1-9]{1}[\d]{0,2}(\,[\d]{3})*(\.[\d]{0,2})?|[1-9]{1}[\d]{0,}(\.[\d]{0,2})?|0(\.[\d]{0,2})?|(\.[\d]{1,2})?)$";
			this.RegexErrorMessage = "Not numeric value !";
		}
		//////////////////////////////////////////////////////////////////////////
		/// <summary>
		/// RegularExpressionMask property
		///   for setting the Regex pattern
		/// </summary>
		public string RegularExpressionMask
		{
			get
			{
				return _str_regex_mask;
			}
			set
			{
				if (value != _str_regex_mask)
				{
					// Set the property value
					_str_regex_mask = value;
					_regex_mask = null;
					_regex_mask = new Regex(value);
				}
			}
		}

		#region � RegexErrorMessage �
		public string RegexErrorMessage
		{
			get {return this._str_regex_err_msg;}
			set {this._str_regex_err_msg = value;}
		}
		#endregion � RegexErrorMessage �


		//////////////////////////////////////////////////////////////////////////
		/// <summary>
		/// function to test whether text in the TextBox matches RegularExpression
		/// for mask.
		/// </summary>
		/// <returns>
		/// boolean value if the text matched regular expression
		/// </returns>
		public
			bool
			matched
			(
			)
		{
			// Find a single match in the string.
			Match m = _regex_mask.Match(this.Text.ToString()); 
			if (m.Success) 
			{
				return true;
			}
			return false;
		}
		//////////////////////////////////////////////////////////////////////////
		private
			void
			Function2HandleKeyPressEvent
			(
			object sender
			, KeyPressEventArgs event_args
			)
		{

		} 
		//////////////////////////////////////////////////////////////////////////
		protected
			override
			void
			OnLeave
			(
			EventArgs event_args
			)
		{
			if ( ! this.matched() )
			{
				this.SelectAll();
				this.Focus();

				// Create the ToolTip and associate with the Form container.
				ToolTip toolTip1 = new ToolTip();

				// Set up the delays for the ToolTip.
				toolTip1.AutoPopDelay = 5000;
				toolTip1.InitialDelay = 10;
				toolTip1.ReshowDelay = 500;
				// Force the ToolTip text to be displayed whether or not the form is active.
				toolTip1.ShowAlways = true;
      
				// Set up the ToolTip text for the Button and Checkbox.
				toolTip1.SetToolTip(this, this._str_regex_err_msg);				
			}
		}
		//////////////////////////////////////////////////////////////////////////
	} 
} 
