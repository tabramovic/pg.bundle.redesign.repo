﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace DealMaker.PlainClasses.Utils
{
    public sealed class ContentCopier
    {
        static readonly ContentCopier _instance = new ContentCopier();

        static ContentCopier()
        { }

        ContentCopier()
        { }

        public static ContentCopier Instance
        {
            get { return _instance; }
        }

        const string ConfigIdentifier = "SSXContentTemplate";
        string _startUpPath = string.Empty;

        public string StartUpPath
        {
            get { return _startUpPath; }
            set { _startUpPath = value; }
        }

        public void SaveOriginPath(string originPath)
        {

            System.Configuration.Configuration config = System.Configuration.ConfigurationManager.OpenExeConfiguration(System.Configuration.ConfigurationUserLevel.None);

            if (null == config.AppSettings.Settings[ConfigIdentifier])
                config.AppSettings.Settings.Add(ConfigIdentifier, originPath);
            else            
                config.AppSettings.Settings[ConfigIdentifier].Value = originPath;                
            
            config.Save(System.Configuration.ConfigurationSaveMode.Modified);
            System.Configuration.ConfigurationManager.RefreshSection("appSettings");

        }

        public string GetOriginPath()
        {
            string path = string.Empty;

            try 
            { 
                path = System.Configuration.ConfigurationManager.AppSettings[ConfigIdentifier]; 
                if (null == path)
                    path = StartUpPath;
            }
            catch { path = StartUpPath; }

            return path;

        }
       

        public bool CopyFolder(string sourceFolder, string destFolder, out string error)
        {
            error = string.Empty;
            try
            {
                if (!Directory.Exists(destFolder))
                    Directory.CreateDirectory(destFolder);
                string[] files = Directory.GetFiles(sourceFolder);
                foreach (string file in files)
                {
                    string name = Path.GetFileName(file);
                    string dest = Path.Combine(destFolder, name);
                    File.Copy(file, dest);
                }
                string[] folders = Directory.GetDirectories(sourceFolder);
                foreach (string folder in folders)
                {
                    string name = Path.GetFileName(folder);
                    string dest = Path.Combine(destFolder, name);
                    CopyFolder(folder, dest, out error);
                }
            }
            catch (Exception exc)
            {
                error = exc.Message;
                return false;
            }
            return true;
        }
    }
}
