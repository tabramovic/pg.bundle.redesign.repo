using System;

namespace DealMaker
{
	/// <summary>
	/// Summary description for LockingObject.
	/// </summary>
	public class LockingObject
	{
		public bool enableCOMPS = false;
		public bool enableGFR = false;
		public bool enableMergeModule = false;
		
		public LockingObject()
		{			
		}

		public LockingObject(string unlockSequence)
		{
			if (3 != unlockSequence.Length)
			{
				this.enableCOMPS = false;
				this.enableGFR = false;
				this.enableMergeModule = false;
			}
			else
			{
				string comps = unlockSequence.Substring(0, 1);
				string gfr = unlockSequence.Substring(1, 1);
				string mergeModule= unlockSequence.Substring(2, 1);

				try {this.enableCOMPS = Convert.ToBoolean(comps);}
				catch{this.enableCOMPS = false;}
				try {this.enableGFR = Convert.ToBoolean(gfr);}
				catch{this.enableGFR = false;}
				try {this.enableMergeModule = Convert.ToBoolean(mergeModule);}
				catch{this.enableMergeModule = false;}
			}
		}

		public LockingObject(bool enableCOMPS, bool enableGFR, bool enableMergeModule)
		{
			this.enableCOMPS = enableCOMPS;
			this.enableGFR = enableGFR;
			this.enableMergeModule = enableMergeModule;
		}

		public bool EnableCOMPS
		{
			get {return this.enableCOMPS;}
			set {this.enableCOMPS = value;}
		}

		public bool EnableGFR
		{
			get {return this.enableGFR;}
			set {this.enableGFR = value;}
		}

		public bool EnableMergeModule
		{
			get {return this.enableMergeModule;}
			set {this.enableMergeModule = value;}
		}
	}
}
