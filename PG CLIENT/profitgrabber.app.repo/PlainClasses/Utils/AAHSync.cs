﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.Threading;
using System.IO;
using System.Collections.Specialized;
using System.Configuration;
using System.Windows.Forms;

//Added
//using DealMaker.AAHIntegration;
using DealMaker.PlainClasses.ContactTypes;
using DealMaker.WebSitesSupportService;

namespace DealMaker.PlainClasses.Utils
{
	/// <summary>
    /// Summary description for AAHSync.
	/// </summary>
	public class AAHSync
    {
        #region DATA
        private DateTime _aahTimeStamp = new DateTime(2000, 1, 1);
        private DateTime _npcTimeStamp = new DateTime(2000, 1, 1);
        private DateTime _acokTimeStamp = new DateTime(2000, 1, 1);
        private DateTime _vcplTimeStamp = new DateTime(2000, 1, 1);
        private DateTime _cbTimeStamp = new DateTime(2000, 1, 1);
        private DateTime _plTimeStamp = new DateTime(2000, 1, 1);

		private SqlConnection dbConnection;
		private ArrayList alGroupsToRefresh = new ArrayList();
        string _webSupportFolder = string.Empty;

		private int initialDelay = 10000;
		private int nextWaitingPeriod = 600000;				
		public delegate void NewGroupAddedBy123Sold(int nodeId);
		public static event NewGroupAddedBy123Sold REFRESH_GROUPS_BY_123SOLD;
		
		public delegate void NewContactInGroupFrom123Sold(int nodeId);
		public static event NewContactInGroupFrom123Sold NEW_CONTACT_IN_GROUP_BY_123_SOLD;

		protected void OnNewGroup(int nodeId)
		{
			if (null != REFRESH_GROUPS_BY_123SOLD)
			{
				REFRESH_GROUPS_BY_123SOLD(nodeId);
			}
		}

		protected void OnNewContact(int nodeId)
		{
			if (null != NEW_CONTACT_IN_GROUP_BY_123_SOLD)
			{
				NEW_CONTACT_IN_GROUP_BY_123_SOLD(nodeId);
			}
		}


		private Thread worker = null;
		private const string AAHLastFetchFileName = @"\AAHLastFetch.txt";
        private const string NPCLastFetchFileName = @"\NPCLastFetch.txt";
        private const string ACOKLastFetchFileName = @"\ACOKLastFetch.txt";
        private const string VCPLLastFetchFileName = @"\VCPLLastFetch.txt";
        private const string CBLastFetchFileName = @"\CBLastFetch.txt";
        private const string PLLastFetchFileName = @"\PLLastFetch.txt";
		private const int LeadsNotProcessedYetGroupId = 6;
		private const string AutoLeadsGroups = "Auto-Leads Groups";        
		private bool firstTimeRun = true;
		private DateTime _aahLastFetch = new DateTime(2006, 1, 1);
        private DateTime _npcLastFetch = new DateTime(2006, 1, 1);
        private DateTime _acokLastFetch = new DateTime(2006, 1, 1);
        private DateTime _vcplLastFetch = new DateTime(2006, 1, 1);
        private DateTime _cbLastFetch = new DateTime(2006, 1, 1);
        private DateTime _plLastFetch = new DateTime(2006, 1, 1);		

        //DealMaker.AAHIntegration.Service1 service123Sold = null;
        //DealMaker.AAHIntegration.CompositeContactOriginDealProperty[] compositeProperties = null;		
        //DealMaker.AAHIntegration.Contact _contact = new DealMaker.AAHIntegration.Contact();
        WebSitesSupportService.WebProxyService webProxyService = null;
        string[][] aahWebServiceResults = null;
        string[][] npcWebServiceResults = null;
        string[][] acokWebServiceResults = null;
        string[][] vcplWebServiceResults = null;
        string[][] cbWebServiceResults = null;
        string[][] plWebServiceResults = null;
        #endregion

        #region CTR
        public AAHSync(string userAppFolder)
		{
            _webSupportFolder = userAppFolder;

            if (Globals.NwVerEnabled)
            {
                //this.dbConnection = new SqlConnection(@"user id=sa;password=" + Globals.DatabasePwd + ";persist security info=False;Data Source=" + Globals.DatabaseServer + ";Initial Catalog=" + Globals.DatabaseName + ";");
                this.dbConnection = new SqlConnection(DealMaker.Network.Util.GetConnectionStringFromServerName(Globals.DatabaseServer));
            }
            else
                this.dbConnection = new SqlConnection(@"Data Source=" + Globals.DatabaseServer + ";Initial Catalog=" + Globals.DatabaseName + ";Integrated Security=SSPI;");


            webProxyService = new WebProxyService(); // new DealMaker.AAHIntegration.Service1();

            try { webProxyService.Url = ConfigurationManager.AppSettings["WSS"]; }
            catch { }

			InitLastFetchTime();

            try
            {
                string url = ConfigurationManager.AppSettings["WebSitesSupport"];
                webProxyService.Url = url;
            }
            catch // use default
            { }


            webProxyService.Timeout = 90000;
            
						
			this.worker = new Thread(new ThreadStart(CheckForNewEntries));			
			worker.Start();
        }
        #endregion



        private void CheckForNewEntries()
		{            
			int mSecs = 0;	
			while (!Globals.AbortAllThreads)
			{
				int delay = 0;
				if (this.firstTimeRun)
				{
					delay = this.initialDelay;
					this.firstTimeRun = false;		
				}
				else				
					delay = this.nextWaitingPeriod;
				

				mSecs = 0;				
				while (mSecs < delay && !Globals.AbortAllThreads)
				{
					if (Globals.breakWaitingPeriod)												
						break;
					
					Thread.Sleep(1000);						
					mSecs += 1000;
				}
				if (true == Globals.AbortAllThreads)				
					break;
																

				try
				{

                    //Guid testAAHId = new Guid("09877309-ac82-48a8-b878-14379c7c5c1f");
                    //Guid testNPCId = new Guid("849dec34-99d3-426e-8553-1ea482b5b772");
                    //Globals.UserRegistration.IDUser = testAAHId;

                    aahWebServiceResults = webProxyService.GetAAHEntries(Globals.UserRegistration.IDUser, this._aahTimeStamp.Ticks);
                    npcWebServiceResults = webProxyService.GetNPCEntries(Globals.UserRegistration.IDUser, this._npcTimeStamp.Ticks);
                    acokWebServiceResults = webProxyService.GetACOKEntries(Globals.UserRegistration.IDUser, this._acokTimeStamp.Ticks);
                    vcplWebServiceResults = webProxyService.GetPGDB3Entries(Globals.UserRegistration.IDUser, this._vcplTimeStamp.Ticks);
                    //Globals.UserRegistration.IDUser = Guid.Parse("5BC78D0D-84EA-40FD-846C-D9933550D077");
                    cbWebServiceResults = webProxyService.GetCBEntries(Globals.UserRegistration.IDUser, this._cbTimeStamp.Ticks);
                    plWebServiceResults = webProxyService.GetPLEntries(Globals.UserRegistration.IDUser, this._plTimeStamp.Ticks);

				}
				catch (Exception e)
				{
					System.Windows.Forms.MessageBox.Show("WS could not be contacted" + System.Environment.NewLine + e.Message);
				}
				finally
				{
					//this.lastFetch = DateTime.Now;
				}

                if (Globals.breakWaitingPeriod)
                {
                    if ((null == aahWebServiceResults || 0 == aahWebServiceResults.Length) &&
                        (null == npcWebServiceResults || 0 == npcWebServiceResults.Length) &&
                        (null == acokWebServiceResults || 0 == acokWebServiceResults.Length) &&
                        (null == vcplWebServiceResults || 0 == vcplWebServiceResults.Length) &&
                        (null == cbWebServiceResults || 0 == cbWebServiceResults.Length) &&
                        (null == plWebServiceResults || 0 == plWebServiceResults.Length)
                        )
                    {
                        MessageBox.Show("You Have NO New Leads at this time!", "ATTENTION", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        Globals.breakWaitingPeriod = false;
                    }
                }
			
				Hashtable htEntries = new Hashtable();
				
				bool aahSuccSaving = false;
                if (null != this.aahWebServiceResults && aahWebServiceResults.Length > 0)
				{
                    foreach (string[] data in aahWebServiceResults)				                    
					{	
                        long srvTicks = 0;
                        try { srvTicks = Convert.ToInt64(data[5]); }
                        catch { srvTicks = 0; 
                        }
                        if (srvTicks >= _aahTimeStamp.Ticks)
                        {
                            _aahTimeStamp = new DateTime(srvTicks);
                            _aahTimeStamp = _aahTimeStamp.AddSeconds(1);
                        }

                        PropertyItem pi = CreatePropertyItemFromAAHData(data);

                        if (!SaveAndAssignPropertyItem(pi, ref alGroupsToRefresh, "AAH", "All American Homebuyers Group"))
                        {
                            aahSuccSaving = false;
                            continue;
                        }
                        else
                            aahSuccSaving = true;

                        if (true == Globals.breakWaitingPeriod)                        
                            Globals.breakWaitingPeriod = false;                        																								
				    }
				}

                alGroupsToRefresh.Clear();

                bool npcSuccSaving = false;
                if (null != this.npcWebServiceResults && npcWebServiceResults.Length > 0)
                {
                    foreach (string[] data in npcWebServiceResults)
                    {
                        long srvTicks = 0;
                        try { srvTicks = Convert.ToInt64(data[51]); }
                        catch (Exception exc)
                        {
                            srvTicks = 0;
                        }
                        if (srvTicks >= _npcTimeStamp.Ticks)
                        {
                            _npcTimeStamp = new DateTime(srvTicks);
                            _npcTimeStamp = _npcTimeStamp.AddSeconds(1);
                        }

                        PropertyItem pi = CreatePropertyItemFromNPCData(data);

                        if (!SaveAndAssignPropertyItem(pi, ref alGroupsToRefresh, "NPC", "National Prevention Center Group"))
                        {
                            npcSuccSaving = false;
                            continue;
                        }
                        else
                            npcSuccSaving = true;

                        if (true == Globals.breakWaitingPeriod)
                            Globals.breakWaitingPeriod = false;
                    }
                }

                alGroupsToRefresh.Clear();

                bool acokSuccSaving = false;
                if (null != this.acokWebServiceResults && acokWebServiceResults.Length > 0)
                {
                    foreach (string[] data in acokWebServiceResults)
                    {
                        long srvTicks = 0;
                        try { srvTicks = Convert.ToInt64(data[19]); }
                        catch (Exception exc)
                        {
                            srvTicks = 0;
                        }
                        if (srvTicks >= _acokTimeStamp.Ticks)
                        {
                            _acokTimeStamp = new DateTime(srvTicks);
                            _acokTimeStamp = _acokTimeStamp.AddSeconds(1);
                        }

                        PropertyItem pi = CreatePropertyItemFromACOKData(data);

                        if (!SaveAndAssignPropertyItem(pi, ref alGroupsToRefresh, "ACOK", "Any Credit OK Group"))
                        {
                            acokSuccSaving = false;
                            continue;
                        }
                        else
                            acokSuccSaving = true;

                        if (true == Globals.breakWaitingPeriod)
                            Globals.breakWaitingPeriod = false;
                    }
                }


                alGroupsToRefresh.Clear();

                bool vcplSuccSaving = false;
                if (null != this.vcplWebServiceResults && vcplWebServiceResults.Length > 0)
                {
                    foreach (string[] data in vcplWebServiceResults)
                    {
                        long srvTicks = 0;
                        try { srvTicks = Convert.ToInt64(data[48]); }
                        catch (Exception exc)
                        {
                            srvTicks = 0;
                        }
                        if (srvTicks >= _vcplTimeStamp.Ticks)
                        {
                            _vcplTimeStamp = new DateTime(srvTicks);
                            _vcplTimeStamp = _vcplTimeStamp.AddSeconds(1);
                        }

                        PropertyItem pi = CreatePropertyItemFromVCPLData(data);

                        if (!SaveAndAssignPropertyItem(pi, ref alGroupsToRefresh, "VC/PL", "VoiceConnect / PatLive Group"))
                        {
                            vcplSuccSaving = false;
                            continue;
                        }
                        else
                            vcplSuccSaving = true;

                        if (true == Globals.breakWaitingPeriod)
                            Globals.breakWaitingPeriod = false;
                    }
                }

                bool cbSuccSaving = false;
                if (null != this.cbWebServiceResults && cbWebServiceResults.Length > 0)
                {
                    foreach (string[] data in cbWebServiceResults)
                    {
                        long srvTicks = 0;
                        try { srvTicks = Convert.ToInt64(data[10]); }
                        catch (Exception exc)
                        {
                            srvTicks = 0;
                        }
                        if (srvTicks >= _cbTimeStamp.Ticks)
                        {
                            _cbTimeStamp = new DateTime(srvTicks);
                            _cbTimeStamp = _cbTimeStamp.AddSeconds(1);
                        }

                        PropertyItem pi = CreatePropertyItemFromCBData(data);

                        if (!SaveAndAssignCustomContactType(ContactTypes.ContactTypes.Buyer, pi, ref alGroupsToRefresh, "CB", "Cash Buyers Group"))
                        {
                            cbSuccSaving = false;
                            continue;
                        }
                        else
                            cbSuccSaving = true;

                        if (true == Globals.breakWaitingPeriod)
                            Globals.breakWaitingPeriod = false;
                    }
                }

                bool plSuccSaving = false;
                if (null != this.plWebServiceResults && plWebServiceResults.Length > 0)
                {
                    foreach (string[] data in plWebServiceResults)
                    {
                        long srvTicks = 0;
                        try { srvTicks = Convert.ToInt64(data[9]); }
                        catch (Exception exc)
                        {
                            srvTicks = 0;
                        }
                        if (srvTicks >= _plTimeStamp.Ticks)
                        {
                            _plTimeStamp = new DateTime(srvTicks);
                            _plTimeStamp = _plTimeStamp.AddSeconds(1);
                        }

                        PropertyItem pi = CreatePropertyItemFromPLData(data);

                        if (!SaveAndAssignCustomContactType(ContactTypes.ContactTypes.Lender, pi, ref alGroupsToRefresh, "PL", "Private Lenders Group"))
                        {
                            plSuccSaving = false;
                            continue;
                        }
                        else
                            plSuccSaving = true;

                        if (true == Globals.breakWaitingPeriod)
                            Globals.breakWaitingPeriod = false;
                    }
                }

				
                if (aahSuccSaving)
                {
                    UpdateGUI(aahWebServiceResults, alGroupsToRefresh);
                    _aahLastFetch = DateTime.Now;

                    try
                    {
                        StreamWriter sw = new StreamWriter(_webSupportFolder + AAHLastFetchFileName);
                        sw.WriteLine(this._aahTimeStamp.Ticks.ToString());

                        sw.Flush();
                        sw.Close();
                    }
                    catch { }
                }

                if (npcSuccSaving)
                {
                    UpdateGUI(npcWebServiceResults, alGroupsToRefresh);
                    _npcLastFetch = DateTime.Now;

                    try
                    {
                        StreamWriter sw = new StreamWriter(_webSupportFolder + NPCLastFetchFileName);
                        sw.WriteLine(this._npcTimeStamp.Ticks.ToString());

                        sw.Flush();
                        sw.Close();
                    }
                    catch { }
                }

                if (acokSuccSaving)
                {
                    UpdateGUI(acokWebServiceResults, alGroupsToRefresh);
                    _acokLastFetch = DateTime.Now;

                    try
                    {
                        StreamWriter sw = new StreamWriter(_webSupportFolder + ACOKLastFetchFileName);
                        sw.WriteLine(this._acokTimeStamp.Ticks.ToString());

                        sw.Flush();
                        sw.Close();
                    }
                    catch { }
                }

                if (vcplSuccSaving)
                {
                    UpdateGUI(vcplWebServiceResults, alGroupsToRefresh);
                    _vcplLastFetch = DateTime.Now;

                    try
                    {
                        StreamWriter sw = new StreamWriter(_webSupportFolder + VCPLLastFetchFileName);
                        sw.WriteLine(this._vcplTimeStamp.Ticks.ToString());

                        sw.Flush();
                        sw.Close();
                    }
                    catch { }
                }

                if (cbSuccSaving)
                {
                    UpdateGUI(cbWebServiceResults, alGroupsToRefresh);
                    _cbLastFetch = DateTime.Now;

                    try
                    {
                        StreamWriter sw = new StreamWriter(_webSupportFolder + CBLastFetchFileName);
                        sw.WriteLine(this._cbTimeStamp.Ticks.ToString());

                        sw.Flush();
                        sw.Close();
                    }
                    catch { }
                }

                if (plSuccSaving)
                {
                    UpdateGUI(plWebServiceResults, alGroupsToRefresh);
                    _plLastFetch = DateTime.Now;

                    try
                    {
                        StreamWriter sw = new StreamWriter(_webSupportFolder + PLLastFetchFileName);
                        sw.WriteLine(this._plTimeStamp.Ticks.ToString());

                        sw.Flush();
                        sw.Close();
                    }
                    catch { }
                }

                if (
                    (null != aahWebServiceResults && aahWebServiceResults.Length > 0) || 
                    (null != npcWebServiceResults && npcWebServiceResults.Length > 0) ||
                    (null != acokWebServiceResults && acokWebServiceResults.Length > 0) ||
                    (null != vcplWebServiceResults && vcplWebServiceResults.Length > 0) ||
                    (null != cbWebServiceResults && cbWebServiceResults.Length > 0) ||
                    (null != plWebServiceResults && plWebServiceResults.Length > 0)
                    )
                {
                    string userMessage = GetUserMessage(aahWebServiceResults, npcWebServiceResults, acokWebServiceResults, vcplWebServiceResults, cbWebServiceResults, plWebServiceResults);
                    OnwTwoThreeSoldNotification notif = new OnwTwoThreeSoldNotification(GetLeadCount(aahWebServiceResults, npcWebServiceResults, acokWebServiceResults, vcplWebServiceResults, cbWebServiceResults, plWebServiceResults));
                    notif.Message = userMessage;
                    notif.ShowDialog();
                }
			}
		}        

		#region · InserGroupMembership ·
		private bool InserGroupMembership(PropertyItem pi, int nodeId, SqlConnection dbConn)
		{
			bool bRes = true;
			try
			{	
				SqlCommand sp_InsertGroupMembership = new SqlCommand("InsertGroupMembership", dbConn);

				sp_InsertGroupMembership.CommandType = CommandType.StoredProcedure;					
				sp_InsertGroupMembership.Parameters.Add("@propertyItemId", pi.IdPropertyItem);
				sp_InsertGroupMembership.Parameters.Add("@nodeId", nodeId);
													
									
				if (ConnectionState.Open != Connection.getInstance().HatchConn.State)
					Connection.getInstance().HatchConn.Open();
				
				sp_InsertGroupMembership.ExecuteNonQuery();				
					
			}
			catch
			{					
				bRes = false;
			}		
			return bRes;
		}
		#endregion · InserGroupMembership ·

		#region · FindGroup ·
		private bool FindGroup(string _origin, out int _nodeId, SqlConnection dbConn)
		{			
			_nodeId = -1;
			SqlDataReader dataReader = null;
			try
			{				
				SqlCommand command = new SqlCommand("select nodeId from Node where nodeName = '" + _origin + "'", dbConn);
				dataReader = command.ExecuteReader();
				while (dataReader.Read())
				{
					_nodeId = dataReader.GetInt32(0);						
				}
			}
			catch(Exception exc)
			{				
				_nodeId = -1;
				string s = exc.ToString();
			}
			finally
			{
				if (null != dataReader && false == dataReader.IsClosed)
				{
					dataReader.Close();
				}
			}

			if (-1 == _nodeId)
			{
				return false;
			}
			return true;				
		}
		#endregion · FindGroup ·

		#region · AddNode ·
		private bool AddNode(int _parentNode, string _nodeDesc, string _nodeName, out int nodeId, SqlConnection dbConn)
		{
			bool bResult = true;
			nodeId = -1;
			try
			{
				SqlCommand sp_dodaj_Node = new SqlCommand("dodaj_Node", dbConn);

				sp_dodaj_Node.CommandType = CommandType.StoredProcedure;
					
				sp_dodaj_Node.Parameters.Add("@parentNode", _parentNode);
				sp_dodaj_Node.Parameters.Add("@nodeDesc", _nodeDesc);
				sp_dodaj_Node.Parameters.Add("@nodeName", _nodeName);
					
				int retVal = -1;
				SqlParameter param = sp_dodaj_Node.Parameters.Add("@retValue", retVal);
				param.Direction = ParameterDirection.Output;

				SqlParameter new_NodeId_param = sp_dodaj_Node.Parameters.Add("@newNodeId ", nodeId);
				new_NodeId_param.Direction = ParameterDirection.Output;
												
				sp_dodaj_Node.ExecuteNonQuery();
	
				if (-1 == (int)param.Value)
				{
					bResult = false;
				}
				else
				{
					//UpdateTreeView();
					nodeId = (System.Int32)new_NodeId_param.Value;				
				}
			}
			catch 
			{
				bResult = false;
			}			
			return bResult;
		}
		#endregion · AddNode ·		

        #region Save And Assign Custom Contact Type
        bool SaveAndAssignCustomContactType(ContactTypes.ContactTypes contactType, PropertyItem pi, ref ArrayList groupsToRefresh, string source, string sourceDesc)
        {
            bool retVal = true;
            int autoLeadNodeId = -1;
            int nodeId = -1;

            try
            {
                this.dbConnection.Open();
                pi.ExecuteSPL(this.dbConnection);
                this.InserGroupMembership(pi, LeadsNotProcessedYetGroupId, this.dbConnection);


                //Find Group - Origin
                if (ContactTypes.ContactTypes.Buyer == contactType)
                {
                    if (!this.FindGroup(ContactTypesManager.CashBuyersGroup, out autoLeadNodeId, this.dbConnection))		//FIND autoleads                
                        this.AddNode(1, ContactTypesManager.CashBuyersGroupDesc, ContactTypesManager.CashBuyersGroup, out autoLeadNodeId, this.dbConnection);	//If we don't have it - create
                }
                else if (ContactTypes.ContactTypes.Lender == contactType)
                {
                    if (!this.FindGroup(ContactTypesManager.PrivateLenderGroup, out autoLeadNodeId, this.dbConnection))		//FIND autoleads                
                        this.AddNode(1, ContactTypesManager.PrivateLenderGroupDesc, ContactTypesManager.PrivateLenderGroup, out autoLeadNodeId, this.dbConnection);	//If we don't have it - create 
                }
                
                if (!this.FindGroup(source, out nodeId, this.dbConnection))	//FIND source group
                {
                    this.AddNode(autoLeadNodeId, sourceDesc, source, out nodeId, this.dbConnection);	//if we don't have it - create
                    //FIRE EVENT
                    this.OnNewGroup(nodeId);
                }

                this.InserGroupMembership(pi, nodeId, this.dbConnection);	//assign pi also to source group
                if (false == groupsToRefresh.Contains(nodeId))
                {
                    groupsToRefresh.Add(nodeId);
                }
            }
            catch (Exception exc)
            {
                MessageBox.Show("WebSitesSupport: " + exc.ToString());
                retVal = false;
            }
            finally
            {
                this.dbConnection.Close();
            }

            return retVal;
        }
        #endregion

        #region Save And Assign Property Item
        bool SaveAndAssignPropertyItem(PropertyItem pi, ref ArrayList groupsToRefresh, string source, string sourceDesc)
        {
            bool retVal = true;
            int autoLeadNodeId = -1;
            int nodeId = -1;

            try
            {
                this.dbConnection.Open();
                pi.ExecuteSPL(this.dbConnection);                
                this.InserGroupMembership(pi, LeadsNotProcessedYetGroupId, this.dbConnection);


                //Find Group - Origin

                if (false == this.FindGroup(AutoLeadsGroups, out autoLeadNodeId, this.dbConnection))		//FIND autoleads
                {
                    this.AddNode(1, AutoLeadsGroups, AutoLeadsGroups, out autoLeadNodeId, this.dbConnection);	//If we don't have it - create
                }

                if (false == this.FindGroup(source, out nodeId, this.dbConnection))	//FIND source group
                {
                    this.AddNode(autoLeadNodeId, sourceDesc, source, out nodeId, this.dbConnection);	//if we don't have it - create
                    //FIRE EVENT
                    this.OnNewGroup(nodeId);
                }

                this.InserGroupMembership(pi, nodeId, this.dbConnection);	//assign pi also to source group
                if (false == groupsToRefresh.Contains(nodeId))
                {
                    groupsToRefresh.Add(nodeId);
                }
            }
            catch (Exception exc)
            {
                MessageBox.Show("WebSitesSupport: " + exc.ToString());                
                retVal = false;
            }
            finally
            {
                this.dbConnection.Close();                
            }

            return retVal;
        }
        #endregion

        #region CREATE PROPERTY ITEM FROM AAH DATA
        PropertyItem CreatePropertyItemFromAAHData(string[] data)
        {
            //1.) Create new PI
            PropertyItem pi = new PropertyItem();
            pi.save = true;
            pi.Owner = new Contact();
            pi.Owner.save = true;

            pi.Owner.FirstName = data[0];
            pi.Owner.MiddleName = string.Empty;
            pi.Owner.LastName = data[1];
            pi.Owner.FullName = string.Empty;
            pi.Owner.GoBy = string.Empty;
            pi.Owner.SalutationName = string.Empty;

            pi.Owner.SiteAddress = new Address();	//FILL
            pi.Owner.SiteAddress.save = true;

            {
                pi.Owner.SiteAddress.SiteStreetName = data[7];
                pi.Owner.SiteAddress.SiteStreetNumber = string.Empty;
                pi.Owner.SiteAddress.SiteStreetPostDirectional = string.Empty;
                pi.Owner.SiteAddress.SiteStreetPreDirectional = string.Empty;
                pi.Owner.SiteAddress.SiteStreetSuffix = string.Empty;
                pi.Owner.SiteAddress.SiteStreetUnitNumber = string.Empty;

                pi.Owner.SiteAddress.SiteCity = data[8];
                pi.Owner.SiteAddress.SiteCitySiteState = string.Empty;
                pi.Owner.SiteAddress.SiteCitySiteStateSiteZIP = string.Empty;

                pi.Owner.SiteAddress.SiteCountry = string.Empty;
                pi.Owner.SiteAddress.SiteState = data[10];
                pi.Owner.SiteAddress.SiteZIP = data[11];

                pi.Owner.SiteAddress.FullSiteStreetAddress = data[7];

                pi.Owner.SiteAddress.FullSiteStreetAddress = pi.Owner.SiteAddress.FullSiteStreetAddress.TrimEnd(pi.Owner.SiteAddress.SiteZIP.ToCharArray());
                pi.Owner.SiteAddress.FullSiteStreetAddress = pi.Owner.SiteAddress.FullSiteStreetAddress.Trim();
                pi.Owner.SiteAddress.FullSiteStreetAddress = pi.Owner.SiteAddress.FullSiteStreetAddress.TrimEnd(pi.Owner.SiteAddress.SiteState.ToCharArray());
                pi.Owner.SiteAddress.FullSiteStreetAddress = pi.Owner.SiteAddress.FullSiteStreetAddress.Trim();
                pi.Owner.SiteAddress.FullSiteStreetAddress = pi.Owner.SiteAddress.FullSiteStreetAddress.TrimEnd(pi.Owner.SiteAddress.SiteCity.ToCharArray());
                pi.Owner.SiteAddress.FullSiteStreetAddress = pi.Owner.SiteAddress.FullSiteStreetAddress.Trim();
            }

            pi.Owner.HomePhone = new Phone();		//FILL
            pi.Owner.HomePhone.save = true;
            pi.Owner.HomePhone.PhoneNr = data[2];
            pi.Owner.HomePhone.BestTimeToCall = string.Empty;


            pi.Owner.WorkPhone = new Phone();		//FILL
            pi.Owner.WorkPhone.save = true;
            pi.Owner.WorkPhone.PhoneNr = data[3];
            pi.Owner.WorkPhone.BestTimeToCall = string.Empty;

            pi.Owner.CellPhone = new Phone();		//FILL
            pi.Owner.CellPhone.save = true;
            pi.Owner.CellPhone.PhoneNr = string.Empty;
            pi.Owner.CellPhone.BestTimeToCall = string.Empty;


            pi.Owner.EMail = string.Empty;

            pi.DealProperty = new DealProperty();	//FILL
            pi.DealProperty.save = true;

            pi.DealProperty.Apn = string.Empty;
            pi.DealProperty.Arv = string.Empty;
            pi.DealProperty.Baths = string.Empty;
            pi.DealProperty.Bedrooms = string.Empty;
            pi.DealProperty.CanCancelList = string.Empty;
            pi.DealProperty.DesiredClosingDate = string.Empty;
            pi.DealProperty.FeatureCondition = data[25];
            pi.DealProperty.FirstBal = data[16];

            pi.DealProperty.ForSaleDate = DateTime.MinValue;
            if (pi.DealProperty.ForSaleDate < new DateTime(1753, 1, 1))
                pi.DealProperty.ForSaleDate = new DateTime(1753, 1, 1);

            pi.DealProperty.Garage = data[14];
            pi.DealProperty.Hoa = string.Empty;
            pi.DealProperty.InForecl = string.Empty;
            pi.DealProperty.Ir1 = string.Empty;
            pi.DealProperty.Ir2 = string.Empty;
            pi.DealProperty.Ir3 = string.Empty;
            pi.DealProperty.LeadSource = string.Empty;
            pi.DealProperty.Listed = data[24];


            pi.DealProperty.ListExpDate = DateTime.MinValue;
            if (pi.DealProperty.ListExpDate < new DateTime(1753, 1, 1))
                pi.DealProperty.ListExpDate = new DateTime(1753, 1, 1);

            pi.DealProperty.LowestPriceAcceptable = string.Empty;
            pi.DealProperty.MinCashNeededWhy = string.Empty;
            pi.DealProperty.MoBehind = string.Empty;
            pi.DealProperty.MoPay1 = data[17];
            pi.DealProperty.MoPay2 = data[20];
            pi.DealProperty.MoPay3 = string.Empty;
            pi.DealProperty.Motivation = string.Empty;
            pi.DealProperty.NextStep += data[27] + Environment.NewLine + data[28] + Environment.NewLine + (null != data[21] && string.Empty != data[21] && "YES" == data[21].ToUpper() ? "Taxes included on 2nd" : "Taxes not included on 2nd");
            pi.DealProperty.OccupiedBy = data[15];
            pi.DealProperty.OtherLiens = string.Empty;
            pi.DealProperty.PmntsCurrent = string.Empty;
            pi.DealProperty.Pool = data[13];
            pi.DealProperty.ReInsAmnt = data[23];
            pi.DealProperty.Repairs = string.Empty;
            pi.DealProperty.SecondBal = data[19];
            pi.DealProperty.Sell4WhatOwes = string.Empty;
            pi.DealProperty.Source = string.Empty;
            pi.DealProperty.Sqft = data[12];
            pi.DealProperty.TaxAmnt = string.Empty;
            pi.DealProperty.TaxIns = string.Empty;
            pi.DealProperty.ThirdBal = string.Empty;
            pi.DealProperty.Type = string.Empty;
            pi.DealProperty.Type1 = string.Empty;
            pi.DealProperty.Type2 = string.Empty;
            pi.DealProperty.Type3 = string.Empty;
            pi.DealProperty.WhySelling = data[26];
            pi.DealProperty.WorthBySeller = string.Empty;
            pi.DealProperty.YearBuilt = string.Empty;

            return pi;
        }
        #endregion

        #region CREATE PROPERTY ITEM FROM NPC DATA
        PropertyItem CreatePropertyItemFromNPCData(string[] data)
        {
            //1.) Create new PI
            PropertyItem pi = new PropertyItem();
            pi.save = true;
            pi.Owner = new Contact();
            pi.Owner.save = true;

            pi.Owner.FirstName = data[0];
            pi.Owner.MiddleName = string.Empty;
            pi.Owner.LastName = data[1];            
            pi.Owner.FullName = string.Empty;
            pi.Owner.EMail = data[2];
            pi.Owner.GoBy = string.Empty;
            pi.Owner.SalutationName = string.Empty;
            

            pi.Owner.SiteAddress = new Address();	//FILL
            pi.Owner.SiteAddress.save = true;

            {
                pi.Owner.SiteAddress.SiteStreetName = string.Empty;
                pi.Owner.SiteAddress.SiteStreetNumber = string.Empty;
                pi.Owner.SiteAddress.SiteStreetPostDirectional = string.Empty;
                pi.Owner.SiteAddress.SiteStreetPreDirectional = string.Empty;
                pi.Owner.SiteAddress.SiteStreetSuffix = string.Empty;
                pi.Owner.SiteAddress.SiteStreetUnitNumber = string.Empty;

                pi.Owner.SiteAddress.SiteCity = data[3];
                pi.Owner.SiteAddress.SiteCitySiteState = string.Empty;
                pi.Owner.SiteAddress.SiteCitySiteStateSiteZIP = string.Empty;

                pi.Owner.SiteAddress.SiteCountry = string.Empty;
                pi.Owner.SiteAddress.SiteState = data[4];
                pi.Owner.SiteAddress.SiteZIP = string.Empty;

                pi.Owner.SiteAddress.FullSiteStreetAddress = string.Empty;

                pi.Owner.SiteAddress.FullSiteStreetAddress = pi.Owner.SiteAddress.FullSiteStreetAddress.TrimEnd(pi.Owner.SiteAddress.SiteZIP.ToCharArray());
                pi.Owner.SiteAddress.FullSiteStreetAddress = pi.Owner.SiteAddress.FullSiteStreetAddress.Trim();
                pi.Owner.SiteAddress.FullSiteStreetAddress = pi.Owner.SiteAddress.FullSiteStreetAddress.TrimEnd(pi.Owner.SiteAddress.SiteState.ToCharArray());
                pi.Owner.SiteAddress.FullSiteStreetAddress = pi.Owner.SiteAddress.FullSiteStreetAddress.Trim();
                pi.Owner.SiteAddress.FullSiteStreetAddress = pi.Owner.SiteAddress.FullSiteStreetAddress.TrimEnd(pi.Owner.SiteAddress.SiteCity.ToCharArray());
                pi.Owner.SiteAddress.FullSiteStreetAddress = pi.Owner.SiteAddress.FullSiteStreetAddress.Trim();
            }

            pi.Owner.HomePhone = new Phone();		//FILL
            pi.Owner.HomePhone.save = true;
            pi.Owner.HomePhone.PhoneNr = data[7];
            pi.Owner.HomePhone.BestTimeToCall = data[8];


            pi.Owner.WorkPhone = new Phone();		//FILL
            pi.Owner.WorkPhone.save = true;
            pi.Owner.WorkPhone.PhoneNr = data[9];
            pi.Owner.WorkPhone.BestTimeToCall = data[10];

            pi.Owner.CellPhone = new Phone();		//FILL
            pi.Owner.CellPhone.save = true;
            pi.Owner.CellPhone.PhoneNr = data[11];
            pi.Owner.CellPhone.BestTimeToCall = string.Empty;
            

            pi.DealProperty = new DealProperty();	//FILL
            pi.DealProperty.save = true;

            pi.DealProperty.Apn = string.Empty;
            pi.DealProperty.Arv = string.Empty;

            pi.DealProperty.LeadSource = data[13];
            pi.DealProperty.Bedrooms = data[18];
            pi.DealProperty.Baths = data[19];
            pi.DealProperty.Pool = data[21];
            pi.DealProperty.YearBuilt = data[22];
            pi.DealProperty.FeatureCondition = data[24];
            pi.DealProperty.Sqft = data[25];
            pi.DealProperty.OccupiedBy = data[26];
            pi.DealProperty.Listed = data[27];

            try { pi.DealProperty.ListExpDate = Convert.ToDateTime(data[28]); }
            catch { pi.DealProperty.ListExpDate = DateTime.MinValue; }
            if (pi.DealProperty.ListExpDate < new DateTime(1753, 1, 1))
                pi.DealProperty.ListExpDate = new DateTime(1753, 1, 1);

            pi.DealProperty.CanCancelList = data[29];

            try { pi.DealProperty.ForSaleDate = Convert.ToDateTime(data[34]); }
            catch { pi.DealProperty.ForSaleDate = DateTime.MinValue; }
            if (pi.DealProperty.ForSaleDate < new DateTime(1753, 1, 1))
                pi.DealProperty.ForSaleDate = new DateTime(1753, 1, 1);



            pi.DealProperty.DesiredClosingDate = string.Empty;
            


            pi.DealProperty.FirstBal = data[36];
            pi.DealProperty.SecondBal = data[37];
            pi.DealProperty.ThirdBal = data[38];

            pi.DealProperty.MoPay1 = data[42];
            pi.DealProperty.MoPay2 = data[43];
            pi.DealProperty.MoPay3 = data[44];

            pi.DealProperty.WorthBySeller = data[46];



            pi.DealProperty.Garage = data[20];
            pi.DealProperty.Hoa = string.Empty;
            pi.DealProperty.InForecl = string.Empty;
            pi.DealProperty.Ir1 = string.Empty;
            pi.DealProperty.Ir2 = string.Empty;
            pi.DealProperty.Ir3 = string.Empty;
            
            


            

            pi.DealProperty.LowestPriceAcceptable = string.Empty;
            pi.DealProperty.MinCashNeededWhy = string.Empty;
            pi.DealProperty.MoBehind = string.Empty;
            
            pi.DealProperty.Motivation = string.Empty;
            pi.DealProperty.NextStep += "GOAL: " + data[12] + Environment.NewLine +                                                                                
                                        "BASEMENT: " + data[23] + Environment.NewLine +
                                        "RESIDENT STATUS: " + data[30] + Environment.NewLine +
                                        "FORECLOSURE NOTICE: " + data[33] + Environment.NewLine +
                                        "SALE DATE: " + data[35] + Environment.NewLine
                                        + "TOTAL PAYMENTS: " + data[39] + Environment.NewLine
                                        + "BANKRUPTCY: " + data[40] + Environment.NewLine
                                        + "SITUATION: " + data[41] + Environment.NewLine
                                        + "TOTAL: " + data[45] + Environment.NewLine
                                        + "NET: " + data[47] + Environment.NewLine
                                        ;
                                        
            
            pi.DealProperty.OtherLiens = string.Empty;
            pi.DealProperty.PmntsCurrent = string.Empty;

            pi.DealProperty.ReInsAmnt = string.Empty;
            pi.DealProperty.Repairs = string.Empty;
            
            pi.DealProperty.Sell4WhatOwes = string.Empty;
            pi.DealProperty.Source = string.Empty;
            
            pi.DealProperty.TaxAmnt = string.Empty;
            pi.DealProperty.TaxIns = string.Empty;
            
            pi.DealProperty.Type = string.Empty;
            pi.DealProperty.Type1 = string.Empty;
            pi.DealProperty.Type2 = string.Empty;
            pi.DealProperty.Type3 = string.Empty;
            pi.DealProperty.WhySelling = string.Empty;
            
            

            return pi;
        }
        #endregion

        #region CREATE PROPERTY ITEM FROM ACOK DATA
        PropertyItem CreatePropertyItemFromACOKData(string[] data)
        {
            //1.) Create new PI
            PropertyItem pi = new PropertyItem();
            pi.save = true;
            pi.Owner = new Contact();
            pi.Owner.save = true;

            pi.Owner.FirstName = string.Empty;
            pi.Owner.MiddleName = string.Empty;
            pi.Owner.LastName = string.Empty;
            pi.Owner.FullName = data[2];
            pi.Owner.EMail = string.Empty;
            pi.Owner.GoBy = string.Empty;
            pi.Owner.SalutationName = string.Empty;


            pi.Owner.SiteAddress = new Address();	//FILL
            pi.Owner.SiteAddress.save = true;

            {
                pi.Owner.SiteAddress.SiteStreetName = data[11];
                pi.Owner.SiteAddress.SiteStreetNumber = string.Empty;
                pi.Owner.SiteAddress.SiteStreetPostDirectional = string.Empty;
                pi.Owner.SiteAddress.SiteStreetPreDirectional = string.Empty;
                pi.Owner.SiteAddress.SiteStreetSuffix = string.Empty;
                pi.Owner.SiteAddress.SiteStreetUnitNumber = string.Empty;

                pi.Owner.SiteAddress.SiteCity = data[12];
                pi.Owner.SiteAddress.SiteCitySiteState = string.Empty;
                pi.Owner.SiteAddress.SiteCitySiteStateSiteZIP = string.Empty;

                pi.Owner.SiteAddress.SiteCountry = string.Empty;
                pi.Owner.SiteAddress.SiteState = data[13];
                pi.Owner.SiteAddress.SiteZIP = data[14];

                pi.Owner.SiteAddress.SiteCountry = data[15];
            }

            pi.Owner.HomePhone = new Phone();		//FILL
            pi.Owner.HomePhone.save = true;
            pi.Owner.HomePhone.PhoneNr = data[3];
            pi.Owner.HomePhone.BestTimeToCall = string.Empty;


            pi.Owner.WorkPhone = new Phone();		//FILL
            pi.Owner.WorkPhone.save = true;
            pi.Owner.WorkPhone.PhoneNr = string.Empty;
            pi.Owner.WorkPhone.BestTimeToCall = string.Empty;

            pi.Owner.CellPhone = new Phone();		//FILL
            pi.Owner.CellPhone.save = true;
            pi.Owner.CellPhone.PhoneNr = string.Empty;
            pi.Owner.CellPhone.BestTimeToCall = string.Empty;


            pi.DealProperty = new DealProperty();	//FILL
            pi.DealProperty.save = true;

            pi.DealProperty.Type = data[4];
            pi.DealProperty.Sqft = data[5];
            pi.DealProperty.Bedrooms = data[6];
            pi.DealProperty.Baths = data[7];
            pi.DealProperty.YearBuilt = data[8];
            pi.DealProperty.FeatureCondition = data[9];
            pi.DealProperty.WhySelling = data[10];


            pi.DealProperty.Apn = string.Empty;
            pi.DealProperty.Arv = string.Empty;
            pi.DealProperty.LeadSource = string.Empty;
            pi.DealProperty.Pool = string.Empty;
            
                        
            pi.DealProperty.OccupiedBy = string.Empty;
            pi.DealProperty.Listed = string.Empty;

            pi.DealProperty.ListExpDate = DateTime.MinValue;            
            if (pi.DealProperty.ListExpDate < new DateTime(1753, 1, 1))
                pi.DealProperty.ListExpDate = new DateTime(1753, 1, 1);

            pi.DealProperty.CanCancelList = string.Empty;

            pi.DealProperty.ForSaleDate = DateTime.MinValue;            
            if (pi.DealProperty.ForSaleDate < new DateTime(1753, 1, 1))
                pi.DealProperty.ForSaleDate = new DateTime(1753, 1, 1);



            pi.DealProperty.DesiredClosingDate = string.Empty;



            pi.DealProperty.FirstBal = string.Empty;
            pi.DealProperty.SecondBal = string.Empty;
            pi.DealProperty.ThirdBal = string.Empty;

            pi.DealProperty.MoPay1 = string.Empty;
            pi.DealProperty.MoPay2 = string.Empty;
            pi.DealProperty.MoPay3 = string.Empty;

            pi.DealProperty.WorthBySeller = string.Empty;

            pi.DealProperty.Garage = string.Empty;
            pi.DealProperty.Hoa = string.Empty;
            pi.DealProperty.InForecl = string.Empty;
            pi.DealProperty.Ir1 = string.Empty;
            pi.DealProperty.Ir2 = string.Empty;
            pi.DealProperty.Ir3 = string.Empty;

            pi.DealProperty.LowestPriceAcceptable = string.Empty;
            pi.DealProperty.MinCashNeededWhy = string.Empty;
            pi.DealProperty.MoBehind = string.Empty;

            pi.DealProperty.Motivation = string.Empty;
            pi.DealProperty.NextStep += "TYPE: " + data[17] + Environment.NewLine +
                                        "STATUS: " + data[18] + Environment.NewLine                                        
                                        ;


            pi.DealProperty.OtherLiens = string.Empty;
            pi.DealProperty.PmntsCurrent = string.Empty;

            pi.DealProperty.ReInsAmnt = string.Empty;
            pi.DealProperty.Repairs = string.Empty;

            pi.DealProperty.Sell4WhatOwes = string.Empty;
            pi.DealProperty.Source = string.Empty;

            pi.DealProperty.TaxAmnt = string.Empty;
            pi.DealProperty.TaxIns = string.Empty;

            pi.DealProperty.Type = string.Empty;
            pi.DealProperty.Type1 = string.Empty;
            pi.DealProperty.Type2 = string.Empty;
            pi.DealProperty.Type3 = string.Empty;
            
            return pi;
        }
        #endregion

        #region CREATE PROPERTY ITEM FROM VCPL DATA
        PropertyItem CreatePropertyItemFromVCPLData(string[] data)
        {
            //1.) Create new PI
            PropertyItem pi = new PropertyItem();
            pi.save = true;
            pi.Owner = new Contact();
            pi.Owner.save = true;

            pi.Owner.FirstName = data[1];
            pi.Owner.MiddleName = string.Empty;
            pi.Owner.LastName = data[2];
            pi.Owner.FullName = data[3];
            pi.Owner.EMail = data[4];
            pi.Owner.GoBy = string.Empty;
            pi.Owner.SalutationName = string.Empty;


            pi.Owner.SiteAddress = new Address();	//FILL
            pi.Owner.SiteAddress.save = true;

            {
                pi.Owner.SiteAddress.SiteStreetName = string.Empty;
                pi.Owner.SiteAddress.SiteStreetNumber = string.Empty;
                pi.Owner.SiteAddress.SiteStreetPostDirectional = string.Empty;
                pi.Owner.SiteAddress.SiteStreetPreDirectional = string.Empty;
                pi.Owner.SiteAddress.SiteStreetSuffix = string.Empty;
                pi.Owner.SiteAddress.SiteStreetUnitNumber = string.Empty;
                pi.Owner.SiteAddress.FullSiteStreetAddress = data[11];

                pi.Owner.SiteAddress.SiteCity = data[12];
                pi.Owner.SiteAddress.SiteCitySiteState = string.Empty;
                pi.Owner.SiteAddress.SiteCitySiteStateSiteZIP = string.Empty;

                pi.Owner.SiteAddress.SiteCountry = string.Empty;
                pi.Owner.SiteAddress.SiteState = data[13];
                pi.Owner.SiteAddress.SiteZIP = data[14];

                pi.Owner.SiteAddress.SiteCountry = string.Empty;
            }

            pi.Owner.HomePhone = new Phone();		//FILL
            pi.Owner.HomePhone.save = true;
            pi.Owner.HomePhone.PhoneNr = data[5];
            pi.Owner.HomePhone.BestTimeToCall = data[6];

            pi.Owner.WorkPhone = new Phone();		//FILL
            pi.Owner.WorkPhone.save = true;
            pi.Owner.WorkPhone.PhoneNr = data[7];
            pi.Owner.WorkPhone.BestTimeToCall = data[8];

            pi.Owner.CellPhone = new Phone();		//FILL
            pi.Owner.CellPhone.save = true;
            pi.Owner.CellPhone.PhoneNr = data[9];
            pi.Owner.CellPhone.BestTimeToCall = data[10];


            pi.DealProperty = new DealProperty();	//FILL
            pi.DealProperty.save = true;

            pi.DealProperty.Type = data[15];
            pi.DealProperty.Sqft = data[18];
            pi.DealProperty.Bedrooms = data[16];
            pi.DealProperty.Baths = data[17];
            pi.DealProperty.YearBuilt = data[19];
            pi.DealProperty.FeatureCondition = data[23];
            pi.DealProperty.WhySelling = data[24];


            pi.DealProperty.Apn = string.Empty;
            pi.DealProperty.Arv = string.Empty;
            pi.DealProperty.LeadSource = data[45];
            pi.DealProperty.Pool = data[21];


            pi.DealProperty.OccupiedBy = data[22];
            pi.DealProperty.Listed = data[38];

            try { pi.DealProperty.ListExpDate = Convert.ToDateTime(data[39]); }
            catch { pi.DealProperty.ListExpDate = DateTime.MinValue; }
            if (pi.DealProperty.ListExpDate < new DateTime(1753, 1, 1))
                pi.DealProperty.ListExpDate = new DateTime(1753, 1, 1);

            pi.DealProperty.CanCancelList = data[40];

            pi.DealProperty.ForSaleDate = DateTime.MinValue;
            if (pi.DealProperty.ForSaleDate < new DateTime(1753, 1, 1))
                pi.DealProperty.ForSaleDate = new DateTime(1753, 1, 1);



            pi.DealProperty.DesiredClosingDate = data[43];



            pi.DealProperty.FirstBal = data[27];
            pi.DealProperty.SecondBal = data[30];
            pi.DealProperty.ThirdBal = string.Empty;

            pi.DealProperty.MoPay1 = data[28];
            pi.DealProperty.MoPay2 = data[31];
            pi.DealProperty.MoPay3 = string.Empty;

            pi.DealProperty.WorthBySeller = data[25];

            pi.DealProperty.Garage = data[20];
            pi.DealProperty.Hoa = data[32];
            pi.DealProperty.InForecl = data[37];
            pi.DealProperty.Ir1 = string.Empty;
            pi.DealProperty.Ir2 = string.Empty;
            pi.DealProperty.Ir3 = string.Empty;

            pi.DealProperty.LowestPriceAcceptable = data[41];
            pi.DealProperty.MinCashNeededWhy = data[42];
            pi.DealProperty.MoBehind = data[35];

            pi.DealProperty.Motivation = data[44];


            pi.DealProperty.NextStep = data[46];


            pi.DealProperty.OtherLiens = data[33];
            pi.DealProperty.PmntsCurrent = data[34];

            pi.DealProperty.ReInsAmnt = data[36];
            pi.DealProperty.Repairs = string.Empty;

            pi.DealProperty.Sell4WhatOwes = string.Empty;
            pi.DealProperty.Source = data[26];

            pi.DealProperty.TaxAmnt = string.Empty;
            pi.DealProperty.TaxIns = data[29];

            pi.DealProperty.Type = string.Empty;
            pi.DealProperty.Type1 = string.Empty;
            pi.DealProperty.Type2 = string.Empty;
            pi.DealProperty.Type3 = string.Empty;

            return pi;
        }
        #endregion

        bool TestForCompanySuffix(string input)
        {
            string[] companySuffix = new string[] { "inc", "inc.", "llc", "llc.", "trust", "corp", "corp.", "lp", "lp." };

            if (null == input || string.Empty == input)
                return false;

            string[] splittedInput = input.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);

            foreach (string suffix in companySuffix)
            {
                if (splittedInput[splittedInput.Length - 1].ToUpper() == suffix.ToUpper())
                    return true;
            }

            return false;
        }

        void ParseNames(string input, out string firstName, out string middleName, out string lastName, out string fullName)
        {
            input = input.Trim();
   
            firstName = string.Empty;
            middleName = string.Empty;
            lastName = string.Empty;
            fullName = input;            

            if (null == input || string.Empty == input)
                return;

            string[] splittedInput = input.Split(new string[] {" "}, StringSplitOptions.RemoveEmptyEntries);
            
            //if 2 words and 2nd word is not (LLC or INC) >> then split into firstName, lastName
            //if 3 words and 3rd word is not (LLC or INC) >> then split into firstName, middleName, lastName            
            if (2 == splittedInput.Length || 3 == splittedInput.Length)
            {
                if (TestForCompanySuffix(splittedInput[splittedInput.Length - 1]))
                    return;
               
                if (2 == splittedInput.Length)
                {
                    firstName = splittedInput[0];
                    lastName = splittedInput[1];                    
                }
                else if (3 == splittedInput.Length && "AND" != splittedInput[1].Trim().ToUpper() && "&" != splittedInput[1].Trim())
                {
                    firstName = splittedInput[0];
                    middleName = splittedInput[1];
                    lastName = splittedInput[2];
                }
            }            
        }

        bool SameLastName(string firstOwner, string secondOwner, out string firstNames, out string lastName, out string fullName)
        {
            firstNames = string.Empty;
            lastName = string.Empty;
            fullName = string.Empty;

            if (null == firstOwner || null == secondOwner)
                return false;

            firstOwner = firstOwner.Trim();
            secondOwner = secondOwner.Trim();

            if (string.Empty == firstOwner || string.Empty == secondOwner)
                return false;

            string[] firstOwnerArray = firstOwner.Split(new string [] {" "}, StringSplitOptions.RemoveEmptyEntries);
            string[] secondOwnerArray = secondOwner.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);

            if (null != firstOwnerArray && null != secondOwnerArray
                && 0 < firstOwnerArray.Length && 0 < secondOwnerArray.Length
                && firstOwnerArray[firstOwnerArray.Length - 1].ToUpper() == secondOwnerArray[secondOwnerArray.Length - 1].ToUpper()
                && !TestForCompanySuffix(firstOwnerArray[firstOwnerArray.Length - 1])
                && !TestForCompanySuffix(secondOwnerArray[secondOwnerArray.Length - 1]) 
                )
            {

                lastName = firstOwnerArray[firstOwnerArray.Length - 1];
                
                for (int i = 0; i < firstOwnerArray.Length - 1; i++)
                {
                    firstNames += firstOwnerArray[i] + " ";                    
                }

                firstNames += "& ";

                for (int i = 0; i < secondOwnerArray.Length - 1; i++)
                {
                    firstNames += secondOwnerArray[i] + " ";
                }

                firstNames = firstNames.TrimEnd();
                fullName = firstNames + " " + lastName;

                return true;
            }

            return false;
        }

        #region CREATE PROPERTY ITEM FROM CB DATA
        PropertyItem CreatePropertyItemFromCBData(string[] data)
        {
            //1.) Create new PI
            PropertyItem pi = new PropertyItem();
            pi.save = true;
            pi.Owner = new Contact();
            pi.Owner.save = true;

            string firstNames = string.Empty;
            string firstName = string.Empty;
            string middleName = string.Empty;
            string lastName = string.Empty;
            string fullName = string.Empty;

            if (SameLastName(data[1], data[2], out firstNames, out lastName, out fullName))
            {
                pi.Owner.FirstName = firstNames;
                pi.Owner.MiddleName = string.Empty;
                pi.Owner.LastName = lastName;
                pi.Owner.FullName = fullName;
            }
            else
            {
                string input = (string.Empty != data[1] ? data[1].Trim() + " " + data[2] : data[2]);
                ParseNames(input, out firstName, out middleName, out lastName, out fullName);

                pi.Owner.FirstName = firstName;
                pi.Owner.MiddleName = middleName;
                pi.Owner.LastName = lastName;
                pi.Owner.FullName = fullName;
            }

            pi.Owner.EMail = string.Empty;
            pi.Owner.GoBy = string.Empty;
            pi.Owner.SalutationName = string.Empty;

            pi.Owner.MailAddress = new Address();	//FILL
            pi.Owner.MailAddress.save = true;

            {
                pi.Owner.MailAddress.SiteStreetName = data[3];
                pi.Owner.MailAddress.SiteStreetNumber = string.Empty;
                pi.Owner.MailAddress.SiteStreetPostDirectional = string.Empty;
                pi.Owner.MailAddress.SiteStreetPreDirectional = string.Empty;
                pi.Owner.MailAddress.SiteStreetSuffix = string.Empty;
                pi.Owner.MailAddress.SiteStreetUnitNumber = string.Empty;
                pi.Owner.MailAddress.FullSiteStreetAddress = string.Empty;

                pi.Owner.MailAddress.SiteCity = data[4];
                pi.Owner.MailAddress.SiteCitySiteState = string.Empty;
                pi.Owner.MailAddress.SiteCitySiteStateSiteZIP = string.Empty;

                pi.Owner.MailAddress.SiteCountry = string.Empty;
                pi.Owner.MailAddress.SiteState = data[5];
                pi.Owner.MailAddress.SiteZIP = data[6];

                pi.Owner.MailAddress.SiteCountry = data[6];
            }

            pi.DealProperty = new DealProperty();	//FILL
            pi.DealProperty.save = true;

            {
                pi.DealProperty.Type = string.Empty;
                pi.DealProperty.Sqft = string.Empty;
                pi.DealProperty.Bedrooms = string.Empty;
                pi.DealProperty.Baths = string.Empty;
                pi.DealProperty.YearBuilt = string.Empty;
                pi.DealProperty.FeatureCondition = string.Empty;
                pi.DealProperty.WhySelling = string.Empty;


                pi.DealProperty.Apn = string.Empty;
                pi.DealProperty.Arv = string.Empty;
                pi.DealProperty.LeadSource = string.Empty;
                pi.DealProperty.Pool = string.Empty;


                pi.DealProperty.OccupiedBy = string.Empty;
                pi.DealProperty.Listed = string.Empty;

                try { pi.DealProperty.ListExpDate = Convert.ToDateTime(string.Empty); }
                catch { pi.DealProperty.ListExpDate = DateTime.MinValue; }
                if (pi.DealProperty.ListExpDate < new DateTime(1753, 1, 1))
                    pi.DealProperty.ListExpDate = new DateTime(1753, 1, 1);

                pi.DealProperty.CanCancelList = string.Empty;

                pi.DealProperty.ForSaleDate = DateTime.MinValue;
                if (pi.DealProperty.ForSaleDate < new DateTime(1753, 1, 1))
                    pi.DealProperty.ForSaleDate = new DateTime(1753, 1, 1);

                pi.DealProperty.DesiredClosingDate = string.Empty;

                pi.DealProperty.FirstBal = string.Empty;
                pi.DealProperty.SecondBal = string.Empty;
                pi.DealProperty.ThirdBal = string.Empty;

                pi.DealProperty.MoPay1 = string.Empty;
                pi.DealProperty.MoPay2 = string.Empty;
                pi.DealProperty.MoPay3 = string.Empty;

                pi.DealProperty.WorthBySeller = string.Empty;

                pi.DealProperty.Garage = string.Empty;
                pi.DealProperty.Hoa = string.Empty;
                pi.DealProperty.InForecl = string.Empty;
                pi.DealProperty.Ir1 = string.Empty;
                pi.DealProperty.Ir2 = string.Empty;
                pi.DealProperty.Ir3 = string.Empty;

                pi.DealProperty.LowestPriceAcceptable = string.Empty;
                pi.DealProperty.MinCashNeededWhy = string.Empty;
                pi.DealProperty.MoBehind = string.Empty;

                pi.DealProperty.Motivation = string.Empty;


                pi.DealProperty.NextStep = string.Empty != data[6] ? "Purchase Date: " + data[7] + Environment.NewLine : string.Empty;
                pi.DealProperty.NextStep += string.Empty != data[7] ? "Purchase Amount: " + data[8] : string.Empty;

                pi.DealProperty.OtherLiens = string.Empty;
                pi.DealProperty.PmntsCurrent = string.Empty;

                pi.DealProperty.ReInsAmnt = string.Empty;
                pi.DealProperty.Repairs = string.Empty;

                pi.DealProperty.Sell4WhatOwes = string.Empty;
                pi.DealProperty.Source = string.Empty;

                pi.DealProperty.TaxAmnt = string.Empty;
                pi.DealProperty.TaxIns = string.Empty;

                pi.DealProperty.Type = string.Empty;
                pi.DealProperty.Type1 = string.Empty;
                pi.DealProperty.Type2 = string.Empty;
                pi.DealProperty.Type3 = string.Empty;
            }

            return pi;
        }
        #endregion

        #region CREATE PROPERTY ITEM FROM PL DATA
        PropertyItem CreatePropertyItemFromPLData(string[] data)
        {
            //1.) Create new PI
            PropertyItem pi = new PropertyItem();
            pi.save = true;
            pi.Owner = new Contact();
            pi.Owner.save = true;            

            string input = data[1].Trim();
            string firstName = string.Empty;
            string middleName = string.Empty;
            string lastName = string.Empty;
            string fullName = string.Empty;
            ParseNames(input, out firstName, out middleName, out lastName, out fullName);

            pi.Owner.FirstName = firstName;
            pi.Owner.MiddleName = middleName;
            pi.Owner.LastName = lastName;
            pi.Owner.FullName = fullName;

            pi.Owner.EMail = string.Empty;
            pi.Owner.GoBy = string.Empty;
            pi.Owner.SalutationName = string.Empty;


            pi.Owner.MailAddress = new Address();	//FILL
            pi.Owner.MailAddress.save = true;

            {
                pi.Owner.MailAddress.SiteStreetName = data[2];
                pi.Owner.MailAddress.SiteStreetNumber = string.Empty;
                pi.Owner.MailAddress.SiteStreetPostDirectional = string.Empty;
                pi.Owner.MailAddress.SiteStreetPreDirectional = string.Empty;
                pi.Owner.MailAddress.SiteStreetSuffix = string.Empty;
                pi.Owner.MailAddress.SiteStreetUnitNumber = string.Empty;
                pi.Owner.MailAddress.FullSiteStreetAddress = string.Empty;

                pi.Owner.MailAddress.SiteCity = data[2];
                pi.Owner.MailAddress.SiteCitySiteState = string.Empty;
                pi.Owner.MailAddress.SiteCitySiteStateSiteZIP = string.Empty;

                pi.Owner.MailAddress.SiteCountry = string.Empty;
                pi.Owner.MailAddress.SiteState = data[4];
                pi.Owner.MailAddress.SiteZIP = data[5];

                pi.Owner.MailAddress.SiteCountry = string.Empty;
            }
           
            pi.DealProperty = new DealProperty();	//FILL
            pi.DealProperty.save = true;

            pi.DealProperty.Type = string.Empty;
            pi.DealProperty.Sqft = string.Empty;
            pi.DealProperty.Bedrooms = string.Empty;
            pi.DealProperty.Baths = string.Empty;
            pi.DealProperty.YearBuilt = string.Empty;
            pi.DealProperty.FeatureCondition = string.Empty;
            pi.DealProperty.WhySelling = string.Empty;


            pi.DealProperty.Apn = string.Empty;
            pi.DealProperty.Arv = string.Empty;
            pi.DealProperty.LeadSource = string.Empty;
            pi.DealProperty.Pool = string.Empty;


            pi.DealProperty.OccupiedBy = string.Empty;
            pi.DealProperty.Listed = string.Empty;

            try { pi.DealProperty.ListExpDate = Convert.ToDateTime(string.Empty); }
            catch { pi.DealProperty.ListExpDate = DateTime.MinValue; }
            if (pi.DealProperty.ListExpDate < new DateTime(1753, 1, 1))
                pi.DealProperty.ListExpDate = new DateTime(1753, 1, 1);

            pi.DealProperty.CanCancelList = string.Empty;

            pi.DealProperty.ForSaleDate = DateTime.MinValue;
            if (pi.DealProperty.ForSaleDate < new DateTime(1753, 1, 1))
                pi.DealProperty.ForSaleDate = new DateTime(1753, 1, 1);

            pi.DealProperty.DesiredClosingDate = string.Empty;

            pi.DealProperty.FirstBal = string.Empty;
            pi.DealProperty.SecondBal = string.Empty;
            pi.DealProperty.ThirdBal = string.Empty;

            pi.DealProperty.MoPay1 = string.Empty;
            pi.DealProperty.MoPay2 = string.Empty;
            pi.DealProperty.MoPay3 = string.Empty;

            pi.DealProperty.WorthBySeller = string.Empty;

            pi.DealProperty.Garage = string.Empty;
            pi.DealProperty.Hoa = string.Empty;
            pi.DealProperty.InForecl = string.Empty;
            pi.DealProperty.Ir1 = string.Empty;
            pi.DealProperty.Ir2 = string.Empty;
            pi.DealProperty.Ir3 = string.Empty;

            pi.DealProperty.LowestPriceAcceptable = string.Empty;
            pi.DealProperty.MinCashNeededWhy = string.Empty;
            pi.DealProperty.MoBehind = string.Empty;

            pi.DealProperty.Motivation = string.Empty;


            pi.DealProperty.NextStep = string.Empty != data[6] ? "Loan Date: " + data[6] + Environment.NewLine : string.Empty;            
            pi.DealProperty.NextStep += string.Empty != data[7] ? "Loan Amount: " + data[7] : string.Empty;

            pi.DealProperty.OtherLiens = string.Empty;
            pi.DealProperty.PmntsCurrent = string.Empty;

            pi.DealProperty.ReInsAmnt = string.Empty;
            pi.DealProperty.Repairs = string.Empty;

            pi.DealProperty.Sell4WhatOwes = string.Empty;
            pi.DealProperty.Source = string.Empty;

            pi.DealProperty.TaxAmnt = string.Empty;
            pi.DealProperty.TaxIns = string.Empty;

            pi.DealProperty.Type = string.Empty;
            pi.DealProperty.Type1 = string.Empty;
            pi.DealProperty.Type2 = string.Empty;
            pi.DealProperty.Type3 = string.Empty;

            return pi;
        }
        #endregion

        #region UPDATE GUI
        void UpdateGUI(string[][] webServiceResults, ArrayList groupToRefresh)
        {
            try
            {
                if (null != webServiceResults && webServiceResults.Length > 0)
                {
                    this.OnNewContact(LeadsNotProcessedYetGroupId);	//refresh always		
                    for (int i = 0; i < groupToRefresh.Count; i++)
                    {
                        this.OnNewContact((int)groupToRefresh[i]);
                    }
                }

            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.ToString());
            }
        }
        #endregion

        #region GET LEAD COUNT
        int GetLeadCount(string[][] aahWebServiceResults, string[][] npcWebServiceResults, string[][] acokWebServiceResults, string[][] vcplWebServiceResults, string[][] cbWebServiceResults, string[][] plWebServiceResults)
        {
            int leadCount = 0;

            leadCount = (null != aahWebServiceResults && aahWebServiceResults.Length > 0) ? aahWebServiceResults.Length : 0;
            leadCount += (null != npcWebServiceResults && npcWebServiceResults.Length > 0) ? npcWebServiceResults.Length : 0;
            leadCount += (null != acokWebServiceResults && acokWebServiceResults.Length > 0) ? acokWebServiceResults.Length : 0;
            leadCount += (null != vcplWebServiceResults && vcplWebServiceResults.Length > 0) ? vcplWebServiceResults.Length : 0;
            leadCount += (null != cbWebServiceResults && cbWebServiceResults.Length > 0) ? cbWebServiceResults.Length : 0;
            leadCount += (null != plWebServiceResults && plWebServiceResults.Length > 0) ? plWebServiceResults.Length : 0;

            return leadCount;
        }
        #endregion

        #region GET USER MESSAGE
        string GetUserMessage(string[][] aahWebServiceResults, string[][] npcWebServiceResults, string[][] acokWebServiceResults, string[][] wcplWebServiceResults, string[][] cbWebServiceResults, string[][] plWebServiceResults)
        {
            string userMsg = string.Empty;

            int leadCount = GetLeadCount(aahWebServiceResults, npcWebServiceResults, acokWebServiceResults, wcplWebServiceResults, cbWebServiceResults, plWebServiceResults);

            userMsg += System.Environment.NewLine;
            userMsg += "Go to “Leads Not Processed Yet” to process ";


            if (1 < leadCount)
                userMsg += "those leads.";
            else
                userMsg += "that lead.";


            userMsg += System.Environment.NewLine;
            userMsg += System.Environment.NewLine;


            //userMsg += "NOTE:" + System.Environment.NewLine;
            //for(int i = 0; i < this.alOrigins.Count; i++)				
            //{							
            //    userMsg += "You have NEW \"" + (string)alOrigins[i] + "\" Lead(s)!" + System.Environment.NewLine;
            //}						            

            return userMsg;
        }
        #endregion

        #region INIT LAST FETCH TIMES
        void InitLastFetchTime()
        {
            try
            {
                if (!Directory.Exists(_webSupportFolder))
                    Directory.CreateDirectory(_webSupportFolder);
            }
            catch (Exception exc)
            {
                System.Windows.Forms.MessageBox.Show("Can´t create WebSupport Folder !" + System.Environment.NewLine + exc.Message);
                return;
            }

            _aahTimeStamp = GetLastInitTime(_webSupportFolder + AAHLastFetchFileName);
            _npcTimeStamp = GetLastInitTime(_webSupportFolder + NPCLastFetchFileName);
            _acokTimeStamp = GetLastInitTime(_webSupportFolder + ACOKLastFetchFileName);
            _vcplTimeStamp = GetLastInitTime(_webSupportFolder + VCPLLastFetchFileName);
            _cbTimeStamp = GetLastInitTime(_webSupportFolder + CBLastFetchFileName);
            _plTimeStamp = GetLastInitTime(_webSupportFolder + PLLastFetchFileName);
        }

        DateTime GetLastInitTime(string path)
        {
            StreamReader sr = null;
            DateTime lastFetchTime = new DateTime(2000, 1, 1);
            try
            {
                if (File.Exists(path))
                {
                    sr = new StreamReader(path);
                    string lastFetchFromFile = sr.ReadLine();
                    lastFetchTime = new DateTime(Convert.ToInt64(lastFetchFromFile));
                }
            }
            catch (Exception e)
            {
                System.Windows.Forms.MessageBox.Show("Can´t get time for " + path + "!" + System.Environment.NewLine + e.Message);
            }
            finally
            {
                if (null != sr)
                    sr.Close();
            }

            return lastFetchTime;
        }
        #endregion

    }
}
