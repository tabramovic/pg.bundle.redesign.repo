﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Data;
using System.Data.SqlClient;
using System.IO;

namespace DealMaker.PlainClasses.Utils
{
    public sealed class PictureManager
    {
        static readonly PictureManager _instance = new PictureManager();

        PictureManager()
        { }

        static PictureManager()
        { }

        public static PictureManager Instance
        {
            get { return _instance; }
        }

        bool _existingDBLayer = false;
        SqlConnection _connection;

        public void SetConnection(SqlConnection conn)
        {
            if (null == _connection)
            {
                _connection = conn;
                _existingDBLayer = TestIfExistsDBLayer();

                if (!_existingDBLayer)
                    CreateDBLayer();
            }
        }

        public bool TestIfExistsDBLayer()
        {
            bool retVal = false;
            SqlDataReader reader = null;
            string cmdText = "SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'PropertyImages'";

            try
            {
                SqlCommand testForLayer = new SqlCommand(cmdText, _connection);
                reader = testForLayer.ExecuteReader();
                while (reader.Read())
                {
                    retVal = true;
                    break;
                }
            }
            catch
            {
                retVal = false;
            }
            finally
            {
                reader.Close();
            }
            return retVal;
        }

        void CreateDBLayer()
        {
            CreateTable();
            CreateInsertSPL();
            CreateUpdateSPL();
        }

        bool CreateTable()
        {
            bool retVal = false;
            string cmdText = "CREATE TABLE PropertyImages (piId uniqueidentifier PRIMARY KEY, picture image)";

            try
            {
                SqlCommand createLayer = new SqlCommand(cmdText, _connection);
                createLayer.ExecuteNonQuery();
                retVal = true;
            }
            catch
            {
                retVal = false;
            }
            return retVal;
        }

        void CreateInsertSPL()
        {
            try
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("CREATE PROCEDURE InsertPropertyImage ");
                sb.Append("( ");
                sb.Append("@propertyItemId uniqueidentifier, ");
                sb.Append("@picture image");
                sb.Append(") ");
                sb.Append("AS ");
                sb.Append("INSERT INTO PropertyImages (piId , picture) ");
                sb.Append("VALUES ( @propertyItemId , @picture) ");

                string splText = sb.ToString();
                SqlCommand createSPL = new SqlCommand(splText, _connection);
                createSPL.ExecuteNonQuery();

            }
            catch (Exception exc)
            {
                System.Windows.Forms.MessageBox.Show("Can't create INSERT procedure for PICTURES module" + System.Environment.NewLine + exc.Message);
            }
        }

        void CreateUpdateSPL()
        {
            try
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("CREATE PROCEDURE UpdatePropertyImage ");
                sb.Append("( ");
                sb.Append("@propertyItemId uniqueidentifier, ");
                sb.Append("@picture image ");
                sb.Append(") ");
                sb.Append("AS ");
                sb.Append("UPDATE PropertyImages SET picture=@picture where piId=@propertyItemId; ");

                string splText = sb.ToString();
                SqlCommand createSPL = new SqlCommand(splText, _connection);
                createSPL.ExecuteNonQuery();

            }
            catch (Exception exc)
            {
                System.Windows.Forms.MessageBox.Show("Can't create UPDATE procedure for PICTURES module" + System.Environment.NewLine + exc.Message);
            }
        }                

        public Image GetData(Guid propertyItem)
        {            
            string cmdText = "SELECT picture FROM PropertyImages WHERE piId='" + propertyItem.ToString() + "'";
            SqlDataReader reader = null;            

            try
            {
                SqlCommand getData = new SqlCommand(cmdText, _connection);
                reader = getData.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        byte[] imgArray = !reader.IsDBNull(0) ? (byte[])reader.GetValue(0) : null;
                        return ByteArrayToImage(imgArray);                                                
                    }
                }                
            }
            catch
            {
                return null;
            }
            finally
            {
                reader.Close();
            }

            return null;
        }

        public bool SaveData(Guid propertyItem, Image img, Color backColor, int width, int height)
        {            
            if (!TestForExistingRecord(propertyItem))
                return InsertData(propertyItem, img);
            else
                return UpdateData(propertyItem, img);
        }

        bool TestForExistingRecord(Guid propertyItem)
        {
            bool retVal = false;

            SqlDataReader reader = null;
            string cmdText = "SELECT piId FROM PropertyImages where piId='" + propertyItem.ToString() + "'";

            try
            {
                SqlCommand testForExistingRecord = new SqlCommand(cmdText, _connection);
                reader = testForExistingRecord.ExecuteReader();
                while (reader.Read())
                {
                    retVal = true;
                    break;
                }
            }
            catch
            {
                retVal = false;
            }
            finally
            {
                if (null != reader)
                    reader.Close();
            }
            return retVal;
        }

        bool InsertData(Guid propertyItem, Image img)
        {
            bool retVal = false;

            if (null == img)
                return true;

            try
            {
                SqlCommand sp_insert_comps = new SqlCommand("InsertPropertyImage", _connection);
                sp_insert_comps.CommandType = CommandType.StoredProcedure;

                sp_insert_comps.Parameters.AddWithValue("@propertyItemId", propertyItem);
                sp_insert_comps.Parameters.AddWithValue("@picture", ImageToByteArray(img));
                sp_insert_comps.ExecuteNonQuery();

                retVal = true;
            }
            catch
            {
                retVal = false;
            }
            return retVal;
        }

        bool UpdateData(Guid propertyItem, Image img)
        {
            bool retVal = false;

            try
            {
                SqlCommand sp_insert_comps = new SqlCommand("UpdatePropertyImage", _connection);
                sp_insert_comps.CommandType = CommandType.StoredProcedure;

                sp_insert_comps.Parameters.AddWithValue("@propertyItemId", propertyItem);
                sp_insert_comps.Parameters.AddWithValue("@picture", ImageToByteArray(img));
                sp_insert_comps.ExecuteNonQuery();

                retVal = true;
            }
            catch
            {
                retVal = false;
            }
            return retVal;
        }

        bool DeleteData(Guid propertyItem)
        {
            bool retVal = false;
            string cmdText = "DELETE FROM PropertyImages WHERE piId = '" + propertyItem.ToString() + "'";

            try
            {
                SqlCommand deleteData = new SqlCommand(cmdText, _connection);
                deleteData.ExecuteNonQuery();
                retVal = true;
            }
            catch
            {
                retVal = false;
            }
            return retVal;
        }        

        public byte[] ImageToByteArray(Image imageIn)
        {
            MemoryStream ms = new MemoryStream();
            imageIn.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
            return ms.ToArray();
        }

        public Image ByteArrayToImage(byte[] byteArrayIn)
        {
            MemoryStream ms = new MemoryStream(byteArrayIn);
            Image returnImage = Image.FromStream(ms);
            return returnImage;
        }
    }
}
