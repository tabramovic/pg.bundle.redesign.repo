﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace DealMaker
{
    public sealed class UserInformationManager
    {
        static readonly UserInformationManager _instance = new UserInformationManager();
        
        static UserInformationManager()
        { }

        UserInformationManager()
        { }

        public static UserInformationManager Instance
        {
            get { return _instance; }
        }

        /// <summary>
        /// Get All Entries in Dictionary collection
        /// </summary>
        /// <param name="ue"></param>
        /// <returns></returns>
        public Dictionary<Guid, string> GetValues(UserEntries ue)
        {
            return GetValues(ue, false);
        }

        Dictionary<CacheKey, Dictionary<Guid, string>> CachedValues = new Dictionary<CacheKey, Dictionary<Guid, string>>();

        class CacheKey
        {
            public UserEntries ue {get; set;}
            public bool appendEmailAddress { get; set; }

            public CacheKey(UserEntries ue, bool appendEmailAddress)
            {
                this.ue = ue;
                this.appendEmailAddress = appendEmailAddress;
            }

            public override bool Equals(object obj)
            {
                return ((((CacheKey)obj).appendEmailAddress == this.appendEmailAddress) && (((CacheKey)obj).ue == this.ue));
            }

            public override int GetHashCode()
            {
                return ue.GetHashCode() * appendEmailAddress.GetHashCode();
            }
        }

        void ClearCache()
        {
            if (null != CachedValues)
            {
                lock (CachedValues)
                {
                    CachedValues.Clear();
                }
            }
            else
                CachedValues = new Dictionary<CacheKey, Dictionary<Guid, string>>();

            if (null != CachedEmailAddresses)
            {
                lock (CachedEmailAddresses)
                {
                    CachedEmailAddresses.Clear();
                }
            }
            else
                CachedEmailAddresses = new Dictionary<Guid, string>();
        }

        Dictionary<Guid, string> GetFromCache(CacheKey cKey)
        {
            if (null != CachedValues)
            {
                lock (CachedValues)
                {
                    if (CachedValues.ContainsKey(cKey))
                        return CachedValues[cKey];
                }
            }
            return null;
        }

        void SetToCache(CacheKey cKey, Dictionary<Guid, string> resTable)
        {
            if (null != CachedValues)
            {
                lock (CachedValues)
                {
                    if (!CachedValues.ContainsKey(cKey))
                        CachedValues.Add(cKey, resTable);
                }
            }
        }


        public Dictionary<Guid, string> GetValues(UserEntries ue, bool appendEmailAddress)
        {
            SqlCommand command = null;
            SqlDataReader reader = null;
            Dictionary<Guid, string> returnTable = new Dictionary<Guid, string>();

            
            //build Cache key
            CacheKey cKey = new CacheKey(ue, appendEmailAddress);

            //get from Cache
            Dictionary<Guid, string> cachedReturnTable = GetFromCache(cKey);
            if (null != cachedReturnTable)
                return cachedReturnTable;

            //NEW            
            //using (SqlConnection sqlConn = new SqlConnection(@"user id=sa;password=" + Globals.DatabasePwd + ";persist security info=False;Data Source=" + Globals.DatabaseServer + ";Initial Catalog=" + Globals.DatabaseName + ";"))
            using (SqlConnection sqlConn = new SqlConnection(DealMaker.Network.Util.GetConnectionStringFromServerName(Globals.DatabaseServer)))
            {
                try
                {
                    if (null != sqlConn/*Connection.getInstance().HatchConn*/)
                    {
                        sqlConn.Open();

                        lock (sqlConn/*Connection.getInstance().HatchConn*/)
                        {
                            if (UserEntries.OfficeStaffList == ue)
                                command = new SqlCommand("GetOfficeStaffList", sqlConn/*Connection.getInstance().HatchConn*/);
                            else if (UserEntries.LeadSourceForSellingList == ue)
                                command = new SqlCommand("GetLeadSourceList", sqlConn/*Connection.getInstance().HatchConn*/);

                            command.CommandType = CommandType.StoredProcedure;

                            if (null != command)
                            {
                                reader = command.ExecuteReader();

                                while (reader.Read())
                                {
                                    Guid key = !reader.IsDBNull(0) ? reader.GetGuid(0) : Guid.Empty;
                                    string value = !reader.IsDBNull(1) ? reader.GetString(1) : string.Empty;

                                    if (UserEntries.OfficeStaffList == ue && appendEmailAddress)
                                    {
                                        string emailAddresses = string.Empty;
                                        try { emailAddresses = !reader.IsDBNull(2) ? reader.GetString(2) : string.Empty; }
                                        catch { emailAddresses = string.Empty; }

                                        if (string.Empty != emailAddresses)
                                            value += " @: " + emailAddresses;
                                    }

                                    returnTable.Add(key, value);
                                }
                            }
                        }
                    }
                }
                catch
#if DEBUG
 (
                        Exception exc
                    )
#endif

                {
#if DEBUG
                    System.Windows.Forms.MessageBox.Show(exc.Message);
#endif
                    return null;
                }

                finally
                {
                    if (null != reader)
                        reader.Close();

                    if (null != sqlConn && ConnectionState.Open == sqlConn.State)
                        sqlConn.Close();
                }
            }

            //set to Cache
            SetToCache(cKey, returnTable);
            
            return returnTable;
        }

        public Dictionary<Guid, string> GetOfficeStaffWithEmailAddress()
        {
            SqlCommand command = null;
            SqlDataReader reader = null;
            Dictionary<Guid, string> returnTable = new Dictionary<Guid, string>();

            //using (SqlConnection sqlConn = new SqlConnection(@"user id=sa;password=" + Globals.DatabasePwd + ";persist security info=False;Data Source=" + Globals.DatabaseServer + ";Initial Catalog=" + Globals.DatabaseName + ";"))
            using (SqlConnection sqlConn = new SqlConnection(DealMaker.Network.Util.GetConnectionStringFromServerName(Globals.DatabaseServer)))
            {
                try
                {
                    sqlConn.Open();
                    command = new SqlCommand("GetOfficeStaffList", sqlConn/*Connection.getInstance().HatchConn*/);
                    command.CommandType = CommandType.StoredProcedure;

                    if (null != command)
                    {
                        reader = command.ExecuteReader();

                        while (reader.Read())
                        {
                            Guid key = !reader.IsDBNull(0) ? reader.GetGuid(0) : Guid.Empty;
                            string value = !reader.IsDBNull(1) ? reader.GetString(1) : string.Empty;
                            string emailAddresses = !reader.IsDBNull(2) ? reader.GetString(2) : string.Empty;

                            if (string.Empty != emailAddresses)
                                returnTable.Add(key, value);
                        }
                    }
                }
                catch
#if DEBUG
 (
                        Exception exc
                    )
#endif

                {
#if DEBUG
                    System.Windows.Forms.MessageBox.Show(exc.Message);
#endif
                    return null;
                }

                finally
                {
                    if (null != reader)
                        reader.Close();

                    if (null != sqlConn && ConnectionState.Open == sqlConn.State)
                        sqlConn.Close();
                }
            }

            return returnTable;
        }

        Dictionary<Guid, string> CachedEmailAddresses = new Dictionary<Guid, string>();

        string GetEmailFromCache(Guid key)
        {
            if (null != CachedValues)
            {
                lock (CachedValues)
                {
                    if (CachedEmailAddresses.ContainsKey(key))
                        return CachedEmailAddresses[key];
                }
            }
            return null;
        }

        void SetEmailToCache(Guid key, string value)
        {
            if (null != CachedValues)
            {
                lock (CachedValues)
                {
                    if (!CachedEmailAddresses.ContainsKey(key))
                        CachedEmailAddresses.Add(key, value);
                }
            }
        }

        /// <summary>
        /// Get Email Addresses assigned to certain contact
        /// </summary>
        /// <param name="ue"></param>
        /// <returns></returns>
        public string GetEmailAddresses(Guid id)
        {
            SqlCommand command = null;
            SqlDataReader reader = null;
            string retVal = string.Empty;

            //get from Cache
            string cachedEmailAddress = GetEmailFromCache(id);
            if (null != cachedEmailAddress)
                return cachedEmailAddress;

            //using (SqlConnection sqlConn = new SqlConnection(@"user id=sa;password=" + Globals.DatabasePwd + ";persist security info=False;Data Source=" + Globals.DatabaseServer + ";Initial Catalog=" + Globals.DatabaseName + ";"))
            using (SqlConnection sqlConn = new SqlConnection(DealMaker.Network.Util.GetConnectionStringFromServerName(Globals.DatabaseServer)))
            {
                try
                {
                    sqlConn.Open();
                    //if (null != Connection.getInstance().HatchConn)
                    {
                        //lock (Connection.getInstance().HatchConn)                    
                        {                            
                            command = new SqlCommand("GetOfficeStaffList", sqlConn /*Connection.getInstance().HatchConn*/);
                            command.CommandType = CommandType.StoredProcedure;

                            if (null != command)
                            {
                                reader = command.ExecuteReader();

                                while (reader.Read())
                                {
                                    Guid key = !reader.IsDBNull(0) ? reader.GetGuid(0) : Guid.Empty;
                                    string value = string.Empty;

                                    try { value = !reader.IsDBNull(2) ? reader.GetString(2) : string.Empty; }
                                    catch { value = string.Empty; }

                                    SetEmailToCache(key, value);

                                    if (key == id)
                                    {
                                        retVal = value;
                                        //try { retVal = !reader.IsDBNull(2) ? reader.GetString(2) : string.Empty; }
                                        //catch { retVal = string.Empty; }
                                        //break;
                                    }
                                }
                            }
                        }
                    }
                }
                catch
#if DEBUG
 (
                        Exception exc
                    )
#endif

                {
#if DEBUG
                    System.Windows.Forms.MessageBox.Show(exc.Message);
#endif
                    return null;
                }

                finally
                {
                    if (null != reader)
                        reader.Close();

                    if (null != sqlConn && ConnectionState.Open == sqlConn.State)
                        sqlConn.Close();
                }
            }

            return retVal;
        }

        /// <summary>
        /// Add Entry to DB
        /// </summary>
        /// <param name="ue"></param>
        public bool AddEntry(UserEntries ue, string value)
        {
            SqlCommand command = null;
            SqlDataReader reader = null;
            bool res = true;

            //using (SqlConnection sqlConn = new SqlConnection(@"user id=sa;password=" + Globals.DatabasePwd + ";persist security info=False;Data Source=" + Globals.DatabaseServer + ";Initial Catalog=" + Globals.DatabaseName + ";"))
            using (SqlConnection sqlConn = new SqlConnection(DealMaker.Network.Util.GetConnectionStringFromServerName(Globals.DatabaseServer)))
            {
                sqlConn.Open();
                try
                {
                    if (UserEntries.OfficeStaffList == ue)
                    {
                        command = new SqlCommand("Insert_OfficeStaff", sqlConn/*Connection.getInstance().HatchConn*/);

                        command.Parameters.AddWithValue("@officeStaffId", Guid.NewGuid());
                        command.Parameters.AddWithValue("@officeStaffName", value);
                    }
                    else if (UserEntries.LeadSourceForSellingList == ue)
                    {
                        command = new SqlCommand("Insert_LeadSource", sqlConn/*Connection.getInstance().HatchConn*/);

                        command.Parameters.AddWithValue("@leadSourceId", Guid.NewGuid());
                        command.Parameters.AddWithValue("@leadSourceName", value);
                    }
                    command.CommandType = CommandType.StoredProcedure;
                    command.ExecuteNonQuery();

                    ClearCache();

                }
                catch
#if DEBUG
 (
                        Exception exc
                    )
#endif

                {
#if DEBUG
                    System.Windows.Forms.MessageBox.Show(exc.Message);
#endif
                    res = false;
                }
                finally
                {
                    if (null != reader)
                        reader.Close();

                    if (null != sqlConn && ConnectionState.Open == sqlConn.State)
                        sqlConn.Close();
                }
            }
            return res;
        }

        /// <summary>
        /// Delete entry
        /// </summary>
        /// <param name="ue"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool DeleteEntry(UserEntries ue, Guid id)
        {
            SqlCommand command = null;
            SqlDataReader reader = null;
            bool res = true;

            //using (SqlConnection sqlConn = new SqlConnection(@"user id=sa;password=" + Globals.DatabasePwd + ";persist security info=False;Data Source=" + Globals.DatabaseServer + ";Initial Catalog=" + Globals.DatabaseName + ";"))
            using (SqlConnection sqlConn = new SqlConnection(DealMaker.Network.Util.GetConnectionStringFromServerName(Globals.DatabaseServer)))
            {

                try
                {
                    sqlConn.Open();

                    if (UserEntries.OfficeStaffList == ue)
                    {
                        command = new SqlCommand("Delete_OfficeStaff", sqlConn/*Connection.getInstance().HatchConn*/);
                        command.Parameters.AddWithValue("@officeStaffId", id);
                    }
                    else if (UserEntries.LeadSourceForSellingList == ue)
                    {
                        command = new SqlCommand("Delete_LeadSource", sqlConn/*Connection.getInstance().HatchConn*/);
                        command.Parameters.AddWithValue("@leadSourceId", id);
                    }
                    command.CommandType = CommandType.StoredProcedure;
                    command.ExecuteNonQuery();

                    ClearCache();
                }
                catch
#if DEBUG
 (
                        Exception exc
                    )
#endif

                {
#if DEBUG
                    System.Windows.Forms.MessageBox.Show(exc.Message);
#endif
                    res = false;
                }
                finally
                {
                    if (null != reader)
                        reader.Close();

                    if (null != sqlConn && ConnectionState.Open == sqlConn.State)
                        sqlConn.Close();
                }
            }

            return res;

        }


        /// <summary>
        /// Update entry (update assigned email addresses)
        /// </summary>
        /// <param name="ue"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool UpdateEntry(Guid id, string emailAddresses)
        {
            SqlCommand command = null;
            SqlDataReader reader = null;
            bool res = true;

            //using (SqlConnection sqlConn = new SqlConnection(@"user id=sa;password=" + Globals.DatabasePwd + ";persist security info=False;Data Source=" + Globals.DatabaseServer + ";Initial Catalog=" + Globals.DatabaseName + ";"))
            using (SqlConnection sqlConn = new SqlConnection(DealMaker.Network.Util.GetConnectionStringFromServerName(Globals.DatabaseServer)))
            {

                try
                {
                    sqlConn.Open();

                    command = new SqlCommand("Update_OfficeStaff", sqlConn/*Connection.getInstance().HatchConn*/);
                    command.Parameters.AddWithValue("@OfficeStaffId", id);
                    command.Parameters.AddWithValue("@OfficeStaffEmailAddresses", emailAddresses);

                    command.CommandType = CommandType.StoredProcedure;
                    command.ExecuteNonQuery();

                    ClearCache();
                }
                catch
#if DEBUG
 (
                        Exception exc
                    )
#endif

                {
#if DEBUG
                    System.Windows.Forms.MessageBox.Show(exc.Message);
#endif
                    res = false;
                }
                finally
                {
                    if (null != reader)
                        reader.Close();

                    if (null != sqlConn && ConnectionState.Open == sqlConn.State)
                        sqlConn.Close();
                }
            }
                
            return res;

        }

    }
}
