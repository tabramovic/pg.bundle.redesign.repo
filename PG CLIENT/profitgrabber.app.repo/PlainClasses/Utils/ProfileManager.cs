//PROFILE MANAGER

using System;
using System.IO;
using System.Windows.Forms;

namespace DealMaker.PlainClasses.Utils
{
	/// <summary>
	/// Summary description for ProfileManager.
	/// </summary>
	public class ProfileManager
	{
		string _fullProfilePath;
		bool _popupQuestion = false;

		/// <summary>
		/// CTR
		/// </summary>
		/// <param name="fullProfilePath">Full Profile Path</param>
		public ProfileManager(string fullProfilePath)
		{
			_fullProfilePath = fullProfilePath;
		}

		/// <summary>
		/// CTR
		/// </summary>
		/// <param name="fullProfilePath">Full Profile Path</param>
		/// <param name="popupQuestion">Pop up "Are you sure..." YES /NO question</param>
		public ProfileManager(string fullProfilePath, bool popupQuestion)
		{
			_fullProfilePath = fullProfilePath;
			_popupQuestion = popupQuestion;
		}

		public bool DeleteProfile(out string msg)
		{
			bool bRes = true;
			msg = string.Empty;

			if (_popupQuestion)
			{
				if (DialogResult.Yes != MessageBox.Show("Delete " + _fullProfilePath + "?", "Delete profile", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
					return true;
			}

			try
			{
				if (!File.Exists(_fullProfilePath))
					throw new FileNotFoundException("File " + _fullProfilePath + " not found", _fullProfilePath);

				File.Delete(_fullProfilePath);
			}
			catch (FileNotFoundException fnfExc)
			{
				bRes = false;
				msg = fnfExc.Message;
			}
			catch (Exception 
#if DEBUG
				exc
#endif
				)
			{
#if DEBUG
				MessageBox.Show(exc.Message);
#endif
				bRes = false;
				msg = "General Error!";
			}
			return bRes;
		}
	}
}
