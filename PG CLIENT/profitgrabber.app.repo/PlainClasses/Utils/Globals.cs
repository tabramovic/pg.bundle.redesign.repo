using System;
using System.Collections;
using System.Drawing;
using NHibernate;
using NHibernate.Cfg;
using System.IO;
using WindowsApplication.AcquisitionCalculator;
using System.Collections.Specialized;
using System.Configuration;
using System.Threading;

using DealMaker.Registration;



namespace DealMaker
{
	public class Globals
	{

		public const string ClientId = "720630394904-lksbl2g8j9ohiroshn5e2g4r8r15jbjt.apps.googleusercontent.com";
		public const string ClientSecret = "FOV0Psg1rXvLgAas7y8iVype";
		public const string UserToAuthorize = "ProfitGrabber";

		public static string GetUserToAuthorize(string user)
        {
			if (string.IsNullOrEmpty(user))
				return UserToAuthorize;
			else
				return user;
        }

        public static string EmailBlastFileName = "emailblastsig.txt";
        public static string EmailBlastSig = string.Empty;

        public static string EmailLOIFileName = "emailloisig.txt";
        public static string EmailLOISig = string.Empty;

        public const string ProductName = "ProfitGrabber Pro";

		public const bool NwVerEnabled = true;

		public const string NoCreditForGivenMonth = "1u2n3f";

		public static string[] ZIPsToExclude = new string[]
			{
				"92131", "92130", "92129", "92128", "92127", "92126"
			};
		public static string PGDatabaseSelectService = "PGDatabaseSelectService";
		public static string MSDE = "MSSQL$PG_DB2";


		public static bool AbortAllThreads = false;
		//sasa: 14.01.06, added app for network lock request
		public static Guid ApplicationID = Guid.NewGuid();

		//TA - locking / unlocking features
		public static LockingObject lockObj = null;
			
		public static string StartUpPath = string.Empty;
		//sasa: 12.12.05, network bugfix, contact screen loaded variable
		public static bool ContactScreenOnMainForm = false;

		public static bool ExitApplicationAfterLoad = false;

		//sasa: 20.11.05, select group
		public static DealMaker.SelectGroup SelectGroup;

		//sasa: 20.11.05, task manager		
		public static DealMaker.Forms.SchedulingActicities.TaskManager TaskManager;

		//sasa: 13.11.05, thread for nw udp listener
        public static System.Windows.Forms.Form MainForm;

		public static Thread NetworkUdpThreadListener;

		//sasa: 06.11.05, for task manager icon thread sleep time

		public const string RegEntry_Database = @"Software\TKS\Profit Grabber Pro\Database";

		public static int SelectedGroupInListView = -1;
		//public static int SelectedGroupInContactView = -1;

		public static ActiveModules activeModules = null;
			
		
		//TA:09.Nov.2005.
		public static bool ExpandedFields = false;
		public static bool breakWaitingPeriod = false;

		//sasa: 17.09.05, for task manager icon thread sleep time
		public static int CheckPendingTasksThread_MsecTimeout = 10000;
		public static int CheckPendingTasksThread_BlinkTime = 500;

		public static Thread taskManagerChecker = null;
		public static Thread blinkTaskManager = null;
		
		public static int registrationTries = 0;
		public static int registrationPeriod = 0;
		public static DateTime dtLastRegistrationTry;
		public static string RegistrationConst = "bc37rtngien1";
		public static Thread registrationChecker = null;
		public static Thread registrationReminder = null;

		public static string ProfitGrabberWebSupportURL = string.Empty;
		public static string ProfitGrabberWebSupportURLDataConsumption = string.Empty;
		
		public const int NotRegisteredConst  = 0x0404;
		public const int RegisteredConst     = 0x12AB;
		public static bool Registered = false;
		

		public static Registration.Address			UserAddress				= null;
		public static Registration.LicenceType		UserLicenceType			= null;
		public static Registration.Product			UserProduct				= null;
		public static DataConsumption.User			UserDataConsumption		= null;
		public static DataConsumption.Registration	DataConsumptionReg		= null;
//		public static AppRegistrationUser			UserRegistration		= null;	 //derived from Registration.User
//		public static AppRegistrationRegistration	RegistrationRegistration = null; //derived from Registration.Registration 
		public static Registration.User				UserRegistration		= null;	 //derived from Registration.User
		public static Registration.Registration 	RegistrationRegistration = null; //derived from Registration.Registration 


		public static PropertyItem CurrentPropertyItem = null;

		//ReturnMailWizardConsts
		public static Hashtable htRetMailWizardItemPosition = null;

		public const string MRWContactId		= "ContactId";	
		public const string MRWLNSAddress		= "LastKnownStreetAddress";
		public const string MRWLNSNumber		= "LastKnownStreetNumber";
		public const string MRWLNSPreDir		= "LastKnownStreetPreDirectional";
		public const string MRWLNSName			= "LastKnownStreetName";
		public const string MRWLNSPostDir		= "LastKnownStreetPostDirectional";
		public const string MRWLNSSuffix		= "LastKnownStreetSuffix";
		public const string MRWLNSUnitNumber	= "LastKnownStreetUnitNumber";
		public const string MRWLNCity			= "LastKnownCity";
		public const string MRWLNState			= "LastKnownState";
		public const string MRWLNZip			= "LastKnownZIP";
		public const string MRWSTPhoneNr1		= "SkipTracedPhoneNumber1";
		public const string MRWSTPhoneNr2		= "SkipTracedPhoneNumber2";

		public static int[] whiteBgIndex	= {
									   -8, -7, -6, -5, -4, -3, -2, -1, 0, 1, 2, 3, 4,
                                       18, 19, 20 , 21, 22, 23, 24, 25, 26, 27, 28, 
									   38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 
									   67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 
									   99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 113, 114, 115
									  };
		
		public static int[] grayBgIndex	= {
									   7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 
									   29, 30, 31, 32, 33, 34, 35, 36, 37, 
									   52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 
									   78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 
									   500, 501, 502, 503, 504, 505, 506, 507
									  };
		
		//Format Provider
		public static System.IFormatProvider format_en_US = new System.Globalization.CultureInfo("en-US", true);
		
		//Letter Of Interest
		public const string LOI = "LOI";
		public const string OfferedPrice = "Offered Price";
        public const string AlphaOfferedPrice = "Alpha Offered Price";

		//MailFaxWizard
		public const string LargeLabel = "Large Label";
		public const string MediumLabel = "Medium Label";
		public const string SmallLabel = "Small Label";
		
		//for group task list event handling (info about group changed)
		public static CustomizedTreeViewUserControl customizedTreeViewUserControl;

		//Progress Bar
		public static int Capacity = 1;
		
		//Printing 
		//LargeLabels
		public static int largeLabelsCount = 0;
		public static int mediumLabelsCount = 0;
		public static int smallLabelsCount = 0;
		
		//History
		public static PropertyItemListViewControl oldListView = null;
		public static FindPropertyItem			  oldFindPropertyItem = null;
		public static EditDocumentTemplateUserControl oldEditDocumentTemplateUserControl = null;
		public static UserControl_AcquisitionCalculator oldAcquisitionCalculator = null;
		public static ContactScreen oldContactScreen = null;

		//NHibernate
		public static ISessionFactory factory = null;
		public static NHibernate.Cfg.Configuration cfg = null;

		public const int    LeadsNotProcessedYetId	= 6;
		public const int    DealsInProgress			= 7;
		public const int    DeadLeads				= 8;
		public const int    DoNotMailGroup			= 21;
		public const int    Purchased				= 9;
		public const int	Pending					= 10;


		public const string IsEqual = "Equals";
		public const string DoesNotEqual = "Does Not Equal";
		public const string IsGreaterThan = "Is greater than";
		public const string IsGreaterThanOrEqualTo = "Is greater than or equal to";
		public const string IsLessThan = "Is less than";
		public const string IsLessThanOrEqualTo = "Is less than or equal to";

		// ihuk, 05.01.2005
		public static object[] FilterConditions = new object[] {
			Globals.IsEqual, Globals.DoesNotEqual, 
			Globals.IsGreaterThan, Globals.IsGreaterThanOrEqualTo,
			Globals.IsLessThan,  Globals.IsLessThanOrEqualTo};

		public const string Plus = "Plus";
		public const string Minus = "Minus";

		// ihuk, 05.01.2005
		public static object[] MathematicalFilterOperations = new object[] {
			Globals.Plus, Globals.Minus };

		public static bool  importWizardStarted = false;
		public static Point importWindowPos = new Point(importWizardStartXPos, importWizardStartYPos);
		
		public const  int importWizardStartXPos = 50;
		public const  int importWizardStartYPos = 50;

		//currenty selected group node
		public static int selectedNodeId = -1;
		public static string selectedNodeName = string.Empty;
		
		public static int selectedExcelIndex = -1;
		public static string selectedExcelFile = string.Empty;

		public static Size	applicationSize;
		
		/// <summary>
		/// Key = MapperObjectInfo (propertyName, propertyDesc)
		/// Value = MapperSourceInfo (columnIndex, columnDesc[if any])
		/// 
		/// We bind "column index value" with mapper object info which defines property in ImportIntermediateObject.
		/// </summary>
		public static Hashtable htMappingInfo = new Hashtable();

		
		/// <summary>
		/// Hashtable that contains SimpleFilter (max. 4) and mathematical filter (max. 1)
		/// </summary>
		public static Hashtable htFilters = new Hashtable();

		/// <summary>
		/// 29.Jan.2006 - zip filter (import wizard) added
		/// </summary>
		public static ArrayList zipFilter = new ArrayList();

		
		public static OwnerName OwnerSpecification = OwnerName.Specified;

		public const string ProfilePath = @"\Profiles\Import Profiles";
		public const string ExportProfilesPath = @"\Profiles\Export Profies";
		public const string ReturnedMailUpdateDataProfilePath = @"\Profiles\Return Mail Import Profiles";
		public const string ReturnedMailProfilePath = @"\Profiles\Return Mail Export Profiles";
		public const string ReturnedMailExportData  = @"Return Mail Exported Data";
		public const string MailPath = @"\Document Templates\Marketing Templates\Letter Templates\";
		public const string FaxPath  = @"\Document Templates\Marketing Templates\Fax Templates\";
        public const string EmailPath = @"\Document Templates\Marketing Templates\Email Templates\";
        public const string DocumentsPath  = @"\Documents\";
		public const string DocumentTemplatesPath  = @"\Document Templates\";
        public const string ExportedRoutes = @"\ExportedRoutes\";

        public const char		TabSeparator = '\t';
		public const string		ProfileExtention = ".pfl";
		public const string		MapperDelimiter = "Filters";
		public const string		SimpleFilterStarter = "SFStart";
		public const string		SimpleFilterEnder = "SFEnd";
		public const string		MathFilterStarter = "MFStart";
		public const string		MathFilterEnder = "MFEnd";
		public const string     ZIPFilterStarter = "ZIPStart";
		public const string     ZIPFilterEnder = "ZIPEnd";
		public const string     DateFilterStarter = "DateStart";
		public const string     DateFilterEnder = "DateEnd";
		

		public static string DatabaseName = "dbDealMaker";
		public static string DatabaseServer = @"localhost\PG_DB2";
        public static string DatabasePwd = "123456789.Aa!"; //"xxxxx"; //"123456789.Aa!";

        //sasa, 15.01.2006: added for nw lock
        ////sasa, 24.03.06: bug removed when no db selected nw 2 lock didn't work
        //sasa, 1.7.06: returned, when no db selected ip should be localhost
        public static string DatabaseServerIP = "127.0.0.1";//= "127.0.0.1";

		//public static string DatabaseServer = @"LOCALHOST\DEFAULT";		

		public static string RootNode = "All Groups";

		public static string GetConnectionString()
		{
			//prije integ
//			return (String.Format("data source={0};initial catalog={1};Integrated Security=SSPI;", DatabaseServer, DatabaseName));
			//sasa
			//CHANGE !!!
//						return (String.Format("data source={0};initial catalog={1};Integrated Security=SSPI;", DatabaseServer, DatabaseName));
			//			return (String.Format("user id=sa;password=xxxxx;persist security info=False;data source={0};initial catalog={1}", DatabaseServer, DatabaseName));

			return Network.Util.GetConnectionStringFromServerName(DatabaseServer);
		}
		
		public static bool InitNHibernate()
		{
			try
			{
                Globals.cfg = new NHibernate.Cfg.Configuration();				
				Globals.cfg.AddAssembly("DealMaker");
                //Globals.cfg.AddAssembly("ProfitGrabberPro");
//				Globals.cfg.Properties["hibernate.connection.connection_string"] = Connection.getInstance().HatchConn.ConnectionString;
//
				//sasa
				Globals.cfg.Properties["hibernate.connection.connection_string"] = Globals.GetConnectionString(); //Connection.getInstance().HatchConn.ConnectionString;

				Globals.factory = Globals.cfg.BuildSessionFactory();
			}
			catch (Exception exc)
			{
				System.Windows.Forms.MessageBox.Show(exc.Message);
				return false;
			}
			return true;
		}

		public static void ReadURLForWebSupport()
		{
			if (string.Empty == Globals.ProfitGrabberWebSupportURL)
			{
				//read from app.config			
				NameValueCollection nvc = (NameValueCollection)ConfigurationSettings.GetConfig("ProfitGrabberWebSupport");
				if (null != nvc && nvc.Count != 0)
				{
					Globals.ProfitGrabberWebSupportURL = (string)nvc["URL"];					
					Globals.ProfitGrabberWebSupportURLDataConsumption = (string)nvc["URLDataConsumption"];					
				}
			}
		}

		public static bool Load()
		{
			try
			{
				Globals.DatabaseServer	= RegUtil.ReadFromRegistry(RegUtil.REGUTIL_KEYS.HKLM, @"Software\DM", "DBServerName", string.Empty);
			}
			catch
			{
				return (false);
			}
			return (true);
		}
		public static bool Save(string serverName)
		{
			try
			{								
				RegUtil.WriteToReg(RegUtil.REGUTIL_KEYS.HKLM, @"Software\DM", "DBServerName", serverName);
				//sasa
				//Globals.DatabaseServer = serverName; -- must restart
			}
			catch
			{
				return (false);
			}
			return (true);
		}
	}
}
