﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace DealMaker
{
    public sealed class DataLinkManager
    {
        static readonly DataLinkManager _instance = new DataLinkManager();

        DataLinkManager()
        { }

        static DataLinkManager()
        { }

        public static DataLinkManager Instance
        {
            get { return _instance; }
        }

        public string GetDataLink(Guid pgId, DataLinkTypes dataLinkType)
        {
            SqlCommand command = null;
            SqlDataReader reader = null;
            string returnValue = string.Empty;

            try
            {
                command = new SqlCommand("GetDataLink", Connection.getInstance().HatchConn);                
                command.Parameters.AddWithValue("@pgId", pgId);
                command.Parameters.AddWithValue("@dataLinkType", (int)dataLinkType);                

                command.CommandType = CommandType.StoredProcedure;

                if (null != command)
                {
                    reader = command.ExecuteReader();

                    while (reader.Read())
                    {
                        returnValue = !reader.IsDBNull(0) ? reader.GetString(0) : string.Empty;                        
                    }
                }
            }
            catch
#if DEBUG
 (
                    Exception exc
                )
#endif

            {
#if DEBUG
                System.Windows.Forms.MessageBox.Show(exc.Message);
#endif
                return null;
            }

            finally
            {
                if (null != reader)
                    reader.Close();
            }

            return returnValue;
        }

        public bool AddDataLink(Guid pgId, DataLinkTypes dataLinkType, string value)
        {
            SqlCommand command = null;
            SqlDataReader reader = null;
            bool res = true;

            try
            {
                command = new SqlCommand("InsertDataLink", Connection.getInstance().HatchConn);
                command.Parameters.AddWithValue("@pgId", Guid.NewGuid());
                command.Parameters.AddWithValue("@dataLinkType", value);
                command.Parameters.AddWithValue("@dataLink", value);                
                command.CommandType = CommandType.StoredProcedure;
                command.ExecuteNonQuery();

            }
            catch
#if DEBUG
 (
                    Exception exc
                )
#endif

            {
#if DEBUG
                System.Windows.Forms.MessageBox.Show(exc.Message);
#endif
                res = false;
            }
            finally
            {
                if (null != reader)
                    reader.Close();
            }
            return res;
        }

        public bool UpdateDataLink(Guid pgId, DataLinkTypes dataLinkType, string value)
        {
            SqlCommand command = null;
            SqlDataReader reader = null;
            bool res = true;

            try
            {
                command = new SqlCommand("UpdateDataLink", Connection.getInstance().HatchConn);
                command.Parameters.AddWithValue("@pgId", Guid.NewGuid());
                command.Parameters.AddWithValue("@dataLinkType", value);
                command.Parameters.AddWithValue("@dataLink", value);
                command.CommandType = CommandType.StoredProcedure;
                command.ExecuteNonQuery();

            }
            catch
#if DEBUG
 (
                    Exception exc
                )
#endif

            {
#if DEBUG
                System.Windows.Forms.MessageBox.Show(exc.Message);
#endif
                res = false;
            }
            finally
            {
                if (null != reader)
                    reader.Close();
            }
            return res;
        }


        public bool DeleteDataLink(Guid pgId, DataLinkTypes dataLinkType)
        {
            SqlCommand command = null;
            SqlDataReader reader = null;
            bool res = true;

            try
            {
                command = new SqlCommand("DeleteDataLink", Connection.getInstance().HatchConn);
                command.Parameters.AddWithValue("@pgId", pgId);
                command.Parameters.AddWithValue("@dataLinkType", dataLinkType);                
                command.CommandType = CommandType.StoredProcedure;
                command.ExecuteNonQuery();

            }
            catch
#if DEBUG
 (
                    Exception exc
                )
#endif

            {
#if DEBUG
                System.Windows.Forms.MessageBox.Show(exc.Message);
#endif
                res = false;
            }
            finally
            {
                if (null != reader)
                    reader.Close();
            }
            return res;

        }

    }



    public enum DataLinkTypes
    {
        BuyerDocumentArchiveFolder = 0,
        DocumentArchiveFolderForProperty = 1,
        CorrespondenceLogFile = 2,
    }
}
