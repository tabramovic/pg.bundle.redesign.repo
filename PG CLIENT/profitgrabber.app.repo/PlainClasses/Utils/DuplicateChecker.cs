//Added
using DealMaker.PlainClasses.Duplicates;
using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Threading;


namespace DealMaker.PlainClasses.Utils
{
	internal enum eAddressType
		{
			eSiteAddress, 
			eMailAddress,
			eSecondAddress, 
			eThirdAddress,
		}
	public enum FindDuplicatesCriteria
	{
		NamesOnly = 1, 
		AddressOnly, 		
		Both,
		EitherNameOrAddress,
	}

	/// <summary>
	/// Summary description for DuplicateChecker.
	/// </summary>
	public sealed class DuplicateChecker
	{
		#region DATA
		static readonly DuplicateChecker _instance = new DuplicateChecker();

		const int Chunk = 25;

		SqlConnection _dbConn;
		bool _stopMatching;
		bool _forcedStopDeleting;
		bool _forcedStopLoading;
		bool _forcedStopMerging;

		//DELETE
		ArrayList _propertyItemsToDelete = null;
		//ContactEntry[] _contactsToDelete = null;
		
		//LOAD
		int[] _selectedGroupIds = null;
		int _totalContactsToLoad;
		ContactEntry[] _contactsLoaded = new ContactEntry[0];
		string _loadError = string.Empty;
		bool _loading = false;
		FindDuplicatesCriteria _criteria;

		//MERGE
		ArrayList _propertyItemsToMerge = null;
		#endregion
		
		#region CTR
		static DuplicateChecker()
		{
		}

		DuplicateChecker()
		{
            if (Globals.NwVerEnabled)
            {
                //_dbConn = new SqlConnection(@"user id=sa;password=" + Globals.DatabasePwd + ";persist security info=False;Data Source=" + Globals.DatabaseServer + ";Initial Catalog=" + Globals.DatabaseName + ";");
                _dbConn = new SqlConnection(DealMaker.Network.Util.GetConnectionStringFromServerName(Globals.DatabaseServer));
            }
            else
                _dbConn = new SqlConnection(String.Format("data source={0};initial catalog={1};Integrated Security=SSPI;", Globals.DatabaseServer, Globals.DatabaseName));


		}
		#endregion 

		#region PROPERTIES
		public static DuplicateChecker Instance
		{
			get { return _instance; }
		}

		public ContactEntry[] ContactsLoaded
		{
			get { return _contactsLoaded; }
		}		

		public bool StopMatching
		{
			get { return _stopMatching; }
			set { _stopMatching = value; }
		}

		public bool ForcedStopDeleting
		{
			get { return _forcedStopDeleting; }
			set { _forcedStopDeleting = value ;}
		}

		public bool ForcedStopLoading
		{
			get { return _forcedStopLoading; }
			set { _forcedStopLoading = value; }
		}

		public bool ForcedStopMerging
		{
			get { return _forcedStopMerging; }
			set { _forcedStopMerging = value; }
		}

		public bool Loading
		{
			get { return _loading; }
		}
		#endregion 

		public void ResetForcedAttributes()
		{
			StopMatching = false;
			ForcedStopLoading = false;
			ForcedStopMerging = false;
			ForcedStopDeleting = false;
										 
		}

		#region LOAD CONTACTS
		public ArrayList GetContactLastModifiedOlderThenInAffectedGroups(int nrOfMonths, int[] affectedGroups)
		{			
			ArrayList alResults = new ArrayList();
			string error = string.Empty;

			try { _dbConn.Open(); }
			catch (Exception exc)
			{ 
				error = exc.Message;				
			}

						
			SqlCommand getSqlCmd = null;
			SqlDataReader reader = null;

			try
			{

				string getSql = "SELECT PropertyItem.IdPropertyItem, NodeId FROM PropertyItem, NodePropertyItems WHERE NodePropertyItems.IdPropertyItem = PropertyItem.IdPropertyItem and DATEDIFF(month, DateLastModified, GETDATE()) > " + nrOfMonths.ToString() ;

				//CONT HERE
				if (null != affectedGroups && affectedGroups.Length > 0)
				{
					getSql += " and (";
					for (int i = 0; i < affectedGroups.Length; i++)
					{
						if (i != 0)
							getSql += " or ";
						
						getSql += "NodeId = " + affectedGroups[i];
					}
					getSql += ")";
				}


				getSqlCmd = new SqlCommand(getSql, _dbConn);
				reader = getSqlCmd.ExecuteReader();

				while (reader.Read())		
				{
					if (!reader.IsDBNull(0))
						alResults.Add(reader.GetGuid(0));					
				}								
			}
			catch (Exception exc)
			{
				error = exc.Message;				
			}
			finally
			{	
				if (null != reader && !reader.IsClosed)
					reader.Close();
										
				if (_dbConn.State != ConnectionState.Closed)
					try { _dbConn.Close(); } 
					catch {}
			}

			return alResults;
		}

		public bool GetContactCountInDB(int[] groupIds, out int count, out string error)
		{
			error = string.Empty;
			count = 0;

			try { _dbConn.Open(); }
			catch (Exception exc)
			{ 
				error = exc.Message;
				return false;
			}
						
			SqlCommand getCountCmd = null;
			SqlDataReader reader = null;
						
			try
			{
				foreach (int groupId in groupIds)
				{
					string getCountSql = "SELECT COUNT(*) FROM NodePropertyItems where NodeId=" + groupId.ToString();
					getCountCmd = new SqlCommand(getCountSql, _dbConn);
					reader = getCountCmd.ExecuteReader();

					while (reader.Read())		
						count += reader.GetInt32(0);					
					
					reader.Close();
				}
			}
			catch (Exception exc)
			{
				error = exc.Message;
				return false;
			}
			finally
			{
				if (null != reader && !reader.IsClosed)
					reader.Close();

				if (_dbConn.State != ConnectionState.Closed)
					try { _dbConn.Close(); } 
					catch {}
			}
			
			return true;
		}
		
		
		public delegate void PropertyItemLoadedHandler(int current, int total, bool forcedStop);
		public event PropertyItemLoadedHandler OnPropertyItemLoaded;

		public void LoadContactsInSelectedGroups(int[] groupIds, int total)
		{
			_selectedGroupIds = groupIds;
			_totalContactsToLoad = total;

			Thread loaderThread = new Thread(new ThreadStart(GetContacts));
			loaderThread.Start();
		}
		
		void GetContacts()
		{
			_loading = true;
			lock (_contactsLoaded)
			{
				_contactsLoaded = new ContactEntry[0];

				_loadError = string.Empty;

				ArrayList contactsInDb = new ArrayList();
			
				try { _dbConn.Open(); }
				catch (Exception exc)
				{ 
					_loadError = exc.Message;
					return;
				}
						
				SqlCommand getCountCmd = null;
				SqlDataReader reader = null;			
						
				try
				{
					int iCnt = 1;
					foreach (int groupId in _selectedGroupIds)
					{					
							
						if (!_forcedStopLoading)									
						{
							/*
								SELECT IdContact, FirstName, MiddleName, LastName, FullName, DateCreated, DateLastModified, 
								FullSiteStreetAddress, SiteStreetNumber, SiteStreetPreDirectional, SiteStreetName, 
								SiteStreetPostDirectional, siteStreetSuffix, SiteStreetUnitNumber,
								SiteCity, SiteState, SiteCitySiteState, SiteCitySiteStateSiteZIP, SiteZIP
							FROM Contact, PropertyItem, Address
							WHERE idContact in 
								(select owner from propertyitem where idpropertyitem in (select idpropertyitem from nodepropertyitems where nodeid=91)) 	
								and PropertyItem.Owner = Contact.IdContact
								and Contact.SiteAddress = Address.IdAddress
							 * */
							string getContactSql = "select IdContact, FirstName, MiddleName, LastName, FullName, DateCreated, DateLastModified, FullSiteStreetAddress, SiteStreetNumber, SiteStreetPreDirectional, SiteStreetName, SiteStreetPostDirectional, siteStreetSuffix, SiteStreetUnitNumber, SiteCity, SiteState, SiteCitySiteState, SiteCitySiteStateSiteZIP, SiteZIP from Contact, PropertyItem, Address where idContact in (select owner from propertyitem where idpropertyitem in (select idpropertyitem from nodepropertyitems where nodeid=" + groupId + ")) and PropertyItem.Owner = Contact.IdContact and Contact.SiteAddress = Address.IdAddress";

                            /*
                             * SELECT IdContact, FirstName, MiddleName, LastName, FullName, DateCreated, DateLastModified 
                                FROM Contact, PropertyItem 
                                WHERE idContact in 
	                                (select owner from propertyitem where idpropertyitem in (select idpropertyitem from nodepropertyitems where nodeid=47)) 	
	                                and PropertyItem.Owner = Contact.IdContact
	
                             */

                            ContactTypes.ContactTypes contactType = ContactTypes.ContactTypesManager.Instance.GetCustomTypeGroup(groupId);
                            if (ContactTypes.ContactTypes.Seller != contactType)
                                getContactSql = "SELECT IdContact, FirstName, MiddleName, LastName, FullName, DateCreated, DateLastModified FROM Contact, PropertyItem WHERE idContact in (select owner from propertyitem where idpropertyitem in (select idpropertyitem from nodepropertyitems where nodeid=" + groupId + "))and PropertyItem.Owner = Contact.IdContact";

                            getCountCmd = new SqlCommand(getContactSql, _dbConn);
							reader = getCountCmd.ExecuteReader();

				
							Guid contactId = Guid.Empty;
							string firstName = string.Empty;
							string middleName = string.Empty;
							string lastName = string.Empty;
							string fullName = string.Empty;
							DateTime dateCreated = DateTime.MinValue;
							DateTime dateLastModified = DateTime.MinValue;

							string fullSiteStreetAddress = string.Empty;
							string siteStreetNumber = string.Empty;
							string siteStreetPreDirectional = string.Empty;
							string siteStreetName = string.Empty; 
							string siteStreetPostDirectional = string.Empty;
							string siteStreetSuffix = string.Empty;
							string siteStreetUnitNumber = string.Empty;
							string siteCity = string.Empty;
							string siteState = string.Empty;
							string siteCitySiteState = string.Empty;
							string siteCitySiteStateSiteZIP = string.Empty;
							string siteZIP = string.Empty;


							while (reader.Read() && !_forcedStopLoading)		
							{
								if (!reader.IsDBNull(0))
									contactId = reader.GetGuid(0);
								else
									continue;

								if (!reader.IsDBNull(1))
									firstName = reader.GetString(1);
								else
									firstName = string.Empty;

								if (!reader.IsDBNull(2))
									middleName = reader.GetString(2);
								else
									middleName = string.Empty;

								if (!reader.IsDBNull(3))
									lastName = reader.GetString(3);
								else
									lastName = string.Empty;

								if (!reader.IsDBNull(4))
									fullName = reader.GetString(4);
								else
									fullName = string.Empty;

								if (!reader.IsDBNull(5))
									dateCreated = reader.GetDateTime(5);
								else
									dateCreated = DateTime.MinValue;

								if (!reader.IsDBNull(6))
									dateLastModified = reader.GetDateTime(6);
								else
									dateLastModified = DateTime.MinValue;


                                if (ContactTypes.ContactTypes.Seller == contactType)
                                {

                                    if (!reader.IsDBNull(7))
                                        fullSiteStreetAddress = reader.GetString(7);
                                    else
                                        fullSiteStreetAddress = string.Empty;

                                    if (!reader.IsDBNull(8))
                                        siteStreetNumber = reader.GetString(8);
                                    else
                                        siteStreetNumber = string.Empty;

                                    if (!reader.IsDBNull(9))
                                        siteStreetPreDirectional = reader.GetString(9);
                                    else
                                        siteStreetPreDirectional = string.Empty;

                                    if (!reader.IsDBNull(10))
                                        siteStreetName = reader.GetString(10);
                                    else
                                        siteStreetName = string.Empty;

                                    if (!reader.IsDBNull(11))
                                        siteStreetPostDirectional = reader.GetString(11);
                                    else
                                        siteStreetPostDirectional = string.Empty;

                                    if (!reader.IsDBNull(12))
                                        siteStreetSuffix = reader.GetString(12);
                                    else
                                        siteStreetSuffix = string.Empty;

                                    if (!reader.IsDBNull(13))
                                        siteStreetUnitNumber = reader.GetString(13);
                                    else
                                        siteStreetUnitNumber = string.Empty;

                                    if (!reader.IsDBNull(14))
                                        siteCity = reader.GetString(14);
                                    else
                                        siteCity = string.Empty;

                                    if (!reader.IsDBNull(15))
                                        siteState = reader.GetString(15);
                                    else
                                        siteState = string.Empty;

                                    if (!reader.IsDBNull(16))
                                        siteCitySiteState = reader.GetString(16);
                                    else
                                        siteCitySiteState = string.Empty;

                                    if (!reader.IsDBNull(17))
                                        siteCitySiteStateSiteZIP = reader.GetString(17);
                                    else
                                        siteCitySiteStateSiteZIP = string.Empty;

                                    if (!reader.IsDBNull(18))
                                        siteZIP = reader.GetString(18);
                                    else
                                        siteZIP = string.Empty;
                                }

								contactsInDb.Add(new ContactEntry(contactId, firstName, middleName, lastName, fullName, dateCreated, dateLastModified, fullSiteStreetAddress, 
									siteStreetNumber, siteStreetPreDirectional, siteStreetName, siteStreetPostDirectional, siteStreetSuffix, siteStreetUnitNumber, siteCity,
									siteState, siteCitySiteState, siteCitySiteStateSiteZIP, siteZIP  ));

								if (null != OnPropertyItemLoaded)
									OnPropertyItemLoaded(iCnt, _totalContactsToLoad, false); 

								iCnt++;						
							}														
							reader.Close();
						}
						else
						{
							if (null != OnPropertyItemLoaded)
								OnPropertyItemLoaded(iCnt, _totalContactsToLoad, true); 
							break;
						}					
					}
				}
				catch (Exception exc)
				{
					_loadError = exc.Message;				
				}
				finally
				{							
					if (_dbConn.State != ConnectionState.Closed)
						try { _dbConn.Close(); } 
						catch {}
				}

				//FOR inconsistant databases, force form close
				if (null != OnPropertyItemLoaded)
					OnPropertyItemLoaded(contactsInDb.Count, contactsInDb.Count, false);
			
				_contactsLoaded = (ContactEntry[])contactsInDb.ToArray(typeof(ContactEntry));
				_loading = false;
			}
		}		
				
		#endregion
		
		#region FIND DUPLICATES
		public delegate void Contact_A_BrowsedHandler(int current, int total, bool forcedStop);
		public event Contact_A_BrowsedHandler OnContact_A_Browsed;

		public delegate void FoundDuplicatesHandler(Hashtable duplicates);
		public event FoundDuplicatesHandler OnDuplicatesFound;

		public void FindDuplicates(FindDuplicatesCriteria criteria)
		{	
			_criteria = criteria;
			Thread findDuplicatesThread = new Thread(new ThreadStart(FindDuplicatesWorker));
			findDuplicatesThread.Start();
		}

		bool TestForDuplicate(FindDuplicatesCriteria criteria, ContactEntry ce1, ContactEntry ce2)
		{			
			if (criteria == FindDuplicatesCriteria.NamesOnly)
			{
				return (string.Empty != ce1.FullName && 
					string.Empty != ce2.FullName &&
					ce1.ContactId != ce2.ContactId &&
					ce1.FullName == ce2.FullName);
			}
			else if (criteria == FindDuplicatesCriteria.AddressOnly)
			{
				return (string.Empty != ce1.ComparableAddress &&
					string.Empty != ce2.ComparableAddress &&
					ce1.ContactId != ce2.ContactId &&
					//ce1.ComparableAddress == ce2.ComparableAddress);
                    AddressMatcher.Instance.Match(ce1.ComparableAddress, ce2.ComparableAddress));
			}
			else if (criteria == FindDuplicatesCriteria.EitherNameOrAddress)
			{
				bool retValForNames = (string.Empty != ce1.FullName && 
					string.Empty != ce2.FullName &&
					ce1.ContactId != ce2.ContactId &&
					ce1.FullName == ce2.FullName);

				bool retValForAddress = (string.Empty != ce1.ComparableAddress &&
					string.Empty != ce2.ComparableAddress &&
					ce1.ContactId != ce2.ContactId &&
					//ce1.ComparableAddress == ce2.ComparableAddress);
                    AddressMatcher.Instance.Match(ce1.ComparableAddress, ce2.ComparableAddress));

				return retValForNames || retValForAddress;
			}
			else	//both
			{
				return (string.Empty != ce1.FullName && 
					string.Empty != ce2.FullName &&
					ce1.ContactId != ce2.ContactId &&
					ce1.FullName == ce2.FullName &&
					string.Empty != ce1.ComparableAddress &&
					string.Empty != ce2.ComparableAddress &&					
					//ce1.ComparableAddress == ce2.ComparableAddress);
                    AddressMatcher.Instance.Match(ce1.ComparableAddress, ce2.ComparableAddress));
			}				
		}

		void FindDuplicatesWorker()
		{			
			Hashtable duplicatesX = new Hashtable();
			
			if (null != _contactsLoaded)
			{
				lock (_contactsLoaded)
				{											
					for (int i = 0; i < _contactsLoaded.Length; i++)
					{		
						if (!_stopMatching)
						{
							for (int j = i + 1; j < _contactsLoaded.Length; j++)
							{					
								ContactEntry cnt = _contactsLoaded[i];
								ContactEntry contact = _contactsLoaded[j];
																								
								if (TestForDuplicate(_criteria, cnt, contact))
								{
									//NEW
									if (!duplicatesX.ContainsKey(cnt.ContactId))
									{
										cnt.ContactEntryAdapter = i;
										duplicatesX.Add(cnt.ContactId, cnt);
									}

									if (!duplicatesX.ContainsKey(contact.ContactId))
									{
										contact.ContactEntryAdapter = i;
										duplicatesX.Add(contact.ContactId, contact);
									}
									//~NEW

								}						
							}
				
							if (null != OnContact_A_Browsed)				
								OnContact_A_Browsed(i + 1, _contactsLoaded.Length, false);
						}
						else
						{
							if (null != OnContact_A_Browsed)				
								OnContact_A_Browsed(i + 1, _contactsLoaded.Length, true);
						}

						if (_stopMatching)
							break;
					}											
				}
			}

			Hashtable finalResults = new Hashtable();

			for (int id = 0; id < _contactsLoaded.Length; id++)
			{
				ArrayList list = new ArrayList();
				IDictionaryEnumerator ide = duplicatesX.GetEnumerator();
				while (ide.MoveNext())
				{
					if (((ContactEntry)ide.Value).ContactEntryAdapter == id && !TestForContactInList(((ContactEntry)ide.Value).ContactId, list))
					{
						list.Add((ContactEntry)ide.Value);
					}																			
				}

				if (null != list && 0 < list.Count)
					finalResults.Add(id, list);
			}
					
			if (null != OnDuplicatesFound)
				OnDuplicatesFound(finalResults);
		}

		bool TestForContactInList(Guid contactId, ArrayList list)
		{
			foreach (ContactEntry ce in list)
			{
				if (ce.ContactId == contactId)
				{
					return true;	
				}
			}
			return false;
		}

		#endregion 
		
		#region DELETE PROPERTY ITEMS
		public delegate void PropertyItemDeletedHandler(int current, int total, bool forcedStop);
		public event PropertyItemDeletedHandler OnPropertyItemDeleted;

		public void DeletePropertyItems(ArrayList propertyItems)
		{
			_propertyItemsToDelete = propertyItems;

			Thread thdDelete = new Thread(new ThreadStart(DeletePropertyItems));
			thdDelete.Start();
		}

		void DeletePropertyItems()
		{			
			try { _dbConn.Open(); }
			catch 
			{ 
				//SILENT SO FAR
			}
						
			SqlCommand sp_DeletePropertyItem = new SqlCommand("DeletePropertyItem", _dbConn);				
			sp_DeletePropertyItem.CommandType = CommandType.StoredProcedure;
			
			int i = 1;
			foreach(Guid propertyItemId in _propertyItemsToDelete)
			{
				if (!_forcedStopDeleting)
				{					
					try
					{
						sp_DeletePropertyItem.Parameters.Clear();
						sp_DeletePropertyItem.Parameters.Add("@propertyItemId", propertyItemId);
						sp_DeletePropertyItem.Parameters.Add("@groupId", 1);	//when destroy is set to 1, SPL ignores groupId
						sp_DeletePropertyItem.Parameters.Add("@destroy", 1);																											
			
						sp_DeletePropertyItem.ExecuteNonQuery();	
		
						if (null != OnPropertyItemDeleted)
						{
							OnPropertyItemDeleted(i, _propertyItemsToDelete.Count, false);
						}
					}
					catch 
					{
						//SILENT SO FAR
					}
											
					i++;
				}
				else
				{
					if (null != OnPropertyItemDeleted)
					{
						OnPropertyItemDeleted(i, _propertyItemsToDelete.Count, true);
					}
					break;
				}
			}						

			string cleanUpComm = GetCleanUpSQLCode();
			
			SqlCommand comm = new SqlCommand(cleanUpComm, _dbConn);
			try
			{
				comm.ExecuteNonQuery();
			}
			catch {}	//SILENT SO FAR			


			if (_dbConn.State != ConnectionState.Closed)
				try { _dbConn.Close(); } 
				catch {}	
		}

		string GetCleanUpSQLCode()
		{
			//CLEAN UP AFTER SPL :( -- nHibernate takes 1 sec per PropertyItem
			/*In the next DB ver this goes as supplement to the SPL. 
			 * 
				delete from contact where contact.idcontact not in (select owner from propertyitem)

				delete from phone where idphone not in (select homephone from contact where homephone is not null) and
										idphone not in (select workphone from contact where workphone is not null) and
										idphone not in (select cellphone from contact where cellphone is not null)

				delete from address where idaddress not in (select firstaddress from contact where firstaddress is not null) and
											idaddress not in (select secondaddress from contact where secondaddress is not null) and
											idaddress not in (select thirdaddress from contact where thirdaddress is not null) and
											idaddress not in (select siteaddress from contact where siteaddress is not null) and
											idaddress not in (select mailaddress from contact where mailaddress is not null)
						
				delete from listingagent where idlistingagent not in (select listingagent from propertyitem where listingagent is not null)

				delete from dealproperty where dealproperty.iddealproperty not in (select dealproperty from propertyitem where dealproperty is not null)

				delete from dealinfo where dealinfo.iddealinfo not in (select dealinfo from propertyitem where dealinfo is not null)

				delete from houseproperties where idhouseproperty not in (select houseproperty from dealinfo where houseproperty is not null)

				delete from loanproperties where idloanproperty not in (select loanproperty from dealinfo where loanproperty is not null)

				delete from districtproperties where iddistrictproperty not in (select districtproperty from dealinfo where districtproperty is not null)

				delete from otherproperties where idotherproperty not in (select otherproperty from dealinfo where otherproperty is not null)

				delete from divorceproperties where iddivorceproperty not in (select divorceproperty from dealinfo where divorceproperty is not null)

				delete from leadsourceproperties where idleadsourceproperty not in (select leadsourceproperty from dealinfo where leadsourceproperty is not null)

				delete from taxproperties where idtaxproperty not in (select taxproperty from dealinfo where taxproperty is not null)

				delete from financialproperties where idfinancialproperty not in (select financialproperty from dealinfo where financialproperty is not null)
			 * */

			string cleanUpComm = string.Empty;
			cleanUpComm += "delete from contact where contact.idcontact not in (select owner from propertyitem)" + System.Environment.NewLine + System.Environment.NewLine;

			cleanUpComm += "delete from phone where idphone not in (select homephone from contact where homephone is not null) and " + System.Environment.NewLine;
			cleanUpComm += "idphone not in (select workphone from contact where workphone is not null) and " + System.Environment.NewLine;
			cleanUpComm += "idphone not in (select cellphone from contact where cellphone is not null) " + System.Environment.NewLine + System.Environment.NewLine;

			cleanUpComm += "delete from address where idaddress not in (select firstaddress from contact where firstaddress is not null) and " + System.Environment.NewLine;
			cleanUpComm += "idaddress not in (select secondaddress from contact where secondaddress is not null) and " + System.Environment.NewLine;
			cleanUpComm += "idaddress not in (select thirdaddress from contact where thirdaddress is not null) and " + System.Environment.NewLine;
			cleanUpComm += "idaddress not in (select siteaddress from contact where siteaddress is not null) and " + System.Environment.NewLine;
			cleanUpComm += "idaddress not in (select mailaddress from contact where mailaddress is not null) " + System.Environment.NewLine + System.Environment.NewLine;
						
			cleanUpComm += "delete from listingagent where idlistingagent not in (select listingagent from propertyitem where listingagent is not null) " + System.Environment.NewLine;
			cleanUpComm += "delete from dealproperty where dealproperty.iddealproperty not in (select dealproperty from propertyitem where dealproperty is not null) " + System.Environment.NewLine;
			cleanUpComm += "delete from dealinfo where dealinfo.iddealinfo not in (select dealinfo from propertyitem where dealinfo is not null) " + System.Environment.NewLine;
			cleanUpComm += "delete from houseproperties where idhouseproperty not in (select houseproperty from dealinfo where houseproperty is not null) " + System.Environment.NewLine;
			cleanUpComm += "delete from loanproperties where idloanproperty not in (select loanproperty from dealinfo where loanproperty is not null) " + System.Environment.NewLine;
			cleanUpComm += "delete from districtproperties where iddistrictproperty not in (select districtproperty from dealinfo where districtproperty is not null) " + System.Environment.NewLine;
			cleanUpComm += "delete from otherproperties where idotherproperty not in (select otherproperty from dealinfo where otherproperty is not null) " + System.Environment.NewLine;
			cleanUpComm += "delete from divorceproperties where iddivorceproperty not in (select divorceproperty from dealinfo where divorceproperty is not null) " + System.Environment.NewLine;
			cleanUpComm += "delete from leadsourceproperties where idleadsourceproperty not in (select leadsourceproperty from dealinfo where leadsourceproperty is not null) " + System.Environment.NewLine;
			cleanUpComm += "delete from taxproperties where idtaxproperty not in (select taxproperty from dealinfo where taxproperty is not null) " + System.Environment.NewLine;
			cleanUpComm += "delete from financialproperties where idfinancialproperty not in (select financialproperty from dealinfo where financialproperty is not null) " + System.Environment.NewLine;
			
			return cleanUpComm;
		}

		#endregion

		
		public delegate void PropertyItemMergedHandler(int current, int total, bool forcedStop);
		public event PropertyItemMergedHandler OnPropertyItemMerged;


		void MergePropertyItems(ArrayList contactPairToMatch, bool mergeNames)
		{			
			_propertyItemsToMerge = contactPairToMatch;

			Thread thdDelete = new Thread(new ParameterizedThreadStart(MergePropertyItems));
			thdDelete.Start(mergeNames);
		}
		
        string GetMergeDealPropertySqlCommand(Guid idFrom, Guid idTo)
        {
            string command = string.Empty;
            command += "update dest ";
            command += "set Type = src.Type, ";
            command += "Bedrooms = src.Bedrooms, ";
            command += "Baths = src.Baths, ";
            command += "Sqft = src.Sqft, ";
            command += "YearBuilt = src.YearBuilt, ";
            command += "Garage = src.Garage, ";
            command += "Pool = src.Pool, ";
            command += "OccupiedBy = src.OccupiedBy, ";
            command += "Apn = src.Apn, ";
            command += "FeatureCondition = src.FeatureCondition, ";
            command += "WhySelling = src.WhySelling, ";
            command += "WorthBySeller = src.WorthBySeller, ";
            command += "Source = src.Source, ";
            command += "FirstBal = src.FirstBal, ";
            command += "Ir1 = src.Ir1, ";
            command += "Type1 = src.Type1, ";
            command += "MoPay1 = src.MoPay1, ";
			command += "SecondBal = src.SecondBal, ";
			command += "Ir2 = src.Ir2, ";
			command += "Type2 = src.Type2, ";
			command += "MoPay2 = src.MoPay2, ";
			command += "ThirdBal = src.ThirdBal, ";
			command += "Ir3 = src.Ir3, ";
			command += "Type3 = src.Type3, ";
			command += "MoPay3 = src.MoPay3, ";
			command += "PmntsCurrent = src.PmntsCurrent, ";
            command += "MoBehind = src.MoBehind, ";
            command += "ReInsAmnt = src.ReInsAmnt, ";
            command += "InForecl = src.InForecl, ";
            command += "ForSaleDate = src.ForSaleDate, ";
            command += "Listed = src.Listed, ";
            command += "ListExpDate = src.ListExpDate, ";
            command += "CanCancelList = src.CanCancelList, ";
            command += "LowestPriceAcceptable = src.LowestPriceAcceptable, ";
            command += "Sell4WhatOwes = src.Sell4WhatOwes, ";
            command += "MinCashNeededWhy = src.MinCashNeededWhy, ";
            command += "DesiredClosingDate = src.DesiredClosingDate, ";
            command += "Motivation = src.Motivation, ";
            command += "LeadSource = src.LeadSource, ";
            command += "Arv = src.Arv, ";
            command += "Repairs = src.Repairs, ";
            command += "NextStep = src.NextStep, ";
			
			command += "TaxIns = src.TaxIns, ";
			command += "TaxAmnt = src.TaxAmnt, ";
			command += "Hoa = src.Hoa, ";
			command += "OtherLiens = src.OtherLiens ";

			command += "from DealProperty src, DealProperty dest ";
            command += "where dest.IdDealProperty = '" + idTo.ToString() + "' and ";
            command += "src.IdDealProperty = '" + idFrom.ToString() + "'";

            return command;
        }

		void MergePropertyItems(object mergeNames)
		{	
			try { _dbConn.Open(); }
			catch { }	//SILENT SO FAR 

			int i = 1;
			foreach (ArrayList list in _propertyItemsToMerge)
			{
				if (!ForcedStopMerging)
				{
					Guid fromId = ((ContactEntry)list[0]).ContactId;
					Guid toId = ((ContactEntry)list[1]).ContactId;

					Guid siteAddressFrom = Guid.Empty;
					Guid mailingAddressFrom = Guid.Empty;
					Guid secondAddessFrom = Guid.Empty;
					Guid thirdAddressFrom = Guid.Empty;

					Guid siteAddressTo = Guid.Empty;
					Guid mailingAddressTo = Guid.Empty;
					Guid secondAddessTo = Guid.Empty;
					Guid thirdAddressTo = Guid.Empty;
    
					SqlDataReader reader = null;                    
					SqlCommand comm;

const string updateContactDetailsSql = "UPDATE dest " +
"SET " +
	"dest.firstname = src.firstname " +
	", dest.lastname = src.lastname " +
	", dest.middlename = src.middlename " +
	", dest.fullname = src.fullname	" +
	", dest.stemail = src.stemail " +
	", dest.stcellphone = src.stcellphone " +
	", dest.stlandline = src.stlandline " +
	", dest.faxnumber = src.faxnumber " +
	", dest.email = src.email " +
	", dest.st_p1_response = src.st_p1_response " +
	"FROM " +
	"	CONTACT src, CONTACT dest " +
	"WHERE " +
		"     src.idContact = @srcIdContact " +
		" and dest.idContact = @destIdContact ";

					var updateContactDetailsSqlComm = new SqlCommand(updateContactDetailsSql, _dbConn);
					try
					{
						updateContactDetailsSqlComm.Parameters.AddWithValue("@srcIdContact", fromId);
						updateContactDetailsSqlComm.Parameters.AddWithValue("@destIdContact", toId);
						updateContactDetailsSqlComm.ExecuteNonQuery();
					}
					catch { }

const string updateHomePhoneSql = "update DestPhone " +
	"set DestPhone.PhoneNr = SrcPhone.PhoneNr, " +
	"DestPhone.BestTimeToCall = SrcPhone.BestTimeToCall " +
	"from Phone SrcPhone, Phone DestPhone, Contact SrcContact, Contact DestContact " +
	"where " +
	"SrcContact.IdContact = @fromId " +
	"and DestContact.IdContact = @toId " +
	"and SrcPhone.IdPhone = SrcContact.HomePhone " +
	"and DestPhone.IdPhone = DestContact.HomePhone ";

const string updateWorkPhoneSql = "update DestPhone " +
	"set DestPhone.PhoneNr = SrcPhone.PhoneNr, " +
	"DestPhone.BestTimeToCall = SrcPhone.BestTimeToCall " +
	"from Phone SrcPhone, Phone DestPhone, Contact SrcContact, Contact DestContact " +
	"where " +
	"SrcContact.IdContact = @fromId " +
	"and DestContact.IdContact = @toId " +
	"and SrcPhone.IdPhone = SrcContact.WorkPhone " +
	"and DestPhone.IdPhone = DestContact.WorkPhone ";

const string updateCellPhoneSql = "update DestPhone " +
	"set DestPhone.PhoneNr = SrcPhone.PhoneNr, " +
	"DestPhone.BestTimeToCall = SrcPhone.BestTimeToCall " +
	"from Phone SrcPhone, Phone DestPhone, Contact SrcContact, Contact DestContact " +
	"where " +
	"SrcContact.IdContact = @fromId " +
	"and DestContact.IdContact = @toId " +
	"and SrcPhone.IdPhone = SrcContact.CellPhone " +
	"and DestPhone.IdPhone = DestContact.CellPhone ";

					using (var sqlComm = new SqlCommand(updateHomePhoneSql, _dbConn))
                    {
						sqlComm.Parameters.AddWithValue("@fromId", fromId);
						sqlComm.Parameters.AddWithValue("@toId", toId);
						try { sqlComm.ExecuteNonQuery(); }
						catch {}
                    }

					using (var sqlComm = new SqlCommand(updateWorkPhoneSql, _dbConn))
					{
						sqlComm.Parameters.AddWithValue("@fromId", fromId);
						sqlComm.Parameters.AddWithValue("@toId", toId);
						try { sqlComm.ExecuteNonQuery(); }
						catch { }
					}

					using (var sqlComm = new SqlCommand(updateCellPhoneSql, _dbConn))
					{
						sqlComm.Parameters.AddWithValue("@fromId", fromId);
						sqlComm.Parameters.AddWithValue("@toId", toId);
						try { sqlComm.ExecuteNonQuery(); }
						catch { }
					}


					comm = new SqlCommand("select mailaddress, secondaddress, thirdaddress, siteaddress from contact where idcontact = '" + fromId.ToString() + "'", _dbConn);
					try
					{
						reader = comm.ExecuteReader();
						while (reader.Read())
						{
							if (!reader.IsDBNull(0))
								mailingAddressFrom = reader.GetGuid(0);

							if (!reader.IsDBNull(1))
								secondAddessFrom = reader.GetGuid(1);

							if (!reader.IsDBNull(2))
								thirdAddressFrom = reader.GetGuid(2);

							if (!reader.IsDBNull(3))
								siteAddressFrom = reader.GetGuid(3);
						}
					}
					catch {	}	//SILENT SO FAR
					finally 
					{ reader.Close(); }

					comm = new SqlCommand("select mailaddress, secondaddress, thirdaddress, siteaddress from contact where idcontact = '" + toId.ToString() + "'", _dbConn);
					try
					{
						reader = comm.ExecuteReader();
						while (reader.Read())
						{
							if (!reader.IsDBNull(0))
								mailingAddressTo = reader.GetGuid(0);

							if (!reader.IsDBNull(1))
								secondAddessTo = reader.GetGuid(1);

							if (!reader.IsDBNull(2))
								thirdAddressTo = reader.GetGuid(2);

							if (!reader.IsDBNull(3))
								siteAddressTo = reader.GetGuid(3);
						}
					}
					catch {}	//SILENT SO FAR
					finally 
					{ reader.Close(); }

					MergeAddresses(siteAddressFrom, siteAddressTo, toId, eAddressType.eSiteAddress);
					MergeAddresses(mailingAddressFrom, mailingAddressTo, toId, eAddressType.eMailAddress);
					MergeAddresses(secondAddessFrom, secondAddessTo, toId, eAddressType.eSecondAddress);
					MergeAddresses(thirdAddressFrom, thirdAddressTo, toId, eAddressType.eThirdAddress);
					
                    ////////////////////////////////////////////////////////////////////////
                    //BEGIN
                    Guid dealPropertyFrom = Guid.Empty;
                    Guid dealPropertyTo = Guid.Empty;

                    comm = new SqlCommand("select dealProperty from propertyItem where owner = '" + fromId.ToString() + "'", _dbConn);
                    try
                    {
                        reader = comm.ExecuteReader();
                        while (reader.Read())
                        {
                            if (!reader.IsDBNull(0))
                                dealPropertyFrom = reader.GetGuid(0);                            
                        }
                    }
                    catch { }	//SILENT SO FAR
                    finally
                    { reader.Close(); }

                    comm = new SqlCommand("select dealProperty from propertyItem where owner = '" + toId.ToString() + "'", _dbConn);
                    try
                    {
                        reader = comm.ExecuteReader();
                        while (reader.Read())
                        {
                            if (!reader.IsDBNull(0))
                                dealPropertyTo = reader.GetGuid(0);                            
                        }
                    }
                    catch { }	//SILENT SO FAR
                    finally
                    { reader.Close(); }

                    if (Guid.Empty == dealPropertyFrom)
                    {
                        // do nothing
                    }
                    else if (Guid.Empty != dealPropertyFrom && Guid.Empty == dealPropertyTo)
                    {
                        // INSERT NEW RECORD AND UPDATE CONTACT TABLE
                        Guid newId = Guid.NewGuid();

                        string sqlCommand = "select Type, Bedrooms, Baths, Sqft, YearBuilt, Garage, Pool, OccupiedBy, ";
                        sqlCommand += "Apn, FeatureCondition, WhySelling, WorthBySeller, Source, FirstBal, Ir1, Type1, MoPay1, ";
                        sqlCommand += "TaxIns, TaxAmnt, SecondBal, Ir2, Type2, MoPay2, Hoa, ThirdBal, Ir3, Type3, MoPay3, OtherLiens, ";
                        sqlCommand += "PmntsCurrent, MoBehind, ReInsAmnt, InForecl, ForSaleDate, Listed, ListExpDate, CanCancelList, ";
                        sqlCommand += "LowestPriceAcceptable, Sell4WhatOwes, MinCashNeededWhy, DesiredClosingDate, Motivation, ";
                        sqlCommand += "LeadSource, Arv, Repairs, NextStep ";
                        sqlCommand += "from DealProperty ";
                        sqlCommand += "where IdDealProperty = '" + dealPropertyFrom.ToString() + "'";
                        
	                    string Type = string.Empty;
                        string Bedrooms = string.Empty;
                        string Baths = string.Empty;
                        string Sqft = string.Empty;
                        string YearBuilt = string.Empty;
                        string Garage = string.Empty;
                        string Pool = string.Empty;
                        string OccupiedBy = string.Empty;
                        string Apn = string.Empty; 
                        string FeatureCondition = string.Empty;
                        string WhySelling = string.Empty;
                        string WorthBySeller = string.Empty;
                        string Source = string.Empty;
                        string FirstBal = string.Empty;
                        string Ir1 = string.Empty;
                        string Type1 = string.Empty;
                        string MoPay1 = string.Empty;
                        string TaxIns = string.Empty;
                        string TaxAmnt = string.Empty;
                        string SecondBal = string.Empty;
                        string Ir2 = string.Empty;
                        string Type2 = string.Empty;
                        string MoPay2 = string.Empty;
                        string Hoa = string.Empty;
                        string ThirdBal = string.Empty;                         
                        string Ir3 = string.Empty;
                        string Type3 = string.Empty;
                        string MoPay3 = string.Empty;
                        string OtherLiens = string.Empty;
                        string PmntsCurrent = string.Empty;
                        string MoBehind = string.Empty;
                        string ReInsAmnt = string.Empty;
                        string InForecl = string.Empty;
                        string ForSaleDate = string.Empty;
                        string Listed = string.Empty;
                        string ListExpDate = string.Empty;
                        string CanCancelList = string.Empty;
                        string LowestPriceAcceptable = string.Empty;
                        string Sell4WhatOwes = string.Empty;
                        string MinCashNeededWhy = string.Empty;
                        string DesiredClosingDate = string.Empty;
                        string Motivation = string.Empty;
                        string LeadSource = string.Empty;
                        string Arv = string.Empty;
                        string Repairs = string.Empty;
                        string NextStep = string.Empty;


                        comm = new SqlCommand(sqlCommand, _dbConn);
                        try
                        {
                            reader = comm.ExecuteReader();
                            while (reader.Read())
                            {
                                if (!reader.IsDBNull(0))
                                    Type = reader.GetString(0);

                                if (!reader.IsDBNull(1))
                                    Bedrooms = reader.GetString(1);

                                if (!reader.IsDBNull(2))
                                    Baths = reader.GetString(2);

                                if (!reader.IsDBNull(3))
                                    Sqft = reader.GetString(3);

                                if (!reader.IsDBNull(4))
                                    YearBuilt = reader.GetString(4);

                                if (!reader.IsDBNull(5))
                                    Garage = reader.GetString(5);

                                if (!reader.IsDBNull(6))
                                    Pool = reader.GetString(6);

                                if (!reader.IsDBNull(7))
                                    OccupiedBy = reader.GetString(7);

                                if (!reader.IsDBNull(8))
                                    Apn = reader.GetString(8);

                                if (!reader.IsDBNull(9))
                                    FeatureCondition = reader.GetString(9);

                                if (!reader.IsDBNull(10))
                                    WhySelling = reader.GetString(10);

                                if (!reader.IsDBNull(11))
                                    WorthBySeller = reader.GetString(11);

                                if (!reader.IsDBNull(12))
                                    Source = reader.GetString(12);

                                if (!reader.IsDBNull(13))
                                    FirstBal = reader.GetString(13);

                                if (!reader.IsDBNull(14))
                                    Ir1 = reader.GetString(14);

                                if (!reader.IsDBNull(15))
                                    Type1 = reader.GetString(15);

                                if (!reader.IsDBNull(16))
                                    MoPay1 = reader.GetString(16);

                                if (!reader.IsDBNull(17))
                                    TaxIns = reader.GetString(17);

                                if (!reader.IsDBNull(18))
                                    TaxAmnt = reader.GetString(18);

                                if (!reader.IsDBNull(19))
                                    SecondBal = reader.GetString(19);

                                if (!reader.IsDBNull(20))
                                    Ir2 = reader.GetString(20);

                                if (!reader.IsDBNull(21))
                                    Type2 = reader.GetString(21);

                                if (!reader.IsDBNull(22))
                                    MoPay2 = reader.GetString(22);

                                if (!reader.IsDBNull(23))
                                    Hoa = reader.GetString(23);

                                if (!reader.IsDBNull(24))
                                    ThirdBal = reader.GetString(24);

                                if (!reader.IsDBNull(25))
                                    Ir3 = reader.GetString(25);

                                if (!reader.IsDBNull(26))
                                    Type3 = reader.GetString(26);

                                if (!reader.IsDBNull(27))
                                    MoPay3 = reader.GetString(27);

                                if (!reader.IsDBNull(28))
                                    OtherLiens = reader.GetString(28);

                                if (!reader.IsDBNull(29))
                                    PmntsCurrent = reader.GetString(29);

                                if (!reader.IsDBNull(30))
                                    MoBehind = reader.GetString(30);

                                if (!reader.IsDBNull(31))
                                    ReInsAmnt = reader.GetString(31);

                                if (!reader.IsDBNull(32))
                                    InForecl = reader.GetString(32);

                                if (!reader.IsDBNull(33))
                                    ForSaleDate = reader.GetString(33);

                                if (!reader.IsDBNull(34))
                                    Listed = reader.GetString(34);

                                if (!reader.IsDBNull(35))
                                    ListExpDate = reader.GetString(35);

                                if (!reader.IsDBNull(36))
                                    CanCancelList = reader.GetString(36);

                                if (!reader.IsDBNull(37))
                                    LowestPriceAcceptable = reader.GetString(37);

                                if (!reader.IsDBNull(38))
                                    Sell4WhatOwes = reader.GetString(38);

                                if (!reader.IsDBNull(39))
                                    MinCashNeededWhy = reader.GetString(39);

                                if (!reader.IsDBNull(40))
                                    DesiredClosingDate = reader.GetString(40);

                                if (!reader.IsDBNull(41))
                                    Motivation = reader.GetString(41);

                                if (!reader.IsDBNull(42))
                                    LeadSource = reader.GetString(42);

                                if (!reader.IsDBNull(43))
                                    Arv = reader.GetString(43);

                                if (!reader.IsDBNull(44))
                                    Repairs = reader.GetString(44);

                                if (!reader.IsDBNull(45))
                                    NextStep = reader.GetString(45);                                

                            }
                        }
                        catch { }
                        finally
                        {
                            reader.Close();
                        }

                        //INSERT
                        string sqlComm = "insert into DealProperty values ('" + newId.ToString() + "'" + ", ";
                        sqlComm += "'" + Type + "', ";
                        sqlComm += "'" + Bedrooms + "', ";
                        sqlComm += "'" + Baths + "', ";
                        sqlComm += "'" + Sqft + "', ";
                        sqlComm += "'" + YearBuilt + "', ";
                        sqlComm += "'" + Garage + "', ";
                        sqlComm += "'" + Pool + "', ";
                        sqlComm += "'" + OccupiedBy + "', ";
                        sqlComm += "'" + Apn + "', ";
                        sqlComm += "'" + FeatureCondition + "', ";
                        sqlComm += "'" + WhySelling + "', ";
                        sqlComm += "'" + WorthBySeller + "', ";
                        sqlComm += "'" + Source + "', ";
                        sqlComm += "'" + FirstBal + "', ";
                        sqlComm += "'" + Ir1 + "', ";
                        sqlComm += "'" + Type1 + "', ";
                        sqlComm += "'" + MoPay1 + "', ";
                        sqlComm += "'" + TaxIns + "', ";
                        sqlComm += "'" + TaxAmnt + "', ";
                        sqlComm += "'" + SecondBal + "', ";
                        sqlComm += "'" + Ir2 + "', ";
                        sqlComm += "'" + Type2 + "', ";
                        sqlComm += "'" + MoPay2 + "', ";
                        sqlComm += "'" + Hoa + "', ";
                        sqlComm += "'" + ThirdBal + "', ";
                        sqlComm += "'" + Ir3 + "', ";
                        sqlComm += "'" + Type3 + "', ";
                        sqlComm += "'" + MoPay3 + "', ";
                        sqlComm += "'" + OtherLiens + "', ";
                        sqlComm += "'" + PmntsCurrent + "', ";
                        sqlComm += "'" + MoBehind + "', ";
                        sqlComm += "'" + ReInsAmnt + "', ";
                        sqlComm += "'" + InForecl + "', ";
                        sqlComm += "'" + ForSaleDate + "', ";
                        sqlComm += "'" + Listed + "', ";
                        sqlComm += "'" + ListExpDate + "', ";
                        sqlComm += "'" + CanCancelList + "', ";
                        sqlComm += "'" + LowestPriceAcceptable + "', ";
                        sqlComm += "'" + Sell4WhatOwes + "', ";
                        sqlComm += "'" + MinCashNeededWhy + "', ";
                        sqlComm += "'" + DesiredClosingDate + "', ";
                        sqlComm += "'" + Motivation + "', ";
                        sqlComm += "'" + LeadSource + "', ";
                        sqlComm += "'" + Arv + "', ";
                        sqlComm += "'" + Repairs + "', ";                        
                        sqlComm += "'" + NextStep + "')";

                        comm = new SqlCommand(sqlComm, _dbConn);
                        try
                        {
                            comm.ExecuteNonQuery();
                        }
                        catch { }

                        //UPDATE DEST CONTACT
                        sqlComm = "UPDATE PropertyItem SET DealProperty = '" + newId.ToString() + "' WHERE owner = '" + toId.ToString() + "'";
                        comm = new SqlCommand(sqlComm, _dbConn);
                        try
                        {
                            comm.ExecuteNonQuery();
                        }
                        catch { }
                    }
                    else
                    {
                        // UPDATE DEST
                        string sqlCommand = GetMergeDealPropertySqlCommand(dealPropertyFrom, dealPropertyTo);
                        comm = new SqlCommand(sqlCommand, _dbConn);
                        try
                        {
                            comm.ExecuteNonQuery();
                        }
                        catch { }	//SILENT SO FAR			
                    }
                    //END
                    ////////////////////////////////////////////////////////////////////////

					if (null != OnPropertyItemMerged)
						OnPropertyItemMerged(i, _propertyItemsToMerge.Count, false);

					i++;
				
				}
				else
				{
					if (null != OnPropertyItemMerged)
						OnPropertyItemMerged(i, _propertyItemsToMerge.Count, true);
				}
			}
			

			if (_dbConn.State != ConnectionState.Closed)
				try { _dbConn.Close(); } 
				catch {}	
		}

		private void MergeAddresses(Guid addressFrom, Guid addressTo, Guid toId, eAddressType addressType)
        {
			if (Guid.Empty == addressFrom)
			{
				// do nothing
			}
			else if (Guid.Empty != addressFrom && Guid.Empty == addressTo)
			{
				// INSERT NEW RECORD AND UPDATE CONTACT TABLE
				Guid newId = Guid.NewGuid();

const string insertSql = @"
insert into [Address] 
	(
		[IdAddress], [SiteStreetNumber], [SiteStreetPreDirectional], [SiteStreetName], [SiteStreetPostDirectional], [siteStreetSuffix], [SiteStreetUnitNumber],
		[FullSiteStreetAddress], [SiteCity], [SiteState], [SiteCitySiteState], [SiteCitySiteStateSiteZIP], [SiteZIP], [SiteCountry]
	) 

select 
		@newId [IdAddress], [SiteStreetNumber], [SiteStreetPreDirectional], [SiteStreetName], [SiteStreetPostDirectional], [siteStreetSuffix], [SiteStreetUnitNumber],
		[FullSiteStreetAddress], [SiteCity], [SiteState], [SiteCitySiteState], [SiteCitySiteStateSiteZIP], [SiteZIP], [SiteCountry]

FROM 
	Address 
where
	[IdAddress] = @sourceAddressId";

				using (var insertSqlComm = new SqlCommand(insertSql, _dbConn))
					try
					{
						insertSqlComm.Parameters.AddWithValue("@newId", newId);
						insertSqlComm.Parameters.AddWithValue("@sourceAddressId", addressFrom);
						insertSqlComm.ExecuteNonQuery();
					}
					catch { }

				//UPDATE DEST CONTACT
				var destinationField = string.Empty;

				switch (addressType)
                {
					case eAddressType.eSiteAddress: 
						destinationField = "siteaddress"; 
						break;

					case eAddressType.eMailAddress:
						destinationField = "mailaddress";
						break;

					case eAddressType.eSecondAddress:
						destinationField = "secondaddress";
						break;

					case eAddressType.eThirdAddress:
						destinationField = "thirdaddress";
						break;

				}

				var updateSql = $"UPDATE CONTACT SET {destinationField} = @newId WHERE idContact = @sourceAddressId ";
				using (var updateSqlComm = new SqlCommand(updateSql, _dbConn))
				{
					try
					{
						updateSqlComm.Parameters.AddWithValue("@newId", newId);
						updateSqlComm.Parameters.AddWithValue("@sourceAddressId", toId);
						updateSqlComm.ExecuteNonQuery();
					}
					catch { }
				}
			}
			else
			{
				// UPDATE DEST

const string updateSql = "update dest "
				+ "set sitestreetnumber = src.sitestreetnumber, "
				+ "sitestreetpredirectional = src.sitestreetpredirectional, "
				+ "sitestreetname = src.sitestreetname, "
				+ "sitestreetpostdirectional = src.sitestreetpostdirectional, "
				+ "sitestreetsuffix = src.sitestreetsuffix, "
				+ "sitestreetunitnumber = src.sitestreetunitnumber, "
				+ "fullsitestreetaddress = src.fullsitestreetaddress, "
				+ "sitecity = src.sitecity, "
				+ "sitestate = src.sitestate, "
				+ "sitezip = src.sitezip, "
				+ "sitecitysitestate = src.sitecitysitestate, "
				+ "sitecitysitestatesitezip = src.sitecitysitestatesitezip, "
				+ "sitecountry = src.sitecountry "
				+ "from address src, address dest "
				+ "where dest.idaddress = @idTo and "
				+ "src.idaddress = @idFrom ";

				using (var comm = new SqlCommand(updateSql, _dbConn))
				{
					comm.Parameters.AddWithValue("@idFrom", addressFrom);
					comm.Parameters.AddWithValue("@idTo", addressTo);
					try
					{
						comm.ExecuteNonQuery();
					}
					catch { }   //SILENT SO FAR			
				}
			}
		}

		public void MergeContacts(ArrayList contactLists, bool mergeNames)
		{			
			
			MergePropertyItems(contactLists, mergeNames);
		}

		public void DeleteContacts(ContactEntry[] contacts)
		{			
			ArrayList results = GetPropertyItemIdsFromDB(contacts);
			DeletePropertyItems(results);
		}

		ArrayList GetPropertyItemIdsFromDB(ContactEntry[] contacts)
		{
			ArrayList results = new ArrayList();

			string crit = "SELECT IdPropertyItem FROM PropertyItem WHERE ";

			for(int i = 0; i < contacts.Length; i++)
			{
				ContactEntry ce = contacts[i];					
				crit += "Owner = '" + ce.ContactId.ToString() + "'";
			
				if (i < contacts.Length - 1)
					crit += " or ";
			}
			
			SqlDataReader reader = null;
			SqlCommand comm = new SqlCommand(crit, _dbConn);

			if (ConnectionState.Open != _dbConn.State)
				_dbConn.Open();

			try
			{
				reader = comm.ExecuteReader();
				while (reader.Read())
				{					
					if (!reader.IsDBNull(0))
						results.Add(reader.GetGuid(0));					
				}
			}
			catch { }	//EMPTY SO FAR
			finally
			{
				if (null != reader && !reader.IsClosed)
					reader.Close();

				_dbConn.Close();
			}

			return results;
		}						
	}
}
