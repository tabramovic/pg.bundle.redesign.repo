﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace DealMaker.PlainClasses.Utils
{
    public class EmailBlastSupport
    {
        public static KeyValuePair<Guid, Tuple<string, string>> GetAssignedEmails(Guid propertyItemId, out KeyValuePair<Guid, string> address)
        {
            var record = new KeyValuePair<Guid, Tuple<string, string>>();
            address = new KeyValuePair<Guid, string>();

            SqlCommand sp_GetMFWPropertyItemDetails = new SqlCommand("GetMFWPropertyItemDetails", Connection.getInstance().HatchConn);
            sp_GetMFWPropertyItemDetails.CommandType = CommandType.StoredProcedure;

            sp_GetMFWPropertyItemDetails.Parameters.Clear();
            sp_GetMFWPropertyItemDetails.Parameters.AddWithValue("@propertyItemId", propertyItemId);

            if (ConnectionState.Open != Connection.getInstance().HatchConn.State)
                Connection.getInstance().HatchConn.Open();

            using (var reader = sp_GetMFWPropertyItemDetails.ExecuteReader())
            {
                while (reader.Read())
                {
                    string fssa = string.Empty;
                    string contactEmail = string.Empty;
                    string listingAgentEmail = string.Empty;
                    string skipTracedContactEmail = string.Empty;

                    //SiteAddress.FullSiteStreetAddress
                    if (DBNull.Value != reader[1])
                    {
                        fssa = reader.GetString(1);
                        address = new KeyValuePair<Guid, string>(propertyItemId, fssa);                        
                    }

                    //Contact.eMail
                    if (DBNull.Value != reader[15])
                    {
                        contactEmail = reader.GetString(15);
                    }

                    //ListingAgent eMail
                    if (DBNull.Value != reader[16])
                    {
                        listingAgentEmail = reader.GetString(16);
                    }

                    //Skiptraced eMail
                    if (DBNull.Value != reader[17])
                    {
                        skipTracedContactEmail = reader.GetString(17);
                    }

                    //use first choice email email. if empty then use skip trace email address.
                    if (string.IsNullOrEmpty(contactEmail))
                        contactEmail = skipTracedContactEmail;

                    Tuple<string, string> emailRecords = new Tuple<string, string>(contactEmail, listingAgentEmail);
                    record = new KeyValuePair<Guid, Tuple<string, string>>(propertyItemId, emailRecords);
                }
            }

            return record;
        }

        public static List<KeyValuePair<Guid, Tuple<string, string>>> GetAssignedEmails(List<Guid> propertyItemIds, out Dictionary<Guid, string> addresses)
        {
            addresses = new Dictionary<Guid, string>();
            var list = new List<KeyValuePair<Guid, Tuple<string, string>>>();

            foreach (Guid propertyItemId in propertyItemIds)
            {
                KeyValuePair<Guid, string> address;
                var emails = GetAssignedEmails(propertyItemId, out address);

                if (!string.IsNullOrEmpty(emails.Value.Item1) || !string.IsNullOrEmpty(emails.Value.Item2))
                    list.Add(emails);

                if (!string.IsNullOrEmpty(address.Value) && !addresses.Keys.Contains(address.Key))                    
                    addresses.Add(address.Key, address.Value);
            }

            return list;
        }
    }
}
