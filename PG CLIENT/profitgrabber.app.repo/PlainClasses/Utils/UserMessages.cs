using System;

namespace DealMaker
{
	/// <summary>
	/// Summary description for UserMessages.
	/// </summary>
	public class UserMessages
	{
		public const string CanNotRemoveContactFromGroupHeader = "Can not remove Contact from its group";
		public const string CanNotRemoveContactFromGroup = "You can remove Contact from its group only when it belongs to multiple groups!";
		public const string GroupNotSelected			 = "Group not selected!";
		public const string GroupNotSelectedHeader		 = "Please select group on listview!";
		
		public const string PermanentRecordDelete = "Permanent Record Delete";
		public const string PermanentRecordDeleteDesc = "Are you sure that you want to permeanently delete tagged records from the program?";
		public const string AlreadyAssigned		  = "Multiple assign";
		public const string AlreadyAssignedDesc	  = "This column has already been assigned!\nIf you want to re-assign the same column, first un-assign it!";		
		public const string	BaseGroupChange		  = "This property item will be automatically removed from current group and won�t be displayed in it!";

		public const string	NoStatusGroupAssigned = "No Status group assigned!";
		public const string Homeowner			  = "Dear Homeowner";	
		public const string CurrentResident		  = "Current Resident";

		
		public const string EmptyFile			= "File is empty!";
		public const string SelectSheet			= "Before you proceed with Excel import file, you must specify which sheet contains data!"   ;
		public const string IncorrectFileName	= "Please enter correct file name !";
			
		public const string	UnAssigned			= "Unassigned";

		public const string	UnKnown				= " 'Unknown'";

		public const string Warning				= "Make sure you mapped all the fields that you want to\nimport into your ProfitGrabber!\n\nAny and all unmapped fields will not be merged into your mailings,\nneither will they be imported into your database!";

		public const string OverwriteExistingFile = "Overvrite existing file?";

		public const string CouldNotCastToInteger = "Could not cast columnIndex to integer! This field will not be mapped!";

		public const string WrongBinding		= "ERROR!\n\n\n The selected Mapping Profile does NOT match your Import File!\n\n You either selected a wrong mapping profile, or the Import File\n has changed from the time you created the profile. Go back\n and select the correct mapping profile,or create a new one!";
		public const string WrongBindingHeader  = "Wrong binding?"	;


		public const string CancelMailFaxWizard = "Exit Mail / Fax Wizard?";
		public const string CancelMailFaxWizardText = "Are you sure that you want to exit Mail / Fax Wizard?";

		public const string CancelImport		= "Cancel Import?";
		public const string	CancelImportText	= "Are you sure that you want to cancel import?";
		public const string EmptyProfileName	= "Please specify profile name!";

		public const string ProblemsWithImportFile = "Problems with import file!";
		public const string ProblemsWithImportFileDesc = "Some import fields are not mapped properly!\n\nThe reason may be that your first raw contains field names,\n     and you did not specify that at step # 1.\nOr the fields are not mapped properly � default value will be used instead!\nEither way, the data will still be imported!\n\nWould you like to see the log?" ;

		public const string SearchInAllGroupsHeader = "Search In All Groups";
		public const string SearchInAllGroupsDesc = "To enable search in all groups, check the Search All Groups checkbox!";
	}
}
