﻿using System;
using System.Data.SqlClient;
using System.Text;

namespace DealMaker.PlainClasses.Utils
{
    public class ContactViewDataLayer
    {
        public static void ExpandDataLayer(SqlConnection conn)
        {
            ExpandDataTable(conn);
            AlterSPL(conn);            
        }

        static void ExpandDataTable(SqlConnection conn)
        {            
            try
            {
                string cmdText = "IF NOT EXISTS ( SELECT  * FROM    syscolumns WHERE id = OBJECT_ID('Contact') AND name = 'STEmail' )  ALTER TABLE Contact ADD STEmail nvarchar(1024) NULL ";
                SqlCommand comm = new SqlCommand(cmdText, conn);
                comm.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                //add log
            }

            try
            {
                string cmdText = "IF NOT EXISTS ( SELECT  * FROM    syscolumns WHERE id = OBJECT_ID('Contact') AND name = 'STCellphone' )  ALTER TABLE Contact ADD STCellphone nvarchar(1024) NULL ";
                SqlCommand comm = new SqlCommand(cmdText, conn);
                comm.ExecuteNonQuery();
            }
            catch (Exception)
            {
                //add log
            }

            try
            {
                string cmdText = "IF NOT EXISTS ( SELECT  * FROM    syscolumns WHERE id = OBJECT_ID('Contact') AND name = 'STLandline' )  ALTER TABLE Contact ADD STLandline nvarchar(1024) NULL ";
                SqlCommand comm = new SqlCommand(cmdText, conn);
                comm.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                //add log                
            }

            try
            {
                string cmdText = "IF NOT EXISTS ( SELECT  * FROM    syscolumns WHERE id = OBJECT_ID('Contact') AND name = 'STLastModified' )  ALTER TABLE Contact ADD STLastModified datetime NULL ";
                SqlCommand comm = new SqlCommand(cmdText, conn);
                comm.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                //add log                
            }


            try
            {
                string cmdText = "IF NOT EXISTS ( SELECT  * FROM    syscolumns WHERE id = OBJECT_ID('Contact') AND name = 'ST_P1_RESPONSE' )  ALTER TABLE Contact ADD ST_P1_RESPONSE NVARCHAR(MAX) NULL ";
                SqlCommand comm = new SqlCommand(cmdText, conn);
                comm.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                //add log                
            }
        }

        static void AlterSPL(SqlConnection conn)
        {
            /*            
                ALTER PROCEDURE [dbo].[insert_Contact]
                (
                @IdContact uniqueidentifier,
                @FirstName nvarchar(1024),
                @LastName nvarchar(1024),
                @MiddleName nvarchar(1024),
                @FullName nvarchar(1024),
                @GoBy nvarchar(1024),
                @SalutationName nvarchar(1024),
                @ImportedAddress uniqueidentifier,
                @FirstAddress uniqueidentifier,
                @SecondAddress uniqueidentifier,
                @ThirdAddress uniqueidentifier,
                @SiteAddress uniqueidentifier,
                @MailAddress uniqueidentifier,
                @HomePhone uniqueidentifier,
                @WorkPhone uniqueidentifier,
                @CellPhone uniqueidentifier,
                @faxNumber nvarchar(1024),
                @eMail nvarchar(1024),
                @stEmail nvarchar(1024),
                @stCellphone nvarchar(1024),
                @stLandline nvarchar(1024)
                )
                AS
                INSERT INTO Contact( IdContact , FirstName , LastName , MiddleName , FullName , GoBy , SalutationName , ImportedAddress , FirstAddress , SecondAddress , ThirdAddress , SiteAddress , MailAddress , HomePhone , WorkPhone , CellPhone , faxNumber , eMail ) VALUES ( @IdContact , @FirstName , @LastName , @MiddleName , @FullName , @GoBy , @SalutationName , @ImportedAddress , @FirstAddress , @SecondAddress , @ThirdAddress , @SiteAddress , @MailAddress , @HomePhone , @WorkPhone , @CellPhone , @faxNumber , @eMail, @stEmail, @stCellphone, @stLandline)
            */

            try
            {
                var sb = new StringBuilder();

                sb.Append("ALTER PROCEDURE[dbo].[insert_Contact] ");
                sb.Append("( ");
                sb.Append("@IdContact uniqueidentifier, ");
                sb.Append("@FirstName nvarchar(1024), ");
                sb.Append("@LastName nvarchar(1024), ");
                sb.Append("@MiddleName nvarchar(1024), ");
                sb.Append("@FullName nvarchar(1024), ");
                sb.Append("@GoBy nvarchar(1024), ");
                sb.Append("@SalutationName nvarchar(1024), ");
                sb.Append("@ImportedAddress uniqueidentifier, ");
                sb.Append("@FirstAddress uniqueidentifier, ");
                sb.Append("@SecondAddress uniqueidentifier, ");
                sb.Append("@ThirdAddress uniqueidentifier, ");
                sb.Append("@SiteAddress uniqueidentifier, ");
                sb.Append("@MailAddress uniqueidentifier, ");
                sb.Append("@HomePhone uniqueidentifier, ");
                sb.Append("@WorkPhone uniqueidentifier, ");
                sb.Append("@CellPhone uniqueidentifier, ");
                sb.Append("@faxNumber nvarchar(1024), ");
                sb.Append("@eMail nvarchar(1024), ");
                sb.Append("@stEmail nvarchar(1024), ");
                sb.Append("@stCellphone nvarchar(1024), ");
                sb.Append("@stLandline nvarchar(1024), ");
                sb.Append("@ST_P1_RESPONSE nvarchar(max) ");
                sb.Append(") ");
                sb.Append("AS ");
                sb.Append("INSERT INTO Contact(IdContact , FirstName , LastName , MiddleName , FullName , GoBy , SalutationName , ImportedAddress , FirstAddress , SecondAddress , ThirdAddress , SiteAddress , MailAddress , HomePhone , WorkPhone , CellPhone , faxNumber , eMail, STEmail, STCellphone, STLandline, ST_P1_RESPONSE ) VALUES(@IdContact , @FirstName , @LastName , @MiddleName , @FullName , @GoBy , @SalutationName , @ImportedAddress , @FirstAddress , @SecondAddress , @ThirdAddress , @SiteAddress , @MailAddress , @HomePhone , @WorkPhone , @CellPhone , @faxNumber , @eMail, @stEmail, @stCellphone, @stLandline, @ST_P1_RESPONSE) ");

                string comm = sb.ToString();
                SqlCommand createSPL = new SqlCommand(comm, conn);
                createSPL.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                //add log
            }
        }        
    }
}
