﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Threading;

namespace DealMaker.PlainClasses.Utils
{
    public class ScriptedFixes
    {
        public delegate void ScriptedFixesStatusEventHandler(string msg, bool valid);
        public event ScriptedFixesStatusEventHandler ScfStatus;
        int nrOfFixes = 27;
        int currentFix = 1;

        public ScriptedFixes()
        { }

        public void ApplyFixes()
        {
            StringBuilder sb = new StringBuilder();
            int retCode = 0;
            bool retVal = true;

            string txt = string.Empty;

            //--
            try
            {
                sb.Append("DELETE FROM NodePropertyItems WHERE (IdPropertyItem NOT IN (SELECT IdPropertyItem FROM PropertyItem))");

                retCode = new SqlCommand(sb.ToString(), Connection.getInstance().HatchConn).ExecuteNonQuery();

                if (null != ScfStatus)
                    ScfStatus($"{currentFix++}/{nrOfFixes} - Applying Group Fix", true);
            }
            catch (Exception exc)
            {
                if (null != ScfStatus)
                    ScfStatus($"{currentFix++}/{nrOfFixes}- Applying Group Fix", false);

                txt += exc.Message + Environment.NewLine;
                retVal = false;
            }
            Thread.Sleep(500);
            //--

            //--
            try
            {
                sb = new StringBuilder();
                sb.Append("delete FROM Task WHERE     (MailFaxWizard NOT IN (SELECT IdMailFaxWizard FROM MailFaxWizard)) AND MailFaxWizard is not NULL");

                retCode = new SqlCommand(sb.ToString(), Connection.getInstance().HatchConn).ExecuteNonQuery();

                if (null != ScfStatus)
                    ScfStatus($"{currentFix++}/{nrOfFixes} - Applying Task Fix", true);
            }
            catch (Exception exc)
            {
                if (null != ScfStatus)
                    ScfStatus($"{currentFix++}/{nrOfFixes} - Applying Task Fix", false);

                txt += exc.Message + Environment.NewLine;
                retVal = false;
            }
            Thread.Sleep(500);
            //--


            //--
            try
            {
                sb = new StringBuilder();
                sb.Append("ALTER PROCEDURE delete_Node ");
                sb.Append("( ");
                sb.Append("@nodeId int, ");
                sb.Append("@retVal int OUTPUT ");
                sb.Append(") ");
                sb.Append("AS ");
                sb.Append("if not exists (SELECT * from Node where nodeId = @nodeId) ");
                sb.Append("begin ");
                sb.Append("set @retVal = -1 ");
                sb.Append("return ");
                sb.Append("end	 ");
                sb.Append("else ");
                sb.Append("begin ");
                sb.Append("delete from Node where nodeId = @nodeId ");
                sb.Append("delete from Task where IdNode = @nodeId  and IdTask not in (select elt from Activity2Tasks) ");  //fix for Unable to load objects (as this delete would delete also tasks which are part of the activities that are or can get assigned to other groups)
                sb.Append("set @retVal = 1 ");
                sb.Append("end ");
                sb.Append("RETURN ");

                retCode = new SqlCommand(sb.ToString(), Connection.getInstance().HatchConn).ExecuteNonQuery();

                if (null != ScfStatus)
                    ScfStatus($"{currentFix++}/{nrOfFixes} - Applying Group belonging Fix", true);
            }
            catch (Exception exc)
            {
                if (null != ScfStatus)
                    ScfStatus($"{currentFix++}/{nrOfFixes} - Applying Group belonging Fix", false);

                txt += exc.Message + Environment.NewLine;
                retVal = false;
            }
            Thread.Sleep(500);
            //--

            //--
            try
            {
                sb = new StringBuilder();
                sb.Append("ALTER PROCEDURE DeletePropertyItemTasks ");
                sb.Append("( ");
                sb.Append("@propertyItemId uniqueidentifier ");
                sb.Append(") ");
                sb.Append("AS ");
                sb.Append("delete from Task where IdPropertyItem = @propertyItemId ");
                sb.Append("RETURN ");

                retCode = new SqlCommand(sb.ToString(), Connection.getInstance().HatchConn).ExecuteNonQuery();

                if (null != ScfStatus)
                    ScfStatus($"{currentFix++}/{nrOfFixes} - Applying Task Fix Part 1", true);
            }
            catch (Exception exc)
            {
                if (null != ScfStatus)
                    ScfStatus($"{currentFix++}/{nrOfFixes} - Applying Task Fix Part 1", false);

                txt += exc.Message + Environment.NewLine;
                retVal = false;
            }
            Thread.Sleep(500);
            //--

            //--
            try
            {
                sb = new StringBuilder();
                sb.Append("ALTER TABLE [dbo].[Activity2Tasks] ALTER COLUMN [elt] uniqueidentifier NOT NULL");

                retCode = new SqlCommand(sb.ToString(), Connection.getInstance().HatchConn).ExecuteNonQuery();

                if (null != ScfStatus)
                    ScfStatus($"{currentFix++}/{nrOfFixes} - Applying Task Fix Part 2", true);
            }
            catch (Exception exc)
            {
                if (null != ScfStatus)
                    ScfStatus($"{currentFix++}/{nrOfFixes} - Applying Task Fix Part 2", false);

                txt += exc.Message + Environment.NewLine;
                retVal = false;
            }
            Thread.Sleep(500);
            //--

            //--
            try
            {
                sb = new StringBuilder();
                sb.Append("ALTER TABLE [dbo].[TaskExcludedPropertyItems] ALTER COLUMN [IdPropertyItem] uniqueidentifier NOT NULL");

                retCode = new SqlCommand(sb.ToString(), Connection.getInstance().HatchConn).ExecuteNonQuery();

                if (null != ScfStatus)
                    ScfStatus($"{currentFix++}/{nrOfFixes} - Applying Task Fix Part 3", true);
            }
            catch (Exception exc)
            {
                if (null != ScfStatus)
                    ScfStatus($"{currentFix++}/{nrOfFixes} - Applying Task Fix Part 3", false);

                txt += exc.Message + Environment.NewLine;
                retVal = false;
            }
            Thread.Sleep(500);
            //--

            //--
            try
            {
                sb = new StringBuilder();
                sb.Append("ALTER TABLE [dbo].[Activities] ADD ");
                sb.Append("CONSTRAINT [PK_Activities] PRIMARY KEY CLUSTERED ");
                sb.Append("( ");
                sb.Append("[IdActivity] ASC ");
                sb.Append(") ON [PRIMARY] ");

                retCode = new SqlCommand(sb.ToString(), Connection.getInstance().HatchConn).ExecuteNonQuery();

                if (null != ScfStatus)
                    ScfStatus($"{currentFix++}/{nrOfFixes} - Applying Activities Fix Part 1", true);
            }
            catch (Exception exc)
            {
                if (null != ScfStatus)
                    ScfStatus($"{currentFix++}/{nrOfFixes} - Applying Activities Fix Part 1", false);

                txt += exc.Message + Environment.NewLine;
                retVal = false;
            }
            Thread.Sleep(500);
            //--

            //--
            try
            {
                sb = new StringBuilder();
                sb.Append("ALTER TABLE [dbo].[MailFaxWizard] ADD ");
                sb.Append("CONSTRAINT [PK_MailFaxWizard] PRIMARY KEY CLUSTERED ");
                sb.Append("( ");
                sb.Append("[IdMailFaxWizard] ASC ");
                sb.Append(") ");

                retCode = new SqlCommand(sb.ToString(), Connection.getInstance().HatchConn).ExecuteNonQuery();

                if (null != ScfStatus)
                    ScfStatus($"{currentFix++}/{nrOfFixes} - Applying Mail Fax Wizard Fix", true);
            }
            catch (Exception exc)
            {
                if (null != ScfStatus)
                    ScfStatus($"{currentFix++}/{nrOfFixes} - Applying Mail Fax Wizard Fix", false);

                txt += exc.Message + Environment.NewLine;
                retVal = false;
            }
            Thread.Sleep(500);
            //--

            //--
            try
            {
                sb = new StringBuilder();
                sb.Append("ALTER TABLE [dbo].[Task] ADD ");
                sb.Append("CONSTRAINT [PK_Task] PRIMARY KEY CLUSTERED ");
                sb.Append("( ");
                sb.Append("[IdTask] ASC ");
                sb.Append(") ");

                retCode = new SqlCommand(sb.ToString(), Connection.getInstance().HatchConn).ExecuteNonQuery();

                if (null != ScfStatus)
                    ScfStatus($"{currentFix++}/{nrOfFixes} - Applying Task Fix Part 4", true);
            }
            catch (Exception exc)
            {
                if (null != ScfStatus)
                    ScfStatus($"{currentFix++}/{nrOfFixes} - Applying Task Fix Part 4", false);

                txt += exc.Message + Environment.NewLine;
                retVal = false;
            }
            Thread.Sleep(500);
            //--

            //--
            try
            {
                sb = new StringBuilder();
                sb.Append("ALTER TABLE [dbo].[Activity2Tasks] ADD ");
                sb.Append("CONSTRAINT [PK_Activity2Tasks] PRIMARY KEY CLUSTERED ");
                sb.Append("( ");
                sb.Append("[IdTask]  , ");
                sb.Append("[elt]  ");
                sb.Append(") ");

                retCode = new SqlCommand(sb.ToString(), Connection.getInstance().HatchConn).ExecuteNonQuery();

                if (null != ScfStatus)
                    ScfStatus($"{currentFix++}/{nrOfFixes} - Applying Activities Fix Part 2", true);
            }
            catch (Exception exc)
            {
                if (null != ScfStatus)
                    ScfStatus($"{currentFix++}/{nrOfFixes} - Applying Activities Fix Part 2", false);

                txt += exc.Message + Environment.NewLine;
                retVal = false;
            }
            Thread.Sleep(500);
            //--

            //--
            try
            {
                sb = new StringBuilder();
                sb.Append("ALTER TABLE [dbo].[TaskExcludedPropertyItems] ADD ");
                sb.Append("CONSTRAINT [PK_TaskExcludedPropertyItems] PRIMARY KEY CLUSTERED ");
                sb.Append("( ");
                sb.Append("[IdTask]  , ");
                sb.Append("[IdPropertyItem]  ");
                sb.Append(") ");

                retCode = new SqlCommand(sb.ToString(), Connection.getInstance().HatchConn).ExecuteNonQuery();

                if (null != ScfStatus)
                    ScfStatus($"{currentFix++}/{nrOfFixes} - Applying Activities Fix Part 3", true);
            }
            catch (Exception exc)
            {
                if (null != ScfStatus)
                    ScfStatus($"{currentFix++}/{nrOfFixes} - Applying Activities Fix Part 3", false);

                txt += exc.Message + Environment.NewLine;
                retVal = false;
            }
            Thread.Sleep(500);
            //--

            //--
            try
            {
                sb = new StringBuilder();
                sb.Append("ALTER TABLE [dbo].[Task] drop CONSTRAINT [FK_Task_MailFaxWizard] ");

                retCode = new SqlCommand(sb.ToString(), Connection.getInstance().HatchConn).ExecuteNonQuery();

                if (null != ScfStatus)
                    ScfStatus($"{currentFix++}/{nrOfFixes} - Applying Task Fix Part 5", true);
            }
            catch (Exception exc)
            {
                if (null != ScfStatus)
                    ScfStatus($"{currentFix++}/{nrOfFixes} - Applying Task Fix Part 5", false);

                txt += exc.Message + Environment.NewLine;
                retVal = false;
            }
            Thread.Sleep(500);
            //--

            //--
            try
            {
                sb = new StringBuilder();
                sb.Append("ALTER TABLE [dbo].[Task]  WITH NOCHECK ADD  CONSTRAINT [FK_Task_MailFaxWizard] FOREIGN KEY(	[MailFaxWizard]) ");
                sb.Append("REFERENCES [dbo].[MailFaxWizard] (	[IdMailFaxWizard]) ");
                sb.Append("ON UPDATE CASCADE ");
                sb.Append("ON DELETE CASCADE ");

                retCode = new SqlCommand(sb.ToString(), Connection.getInstance().HatchConn).ExecuteNonQuery();

                if (null != ScfStatus)
                    ScfStatus($"{currentFix++}/{nrOfFixes} - Applying Task Fix Part 6", true);
            }
            catch (Exception exc)
            {
                if (null != ScfStatus)
                    ScfStatus($"{currentFix++}/{nrOfFixes} - Applying Task Fix Part 6", false);

                txt += exc.Message + Environment.NewLine;
                retVal = false;
            }
            Thread.Sleep(500);
            //--

            //--
            try
            {
                sb = new StringBuilder();
                sb.Append("ALTER TABLE [dbo].[Task] CHECK CONSTRAINT [FK_Task_MailFaxWizard] ");

                retCode = new SqlCommand(sb.ToString(), Connection.getInstance().HatchConn).ExecuteNonQuery();

                if (null != ScfStatus)
                    ScfStatus($"{currentFix++}/{nrOfFixes} - Applying Task Fix Part 7", true);
            }
            catch (Exception exc)
            {
                if (null != ScfStatus)
                    ScfStatus($"{currentFix++}/{nrOfFixes} - Applying Task Fix Part 7", false);

                txt += exc.Message + Environment.NewLine;
                retVal = false;
            }
            Thread.Sleep(500);
            //--

            //--
            try
            {
                sb = new StringBuilder();
                sb.Append("ALTER TABLE [dbo].[TaskExcludedPropertyItems] drop CONSTRAINT [FK_TaskExcludedPropertyItems_Task] ");

                retCode = new SqlCommand(sb.ToString(), Connection.getInstance().HatchConn).ExecuteNonQuery();

                if (null != ScfStatus)
                    ScfStatus($"{currentFix++}/{nrOfFixes} - Applying Task Fix Part 8", true);
            }
            catch (Exception exc)
            {
                if (null != ScfStatus)
                    ScfStatus($"{currentFix++}/{nrOfFixes} - Applying Task Fix Part 8", false);

                txt += exc.Message + Environment.NewLine;
                retVal = false;
            }
            Thread.Sleep(500);
            //--

            //--
            try
            {
                sb = new StringBuilder();
                sb.Append("ALTER TABLE [dbo].[TaskExcludedPropertyItems]  WITH NOCHECK ADD  CONSTRAINT [FK_TaskExcludedPropertyItems_Task] FOREIGN KEY(	[IdTask]) ");
                sb.Append("REFERENCES [dbo].[Task] (	[IdTask]) ");
                sb.Append("ON UPDATE CASCADE ");
                sb.Append("ON DELETE CASCADE ");

                retCode = new SqlCommand(sb.ToString(), Connection.getInstance().HatchConn).ExecuteNonQuery();

                if (null != ScfStatus)
                    ScfStatus($"{currentFix++}/{nrOfFixes} - Applying Task Fix Part 9", true);
            }
            catch (Exception exc)
            {
                if (null != ScfStatus)
                    ScfStatus($"{currentFix++}/{nrOfFixes} - Applying Task Fix Part 9", false);

                txt += exc.Message + Environment.NewLine;
                retVal = false;
            }
            Thread.Sleep(500);
            //--

            //--
            try
            {
                sb = new StringBuilder();
                sb.Append("ALTER TABLE [dbo].[TaskExcludedPropertyItems] CHECK CONSTRAINT [FK_TaskExcludedPropertyItems_Task]");

                retCode = new SqlCommand(sb.ToString(), Connection.getInstance().HatchConn).ExecuteNonQuery();

                if (null != ScfStatus)
                    ScfStatus($"{currentFix++}/{nrOfFixes} - Applying Task Fix Part 10", true);
            }
            catch (Exception exc)
            {
                if (null != ScfStatus)
                    ScfStatus($"{currentFix++}/{nrOfFixes} - Applying Task Fix Part 10", true);

                txt += exc.Message + Environment.NewLine;
                retVal = false;
            }
            Thread.Sleep(500);
            //--

            //--fix activities
            //--
            try
            {                
                sb = new StringBuilder();
                sb.Append("ALTER TABLE [dbo].[Activity2Tasks] drop CONSTRAINT [FK_Activity2Tasks_Activities]");

                retCode = new SqlCommand(sb.ToString(), Connection.getInstance().HatchConn).ExecuteNonQuery();

                if (null != ScfStatus)
                    ScfStatus($"{currentFix++}/{nrOfFixes} - Applying Task Fix Part 11", true);
            }
            catch (Exception exc)
            {
                if (null != ScfStatus)
                    ScfStatus($"{currentFix++}/{nrOfFixes} - Applying Task Fix Part 11", false);

                txt += exc.Message + Environment.NewLine;
                retVal = false;
            }
            Thread.Sleep(500);
            //--

            //--
            try
            {
                sb = new StringBuilder();
                sb.Append("ALTER TABLE [dbo].[Activity2Tasks] drop CONSTRAINT [FK_Activity2Tasks_Task]");

                retCode = new SqlCommand(sb.ToString(), Connection.getInstance().HatchConn).ExecuteNonQuery();

                if (null != ScfStatus)
                    ScfStatus($"{currentFix++}/{nrOfFixes} - Applying Task Fix Part 12", true);
            }
            catch (Exception exc)
            {
                if (null != ScfStatus)
                    ScfStatus($"{currentFix++}/{nrOfFixes} - Applying Task Fix Part 12", false);

                txt += exc.Message + Environment.NewLine;
                retVal = false;
            }
            Thread.Sleep(500);
            //--

            //--
            try
            {
                sb = new StringBuilder();
                sb.Append("delete from Activity2Tasks where elt not in (select IdTask from Task)");

                retCode = new SqlCommand(sb.ToString(), Connection.getInstance().HatchConn).ExecuteNonQuery();

                if (null != ScfStatus)
                    ScfStatus($"{currentFix++}/{nrOfFixes} - Applying Task Fix Part 13", true);
            }
            catch (Exception exc)
            {
                if (null != ScfStatus)
                    ScfStatus($"{currentFix++}/{nrOfFixes} - Applying Task Fix Part 13", false);

                txt += exc.Message + Environment.NewLine;
                retVal = false;
            }
            Thread.Sleep(500);
            //--

            //--
            try
            {
                sb = new StringBuilder();
                sb.Append("delete from Activity2Tasks where IdTask not in (select IdActivity from Activities)");

                retCode = new SqlCommand(sb.ToString(), Connection.getInstance().HatchConn).ExecuteNonQuery();

                if (null != ScfStatus)
                    ScfStatus($"{currentFix++}/{nrOfFixes} - Applying Task Fix Part 14", true);
            }
            catch (Exception exc)
            {
                if (null != ScfStatus)
                    ScfStatus($"{currentFix++}/{nrOfFixes} - Applying Task Fix Part 14", false);

                txt += exc.Message + Environment.NewLine;
                retVal = false;
            }
            Thread.Sleep(500);
            //--

            //--
            try
            {
                sb = new StringBuilder();
                sb.Append("ALTER TABLE Task ALTER COLUMN Notes NVARCHAR(2047)");

                retCode = new SqlCommand(sb.ToString(), Connection.getInstance().HatchConn).ExecuteNonQuery();

                if (null != ScfStatus)
                    ScfStatus($"{currentFix++}/{nrOfFixes} - Expanding notes field", true);
            }
            catch (Exception exc)
            {
                if (null != ScfStatus)
                    ScfStatus($"{currentFix++}/{nrOfFixes} - Expanding notes field", false);

                txt += exc.Message + Environment.NewLine;
                retVal = false;
            }
            Thread.Sleep(500);
            //--


            //--
            try
            {
                sb = new StringBuilder();
                sb.Append("delete from Task where (IdTask not in (select elt from Activity2Tasks)) and (IdPropertyItem not in (select IdPropertyItem from PropertyItem)) and (IdNode not in (select nodeId from Node))");

                retCode = new SqlCommand(sb.ToString(), Connection.getInstance().HatchConn).ExecuteNonQuery();

                if (null != ScfStatus)
                    ScfStatus($"{currentFix++}/{nrOfFixes} - Expanding Task Fix 15", true);
            }
            catch (Exception exc)
            {
                if (null != ScfStatus)
                    ScfStatus($"{currentFix++}/{nrOfFixes} - Expanding Task Fix 15", false);

                txt += exc.Message + Environment.NewLine;
                retVal = false;
            }
            Thread.Sleep(500);
            //--

            //--
            try
            {
                sb = new StringBuilder();
                sb.Append("delete from TaskExcludedPropertyItems where IdTask not in (select IdTask from Task)");

                retCode = new SqlCommand(sb.ToString(), Connection.getInstance().HatchConn).ExecuteNonQuery();

                if (null != ScfStatus)
                    ScfStatus($"{currentFix++}/{nrOfFixes} - Expanding Task Fix 16", true);
            }
            catch (Exception exc)
            {
                if (null != ScfStatus)
                    ScfStatus($"{currentFix++}/{nrOfFixes} - Expanding Task Fix 16", false);

                txt += exc.Message + Environment.NewLine;
                retVal = false;
            }
            Thread.Sleep(500);
            //--


            //--
            try
            {
                sb = new StringBuilder();
                sb.Append("delete from TaskExcludedPropertyItems where IdPropertyItem not in (select IdPropertyItem from PropertyItem)");

                retCode = new SqlCommand(sb.ToString(), Connection.getInstance().HatchConn).ExecuteNonQuery();

                if (null != ScfStatus)
                    ScfStatus($"{currentFix++}/{nrOfFixes} - Expanding Task Fix 17", true);
            }
            catch (Exception exc)
            {
                if (null != ScfStatus)
                    ScfStatus($"{currentFix++}/{nrOfFixes} - Expanding Task Fix 17", false);

                txt += exc.Message + Environment.NewLine;
                retVal = false;
            }
            Thread.Sleep(500);
            //--

            //--
            try
            {
                sb = new StringBuilder();
                sb.Append("ALTER PROCEDURE [dbo].[GetMFWPropertyItemDetails] ( @propertyItemId uniqueidentifier ) AS  SET NOCOUNT ON  SELECT Contact.FullName, SiteAddress.FullSiteStreetAddress, SiteAddress.SiteCity, SiteAddress.SiteState, SiteAddress.SiteZIP, MailAddress.FullSiteStreetAddress, MailAddress.SiteCity, MailAddress.SiteState, MailAddress.SiteZIP, HouseProperties.HomeSqFt, ListingAgent.ListingAgentFullName, ListingAgent.ListingPrice, ListingAgent.ListingAgentFaxNumber, ForeClosureProperties.ForeClosureFileDate, ForeClosureProperties.ForeClosureSaleDate, Contact.eMail, ListingAgent.ListingAgentEMail   FROM PropertyItem LEFT OUTER JOIN Contact LEFT OUTER JOIN Address as SiteAddress ON Contact.SiteAddress = SiteAddress.IdAddress ON Contact.IdContact = PropertyItem.Owner LEFT OUTER JOIN Contact as MAContact LEFT OUTER JOIN Address as MailAddress ON MAContact.MailAddress = MailAddress.IdAddress ON MAContact.IdContact = PropertyItem.Owner LEFT OUTER JOIN DealInfo LEFT OUTER JOIN HouseProperties ON DealInfo.HouseProperty = HouseProperties.IdHouseProperty ON DealInfo.IdDealInfo = PropertyItem.DealInfo LEFT OUTER JOIN ListingAgent ON PropertyItem.ListingAgent = ListingAgent.IdListingAgent LEFT OUTER JOIN DealInfo as FCDealInfo LEFT OUTER JOIN ForeClosureProperties ON FCDealInfo.ForeClosureProperty = ForeClosureProperties.IdForeClosureProperty ON FCDealInfo.IdDealInfo = PropertyItem.DealInfo WHERE PropertyItem.IdPropertyItem = @propertyItemId  RETURN");

                retCode = new SqlCommand(sb.ToString(), Connection.getInstance().HatchConn).ExecuteNonQuery();

                if (null != ScfStatus)
                    ScfStatus($"{currentFix++}/{nrOfFixes} - Blind Offers Support", true);
            }
            catch (Exception exc)
            {
                if (null != ScfStatus)
                    ScfStatus($"{currentFix++}/{nrOfFixes} - Blind Offers Support", false);

                txt += exc.Message + Environment.NewLine;
                retVal = false;
            }
            Thread.Sleep(500);
            //--

            //--
            try
            {
                sb = new StringBuilder();
                sb.Append("delete from Activity2Tasks where elt in (select elt from Activity2Tasks A left join Task B on A.elt = B.IdTask where B.IdTask is null);");

                retCode = new SqlCommand(sb.ToString(), Connection.getInstance().HatchConn).ExecuteNonQuery();

                if (null != ScfStatus)
                    ScfStatus($"{currentFix++}/{nrOfFixes} - Activities fix", true);
            }
            catch (Exception exc)
            {
                if (null != ScfStatus)
                    ScfStatus($"{currentFix++}/{nrOfFixes} - Activities fix", false);

                txt += exc.Message + Environment.NewLine;
                retVal = false;
            }
            Thread.Sleep(500);
            //--

            //return retVal;
        }
    }
}
