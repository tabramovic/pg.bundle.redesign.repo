using System;

namespace DealMaker
{
	public delegate void FillProgressBarEventHandler(object sender, EventArgs e);

	/// <summary>
	/// Summary description for EventTrigger.
	/// </summary>
	public class EventTrigger
	{
		// An event that clients can use to be notified whenever the
		// elements of the list change.
		public event FillProgressBarEventHandler LineReadingFinished;

		// Invoke the Changed event; called whenever list changes
		protected virtual void OnLineChange(EventArgs e) 
		{
			if (LineReadingFinished != null)
				LineReadingFinished(this, e);
		}

		public void TriggerChange ()
		{
			OnLineChange(EventArgs.Empty);
		}

		public EventTrigger()
		{
		}				
	}
}
