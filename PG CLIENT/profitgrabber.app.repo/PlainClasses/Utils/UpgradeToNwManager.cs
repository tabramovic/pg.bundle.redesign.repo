﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Diagnostics;

namespace DealMaker
{
    public sealed class UpgradeToNwManager
    {
        static readonly UpgradeToNwManager _instance = new UpgradeToNwManager();

        static UpgradeToNwManager()
        { }

        UpgradeToNwManager()
        { }

        public static UpgradeToNwManager Instance
        {
            get { return _instance; }
        }

        const string LogName = "ConversionToNwLog.txt";
        string _assemblyPath = string.Empty;
        TextWriter _tw = null;

        public void UpgradeToNwVersion(string assemblyProductName, string assemblyPath)
        {
            _assemblyPath = assemblyPath;
            bool testForUpgrade = TestForUpgrade(assemblyPath);            
            
            if (testForUpgrade)
            {
                _tw = new StreamWriter(LogName);
                InstallPGDatabaseSelectService();
                UpdateUserAppFolderName();                
            }

            if (TestForUnpack(assemblyPath))
            {
                if (null == _tw)
                    _tw = new StreamWriter(LogName);
                
                UnpackSSXZIPArchive(assemblyPath);
            }

            if (null != _tw)
                _tw.Flush();
        }

        bool TestForUpgrade(string assemblyPath)
        {
            string[] files = Directory.GetFiles(assemblyPath);

            foreach (string file in files)
            {
                if (-1 != file.IndexOf(LogName))
                    return false;
            }
            
            return true;            
        }

        static IEnumerable<FileInfo> GetFilesRecursive(DirectoryInfo dirInfo)
        {
            return GetFilesRecursive(dirInfo, "installutil.exe");
        }

        static IEnumerable<FileInfo> GetFilesRecursive(DirectoryInfo dirInfo, string searchPattern)
        {
            foreach (DirectoryInfo di in dirInfo.GetDirectories())
                foreach (FileInfo fi in GetFilesRecursive(di, searchPattern))
                    yield return fi;

            foreach (FileInfo fi in dirInfo.GetFiles(searchPattern))
                yield return fi;
        }

        IEnumerable<FileInfo> BrowseForInstallUtil()
        {
            string basePath = System.Environment.GetEnvironmentVariable("windir") + @"\Microsoft.NET\Framework\";
            DirectoryInfo di = new DirectoryInfo(basePath);

            IEnumerable<FileInfo> binariesFound = GetFilesRecursive(di, "installutil.exe");
            return binariesFound;
        }
        
        void InstallPGDatabaseSelectService()
        {
            _tw.WriteLine("Try To Install PG DatabaseSelect Service");
            IEnumerable<FileInfo> installUtils = BrowseForInstallUtil();
            IEnumerator<FileInfo> enumerator = installUtils.GetEnumerator();
            FileInfo fi = null;

            while (enumerator.MoveNext())
            {
                fi = enumerator.Current;
                _tw.WriteLine("InstallUtil found at: " + fi.FullName);
            }

            _tw.WriteLine("trying with : " + fi.FullName + " " + _assemblyPath + @"\PGDatabaseSelectService.exe");
            Process.Start(fi.FullName, _assemblyPath + @"\PGDatabaseSelectService.exe");
        }

        void UpdateUserAppFolderName()
        {
            _tw.WriteLine(Environment.NewLine);
            _tw.WriteLine("Renaming userAppFolder");
            string dest = OSFolderManager.Instance.GetPlainLocalUserAppDataPath();
            string src = dest.Substring(0, dest.IndexOf(" - Network Version"));

            _tw.WriteLine("from (src): " + src);
            _tw.WriteLine("to (dest): " + dest);

            if (Directory.Exists(src))
            {
                try
                {
                    File.Move(src + "/profitgrabber.dat", dest + "/profitgrabber.dat");
                    File.Move(src + "/user.dat", dest + "/user.dat");
                    File.Move(src + "/am.dat", dest + "/am.dat");
                    
                    _tw.WriteLine("Success!");
                }
                catch (Exception exc)
                {
                    _tw.WriteLine("Error: " + exc.Message);
                }
            }
            else
            {
                _tw.WriteLine("Desc folder exists (" + dest + ")");
            }
        }

        bool TestForUnpack(string assemblyPath)
        {            
            if (Directory.Exists(assemblyPath + @"\SSX"))                            
                return false;
                                                    
            return true;            
        }

        void UnpackSSXZIPArchive(string assemblyPath)        
        {            
            _tw.WriteLine("Unzipping archive: ");
            //Process.Start(assemblyPath + @"\SSX.exe");

            ProcessStartInfo i = new ProcessStartInfo(AppDomain.CurrentDomain.BaseDirectory + "pgunzip.exe");
            i.CreateNoWindow = true;
            string args = "";


            if (assemblyPath.IndexOf(" ") != -1)
            {
                //we got a space in the path so wrap it in double qoutes
                args += "\"" + assemblyPath + @"\SSX.zip" + "\"";
            }
            else
            {
                args += assemblyPath + @"\SSX.zip";
            }

            if (assemblyPath.IndexOf(" ") != -1)
            {
                //we got a space in the path so wrap it in double qoutes
                args += " " + "\"" + assemblyPath + "\"";
            }
            else
            {
                args += " " + assemblyPath;
            }
            i.Arguments = args;

            //Mark the process window as hidden so that the progress copy window doesn't show
            i.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
            System.Diagnostics.Process p = System.Diagnostics.Process.Start(i);
            p.WaitForExit();

            _tw.WriteLine("Completed!");
        }
    }
}
