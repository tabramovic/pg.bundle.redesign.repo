using System;
using System.Threading;

using DealMaker.Registration;

namespace DealMaker
{
	/// <summary>
	/// Summary description for RegistrationChecker.
	/// </summary>
	public class RegistrationChecker
	{
		private Thread worker = null;
		private ActiveModules am = null;

		public RegistrationChecker()
		{
			this.worker = new Thread(new ThreadStart(CheckRegistration));			
			worker.Start();			
		}

		private void CheckRegistration()
		{
			while (!Globals.AbortAllThreads)
			{
				string msg = string.Empty;
				am = ApplicationHelper.GetActiveModulesFromWS(Globals.UserRegistration.IDUser, out msg);

				if (null != am && am != Globals.activeModules)
				{
					Globals.activeModules = am;
					ApplicationHelper.SaveActiveModules(am);
				}

				if (null != am && false == am.MOD_0)
				{
					ApplicationHelper.SelfDeactivate();
				}
				for (int i = 0; i < 60000; i++) //Sleep for 10 mins ( = 60 secs == 60.000 mili secs)
				{
					if (Globals.AbortAllThreads)
						break;						
					Thread.Sleep(1000);	
				}
			}
		}
	}
}
