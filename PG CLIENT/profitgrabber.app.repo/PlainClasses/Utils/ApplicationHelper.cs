#region � Using �
using System;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using System.Drawing;
using System.Drawing.Printing;
using System.Collections;
using System.Collections.Generic;
using NHibernate;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Collections.Specialized;
using System.Configuration;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Win32;
using DTD.Response;
using DataAppendService.Lib.Models.IDI;
using DealMaker.Registration;
using Newtonsoft.Json;
#endregion � Using �
#region � Namespace DealMaker �
namespace DealMaker
{
    #region � Class ApplicationHelper �
    /// <summary>
    /// Summary description for ApplicationHelper.
    /// </summary>
    public class ApplicationHelper
	{
        public static void LoadEmailMarketingSignatures()
        {
            if (File.Exists(OSFolderManager.Instance.GetPlainLocalUserAppDataPath() + @"\" + Globals.EmailBlastFileName))
            {
                using (StreamReader sr = new StreamReader(OSFolderManager.Instance.GetPlainLocalUserAppDataPath() + @"\" + Globals.EmailBlastFileName))
                {
                    Globals.EmailBlastSig = sr.ReadToEnd();
                }
            }

            if (File.Exists(OSFolderManager.Instance.GetPlainLocalUserAppDataPath() + @"\" + Globals.EmailLOIFileName))
            {
                using (StreamReader sr = new StreamReader(OSFolderManager.Instance.GetPlainLocalUserAppDataPath() + @"\" + Globals.EmailLOIFileName))
                {
                    Globals.EmailLOISig = sr.ReadToEnd();
                }
            }
        }

		#region � KillThreads �
		//sasa 4.12.05: kill threads moved to funct, must call from splash screen too
		public static void KillThreads()
		{
			//sasa, 27.01.06: nw lock query stop
			Network.NetworkLock.Stop();

			if (
				null != Globals.registrationChecker 
				//System.Threading.ThreadState.Stopped != Globals.registrationChecker.ThreadState ||
				//System.Threading.ThreadState.Aborted != Globals.registrationChecker.ThreadState
				)
			{
				Globals.registrationChecker.Abort();
				if (null != Globals.registrationReminder)
				{
					Globals.registrationReminder.Abort();
				}
				Globals.registrationChecker = null;
				Globals.registrationReminder = null;
			}


			if (null != Globals.taskManagerChecker)
			{
				Globals.taskManagerChecker.Abort();
			}
			Globals.taskManagerChecker = null;

			if (null != Globals.blinkTaskManager)
			{
				Globals.blinkTaskManager.Abort();
			}
			Globals.blinkTaskManager = null;


			if (null != Globals.NetworkUdpThreadListener)
			{
				Globals.NetworkUdpThreadListener.Abort();
				try
				{
					Network.NetworkUDP.udpClient.Close();
				}
				catch
				{}
			}
			Globals.taskManagerChecker = null;
		}
		#endregion � KillThreads �


		#region � PurifyStringToIntOrDoubleCastable �
		public static string PurifyStringToIntOrDoubleCastable (string inputString)
		{
			StringBuilder outputString = new StringBuilder();
			if (0 != inputString.Length)
			{
				char[] inputStringCharArray = inputString.ToCharArray();

				for (int i = 0; i < inputStringCharArray.Length; i++)
				{
					char tempCh = (char)inputStringCharArray[i];						
					if (Char.IsNumber(tempCh))
					{
						outputString.Append(tempCh);
					}
					else if (tempCh == '.' || tempCh == ',' || '-' == tempCh)
					{
						outputString.Append(tempCh);
					}
				}
			}
			return outputString.ToString();
		}
		#endregion � PurifyStringToIntOrDoubleCastable �
		
		#region � GetFullName �
		public static string GetFullName (string firstName, string middleName, string lastName)
        {
            var fullName = string.Empty;

            if (!string.IsNullOrWhiteSpace(firstName))
                fullName += firstName;

            switch (string.IsNullOrWhiteSpace(middleName))
            {
                case false when !string.IsNullOrWhiteSpace(fullName):
                    fullName += " " + middleName;
                    break;
                case false:
                    fullName = middleName;
                    break;
            }

            switch (string.IsNullOrWhiteSpace(lastName))
            {
                case false when !string.IsNullOrWhiteSpace(fullName):
                    fullName += " " + lastName;
                    break;
                case false:
                    fullName = lastName;
                    break;
            }

            return fullName;
        }
		#endregion � GetFullName �

		#region AssignPropertyItemToGroup
		/// <summary>		
		/// </summary>
		/// <param name="groupID">Group that PI should be assigned to</param>
		/// <param name="piID">ID of the PI</param>
		public static bool AssignPropertyItemToGroup(int groupID, Guid piID)
		{
			bool res = true;			
			try	//insert new one
			{							
				SqlCommand sp_InsertGroupMembership = new SqlCommand("InsertGroupMembership", Connection.getInstance().HatchConn);

				sp_InsertGroupMembership.CommandType = CommandType.StoredProcedure;					
				sp_InsertGroupMembership.Parameters.Add("@propertyItemId", piID);
				sp_InsertGroupMembership.Parameters.Add("@nodeId", groupID);
													
									
				if (ConnectionState.Open != Connection.getInstance().HatchConn.State)
					Connection.getInstance().HatchConn.Open();
				
				sp_InsertGroupMembership.ExecuteNonQuery();				
					
			}
			catch( Exception ex)
			{	
				res = false;
#if DEBUG
				MessageBox.Show(ex.ToString());				
#endif
			}
			return res;
		}
		#endregion
		#region � DeletePropertyItemFromGroup �
		public static bool DeletePropertyItemFromGroup(Guid propertyItemId, int nodeId)
		{
			try
			{					
				SqlCommand sp_DeleteGroupMembership = new SqlCommand("DeleteGroupMembership", Connection.getInstance().HatchConn);

				sp_DeleteGroupMembership.CommandType = CommandType.StoredProcedure;					
				sp_DeleteGroupMembership.Parameters.Add("@propertyItemId", propertyItemId);
				sp_DeleteGroupMembership.Parameters.Add("@nodeId", nodeId);
													
									
				if (ConnectionState.Open != Connection.getInstance().HatchConn.State)
					Connection.getInstance().HatchConn.Open();
				
				sp_DeleteGroupMembership.ExecuteNonQuery();				
					
			}
			catch( Exception ex)
			{					
				MessageBox.Show(ex.ToString());	
				return false;
			}
			return true;
		}
		#endregion � DeletePropertyItemFromGroup �

		#region � MathematicalFilterResult �
		public static bool MathematicalFilterResult(double val1, string oper1, double val2, string oper2, double result )
		{
			double val;
			switch (oper1)
			{
				case Globals.Plus:
					val = val1 + val2;
					return ApplicationHelper.SimpleFilterResult(val, result, oper2);
					

				case Globals.Minus:
					val = val1 - val2;
					return ApplicationHelper.SimpleFilterResult(val, result, oper2);
					

				default:
					return false;

			}
		}
		#endregion � MathematicalFilterResult �
		#region � SimpleFilterResult �
		public static bool SimpleFilterResult (double val1, double val2, string oper)
		{
			switch (oper)
			{
				case Globals.IsEqual:
					if (val1 == val2)
						return true;
					else 
						return false;					

				case Globals.DoesNotEqual:
					if (val1 != val2)
						return true;
					else 
						return false;
					

				case Globals.IsGreaterThan:
					if (val1 > val2)
						return true;
					else 
						return false;
					

				case Globals.IsGreaterThanOrEqualTo:
					if (val1 >= val2)
						return true;
					else 
						return false;
					

				case Globals.IsLessThan:
					if (val1 < val2)
						return true;
					else 
						return false;
					

				case Globals.IsLessThanOrEqualTo:
					if (val1 <= val2)
						return true;
					else 
						return false;
					

				default:
					return false;
			}
		}
		#endregion � SimpleFilterResult �
		#region � GetParentSize �
		public static System.Drawing.Size GetParentSize (System.Windows.Forms.Form parentForm)
		{
			Size s = parentForm.Size;
			return s;
		}
		#endregion � GetParentSize �		
		#region � GetDelimiter �
		public static char GetDelimiter(Delimiter dlm)
		{
			switch (dlm)
			{
				case Delimiter.comma:
					return ',';					
				case Delimiter.hash:
					return '#';					
				case Delimiter.semiComma:
					return ';';					
				case Delimiter.tab:
					return '\t';					
				default:
					return ',';
			}
		}
		#endregion � GetDelimiter �	
		#region � TestConnection �
		//sasa 06.11.2005: new sql server select logic added
		public static bool TestConnection()
		{
			return Network.Util.SetupDatabase();
						
//			//moramo smislit attach?!!, rijeseno
//
//			// "commented" down 
//			//return true;
//
//			//DatabaseSettings dbSettings = new DatabaseSettings();
//			SqlConnection connection = new SqlConnection();
//
//			do
//			{
//				try
//				{
//					NameValueCollection nvc = (NameValueCollection)ConfigurationSettings.GetConfig("ProfitGrabberDBSupport");
//					if (null != nvc && nvc.Count != 0)
//					{
//						Globals.DatabaseServer = (string)nvc["DBServerInstance"];						
//					}
//					
//					connection.ConnectionString = Globals.GetConnectionString();
//					connection.Open();
//					//MessageBox.Show("Test Connection Success: " + connection.ConnectionString.ToString());
//					break;
//				}
//				catch
//				{
//					
//					//TA++: 29.05.2005. 
//					//Req. Nr.:
//					//OLD++
//					/*
//					DefineDBServer ddbs = new DefineDBServer();
//					ddbs.ShowDialog();
//					if (true == ddbs.Valid)
//					{
//						Globals.Save(ddbs.DBServerName);
//						Connection.Reset();
//						Globals.InitNHibernate();
//					}
//					else
//					{
//						return (false);
//					}
//					*/
//					//OLD--
//
//					//NEW++
//					//Attach Database action:
//					//	.mdf && .ldf are located at the same directory as .exe itself
//					//	We will manually evoke attach database action if installer didn't succeed
//					//Syntax:
//					//	osql -E -S 127.0.0.1\PG_DB2 - Q "exec sp_attach_db @dbName = N'dbDealMaker', 
//					//		@filename1 = N'<FULLPATH>\dbDealMaker_Data.MDF', 
//					//		@filename2 = N'<FULLPATH>\dbDealMaker_Log.LDF'"
//					
//					string osqlArgs = string.Empty;
//					osqlArgs += @"-E -S 127.0.0.1\PG_DB2 -Q ""exec sp_attach_db @dbName = N'dbDealMaker', "; 
//					//osqlArgs += "\n";
//					osqlArgs += @"@filename1 = N'" + Application.StartupPath + @"\dbDealMaker_Data.MDF', ";
//					//osqlArgs += "\n";
//					osqlArgs += @"@filename2 = N'" + Application.StartupPath + @"\dbDealMaker_Log.LDF'"" ";
//					//osqlArgs += "\n";
//					//osqlArgs += "\npause";
//					//MessageBox.Show(osqlArgs);
//					try
//					{
//						System.Diagnostics.Process.Start("osql.exe", osqlArgs);
//						System.Threading.Thread.Sleep(3000);
//					}
//					catch (Exception e)
//					{
//						MessageBox.Show(e.Message, "\n" + e.InnerException);
//						
//					}
//					
//					//NEW--
//					
//					//TA--: 29.05.2005. 
//				}
//
//				
//			} while (true);
//
//			return (true);
		}
		#endregion � TestConnection �
		#region � GetPropertyItemsInNode �
		public static Hashtable GetPropertyItemsInNode (int nodeId)
		{
			Hashtable	htNodePosPropertyItemId = new Hashtable();
			SqlDataReader nodeReader = null;
			try
			{
				SqlCommand sp_GetPropertyItemsInNode = new SqlCommand("GetPropertyItemsInNode", Connection.getInstance().HatchConn);
				sp_GetPropertyItemsInNode.CommandType = CommandType.StoredProcedure;								
				sp_GetPropertyItemsInNode.Parameters.Add("@NodeId", nodeId);
									
				if (ConnectionState.Open != Connection.getInstance().HatchConn.State)
					Connection.getInstance().HatchConn.Open();

				sp_GetPropertyItemsInNode.ExecuteNonQuery();

				nodeReader = sp_GetPropertyItemsInNode.ExecuteReader();

				int iPos = 1;
				while (nodeReader.Read())
				{
					Guid	propertyItemId		= nodeReader.GetGuid(1);
					htNodePosPropertyItemId.Add(iPos, propertyItemId);	
					iPos++;
				}
			}
			catch	(Exception exc)
			{
				MessageBox.Show(exc.Message);
			}
			finally
			{
				if (null != nodeReader)
                    nodeReader.Close();				
			}
			return htNodePosPropertyItemId;
		}
		#endregion � GetPropertyItemsInNode �

        public static List<Guid> GetPropertyItemIDsInNode(int nodeId)
        {
            var res = new List<Guid>();
			SqlDataReader nodeReader = null;
            try
            {
                SqlCommand sp_GetPropertyItemsInNode = new SqlCommand("GetPropertyItemsInNode", Connection.getInstance().HatchConn);
                sp_GetPropertyItemsInNode.CommandType = CommandType.StoredProcedure;
                sp_GetPropertyItemsInNode.Parameters.Add("@NodeId", nodeId);

                if (ConnectionState.Open != Connection.getInstance().HatchConn.State)
                    Connection.getInstance().HatchConn.Open();

                sp_GetPropertyItemsInNode.ExecuteNonQuery();

                nodeReader = sp_GetPropertyItemsInNode.ExecuteReader();

                while (nodeReader.Read())
                    res.Add(nodeReader.GetGuid(1));
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }
            finally
            {
                if (null != nodeReader)
                    nodeReader.Close();
            }
            return res;
        }

		#region � GetAllPropertyItems �
		public static Hashtable GetAllPropertyItems()
		{
			Hashtable	htNodePosPropertyItemId = new Hashtable();
			SqlDataReader nodeReader = null;
			try
			{
				SqlCommand sp_GetAllPropertyItems = new SqlCommand("GetAllPropertyItems", Connection.getInstance().HatchConn);				
				sp_GetAllPropertyItems.CommandType = CommandType.StoredProcedure;				
									
				if (ConnectionState.Open != Connection.getInstance().HatchConn.State)
					Connection.getInstance().HatchConn.Open();

				sp_GetAllPropertyItems.ExecuteNonQuery();

				nodeReader = sp_GetAllPropertyItems.ExecuteReader();

				int iPos = 1;
				while (nodeReader.Read())
				{
					Guid	propertyItemId		= nodeReader.GetGuid(0);
					htNodePosPropertyItemId.Add(iPos, propertyItemId);	
					iPos++;
				}
			}
			catch	(Exception exc)
			{
				MessageBox.Show(exc.Message);
			}
			finally
			{
				nodeReader.Close();				
			}
			return htNodePosPropertyItemId;
		}
		#endregion � GetAllPropertyItems �
		#region � GetPropertyCountInNode �
		public static int GetPropertyCountInNode (int nodeId)
		{
			Int32 count = 0;
			SqlDataReader nodeReader = null;
			try
			{
				SqlCommand sp_GetPropertyCount = new SqlCommand("GetPropertyCount", Connection.getInstance().HatchConn);
				sp_GetPropertyCount.CommandType = CommandType.StoredProcedure;								
				sp_GetPropertyCount.Parameters.Add("@NodeId", nodeId);
									
				if (ConnectionState.Open != Connection.getInstance().HatchConn.State)
					Connection.getInstance().HatchConn.Open();

				sp_GetPropertyCount.ExecuteNonQuery();

				nodeReader = sp_GetPropertyCount.ExecuteReader();

				while (nodeReader.Read())
				{
					count = nodeReader.GetInt32(0);					
				}
			}
			catch	(Exception exc)
			{
				MessageBox.Show(exc.Message);
			}
			finally
			{
				if (null != nodeReader)
                    nodeReader.Close();				
			}
			return count;
		}
		#endregion � GetPropertyCountInNode �
		#region � LoadPropertyItem �
		public static PropertyItem LoadPropertyItem (Guid propertyItemId)
		{
			ISession session = null;
			ITransaction transaction = null;
			PropertyItem pi = null;

            //using (SqlConnection sqlConn = new SqlConnection(@"user id=sa;password=" + Globals.DatabasePwd + ";persist security info=False;Data Source=" + Globals.DatabaseServer + ";Initial Catalog=" + Globals.DatabaseName + ";"))
            using (SqlConnection sqlConn = new SqlConnection(DealMaker.Network.Util.GetConnectionStringFromServerName(Globals.DatabaseServer)))
            {

                try
                {
                    //Connection conn = Connection.getInstance();
                    //SqlConnection sqlConn = conn.HatchConn;

                    sqlConn.Open();

                    IDbConnection iSqlConn = sqlConn as IDbConnection;

                    NHibernate.Cfg.Configuration cfg = Globals.cfg;
                    ISessionFactory factory = Globals.factory;


                    if (null != iSqlConn)
                    {
                        session = factory.OpenSession(iSqlConn);
                        if (ConnectionState.Closed == iSqlConn.State)
                        {
                            iSqlConn.Open();
                        }
                    }
                    else
                    {
                        session = factory.OpenSession();
                    }
                    transaction = session.BeginTransaction();
                    pi = (PropertyItem)session.Load(typeof(PropertyItem), propertyItemId);
                    transaction.Commit();
                }
                catch (Exception exc)
                {
                    MessageBox.Show(exc.ToString());
                    transaction.Rollback();
                }
                finally
                {
                    session.Close();

                    if (null != sqlConn && ConnectionState.Open == sqlConn.State)
                        sqlConn.Close();
                }
            }

			return pi;
		}
		#endregion � LoadPropertyItem �
		#region � DeleteTaggedPropertyItems �
		public static void DeleteTaggedPropertyItems(ArrayList alPropertyItemsId, out bool updateInputData)
		{
			if (DialogResult.No == MessageBox.Show(null, UserMessages.PermanentRecordDeleteDesc, UserMessages.PermanentRecordDelete , MessageBoxButtons.YesNo, MessageBoxIcon.Warning ))
			{
				updateInputData = false;
				return;
			}
			ISession session = null;
			ITransaction transaction = null;
			PropertyItem pi = null;
			try
			{
			
				Connection conn = Connection.getInstance();     
				SqlConnection sqlConn = conn.HatchConn;
				IDbConnection iSqlConn = sqlConn as IDbConnection;

                NHibernate.Cfg.Configuration cfg = Globals.cfg;				
				ISessionFactory factory = Globals.factory;
				
				
				if (null != iSqlConn) 
				{
					session = factory.OpenSession(iSqlConn);
					if (ConnectionState.Closed == iSqlConn.State) 
					{
						iSqlConn.Open();
					}
				}
				else 
				{
					session = factory.OpenSession();
				}

				foreach(Guid propertyItemId in alPropertyItemsId)
				{
					transaction = session.BeginTransaction();					
					pi = (PropertyItem)session.Load(typeof(PropertyItem), propertyItemId);
					session.Delete(pi);					
					transaction.Commit();										
					ApplicationHelper.DeletePropertyItemFromGroups(propertyItemId);
				}
			}
			catch (Exception exc)
			{
				MessageBox.Show(exc.ToString());
				transaction.Rollback();
			}				
			finally
			{				
				session.Close();
				updateInputData = true;
			}			
		}
		#endregion � DeleteTaggedPropertyItems �
		#region � DeletePropertyItemFromGroups �
		public static bool DeletePropertyItemFromGroups (Guid propertyItemId)
		{
			try
			{					
				SqlCommand sp_DeletePropertyItemFromGroups = new SqlCommand("DeletePropertyItemFromGroups", Connection.getInstance().HatchConn);				

				sp_DeletePropertyItemFromGroups.CommandType = CommandType.StoredProcedure;					
				sp_DeletePropertyItemFromGroups.Parameters.Add("@propertyItemId", propertyItemId);
																						
				if (ConnectionState.Open != Connection.getInstance().HatchConn.State)
					Connection.getInstance().HatchConn.Open();
				
				sp_DeletePropertyItemFromGroups.ExecuteNonQuery();				
					
			}
			catch( Exception ex)
			{					
				MessageBox.Show(ex.ToString());	
				return false;
			}
			return true;
		}
		#endregion � DeletePropertyItemFromGroups �
		#region � GetSubNodes �
		public void GetSubNodes(int parentNode, ref ArrayList alSubNodes)
		{
			//ArrayList alSubNodes = new ArrayList();            
            if (null == alSubNodes)
                alSubNodes = new ArrayList();

            alSubNodes.Add(parentNode);

			ArrayList tempNodeList = new ArrayList();
			SqlDataReader nodeReader = null;
			try
			{
				SqlCommand sp_getChildrenFromNode = new SqlCommand("getChildrenFromNode", Connection.getInstance().HatchConn);
				sp_getChildrenFromNode.Parameters.Add("@parentNode", parentNode);
				sp_getChildrenFromNode.CommandType = CommandType.StoredProcedure;				
									
				if (ConnectionState.Open != Connection.getInstance().HatchConn.State)
					Connection.getInstance().HatchConn.Open();

				sp_getChildrenFromNode.ExecuteNonQuery();

				nodeReader = sp_getChildrenFromNode.ExecuteReader();				
				if (nodeReader.HasRows)
				{
					while (nodeReader.Read())
					{
						int currentNodeId = nodeReader.GetInt32(0);
						tempNodeList.Add(currentNodeId);						
					}
				}
				else
				{
					alSubNodes.Add(parentNode);
                    return; // alSubNodes;
				}				
			}
			catch	(Exception exc)
			{
				MessageBox.Show(exc.Message);
			}
			finally
			{
				nodeReader.Close();				
			}
			if (tempNodeList.Count > 0)
			{
				while (tempNodeList.Count > 0)
				{
                    ArrayList tempAl = new ArrayList();
                    GetSubNodes((int)tempNodeList[0], ref alSubNodes);

					tempNodeList.RemoveAt(0);
					//alSubNodes.AddRange(tempAl);
				}
			}
            return; //alSubNodes;
		}
		#endregion � GetSubNodes �
		#region � GetStatusGroupMembership �
		public static string GetStatusGroupMembership(Guid propertyItemId)
		{
			SqlDataReader statusGroupReader = null;
			string statusGroupName = string.Empty;
			try
			{				
				SqlCommand sp_GetStatusGroupMembership = new SqlCommand("GetStatusGroupMembership", Connection.getInstance().HatchConn);

				sp_GetStatusGroupMembership.CommandType = CommandType.StoredProcedure;					
				sp_GetStatusGroupMembership.Parameters.Add("@propertyItemId", propertyItemId);
													
									
				if (ConnectionState.Open != Connection.getInstance().HatchConn.State)
					Connection.getInstance().HatchConn.Open();
				
				statusGroupReader = sp_GetStatusGroupMembership.ExecuteReader();
				while (statusGroupReader.Read())
				{															
					statusGroupName = statusGroupReader.GetString(1).TrimEnd();						
				}
				statusGroupReader.Close();
			}
			catch( Exception ex)
			{				
				statusGroupReader.Close();
				MessageBox.Show(ex.ToString());				
			}
			return statusGroupName;
		}
		#endregion � GetStatusGroupMembership �
		#region � GetGroupMembershipData �
		public static ArrayList GetGroupMembershipData(Guid propertyItemId)
		{
			ArrayList alGroupNames = new ArrayList();			
			SqlDataReader groupReader = null;
			SqlDataReader statusGroupReader = null;
			try
			{							
				SqlCommand sp_GetGroupMembership = new SqlCommand("GetGroupMembership", Connection.getInstance().HatchConn);

				sp_GetGroupMembership.CommandType = CommandType.StoredProcedure;					
				sp_GetGroupMembership.Parameters.Add("@propertyItemId", propertyItemId);
																						
				if (ConnectionState.Open != Connection.getInstance().HatchConn.State)
					Connection.getInstance().HatchConn.Open();
				
				groupReader = sp_GetGroupMembership.ExecuteReader();
				while (groupReader.Read())
				{
					string groupName = groupReader.GetString(1).TrimEnd();
					alGroupNames.Add(groupName);
				}
				groupReader.Close();
			}
			catch( Exception ex)
			{
				groupReader.Close();
				statusGroupReader.Close();
				MessageBox.Show(ex.ToString());				
			}
			return alGroupNames;
		}

		public static ArrayList GetGroupMembership(Guid propertyItemId, out Node statusGroup)
		{
			ArrayList alGroupNames = new ArrayList();
			Node currentStatusGroup = null;
			SqlDataReader groupReader = null;
			SqlDataReader statusGroupReader = null;
			try
			{							
				SqlCommand sp_GetGroupMembership = new SqlCommand("GetGroupMembership", Connection.getInstance().HatchConn);

				sp_GetGroupMembership.CommandType = CommandType.StoredProcedure;					
				sp_GetGroupMembership.Parameters.Add("@propertyItemId", propertyItemId);
													
									
				if (ConnectionState.Open != Connection.getInstance().HatchConn.State)
					Connection.getInstance().HatchConn.Open();
				
				groupReader = sp_GetGroupMembership.ExecuteReader();
				while (groupReader.Read())
				{
					Node n = new Node();
					n.nodeId = groupReader.GetInt32(0);
					n.nodeName = groupReader.GetString(1);
					n.nodeDesc = groupReader.GetString(2);
					alGroupNames.Add(n);
				}
				groupReader.Close();

				SqlCommand sp_GetStatusGroupMembership = new SqlCommand("GetStatusGroupMembership", Connection.getInstance().HatchConn);

				sp_GetStatusGroupMembership.CommandType = CommandType.StoredProcedure;					
				sp_GetStatusGroupMembership.Parameters.Add("@propertyItemId", propertyItemId);
													
									
				if (ConnectionState.Open != Connection.getInstance().HatchConn.State)
					Connection.getInstance().HatchConn.Open();
				
				statusGroupReader = sp_GetStatusGroupMembership.ExecuteReader();
				while (statusGroupReader.Read())
				{					
					currentStatusGroup = new Node();
					currentStatusGroup.nodeId = statusGroupReader.GetInt32(0);
					currentStatusGroup.nodeName = statusGroupReader.GetString(1).TrimEnd();	
					currentStatusGroup.nodeDesc = statusGroupReader.GetString(2).TrimEnd();
				}
				statusGroupReader.Close();
			}
			catch( Exception ex)
			{
				groupReader.Close();
				statusGroupReader.Close();
				MessageBox.Show(ex.ToString());				
			}
			finally 
			{
				statusGroup = currentStatusGroup;				
			}
			return alGroupNames;
		}
		#endregion � GetGroupMembershipData �

		public static int GetParentNodeId(int nodeId)
		{			
			SqlDataReader reader = null;
			int parentNodeId = -1;
			string cmdText = "select parentNodeId from Node where nodeId = " + nodeId.ToString();
			try
			{
				SqlCommand command = new SqlCommand(cmdText, Connection.getInstance().HatchConn);
				reader = command.ExecuteReader();
				
				while (reader.Read())
				{
					if (!reader.IsDBNull(0))
						parentNodeId = reader.GetInt32(0);

					break;
				}
			}
			catch {}
			finally
			{
				if (null != reader && !reader.IsClosed)
					reader.Close();
			}
			
			return parentNodeId;
		}

        public static List<KeyValuePair<int, string>> GetChildNodeIds(int nodeId)
        {
            var resList = new List<KeyValuePair<int, string>>();            
            
            string cmdText = "select nodeId, nodeName from Node where parentNodeId = " + nodeId.ToString();
            try
            {
                SqlCommand command = new SqlCommand(cmdText, Connection.getInstance().HatchConn);
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                        if (!reader.IsDBNull(0) && !reader.IsDBNull(1))
                            resList.Add(new KeyValuePair<int, string>(reader.GetInt32(0), reader.GetString(1).Trim()));
                }                                                     
            }
            catch { }
            
            return resList;
        }

        /// <summary>
        /// Create Node
        /// </summary>
        /// <param name="parentNode">Parent Node Id</param>
        /// <param name="nodeDesc">Node description</param>
        /// <param name="nodeName">Node Name</param>
        /// <param name="nodeId">Id of the newly created node</param>
        /// <param name="dbConn">Database connection</param>
        /// <returns></returns>
        public static bool CreateNode(int parentNode, string nodeDesc, string nodeName, out int nodeId, SqlConnection dbConn)
		{
			bool bResult = true;
			nodeId = -1;
			try
			{
				SqlCommand sp_dodaj_Node = new SqlCommand("dodaj_Node", dbConn);

				sp_dodaj_Node.CommandType = CommandType.StoredProcedure;
					
				sp_dodaj_Node.Parameters.Add("@parentNode", parentNode);
				sp_dodaj_Node.Parameters.Add("@nodeDesc", nodeDesc);
				sp_dodaj_Node.Parameters.Add("@nodeName", nodeName);
					
				int retVal = -1;
				SqlParameter param = sp_dodaj_Node.Parameters.Add("@retValue", retVal);
				param.Direction = ParameterDirection.Output;

				SqlParameter new_NodeId_param = sp_dodaj_Node.Parameters.Add("@newNodeId ", nodeId);
				new_NodeId_param.Direction = ParameterDirection.Output;
												
				sp_dodaj_Node.ExecuteNonQuery();
	
				if (-1 == (int)param.Value)
				{
					bResult = false;
				}
				else
				{					
					nodeId = (System.Int32)new_NodeId_param.Value;				
				}
			}
			catch 
			{
				bResult = false;
			}			
			return bResult;
		}

		#region � LoadUser �
		public static User LoadUser()
		{            
			ISession session = null;
			ITransaction transaction = null;
			User u = null;

            //NEW
            //SqlConnection sqlConn = new SqlConnection(@"user id=sa;password=" + Globals.DatabasePwd + ";persist security info=False;Data Source=" + Globals.DatabaseServer + ";Initial Catalog=" + Globals.DatabaseName + ";");
            SqlConnection sqlConn = new SqlConnection(DealMaker.Network.Util.GetConnectionStringFromServerName(Globals.DatabaseServer));

            try
			{							                
                //OLD
                //Connection conn = Connection.getInstance();
                //SqlConnection sqlConn = conn.HatchConn;
               
                IDbConnection iSqlConn = sqlConn as IDbConnection;

                NHibernate.Cfg.Configuration cfg = Globals.cfg;
                ISessionFactory factory = Globals.factory;


                if (null != iSqlConn)
                {
                    session = factory.OpenSession(iSqlConn);
                    if (ConnectionState.Closed == iSqlConn.State)
                    {
                        iSqlConn.Open();
                    }
                }
                else
                {
                    session = factory.OpenSession();
                }
                transaction = session.BeginTransaction();
                u = (User)session.Load(typeof(User), Guid.Empty);
                transaction.Commit();
                
			}
			catch (Exception exc)
			{
				//MessageBox.Show("Please go to File -> User Information and confirm Your user information data!", "User Information Confirmation needed!", MessageBoxButtons.OK , MessageBoxIcon.Exclamation);
				transaction.Rollback();
			}				
			finally
			{				
				//u = new User();
				session.Close();

                if (null != sqlConn && ConnectionState.Open == sqlConn.State)
                    sqlConn.Close();
			}			
			return u;
		}
		#endregion � LoadUser �
		#region � SaveUser �
		public static void SaveUser (User u)
		{
			ISession session = null;
			ITransaction transaction = null;

            //NEW
            //SqlConnection sqlConn = new SqlConnection(@"user id=sa;password=" + Globals.DatabasePwd + ";persist security info=False;Data Source=" + Globals.DatabaseServer + ";Initial Catalog=" + Globals.DatabaseName + ";");
            SqlConnection sqlConn = new SqlConnection(DealMaker.Network.Util.GetConnectionStringFromServerName(Globals.DatabaseServer));

            try
			{
			    //OLD
				//Connection conn = Connection.getInstance();
                //SqlConnection sqlConn = conn.HatchConn;                
				
				IDbConnection iSqlConn = sqlConn as IDbConnection;

                NHibernate.Cfg.Configuration cfg = Globals.cfg;				
				ISessionFactory factory = Globals.factory;
				
				
				if (null != iSqlConn) 
				{
					session = factory.OpenSession(iSqlConn);
					if (ConnectionState.Closed == iSqlConn.State) 
					{
						iSqlConn.Open();
					}
				}
				else 
				{
					session = factory.OpenSession();
				}				
								
				
				if (null != ApplicationHelper.LoadUser())
				{
					transaction = session.BeginTransaction();	
					u.NhSaveOrUpdate(session);				
				}
				else
				{
					transaction = session.BeginTransaction();	
					u.NhSave(session);
				}
				transaction.Commit();																			
			}
			catch (Exception exc)
			{
				MessageBox.Show(exc.ToString());
				transaction.Rollback();
			}				
			finally
			{				
				session.Close();

                if (null != sqlConn && ConnectionState.Open == sqlConn.State)
                    sqlConn.Close();
			}			
		}
		#endregion � SaveUser �
		#region � GetMediaList �
		public static ArrayList GetMediaList()
		{				
            ArrayList	alMedia = new ArrayList();
			SqlDataReader mediaReader = null;

            //using (SqlConnection sqlConn = new SqlConnection(@"user id=sa;password=" + Globals.DatabasePwd + ";persist security info=False;Data Source=" + Globals.DatabaseServer + ";Initial Catalog=" + Globals.DatabaseName + ";"))
            using (SqlConnection sqlConn = new SqlConnection(DealMaker.Network.Util.GetConnectionStringFromServerName(Globals.DatabaseServer)))
            {

                try
                {
                    sqlConn.Open();

                    SqlCommand sp_GetAllMedia = new SqlCommand("GetAllMedia", sqlConn/*Connection.getInstance().HatchConn*/);
                    sp_GetAllMedia.CommandType = CommandType.StoredProcedure;

                    if (ConnectionState.Open != Connection.getInstance().HatchConn.State)
                        Connection.getInstance().HatchConn.Open();

                    sp_GetAllMedia.ExecuteNonQuery();

                    mediaReader = sp_GetAllMedia.ExecuteReader();

                    while (mediaReader.Read())
                    {
                        Guid mediaId = mediaReader.GetGuid(0);
                        alMedia.Add(mediaId);
                    }
                }
                catch (Exception exc)
                {
                    MessageBox.Show("GetMediaList: " + exc.Message);
                }
                finally
                {
                    mediaReader.Close();

                    if (null != sqlConn && ConnectionState.Open == sqlConn.State)
                        sqlConn.Close();
                }
            }

			return alMedia;
		}
		#endregion � GetMediaList �
		#region � LoadMedia �
		public static Media LoadMedia (Guid idMedia)
		{
			ISession session = null;
			ITransaction transaction = null;
			Media m = null;
            //Connection conn = Connection.getInstance();
            //lock (conn)
            {
                //NEW
                //SqlConnection sqlConn = new SqlConnection(@"user id=sa;password=" + Globals.DatabasePwd + ";persist security info=False;Data Source=" + Globals.DatabaseServer + ";Initial Catalog=" + Globals.DatabaseName + ";");
                SqlConnection sqlConn = new SqlConnection(DealMaker.Network.Util.GetConnectionStringFromServerName(Globals.DatabaseServer));

                try
                {
                    //OLD
                    //Connection conn = Connection.getInstance();     
                    //SqlConnection sqlConn = conn.HatchConn;                    

                    IDbConnection iSqlConn = sqlConn as IDbConnection;

                    NHibernate.Cfg.Configuration cfg = Globals.cfg;
                    ISessionFactory factory = Globals.factory;


                    if (null != iSqlConn)
                    {
                        session = factory.OpenSession(iSqlConn);
                        if (ConnectionState.Closed == iSqlConn.State)
                        {
                            iSqlConn.Open();
                        }
                    }
                    else
                    {
                        session = factory.OpenSession();
                    }
                    transaction = session.BeginTransaction();
                    m = (Media)session.Load(typeof(Media), idMedia);
                    transaction.Commit();
                }
                catch (Exception exc)
                {
                    MessageBox.Show("LoadMedia: " + exc.ToString());
                    transaction.Rollback();
                }
                finally
                {
                    session.Close();

                    if (null != sqlConn && ConnectionState.Open == sqlConn.State)
                        sqlConn.Close();
                }
            }
			return m;
		}
		#endregion � LoadMedia �

		public static void SaveMedia(Media m)
		{
			ISession session = null;
			ITransaction transaction = null;			
			try
			{
			
				Connection conn = Connection.getInstance();     
				SqlConnection sqlConn = conn.HatchConn;
				IDbConnection iSqlConn = sqlConn as IDbConnection;

                NHibernate.Cfg.Configuration cfg = Globals.cfg;				
				ISessionFactory factory = Globals.factory;
				
				
				if (null != iSqlConn) 
				{
					session = factory.OpenSession(iSqlConn);
					if (ConnectionState.Closed == iSqlConn.State) 
					{
						iSqlConn.Open();
					}
				}
				else 
				{
					session = factory.OpenSession();
				}				
				transaction = session.BeginTransaction();					
				m.NhSave(session);				
				transaction.Commit();																			
			}
			catch (Exception exc)
			{
				MessageBox.Show(exc.ToString());
				transaction.Rollback();
			}				
			finally
			{				
				session.Close();				
			}						
		}
		#region � UpdateMedia �
		public static void SaveMedia(Media m, eSaveOrUpdate saveOrUpdate)
		{
			ISession session = null; 
			ITransaction transaction = null;			
			try
			{
			
				Connection conn = Connection.getInstance();     
				SqlConnection sqlConn = conn.HatchConn;
				IDbConnection iSqlConn = sqlConn as IDbConnection;

                NHibernate.Cfg.Configuration cfg = Globals.cfg;				
				ISessionFactory factory = Globals.factory;
				
				
				if (null != iSqlConn) 
				{
					session = factory.OpenSession(iSqlConn);
					if (ConnectionState.Closed == iSqlConn.State) 
					{
						iSqlConn.Open();
					}
				}
				else 
				{
					session = factory.OpenSession();
				}				
				transaction = session.BeginTransaction();					
				
				if (saveOrUpdate == eSaveOrUpdate.Update)
				{
					m.NhSaveOrUpdate(session);				
				}
				else
				{
					m.NhSave(session);
				}
				transaction.Commit();																			
			}
			catch (Exception exc)
			{
				MessageBox.Show(exc.ToString());
				transaction.Rollback();
			}				
			finally
			{				
				session.Close();				
			}			
		}
		#endregion � UpdateMedia �
		#region � DeleteMedia �
		public static void DeleteMedia(Guid mediaId)
		{
			ISession session = null;
			ITransaction transaction = null;
			Media m = null;
			try
			{
			
				Connection conn = Connection.getInstance();     
				SqlConnection sqlConn = conn.HatchConn;
				IDbConnection iSqlConn = sqlConn as IDbConnection;

                NHibernate.Cfg.Configuration cfg = Globals.cfg;				
				ISessionFactory factory = Globals.factory;
				
				
				if (null != iSqlConn) 
				{
					session = factory.OpenSession(iSqlConn);
					if (ConnectionState.Closed == iSqlConn.State) 
					{
						iSqlConn.Open();
					}
				}
				else 
				{
					session = factory.OpenSession();
				}
				

				transaction = session.BeginTransaction();					
				m = (Media)session.Load(typeof(Media), mediaId);
				session.Delete(m);					
				transaction.Commit();
			}
			catch (Exception exc)
			{
				MessageBox.Show(exc.ToString());
				transaction.Rollback();
			}				
			finally
			{				
				session.Close();				
			}		
		}
		#endregion � DeleteMedia �
		
		#region � OLD GRAPHICS �
		/*

		#region � GetLargeLabelGraphics �
		public static Graphics GetLargeLabelGraphics (PrintPageEventArgs e, ArrayList alSelectedRecords)
		{
			
			Font m_BarcodeFont = new Font("IDAutomationHC39M", 6);
			Font m_DisplayFont = new Font("Tahoma", 8);
			Graphics g = e.Graphics;			

			Brush br = new SolidBrush(Color.Black);				

			int iCnt  = Globals.mediumLabelsCount;
			int iLocalCnt = 0;
			int iCols = 3;
			int iColNr = -1;

			int horizontalOffset = 72;
			//int horizontalBarcodeOffset = 75;
			
			int iHorizontalDistance = 275;
			int iVerticalDistance = 100;


			while (iLocalCnt < 30 && iCnt < alSelectedRecords.Count)
			{
				Guid propertyItemId = (Guid)alSelectedRecords[iCnt];
				PropertyItem pi = ApplicationHelper.LoadPropertyItem(propertyItemId);
			
				int iRes = (int)(iCnt % iCols);
				if (0 == iRes)
					iColNr++;

				if (null != pi.Owner)
				{
					if (string.Empty != pi.Owner.FullName.Trim())
						g.DrawString(pi.Owner.FullName.ToUpper(), m_DisplayFont, br, (iRes * iHorizontalDistance + horizontalOffset), (iColNr * iVerticalDistance) +  53); 
					else
						g.DrawString(UserMessages.CurrentResident, m_DisplayFont, br, (iRes * iHorizontalDistance + horizontalOffset), (iColNr * iVerticalDistance) +  53); 
				}
				else
					g.DrawString(string.Empty, m_DisplayFont, br, (iRes * iHorizontalDistance + horizontalOffset), (iColNr * iVerticalDistance) +  53); 


				if (
					null != pi.Owner && 
					null != pi.Owner.MailAddress && 
						(
							string.Empty != pi.Owner.MailAddress.FullSiteStreetAddress.Trim() ||
							string.Empty != pi.Owner.MailAddress.SiteCitySiteStateSiteZIP.Trim()
						)
					)
				{
					g.DrawString(pi.Owner.MailAddress.FullSiteStreetAddress.ToUpper(), m_DisplayFont, br, (iRes * iHorizontalDistance + horizontalOffset), (iColNr * iVerticalDistance) + 73); 
					g.DrawString(pi.Owner.MailAddress.SiteCitySiteStateSiteZIP.ToUpper(), m_DisplayFont, br, (iRes * iHorizontalDistance + horizontalOffset), (iColNr * iVerticalDistance) + 93); 
				}
				else if (
							null != pi.Owner && 
							null != pi.Owner.SiteAddress &&
								(
									string.Empty != pi.Owner.SiteAddress.FullSiteStreetAddress.Trim() ||
									string.Empty != pi.Owner.SiteAddress.SiteCitySiteStateSiteZIP.Trim()
								)
						)
				{
					g.DrawString(pi.Owner.SiteAddress.FullSiteStreetAddress.ToUpper(), m_DisplayFont, br, (iRes * iHorizontalDistance + horizontalOffset), (iColNr * iVerticalDistance) + 73); 
					g.DrawString(pi.Owner.SiteAddress.SiteCitySiteStateSiteZIP.ToUpper(), m_DisplayFont, br, (iRes * iHorizontalDistance + horizontalOffset), (iColNr * iVerticalDistance) + 93); 
				}
				else
				{
					g.DrawString(string.Empty, m_DisplayFont, br, (iRes * iHorizontalDistance + horizontalOffset), (iColNr * iVerticalDistance) + 73); 
					g.DrawString(string.Empty, m_DisplayFont, br, (iRes * iHorizontalDistance + horizontalOffset), (iColNr * iVerticalDistance) + 93); 
				}


				StringFormat drawFormat = new StringFormat();
				drawFormat.FormatFlags = StringFormatFlags.DirectionVertical;
				g.DrawString("*" + pi.IdPropertyItem.ToString().Substring(0, 6) + "*", m_BarcodeFont, br, (iRes * iHorizontalDistance + 12), (iColNr * iVerticalDistance) +  40, drawFormat); 
				iCnt++;
				iLocalCnt++;

				if (iCnt % 30 == 0)
				{
					Globals.mediumLabelsCount += 30;
					e.HasMorePages = true;

				}

			}

			return g;
		}
		#endregion � GetLargeLabelGraphics �
		#region � GetMediumLabelGraphics �
		public static Graphics GetMediumLabelGraphics (PrintPageEventArgs e, ArrayList alSelectedRecords)
		{
			

			//Font m_BarcodeFont = new Font("IDAutomationHC39M", 6);
			Font m_DisplayFont = new Font("Tahoma", 9);
			Graphics g = e.Graphics;			

			Brush br = new SolidBrush(Color.Black);				

			int iCnt  = Globals.mediumLabelsCount;
			int iLocalCnt = 0;
			int iCols = 3;
			int iColNr = -1;

			int horizontalOffset = 15;
			//int horizontalBarcodeOffset = 75;
			
			int iHorizontalDistance = 275;
			int iVerticalDistance = 100;


			while (iLocalCnt < 30 && iCnt < alSelectedRecords.Count)
			{
				Guid propertyItemId = (Guid)alSelectedRecords[iCnt];
				PropertyItem pi = ApplicationHelper.LoadPropertyItem(propertyItemId);
			
				int iRes = (int)(iCnt % iCols);
				if (0 == iRes)
					iColNr++;

				if (null != pi.Owner)
				{
					if (string.Empty != pi.Owner.FullName.Trim())
						g.DrawString(pi.Owner.FullName.ToUpper(), m_DisplayFont, br, (iRes * iHorizontalDistance + horizontalOffset), (iColNr * iVerticalDistance) +  53); 
					else
						g.DrawString(UserMessages.CurrentResident, m_DisplayFont, br, (iRes * iHorizontalDistance + horizontalOffset), (iColNr * iVerticalDistance) +  53); 
				}
				else
					g.DrawString(string.Empty, m_DisplayFont, br, (iRes * iHorizontalDistance + horizontalOffset), (iColNr * iVerticalDistance) +  53); 


				//if (null != pi.Owner && null != pi.Owner.MailAddress)
				if (
						null != pi.Owner && 
						null != pi.Owner.MailAddress && 
						(
						string.Empty != pi.Owner.MailAddress.FullSiteStreetAddress.Trim() ||
						string.Empty != pi.Owner.MailAddress.SiteCitySiteStateSiteZIP.Trim()
						)
					)
				{
					g.DrawString(pi.Owner.MailAddress.FullSiteStreetAddress.ToUpper(), m_DisplayFont, br, (iRes * iHorizontalDistance + horizontalOffset), (iColNr * iVerticalDistance) + 73); 
					g.DrawString(pi.Owner.MailAddress.SiteCitySiteStateSiteZIP.ToUpper(), m_DisplayFont, br, (iRes * iHorizontalDistance + horizontalOffset), (iColNr * iVerticalDistance) + 93); 
				}
				//else if (null != pi.Owner && null != pi.Owner.SiteAddress)
				else if (
						null != pi.Owner && 
						null != pi.Owner.SiteAddress &&
						(
						string.Empty != pi.Owner.SiteAddress.FullSiteStreetAddress.Trim() ||
						string.Empty != pi.Owner.SiteAddress.SiteCitySiteStateSiteZIP.Trim()
						)
					)
				{
					g.DrawString(pi.Owner.SiteAddress.FullSiteStreetAddress.ToUpper(), m_DisplayFont, br, (iRes * iHorizontalDistance + horizontalOffset), (iColNr * iVerticalDistance) + 73); 
					g.DrawString(pi.Owner.SiteAddress.SiteCitySiteStateSiteZIP.ToUpper(), m_DisplayFont, br, (iRes * iHorizontalDistance + horizontalOffset), (iColNr * iVerticalDistance) + 93); 
				}
				else
				{
					g.DrawString(string.Empty, m_DisplayFont, br, (iRes * iHorizontalDistance + horizontalOffset), (iColNr * iVerticalDistance) + 73); 
					g.DrawString(string.Empty, m_DisplayFont, br, (iRes * iHorizontalDistance + horizontalOffset), (iColNr * iVerticalDistance) + 93); 
				}


				//StringFormat drawFormat = new StringFormat();
				//drawFormat.FormatFlags = StringFormatFlags.DirectionVertical;
				//g.DrawString("*" + pi.IdPropertyItem.ToString().Substring(0, 6) + "*", m_BarcodeFont, br, (iRes * iHorizontalDistance + 15), (iColNr * iVerticalDistance) +  42, drawFormat); 
				iCnt++;
				iLocalCnt++;

				if (iCnt % 30 == 0)
				{
					Globals.mediumLabelsCount += 30;
					e.HasMorePages = true;

				}

			}

			return g;
		}
		#endregion � GetMediumLabelGraphics �
		#region � GetSmallLabelGraphics �
		public static Graphics GetSmallLabelGraphics (PrintPageEventArgs e, ArrayList alSelectedRecords)
		{
			Font m_BarcodeFont = new Font("IDAutomationHC39M", 8);
			Font m_DisplayFont = new Font("Tahoma", 6);
			Graphics g = e.Graphics;			

			Brush br = new SolidBrush(Color.Black);				

			int iCnt  = Globals.smallLabelsCount;
			int iLocalCnt = 0;
			int iCols = 2;
			int iColNr = -1;

			int horizontalOffset = 200;
			int horizontalBarcodeOffset = 50;
			
			int iHorizontalDistance = 400;
			int iVerticalDistance = 50;


			while (iLocalCnt < 40 && iCnt < alSelectedRecords.Count)
			{
				Guid propertyItemId = (Guid)alSelectedRecords[iCnt];
				PropertyItem pi = ApplicationHelper.LoadPropertyItem(propertyItemId);
			
				int iRes = (int)(iCnt % iCols);
				if (0 == iRes)
					iColNr++;				

				if (null != pi.Owner)
				{
					if (string.Empty != pi.Owner.FullName.Trim())
						g.DrawString(pi.Owner.FullName.ToUpper(), m_DisplayFont, br, (iRes * iHorizontalDistance + horizontalOffset + 20), (iColNr * iVerticalDistance) +  42); 
					else
						g.DrawString(UserMessages.CurrentResident, m_DisplayFont, br, (iRes * iHorizontalDistance + horizontalOffset + 20), (iColNr * iVerticalDistance) +  42); 
				}
				else
					g.DrawString(string.Empty, m_DisplayFont, br, (iRes * iHorizontalDistance + horizontalOffset + 20), (iColNr * iVerticalDistance) +  42); 

				//if (null != pi.Owner && null != pi.Owner.MailAddress)
				if (
						null != pi.Owner && 
						null != pi.Owner.MailAddress && 
						(
						string.Empty != pi.Owner.MailAddress.FullSiteStreetAddress.Trim() ||
						string.Empty != pi.Owner.MailAddress.SiteCitySiteStateSiteZIP.Trim()
						)
					)
				{
					g.DrawString(pi.Owner.MailAddress.FullSiteStreetAddress.ToUpper(), m_DisplayFont, br, (iRes * iHorizontalDistance + horizontalOffset + 20), (iColNr * iVerticalDistance) + 57); 
					g.DrawString(pi.Owner.MailAddress.SiteCitySiteStateSiteZIP.ToUpper(), m_DisplayFont, br, (iRes * iHorizontalDistance + horizontalOffset + 20), (iColNr * iVerticalDistance) + 72); 
				}
				//else if (null != pi.Owner && null != pi.Owner.SiteAddress)
				if (
						null != pi.Owner && 
						null != pi.Owner.SiteAddress &&
						(
						string.Empty != pi.Owner.SiteAddress.FullSiteStreetAddress.Trim() ||
						string.Empty != pi.Owner.SiteAddress.SiteCitySiteStateSiteZIP.Trim()
						)
					)
				{
					g.DrawString(pi.Owner.SiteAddress.FullSiteStreetAddress.ToUpper(), m_DisplayFont, br, (iRes * iHorizontalDistance + horizontalOffset + 20), (iColNr * iVerticalDistance) + 57); 
					g.DrawString(pi.Owner.SiteAddress.SiteCitySiteStateSiteZIP.ToUpper(), m_DisplayFont, br, (iRes * iHorizontalDistance + horizontalOffset + 20), (iColNr * iVerticalDistance) + 72); 
				}				
				else
				{
					g.DrawString(string.Empty, m_DisplayFont, br, (iRes * iHorizontalDistance + horizontalOffset + 20), (iColNr * iVerticalDistance) + 57); 
					g.DrawString(string.Empty, m_DisplayFont, br, (iRes * iHorizontalDistance + horizontalOffset + 20), (iColNr * iVerticalDistance) + 72); 
				}

				g.DrawString("*" + pi.IdPropertyItem.ToString().Substring(0, 6) + "*", m_BarcodeFont, br, (iRes * iHorizontalDistance + horizontalBarcodeOffset), (iColNr * iVerticalDistance) +  38); 

				iCnt++;
				iLocalCnt++;

				if (iCnt % 40 == 0)
				{
					Globals.smallLabelsCount += 40;
					e.HasMorePages = true;
				}
			}
			return g;
		}
		#endregion � GetSmallLabelGraphics �

		*/
		#endregion � OLD GRAPHICS �

		static bool SplitLine(Graphics gfx, string fullName, string firstName, string middleName, string lastName, bool searchForMultipleOwners, Font font, int maxWidth, out string upperLine, out string lowerLine)
		{			
			upperLine = string.Empty;
			lowerLine = string.Empty;
											  
			if (searchForMultipleOwners)
			{				
				int idx = fullName.IndexOf("/");

				if (-1 != idx)
				{					
					upperLine = fullName.Substring(0, idx).Trim();
					lowerLine = fullName.Substring(idx + 1, fullName.Length - (idx + 1)).Trim();
					return true;
				}
			}

			if ((int)(gfx.MeasureString(fullName, font).Width) > maxWidth)
			{
				int wordCnt = 0;
				string[] words = fullName.Split(' ');
				
				while (wordCnt < words.Length && (int)(gfx.MeasureString(upperLine, font).Width) + (int)(gfx.MeasureString(words[wordCnt], font).Width) < maxWidth)
				{
					if (0 != wordCnt)
						upperLine += " ";

					upperLine += words[wordCnt];
					
					wordCnt++;
				}

				while (wordCnt < words.Length)
				{
					if (0 != wordCnt)
						lowerLine += " ";

					lowerLine += words[wordCnt];					
					wordCnt++;
				}

				upperLine = upperLine.Trim();
				lowerLine = lowerLine.Trim();

				return true;
			}

			return false;
		}

		#region � GetLargeLabelGraphics �
		public static Graphics GetLargeLabelGraphics (PrintPageEventArgs e, PropertyItem dummy,  ArrayList alSelectedRecords, int horizontalOffsetToAdd, int verticalOffsetToAdd, MFW_PrintDescriptor pd)
		{
			#region COMMENTED OUT 
			/*Font m_BarcodeFont = new Font("3 of 9 Barcode", 14);
			Font m_DisplayFont = new Font("Tahoma", 10);
			Graphics g = e.Graphics;			

			Brush br = new SolidBrush(Color.Black);				

			int iCnt  = Globals.largeLabelsCount;
			int iLocalCnt = 0;
			int iCols = 2;
			int iColNr = -1;

			int horizontalOffset = 150;
			int horizontalBarcodeOffset = 75;
			
			int iHorizontalDistance = 400;
			int iVerticalDistance = 225;


			while (iLocalCnt < 10 && iCnt < alSelectedRecords.Count)
			{
				Guid propertyItemId = (Guid)alSelectedRecords[iCnt];
				PropertyItem pi = ApplicationHelper.LoadPropertyItem(propertyItemId);
			
				int iRes = (int)(iCnt % iCols);
				if (0 == iRes)
					iColNr++;

				if (null != pi.Owner)
				{
					if (string.Empty != pi.Owner.FullName.Trim())
						g.DrawString(pi.Owner.FullName.ToUpper(), m_DisplayFont, br, (iRes * iHorizontalDistance + horizontalOffset), (iColNr * iVerticalDistance) +  70); 
					else
						g.DrawString(UserMessages.Homeowner, m_DisplayFont, br, (iRes * iHorizontalDistance + horizontalOffset), (iColNr * iVerticalDistance) +  70); 
				}
				else
					g.DrawString(string.Empty, m_DisplayFont, br, (iRes * iHorizontalDistance + horizontalOffset), (iColNr * iVerticalDistance) +  70); 
				
				if (null != pi.Owner && null != pi.Owner.SiteAddress)
				{
					g.DrawString(pi.Owner.SiteAddress.FullSiteStreetAddress.ToUpper(), m_DisplayFont, br, (iRes * iHorizontalDistance + horizontalOffset), (iColNr * iVerticalDistance) + 90); 
					g.DrawString(pi.Owner.SiteAddress.SiteCitySiteStateSiteZIP.ToUpper(), m_DisplayFont, br, (iRes * iHorizontalDistance + horizontalOffset), (iColNr * iVerticalDistance) + 110); 
				}
				else if (null != pi.Owner && null != pi.Owner.MailAddress)
				{
					g.DrawString(pi.Owner.MailAddress.FullSiteStreetAddress.ToUpper(), m_DisplayFont, br, (iRes * iHorizontalDistance + horizontalOffset), (iColNr * iVerticalDistance) + 90); 
					g.DrawString(pi.Owner.MailAddress.SiteCitySiteStateSiteZIP.ToUpper(), m_DisplayFont, br, (iRes * iHorizontalDistance + horizontalOffset), (iColNr * iVerticalDistance) + 110); 
				}
				else
				{
					g.DrawString(string.Empty, m_DisplayFont, br, (iRes * iHorizontalDistance + horizontalOffset), (iColNr * iVerticalDistance) + 90); 
					g.DrawString(string.Empty, m_DisplayFont, br, (iRes * iHorizontalDistance + horizontalOffset), (iColNr * iVerticalDistance) + 110); 
				}


				StringFormat drawFormat = new StringFormat();
				drawFormat.FormatFlags = StringFormatFlags.DirectionVertical;				
				g.DrawString(pi.IdPropertyItem.ToString().Substring(0, 8), m_BarcodeFont, br, (iRes * iHorizontalDistance + horizontalBarcodeOffset), (iColNr * iVerticalDistance) +  70, drawFormat); 
				//temp string
				g.DrawString(pi.IdPropertyItem.ToString().Substring(0, 8), m_DisplayFont, br, (iRes * iHorizontalDistance + horizontalBarcodeOffset + 30), (iColNr * iVerticalDistance) +  70, drawFormat); 				
				iCnt++;
				iLocalCnt++;

				if (iCnt % 10 == 0)
				{
					Globals.largeLabelsCount += 10;
					e.HasMorePages = true;

				}

			}

			return g;*/
			#endregion COMMENTED OUT

			verticalOffsetToAdd *= 4;
			horizontalOffsetToAdd *= 4;

			Font m_BarcodeFont = new Font("IDAutomationHC39M", 6);
			Font m_DisplayFont = new Font("Tahoma", 8);
			Graphics g = e.Graphics;			

			Brush br = new SolidBrush(Color.Black);				

			int iCnt  = Globals.mediumLabelsCount;
			int iLocalCnt = 0;
			int iCols = 3;
			int iColNr = -1;

			int horizontalOffset = 60 + horizontalOffsetToAdd;	//72 --> 60
			verticalOffsetToAdd *= 2;
			//int horizontalBarcodeOffset = 75;
			
			int iHorizontalDistance = 275;
			int iVerticalDistance = 100;

			if (null != dummy && null == alSelectedRecords)
			{
				int iRes = (int)(iCnt % iCols);
				if (0 == iRes)
					iColNr++;

				if (null != dummy.Owner)
				{
					if (string.Empty != dummy.Owner.FullName.Trim())
						g.DrawString(dummy.Owner.FullName.ToUpper(), m_DisplayFont, br, (iRes * iHorizontalDistance + horizontalOffset), (iColNr * iVerticalDistance) +  45 + verticalOffsetToAdd); 
					else
						g.DrawString(UserMessages.CurrentResident, m_DisplayFont, br, (iRes * iHorizontalDistance + horizontalOffset), (iColNr * iVerticalDistance) +  45 + verticalOffsetToAdd); 
				}
				else
					g.DrawString(string.Empty, m_DisplayFont, br, (iRes * iHorizontalDistance + horizontalOffset), (iColNr * iVerticalDistance) +  40 + verticalOffsetToAdd); 


				if (null != dummy.Owner && null != dummy.Owner.MailAddress)
				{
					g.DrawString(dummy.Owner.MailAddress.FullSiteStreetAddress.ToUpper(), m_DisplayFont, br, (iRes * iHorizontalDistance + horizontalOffset), (iColNr * iVerticalDistance) + 65 + verticalOffsetToAdd); 
					g.DrawString(dummy.Owner.MailAddress.SiteCitySiteStateSiteZIP.ToUpper(), m_DisplayFont, br, (iRes * iHorizontalDistance + horizontalOffset), (iColNr * iVerticalDistance) + 85 + verticalOffsetToAdd); 
				}
				else if (null != dummy.Owner && null != dummy.Owner.SiteAddress)
				{
					g.DrawString(dummy.Owner.SiteAddress.FullSiteStreetAddress.ToUpper(), m_DisplayFont, br, (iRes * iHorizontalDistance + horizontalOffset), (iColNr * iVerticalDistance) + 65 + verticalOffsetToAdd); 
					g.DrawString(dummy.Owner.SiteAddress.SiteCitySiteStateSiteZIP.ToUpper(), m_DisplayFont, br, (iRes * iHorizontalDistance + horizontalOffset), (iColNr * iVerticalDistance) + 85 + verticalOffsetToAdd); 
				}
				else
				{
					g.DrawString(string.Empty, m_DisplayFont, br, (iRes * iHorizontalDistance + horizontalOffset), (iColNr * iVerticalDistance) + 65 + verticalOffsetToAdd); 
					g.DrawString(string.Empty, m_DisplayFont, br, (iRes * iHorizontalDistance + horizontalOffset), (iColNr * iVerticalDistance) + 85 + verticalOffsetToAdd); 
				}


				StringFormat drawFormat = new StringFormat();
				drawFormat.FormatFlags = StringFormatFlags.DirectionVertical;
				g.DrawString("*" + dummy.IdPropertyItem.ToString().Substring(0, 6) + "*", m_BarcodeFont, br, (iRes * iHorizontalDistance + horizontalOffsetToAdd + 12), (iColNr * iVerticalDistance) +  32 + verticalOffsetToAdd, drawFormat); 
		
				return g;
			}

		


			while (iLocalCnt < 30 && iCnt < alSelectedRecords.Count)
			{
				PropertyItem pi = null;
				bool dummyData = false;
				if (alSelectedRecords[iCnt] is Guid)
				{
					Guid propertyItemId = (Guid)alSelectedRecords[iCnt];
					pi = ApplicationHelper.LoadPropertyItem(propertyItemId);
				}
				else
				{
					pi = new PropertyItem();
					dummyData = true;
				}
			
				int iRes = (int)(iCnt % iCols);
				if (0 == iRes)
					iColNr++;

				if (null != pi.Owner)
				{
					if (string.Empty != pi.Owner.FullName.Trim())
					{
						string upperLine = string.Empty;
						string lowerLine = string.Empty;
						if (SplitLine(e.Graphics, pi.Owner.FullName.Trim(), pi.Owner.FirstName, pi.Owner.MiddleName, pi.Owner.LastName, true, m_DisplayFont, 180, out upperLine, out lowerLine))
						{
							g.DrawString(upperLine, m_DisplayFont, br, (iRes * iHorizontalDistance + horizontalOffset), (iColNr * iVerticalDistance) +  30 + verticalOffsetToAdd); 
							g.DrawString(lowerLine, m_DisplayFont, br, (iRes * iHorizontalDistance + horizontalOffset), (iColNr * iVerticalDistance) +  50 + verticalOffsetToAdd); 
						}
						else
							g.DrawString(pi.Owner.FullName.ToUpper(), m_DisplayFont, br, (iRes * iHorizontalDistance + horizontalOffset), (iColNr * iVerticalDistance) +  40 + verticalOffsetToAdd); 
					}
					else
					{
						g.DrawString(UserMessages.CurrentResident, m_DisplayFont, br, (iRes * iHorizontalDistance + horizontalOffset), (iColNr * iVerticalDistance) +  40 + verticalOffsetToAdd); 						
					}
				}
				else
					g.DrawString(string.Empty, m_DisplayFont, br, (iRes * iHorizontalDistance + horizontalOffset), (iColNr * iVerticalDistance) +  40 + verticalOffsetToAdd); 


				if (null != pi.Owner && null != pi.Owner.MailAddress && 
					(	pi.Owner.MailAddress.FullSiteStreetAddress.Trim() != string.Empty ||
						pi.Owner.MailAddress.SiteCitySiteStateSiteZIP.Trim() != string.Empty)
					)
				{
					g.DrawString(pi.Owner.MailAddress.FullSiteStreetAddress.ToUpper(), m_DisplayFont, br, (iRes * iHorizontalDistance + horizontalOffset), (iColNr * iVerticalDistance) + 70 + verticalOffsetToAdd); 
					g.DrawString(pi.Owner.MailAddress.SiteCitySiteStateSiteZIP.ToUpper(), m_DisplayFont, br, (iRes * iHorizontalDistance + horizontalOffset), (iColNr * iVerticalDistance) + 90 + verticalOffsetToAdd); 
				}
				else if (null != pi.Owner && null != pi.Owner.SiteAddress)
				{
					g.DrawString(pi.Owner.SiteAddress.FullSiteStreetAddress.ToUpper(), m_DisplayFont, br, (iRes * iHorizontalDistance + horizontalOffset), (iColNr * iVerticalDistance) + 70 + verticalOffsetToAdd); 
					g.DrawString(pi.Owner.SiteAddress.SiteCitySiteStateSiteZIP.ToUpper(), m_DisplayFont, br, (iRes * iHorizontalDistance + horizontalOffset), (iColNr * iVerticalDistance) + 90 + verticalOffsetToAdd); 
				}
				else
				{
					g.DrawString(string.Empty, m_DisplayFont, br, (iRes * iHorizontalDistance + horizontalOffset), (iColNr * iVerticalDistance) + 70 + verticalOffsetToAdd); 
					g.DrawString(string.Empty, m_DisplayFont, br, (iRes * iHorizontalDistance + horizontalOffset), (iColNr * iVerticalDistance) + 90 + verticalOffsetToAdd); 
				}


				StringFormat drawFormat = new StringFormat();
				drawFormat.FormatFlags = StringFormatFlags.DirectionVertical;
				if (false == dummyData)
				{
					g.DrawString("*" + pi.IdPropertyItem.ToString().Substring(0, 6) + "*", m_BarcodeFont, br, (iRes * iHorizontalDistance + 12 + horizontalOffsetToAdd), (iColNr * iVerticalDistance) +  32 + verticalOffsetToAdd, drawFormat); 
				}
				iCnt++;
				iLocalCnt++;

				if (iCnt % 30 == 0 && iCnt < alSelectedRecords.Count)
				{
					Globals.mediumLabelsCount += 30;
					e.HasMorePages = true;

				}

			}

			return g;
		}
		#endregion � GetLargeLabelGraphics �
		#region � GetMediumLabelGraphics �
		public static Graphics GetMediumLabelGraphics (PrintPageEventArgs e, PropertyItem dummy, ArrayList alSelectedRecords, int horizontalOffsetToAdd, int verticalOffsetToAdd, MFW_PrintDescriptor pd)
		{
			#region  COMMENTED OUT 
			/*
			Font m_BarcodeFont = new Font("IDAutomationHC39M", 10);
			Font m_DisplayFont = new Font("Tahoma", 10);
			Graphics g = e.Graphics;			

			Brush br = new SolidBrush(Color.Black);				

			int iCnt  = Globals.mediumLabelsCount;
			int iLocalCnt = 0;
			int iCols = 2;
			int iColNr = -1;

			int horizontalOffset = 150;
			//int horizontalBarcodeOffset = 75;
			
			int iHorizontalDistance = 400;
			int iVerticalDistance = 115;


			while (iLocalCnt < 18 && iCnt < alSelectedRecords.Count)
			{
				Guid propertyItemId = (Guid)alSelectedRecords[iCnt];
				PropertyItem pi = ApplicationHelper.LoadPropertyItem(propertyItemId);
			
				int iRes = (int)(iCnt % iCols);
				if (0 == iRes)
					iColNr++;

				if (null != pi.Owner)
				{
					if (string.Empty != pi.Owner.FullName.Trim())
						g.DrawString(pi.Owner.FullName.ToUpper(), m_DisplayFont, br, (iRes * iHorizontalDistance + horizontalOffset), (iColNr * iVerticalDistance) +  40); 
					else
						g.DrawString(UserMessages.CurrentResident, m_DisplayFont, br, (iRes * iHorizontalDistance + horizontalOffset), (iColNr * iVerticalDistance) +  40); 
				}
				else
					g.DrawString(string.Empty, m_DisplayFont, br, (iRes * iHorizontalDistance + horizontalOffset), (iColNr * iVerticalDistance) +  40); 

				if (null != pi.Owner && null != pi.Owner.MailAddress)
				{
					g.DrawString(pi.Owner.MailAddress.FullSiteStreetAddress.ToUpper(), m_DisplayFont, br, (iRes * iHorizontalDistance + horizontalOffset), (iColNr * iVerticalDistance) + 60); 
					g.DrawString(pi.Owner.MailAddress.SiteCitySiteStateSiteZIP.ToUpper(), m_DisplayFont, br, (iRes * iHorizontalDistance + horizontalOffset), (iColNr * iVerticalDistance) + 80); 
				}
				else if (null != pi.Owner && null != pi.Owner.SiteAddress)
				{
					g.DrawString(pi.Owner.SiteAddress.FullSiteStreetAddress.ToUpper(), m_DisplayFont, br, (iRes * iHorizontalDistance + horizontalOffset), (iColNr * iVerticalDistance) + 60); 
					g.DrawString(pi.Owner.SiteAddress.SiteCitySiteStateSiteZIP.ToUpper(), m_DisplayFont, br, (iRes * iHorizontalDistance + horizontalOffset), (iColNr * iVerticalDistance) + 80); 
				}				
				else
				{
					g.DrawString(string.Empty, m_DisplayFont, br, (iRes * iHorizontalDistance + horizontalOffset), (iColNr * iVerticalDistance) + 60); 
					g.DrawString(string.Empty, m_DisplayFont, br, (iRes * iHorizontalDistance + horizontalOffset), (iColNr * iVerticalDistance) + 80); 
				}

				StringFormat drawFormat = new StringFormat();
				drawFormat.FormatFlags = StringFormatFlags.DirectionVertical;
				//g.DrawString("*" + pi.IdPropertyItem.ToString().Substring(0, 8) + "*", m_BarcodeFont, br, (iRes * iHorizontalDistance + horizontalBarcodeOffset), (iColNr * iVerticalDistance) +  40, drawFormat); 
				iCnt++;
				iLocalCnt++;

				if (iCnt % 18 == 0)
				{
					Globals.mediumLabelsCount += 18;
					e.HasMorePages = true;

				}

			}

			return g;
			*/
			#endregion  COMMENTED OUT 

			verticalOffsetToAdd *= 4;
			horizontalOffsetToAdd *= 4;			
			
			//Font m_BarcodeFont = new Font("IDAutomationHC39M", 6);
			Font m_DisplayFont = new Font("Tahoma", 9);
			Graphics g = e.Graphics;			

			Brush br = new SolidBrush(Color.Black);				

			int iCnt  = Globals.mediumLabelsCount;
			int iLocalCnt = 0;
			int iCols = 3;
			int iColNr = -1;
						

			int horizontalOffset = 15 + horizontalOffsetToAdd;
			//int horizontalBarcodeOffset = 75;
			
			int iHorizontalDistance = 275;
			int iVerticalDistance = 100;

			if (null != dummy && null == alSelectedRecords)
			{

				int iRes = (int)(iCnt % iCols);
				if (0 == iRes)
					iColNr++;

				if (null != dummy.Owner)
				{
					if (string.Empty != dummy.Owner.FullName.Trim())
						g.DrawString(dummy.Owner.FullName.ToUpper(), m_DisplayFont, br, (iRes * iHorizontalDistance + horizontalOffset), (iColNr * iVerticalDistance) +  45 + verticalOffsetToAdd); 
					else
						g.DrawString(UserMessages.CurrentResident, m_DisplayFont, br, (iRes * iHorizontalDistance + horizontalOffset), (iColNr * iVerticalDistance) +  45 + verticalOffsetToAdd); 
				}
				else
					g.DrawString(string.Empty, m_DisplayFont, br, (iRes * iHorizontalDistance + horizontalOffset), (iColNr * iVerticalDistance) +  40 + verticalOffsetToAdd); 


				if (null != dummy.Owner && null != dummy.Owner.MailAddress)
				{
					g.DrawString(dummy.Owner.MailAddress.FullSiteStreetAddress.ToUpper(), m_DisplayFont, br, (iRes * iHorizontalDistance + horizontalOffset), (iColNr * iVerticalDistance) + 65 + verticalOffsetToAdd); 
					g.DrawString(dummy.Owner.MailAddress.SiteCitySiteStateSiteZIP.ToUpper(), m_DisplayFont, br, (iRes * iHorizontalDistance + horizontalOffset), (iColNr * iVerticalDistance) + 85 + verticalOffsetToAdd); 
				}
				else if (null != dummy.Owner && null != dummy.Owner.SiteAddress)
				{
					g.DrawString(dummy.Owner.SiteAddress.FullSiteStreetAddress.ToUpper(), m_DisplayFont, br, (iRes * iHorizontalDistance + horizontalOffset), (iColNr * iVerticalDistance) + 65 + verticalOffsetToAdd); 
					g.DrawString(dummy.Owner.SiteAddress.SiteCitySiteStateSiteZIP.ToUpper(), m_DisplayFont, br, (iRes * iHorizontalDistance + horizontalOffset), (iColNr * iVerticalDistance) + 85 + verticalOffsetToAdd); 
				}
				else
				{
					g.DrawString(string.Empty, m_DisplayFont, br, (iRes * iHorizontalDistance + horizontalOffset), (iColNr * iVerticalDistance) + 65 + verticalOffsetToAdd); 
					g.DrawString(string.Empty, m_DisplayFont, br, (iRes * iHorizontalDistance + horizontalOffset), (iColNr * iVerticalDistance) + 85 + verticalOffsetToAdd); 
				}
				return g;
			}
						
			while (iLocalCnt < 30 && iCnt < alSelectedRecords.Count)			
			{																
				PropertyItem pi = null;				
				if (alSelectedRecords[iCnt] is Guid)
				{
					Guid propertyItemId = (Guid)alSelectedRecords[iCnt];
					pi = ApplicationHelper.LoadPropertyItem(propertyItemId);
				}
				else
				{
					pi = new PropertyItem();					
				}								
		
				int iRes = (int)(iCnt % iCols);				
				if (0 == iRes)
					iColNr++;

				if (null != pi.Owner)
				{
					//OLD
					/*
					if (string.Empty != pi.Owner.FullName.Trim())
						g.DrawString(pi.Owner.FullName.ToUpper(), m_DisplayFont, br, (iRes * iHorizontalDistance + horizontalOffset), (iColNr * iVerticalDistance) +  45 + verticalOffsetToAdd); 
					else
						g.DrawString(UserMessages.CurrentResident, m_DisplayFont, br, (iRes * iHorizontalDistance + horizontalOffset), (iColNr * iVerticalDistance) +  45 + verticalOffsetToAdd); 
					*/

					//NEW
					string upperLine = string.Empty;
					string lowerLine = string.Empty;
					if (string.Empty != pi.Owner.FullName.Trim())
					{
						if (SplitLine(e.Graphics, pi.Owner.FullName.ToUpper().Trim(), pi.Owner.FirstName, pi.Owner.MiddleName, pi.Owner.LastName, true, m_DisplayFont, 180, out upperLine, out lowerLine))
						{
							g.DrawString(upperLine, m_DisplayFont, Brushes.Black, (iRes * iHorizontalDistance + horizontalOffset), (iColNr * iVerticalDistance) +  30 + verticalOffsetToAdd); 
							g.DrawString(lowerLine, m_DisplayFont, Brushes.Black, (iRes * iHorizontalDistance + horizontalOffset), (iColNr * iVerticalDistance) +  50 + verticalOffsetToAdd); 
						}
						else
							g.DrawString(pi.Owner.FullName.ToUpper(), m_DisplayFont, Brushes.Black, (iRes * iHorizontalDistance + horizontalOffset), (iColNr * iVerticalDistance) +  40 + verticalOffsetToAdd); 
					}
					else
						g.DrawString(UserMessages.CurrentResident, m_DisplayFont, Brushes.Black, (iRes * iHorizontalDistance + horizontalOffset), (iColNr * iVerticalDistance) +  40 + verticalOffsetToAdd); 

				}
				else
					g.DrawString(string.Empty, m_DisplayFont, br, (iRes * iHorizontalDistance + horizontalOffset), (iColNr * iVerticalDistance) +  40 + verticalOffsetToAdd); 


				if (null != pi.Owner && null != pi.Owner.MailAddress &&
					(	pi.Owner.MailAddress.FullSiteStreetAddress.Trim() != string.Empty ||
					pi.Owner.MailAddress.SiteCitySiteStateSiteZIP.Trim() != string.Empty)
					)
				{
					g.DrawString(pi.Owner.MailAddress.FullSiteStreetAddress.ToUpper(), m_DisplayFont, br, (iRes * iHorizontalDistance + horizontalOffset), (iColNr * iVerticalDistance) + 65 + verticalOffsetToAdd); 
					g.DrawString(pi.Owner.MailAddress.SiteCitySiteStateSiteZIP.ToUpper(), m_DisplayFont, br, (iRes * iHorizontalDistance + horizontalOffset), (iColNr * iVerticalDistance) + 85 + verticalOffsetToAdd); 
				}
				else if (null != pi.Owner && null != pi.Owner.SiteAddress)
				{
					g.DrawString(pi.Owner.SiteAddress.FullSiteStreetAddress.ToUpper(), m_DisplayFont, br, (iRes * iHorizontalDistance + horizontalOffset), (iColNr * iVerticalDistance) + 65 + verticalOffsetToAdd); 
					g.DrawString(pi.Owner.SiteAddress.SiteCitySiteStateSiteZIP.ToUpper(), m_DisplayFont, br, (iRes * iHorizontalDistance + horizontalOffset), (iColNr * iVerticalDistance) + 85 + verticalOffsetToAdd); 
				}
				else
				{
					g.DrawString(string.Empty, m_DisplayFont, br, (iRes * iHorizontalDistance + horizontalOffset), (iColNr * iVerticalDistance) + 65 + verticalOffsetToAdd); 
					g.DrawString(string.Empty, m_DisplayFont, br, (iRes * iHorizontalDistance + horizontalOffset), (iColNr * iVerticalDistance) + 85 + verticalOffsetToAdd); 
				}
			
				iCnt++;
				iLocalCnt++;
												
				if (iCnt % 30 == 0 && iCnt < alSelectedRecords.Count)																																	
				{
					Globals.mediumLabelsCount += 30;
					e.HasMorePages = true;

				}

			}

			return g;
		}
		#endregion � GetMediumLabelGraphics �
		#region � GetSmallLabelGraphics �
		public static Graphics GetSmallLabelGraphics (PrintPageEventArgs e, PropertyItem dummy, ArrayList alSelectedRecords, int horizontalOffsetToAdd, int verticalOffsetToAdd, MFW_PrintDescriptor pd)
		{
			verticalOffsetToAdd *= 4;
			horizontalOffsetToAdd *= 4;
			
			Font m_BarcodeFont = new Font("IDAutomationHC39M", 8);
			Font m_DisplayFont = new Font("Tahoma", 6);
			Graphics g = e.Graphics;			

			Brush br = new SolidBrush(Color.Black);				

			int iCnt  = Globals.smallLabelsCount;
			int iLocalCnt = 0;
			int iCols = 2;
			int iColNr = -1;

			int horizontalOffset = 200 + horizontalOffsetToAdd;
			int horizontalBarcodeOffset = 50;
			
			int iHorizontalDistance = 400;
			int iVerticalDistance = 50;

			if (null != dummy && null == alSelectedRecords)
			{
				int iRes = (int)(iCnt % iCols);
				if (0 == iRes)
					iColNr++;				

				if (null != dummy.Owner)
				{
					if (string.Empty != dummy.Owner.FullName.Trim())
						g.DrawString(dummy.Owner.FullName.ToUpper(), m_DisplayFont, Brushes.Red, (iRes * iHorizontalDistance + horizontalOffset + 20), (iColNr * iVerticalDistance) +  34 + verticalOffsetToAdd); 
					else
						g.DrawString(UserMessages.CurrentResident, m_DisplayFont, Brushes.Blue, (iRes * iHorizontalDistance + horizontalOffset + 20), (iColNr * iVerticalDistance) +  34 + verticalOffsetToAdd); 
				}
				else
					g.DrawString(string.Empty, m_DisplayFont, br, (iRes * iHorizontalDistance + horizontalOffset + 20), (iColNr * iVerticalDistance) +  34 + verticalOffsetToAdd); 

				if (null != dummy.Owner && null != dummy.Owner.MailAddress)
				{
					g.DrawString(dummy.Owner.MailAddress.FullSiteStreetAddress.ToUpper(), m_DisplayFont, br, (iRes * iHorizontalDistance + horizontalOffset + 20), (iColNr * iVerticalDistance) + 49 + verticalOffsetToAdd); 
					g.DrawString(dummy.Owner.MailAddress.SiteCitySiteStateSiteZIP.ToUpper(), m_DisplayFont, br, (iRes * iHorizontalDistance + horizontalOffset + 20), (iColNr * iVerticalDistance) + 64 + verticalOffsetToAdd); 
				}
				else if (null != dummy.Owner && null != dummy.Owner.SiteAddress)
				{
					g.DrawString(dummy.Owner.SiteAddress.FullSiteStreetAddress.ToUpper(), m_DisplayFont, br, (iRes * iHorizontalDistance + horizontalOffset + 20), (iColNr * iVerticalDistance) + 49 + verticalOffsetToAdd); 
					g.DrawString(dummy.Owner.SiteAddress.SiteCitySiteStateSiteZIP.ToUpper(), m_DisplayFont, br, (iRes * iHorizontalDistance + horizontalOffset + 20), (iColNr * iVerticalDistance) + 64 + verticalOffsetToAdd); 
				}				
				else
				{
					g.DrawString(string.Empty, m_DisplayFont, br, (iRes * iHorizontalDistance + horizontalOffset + 20), (iColNr * iVerticalDistance) + 49 + verticalOffsetToAdd); 
					g.DrawString(string.Empty, m_DisplayFont, br, (iRes * iHorizontalDistance + horizontalOffset + 20), (iColNr * iVerticalDistance) + 64 + verticalOffsetToAdd); 
				}

				g.DrawString("*" + dummy.IdPropertyItem.ToString().Substring(0, 6) + "*", m_BarcodeFont, br, (iRes * iHorizontalDistance + horizontalBarcodeOffset + horizontalOffsetToAdd), (iColNr * iVerticalDistance) +  30 + verticalOffsetToAdd); 

				return g;
			}

			while (iLocalCnt < 40 && iCnt < alSelectedRecords.Count)
			{
				PropertyItem pi = null;
				bool dummyData = false;
				if (alSelectedRecords[iCnt] is Guid)
				{
					Guid propertyItemId = (Guid)alSelectedRecords[iCnt];
					pi = ApplicationHelper.LoadPropertyItem(propertyItemId);
				}
				else
				{
					pi = new PropertyItem();
					dummyData = true;
				}
			
				int iRes = (int)(iCnt % iCols);
				if (0 == iRes)
					iColNr++;				

				if (null != pi.Owner)
				{
					//OLD 
					/*
					if (string.Empty != pi.Owner.FullName.Trim())
						g.DrawString(pi.Owner.FullName.ToUpper(), m_DisplayFont, Brushes.Gray, (iRes * iHorizontalDistance + horizontalOffset + 20), (iColNr * iVerticalDistance) +  34 + verticalOffsetToAdd); 
					else
						g.DrawString(UserMessages.CurrentResident, m_DisplayFont, Brushes.Gray, (iRes * iHorizontalDistance + horizontalOffset + 20), (iColNr * iVerticalDistance) +  34 + verticalOffsetToAdd); 
					*/

					//NEW
					string upperLine = string.Empty;
					string lowerLine = string.Empty;

					if (string.Empty != pi.Owner.FullName.Trim())
					{
						if (SplitLine(e.Graphics, pi.Owner.FullName.ToUpper().Trim(), pi.Owner.FirstName, pi.Owner.MiddleName, pi.Owner.LastName, true, m_DisplayFont, 130, out upperLine, out lowerLine))
						{
							g.DrawString(upperLine, m_DisplayFont, Brushes.Black, (iRes * iHorizontalDistance + horizontalOffset + 20), (iColNr * iVerticalDistance) +  27 + verticalOffsetToAdd); 
							g.DrawString(lowerLine, m_DisplayFont, Brushes.Black, (iRes * iHorizontalDistance + horizontalOffset + 20), (iColNr * iVerticalDistance) +  40 + verticalOffsetToAdd); 
						}
						else
							g.DrawString(pi.Owner.FullName.ToUpper(), m_DisplayFont, Brushes.Black, (iRes * iHorizontalDistance + horizontalOffset + 20), (iColNr * iVerticalDistance) +  35 + verticalOffsetToAdd); 
					}
					else
						g.DrawString(UserMessages.CurrentResident, m_DisplayFont, Brushes.Black, (iRes * iHorizontalDistance + horizontalOffset + 20), (iColNr * iVerticalDistance) +  35 + verticalOffsetToAdd); 
				}
				else
					g.DrawString(string.Empty, m_DisplayFont, br, (iRes * iHorizontalDistance + horizontalOffset + 20), (iColNr * iVerticalDistance) +  34 + verticalOffsetToAdd); 

				if (null != pi.Owner && null != pi.Owner.MailAddress &&
					(	pi.Owner.MailAddress.FullSiteStreetAddress.Trim() != string.Empty ||
					pi.Owner.MailAddress.SiteCitySiteStateSiteZIP.Trim() != string.Empty)
					)
				{
					g.DrawString(pi.Owner.MailAddress.FullSiteStreetAddress.ToUpper(), m_DisplayFont, Brushes.Black, (iRes * iHorizontalDistance + horizontalOffset + 20), (iColNr * iVerticalDistance) + 52 + verticalOffsetToAdd); 
					g.DrawString(pi.Owner.MailAddress.SiteCitySiteStateSiteZIP.ToUpper(), m_DisplayFont, Brushes.Black, (iRes * iHorizontalDistance + horizontalOffset + 20), (iColNr * iVerticalDistance) + 64 + verticalOffsetToAdd); 
				}
				else if (null != pi.Owner && null != pi.Owner.SiteAddress)
				{
					g.DrawString(pi.Owner.SiteAddress.FullSiteStreetAddress.ToUpper(), m_DisplayFont, br, (iRes * iHorizontalDistance + horizontalOffset + 20), (iColNr * iVerticalDistance) + 52 + verticalOffsetToAdd); 
					g.DrawString(pi.Owner.SiteAddress.SiteCitySiteStateSiteZIP.ToUpper(), m_DisplayFont, br, (iRes * iHorizontalDistance + horizontalOffset + 20), (iColNr * iVerticalDistance) + 64 + verticalOffsetToAdd); 
				}				
				else
				{
					g.DrawString(string.Empty, m_DisplayFont, br, (iRes * iHorizontalDistance + horizontalOffset + 20), (iColNr * iVerticalDistance) + 52 + verticalOffsetToAdd); 
					g.DrawString(string.Empty, m_DisplayFont, br, (iRes * iHorizontalDistance + horizontalOffset + 20), (iColNr * iVerticalDistance) + 64 + verticalOffsetToAdd); 
				}

				if (false == dummyData)
				{
					g.DrawString("*" + pi.IdPropertyItem.ToString().Substring(0, 6) + "*", m_BarcodeFont, br, (iRes * iHorizontalDistance + horizontalBarcodeOffset + horizontalOffsetToAdd), (iColNr * iVerticalDistance) +  30 + verticalOffsetToAdd); 
				}

				iCnt++;
				iLocalCnt++;

				if (iCnt % 40 == 0 && iCnt < alSelectedRecords.Count)
				{
					Globals.smallLabelsCount += 40;
					e.HasMorePages = true;
				}
			}
			return g;
		}
		#endregion � GetSmallLabelGraphics �

		public static Graphics PrintDataReportHeader(PrintPageEventArgs e, int headerOffset, int current, int total)
		{
			Graphics g = e.Graphics;
			Font font_12 = new Font("Arial", 12);
			Font font_14 = new Font("Arial", 14);									
			Font font_14B = new Font("Arial", 14, FontStyle.Bold);
			Font font_13B = new Font("Arial", 13, FontStyle.Bold);						
			Brush brush = new SolidBrush(Color.Black);
	
			if (1 == total)
			{
				g.DrawString("SUBJECT PROPERTY PROFILE", font_13B, brush, 230, 50 + headerOffset);			
				g.DrawString("DATA REPORT", font_13B, brush, 335, 75 + headerOffset);
			}
			else
			{
				g.DrawString("COMPARABLE " + (current + 1).ToString() + " PROPERTY PROFILE", font_13B, brush, 230, 50 + headerOffset);			
				g.DrawString("DATA REPORT", font_13B, brush, 335, 75 + headerOffset);
			}
			
			if (0 == headerOffset)
				g.DrawString(DateTime.Now.ToShortDateString(), font_12, brush, 700, 30 + headerOffset);

			return g;
		}

		public static Graphics PrintReportFooter(PrintPageEventArgs e)
		{
			Graphics g = e.Graphics;
			
			Font font_10I = new Font("Arial", 10, FontStyle.Italic);
			Brush brush = new SolidBrush(Color.Black);
	
			g.DrawString("ULTIMATE DATA SOURCE REPORTS. Copyright 2005, TKS Inc. All rights reserved.", font_10I, brush, 50, 975);
			
			return g;
		}
		
		public static Graphics PrintDataReport(PrintPageEventArgs e, PROPERTY property, int verticalOffset, int current, int total)
		{
			Graphics g = e.Graphics;
			Font font_16 = new Font("Arial", 16);			
			Font font_14 = new Font("Arial", 13);
			Font font_12 = new Font("Arial", 11);
			Font font_10 = new Font("Arial", 10);
			Font font_10B = new Font("Arial", 10, FontStyle.Bold);
			Font font_12B = new Font("Arial", 11, FontStyle.Bold);
			Font font_9 = new Font("Arial", 8);			
			Brush brush = new SolidBrush(Color.Black);
			Pen pen = new Pen(brush, 2);
			
			ApplicationHelper.PrintDataReportHeader(e, verticalOffset, current, total);
			
			ApplicationHelper.PrintReportFooter(e);

			//g.DrawLine(pen, new Point(25, 110 + verticalOffset), new Point(825, 110 + verticalOffset));

			//Subject Property
			string subjectProperty = "Subject Property: ";
			try
			{
				subjectProperty +=	property._PARSED_STREET_ADDRESS._HouseNumber + " " + 
									property._PARSED_STREET_ADDRESS._StreetName + " " +
									property._City + ", " +
									property._State + " " +
									property._PostalCode + " ";
			}
			catch{}			
			g.DrawString(subjectProperty, font_10B, brush, 50, 130 + verticalOffset);				
			
			string subDivision = "Subdivision: ";
			try {subDivision += property._SubdivisionIdentifier;} catch {}
			g.DrawString(subDivision, font_10, brush, 75, 165 + verticalOffset);
			
			//g.DrawLine(pen, new Point(25, 180 + verticalOffset), new Point(825, 180 + verticalOffset));

			//Owner Name
			string ownerName = "Owner Name: ";
			try {ownerName += property.PROPERTY_OWNER[0]._OwnerName;} catch {}
			g.DrawString(ownerName, font_10B, brush, 50, 200 + verticalOffset);

			string phoneNumber = "Phone Number: ";
			try {phoneNumber += property.PROPERTY_OWNER[0]._PhoneNumber;} catch {}
			g.DrawString(phoneNumber, font_10, brush, 555, 200 + verticalOffset);

			string mailingAddress = "Mailing Address: ";
			try {mailingAddress += property.PROPERTY_OWNER[0]._MailingAddress + " " + property.PROPERTY_OWNER[0]._MailingCityAndState + ", " + property.PROPERTY_OWNER[0]._MailingPostalCode;} catch {}
			g.DrawString(mailingAddress, font_10, brush, 75, 235 + verticalOffset);
		
			//g.DrawLine(pen, new Point(25, 255 + verticalOffset), new Point(825, 255 + verticalOffset));

			//Property Characteristics
			g.DrawString("Property Characteristics:", font_12B, brush, 50, 285 + verticalOffset);				
			
			string totalLivingArea = "Total Living Area: ";
			try 
			{
				totalLivingArea += Convert.ToDouble(property._PROPERTY_CHARACTERISTICS._IMPROVEMENTS._ROOM_COUNT._TotalLivingAreaSquareFeetNumber).ToString("N");
				totalLivingArea = totalLivingArea.Substring(0, totalLivingArea.Length - 3);
			} catch {}
			g.DrawString(totalLivingArea, font_10, brush, 50, 305 + verticalOffset);

			string yearBuilt = "Year Built: ";
			try {yearBuilt += property._PROPERTY_CHARACTERISTICS._IMPROVEMENTS._GENERAL_DESCRIPTION._YearBuiltDateIdentifier;} catch {}
			g.DrawString(yearBuilt, font_10, brush, 300, 305 + verticalOffset);

			string totalRooms = "Total Rooms: ";
			try {totalRooms += property._PROPERTY_CHARACTERISTICS._IMPROVEMENTS._ROOM_COUNT._TotalRoomCount;} catch {}
			g.DrawString(totalRooms, font_10, brush, 555, 305 + verticalOffset);
			
			string lotArea = "Lot Area: " ;
			try 
			{				
				lotArea += Convert.ToDouble(property._PROPERTY_CHARACTERISTICS._SITE._DIMENSIONS._LotAreaSquareFeetNumber).ToString("N");
				lotArea = lotArea.Substring(0, lotArea.Length - 3);
			} 
			catch {}
			g.DrawString(lotArea, font_10, brush, 50, 325 + verticalOffset);

			string parkingType = "Parking Type: ";
			try {parkingType += property._PROPERTY_CHARACTERISTICS._IMPROVEMENTS._PARKING._TypeDescription;} 
			catch {}
			g.DrawString(parkingType, font_10, brush, 300, 325 + verticalOffset);

            string totalBedrooms = "Total Bedrooms: ";
            try { totalBedrooms += property._PROPERTY_CHARACTERISTICS._IMPROVEMENTS._ROOM_COUNT._TotalBedroomsCount; } catch  { }
            g.DrawString(totalBedrooms, font_10, brush, 555, 325 + verticalOffset);
            
			string pool = "Pool: ";
			try {pool += property._PROPERTY_CHARACTERISTICS._IMPROVEMENTS._FEATURES._POOL._HasFeatureIndicator;} 
			catch {}
			g.DrawString(pool, font_10, brush, 50, 345 + verticalOffset);

			string garage = "Garage: ";
			try {garage += property._PROPERTY_CHARACTERISTICS._IMPROVEMENTS._PARKING._TypeDescription;} 
			catch {}			
			g.DrawString(garage, font_10, brush, 300, 345 + verticalOffset);

            string totalBaths = "Total Baths: ";
            try { totalBaths += property._PROPERTY_CHARACTERISTICS._IMPROVEMENTS._ROOM_COUNT._TotalBathsCount; } catch { }
            g.DrawString(totalBaths, font_10, brush, 555, 345 + verticalOffset);
            
			string zoning = "Zoning: ";
			try {zoning += property._PROPERTY_CHARACTERISTICS._SITE._ZONING._ClassificationIdentifier;} catch {}
			g.DrawString(zoning, font_10, brush, 50, 365 + verticalOffset);

			string landUse = "Land Use: ";
			try {landUse += property._PROPERTY_CHARACTERISTICS._SITE._CHARACTERISTICS._LandUseDescription;} catch {}
			g.DrawString(landUse, font_10, brush, 300, 365 + verticalOffset);

            string fireplaces = "Fireplaces: ";
            try { fireplaces += property._PROPERTY_CHARACTERISTICS._IMPROVEMENTS._FEATURES._FIREPLACES._HasFeatureIndicator; }
            catch { }
            g.DrawString(fireplaces, font_10, brush, 555, 365 + verticalOffset);

            //g.DrawLine(pen, new Point(25, 385 + verticalOffset), new Point(825, 385 + verticalOffset));

            //Financials
            g.DrawString("Financials:", font_12B, brush, 50, 400 + verticalOffset);
			string lastSaleDate = "Last Sale Date: ";
			try 
			{
				string lsd = property._PROPERTY_HISTORY._SALES_HISTORY[0]._LastSalesDate;
				DateTime d = new DateTime
					(
					(int.Parse(lsd.Substring(0,4))),
					(int.Parse(lsd.Substring(4,2))),
					(int.Parse(lsd.Substring(6,2)))
					);

				
				lastSaleDate += d.ToShortDateString();
			} catch {}
			g.DrawString(lastSaleDate, font_10, brush, 50, 420 + verticalOffset);

			string lastSalePrice = "Last Sale Price: ";
			try {lastSalePrice += Convert.ToDouble(property._PROPERTY_HISTORY._SALES_HISTORY[0]._LastSalesPriceAmount).ToString("C");} catch {}
			g.DrawString(lastSalePrice, font_10, brush, 300, 420 + verticalOffset);

			string pricePerSqFt = "Price Per Sq.Ft.: ";
			try {pricePerSqFt += Convert.ToDouble(property._PROPERTY_HISTORY._SALES_HISTORY[0]._PricePerSquareFootAmount).ToString("C");} catch {}
			g.DrawString(pricePerSqFt, font_10, brush, 555, 420 + verticalOffset);

			string firstLoan = "First Loan: ";
			//try {firstLoan += property._PROPERTY_HISTORY._MORTGAGE_HISTORY[0].;} catch {}
			g.DrawString(firstLoan, font_10, brush, 50, 440 + verticalOffset);

			string secondLoan = "Second Loan: ";
			//try {secondLoan += property._PROPERTY_HISTORY._SALES_HISTORY[0]._PricePerSquareFootAmount;} catch {}
			g.DrawString(secondLoan, font_10, brush, 300, 440 + verticalOffset);

			string cashDown = "Cash Down: ";
			try {cashDown += Convert.ToDouble(property._PROPERTY_HISTORY._SALES_HISTORY[0]._CashDownAmount).ToString("C");} catch {}
			g.DrawString(cashDown, font_10, brush, 555, 440 + verticalOffset);

			string deedType = "Deed Type: ";
			try {deedType += property._PROPERTY_HISTORY._SALES_HISTORY[0]._DeedTypeDescription;} catch {}
			g.DrawString(deedType, font_10, brush, 50, 460 + verticalOffset);

			string recDate = "Rec. Date: ";
			try 
			{
				string rd = property._PROPERTY_HISTORY._SALES_HISTORY[0]._LastRecordingDate;
				DateTime d = new DateTime
					(
					(int.Parse(rd.Substring(0,4))),
					(int.Parse(rd.Substring(4,2))),
					(int.Parse(rd.Substring(6,2)))
					);

				recDate += d.ToShortDateString();
			} catch {}
			g.DrawString(recDate, font_10, brush, 300, 460 + verticalOffset);

			string docNo = "Doc.No.: ";			
			try 
			{docNo += property._PROPERTY_HISTORY._SALES_HISTORY[0]._DocumentNumberIdentifier;} catch {}
			g.DrawString(docNo, font_10, brush, 555, 460 + verticalOffset);

			//string sellerName = "Seller Name: ";			
			//try {sellerName += property.PROPERTY_OWNER[0]._OwnerName;}  catch {}
			//g.DrawString(sellerName, font_12, brush, 600, 455 + verticalOffset);

			//g.DrawLine(pen, new Point(25, 470 + verticalOffset), new Point(825, 470 + verticalOffset));

			//Taxes
			g.DrawString("Taxes:", font_12B, brush, 50, 495 + verticalOffset);

			string apn = "APN: ";
			try {apn += property._AssessorsParcelIdentifier;}  catch {}
			g.DrawString(apn, font_10, brush, 150, 495 + verticalOffset);

			string propTax = "Prop. Tax.: ";			
			try {propTax += Convert.ToDouble(property._PROPERTY_TAX._RealEstateTotalTaxAmount).ToString("C");}  catch {}
			g.DrawString(propTax, font_10, brush, 50, 515 + verticalOffset);

			string assesmYear = "Assesm. Year: ";
			try {assesmYear += property._PROPERTY_TAX._AssessmentYear;}  catch {}
			g.DrawString(assesmYear, font_10, brush, 300, 515 + verticalOffset);

			string deliqYear =   "Deliq. Year: ";
			try {deliqYear += property._PROPERTY_TAX._DelinquentYear;}  catch {}
			g.DrawString(deliqYear, font_10, brush, 555, 515 + verticalOffset);

			string taxArea = "Tax Area: ";
			try {deliqYear += property._PROPERTY_TAX._RateAreaIdentifier;}  catch {}
			g.DrawString(taxArea, font_10, brush, 50, 535 + verticalOffset);

			string taxYear = "Tax Year: ";
			try {taxYear += property._PROPERTY_TAX._TaxYear;}  catch {}
			g.DrawString(taxYear, font_10, brush, 300, 535 + verticalOffset);

			string exempType = "Exempt. Type: ";
			try {exempType += property._PROPERTY_TAX._ExemptionTypeDescription;}  catch {}
			g.DrawString(exempType, font_10, brush, 555, 535 + verticalOffset);
			
			string totalAssessedValue = "Assessed Value: ";
			try {totalAssessedValue += Convert.ToDouble(property._PROPERTY_TAX._TotalAssessedValueAmount).ToString("C");}  catch {}
			g.DrawString(totalAssessedValue, font_10, brush, 50, 555 + verticalOffset);

			string landValue = "Land Use: ";
			try {landValue += property._PROPERTY_TAX._LandValueAmount;}  catch {}
			g.DrawString(landValue, font_10, brush, 300, 555 + verticalOffset);

			string improvementValue =  "Improvement Value: ";
			try {improvementValue += Convert.ToDouble(property._PROPERTY_TAX._ImprovementValueAmount).ToString("C");}  catch {}
			g.DrawString(improvementValue, font_10, brush, 555, 555 + verticalOffset);

			//g.DrawLine(pen, new Point(25, 565 + verticalOffset), new Point(825, 565 + verticalOffset));

			//Legal Description			
			string legalDesc = string.Empty;			
			try {legalDesc += property._LEGAL_DESCRIPTION._TextDescription;}  catch {}
			g.DrawString("Legal Description: ", font_12B, brush, 50, 590 + verticalOffset);
			g.DrawString(legalDesc, font_10, brush, 75, 610 + verticalOffset);
			

			//Additional
			g.DrawString("Additional:", font_12B, brush, 50, 640 + verticalOffset);
			string schoolDistrict = "School District: ";
			try {schoolDistrict += property._NEIGHBORHOOD_AREA_INFORMATION._SchoolDistrictName;}  catch {}
			g.DrawString(schoolDistrict, font_10, brush, 50, 660 + verticalOffset);

			string mapRefOne = "Map Ref. One: ";
			try {mapRefOne += property._NEIGHBORHOOD_AREA_INFORMATION._MapReferenceOneIdentifier;}  catch {}			
			g.DrawString(mapRefOne, font_10, brush, 300, 660 + verticalOffset);
			
			string mapRefTwo = "Map Ref Two:"; 
			try {mapRefTwo += property._NEIGHBORHOOD_AREA_INFORMATION._MapReferenceTwoIdentifier;}  catch {}			
			g.DrawString(mapRefTwo, font_10, brush, 600, 660 + verticalOffset);

			//g.DrawLine(pen, new Point(25, 620 + verticalOffset), new Point(825, 620 + verticalOffset));


			return g;

		}

		public static Graphics PrintMultipleDataReportHeader
			(
			PrintPageEventArgs e, 
			string subject, 
			string sFor,
			string subjLivingArea, 
			string searchCriteria, 
			string sqftRange, 
			string allSalesInLast, 
			string searchRadius, 
			string estimatedValue, 
			string compsNr
			)
		{
			Graphics g = e.Graphics;
			Font font_16 = new Font("Arial", 16);			
			Font font_14 = new Font("Arial", 14);
			Font font_14B = new Font("Arial", 14, FontStyle.Bold);
			Font font_12 = new Font("Arial", 12);
			Font font_12B = new Font("Arial", 12, FontStyle.Bold);
			Font font_9 = new Font("Arial", 9);
			Font font_10 = new Font("Arial", 10);
			Font font_10B = new Font("Arial", 10, FontStyle.Bold);
			Brush brush = new SolidBrush(Color.Black);
			Pen pen = new Pen(brush, 2);
	
			g.DrawString("ESTIMATED VALUE REPORT", font_14B, brush, 275, 50);			
			g.DrawString(DateTime.Now.ToShortDateString(), font_10, brush, 700, 30);

			g.DrawString("Subject Property:" + subject, font_10B, brush, 35, 90);
			g.DrawString("Site Address: " + sFor, font_10, brush, 75, 125);
			g.DrawString("Subject Living Area: " + subjLivingArea, font_10, brush, 400, 125);

			g.DrawString("Search Criteria: " + searchCriteria, font_10B, brush, 35, 160);
			g.DrawString("Sq.Ft. Range: " + sqftRange, font_10, brush, 75, 195);
			g.DrawString("All Sales In Last: " + allSalesInLast, font_10, brush, 300, 195);
			g.DrawString("Search Radius: " + searchRadius, font_10, brush, 500, 195);

			g.DrawString("Estimated Value: " + estimatedValue, font_10B, brush, 35, 240);
			g.DrawString("Number of COMPS found: " + compsNr, font_10B, brush, 500, 240);

			return g;
		}

		
		public static Graphics PrintMultipleDataReport
			(
				PrintPageEventArgs e, 
				PROPERTY[] property, 
				int iPageCnt, 
				string subject, 
				string sFor,
				string subjLivingArea, 
				string searchCriteria, 
				string sqftRange, 
				string allSalesInLast, 
				string searchRadius, 
				string estimatedValue, 
				string compsNr
			)
		{
			int verticalOffset = 0;
			if (iPageCnt == 0)
			{
				ApplicationHelper.PrintMultipleDataReportHeader
					(e, subject, sFor, subjLivingArea, searchCriteria, sqftRange, allSalesInLast, searchRadius, estimatedValue, compsNr);				
				verticalOffset += 250;
			}
					
			ApplicationHelper.PrintDataReport(e, property[iPageCnt], verticalOffset, iPageCnt, property.Length);

			if (iPageCnt < (property.Length - 1))
				e.HasMorePages = true;

			return e.Graphics;
		}

		#region � DeletePropertyItemsFromGroups �
		/// <summary>
		/// Delete Property Items From Group
		/// </summary>
		/// <param name="alPropertyItems">ArrayList that contains PropertyItemIds</param>
		/// <param name="nodeId">NodeId where propertyItem(s) belong</param>
		/// <param name="destroy">Permanently delete, or just remove from subgroup</param>
		public static void DeletePropertyItemsFromGroups(ArrayList alPropertyItems, int nodeId, int destroy)
		{
			SqlCommand sp_DeletePropertyItem = new SqlCommand("DeletePropertyItem", Connection.getInstance().HatchConn);				
			sp_DeletePropertyItem.CommandType = CommandType.StoredProcedure;								

			try
			{					
				if (null == alPropertyItems || 0 == alPropertyItems.Count)	//get propertyItemIds from group where they belong
				{
					alPropertyItems = new ArrayList();
					Hashtable htPropertyItems = ApplicationHelper.GetPropertyItemsInNode(nodeId);
					IDictionaryEnumerator ide = htPropertyItems.GetEnumerator();
					while (ide.MoveNext())
					{
						alPropertyItems.Add((Guid)ide.Value);
					}
				}
				
				foreach (Guid propertyItemId in alPropertyItems)
				{
					sp_DeletePropertyItem.Parameters.Clear();
					sp_DeletePropertyItem.Parameters.Add("@propertyItemId", propertyItemId);
					sp_DeletePropertyItem.Parameters.Add("@groupId", nodeId);
					sp_DeletePropertyItem.Parameters.Add("@destroy", destroy);
																						
					if (ConnectionState.Open != Connection.getInstance().HatchConn.State)
						Connection.getInstance().HatchConn.Open();
				
					sp_DeletePropertyItem.ExecuteNonQuery();				
				}
					
			}
			catch( Exception ex)
			{					
				MessageBox.Show(ex.ToString());
			}
		}
		#endregion � DeletePropertyItemsFromGroups �

		#region ResetFilters 
		public static void ResetFilters()
		{
			Globals.htFilters.Clear();
		}
		#endregion ResetFilters 

		public static DataConsumption.Registration GetDataConsumptionRegistration (Registration.Registration reg)
		{
			DataConsumption.Registration dcReg = null; 
			if (null != reg)
			{
				dcReg = 	new DealMaker.DataConsumption.Registration();
				dcReg.DateActivation = reg.DateActivation;		
				dcReg.IDRegistration = reg.IDRegistration;
				dcReg.KeyCode		 = reg.KeyCode;
				dcReg.LicenceType	 = GetDataConsumptionLicenceType(reg.LicenceType);
				dcReg.Product		 = GetDataConsupmtionProduct(reg.Product);
				dcReg.User			 = GetDataConsumptionUser(reg.User);
				dcReg.VolumeNumber	 = reg.VolumeNumber;			
			}
			return dcReg;
		}

		public static DataConsumption.LicenceType GetDataConsumptionLicenceType (Registration.LicenceType lt)
		{
			DataConsumption.LicenceType licType = null;
			if (null != lt)
			{
				licType = new DealMaker.DataConsumption.LicenceType();
				licType.IDLicenceType	= lt.IDLicenceType;
				licType.Name			= lt.Name;
				licType.Price			= lt.Price;
			}
			return licType;
		}

		public static DataConsumption.Product GetDataConsupmtionProduct (Registration.Product pr)
		{
			DataConsumption.Product product = null; 
			if (null != pr)
			{
				product = new DealMaker.DataConsumption.Product();
				product.IDProduct = pr.IDProduct;
				product.Name = pr.Name;
				product.Version= pr.Version;
				product.Type = pr.Type;
			}
			return product;
		}

		public static DataConsumption.User GetDataConsumptionUser (Registration.User u)
		{
			DataConsumption.User user = null;
			if (null != u)
			{
				user = new DealMaker.DataConsumption.User();
				user.MiddleName = u.MiddleName;
				user.FirstName  = u.FirstName;
				user.LastName	= u.LastName;
				user.Address	= GetDataConsumptionAddress(u.Address);
			}
			return user;
		}

		public static DataConsumption.Address GetDataConsumptionAddress (Registration.Address addr)
		{
			DataConsumption.Address address = null; 
			if (null != addr)
			{
				address = new DealMaker.DataConsumption.Address();
				address.Company = addr.Company;
				address.EMail = addr.EMail;
				address.IDAddress = addr.IDAddress;
				address.PhoneBusiness = addr.PhoneBusiness;
				address.PhoneCell = addr.PhoneCell;
				address.SiteCity = addr.SiteCity;
				address.SiteCitySiteState = addr.SiteCitySiteState;
				address.SiteCitySiteStateSiteZIP = addr.SiteCitySiteStateSiteZIP;
				address.SiteCountry = addr.SiteCountry;
				address.SiteState = addr.SiteState;
				address.SiteStreetName = addr.SiteStreetName;
				address.SiteStreetNumber = addr.SiteStreetNumber;
				address.SiteStreetPostDirectional= addr.SiteStreetPostDirectional;
				address.SiteStreetPreDirectional = addr.SiteStreetPreDirectional;
				address.SiteStreetSuffix = addr.SiteStreetSuffix;
				address.SiteStreetUnitNumber = addr.SiteStreetUnitNumber;
				address.SiteZIP = addr.SiteZIP;
			}
			return address;
		}


		


		#region LoadRegistrationData  
		public static bool LoadRegistrationData()
		{
			Hashtable htLoadedData = new Hashtable();
			FileStream fs = null;
            if (false == Directory.Exists(OSFolderManager.Instance.GetPlainLocalUserAppDataPath()))
			{
				return false;
			}
			try
			{
                if (!File.Exists(OSFolderManager.Instance.GetPlainLocalUserAppDataPath() + @"\profitgrabber.dat"))
                    return false;

                fs = new FileStream(OSFolderManager.Instance.GetPlainLocalUserAppDataPath() + @"\profitgrabber.dat", FileMode.Open, FileAccess.Read);
			}
			catch (Exception exc)
			{
				return false;
			}
			if (null != fs)
			{
				try
				{					
					BinaryFormatter bf=new BinaryFormatter ();
					htLoadedData = (Hashtable)bf.Deserialize(fs);					
					int regConst =  (int)htLoadedData["RV"];
					if (Globals.NotRegisteredConst == regConst)
						Globals.Registered = false;
					else
						Globals.Registered = true;

					Globals.UserRegistration = (Registration.User)htLoadedData["UID"];
					Registration.Registration reg = new DealMaker.Registration.Registration();
					reg = (Registration.Registration)htLoadedData["RID"];
					Globals.RegistrationRegistration = reg;
					Globals.DataConsumptionReg = GetDataConsumptionRegistration(reg);										
				}
				catch(Exception exc)				
				{
					MessageBox.Show("Maybe missing [serializable] attributes in reference.cs from web services\n\n" + exc.Message);
				}
				finally
				{
					fs.Close ();
				}									
			}
			return Globals.Registered;
		}
		#endregion LoadRegistrationData
 
		#region SaveRegistrationData 
		public static void SaveRegistrationData(bool registered, Registration.User user, Registration.Registration reg)
		{
			Globals.Registered = registered;
			Globals.UserRegistration = user;
			Globals.DataConsumptionReg = ApplicationHelper.GetDataConsumptionRegistration(reg);
	
			Hashtable htToSave =  new Hashtable();
			if (true == registered)
			{
				htToSave.Add("RV", Globals.RegisteredConst);
			}
			else
			{
				htToSave.Add("RV", Globals.NotRegisteredConst);
			}
			htToSave.Add("UID", user);
			htToSave.Add("RID", reg);


            if (false == Directory.Exists(OSFolderManager.Instance.GetPlainLocalUserAppDataPath()))
			{
                Directory.CreateDirectory(OSFolderManager.Instance.GetPlainLocalUserAppDataPath());
			}
            FileStream fs = new FileStream(OSFolderManager.Instance.GetPlainLocalUserAppDataPath() + @"\profitgrabber.dat", FileMode.OpenOrCreate, FileAccess.Write);
			try
			{
				BinaryFormatter bf=new BinaryFormatter ();
				bf.Serialize (fs, htToSave);
			}
			catch (Exception exc)
			{
				MessageBox.Show("Maybe missing [serializable] attributes in reference.cs from web services\n\n" + exc.Message);
			}
			finally
			{
				fs.Close ();
			}
			ApplicationHelper.WriteInitialRegistrationData(user.IDUser);
		}
		#endregion SaveRegistrationData 

		public static decimal GetTotalOwed(PropertyItem pi)
		{
			decimal dTotal = 0;
			decimal dTemp = 0;
			
			//1st
			try
			{
                if (null != pi.DealProperty && string.Empty != pi.DealProperty.FirstBal)
				    dTemp = Convert.ToDecimal(pi.DealProperty.FirstBal);
			}
			catch(Exception)
			{
				dTemp = 0;
			}
			finally
			{
				dTotal += dTemp;
			}
			
			//2nd
            dTemp = 0;
			try
			{
                if (null != pi.DealProperty && string.Empty != pi.DealProperty.SecondBal)
				    dTemp = Convert.ToDecimal(pi.DealProperty.SecondBal);
			}
			catch(Exception)
			{
				dTemp = 0;
			}
			finally
			{
				dTotal += dTemp;
			}
			
			//3rd
            dTemp = 0;
			try
			{
                if (null != pi.DealProperty && string.Empty != pi.DealProperty.ThirdBal)
				    dTemp = Convert.ToDecimal(pi.DealProperty.ThirdBal);
			}
			catch(Exception)
			{
				dTemp = 0;
			}
			finally
			{
				dTotal += dTemp;
			}

			//OtherLiens
            dTemp = 0;
			try
			{
                if (null != pi.DealProperty && string.Empty != pi.DealProperty.OtherLiens)
				    dTemp = Convert.ToDecimal(pi.DealProperty.OtherLiens);
			}
			catch(Exception)
			{
				dTemp = 0;
			}
			finally
			{
				dTotal += dTemp;
			}

			return dTotal;
		}

		public static decimal GetTotalMonthlyPayment(PropertyItem pi)
		{
			decimal dTotal = 0;
			decimal dTemp = 0;

			//monthly paayment 1
			try
			{
                if (null != pi.DealProperty && string.Empty != pi.DealProperty.MoPay1)
				    dTemp = Convert.ToDecimal(pi.DealProperty.MoPay1);
			}
			catch(Exception)
			{
				dTemp = 0;
			}
			finally
			{
				dTotal += dTemp;
			}

			//monthly payment 2
            dTemp = 0;
			try
			{
                if (null != pi.DealProperty && string.Empty != pi.DealProperty.MoPay2)
				    dTemp = Convert.ToDecimal(pi.DealProperty.MoPay2);
			}
			catch(Exception)
			{
				dTemp = 0;
			}
			finally
			{
				dTotal += dTemp;
			}

			//monthly payment 3
            dTemp = 0;
			try
			{
                if (null != pi.DealProperty && string.Empty != pi.DealProperty.MoPay3)
				    dTemp = Convert.ToDecimal(pi.DealProperty.MoPay3);
			}
			catch(Exception)
			{
				dTemp = 0;
			}
			finally
			{
				dTotal += dTemp;
			}
			return dTotal;
		}

		
		internal static void CleanDatabase()
		{
			//TA++: some customer had a strange activity (I believe he manually used records)
			try
			{
				string cmdText = "DELETE FROM NodePropertyItems WHERE (IdPropertyItem NOT IN (SELECT IdPropertyItem FROM PropertyItem))";
				SqlCommand purgeDatabase = new SqlCommand(cmdText, Connection.getInstance().HatchConn);
				purgeDatabase.ExecuteNonQuery();
			}
			catch(Exception exc)
			{
				MessageBox.Show(exc.Message + System.Environment.NewLine + exc.InnerException);
			}
		}

		internal static void Database_AlterTable_ExpandTables()
		{
			try
			{								
				string alterTableText = string.Empty;
				SqlCommand alterTable = null;

				alterTableText = "ALTER TABLE DealProperty ALTER COLUMN WhySelling nvarchar(184)";				
				alterTable = new SqlCommand(alterTableText, Connection.getInstance().HatchConn);
				alterTable.ExecuteNonQuery();

				
				alterTableText = "ALTER TABLE DealProperty ALTER COLUMN FeatureCondition nvarchar(184)";
				alterTable = new SqlCommand(alterTableText, Connection.getInstance().HatchConn);
				alterTable.ExecuteNonQuery();


				alterTableText = "ALTER TABLE DealProperty ALTER COLUMN NextStep nvarchar(184)";
				alterTable = new SqlCommand(alterTableText, Connection.getInstance().HatchConn);
				alterTable.ExecuteNonQuery();
                
                Globals.ExpandedFields = true;
			}
			catch (Exception 
				exc)
			{
				MessageBox.Show("Cannot alter tables: whyselling, featurecondition, nextstep!" + System.Environment.NewLine + exc.Message);
			}			
		}

        internal static void Database_AlterTable_ExpandMailFaxWizardTable()
        {
            try
            {
                string alterTableText = string.Empty;
                SqlCommand alterTable = null;
                
                alterTableText = "ALTER TABLE MailFaxWizard ALTER COLUMN DocumentName nvarchar(2048)";
                alterTable = new SqlCommand(alterTableText, Connection.getInstance().HatchConn);
                alterTable.ExecuteNonQuery();

                /*                
                        public string DisplayName { get; set; }
                        public string ReplyTo { get; set; }
                        public string SubjectPrefix { get; set; }
                        public bool   AppendAddressToSubject { get; set; }
                        public string Body { get; set; }                
                */

                alterTableText = "if COL_LENGTH('MailFaxWizard', 'DisplayName') IS NULL BEGIN ALTER TABLE MailFaxWizard ADD DisplayName nvarchar(1024) END";
                alterTable = new SqlCommand(alterTableText, Connection.getInstance().HatchConn);
                alterTable.ExecuteNonQuery();
                                
                alterTableText = "if COL_LENGTH('MailFaxWizard', 'ReplyTo') IS NULL BEGIN ALTER TABLE MailFaxWizard ADD ReplyTo nvarchar(512) END";
                alterTable = new SqlCommand(alterTableText, Connection.getInstance().HatchConn);
                alterTable.ExecuteNonQuery();

                alterTableText = "if COL_LENGTH('MailFaxWizard', 'SubjectPrefix') IS NULL BEGIN ALTER TABLE MailFaxWizard ADD SubjectPrefix nvarchar(512) END";
                alterTable = new SqlCommand(alterTableText, Connection.getInstance().HatchConn);
                alterTable.ExecuteNonQuery();

                alterTableText = "if COL_LENGTH('MailFaxWizard', 'AppendAddressToSubject') IS NULL BEGIN ALTER TABLE MailFaxWizard ADD AppendAddressToSubject bit END";
                alterTable = new SqlCommand(alterTableText, Connection.getInstance().HatchConn);
                alterTable.ExecuteNonQuery();

                alterTableText = "if COL_LENGTH('MailFaxWizard', 'Body') IS NULL BEGIN ALTER TABLE MailFaxWizard ADD Body nvarchar(4000) END";
                alterTable = new SqlCommand(alterTableText, Connection.getInstance().HatchConn);
                alterTable.ExecuteNonQuery();

                alterTableText = "ALTER TABLE MailFaxWizard ALTER COLUMN Body nvarchar(4000)";
                alterTable = new SqlCommand(alterTableText, Connection.getInstance().HatchConn);
                alterTable.ExecuteNonQuery();
            }
            catch (Exception
                exc)
            {
                MessageBox.Show("Cannot alter tables: MailFaxWizard" + System.Environment.NewLine + exc.Message);
            }
        }

        internal static void Database_AlterTable_Users()
        {
            try
            {
                string alterTableText = string.Empty;
                SqlCommand alterTable = null;

                /*                
                    <property name="Email2" column="Email2"/>
                    <property name="DirectNumber" column="DirectNumber"/>
                    <property name="BuyingWebsite" column="BuyingWebsite"/>
                    <property name="ForeclosureWebsite" column="ForeclosureWebsite"/>        
                */

                alterTableText = "if COL_LENGTH('Users', 'Email2') IS NULL BEGIN ALTER TABLE Users ADD Email2 nvarchar(512) END";
                alterTable = new SqlCommand(alterTableText, Connection.getInstance().HatchConn);
                alterTable.ExecuteNonQuery();

                alterTableText = "if COL_LENGTH('Users', 'DirectNumber') IS NULL BEGIN ALTER TABLE Users ADD DirectNumber nvarchar(512) END";
                alterTable = new SqlCommand(alterTableText, Connection.getInstance().HatchConn);
                alterTable.ExecuteNonQuery();

                alterTableText = "if COL_LENGTH('Users', 'BuyingWebsite') IS NULL BEGIN ALTER TABLE Users ADD BuyingWebsite nvarchar(512) END";
                alterTable = new SqlCommand(alterTableText, Connection.getInstance().HatchConn);
                alterTable.ExecuteNonQuery();

                alterTableText = "if COL_LENGTH('Users', 'ForeclosureWebsite') IS NULL BEGIN ALTER TABLE Users ADD ForeclosureWebsite nvarchar(512) END";
                alterTable = new SqlCommand(alterTableText, Connection.getInstance().HatchConn);
                alterTable.ExecuteNonQuery();                
            }
            catch (Exception exc)
            {
                MessageBox.Show("Cannot alter tables: Users" + System.Environment.NewLine + exc.Message);
            }
        }

        //sasass
        internal static void TestDatabase_CreateShortSaleTable()
        {
            SqlDataReader reader = null;
            try
            {
                bool isInstalled = false;
                string cmdText = "select * from dbo.sysobjects where id = object_id(N'[dbo].[ShortSale]')";
                SqlCommand testForSPL = new SqlCommand(cmdText, Connection.getInstance().HatchConn);
                reader = testForSPL.ExecuteReader();

                while (reader.Read())
                {
                    isInstalled = true;
                    break;
                }
                reader.Close();

                if (false == isInstalled)
                {
                    SqlCommand createSPL = new SqlCommand("ALTER TABLE [dbo].[PropertyItem] ADD  CONSTRAINT [PK_PropertyItem] PRIMARY KEY CLUSTERED (IdPropertyItem ASC)  ON [PRIMARY]", Connection.getInstance().HatchConn);
                    createSPL.ExecuteNonQuery();
                    
                    createSPL = new SqlCommand("CREATE TABLE [dbo].[ShortSale]( [IdPropertyItem] [uniqueidentifier] NOT NULL, [ShortSaleDocument] [image] NOT NULL, [IncludeInSummaryReport] [bit] NULL) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]", Connection.getInstance().HatchConn);
                    createSPL.ExecuteNonQuery();
                    
                    createSPL = new SqlCommand("ALTER TABLE [dbo].[ShortSale]  WITH CHECK ADD  CONSTRAINT [FK_ShortSale_PropertyItem] FOREIGN KEY([IdPropertyItem]) REFERENCES [dbo].[PropertyItem] ([IdPropertyItem]) ON DELETE CASCADE", Connection.getInstance().HatchConn);
                    createSPL.ExecuteNonQuery();
                    
                    createSPL = new SqlCommand("ALTER TABLE [dbo].[ShortSale] CHECK CONSTRAINT [FK_ShortSale_PropertyItem]", Connection.getInstance().HatchConn);
                    createSPL.ExecuteNonQuery();
                }
            }
            catch (Exception
                exc)
            {
                MessageBox.Show("ShortSaleTable can not be created!" + System.Environment.NewLine + exc.Message);
            }
            finally
            {
                if (false == reader.IsClosed)
                {
                    reader.Close();
                }
            }
        }

        internal static void AlterTable_ExpandTaskTable()
        {
            SqlDataReader reader = null;
            try
            {                
                bool isExpanded = false;
                string testText = "SELECT * FROM SysObjects O INNER JOIN SysColumns C ON O.ID=C.ID WHERE ObjectProperty(O.ID,'IsUserTable')=1 AND O.Name='Task' AND C.Name='Priority'";
                SqlCommand testForExp = new SqlCommand(testText, Connection.getInstance().HatchConn);
                reader = testForExp.ExecuteReader();

                while (reader.Read())
                {
                    isExpanded = true;
                    break;
                }
                reader.Close();

                if (!isExpanded)
                {
                    string alterTableText = string.Empty;
                    SqlCommand alterTable = null;

                    alterTableText = "ALTER TABLE Task ADD Priority int";
                    alterTable = new SqlCommand(alterTableText, Connection.getInstance().HatchConn);
                    alterTable.ExecuteNonQuery();
                }
            }
            catch (Exception
                exc)
            {
                MessageBox.Show("Can not ALTER TABLE Task!" + System.Environment.NewLine + exc.Message);
            }
            finally
            {
                if (false == reader.IsClosed)
                {
                    reader.Close();
                }
            }
        }        

		internal static void TestDatabase_CreateSPL_GetPropertyItemsDescInNodeExt()
		{
			SqlDataReader reader = null;
			try
			{
				bool isInstalled = false;
				string cmdText = "select * from dbo.sysobjects where id = object_id(N'[dbo].[GetPropertyItemsDescInNodeExt]') and OBJECTPROPERTY(id, N'IsProcedure') = 1";
				SqlCommand testForSPL = new SqlCommand(cmdText, Connection.getInstance().HatchConn);
				reader = testForSPL.ExecuteReader();
				
				while (reader.Read())
				{
					isInstalled = true;
					break;
				}
				reader.Close();

				if (false == isInstalled)
				{
					StringBuilder sb = new StringBuilder();
					sb.Append("CREATE PROCEDURE GetPropertyItemsDescInNodeExt");
					sb.Append(System.Environment.NewLine);
					sb.Append("(");
					sb.Append(System.Environment.NewLine);
					sb.Append("@NodeId int");
					sb.Append(System.Environment.NewLine);
					sb.Append(")");
					sb.Append(System.Environment.NewLine);
					sb.Append("AS");
					sb.Append(System.Environment.NewLine);
					sb.Append("SET NOCOUNT ON");
					sb.Append(System.Environment.NewLine);

					sb.Append("SELECT");
					sb.Append(System.Environment.NewLine);
					sb.Append("PropertyItem.IdPropertyItem,");
					sb.Append(System.Environment.NewLine);
					sb.Append("PropertyItem.DateCreated,");
					sb.Append(System.Environment.NewLine);
					sb.Append("Contact.FullName,");
					sb.Append(System.Environment.NewLine);
					sb.Append("SiteAddress.FullSiteStreetAddress,");
					sb.Append(System.Environment.NewLine);
					sb.Append("SiteAddress.SiteCity,");
					sb.Append(System.Environment.NewLine);
					sb.Append("SiteAddress.SiteState,");
					sb.Append(System.Environment.NewLine);
					sb.Append("SiteAddress.SiteCitySiteState,");
					sb.Append(System.Environment.NewLine);
					sb.Append("SiteAddress.SiteZIP,");
					sb.Append(System.Environment.NewLine);
					sb.Append("MailAddress.FullSiteStreetAddress,");
					sb.Append(System.Environment.NewLine);
					sb.Append("MailAddress.SiteCity,");
					sb.Append(System.Environment.NewLine);
					sb.Append("MailAddress.SiteState,");
					sb.Append(System.Environment.NewLine);
					sb.Append("MailAddress.SiteCitySiteState,");
					sb.Append(System.Environment.NewLine);
					sb.Append("MailAddress.SiteZIP,");
					sb.Append(System.Environment.NewLine);
					sb.Append("HouseProperties.HomeSqFt,");
					sb.Append(System.Environment.NewLine);
					sb.Append("ListingAgent.ListingAgentFullName,");
					sb.Append(System.Environment.NewLine);
					sb.Append("ListingAgent.ListingPrice,");
					sb.Append(System.Environment.NewLine);
					sb.Append("ListingAgent.ListingAgentFaxNumber,");
					sb.Append(System.Environment.NewLine);
					sb.Append("ForeClosureProperties.ForeClosureFileDate,");
					sb.Append(System.Environment.NewLine);
					sb.Append("ForeClosureProperties.ForeClosureSaleDate");
					sb.Append(System.Environment.NewLine);
					
					sb.Append("FROM");
					sb.Append(System.Environment.NewLine);
					sb.Append("PropertyItem");
					sb.Append(System.Environment.NewLine);
					sb.Append("LEFT OUTER JOIN Contact");
					sb.Append(System.Environment.NewLine);
					sb.Append("LEFT OUTER JOIN Address as SiteAddress ON Contact.SiteAddress = SiteAddress.IdAddress");
					sb.Append(System.Environment.NewLine);
					sb.Append("ON Contact.IdContact = PropertyItem.Owner");
					sb.Append(System.Environment.NewLine);
					sb.Append("LEFT OUTER JOIN Contact as MAContact");
					sb.Append(System.Environment.NewLine);
					sb.Append("LEFT OUTER JOIN Address as MailAddress ON MAContact.MailAddress = MailAddress.IdAddress");
					sb.Append(System.Environment.NewLine);
					sb.Append("ON MAContact.IdContact = PropertyItem.Owner");
					sb.Append(System.Environment.NewLine);
					sb.Append("LEFT OUTER JOIN DealInfo");
					sb.Append(System.Environment.NewLine);
					sb.Append("LEFT OUTER JOIN HouseProperties ON DealInfo.HouseProperty = HouseProperties.IdHouseProperty");
					sb.Append(System.Environment.NewLine);
					sb.Append("ON DealInfo.IdDealInfo = PropertyItem.DealInfo");
					sb.Append(System.Environment.NewLine);
					sb.Append("LEFT OUTER JOIN ListingAgent ON PropertyItem.ListingAgent = ListingAgent.IdListingAgent");
					sb.Append(System.Environment.NewLine);
					sb.Append("LEFT OUTER JOIN DealInfo as FCDealInfo");
					sb.Append(System.Environment.NewLine);
					sb.Append("LEFT OUTER JOIN ForeClosureProperties ON FCDealInfo.ForeClosureProperty = ForeClosureProperties.IdForeClosureProperty");
					sb.Append(System.Environment.NewLine);
					sb.Append("ON FCDealInfo.IdDealInfo = PropertyItem.DealInfo");
					sb.Append(System.Environment.NewLine);
					sb.Append("WHERE");
					sb.Append(System.Environment.NewLine);
					sb.Append("PropertyItem.IdPropertyItem IN (SELECT IdPropertyItem from NodePropertyItems where NodeId = @NodeId)");					


					sb.Append(System.Environment.NewLine);
					sb.Append("RETURN");
					sb.Append(System.Environment.NewLine);

					string splText = sb.ToString();
					SqlCommand createSPL = new SqlCommand(splText, Connection.getInstance().HatchConn);
					createSPL.ExecuteNonQuery();
				}
			}
			catch (Exception 
				exc)
			{
                MessageBox.Show("SPL GetPropertyItemsDescInNodeExt can not be created!" + System.Environment.NewLine + exc.Message);
			}
			finally
			{
				if (false == reader.IsClosed)
				{
					reader.Close();
				}
			}
		}

		internal static void TestDatabase_CreateSPL_CreatePropertyItemFilter()
		{
			SqlDataReader reader = null;
			try
			{
				bool isInstalled = false;
				string cmdText = "select * from dbo.sysobjects where id = object_id(N'[dbo].[CreatePropertyItemFilter]') and OBJECTPROPERTY(id, N'IsProcedure') = 1";
				SqlCommand testForSPL = new SqlCommand(cmdText, Connection.getInstance().HatchConn);
				reader = testForSPL.ExecuteReader();
				
				while (reader.Read())
				{
					isInstalled = true;
					break;
				}
				reader.Close();

				if (false == isInstalled)
				{
					StringBuilder sb = new StringBuilder();
					sb.Append("CREATE PROCEDURE CreatePropertyItemFilter");
					sb.Append(System.Environment.NewLine);
					sb.Append("(");
					sb.Append(System.Environment.NewLine);
					sb.Append("@NodeId int = 1,");
					sb.Append(System.Environment.NewLine);
					sb.Append("@SubGroupsSearch int");
					sb.Append(System.Environment.NewLine);
					sb.Append(")");
					sb.Append(System.Environment.NewLine);
					sb.Append("AS");
					sb.Append(System.Environment.NewLine);
					sb.Append("SET NOCOUNT ON");
					sb.Append(System.Environment.NewLine);
					sb.Append("if exists");
					sb.Append(System.Environment.NewLine);
					sb.Append("(select * from dbo.sysobjects where id = object_id(N'[dbo].[AffectedPropertyItems]'))");
					sb.Append(System.Environment.NewLine);
					sb.Append("drop table [dbo].[AffectedPropertyItems]");
					sb.Append(System.Environment.NewLine);
					sb.Append("CREATE TABLE dbo.AffectedPropertyItems (PropertyItem uniqueidentifier /*PRIMARY KEY*/)");
					sb.Append(System.Environment.NewLine);
					sb.Append("exec GetAffectedPropertyItems @NodeId, @SubGroupsSearch");
					sb.Append(System.Environment.NewLine);
					sb.Append("select PropertyItem from AffectedPropertyItems");
					sb.Append(System.Environment.NewLine);
					sb.Append("RETURN");
					sb.Append(System.Environment.NewLine);

					string splText = sb.ToString();
					SqlCommand createSPL = new SqlCommand(splText, Connection.getInstance().HatchConn);
					createSPL.ExecuteNonQuery();
				}
			}
			catch (Exception 
				exc)
			{
				MessageBox.Show("SPL CreatePropertyItemFilter can not be created!" + System.Environment.NewLine + exc.Message);
			}
			finally
			{
				if (false == reader.IsClosed)
				{
					reader.Close();
				}
			}
		}

		internal static void TestDatabase_AlterSPL_InsertPropertyItem()
		{			
			try
			{							
				StringBuilder sb = new StringBuilder();
				sb.Append("ALTER PROCEDURE insert_PropertyItem");
				sb.Append(System.Environment.NewLine);
				sb.Append("(");
				sb.Append(System.Environment.NewLine);
				sb.Append("@IdPropertyItem uniqueidentifier,");
				sb.Append(System.Environment.NewLine);
				sb.Append("@Owner uniqueidentifier,");
				sb.Append(System.Environment.NewLine);
				sb.Append("@DealInfo uniqueidentifier,");
				sb.Append(System.Environment.NewLine);
				sb.Append("@DealProperty uniqueidentifier,");
				sb.Append(System.Environment.NewLine);
				sb.Append("@ListingAgent uniqueidentifier,");
				sb.Append(System.Environment.NewLine);
				sb.Append("@DateCreated DateTime,");
				sb.Append(System.Environment.NewLine);
				sb.Append("@DateLastModified DateTime");
				sb.Append(System.Environment.NewLine);
				sb.Append(")");
				sb.Append(System.Environment.NewLine);
				sb.Append("AS");
				sb.Append(System.Environment.NewLine);
				sb.Append("INSERT INTO PropertyItem( IdPropertyItem , Owner , DealInfo , ListingAgent , DealProperty , DateCreated , DateLastModified ) VALUES ( @IdPropertyItem , @Owner , @DealInfo , @ListingAgent , @DealProperty , @DateCreated , @DateLastModified )");
				sb.Append(System.Environment.NewLine);

				string splText = sb.ToString();
				SqlCommand createSPL = new SqlCommand(splText, Connection.getInstance().HatchConn);
				createSPL.ExecuteNonQuery();
				
			}
			catch (Exception 
				exc)
			{
				MessageBox.Show("SPL insert_PropertyItem can not be ALTERED!" + System.Environment.NewLine + exc.Message);
			}			
		}

        internal static void TestDatabase_AlterSPL_GetMFWPropertyItemDetails()
        {
            try
            {
                StringBuilder sb = new StringBuilder();
                sb = new StringBuilder();
                sb.Append("ALTER PROCEDURE [dbo].[GetMFWPropertyItemDetails] ( @propertyItemId uniqueidentifier ) AS  SET NOCOUNT ON  SELECT Contact.FullName, SiteAddress.FullSiteStreetAddress, SiteAddress.SiteCity, SiteAddress.SiteState, SiteAddress.SiteZIP, MailAddress.FullSiteStreetAddress, MailAddress.SiteCity, MailAddress.SiteState, MailAddress.SiteZIP, HouseProperties.HomeSqFt, ListingAgent.ListingAgentFullName, ListingAgent.ListingPrice, ListingAgent.ListingAgentFaxNumber, ForeClosureProperties.ForeClosureFileDate, ForeClosureProperties.ForeClosureSaleDate, Contact.eMail, ListingAgent.ListingAgentEMail, Contact.STEmail  FROM PropertyItem LEFT OUTER JOIN Contact LEFT OUTER JOIN Address as SiteAddress ON Contact.SiteAddress = SiteAddress.IdAddress ON Contact.IdContact = PropertyItem.Owner LEFT OUTER JOIN Contact as MAContact LEFT OUTER JOIN Address as MailAddress ON MAContact.MailAddress = MailAddress.IdAddress ON MAContact.IdContact = PropertyItem.Owner LEFT OUTER JOIN DealInfo LEFT OUTER JOIN HouseProperties ON DealInfo.HouseProperty = HouseProperties.IdHouseProperty ON DealInfo.IdDealInfo = PropertyItem.DealInfo LEFT OUTER JOIN ListingAgent ON PropertyItem.ListingAgent = ListingAgent.IdListingAgent LEFT OUTER JOIN DealInfo as FCDealInfo LEFT OUTER JOIN ForeClosureProperties ON FCDealInfo.ForeClosureProperty = ForeClosureProperties.IdForeClosureProperty ON FCDealInfo.IdDealInfo = PropertyItem.DealInfo WHERE PropertyItem.IdPropertyItem = @propertyItemId  RETURN");

                var retCode = new SqlCommand(sb.ToString(), Connection.getInstance().HatchConn).ExecuteNonQuery();

                
            }
            catch (Exception exc)
            {
                MessageBox.Show("SPL GetMFWPropertyItemDetails can not be ALTERED!" + System.Environment.NewLine + exc.Message);
            }
        }



		#region � SelfDeactivation �		
		internal static bool TestForUser()
		{
			//Read UserId and compare it with UserId stored in user.dat
			RegistryKey rk = Registry.LocalMachine.OpenSubKey(@"Software\TKS\Profit Grabber Pro\Reg");
			if (null != rk)
			{
				string regUserId = rk.GetValue("C1").ToString();

				if (Globals.UserRegistration.IDUser.ToString() == regUserId)
					return true;
				else
					return false;
			}
			return false;

		}

		internal static object GetDateFromString(string sDate)
		{
			DateTime dtActivationDate;

			if (string.Empty == sDate)
				return null;

			int iPos = sDate.IndexOf("/", 0);
			string year = sDate.Substring(0, iPos);
			int iPos2 = sDate.IndexOf("/", iPos + 1);
			string month = sDate.Substring(iPos + 1, iPos2 - (iPos + 1));
			string day = sDate.Substring(iPos2 + 1);

			int iYear = 0;
			int iMonth = 0;
			int iDay = 0;

			try
			{
				iYear = Convert.ToInt16(year);
				iMonth = Convert.ToInt16(month);
				iDay = Convert.ToInt16(day);
				dtActivationDate = new DateTime(iYear, iMonth, iDay);
			}
			catch (Exception exc)
			{
				MessageBox.Show(exc.Message + System.Environment.NewLine + exc.InnerException);
				return null;
			}			
			return dtActivationDate;			
		}

		internal static bool TestIfRegisteredInPeriod(out int period)
		{
			bool bResult = false;
			int iYear = 0;
			int iMonth = 0;
			int iDay = 0;

			try
			{
				//Read DateTime Of Activation
				RegistryKey rk = Registry.LocalMachine.OpenSubKey(@"Software\TKS\Profit Grabber Pro\Reg");
				string activationDate = rk.GetValue("C2").ToString();
			
				int iPos = activationDate.IndexOf("/", 0);
				string year = activationDate.Substring(0, iPos);
				int iPos2 = activationDate.IndexOf("/", iPos + 1);
				string month = activationDate.Substring(iPos + 1, iPos2 - (iPos + 1));
				string day = activationDate.Substring(iPos2 + 1);

				iYear = Convert.ToInt16(year);
				iMonth = Convert.ToInt16(month);
				iDay = Convert.ToInt16(day);
			}
			catch (Exception exc)
			{
				MessageBox.Show(exc.Message + System.Environment.NewLine + exc.InnerException);
				period = 0;
				return false;
			}

			DateTime dtActivationDate = new DateTime(iYear, iMonth, iDay);
              
			int registrationPeriod = ApplicationHelper.DetermineRegistrationPeriod(dtActivationDate);
			period = registrationPeriod;
			string registeredDate = string.Empty;

			try
			{
				RegistryKey rkReg = Registry.LocalMachine.OpenSubKey(@"Software\TKS\Profit Grabber Pro\Reg");
				switch (registrationPeriod)
				{
					case 0:	//Fraud
						ApplicationHelper.DeleteRegistrationData("ProfitGrabber Pro will now deactivate!");										
						break;

					case 1: //Initial perion, first 40 days
						bResult = true;
						break;

					case 2:
						registeredDate = (string)rkReg.GetValue("C3");
						object obj = ApplicationHelper.GetDateFromString(registeredDate);
						if (null != obj)
						{
							DateTime dtRegisteredDate = (DateTime)obj;						
							bResult = true;
						}
						break;

					case 3:
						registeredDate = (string)rkReg.GetValue("C4");
						obj = ApplicationHelper.GetDateFromString(registeredDate);
						if (null != obj)
						{
							DateTime dtRegisteredDate = (DateTime)obj;						
							bResult = true;
						}
						break;

					case 4:
						registeredDate = (string)rkReg.GetValue("C5");
						obj = ApplicationHelper.GetDateFromString(registeredDate);
						if (null != obj)
						{
							DateTime dtRegisteredDate = (DateTime)obj;						
							bResult = true;
						}
						break;

					case 5:
						registeredDate = (string)rkReg.GetValue("C6");
						obj = ApplicationHelper.GetDateFromString(registeredDate);
						if (null != obj)
						{
							DateTime dtRegisteredDate = (DateTime)obj;						
							bResult = true;
						}
						break;
				}
			}
			catch (Exception exc)
			{
				MessageBox.Show(exc.ToString());
			}


			return bResult;
		}

		internal static int DetermineRegistrationPeriod(DateTime dtActivationDate)
		{
			TimeSpan ts = DateTime.Now - dtActivationDate;
			if (ts.Days < 0)
			{
				return 0;
			}
			else if (ts.Days > 40 && ts.Days < 80)
			{
				return 2;
			}
			else if (ts.Days >= 80 && ts.Days < 120)
			{
				return 3;
			}
			else if (ts.Days >= 120 && ts.Days < 240)
			{
				return 4;
			}
			else if (ts.Days >= 240)
			{
				return 5;
			}
			else
				return 1;

		}		

		internal static void RegistrationReminderWorker()
		{			
			int iCnt = 0;
			int registrationPeriod;
			if (false == ApplicationHelper.TestForUser())
			{					
				ApplicationHelper.DeleteRegistrationData("Your credentials do not match registered credentials!\nProfitGrabber Pro will now deactivate!");
				return;
			}

			while (true)
			{
				if (0 == iCnt)
				{
					System.Threading.Thread.Sleep(10000);	//initial 10 secs sleep
					iCnt++;
				}
				//Test if registered 
				if (true == ApplicationHelper.TestIfRegisteredInPeriod(out registrationPeriod))
				{
					//if so - break a loop and end a thread
					break;
				}								
				
				//Display waiting Messagebox (15 secs)
				RegistrationReminderForm rrf = new RegistrationReminderForm();
				rrf.ShowDialog();

				//try to register user in registration period
				bool bResult = ApplicationHelper.RegisterUserInRegistrationPeriod(registrationPeriod);						
				if (true == bResult)
				{
#if DEBUG
					MessageBox.Show("Registration checked and ok in period " + registrationPeriod.ToString());
#endif
				}
				//Sleep For a 30 min 30 min * 60 sec = 1800000
				System.Threading.Thread.Sleep(1800000);
			}
		}

		internal static bool RegisterUserInRegistrationPeriod(int registrationPeriod)
		{
			bool bResult = false;
			//SoftwareRegistration.ClassLibrary.WebServiceCore wsc;
			//wsc = new SoftwareRegistration.ClassLibrary.WebServiceCore();
			DealMaker.Registration.Service1 service = null;
			try
			{
				service = new DealMaker.Registration.Service1();
			}
			catch (Exception exc)
			{
#if DEBUG
				MessageBox.Show("Message: " + exc.ToString() + "\nInner Exception: " + exc.InnerException);
#endif
				throw new Exception("service = new DealMaker.Registration.Service1()",  exc);	//TA:11.04.2005 - For saving exceptions that may come...
			}
			service.Timeout = 90000;
			
			//read from app.config			
			try
			{
				NameValueCollection nvc = (NameValueCollection)ConfigurationSettings.GetConfig("ProfitGrabberWebSupport");
				if (null != nvc && nvc.Count != 0)
				{
					Globals.ProfitGrabberWebSupportURL = (string)nvc["URL"];
					service.Url = (string)nvc["URL"];				
				}
			}
			catch (Exception exc)
			{
				throw new Exception("Reading from app.Config",  exc);	//TA:11.04.2005 - For saving exceptions that may come...
			}

			IList ilRes = null;
			try
			{
				string message = string.Empty;
				ilRes =  service.RegisterUser(Globals.UserRegistration, Globals.RegistrationRegistration, out message);
				if (null != ilRes && 0 != ilRes.Count)
				{
					//success					
					string strResult = ilRes[3] as string;
					if (null != strResult && strResult.IndexOf("Registration repeated") != -1)
					{
						//Write to registry						
						bResult = true;
					}
				}
			}
			catch (Exception 
#if DEBUG
				exc
#endif
				)
			{
				//silent so far
				//either no connection, or server is down, anyway user has 5 days to do it!

#if DEBUG
				MessageBox.Show(exc.Message + System.Environment.NewLine + exc.InnerException);
#endif
			}
			finally
			{
				ApplicationHelper.UpdateRegistrationData(bResult, registrationPeriod);
			}
			return bResult;
		}

		internal static bool CheckForSelfDeactivation()
		{
			bool bResult = false;
			Globals.registrationChecker = new Thread(new ThreadStart(ApplicationHelper.RegistrationReminderWorker));
			Globals.registrationChecker.Start();
			
			return bResult;
		}

		internal static void UpdateRegistrationData(bool bSuccess, int registrationPeriod)
		{
			bool bNew = false;
			int iDay = 0;
			int iMonth = 0;
			int iYear = 0;
			int iPeriod = 0;
			int iTry = 0;
			try
			{
				RegistryKey rk = Registry.LocalMachine.OpenSubKey(@"Software\TKS\Profit Grabber Pro\Reg");
				if (false == bSuccess)
				{
					if (0 == Globals.registrationTries)
					{
						//read from registry last registration try period, date and its number
						string lastRegTry = rk.GetValue("C7").ToString();
					
						if (string.Empty == lastRegTry)
						{
							bNew = true;
						}
						else

						{					
							//parse it - format is day/month/year/period/try					
							try
							{
								int iPos1 = lastRegTry.IndexOf("/");
								int iPos2 = lastRegTry.IndexOf("/", iPos1 + 1);
								int iPos3 = lastRegTry.IndexOf("/", iPos2 + 1);
								int iPos4 = lastRegTry.IndexOf("/", iPos3 + 1);						

								iYear	= Convert.ToInt16(lastRegTry.Substring(0, iPos1));
								iMonth  = Convert.ToInt16(lastRegTry.Substring(iPos1 + 1, iPos2 - (iPos1 + 1)));
								iDay	= Convert.ToInt16(lastRegTry.Substring(iPos2 + 1, iPos3 - (iPos2 + 1)));
								iPeriod	= Convert.ToInt16(lastRegTry.Substring(iPos3 + 1, iPos4 - (iPos3 + 1)));
								iTry	= Convert.ToInt16(lastRegTry.Substring(iPos4 + 1, lastRegTry.Length - (iPos4 + 1)));
							}
							catch (Exception 
#if DEBUG
								exc
#endif
								)
							{
								MessageBox.Show("Can not read registration data.\nPlease contact ProfitGrabber Pro support!");
#if DEBUG
								MessageBox.Show(exc.ToString());
#endif
								return;
							}
						}										
					}
								
					if (true == bNew)
					{
						//write initial data
						string sDate = ApplicationHelper.GetDateForRegistry();
						string regInfo = sDate + "/" + registrationPeriod.ToString() + "/" + "1";					

						RegUtil.WriteToReg(RegUtil.REGUTIL_KEYS.HKLM,
                            @"Software\TKS\Profit Grabber Pro\Reg", 
							"C7", 
							regInfo);

						return;
					}
					else //if period and dates match do not increase counter (2nd, 3rd, or Xth start of PG in one day)
					{
						if (iPeriod == registrationPeriod && DateTime.Today == new DateTime (iYear, iMonth, iDay))
						{
							// do nothing
							return;
						}
						else
						{
							//if today is > last registration try --> incerase registration try number 
							if (DateTime.Today > new DateTime(iYear, iMonth, iDay))
							{
								//(if greater than 5 --> remove registration data and files)
								if (iTry >= 5)
								{
									//Delete registration data
									ApplicationHelper.DeleteRegistrationData("Grace period has expired.\n\nProfitGrabber Pro will now deactivate itself.");
								}
								else
								{
									//increase counter in CURRENT period								
									string sDate = ApplicationHelper.GetDateForRegistry();
									string regInfo = sDate + "/" + registrationPeriod.ToString() + "/" + ((int)(iTry + 1)).ToString();	
									string sPeriodId = string.Empty;
																	
									RegUtil.WriteToReg(RegUtil.REGUTIL_KEYS.HKLM,
                                        @"Software\TKS\Profit Grabber Pro\Reg", 
										"C7", 
										regInfo);
									MessageBox.Show("You have " + ((int)(5 - (iTry + 1))).ToString() + " more days left to re-register.\nPlease connect to the Internet and restart ProfitGrabber Pro");
								}
							}																		
						}
					}							
				}
				else
				{
				
					switch (registrationPeriod)
					{						
						case 2:												
							RegUtil.WriteToReg(RegUtil.REGUTIL_KEYS.HKLM,
                                @"Software\TKS\Profit Grabber Pro\Reg", 
								"C3", 
								ApplicationHelper.GetDateForRegistry());
							break;

						case 3:
							RegUtil.WriteToReg(RegUtil.REGUTIL_KEYS.HKLM,
                                @"Software\TKS\Profit Grabber Pro\Reg", 
								"C4", 
								ApplicationHelper.GetDateForRegistry());
							break;

						case 4:
							RegUtil.WriteToReg(RegUtil.REGUTIL_KEYS.HKLM,
                                @"Software\TKS\Profit Grabber Pro\Reg", 
								"C5", 
								ApplicationHelper.GetDateForRegistry());
							break;

						case 5:
							RegUtil.WriteToReg(RegUtil.REGUTIL_KEYS.HKLM,
                                @"Software\TKS\Profit Grabber Pro\Reg", 
								"C6", 
								ApplicationHelper.GetDateForRegistry());						
							break;
					}
				}
			}
			catch (Exception 
#if DEBUG
				exc
#endif
				)
			{
				MessageBox.Show("You must have administrator rights to use ProfitGrabber Pro!");
#if DEBUG
				MessageBox.Show(exc.Message + System.Environment.NewLine + exc.InnerException);
#endif
			}
		}

		internal static string GetDateForRegistry()
		{
			return DateTime.Now.Year + "/" + DateTime.Now.Month + "/" + DateTime.Now.Day;
		}

		internal static void WriteInitialRegistrationData(Guid userId)
		{			
			RegUtil.WriteToReg(RegUtil.REGUTIL_KEYS.HKLM, @"Software\TKS\Profit Grabber Pro\Reg", "C1", userId.ToString());
			RegUtil.WriteToReg(RegUtil.REGUTIL_KEYS.HKLM, @"Software\TKS\Profit Grabber Pro\Reg", "C2", ApplicationHelper.GetDateForRegistry());
			RegUtil.WriteToReg(RegUtil.REGUTIL_KEYS.HKLM, @"Software\TKS\Profit Grabber Pro\Reg", "C3", string.Empty);
			RegUtil.WriteToReg(RegUtil.REGUTIL_KEYS.HKLM, @"Software\TKS\Profit Grabber Pro\Reg", "C4", string.Empty);
			RegUtil.WriteToReg(RegUtil.REGUTIL_KEYS.HKLM, @"Software\TKS\Profit Grabber Pro\Reg", "C5", string.Empty);
			RegUtil.WriteToReg(RegUtil.REGUTIL_KEYS.HKLM, @"Software\TKS\Profit Grabber Pro\Reg", "C6", string.Empty);
			RegUtil.WriteToReg(RegUtil.REGUTIL_KEYS.HKLM, @"Software\TKS\Profit Grabber Pro\Reg", "C7", string.Empty);            
		}

		internal static void DeleteRegistrationData(string message)
		{
			try
			{
				MessageBox.Show(message);
				try
				{
                    File.Delete(OSFolderManager.Instance.GetPlainLocalUserAppDataPath() + @"\profitgrabber.dat");
				}
				catch{}
				
				try
				{
                    File.Delete(OSFolderManager.Instance.GetPlainLocalUserAppDataPath() + @"\am.dat");
				}
				catch{}

				try
				{
					Registry.LocalMachine.DeleteSubKeyTree(@"Software\TKS\Profit Grabber Pro\Reg");									
				}
				catch{}
			}
			catch(Exception exc)
			{
				MessageBox.Show(exc.ToString());
			}
			return;
		}

		internal static void SelfDeactivate()
		{			
			ApplicationHelper.DeleteRegistrationData("Registration period for ProfitGrabber Pro has expired!");
			MessageBox.Show("You may save your work", "Contact ProfitGrabber Support", MessageBoxButtons.OK, MessageBoxIcon.Information);
			return;
		}

		#endregion � SelfDeactivation �

		

		public static ActiveModules GetActiveModulesFromWS(Guid idReg, out string message)
		{			
			message = string.Empty;
			DealMaker.Registration.Service1 service = null;
			try
			{
				service = new DealMaker.Registration.Service1();
			}
			catch (Exception exc)
			{
#if DEBUG
				MessageBox.Show("Message: " + exc.ToString() + "\nInner Exception: " + exc.InnerException);
#endif
				throw new Exception("service = new DealMaker.Registration.Service1()",  exc);	//TA:11.04.2005 - For saving exceptions that may come...
			}
			service.Timeout = 90000;
			
			//read from app.config
			try
			{
				NameValueCollection nvc = (NameValueCollection)ConfigurationSettings.GetConfig("ProfitGrabberWebSupport");
				if (null != nvc && nvc.Count != 0)
				{
					Globals.ProfitGrabberWebSupportURL = (string)nvc["URL"];
					service.Url = (string)nvc["URL"];				
				}
			}
			catch (Exception exc)
			{
				throw new Exception("Reading from app.Config",  exc);	//TA:11.04.2005 - For saving exceptions that may come...
			}

			string msg = string.Empty;
			ActiveModules am = ApplicationHelper.GetActiveModulesFromWS(Globals.UserRegistration.IDUser, service, out msg);
			message = msg;
			return am;
			
		}

		public static ActiveModules GetActiveModulesFromWS(Guid idReg, DealMaker.Registration.Service1 service, out string message)
		{						
			message = string.Empty;
			ActiveModules am = null;
			try
			{
				am = service.GetActiveModules(idReg, out message);
			}
			catch 
			{				
				am = null;
				message = message;
			}
			return am;
		}


		public static bool SaveActiveModules(ActiveModules am)
		{
			bool res = true;
			FileStream fs = null; 
			try
			{
                fs = new FileStream(OSFolderManager.Instance.GetPlainLocalUserAppDataPath() + @"\am.dat", FileMode.OpenOrCreate, FileAccess.Write);
				BinaryFormatter bf=new BinaryFormatter ();
				bf.Serialize (fs, am);
			}
			catch (Exception exc)
			{
				MessageBox.Show("Can not save Active Modules" + System.Environment.NewLine + "Maybe missing [serializable] attributes in reference.cs from web services\n\n" + exc.Message);
				res = false;
			}
			finally
			{
				fs.Close ();
			}
			return res;
		}

		public static ActiveModules LoadActiveModules()
		{
			ActiveModules am = null;

			FileStream fs = null;
            if (!Directory.Exists(OSFolderManager.Instance.GetPlainLocalUserAppDataPath()))
			{
				return null;
			}
			try
			{
                fs = new FileStream(OSFolderManager.Instance.GetPlainLocalUserAppDataPath() + @"\am.dat", FileMode.Open, FileAccess.Read);
			}
			catch (Exception)
			{
				return null;
			}
			if (null != fs)
			{
				try
				{					
					BinaryFormatter bf=new BinaryFormatter ();
					am = (ActiveModules)bf.Deserialize(fs);																			
				}
				catch(Exception exc)				
				{
					MessageBox.Show("Can not load Active Modules" + System.Environment.NewLine + "Maybe missing [serializable] attributes in reference.cs from web services\n\n" + exc.Message);
					am = null;
				}
				finally
				{
					fs.Close ();
				}				
			}
			return am;
		}

		public static bool SavePropertyItem(PropertyItem pi)
		{						
			bool bRes = false;
			ISession session = null;
			ITransaction transaction = null;			
			IDbConnection iSqlConn = null;
						
			try
			{	
				Connection conn = Connection.getInstance();     
				SqlConnection sqlConn = conn.HatchConn;				
				iSqlConn = sqlConn as IDbConnection;
						
				ISessionFactory factory = Globals.factory;
		
		
				if (null != iSqlConn) 
				{
					session = factory.OpenSession(iSqlConn);
					if (ConnectionState.Closed == iSqlConn.State) 
					{
						iSqlConn.Open();
					}
				}
				else 
				{
					session = factory.OpenSession();
				}							
		
				transaction = session.BeginTransaction();							
				pi.NhSaveOrUpdate(session);				
				transaction.Commit();
                bRes = true;
			}
			catch 
#if DEBUG
				(Exception exc)
#endif
			{
#if DEBUG
                //string name = (null != pi && null != pi.Owner) ? pi.Owner.FullName : string.Empty;
                //string siteAddress = (null != pi && null != pi.Owner && null != pi.Owner.SiteAddress) ? pi.Owner.SiteAddress.FullSiteStreetAddress : string.Empty;
                //string mailAddress = (null != pi && null != pi.Owner && null != pi.Owner.MailAddress) ? pi.Owner.MailAddress.FullSiteStreetAddress : string.Empty;
                //string phone = (null != pi && null != pi.Owner && null != pi.Owner.HomePhone) ? pi.Owner.HomePhone.PhoneNr : string.Empty;

                //MessageBox.Show("2" + name + System.Environment.NewLine + siteAddress + System.Environment.NewLine + mailAddress + System.Environment.NewLine + phone + System.Environment.NewLine + exc.ToString());                
#endif
		
				if (!transaction.WasCommitted)
					transaction.Rollback();
		
				bRes = false;
			}				
			finally
			{		
				if (null != session)
				    session.Close();
								
                if (null != iSqlConn)
				    iSqlConn.Close();
			}
			
			return bRes;
		}

		/// <summary>
		/// Returns arraylist of PropertyItemIds which represent contacts in DO NOT MAIL GROUP
		/// </summary>
		/// <returns>ArrayList (count = 0 - nothing found, count > 0 - results found, null = error occured</returns>
		public static ArrayList GetDoNotMailData()
		{
			ArrayList doNotMailData = new ArrayList();
			SqlDataReader dataReader = null;
			
			try
			{				
				string textCmd = "SELECT IdPropertyItem from NodePropertyItems where NodeId = 21";
			
				SqlCommand cmd = new SqlCommand(textCmd, Connection.getInstance().HatchConn);

				if (ConnectionState.Open != Connection.getInstance().HatchConn.State)
					Connection.getInstance().HatchConn.Open();

				dataReader = cmd.ExecuteReader();
				while (dataReader.Read())
				{
					doNotMailData.Add(dataReader.GetGuid(0));
				}
			}
			catch (Exception 
#if DEBUG
				exc
#endif
				)
			{
#if DEBUG
				MessageBox.Show(exc.Message + System.Environment.NewLine + exc.InnerException + 
								System.Environment.NewLine + exc.Source + 
								System.Environment.NewLine + exc.StackTrace);
#endif
				doNotMailData = null;
			}
			finally
			{
				dataReader.Close();	
			}

			return doNotMailData;
		}

		public static bool SaveServerKeyCode(string srvKeyCode)
		{
			bool res = true;
			FileStream fs = null; 
			try
			{
                fs = new FileStream(OSFolderManager.Instance.GetPlainLocalUserAppDataPath() + @"\profitgrabberEx.dat", FileMode.OpenOrCreate, FileAccess.Write);
				BinaryFormatter bf=new BinaryFormatter ();
				bf.Serialize (fs, srvKeyCode);
			}
			catch (Exception exc)
			{
				MessageBox.Show("Can not save Server Key Code!" + System.Environment.NewLine + exc.Message);
				res = false;
			}
			finally
			{
				fs.Close ();
			}
			return res;
		}

		public static string LoadServerKeyCode()
		{			
			FileStream fs = null;
			string srvKeyCode = string.Empty;
            if (false == Directory.Exists(OSFolderManager.Instance.GetPlainLocalUserAppDataPath()))
			{
				return string.Empty;
			}
			try
			{
                fs = new FileStream(OSFolderManager.Instance.GetPlainLocalUserAppDataPath() + @"\profitgrabberEx.dat", FileMode.Open, FileAccess.Read);
			}
			catch (Exception)
			{
				return string.Empty;
			}
			if (null != fs)
			{
				try
				{					
					BinaryFormatter bf=new BinaryFormatter ();
					srvKeyCode = (string)bf.Deserialize(fs);																			
				}
				catch(Exception exc)				
				{
					MessageBox.Show("Can not load Server Key Code" + System.Environment.NewLine + exc.Message);					
				}
				finally
				{
					fs.Close ();
				}				
			}
			return srvKeyCode;
		}


		public static string CaseString(string inputString)
		{			
			bool capitalize = true;
			string outString = string.Empty;

			inputString = inputString.TrimStart();
			inputString = inputString.TrimEnd();
			inputString = inputString.ToLower();

			for (int i = 0; i < inputString.Length; i++)
			{
				if (capitalize)
				{
					outString += Char.ToUpper(inputString[i]);
					capitalize = false;
				}
				else 
				{
					outString += inputString[i];
					if (' ' == inputString[i])
						capitalize = true;					
				}
			}

			return outString;
		}

		public static bool DeleteGroup(int groupId)
		{
			SqlConnection connection = new SqlConnection();
			connection.ConnectionString = Globals.GetConnectionString();			
			connection.Open();
			int retVal = 0;

			try
			{
				SqlCommand sp_delete_Node = new SqlCommand("delete_Node", connection);
				sp_delete_Node.CommandType = CommandType.StoredProcedure;					
				sp_delete_Node.Parameters.Add("@nodeId", groupId);	

				SqlParameter param = sp_delete_Node.Parameters.Add("@retVal", retVal);
				param.Direction = ParameterDirection.Output;
									
				if (ConnectionState.Open != Connection.getInstance().HatchConn.State)
					Connection.getInstance().HatchConn.Open();

				sp_delete_Node.ExecuteNonQuery();
				if (-1 == (int)param.Value)
				{
					return false;
				}
			}
			catch 
			{
				return false;
			}
	
			
			return true;
		}

        public static async Task<int> MoveGroup(int sourceGroupId, int destinationGroupId)
        {
            try
            {
                using (var connection = new SqlConnection())
                {
                    connection.ConnectionString = Globals.GetConnectionString();
                    connection.Open();

                    var testSourceCmd = new SqlCommand("update node set parentNodeId = @parentNodeId where nodeId = @nodeId", connection);
                    testSourceCmd.Parameters.AddWithValue("@parentNodeId", destinationGroupId);
                    testSourceCmd.Parameters.AddWithValue("@nodeId", sourceGroupId);
                    
					return await testSourceCmd.ExecuteNonQueryAsync();
                }
			}
            catch
            {
                throw;
            }
        }

		

		public static bool UpdateGroupName(int groupID, string newGroupName, string newGroupDesc)
		{
			try
			{														
				SqlConnection connection = new SqlConnection();
				connection.ConnectionString = Globals.GetConnectionString();
				connection.Open();
				SqlCommand sp_update_Node = new SqlCommand("update_Node", connection);
				sp_update_Node.CommandType = CommandType.StoredProcedure;			
				sp_update_Node.Parameters.Add("@nodeId", groupID);
				sp_update_Node.Parameters.Add("@nodeName", newGroupName);
				sp_update_Node.Parameters.Add("@nodeDesc", newGroupDesc);

				if (ConnectionState.Open != Connection.getInstance().HatchConn.State)
					Connection.getInstance().HatchConn.Open();

				sp_update_Node.ExecuteNonQuery();
			}
			catch
			{				
				return false; 
			}	
			return true;
		}

        public static bool IsCellPhoneEmailAddress(string emailAddress, out bool cell)
        {            
            cell = true;

            if (null != emailAddress && string.Empty != emailAddress)
            {
                string[] arr = emailAddress.Split('@');

                if (null != arr && 2 == arr.Length)
                {
                    string to = arr[0];
                    to = to.Trim();

                    foreach (char c in to)
                    {
                        if (!char.IsDigit(c))
                        {
                            cell = false;
                            return true;
                        }
                    }
                }
                else
                    return false;
            }
            else
                return false;

            return true;
        }

        public static bool CreateRVMPhoneListForNode(int nodeId, out string filePath)
        {
            filePath = string.Empty;
            List<ProfitGrabber.RVM.Common.RVMPhoneOwner> list = new List<ProfitGrabber.RVM.Common.RVMPhoneOwner>();

            Hashtable propertyItemIds = ApplicationHelper.GetPropertyItemsInNode(nodeId);
            var enumerator = propertyItemIds.GetEnumerator();
            while (enumerator.MoveNext())
            {
                if (enumerator.Value is Guid)
                {
                    var pi = ApplicationHelper.LoadPropertyItem((Guid)enumerator.Value);
                    var name = string.IsNullOrEmpty(pi?.Owner?.FullName?.Trim()) ? "-" : pi?.Owner?.FullName?.Trim().Replace(' ', '-');
                    string phoneNumber = null;
                    string address = null;

                    address = pi.Owner.SiteAddress.FullSiteStreetAddress;

                    if (!string.IsNullOrEmpty(pi?.Owner?.CellPhone?.PhoneNr))
                        phoneNumber = pi?.Owner?.CellPhone?.PhoneNr;
                    else
                    {
                        if (!string.IsNullOrEmpty(pi?.Owner?.STCellphone))
                            phoneNumber = pi?.Owner?.STCellphone;
                    }

                    if (null != phoneNumber)
                        list.Add(new ProfitGrabber.RVM.Common.RVMPhoneOwner { Address = address, PhoneNr = phoneNumber });
                }                
            }

            if (!list.Any())
                return false;

            var path = $@"{Application.StartupPath}\RVM\";

            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);

            filePath = $"{path}{Guid.NewGuid()}.csv";

            using (var sw = new StreamWriter(filePath))
            {
                foreach (var pmo in list)
                    sw.WriteLine(pmo.ToString());

                sw.Flush();
            }

            return true;
        }

		public static ProfitGrabber.RVM.DropCowboy.DTO.Request.AddContactsToList.AddContactToListRequest CreateDropCowboyRequest(int nodeId, Tuple<bool, bool, bool> selectedPhoneTypeFlags)
		{
			ProfitGrabber.RVM.DropCowboy.DTO.Request.AddContactsToList.AddContactToListRequest req = new ProfitGrabber.RVM.DropCowboy.DTO.Request.AddContactsToList.AddContactToListRequest();

			req.Region = "US";
			req.Fields = new string[] { "record_id",
										"first_name",
										"last_name",										
										"email",
										"email",
										"phone",
										"phone",
										"phone",
										"phone",
										"address1",										
										"city",
										"state",
										"postal"										
					};
			

			Hashtable propertyItemIds = ApplicationHelper.GetPropertyItemsInNode(nodeId);
			var enumerator = propertyItemIds.GetEnumerator();
			var resList = new List<string[]>();
			while (enumerator.MoveNext())
			{
				if (enumerator.Value is Guid)
				{
					var pi = ApplicationHelper.LoadPropertyItem((Guid)enumerator.Value);
					if (null == pi)
						continue;

					//defaults
					var phone0 = pi.Owner.CellPhoneString?.Trim();
					var phone1 = pi.Owner.HomePhoneString?.Trim();
					var phone2 = pi.Owner.STCellphone?.Trim();
					var phone3 = pi.Owner.STLandline?.Trim();

					SkiptraceClientResultset skipTracedResponse = null;
					if (!string.IsNullOrEmpty(pi.Owner?.ST_P1_RESPONSE))
						try { skipTracedResponse = JsonConvert.DeserializeObject<SkiptraceClientResultset>(pi.Owner.ST_P1_RESPONSE); }
						catch { skipTracedResponse = null; }


					if (null != skipTracedResponse?.MobilePhones && skipTracedResponse?.MobilePhones.Count > 0)
						phone0 = (skipTracedResponse.MobilePhones[0]);

					if (selectedPhoneTypeFlags.Item2)
					{						
                        if (null != skipTracedResponse?.MobilePhones && skipTracedResponse?.MobilePhones.Count > 1)
                            phone1 = skipTracedResponse.MobilePhones[1];
					}
					else
						phone1 = string.Empty;

					if (selectedPhoneTypeFlags.Item2)
					{
						if (null != skipTracedResponse?.MobilePhones && skipTracedResponse?.MobilePhones.Count > 2)
							phone2 = skipTracedResponse.MobilePhones[2];
					}
					else
						phone2 = string.Empty;

					var na = "N/A";
					if (string.IsNullOrEmpty(phone0) && string.IsNullOrEmpty(phone1) && string.IsNullOrEmpty(phone2) || phone0 == na && phone1 == na && phone2 == na)
						continue;

										
					var propList = new List<string>
					{
						pi.IdPropertyItem.ToString(),

						pi.Owner?.FirstName?.Trim(),
						pi.Owner?.LastName?.Trim(),						

						pi.Owner.EMail?.Trim(),
						pi.Owner.STEmail?.Trim(),

						phone0,
						phone1,
						phone2,
						phone3,					

						pi.Owner?.SiteAddress?.FullSiteStreetAddress?.Trim() ?? string.Empty,
						pi.Owner?.SiteAddress?.SiteCity?.Trim() ?? string.Empty,
						pi.Owner?.SiteAddress?.SiteState?.Trim() ?? string.Empty,
						pi.Owner?.SiteAddress?.SiteZIP?.Trim() ?? string.Empty
					};
					resList.Add(propList.ToArray());
				}
			}

			req.Values = resList.ToArray();

			return req;
		}

		public static bool FindGroup(string origin, out int _nodeId, SqlConnection dbConn)
		{
			_nodeId = -1;
			SqlDataReader dataReader = null;
			try
			{
				SqlCommand command = new SqlCommand($"select nodeId from Node where nodeName = '{origin}'", dbConn);
				dataReader = command.ExecuteReader();
				while (dataReader.Read())
				{
					_nodeId = dataReader.GetInt32(0);
				}
			}
			catch (Exception exc)
			{
				_nodeId = -1;
				string s = exc.ToString();
			}
			finally
			{
				if (null != dataReader && false == dataReader.IsClosed)
				{
					dataReader.Close();
				}
			}

			if (-1 == _nodeId)
			{
				return false;
			}
			return true;
		}

		internal static Dictionary<int, string> GetSubGroups(int parentNode)
		{
			var res = new Dictionary<int, string>();

			SqlDataReader nodeReader = null;
			try
			{
				SqlCommand sp_getChildrenFromNode = new SqlCommand("getChildrenFromNode", Connection.getInstance().HatchConn);
                sp_getChildrenFromNode.Parameters.Add("@parentNode", parentNode);
				sp_getChildrenFromNode.CommandType = CommandType.StoredProcedure;

				if (ConnectionState.Open != Connection.getInstance().HatchConn.State)
					Connection.getInstance().HatchConn.Open();

				sp_getChildrenFromNode.ExecuteNonQuery();

				nodeReader = sp_getChildrenFromNode.ExecuteReader();
				if (nodeReader.HasRows)
				{
					while (nodeReader.Read())
					{
						var nodeId = nodeReader.GetInt32(0);
						var nodeName = nodeReader.GetString(2).Trim();
						res.Add(nodeId, nodeName);
					}
				}
			}
			catch (Exception exc)
			{
				MessageBox.Show(exc.Message);
			}
			finally
			{
				nodeReader.Close();
			}

			return res;
		}

	}        

	#endregion � Class ApplicationHelper �

}
#endregion � Namespace DealMaker � 