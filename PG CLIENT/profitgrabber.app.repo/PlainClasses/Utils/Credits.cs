using System;
using DealMaker.DataConsumption;

namespace DealMaker
{
	/// <summary>
	/// id : 0 - "Ultimate Data Source COMPS" 
	/// id : 1 - "Ultimate Data Source Property Profile"
	/// id : 2 - "Matching Module"
	/// </summary>
	public enum TypeOfCharge
	{
		COMPS = 0, 
		PP = 1,
		MM = 2,
	}

	/// <summary>
	/// Summary description for Credits.
	/// </summary>
	public sealed class UserCredits
	{
		static readonly UserCredits _instance=new UserCredits();

		static UserCredits()
		{			
		}

		UserCredits()
		{			
		}

		public static UserCredits Instance
		{
			get
			{
				return _instance;
			}
		}

		public bool QueryAccount (Guid userId, out int monthlyCredits, out int topUpCredits)
		{
			monthlyCredits = 0;
			topUpCredits = 0;

			ServiceDataConsumption sdc = new ServiceDataConsumption();

            if (string.Empty != Globals.ProfitGrabberWebSupportURLDataConsumption)
            {
                sdc.Url = Globals.ProfitGrabberWebSupportURLDataConsumption;
            }

            try
			{
				return sdc.QueryAcc(userId, out monthlyCredits, out topUpCredits);
			}
			catch 
			{
				return false;
			}
		}		
		
		public bool TestForEnough(string keyCode, TypeOfCharge typeOfCharge, int amount, out int totalCost, out int totalAmount, out int monthlyCredits, out int topUpCredits, out string outMsg)
		{			
			monthlyCredits = 0;
			topUpCredits = 0;
			totalAmount = 0;
			totalCost = 0;
			Guid userId = Guid.Empty;
			string message = string.Empty;
			
			ServiceDataConsumption sdc = new ServiceDataConsumption();

            if (string.Empty != Globals.ProfitGrabberWebSupportURLDataConsumption)
            {
                sdc.Url = Globals.ProfitGrabberWebSupportURLDataConsumption;
            }

            try
			{
				bool queryGuidFromKey = sdc.QueryGuidFromKey(keyCode, out userId, out message);				
				bool queryAccountRes = sdc.QueryAcc(userId, out monthlyCredits, out topUpCredits);
				bool requestSucc = sdc.QueryDataRequestSuccess(userId, amount, (int)typeOfCharge, out totalCost, out totalAmount, out outMsg);

				if (!queryAccountRes)
					outMsg += Environment.NewLine + "Can't get account balance.";

				return queryAccountRes && requestSucc;
			}
			catch
			{
				outMsg = "Error";
				return false;
			}
		}

	}
}
