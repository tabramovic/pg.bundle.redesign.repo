using System;
using System.Text;

namespace DealMaker.PlainClasses.Utils
{
	/// <summary>
	/// Summary description for GUIDtoCODA39.
	/// </summary>
	public class GUIDtoCODA39
	{
		public GUIDtoCODA39()
		{
		}

		public static string GetCODA39(System.Guid guid)
		{
			char[] pool = {'0','1','2','3','4','5','6','7','8','9','Q','W','E','R','T','Y','U','I','O','P','A','S','D','F','G','H','J','K','L','Z','X','C','V','B','N','M'};

			int i;

			byte[] guidArray = guid.ToByteArray();

			char[] charCode = new char[8];

			for(i=0;i<16;i+=2)
				charCode[i/2]=pool[(guidArray[i]^guidArray[i+1])%pool.Length];

			return '*' + new string(charCode) + '*';
		}

		public static bool IsGUID(string coda, System.Guid guid)
		{
			return String.Compare(GetCODA39(guid), "*" + coda.ToUpper() + "*") == 0;
		}
	}
}
