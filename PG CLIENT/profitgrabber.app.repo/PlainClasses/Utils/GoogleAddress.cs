using System;

namespace DealMaker
{
	/// <summary>
	/// Summary description for GoogleAddress.
	/// </summary>
	public class GoogleAddress
	{
		public GoogleAddress()
		{
		}

		public static void OpenAddress(Address address)
		{
			string query = 
				address.SiteStreetNumber + " " +
				address.SiteStreetName + ", " +
				address.SiteCity + ", " +
				address.SiteState + " " +
				address.SiteZIP;

			query = query.Replace(" ","+");

			System.Diagnostics.Process.Start("http://maps.google.com/maps?q="+query);
		}
		
		public static void OpenAddress (string siteStreetNumber, string siteStreetName, string siteCity, string siteState, string siteZIP)
		{
			string query = 
				siteStreetNumber + " " +
				siteStreetName + ", " +
				siteCity + ", " +
				siteState + " " +
				siteZIP;

			query = query.Replace(" ","+");

			System.Diagnostics.Process.Start("http://maps.google.com/maps?q="+query);
		}
	}
}