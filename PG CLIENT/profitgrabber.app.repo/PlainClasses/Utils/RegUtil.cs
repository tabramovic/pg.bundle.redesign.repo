using System;
using Microsoft.Win32; 

namespace DealMaker
{

	public class RegUtil
	{ 
		public const int REG_SUCCESS = 0;
		public const int REG_ERR_NULL_REFERENCE = 12;
		public const int REG_ERR_SECURITY = 13; 
		public const int REG_ERR_UNKNOWN = 14; 
		public enum REGUTIL_KEYS 
		{
			HKCR = 0,
			HKCU = 1,
			HKLM = 2,
			HKU = 3,
			HKCC = 4
		}

		private static char[] Delimit = {'\x005c'};  //Hex for '\'

		public RegUtil()
		{} 

		public static int WriteToReg(REGUTIL_KEYS Key, string RegPath, string KeyName, string KeyValue)
		{
			string[] RegString;

			RegString = RegPath.Split(Delimit); 

			//First item of array will be the base key, so be carefull iterating below
			RegistryKey[] RegKey = new RegistryKey[RegPath.Length + 1]; 

			//Returns proper key --  Will Default to hkey_current_user
			RegKey[0] = SelectKey(Key); 

			for(int i = 0;i < RegString.Length;i++)
			{ 

				RegKey[i + 1] = RegKey[i].OpenSubKey(RegString[i], true);
				//If key does not exist, create it.  This logic usually suits my needs, but 
				//you may change it if you wish.
				if (RegKey[i + 1] == null)  
				{
					RegKey[i + 1] = RegKey[i].CreateSubKey(RegString[i]);
				}
			} 
			//Write the value to the registry.  If fail, return the constant values defined at the beginning of the class
			try
			{
				RegKey[RegString.Length].SetValue(KeyName, KeyValue);     
			}
			catch (System.NullReferenceException)
			{
				return REG_ERR_NULL_REFERENCE;
			}
			catch (System.UnauthorizedAccessException)
			{
				return REG_ERR_SECURITY;
			}

			return REG_SUCCESS; 
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="Key"> </param>
		/// <param name="RegPath"> </param>
		/// <param name="KeyName"> </param>
		/// <param name="DefaultValue"> </param>
		public static string ReadFromRegistry(REGUTIL_KEYS Key, string RegPath, string KeyName, string DefaultValue)
		{
			string[] RegString;
			string Result = ""; 
			RegString = RegPath.Split(Delimit);
			//First item of array will be the base key, so be carefull iterating below
			RegistryKey[] RegKey = new RegistryKey[RegPath.Length + 1]; 
			//Returns proper key --  Will Default to HKEY_CURRENT_USER
			RegKey[0] = SelectKey(Key); 
			for(int i = 0;i < RegString.Length;i++)
			{
				if (null != RegKey[i])
				{
					RegKey[i + 1] = RegKey[i].OpenSubKey(RegString[i]);
					if (i == RegString.Length - 1)
					{
						Result = (string)RegKey[i + 1].GetValue(KeyName, DefaultValue);
					}
				}
			} 
			return Result; 
		}  


		//Separated for cleanliness
		private static RegistryKey SelectKey(REGUTIL_KEYS Key)
		{
			switch (Key)
			{
				case REGUTIL_KEYS.HKCR:
					return Registry.ClassesRoot;
				case REGUTIL_KEYS.HKCU:
					return Registry.CurrentUser;
				case REGUTIL_KEYS.HKLM:
					return Registry.LocalMachine;
				case REGUTIL_KEYS.HKU:
					return Registry.Users;
				case REGUTIL_KEYS.HKCC:
					return Registry.CurrentConfig;
				default:
					return Registry.CurrentUser;
			}
		}
	}
}
