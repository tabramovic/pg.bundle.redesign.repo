﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;

namespace DealMaker
{
    public sealed class MediaObject
    {
        static readonly MediaObject _instance = new MediaObject();

        static MediaObject()
        {            
        }

        MediaObject()
        {
            ScanMedia();
        }

        public static MediaObject Instance
        {
            get { return _instance; }
        }

        List<Media> _mediaList = new List<Media>();

        public List<Media> MediaList
        {
            get { return _mediaList; }
        }

        public void ScanMedia()
        {
            _mediaList = GetMediaFromDatabase();
        }

        List<Media> GetMediaFromDatabase()
        {
            List<Media> medList = new List<Media>();
            ArrayList alMedia = ApplicationHelper.GetMediaList();

            foreach (Guid mediaId in alMedia)
            {
                Media med = ApplicationHelper.LoadMedia(mediaId);
                if ((null != med) && (!ContainsValue(medList, med)))
                    medList.Add(med);
            }

            return medList;
        }

        bool ContainsValue(List<Media> mediaList, Media media)
        {
            if (null != mediaList && null != media && null != media.MediaName)
            {
                foreach (Media m in mediaList)
                {
                    if (m.MediaName == media.MediaName)
                        return true;
                }
            }

            return false;
        }
    }
}
