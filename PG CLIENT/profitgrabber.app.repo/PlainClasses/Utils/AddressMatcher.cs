﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DealMaker.PlainClasses.Utils
{
    public sealed class AddressMatcher
    {
        static readonly AddressMatcher _instance = new AddressMatcher();

        static AddressMatcher()
        { }

        AddressMatcher()
        {
            InitLists();
        }

        public static AddressMatcher Instance
        {
            get { return _instance; }
        }

        List<string> avArray = new List<string>();        
        List<string> stArray = new List<string>();    
        List<string> blArray = new List<string>();


        void InitLists()
        {
            avArray.Add("AVENUE");
            avArray.Add("AVE");
            avArray.Add("AV");

            stArray.Add("STREET");
            stArray.Add("STR");
            stArray.Add("ST");

            blArray.Add("BOULEVARD");
            blArray.Add("BLVD");
            blArray.Add("BL");
        }

        public bool Match(string addr1, string addr2)
        {
            if (null == addr1 || null == addr2)
                return false;

            string newAddr1 = string.Empty;
            string newAddr2 = string.Empty;

            addr1 = addr1.Replace(" ,", ",");
            addr2 = addr2.Replace(" ,", ",");

            
            addr1 = addr1.Replace('.', ' ');
            addr1 = addr1.Trim();
            addr1 = addr1.ToUpper();

            addr2 = addr2.Replace('.', ' ');
            addr2 = addr2.Trim();
            addr2 = addr2.ToUpper();
            
            string[] sAddr1 = addr1.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            string[] sAddr2 = addr2.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            
            if (sAddr1.Length != sAddr2.Length)
                return false;

            for (int i = 0; i < sAddr1.Length; i++)
            {
                if (sAddr1[i] == sAddr2[i])
                {
                    //NOP
                }
                else
                {
                    if (
                        (avArray.Contains(sAddr1[i]) && avArray.Contains(sAddr2[i])) ||
                        (stArray.Contains(sAddr1[i]) && stArray.Contains(sAddr2[i])) ||
                        (blArray.Contains(sAddr1[i]) && blArray.Contains(sAddr2[i])))
                    {
                        //NOP
                    }
                    else
                    {
                        return false;
                    }
                }
            }

            return true;
        }

    }
}
