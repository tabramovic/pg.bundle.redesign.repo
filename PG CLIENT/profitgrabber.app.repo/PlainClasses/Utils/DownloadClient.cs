﻿using System;
using System.Net;
using System.IO;

using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DealMaker.PlainClasses.Utils
{
    public class DownloadClient
    {
        public DownloadClient()
        {

        }

        public void DownloadFile(Uri address, string filename, string path)
        {
            using (WebClient wc = new WebClient())
            {
                wc.DownloadFileAsync(address, $@"{path}\{filename}");
            }
        }
    }
}
