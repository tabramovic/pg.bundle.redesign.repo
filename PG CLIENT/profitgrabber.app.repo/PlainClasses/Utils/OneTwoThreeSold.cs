using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.Threading;
using System.IO;
using System.Collections.Specialized;
using System.Configuration;
using System.Windows.Forms;

//Added
using DealMaker.WebSitesSupportService;

namespace DealMaker
{
	/// <summary>
	/// Summary description for OneTwoThreeSold.
	/// </summary>
	public class OneTwoThreeSold
	{		
        //private DateTime _timeStamp = new DateTime(2000, 1, 1);        
        //private SqlConnection dbConnection;
        //private ArrayList alGroupsToRefresh = new ArrayList();
        //private ArrayList alOrigins = new ArrayList();

        //string _aahFolder = string.Empty;

        //private int initialDelay = 10000;
        //private int nextWaitingPeriod = 100000;				
		public delegate void NewGroupAddedBy123Sold(int nodeId);
		public static event NewGroupAddedBy123Sold REFRESH_GROUPS_BY_123SOLD;
		
		public delegate void NewContactInGroupFrom123Sold(int nodeId);
		public static event NewContactInGroupFrom123Sold NEW_CONTACT_IN_GROUP_BY_123_SOLD;

		protected void OnNewGroup(int nodeId)
		{
			if (null != REFRESH_GROUPS_BY_123SOLD)
			{
				REFRESH_GROUPS_BY_123SOLD(nodeId);
			}
		}

		protected void OnNewContact(int nodeId)
		{
			if (null != NEW_CONTACT_IN_GROUP_BY_123_SOLD)
			{
				NEW_CONTACT_IN_GROUP_BY_123_SOLD(nodeId);
			}
		}


        //private Thread worker = null;
        //private const string LastFetchFileName = @"\LastFetch.txt";
        //private const int LeadsNotProcessedYetGroupId = 6;
        //private const string AutoLeadsGroups = "Auto-Leads Groups";
        //private bool firstTimeRun = true;
        //private DateTime lastFetch = new DateTime(2006, 1, 1);		

        //DealMaker.AAHIntegration.Service1 service123Sold = null;
        //DealMaker.AAHIntegration.CompositeContactOriginDealProperty[] compositeProperties = null;		
        //DealMaker.AAHIntegration.Contact _contact = new DealMaker.AAHIntegration.Contact();

		

		public OneTwoThreeSold(string userAppFolder)
		{
            //_aahFolder = userAppFolder;

            //if (Globals.NwVerEnabled)
            //    this.dbConnection = new SqlConnection(@"user id=sa;password=xxxxx;persist security info=False;Data Source=" + Globals.DatabaseServer + ";Initial Catalog=" + Globals.DatabaseName + ";");			
            //else
            //    this.dbConnection = new SqlConnection(@"Data Source=" + Globals.DatabaseServer + ";Initial Catalog=" + Globals.DatabaseName + ";Integrated Security=SSPI;");

            //service123Sold = new DealMaker.AAHIntegration.Service1();	
            //this.InitLastFetchTime();

            //service123Sold.Timeout = 90000;			
						
            //this.worker = new Thread(new ThreadStart(CheckForNewEntries));			
            //worker.Start();								
		}

        /*
		private void InitLastFetchTime()
		{
			StreamReader sr = null;
			try
			{
                if (!Directory.Exists(_aahFolder))
                    Directory.CreateDirectory(_aahFolder);

                if (true == File.Exists(_aahFolder + LastFetchFileName))
				{
                    sr = new StreamReader(_aahFolder + LastFetchFileName);					
					string lastFetchFromFile = sr.ReadLine();
					//this._timeStamp = DateTime.FromFileTime(Convert.ToInt64(lastFetchFromFile));
                    this._timeStamp = new DateTime(Convert.ToInt64(lastFetchFromFile));

                    
				}				
			}
			catch (Exception 
#if DEBUG
				e
#endif
				)
			{
#if DEBUG
				System.Windows.Forms.MessageBox.Show("Can�t get time !" + System.Environment.NewLine + e.Message);				
#endif
			}
			finally 
			{
				if (null != sr)
				{
					sr.Close();
				}
			}
		}
        */		
		/*
		private void CheckForNewEntries()
		{			
			//Guid key = new Guid("3C2AA973-F68A-45dc-9395-89B727C2A6C9");
			int mSecs = 0;	
			while (false == Globals.AbortAllThreads)
			{
				int delay = 0;
				if (true == this.firstTimeRun)
				{
					delay = this.initialDelay;
					this.firstTimeRun = false;		
				}
				else
				{
					delay = this.nextWaitingPeriod;
				}

				mSecs = 0;				
				while (mSecs < delay && false == Globals.AbortAllThreads)
				{
					if (true == Globals.breakWaitingPeriod)
					{							
						break;
					}
					Thread.Sleep(1000);						
					mSecs += 1000;
				}
				if (true == Globals.AbortAllThreads)
				{
					break;
				}					
							


				try
				{												
					//Guid debugIdPgUser = new Guid("6C7330E1-AEB2-497D-A1C1-C5958A41BF3B");
					//DateTime debugdt = new DateTime(2000, 1, 1);
					//this.compositeProperties = service123Sold.GetEntry(key, DateTime.Now, true);

					//INTEGRATED MODE
					//this.compositeProperties = null;
					string outMsg = string.Empty;

                    //DateTime dtx = new DateTime(DateTime.Now.Ticks, DateTimeKind.Unspecified);

                    //this.compositeProperties = service123Sold.GetEntry_V3(Globals.UserRegistration.IDUser, this._timeStamp.Ticks, false, out outMsg);
					//this.compositeProperties = service123Sold.GetEntry(Globals.UserRegistration.IDUser, this._timeStamp, false, out outMsg);
                    //this.compositeProperties = service123Sold.GetEntry_V2(Globals.UserRegistration.IDUser, _lastFetchFromFile, false, out outMsg);		
				}
				catch (Exception
#if DEBUG 
					e
#endif
					)
				{
#if DEBUG
					System.Windows.Forms.MessageBox.Show("WS could not be contacted" + System.Environment.NewLine + e.Message);
#endif
				}
				finally
				{
					//this.lastFetch = DateTime.Now;
				}
			
				Hashtable htEntries = new Hashtable();
				//Contact[] htEntries = service123Sold.Select(this._reg, this._contact, DateTime.Now);
				bool succSaving = true;
				if (null != this.compositeProperties && this.compositeProperties.Length > 0)
				{
					this.alOrigins.Clear();
					foreach (CompositeContactOriginDealProperty codp in this.compositeProperties)				
					{
						
						if (codp.TimeStamp >= _timeStamp)
						{
							_timeStamp = codp.TimeStamp;
							_timeStamp = _timeStamp.AddSeconds(1);
						}

                        DealMaker.AAHIntegration.AdvancedContact _advContact = codp.advancedContact;
                        DealMaker.AAHIntegration.DealProperty _dealProperty = codp.DealProperty;
						string _origin = codp.Origin.P_OriginDesc;

						if (false == this.alOrigins.Contains(_origin))
						{
							this.alOrigins.Add(_origin);
						}


						//1.) Create new PI
						PropertyItem pi = new PropertyItem();
						pi.save = true;
						pi.Owner = new Contact();
						pi.Owner.save = true;

						#region � ADV CONTACT �
						if (null != _advContact)
						{
						
							pi.Owner.FirstName = _advContact.P_FirstName;
							pi.Owner.MiddleName = _advContact.P_MiddleName;
							pi.Owner.LastName = _advContact.P_LastName;
							pi.Owner.FullName = _advContact.P_FullName;
							pi.Owner.GoBy = _advContact.P_GoBy;
							pi.Owner.SalutationName = _advContact.P_SalutationName;
				
							pi.Owner.SiteAddress = new Address();	//FILL
							pi.Owner.SiteAddress.save = true;
							if (null != _advContact.ADV_Address)
							{
								pi.Owner.SiteAddress.SiteStreetName				= _advContact.ADV_Address.P_SiteStreetName;
								pi.Owner.SiteAddress.SiteStreetNumber			= _advContact.ADV_Address.P_SiteStreetNumber;
								pi.Owner.SiteAddress.SiteStreetPostDirectional	= _advContact.ADV_Address.P_SiteStreetPostDirectional;
								pi.Owner.SiteAddress.SiteStreetPreDirectional	= _advContact.ADV_Address.P_SiteStreetPreDirectional;
								pi.Owner.SiteAddress.SiteStreetSuffix			= _advContact.ADV_Address.P_siteStreetSuffix;
								pi.Owner.SiteAddress.SiteStreetUnitNumber		= _advContact.ADV_Address.P_SiteStreetUnitNumber;

								pi.Owner.SiteAddress.SiteCity					= _advContact.ADV_Address.P_SiteCity;
								pi.Owner.SiteAddress.SiteCitySiteState			= _advContact.ADV_Address.P_SiteCitySiteState;
								pi.Owner.SiteAddress.SiteCitySiteStateSiteZIP	= _advContact.ADV_Address.P_SiteCitySiteStateSiteZIP;
						
								pi.Owner.SiteAddress.SiteCountry				= _advContact.ADV_Address.P_SiteCountry;
								pi.Owner.SiteAddress.SiteState					= _advContact.ADV_Address.P_SiteState;
								pi.Owner.SiteAddress.SiteZIP					= _advContact.ADV_Address.P_SiteZIP;
						
								pi.Owner.SiteAddress.FullSiteStreetAddress		= _advContact.ADV_Address.P_FullSiteStreetAddress;

								pi.Owner.SiteAddress.FullSiteStreetAddress = pi.Owner.SiteAddress.FullSiteStreetAddress.TrimEnd(pi.Owner.SiteAddress.SiteZIP.ToCharArray());
								pi.Owner.SiteAddress.FullSiteStreetAddress = pi.Owner.SiteAddress.FullSiteStreetAddress.Trim();
								pi.Owner.SiteAddress.FullSiteStreetAddress = pi.Owner.SiteAddress.FullSiteStreetAddress.TrimEnd(pi.Owner.SiteAddress.SiteState.ToCharArray());
								pi.Owner.SiteAddress.FullSiteStreetAddress = pi.Owner.SiteAddress.FullSiteStreetAddress.Trim();
								pi.Owner.SiteAddress.FullSiteStreetAddress = pi.Owner.SiteAddress.FullSiteStreetAddress.TrimEnd(pi.Owner.SiteAddress.SiteCity.ToCharArray());
								pi.Owner.SiteAddress.FullSiteStreetAddress = pi.Owner.SiteAddress.FullSiteStreetAddress.Trim();
							}							

							pi.Owner.HomePhone = new Phone();		//FILL
							pi.Owner.HomePhone.save = true;
							if (null != _advContact.ADV_HomePhone)
							{
								pi.Owner.HomePhone.PhoneNr						= _advContact.ADV_HomePhone.P_PhoneNr;
								pi.Owner.HomePhone.BestTimeToCall				= _advContact.ADV_HomePhone.P_BestTimeToCall;
							}

							pi.Owner.WorkPhone = new Phone();		//FILL
							pi.Owner.WorkPhone.save = true;
							if (null != _advContact.ADV_WorkPhone)
							{
								pi.Owner.WorkPhone.PhoneNr						= _advContact.ADV_WorkPhone.P_PhoneNr;
								pi.Owner.WorkPhone.BestTimeToCall				= _advContact.ADV_WorkPhone.P_BestTimeToCall;
							}

							pi.Owner.CellPhone = new Phone();		//FILL
							pi.Owner.CellPhone.save = true;
							if (null != _advContact.ADV_CellPhone)
							{
								pi.Owner.CellPhone.PhoneNr						= _advContact.ADV_CellPhone.P_PhoneNr;
								pi.Owner.CellPhone.BestTimeToCall				= _advContact.ADV_CellPhone.P_BestTimeToCall;
							}
						
							pi.Owner.EMail = _advContact.P_eMail;

							pi.DealProperty = new DealProperty();	//FILL
							pi.DealProperty.save = true;
							if (null != _dealProperty)
							{
								pi.DealProperty.Apn				= _dealProperty.P_Apn;
								pi.DealProperty.Arv				= _dealProperty.P_Arv;
								pi.DealProperty.Baths			= _dealProperty.P_Baths;
								pi.DealProperty.Bedrooms		= _dealProperty.P_Bedrooms;
								pi.DealProperty.CanCancelList	= _dealProperty.P_CanCancelList;
								pi.DealProperty.DesiredClosingDate = _dealProperty.P_DesiredClosingDate;
								pi.DealProperty.FeatureCondition = _dealProperty.P_FeatureCondition;
								pi.DealProperty.FirstBal		= _dealProperty.P_FirstBal;
								pi.DealProperty.ForSaleDate		= _dealProperty.P_ForSaleDate;
								if (pi.DealProperty.ForSaleDate < new DateTime(1753, 1, 1))
								{
									pi.DealProperty.ForSaleDate = new DateTime(1753, 1, 1);
								}
								pi.DealProperty.Garage			= _dealProperty.P_Garage;
								pi.DealProperty.Hoa				= _dealProperty.P_Hoa;
								pi.DealProperty.InForecl		= _dealProperty.P_InForecl;
								pi.DealProperty.Ir1				= _dealProperty.P_Ir1;
								pi.DealProperty.Ir2				= _dealProperty.P_Ir2;
								pi.DealProperty.Ir3				= _dealProperty.P_Ir3;
								pi.DealProperty.LeadSource		= _dealProperty.P_LeadSource;
								pi.DealProperty.Listed			= _dealProperty.P_Listed;
								pi.DealProperty.ListExpDate		= _dealProperty.P_ListExpDate;
								if (pi.DealProperty.ListExpDate < new DateTime(1753, 1, 1))
								{
									pi.DealProperty.ListExpDate = new DateTime(1753, 1, 1);
								}
								pi.DealProperty.LowestPriceAcceptable = _dealProperty.P_LowestPriceAcceptable;
								pi.DealProperty.MinCashNeededWhy = _dealProperty.P_MinCashNeededWhy;
								pi.DealProperty.MoBehind		= _dealProperty.P_MoBehind;
								pi.DealProperty.MoPay1			= _dealProperty.P_MoPay1;
								pi.DealProperty.MoPay2			= _dealProperty.P_MoPay2;
								pi.DealProperty.MoPay3			= _dealProperty.P_MoPay3;
								pi.DealProperty.Motivation		= _dealProperty.P_Motivation;
								pi.DealProperty.NextStep		+= "Main objective: " +  _dealProperty.P_Motivation + System.Environment.NewLine +  _dealProperty.P_NextStep;
								pi.DealProperty.OccupiedBy		= _dealProperty.P_OccupiedBy;
								pi.DealProperty.OtherLiens		= _dealProperty.P_OtherLiens;
								pi.DealProperty.PmntsCurrent	= _dealProperty.P_PmntsCurrent;
								pi.DealProperty.Pool			= _dealProperty.P_Pool;
								pi.DealProperty.ReInsAmnt		= _dealProperty.P_ReInsAmnt;
								pi.DealProperty.Repairs			= _dealProperty.P_Repairs;
								pi.DealProperty.SecondBal		= _dealProperty.P_SecondBal;
								pi.DealProperty.Sell4WhatOwes	= _dealProperty.P_Sell4WhatOwes;
								pi.DealProperty.Source			= _dealProperty.P_Source;
								pi.DealProperty.Sqft			= _dealProperty.P_Sqft;
								pi.DealProperty.TaxAmnt			= _dealProperty.P_TaxAmnt;
								pi.DealProperty.TaxIns			= _dealProperty.P_TaxIns;
								pi.DealProperty.ThirdBal		= _dealProperty.P_ThirdBal;
								pi.DealProperty.Type			= _dealProperty.P_Type;
								pi.DealProperty.Type1			= _dealProperty.P_Type1;
								pi.DealProperty.Type2			= _dealProperty.P_Type2;
								pi.DealProperty.Type3			= _dealProperty.P_Type3;
								pi.DealProperty.WhySelling		= _dealProperty.P_WhySelling;
								pi.DealProperty.WorthBySeller	= _dealProperty.P_WorthBySeller;
								pi.DealProperty.YearBuilt		= _dealProperty.P_YearBuilt;
							}
						}
						#endregion � ADV CONTACT �
						
						
						//save pi	BEWARE NEW CONNECTION - access from different thread
						int autoLeadNodeId = -1;
						int nodeId = -1;
						try
						{
							this.dbConnection.Open();
							pi.ExecuteSPL(this.dbConnection);

							//call SPL InsertGroupMembership 
							//insert it !!!
							this.InserGroupMembership(pi, LeadsNotProcessedYetGroupId, this.dbConnection);
							
				
							//Find Group - Origin
							
							if (false == this.FindGroup(AutoLeadsGroups, out autoLeadNodeId, this.dbConnection))		//FIND autoleads
							{					
								this.AddNode(1, AutoLeadsGroups, AutoLeadsGroups, out autoLeadNodeId, this.dbConnection);	//If we don't have it - create
							}

							if (false == this.FindGroup(_origin, out nodeId, this.dbConnection))	//FIND _origin group
							{
								this.AddNode(autoLeadNodeId, _origin, _origin, out nodeId, this.dbConnection);	//if we don't have it - create
								//FIRE EVENT
								this.OnNewGroup(nodeId);

							}
							this.InserGroupMembership(pi, nodeId, this.dbConnection);	//assign pi also to origin
							if (false == this.alGroupsToRefresh.Contains(nodeId))
							{
								this.alGroupsToRefresh.Add(nodeId);
							}
						}
						catch(Exception 
#if DEBUG 
							exc
#endif
							)
						{

#if DEBUG 
							MessageBox.Show("123Sold: " + exc.ToString());	
#endif

							//reset last fetch
							succSaving = false;
							continue;
						}
						finally
						{
							this.dbConnection.Close();	
							if (true == Globals.breakWaitingPeriod)
							{
								Globals.breakWaitingPeriod = false;
							}
						}												
					}
				}
				else
				{
					if (true == Globals.breakWaitingPeriod)
					{
						MessageBox.Show("You Have NO New Leads at this time!", "ATTENTION", MessageBoxButtons.OK, MessageBoxIcon.Information);
						Globals.breakWaitingPeriod = false;
					}
				}
			
				try
				{
					if (null != this.compositeProperties && this.compositeProperties.Length > 0 && true == succSaving)
					{																							
						this.OnNewContact(LeadsNotProcessedYetGroupId);	//refresh always		
						for (int i = 0; i < this.alGroupsToRefresh.Count; i++)
						{
							this.OnNewContact((int)alGroupsToRefresh[i]);
						}
						this.alGroupsToRefresh.Clear();

						string userMsg = string.Empty;
                        //userMsg += "You have just received " + this.compositeProperties.Length.ToString() + " new Lead";
                        //if (1 < this.compositeProperties.Length)
                        //{
                        //    userMsg += "s!";
                        //}
                        //else
                        //{
                        //    userMsg += "!";
                        //}

						userMsg += System.Environment.NewLine;
						userMsg += "Go to �Leads Not Processed Yet� to process ";
						if (1 < this.compositeProperties.Length)
						{
							userMsg += "those leads.";
						}
						else
						{
							userMsg += "that lead.";
						}
						
						userMsg += System.Environment.NewLine;
						userMsg += System.Environment.NewLine;


						userMsg += "NOTE:" + System.Environment.NewLine;
						for(int i = 0; i < this.alOrigins.Count; i++)				
						{							
							userMsg += "You have NEW \"" + (string)alOrigins[i] + "\" Lead(s)!" + System.Environment.NewLine;
						}						
						
						//MessageBox.Show(userMsg, "ATTENTION", MessageBoxButtons.OK, MessageBoxIcon.Information);
						OnwTwoThreeSoldNotification notif = new OnwTwoThreeSoldNotification(this.compositeProperties.Length);
						notif.Message = userMsg;
						notif.ShowDialog();

						this.lastFetch = DateTime.Now;
						//MessageBox.Show("GOT 123SOLD ENTRIES - " + this.compositeProperties.Length.ToString() + "last fetch: " + this.lastFetch.ToString());
						try
						{
                            StreamWriter sw = new StreamWriter(_aahFolder + LastFetchFileName);
							//sw.WriteLine(this._timeStamp.ToFileTime().ToString());
                            sw.WriteLine(this._timeStamp.Ticks.ToString());                            

							sw.Flush();
							sw.Close();	
						}
						catch {}
					}
				}
				catch ( Exception 
#if DEBUG
					exc 
#endif
					)								
				{
#if DEBUG
					MessageBox.Show(exc.ToString());
#endif
				}
			}
		}
        */
        /*
		#region � InserGroupMembership �
		private bool InserGroupMembership(PropertyItem pi, int nodeId, SqlConnection dbConn)
		{
			bool bRes = true;
			try
			{	
				SqlCommand sp_InsertGroupMembership = new SqlCommand("InsertGroupMembership", dbConn);

				sp_InsertGroupMembership.CommandType = CommandType.StoredProcedure;					
				sp_InsertGroupMembership.Parameters.Add("@propertyItemId", pi.IdPropertyItem);
				sp_InsertGroupMembership.Parameters.Add("@nodeId", nodeId);
													
									
				if (ConnectionState.Open != Connection.getInstance().HatchConn.State)
					Connection.getInstance().HatchConn.Open();
				
				sp_InsertGroupMembership.ExecuteNonQuery();				
					
			}
			catch
			{					
				bRes = false;
			}		
			return bRes;
		}
		#endregion � InserGroupMembership �


		#region � FindGroup �
		private bool FindGroup(string _origin, out int _nodeId, SqlConnection dbConn)
		{			
			_nodeId = -1;
			SqlDataReader dataReader = null;
			try
			{				
				SqlCommand command = new SqlCommand("select nodeId from Node where nodeName = '" + _origin + "'", dbConn);
				dataReader = command.ExecuteReader();
				while (dataReader.Read())
				{
					_nodeId = dataReader.GetInt32(0);						
				}
			}
			catch(Exception exc)
			{				
				_nodeId = -1;
				string s = exc.ToString();
			}
			finally
			{
				if (null != dataReader && false == dataReader.IsClosed)
				{
					dataReader.Close();
				}
			}

			if (-1 == _nodeId)
			{
				return false;
			}
			return true;				
		}
		#endregion � FindGroup �


		#region � AddNode �
		private bool AddNode(int _parentNode, string _nodeDesc, string _nodeName, out int nodeId, SqlConnection dbConn)
		{
			bool bResult = true;
			nodeId = -1;
			try
			{
				SqlCommand sp_dodaj_Node = new SqlCommand("dodaj_Node", dbConn);

				sp_dodaj_Node.CommandType = CommandType.StoredProcedure;
					
				sp_dodaj_Node.Parameters.Add("@parentNode", _parentNode);
				sp_dodaj_Node.Parameters.Add("@nodeDesc", _nodeDesc);
				sp_dodaj_Node.Parameters.Add("@nodeName", _nodeName);
					
				int retVal = -1;
				SqlParameter param = sp_dodaj_Node.Parameters.Add("@retValue", retVal);
				param.Direction = ParameterDirection.Output;

				SqlParameter new_NodeId_param = sp_dodaj_Node.Parameters.Add("@newNodeId ", nodeId);
				new_NodeId_param.Direction = ParameterDirection.Output;
												
				sp_dodaj_Node.ExecuteNonQuery();
	
				if (-1 == (int)param.Value)
				{
					bResult = false;
				}
				else
				{
					//UpdateTreeView();
					nodeId = (System.Int32)new_NodeId_param.Value;				
				}
			}
			catch 
			{
				bResult = false;
			}			
			return bResult;
		}
		#endregion � AddNode �
		*/

	}
}
