﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Printing;

//Added
using DealMaker.Repairs;
using DealMaker.ShortSale.Utils;

namespace DealMaker.PlainClasses.Repairs
{
    public class EstimateRepairsReportGenerator
    {
        Graphics _gfx;
        PrintPageEventArgs _e;        

        Brush _blackBrush = Brushes.Black;
        Pen _blackPen = Pens.Black;
        Font _arial9 = new Font("Arial", 9);
        Font _arial10 = new Font("Arial", 10);
        Font _arial10B = new Font("Arial", 10, FontStyle.Bold);
        Font _arial10I = new Font("Arial", 10, FontStyle.Italic);
        Font _arial12 = new Font("Arial", 12);
        Font _arial12B = new Font("Arial", 12, FontStyle.Bold);
        Font _arial12I = new Font("Arial", 12, FontStyle.Italic);
        Font _arial14 = new Font("Arial", 14);
        Font _arial14B = new Font("Arial", 14, FontStyle.Bold);
        Font _arial16 = new Font("Arial", 16);
        Font _arial18 = new Font("Arial", 18);
        Font _arial20 = new Font("Arial", 20);
        Font _arial22 = new Font("Arial", 22);
        Font _arial24 = new Font("Arial", 24);
        Font _arial26 = new Font("Arial", 26);

        string _loanNr;
        string _accountNr;
        string _address;
        string _fullName;         
        string _primSSNr;
        string _secSSNr;

        bool _extPaintRendered;        
        bool _roofRendered;
        bool _flooringRendered;
        bool _heatCoolRendered;        
        bool _doorsRendered;
        bool _windowsRendered;
        bool _plumbingRendered;
        bool _electricalRendered;
        bool _termiteDamageRendered;
        bool _waterDamageRendered;        
        bool _masterBathRendered;
        bool _kitchenRendered;        
        bool _miscRendered;
        bool _generalCleanUpRendered;
        bool _poolRendered;
        bool _landscapingRendered;
        bool _renderHeader = true;
        bool _dummyReport = true;

        StringFormat sf = new StringFormat(StringFormatFlags.DirectionRightToLeft);
        User _user = null;

        public EstimateRepairsReportGenerator(PrintPageEventArgs e)
		{
			_e = e;
			_gfx = e.Graphics;                                    
		}

        public void ResetGen()
        {
            _renderHeader = true;
            _extPaintRendered = false;            
            _roofRendered = false;
            _flooringRendered = false;
            _heatCoolRendered = false;            
            _doorsRendered = false;
            _windowsRendered = false;
            _plumbingRendered = false;
            _electricalRendered = false;
            _termiteDamageRendered = false;
            _waterDamageRendered = false;            
            _masterBathRendered = false;
            _kitchenRendered = false;            
            _miscRendered = false;
            _generalCleanUpRendered = false;
            _poolRendered = false;
            _landscapingRendered = false;
        }

        public void Generate(PrintPageEventArgs e, string loanNr, string accountNr, string address, string fullName, string primSSNr, string secSSNr)
        {
            _loanNr = loanNr;
            _accountNr = accountNr;
            _address = address;
            _fullName = fullName;            
            _primSSNr = primSSNr;
            _secSSNr = secSSNr;

            _dummyReport = false;

            Generate(e);
        }


        public void Generate(PrintPageEventArgs e, string address, string fullName, string primSSNr, string secSSNr)
        {            
            _address = address;
            _fullName = fullName;
            _primSSNr = primSSNr;
            _secSSNr = secSSNr;

            _dummyReport = true;

            Generate(e);
        }                

        public void Generate(PrintPageEventArgs e)
        {
            _user = ApplicationHelper.LoadUser();
            _e = e;
            _gfx = e.Graphics;

            int deltaY = 5;            
            int deltaYText = 20;
            int currY = 310;
            int valPos = 290;

            if (_renderHeader)
            {
                if (!_dummyReport)
                    _gfx.DrawString("REPAIRS ESTIMATE", _arial26, _blackBrush, new PointF(150, 25));
                else
                    _gfx.DrawString("REPAIRS ESTIMATE", _arial26, _blackBrush, new PointF(255, 25));

                if (!_dummyReport)
                {
                    //RECT
                    _gfx.DrawRectangle(Pens.Black, new Rectangle(new Point(600, 25), new Size(200, 200)));
                    _gfx.DrawString("URGENT", _arial26, _blackBrush, new Point(620, 30));
                    _gfx.DrawString("LOAN #", _arial26, _blackBrush, new Point(635, 75));
                    _gfx.DrawString(_accountNr, _arial26, _blackBrush, new Point(610, 120));
                    _gfx.DrawLine(Pens.Black, new Point(600, 175), new Point(800, 175));
                    _gfx.DrawString("Page 1", _arial20, _blackBrush, new Point(655, 180));
                }


                _gfx.DrawString("Address: " + _address, _arial14, _blackBrush, new Point(25, 80));
                _gfx.DrawString("Borrower: " + _fullName, _arial14, _blackBrush, new Point(25, 120));
                
                if (null != _primSSNr && string.Empty != _primSSNr)
                    _gfx.DrawString("Prim. SS#: " + _primSSNr, _arial14, _blackBrush, new Point(25, 160));

                if (null != _secSSNr && string.Empty != _secSSNr)
                    _gfx.DrawString("Sec. SS#: " + _secSSNr, _arial14, _blackBrush, new Point(350, 160));

                if (null != _user.PhoneNr && string.Empty != _user.PhoneNr)
                    _gfx.DrawString("To discuss this offer CALL " + _user.PhoneNr, _arial12, _blackBrush, new Point(25, 235));

                if (null != _user.Fax && string.Empty != _user.Fax)
                    _gfx.DrawString("Fax: " + _user.Fax, _arial12, _blackBrush, new Point(350, 235));

                _gfx.DrawLine(Pens.Black, new Point(20, 255), new Point(800, 255));

                _gfx.DrawString("To insure the resale within 6 months or less, here is the list of minimum repairs needed:", _arial12I, _blackBrush, new Point(25, 265));

                _renderHeader = false;
            }
            else
            {
                currY = 25;
            }                                       

            _gfx.DrawString("Item", _arial12B, _blackBrush, new Point(25, currY));
            _gfx.DrawString("Cost", _arial12B, _blackBrush, new Point(250, currY));
            _gfx.DrawString("Comments", _arial12B, _blackBrush, new Point(305, currY));
            _gfx.DrawLine(Pens.Black, new Point(20, currY + 20), new Point(800, currY + 20));

            currY += 40;

            if (null == RepairsManager.Instance.P_RepairObject)
                return;

            if (!_extPaintRendered)
            {
                string[] lines = GetPartialLinesFromString(RepairsManager.Instance.P_RepairObject.ExtPaint);

                if (!TestForFreeSpace(currY, lines.Length, deltaYText))                
                    { e.HasMorePages = true; return; }
                                    
                _gfx.DrawString("Ext./Int. Paint", _arial12, _blackBrush, new Point(25, currY));
                _gfx.DrawString(RepairsManager.Instance.P_RepairObject.ExtPaintUSD.ToString("c"), _arial12, _blackBrush, new Point(valPos, currY), sf);
                foreach (string s in lines)
                {
                    _gfx.DrawString(s, _arial12, _blackBrush, new Point(305, currY));
                    currY += deltaYText;
                }
                _extPaintRendered = true;
                currY += deltaY;
            }            

            if (!_roofRendered)
            {
                string[] lines = GetPartialLinesFromString(RepairsManager.Instance.P_RepairObject.Roof);

                if (!TestForFreeSpace(currY, lines.Length, deltaYText))
                    { e.HasMorePages = true; return; }

                _gfx.DrawString("Roof", _arial12, _blackBrush, new Point(25, currY));
                _gfx.DrawString(RepairsManager.Instance.P_RepairObject.RoofUSD.ToString("c"), _arial12, _blackBrush, new Point(valPos, currY), sf);
                foreach (string s in lines)
                {
                    _gfx.DrawString(s, _arial12, _blackBrush, new Point(305, currY));
                    currY += deltaYText;
                }
                _roofRendered = true;

                currY += deltaY;
            }

            if (!_flooringRendered)
            {
                string[] lines = GetPartialLinesFromString(RepairsManager.Instance.P_RepairObject.Flooring);

                if (!TestForFreeSpace(currY, lines.Length, deltaYText))
                    { e.HasMorePages = true; return; }

                _gfx.DrawString("Flooring", _arial12, _blackBrush, new Point(25, currY));
                _gfx.DrawString(RepairsManager.Instance.P_RepairObject.FlooringUSD.ToString("c"), _arial12, _blackBrush, new Point(valPos, currY), sf);
                foreach (string s in lines)
                {
                    _gfx.DrawString(s, _arial12, _blackBrush, new Point(305, currY));
                    currY += deltaYText;
                }
                _flooringRendered = true;

                currY += deltaY;
            }

            if (!_heatCoolRendered)
            {
                string[] lines = GetPartialLinesFromString(RepairsManager.Instance.P_RepairObject.HeatCool);

                if (!TestForFreeSpace(currY, lines.Length, deltaYText))
                    { e.HasMorePages = true; return; }

                _gfx.DrawString("Heating/Cooling", _arial12, _blackBrush, new Point(25, currY));
                _gfx.DrawString(RepairsManager.Instance.P_RepairObject.HeatCoolUSD.ToString("c"), _arial12, _blackBrush, new Point(valPos, currY), sf);
                foreach (string s in lines)
                {
                    _gfx.DrawString(s, _arial12, _blackBrush, new Point(305, currY));
                    currY += deltaYText;
                }
                _heatCoolRendered = true;

                currY += deltaY;
            }

            if (!_electricalRendered)
            {
                string[] lines = GetPartialLinesFromString(RepairsManager.Instance.P_RepairObject.Electrical);

                if (!TestForFreeSpace(currY, lines.Length, deltaYText))
                    { e.HasMorePages = true; return; }

                _gfx.DrawString("Electrical", _arial12, _blackBrush, new Point(25, currY));
                _gfx.DrawString(RepairsManager.Instance.P_RepairObject.ElectricalUSD.ToString("c"), _arial12, _blackBrush, new Point(valPos, currY), sf);
                foreach (string s in lines)
                {
                    _gfx.DrawString(s, _arial12, _blackBrush, new Point(305, currY));
                    currY += deltaYText;
                }
                _electricalRendered = true;

                currY += deltaY;
            }

            if (!_plumbingRendered)
            {
                string[] lines = GetPartialLinesFromString(RepairsManager.Instance.P_RepairObject.Plumbing);

                if (!TestForFreeSpace(currY, lines.Length, deltaYText))
                    { e.HasMorePages = true; return; }

                _gfx.DrawString("Plumbing", _arial12, _blackBrush, new Point(25, currY));
                _gfx.DrawString(RepairsManager.Instance.P_RepairObject.PlumbingUSD.ToString("c"), _arial12, _blackBrush, new Point(valPos, currY), sf);
                foreach (string s in lines)
                {
                    _gfx.DrawString(s, _arial12, _blackBrush, new Point(305, currY));
                    currY += deltaYText;
                }
                _plumbingRendered = true;

                currY += deltaY;
            }

            if (!_windowsRendered)
            {
                string[] lines = GetPartialLinesFromString(RepairsManager.Instance.P_RepairObject.Windows);

                if (!TestForFreeSpace(currY, lines.Length, deltaYText))
                    { e.HasMorePages = true; return; }

                _gfx.DrawString("Windows", _arial12, _blackBrush, new Point(25, currY));
                _gfx.DrawString(RepairsManager.Instance.P_RepairObject.WindowsUSD.ToString("c"), _arial12, _blackBrush, new Point(valPos, currY), sf);
                foreach (string s in lines)
                {
                    _gfx.DrawString(s, _arial12, _blackBrush, new Point(305, currY));
                    currY += deltaYText;
                }
                _windowsRendered = true;

                currY += deltaY;
            }

            if (!_doorsRendered)
            {
                string[] lines = GetPartialLinesFromString(RepairsManager.Instance.P_RepairObject.Doors);

                if (!TestForFreeSpace(currY, lines.Length, deltaYText))
                    { e.HasMorePages = true; return; }

                _gfx.DrawString("Doors", _arial12, _blackBrush, new Point(25, currY));
                _gfx.DrawString(RepairsManager.Instance.P_RepairObject.DoorsUSD.ToString("c"), _arial12, _blackBrush, new Point(valPos, currY), sf);
                foreach (string s in lines)
                {
                    _gfx.DrawString(s, _arial12, _blackBrush, new Point(305, currY));
                    currY += deltaYText;
                }
                _doorsRendered = true;

                currY += deltaY;
            }            

            if (!_kitchenRendered)
            {
                string[] lines = GetPartialLinesFromString(RepairsManager.Instance.P_RepairObject.Kitchen);

                if (!TestForFreeSpace(currY, lines.Length, deltaYText))
                    { e.HasMorePages = true; return; }

                _gfx.DrawString("Kitchen", _arial12, _blackBrush, new Point(25, currY));
                _gfx.DrawString(RepairsManager.Instance.P_RepairObject.KitchenUSD.ToString("c"), _arial12, _blackBrush, new Point(valPos, currY), sf);
                foreach (string s in lines)
                {
                    _gfx.DrawString(s, _arial12, _blackBrush, new Point(305, currY));
                    currY += deltaYText;
                }
                _kitchenRendered = true;
                currY += deltaY;
            }

            if (!_masterBathRendered)
            {
                string[] lines = GetPartialLinesFromString(RepairsManager.Instance.P_RepairObject.MasterBath);

                if (!TestForFreeSpace(currY, lines.Length, deltaYText))
                    { e.HasMorePages = true; return; }

                _gfx.DrawString("Bath", _arial12, _blackBrush, new Point(25, currY));
                _gfx.DrawString(RepairsManager.Instance.P_RepairObject.MasterBathUSD.ToString("c"), _arial12, _blackBrush, new Point(valPos, currY), sf);
                foreach (string s in lines)
                {
                    _gfx.DrawString(s, _arial12, _blackBrush, new Point(305, currY));
                    currY += deltaYText;
                }
                _masterBathRendered = true;
                currY += deltaY;
            }            

            if (!_waterDamageRendered)
            {
                string[] lines = GetPartialLinesFromString(RepairsManager.Instance.P_RepairObject.WaterDamage);

                if (!TestForFreeSpace(currY, lines.Length, deltaYText))
                    { e.HasMorePages = true; return; }

                _gfx.DrawString("Water Damage", _arial12, _blackBrush, new Point(25, currY));
                _gfx.DrawString(RepairsManager.Instance.P_RepairObject.WaterDamageUSD.ToString("c"), _arial12, _blackBrush, new Point(valPos, currY), sf);
                foreach (string s in lines)
                {
                    _gfx.DrawString(s, _arial12, _blackBrush, new Point(305, currY));
                    currY += deltaYText;
                }
                _waterDamageRendered = true;

                currY += deltaY;
            }

            if (!_termiteDamageRendered)
            {
                string[] lines = GetPartialLinesFromString(RepairsManager.Instance.P_RepairObject.TermiteDamage);

                if (!TestForFreeSpace(currY, lines.Length, deltaYText))
                    { e.HasMorePages = true; return; }

                _gfx.DrawString("Termite Damage", _arial12, _blackBrush, new Point(25, currY));
                _gfx.DrawString(RepairsManager.Instance.P_RepairObject.TermiteDamageUSD.ToString("c"), _arial12, _blackBrush, new Point(valPos, currY), sf);
                foreach (string s in lines)
                {
                    _gfx.DrawString(s, _arial12, _blackBrush, new Point(305, currY));
                    currY += deltaYText;
                }
                _termiteDamageRendered = true;

                currY += deltaY;
            }

            if (!_landscapingRendered)
            {
                string[] lines = GetPartialLinesFromString(RepairsManager.Instance.P_RepairObject.Landscaping);

                if (!TestForFreeSpace(currY, lines.Length, deltaYText))
                    { e.HasMorePages = true; return; }

                _gfx.DrawString("Landscaping", _arial12, _blackBrush, new Point(25, currY));
                _gfx.DrawString(RepairsManager.Instance.P_RepairObject.LandscapingUSD.ToString("c"), _arial12, _blackBrush, new Point(valPos, currY), sf);
                foreach (string s in lines)
                {
                    _gfx.DrawString(s, _arial12, _blackBrush, new Point(305, currY));
                    currY += deltaYText;
                }
                _landscapingRendered = true;

                currY += deltaY;
            }

            if (!_poolRendered)
            {
                string[] lines = GetPartialLinesFromString(RepairsManager.Instance.P_RepairObject.Pool);

                if (!TestForFreeSpace(currY, lines.Length, deltaYText))
                    { e.HasMorePages = true; return; }

                _gfx.DrawString("Pool", _arial12, _blackBrush, new Point(25, currY));
                _gfx.DrawString(RepairsManager.Instance.P_RepairObject.PoolUSD.ToString("c"), _arial12, _blackBrush, new Point(valPos, currY), sf);
                foreach (string s in lines)
                {
                    _gfx.DrawString(s, _arial12, _blackBrush, new Point(305, currY));
                    currY += deltaYText;
                }
                _poolRendered = true;

                currY += deltaY;
            }

            if (!_generalCleanUpRendered)
            {
                string[] lines = GetPartialLinesFromString(RepairsManager.Instance.P_RepairObject.GeneralCleanUp);

                if (!TestForFreeSpace(currY, lines.Length, deltaYText))
                    { e.HasMorePages = true; return; }

                _gfx.DrawString("General Cleanup", _arial12, _blackBrush, new Point(25, currY));
                _gfx.DrawString(RepairsManager.Instance.P_RepairObject.GeneralCleanUpUSD.ToString("c"), _arial12, _blackBrush, new Point(valPos, currY), sf);
                foreach (string s in lines)
                {
                    _gfx.DrawString(s, _arial12, _blackBrush, new Point(305, currY));
                    currY += deltaYText;
                }
                _generalCleanUpRendered = true;

                currY += deltaY;
            }

            if (!_miscRendered)
            {
                string[] lines = GetPartialLinesFromString(RepairsManager.Instance.P_RepairObject.Misc);

                if (!TestForFreeSpace(currY, lines.Length, deltaYText))
                    { e.HasMorePages = true; return; }

                _gfx.DrawString("Miscellaneous", _arial12, _blackBrush, new Point(25, currY));
                _gfx.DrawString(RepairsManager.Instance.P_RepairObject.MiscUSD.ToString("c"), _arial12, _blackBrush, new Point(valPos, currY), sf);
                foreach (string s in lines)
                {
                    _gfx.DrawString(s, _arial12, _blackBrush, new Point(305, currY));
                    currY += deltaYText;
                }
                _miscRendered = true;

                currY += deltaY;
            }            

            _gfx.DrawString("TOTAL:", _arial14B, _blackBrush, new Point(20, currY));
            _gfx.DrawString(RepairsManager.Instance.P_RepairObject.Total.ToString("c"), _arial14B, _blackBrush, new Point(valPos, currY), sf);
            
        }

        string[] GetPartialLinesFromString(string text)
        {            
            return Util.GetPartialLinesFromString(_gfx, _arial12, 500, text);
            return new string[] { text };
        }

        bool TestForFreeSpace(int currY, int nrOfLines, int deltaYText)
        {
            if (nrOfLines * (_gfx.MeasureString("L", _arial12).Height + deltaYText) + currY < 1050)
                return true;

            return false;
        }
        
    }
}
