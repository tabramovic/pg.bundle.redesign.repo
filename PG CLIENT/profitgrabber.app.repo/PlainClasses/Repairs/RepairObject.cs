﻿using System;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;
using System.Text;

namespace DealMaker.Repairs
{
    [XmlRoot("RepairObject")]
    [Serializable]
    public class RepairObject
    {
        #region DATA
        //DESC
        string _extPaint;
        
        string _roof;
        string _flooring;
        string _heatCool;
        
        string _doors;
        string _windows;
        string _plumbing;
        string _electrical;
        string _termiteDamage;
        string _waterDamage;
        
        string _masterBath;
        string _kitchen;
        
        string _misc;
        string _generalCleanUp;
        string _pool;
        string _landscaping;

        //USD
        decimal _otherUSD;
        decimal _miscUSD;
        decimal _generalCleanUpUSD;
        decimal _poolUSD;
        decimal _landscapingUSD;
        decimal _termiteDamageUSD;
        decimal _waterDamageUSD;
        decimal _otherBathsUSD;
        decimal _masterBathUSD;
        decimal _kitchenUSD;
        decimal _garageDoorUSD;
        decimal _doorsUSD;
        decimal _windowsUSD;
        decimal _plumbingUSD;
        decimal _electricalUSD;
        decimal _heatCoolUSD;
        decimal _flooringUSD;
        decimal _roofUSD;
        decimal _intPaintUSD;
        decimal _extPaintUSD;

        //BREAK DOWNs
        CostItem[] _extPaintBD;
        CostItem[] _intPaintBD;
        CostItem[] _roofBD;
        CostItem[] _electricalBD;
        CostItem[] _heatCoolBD;
        CostItem[] _flooringBD;
        CostItem[] _masterBathBD;
        CostItem[] _kitchenBD;
        CostItem[] _garageDoorBD;
        CostItem[] _doorsBD;
        CostItem[] _windowsBD;
        CostItem[] _plumbingBD;
        CostItem[] _generalCleanUpBD;
        CostItem[] _poolBD;
        CostItem[] _landscapingBD;
        CostItem[] _termiteDamageBD;
        CostItem[] _waterDamageBD;
        CostItem[] _otherBathsBD;
        CostItem[] _otherBD;
        CostItem[] _miscBD;

        //TOTAL
        decimal _total;
        #endregion

        #region PROPERTIES
        //DESC
        public string ExtPaint
        {
            get { return _extPaint; }
            set { _extPaint = value; }
        }
        
        
        
        public string Roof
        {
            get { return _roof; }
            set { _roof = value; }
        }
        
        public string Flooring
        {
            get { return _flooring; }
            set { _flooring = value; }
        }
        
        public string HeatCool
        {
            get { return _heatCool; }
            set { _heatCool = value; }
        }              
        
        public string Doors
        {
            get { return _doors; }
            set { _doors = value; }
        }
        
        public string Windows
        {
            get { return _windows; }
            set { _windows = value; }
        }
        
        public string Plumbing
        {
            get { return _plumbing; }
            set { _plumbing = value; }
        }
        
        public string Electrical
        {
            get { return _electrical; }
            set { _electrical = value; }
        }
        
        public string TermiteDamage
        {
            get { return _termiteDamage; }
            set { _termiteDamage = value; }
        }
        
        public string WaterDamage
        {
            get { return _waterDamage; }
            set { _waterDamage = value; }
        }                
        
        public string MasterBath
        {
            get { return _masterBath; }
            set { _masterBath = value; }
        }
        
        public string Kitchen
        {
            get { return _kitchen; }
            set { _kitchen = value; }
        }
                        
        public string Misc
        {
            get { return _misc; }
            set { _misc = value; }
        }
        
        public string GeneralCleanUp
        {
            get { return _generalCleanUp; }
            set { _generalCleanUp = value; }
        }
                        
        public string Pool
        {
            get { return _pool; }
            set { _pool = value; }
        }
        
        public string Landscaping
        {
            get { return _landscaping; }
            set { _landscaping = value; }
        }

        
        //USD
        public decimal OtherUSD
        {
            get { return _otherUSD; }
            set { _otherUSD = value; }
        }
        
        public decimal MiscUSD
        {
            get { return _miscUSD; }
            set { _miscUSD = value; }
        }
        
        public decimal GeneralCleanUpUSD
        {
            get { return _generalCleanUpUSD; }
            set { _generalCleanUpUSD = value; }
        }
       
        public decimal PoolUSD
        {
            get { return _poolUSD; }
            set { _poolUSD = value; }
        }
        
        public decimal LandscapingUSD
        {
            get { return _landscapingUSD; }
            set { _landscapingUSD = value; }
        }
        
        public decimal TermiteDamageUSD
        {
            get { return _termiteDamageUSD; }
            set { _termiteDamageUSD = value; }
        }
        
        public decimal WaterDamageUSD
        {
            get { return _waterDamageUSD; }
            set { _waterDamageUSD = value; }
        }
        
        public decimal OtherBathsUSD
        {
            get { return _otherBathsUSD; }
            set { _otherBathsUSD = value; }
        }
        
        public decimal MasterBathUSD
        {
            get { return _masterBathUSD; }
            set { _masterBathUSD = value; }
        }
       
        public decimal KitchenUSD
        {
            get { return _kitchenUSD; }
            set { _kitchenUSD = value; }
        }        

        public decimal GarageDoorUSD
        {
            get { return _garageDoorUSD; }
            set { _garageDoorUSD = value; }
        }
        
        public decimal DoorsUSD
        {
            get { return _doorsUSD; }
            set { _doorsUSD = value; }
        }
       
        public decimal WindowsUSD
        {
            get { return _windowsUSD; }
            set { _windowsUSD = value; }
        }
        
        public decimal PlumbingUSD
        {
            get { return _plumbingUSD; }
            set { _plumbingUSD = value; }
        }
        
        public decimal ElectricalUSD
        {
            get { return _electricalUSD; }
            set { _electricalUSD = value; }
        }
        
        public decimal HeatCoolUSD
        {
            get { return _heatCoolUSD; }
            set { _heatCoolUSD = value; }
        }
        
        public decimal FlooringUSD
        {
            get { return _flooringUSD; }
            set { _flooringUSD = value; }
        }
        
        public decimal RoofUSD
        {
            get { return _roofUSD; }
            set { _roofUSD = value; }
        }
        
        public decimal IntPaintUSD
        {
            get { return _intPaintUSD; }
            set { _intPaintUSD = value; }
        }
        
        public decimal ExtPaintUSD
        {
            get { return _extPaintUSD; }
            set { _extPaintUSD = value; }
        }

        
        //BREAKDOWNs
        public CostItem[] ExtPaintBD
        {
            get { return _extPaintBD; }
            set { _extPaintBD = value; }
        }
        
        public CostItem[] IntPaintBD
        {
            get { return _intPaintBD; }
            set { _intPaintBD = value; }
        }
        
        public CostItem[] RoofBD
        {
            get { return _roofBD; }
            set { _roofBD = value; }
        }
        
        public CostItem[] ElectricalBD
        {
            get { return _electricalBD; }
            set { _electricalBD = value; }
        }
        
        public CostItem[] HeatCoolBD
        {
            get { return _heatCoolBD; }
            set { _heatCoolBD = value; }
        }
        
        public CostItem[] FlooringBD
        {
            get { return _flooringBD; }
            set { _flooringBD = value; }
        }
        
        public CostItem[] MasterBathBD
        {
            get { return _masterBathBD; }
            set { _masterBathBD = value; }
        }
        
        public CostItem[] KitchenBD
        {
            get { return _kitchenBD; }
            set { _kitchenBD = value; }
        }
        
        public CostItem[] GarageDoorBD
        {
            get { return _garageDoorBD; }
            set { _garageDoorBD = value; }
        }
       
        public CostItem[] DoorsBD
        {
            get { return _doorsBD; }
            set { _doorsBD = value; }
        }
        
        public CostItem[] WindowsBD
        {
            get { return _windowsBD; }
            set { _windowsBD = value; }
        }
       
        public CostItem[] PlumbingBD
        {
            get { return _plumbingBD; }
            set { _plumbingBD = value; }
        }
        
        public CostItem[] GeneralCleanUpBD
        {
            get { return _generalCleanUpBD; }
            set { _generalCleanUpBD = value; }
        }
        
        public CostItem[] PoolBD
        {
            get { return _poolBD; }
            set { _poolBD = value; }
        }
        
        public CostItem[] LandscapingBD
        {
            get { return _landscapingBD; }
            set { _landscapingBD = value; }
        }
        
        public CostItem[] TermiteDamageBD
        {
            get { return _termiteDamageBD; }
            set { _termiteDamageBD = value; }
        }
        
        public CostItem[] WaterDamageBD
        {
            get { return _waterDamageBD; }
            set { _waterDamageBD = value; }
        }
        
        public CostItem[] OtherBathsBD
        {
            get { return _otherBathsBD; }
            set { _otherBathsBD = value; }
        }
        
        public CostItem[] OtherBD
        {
            get { return _otherBD; }
            set { _otherBD = value; }
        }
        
        public CostItem[] MiscBD
        {
            get { return _miscBD; }
            set { _miscBD = value; }
        }

        
        //TOTAL
        public decimal Total
        {
            get { return _total; }
            set { _total = value; }
        }
        #endregion
    }
}
