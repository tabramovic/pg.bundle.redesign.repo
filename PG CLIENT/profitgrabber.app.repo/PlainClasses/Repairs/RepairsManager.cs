﻿using System;
using System.Collections;
using System.Text;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Xml;
using System.Xml.Serialization;
using System.Data;
using System.Data.SqlClient;

namespace DealMaker.Repairs
{
    public sealed class RepairsManager
    {
        static readonly RepairsManager _instance = new RepairsManager();

        static RepairsManager()
        { }

        RepairsManager()
        { }

        public static RepairsManager Instance
        {
            get { return _instance; }
        }

        bool _existingDBLayer = false;
        SqlConnection _connection = null;

        string _path = string.Empty;
        string _documentId = string.Empty;
        RepairObject _repairObject = new RepairObject();        

        public void ResetObject()
        {
            _repairObject = new RepairObject();
        }        

        public RepairObject P_RepairObject
        {
            get { return _repairObject; }
            set { _repairObject = value; }
        }

        public void TestCreateDBLayer(SqlConnection conn)
        {
            if (null == _connection)
            {
                _connection = conn;
                _existingDBLayer = TestIfExistsDBLayer();

                if (!_existingDBLayer)
                    CreateDBLayer();
            }
        }


        public bool TestIfExistsDBLayer()
        {            
            bool retVal = false;
            SqlDataReader reader = null;
            string cmdText = "SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Repairs'";

            try
            {
                SqlCommand testForLayer = new SqlCommand(cmdText, _connection);
                reader = testForLayer.ExecuteReader();
                while (reader.Read())
                {
                    retVal = true;
                    break;
                }
            }
            catch
            {
                retVal = false;
            }
            finally
            {
                reader.Close();
            }
            return retVal;
        }

        void CreateDBLayer()
        {
            CreateTable();
            CreateInsertSPL();
            CreateUpdateSPL();
            CreateSelectSPL();
        }

        bool CreateTable()
        {
            bool retVal = false;
            string cmdText = "CREATE TABLE Repairs (piId uniqueidentifier PRIMARY KEY, xmlStream image)";

            try
            {
                SqlCommand createLayer = new SqlCommand(cmdText, _connection);
                createLayer.ExecuteNonQuery();
                retVal = true;
            }
            catch
            {
                retVal = false;
            }
            return retVal;
        }

        void CreateInsertSPL()
        {
            try
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("CREATE PROCEDURE insert_Repairs ");
                sb.Append("( ");
                sb.Append("@propertyItemId uniqueidentifier, ");
                sb.Append("@xmlStream image");
                sb.Append(") ");
                sb.Append("AS ");
                sb.Append("INSERT INTO Repairs (piId , xmlStream) ");
                sb.Append("VALUES ( @propertyItemId , @xmlStream) ");

                string splText = sb.ToString();
                SqlCommand createSPL = new SqlCommand(splText, _connection);
                createSPL.ExecuteNonQuery();

            }
            catch (Exception exc)
            {
                System.Windows.Forms.MessageBox.Show("Can't create INSERT procedure for Repairs module" + System.Environment.NewLine + exc.Message);
            }
        }

        void CreateUpdateSPL()
        {
            try
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("CREATE PROCEDURE update_Repairs ");
                sb.Append("( ");
                sb.Append("@propertyItemId uniqueidentifier, ");
                sb.Append("@xmlStream image ");
                sb.Append(") ");
                sb.Append("AS ");
                sb.Append("UPDATE Repairs SET xmlStream=@xmlStream where piId=@propertyItemId; ");

                string splText = sb.ToString();
                SqlCommand createSPL = new SqlCommand(splText, _connection);
                createSPL.ExecuteNonQuery();

            }
            catch (Exception exc)
            {
                System.Windows.Forms.MessageBox.Show("Can't create UPDATE procedure for REPAIRS module" + System.Environment.NewLine + exc.Message);
            }
        }

        void CreateSelectSPL()
        {
            try
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("CREATE PROCEDURE select_Repairs ");
                sb.Append("( ");
                sb.Append("@propertyItemId uniqueidentifier");                
                sb.Append(") ");
                sb.Append("AS ");
                sb.Append("SELECT xmlStream FROM Repairs WHERE piId=@propertyItemId; ");

                string splText = sb.ToString();
                SqlCommand createSPL = new SqlCommand(splText, _connection);
                createSPL.ExecuteNonQuery();

            }
            catch (Exception exc)
            {
                System.Windows.Forms.MessageBox.Show("Can't create UPDATE procedure for REPAIRS module" + System.Environment.NewLine + exc.Message);
            }
        }

       








        public bool LoadData(Guid propertyItemId)
        {
            MemoryStream stream = new MemoryStream();                
            try
            {
                XmlSerializer s = new XmlSerializer(typeof(RepairObject));                
                _repairObject = Select(propertyItemId);
            }
            catch (Exception ex)
            {
                return false;
            }
                        
            return true;
        }

        public bool SaveData(Guid propertyItemId)
        {                        
            if (Guid.Empty == propertyItemId)
                return false;

            byte[] arr = null;
            try
            {
                MemoryStream ms = new MemoryStream();
                BinaryFormatter bf1 = new BinaryFormatter();
                bf1.Serialize(ms, _repairObject);
                arr = ms.ToArray();
                                
            }
            catch (Exception ex)
            {
                return false;
            }            
            
            if (null == Select(new object[] { propertyItemId }))
                return Insert(propertyItemId, arr);
            else
                return Update(propertyItemId, arr);            
        }

        bool Insert(Guid propertyItemId, byte[] xmlStream)
        {
            bool retVal = false;

            try
            {
                SqlCommand sp_insert_PropertyProfile = new SqlCommand("insert_Repairs", Connection.getInstance().HatchConn);
                sp_insert_PropertyProfile.CommandType = CommandType.StoredProcedure;
                sp_insert_PropertyProfile.Parameters.AddWithValue("@propertyItemId", propertyItemId);
                sp_insert_PropertyProfile.Parameters.AddWithValue("@xmlStream", xmlStream);
                sp_insert_PropertyProfile.ExecuteNonQuery();
                retVal = true;
            }
            catch (Exception exc)
            {
                retVal = false;
            }
            return retVal;
        }

        bool Update(Guid propertyItemId, byte[] xmlStream)
        {
            bool retVal = false;

            try
            {
                SqlCommand sp_update_PropertyProfile = new SqlCommand("update_Repairs", Connection.getInstance().HatchConn);
                sp_update_PropertyProfile.CommandType = CommandType.StoredProcedure;

                sp_update_PropertyProfile.Parameters.AddWithValue("@propertyItemId", propertyItemId);
                sp_update_PropertyProfile.Parameters.AddWithValue("@xmlStream", xmlStream);
                sp_update_PropertyProfile.ExecuteNonQuery();
                retVal = true;
            }
            catch (Exception exc)
            {
                retVal = false;
            }
            return retVal;
        }

        public RepairObject Select(params object[] list)
        {            
            SqlDataReader reader = null;
            RepairObject repairObject = null;
            try
            {
                SqlCommand sp_select_PropertyProfile = new SqlCommand("select_Repairs", Connection.getInstance().HatchConn);

                sp_select_PropertyProfile.CommandType = CommandType.StoredProcedure;

                if (null != list && 0 != list.Length)
                {
                    sp_select_PropertyProfile.Parameters.AddWithValue("@propertyItemId", (System.Guid)list[0]);
                }
                else
                {
                    sp_select_PropertyProfile.Parameters.AddWithValue("@propertyItemId", DBNull.Value);
                }
                reader = sp_select_PropertyProfile.ExecuteReader();
                while (reader.Read())
                {
                    System.Data.SqlTypes.SqlBinary sqlBinary1 = reader.GetSqlBinary(0);
                    byte[] arr =  (byte[])sqlBinary1.Value;
                    
                    MemoryStream ms = new MemoryStream(arr);
                    BinaryFormatter bf1 = new BinaryFormatter();
                    ms.Position = 0;

                    repairObject = bf1.Deserialize(ms) as RepairObject;
                }
            }
            catch (Exception exc)
            {
                repairObject = null;
            }
            finally
            {
                if (null != reader)
                    reader.Close();
            }
            return repairObject;
        }        
    }
}
