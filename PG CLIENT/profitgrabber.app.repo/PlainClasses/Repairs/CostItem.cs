using System;
using System.Xml;
using System.Xml.Serialization;
using System.Collections;

namespace DealMaker.Repairs
{
	/// <summary>
	/// Summary description for CostItem.
	/// </summary>
	[XmlInclude(typeof(CostItem))]
    [Serializable]
	public struct CostItem
	{
		public string CostDescription;
		public decimal Cost;

        public CostItem(string costDescription)
        {
            CostDescription = costDescription;
            Cost = 0;
        }
	}
}
