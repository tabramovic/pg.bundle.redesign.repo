#region � Using �
using System;
#endregion � Using �

#region � Namespace DealMaker �
namespace DealMaker
{
	#region � Enum OwnerState �
	/// <summary>
	/// Owner state (either is occupied or absent)
	/// </summary>
	public enum OwnerState
	{
		/// <summary>Occupied</summary>
		Occupied = 0, 
		/// <summary>Absent</summary>
		Absent, 
	}
	#endregion � Enum OwnerState �

	#region � Delimiter �
	/// <summary>
	/// Delimiters in input files
	/// </summary>
	public enum Delimiter
	{
		/// <summary>Comma (,)</summary>				
		comma = 0, 
		/// <summary>Tab (\t)</summary>
		tab, 
		/// <summary>Hash (#)</summary>
		hash, 
		/// <summary>Semi comma (;)</summary>
		semiComma,
	}
	#endregion � Delimiter �

	#region � OwnerName �
	public enum OwnerName
	{
		Specified = 0, 
		Neighbor, 
		Homeowner,
	}
	#endregion � OwnerName �

	#region � eSaveOrUpdate �
	public enum eSaveOrUpdate
	{
		Save = 0, 

		Update
	}
	#endregion � eSaveOrUpdate �
}
#endregion � Namespace DealMaker �