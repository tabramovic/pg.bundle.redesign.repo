using System;

namespace DealMaker
{
	public enum TaskDateType
	{
		From = 0, 
		PriorTo, 
	}

	public enum Status
	{
		Activity = 0,
		Pending, 
		Done,
		Canceled,
		PastDo,
		NotAssigned
	}

	public enum TaskType
	{
		ToDo = 1, 
		Call, 
		Mail,
		Visit,
		Fax,
		PastAction
	}
}
