using System;
using System.Collections;

namespace DealMaker
{
	/// <summary>
	/// Summary description for ZIPFilter.
	/// </summary>
	public class ZIPFilter : IFilterStoreable
	{
		private const string _Equals = "Equals";
		private const string _NotEquals = "Does Not Equal";
		private string propertyName;
		private string condition;
		private string[] zips;
		
		public ZIPFilter(string[] zips, string propertyName, string condition)
		{
			this.propertyName = propertyName;
			this.condition = condition;
			this.zips = zips;
		}

		public ZIPFilter(string commaSeparatedZips, string propertyName, string condition)
		{
			this.propertyName = propertyName;
			this.condition = condition;

			ArrayList alZips = new ArrayList();
			while (-1 != commaSeparatedZips.IndexOf(","))
			{
				string tempSubString = commaSeparatedZips.Substring(0, commaSeparatedZips.IndexOf(","));
				tempSubString = tempSubString.Trim();
				alZips.Add(tempSubString);

				commaSeparatedZips = commaSeparatedZips.Remove(0, commaSeparatedZips.IndexOf(",") + 1);
			}

			if (0 != commaSeparatedZips.Length)
			{
				alZips.Add(commaSeparatedZips);
			}
			this.zips = (string[])alZips.ToArray(typeof(string));
		}

		public string Zips 
		{
			get 
			{
				string temp = string.Empty;
				for (int i = 0; i < this.zips.Length; i++)
				{
					temp += this.zips[i];
					if (i < this.zips.Length -1)
					{
						temp += ", ";
					}
				}
				return temp; 
			}
		}

		public string PropertyName
		{
			get {return this.propertyName;}
		}

		public string Condition
		{
			get {return this.condition;}
		}

		public bool TestForValue(string searchedZip)
		{
			searchedZip = searchedZip.Trim();

			if (this.condition == _Equals)
			{
				for (int i = 0; i < this.zips.Length; i++)
				{
					string zip = this.zips[i];
					zip = zip.Trim();

					if (searchedZip.Equals(zip))
					{
						//found it !!!
						return true;
					}
				}
			}
			else
			{
				for (int i = 0; i < this.zips.Length; i++)
				{
					string zip = this.zips[i];
					zip = zip.Trim();

					if (searchedZip.Equals(zip))
					{
						//found it !!!
						return false;
					}
				}
			}

			if (this.condition == _Equals)
			{
				//we didn't find appropriate value in filter array
				return false;
			}
			else
			{
				return true;
			}
		}

		public bool TestForValue(string searchedZip, bool filterUponFirstFiveDigits)
		{
			if (!filterUponFirstFiveDigits)
				return  TestForValue(searchedZip);


			searchedZip = searchedZip.Trim();
			
			//ta, Jan, 16th 2008
			//Bugfix
			//when applying zip filter upon an import file containing empty string for a zip
			try
			{
				searchedZip = searchedZip.Substring(0, 5);
				return TestForValue(searchedZip);
			}
			catch
			{
				return false;
			}			
		}


		#region IFilterStoreable Members

		public void StoreObject(System.IO.StreamWriter sw)
		{
			if (null != this.zips && zips.Length > 0)
			{
				sw.WriteLine(Globals.ZIPFilterStarter);
				sw.WriteLine(this.propertyName);
				sw.WriteLine(this.condition);
				sw.WriteLine(this.Zips);
				sw.WriteLine(Globals.ZIPFilterEnder);
			}
		}
		#endregion
	}
}
