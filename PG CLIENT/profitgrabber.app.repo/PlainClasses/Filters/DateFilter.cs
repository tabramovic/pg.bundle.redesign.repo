using System;
using System.Collections;

namespace DealMaker
{
	/// <summary>
	/// Summary description for DateFilter.
	/// </summary>
	public class DateFilter : IFilterStoreable
	{
		DateTime[] dates;
		string propertyName;
		string condition; 

		public DateFilter(DateTime[] dates, string propertyName, string condition)
		{
			this.condition = condition;
			this.dates = dates;
			this.propertyName = propertyName;
		}

		public DateFilter(string commaSeparatedDates, string propertyName, string condition)
		{
			System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.CreateSpecificCulture("EN-US");
			this.propertyName = propertyName;
			this.condition = condition;

			ArrayList alDates = new ArrayList();
			while (-1 != commaSeparatedDates.IndexOf(","))
			{
				string tempDate = commaSeparatedDates.Substring(0, commaSeparatedDates.IndexOf(","));
				tempDate = tempDate.Trim();
				
				try 
				{
					DateTime dt = Convert.ToDateTime(tempDate);
					alDates.Add(dt);
				}
				catch 
				{}
				
				commaSeparatedDates = commaSeparatedDates.Remove(0, commaSeparatedDates.IndexOf(",") + 1);
			}
			if (0 != commaSeparatedDates.Length)
			{				
				try 
				{
					DateTime dt = Convert.ToDateTime(commaSeparatedDates);
					alDates.Add(dt);
				}
				catch 
				{}
			}
			this.dates = (DateTime[])alDates.ToArray(typeof(DateTime));
		}
		
		public string Dates
		{
			get 
			{
				string temp = string.Empty;
				System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.CreateSpecificCulture("EN-US");

				for (int i = 0; i < this.dates.Length; i++)
				{		
					temp += this.dates[i] .ToShortDateString();
					if (i != this.dates.Length - 1)
					{
						temp += ", ";
					}
				}	
				return temp;
			}
		}

		public string PropertyName
		{
			get {return this.propertyName;}
		}

		public string Condition
		{
			get {return this.condition;}
		}

		public bool TestForValue(string searchedDate)
		{
			System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.CreateSpecificCulture("EN-US");
			try
			{
				DateTime tempDt = Convert.ToDateTime(searchedDate);
				if (true == this.TestForValue(tempDt))
				{
					return true; 
				}
				else
				{
					return false;
				}
			}
			catch 
			{}

			//couldn't cast it!
			return false;
		}

		private bool TestForValue(DateTime searchedDate)
		{
			for (int i = 0; i < this.dates.Length; i++)
			{
				DateTime dt = this.dates[i];

				switch (this.condition)
				{
					case Globals.IsEqual:
						if (dt.Equals(searchedDate))
						{							
							return true;
						}
						break;

					case Globals.IsGreaterThan:
						if (searchedDate > dt)
						{							
							return true;
						}
						break;

					case Globals.IsGreaterThanOrEqualTo:
						if (searchedDate >= dt)
						{							
							return true;
						}
						break;

					case Globals.IsLessThan:
						if (searchedDate < dt)
						{							
							return true;
						}
						break;

					case Globals.IsLessThanOrEqualTo:
						if (searchedDate <= dt)
						{							
							return true;
						}
						break;					
				}

				
			}
			
			//we didn't find appropriate value in filter array
			return false;
		}


		#region IFilterStoreable Members

		public void StoreObject(System.IO.StreamWriter sw)
		{
			System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.CreateSpecificCulture("EN-US");
			if (null != this.dates && dates.Length > 0)
			{
				sw.WriteLine(Globals.DateFilterStarter);
				sw.WriteLine(this.propertyName);
				sw.WriteLine(this.condition);
				
				for (int i = 0; i < this.dates.Length; i++)
				{
					DateTime date = this.dates[i];
					sw.WriteLine(date.ToString("d"));
					if (i != this.dates.Length -	1)
					{
						sw.WriteLine(", ");
					}
				}
				sw.WriteLine(Globals.DateFilterEnder);
			}
		}

		#endregion
	}
}
