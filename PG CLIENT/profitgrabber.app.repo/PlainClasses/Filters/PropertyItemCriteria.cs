// ihuk, 16.01.2005
// Weather this belongs to DealMaker.Filters could be discussed,
// since this class is not exactly a filter but...

using System;
using System.Collections;
using DealMaker;

namespace DealMaker.Filters
{
	/// <summary>
	/// Provides functionality to filter instances of PropertyItem class
	/// that meet certain criteria
	/// </summary>
	public class PropertyItemCriteria
	{
		PropertyItem[] items;
		IPropertyItemFilter[] filters;

		public PropertyItemCriteria(PropertyItem[] items, IPropertyItemFilter[] filters)
		{
			this.items = items;
			this.filters = filters;
		}

		public PropertyItem[] List()
		{
			ArrayList result = new ArrayList();

			foreach (PropertyItem item in items)
			{
				bool passed = true;

				//TA (24.02.2005.): 
				//check for null values - needed in return mail wizard
				if (null != filters)	//TA added
				{						//TA added
					foreach (IPropertyItemFilter filter in filters)
					{
						if (false == (passed &= filter.EvaluateObject(item)))
							break;
					}
				}						//TA added

				if (passed)
					result.Add(item);
			}

			return (PropertyItem[])result.ToArray(typeof(PropertyItem));
		}
	}
}