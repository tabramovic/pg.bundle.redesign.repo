using System;
using DealMaker.Filters;
using DealMaker.PlainClasses;

namespace DealMaker
{
	/// <summary>
	/// MathematicalFilter class implements functionality defined in preRequisites
	/// </summary>
	public class MathematicalFilter : IPropertyItemFilter, IFilterStoreable
	{
		#region � Data �
		private string dataColumn1		= string.Empty;
		private string dataColumn2		= string.Empty;
		private string definedOperator	= string.Empty;
		private string resultCondition	= string.Empty;
		private double definedValue		= 0;
		#endregion � Data �
		#region � Automatically Generated Properties �
		#region · DataColumn1 ·
		[Description("Data Column1")]
		public string DataColumn1
		{
			get
			{
				return this.dataColumn1;
			}
			set
			{
				this.dataColumn1 = value;
			}
		}
		#endregion · DataColumn1 ·
		#region · DataColumn2 ·
		[Description("Data Column2")]
		public string DataColumn2
		{
			get
			{
				return this.dataColumn2;
			}
			set
			{
				this.dataColumn2 = value;
			}
		}
		#endregion · DataColumn2 ·
		#region · DefinedOperator ·
		[Description("Defined Operator")]
		public string DefinedOperator
		{
			get
			{
				return this.definedOperator;
			}
			set
			{
				this.definedOperator = value;
			}
		}
		#endregion · DefinedOperator ·
		#region · ResultCondition ·
		[Description("Result Condition")]
		public string ResultCondition
		{
			get
			{
				return this.resultCondition;
			}
			set
			{
				this.resultCondition = value;
			}
		}
		#endregion · ResultCondition ·
		#region · DefinedValue ·
		[Description("Defined Value")]
		public double DefinedValue
		{
			get
			{
				return this.definedValue;
			}
			set
			{
				this.definedValue = value;
			}
		}
		#endregion · DefinedValue ·
		#endregion � Automatically Generated Properties �
		#region � CTR �
		public MathematicalFilter()
		{
		}
		public MathematicalFilter(string dataColumn1, string dataColumn2, string definedOperator, string resultOperator, double definedValue)
		{
			this.dataColumn1		= dataColumn1;
			this.dataColumn2		= dataColumn2;
			this.definedOperator	= definedOperator;
			this.resultCondition	= resultOperator;
			this.definedValue		= definedValue;
		}
		#endregion � CTR �
	
		/// ihuk, 05.12.2004
		/// <summary>
		/// Implements IPropertyItemFilter
		/// </summary>
		/// <param name="item">Instance of PropertyItemClass to be filtered.</param>
		/// <returns>
		/// True if item meets filter criteria, false otherwise.
		/// </returns>
		public bool EvaluateObject(PropertyItem item)
		{
			double resultValue = 0;
			double attribute1Value = 0;
			double attribute2Value = 0;
			PropertyItemAttribute attribute1 = item[dataColumn1];
			PropertyItemAttribute attribute2 = item[dataColumn2];

			if (null == attribute1 || null == attribute2)
				return false;

			try
			{
				attribute1Value = (double)attribute1.AttributeValue;
				attribute2Value = (double)attribute2.AttributeValue;
			}
			catch
			{
				return false;
			}

			switch (this.definedOperator)
			{
				case Globals.Plus:
					resultValue = attribute1Value + attribute2Value;
					break;

				case Globals.Minus:
					resultValue = attribute1Value - attribute2Value;
					break;
			}

			switch (this.resultCondition)
			{
				case Globals.IsEqual:
					return resultValue == definedValue;				

				case Globals.DoesNotEqual:
					return resultValue != definedValue;
				
				case Globals.IsGreaterThan:
					return resultValue > definedValue;

				case Globals.IsGreaterThanOrEqualTo:
					return resultValue >= definedValue;

				case Globals.IsLessThan:
					return resultValue < definedValue;

				case Globals.IsLessThanOrEqualTo:
					return resultValue <= definedValue;

				default:
					return false;
			}
		}

		#region IFilterStoreable Members

		public void StoreObject(System.IO.StreamWriter sw)
		{
			sw.WriteLine(Globals.MathFilterStarter);
			sw.WriteLine(this.DataColumn1 + Globals.TabSeparator +
						 this.DataColumn2 + Globals.TabSeparator + 
						 this.DefinedOperator + Globals.TabSeparator + 
						 this.ResultCondition + Globals.TabSeparator + 
						 this.DefinedValue);

			sw.WriteLine(Globals.MathFilterEnder);
		}

		#endregion
	}
}
