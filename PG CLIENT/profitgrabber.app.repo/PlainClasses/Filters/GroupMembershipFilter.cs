using System;
using DealMaker.DALC;
using DealMaker.Classes;
using DealMaker.Filters;
using DealMaker.PlainClasses;

// ihuk, 23.01.2005
// Insert standard namespace disclaimer :)
//
namespace DealMaker
{
	/// <summary>
	/// GroupMembershipFilter implementes filtering of
	/// property items by group membership
	/// </summary>
	public class GroupMembershipFilter : IPropertyItemFilter, IFilterStoreable
	{
		int groupId;
		Group group = null;
		bool membership = true;

		public GroupMembershipFilter()
		{
		}

		public GroupMembershipFilter(int groupId, bool membership)
		{
			this.GroupId = groupId;
			this.membership = membership;

			group = GroupFactory.GetInstance().GetGroup(groupId);
		}

		public bool EvaluateObject(PropertyItem item)
		{
			// ihuk, 23.01.2005
			// We would need to haave FielterException class
			// in order to o this right, but thiss will have to.
			if (null == group)
				throw new System.Exception("Invalid group");

			foreach (PropertyItem groupItem in group.PropertyItems)
				if (groupItem.IdPropertyItem == item.IdPropertyItem)
					return membership ? true : false;

			return membership ? false : true;
		}

		public void StoreObject(System.IO.StreamWriter sw)
		{
			/// ihuk, 23.01.2005
			/// I don't need this right now so I'll just throw 
			/// a message to whoever is going to need this.
			throw new System.Exception("Not implemented, sorry...");
		}

		#region Properties
		public int GroupId
		{
			get
			{
				return groupId;
			}
			set
			{
					groupId = value;
					group = GroupFactory.GetInstance().GetGroup(groupId);
			}
		}

		/// <summary>
		/// Membership property defines ther criteria for filtering.
		/// If Membership property is set to true filter returns those
		/// items that are members of group. Otherwise items not
		/// belonging to group are returned.
		/// </summary>
		public bool Membership
		{
			get
			{
				return membership;
			}
			set
			{
				membership = value;
			}
		}
		#endregion
	}
}
