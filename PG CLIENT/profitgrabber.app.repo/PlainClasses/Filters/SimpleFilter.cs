#region � Using �
using System;
using DealMaker.Filters;
using DealMaker.PlainClasses;
#endregion � Using �
#region � Namespace DealMaker �
namespace DealMaker
{
	#region � Class SimpleFilter �
	/// <summary>
	/// SimpleFilter - implements functionality defined for simple filters
	/// </summary>
	public class SimpleFilter : IPropertyItemFilter, IFilterStoreable
	{
		#region � Data �
		private string dataColumn	= string.Empty;
		private string condition	= string.Empty;
		private double valueDefined	 = 0;
		#endregion � Data �
		#region � Automatically Generated Properties �
		#region · DataColumn ·
		[Description("Data Column")]
		public string DataColumn
		{
			get
			{
				return this.dataColumn;
			}
			set
			{
				this.dataColumn = value;
			}
		}
		#endregion · DataColumn ·
		#region · Condition ·
		[Description("Condition")]
		public string Condition
		{
			get
			{
				return this.condition;
			}
			set
			{
				this.condition = value;
			}
		}
		#endregion · Condition ·
		#region · ValueDefined ·
		[Description("Value Defined")]
		public double ValueDefined
		{
			get
			{
				return this.valueDefined;
			}
			set
			{
				this.valueDefined = value;
			}
		}
		#endregion · ValueDefined ·
		#endregion � Automatically Generated Properties �
		#region � CTR �
		public SimpleFilter()
		{
		}
		public SimpleFilter(string dataColumn, string condition, double valueDefined)
		{
			this.dataColumn = dataColumn;
			this.condition = condition;
			this.valueDefined = valueDefined;
		}
		#endregion � CTR �

		/// ihuk, 05.12.2004
		/// <summary>
		/// Implements IPropertyItemFilter
		/// </summary>
		/// <param name="item">Instance of PropertyItemClass to be filtered.</param>
		/// <returns></returns>
		public bool EvaluateObject(PropertyItem item)
		{
			double attributeValue;
			PropertyItemAttribute attribute = item[dataColumn];
			if (null == attribute)
				return false;

			try
			{
				attributeValue = (double)attribute.AttributeValue;
			}
			catch
			{
				return false;
			}

			switch (this.condition)
			{
				case Globals.IsEqual:
					return attributeValue == valueDefined;				

				case Globals.DoesNotEqual:
					return attributeValue != valueDefined;
				
				case Globals.IsGreaterThan:
					return attributeValue > valueDefined;

				case Globals.IsGreaterThanOrEqualTo:
					return attributeValue >= valueDefined;

				case Globals.IsLessThan:
					return attributeValue < valueDefined;

				case Globals.IsLessThanOrEqualTo:
					return attributeValue <= valueDefined;

				default:
					return false;
			}
		}

		#region IFilterStoreable Members

		public void StoreObject(System.IO.StreamWriter sw)
		{
			sw.WriteLine(Globals.SimpleFilterStarter);
			sw.WriteLine(this.DataColumn + Globals.TabSeparator + this.Condition + Globals.TabSeparator + this.ValueDefined);
			sw.WriteLine(Globals.SimpleFilterEnder);
		}

		#endregion
	}
	#endregion � Class SimpleFilter �
}
#endregion � Namespace DealMaker �
