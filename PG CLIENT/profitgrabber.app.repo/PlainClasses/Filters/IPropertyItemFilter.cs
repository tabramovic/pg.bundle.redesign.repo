// ihuk, 05.12.2004
// It is good practice to have namespaces match 
// directory structure. But here everything is in this
// DealMaker namespace. So I'm not going to put this
// into DealMaker.PlainClasses.Filter and hope that
// this code will get reorganized :)
namespace DealMaker.Filters
{
	/// <summary>
	/// Provides functionality to filter instances of PropertyItem class
	/// </summary>
	public interface IPropertyItemFilter
	{
		/// <summary>
		/// Checks if instance of PropertyItem class matches the filter.
		/// </summary>
		/// <param name="item">Instance of PropertyOItem class to be matched</param>
		/// <returns></returns>
		bool EvaluateObject(PropertyItem item);
	}
}