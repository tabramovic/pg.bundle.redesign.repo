﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml.Serialization;

namespace DealMaker
{
    public class Task2EmailSettingsManager
    {
        const string FileName = "Task2EmailSettings.xml";
        public task2emailsettings LoadCustomSettings(out string error)
        {
            error = string.Empty;

            if (!TestForFile())
                return null;

            StreamReader reader = null;

            task2emailsettings cs;
            try	// Deserialize
            {
                XmlSerializer ser = new XmlSerializer(typeof(task2emailsettings));
                reader = new StreamReader(OSFolderManager.Instance.GetPlainLocalUserAppDataPath() + @"\" + FileName);
                cs = (task2emailsettings)ser.Deserialize(reader);
            }
            catch (Exception ex)
            {
                cs = null;
                error = ex.Message;
            }
            finally
            {
                if (reader != null)
                    reader.Close();
                reader = null;
            }
            return cs;
        }

        public bool SaveCustomSettings(task2emailsettings cs, out string error)
        {            
			bool returnValue = true;
            error = string.Empty;
			StreamWriter writer = null;

            if (!TestForFolder())
                Directory.CreateDirectory(OSFolderManager.Instance.GetPlainLocalUserAppDataPath());

			try //Serialize
			{
                XmlSerializer ser = new XmlSerializer(typeof(task2emailsettings));
                writer = new StreamWriter(OSFolderManager.Instance.GetPlainLocalUserAppDataPath() + @"\" + FileName);
				ser.Serialize(writer, cs);				
			} 
			catch (Exception ex)
			{
				returnValue = false;
                error = ex.Message;
			} 
			finally
			{
				if (writer != null) 
					writer.Close();
				writer = null;
			} 

			return returnValue;
        }

        bool TestForFolder()
        {
            if (!Directory.Exists(OSFolderManager.Instance.GetPlainLocalUserAppDataPath()))
                return false;

            return true;
        }

        bool TestForFile()
        {
            if (!TestForFolder())
                return false;
            if (!File.Exists(OSFolderManager.Instance.GetPlainLocalUserAppDataPath() + @"\" + FileName))
                return false;

            return true;
        }
    }
}
