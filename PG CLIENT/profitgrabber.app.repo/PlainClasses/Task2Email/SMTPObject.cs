﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Net.Mail;

//Added
using System.Threading;

namespace DealMaker
{
    public class SMTPObject
    {
        public string OutgoingSMTPServer { get; set; }
        public string Port { get; set; }
        public bool EnableSSL { get; set; }
        public string AccountName { get; set; }
        public string EmailAddress { get; set; }
        public string Password { get; set; }
        public string DisplayName { get; set; }
        public string ReplyTo { get; set; }

        public string TestRecipient { get; set; }
                
        public SMTPObject()
        {
            
        }
                     
        public bool TestSettings(string recipientsEmailAddress, out string errMsg)
        {
            bool retVal = true;
            errMsg = string.Empty;

            try
            {
                MailMessage message = new MailMessage();
                message.To.Add(recipientsEmailAddress);
                message.Subject = "ProfitGrabber's Test Email";

                if (!string.IsNullOrEmpty(DisplayName))
                    message.From = new MailAddress(string.IsNullOrEmpty(AccountName) ? EmailAddress : AccountName, DisplayName);
                else
                    message.From = new MailAddress(string.IsNullOrEmpty(AccountName) ? EmailAddress : AccountName);

                message.Body = "Test";

                if (!string.IsNullOrEmpty(ReplyTo))
                    message.ReplyToList.Add(ReplyTo);

                SmtpClient smtpClient = new SmtpClient(OutgoingSMTPServer);

                int port = 0;
                try { port = Convert.ToInt32(Port); }
                catch { port = 0; }

                smtpClient.Port = port; //587; 
                smtpClient.EnableSsl = true;
                smtpClient.UseDefaultCredentials = false;
                smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;


                smtpClient.Credentials = new NetworkCredential(EmailAddress, Password);                
               
                //smtpClient.Timeout = 5000;
                smtpClient.Send(message);

                   
            }
            catch (Exception exc)
            {
                errMsg = exc.Message + Environment.NewLine + exc.InnerException;
                retVal = false;
            }

            return retVal;
        }

        public bool SendOutTasksPerEmail(Dictionary<Guid, List<string>> officeStaffTasks, out string errMsg)
        {
            bool retVal = true;
            errMsg = string.Empty;
            string allRecipients = string.Empty;

            try
            {
                Dictionary<Guid, List<string>>.Enumerator enumerator = officeStaffTasks.GetEnumerator();
                List<MailMessage> mailMessages = new List<MailMessage>();
                while (enumerator.MoveNext())
                {
                    List<string> list = enumerator.Current.Value;

                    if (list.Count < 3)
                        continue;

                    //break recipients 
                    string[] emailRecipients = list[1].Split(',');

                    //foreach recipient create message
                    foreach (string recipient in emailRecipients)
                    {
                        bool cellEmailAddress = false;
                        bool validEmail = ApplicationHelper.IsCellPhoneEmailAddress(recipient, out cellEmailAddress);

                        if (!validEmail)
                            continue;

                        if (!cellEmailAddress)
                        {
                            MailMessage message = new MailMessage();
                            message.To.Add(recipient);
                            message.Subject = "ProfitGrabber Task Reminder";// "Today's task list for " + list[0];
                            message.From = new MailAddress(AccountName, DisplayName);
                            message.Body = list[2];

                            mailMessages.Add(message);
                        }
                        else
                        {
                            for (int i = 0; i < list.Count - 3; i++)
                            {
                                MailMessage message = new MailMessage();
                                message.To.Add(recipient);
                                message.Subject = "ProfitGrabber Task Reminder"; //"Today's task list for " + list[0];
                                message.From = new MailAddress(AccountName);
                                message.Body = list[3 + i]; //[list.Count - 4 + i];

                                mailMessages.Add(message);
                            }
                        }
                    }
                    allRecipients += list[1] + "; ";
                }

                if (null == mailMessages || 0 == mailMessages.Count)
                {
                    errMsg = "Could not generate mail messages. Please check email address format.";
                    return false;
                }
                                
                foreach (MailMessage mailMsg in mailMessages)
                {
                    EmailSenderWorker.Instance.mailMessagesToSend.Enqueue(mailMsg);                    
                }                                                
            }
            catch (Exception exc)
            {
                errMsg = exc.Message + Environment.NewLine + exc.InnerException;
                retVal = false;
            }

            try
            {
                if (!String.IsNullOrEmpty(allRecipients))
                {
                    DealMaker.Properties.Settings.Default["Task2EmailLastSent"] = allRecipients + " at " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString();
                    DealMaker.Properties.Settings.Default.Save();
                }
            }
            catch { }

            return retVal;
        }



       

        public void PersistSettings()
        {
            System.Xml.Serialization.XmlSerializer xmlser = new System.Xml.Serialization.XmlSerializer(typeof(SMTPObject));
            System.IO.FileStream fs = new System.IO.FileStream(OSFolderManager.Instance.GetPlainLocalUserAppDataPath() + @"\SmtpSettings.xml", System.IO.FileMode.Create);
            xmlser.Serialize(fs, this);
            fs.Flush();
            fs.Close();
        }

        public SMTPObject LoadSettigns()
        {
            System.IO.FileStream fs = null;
            SMTPObject smtpObj = new SMTPObject();
            System.Xml.Serialization.XmlSerializer xmlser = new System.Xml.Serialization.XmlSerializer(typeof(SMTPObject));
            try
            {
                fs = new System.IO.FileStream(OSFolderManager.Instance.GetPlainLocalUserAppDataPath() + @"\SmtpSettings.xml", System.IO.FileMode.Open);
                smtpObj = (SMTPObject)xmlser.Deserialize(fs);
                
            }
            catch (System.IO.FileNotFoundException)
            {
                // File does not exist! 
            }
            catch (Exception x)
            {
                throw new Exception("Opening SMTP Settings File" + "\n" + x.Message + "\n" + x.StackTrace);
            }
            finally
            {
                try
                {
                    if (null != fs)
                        fs.Close();
                }
                catch
                {
                    // file not opened !
                }
            }

            return smtpObj;
        }
    }
}
