﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Threading;

//Added
using NHibernate;
using NHibernate.Expression;
using DealMaker.PlainClasses;
using System.Net.Mail;
using google.oauth.lib;
using System.Threading.Tasks;
using System.Windows.Forms;
using DTD.FEB.Response;

namespace DealMaker
{
    public sealed class Task2EmailWorker
    {        
        static readonly Task2EmailWorker _instance = new Task2EmailWorker();

        static Task2EmailWorker()
        { }

        Task2EmailWorker()
        {            
            _ct = _source.Token;
        }

        public static Task2EmailWorker Instance
        {
            get { return _instance; }
        }

        //data
        readonly CancellationTokenSource _source = new CancellationTokenSource();
        CancellationToken _ct;

        bool _stopSync = false;
        public bool StopSync {
            get 
            { 
                return _stopSync; 
            } 
            
            set 
            { 
                _stopSync = value; 
                _source.Cancel(); 
            }
        }
        int _sleepPeriod = 60000; //60 secs = 1 min
        Dictionary<Guid, List<string>> _officeStaffList = null;
        

        public void SendOutEmails()
        {
            _officeStaffList = GetOfficeStaffTasks();

            System.Threading.Tasks.Task.Run(StaffSenderWorker);
            //Thread th = new Thread(StaffSenderWorker);
            //th.Start();

            EmailSenderWorker.Instance.Start();
        }

        #region GET OFFICE STAFF TASKS
        public Dictionary<Guid, List<string>> GetOfficeStaffTasks()
        {
            Dictionary<Guid, List<string>> resDict = GetOfficeStaffData();

            ExpandOfficeStaffListWithTodaysTasks(ref resDict);

            return resDict;
        }

        public Dictionary<Guid, List<string>> GetOfficeStaffTasks(IList taskList)
        {
            Dictionary<Guid, List<string>> resDict = GetOfficeStaffData();

            ExpandOfficeStaffListWithTasks(ref resDict, taskList);

            return resDict;
        }
        #endregion

        #region GET OFFICE STAFF DATA
        private static Dictionary<Guid, List<string>> GetOfficeStaffData()
        {
            Dictionary<Guid, List<string>> resDict = new Dictionary<Guid, List<string>>();

            Dictionary<Guid, string> dict = UserInformationManager.Instance.GetValues(UserEntries.OfficeStaffList);
            if (null != dict)
            {
                Dictionary<Guid, string>.Enumerator enumerator = dict.GetEnumerator();
                while (enumerator.MoveNext())
                {
                    string emailAddresses = UserInformationManager.Instance.GetEmailAddresses(enumerator.Current.Key);

                    if (null != emailAddresses && string.Empty != emailAddresses)
                    {
                        List<string> list = new List<string>();
                        list.Add(enumerator.Current.Value);
                        list.Add(emailAddresses);

                        resDict.Add(enumerator.Current.Key, list);
                    }
                }
            }
            return resDict;
        }
        #endregion

        #region EXPAND OFFICE STAFF LIST WITH (TODAY's) TASKS
        void ExpandOfficeStaffListWithTodaysTasks(ref Dictionary<Guid, List<string>> officeStaffDict)
        {
            Dictionary<Guid, List<string>>.Enumerator enumerator = officeStaffDict.GetEnumerator();
            while (enumerator.MoveNext())
            {
                List<string> list = enumerator.Current.Value;
                if (null != list && null != list[0])
                {
                    IList todaysTasks = GetTodaysTaskForUser(list[0]);
                    string formattedTasksData = GetFormattedTasksData(todaysTasks);
                    if (null != formattedTasksData && string.Empty != formattedTasksData)
                        list.Add(formattedTasksData);

                    foreach (Task t in todaysTasks)
                    {
                        IList taskInList = new List<Task>();
                        taskInList.Add(t);
                        string formattedTaskData = GetFormattedTasksData(taskInList);

                        if (null != formattedTaskData && string.Empty != formattedTaskData)
                            list.Add(formattedTaskData);
                    }
                }
            }
        }

        void ExpandOfficeStaffListWithTasks(ref Dictionary<Guid, List<string>> officeStaffDict, IList tasks)
        {
            Dictionary<Guid, List<string>>.Enumerator enumerator = officeStaffDict.GetEnumerator();
            while (enumerator.MoveNext())
            {
                List<string> list = enumerator.Current.Value;
                if (null != list && null != list[0])
                {
                    
                    string formattedTasksData = GetFormattedTasksData(tasks);
                    if (null != formattedTasksData && string.Empty != formattedTasksData)
                        list.Add(formattedTasksData);

                    foreach (Task t in tasks)
                    {
                        IList taskInList = new List<Task>();
                        taskInList.Add(t);
                        string formattedTaskData = GetFormattedTasksData(taskInList);

                        if (null != formattedTaskData && string.Empty != formattedTaskData)
                            list.Add(formattedTaskData);
                    }
                }
            }
        }
        #endregion

        #region GET TODAY's TASK FOR USER
        public IList GetTodaysTaskForUser(string createdFor)
        {
            NHibernate.ISession session = Globals.factory.OpenSession();
            IList list = null;
            IList filteredList = new ArrayList();

            list = (IList)session.CreateCriteria(typeof(Task)).                        
                        Add(Expression.Le("TaskDate", DateTime.Now.Date)).
                        //Add(Expression.IsNotNull("CheckOut")).
                        Add(Expression.Eq("Status", (int)Status.Pending)).
                        Add(Expression.Eq("CreatedFor", createdFor)).                                                
                        List();

            foreach (Task t in list)
            {
                if (t.TaskDate <= DateTime.Now.Date &&
                    t.Status != (int)Status.Done &&
                    t.Status != (int)Status.Canceled &&
                    t.Status != (int)Status.Activity)
                {
                    filteredList.Add(t);
                }
                //else
                //    filteredList.Add(t);

            }

            return filteredList;
        }
        #endregion

        #region GET FORMATTED TASK DATA
        string GetFormattedTasksData(IList todaysTasks)
        {
            string retVal = string.Empty;

            if (null == todaysTasks)
                return null;

            int i = 0;
            foreach (object task in todaysTasks)
            {
                PropertyItem pi = null;
                Task t = task as Task;

                if (0 < i)
                    retVal += Environment.NewLine + "--" + Environment.NewLine;

                string ownerName = string.Empty;
                string siteAddress = string.Empty;
                string addPhones = string.Empty;
                if (null != t)
                {
                    if ((null != t.IdPropertyItem) && (Guid.Empty != t.IdPropertyItem))
                    {
                        pi = ApplicationHelper.LoadPropertyItem(t.IdPropertyItem);

                        if (null != pi && null != pi.Owner && null != pi.Owner.FullName)
                            ownerName = pi.Owner.FullName;

                        if (null != pi && null != pi.Owner && null != pi.Owner.SiteAddress && null != pi.Owner.SiteAddress.FullSiteStreetAddress)
                            siteAddress = pi.Owner.SiteAddress.FullSiteStreetAddress;
                    }

                    if ((t.Type == (int)TaskType.Call) && (null != t.IdPropertyItem) && (Guid.Empty != t.IdPropertyItem))
                    {
                        if (null == pi)
                            pi = ApplicationHelper.LoadPropertyItem(t.IdPropertyItem);

                        if (null != pi && null != pi.Owner)
                        {
                            if (null != pi.Owner.HomePhone && null != pi.Owner.HomePhone.PhoneNr && string.Empty != pi.Owner.HomePhone.PhoneNr)
                                addPhones += "Home: " + pi.Owner.HomePhone.PhoneNr + Environment.NewLine;

                            if (null != pi.Owner.WorkPhone && null != pi.Owner.WorkPhone.PhoneNr && string.Empty != pi.Owner.WorkPhone.PhoneNr)
                                addPhones += "Work: " + pi.Owner.WorkPhone.PhoneNr + Environment.NewLine;

                            if (null != pi.Owner.CellPhone && null != pi.Owner.CellPhone.PhoneNr && string.Empty != pi.Owner.CellPhone.PhoneNr)
                                addPhones += "Cell: " + pi.Owner.CellPhone.PhoneNr + Environment.NewLine;                            
                        }                        
                    }

                    retVal += "Task Date: " + t.TaskDate.ToShortDateString() + ", " + ((TaskType)t.Type).ToString() + ", " + "Priority " + t.Priority.ToString() + Environment.NewLine +
                        (string.Empty != ownerName ? "Owner: " + ownerName + Environment.NewLine: string.Empty) + 
                        (string.Empty != siteAddress ? "Address: " + siteAddress + Environment.NewLine: string.Empty) + 
                              t.Description + Environment.NewLine +
                              ((t.Type == (int)TaskType.Call) ? "Phone: " + t.PhoneNumber + Environment.NewLine : string.Empty) +
                              (string.Empty != addPhones ? (addPhones + Environment.NewLine) : string.Empty) +
                              "Notes: " + t.Notes + Environment.NewLine;                                                            
                    i++;
                }
            }

            return retVal;
        }
        #endregion

        private async Task<bool> StaffSenderWorker()
        {            
            var smtpObject = new SMTPObject().LoadSettigns();

            Task2EmailSettingsManager settingsManager = new Task2EmailSettingsManager();
            string err;
            string smtpError = string.Empty;

            while (!StopSync)
            {
                try
                {
                    task2emailsettings settings = settingsManager.LoadCustomSettings(out err);
                    DateTime now = DateTime.Now;

                    if (null != settings?.data && !string.IsNullOrEmpty(smtpObject?.EmailAddress) && !string.IsNullOrEmpty(smtpObject?.DisplayName))
                    {
                        foreach (task2emailsettingsData data in settings.data)
                        {
                            //global (morning) send
                            if (1 == data.serviceEnabled && now.Hour == data.hour && now.Minute == data.minute)
                            {
                                Dictionary<Guid, List<string>> officeStaffTasks = GetOfficeStaffTasks();

                                if (null != officeStaffTasks && officeStaffTasks.ContainsKey(new Guid(data.officeStaffId)))
                                {
                                    Dictionary<Guid, List<string>> filteredOfficeStaffTasks = new Dictionary<Guid, List<string>>();
                                    filteredOfficeStaffTasks.Add(new Guid(data.officeStaffId), officeStaffTasks[new Guid(data.officeStaffId)]);

                                    var mailMsgs = Task2EmailWorker.Instance.PrepareMailMsgs(filteredOfficeStaffTasks, smtpObject.EmailAddress, smtpObject.DisplayName);
                                    using (var emailService = new EmailService())
                                    {
                                        try { await emailService.InitializeGmailAccountService(Globals.ProductName, Globals.ClientId, Globals.ClientSecret, Globals.GetUserToAuthorize(smtpObject?.EmailAddress)); }
                                        catch (Exception exc)
                                        {
                                            //log error
                                        }

                                        foreach (var mailMsg in mailMsgs)
                                        {
                                            try
                                            {
                                                var response = emailService.SendEmail(mailMsg);
                                            }
                                            catch (Exception)
                                            {
                                                //build list of msgs that didn't send successfully
                                                //display error
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    Dictionary<Guid, List<string>> officeStaffData = GetOfficeStaffData();
                    foreach (Guid officeStaffId in officeStaffData.Keys)
                    {
                        Dictionary<Guid, List<string>> filteredOfficeStaffData = new Dictionary<Guid, List<string>>();

                        {
                            IList todaysTasks = GetTodaysTaskForUser(officeStaffData[officeStaffId][0]);

                            if (null != officeStaffData && officeStaffData.ContainsKey(officeStaffId))
                                filteredOfficeStaffData.Add(officeStaffId, officeStaffData[officeStaffId]);

                            IList tasksToSend = new List<Task>();
                            foreach (Task t in todaysTasks)
                            {
                                if (t.CheckOut.Date == now.Date && t.CheckOut.Hour == now.Hour && t.CheckOut.Minute == now.Minute)
                                    tasksToSend.Add(t);
                            }

                            if (null != tasksToSend && tasksToSend.Count > 0)
                            {
                                ExpandOfficeStaffListWithTasks(ref filteredOfficeStaffData, tasksToSend);

                                var mailMsgs = Task2EmailWorker.Instance.PrepareMailMsgs(filteredOfficeStaffData, smtpObject.EmailAddress, smtpObject.DisplayName);
                                using (var emailService = new EmailService())
                                {
                                    try { await emailService.InitializeGmailAccountService(Globals.ProductName, Globals.ClientId, Globals.ClientSecret, Globals.GetUserToAuthorize(smtpObject?.EmailAddress)); }
                                    catch (Exception exc)
                                    {
                                        //log error
                                    }

                                    foreach (var mailMsg in mailMsgs)
                                    {
                                        try
                                        {
                                            var response = emailService.SendEmail(mailMsg);
                                        }
                                        catch (Exception)
                                        {
                                            //build list of msgs that didn't send successfully
                                            //display error
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception exc)
                {
                    //log this!
                }

                await System.Threading.Tasks.Task.Delay(_sleepPeriod, _ct);
           }

            return true;
        }

        public async Task<bool> SendTasksNow(string taskOwner, IList tasks)
        {
            SMTPObject smtpObject = new SMTPObject();            
            string smtpError = string.Empty;

            Dictionary<Guid, List<string>> officeStaffData = GetOfficeStaffData();
            Dictionary<Guid, List<string>> filteredOfficeStaffData = new Dictionary<Guid, List<string>>();

            Dictionary<Guid, List<string>>.Enumerator enumerator = officeStaffData.GetEnumerator();

            while (enumerator.MoveNext())
            {
                if (enumerator.Current.Value[0] == taskOwner)
                {
                    filteredOfficeStaffData.Add(enumerator.Current.Key, enumerator.Current.Value);
                    break;
                }
            }

            ExpandOfficeStaffListWithTasks(ref filteredOfficeStaffData, tasks);
            smtpObject = smtpObject.LoadSettigns();

            if (string.IsNullOrEmpty(smtpObject?.EmailAddress) || string.IsNullOrEmpty(smtpObject?.DisplayName))                            
                return false;

            var mailMsgs = Task2EmailWorker.Instance.PrepareMailMsgs(filteredOfficeStaffData, smtpObject.EmailAddress, smtpObject.DisplayName);
                        
            using (var emailService = new EmailService())
            {
                try { await emailService.InitializeGmailAccountService(Globals.ProductName, Globals.ClientId, Globals.ClientSecret, Globals.GetUserToAuthorize(smtpObject?.EmailAddress)); }
                catch (Exception exc)
                {
                    //log error
                }

                foreach (var mailMsg in mailMsgs)
                {
                    try
                    {
                        var response = emailService.SendEmail(mailMsg);
                    }
                    catch (Exception)
                    {
                        //build list of msgs that didn't send successfully
                        //display error
                    }
                }
            }
            
            return true;


            bool res = smtpObject.SendOutTasksPerEmail(filteredOfficeStaffData, out smtpError);
            

            if (res)
            {
                string header = (1 == tasks.Count ? "Sending Task to Email/Text" : "Sending Tasks to Email/Text");
                string body = (1 == tasks.Count ? "Sending 1 task to " + taskOwner : "Sending " + tasks.Count.ToString() + " tasks to Email/Text to " + taskOwner);

                System.Windows.Forms.MessageBox.Show(body, header, System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Information);
            }
            else
            {
                System.Windows.Forms.MessageBox.Show(smtpError, "Taks sending to Email/Text failed", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
            }
        }

        public List<MailMessage> PrepareMailMsgs(Dictionary<Guid, List<string>> officeStaffTasks, string fromEmailAddress, string displayName)
        {                        
            string allRecipients = string.Empty;
            var mailMessages = new List<MailMessage>();
            var from = new MailAddress(fromEmailAddress, displayName);

            try
            {
                Dictionary<Guid, List<string>>.Enumerator enumerator = officeStaffTasks.GetEnumerator();
                
                while (enumerator.MoveNext())
                {
                    List<string> list = enumerator.Current.Value;

                    if (list.Count < 3)
                        continue;

                    //break recipients 
                    string[] emailRecipients = list[1].Split(',');

                    //foreach recipient create message
                    foreach (string recipient in emailRecipients)
                    {
                        bool cellEmailAddress = false;
                        bool validEmail = ApplicationHelper.IsCellPhoneEmailAddress(recipient, out cellEmailAddress);

                        if (!validEmail)
                            continue;

                        if (!cellEmailAddress)
                        {
                            MailMessage message = new MailMessage();
                            message.To.Add(recipient);
                            message.Subject = "ProfitGrabber Task Reminder";
                            message.From = from;
                            message.Body = list[2];
                            message.ReplyToList.Add(from);

                            mailMessages.Add(message);
                        }
                        else
                        {
                            for (int i = 0; i < list.Count - 3; i++)
                            {
                                MailMessage message = new MailMessage();
                                message.To.Add(recipient);
                                message.Subject = "ProfitGrabber Task Reminder";
                                message.From = from;
                                message.Body = list[3 + i]; //[list.Count - 4 + i];
                                message.ReplyToList.Add(from);

                                mailMessages.Add(message);
                            }
                        }
                    }
                    allRecipients += list[1] + "; ";
                }
                
            }
            catch 
            {                
                throw;                
            }

            return mailMessages;
        }
    }
}
