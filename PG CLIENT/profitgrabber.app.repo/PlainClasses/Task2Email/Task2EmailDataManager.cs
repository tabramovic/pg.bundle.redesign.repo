﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;


namespace DealMaker
{
    public sealed class Task2EmailDataManager
    {
        static readonly Task2EmailDataManager _instance = new Task2EmailDataManager();

        static Task2EmailDataManager()
        { }

        Task2EmailDataManager()
        { }

        public static Task2EmailDataManager Instance
        {
            get { return _instance; }
        }
        
        SqlConnection _connection;

        public void TestCreateDBLayer(SqlConnection conn)
        {
            if (null == _connection)
            {
                _connection = conn;

                string cmdText = "IF NOT EXISTS ( SELECT  * FROM    syscolumns WHERE id = OBJECT_ID('OfficeStaff') AND name = 'OfficeStaffEmailAddresses' )  ALTER TABLE OfficeStaff ADD OfficeStaffEmailAddresses nvarchar(1024) NULL ";

                try
                {
                    SqlCommand createOfficeStaffLayer = new SqlCommand(cmdText, _connection);
                    createOfficeStaffLayer.ExecuteNonQuery();
                    
                }
                catch (Exception exc)
                {
                    bool stopHere = true;
                }


                cmdText = "IF NOT EXISTS ( SELECT  * FROM    syscolumns WHERE id = OBJECT_ID('Task') AND name = 'CheckOut' ) BEGIN ALTER TABLE Task ADD CheckOut datetime NULL, CheckOutPrior int NULL  END";

                try
                {
                    SqlCommand createLayer = new SqlCommand(cmdText, _connection);
                    createLayer.ExecuteNonQuery();
                    
                }
                catch (Exception exc)
                {
                    bool stopHere = true;
                }

                cmdText = "if object_id('update_OfficeStaff') is null exec ('CREATE PROCEDURE update_OfficeStaff ( @OfficeStaffId uniqueidentifier, @OfficeStaffEmailAddresses nvarchar(1024)) AS UPDATE OfficeStaff SET OfficeStaffEmailAddresses = @OfficeStaffEmailAddresses WHERE OfficeStaffId = @OfficeStaffId')";

                try
                {
                    SqlCommand createSPL = new SqlCommand(cmdText, _connection);
                    createSPL.ExecuteNonQuery();

                }
                catch (Exception exc)
                {
                    bool stopHere = true;
                }

                cmdText = "ALTER PROCEDURE GetOfficeStaffList AS BEGIN SET NOCOUNT ON; SELECT OfficeStaffId, OfficeStaffName, OfficeStaffEmailAddresses from OfficeStaff END ";

                try
                {
                    SqlCommand updateSPL = new SqlCommand(cmdText, _connection);
                    updateSPL.ExecuteNonQuery();

                }
                catch (Exception exc)
                {
                    bool stopHere = true;
                }
            }
        }                
    }
}
