﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Text;

//Added
using System.Threading;
using System.Net;
using System.Net.Mail;
using google.oauth.lib;
using System.Data.SqlTypes;
using System.Windows.Forms;

namespace DealMaker
{
    public sealed class EmailSenderWorker
    {
        static readonly EmailSenderWorker _instance = new EmailSenderWorker();

        EmailSenderWorker()
        {
            mailMessagesToSend = new ConcurrentQueue<MailMessage>();               
        }

        static EmailSenderWorker()
        { }

        public static EmailSenderWorker Instance
        {
            get { return _instance; }
        }

        public delegate void StartSendingDelegate(MailMessage mm, int remaining);
        public event StartSendingDelegate StartSendingEmail;

        public delegate void SuccessSendingDelegate(MailMessage mm);
        public event SuccessSendingDelegate EmailSent;

        public delegate void FailedSendingDelegate(MailMessage mm, string msg);
        public event FailedSendingDelegate EmailFailed;

        public ConcurrentQueue<MailMessage> mailMessagesToSend { get; set; }

        public void Start()
        {
            System.Threading.Tasks.Task.Run(SendMailAsync);
            //Thread th = new Thread(SendMailAsync);
            //th.Start();
        }

        void OnStartSendingEmail(MailMessage mm, int count)
        {
            if (null != StartSendingEmail)
            {
                StartSendingEmail(mm, count);
            }
        }

        void OnEmailSent(MailMessage mm)
        {
            if (null != EmailSent)
            {
                EmailSent(mm);
            }
        }

        void OnEmailFailed(MailMessage mm, string msg)
        {
            if (null != EmailFailed)
            {
                EmailFailed(mm, msg);
            }
        }
                
        SmtpClient GetSMTPClient()
        {
            SMTPObject smtpObject = new SMTPObject().LoadSettigns();

            SmtpClient smtpClient = new SmtpClient(smtpObject.OutgoingSMTPServer);

            int port = 0;
            try { port = Convert.ToInt32(smtpObject.Port); }
            catch { port = 0; }

            smtpClient.Port = port; //587; 
            smtpClient.EnableSsl = true;
            smtpClient.UseDefaultCredentials = false;
            smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;

            smtpClient.Credentials = new NetworkCredential(smtpObject.EmailAddress, smtpObject.Password);

            //smtpClient.Timeout = 5000;

            return smtpClient;
        }

        async System.Threading.Tasks.Task SendMailAsync()
        {            
            while (!Task2EmailWorker.Instance.StopSync)
            {
                if (null != EmailSenderWorker.Instance.mailMessagesToSend && 0 < EmailSenderWorker.Instance.mailMessagesToSend.Count)
                {                    
                    MailMessage mm = null;
                    var deqRes = EmailSenderWorker.Instance.mailMessagesToSend.TryDequeue(out mm);
                    
                    if (deqRes && null != mm)
                    {
                        var smtpObject = new SMTPObject().LoadSettigns();
                        OnStartSendingEmail(mm, mailMessagesToSend.Count);
                        try 
                        { 
                            //smtpClient.Send(mm);

                            //new - beginning
                            
                            using (var emailService = new EmailService())
                            {
                                try { await emailService.InitializeGmailAccountService(Globals.ProductName, Globals.ClientId, Globals.ClientSecret, Globals.GetUserToAuthorize(smtpObject?.EmailAddress)); }
                                catch (Exception exc)
                                {
                                    //log error
                                    MessageBox.Show($"Gmail authorization failed or timed-out.{Environment.NewLine}{exc.Message}");
                                    return;

                                }
                                                                
                                try
                                {
                                    var response = emailService.SendEmail(mm);
                                }
                                catch (Exception e)
                                {
                                    //build list of msgs that didn't send successfully
                                    //display error

                                    OnEmailFailed(mm, e.Message);
                                }                                
                            }

                            //new - end
                            OnEmailSent(mm);
                        }
                        catch (Exception exc)
                        {
                            EmailSenderWorker.Instance.mailMessagesToSend.Enqueue(mm);
                            OnEmailFailed(mm, exc.Message);

                            //throw (exc);    //so that logger catches it.
                        }

                        Thread.Sleep(500);
                    }
                    else
                        Thread.Sleep(500);
                }
                else
                    Thread.Sleep(500);
            }
        }
    }
}
