using System;

//ADDED
using DealMaker;

namespace DealMaker.PlainClasses.ReturnMailWizard
{	
	/// <summary>
	/// Summary description for SkipTracedGroupAdapter.
	/// </summary>
	public class SkipTracedGroupAdapter
	{
		Node _fromGroup = null;
		Node _toGroup = null;

		public SkipTracedGroupAdapter()
		{			
		}

		public SkipTracedGroupAdapter(Node fromGroup)
		{			
			_fromGroup = fromGroup;
		}

		public SkipTracedGroupAdapter(Node fromGroup, Node toGroup)
		{			
			_fromGroup = fromGroup;
			_toGroup = toGroup;
		}

		public Node FromGroup
		{
			get { return _fromGroup; }
			set { _toGroup = value; }
		}

		public Node ToGroup
		{
			get { return _toGroup; }
			set { _toGroup = value; }
		}
	}
}
