using System;
using System.Collections;

//ADDED
using DealMaker;
using DealMaker.Forms.SchedulingActicities;

namespace DealMaker.PlainClasses.ReturnMailWizard
{
	/// <summary>
	/// SkipTracedContactAssistant implements new logic behind the skiptrace process
	/// a.) Creates new subgroup(s) and moves contacts into them when contacts either get scanned or returned by the skip tracer
	/// b.) Assignes parent activities to newly created groups 
	/// </summary>
	public sealed class SkipTracedContactAssistant
	{
		//SINGLETON
		static readonly SkipTracedContactAssistant _instance = new SkipTracedContactAssistant();
		static SkipTracedContactAssistant()
		{			
		}
		
		SkipTracedContactAssistant()
		{			
		}

		public static SkipTracedContactAssistant Instance
		{
			get { return _instance; }
		}
		//~SINGLETON

		Hashtable _createdGroupPairs = new Hashtable();

		public delegate void NewGroupsCreatedBySkipTracedContactAssistant();
		public static event NewGroupsCreatedBySkipTracedContactAssistant GROUPS_CREATED_BY_SKIPTRACED_CONTACT_ASSISTANT;

		public delegate void ShowGroupCreatedBySkipTracedContactAssistant(int groupId);
		public static event ShowGroupCreatedBySkipTracedContactAssistant SHOW_GROUP_CREATED_BY_SKIPTRACED_CONTACT_ASSISTANT;

		public delegate void ActivityAssignedBySkipTracedContactAssistant();
		public static event ActivityAssignedBySkipTracedContactAssistant ACTIVITY_ASSIGNED_BY_SKIPTRACED_CONTACT_ASSISTANT;

		int _lastGroupIdCreatedByAssistant = -1;

		public int LastGroupIdCreatedByAssistant
		{
			get {return _lastGroupIdCreatedByAssistant; }
		}

		
		/// <summary>
		/// ProcessScannedRecords:
		/// Foreach record (contact) in a list, do following:
		///		a.) Remove contact from original group(s)
		///		b.) Add contact to newly created subgroup called Scanned_(DATE)
		///		c.) Fire (trigger) event to other groupViews to force refresh
		///		
		///		RESULT:
		///		* Foreclosure week #30				PARENT GROUP
		///		*	* Scanned_(8/15/2008)			NEWLY CREATED SUBGROUP / SCAN PROCESS
		/// </summary>
		public void ProcessScannedRecords(ArrayList propertyItemIds)
		{
			_createdGroupPairs.Clear(); //RESET LIST

			//GET AFFECTED GROUPS
			Hashtable affectedGroupsForPropertyItem = GetAffectedGroupsForPropertyItem(ReturnMailWizardProcess.scanIn, propertyItemIds);

			//CREATE NEW GROUPS AND FILL FROM-TO LIST
			CreateSubgroups(ReturnMailWizardProcess.scanIn, ref affectedGroupsForPropertyItem);

			//ASSIGN CONTACTS TO NEW GROUPS (AND REMOVE FROM ORIGINAL GROUP)
			AssignContactsToNewGroups(affectedGroupsForPropertyItem);

			//FIRE TRIGGER TO UPDATE GROUP TREE VIEW
			//OnNewGroups();
		}

		/// <summary>
		/// ProcessReImportedRecords:
		/// Foreach record (contact) in a list, do following:
		///		a.) Place into newly generated subgroup called SkipTracedRecords_(DATE)
		///		
		///		RESULT:
		///		* Foreclosure week #30				PARENT GROUP
		///		*	* Scanned_(8/15/2008)			NEWLY CREATED SUBGROUP / SCAN PROCESS
		///		*	* SkipTracedRecords_(8/25/2008)	NEWLY CREATED SUBGROUP / RE-IMPORT PROCESS
		/// </summary>
		public void ProcessReImportedRecords(ArrayList propertyItemIds)
		{			
			_createdGroupPairs.Clear(); //RESET LIST

			//GET AFFECTED GROUPS
			Hashtable affectedGroupsForPropertyItem = GetAffectedGroupsForPropertyItem(ReturnMailWizardProcess.reImport, propertyItemIds);

			//CREATE NEW GROUPS AND FILL FROM-TO LIST
			CreateSubgroups(ReturnMailWizardProcess.reImport, ref affectedGroupsForPropertyItem);

			//ASSIGN CONTACTS TO NEW GROUPS (AND REMOVE FROM ORIGINAL GROUP)
			AssignContactsToNewGroups(affectedGroupsForPropertyItem);

			//FIRE TRIGGER TO UPDATE GROUP TREE VIEW
			//OnNewGroups();
		}

		public void ApplyParentGroupMarketingCapmaign(int nodeId)
		{
			int parentNodeId = ApplicationHelper.GetParentNodeId(nodeId);
			IList parentTasks = Static.GetGroupTasks(new Node(parentNodeId));

			foreach (Task task in parentTasks)
			{			
				//ALTER TASK DATE				
				task.EventDate = DateTime.Now;
				task.TaskDate = DateTime.Now;
				Static.AssignTaskToGroup(task, new Node(nodeId));
			}
			OnNewActivities();
		}

		public void ForceParentGroupMarketingCampaign(int nodeId)
		{
			int parentNodeId = ApplicationHelper.GetParentNodeId(nodeId);
			IList parentTasks = Static.GetGroupTasks(new Node(parentNodeId));

			foreach (Task task in parentTasks)
			{							
				Static.AssignTaskToGroup(task, new Node(nodeId));
			}
			OnNewActivities();
		}

		Hashtable GetAffectedGroupsForPropertyItem(ReturnMailWizardProcess process, ArrayList propertyItemIds)
		{
			Hashtable affectedGroupsForProperyItems = new Hashtable();
			Node statusGroup;
			ArrayList groups = new ArrayList();			

			foreach (Guid propertyItemId in propertyItemIds)
			{								
				groups = ApplicationHelper.GetGroupMembership(propertyItemId, out statusGroup);	
				ArrayList groupAdapterList = new ArrayList();
				
				switch (process)
				{
					case ReturnMailWizardProcess.scanIn:	//SCAN IN					
						if (null != statusGroup)
							groups.Add(statusGroup);	//we don't care about status group in this process
						
						foreach (Node group in groups)						
							groupAdapterList.Add(new SkipTracedGroupAdapter(group));
												
						break;

					case ReturnMailWizardProcess.reImport:	//RE-IMPORT												
						foreach (Node group in groups)						
							if (IsAutoGeneratedGroup(group))							
								groupAdapterList.Add(new SkipTracedGroupAdapter(group));
																			
						break;
				}
				affectedGroupsForProperyItems.Add(propertyItemId, (SkipTracedGroupAdapter[])groupAdapterList.ToArray(typeof(SkipTracedGroupAdapter)));
			}
			
			return affectedGroupsForProperyItems;
		}

		
		void CreateSubgroups(ReturnMailWizardProcess process, ref Hashtable affectedGroupsForPropertyItem)
		{			
			IDictionaryEnumerator ide = affectedGroupsForPropertyItem.GetEnumerator();

			while (ide.MoveNext())
			{
				Guid propertyItem = (Guid)ide.Key;
				SkipTracedGroupAdapter[] affectedGroupPairs = (SkipTracedGroupAdapter[])ide.Value;

				foreach (SkipTracedGroupAdapter affectedGroupPair in affectedGroupPairs)
				{
					string groupName = GetGroupName(process);
					string groupDescription = GetGroupDescription(process);

					switch (process)
					{
						case ReturnMailWizardProcess.scanIn:							
							int createdGroupId = GetCreatedGroupId(affectedGroupPair.FromGroup.nodeId);
							if (-1 == createdGroupId)
							{
								ApplicationHelper.CreateNode(affectedGroupPair.FromGroup.nodeId, groupDescription, groupName, out createdGroupId, Connection.getInstance().HatchConn);
								AddToCreatedGroupIds(affectedGroupPair.FromGroup.nodeId, createdGroupId);
							}
							affectedGroupPair.ToGroup = new Node(createdGroupId, affectedGroupPair.FromGroup.nodeId, groupDescription, groupName);							
							break;

						case ReturnMailWizardProcess.reImport:
							affectedGroupPair.FromGroup.parentNodeId = ApplicationHelper.GetParentNodeId(affectedGroupPair.FromGroup.nodeId);
							createdGroupId = GetCreatedGroupId(affectedGroupPair.FromGroup.nodeId);
							if (-1 == createdGroupId)
							{
								ApplicationHelper.CreateNode(affectedGroupPair.FromGroup.parentNodeId, groupDescription, groupName, out createdGroupId, Connection.getInstance().HatchConn);
								AddToCreatedGroupIds(affectedGroupPair.FromGroup.nodeId, createdGroupId);
							}
							affectedGroupPair.ToGroup = new Node(createdGroupId, affectedGroupPair.FromGroup.nodeId, groupDescription, groupName);
							//ShowNewGroup(createdGroupId);
							break;
					}
				}
			}						
		}

		
		void AssignContactsToNewGroups(Hashtable affectedGroupsForPropertyItem)
		{
			int lastAffectedGroupPair = -1;
			IDictionaryEnumerator ide = affectedGroupsForPropertyItem.GetEnumerator();
			while (ide.MoveNext())
			{
				Guid propertyItemId = (Guid)ide.Key;
				SkipTracedGroupAdapter[] affectedGroupPairs = (SkipTracedGroupAdapter[])ide.Value;
				
				foreach (SkipTracedGroupAdapter affectedGroupPair in affectedGroupPairs)
				{
					ApplicationHelper.DeletePropertyItemFromGroup(propertyItemId, affectedGroupPair.FromGroup.nodeId);
					ApplicationHelper.AssignPropertyItemToGroup(affectedGroupPair.ToGroup.nodeId, propertyItemId);

					//if (lastAffectedGroupPair != affectedGroupPair.ToGroup.nodeId)
					//	ShowNewGroup(affectedGroupPair.ToGroup.nodeId); //refresh only for new items

					if (-1 != affectedGroupPair.ToGroup.nodeId && lastAffectedGroupPair != affectedGroupPair.ToGroup.nodeId)
						lastAffectedGroupPair = affectedGroupPair.ToGroup.nodeId;
				}
			}

			if (-1 != lastAffectedGroupPair)
				ShowNewGroup(lastAffectedGroupPair); //refresh GroupView
		}
		
		
		void AddToCreatedGroupIds(int fromNodeId, int toNodeId)
		{
			if (!_createdGroupPairs.Contains(fromNodeId))
				_createdGroupPairs.Add(fromNodeId, toNodeId);
		}

		
		int GetCreatedGroupId(int fromNodeId)
		{
			if (!_createdGroupPairs.Contains(fromNodeId))
				return -1;
			else 
				return (int)_createdGroupPairs[fromNodeId];
		}
		
		void ShowNewGroup(int groupId)
		{
			if (null != SHOW_GROUP_CREATED_BY_SKIPTRACED_CONTACT_ASSISTANT)
			{
				SHOW_GROUP_CREATED_BY_SKIPTRACED_CONTACT_ASSISTANT(groupId);
				_lastGroupIdCreatedByAssistant = -1;	//-1 DO NOT SHOW, TRIGGER WILL DO IT
			}
			_lastGroupIdCreatedByAssistant = groupId;
		}

		void OnNewGroups()
		{
			if (null != GROUPS_CREATED_BY_SKIPTRACED_CONTACT_ASSISTANT)
			{
				GROUPS_CREATED_BY_SKIPTRACED_CONTACT_ASSISTANT();
			}
		}

		void OnNewActivities()
		{
			if (null != ACTIVITY_ASSIGNED_BY_SKIPTRACED_CONTACT_ASSISTANT)
			{
				ACTIVITY_ASSIGNED_BY_SKIPTRACED_CONTACT_ASSISTANT();
			}
		}

		string GetGroupName(ReturnMailWizardProcess process)
		{
			switch (process)
			{
				case ReturnMailWizardProcess.scanIn:
					return "Scanned_(" + DateTime.Now.ToShortDateString() + ", " + DateTime.Now.ToShortTimeString() + ")";					

				case ReturnMailWizardProcess.reImport:
					return "SkipTracedRecords_(" + DateTime.Now.ToShortDateString() + ", " + DateTime.Now.ToShortTimeString() + ")";
			}

			return string.Empty;
		}

		string GetGroupDescription(ReturnMailWizardProcess process)
		{
			switch (process)
			{
				case ReturnMailWizardProcess.scanIn:
					return "PG-STW-ScanIn AutoGeneratedGroup (DO NOT RENAME)";					

				case ReturnMailWizardProcess.reImport:
					return "PG-STW-SkipTracedRecords AutoGeneratedGroup (DO NOT RENAME)";
			}

			return string.Empty;
		}
		
		public bool IsAutoGeneratedGroup(Node group)
		{
			if (0 == group.nodeDesc.IndexOf("PG-STW-ScanIn") || 0 == group.nodeDesc.IndexOf("PG-STW-SkipTracedRecords"))
				return true;
			else
				return false;
		}
	}	
}
