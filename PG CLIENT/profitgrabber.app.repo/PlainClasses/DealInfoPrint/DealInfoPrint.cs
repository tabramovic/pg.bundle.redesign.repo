using System;
using System.Drawing;
using System.Drawing.Printing;
using System.Windows.Forms;

//added
using ProfitGrabber.Common;

namespace DealMaker.DealInfoPrint
{
	/// <summary>
	/// Summary description for DealInfoPrint.
	/// </summary>
	public class DealInfoPrint
	{
		public DealInfoPrint()
		{
			//
			// TODO: Add constructor logic here
			//
		}

/*			PrintDocument doc = new PrintDocument();
			doc.PrintPage += new PrintPageEventHandler(toPrinter);
			PrintPreviewDialog ppd = new PrintPreviewDialog();
			ppd.Document = doc;

			ppd.ShowDialog();

			PrintDialog pd = new PrintDialog();

			PrintDocument tmpprndoc = new PrintDocument();
			tmpprndoc.PrintPage += new PrintPageEventHandler(toPrinter);

			pd.Document = tmpprndoc;

			PrintPreviewDialog tmpprdiag = new PrintPreviewDialog();
				
			tmpprdiag.Document = tmpprndoc;
			tmpprdiag.ShowDialog();

		}
*/
		public static void ShowPrintForm(PropertyItem propertyItem)
		{
			pi = propertyItem;

			PrintDialog pd = new PrintDialog();

			PrintDocument tmpprndoc = new PrintDocument();
			tmpprndoc.PrintPage += new PrintPageEventHandler(toPrinter);

			pd.Document = tmpprndoc;
			pd.Document.DefaultPageSettings.Landscape = false;

            PrintPreviewDialogEx tmpprdiag = new PrintPreviewDialogEx();
				
			tmpprdiag.Document = tmpprndoc;
			tmpprdiag.ShowDialog();
		}

		static PropertyItem pi;
		static Graphics g;
		static Brush br;				

		private static void placeLabel(float x, float y, Font labelFont, string label, Font valFont, string val, float offset)
		{
			g.DrawString(label, labelFont, br, x, y);
			g.DrawString(val, valFont, br, x+g.MeasureString(label,labelFont).Width+offset, y);
		}

		private static void toPrinter(Object sender, PrintPageEventArgs e)
		{			

			//this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));

			Font font8  = new Font("Arial", 8);
			Font font8b  = new Font("Arial", 8, System.Drawing.FontStyle.Bold);
			Font font10  = new Font("Arial", 10);
			Font font10b  = new Font("Arial", 10, System.Drawing.FontStyle.Bold);
			Font font10i  = new Font("Arial", 10, System.Drawing.FontStyle.Italic);
			Font font12  = new Font("Arial", 12);
			Font font12b  = new Font("Arial", 12, System.Drawing.FontStyle.Bold);
			Font font14 = new Font( "Arial", 14);
			Font font14b = new Font( "Arial", 14, System.Drawing.FontStyle.Bold);
			Font font16  = new Font("Arial", 16);
			Font font16b  = new Font("Arial", 16, System.Drawing.FontStyle.Bold);

			g = e.Graphics;			

			br = new SolidBrush(Color.Black);				

			string fullSSA = string.Empty;	
			try{fullSSA = pi.Owner.SiteAddress.FullSiteStreetAddress + " " + pi.Owner.SiteAddress.SiteCitySiteStateSiteZIP;}
			catch{};
			g.DrawString("PROPERTY INFO SHEET - " + fullSSA, font14b, br, 100, 50);

			float labelPosRight = 40;
			float labelPosY = 70; //50;
			float labelPosYMove = 30;

			string fullName = "";
			try{fullName = pi.Owner.FullName;}
			catch{};
			placeLabel(labelPosRight,labelPosY+=labelPosYMove,font10b,"Full Name:",font10,fullName,5);

			string siteAddress = "";
			try{siteAddress = pi.Owner.SiteAddress.FullSiteStreetAddress;}
			catch{};
			placeLabel(labelPosRight,labelPosY+=labelPosYMove,font10b,"Site Address:",font10,siteAddress,5);

			string siteCSZ = "";
			try{siteCSZ = pi.Owner.SiteAddress.SiteCitySiteStateSiteZIP;}
			catch{};
			placeLabel(labelPosRight,labelPosY+=labelPosYMove,font10b,"",font10,siteCSZ,135);

			string mailAddress = "";
			try{mailAddress = pi.Owner.MailAddress.FullSiteStreetAddress;}
			catch{};
			placeLabel(labelPosRight,labelPosY+=labelPosYMove,font10b,"Mail Address:",font10,mailAddress,5);

			string mailCSZ = "";
			try{mailCSZ = pi.Owner.MailAddress.SiteCitySiteStateSiteZIP;}
			catch{};
			placeLabel(labelPosRight,labelPosY+=labelPosYMove,font10b,"",font10,mailCSZ,135);

			string oldAddress = "";
			try{oldAddress = pi.Owner.SecondAddress.FullSiteStreetAddress;}
			catch{};
			placeLabel(labelPosRight,labelPosY+=labelPosYMove,font10b,"Old Mail Address:",font10,oldAddress,5);

			string oldCSZ = "";
			try{oldCSZ = pi.Owner.SecondAddress.SiteCitySiteStateSiteZIP;}
			catch{};
			placeLabel(labelPosRight,labelPosY+=labelPosYMove,font10b,"",font10,oldCSZ,135);

			string propertyType = "";
			try{propertyType = pi.DealProperty.Type;}
			catch{};
			placeLabel(labelPosRight,labelPosY+=labelPosYMove*2,font10b,"Property Type:",font10,propertyType,5);

			string apn = "";
			try{apn = pi.DealProperty.Apn;}
			catch{};
			placeLabel(labelPosRight+305,labelPosY,font10b,"APN:",font10,apn,5);

			string bedrooms = "";
			try{bedrooms = pi.DealProperty.Bedrooms.ToString();}
			catch{};
			placeLabel(labelPosRight,labelPosY+=labelPosYMove,font10b,"Bedrooms:",font10,bedrooms,5);

			string baths = "";
			try{baths = pi.DealProperty.Baths.ToString();}
			catch{};
			placeLabel(labelPosRight+=110,labelPosY,font10b,"Baths:",font10,baths,5);

			string sqft = "";
			try{sqft = pi.DealProperty.Sqft.ToString();}
			catch{};
			placeLabel(labelPosRight+=85,labelPosY,font10b,"SqFt:",font10,sqft,5);

			string yb = "";
			try{yb = pi.DealProperty.YearBuilt.ToString();}
			catch{};
			placeLabel(labelPosRight+=110,labelPosY,font10b,"YB:",font10,yb,5);

			string garage = "";
			try{garage = pi.DealProperty.Garage;}
			catch{};
			placeLabel(labelPosRight+=100,labelPosY,font10b,"Garage:",font10,garage,5);

			string pool = "";
			try{pool = pi.DealProperty.Pool;}
			catch{};
			placeLabel(labelPosRight+=190,labelPosY,font10b,"Pool:",font10,pool,5);

			float labelPosRightOrig = labelPosRight = 40;

			string feature = "";
			try{feature = pi.DealProperty.FeatureCondition;}
			catch{};
			placeLabel(labelPosRight,labelPosY+=labelPosYMove,font10b,"Feature/Condition:",font10,feature,5);

			string why = "";
			try{why = pi.DealProperty.WhySelling;}
			catch{};
			placeLabel(labelPosRight,labelPosY+=labelPosYMove*(float)1.5,font10b,"WHY Selling:",font10,why,5);


			string wrth = "";
			try{wrth = pi.DealProperty.WorthBySeller;}
			catch{};
			placeLabel(labelPosRight,labelPosY+=labelPosYMove*(float)1.5,font10b,"WORTH By Seller: $",font10,wrth,5);


			string source = "";
			try{source = pi.DealProperty.Source;}
			catch{};
			placeLabel(labelPosRight+=300,labelPosY,font10b,"Source:",font10,source,5);


			labelPosRight = 40;
			
			placeLabel(labelPosRight,labelPosY+=labelPosYMove*(float)1.5,font10b,"OWE:",font10,"",5);








		{
			string val = "";
			try{val = pi.DealProperty.FirstBal.ToString();}
			catch{};
			placeLabel(labelPosRight+=50,labelPosY+=labelPosYMove,font10b,"1st Balance: $",font10,val,5);
		}

		{
			string val = "";
			try{val = pi.DealProperty.Ir1+"%";}
			catch{};
			placeLabel(labelPosRight+=200,labelPosY,font10b,"Int.Rate:",font10,val,5);
		}
		{
			string val = "";
			try{val = pi.DealProperty.Type1;}
			catch{};
			placeLabel(labelPosRight+=130,labelPosY,font10b,"Type:",font10,val,5);
		}
		{
			string val = "";
			try{val = pi.DealProperty.MoPay1;}
			catch{};
			placeLabel(labelPosRight+=150,labelPosY,font10b,"Mo.Payment: $",font10,val,5);
		}





			labelPosRight = labelPosRightOrig;

		{
			string val = "";
			try{val = pi.DealProperty.SecondBal;}
			catch{};
			placeLabel(labelPosRight+=50,labelPosY+=labelPosYMove,font10b,"2st Balance: $",font10,val,5);
		}

		{
			string val = "";
			try{val = pi.DealProperty.Ir2+"%";}
			catch{};
			placeLabel(labelPosRight+=200,labelPosY,font10b,"Int.Rate:",font10,val,5);
		}
		{
			string val = "";
			try{val = pi.DealProperty.Type2;}
			catch{};
			placeLabel(labelPosRight+=130,labelPosY,font10b,"Type:",font10,val,5);
		}
		{
			string val = "";
			try{val = pi.DealProperty.MoPay2;}
			catch{};
			placeLabel(labelPosRight+=150,labelPosY,font10b,"Mo.Payment: $",font10,val,5);
		}





			labelPosRight = labelPosRightOrig;
		{
			string val = "";
			try{val = pi.DealProperty.ThirdBal;}
			catch{};
			placeLabel(labelPosRight+=50,labelPosY+=labelPosYMove,font10b,"3rd Balance: $",font10,val,5);
		}

		{
			string val = "";
			try{val = pi.DealProperty.Ir3+"%";}
			catch{};
			placeLabel(labelPosRight+=200,labelPosY,font10b,"Int.Rate:",font10,val,5);
		}
		{
			string val = "";
			try{val = pi.DealProperty.Type3;}
			catch{};
			placeLabel(labelPosRight+=130,labelPosY,font10b,"Type:",font10,val,5);
		}
		{
			string val = "";
			try{val = pi.DealProperty.MoPay3;}
			catch{};
			placeLabel(labelPosRight+=150,labelPosY,font10b,"Mo.Payment: $",font10,val,5);
		}






		{
			string val = "";
			try{val = pi.DealProperty.TaxIns;}
			catch{};
			placeLabel(labelPosRight=120,labelPosY+=labelPosYMove,font10b,"1st includes Taxes&Ins:",font10,val,5);
		}

		{
			string val = "";
			try{val = pi.DealProperty.TaxAmnt;}
			catch{};
			placeLabel(labelPosRight+=300,labelPosY,font10b,"Tax&Ins/mo.: $",font10,val,5);
		}

		{
			string val = "";
			try{val = pi.DealProperty.Hoa;}
			catch{};
			placeLabel(labelPosRight+=200,labelPosY,font10b,"HOA/mo.: $",font10,val,5);
		}

		{
			string val = "";
			try{val = pi.DealProperty.OtherLiens;}
			catch{};
			placeLabel(labelPosRight=90,labelPosY+=labelPosYMove,font10b,"Other Liens: $",font10,val,5);
		}

		{
			string val = "";
			try{val = ApplicationHelper.GetTotalOwed(Globals.CurrentPropertyItem).ToString();}
			catch{};
			placeLabel(labelPosRight+=200,labelPosY,font10b,"*TOTAL OWED: $",font10,val,5);
		}

		{
			string val = "";
			try{val = ApplicationHelper.GetTotalMonthlyPayment(Globals.CurrentPropertyItem).ToString();}
			catch{};
			placeLabel(labelPosRight+=330,labelPosY,font10b,"*TOTAL MO: $",font10,val,5);
		}

			g.DrawLine(new Pen(Brushes.Black),90, labelPosY+=labelPosYMove, 800, labelPosY);

		{
			string val = "";
			try{val = pi.DealProperty.PmntsCurrent;}
			catch{};
			placeLabel(labelPosRight=90,labelPosY+=labelPosYMove/2,font10b,"Payments Current:",font10,val,5);
		}

		{
			string val = "";
			try{val = pi.DealProperty.MoBehind;}
			catch{};
			placeLabel(labelPosRight+=230,labelPosY,font10b,"Mo. Behind:",font10,val,5);
		}

		{
			string val = "";
			try{val = pi.DealProperty.InForecl;}
			catch{};
			placeLabel(labelPosRight+=120,labelPosY,font10b,"In Forecl.?",font10,val,5);
		}

		{
			string val = "";
			try{val = pi.DealProperty.ReInsAmnt;}
			catch{};
			placeLabel(labelPosRight+=140,labelPosY,font10b,"Reins.Amnt.: $",font10,val,5);
		}

		{
			string val = "";
			try{val = pi.DealProperty.ForSaleDate.ToShortDateString();}
			catch{};
			placeLabel(labelPosRight=90,labelPosY+=labelPosYMove,font10b,"Foreclosure Sale Date:",font10,val,5);
		}

		{
			string val = "";
			try{val = pi.DealProperty.Listed;}
			catch{};
			placeLabel(labelPosRight=90,labelPosY+=labelPosYMove,font10b,"Is it listed:",font10,val,5);
		}

		{
			string val = "";
			try{val = pi.DealProperty.ListExpDate.ToShortDateString();}
			catch{};
			placeLabel(labelPosRight+=150,labelPosY,font10b,"Listing Exp Date:",font10,val,5);
		}

		{
			string val = "";
			try{val = pi.DealProperty.CanCancelList;}
			catch{};
			placeLabel(labelPosRight+=300,labelPosY,font10b,"Can cancel list?",font10,val,5);
		}
			
			placeLabel(labelPosRightOrig,labelPosY+=labelPosYMove*(float)1.5,font10b,"WANTS:",font10,"",5);

		{
			string val = "";
			try{val = pi.DealProperty.LowestPriceAcceptable;}
			catch{};
			placeLabel(labelPosRight=90,labelPosY+=labelPosYMove,font10b,"Lowest Price Acceptable: $",font10,val,5);
		}

		{
			string val = "";
			try{val = pi.DealProperty.Sell4WhatOwes;}
			catch{};
			placeLabel(labelPosRight+=400,labelPosY,font10b,"Sell 4 What Owes?",font10,val,5);
		}

		{
			string val = "";
			try{val = pi.DealProperty.MinCashNeededWhy;}
			catch{};
			placeLabel(labelPosRight=90,labelPosY+=labelPosYMove,font10b,"Min. Cash Needed-why: $",font10,val,5);
		}

		{
			string val = "";
			try{val = pi.DealProperty.DesiredClosingDate;}
			catch{};
			placeLabel(labelPosRight+=400,labelPosY,font10b,"Desired Closing Date: ",font10,val,5);
		}

		{
			string val = "";
			try{val = pi.DealProperty.Motivation;}
			catch{};
			placeLabel(labelPosRight=labelPosRightOrig,labelPosY+=labelPosYMove*(float)1.5,font10b,"MOTIVATION:",font10,val,5);
		}

		/*{
			string val = "";
			try{val = pi.DealProperty.LeadSource;}
			catch{};
			placeLabel(labelPosRight+=400,labelPosY,font10b,"Lead Source:",font10,val,5);
		}

		{
			string val = "";
			try{val = pi.DealProperty.Arv;}
			catch{};
			placeLabel(labelPosRight=labelPosRightOrig,labelPosY+=labelPosYMove,font10b,"ARV: $",font10,val,5);
		}*/
		{
			string val = "";
			try{val = pi.DealProperty.Arv;}
			catch{};
			placeLabel(labelPosRight+=450,labelPosY,font10b,"ARV: $",font10,val,5);
		}

		{
			string val = "";
			try{val = pi.DealProperty.LeadSource;}
			catch{};
			placeLabel(labelPosRight=labelPosRightOrig,labelPosY+=labelPosYMove,font10b,"Lead Source:",font10,val,5);
		}

		{
			string val = "";
			try{val = pi.DealProperty.Repairs;}
			catch{};
			placeLabel(labelPosRight+=450,labelPosY,font10b,"Repairs: $",font10,val,5);
		}

		{
			string val = "";
			try{val = pi.DealProperty.NextStep;}//valjda
			catch{};
			placeLabel(labelPosRight=labelPosRightOrig,labelPosY+=labelPosYMove,font10b,"Notes:",font10,val,5);
		}

			float phoneLabelPosY = labelPosY = 110;
			float phoneLabelPosRight = 450;
			float phoneLabelPosYMove = 25;
			float phoneMoveRight = 220;

		{
			string val = "";
			try{val = pi.Owner.HomePhone.PhoneNr;}
			catch{};
			placeLabel(labelPosRight=phoneLabelPosRight,labelPosY,font10b,"Home:",font10,val,5);
		}
		{
			string val = "";
			try{val = pi.Owner.HomePhone.BestTimeToCall;}
			catch{};
			placeLabel(labelPosRight+=phoneMoveRight,labelPosY,font10b,"Time:",font10,val,5);
		}

		{
			string val = "";
			try{val = pi.Owner.WorkPhone.PhoneNr;}
			catch{};
			placeLabel(labelPosRight=phoneLabelPosRight,labelPosY+=phoneLabelPosYMove,font10b,"Work:",font10,val,5);
		}
		{
			string val = "";
			try{val = pi.Owner.WorkPhone.BestTimeToCall;}
			catch{};
			placeLabel(labelPosRight+=phoneMoveRight,labelPosY,font10b,"Time:",font10,val,5);
		}

		{
			string val = "";
			try{val = pi.Owner.CellPhone.PhoneNr;}
			catch{};
			placeLabel(labelPosRight=phoneLabelPosRight,labelPosY+=phoneLabelPosYMove,font10b,"Cell:",font10,val,5);
		}
		{
			string val = "";
			try{val = pi.Owner.CellPhone.BestTimeToCall;}
			catch{};
			placeLabel(labelPosRight+=phoneMoveRight,labelPosY,font10b,"Time:",font10,val,5);
		}

		{
			string val = "";
			try{val = pi.Owner.FaxNumber;}
			catch{};
			placeLabel(labelPosRight=phoneLabelPosRight,labelPosY+=phoneLabelPosYMove,font10b,"Fax:",font10,val,5);
		}

		{
			string val = "";
			try{val = pi.Owner.EMail;}
			catch{};
			placeLabel(labelPosRight=phoneLabelPosRight,labelPosY+=phoneLabelPosYMove,font10b,"Email:",font10,val,5);
		}

			g.DrawRectangle(new Pen(Brushes.Black), phoneLabelPosRight-10,phoneLabelPosY-10,350,labelPosY+40-phoneLabelPosY);

		{
			string val = "";
			try{val = pi.DateCreated.ToShortDateString();}
			catch{};
			placeLabel(phoneLabelPosRight-10,phoneLabelPosY-30,font10i,"First Entered:",font10i,val,5);
		}

		{
			string val = "";
			try{val = pi.DateLastModified.ToShortDateString();}
			catch{};
			placeLabel(phoneLabelPosRight+165,phoneLabelPosY-30,font10i,"Last Modified:",font10i,val,5);
		}

		}		
	}
}
