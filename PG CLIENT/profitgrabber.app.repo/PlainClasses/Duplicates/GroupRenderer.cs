using System;
using System.Drawing;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Collections;
using System.Windows.Forms;

using DealMaker;

namespace DealMaker.PlainClasses.Duplicates
{	
	public enum GroupSelectionStatus
	{
		NotSelectedGroup = 0,
		NotSelectedGroupAndSubGroups = 1,		
		SelectedGroup = 2,				
		SelectedGroupAndSubGroups = 3,
	}
	
	public class GroupRenderer
	{
		ArrayList alExpandedNodes = new ArrayList();
		Hashtable htNodeId_TreeNode = new Hashtable();		


		public GroupRenderer()
		{
			
		}

		public TreeNode BuildStructureFromScratch()
		{
			TreeNode tn = new TreeNode(Globals.RootNode, 2, 2);
			tn.Tag = new object[] { new Node(1), GroupSelectionStatus.NotSelectedGroupAndSubGroups };
			tn.Expand();

			return BuildStructure(tn);
		}

		public TreeNode BuildStructure(TreeNode tn)
		{
			
			ArrayList tempTreeNodeList = new ArrayList();
			bool hasChildren = false;			
						
			// get children nodes
			try
			{	
				object[] arr = (object[])tn.Tag;
				Node n = (Node)arr[0];
								
				SqlConnection connection = new SqlConnection();				
				connection.ConnectionString = Globals.GetConnectionString();
				connection.Open();
				SqlCommand sp_getChildrenFromNode = new SqlCommand("getChildrenFromNode", connection);
				sp_getChildrenFromNode.CommandType = CommandType.StoredProcedure;								
				sp_getChildrenFromNode.Parameters.Add("@parentNode", n.nodeId);
									
				if (ConnectionState.Open != Connection.getInstance().HatchConn.State)
					Connection.getInstance().HatchConn.Open();

				sp_getChildrenFromNode.ExecuteNonQuery();

				SqlDataReader nodeReader = sp_getChildrenFromNode.ExecuteReader();

				while (nodeReader.Read())
				{
					Int32	nodeId				= nodeReader.GetInt32(0);
					Int32	parentNodeId		= nodeReader.GetInt32(1);					
					string	nodeDesc			= nodeReader.GetString(2).Trim();
					string	nodeName			= nodeReader.GetString(3).Trim();
					SqlBoolean isPreDefined		= nodeReader.GetBoolean(4);

					TreeNode tempTreeNode = new TreeNode(nodeName);
					
					if (nodeId <= 28)
						tempTreeNode.ForeColor = Color.Blue;

					tempTreeNode.Tag = new object[] { 
													new Node(nodeId, parentNodeId, nodeDesc, nodeName, false, isPreDefined.Value), 
													GroupSelectionStatus.NotSelectedGroupAndSubGroups };
					tempTreeNode.ImageIndex = tempTreeNode.SelectedImageIndex = 2;					
					tempTreeNodeList.Add(tempTreeNode);
					hasChildren = true;	
					
					
					foreach (TreeNode expandedTreeNode in this.alExpandedNodes)
					{
						object[] arrSub = (object[])expandedTreeNode.Tag;
						Node node = (Node)arrSub[0];

						if (node.nodeId == nodeId)
						{	
							object[] tempArr = (object[])tempTreeNode.Tag;
							Node savedNode = (Node)tempArr[0];
							savedNode.isExpanded = true;
						}
					}					
					
					if (false == this.htNodeId_TreeNode.Contains(nodeId))
					{
						this.htNodeId_TreeNode.Add(nodeId, tempTreeNode);
					}

				}				
				nodeReader.Close();				
				connection.Close();				
			}
			catch( Exception ex)
			{
				MessageBox.Show(ex.ToString());
				return null;
			}

			// has children
			if (true == hasChildren)
			{
				while (tempTreeNodeList.Count > 0)
				{	
					TreeNode tnTemp = (TreeNode)tempTreeNodeList[0];
					tempTreeNodeList.RemoveAt(0);
					TreeNode outTn = BuildStructure(tnTemp);
				
					tn.Nodes.Add(outTn);
				}
				return tn;
			}
			else
			{
				return tn;
			}
		}

	}
}
