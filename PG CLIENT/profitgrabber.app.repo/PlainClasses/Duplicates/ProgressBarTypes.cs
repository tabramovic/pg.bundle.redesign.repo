using System;

namespace DealMaker.PlainClasses.Duplicates
{
	/// <summary>
	/// Progress Bar renderer subscribes to various events defined in this enum
	/// </summary>
	public enum eProgressBarTypes
	{
		Load = 0, 
		Match, 
		Merge,
		Delete,
	}
}
