using System;
using System.Collections;

namespace DealMaker.PlainClasses.Duplicates
{
	/// <summary>
	/// Summary description for DuplicatePurgatory.
	/// </summary>
	public class DuplicatePurgatory
	{
		ArrayList _contacts = null;
		bool _keepLastModified = true;

		public DuplicatePurgatory(ArrayList alDuplicates, bool keepLastModified)
		{
			_contacts = alDuplicates;
			_keepLastModified = keepLastModified;
		}

		public ArrayList GetSortedList()
		{
			if (_keepLastModified)
				_contacts.Sort(new DateLastModifiedComparer());
			else
				_contacts.Sort(new DateCreatedComparer());

			return _contacts;
		}

		public ArrayList GetNewList()
		{			
			_contacts = GetSortedList();			
			_contacts.RemoveAt(0);
			
			return _contacts;			
		}
	}

	public class DateLastModifiedComparer : IComparer
	{
		#region IComparer Members

		public int Compare(object x, object y)
		{
			ContactEntry ceX = (ContactEntry)x;
			ContactEntry ceY = (ContactEntry)y;

			return DateTime.Compare(ceY.DateLastModified, ceX.DateLastModified);
		}

		#endregion
	}

	public class DateCreatedComparer : IComparer
	{
		#region IComparer Members

		public int Compare(object x, object y)
		{
			ContactEntry ceX = (ContactEntry)x;
			ContactEntry ceY = (ContactEntry)y;

			return DateTime.Compare(ceX.DateCreated, ceY.DateCreated);
		}

		#endregion
	}
}
