using System;
using System.Collections;

namespace DealMaker.PlainClasses.Duplicates
{
	/// <summary>
	/// Summary description for DuplicateEntry.
	/// </summary>
	public class ContactEntry
	{
		Guid _contactId;
		string _firstName;
		string _middleName;
		string _lastName;
		string _fullName;
		DateTime _dateCreated;
		DateTime _dateLastModified;

		//FOR SORTING PURPOSES
		int _contactEntryAdapter;

		//Added
		string _fullSiteStreetAddress = string.Empty;
		string _siteStreetNumber = string.Empty;
		string _siteStreetPreDirectional = string.Empty;
		string _siteStreetName = string.Empty; 
		string _siteStreetPostDirectional = string.Empty;
		string _siteStreetSuffix = string.Empty;
		string _siteStreetUnitNumber = string.Empty;
		string _siteCity = string.Empty;
		string _siteState = string.Empty;
		string _siteCitySiteState = string.Empty;
		string _siteCitySiteStateSiteZIP = string.Empty;
		string _siteZIP = string.Empty;

		public ContactEntry(Guid contactId, string firstName, string middleName, string lastName, string fullName, DateTime dateCreated, DateTime dateLastModified, 
			string fullSiteStreetAddress, string siteStreetNumber, string siteStreetPreDirectional, string siteStreetName, string siteStreetPostDirectional, 
			string siteStreetSuffix, string siteStreetUnitNumber, string siteCity, string siteState, string siteCitySiteState, string siteCitySiteStateSiteZIP, 
			string siteZIP)
		{
			_contactId = contactId;
			_firstName = firstName;
			_middleName = middleName;
			_lastName = lastName;
			_fullName = fullName;
			_dateCreated = dateCreated;
			_dateLastModified = dateLastModified;

			_fullSiteStreetAddress = fullSiteStreetAddress;
			_siteStreetNumber = siteStreetNumber;
			_siteStreetPreDirectional = siteStreetPreDirectional;
			_siteStreetName = siteStreetName; 
			_siteStreetPostDirectional = siteStreetPostDirectional;
			_siteStreetSuffix = siteStreetSuffix;
			_siteStreetUnitNumber = siteStreetUnitNumber;
			_siteCity = siteCity;
			_siteState = siteState;
			_siteCitySiteState = siteCitySiteState;
			_siteCitySiteStateSiteZIP = siteCitySiteStateSiteZIP;
			_siteZIP = siteZIP;

		}

		public Guid ContactId
		{
			get { return _contactId; }
		}

		public int ContactEntryAdapter
		{
			get { return _contactEntryAdapter; }
			set { _contactEntryAdapter = value; }
		}

		public string FirstName
		{
			get { return _firstName.Trim(); }
		}

		public string MiddleName
		{
			get { return _middleName.Trim(); }
		}

		public string LastName
		{
			get { return _lastName.Trim(); }
		}

		public string FullName
		{
			get 
			{ 				
				if (string.Empty == _fullName.Trim())
				{					
					string fn = string.Empty;
					if (string.Empty != FirstName)
						fn += FirstName;
					if (string.Empty != MiddleName)
					{
						if (string.Empty != fn)
							fn += " ";
						fn += MiddleName;
					}
					if (string.Empty != LastName)
					{
						if (string.Empty != fn)
							fn += " ";
						fn += LastName;
					}
					return fn;
				}
				else
					return _fullName.Trim();
			}
		}

		public DateTime DateCreated
		{
			get { return _dateCreated; }
		}

		public DateTime DateLastModified
		{
			get { return _dateLastModified; }
		}

		public string FullSiteStreetAddress 
		{
			get { return _fullSiteStreetAddress; }
		}

		public string SiteStreetNumber
		{
			get { return _siteStreetNumber; }
		}

		public string SiteStreetPreDirectional
		{
			get { return _siteStreetPreDirectional; }
		}

		public string SiteStreetName
		{
			get { return _siteStreetName; }
		}

		public string SiteStreetPostDirectional
		{
			get { return _siteStreetPostDirectional; }
		}

		public string SiteStreetSuffix
		{
			get { return _siteStreetSuffix; }
		}

		public string SiteStreetUnitNumber
		{
			get { return _siteStreetUnitNumber; }
		}

		public string SiteCity
		{
			get { return _siteCity; }
		}

		public string SiteState
		{
			get { return _siteState; }
		}

		public string SiteCitySiteState
		{
			get 
			{ 				
				if (string.Empty != _siteCitySiteState)
					return _siteCitySiteState;
				else
				{
					if (string.Empty != _siteCity.Trim() && string.Empty != _siteState.Trim())
						return _siteCity.Trim() + ", " + _siteState.Trim();
					else
					{
						if (string.Empty != _siteCity.Trim())
							return _siteCity.Trim();
						else
							return _siteState.Trim();
					}
				}
			}
		}

		public string SiteCitySiteStateSiteZIP
		{
			get 
			{ 
				if (string.Empty != _siteCitySiteStateSiteZIP.Trim())
				{
					return _siteCitySiteStateSiteZIP.Trim();
				}
				else
				{
					if (string.Empty != SiteZIP)
						return SiteCitySiteState + " " + SiteZIP;
					else
						return SiteCitySiteState;
				}
			}
		}

		public string SiteZIP
		{
			get { return _siteZIP.Trim(); }
		}

		public string ComparableAddress
		{
			get 
			{
				string fssa = string.Empty;
				if (string.Empty == FullSiteStreetAddress.Trim())
				{
					fssa =	string.Empty;
					if (null != _siteStreetNumber && string.Empty != _siteStreetNumber.Trim())
						fssa += _siteStreetNumber.Trim();

					if (null != _siteStreetPreDirectional && string.Empty != _siteStreetPreDirectional.Trim())
						fssa += " " + _siteStreetPreDirectional.Trim();

					if (null != _siteStreetName && string.Empty != _siteStreetName.Trim())
						fssa += " " + _siteStreetName.Trim();

					if (null != _siteStreetPostDirectional && string.Empty != _siteStreetPostDirectional.Trim())
						fssa += " " + _siteStreetPostDirectional.Trim();

					if (null != _siteStreetSuffix && string.Empty != _siteStreetSuffix.Trim())
						fssa += " " + _siteStreetSuffix.Trim();

					if (null != _siteStreetUnitNumber && string.Empty != _siteStreetUnitNumber.Trim())
						fssa += " " + _siteStreetUnitNumber.Trim();																	
				}
				else
					fssa = FullSiteStreetAddress.Trim();

				if (null != SiteCitySiteStateSiteZIP && string.Empty != SiteCitySiteStateSiteZIP.Trim())
					fssa += " " + SiteCitySiteStateSiteZIP.Trim();
				
				return fssa;
			}
		}

		public override bool Equals(object obj)
		{
			ContactEntry ce = (ContactEntry)obj;

			return Guid.Equals(_contactId, ce.ContactId);
			//return base.Equals (obj);
		}

		public override int GetHashCode()
		{
			return base.GetHashCode ();
		}



		public override string ToString()
		{
			return "FN: " + FirstName + string.Empty != MiddleName ? "MN: " + MiddleName : " " + "LN: " + LastName + ", " + ContactId.ToString();
		}
	}
}
