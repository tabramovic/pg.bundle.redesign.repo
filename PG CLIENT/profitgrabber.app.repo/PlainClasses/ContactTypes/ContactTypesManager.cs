﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace DealMaker.PlainClasses.ContactTypes
{
    public enum ContactTypes
    {
        Seller, 
        Buyer,
        Lender,
        Undefined
    }

    public sealed class ContactTypesManager
    {
        #region SINGLETON
        static readonly ContactTypesManager _instance = new ContactTypesManager();

        static ContactTypesManager()
        { }

        ContactTypesManager()
        { }

        public static ContactTypesManager Instance
        {
            get { return _instance; }
        }
        #endregion

        #region DATA
        const int RootNodeId = 1;
        const int StatusGroupId = 2;

        public const string PrivateLenderGroup = "Private Lenders Group";
        public const string CashBuyersGroup = "Cash Buyers Group";

        public const string PrivateLenderGroupDesc = "Private Lenders Group";
        public const string CashBuyersGroupDesc = "Cash Buyers Group";

        int _privateLenderGroupId = -1;
        int _cashBuyersGroupId = -1;
        #endregion

        #region PROPERTIES
        public int PrivateLenderGroupId
        {
            get 
            {
                if (!InitedClass())
                    TestCreateNewContactTypes();

                return _privateLenderGroupId;
            }
        }

        public int CashBuyersGroupId
        {
            get
            {
                if (!InitedClass())
                    TestCreateNewContactTypes();

                return _cashBuyersGroupId;
            }
        }
        #endregion

        #region METHODS
        bool InitedClass()
        {
            if (-1 == _privateLenderGroupId || -1 == _cashBuyersGroupId)
                return false;
            else
                return true;
        }
        
        public void TestCreateNewContactTypes()
        {
            if (InitedClass())
                return;
 
            if (!TestForGroup(PrivateLenderGroup, out _privateLenderGroupId))
                ApplicationHelper.CreateNode(RootNodeId, PrivateLenderGroupDesc, PrivateLenderGroup, out _privateLenderGroupId, Connection.getInstance().HatchConn);

            if (!TestForGroup(CashBuyersGroup, out _cashBuyersGroupId))
                ApplicationHelper.CreateNode(RootNodeId, CashBuyersGroupDesc, CashBuyersGroup, out _cashBuyersGroupId, Connection.getInstance().HatchConn);            
        }

        bool TestForGroup(string groupName, out int groupId)
        {
            bool bRes = false;
            groupId = -1;
            SqlDataReader reader = null;            

            string cmdText = "select nodeId from Node where nodeName = '" + groupName + "'";

            try
            {
                SqlCommand command = new SqlCommand(cmdText, Connection.getInstance().HatchConn);
                reader = command.ExecuteReader();

                while (reader.Read())
                {
                    bRes = true;
                    groupId = !reader.IsDBNull(0) ? reader.GetInt32(0) : -1;

                    break;
                }
            }
            catch { }
            finally
            {
                if (null != reader && !reader.IsClosed)
                    reader.Close();
            }

            return bRes;
        }

        public bool AllowImport(ContactTypes contactType, int destGroupId)
        {
            if (ContactTypes.Buyer == contactType)
            {
                ArrayList cashBuyersSubGroups = new ArrayList();
                new ApplicationHelper().GetSubNodes(ContactTypesManager.Instance.CashBuyersGroupId, ref cashBuyersSubGroups);

                if (cashBuyersSubGroups.Contains(destGroupId))
                    return true;
                else
                    return false;
            }

            else if (ContactTypes.Lender == contactType)
            {
                ArrayList privateLendersSubGroups = new ArrayList();
                new ApplicationHelper().GetSubNodes(ContactTypesManager.Instance.PrivateLenderGroupId, ref privateLendersSubGroups);

                if (privateLendersSubGroups.Contains(destGroupId))
                    return true;
                else
                    return false;
            }

            else if (ContactTypes.Seller == contactType)
            {
                ArrayList privateLendersSubGroups = new ArrayList();
                new ApplicationHelper().GetSubNodes(ContactTypesManager.Instance.PrivateLenderGroupId, ref privateLendersSubGroups);

                ArrayList cashBuyersSubGroups = new ArrayList();
                new ApplicationHelper().GetSubNodes(ContactTypesManager.Instance.CashBuyersGroupId, ref cashBuyersSubGroups);

                if (privateLendersSubGroups.Contains(destGroupId))
                    return false;

                if (cashBuyersSubGroups.Contains(destGroupId))
                    return false;
                
            }

            return true;
        }

        public ContactTypes GetCustomTypeGroup(Guid propertyItemId)
        {
            Node statusGroup;
            ArrayList belongingGroups = ApplicationHelper.GetGroupMembership(propertyItemId, out statusGroup);

            if (null != belongingGroups && belongingGroups.Count > 0)
                return GetCustomTypeGroup(((Node)belongingGroups[0]).nodeId);
            else
                return ContactTypes.Undefined;
        }

        public ContactTypes GetCustomTypeGroup(int nodeId)
        {
            ArrayList privateLendersSubGroups = new ArrayList();
            new ApplicationHelper().GetSubNodes(ContactTypesManager.Instance.PrivateLenderGroupId, ref privateLendersSubGroups);

            ArrayList cashBuyersSubGroups = new ArrayList();
            new ApplicationHelper().GetSubNodes(ContactTypesManager.Instance.CashBuyersGroupId, ref cashBuyersSubGroups);

            if (privateLendersSubGroups.Contains(nodeId))
                return ContactTypes.Lender;

            if (cashBuyersSubGroups.Contains(nodeId))
                return ContactTypes.Buyer;

            return ContactTypes.Seller;
        }

        bool AllowBelonging(int srcGroupId, int destGroupId)
        {
            bool bRes = false;

            //PrivateLenders & CashBuyers may belong to:
            //  their own respecive subgroups,
            //  status group (and its subgroups)
            //  auto lead group

            ArrayList cashBuyersSubGroups = new ArrayList();
            new ApplicationHelper().GetSubNodes(ContactTypesManager.Instance.CashBuyersGroupId, ref cashBuyersSubGroups);

            ArrayList privateLendersSubGroups = new ArrayList();
            new ApplicationHelper().GetSubNodes(ContactTypesManager.Instance.PrivateLenderGroupId, ref privateLendersSubGroups);

            ArrayList statusGroupSubGroups = new ArrayList();
            new ApplicationHelper().GetSubNodes(StatusGroupId, ref statusGroupSubGroups);
            
            if (srcGroupId == ContactTypesManager.Instance.CashBuyersGroupId ||                     
                cashBuyersSubGroups.Contains(srcGroupId)) 
            {                
                if (cashBuyersSubGroups.Contains(destGroupId))
                    return true;
                
                if (statusGroupSubGroups.Contains(destGroupId))
                    return true;

                return false;
            }

            if (srcGroupId == ContactTypesManager.Instance.PrivateLenderGroupId ||
                privateLendersSubGroups.Contains(srcGroupId))
            {
                if (privateLendersSubGroups.Contains(destGroupId))
                    return true;

                if (privateLendersSubGroups.Contains(destGroupId))
                    return true;

                return false;
            }


            if (destGroupId == ContactTypesManager.Instance.CashBuyersGroupId ||
                cashBuyersSubGroups.Contains(destGroupId))
            {
                if (cashBuyersSubGroups.Contains(srcGroupId))
                    return true;

                return false;
            }

            if (destGroupId == ContactTypesManager.Instance.PrivateLenderGroupId ||
                privateLendersSubGroups.Contains(destGroupId))
            {
                if (privateLendersSubGroups.Contains(srcGroupId))
                    return true;

                return false;
            }

            

            return bRes;
        }
        #endregion
    }
}
