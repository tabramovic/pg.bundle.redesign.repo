#region � Using �
using System;
using System.Data.SqlClient;
using System.Data;
using System.Windows.Forms;
#endregion � Using �
#region � Namespace DealMaker �
namespace DealMaker
{
	#region � Class LoanProperties �
	/// <summary>
	/// LoanProperties.
	/// </summary>
	public class LoanProperties : INHibernate, ISPL
	{
		#region � Data �
		private Guid	idLoanProperty		=	Guid.Empty;
		private	string	firstLoanBalance	=	string.Empty;
		private	string	firstLoanMonthly	=	string.Empty;
		private	string	secondLoanBalance	=	string.Empty;
		private	string	secondLoanMonthly	=	string.Empty;
		private	string	thirdLoanBalance	=	string.Empty;
		private	string	thirdLoanMonthly	=	string.Empty;
		private	double	amntOfOtherLiens	=	0;
		private	string	hOAPerMonth			=	string.Empty;
		private	double	totalLoans			=	0;
		private	string	maxOffer			=	string.Empty;
		#endregion � Data �
		#region � CTR �
		public LoanProperties()
		{
			this.idLoanProperty = Guid.NewGuid();
		}
		#endregion � CTR �
		#region � Automatically Generated Properties �
		#region � IdLoanProperty �
		public Guid IdLoanProperty
		{
			get {return this.idLoanProperty;}
			set {this.idLoanProperty = value;}
		}
		#endregion � IdLoanProperty �
		#region · FirstLoanBalance ·
		//[Description("First Loan Balance")]
		public string FirstLoanBalance
		{
			get
			{
				return this.firstLoanBalance;
			}
			set
			{
				this.firstLoanBalance = value;
			}
		}
		#endregion · FirstLoanBalance ·
		#region · FirstLoanMonthly ·
		//[Description("First Loan Monthly")]
		public string FirstLoanMonthly
		{
			get
			{
				return this.firstLoanMonthly;
			}
			set
			{
				this.firstLoanMonthly = value;
			}
		}
		#endregion · FirstLoanMonthly ·
		#region · SecondLoanBalance ·
		//[Description("Second Loan Balance")]
		public string SecondLoanBalance
		{
			get
			{
				return this.secondLoanBalance;
			}
			set
			{
				this.secondLoanBalance = value;
			}
		}
		#endregion · SecondLoanBalance ·
		#region · SecondLoanMonthly ·
		//[Description("Second Loan Monthly")]
		public string SecondLoanMonthly
		{
			get
			{
				return this.secondLoanMonthly;
			}
			set
			{
				this.secondLoanMonthly = value;
			}
		}
		#endregion · SecondLoanMonthly ·
		#region · ThirdLoanBalance ·
		//[Description("Third Loan Balance")]
		public string ThirdLoanBalance
		{
			get
			{
				return this.thirdLoanBalance;
			}
			set
			{
				this.thirdLoanBalance = value;
			}
		}
		#endregion · ThirdLoanBalance ·
		#region · ThirdLoanMonthly ·
		//[Description("Third Loan Monthly")]
		public string ThirdLoanMonthly
		{
			get
			{
				return this.thirdLoanMonthly;
			}
			set
			{
				this.thirdLoanMonthly = value;
			}
		}
		#endregion · ThirdLoanMonthly ·
		#region · AmntOfOtherLiens ·
		//[Description("Amnt Of Other Liens")]
		public double AmntOfOtherLiens
		{
			get
			{
				return this.amntOfOtherLiens;
			}
			set
			{
				this.amntOfOtherLiens = value;
			}
		}
		#endregion · AmntOfOtherLiens ·
		#region · HOAPerMonth ·
		//[Description("HOA Per Month")]
		public string HOAPerMonth
		{
			get
			{
				return this.hOAPerMonth;
			}
			set
			{
				this.hOAPerMonth = value;
			}
		}
		#endregion · HOAPerMonth ·
		#region · TotalLoans ·
		//[Description("Total Loans")]
		public double TotalLoans
		{
			get
			{
				return this.totalLoans;
			}
			set
			{
				this.totalLoans = value;
			}
		}
		#endregion · TotalLoans ·
		#region · MaxOffer ·
		[Description("Max Offer")]
		public string MaxOffer
		{
			get
			{
				return this.maxOffer;
			}
			set
			{
				this.maxOffer = value;
			}
		}
		#endregion · MaxOffer ·

		#endregion � Automatically Generated Properties �

		#region INHibernate Members

		public void NhSave(NHibernate.ISession session)
		{
			session.Save(this);
		}

		public void NhSaveOrUpdate (NHibernate.ISession session)
		{
			try
			{
			session.SaveOrUpdate(this);
			}
			catch (Exception exc)
			{
				MessageBox.Show(exc.ToString());
			}
		}


		#endregion

		#region ISPL Members

		public void ExecuteSPL(SqlConnection dbConn)
		{
			try
			{
				SqlCommand sp_insert_LoanProperties = new SqlCommand("insert_LoanProperties", dbConn);
				sp_insert_LoanProperties.CommandType = CommandType.StoredProcedure;

				sp_insert_LoanProperties.Parameters.Add("@IdLoanProperty", this.IdLoanProperty);
				sp_insert_LoanProperties.Parameters.Add("@FirstLoanBalance", this.FirstLoanBalance);
				sp_insert_LoanProperties.Parameters.Add("@FirstLoanMonthly", this.FirstLoanMonthly);
				sp_insert_LoanProperties.Parameters.Add("@SecondLoanBalance", this.SecondLoanBalance);
				sp_insert_LoanProperties.Parameters.Add("@SecondLoanMonthly", this.SecondLoanMonthly);
				sp_insert_LoanProperties.Parameters.Add("@ThirdLoanBalance", this.ThirdLoanBalance);
				sp_insert_LoanProperties.Parameters.Add("@ThirdLoanMonthly", this.ThirdLoanMonthly);
				sp_insert_LoanProperties.Parameters.Add("@AmntOfOtherLiens", this.AmntOfOtherLiens);
				sp_insert_LoanProperties.Parameters.Add("@HOAPerMonth", this.HOAPerMonth);
				sp_insert_LoanProperties.Parameters.Add("@TotalLoans", this.TotalLoans);
				sp_insert_LoanProperties.Parameters.Add("@MaxOffer", this.MaxOffer);
				sp_insert_LoanProperties.ExecuteNonQuery();
			}
			catch (Exception exc)
			{
				MessageBox.Show(exc.ToString());
			}
		}

		#endregion
	}
	#endregion � Class LoanProperties �
}
#endregion � Namespace DealMaker �