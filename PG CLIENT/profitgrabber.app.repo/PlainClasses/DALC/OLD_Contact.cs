#region � Using �
using System;
using System.Data;
using System.Windows.Forms;
using System.Data.SqlClient;
#endregion � Using �
#region � Namespace DealMaker �
namespace DealMaker
{
	#region � Class Contact �
	/// <summary>
	/// Contact
	/// </summary>
	public class Contact : INHibernate, ISPL	
	{		
		#region � Data �
		public bool save = false;
		
		private Guid		idContact						= Guid.Empty;
		/// <summary>First Name</summary>				
		private string		firstName						= string.Empty;
		/// <summary>Last Name</summary>
		private string		lastName						= string.Empty;
		/// <summary>Middle Name</summary>
		private string		middleName						= string.Empty;
		/// <summary>Full Name</summary>
		private string		fullName						= string.Empty;		
		/// <summary>Go By - NickName for that contact - For Example, William may go by Bill</summary>				
		private string		goBy							= string.Empty;
		/// <summary>Salutation Name</summary>
		private string		salutationName					= string.Empty;
		
		
		/// <summary>Imported Address</summary>
		private Address		importedAddress					= null;
		/// <summary>Last Known Address</summary>
		//private Address		firstAddress					= null;
		/// <summary>First Mail Address</summary>
		private Address		mailAddress						= null;
		/// <summary>Previous Mail Address</summary>
		private Address		secondAddress					= null;
		/// <summary>Old Mail Address</summary>
		private Address		thirdAddress					= null;
		/// <summary>Site Address (where the property is located)</summary>
		private Address		siteAddress						= null;
		

		
		
		private Phone		homePhone						= null;
		/// <summary>Work Phone</summary>
		private Phone		workPhone						= null;
		/// <summary>Cell Phone</summary>
		private Phone		cellPhone						= null;
		/// <summary>Fax Number</summary>
		
		private string		faxNumber						= string.Empty;
		/// <summary>e-mail</summary>
		private string		eMail							= string.Empty;
		#endregion � Data �	
		#region � CTR �
		public Contact()
		{	
			this.idContact = Guid.NewGuid();
		}
		#endregion � CTR �
		#region � Properties �
		#region � IdContact �
		public Guid IdContact
		{
			get {return this.idContact;}
			set {this.idContact = value;}
		}
		#endregion � IdContact �
		#region � FirstName �
		/// <summary>
		/// First Name
		/// </summary>
		[Description("First Name")]
		public string FirstName
		{
			get {return this.firstName;}
			set {this.firstName = value;}
		}
		#endregion � FirstName �
		#region � LastName �
		/// <summary>
		/// Last Name
		/// </summary>
		[Description("Last Name")]
		public string LastName 
		{
			get {return this.lastName;}
			set {this.lastName = value;}
		}
		#endregion � LastName �
		#region � MiddleName �
		/// <summary>
		/// Middle Name
		/// </summary>
		[Description("Middle Name")]
		public string MiddleName
		{
			get {return this.middleName;}
			set {this.middleName = value;}
		}
		#endregion � MiddleName �
		#region � FullName �
		/// <summary>
		/// Full Name (First Name + Middle Name + Last Name)
		/// </summary>
		[Description("Full Name")]
		public string FullName
		{
			get 
			{
				if (string.Empty == this.fullName.Trim())
				{
					//TA++: 03.04.2005.:HACK for Return Mail Wizard request:
					//if user imports just first name - full name is still empty
					//if user manually saves full name that is concatenaded just from First name or middle name, but without last name - we accept it!
						if (string.Empty == this.LastName)
							return string.Empty;
					//TA--: 03.04.2005.:HACK for Return Mail Wizard request
					string fn = string.Empty;
					if (string.Empty != this.FirstName)
						fn += this.FirstName;
					if (string.Empty != this.MiddleName)
					{
						if (string.Empty != fn)
							fn += " ";
						fn += this.MiddleName;
					}
					if (string.Empty != this.LastName)
					{
						if (string.Empty != fn)
							fn += " ";
						fn += this.LastName;
					}
						return fn;
				}
				else
					return this.fullName;
			}
			set {this.fullName = value;}
		}
		#endregion � FullName �		
		#region � GoBy �
		/// <summary>		
		/// Go By - NickName for that contact - For Example, William may go by Bill
		/// </summary>				
		[Description("Go By")]
		public string GoBy
		{
			get {return this.goBy;}
			set {this.goBy = value;}
		}
		#endregion � GoBy �
		#region � SalutationName �
		/// <summary>
		/// Salutation Name (mr, miss, mrs, ...)
		/// </summary>
		[Description("Salutation Name")]
		public string SalutationName
		{
			get {return this.salutationName;}
			set {this.salutationName = value;}
		}
		#endregion � SalutationName �		
		#region � ImportedAddress �
		/// <summary>
		/// Imported Address
		/// </summary>
		public Address ImportedAddress
		{
			get {return this.importedAddress;}
			set {this.importedAddress = value;}
		}
		#endregion � ImportedAddress �
		#region � FirstAddress �
		/// <summary>
		/// Last Known Address
		/// </summary>
		//public Address FirstAddress
		//{
		//	get {return this.firstAddress;}
		//	set {this.firstAddress = value;}
		//}
		#endregion � FirstAddress �
		#region � SecondAddress �
		/// <summary>
		/// Previous Mail Address
		/// </summary>
		public Address SecondAddress
		{
			get {return this.secondAddress;}
			set {this.secondAddress = value;}
		}
		#endregion � SecondAddress �
		#region � ThirdAddress �
		/// <summary>
		/// Old Mail Address
		/// </summary>
		public Address ThirdAddress
		{
			get {return this.thirdAddress;}
			set {this.thirdAddress = value;}
		}
		#endregion � ThirdAddress �
		#region � SiteAddress �
		/// <summary>
		/// Site Address (where the property is located)
		/// </summary>
		public Address SiteAddress 
		{
			get {return this.siteAddress;}
			set {this.siteAddress = value;}
		}
		#endregion � SiteAddress �		
		#region � MailAddress �
		/// <summary>
		/// Mail Address (where the property is located)
		/// </summary>
		public Address MailAddress 
		{
			get {return this.mailAddress;}
			set {this.mailAddress = value;}
		}
		#endregion � MailAddress �		
		#region � HomePhone �
		/// <summary>
		/// Home Phone
		/// </summary>
		public Phone HomePhone
		{
			get {return this.homePhone;}
			set {this.homePhone = value;}
		}
		#endregion � HomePhone �
		#region � WorkPhone �
		/// <summary>
		/// Work Phone
		/// </summary>
		public Phone WorkPhone
		{
			get {return this.workPhone;}
			set {this.workPhone = value;}
		}
		#endregion � WorkPhone �
		#region � CellPhone �
		/// <summary>
		/// Cell Phone
		/// </summary>
		public Phone CellPhone
		{
			get {return this.cellPhone;}
			set {this.cellPhone = value;}
		}
		#endregion � CellPhone �
		#region � FaxNumber �
		/// <summary>
		/// Fax Number
		/// </summary>
		[Description("Fax Number")]
		public string FaxNumber
		{
			get {return this.faxNumber;}
			set {this.faxNumber = value;}
		}
		#endregion � FaxNumber �
		#region � EMail �
		/// <summary>
		/// e-mail
		/// </summary>
		[Description("eMail")]
		public string EMail
		{
			get {return this.eMail;}
			set {this.eMail = value;}
		}
		#endregion � EMail �		
		#endregion � Properties �

		#region INHibernate Members

		public void NhSave(NHibernate.ISession session)
		{
			if (null != this.ImportedAddress)
				this.ImportedAddress.NhSave(session);

			//if (null != this.FirstAddress)
			//	this.FirstAddress.NhSave(session);

			if (null != this.MailAddress)
				this.MailAddress.NhSave(session);

			if (null != this.SecondAddress)
				this.SecondAddress.NhSave(session);

			if (null != this.ThirdAddress)
				this.ThirdAddress.NhSave(session);

			if (null != this.SiteAddress)
				this.SiteAddress.NhSave(session);
			
			if (null != this.HomePhone)
				this.HomePhone.NhSave(session);

			if (null != this.WorkPhone)
				this.WorkPhone.NhSave(session);

			if (null != this.CellPhone)
				this.CellPhone.NhSave(session);			

			session.Save(this);
		}

		public void NhSaveOrUpdate (NHibernate.ISession session)
		{
			try
			{
				if (null != this.ImportedAddress)
					this.ImportedAddress.NhSaveOrUpdate(session);

				//if (null != this.FirstAddress)
				//	this.FirstAddress.NhSave(session);

				if (null != this.MailAddress)
					this.MailAddress.NhSaveOrUpdate(session);

				if (null != this.SecondAddress)
					this.SecondAddress.NhSaveOrUpdate(session);

				if (null != this.ThirdAddress)
					this.ThirdAddress.NhSaveOrUpdate(session);

				if (null != this.SiteAddress)
					this.SiteAddress.NhSaveOrUpdate(session);
				
				if (null != this.HomePhone)
					this.HomePhone.NhSaveOrUpdate(session);

				if (null != this.WorkPhone)
					this.WorkPhone.NhSaveOrUpdate(session);

				if (null != this.CellPhone)
					this.CellPhone.NhSaveOrUpdate(session);			

			
				if (true == this.save)							
					session.Save(this);					
				else
					session.SaveOrUpdate(this);
			}
			catch (Exception exc)
			{
				MessageBox.Show(exc.ToString());
			}
		}



		#endregion

		#region ISPL Members

		public void ExecuteSPL(System.Data.SqlClient.SqlConnection dbConn)
		{
			
			try
			{
				if (null != this.ImportedAddress)
					this.ImportedAddress.ExecuteSPL(dbConn);

				//if (null != this.FirstAddress)
				//	this.FirstAddress.NhSave(session);

				if (null != this.MailAddress)
					this.MailAddress.ExecuteSPL(dbConn);

				if (null != this.SecondAddress)
					this.SecondAddress.ExecuteSPL(dbConn);

				if (null != this.ThirdAddress)
					this.ThirdAddress.ExecuteSPL(dbConn);

				if (null != this.SiteAddress)
					this.SiteAddress.ExecuteSPL(dbConn);
			
				if (null != this.HomePhone)
					this.HomePhone.ExecuteSPL(dbConn);

				if (null != this.WorkPhone)
					this.WorkPhone.ExecuteSPL(dbConn);

				if (null != this.CellPhone)
					this.CellPhone.ExecuteSPL(dbConn);		

			

				SqlCommand sp_insert_Contact = new SqlCommand("insert_Contact", dbConn);
				sp_insert_Contact.CommandType = CommandType.StoredProcedure;

				sp_insert_Contact.Parameters.Add("@IdContact", this.IdContact);
				sp_insert_Contact.Parameters.Add("@FirstName", this.FirstName);
				sp_insert_Contact.Parameters.Add("@LastName", this.LastName);
				sp_insert_Contact.Parameters.Add("@MiddleName", this.MiddleName);
				sp_insert_Contact.Parameters.Add("@FullName", this.FullName);
				sp_insert_Contact.Parameters.Add("@GoBy", this.GoBy);
				sp_insert_Contact.Parameters.Add("@SalutationName", this.SalutationName);
				
				if (null != this.ImportedAddress)
					sp_insert_Contact.Parameters.Add("@ImportedAddress", this.ImportedAddress);
				else
					sp_insert_Contact.Parameters.Add("@ImportedAddress", DBNull.Value);
				
				sp_insert_Contact.Parameters.Add("@FirstAddress", DBNull.Value);
			
				if (null != this.SecondAddress)
					sp_insert_Contact.Parameters.Add("@SecondAddress", this.SecondAddress.IdAddress);
				else
					sp_insert_Contact.Parameters.Add("@SecondAddress", DBNull.Value);
			
				if (null != this.ThirdAddress)
					sp_insert_Contact.Parameters.Add("@ThirdAddress", this.ThirdAddress.IdAddress);
				else
					sp_insert_Contact.Parameters.Add("@ThirdAddress", DBNull.Value);

				if (null != this.SiteAddress)
					sp_insert_Contact.Parameters.Add("@SiteAddress", this.SiteAddress.IdAddress);
				else
					sp_insert_Contact.Parameters.Add("@SiteAddress", DBNull.Value);

				if (null != this.MailAddress)
					sp_insert_Contact.Parameters.Add("@MailAddress", this.MailAddress.IdAddress);
				else
					sp_insert_Contact.Parameters.Add("@MailAddress", DBNull.Value);

				if (null != this.HomePhone)
					sp_insert_Contact.Parameters.Add("@HomePhone", this.HomePhone.IdPhone);
				else
					sp_insert_Contact.Parameters.Add("@HomePhone", DBNull.Value);

				if (null != this.WorkPhone)
					sp_insert_Contact.Parameters.Add("@WorkPhone", this.WorkPhone.IdPhone);
				else
					sp_insert_Contact.Parameters.Add("@WorkPhone", DBNull.Value);

				if (null != this.CellPhone)
					sp_insert_Contact.Parameters.Add("@CellPhone", this.CellPhone.IdPhone);
				else
					sp_insert_Contact.Parameters.Add("@CellPhone", DBNull.Value);
			
			
				sp_insert_Contact.Parameters.Add("@faxNumber", this.faxNumber);
				sp_insert_Contact.Parameters.Add("@eMail", this.eMail);
				sp_insert_Contact.ExecuteNonQuery();
			}
			catch (Exception exc)
			{
				MessageBox.Show(exc.Message);
			}


		}

		#endregion
	}
	#endregion � Class Contact �
}
#endregion � Namespace DealMaker �