using System;

namespace DealMaker
{
	public class User : INHibernate
	{
		private Guid idUser;
		private string firstName;
		private string lastName;
		private string companyName;
		private string address;
		private string city;
		private string state;
		private string zip;
		private string phoneNr;
        public string DirectNumber { get; set; }
        private string fax;
        private string email;
        public string Email2 { get; set; }
        public string BuyingWebsite { get; set; }
        public string ForeclosureWebsite { get; set; }

        public User()
		{
			this.idUser = Guid.Empty;
		}

		public Guid IdUser
		{
			get { return idUser; }
			set { idUser = value; }
		}

		public string FirstName 
		{
			get { return firstName; }
			set { firstName = value; }
		}

		public string LastName 
		{
			get { return lastName; }
			set { lastName = value; }
		}

		public string CompanyName 
		{
			get { return companyName; }
			set { companyName = value; }
		}

		public string Address 
		{
			get { return address; }
			set { address = value; }
		}

		public string City
		{
			get {return this.city;}
			set {this.city = value;}
		}

		public string State
		{
			get {return this.state;}
			set {this.state = value;}
		}

		public string ZIP
		{
			get {return this.zip;}
			set {this.zip = value;}
		}

		public string PhoneNr 
		{
			get { return phoneNr; }
			set { phoneNr = value; }
		}
		
		public string Fax
		{
			get {return this.fax;}
			set {this.fax = value;}
		}

        public string Email
        {
            get { return this.email; }
            set { this.email = value; }
        }
		
		

		#region INHibernate Members

		public void NhSave(NHibernate.ISession session)
		{
			session.Save(this);
		}

		public void NhSaveOrUpdate (NHibernate.ISession session)
		{
			session.SaveOrUpdate(this);
		}
		#endregion
	}
}
