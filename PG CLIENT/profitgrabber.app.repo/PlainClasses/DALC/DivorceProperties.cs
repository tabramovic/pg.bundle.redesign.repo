#region � Using �
using System;
using System.Data.SqlClient;
using System.Windows.Forms;
using System.Data;
#endregion � Using �
#region � Namespace DealMaker �
namespace DealMaker
{
	#region � Class DivorceProperties �
	/// <summary>
	/// DivorceProperties
	/// </summary>
	public class DivorceProperties : INHibernate, ISPL
	{
		#region � Data �
		private Guid	idDivorceProperty = Guid.Empty;
		private string divorceStatus	= string.Empty;
		private DateTime divorceDate;
		#endregion � Data �

		#region � Automatically Generated Properties �
		#region � IdDivorceProperty �
		public Guid IdDivorceProperty
		{
			get {return this.idDivorceProperty;}
			set {this.idDivorceProperty = value;}
		}
		#endregion � IdDivorceProperty �
		#region · DivorceStatus ·
		[Description("Divorce Status")]
		public string DivorceStatus
		{
			get
			{
				return this.divorceStatus;
			}
			set
			{
				this.divorceStatus = value;
			}
		}
		#endregion · DivorceStatus ·
		#region · DivorceDate ·
		[Description("Divorce Date")]
		public DateTime DivorceDate
		{
			get
			{
				return this.divorceDate;
			}
			set
			{
				this.divorceDate = value;
			}
		}
		#endregion · DivorceDate ·
		#endregion � Automatically Generated Properties �

		#region � CTR �
		public DivorceProperties()
		{
			this.idDivorceProperty = Guid.NewGuid();
		}
		#endregion � CTR �

		#region INHibernate Members

		public void NhSave(NHibernate.ISession session)
		{
			session.Save(this);
		}
		public void NhSaveOrUpdate (NHibernate.ISession session)
		{
			try
			{
			session.SaveOrUpdate(this);
			}
			catch (Exception exc)
			{
				MessageBox.Show(exc.ToString());
			}
		}


		#endregion

		#region ISPL Members

		public void ExecuteSPL(SqlConnection dbConn)
		{
			
			try
			{
			SqlCommand sp_insert_DivorceProperties = new SqlCommand("insert_DivorceProperties", dbConn);
			sp_insert_DivorceProperties.CommandType = CommandType.StoredProcedure;

			sp_insert_DivorceProperties.Parameters.Add("@IdDivorceProperty", this.IdDivorceProperty);
			sp_insert_DivorceProperties.Parameters.Add("@DivorceStatus", this.DivorceStatus);
			if (this.DivorceDate != new DateTime(1, 1, 1))
				sp_insert_DivorceProperties.Parameters.Add("@DivorceDate", this.DivorceDate);
			else
				sp_insert_DivorceProperties.Parameters.Add("@DivorceDate", DBNull.Value);
			sp_insert_DivorceProperties.ExecuteNonQuery();
			}
			catch (Exception exc)
			{
				MessageBox.Show(exc.ToString());
			}
		}

		#endregion
	}
	#endregion � Class DivorceProperties �
}
#endregion � Namespace DealMaker �