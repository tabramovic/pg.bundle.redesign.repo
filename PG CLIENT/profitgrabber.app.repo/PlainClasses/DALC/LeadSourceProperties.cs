#region � Using �
using System;
using System.Data.SqlClient;
using System.Data;
using System.Windows.Forms;
#endregion � Using �
#region � Namespace DealMaker �
namespace DealMaker
{
	#region � Class LeadSourceProperties �
	/// <summary>
	/// Summary description for LeadSourceProperties.
	/// </summary>
	public class LeadSourceProperties : INHibernate, ISPL
	{
		#region � Data �
		private Guid   idLeadSourceProperty = Guid.Empty;
		private string leadSource	= string.Empty;
		private string dateEntered	= string.Empty;
		private string dateEdited	= string.Empty;
		#endregion � Data �
		#region � Automatically Generated Properties �
		#region � IdLeadSourceProperty �
		public Guid IdLeadSourceProperty
		{
			get {return this.idLeadSourceProperty; }
			set {this.idLeadSourceProperty = value; }
		}
		#endregion � IdLeadSourceProperty �
		#region � LeadSource �
		[Description("Lead Source")]
		public string LeadSource
		{
			get
			{
				return this.leadSource;
			}
			set
			{
				this.leadSource = value;
			}
		}
		#endregion � LeadSource �
		#region � DateEntered �
		[Description("Date Entered")]
		public string DateEntered
		{
			get
			{
				return this.dateEntered;
			}
			set
			{
				this.dateEntered = value;
			}
		}
		#endregion � DateEntered �
		#region � DateEdited �
		[Description("Date Edited")]
		public string DateEdited
		{
			get
			{
				return this.dateEdited;
			}
			set
			{
				this.dateEdited = value;
			}
		}
		#endregion � DateEdited �
		#endregion � Automatically Generated Properties �
		#region � CTR �
		public LeadSourceProperties()
		{
			this.idLeadSourceProperty = Guid.NewGuid();
		}
		#endregion � CTR �

		#region INHibernate Members

		public void NhSave(NHibernate.ISession session)
		{
			session.Save(this);
		}

		public void NhSaveOrUpdate (NHibernate.ISession session)
		{
			try
			{
			session.SaveOrUpdate(this);
			}
			catch (Exception exc)
			{
				MessageBox.Show(exc.ToString());
			}
		}


		#endregion

		#region ISPL Members

		public void ExecuteSPL(SqlConnection dbConn)
		{
			try
			{
			
				SqlCommand sp_insert_LeadSourceProperties = new SqlCommand("insert_LeadSourceProperties", dbConn);
				sp_insert_LeadSourceProperties.CommandType = CommandType.StoredProcedure;

				sp_insert_LeadSourceProperties.Parameters.Add("@IdLeadSourceProperty", this.IdLeadSourceProperty);
				sp_insert_LeadSourceProperties.Parameters.Add("@LeadSource", this.LeadSource);
				sp_insert_LeadSourceProperties.Parameters.Add("@DateEntered", this.DateEntered);
				sp_insert_LeadSourceProperties.Parameters.Add("@DateEdited", this.DateEdited);
				sp_insert_LeadSourceProperties.ExecuteNonQuery();
			}
			catch (Exception exc)
			{
				MessageBox.Show(exc.ToString());
			}
		}

		#endregion
	}
	#endregion � Class LeadSourceProperties �
}
#endregion � Namespace DealMaker �