#region � Using �
using System;
using System.Collections;
using System.Data.SqlClient;
using System.Data;
using System.Windows.Forms;
using DealMaker.PlainClasses;
using System.Reflection;
using DataAppendService.Lib.Models.IDI;
using Newtonsoft.Json;
using NHibernate;
using NHibernate.Cfg;
#endregion � Using �
#region � Namespace DealMaker �
namespace DealMaker
{
	#region � Class PropertyItem �
	/// <summary>
	/// Class PropertyItem - describes property on sale !
	/// </summary>
	public class PropertyItem : INHibernate, ISPL, IComparable
	{
		#region � Data �
		public bool save = false;
		
		private		Guid		idPropertyItem	= Guid.Empty;
		private		Contact		owner			= null;
		private		DealInfo	dealInfo		= null;
		/// <summary>Listing Agent</summary>
		private		ListingAgent listingAgent	= null;
		/// <summary>Deal Property</summary>
		private     DealProperty dealProperty  = null;
		/// <summary>Create Date</summary>
		private DateTime	dateCreated			= DateTime.Now;
		/// <summary>Last Modify Date</summary>
		private DateTime	dateLastModified	= DateTime.Now;
		// ihuk, 04.12.2004
		// Hashtable that will hold PropertyItem attributes.
		// For more information on what 'attributes' are look at
		// PropertyItemAttribute class.
		private Hashtable attributes = null;
		#endregion � Data �		
		#region � CTR �
		public PropertyItem()
		{
			this.idPropertyItem = Guid.NewGuid();
		}
		#endregion � CTR �

		
		public static PropertyItem LoadPropertyItem (Guid propertyItemId)
		{
			ISession session = null;
			ITransaction transaction = null;
			PropertyItem pi = null;
			try
			{
			
				Connection conn = Connection.getInstance();     
				SqlConnection sqlConn = conn.HatchConn;
				IDbConnection iSqlConn = sqlConn as IDbConnection;

				Configuration cfg = Globals.cfg;				
				ISessionFactory factory = Globals.factory;
				
				
				if (null != iSqlConn) 
				{
					session = factory.OpenSession(iSqlConn);
					if (ConnectionState.Closed == iSqlConn.State) 
					{
						iSqlConn.Open();
					}
				}
				else 
				{
					session = factory.OpenSession();
				}

				transaction = session.BeginTransaction();
			
				//User joeCool = (User)session.Load(typeof(User), "joe_coolYYY");
				pi = (PropertyItem)session.Load(typeof(PropertyItem), propertyItemId);				

				transaction.Commit();
				

			}
			catch (Exception exc)
			{
				MessageBox.Show(exc.ToString());
				transaction.Rollback();
			}				
			finally
			{
				
				session.Close();
			}
			return pi;
		}

		/// ihuk, 04.12.2004
		/// <summary>
		/// Enymerates properties of an object.
		/// </summary>
		/// <param name="o">object to enumerate</param>
		private void EnumerateAttributes(object o)
		{
			Type type;
			PropertyInfo[] properties;

			if (null  == o)
				return;

			type = o.GetType();
			properties = type.GetProperties();

			foreach (PropertyInfo property in properties)
			{
				DescriptionAttribute[] a;
				
				try
				{				
					// ihuk, 06.03.2005
					// The original idea was to go trough all properties
					// of PropertyItem class looking for DescriptionAttributes.
					// If property was primitive and had a valid description attrinute
					// we would add it to attributes list. If property as a class
					// (PropertyType.IsClass == true) we would recurse to that property
					// by calling EnumerateAttributes. While this is conceptually 
					// correct it does not give the desired result. This is due to
					// the fact that some properties that have valid correct description
					// attribute are in fact classes. String properties are a good
					// example of this. 
					// This is the new plan:
					// We go trought all properties of PropertyItem class looking for
					// properties with description attribute. If we find property with
					// description we add it to the list without tesing it's type.
					// If we don't find description attribute and property is as class
					// (PropertyType.IsClass == true) we give it another chance and
					// call EnumerateAttributes on it.
					// This plain also fails because subclasses also have description
					// attributes. Back to square one. 
					// If it has description and it is a class and it is not 
					// string we enumerate it, otherwise we add it to the list.
					// Design errors are realy not my problem

					// get property attributes.
					a = property.GetCustomAttributes(
						typeof(DescriptionAttribute), true) as DescriptionAttribute[];



					//hm? maybe that's ok?
					//23.09.05, sasa: MAJOR BUG found and commented, if some property is class
					//and has no description attribute, but has properties with desctiption, in previous
					//was IGNORED !!!! 

					//				a = new DescriptionAttribute("");

					// check if property has description attribute
									if (null == a || 0 == a.Length)
									{
										continue;
									}
									else
									{
										if ((property.PropertyType.IsClass &&
											property.PropertyType != typeof(string)
											)
											)
										{
											//sasa 24.08.2005:
											//bug fix, this traverse only trough object that has property value that is not always the case
											//in this version if required property is null, we create dummy supplement
						
											//EnumerateAttributes(property.GetValue(o, null));
											//EnumerateAttributes(Activator.CreateInstance(property.PropertyType));
											object obj;
											
											//if(o is Array) return;

											try
											{
												obj = property.GetValue(o, null);
											}
											catch (Exception 
#if DEBUG
												exc
#endif
												)
											{
#if DEBUG
												MessageBox.Show(exc.ToString());
#endif

												//some properties cannot be read
												continue;
											}

											if(obj == null)
											{
												EnumerateAttributes(Activator.CreateInstance(property.PropertyType));
											}
											else
											{
												EnumerateAttributes(obj);
											}
										}
										else //if (null != a && a.Length > 0) //sasa added if 23.9
										{
											string description = string.Empty;

											// prperty has description attribute, add it to the list
											// ihuk, 05.12.2004
											// HACK: I have noticed that some Description attributes have 
											// leading or trailing spaces so I do Trim on them. 
											description = a[0].Desc.Trim();

											if (attributes[description] == null)
											{
												attributes.Add(description, 
													new PropertyItemAttribute(property.Name, o, description, property));
											}
											else
												throw new System.Exception(String.Format("Attribute description '{0}' for property '{0} already exists",
													description, property.Name));	
										}
									}				
				}
				catch(Exception 
#if DEBUG
					ee
#endif
					)
				{
#if DEBUG
					MessageBox.Show(ee.ToString());
#endif
//					throw ee;
				}
			}
		}

/*
		//sasa 22.09.05: aaaaa, checks for attributes only once, let's add property NextRebuild

		bool nextRebuildProperties = false;
		public bool NextRebuildProperties
		{
			set
			{
				nextRebuildProperties = true;
			}
		}
*/
		
		//sasa, 10.04.06: ugliest hack ever
		bool reload_attributes = false;

		//sasa: 10.04.05, empty first line bug fix
		private void AddCustomAttributes()
		{
			//TA++:01.04.2005
			//hack (again) - we manually add properties which will need for Return Mail Wizard 

			//not needed afterall //?
			//// ???
			////sasa 22.09.05: shit, if object doesn't contain owner doesn't work
			////let's fix it
			//if(this.Owner == null) this.Owner = (Contact)Activator.CreateInstance(typeof(Contact));
			//if(this.Owner.SiteAddress == null) this.Owner.SiteAddress = (Address)Activator.CreateInstance(typeof(Address));

			if (null != this.Owner && null != this.Owner.SiteAddress)
			{
				if (false == attributes.Contains("Site Street Address"))
					attributes["Site Street Address"] = new PropertyItemAttribute(this.Owner.SiteAddress.FullSiteStreetAddress, this, string.Empty, null);
				if (false == attributes.Contains("Site Street Number"))
					attributes["Site Street Number"] = new PropertyItemAttribute(this.Owner.SiteAddress.SiteStreetNumber, this, string.Empty, null);

				if (false == attributes.Contains("Site Steet PreDirectional"))
					attributes["Site Street PreDirectional"] = new PropertyItemAttribute(this.Owner.SiteAddress.SiteStreetPreDirectional, this, string.Empty, null);						

				if (false == attributes.Contains("Site Steet Name"))
					attributes["Site Street Name"] = new PropertyItemAttribute(this.Owner.SiteAddress.SiteStreetName, this, string.Empty, null);						
						
				if (false == attributes.Contains("Site Street Unit Number"))
					attributes["Site Street Unit Number"] = new PropertyItemAttribute(this.Owner.SiteAddress.SiteStreetUnitNumber, this, string.Empty, null);						

				if (false == attributes.Contains("Site Street Suffix"))
					attributes["Site Street Suffix"] = new PropertyItemAttribute(this.Owner.SiteAddress.SiteStreetSuffix, this, string.Empty, null);						

				if (false == attributes.Contains("Site Steet PostDirectional"))
					attributes["Site Street PostDirectional"] = new PropertyItemAttribute(this.Owner.SiteAddress.SiteStreetPostDirectional, this, string.Empty, null);						

				if (false == attributes.Contains("Site City"))
					attributes["Site City"] = new PropertyItemAttribute(this.Owner.SiteAddress.SiteCity, this, string.Empty, null);
				if (false == attributes.Contains("Site State"))
					attributes["Site State"] = new PropertyItemAttribute(this.Owner.SiteAddress.SiteState, this, string.Empty, null);
				if (false == attributes.Contains("Site City, State"))
					attributes["Site City, State"] = new PropertyItemAttribute(this.Owner.SiteAddress.SiteCitySiteState, this, string.Empty, null);
				if (false == attributes.Contains("Site ZIP"))
					attributes["Site ZIP"] = new PropertyItemAttribute(this.Owner.SiteAddress.SiteZIP, this, string.Empty, null);
			}
			if (null != this.Owner && null != this.Owner.MailAddress)
			{
						
				attributes["Last Known Mailing Street Address"] = new PropertyItemAttribute(this.Owner.MailAddress.FullSiteStreetAddress, this, string.Empty, null);
				attributes["Last Known Mailing City, State"] = new PropertyItemAttribute(this.Owner.MailAddress.SiteCitySiteState, this, string.Empty, null);
				attributes["Last Known Mailing City"] = new PropertyItemAttribute(this.Owner.MailAddress.SiteCity, this, string.Empty, null);
				attributes["Last Known Mailing State"] = new PropertyItemAttribute(this.Owner.MailAddress.SiteState, this, string.Empty, null);
				attributes["Last Known Mailing ZIP"] = new PropertyItemAttribute(this.Owner.MailAddress.SiteZIP, this, string.Empty, null);

			}

			if (null != this.Owner && null != this.Owner.SecondAddress)
			{
				attributes["Previously Known Mailing Street Address"] = new PropertyItemAttribute(this.Owner.SecondAddress.FullSiteStreetAddress, this, string.Empty, null);
				attributes["Previously Known Mailing City, State"] = new PropertyItemAttribute(this.Owner.SecondAddress.SiteCitySiteState, this, string.Empty, null);	
				attributes["Previously Known Mailing City"] = new PropertyItemAttribute(this.Owner.SecondAddress.SiteCity, this, string.Empty, null);
				attributes["Previously Known Mailing State"] = new PropertyItemAttribute(this.Owner.SecondAddress.SiteState, this, string.Empty, null);
				attributes["Previously Known Mailing ZIP"] = new PropertyItemAttribute(this.Owner.SecondAddress.SiteZIP, this, string.Empty, null);
			}

            if (null != this.Owner && !string.IsNullOrEmpty(Owner.ST_P1_RESPONSE))
            {
                SkiptraceClientResultset skipTraceData;

				try
                {
                    skipTraceData = JsonConvert.DeserializeObject<DataAppendService.Lib.Models.IDI.SkiptraceClientResultset>(Owner.ST_P1_RESPONSE);
                }
                catch
                {
					//log inappropriate JSON format (exception in deserializing) + stack trace into log
                    skipTraceData = null;
                }

				if (null != skipTraceData)
                {
                    if (null != skipTraceData.Emails)
                    {
						for (var i = 0; i < skipTraceData.Emails.Count; i++)
                            attributes[$"Skiptraced Email Rank {i}"] = new PropertyItemAttribute(skipTraceData.Emails[i], this, string.Empty, null);
                    }
                    if (null != skipTraceData.MobilePhones)
                    {
                        for (var i = 0; i < skipTraceData.MobilePhones.Count; i++)
                            attributes[$"Skiptraced Mobile Rank {i}"] = new PropertyItemAttribute(skipTraceData.MobilePhones[i], this, string.Empty, null);
                    }
                    if (null != skipTraceData.ResidentialPhones)
                    {
                        for (var i = 0; i < skipTraceData.ResidentialPhones.Count; i++)
                            attributes[$"Skiptraced Residential Phone Rank {i}"] = new PropertyItemAttribute(skipTraceData.ResidentialPhones[i], this, string.Empty, null);
                    }
                }
            }
					
			if (false == attributes.Contains("First Entered "))
				attributes["First Entered "] = new PropertyItemAttribute(this.DateCreated.ToShortDateString(), this, string.Empty, null);
					
			string groups = string.Empty;
			ArrayList alGroupMembership = ApplicationHelper.GetGroupMembershipData(this.idPropertyItem);
			for (int i = 0; i < alGroupMembership.Count; i++)
			{
				if (string.Empty != groups)
					groups += " ";
				groups += (string)alGroupMembership[i];
			}
					
			if (false == attributes.Contains("Group"))
				attributes["Group"] = new PropertyItemAttribute(groups, this, string.Empty, null);

			if (false == attributes.Contains("Status Group"))
				attributes["Status Group"] = new PropertyItemAttribute(ApplicationHelper.GetStatusGroupMembership(this.idPropertyItem), this, string.Empty, null);
			//TA--:01.04.2005

		}
		/// ihuk, 04.12.2004
		///<summary>
		/// Returns PropertyItem attribute with <c>attributeName</c>
		///</summary>
		public PropertyItemAttribute this [string attributeName]
		{
			get
			{
				// 04.12.2004, ihuk
				// Getting all PropertyItem properties takes time so
				// we do it only when we need it
				//				if (null == attributes || nextRebuildProperties)
				
				if (null == attributes || reload_attributes)
				{
					reload_attributes = false;
					//nextRebuildProperties = false;

					attributes = new Hashtable();
					EnumerateAttributes(this);
					
					
					AddCustomAttributes();
					
				}

				return attributes[attributeName] as PropertyItemAttribute;
			}
		}
		
		/// ihuk, 05.01.2005
		/// <summary>
		/// Returns array of PropertyItem attributes
		/// </summary>
		public PropertyItemAttribute[] PropertyItemAttributes
		{
			get
			{
				IEnumerator ie;
				ArrayList al = new ArrayList();

				// 05.12.2004, ihuk
				// Fetch only when needed
				if (null == attributes)
				{
					attributes = new Hashtable();
					EnumerateAttributes(this);

					//sasa, 10.04.06: ugliest hack ever
					reload_attributes = true;

					//this breaks createprofilepage...
					//AddCustomAttributes();
				}

				ie = attributes.GetEnumerator();
				while (ie.MoveNext())
					al.Add(((DictionaryEntry)ie.Current).Value as PropertyItemAttribute);

				return (PropertyItemAttribute[])al.ToArray(typeof(PropertyItemAttribute));
			}
		}

		#region SavePropertyItem 
		public static bool SavePropertyItem(PropertyItem pi)
		{
			bool bRes = true;
			ISession session = null;
			ITransaction transaction = null;
			
			try
			{
			
				Connection conn = Connection.getInstance();     
				SqlConnection sqlConn = conn.HatchConn;
				IDbConnection iSqlConn = sqlConn as IDbConnection;
				
				Configuration cfg = Globals.cfg;
				ISessionFactory factory = Globals.factory;
				
				
				if (null != iSqlConn) 
				{
					session = factory.OpenSession(iSqlConn);
					if (ConnectionState.Closed == iSqlConn.State) 
					{
						iSqlConn.Open();
					}
				}
				else 
				{
					session = factory.OpenSession();
				}

				transaction = session.BeginTransaction();
			
				pi.NhSaveOrUpdate(session);				

				transaction.Commit();				
			}
			catch (Exception 
#if DEBUG
				exc
#endif
				)
			{
#if DEBUG
				MessageBox.Show(exc.ToString());
#endif
				transaction.Rollback();
				bRes = false;
			}				
			finally
			{				
				session.Close();
			}
			return bRes;
		}
		#endregion SavePropertyItem 

		#region � Automatically Generated Properties �
		#region � IdPropertyItem �
		[Description("PropertyItem Id")]
		public Guid IdPropertyItem
		{
			get {return this.idPropertyItem;}
			set {this.idPropertyItem = value;}
		}
		#endregion � IdPropertyItem �

		#region � BarcodeId �
		//sasa, 24.08.2005: barcode id added
		[Description("Barcode Id")]
		public string BarcodeId
		{
			get
			{
				return "*" + this.idPropertyItem.ToString().Substring(0,6) + "*";
			}
			set {}
		}
		#endregion � IdPropertyItem �

		#region � Listing Agent �
		public ListingAgent ListingAgent
		{
			get {return this.listingAgent;}
			set {this.listingAgent = value;}
		}
		#endregion � Listing Agent �

		#region · Owner ·
		[Description("Owner")]
		public Contact Owner
		{
			get
			{
				return this.owner;
			}
			set
			{
				this.owner = value;
			}
		}
		#endregion · Owner ·
		#region · DealInfo ·
		[Description("Deal Info")]
		public DealInfo DealInfo
		{
			get
			{
				return this.dealInfo;
			}
			set
			{
				this.dealInfo = value;
			}
		}
		#endregion · DealInfo ·
		#region DealProperty
		[Description("Deal Property")]
		public DealProperty DealProperty
		{
			get {return this.dealProperty;}
			set {this.dealProperty = value;}
		}
		#endregion DealProperty
		#region � DateCreated �
		/// <summary>
		/// Create Date
		/// </summary>
		[Description("Date Created")]
		public DateTime DateCreated 
		{
			get {return this.dateCreated;}
			set {this.dateCreated = value;}
		}
		#endregion � DateCreated �
		#region � DateLastModified �
		/// <summary>
		/// Last Modify Date
		/// </summary>
		[Description("Date Last Modified")]
		public DateTime DateLastModified
		{
			get {return this.dateLastModified;}
			set {this.dateLastModified = value;}
		}
		#endregion � DateLastModified �
		#endregion � Automatically Generated Properties �

		#region INHibernate Members

		public void NhSave(NHibernate.ISession session)
		{
			if (null != this.Owner)
			{
				this.Owner.NhSave(session);
			}
			
			if (null != this.ListingAgent)
				this.ListingAgent.NhSave(session);

			if (null != this.DealInfo)
				this.DealInfo.NhSave(session);

			session.Save(this);
		}

		public void NhSaveOrUpdate (NHibernate.ISession session)
		{
			
			try
			{
				if (null != this.Owner)
				{
					this.Owner.NhSaveOrUpdate(session);
				}
			
				if (null != this.ListingAgent)
					this.ListingAgent.NhSaveOrUpdate(session);

				if (null != this.DealInfo)
					this.DealInfo.NhSaveOrUpdate(session);

				if (null != this.DealProperty)
					this.DealProperty.NhSaveOrUpdate(session);

				if (true == this.save)							
					session.Save(this);					
				else
					session.SaveOrUpdate(this);
			}
			catch (Exception exc)
			{
				MessageBox.Show(exc.ToString());
			}
		}

 
		#endregion

		#region ISPL Members

		public void ExecuteSPL(SqlConnection dbConn)
		{	
			try
			{
				if (null != this.Owner)
					this.Owner.ExecuteSPL(dbConn);
			
				if (null != this.ListingAgent)
					this.ListingAgent.ExecuteSPL(dbConn);

				if (null != this.DealInfo)
					this.DealInfo.ExecuteSPL(dbConn);

				if (null != this.DealProperty)
				{
					this.DealProperty.ExecuteSPL(dbConn);
				}


				SqlCommand sp_insert_PropertyItem = new SqlCommand("insert_PropertyItem", dbConn);
				sp_insert_PropertyItem.CommandType = CommandType.StoredProcedure;

				sp_insert_PropertyItem.Parameters.Add("@IdPropertyItem", this.IdPropertyItem);
			
				if (null != Owner)
					sp_insert_PropertyItem.Parameters.Add("@Owner", this.Owner.IdContact);
				else
					sp_insert_PropertyItem.Parameters.Add("@Owner", DBNull.Value);

				if (null != this.DealInfo)
					sp_insert_PropertyItem.Parameters.Add("@DealInfo", this.DealInfo.IdDealInfo);
				else
					sp_insert_PropertyItem.Parameters.Add("@DealInfo", DBNull.Value);

				if (null != this.DealProperty)
					sp_insert_PropertyItem.Parameters.Add("@DealProperty", this.DealProperty.IdDealProperty);
				else
					sp_insert_PropertyItem.Parameters.Add("@DealProperty", DBNull.Value);

				if (null != this.ListingAgent)
					sp_insert_PropertyItem.Parameters.Add("@ListingAgent", this.ListingAgent.IdListingAgent);
				else
					sp_insert_PropertyItem.Parameters.Add("@ListingAgent", DBNull.Value);

				sp_insert_PropertyItem.Parameters.Add("@DateCreated", this.DateCreated);
				sp_insert_PropertyItem.Parameters.Add("@DateLastModified", this.DateLastModified);
				sp_insert_PropertyItem.ExecuteNonQuery();
			}
			catch (Exception 
#if DEBUG
				exc
#endif
				)
			{
#if DEBUG
				MessageBox.Show(exc.ToString());
#endif
			}
		}

		#endregion

		public int CompareTo(object obj)
		{						
			return this.idPropertyItem.CompareTo(((PropertyItem)obj).idPropertyItem);
		}
	}
	#endregion � Class PropertyItem �
}
#endregion � Namespace DealMaker �
