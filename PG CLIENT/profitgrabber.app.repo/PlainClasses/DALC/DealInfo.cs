#region � Using �
using System;
using System.Data;
using System.Windows.Forms;
using System.Data.SqlClient;
#endregion � Using �

#region � Namespace DealMaker �
namespace DealMaker
{
	/// <summary>
	/// Summary description for DealInfo.
	/// </summary>
	public class DealInfo : INHibernate, ISPL
	{
		#region � Data �
		public bool save = false;
		
		private Guid					idDealInfo			= Guid.Empty;
		private FinancialProperties		financialProperty	= null;
		private TaxProperties			taxProperty			= null;
		private ForeClosureProperties	foreClosureProperty = null;
		private HouseProperties			houseProperty		= null;
		private LoanProperties			loanProperty		= null;
		private DistrictProperties		districtProperty	= null;
		private LeadSourceProperties	leadSourceProperty	= null;
		private DivorceProperties		divorceProperty		= null;
		private OtherProperties			otherProperty		= null;
		#endregion � Data �

		#region � Automatically Generated Properties �
		#region � IdDealInfo �			
		public Guid IdDealInfo
		{
			get {return this.idDealInfo;}
			set {this.idDealInfo = value;}
		}
		#endregion � IdDealInfo �
		#region · FinancialProperty ·
		[Description("Financial Property")]
		public FinancialProperties FinancialProperty
		{
			get
			{
				return this.financialProperty;
			}
			set
			{
				this.financialProperty = value;
			}
		}
		#endregion · FinancialProperty ·
		#region · TaxProperty ·
		[Description("Tax Property")]
		public TaxProperties TaxProperty
		{
			get
			{
				return this.taxProperty;
			}
			set
			{
				this.taxProperty = value;
			}
		}
		#endregion · TaxProperty ·
		#region · ForeClosureProperty ·
		[Description("Fore Closure Property")]
		public ForeClosureProperties ForeClosureProperty
		{
			get
			{
				return this.foreClosureProperty;
			}
			set
			{
				this.foreClosureProperty = value;
			}
		}
		#endregion · ForeClosureProperty ·
		#region · HouseProperty ·
		[Description("House Property")]
		public HouseProperties HouseProperty
		{
			get
			{
				return this.houseProperty;
			}
			set
			{
				this.houseProperty = value;
			}
		}
		#endregion · HouseProperty ·
		#region · LoanProperty ·
		[Description("Loan Property")]
		public LoanProperties LoanProperty
		{
			get
			{
				return this.loanProperty;
			}
			set
			{
				this.loanProperty = value;
			}
		}
		#endregion · LoanProperty ·
		#region · DistrictProperty ·
		[Description("District Property")]
		public DistrictProperties DistrictProperty
		{
			get
			{
				return this.districtProperty;
			}
			set
			{
				this.districtProperty = value;
			}
		}
		#endregion · DistrictProperty ·
		#region · LeadSourceProperty ·
		[Description("Lead Source Property")]
		public LeadSourceProperties LeadSourceProperty
		{
			get
			{
				return this.leadSourceProperty;
			}
			set
			{
				this.leadSourceProperty = value;
			}
		}
		#endregion · LeadSourceProperty ·
		#region · DivorceProperty ·
		[Description("Divorce Property")]
		public DivorceProperties DivorceProperty
		{
			get
			{
				return this.divorceProperty;
			}
			set
			{
				this.divorceProperty = value;
			}
		}
		#endregion · DivorceProperty ·
		#region · OtherProperty ·
		[Description("Other Property")]
		public OtherProperties OtherProperty
		{
			get
			{
				return this.otherProperty;
			}
			set
			{
				this.otherProperty = value;
			}
		}
		#endregion · OtherProperty ·
		#endregion � Automatically Generated Properties �
		
		public DealInfo()
		{
			this.idDealInfo = Guid.NewGuid();
		}
		#region INHibernate Members

		public void NhSave(NHibernate.ISession session)
		{
			try
			{
			if (null != FinancialProperty)
				FinancialProperty.NhSave(session);

			if (null != TaxProperty)
				TaxProperty.NhSave(session);

			if (null != ForeClosureProperty)
				ForeClosureProperty.NhSave(session);

			if (null != HouseProperty)
				HouseProperty.NhSave(session);

			if (null != LoanProperty)
				LoanProperty.NhSave(session);

			if (null != DistrictProperty)
				DistrictProperty.NhSave(session);

			if (null != LeadSourceProperty)
				LeadSourceProperty.NhSave(session);

			if (null != DivorceProperty)
				DivorceProperty.NhSave(session);

			if (null != OtherProperty)
				OtherProperty.NhSave(session);

			session.Save(this);
			}
			catch (Exception exc)
			{
				MessageBox.Show(exc.ToString());
			}
		}

		public void NhSaveOrUpdate (NHibernate.ISession session)
		{
			if (null != FinancialProperty)
				FinancialProperty.NhSaveOrUpdate(session);

			if (null != TaxProperty)
				TaxProperty.NhSaveOrUpdate(session);

			if (null != ForeClosureProperty)
				ForeClosureProperty.NhSaveOrUpdate(session);

			if (null != HouseProperty)
				HouseProperty.NhSaveOrUpdate(session);

			if (null != LoanProperty)
				LoanProperty.NhSaveOrUpdate(session);

			if (null != DistrictProperty)
				DistrictProperty.NhSaveOrUpdate(session);

			if (null != LeadSourceProperty)
				LeadSourceProperty.NhSaveOrUpdate(session);

			if (null != DivorceProperty)
				DivorceProperty.NhSaveOrUpdate(session);

			if (null != OtherProperty)
				OtherProperty.NhSaveOrUpdate(session);

			if (true == this.save)							
				session.Save(this);					
			else
				session.SaveOrUpdate(this);
		}

		#endregion

		#region ISPL Members

		public void ExecuteSPL(System.Data.SqlClient.SqlConnection dbConn)
		{
			
			try
			{
				SqlCommand sp_insert_DealInfo = new SqlCommand("insert_DealInfo", dbConn);
				sp_insert_DealInfo.CommandType = CommandType.StoredProcedure;

				sp_insert_DealInfo.Parameters.Add("@IdDealInfo", this.IdDealInfo);

				if (null != this.FinancialProperty)
				{
					sp_insert_DealInfo.Parameters.Add("@FinancialProperty", this.FinancialProperty.IdFinancialProperty);
					this.FinancialProperty.ExecuteSPL(dbConn);
				}
				else
					sp_insert_DealInfo.Parameters.Add("@FinancialProperty", DBNull.Value);

				if (null != this.TaxProperty)
				{
					this.TaxProperty.ExecuteSPL(dbConn);
					sp_insert_DealInfo.Parameters.Add("@TaxProperty", this.TaxProperty.IdTaxProperty);
				}
				else
					sp_insert_DealInfo.Parameters.Add("@TaxProperty", DBNull.Value);

				if (null != this.ForeClosureProperty)
				{
					this.ForeClosureProperty.ExecuteSPL(dbConn);
					sp_insert_DealInfo.Parameters.Add("@ForeClosureProperty", this.ForeClosureProperty.IdForeClosureProperty);
				}
				else
					sp_insert_DealInfo.Parameters.Add("@ForeClosureProperty", DBNull.Value);

				if (null != this.HouseProperty)
				{
					this.HouseProperty.ExecuteSPL(dbConn);
					sp_insert_DealInfo.Parameters.Add("@HouseProperty", this.HouseProperty.IdHouseProperty);
				}
				else
					sp_insert_DealInfo.Parameters.Add("@HouseProperty", DBNull.Value);

				if (null != this.LoanProperty)
				{
					this.LoanProperty.ExecuteSPL(dbConn);
					sp_insert_DealInfo.Parameters.Add("@LoanProperty", this.LoanProperty.IdLoanProperty);
				}
				else
					sp_insert_DealInfo.Parameters.Add("@LoanProperty", DBNull.Value);
				
				if (null != this.DistrictProperty)
				{
					this.DistrictProperty.ExecuteSPL(dbConn);
					sp_insert_DealInfo.Parameters.Add("@DistrictProperty", this.DistrictProperty.IdDistrictProperty);
				}
				else
					sp_insert_DealInfo.Parameters.Add("@DistrictProperty", DBNull.Value);

				if (null != this.LeadSourceProperty)
				{
					this.LeadSourceProperty.ExecuteSPL(dbConn);
					sp_insert_DealInfo.Parameters.Add("@LeadSourceProperty", this.LeadSourceProperty.IdLeadSourceProperty);
				}
				else
					sp_insert_DealInfo.Parameters.Add("@LeadSourceProperty", DBNull.Value);

				if (null != this.DivorceProperty)
				{
					this.DivorceProperty.ExecuteSPL(dbConn);
					sp_insert_DealInfo.Parameters.Add("@DivorceProperty", this.DivorceProperty.IdDivorceProperty);
				}
				else
					sp_insert_DealInfo.Parameters.Add("@DivorceProperty", DBNull.Value);

				if (null != this.OtherProperty)
				{
					this.OtherProperty.ExecuteSPL(dbConn);
					sp_insert_DealInfo.Parameters.Add("@OtherProperty", this.OtherProperty.IdOtherProperty);
				}
				else
					sp_insert_DealInfo.Parameters.Add("@OtherProperty", DBNull.Value);

				sp_insert_DealInfo.ExecuteNonQuery();
			}
			catch (Exception exc)
			{
				MessageBox.Show(exc.Message);
			}

		}

		#endregion
	}
}
#endregion � Namespace DealMaker �