#region � Using �
using System;
#endregion � Using �
#region � Namespace DealMaker �
namespace DealMaker
{
	#region � Class NodePropertyItems �
	/// <summary>
	/// Class NodePropertyInfos - tells us shich propertyInfos belong to which group
	/// </summary>
	public class NodePropertyItems
	{
		#region � Data �
		private int nodeId = 0;
		private Guid idPropertyItem = Guid.Empty;
		#endregion � Data �
		#region � Properties �
		#region � NodeId �
		public int NodeId
		{
			get { return this.nodeId; }
			set { this.nodeId = value; }
		}
		#endregion � NodeId �
		#region � IdPropertyItem �
		public Guid IdPropertyItem
		{
			get {return this.idPropertyItem;}
			set {this.idPropertyItem = value;}
		}
		#endregion � IdPropertyItem �
		#endregion � Properties �
		#region � CTR �
		public NodePropertyItems()
		{
		}
		public NodePropertyItems(int nodeId, Guid idPropertyItem)
		{
			this.nodeId = nodeId;
			this.idPropertyItem = idPropertyItem;
		}
		#endregion � CTR �
	}

	#endregion � Class NodePropertyItems �
}
#endregion � Namespace DealMaker �