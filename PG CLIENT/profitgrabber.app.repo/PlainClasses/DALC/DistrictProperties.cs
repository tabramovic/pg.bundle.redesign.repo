#region � Using �
using System;
using System.Windows.Forms;
using System.Data;
using System.Data.SqlClient;


#endregion � Using �
#region � Namespace DealMaker �
namespace DealMaker
{
	#region � Class DistrictProperties �
	/// <summary>
	/// DistrictProperties
	/// </summary>
	public class DistrictProperties : INHibernate, ISPL
	{
		#region � Data �
		public bool save = false;
		private Guid	idDistrictProperty	=	Guid.Empty;
		private	string	subdivision			=	string.Empty;
		private	string	legalDescription	=	string.Empty;
		private	string	vesting				=	string.Empty;
		private	string	mapGrid				=	string.Empty;
		private	string	mapArea				=	string.Empty;
		private	string	cityCode			=	string.Empty;
		private	string	schoolDistrict		=	string.Empty;
		#endregion � Data �	
		#region � Automatically Generated Properties �
		#region � IdDistrictProperty �
		public Guid IdDistrictProperty
		{
			get {return this.idDistrictProperty;}
			set {this.idDistrictProperty = value;}
		}
		#endregion � IdDistrictProperty �
		#region � Subdivision �
		[Description("Subdivision")]
		public string Subdivision
		{
			get
			{
				return this.subdivision;
			}
			set
			{
				this.subdivision = value;
			}
		}
		#endregion � Subdivision �
		#region � LegalDescription �
		[Description("Legal Description")]
		public string LegalDescription
		{
			get
			{
				return this.legalDescription;
			}
			set
			{
				this.legalDescription = value;
			}
		}
		#endregion � LegalDescription �
		#region � Vesting �
		[Description(" Vesting")]
		public string Vesting
		{
			get
			{
				return this.vesting;
			}
			set
			{
				this.vesting = value;
			}
		}
		#endregion � Vesting �
		#region � MapGrid �
		[Description("Map Grid")]
		public string MapGrid
		{
			get
			{
				return this.mapGrid;
			}
			set
			{
				this.mapGrid = value;
			}
		}
		#endregion � MapGrid �
		#region � MapArea �
		[Description("Map Area")]
		public string MapArea
		{
			get
			{
				return this.mapArea;
			}
			set
			{
				this.mapArea = value;
			}
		}
		#endregion � MapArea �
		#region � CityCode �
		[Description("City Code")]
		public string CityCode
		{
			get
			{
				return this.cityCode;
			}
			set
			{
				this.cityCode = value;
			}
		}
		#endregion � CityCode �
		#region � SchoolDistrict �
		[Description("School District")]
		public string SchoolDistrict
		{
			get
			{
				return this.schoolDistrict;
			}
			set
			{
				this.schoolDistrict = value;
			}
		}
		#endregion � SchoolDistrict �
		#endregion � Automatically Generated Properties �
		#region � CTR �
		public DistrictProperties()
		{
			this.idDistrictProperty = Guid.NewGuid();	
		}	
		#endregion � CTR �

		#region INHibernate Members

		public void NhSave(NHibernate.ISession session)
		{
			session.Save(this);
		}

		public void NhSaveOrUpdate (NHibernate.ISession session)
		{
			try
			{	
				if (true == this.save)							
					session.Save(this);					
				else
					session.SaveOrUpdate(this);
			}
			catch (Exception exc)
			{
				MessageBox.Show(exc.ToString());
			}
		}
		#endregion

		#region ISPL Members

		public void ExecuteSPL(System.Data.SqlClient.SqlConnection dbConn)
		{
			
			try
			{

				SqlCommand sp_insert_DistrictProperties = new SqlCommand("insert_DistrictProperties", dbConn);
				sp_insert_DistrictProperties.CommandType = CommandType.StoredProcedure;

				sp_insert_DistrictProperties.Parameters.Add("@IdDistrictProperty", this.IdDistrictProperty);
				sp_insert_DistrictProperties.Parameters.Add("@Subdivision", this.Subdivision);
				sp_insert_DistrictProperties.Parameters.Add("@LegalDescription", this.LegalDescription);
				sp_insert_DistrictProperties.Parameters.Add("@Vesting", this.Vesting);
				sp_insert_DistrictProperties.Parameters.Add("@MapGrid", this.MapGrid);
				sp_insert_DistrictProperties.Parameters.Add("@MapArea", this.MapArea);
				sp_insert_DistrictProperties.Parameters.Add("@CityCode", this.CityCode);
				sp_insert_DistrictProperties.Parameters.Add("@SchoolDistrict", this.SchoolDistrict);
				sp_insert_DistrictProperties.ExecuteNonQuery();
			}
			catch (Exception exc)
			{
				MessageBox.Show(exc.ToString());
			}
		}

		#endregion
	}
	#endregion � Class DistrictProperties �
}
#endregion � Namespace DealMaker �