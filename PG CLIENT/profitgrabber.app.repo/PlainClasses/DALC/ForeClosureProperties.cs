#region � Using �
using System;
using System.Data.SqlClient;
using System.Data;
using System.Windows.Forms;
#endregion � Using �
#region � Namespace DealMaker �
namespace DealMaker
{
	#region � Class ForeClosureProperties �
	/// <summary>
	/// Summary description for ForeClosureProperties.
	/// </summary>
	public class ForeClosureProperties : INHibernate, ISPL
	{
		#region � Data �
		private Guid	idForeClosureProperty		=	Guid.Empty;
		private	string	foreclosureStatus			=	string.Empty;
		private	string	foreclosureFileNumber		=	string.Empty;
		private	string	foreclosureFileDate			=	string.Empty;
		private	string	originalMortgageBalance		=	string.Empty;
		private	string	unpaidBalance				=	string.Empty;
		private	string	reinstatementAmount			=	string.Empty;
		private	string	originalFileNumber			=	string.Empty;
		private	string	originalDate				=	string.Empty;
		private	string	deedDate					=	string.Empty;
		private	string	originalTrustor				=	string.Empty;
		private	string	foreclosureSaleDate			=	string.Empty;
		private	string	foreclosureSaleTime			=	string.Empty;
		private	string	foreclosureSalePlace		=	string.Empty;
		private	string	trusteeSaleNumber			=	string.Empty;
		private	string	trusteeName					=	string.Empty;
		private	string	trusteePhoneNumber			=	string.Empty;
		private	string	trusteeFullStreetAddress	=	string.Empty;
		private	string	trusteeCity					=	string.Empty;
		private	string	trusteeState				=	string.Empty;
		private	string	trusteeZip					=	string.Empty;
		private	string	openBidAmount				=	string.Empty;
		private	string	mortgageBeneficiaryFirst	=	string.Empty;
		private	string	mortgageBeneficiaryLast		=	string.Empty;
		
		private string  loanRecordingNumber			=	string.Empty;
		private string  loanRecordingDate			=	string.Empty;												
		private string  trusteePhoneNr				=	string.Empty;												
		#endregion � Data �

		#region � Automatically Generated Properties �		
		#region � IdForeClosureProperty �
		public Guid IdForeClosureProperty
		{
			get {return this.idForeClosureProperty;}
			set {this.idForeClosureProperty = value;}
		}
		#endregion � IdForeClosureProperty �
		#region � ForeclosureStatus �
		[Description("Foreclosure Status")]
		public string ForeclosureStatus
		{
			get
			{
				return this.foreclosureStatus;
			}
			set
			{
				this.foreclosureStatus = value;
			}
		}
		#endregion � ForeclosureStatus �
		#region � ForeclosureFileNumber �
		[Description("Foreclosure File Number")]
		public string ForeclosureFileNumber
		{
			get
			{
				return this.foreclosureFileNumber;
			}
			set
			{
				this.foreclosureFileNumber = value;
			}
		}
		#endregion � ForeclosureFileNumber �
		#region � ForeclosureFileDate �
		[Description("Foreclosure File Date")]
		public string ForeclosureFileDate
		{
			get
			{
				return this.foreclosureFileDate;
			}
			set
			{
				this.foreclosureFileDate = value;
			}
		}
		#endregion � ForeclosureFileDate �
		#region � OriginalMortgageBalance �
		[Description("Original Mortgage Balance")]
		public string OriginalMortgageBalance
		{
			get
			{
				return this.originalMortgageBalance;
			}
			set
			{
				this.originalMortgageBalance = value;
			}
		}
		#endregion � OriginalMortgageBalance �
		#region � UnpaidBalance �
		[Description("Unpaid Balance")]
		public string UnpaidBalance
		{
			get
			{
				return this.unpaidBalance;
			}
			set
			{
				this.unpaidBalance = value;
			}
		}
		#endregion � UnpaidBalance �
		#region � ReinstatementAmount �
		[Description("Reinstatement Amount")]
		public string ReinstatementAmount
		{
			get
			{
				return this.reinstatementAmount;
			}
			set
			{
				this.reinstatementAmount = value;
			}
		}
		#endregion � ReinstatementAmount �
		#region � OriginalFileNumber �
		[Description("Original File Number")]
		public string OriginalFileNumber
		{
			get
			{
				return this.originalFileNumber;
			}
			set
			{
				this.originalFileNumber = value;
			}
		}
		#endregion � OriginalFileNumber �
		#region � OriginalDate �
		[Description("Original Date")]
		public string OriginalDate
		{
			get
			{
				return this.originalDate;
			}
			set
			{
				this.originalDate = value;
			}
		}
		#endregion � OriginalDate �
		#region � DeedDate �
		[Description("Foreclosure Deed Date")]	//ta23.09.2005 Added "Foreclosure " infront off "Deed Date"
		public string DeedDate
		{
			get
			{
				return this.deedDate;
			}
			set
			{
				this.deedDate = value;
			}
		}
		#endregion � DeedDate �
		#region � OriginalTrustor �
		[Description("Original Trustor")]
		public string OriginalTrustor
		{
			get
			{
				return this.originalTrustor;
			}
			set
			{
				this.originalTrustor = value;
			}
		}
		#endregion � OriginalTrustor �
		#region � ForeclosureSaleDate �
		[Description("Foreclosure Sale Date")]
		public string ForeclosureSaleDate
		{
			get
			{
				return this.foreclosureSaleDate;
			}
			set
			{
				this.foreclosureSaleDate = value;
			}
		}
		#endregion � ForeclosureSaleDate �
		#region � ForeclosureSaleTime �
		[Description("Foreclosure Sale Time")]
		public string ForeclosureSaleTime
		{
			get
			{
				return this.foreclosureSaleTime;
			}
			set
			{
				this.foreclosureSaleTime = value;
			}
		}
		#endregion � ForeclosureSaleTime �
		#region � ForeclosureSalePlace �
		[Description("Foreclosure Sale Place")]
		public string ForeclosureSalePlace
		{
			get
			{
				return this.foreclosureSalePlace;
			}
			set
			{
				this.foreclosureSalePlace = value;
			}
		}
		#endregion � ForeclosureSalePlace �
		#region � TrusteeSaleNumber �
		[Description("Trustee Sale Number")]
		public string TrusteeSaleNumber
		{
			get
			{
				return this.trusteeSaleNumber;
			}
			set
			{
				this.trusteeSaleNumber = value;
			}
		}
		#endregion � TrusteeSaleNumber �
		#region � TrusteeName �
		[Description("Trustee Name")]
		public string TrusteeName
		{
			get
			{
				return this.trusteeName;
			}
			set
			{
				this.trusteeName = value;
			}
		}
		#endregion � TrusteeName �
		#region � TrusteePhoneNumber �
		[Description("Trustee Phone Number")]
		public string TrusteePhoneNumber
		{
			get
			{
				return this.trusteePhoneNumber;
			}
			set
			{
				this.trusteePhoneNumber = value;
			}
		}
		#endregion � TrusteePhoneNumber �
		#region � TrusteeFullStreetAddress �
		[Description("Trustee Full Street Address")]
		public string TrusteeFullStreetAddress
		{
			get
			{
				return this.trusteeFullStreetAddress;
			}
			set
			{
				this.trusteeFullStreetAddress = value;
			}
		}
		#endregion � TrusteeFullStreetAddress �
		#region � TrusteeCity �
		[Description("Trustee City")]
		public string TrusteeCity
		{
			get
			{
				return this.trusteeCity;
			}
			set
			{
				this.trusteeCity = value;
			}
		}
		#endregion � TrusteeCity �
		#region � TrusteeState �
		[Description("Trustee State")]
		public string TrusteeState
		{
			get
			{
				return this.trusteeState;
			}
			set
			{
				this.trusteeState = value;
			}
		}
		#endregion � TrusteeState �
		#region � TrusteeZip �
		[Description("Trustee Zip")]
		public string TrusteeZip
		{
			get
			{
				return this.trusteeZip;
			}
			set
			{
				this.trusteeZip = value;
			}
		}
		#endregion � TrusteeZip �
		#region � OpenBidAmount �
		[Description("Open Bid Amount")]
		public string OpenBidAmount
		{
			get
			{
				return this.openBidAmount;
			}
			set
			{
				this.openBidAmount = value;
			}
		}
		#endregion � OpenBidAmount �
		#region � MortgageBeneficiaryFirst �
		[Description("Mortgage Beneficiary First")]
		public string MortgageBeneficiaryFirst
		{
			get
			{
				return this.mortgageBeneficiaryFirst;
			}
			set
			{
				this.mortgageBeneficiaryFirst = value;
			}
		}
		#endregion � MortgageBeneficiaryFirst �
		#region � MortgageBeneficiaryLast �
		[Description("Mortgage Beneficiary Last")]
		public string MortgageBeneficiaryLast
		{
			get
			{
				return this.mortgageBeneficiaryLast;
			}
			set
			{
				this.mortgageBeneficiaryLast = value;
			}
		}
		#endregion � MortgageBeneficiaryLast �
		
		#region � LoanRecordingNumber �
		[Description("Loan Recording Number")]
		public string LoanRecordingNumber
		{
			get { return this.loanRecordingNumber; }
			set { this.loanRecordingNumber = value; }
		}
		#endregion � LoanRecordingNumber �

		#region � LoanRecordingDate �
		[Description("Loan Recording Date")]
		public string LoanRecordingDate
		{
			get { return this.loanRecordingDate; }
			set { this.loanRecordingDate = value; }
		}
		#endregion � LoanRecordingDate �

		#region � TrusteePhoneNr �
		[Description("Trustee Phone Nr")] //TA 23.09.2005 "Number" was truncated to "Nr"
		public string TrusteePhoneNr
		{
			get { return this.trusteePhoneNr; }
			set { this.trusteePhoneNr = value; }
		}
		#endregion � TrusteePhoneNr �

		#endregion � Automatically Generated Properties �

		#region � CTR �
		public ForeClosureProperties()
		{
			this.idForeClosureProperty = Guid.NewGuid();
		}
		#endregion � CTR �

		#region INHibernate Members

		public void NhSave(NHibernate.ISession session)
		{
			session.Save(this);
		}

		public void NhSaveOrUpdate (NHibernate.ISession session)
		{
			try
			{
			session.SaveOrUpdate(this);
			}
			catch (Exception exc)
			{
				MessageBox.Show(exc.ToString());
			}
		}


		#endregion

		#region ISPL Members

		public void ExecuteSPL(SqlConnection dbConn)
		{
			
			try
			{
				SqlCommand sp_insert_ForeClosureProperties = new SqlCommand("insert_ForeClosureProperties", dbConn);
				sp_insert_ForeClosureProperties.CommandType = CommandType.StoredProcedure;

				sp_insert_ForeClosureProperties.Parameters.Add("@IdForeClosureProperty", this.IdForeClosureProperty);
				sp_insert_ForeClosureProperties.Parameters.Add("@ForeclosureStatus", this.ForeclosureStatus);
				sp_insert_ForeClosureProperties.Parameters.Add("@ForeclosureFileNumber", this.ForeclosureFileNumber);
				sp_insert_ForeClosureProperties.Parameters.Add("@ForeclosureFileDate", this.ForeclosureFileDate);
				sp_insert_ForeClosureProperties.Parameters.Add("@OriginalMortgageBalance", this.OriginalMortgageBalance);
				sp_insert_ForeClosureProperties.Parameters.Add("@UnpaidBalance", this.UnpaidBalance);
				sp_insert_ForeClosureProperties.Parameters.Add("@ReinstatementAmount", this.ReinstatementAmount);
				sp_insert_ForeClosureProperties.Parameters.Add("@OriginalFileNumber", this.OriginalFileNumber);
				sp_insert_ForeClosureProperties.Parameters.Add("@OriginalDate", this.OriginalDate);
				sp_insert_ForeClosureProperties.Parameters.Add("@DeedDate", this.DeedDate);
				sp_insert_ForeClosureProperties.Parameters.Add("@OriginalTrustor", this.OriginalTrustor);
				sp_insert_ForeClosureProperties.Parameters.Add("@ForeclosureSaleDate", this.ForeclosureSaleDate);
				sp_insert_ForeClosureProperties.Parameters.Add("@ForeclosureSaleTime", this.ForeclosureSaleTime);
				sp_insert_ForeClosureProperties.Parameters.Add("@ForeclosureSalePlace", this.ForeclosureSalePlace);
				sp_insert_ForeClosureProperties.Parameters.Add("@TrusteeSaleNumber", this.TrusteeSaleNumber);
				sp_insert_ForeClosureProperties.Parameters.Add("@TrusteeName", this.TrusteeName);
				sp_insert_ForeClosureProperties.Parameters.Add("@TrusteePhoneNumber", this.TrusteePhoneNumber);
				sp_insert_ForeClosureProperties.Parameters.Add("@TrusteeFullStreetAddress", this.TrusteeFullStreetAddress);
				sp_insert_ForeClosureProperties.Parameters.Add("@TrusteeCity", this.TrusteeCity);
				sp_insert_ForeClosureProperties.Parameters.Add("@TrusteeState", this.TrusteeState);
				sp_insert_ForeClosureProperties.Parameters.Add("@TrusteeZip", this.TrusteeZip);
				sp_insert_ForeClosureProperties.Parameters.Add("@OpenBidAmount", this.OpenBidAmount);
				sp_insert_ForeClosureProperties.Parameters.Add("@MortgageBeneficiaryFirst", this.MortgageBeneficiaryFirst);
				sp_insert_ForeClosureProperties.Parameters.Add("@MortgageBeneficiaryLast", this.MortgageBeneficiaryLast);
				sp_insert_ForeClosureProperties.Parameters.Add("@LoanRecordingNumber", this.LoanRecordingNumber);
				sp_insert_ForeClosureProperties.Parameters.Add("@LoanRecordingDate", this.LoanRecordingDate);
				sp_insert_ForeClosureProperties.Parameters.Add("@TrusteePhoneNr", this.TrusteePhoneNr);				
				sp_insert_ForeClosureProperties.ExecuteNonQuery();
			}
			catch (Exception exc)
			{
				MessageBox.Show(exc.ToString());
			}
		}

		#endregion
	}
	#endregion � Class ForeClosureProperties �
}
#endregion � Namespace DealMaker �