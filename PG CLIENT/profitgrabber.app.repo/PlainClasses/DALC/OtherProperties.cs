#region � Using �
using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

#endregion � Using �
#region � Namespace DealMaker �
namespace DealMaker
{
	#region � Class OtherProperties �
	/// <summary>
	/// Summary description for OtherProperties.
	/// </summary>
	public class OtherProperties : INHibernate, ISPL
	{
		#region � Data �
		private Guid	idOtherProperty		=	Guid.Empty;
		private	string	mailCRRT			=	string.Empty;
		private	string	mailScore			=	string.Empty;
		private	string	privacyFlag			=	string.Empty;
		private	string	doNotCallFlag		=	string.Empty;
		private	string	barCodeStartNumber	=	string.Empty;
		private	string	barCodeEndNumber	=	string.Empty;
		#endregion � Data �	
		#region � Automatically Generated Properties �
		#region � IdOtherProperty �
		public Guid IdOtherProperty
		{
			get {return this.idOtherProperty;}
			set {this.idOtherProperty = value;}
		}
		#endregion � IdOtherProperty �
		#region � MailCRRT �
		[Description("Mail CRRT")]
		public string MailCRRT
		{
			get
			{
				return this.mailCRRT;
			}
			set
			{
				this.mailCRRT = value;
			}
		}
		#endregion � MailCRRT �
		#region � MailScore �
		[Description("Mail Score")]
		public string MailScore
		{
			get
			{
				return this.mailScore;
			}
			set
			{
				this.mailScore = value;
			}
		}
		#endregion � MailScore �
		#region � PrivacyFlag �
		[Description("Privacy Flag")]
		public string PrivacyFlag
		{
			get
			{
				return this.privacyFlag;
			}
			set
			{
				this.privacyFlag = value;
			}
		}
		#endregion � PrivacyFlag �
		#region � DoNotCallFlag �
		[Description("Do Not Call Flag")]
		public string DoNotCallFlag
		{
			get
			{
				return this.doNotCallFlag;
			}
			set
			{
				this.doNotCallFlag = value;
			}
		}
		#endregion � DoNotCallFlag �
		#region � BarCodeStartNumber �
		[Description("Bar Code Start Number")]
		public string BarCodeStartNumber
		{
			get
			{
				return this.barCodeStartNumber;
			}
			set
			{
				this.barCodeStartNumber = value;
			}
		}
		#endregion � BarCodeStartNumber �
		#region � BarCodeEndNumber �
		[Description("Bar Code End Number")]
		public string BarCodeEndNumber
		{
			get
			{
				return this.barCodeEndNumber;
			}
			set
			{
				this.barCodeEndNumber = value;
			}
		}
		#endregion � BarCodeEndNumber �
		#endregion � Automatically Generated Properties �
		#region � CTR �
		public OtherProperties()
		{
			this.idOtherProperty = Guid.NewGuid();
		}
		#endregion � CTR �	

		#region INHibernate Members

		public void NhSave(NHibernate.ISession session)
		{
			session.Save(this);
		}

		public void NhSaveOrUpdate (NHibernate.ISession session)
		{
			try
			{
			session.SaveOrUpdate(this);
			}
			catch (Exception exc)
			{
				MessageBox.Show(exc.ToString());
			}
		}


		#endregion

		#region ISPL Members

		public void ExecuteSPL(SqlConnection dbConn)
		{
			try
			{
				SqlCommand sp_insert_OtherProperties = new SqlCommand("insert_OtherProperties", dbConn);
				sp_insert_OtherProperties.CommandType = CommandType.StoredProcedure;

				sp_insert_OtherProperties.Parameters.Add("@IdOtherProperty", this.IdOtherProperty);
				sp_insert_OtherProperties.Parameters.Add("@MailCRRT", this.MailCRRT);
				sp_insert_OtherProperties.Parameters.Add("@MailScore", this.MailScore);
				sp_insert_OtherProperties.Parameters.Add("@PrivacyFlag", this.PrivacyFlag);
				sp_insert_OtherProperties.Parameters.Add("@DoNotCallFlag", this.DoNotCallFlag);
				sp_insert_OtherProperties.Parameters.Add("@BarCodeStartNumber", this.BarCodeStartNumber);
				sp_insert_OtherProperties.Parameters.Add("@BarCodeEndNumber", this.BarCodeEndNumber);
				sp_insert_OtherProperties.ExecuteNonQuery();
			}
			catch (Exception exc)
			{
				MessageBox.Show(exc.ToString());
			}
		}

		#endregion
	}
	#endregion � Class OtherProperties �
}
#endregion � Namespace DealMaker �
