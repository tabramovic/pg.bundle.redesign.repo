using System;

namespace DealMaker
{
	/// <summary>
	/// Summary description for Media.
	/// </summary>
	public class Media : INHibernate
	{
		private Guid idMedia;
		private string mediaName;
		public Media()
		{
			this.idMedia = Guid.NewGuid();
		}
		
		public Guid IdMedia
		{
			get {return this.idMedia;}
			set {this.idMedia = value;}
		}

		public string MediaName
		{
			get {return this.mediaName;}
			set {this.mediaName = value;}
		}

		#region INHibernate Members

		public void NhSave(NHibernate.ISession session)
		{
			session.Save(this);
		}

		public void NhSaveOrUpdate (NHibernate.ISession session)
		{
			session.SaveOrUpdate(this); 
		}
		#endregion
	}
}
