#region � Using �
using System;
using System.Data.SqlClient;
using System.Data;
using System.Windows.Forms;
#endregion � Using �
#region � Namespace DealMaker �
namespace DealMaker
{
	#region � Class ListingAgent �
	/// <summary>
	/// Summary description for ListingAgent.
	/// </summary>
	public class ListingAgent : INHibernate,ISPL
	{
		#region � Data �
		
		public bool save = false;

		private Guid   idListingAgent = Guid.Empty;
		private string listingAgentFullName = string.Empty;
		private string listingAgentFirstName = string.Empty;
		private string listingAgentLastName = string.Empty;
		private string listingAgentPhoneNumber = string.Empty;
		private string listingAgentFaxNumber = string.Empty;
		private string listingAgentOffice	= string.Empty;
		private string listingAgentCode = string.Empty;
		private string listingAgentMLSNumber = string.Empty;
		private double listingPrice;
		private string listingAgentEMail = string.Empty;
		#endregion � Data �
		#region � Automatically Generated Properties �
		#region � IdListingAgent �
		public Guid IdListingAgent
		{
			get {return this.idListingAgent;}
			set {this.idListingAgent = value;}
		}
		#endregion � IdListingAgent �
		#region · ListingAgentFullName ·
		[Description("Listing Agent Full Name")]
		public string ListingAgentFullName
		{
			get
			{
				if (string.Empty == this.listingAgentFullName.Trim())
				{
					string fn = string.Empty;
					if (string.Empty != this.listingAgentFirstName)
						fn += this.listingAgentFirstName;
					
					if (string.Empty != this.listingAgentLastName)
					{
						if (string.Empty != fn)
							fn += " ";
						fn += this.listingAgentLastName;
					}
					return fn;
				}
				else
				{
					return this.listingAgentFullName;
				}
			}
			set
			{
				this.listingAgentFullName = value;
			}
		}
		#endregion · ListingAgentFullName ·
		#region · ListingAgentFirstName ·
		[Description("Listing Agent First Name")]
		public string ListingAgentFirstName
		{
			get
			{
				return this.listingAgentFirstName;
			}
			set
			{
				this.listingAgentFirstName = value;
			}
		}
		#endregion · ListingAgentFirstName ·
		#region · ListingAgentLastName ·
		[Description("Listing Agent Last Name")]
		public string ListingAgentLastName
		{
			get
			{
				return this.listingAgentLastName;
			}
			set
			{
				this.listingAgentLastName = value;
			}
		}
		#endregion · ListingAgentLastName ·
		#region · ListingAgentPhoneNumber ·
		[Description("Listing Agent Phone Number")]
		public string ListingAgentPhoneNumber
		{
			get
			{
				return this.listingAgentPhoneNumber;
			}
			set
			{
				this.listingAgentPhoneNumber = value;
			}
		}
		#endregion · ListingAgentPhoneNumber ·
		#region · ListingAgentFaxNumber ·
		[Description("Listing Agent Fax Number")]
		public string ListingAgentFaxNumber
		{
			get
			{
				return this.listingAgentFaxNumber;
			}
			set
			{
				this.listingAgentFaxNumber = value;
			}
		}
		#endregion · ListingAgentFaxNumber ·
		#region · ListingAgentCode ·
		[Description("Listing Agent Office")]
		public string ListingAgentOffice
		{
			get
			{
				return this.listingAgentOffice;
			}
			set
			{
				this.listingAgentOffice = value;
			}
		}
		#endregion · ListingAgentOffice ·
		#region · ListingAgentCode ·
		[Description("Listing Agent Code")]
		public string ListingAgentCode
		{
			get
			{
				return this.listingAgentCode;
			}
			set
			{
				this.listingAgentCode = value;
			}
		}
		#endregion · ListingAgentCode ·
		#region · ListingAgentMLSNumber ·
		[Description("Listing Agent MLS Number")]
		public string ListingAgentMLSNumber
		{
			get
			{
				return this.listingAgentMLSNumber;
			}
			set
			{
				this.listingAgentMLSNumber = value;
			}
		}
		#endregion · ListingAgentMLSNumber ·
		#region · ListingPrice ·
		[Description("Listing Price")]
		public double ListingPrice
		{
			get
			{
				return this.listingPrice;
			}
			set
			{
				this.listingPrice = value;
			}
		}
		#endregion · ListingPrice ·
		#region · ListingAgentEMail ·
		[Description("Listing Agent eMail")]
		public string ListingAgentEMail
		{
			get
			{
				return this.listingAgentEMail;
			}
			set
			{
				this.listingAgentEMail = value;
			}
		}
		#endregion · ListingAgentEMail ·
		#endregion � Automatically Generated Properties �	
		#region � CTR �
		public ListingAgent()
		{
			this.idListingAgent = Guid.NewGuid();
		}
		#endregion � CTR �

		#region INHibernate Members

		public void NhSave(NHibernate.ISession session)
		{
			session.Save(this); 
		}

		public void NhSaveOrUpdate (NHibernate.ISession session)
		{
			try
			{
				//OLD RQ_0008
				//session.SaveOrUpdate(this);
				//OLD RQ_0008

				if (true == this.save)							
					session.Save(this);					
				else
					session.SaveOrUpdate(this);

			}
			catch (Exception exc)
			{
				MessageBox.Show(exc.ToString());
			}
		}

		#endregion

		#region ISPL Members

		public void ExecuteSPL(SqlConnection dbConn)
		{
			try
			{
				SqlCommand sp_insert_ListingAgent = new SqlCommand("insert_ListingAgent", dbConn);
				sp_insert_ListingAgent.CommandType = CommandType.StoredProcedure;

				sp_insert_ListingAgent.Parameters.Add("@IdListingAgent", this.IdListingAgent);
				sp_insert_ListingAgent.Parameters.Add("@ListingAgentFullName", this.ListingAgentFullName);
				sp_insert_ListingAgent.Parameters.Add("@ListingAgentFirstName", this.ListingAgentFirstName);
				sp_insert_ListingAgent.Parameters.Add("@ListingAgentLastName", this.ListingAgentLastName);
				sp_insert_ListingAgent.Parameters.Add("@ListingAgentPhoneNumber", this.ListingAgentPhoneNumber);
				sp_insert_ListingAgent.Parameters.Add("@ListingAgentFaxNumber", this.ListingAgentFaxNumber);
				sp_insert_ListingAgent.Parameters.Add("@ListingAgentOffice", this.ListingAgentOffice);
				sp_insert_ListingAgent.Parameters.Add("@ListingAgentCode", this.ListingAgentCode);
				sp_insert_ListingAgent.Parameters.Add("@ListingAgentMLSNumber", this.ListingAgentMLSNumber);
				sp_insert_ListingAgent.Parameters.Add("@ListingPrice", this.ListingPrice);
				sp_insert_ListingAgent.Parameters.Add("@ListingAgentEMail", this.ListingAgentEMail);
				sp_insert_ListingAgent.ExecuteNonQuery();
			}
			catch (Exception exc)
			{
				MessageBox.Show(exc.ToString());
			}
		}

		#endregion
	}
	#endregion � class ListingAgent �
}
#endregion � Namespace DealMaker �