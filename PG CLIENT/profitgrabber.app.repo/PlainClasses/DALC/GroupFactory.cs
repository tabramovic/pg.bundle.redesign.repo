using System;
using System.Collections;
using NHibernate;
using DealMaker.Classes;
using System.Windows.Forms;

namespace DealMaker.DALC
{
	/// <summary>
	/// Represents a class for creating instances of Group class.
	/// </summary>
	public class GroupFactory
	{
		private static GroupFactory instance = new GroupFactory();

		private GroupFactory()
		{
		}

		public Group GetGroup(int groupId)
		{
			Group result = null;

			try
			{
				ISession s = Globals.factory.OpenSession();
				
				try
				{
					result = s.Load(typeof(Group), groupId) as Group;
				}
				finally
				{
					s.Close();
				}
			}
			catch (Exception e)
			{
				MessageBox.Show(e.Message); 
			}

			return result;
		}

		public Group[] GetGroups()
		{
			ArrayList groups = new ArrayList();

			try
			{
				ISession s = Globals.factory.OpenSession();
				
				try
				{
					IList list;
					ICriteria c;

					c = s.CreateCriteria(typeof(Group));
					list = c.List();

					foreach (object o in list)
						groups.Add(o as Group);
				}
				finally
				{
					s.Close();
				}
			}
			catch(System.Exception e)
			{
				System.Windows.Forms.MessageBox.Show(e.ToString());
			}

			return (Group[])groups.ToArray(typeof(Group));
		}

		public static GroupFactory GetInstance()
		{
			return instance;
		}
	}
}