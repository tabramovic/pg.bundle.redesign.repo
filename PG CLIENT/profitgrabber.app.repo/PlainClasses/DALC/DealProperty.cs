#region Using 
using System;
using System.Windows.Forms;
using System.Data;
using System.Data.SqlClient;
#endregion Using 

namespace DealMaker  
{
	/// <summary>
	/// Summary description for DealProperty.
	/// </summary>
	public class DealProperty : INHibernate, ISPL
	{
		#region Data 
		public bool save = false;
		
		private Guid  idDealProperty		= Guid.Empty;
		private string type					= string.Empty;
		private string bedrooms				= string.Empty;
		private string baths					= string.Empty;
		private string sqft					= string.Empty;
		private string yearBuilt				= string.Empty;
		private string garage				= string.Empty;
		private string pool					= string.Empty;
		private string occupiedBy			= string.Empty;
		private string apn					= string.Empty;
		private string featureCondition		= string.Empty;
		private string whySelling			= string.Empty;
		private string worthBySeller			= string.Empty;
		private string source				= string.Empty;
		private string firstBal				= string.Empty;
		private string ir1					= string.Empty;
		private string type1					= string.Empty;
		private string moPay1				= string.Empty;
		private string taxIns				= string.Empty;
		private string taxAmnt				= string.Empty;
		private string secondBal				= string.Empty;
		private string ir2					= string.Empty;
		private string type2					= string.Empty;
		private string moPay2				= string.Empty;
		private string hoa					= string.Empty;
		private string thirdBal				= string.Empty;
		private string ir3					= string.Empty;
		private string type3					= string.Empty;
		private string moPay3				= string.Empty;
		private string otherLiens			= string.Empty;
		private string pmntsCurrent			= string.Empty;
		private string moBehind				= string.Empty;
		private string reInsAmnt				= string.Empty;
		private string inForecl				= string.Empty;
		private DateTime forSaleDate;
		private string listed				= string.Empty;
		private DateTime listExpDate;
		private string canCancelList			= string.Empty;
		private string lowestPriceAcceptable = string.Empty;
		private string sell4WhatOwes			= string.Empty;
		private string minCashNeededWhy		= string.Empty;
		private string desiredClosingDate	= string.Empty;
		private string motivation			= string.Empty;
		private string leadSource			= string.Empty;
		private string arv					= string.Empty;
		private string repairs				= string.Empty;
		private string nextStep				= string.Empty;
		#endregion Data 
		#region Properties 
		public Guid IdDealProperty
		{
			get {return this.idDealProperty;}
			set {this.idDealProperty = value;}
		}

		[Description("Next Step (DI)")]
		public string NextStep
		{
			get {return this.nextStep  ;}
			set {this.nextStep  = value;}
		}

		[Description("Repairs (DI)")]
		public string Repairs
		{
		get {return this.repairs  ;}
		set {this.repairs  = value;}
		}

		[Description("ARV (DI)")]
		public string Arv
		{
		get {return this.arv  ;}
		set {this.arv  = value;}
		}

		[Description("Lead Source (DI)")]
		public string LeadSource
		{
		get {return this.leadSource  ;}
		set {this.leadSource  = value;}
		}


		[Description("Motivation (DI)")]
		public string Motivation
		{
		get {return this.motivation  ;}
		set {this.motivation  = value;}
		}

		[Description("Desired Closing Date (DI)")]
		public string DesiredClosingDate
		{
		get {return this.desiredClosingDate  ;}
		set {this.desiredClosingDate  = value;}
		}

		[Description("Min. Cash Needed (DI)")]
		public string MinCashNeededWhy
		{
		get {return this.minCashNeededWhy  ;}
		set {this.minCashNeededWhy  = value;}
		}
		
		public string Sell4WhatOwes
		{
		get {return this.sell4WhatOwes  ;}
		set {this.sell4WhatOwes  = value;}
		}
		
		[Description("Lowest Price Acceptable (DI)")]
		public string LowestPriceAcceptable
		{
		get {return this.lowestPriceAcceptable  ;}
		set {this.lowestPriceAcceptable  = value;}
		}

		[Description("Can Cancel List (DI)")]
		public string CanCancelList
		{
		get {return this.canCancelList  ;}
		set {this.canCancelList  = value;}
		}

		[Description("Listing Exp. Date (DI)")]
		public DateTime ListExpDate
		{
		get {return this.listExpDate  ;}
		set {this.listExpDate  = value;}
		}

		[Description("Listed (DI)")]
		public string Listed
		{
		get {return this.listed  ;}
		set {this.listed  = value;}
		}

		[Description("Forecl. Sale Date (DI)")]
		public DateTime ForSaleDate
		{
		get {return this.forSaleDate  ;}
		set {this.forSaleDate  = value;}
		}

		[Description("In Forecl. (DI)")]
		public string InForecl
		{
		get {return this.inForecl  ;}
		set {this.inForecl  = value;}
		}

		[Description("ReInst. Amount (DI)")]
		public string ReInsAmnt
		{
		get {return this.reInsAmnt  ;}
		set {this.reInsAmnt  = value;}
		}

		[Description("Mo. Behind (DI)")]
		public string MoBehind
		{
		get {return this.moBehind  ;}
		set {this.moBehind  = value;}
		}

		[Description("Payments Current (DI)")]
		public string PmntsCurrent
		{
		get {return this.pmntsCurrent  ;}
		set {this.pmntsCurrent  = value;}
		}

		[Description("Amnt Of Other Liens (DI)")]
		public string OtherLiens
		{
		get {return this.otherLiens  ;}
		set {this.otherLiens  = value;}
		}

		[Description("3rd Loan Monthly (DI)")]
		public string MoPay3
		{
		get {return this.moPay3  ;}
		set {this.moPay3  = value;}
		}

		[Description("3rd Loan Type (DI)")]
		public string Type3
		{
		get {return this.type3  ;}
		set {this.type3  = value;}
		}

		[Description("3rd Loan Int. Rate(DI)")]
		public string Ir3
		{
		get {return this.ir3  ;}
		set {this.ir3  = value;}
		}

		[Description("3rd Loan Balance (DI)")]
		public string ThirdBal
		{
		get {return this.thirdBal  ;}
		set {this.thirdBal  = value;}
		}

		[Description("HOA Per Month (DI)")]
		public string Hoa
		{
		get {return this.hoa  ;}
		set {this.hoa  = value;}
		}

		[Description("2nd Loan Monthly (DI)")]
		public string MoPay2
		{
		get {return this.moPay2  ;}
		set {this.moPay2  = value;}
		}
		
		[Description("2nd Loan Type (DI)")]
		public string Type2
		{
		get {return this.type2  ;}
		set {this.type2  = value;}
		}

		[Description("2nd Loan Int. rate(DI)")]
		public string Ir2
		{
		get {return this.ir2  ;}
		set {this.ir2  = value;}
		}

		[Description("2nd Loan Balance")]
		public string SecondBal
		{
		get {return this.secondBal  ;}
		set {this.secondBal  = value;}
		}

		[Description("Tax Amount (DI)")]
		public string TaxAmnt
		{
		get {return this.taxAmnt  ;}
		set {this.taxAmnt  = value;}
		}

		[Description("Tax Insurance (DI)")]
		public string TaxIns
		{
		get {return this.taxIns  ;}
		set {this.taxIns  = value;}
		}

		[Description("1st Loan Monthly (DI)")]
		public string MoPay1
		{
		get {return this.moPay1  ;}
		set {this.moPay1  = value;}
		}

		[Description("1st Loan Type (DI)")]
		public string Type1
		{
		get {return this.type1  ;}
		set {this.type1  = value;}
		}

		[Description("1st Loan Int. Rate (DI)")]
		public string Ir1
		{
		get {return this.ir1  ;}
		set {this.ir1  = value;}
		}

		[Description("1st Loan Balance")]
		public string FirstBal
		{
		get {return this.firstBal  ;}
		set {this.firstBal  = value;}
		}

		[Description("Source (DI)")]
		public string Source
		{
		get {return this.source  ;}
		set {this.source  = value;}
		}

		[Description("Worth By Seller (DI)")]
		public string WorthBySeller
		{
		get {return this.worthBySeller  ;}
		set {this.worthBySeller  = value;}
		}

		[Description("Why Selling (DI)")]
		public string WhySelling
		{
		get {return this.whySelling  ;}
		set {this.whySelling  = value;}
		}

		[Description("Condition (DI)")]
		public string FeatureCondition
		{
		get {return this.featureCondition  ;}
		set {this.featureCondition  = value;}
		}

		[Description("APN (DI)")]
		public string Apn
		{
		get {return this.apn  ;}
		set {this.apn  = value;}
		}

		public string OccupiedBy
		{
		get {return this.occupiedBy  ;}
		set {this.occupiedBy  = value;}
		}

		[Description("Pool (DI)")]
		public string Pool
		{
		get {return this.pool  ;}
		set {this.pool  = value;}
		}

		[Description("Garage (DI)")]
		public string Garage
		{
		get {return this.garage  ;}
		set {this.garage  = value;}
		}

		[Description("Year Built (DI)")]
		public string YearBuilt
		{
		get {return this.yearBuilt  ;}
		set {this.yearBuilt  = value;}
		}

		[Description("SqFt (DI)")]
		public string Sqft
		{
		get {return this.sqft  ;}
		set {this.sqft  = value;}
		}


		[Description("Baths (DI)")]
		public string Baths
		{
		get {return this.baths  ;}
		set {this.baths  = value;}
		}

		[Description("Type (DI)")]
		public string Type
		{
			get {return this.type  ;}
			set {this.type  = value;}
		}

		[Description("Bedrooms (DI)")]
		public string Bedrooms
		{
		get {return this.bedrooms  ;}
		set {this.bedrooms  = value;}
		}

		public DealProperty()
		{
			this.idDealProperty = Guid.NewGuid();
		}
		#endregion Properties 
		
		#region INHibernate Members

		public void NhSave(NHibernate.ISession session)
		{
			session.Save(this);
		}

		public void NhSaveOrUpdate(NHibernate.ISession session)
		{
			try
			{
				if (true == this.save)							
					session.Save(this);					
				else
					session.SaveOrUpdate(this);
			}
			catch (Exception exc)
			{
				MessageBox.Show(exc.ToString());
			}
		}

		#endregion

		#region ISPL Members

		public void ExecuteSPL(System.Data.SqlClient.SqlConnection dbConn)
		{
			try
			{
				SqlCommand sp_insert_DealProperty = new SqlCommand("insert_DealProperty", dbConn);
				sp_insert_DealProperty.CommandType = CommandType.StoredProcedure;

				sp_insert_DealProperty.Parameters.Add("@IdDealProperty", this.IdDealProperty);
				sp_insert_DealProperty.Parameters.Add("@Type", this.Type ?? string.Empty);
				sp_insert_DealProperty.Parameters.Add("@Bedrooms", this.Bedrooms ?? string.Empty);
				sp_insert_DealProperty.Parameters.Add("@Baths", this.Baths ?? string.Empty);
				sp_insert_DealProperty.Parameters.Add("@Sqft", this.Sqft ?? string.Empty);
				sp_insert_DealProperty.Parameters.Add("@YearBuilt", this.YearBuilt ?? string.Empty);
				sp_insert_DealProperty.Parameters.Add("@Garage", this.Garage ?? string.Empty);
				sp_insert_DealProperty.Parameters.Add("@Pool", this.Pool ?? string.Empty);
				sp_insert_DealProperty.Parameters.Add("@OccupiedBy", this.OccupiedBy ?? string.Empty);
				sp_insert_DealProperty.Parameters.Add("@Apn", this.Apn ?? string.Empty);
				sp_insert_DealProperty.Parameters.Add("@FeatureCondition", this.FeatureCondition ?? string.Empty);
				sp_insert_DealProperty.Parameters.Add("@WhySelling", this.WhySelling ?? string.Empty);
				sp_insert_DealProperty.Parameters.Add("@WorthBySeller", this.WorthBySeller ?? string.Empty);
				sp_insert_DealProperty.Parameters.Add("@Source", this.Source ?? string.Empty);
				sp_insert_DealProperty.Parameters.Add("@FirstBal", this.FirstBal ?? string.Empty);
				sp_insert_DealProperty.Parameters.Add("@Ir1", this.Ir1 ?? string.Empty);
				sp_insert_DealProperty.Parameters.Add("@Type1", this.Type1 ?? string.Empty);
				sp_insert_DealProperty.Parameters.Add("@MoPay1", this.MoPay1 ?? string.Empty);
				sp_insert_DealProperty.Parameters.Add("@TaxIns", this.TaxIns ?? string.Empty);
				sp_insert_DealProperty.Parameters.Add("@TaxAmnt", this.TaxAmnt ?? string.Empty);
				sp_insert_DealProperty.Parameters.Add("@SecondBal", this.SecondBal ?? string.Empty);
				sp_insert_DealProperty.Parameters.Add("@Ir2", this.Ir2 ?? string.Empty);
				sp_insert_DealProperty.Parameters.Add("@Type2", this.Type2 ?? string.Empty);
				sp_insert_DealProperty.Parameters.Add("@MoPay2", this.MoPay2 ?? string.Empty);
				sp_insert_DealProperty.Parameters.Add("@Hoa", this.Hoa ?? string.Empty);
				sp_insert_DealProperty.Parameters.Add("@ThirdBal", this.ThirdBal ?? string.Empty);
				sp_insert_DealProperty.Parameters.Add("@Ir3", this.Ir3 ?? string.Empty);
				sp_insert_DealProperty.Parameters.Add("@Type3", this.Type3 ?? string.Empty);
				sp_insert_DealProperty.Parameters.Add("@MoPay3", this.MoPay3 ?? string.Empty);
				sp_insert_DealProperty.Parameters.Add("@OtherLiens", this.OtherLiens ?? string.Empty);
				sp_insert_DealProperty.Parameters.Add("@PmntsCurrent", this.PmntsCurrent ?? string.Empty);
				sp_insert_DealProperty.Parameters.Add("@MoBehind", this.MoBehind ?? string.Empty);
				sp_insert_DealProperty.Parameters.Add("@ReInsAmnt", this.ReInsAmnt ?? string.Empty);
				sp_insert_DealProperty.Parameters.Add("@InForecl", this.InForecl ?? string.Empty);

				if (DateTime.MinValue != ForSaleDate)
					sp_insert_DealProperty.Parameters.Add("@ForSaleDate", this.ForSaleDate);
				else
					sp_insert_DealProperty.Parameters.Add("@ForSaleDate", DBNull.Value);

				sp_insert_DealProperty.Parameters.Add("@Listed", this.Listed);

				if (DateTime.MinValue != ListExpDate)
					sp_insert_DealProperty.Parameters.Add("@ListExpDate", this.ListExpDate);
				else
					sp_insert_DealProperty.Parameters.Add("@ListExpDate", DBNull.Value);

				sp_insert_DealProperty.Parameters.Add("@CanCancelList", this.CanCancelList);
				sp_insert_DealProperty.Parameters.Add("@LowestPriceAcceptable", this.LowestPriceAcceptable);
				sp_insert_DealProperty.Parameters.Add("@Sell4WhatOwes", this.Sell4WhatOwes);
				sp_insert_DealProperty.Parameters.Add("@MinCashNeededWhy", this.MinCashNeededWhy);
				sp_insert_DealProperty.Parameters.Add("@DesiredClosingDate", this.DesiredClosingDate);
				sp_insert_DealProperty.Parameters.Add("@Motivation", this.Motivation);
				sp_insert_DealProperty.Parameters.Add("@LeadSource", this.LeadSource);
				sp_insert_DealProperty.Parameters.Add("@Arv", this.Arv);
				sp_insert_DealProperty.Parameters.Add("@Repairs", this.Repairs);
				sp_insert_DealProperty.Parameters.Add("@NextStep", this.NextStep);
				sp_insert_DealProperty.ExecuteNonQuery();
			}
			catch (Exception exc)
			{
				MessageBox.Show(exc.ToString());
			}
		}

		#endregion
	}
}
