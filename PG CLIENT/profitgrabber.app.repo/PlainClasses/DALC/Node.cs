#region � Using �
using System;
#endregion � Using �
#region � Namespace DealMaker �
namespace DealMaker
{
	#region � Class Node �
	public class Node
	{
		#region � Data �
		public int		nodeId;
		public int		parentNodeId;
		public string	nodeDesc = string.Empty;
		public string	nodeName = string.Empty;
		public bool		isExpanded = false;
		public bool		isPreDefined = false;
		#endregion � Data �
		#region � CTR �
		public Node(int nodeId, int parentNodeId, string nodeDesc, string nodeName, bool isExpanded, bool isPreDefined)
		{
			this.nodeId				= nodeId;
			this.parentNodeId		= parentNodeId;
			this.nodeDesc			= nodeDesc;
			this.nodeName			= nodeName;
			this.isExpanded			= isExpanded;
			this.isPreDefined		= isPreDefined;
		}
		
		public Node(int nodeId, int parentNodeId, string nodeDesc, string nodeName, bool isExpanded)
		{
			this.nodeId				= nodeId;
			this.parentNodeId		= parentNodeId;
			this.nodeDesc			= nodeDesc;
			this.nodeName			= nodeName;
			this.isExpanded			= isExpanded;
		}

		public Node(int nodeId, int parentNodeId, string nodeDesc, string nodeName)
		{
			this.nodeId				= nodeId;
			this.parentNodeId		= parentNodeId;
			this.nodeDesc			= nodeDesc;
			this.nodeName			= nodeName;
		}

		public Node(int nodeId)
		{
			this.nodeId				= nodeId;
			this.parentNodeId		= 0;			
		}

		public Node()
		{
		}
		#endregion � CTR �
	}
	#endregion � Class Node �
}
#endregion � Namespace DealMaker �
