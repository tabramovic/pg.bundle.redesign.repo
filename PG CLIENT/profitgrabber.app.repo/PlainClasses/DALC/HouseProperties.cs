#region � Using �
using System;
using System.Data.SqlClient;
using System.Data;
using System.Windows.Forms;
#endregion � Using �
#region � Namespace DealMaker �
namespace DealMaker
{
	#region � Class HouseProperties �
	/// <summary>
	/// Summary description for HouseProperties.
	/// </summary>
	public class HouseProperties : INHibernate, ISPL
	{
	
		#region � Data �
		private Guid    idHouseProperty = Guid.Empty;
		private	double	bedrooms	=	0;
		private	double	bathrooms	=	0;
		private	string	garage		=	string.Empty;
		private	string	pool		=	string.Empty;
		private	double	homeSqFt 	=	0;
		private	double	lotSqFt		=	0;
		private	int		yearBuilt	=	0;
		private	double	story		=	0;
		private	string	fireplace	=	string.Empty;
		private string  propertyType = string.Empty;
		#endregion � Data �
		#region � Automatically Generated Properties �
		#region � IdHouseProperty �
		public Guid IdHouseProperty
		{
			get {return this.idHouseProperty;}
			set {this.idHouseProperty = value;}
		}
		#endregion � IdHouseProperty �
		#region � Bedrooms �
		[Description("Bedrooms")]
		public double Bedrooms
		{
			get
			{
				return this.bedrooms;
			}
			set
			{
				this.bedrooms = value;
			}
		}
		#endregion � Bedrooms �
		#region � Bathrooms �
		[Description("Bathrooms")]
		public double Bathrooms
		{
			get
			{
				return this.bathrooms;
			}
			set
			{
				this.bathrooms = value;
			}
		}
		#endregion � Bathrooms �
		#region � Garage �
		[Description("Garage")]
		public string Garage
		{
			get
			{
				return this.garage;
			}
			set
			{
				this.garage = value;
				if (this.garage.Length > 50)
					this.garage = this.garage.Substring(0, 50);
			}
		}
		#endregion � Garage �
		#region � Pool �
		[Description("Pool")]
		public string Pool
		{
			get
			{
				return this.pool;
			}
			set
			{
				this.pool = value;
			}
		}
		#endregion � Pool �
		#region � HomeSqFt �
		[Description("Home Sq Ft")]
		public double HomeSqFt
		{
			get
			{
				return this.homeSqFt;
			}
			set
			{
				this.homeSqFt = value;
			}
		}
		#endregion � HomeSqFt �
		#region � LotSqFt �
		[Description("Lot Sq Ft")]
		public double LotSqFt
		{
			get
			{
				return this.lotSqFt;
			}
			set
			{
				this.lotSqFt = value;
			}
		}
		#endregion � LotSqFt �
		#region � YearBuilt �
		[Description("Year Built")]
		public int YearBuilt
		{
			get
			{
				return this.yearBuilt;
			}
			set
			{
				this.yearBuilt = value;
			}
		}
		#endregion � YearBuilt �
		#region � Story �
		[Description("Story")]
		public double Story
		{
			get
			{
				return this.story;
			}
			set
			{
				this.story = value;
			}
		}
		#endregion � Story �
		#region � Fireplace �
		[Description("Fireplace")]
		public string Fireplace
		{
			get
			{
				return this.fireplace;
			}
			set
			{
				this.fireplace = value;
			}
		}
		#endregion � Fireplace �
		#region � PropertyType �
		[Description("PropertyType")]
		public string PropertyType
		{
			get { return this.propertyType; }
			set { this.propertyType = value; }
		}
		#endregion � PropertyType �
		#endregion � Automatically Generated Properties �
		#region � CTR �
		public HouseProperties()
		{
			this.idHouseProperty = Guid.NewGuid();
		}
		#endregion � CTR �

		#region INHibernate Members

		public void NhSave(NHibernate.ISession session)
		{
			session.Save(this);
		}
		public void NhSaveOrUpdate (NHibernate.ISession session)
		{
			try
			{
			session.SaveOrUpdate(this);
			}
			catch (Exception exc)
			{
				MessageBox.Show(exc.ToString());
			}
		}

		#endregion

		#region ISPL Members

		public void ExecuteSPL(SqlConnection dbConn)
		{
			try
			{

				SqlCommand sp_insert_HouseProperties = new SqlCommand("insert_HouseProperties", dbConn);
				sp_insert_HouseProperties.CommandType = CommandType.StoredProcedure;

				sp_insert_HouseProperties.Parameters.Add("@IdHouseProperty", this.IdHouseProperty);
				sp_insert_HouseProperties.Parameters.Add("@Bedrooms", this.Bedrooms);
				sp_insert_HouseProperties.Parameters.Add("@Bathrooms", this.Bathrooms);
				sp_insert_HouseProperties.Parameters.Add("@Garage", this.Garage);
				sp_insert_HouseProperties.Parameters.Add("@Pool", this.Pool);
				sp_insert_HouseProperties.Parameters.Add("@HomeSqFt", this.HomeSqFt);
				sp_insert_HouseProperties.Parameters.Add("@LotSqFt", this.LotSqFt);
				sp_insert_HouseProperties.Parameters.Add("@YearBuilt", this.YearBuilt);
				sp_insert_HouseProperties.Parameters.Add("@Story", this.Story);
				sp_insert_HouseProperties.Parameters.Add("@Fireplace", this.Fireplace);				
				sp_insert_HouseProperties.Parameters.Add("@PropertyType", this.PropertyType);

				sp_insert_HouseProperties.ExecuteNonQuery();
			}
			catch (Exception exc)
			{
				MessageBox.Show(exc.ToString());
			}
		}

		#endregion
	}
	#endregion � Class HouseProperties �
}
#endregion � Namespace DealMaker �