#region � Using �
using System;
using System.Data.SqlClient;
using System.Data;
using System.Windows.Forms;
#endregion � Using �
#region � Namespace DealMaker �
namespace DealMaker
{
	#region � Class Phone �
	/// <summary>
	/// Phone Class
	/// </summary>
	public class Phone : INHibernate, ISPL
	{
		#region � Data �
		public bool save = false;

		private Guid		idPhone = Guid.Empty;
		private string		phoneNr = string.Empty;
		private string		bestTimeToCall;
		#endregion � Data �
		#region � CTR �
		public Phone()
		{
			this.idPhone = Guid.NewGuid();
		}
		public Phone(string phoneNumber, string bestTimeToCall)
		{
			this.idPhone = Guid.NewGuid();
			this.phoneNr = phoneNumber;
			this.BestTimeToCall = bestTimeToCall;
		}
		#endregion � CTR �

		#region � Properties �
		#region � IdPhone �
		public Guid IdPhone
		{
			get {return this.idPhone;}
			set {this.idPhone = value;}
		}
		#endregion � IdPhone �
		#region � PhoneNr �
		public string PhoneNr
		{
			get {return this.phoneNr;}
			set {this.phoneNr = value;}
		}
		#endregion � PhoneNr �
		#region � BestTimeToCall �
		public string BestTimeToCall
		{
			get {return this.bestTimeToCall;}
			set	{this.bestTimeToCall = value;}
		}
		#endregion � BestTimeToCall �
		#endregion � Properties �

		#region INHibernate Members

		public void NhSave(NHibernate.ISession session)
		{
			session.Save(this);			
		}

		public void NhSaveOrUpdate (NHibernate.ISession session)
		{
			try
			{	
				if (true == this.save)							
					session.Save(this);					
				else
					session.SaveOrUpdate(this);
			}
			catch (Exception exc)
			{
				MessageBox.Show(exc.ToString());
			}
		}


		#endregion

		#region ISPL Members

		public void ExecuteSPL(SqlConnection dbConn)
		{	
			try
			{
				SqlCommand sp_insert_Phone = new SqlCommand("insert_Phone", dbConn);
				sp_insert_Phone.CommandType = CommandType.StoredProcedure;

				sp_insert_Phone.Parameters.Add("@IdPhone", this.IdPhone);

                if (this.PhoneNr.Length > 32)
                    this.PhoneNr = this.PhoneNr.Substring(0, 32);

				sp_insert_Phone.Parameters.Add("@PhoneNr", this.PhoneNr);

                if (this.BestTimeToCall.Length > 16)
                    this.BestTimeToCall = BestTimeToCall.Substring(0, 16);

				sp_insert_Phone.Parameters.Add("@BestTimeToCall", this.BestTimeToCall);
				sp_insert_Phone.ExecuteNonQuery();
			}
			catch (Exception exc)
			{
				MessageBox.Show(exc.ToString());
			}
		}

		#endregion
	}
	#endregion � Class Phone �
}
#endregion � Namespace DealMaker �