using System;
using System.Reflection;

namespace DealMaker.PlainClasses
{
	/// ihuk
	/// <summary>
	/// Represents a abstraction of PropertyInfo attrinute.
	/// </summary>
	/// <remarks>
	/// PropertyInfo class has some primitive members but most of its
	/// attributes are classes. Therefore it is natural to look at this
	/// agregated classes and their properties as attributes of PropertyInfo
	/// class.
	/// Also this class does <b>not</b> belong to DALC namespace since it
	/// doesn't do any DB access but since there are no namespaces deeper then
	/// Deal maker I'll leave it here for now :)
	/// </remarks>
	public class PropertyItemAttribute
	{
		public object owningObject;
		public string attributeName;
		public string attributeDescription;
		public PropertyInfo attributeProperty;

		/// <summary>
		/// Initializes a new instance of PropertyItemAttribute class.
		/// </summary>
		/// <param name="attributeName">Attribute name</param>
		/// <param name="attributeDescription">Attribute description</param>
		/// <param name="attributeProperty">Instance of <c>PropertyInfo</c> class</param>
		public PropertyItemAttribute(
			string attributeName,
			object owningObject,
			string attributeDescription,
			PropertyInfo attributeProperty)
		{
			this.attributeName = attributeName;
			this.owningObject = owningObject;
			this.attributeDescription = attributeDescription;
			this.attributeProperty = attributeProperty;
		}

		public override string ToString()
		{
			return AttributeDescription;
		}


		#region Properties
		public string AttributeName
		{
			get { return this.attributeName; }
		}

		public string AttributeDescription
		{
			get { return this.attributeDescription; }
		}

		public PropertyInfo AttributeProperty
		{
			get { return this.attributeProperty; }
		}

		public object OwningObject
		{
			get { return this.owningObject; }
		}

		public object AttributeValue
		{
			get
			{
                if (null != this.attributeProperty)
                    return this.attributeProperty.GetValue(this.owningObject, null);
                else
                    return null;
			}
		}
		#endregion

	}
}