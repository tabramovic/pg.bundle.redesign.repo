using System;
using System.Collections;
using NHibernate;
using DealMaker.Classes;

// ihuk, 05.12.2004
// Again, it would be good prtactice to have namespaces
// match directory structure, but there is only one namespace
// anyhow. To make things worse none of the classes from DALC
// subdirectory belongs there. 
// DALC stands for Data Access Logic Components. I see no data
// access classes found in DALC subdirectory. 
// So here goes...
namespace DealMaker.DALC
{

	/// <summary>
	/// Represent a class for creating instances of
	/// PropertyItem class 
	/// </summary>
	/// <remarks>
	/// This class follows the Singleton design pattern
	/// </remarks>
	public class  PropertyItemFactory
	{
		private static PropertyItemFactory instance = new PropertyItemFactory();

		private PropertyItemFactory()
		{
		}

		/// <summary>
		/// Returns a instance of PropertyItem class with
		/// the given id.
		/// </summary>
		/// <param name="item">Id of item to retrieve</param>
		/// <returns>Instance of PropertyItem class or <c>null</c></returns>
		public PropertyItem GetPropertyItem(Guid item)
		{
			try
			{
				ISession s = Globals.factory.OpenSession();

				return s.Load(typeof(PropertyItem), item) as PropertyItem;
			}
			catch
			{
				return null;
			}
		}

		/// <summary>
		/// Returns array of instances of PropertyItem classes
		/// </summary>
		/// <returns>Array of PropertyItem class instances</returns>
		public PropertyItem[] GetPropertyItems()
		{
			ArrayList items = new ArrayList();

			try
			{
				ISession s = Globals.factory.OpenSession();
				
				try
				{
					IList list;
					ICriteria c;

					c = s.CreateCriteria(typeof(PropertyItem));
					list = c.List();

					foreach (object o in list)
						items.Add(o as PropertyItem);
				}
				finally
				{
					s.Close();
				}
			}
			catch(System.Exception e)
			{
				System.Windows.Forms.MessageBox.Show(e.ToString());
			}

			return (PropertyItem[])items.ToArray(typeof(PropertyItem));
		}


		/// <summary>
		/// Returns array of PropertyItem objects assigned to 
		/// a group.
		/// </summary>
		/// <param name="groupId">Group id.</param>
		/// <returns>Array of PropertyItem objects</returns>
		/// <remarks>
		/// Note that Group id's are integers and not Guids
		/// </remarks>
		public PropertyItem[] GetPropertyItemsInGroup(int groupId)
		{
			Group g = GroupFactory.GetInstance().GetGroup(groupId);

			if (g == null)
				return null;
			else
				return g.PropertyItems;
		}

		/// <summary>
		/// Returns a instance of PropertyItemFactory.
		/// </summary>
		/// <returns>Instance of propertyItemFactory</returns>
		public static PropertyItemFactory GetInstance()
		{
			return instance;
		}
	}
}