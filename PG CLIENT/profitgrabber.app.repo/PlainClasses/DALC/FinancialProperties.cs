#region � Using �
using System;
using System.Data.SqlClient;
using System.Data;
using System.Windows.Forms;

#endregion � Using �
#region � Namespace DealMaker �
namespace DealMaker
{
	#region � Class FinancialProperties �
	/// <summary>
	/// Summary description for FinancialProperty.
	/// </summary>
	public class FinancialProperties : INHibernate, ISPL
	{
		#region � Data �
		private Guid	idFinancialProperty		=	Guid.Empty;
		private	string	status					=	string.Empty;
		private	string	parcelNumber			=	string.Empty;
		private	DateTime lastSaleDate;	
		private	double	lastSaleValue			=	0;
		private	string	saleDocumentNumber		=	string.Empty;
		private	double	costPerSquareFoot		=	0;
		private	double	firstLoan				=	0;
		private	double	secondLoan				=	0;
		private	string	firstLoanType			=	string.Empty;
		private	string	firstLoanInterestRate	=	string.Empty;
		private	string	lender					=	string.Empty;
		private	double	loans					=	0;
		private	double	equity					=	0;
		private	double	cashDownPayment			=	0;
		private string  deedDate				=	string.Empty;
		private string  bookAPN					=	string.Empty;
		private string  mapAPN					=   string.Empty;
		private string  parcelAPN				=	string.Empty;
		private string  parcelNumAPN			=	string.Empty;
		private double  estimatedHomeValue		=	0;  
		#endregion � Data �

		#region � Automatically Generated Properties �
		#region � IdFinancialProperty �
		public Guid IdFinancialProperty
		{
			get {return this.idFinancialProperty;}
			set {this.idFinancialProperty = value;}
		}
		#endregion � IdFinancialProperty �
		#region · Status ·
		[Description("Status")]
		public string Status
		{
			get
			{
				return this.status;
			}
			set
			{
				this.status = value;
			}
		}
		#endregion · Status ·
		#region · ParcelNumber ·
		[Description("Parcel Number")]
		public string ParcelNumber
		{
			get
			{
				if (this.parcelNumber != string.Empty)
					return this.parcelNumber;
				else
					return this.bookAPN + " " + this.mapAPN + " " + this.parcelAPN + " " + this.parcelNumAPN;
			}
			set
			{
				this.parcelNumber = value;
			}
		}
		#endregion · ParcelNumber ·
		#region · LastSaleDate; ·
		[Description("Last Sale Date")]
		public DateTime LastSaleDate
		{
			get
			{
				return this.lastSaleDate;
			}
			set
			{
				this.lastSaleDate = value;
			}
		}
		#endregion · LastSaleDate; ·
		#region · LastSaleValue ·
		[Description("Last Sale Value")]
		public double LastSaleValue
		{
		get
		{
		return this.lastSaleValue;
		}
		set
		{
		this.lastSaleValue = value;
		}
		}
		#endregion · LastSaleValue ·
		#region · SaleDocumentNumber ·
		[Description("Sale Document Number")]
		public string SaleDocumentNumber
		{
		get
		{
		return this.saleDocumentNumber;
		}
		set
		{
		this.saleDocumentNumber = value;
		}
		}
		#endregion · SaleDocumentNumber ·
		#region · CostPerSquareFoot ·
		[Description("Cost Per Square Foot")]
		public double CostPerSquareFoot
		{
		get
		{
		return this.costPerSquareFoot;
		}
		set
		{
		this.costPerSquareFoot = value;
		}
		}
		#endregion · CostPerSquareFoot ·
		#region · FirstLoan ·
		[Description("First Loan")]
		public double FirstLoan
		{
		get
		{
		return this.firstLoan;
		}
		set
		{
		this.firstLoan = value;
		}
		}
		#endregion · FirstLoan ·
		#region · SecondLoan ·
		[Description("Second Loan")]
		public double SecondLoan
		{
		get
		{
		return this.secondLoan;
		}
		set
		{
		this.secondLoan = value;
		}
		}
		#endregion · SecondLoan ·
		#region · FirstLoanType ·
		[Description("First Loan Type")]
		public string FirstLoanType
		{
		get
		{
		return this.firstLoanType;
		}
		set
		{
		this.firstLoanType = value;
		}
		}
		#endregion · FirstLoanType ·
		#region · FirstLoanInterestRate ·
		[Description("First Loan Interest Rate")]
		public string FirstLoanInterestRate
		{
		get
		{
		return this.firstLoanInterestRate;
		}
		set
		{
		this.firstLoanInterestRate = value;
		}
		}
		#endregion · FirstLoanInterestRate ·
		#region · Lender ·
		[Description("Lender")]
		public string Lender
		{
		get
		{
		return this.lender;
		}
		set
		{
		this.lender = value;
		}
		}
		#endregion · Lender ·
		#region · Loans ·
		[Description("Loans")]
		public double Loans
		{
		get
		{
		return this.loans;
		}
		set
		{
		this.loans = value;
		}
		}
		#endregion · Loans ·
		#region · Equity ·
		[Description("Equity")]
		public double Equity
		{
		get
		{
		return this.equity;
		}
		set
		{
		this.equity = value;
		}
		}
		#endregion · Equity ·
		#region · CashDownPayment ·
		[Description("Cash Down Payment")]
		public double CashDownPayment
		{
		get
		{
		return this.cashDownPayment;
		}
		set
		{
		this.cashDownPayment = value;
		}
		}
		#endregion · CashDownPayment ·
		#region � DeedDate �
		[Description("Deed Date")]
		public string DeedDate 
		{
			get { return this.deedDate; }
			set { this.deedDate = value; }
		}
		#endregion � DeedDate �
		#region � BookAPN �
		[Description("Book Number Part of APN")]
		public string BookAPN
		{
			get { return this.bookAPN; }
			set { this.bookAPN = value; }
		}
		#endregion � BookAPN �

		#region � MapAPN �
		[Description("Map Number Part of APN")]
		public string MapAPN
		{
			get { return this.bookAPN; }
			set { this.bookAPN = value; }
		}
		#endregion � MapAPN �

		#region � ParcelAPN �
		[Description("Parcel Number Part of APN")]
		public string ParcelAPN
		{
			get { return this.parcelAPN; }
			set { this.parcelAPN = value; }
		}
		#endregion � ParcelAPN �

		#region � ParcelNumAPN �
		[Description("Parcel Num Suffix Part of APN")]
		public string ParcelNumAPN
		{
			get { return this.parcelNumAPN; }
			set { this.parcelNumAPN = value; }
		}
		#endregion � ParcelNumAPN �

		#region � EstimatedHomeValue �
		[Description("Estimated Home Value")]
		public double EstimatedHomeValue
		{
			get { return this.estimatedHomeValue; }
			set { this.estimatedHomeValue = value; }
		}
		#endregion � EstimatedHomeValue �

		#endregion � Automatically Generated Properties �

		#region � CTR �
		public FinancialProperties()
		{
			this.idFinancialProperty = Guid.NewGuid();
		}
		#endregion � CTR �

		#region INHibernate Members

		public void NhSave(NHibernate.ISession session)
		{
			session.Save(this);
		}
		public void NhSaveOrUpdate (NHibernate.ISession session)
		{
			try
			{
			session.SaveOrUpdate(this);
			}
			catch (Exception exc)
			{
				MessageBox.Show(exc.ToString());
			}
		}


		#endregion

		#region ISPL Members

		public void ExecuteSPL(SqlConnection dbConn)
		{
			
			try
			{
				SqlCommand sp_insert_FinancialProperties = new SqlCommand("insert_FinancialProperties", dbConn);
				sp_insert_FinancialProperties.CommandType = CommandType.StoredProcedure;

				
				
				
				sp_insert_FinancialProperties.Parameters.Add("@IdFinancialProperty", this.IdFinancialProperty);
				sp_insert_FinancialProperties.Parameters.Add("@Status", this.Status);
				sp_insert_FinancialProperties.Parameters.Add("@ParcelNumber", this.ParcelNumber);
				if (this.LastSaleDate != new DateTime(1, 1, 1))
					sp_insert_FinancialProperties.Parameters.Add("@LastSaleDate", this.LastSaleDate);
				else
					sp_insert_FinancialProperties.Parameters.Add("@LastSaleDate", DBNull.Value);
				sp_insert_FinancialProperties.Parameters.Add("@LastSaleValue", this.LastSaleValue);
				sp_insert_FinancialProperties.Parameters.Add("@SaleDocumentNumber", this.SaleDocumentNumber);
				sp_insert_FinancialProperties.Parameters.Add("@CostPerSquareFoot", this.CostPerSquareFoot);
				sp_insert_FinancialProperties.Parameters.Add("@FirstLoan", this.FirstLoan);
				sp_insert_FinancialProperties.Parameters.Add("@SecondLoan", this.SecondLoan);
				sp_insert_FinancialProperties.Parameters.Add("@FirstLoanType", this.FirstLoanType);
				sp_insert_FinancialProperties.Parameters.Add("@FirstLoanInterestRate", this.FirstLoanInterestRate);
				sp_insert_FinancialProperties.Parameters.Add("@Lender", this.Lender);
				sp_insert_FinancialProperties.Parameters.Add("@Loans", this.Loans);
				sp_insert_FinancialProperties.Parameters.Add("@Equity", this.Equity);
				sp_insert_FinancialProperties.Parameters.Add("@CashDownPayment", this.CashDownPayment);				
				sp_insert_FinancialProperties.Parameters.Add("@DeedDate", this.DeedDate);
				sp_insert_FinancialProperties.Parameters.Add("@BookAPN", this.BookAPN);
				sp_insert_FinancialProperties.Parameters.Add("@MapAPN", this.MapAPN);
				sp_insert_FinancialProperties.Parameters.Add("@ParcelAPN", this.ParcelAPN);
				sp_insert_FinancialProperties.Parameters.Add("@ParcelNumAPN", this.ParcelNumAPN);								
				sp_insert_FinancialProperties.Parameters.Add("@EstimatedHomeValue", this.EstimatedHomeValue);			
				sp_insert_FinancialProperties.ExecuteNonQuery();
			}
			catch (Exception exc)
			{
				MessageBox.Show(exc.ToString());
			}
		}

		#endregion
	}
	#endregion � Class FinancialProperties �
}
#endregion � Namespace DealMaker �