using System;

namespace DealMaker
{
	/// <summary>
	/// Summary description for TaskTaskManager.
	/// </summary>
	public class Task : TaskGenerated, IComparable
	{
/*		bool _checked;
		public bool Checked
		{
			get
			{
				return _checked;
			}
			set
			{
				_checked = value;
			}
		}
*/
		string contactOrGroup;
		public String ContactOrGroup
		{
			get
			{
				return contactOrGroup;
			}
			set
			{
				contactOrGroup = value;
			}
		}

		string siteAddress;
		public String SiteAddress
		{
			get
			{
				return siteAddress;
			}
			set
			{
				siteAddress = value;
			}
		}

		public Task()
		{
			//
			// TODO: Add constructor logic here
			//
		}

        #region IComparable Members

        public int CompareTo(object obj)
        {
            if (obj is Task)
            {
                //sd - ORIG
                //return Convert.ToInt32(this.TaskDate.Subtract(((Task)obj).TaskDate).TotalDays);

                //ta:
                int prioLHS = this.Priority;
                int prioRHS = ((Task)obj).Priority;

                if (prioLHS == prioRHS)
                    return Convert.ToInt32(this.TaskDate.Subtract(((Task)obj).TaskDate).TotalDays);
                else
                    return prioLHS.CompareTo(prioRHS);
            }
            else
                return 0;
        }

        #endregion
    }
}
