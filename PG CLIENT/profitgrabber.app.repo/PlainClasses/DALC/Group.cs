using System;
using System.Collections;
using NHibernate;

// ihuk, 05.12.2004
// Standard namespace disclaimer :)
namespace DealMaker.Classes
{
	public class Group: INHibernate
	{
		int groupId;
		int parentGroupId;
		string name;
		string description;
		ArrayList propertyItems;

		public Group()
		{
		}

		public Group(int id, int parentGroupId, string name, string description)
		{
			this.groupId = id;
			this.parentGroupId = parentGroupId;
			this.parentGroupId = 0;
			this.name = name;
			this.description = description;
		}

		public void NhSave(NHibernate.ISession session)
		{
			session.Save(this);			
		}

		public void NhSaveOrUpdate (NHibernate.ISession session)
		{
			session.SaveOrUpdate(this);
		}

		#region Properties
		public int GroupId
		{
			get 
			{ 
				return this.groupId; 
			}
			set
			{
				// ihuk, 05.12.2004
				// Can I have read-only property and still use NHibernate?
				this.groupId = value;
			}
		}

		public int ParentGroupId
		{
			get 
			{ 
				return this.parentGroupId; 
			}
			set
			{
				this.parentGroupId = value;
			}
		}

		public string Name
		{
			get
			{
				return this.name;
			}
			set
			{
				this.name = value.Trim();
			}
		}

		public string Description
		{
			get
			{
				return this.description;
			}
			set
			{
				this.description = value.Trim();
			}
		}

		public IList PropertyItemsList
		{
			get 
			{
				return propertyItems;
			}
			set
			{
					this.propertyItems = ArrayList.Adapter(value);
			}
		}

		public PropertyItem[] PropertyItems
		{
			get 
			{
				return (PropertyItem[])propertyItems.ToArray(typeof(PropertyItem));
			}
		}

		#endregion
	}
}