#region � Using �
using System;
using System.Data.SqlClient;
using System.Data;
using System.Windows.Forms;
#endregion � Using �
#region � Namespace DealMaker �
namespace DealMaker
{
	#region � class TaxProperties �
	/// <summary>
	/// TaxProperty - implements tax properties
	/// </summary>
	public class TaxProperties : INHibernate, ISPL
	{
		#region � Data �
		private Guid    idTaxProperty		= Guid.Empty;
		private double  taxAssessedValue	= 0; 
		private string	fullCashValue		=	string.Empty;
		private	string	taxStatus			=	string.Empty;
		private	string	taxDeliquentYear	=	string.Empty;
		private	string	taxAmount			=	string.Empty;
		private	string	taxRateArea			=	string.Empty;
		private	string	taxExemption		=	string.Empty;
		private	string	percentImprovement	=	string.Empty;
		private	string	useCode				=	string.Empty;
		private	string	ownerAbsentOccupied	=	string.Empty;
		private	string	landValue			=	string.Empty;
		#endregion � Data �
		#region � Automatically Generated Properties �
		#region � IdTaxProperty �
		public Guid IdTaxProperty
		{
			get {return this.idTaxProperty;}
			set {this.idTaxProperty = value;}
		}
		#endregion � IdTaxProperty �
		#region � TaxAssessedValue �
		[Description("Tax Assessed Value")]
		public double TaxAssessedValue
		{
			get {return this.taxAssessedValue;}
			set {this.taxAssessedValue = value;}
		}
		#endregion � TaxAssessedValue �
		#region · FullCashValue ·
		[Description("Full Cash Value")]
		public string FullCashValue
		{
			get
			{
				return this.fullCashValue;
			}
			set
			{
				this.fullCashValue = value;
			}
		}
		#endregion · FullCashValue ·
		#region · TaxStatus ·
		[Description("Tax Status")]
		public string TaxStatus
		{
			get
			{
				return this.taxStatus;
			}
			set
			{
				this.taxStatus = value;
			}
		}
		#endregion · TaxStatus ·
		#region · TaxDeliquentYear ·
		[Description("Tax Deliquent Year")]
		public string TaxDeliquentYear
		{
			get
			{
				return this.taxDeliquentYear;
			}
			set
			{
				this.taxDeliquentYear = value;
			}
		}
		#endregion · TaxDeliquentYear ·
		#region · TaxAmount ·
		[Description("Tax Amount")]
		public string TaxAmount
		{
			get
			{
				return this.taxAmount;
			}
			set
			{
				this.taxAmount = value;
			}
		}
		#endregion · TaxAmount ·
		#region · TaxRateArea ·
		[Description("Tax Rate Area")]
		public string TaxRateArea
		{
			get
			{
				return this.taxRateArea;
			}
			set
			{
				this.taxRateArea = value;
			}
		}
		#endregion · TaxRateArea ·
		#region · TaxExemption ·
		[Description("Tax Exemption")]
		public string TaxExemption
		{
			get
			{
				return this.taxExemption;
			}
			set
			{
				this.taxExemption = value;
			}
		}
		#endregion · TaxExemption ·
		#region · PercentImprovement ·
		[Description("Percent Improvement")]
		public string PercentImprovement
		{
			get
			{
				return this.percentImprovement;
			}
			set
			{
				this.percentImprovement = value;
			}
		}
		#endregion · PercentImprovement ·
		#region · UseCode ·
		[Description("Use Code")]
		public string UseCode
		{
			get
			{
				return this.useCode;
			}
			set
			{
				this.useCode = value;
			}
		}
		#endregion · UseCode ·
		#region · OwnerAbsentOccupied ·
		[Description("Owner Absent Occupied")]
		public string OwnerAbsentOccupied
		{
			get
			{
				return this.ownerAbsentOccupied;
			}
			set
			{
				this.ownerAbsentOccupied = value;
			}
		}
		#endregion · OwnerAbsentOccupied ·
		#region · LandValue ·
		[Description("Land Value")]
		public string LandValue
		{
			get
			{
				return this.landValue;
			}
			set
			{
				this.landValue = value;
			}
		}
		#endregion · LandValue ·

		#endregion � Automatically Generated Properties �		
		#region � CTR �
		public TaxProperties()
		{
			this.idTaxProperty = Guid.NewGuid();
		}
		#endregion � CTR �

		#region INHibernate Members

		public void NhSave(NHibernate.ISession session)
		{
			session.Save(this);
		}

		public void NhSaveOrUpdate (NHibernate.ISession session)
		{
			try
			{
			session.SaveOrUpdate(this);
			}
			catch (Exception exc)
			{
				MessageBox.Show(exc.ToString());
			}
		}
		#endregion

		#region ISPL Members

		public void ExecuteSPL(SqlConnection dbConn)
		{	
			try
			{
				SqlCommand sp_insert_TaxProperties = new SqlCommand("insert_TaxProperties", dbConn);
				sp_insert_TaxProperties.CommandType = CommandType.StoredProcedure;

				sp_insert_TaxProperties.Parameters.Add("@IdTaxProperty", this.IdTaxProperty);
				sp_insert_TaxProperties.Parameters.Add("@TaxAssessedValue", this.TaxAssessedValue);
				sp_insert_TaxProperties.Parameters.Add("@FullCashValue", this.FullCashValue);
				sp_insert_TaxProperties.Parameters.Add("@TaxStatus", this.TaxStatus);
				sp_insert_TaxProperties.Parameters.Add("@TaxDeliquentYear", this.TaxDeliquentYear);
				sp_insert_TaxProperties.Parameters.Add("@TaxAmount", this.TaxAmount);
				sp_insert_TaxProperties.Parameters.Add("@TaxRateArea", this.TaxRateArea);
				sp_insert_TaxProperties.Parameters.Add("@TaxExemption", this.TaxExemption);
				sp_insert_TaxProperties.Parameters.Add("@PercentImprovement", this.PercentImprovement);
				sp_insert_TaxProperties.Parameters.Add("@UseCode", this.UseCode);
				sp_insert_TaxProperties.Parameters.Add("@OwnerAbsentOccupied", this.OwnerAbsentOccupied);
				sp_insert_TaxProperties.Parameters.Add("@LandValue", this.LandValue);
				sp_insert_TaxProperties.ExecuteNonQuery();
			}
			catch (Exception exc)
			{
				MessageBox.Show(exc.ToString());
			}
		}

		#endregion
	}
	#endregion � class TaxProperties �
}
#endregion � Namespace DealMaker �