#region � Using �
using System;
using System.Data;
using System.Windows.Forms;
using System.Data.SqlClient;

#endregion � Using �

#region � Namespace DealMaker � 
namespace DealMaker
{
	#region � Class Address �
	/// <summary>
	/// Address
	/// </summary>	
	public class Address : INHibernate, ISPL	
	{
		#region � Data �
		public bool save = false;

		private  Guid		idAddress						= Guid.Empty;
		/// <summary>Site Street Number</summary>				
		private string		siteStreetNumber				= string.Empty;
		/// <summary>Site Street PreDirectional</summary>
		private string		siteStreetPreDirectional		= string.Empty;
		/// <summary>Site Street Name</summary>
		private string		siteStreetName					= string.Empty;
		/// <summary>Site Street PostDirectional</summary>
		private string		siteStreetPostDirectional		= string.Empty;	
		/// <summary>Site Street Suffix</summary>
		private string		siteStreetSuffix				= string.Empty;
		/// <summary>Site Street Unit</summary>
		private string		siteStreetUnitNumber			= string.Empty;		
		/// <summary>Full Site Street Address</summary>
		private string		fullSiteStreetAddress			= string.Empty;
		/// <summary>Site City (Phoenix)</summary>
		private string		siteCity						= string.Empty;
		/// <summary>Site State (AZ for Arizona)</summary>
		private string		siteState						= string.Empty;
		/// <summary>Site City Site State - (Phoen�x / AZ)</summary>
		private string		siteCitySiteState				= string.Empty;
		/// <summary>Site City Site State Site ZIP (Phoen�x / AZ / 234327)</summary>
		private string		siteCitySiteStateSiteZIP		= string.Empty;
		/// <summary>Site ZIP</summary>
		private string		siteZIP							= string.Empty;		
		/// <summary>Site Country</summary>
		private string		siteCountry						= string.Empty;
		#endregion � Data �		
		#region � CTR �
		public Address()
		{
			this.idAddress = Guid.NewGuid();
		}

		public Address(Address addr)
		{
			this.idAddress = Guid.NewGuid();
			this.siteStreetNumber = addr.SiteStreetNumber;
			this.siteStreetPreDirectional = addr.SiteStreetPreDirectional;
			this.siteStreetName	= addr.SiteStreetName;
			this.siteStreetPostDirectional = addr.SiteStreetPostDirectional;
			this.siteStreetSuffix = addr.SiteStreetSuffix;
			this.siteStreetUnitNumber = addr.SiteStreetUnitNumber;
			this.fullSiteStreetAddress = addr.FullSiteStreetAddress;
			this.siteCity = addr.SiteCity;
			this.siteState = addr.SiteState;
			this.siteCitySiteState = addr.SiteCitySiteState;
			this.siteCitySiteStateSiteZIP = addr.SiteCitySiteStateSiteZIP;
			this.siteZIP = addr.SiteZIP;
			this.siteCountry = addr.SiteCountry;
		}
		#endregion � CTR �
		#region � Properties �
		public Guid  IdAddress
		{
			get {return this.idAddress;}
			set {this.idAddress = value;}
		}
		#region � SiteStreetNumber �
		/// <summary>Site Street Number</summary>				
		[Description("SiteStreet Number")]
		public string	SiteStreetNumber
		{
			get {return this.siteStreetNumber;}
			set {this.siteStreetNumber = value;}
		}
		#endregion � SiteStreetNumber �
		#region � SiteStreetPreDirectional �
		/// <summary>Site Street PreDirectional</summary>
		[Description("SiteStreet PreDirectional")]
		public string SiteStreetPreDirectional
		{
			get {return this.siteStreetPreDirectional;}
			set {this.siteStreetPreDirectional = value;}
		}
		#endregion � SiteStreetPreDirectional �
		#region � SiteStreetName �
		/// <summary>Site Street Name</summary>
		[Description("SiteStreet Name")]
		public string SiteStreetName
		{
			get {return this.siteStreetName;}
			set {this.siteStreetName = value;}
		}
		#endregion � SiteStreetName �
		#region � SiteStreetPostDirectional �
		/// <summary>Site Street PostDirectional</summary>
		[Description("SiteStreet PostDirectional")]
		public string SiteStreetPostDirectional
		{
			get {return this.siteStreetPostDirectional;}
			set {this.siteStreetPostDirectional = value;}
		}
		#endregion � SiteStreetPostDirectional �
		#region � SiteStreetSuffix �
		/// <summary>Site Street Suffix</summary>
		[Description("SiteStreet Suffix")]
		public string SiteStreetSuffix
		{
			get {return this.siteStreetSuffix;}
			set {this.siteStreetSuffix = value;}
		}
		#endregion � SiteSteeetSuffix �
		#region � SiteStreetUnitNumber �
		/// <summary>Site Street Unit</summary>
		[Description("SiteStreet Unit Number")]
		public string SiteStreetUnitNumber
		{
			get {return this.siteStreetUnitNumber;}
			set {this.siteStreetUnitNumber = value;}
		}
		#endregion � SiteStreetUnitNumber �
		#region � FullSiteStreetAddress �
		/// <summary>
		/// Full Site Street Address -->
		///		siteStreetPreDirectional + siteStreetName + siteStreetNumber + siteStreetPostDirectional;		
		/// </summary>
		[Description("Full SiteStreet Address")]
		public string FullSiteStreetAddress
		{			
			get 
			{
				if (string.Empty == this.fullSiteStreetAddress.Trim())
				{
					this.fullSiteStreetAddress =	
						this.siteStreetNumber + " " + 
						this.siteStreetPreDirectional + " " + 
						this.siteStreetName + " " + 						
						this.siteStreetPostDirectional + " " + 
						this.siteStreetSuffix + " " + 
						this.siteStreetUnitNumber;												
				}
				return this.fullSiteStreetAddress.Trim() ;
			}
			set {this.fullSiteStreetAddress = value;}
		}
		#endregion � FullSiteStreetAddress �		
		#region � SiteCity �
		/// <summary>Site City (Phoenix)</summary>
		[Description("SiteCity")]
		public string SiteCity
		{
			get {return this.siteCity;}
			set {this.siteCity = value;}
		}
		#endregion � SiteCity �
		#region � SiteState �
		/// <summary>Site State (AZ for Arizona)</summary>
		[Description("SiteState")]
		public string SiteState
		{
			get {return this.siteState;}
			set {this.siteState = value;}
		}
		#endregion � SiteState �
		#region � SiteCitySiteState �
		/// <summary>Site City Site State - (Phoen�x / AZ)</summary>
		[Description("SiteCity SiteState")]
		public string SiteCitySiteState
		{
			get 
			{
				if (string.Empty != this.siteCitySiteState)
					return this.siteCitySiteState;
				else
				{
					if (string.Empty != this.siteCity && string.Empty != this.siteState)
						return this.siteCity + ", " + this.siteState;
					else
					{
						if (string.Empty != this.siteCity)
							return this.siteCity;
						else
							return this.siteState;
					}

				}
			}
			set 
			{
				this.siteCitySiteState = value;
			}
		}
		#endregion � SiteCitySiteState �
		#region � SiteCitySiteStateSiteZIP �
		/// <summary>Site City Site State Site ZIP (Phoen�x / AZ / 234327)</summary>
		[Description("SiteCity SiteState SiteZIP")]
		public string SiteCitySiteStateSiteZIP
		{
			get 
			{
				if (string.Empty != this.siteCitySiteStateSiteZIP)
				{
					return this.siteCitySiteStateSiteZIP;
				}
				else
				{
					if (string.Empty != this.SiteZIP)
						return this.SiteCitySiteState + " " + this.SiteZIP;
					else
						return this.SiteCitySiteState;
				}
			}
			set {this.siteCitySiteStateSiteZIP = value;}
		}
		#endregion � SiteCitySiteStateSiteZIP �
		#region � SiteZIP �
		/// <summary>Site ZIP</summary>
		[Description("SiteZIP")]
		public string SiteZIP
		{
			get {return this.siteZIP;}
			set {this.siteZIP = value;}
		}
		#endregion � SiteZIP �
		#region � SiteCountry �
		/// <summary>Site County</summary>
		[Description("SiteCountry")]
		public string SiteCountry
		{
			get {return this.siteCountry;}
			set {this.siteCountry= value;}
		}
		#endregion � SiteCountry �
		#endregion � Properties �

		#region INHibernate Members

		public void NhSave(NHibernate.ISession session)
		{			
			session.Save(this);			
		}

		public void NhSaveOrUpdate (NHibernate.ISession session)
		{
			try
			{	
				if (true == this.save)							
					session.Save(this);					
				else
					session.SaveOrUpdate(this);
			}
			catch (Exception exc)
			{
				MessageBox.Show(exc.ToString());
			}
		}

		#endregion

		#region ISPL Members

		public void ExecuteSPL(System.Data.SqlClient.SqlConnection dbConn)
		{	
			try
			{
				SqlCommand sp_insert_Address = new SqlCommand("insert_Address", dbConn);
				sp_insert_Address.CommandType = CommandType.StoredProcedure;

				sp_insert_Address.Parameters.Add("@IdAddress", this.IdAddress);
				sp_insert_Address.Parameters.Add("@SiteStreetNumber", this.SiteStreetNumber);
				sp_insert_Address.Parameters.Add("@SiteStreetPreDirectional", this.SiteStreetPreDirectional);
				sp_insert_Address.Parameters.Add("@SiteStreetName", this.SiteStreetName);
				sp_insert_Address.Parameters.Add("@SiteStreetPostDirectional", this.SiteStreetPostDirectional);
				sp_insert_Address.Parameters.Add("@siteStreetSuffix", this.siteStreetSuffix);
				sp_insert_Address.Parameters.Add("@SiteStreetUnitNumber", this.SiteStreetUnitNumber);
				sp_insert_Address.Parameters.Add("@FullSiteStreetAddress", this.FullSiteStreetAddress);
				sp_insert_Address.Parameters.Add("@SiteCity", this.SiteCity);
				sp_insert_Address.Parameters.Add("@SiteState", this.SiteState);
				sp_insert_Address.Parameters.Add("@SiteCitySiteState", this.SiteCitySiteState);
				sp_insert_Address.Parameters.Add("@SiteCitySiteStateSiteZIP", this.SiteCitySiteStateSiteZIP);
				sp_insert_Address.Parameters.Add("@SiteZIP", this.SiteZIP);
				sp_insert_Address.Parameters.Add("@SiteCountry", this.SiteCountry);
			
				sp_insert_Address.ExecuteNonQuery();
			}
			catch (Exception exc)
			{
				MessageBox.Show(exc.ToString());
			}

		}

		#endregion
	}
	#endregion � Class Address �
}
#endregion � Namespace DealMaker �