﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DealMaker.PlainClasses.RealEFlowImport
{
    public static class RealEFlowExtensions
    {
        public static List<PropertyItem> ToPropertyItemList(this List<RealEFlowRecord> recordList)
        {
            return recordList.Select(record => record.ToPropertyItem()).ToList();
        }

        public static PropertyItem ToPropertyItem(this RealEFlowRecord record)
        {
            var pi = new PropertyItem
            {
                Owner = new Contact
                {
                    FirstName = record.FirstName,
                    LastName = record.LastName,

                    MailAddress = new Address
                    {
                        SiteStreetName = record.RecipientAddress,
                        SiteCity = record.RecipientCity,
                        SiteState = record.RecipientState,
                        SiteZIP = record.RecipientPostalCode
                    },

                    SiteAddress = new Address
                    {
                        SiteStreetName = record.PropertyAddress,
                        SiteCity = record.PropertyCity,
                        SiteState = record.PropertyState,
                        SiteZIP = record.PropertyPostalCode,
                        SiteCountry = record.County
                    },
                },

                DealProperty = new DealProperty
                {
                    WorthBySeller = record.MarketValue,
                    Baths = record.Baths,
                    Bedrooms = record.Beds,
                    Apn = record.ParcelNumber,
                    Sqft = record.SquareFootage,
                    YearBuilt = record.YearBuilt,
                    Type = record.PropertyType
                },


                DealInfo = new DealInfo
                {
                    FinancialProperty = new FinancialProperties()
                }
            };

            if (DateTime.TryParse(record.LastSalesDate, out var date))
                pi.DealInfo.FinancialProperty.LastSaleDate = date;

            if (double.TryParse(record.LastSalesPrice, out var price))
                pi.DealInfo.FinancialProperty.LastSaleValue = price;

            if (double.TryParse(record.TotalLoans, out var loans))
                pi.DealInfo.FinancialProperty.Loans = loans;
                        
            return pi;
        }
    }
}
