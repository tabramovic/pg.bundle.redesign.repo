﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace DealMaker.PlainClasses.RealEFlowImport
{
    public class RealEFlowRecord
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string RecipientAddress { get; set; }
        public string RecipientCity { get; set; }
        public string RecipientState { get; set; }
        public string RecipientPostalCode { get; set; }
        public string PropertyAddress { get; set; }
        public string PropertyCity { get; set; }
        public string PropertyState { get; set; }
        public string PropertyPostalCode { get; set; }
        
        //public string Value { get; set; }
        public string MarketValue { get; set; }
        
        public string Baths { get; set; }
        public string Beds { get; set; }
        public string ParcelNumber { get; set; }
        public string LastSalesDate { get; set; }
        public string LastSalesPrice { get; set; }
        
        //not present in PG model
        public string PricePerSqFt { get; set; }
        public string SquareFootage { get; set; }
        public string YearBuilt { get; set; }
        public string TotalLoans { get; set; }
        public string PropertyType { get; set; }
        public string County { get; set; }
        
        //not present in PG model
        public string LotSizeSqFt { get; set; }

        /// <summary>
        /// We need to check if mappings between the data provider and data consumer (PG) are still valid and warn user if they are not anymore.
        /// Import will place empty strings into the model in case there is no data in the recspective column.
        /// If in the .csv file there is no column at all that our model searches for, import will put null value into the record field        
        /// </summary>
        /// <returns>true if all properties can be found in exported .csv file, false otherwise</returns>
        public bool AreAllPropertiesAvailable()
        {
            foreach (string prop in GetReadProfilePropertyNames())
                try
                {
                    var value = this.GetType().GetProperty(prop).GetValue(this);
                    if (null == value)
                        return false;
                }
                catch
                {
                    return false;
                }

            return true;
        }

        public static List<string> GetReadProfilePropertyNames()
        {
            var propInfoArr = new RealEFlowRecord().GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance);
            return propInfoArr.Select(pi => pi.Name).ToList();
        }
    }
}
