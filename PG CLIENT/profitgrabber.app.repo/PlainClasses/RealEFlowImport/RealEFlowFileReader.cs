﻿using CsvHelper;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Threading.Tasks;

namespace DealMaker.PlainClasses.RealEFlowImport
{
    public class RealEFlowFileReader
    {
        private readonly string _pathToFile;

        public RealEFlowFileReader(string pathToFile)
        {
            this._pathToFile = pathToFile;
        }
          
        public async Task<List<RealEFlowRecord>> ReadAndMap()
        {
            var records = new List<RealEFlowRecord>();
            var readProfilePropertyNames = RealEFlowRecord.GetReadProfilePropertyNames();

            using (StreamReader sr = new StreamReader(_pathToFile))
            {
                using (var reader = new CsvReader(sr, CultureInfo.InvariantCulture))
                {
                    var readerAdvanced = await reader.ReadAsync();
                    if (!readerAdvanced)
                        return records;

                    readerAdvanced = reader.ReadHeader();

                    if (!readerAdvanced) 
                        return records;

                    while (await reader.ReadAsync())
                    {
                        var record = new RealEFlowRecord();
                        readProfilePropertyNames.ForEach(propName => {

                            string propValue;
                            try
                            {
                                propValue = reader.GetField(propName);
                            }
                            catch 
                            {
                                //if this happens, this means that format of exported file from data provider has changed.
                                //this will cause for AreAllPropertiesAvailable to return false and indicate model change.
                                propValue = null;
                            }

                            var propInfo = record.GetType().GetProperty(propName);
                            if (null != propInfo && propInfo.CanWrite)                            
                                propInfo.SetValue(record, propValue);                                                            
                        });

                        records.Add(record);
                    }
                }
            }

            return records;
        }        
    }
}
