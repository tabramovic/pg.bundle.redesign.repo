﻿using System.Text;
using System.IO;
using System.IO.Compression;
using System;
using System.Linq;

namespace DealMaker.PlainClasses.RealEFlowImport
{
    public static class CompressedFileHelper
    {
        public static bool ExtractFile(string fileName, out string extractedFileName, out string error)
        {
            error = extractedFileName = string.Empty;

            if (string.IsNullOrEmpty(fileName))
                return false;

            if (!File.Exists(fileName))
            {
                error = "File does not exist";
                return false;
            }
            
            var destinationPath = $@"{Path.GetDirectoryName(fileName)}\{Path.GetFileNameWithoutExtension(fileName)}";
            try
            {
                if (Directory.Exists(destinationPath))
                    Directory.Delete(destinationPath, true);

                ZipFile.ExtractToDirectory(fileName, destinationPath, Encoding.UTF8);

                extractedFileName = Directory.GetFiles(destinationPath).FirstOrDefault();                
                return true;
            }            
            catch(Exception e)
            {
                error = e.Message;
                throw;
            }
        }
    }
}

