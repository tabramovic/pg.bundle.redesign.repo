﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Net;
using System.Net.Mime;
using System.Collections.Specialized;
using System.Net.Mail;
using google.oauth.lib;

namespace DealMaker.PlainClasses.EmailSender
{
    public enum BlastMailTo
    {
        eOwner,
        eAgent,
    }

    public class EmailSenderClient
    {
        //TODO: Set into Config
        string url = "https://www.profitgrabber.com/EmailSenderService/api/sendrequests/upload";
        //string url = "http://localhost:54052/api/sendrequests/upload";

        const string DefaultSubjectLine = "Hello";
        const string DefaultBodyLine = "Hello";

        public delegate void SendRequestUploadedEventHandler(string to, string subject, string file, bool success);
        public event SendRequestUploadedEventHandler OnSendRequestUploaded;
        

        private Guid PGID { get; }
        private string PGKeyCode { get; }
        private string PDFPath { get; }
        private List<KeyValuePair<Guid, Tuple<string, string>>> EmailRecords { get; }
        private Dictionary<Guid, string> AddressDict { get; }

        private string DisplayName { get; }
        private string ReplyTo { get; }
        private string SubjectPrefix { get; }
        private bool AppendAddressToSubject { get; }
        private string Body { get; }

        private SMTPObject SMTPSettings { get; }
        private BlastMailTo BlastMailTo { get; }

        private List<FileInfo> filesFound { get; set; }

        private string Hash { get; }

        bool useOwnEmailAccount { get; }

        string StaticAttach { get; set; }

        public EmailSenderClient(string staticAttach, string pdfPath, List<KeyValuePair<Guid, Tuple<string, string>>> emailRecords, Dictionary<Guid, string> addressDict, string displayName, string replyTo, 
            string subjectPrefix, bool appendAddressToSubject, string body, SMTPObject smtpSettings, BlastMailTo blastMailTo)
        {
            StaticAttach = staticAttach;
            PDFPath = pdfPath;
            EmailRecords = emailRecords;
            AddressDict = addressDict;
            DisplayName = displayName;
            ReplyTo = replyTo;
            SubjectPrefix = subjectPrefix;
            AppendAddressToSubject = appendAddressToSubject;
            Body = body;
            SMTPSettings = smtpSettings;
            BlastMailTo = blastMailTo;
            PGID = Globals.UserRegistration.IDUser;
            PGKeyCode = Globals.RegistrationRegistration.KeyCode;

            filesFound = ReadFolder(pdfPath);

            useOwnEmailAccount = true;

            //if ("own" == (string)DealMaker.Properties.Settings.Default["BlindOffersEmailAccount"])
            //    useOwnEmailAccount = true;
            //else if ("pg" == (string)DealMaker.Properties.Settings.Default["BlindOffersEmailAccount"])
            //    useOwnEmailAccount = false;


            var dataAsString = $"{PGID}{PGKeyCode}{DisplayName}{ReplyTo}{Body}";
            Hash = Utils.Crypto.EncryptString(dataAsString);
            Hash = Uri.EscapeDataString(Hash);
        }

       

        List<FileInfo> ReadFolder(string pdfPath)
        {
            try {
                if (Directory.Exists(pdfPath))
                {
                    DirectoryInfo di = new DirectoryInfo(pdfPath);
                    var files = di.GetFiles();

                    return files.ToList();
                }
            }
            catch (Exception)
            {
                return null;
            }

            return null;
        }

        string GetFileNameForId(Guid fileId)
        {
            return filesFound?.Find(fi => fi.FullName.Contains(fileId.ToString()))?.Name;
        }

        public void ProcessRequests()
        {
            if (null == EmailRecords)
                return;


            EmailRecords.ForEach(kvp => {
            var documentSuffixId = kvp.Key;
            var ownerEmail = kvp.Value.Item1;
            var agentEmail = kvp.Value.Item2;

            var sendTo = (BlastMailTo.eAgent == BlastMailTo) ? agentEmail : ownerEmail;

            if (string.IsNullOrEmpty(sendTo))
                return;

            var documentName = GetFileNameForId(documentSuffixId);
            var file = null != documentName ? PDFPath + @"\" + documentName : null;

            try
            {
                string subject = string.Empty;

                var subjectSuffix = AddressDict.ContainsKey(documentSuffixId) ? AddressDict[documentSuffixId] : string.Empty;

                if (!string.IsNullOrEmpty(SubjectPrefix))
                    subject = AppendAddressToSubject ? $"{SubjectPrefix} - {subjectSuffix}" : SubjectPrefix;
                else
                    subject = AppendAddressToSubject ? $"{subjectSuffix}" : DefaultSubjectLine;

                var body = !string.IsNullOrEmpty(Body) ? Body : DefaultBodyLine;

                string pureDocumentName = null;
                if (null != file)
                {
                    var suffixIdx = documentName.IndexOf(documentSuffixId.ToString());
                    pureDocumentName = documentName.Substring(0, suffixIdx - 1);
                    var ext = Path.GetExtension(documentName);

                    pureDocumentName += ext;
                }
                
                if (useOwnEmailAccount)
                {
                    var from = new MailAddress(ReplyTo, DisplayName);
                    var to = new MailAddress(sendTo);

                    var mailMessage = new MailMessage(from, to);

                    mailMessage.ReplyToList.Add(ReplyTo);
                    mailMessage.Subject = subject;
                    mailMessage.Body = body;

                    if (!string.IsNullOrEmpty(StaticAttach))
                    {
                        file = StaticAttach;
                        pureDocumentName = Path.GetFileName(StaticAttach);
                    }

                    if (!string.IsNullOrEmpty(file))
                    {
                        var fileAsBytes = File.ReadAllBytes(file);
                        var attach2 = new Attachment(new MemoryStream(fileAsBytes), (string)pureDocumentName, ProjectUtils.GetMediaType(file));
                        mailMessage.Attachments.Add(attach2);

                        /*
                        MemoryStream ms;
                        using (FileStream fileStream = new FileStream(file, FileMode.Open, FileAccess.Read))
                        {
                            ms = new MemoryStream();
                            ms.SetLength(fileStream.Length);


                            fileStream.Read(ms.GetBuffer(), 0, (int)fileStream.Length);            
                        }

                            ContentType ct = null;
                            //mailMessage.Attachments.Add(new Attachment(file, MediaTypeNames.Application.Pdf));
                            var ext = Path.GetExtension(file);

                        switch (ext)
                            {
                                case ".pdf":
                                    ct = new ContentType(MediaTypeNames.Application.Pdf);
                                    break;

                                case ".jpg":
                                    ct = new ContentType(MediaTypeNames.Image.Jpeg);
                                    break;

                                case ".png":
                                    ct = new ContentType("image/png");
                                    break;

                                case ".xls":
                                    ct = new ContentType("application/vnd.ms-excel");
                                    break;

                                case ".xlsx":
                                    ct = new ContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
                                    break;

                                case ".doc":
                                    ct = new ContentType("application/msword");
                                    break;

                                case ".docx":
                                    ct = new ContentType("application/vnd.openxmlformats-officedocument.wordprocessingml.document");
                                    break;

                                default:
                                    ct = new ContentType(MediaTypeNames.Application.Pdf);
                                    break;
                            }

                        
                        var attach = new Attachment(ms, ct);
                        attach.ContentDisposition.FileName = pureDocumentName;

                        mailMessage.Attachments.Add(attach);
                            */
                    }

                    //mailMessage.IsBodyHtml = true;
                    EmailSenderWorker.Instance.mailMessagesToSend.Enqueue(mailMessage);

                    return;
                }
                    
                else
                {

                    var data = new NameValueCollection();
                    if (!string.IsNullOrEmpty(StaticAttach))
                        file = StaticAttach;

                    if (null != pureDocumentName)
                        data["AttachmentName"] = pureDocumentName;

                    if (null != StaticAttach)
                        data["AttachmentName"] = Path.GetFileName(StaticAttach);

                    if (string.IsNullOrEmpty(data["AttachmentName"]))
                        data["AttachmentName"] = string.Empty;

                    data["PGID"] = PGID.ToString();
                    data["From"] = !string.IsNullOrEmpty(SMTPSettings.AccountName) ? SMTPSettings.AccountName : SMTPSettings.EmailAddress;
                    data["To"] = sendTo;
                    data["Body"] = body;
                    data["PGKeyCode"] = PGKeyCode;
                    data["DisplayName"] = DisplayName;
                    data["ReplyTo"] = ReplyTo;
                    data["Subject"] = subject;

                    if (null != pureDocumentName)
                        data["OriginalFileName"] = documentName;

                    if (null != StaticAttach)
                        data["OriginalFileName"] = StaticAttach;

                    data["UseOwnAccount"] = useOwnEmailAccount.ToString();
                    data["Hash"] = Hash.ToString();

                    //if (null != pureDocumentName)                    
                    HttpUploadFile(url, file, "AttachmentData", "AttachmentData", data);

                    //UploadFilesToRemoteUrl(url, new string[] { file }, "AttachmentData", "AttachmentData", data);
                    //string responseInString = Encoding.UTF8.GetString(response);
                }
            }
            catch (WebException we)
            {
                //Add applog  (server unreachable)                  
                //throw;
                bool stopHere = true;
            }
            catch (Exception e)
            {
                //Add applog                    
                //throw;
                bool stopHere = true;
            }

            //System.Threading.Thread.Sleep(1000);
        });
    }

        //new
        public static string UploadFilesToRemoteUrl(string url, string[] files, string paramName, string contentType, NameValueCollection formFields = null)
        {
            string boundary = "----------------------------" + DateTime.Now.Ticks.ToString("x");

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.ContentType = "multipart/form-data; boundary=" +
                                    boundary;
            request.Method = "POST";
            request.KeepAlive = true;

            Stream memStream = new System.IO.MemoryStream();

            var boundarybytes = System.Text.Encoding.ASCII.GetBytes("\r\n--" +
                                                                    boundary + "\r\n");
            var endBoundaryBytes = System.Text.Encoding.ASCII.GetBytes("\r\n--" +
                                                                        boundary + "--");


            string formdataTemplate = "\r\n--" + boundary + "\r\nContent-Disposition: form-data; name=\"{0}\";\r\n\r\n{1}";

            if (formFields != null)
            {
                foreach (string key in formFields.Keys)
                {
                    string formitem = string.Format(formdataTemplate, key, formFields[key]);
                    byte[] formitembytes = System.Text.Encoding.UTF8.GetBytes(formitem);
                    memStream.Write(formitembytes, 0, formitembytes.Length);
                }
            }

            string headerTemplate = "Content-Disposition: form-data; name=\"{0}\"; filename=\"{1}\"\r\n" + "Content-Type: {2}\r\n\r\n";

            for (int i = 0; i < files.Length; i++)
            {
                memStream.Write(boundarybytes, 0, boundarybytes.Length);
                var header = string.Format(headerTemplate, "AttachmentData", files[i], contentType);
                var headerbytes = System.Text.Encoding.UTF8.GetBytes(header);

                memStream.Write(headerbytes, 0, headerbytes.Length);

                using (var fileStream = new FileStream(files[i], FileMode.Open, FileAccess.Read))
                {
                    var buffer = new byte[1024];
                    var bytesRead = 0;
                    while ((bytesRead = fileStream.Read(buffer, 0, buffer.Length)) != 0)
                    {
                        memStream.Write(buffer, 0, bytesRead);
                    }
                }
            }

            memStream.Write(endBoundaryBytes, 0, endBoundaryBytes.Length);
            request.ContentLength = memStream.Length;

            using (Stream requestStream = request.GetRequestStream())
            {
                memStream.Position = 0;
                byte[] tempBuffer = new byte[memStream.Length];
                memStream.Read(tempBuffer, 0, tempBuffer.Length);
                memStream.Close();
                requestStream.Write(tempBuffer, 0, tempBuffer.Length);
            }

            using (var response = request.GetResponse())
            {
                Stream stream2 = response.GetResponseStream();
                StreamReader reader2 = new StreamReader(stream2);
                return reader2.ReadToEnd();
            }
        }

        //old
        public void HttpUploadFile(string url, string file, string paramName, string contentType, NameValueCollection nvc)
        {
            if (string.IsNullOrEmpty(nvc["To"]) && null != OnSendRequestUploaded)
            {
                OnSendRequestUploaded("No email address provided", nvc["Subject"], nvc["AttachmentName"], false);
                return;
            }

            //log.Debug(string.Format("Uploading {0} to {1}", file, url));
            string boundary = "---------------------------" + DateTime.Now.Ticks.ToString("x");
            byte[] boundarybytes = System.Text.Encoding.ASCII.GetBytes("\r\n--" + boundary + "\r\n");

            HttpWebRequest wr = (HttpWebRequest)WebRequest.Create(url);
            wr.ContentType = "multipart/form-data; boundary=" + boundary;
            wr.Method = "POST";
            wr.KeepAlive = true;
            wr.Credentials = System.Net.CredentialCache.DefaultCredentials;

            Stream rs = wr.GetRequestStream();
            
            string formdataTemplate = "Content-Disposition: form-data; name=\"{0}\"\r\n\r\n{1}";
            foreach (string key in nvc.Keys)
            {
                rs.Write(boundarybytes, 0, boundarybytes.Length);
                string formitem = string.Format(formdataTemplate, key, nvc[key]);
                byte[] formitembytes = System.Text.Encoding.UTF8.GetBytes(formitem);
                rs.Write(formitembytes, 0, formitembytes.Length);
            }
            rs.Write(boundarybytes, 0, boundarybytes.Length);

            string headerTemplate = "Content-Disposition: form-data; name=\"{0}\"; filename=\"{1}\"\r\nContent-Type: {2}\r\n\r\n";
            string header = string.Format(headerTemplate, paramName, file, contentType);
            byte[] headerbytes = System.Text.Encoding.UTF8.GetBytes(header);
            rs.Write(headerbytes, 0, headerbytes.Length);

            if (null != file)
            {
                FileStream fileStream = new FileStream(file, FileMode.Open, FileAccess.Read);
                byte[] buffer = new byte[32768];
                int bytesRead = 0;
                while ((bytesRead = fileStream.Read(buffer, 0, buffer.Length)) != 0)
                {
                    rs.Write(buffer, 0, bytesRead);
                }
                fileStream.Close();
            }

            byte[] trailer = System.Text.Encoding.ASCII.GetBytes("\r\n--" + boundary + "--\r\n");
            rs.Write(trailer, 0, trailer.Length);
            rs.Close();

            using (var response = wr.GetResponse())
            {
                Stream stream2 = response.GetResponseStream();
                StreamReader reader2 = new StreamReader(stream2);
                var res = reader2.ReadToEnd();

                if (null != OnSendRequestUploaded)
                    OnSendRequestUploaded(nvc["To"], nvc["Subject"],  nvc["AttachmentName"], true);
            }


            //WebResponse wresp = null;
            //try
            //{
            //    wresp = wr.GetResponse();
            //    //Stream stream2 = wresp.GetResponseStream();
            //    //StreamReader reader2 = new StreamReader(stream2);
            //    //log.Debug(string.Format("File uploaded, server response is: {0}", reader2.ReadToEnd()));

            //    if (null != OnSendRequestUploaded)
            //        OnSendRequestUploaded(nvc["AttachmentName"], true);
            //}
            //catch (Exception ex)
            //{
            //    //log.Error("Error uploading file", ex);

            //    if (null != OnSendRequestUploaded)
            //        OnSendRequestUploaded(nvc["AttachmentName"], false);

            //    if (wresp != null)
            //    {
            //        wresp.Close();
            //        wresp = null;
            //    }
            //}
            //finally
            //{                
            //    wr = null;
            //    wresp = null;
            //}
        }
    }    
}
