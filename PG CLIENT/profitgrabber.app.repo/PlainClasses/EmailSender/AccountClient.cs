﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Text;

namespace DealMaker.PlainClasses.EmailSender
{
    public class AccountClient
    {
        //TODO: Set into Config
        string url = $"https://www.profitgrabber.com/EmailSenderService/api/accounts/setaccount";
        //string url = "http://localhost:54052/api/accounts/setaccount";

        string KeyCode { get; }
        Guid PGId { get; }
        string SMTP { get; }
        int PORT { get; }
        bool SSL { get; }
        string DisplayName { get; }
        string ReplyTo { get; }
        string AccountName { get;  }
        string EmailAddress { get; }
        string Password { get; }
        string Hash { get; }

        public AccountClient(string keyCode, Guid pgId, string smtp, int port, bool ssl, string displayName, string replyTo, string accountName, string emailAddress, string password)
        {
            KeyCode = keyCode;
            PGId = pgId;
            SMTP = smtp;
            PORT = port;
            SSL = ssl;
            DisplayName = displayName;
            ReplyTo = replyTo;
            AccountName = accountName;
            EmailAddress = emailAddress;
            Password = Utils.Crypto.EncryptString(password);

            var dataAsString = $"{KeyCode}{PGId.ToString()}{SMTP}{PORT.ToString()}{SSL.ToString()}{DisplayName}{ (!string.IsNullOrEmpty(ReplyTo) ? ReplyTo : EmailAddress)}{(!string.IsNullOrEmpty(AccountName) ? AccountName : EmailAddress)}{EmailAddress}";
            Hash = Utils.Crypto.EncryptString(dataAsString);            
        }

        public bool Upload()
        {
            //upgrade to .Net 4.5 and use HTTP response codes (OK, BadRequest)
            using (var wb = new WebClient())
            {
                try
                {
                    var data = new NameValueCollection();
                    data["KeyCode"] = KeyCode;
                    data["PGId"] = PGId.ToString();
                    data["SMTP"] = SMTP;
                    data["PORT"] = PORT.ToString();
                    data["SSL"] = SSL.ToString();
                    data["DisplayName"] = DisplayName;
                    data["ReplyTo"] = !string.IsNullOrEmpty(ReplyTo) ? ReplyTo : EmailAddress;
                    data["AccountName"] = !string.IsNullOrEmpty(AccountName) ? AccountName : EmailAddress;
                    data["EmailAddress"] = EmailAddress;
                    data["Password"] = Password;
                    data["Hash"] = Hash;

                    var response = wb.UploadValues(url, "POST", data);
                    string responseInString = Encoding.UTF8.GetString(response);

                    return true;
                }
                catch (WebException we)
                {
                    //Add applog  (server unreachable)                  
                    throw;
                }
                catch (Exception e)
                {
                    //Add applog                    
                    throw;
                }
            }
        }
    }
}
