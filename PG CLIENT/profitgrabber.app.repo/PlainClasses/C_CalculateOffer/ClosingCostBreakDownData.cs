﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace DealMaker.PlainClasses.CalculateOffer
{
    [Serializable]
    [XmlRoot("ClosingCostBreakDownData")]
    public class ClosingCostBreakDownData
    {
        decimal _escrowFeeValue = 0;
        decimal _titleInsuranceValue = 0;
        decimal _appraisalFeeValue = 0;
        decimal _creditReportValue = 0;
        decimal _recordingFeeValue = 0;
        decimal _hoaTransferFeeValue = 0;

        string others1Name = string.Empty;
        decimal others1Value = 0;
        string others2Name = string.Empty;
        decimal others2Value = 0;
        string others3Name = string.Empty;
        decimal others3Value = 0;
        string others4Name = string.Empty;
        decimal others4Value = 0;
        string others5Name = string.Empty;
        decimal others5Value = 0;
        string others6Name = string.Empty;
        decimal others6Value = 0;

        

        public ClosingCostBreakDownData()
        { }

        public decimal GetTotal()
        {
            decimal retVal = 0;
            retVal += _escrowFeeValue;
            retVal += _titleInsuranceValue;
            retVal += _appraisalFeeValue;
            retVal += _creditReportValue;
            retVal += _recordingFeeValue;
            retVal += _hoaTransferFeeValue;

            retVal += others1Value;
            retVal += others2Value;
            retVal += others3Value;
            retVal += others4Value;
            retVal += others5Value;
            retVal += others6Value;

            return retVal;
        }

        public decimal EscrowFeeValue
        {
            get { return _escrowFeeValue; }
            set { _escrowFeeValue = value; }
        }

        public decimal TitleInsuranceValue
        {
            get { return _titleInsuranceValue; }
            set { _titleInsuranceValue = value; }
        }

        public decimal AppraisalFeeValue
        {
            get { return _appraisalFeeValue; }
            set { _appraisalFeeValue = value; }
        }

        public decimal CreditReportValue
        {
            get { return _creditReportValue; }
            set { _creditReportValue = value; }
        }

        public decimal RecordingFeeValue
        {
            get { return _recordingFeeValue; }
            set { _recordingFeeValue = value; }
        }

        public decimal HoaTransferFeeValue
        {
            get { return _hoaTransferFeeValue; }
            set { _hoaTransferFeeValue = value; }
        }

        public string Others1Name
        {
            get { return others1Name; }
            set { others1Name = value; }
        }

        public decimal Others1Value
        {
            get { return others1Value; }
            set { others1Value = value; }
        }

        public string Others2Name
        {
            get { return others2Name; }
            set { others2Name = value; }
        }

        public decimal Others2Value
        {
            get { return others2Value; }
            set { others2Value = value; }
        }

        public string Others3Name
        {
            get { return others3Name; }
            set { others3Name = value; }
        }

        public decimal Others3Value
        {
            get { return others3Value; }
            set { others3Value = value; }
        }

        public string Others4Name
        {
            get { return others4Name; }
            set { others4Name = value; }
        }

        public decimal Others4Value
        {
            get { return others4Value; }
            set { others4Value = value; }
        }

        public string Others5Name
        {
            get { return others5Name; }
            set { others5Name = value; }
        }

        public decimal Others5Value
        {
            get { return others5Value; }
            set { others5Value = value; }
        }

        public string Others6Name
        {
            get { return others6Name; }
            set { others6Name = value; }
        }

        public decimal Others6Value
        {
            get { return others6Value; }
            set { others6Value = value; }
        }
    }
}
