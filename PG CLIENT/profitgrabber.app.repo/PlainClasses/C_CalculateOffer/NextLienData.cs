﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace DealMaker.PlainClasses.CalculateOffer
{
    [Serializable]
    [XmlRoot("NextLienData")]
    public class NextLienData
    {
        decimal _conservativePropertyValue = 0;
        decimal _conservativeRepairsValue = 0;
        decimal _asIsValue = 0;
        decimal _firstOfferMultiplierValue = 0;

        decimal _initialOfferSalesPriceValue = 0;

        decimal _initialRealEstateComissionPerc = 0;
        decimal _initialRealEstateComissionValue = 0;
        RealEstateCommisionBreakDownData _initialRealEstateCommisionBreakDown = null;

        decimal _initialSellerConcessionPerc = 0;
        decimal _initialSellerConcessionValue = 0;
        SellerConcessionBreakDownData _initialSellerConcessionBreakDown = null;

        decimal _initialSellerClosingCostPerc = 0;
        decimal _initialSellerClosingCostValue = 0;
        ClosingCostBreakDownData _initialSellerClosingCostBreakDown = null;
        ClosingCostBreakDownData _initialBuyerClosingCostBreakDown = null;

        decimal _initialOfferToFirstLender = 0;
        decimal _initialOfferToSecondLender = 0;
        decimal _initialOfferToThirdLender = 0;
        
        public NextLienData()
        { }


        public decimal ConservativePropertyValue
        {
            get { return _conservativePropertyValue; }
            set { _conservativePropertyValue = value; }
        }

        public decimal ConservativeRepairsValue
        {
            get { return _conservativeRepairsValue; }
            set { _conservativeRepairsValue = value; }
        }

        public decimal AsIsValue
        {
            get { return _asIsValue; }
            set { _asIsValue = value; }
        }

        public decimal FirstOfferMultiplierValue
        {
            get { return _firstOfferMultiplierValue; }
            set { _firstOfferMultiplierValue = value; }
        }

        public decimal InitialOfferSalesPriceValue
        {
            get { return _initialOfferSalesPriceValue; }
            set { _initialOfferSalesPriceValue = value; }
        }

        public decimal InitialRealEstateComissionPerc
        {
            get { return _initialRealEstateComissionPerc; }
            set { _initialRealEstateComissionPerc = value; }
        }

        public decimal InitialRealEstateComissionValue
        {
            get { return _initialRealEstateComissionValue; }
            set { _initialRealEstateComissionValue = value; }
        }

        public RealEstateCommisionBreakDownData InitialRealEstateCommisionBreakDown
        {
            get { return _initialRealEstateCommisionBreakDown; }
            set { _initialRealEstateCommisionBreakDown = value; }
        }

        public decimal InitialSellerConcessionPerc
        {
            get { return _initialSellerConcessionPerc; }
            set { _initialSellerConcessionPerc = value; }
        }

        public decimal InitialSellerConcessionValue
        {
            get { return _initialSellerConcessionValue; }
            set { _initialSellerConcessionValue = value; }
        }

        public SellerConcessionBreakDownData InitialSellerConcessionBreakDown
        {
            get { return _initialSellerConcessionBreakDown; }
            set { _initialSellerConcessionBreakDown = value; }
        }

        public decimal InitialSellerClosingCostPerc
        {
            get { return _initialSellerClosingCostPerc; }
            set { _initialSellerClosingCostPerc = value; }
        }

        public decimal InitialSellerClosingCostValue
        {
            get { return _initialSellerClosingCostValue; }
            set { _initialSellerClosingCostValue = value; }
        }

        public ClosingCostBreakDownData InitialSellerClosingCostBreakDown
        {
            get { return _initialSellerClosingCostBreakDown; }
            set { _initialSellerClosingCostBreakDown = value; }
        }

        public ClosingCostBreakDownData InitialBuyerClosingCostBreakDown
        {
            get { return _initialBuyerClosingCostBreakDown; }
            set { _initialBuyerClosingCostBreakDown = value; }
        }

        public decimal InitialOfferToFirstLender
        {
            get { return _initialOfferToFirstLender; }
            set { _initialOfferToFirstLender = value; }
        }

        public decimal InitialOfferToSecondLender
        {
            get { return _initialOfferToSecondLender; }
            set { _initialOfferToSecondLender = value; }
        }

        public decimal InitialOfferToThirdLender
        {
            get { return _initialOfferToThirdLender; }
            set { _initialOfferToThirdLender = value; }
        }
    }
}
