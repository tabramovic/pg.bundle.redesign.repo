﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace DealMaker.PlainClasses.CalculateOffer
{
    [Serializable]
    [XmlRoot("SellerConcessionBreakDownData")]
    public class SellerConcessionBreakDownData
    {
        decimal _loanOriginationFeeValue = 0;        
        decimal _loanDiscountFeeValue = 0;        
        decimal _total = 0;
        
        public SellerConcessionBreakDownData()
        { }

        public decimal LoanOriginationFeeValue
        {
            get { return _loanOriginationFeeValue; }
            set { _loanOriginationFeeValue = value; }
        }

        public decimal LoanDiscountFeeValue
        {
            get { return _loanDiscountFeeValue; }
            set { _loanDiscountFeeValue = value; }
        }

        public decimal Total
        {
            get { return _total; }
            set { _total = value; }
        }

    }
}
