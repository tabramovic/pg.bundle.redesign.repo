﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace DealMaker.PlainClasses.CalculateOffer
{
    [Serializable]
    [XmlRoot("RealEstateCommisionBreakDownData")]
    public class RealEstateCommisionBreakDownData
    {
        string _nameOfListingAgency = string.Empty;
        string _nameOfSellingAgency = string.Empty;

        decimal _listingAgencyValue = 0;
        decimal _sellingAgencyValue = 0;
        decimal _total = 0;
        
        public RealEstateCommisionBreakDownData()
        { }

        public string NameOfListingAgency
        {
            get { return _nameOfListingAgency; }
            set { _nameOfListingAgency = value; }
        }

        public string NameOfSellingAgency
        {
            get { return _nameOfSellingAgency; }
            set { _nameOfSellingAgency = value; }
        }

        public decimal ListingAgencyValue
        {
            get { return _listingAgencyValue; }
            set { _listingAgencyValue = value; }
        }

        public decimal SellingAgencyValue
        {
            get { return _sellingAgencyValue; }
            set { _sellingAgencyValue = value; }
        }

        public decimal Total
        {
            get { return _total; }
            set { _total = value; }
        }
    }
}
