﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace DealMaker.PlainClasses.CalculateOffer
{
    [Serializable]
    [XmlRoot("CalculateOfferData")]
    public class CalculateOfferData
    {
        FirstLienData _firstLienData = new FirstLienData();
        NextLienData[] _nextLienData = null;
        
        public CalculateOfferData()
        { }

        public FirstLienData FirstLienData
        {
            get { return _firstLienData; }
            set { _firstLienData = value; }
        }

        public NextLienData[] NextLienData
        {
            get { return _nextLienData; }
            set { _nextLienData = value; }
        }

        public void AddNextLien()
        {
            if (null == _nextLienData)
                _nextLienData = new NextLienData[1] { new NextLienData() };
            else
            {
                List<NextLienData> newList = new List<NextLienData>(_nextLienData);
                newList.Add(new NextLienData());

                _nextLienData = newList.ToArray();
            }
        }

        //firstLienCan't be removed, only cleaned
        public void RemoveLien(int idx)
        {
            if (0 == idx)
                _firstLienData = new FirstLienData();
            else
            {
                List<NextLienData> newList = new List<NextLienData>(_nextLienData);
                try
                {
                    newList.RemoveAt(idx - 1);
                }
                catch { }

                _nextLienData = newList.ToArray();
            }
        }
    }
}
