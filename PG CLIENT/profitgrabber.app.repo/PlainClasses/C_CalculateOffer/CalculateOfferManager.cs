﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

//Added
using DealMaker.ShortSale;
using DealMaker.ShortSale.Interfaces;
using DealMaker.ShortSale.Utils;
using DealMaker.PlainClasses.ShortSale.Adapters;

namespace DealMaker.PlainClasses.CalculateOffer
{
    public sealed class CalculateOfferManager
    {
        static readonly CalculateOfferManager _instance = new CalculateOfferManager();

        static CalculateOfferManager()
        { }

        CalculateOfferManager()
        { }

        public static CalculateOfferManager Instance
        {
            get { return _instance; }
        }
        
        CalculateOfferData _calculateOfferData = new CalculateOfferData();
        CO_DefaultValues _coDefaultValues = null;

        public void ResetObject()
        {
            _calculateOfferData = new CalculateOfferData();            
        }


        public int NumberOfLiens
        {
            get { return 1 + _calculateOfferData.NextLienData.Length; }
        }

        public CalculateOfferData P_CalculateOfferData
        {
            get { return _calculateOfferData; }
            set { _calculateOfferData = value; }
        }

        public CO_DefaultValues P_CO_DefaultValues
        {
            get { return _coDefaultValues; }
            set { _coDefaultValues = value; }
        }
                

        public void LoadData()
        {
            _calculateOfferData = DataManager.Instance.GetCurrentShortSaleDocument().CalcOfferData;                        
        }

        public void SaveData()
        {
            DataManager.Instance.GetCurrentShortSaleDocument().CalcOfferData = _calculateOfferData;            
        }
        

        public void LoadCODefaultValues()
        {
            _coDefaultValues = DataManager.Instance.GetCurrentShortSaleDocument().CO_defaultValues;                       
        }

        public void SaveCODefaultValues()
        {
            DataManager.Instance.GetCurrentShortSaleDocument().CO_defaultValues = _coDefaultValues;            
        }
    }
}
