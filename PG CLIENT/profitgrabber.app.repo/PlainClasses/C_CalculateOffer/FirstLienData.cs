﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace DealMaker.PlainClasses.CalculateOffer
{
    [Serializable]
    [XmlRoot("FirstLienData")]
    public class FirstLienData
    {        
        decimal _conservativePropertyValue = 0;
        decimal _conservativeRepairsValue = 0;
        decimal _asIsValue = 0;
        decimal _firstOfferMultiplierValue = 0;

        decimal _initialOfferSalesPriceValue = 0;

        decimal _initialRealEstateComissionPerc = 0;
        decimal _initialRealEstateComissionValue = 0;
        RealEstateCommisionBreakDownData _initialRealEstateCommisionBreakDown = null;

        decimal _initialSellerConcessionPerc = 0;
        decimal _initialSellerConcessionValue = 0;
        SellerConcessionBreakDownData _initialSellerConcessionBreakDown = null;

        decimal _initialSellerClosingCostPerc = 0;
        decimal _initialSellerClosingCostValue = 0;
        ClosingCostBreakDownData _initialSellerClosingCostBreakDown = null;        
        ClosingCostBreakDownData _initialBuyerClosingCostBreakDown = null;

        decimal _secondPayOff = 0;
        decimal _initialOfferToFirstLender = 0;        

        decimal _realisticPropertyValue = 0;
        decimal _realisticMultiplierValue = 0;
        decimal _realisticRepairsValue = 0;
        decimal _maximumOfferSalesPriceValue = 0;

        decimal _realisticRealEstateComissionPerc = 0;
        decimal _realisticRealEstateComissionValue = 0;
        RealEstateCommisionBreakDownData _realisticRealEstateCommisionBreakDown = null;
        decimal _realisticSellerConcessionPerc = 0;
        decimal _realisticSellerConcessionValue = 0;
        SellerConcessionBreakDownData _realisticSellerConcessionBreakDown = null;
        decimal _realisticSellerClosingCostPerc = 0;
        decimal _realisticSellerClosingCostValue = 0;
        ClosingCostBreakDownData _realisticSellerClosingCostBreakDown = null;
        ClosingCostBreakDownData _realisticBuyerClosingCostBreakDown = null;

        decimal _realisticSecondPayOff = 0;
        decimal _maximumOfferNetToFirstLender = 0;        
        
        public FirstLienData()
        { }
                              
        public decimal ConservativePropertyValue
        {
            get { return _conservativePropertyValue; }
            set { _conservativePropertyValue = value; }
        }

        public decimal ConservativeRepairsValue
        {
            get { return _conservativeRepairsValue; }
            set { _conservativeRepairsValue = value; }
        }

        public decimal AsIsValue
        {
            get { return _asIsValue; }
            set { _asIsValue = value; }
        }

        public decimal FirstOfferMultiplierValue
        {
            get { return _firstOfferMultiplierValue; }
            set { _firstOfferMultiplierValue = value; }
        }
        
        public decimal InitialOfferSalesPriceValue
        {
            get { return _initialOfferSalesPriceValue; }
            set { _initialOfferSalesPriceValue = value; }
        }

        public decimal InitialRealEstateComissionPerc
        {
            get { return _initialRealEstateComissionPerc; }
            set { _initialRealEstateComissionPerc = value; }
        }

        public decimal InitialRealEstateComissionValue
        {
            get { return _initialRealEstateComissionValue; }
            set { _initialRealEstateComissionValue = value; }
        }

        public RealEstateCommisionBreakDownData InitialRealEstateCommisionBreakDown
        {
            get { return _initialRealEstateCommisionBreakDown; }
            set { _initialRealEstateCommisionBreakDown = value; }
        }

        public decimal InitialSellerConcessionPerc
        {
            get { return _initialSellerConcessionPerc; }
            set { _initialSellerConcessionPerc = value; }
        }

        public decimal InitialSellerConcessionValue
        {
            get { return _initialSellerConcessionValue; }
            set { _initialSellerConcessionValue = value; }
        }

        public SellerConcessionBreakDownData InitialSellerConcessionBreakDown
        {
            get { return _initialSellerConcessionBreakDown; }
            set { _initialSellerConcessionBreakDown = value; }
        }

        public decimal InitialSellerClosingCostPerc
        {
            get { return _initialSellerClosingCostPerc; }
            set { _initialSellerClosingCostPerc = value; }
        }

        public decimal InitialSellerClosingCostValue
        {
            get { return _initialSellerClosingCostValue; }
            set { _initialSellerClosingCostValue = value; }
        }

        public ClosingCostBreakDownData InitialSellerClosingCostBreakDown
        {
            get { return _initialSellerClosingCostBreakDown; }
            set { _initialSellerClosingCostBreakDown = value; }
        }        

        public ClosingCostBreakDownData InitialBuyerClosingCostBreakDown
        {
            get { return _initialBuyerClosingCostBreakDown; }
            set { _initialBuyerClosingCostBreakDown = value; }
        }

        public decimal SecondPayOff
        {
            get { return _secondPayOff; }
            set { _secondPayOff = value; }
        }

        public decimal InitialOfferToFirstLender
        {
            get { return _initialOfferToFirstLender; }
            set { _initialOfferToFirstLender = value; }
        }

        public decimal RealisticPropertyValue
        {
            get { return _realisticPropertyValue; }
            set { _realisticPropertyValue = value; }
        }

        public decimal RealisticMultiplierValue
        {
            get { return _realisticMultiplierValue; }
            set { _realisticMultiplierValue = value; }
        }

        public decimal RealisticRepairsValue
        {
            get { return _realisticRepairsValue; }
            set { _realisticRepairsValue = value; }
        }

        public decimal MaximumOfferSalesPriceValue
        {
            get { return _maximumOfferSalesPriceValue; }
            set { _maximumOfferSalesPriceValue = value; }
        }

        public decimal RealisticRealEstateComissionPerc
        {
            get { return _realisticRealEstateComissionPerc; }
            set { _realisticRealEstateComissionPerc = value; }
        }

        public decimal RealisticRealEstateComissionValue
        {
            get { return _realisticRealEstateComissionValue; }
            set { _realisticRealEstateComissionValue = value; }
        }

        public RealEstateCommisionBreakDownData RealisticRealEstateCommisionBreakDown
        {
            get { return _realisticRealEstateCommisionBreakDown; }
            set { _realisticRealEstateCommisionBreakDown = value; }
        }

        public decimal RealisticSellerConcessionPerc
        {
            get { return _realisticSellerConcessionPerc; }
            set { _realisticSellerConcessionPerc = value; }
        }

        public decimal RealisticSellerConcessionValue
        {
            get { return _realisticSellerConcessionValue; }
            set { _realisticSellerConcessionValue = value; }
        }

        public SellerConcessionBreakDownData RealisticSellerConcessionBreakDown
        {
            get { return _realisticSellerConcessionBreakDown; }
            set { _realisticSellerConcessionBreakDown = value; }
        }

        public decimal RealisticSellerClosingCostPerc
        {
            get { return _realisticSellerClosingCostPerc; }
            set { _realisticSellerClosingCostPerc = value; }
        }

        public decimal RealisticSellerClosingCostValue
        {
            get { return _realisticSellerClosingCostValue; }
            set { _realisticSellerClosingCostValue = value; }
        }

        public ClosingCostBreakDownData RealisticSellerClosingCostBreakDown
        {
            get { return _realisticSellerClosingCostBreakDown; }
            set { _realisticSellerClosingCostBreakDown = value; }
        }

        public ClosingCostBreakDownData RealisticBuyerClosingCostBreakDown
        {
            get { return _realisticBuyerClosingCostBreakDown; }
            set { _realisticBuyerClosingCostBreakDown = value; }
        }

        public decimal RealisticSecondPayOff
        {
            get { return _realisticSecondPayOff; }
            set { _realisticSecondPayOff = value; }
        }
        
        public decimal MaximumOfferNetToFirstLender
        {
            get { return _maximumOfferNetToFirstLender; }
            set { _maximumOfferNetToFirstLender = value; }
        }
    }
}
