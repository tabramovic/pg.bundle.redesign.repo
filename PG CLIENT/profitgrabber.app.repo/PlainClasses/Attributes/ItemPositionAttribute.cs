#region � Using �
using System;
#endregion � Using �
#region � Namespace GetFormsReady �
namespace DealMaker
{
	#region � Class ItemPositionAttribute �
	/// <summary>
	/// Class that contains comments for RT
	/// </summary>
	[AttributeUsage(AttributeTargets.All)]	
	public class ItemPositionAttribute : Attribute
	{
		#region � Data �
		private string desc;
		#endregion � Data �		
		#region � Properties �
		#region � Desc �
		public string Desc 
		{ 
			get { return this.desc; }
		}
		#endregion � Desc �
		#endregion � Properties �
		#region � CTR �
		public ItemPositionAttribute(string desc) 
		{
			this.desc = desc;
		}
		#endregion � CTR �
	}
	#endregion � Class ItemPositionAttribute �
}
#endregion � Namespace GetFormsReady �