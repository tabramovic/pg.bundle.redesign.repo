#region � Using �
using System;
#endregion � Using �
#region � Namespace DealMaker �
namespace DealMaker
{
	#region � Class DescriptionAttribute �
	/// <summary>
	/// Class that contains comments for RT
	/// </summary>
	[AttributeUsage(AttributeTargets.All)]	
	public class DescriptionAttribute : Attribute
	{
		#region � Data �
		private string desc;
		#endregion � Data �		
		#region � Properties �
		#region � Desc �
		public string Desc 
		{ 
			get { return this.desc; }
		}
		#endregion � Desc �
		#endregion � Properties �
		#region � CTR �
		public DescriptionAttribute(string desc) 
		{
			this.desc = desc;
		}
		#endregion � CTR �
	}
	#endregion � Class DescriptionAttribute �
}
#endregion � Namespace DealMaker �