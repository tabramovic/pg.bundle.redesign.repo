using System;
using System.IO;
using System.Collections;
using System.Xml.Serialization;

using System.Data;
using System.Data.SqlClient;

using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;

//Added
using DealMaker.ShortSale;
using DealMaker.ShortSale.Utils;

namespace DealMaker.PlainClasses.ShortSale.Adapters
{
	/// <summary>
	/// Summary description for DataManager.
	/// </summary>
	public sealed class DataManager
	{
		#region SINGLETON
		static readonly DataManager _instance = new DataManager();
		
		static DataManager()
		{		
		}
		
		DataManager()
		{			
		}		

		public static DataManager Instance
		{
			get { return _instance;	}
		}
		#endregion

		public event DocumentCreatedEventHandler DocumentCreated;
		public event DocumentModifiedEventHandler DocumentModified;
        public event AssignedTaskToSSX AssignedTask;

		public delegate void DocumentCreatedEventHandler (DateTime time);
		public delegate void DocumentModifiedEventHandler (DateTime time);
        public delegate void AssignedTaskToSSX();

		const string DefaultInfoValuesFileName = "DefaultMergeValues.xml";

		ArrayList _documentsMarkedForGroupReport = new ArrayList();

		DefaultInfoValues _div = null;
		ShortSaleDocument _ssd = null;
        string _userAppFolder = string.Empty;

		string _documentName = string.Empty;

        //sasass - pg gata
        public Guid PropertyItemID {get; set;}
        public string PropertyAddress { get; set; }
        public string PropertyPlainAddress { get; set; }
        public string PropertyCityStateZIP { get; set; }
        public string HomeOwnerFullName { get; set; }
        public string PrimaryPhone { get; set; }
        public string SecondaryPhone { get; set; }
        public string Email { get; set; }
        public string Log { get; set; }

        public SqlConnection SQLConnection { get; set; }

        DateTime _lastSave = DateTime.MinValue;
        long _numberOfSaves = 0;

        public DateTime LastSave
        {
            get { return _lastSave; }
        }

        public bool NeedsSave
        {
            get { return (DateTime.MinValue == _lastSave) ? false : true; }
        }

        public string UserAppFolder
        {
            get { return _userAppFolder; }
            set { _userAppFolder = value; }
        }

        /// <summary>Has name</summary>				
        public bool IsObjectReadyToSave
        {
            get { return (string.Empty != _documentName) ? true : false; }
        }

        public string DocumentName
        {
            get { return _documentName; }
            set { _documentName = value; }
        }

        public string ShortDocumentName
        {
            get
            {
                try
                {
                    string[] temp = DocumentName.Split('\\');
                    int loc = temp[temp.Length - 1].IndexOf(".");
                    return "\\" + temp[temp.Length - 1].Substring(0, loc);
                }
                catch
                {
                    return string.Empty;
                }
            }
        }

        public void ResetObject()
        {
            CreateNewShortSaleDocument();
            this.PropertyItemID = Guid.Empty;
        }


		public void CreateNewShortSaleDocument()
		{
			_documentName = string.Empty;
			
            _ssd = new ShortSaleDocument();

            _ssd.CalcOfferData = new DealMaker.PlainClasses.CalculateOffer.CalculateOfferData();

            _ssd.DocumentId = Guid.NewGuid().ToString();
			_ssd.ShortSaleInfo = new ShortSaleDocumentShortSaleInfo();	
			_ssd.ContactInfo = new ShortSaleDocumentContactInfo();
            
            //ShortSaleDocumentShortSaleInfoLiensLien lien = CreateLien();
            //lien.LienID = 1;
            _ssd.ShortSaleInfo.Liens = new ShortSaleDocumentShortSaleInfoLiens(); //new ShortSaleDocumentShortSaleInfoLiens[1] { lien }; 
            _ssd.ShortSaleInfo.Liens.Lien = new ShortSaleDocumentShortSaleInfoLiensLien[0] {};
			
			_ssd.DocumentGenerationArchive = new ShortSaleDocumentDocumentGenerationArchive();
			_ssd.DocumentGenerationArchive.AgentInfo = new ShortSaleDocumentDocumentGenerationArchiveAgentInfo();
			_ssd.DocumentGenerationArchive.AgentInfo.AddressInfo = new ShortSaleDocumentDocumentGenerationArchiveAgentInfoAddressInfo();
			_ssd.DocumentGenerationArchive.AgentInfo.AddressInfo.SiteAddress = new ShortSaleDocumentDocumentGenerationArchiveAgentInfoAddressInfoSiteAddress();
			_ssd.DocumentGenerationArchive.AgentInfo.AddressInfo.SiteAddress.Address = new ShortSaleDocumentDocumentGenerationArchiveAgentInfoAddressInfoSiteAddressAddress();
			_ssd.DocumentGenerationArchive.BuyerInfo = new ShortSaleDocumentDocumentGenerationArchiveBuyerInfo();
			_ssd.DocumentGenerationArchive.BuyerInfo.AddressInfo = new ShortSaleDocumentDocumentGenerationArchiveBuyerInfoAddressInfo();
			_ssd.DocumentGenerationArchive.BuyerInfo.AddressInfo.SiteAddress = new ShortSaleDocumentDocumentGenerationArchiveBuyerInfoAddressInfoSiteAddress();			
			_ssd.DocumentGenerationArchive.BuyerInfo.AddressInfo.SiteAddress.Address = new ShortSaleDocumentDocumentGenerationArchiveBuyerInfoAddressInfoSiteAddressAddress();
			_ssd.DocumentGenerationArchive.OwnerInfo = new ShortSaleDocumentDocumentGenerationArchiveOwnerInfo();
			_ssd.DocumentGenerationArchive.OwnerInfo.AddressInfo = new ShortSaleDocumentDocumentGenerationArchiveOwnerInfoAddressInfo();						
			_ssd.DocumentGenerationArchive.OwnerInfo.AddressInfo.SiteAddress = new ShortSaleDocumentDocumentGenerationArchiveOwnerInfoAddressInfoSiteAddress();
			_ssd.DocumentGenerationArchive.OwnerInfo.AddressInfo.SiteAddress.Address = new ShortSaleDocumentDocumentGenerationArchiveOwnerInfoAddressInfoSiteAddressAddress();
			_ssd.DocumentGenerationArchive.OwnerInfo.AddressInfo.MailAddress = new ShortSaleDocumentDocumentGenerationArchiveOwnerInfoAddressInfoMailAddress();
			_ssd.DocumentGenerationArchive.OwnerInfo.AddressInfo.MailAddress.Address = new ShortSaleDocumentDocumentGenerationArchiveOwnerInfoAddressInfoMailAddressAddress();

            _numberOfSaves = 0;
            _lastSave = DateTime.MinValue;

			DateTime dt = DateTime.Now;
			_ssd.DocumentTracker = new ShortSaleDocumentDocumentTracker();
			_ssd.DocumentTracker.FirstEntered = dt.ToShortDateString() + ", " + dt.ToLongTimeString();
			OnNewDocumentCreated(dt);

		}

		public bool AddLienToExistingShortSaleDocument()
		{
            if (null == _ssd)
                CreateNewShortSaleDocument();


            ShortSaleDocumentShortSaleInfoLiensLien[] existingLiens = null;
            
            if (null != _ssd.ShortSaleInfo && null != _ssd.ShortSaleInfo.Liens && null != _ssd.ShortSaleInfo.Liens.Lien)
                existingLiens = _ssd.ShortSaleInfo.Liens.Lien;

            if (null != existingLiens && 3 == existingLiens.Length)
                return false;

            //if (null != existingLiens && 1 == existingLiens.Length && null == existingLiens[0].AccountNr
            //        && null == existingLiens[0].CompanyName && 1 == existingLiens[0].LienID
            //        && null == existingLiens[0].LogFilePath)
            //{                
            //    return false; //EMPTY FIRST LIEN
            //}

            ShortSaleDocumentShortSaleInfoLiensLien newLien = CreateLien();

            try { newLien.LienID = (byte)(_ssd.ShortSaleInfo.Liens.Lien.Length + 1); }
            catch { newLien.LienID = 1; }

            int existingLiensLength = null != existingLiens ? existingLiens.Length : 0;
            _ssd.ShortSaleInfo.Liens.Lien = new ShortSaleDocumentShortSaleInfoLiensLien[existingLiensLength + 1];

            for (int i = 0; i < existingLiensLength; i++)
				_ssd.ShortSaleInfo.Liens.Lien[i] = existingLiens[i];

            _ssd.ShortSaleInfo.Liens.Lien[existingLiensLength] = newLien;

            return true;
		}

		public void RemoveLienFromExistingShortSaleDocument(int lienIdx)
		{
            ShortSaleDocumentShortSaleInfoLiensLien[] existingLiens = _ssd.ShortSaleInfo.Liens.Lien;
            _ssd.ShortSaleInfo.Liens.Lien = new ShortSaleDocumentShortSaleInfoLiensLien[existingLiens.Length - 1];

			int newLienCnt = 0;
			for (int existingLienCnt = 0; existingLienCnt < existingLiens.Length; existingLienCnt++)
			{
				if (lienIdx != existingLienCnt)
				{
					_ssd.ShortSaleInfo.Liens.Lien[newLienCnt] = existingLiens[existingLienCnt];
					newLienCnt++;
				}				
			}
		}
		
		public ShortSaleDocument GetCurrentShortSaleDocument()
		{
			return _ssd;
		}

		public DefaultInfoValues GetDefaultInfoValues()
		{	
			string err;	
			if (null == _div)
				LoadDefaultInfoValues(out _div, out err);
					
			return _div;
		}
       
		public bool GetDefaultInfoValues(out DefaultInfoValues div, out string err)
		{
			if (null == _div)
				return DataManager.Instance.LoadDefaultInfoValues(out div, out err);
			else
			{
				div = _div;
				err = string.Empty;
				return true;
			}
		}

		public bool UpdateDefaultInfoValues(DefaultInfoValues div, out string error)
		{
			_div = div;
			return SaveDefaultInfoValues(div, out error);			
		}

		public bool AutoSaveShortSaleDocument(out string error)
		{			
			return SaveShortSaleDocument(Instance.GetCurrentShortSaleDocument(), _documentName, out error);
		}

		public bool SaveShortSaleDocument(ShortSaleDocument ssd, string documentName, out string error)
		{
            if (0 != _numberOfSaves && StringStore.AutoSave == documentName)
                _lastSave = DateTime.Now;

            _numberOfSaves++;


			bool returnValue = true;
			error = string.Empty;

            //we set it manually in ContactScreen
			//_documentName = documentName;

			DateTime dt = DateTime.Now;
			if (null == ssd.DocumentTracker)
				ssd.DocumentTracker = new ShortSaleDocumentDocumentTracker();
			
			ssd.DocumentTracker.LastModified = dt.ToShortDateString() + ", " + dt.ToLongTimeString();

            if (SQLConnection != null && PropertyItemID != Guid.Empty)
            {
                BinaryFormatter bf = new BinaryFormatter();

                MemoryStream ms = new MemoryStream();

                bf.Serialize(ms, ssd);

                System.Data.SqlClient.SqlCommand insertCommand =
                    new System.Data.SqlClient.SqlCommand(
                    "IF EXISTS (SELECT * FROM ShortSale WHERE IdPropertyItem = @IdPropertyItem)" +
                    "   UPDATE ShortSale SET ShortSaleDocument = @ShortSale, IncludeInSummaryReport = @IncludeInSummaryReport WHERE IdPropertyItem = @IdPropertyItem " +
                    "ELSE " +
                    "   Insert into ShortSale (IdPropertyItem, ShortSaleDocument, IncludeInSummaryReport) Values (@IdPropertyItem, @ShortSale, @IncludeInSummaryReport)"
                    , SQLConnection);

                insertCommand.Parameters.Add("IdPropertyItem", SqlDbType.UniqueIdentifier, 0).Value = PropertyItemID;
                insertCommand.Parameters.Add("ShortSale", SqlDbType.Image, 0).Value = ms.ToArray();

                if (ssd.DocumentTracker.MarkForGroupReport == true.ToString())
                    insertCommand.Parameters.Add("IncludeInSummaryReport", SqlDbType.Bit, 0).Value = 1;
                else
                    insertCommand.Parameters.Add("IncludeInSummaryReport", SqlDbType.Bit, 0).Value = 0;

                int res = insertCommand.ExecuteNonQuery();
            }            
            
			return returnValue;
		}

        public ShortSaleDocument[] GetDocumentsFromReader(SqlDataReader rdr)
        {
            ArrayList ssDocuments = new ArrayList();

            while (rdr.Read())
            {
                MemoryStream ms = new MemoryStream((byte[])rdr["ShortSaleDocument"]);

                BinaryFormatter bf = new BinaryFormatter();

                ms.Position = 0;

                object obj = null;

                try { obj = bf.Deserialize(ms); }
                catch (Exception exc)
                { 
                    obj = null; 
                }

                if (null != obj)
                {
                    ((ShortSaleDocument)obj).DocumentTracker.MarkForGroupReport = rdr["IncludeInSummaryReport"].ToString();                                        
                    ssDocuments.Add(obj);
                }
            }

            rdr.Close();

            return (ShortSaleDocument[])ssDocuments.ToArray(typeof(ShortSaleDocument));
        }

        //public ShortSaleDocument[] GetDocumentsMarkedForGroupReport()
        //{
        //    if (SQLConnection != null && PropertyItemID != null)
        //    {

        //        SqlCommand selcmd = new SqlCommand
        //             //("select ShortSaleDocument,IncludeInSummaryReport from ShortSale where IncludeInSummaryReport = 'True'"
        //             ("select ShortSaleDocument,IncludeInSummaryReport from ShortSale where IncludeInSummaryReport = 1"
        //              , SQLConnection);

        //        return GetDocumentsFromReader(selcmd.ExecuteReader());
        //    }
        //    else
        //    {
        //        return GroupFilesManager.Instance.GetShortSaleDocuments();
        //    }
        //}        

        public bool LoadShortSaleDocument(string documentName, out string error)
        {
            bool returnValue = true;
            error = string.Empty;
            _ssd = null;
            ShortSaleDocument[] ssds;

            if (SQLConnection != null && PropertyItemID != null)
            {
                SqlCommand selcmd = new SqlCommand
                    ("select ShortSaleDocument,IncludeInSummaryReport from ShortSale where IdPropertyItem = @IdPropertyItem"                    
                    , SQLConnection);
                selcmd.Parameters.Add("IdPropertyItem", SqlDbType.UniqueIdentifier, 0).Value = PropertyItemID;

                ssds = GetDocumentsFromReader(selcmd.ExecuteReader());

                if (ssds.Length > 0)
                {
                    _ssd = ssds[0];
                    if (null != _ssd)
                    {
                        CalculateOffer.CalculateOfferManager.Instance.P_CalculateOfferData = _ssd.CalcOfferData;
                        CalculateOffer.CalculateOfferManager.Instance.P_CO_DefaultValues = _ssd.CO_defaultValues;
                    }
                }
                else
                {
                    //create new
                    DataManager.Instance.CreateNewShortSaleDocument();
                }
            }
            
            return returnValue;
        }

        public ShortSaleDocument LoadShortSaleDocumentObject(string documentName, out bool returnValue, out string error)
        {
            returnValue = true;
            error = string.Empty;
            ShortSaleDocument ssd = null;

            StreamReader reader = null;
            try	// Deserialize
            {
                XmlSerializer ser = new XmlSerializer(typeof(ShortSaleDocument));
                reader = new StreamReader(documentName);
                ssd = (ShortSaleDocument)ser.Deserialize(reader);
                if (null == ssd)
                    returnValue = false;
            }
            catch (Exception ex)
            {
                returnValue = false;
                error = ex.Message;
            }
            finally
            {
                if (reader != null)
                    reader.Close();
                reader = null;
            }

            return ssd;
        }

		public bool SaveDefaultInfoValues(DefaultInfoValues div, out string error)
		{
			bool returnValue = true;
			error = string.Empty;
			
			StreamWriter writer = null;
			try  //Serialize the cards to a file
			{
                if (!Directory.Exists(UserAppFolder + @"\DefaultValues\"))
                    Directory.CreateDirectory(UserAppFolder + @"\DefaultValues\");

				XmlSerializer ser = new XmlSerializer(typeof(DefaultInfoValues));
				writer =  new StreamWriter(UserAppFolder+ @"\DefaultValues\" + DefaultInfoValuesFileName);
				ser.Serialize(writer, div);
				_div = div;
			} 
			catch (Exception ex)
			{
				returnValue = false;
				error = ex.Message;
			} 
			finally
			{
				if (writer != null) 
					writer.Close();
				writer = null;
			} 

			return returnValue;
		}
	
		public bool LoadDefaultInfoValues(out DefaultInfoValues div, out string error)
		{
			bool returnValue = true;
			StreamReader reader = null;

			div = null;
			error = string.Empty;

			try	// Deserialize
			{				
				XmlSerializer ser = new XmlSerializer(typeof(DefaultInfoValues));
				reader =  new StreamReader(UserAppFolder + @"\DefaultValues\" + DefaultInfoValuesFileName);
				div = (DefaultInfoValues)ser.Deserialize(reader);
				if ( null == div) 
					returnValue = false;
			}
			catch (Exception ex)
			{
				returnValue = false;
				error = ex.Message;
			} 
			finally
			{
				if (reader != null) 
					reader.Close();
				reader = null;
			} 

			return returnValue;
		}


		ShortSaleDocumentShortSaleInfoLiensLien CreateLien()
		{
            ShortSaleDocumentShortSaleInfoLiensLien lien = new ShortSaleDocumentShortSaleInfoLiensLien();            
            lien.LienStatus = new ShortSaleDocumentShortSaleInfoLiensLienLienStatus();
            lien.LienStatus.AuthorizationFaxedIn = new ShortSaleDocumentShortSaleInfoLiensLienLienStatusAuthorizationFaxedIn();
            lien.LienStatus.AuthorizationVerifiedReceived = new ShortSaleDocumentShortSaleInfoLiensLienLienStatusAuthorizationVerifiedReceived();
            lien.LienStatus.AllIntakeDocsFromSeller = new ShortSaleDocumentShortSaleInfoLiensLienLienStatusAllIntakeDocsFromSeller();
            lien.LienStatus.RequiredDocsScanned = new ShortSaleDocumentShortSaleInfoLiensLienLienStatusRequiredDocsScanned();
            lien.LienStatus.RequiredDocsRecorded = new ShortSaleDocumentShortSaleInfoLiensLienLienStatusRequiredDocsRecorded();
            lien.LienStatus.ReturnOriginalsToHomeOwner = new ShortSaleDocumentShortSaleInfoLiensLienLienStatusReturnOriginalsToHomeOwner();
            lien.LienStatus.AllInfoEnteredInProfitGrabber = new ShortSaleDocumentShortSaleInfoLiensLienLienStatusAllInfoEnteredInProfitGrabber();
            lien.LienStatus.PropertyProfileInProfitGrabber = new ShortSaleDocumentShortSaleInfoLiensLienLienStatusPropertyProfileInProfitGrabber();
            lien.LienStatus.PreliminaryTitleSearchOrdered = new ShortSaleDocumentShortSaleInfoLiensLienLienStatusPreliminaryTitleSearchOrdered();
            lien.LienStatus.PreliminaryTitleSearchReceived = new ShortSaleDocumentShortSaleInfoLiensLienLienStatusPreliminaryTitleSearchReceived();
            lien.LienStatus.AnyTitleIssues = new ShortSaleDocumentShortSaleInfoLiensLienLienStatusAnyTitleIssues();			
            lien.LienStatus.PayoffOrdered = new ShortSaleDocumentShortSaleInfoLiensLienLienStatusPayoffOrdered();
            lien.LienStatus.ReinstatementOrdered = new ShortSaleDocumentShortSaleInfoLiensLienLienStatusReinstatementOrdered();
            lien.LienStatus.PayoffAmount = new ShortSaleDocumentShortSaleInfoLiensLienLienStatusPayoffAmount();
            lien.LienStatus.ReinstatementAmount = new ShortSaleDocumentShortSaleInfoLiensLienLienStatusReinstatementAmount();
            lien.LienStatus.SSPacketComplete = new ShortSaleDocumentShortSaleInfoLiensLienLienStatusSSPacketComplete();
            lien.LienStatus.SSPacketReviewed = new ShortSaleDocumentShortSaleInfoLiensLienLienStatusSSPacketReviewed();
            lien.LienStatus.SSPacketSentToLienHolder = new ShortSaleDocumentShortSaleInfoLiensLienLienStatusSSPacketSentToLienHolder();
            lien.LienStatus.SSPacketVerifiedReceived = new ShortSaleDocumentShortSaleInfoLiensLienLienStatusSSPacketVerifiedReceived();
            lien.LienStatus.PrelimInspectionOrdered = new ShortSaleDocumentShortSaleInfoLiensLienLienStatusPrelimInspectionOrdered();
            lien.LienStatus.PrelimInspectionDate = new ShortSaleDocumentShortSaleInfoLiensLienLienStatusPrelimInspectionDate();
            lien.LienStatus.BPOPacketComplete = new ShortSaleDocumentShortSaleInfoLiensLienLienStatusBPOPacketComplete();
            lien.LienStatus.NegotiatorAssigned = new ShortSaleDocumentShortSaleInfoLiensLienLienStatusNegotiatorAssigned();
            lien.LienStatus.BPOAppraisalOrdered = new ShortSaleDocumentShortSaleInfoLiensLienLienStatusBPOAppraisalOrdered();
            lien.LienStatus.BPOAppraisalCompleted = new ShortSaleDocumentShortSaleInfoLiensLienLienStatusBPOAppraisalCompleted();
            lien.LienStatus.BPOAppraisalValue = new ShortSaleDocumentShortSaleInfoLiensLienLienStatusBPOAppraisalValue();
            lien.LienStatus.RepairListSentToNegotiator = new ShortSaleDocumentShortSaleInfoLiensLienLienStatusRepairListSentToNegotiator();
            lien.LienStatus.MarketDataSentToNegotiator = new ShortSaleDocumentShortSaleInfoLiensLienLienStatusMarketDataSentToNegotiator();
            lien.LienStatus.NoteHolderInvestorName = new ShortSaleDocumentShortSaleInfoLiensLienLienStatusNoteHolderInvestorName();
            lien.LienStatus.MinPercentOfBPOAccepted = new ShortSaleDocumentShortSaleInfoLiensLienLienStatusMinPercentOfBPOAccepted();
            lien.LienStatus.AcceptanceLetterReceived = new ShortSaleDocumentShortSaleInfoLiensLienLienStatusAcceptanceLetterReceived();
            lien.LienStatus.DeficiencyJudgmentWaived = new ShortSaleDocumentShortSaleInfoLiensLienLienStatusDeficiencyJudgmentWaived();
            lien.LienStatus.SellerAgreedToLetterTerms = new ShortSaleDocumentShortSaleInfoLiensLienLienStatusSellerAgreedToLetterTerms();

            lien.LienHolderContactInfo = new ShortSaleDocumentShortSaleInfoLiensLienLienHolderContactInfo();
            lien.LienHolderContactInfo.SetupDept = new ShortSaleDocumentShortSaleInfoLiensLienLienHolderContactInfoSetupDept();
            lien.LienHolderContactInfo.SetupManager = new ShortSaleDocumentShortSaleInfoLiensLienLienHolderContactInfoSetupManager();
            lien.LienHolderContactInfo.Negotiator = new ShortSaleDocumentShortSaleInfoLiensLienLienHolderContactInfoNegotiator();
            lien.LienHolderContactInfo.NegManager = new ShortSaleDocumentShortSaleInfoLiensLienLienHolderContactInfoNegManager();
            lien.LienHolderContactInfo.BPOAppraisal1 = new ShortSaleDocumentShortSaleInfoLiensLienLienHolderContactInfoBPOAppraisal1();
            lien.LienHolderContactInfo.BPOAppraisal2 = new ShortSaleDocumentShortSaleInfoLiensLienLienHolderContactInfoBPOAppraisal2();
            lien.LienHolderContactInfo.BankruptcyDeptField = new ShortSaleDocumentShortSaleInfoLiensLienLienHolderContactInfoBankruptcyDeptField();
            lien.LienHolderContactInfo.CustomerService = new ShortSaleDocumentShortSaleInfoLiensLienLienHolderContactInfoCustomerService();
            lien.LienHolderContactInfo.ForeClosureAttorney = new ShortSaleDocumentShortSaleInfoLiensLienLienHolderContactInfoForeClosureAttorney();
            
                                    
            lien.LienHolderContactInfo.Strategy = new ShortSaleDocumentShortSaleInfoLiensLienLienHolderContactInfoStrategy(); // new ShortSaleDocumentShortSaleInfoLienLienHolderContactInfoStrategy();            
            lien.LienHolderContactInfo.Financials = new ShortSaleDocumentShortSaleInfoLiensLienLienHolderContactInfoFinancials();// new ShortSaleDocumentShortSaleInfoLienLienHolderContactInfoFinancials();
            lien.LienHolderContactInfo.Financials.Offers = new ShortSaleDocumentShortSaleInfoLiensLienLienHolderContactInfoFinancialsOffers();// new ShortSaleDocumentShortSaleInfoLienLienHolderContactInfoFinancialsOffers();
            lien.LienHolderContactInfo.Financials.Offers.First = new ShortSaleDocumentShortSaleInfoLiensLienLienHolderContactInfoFinancialsOffersFirst();// new ShortSaleDocumentShortSaleInfoLienLienHolderContactInfoFinancialsOffersFirst();
            lien.LienHolderContactInfo.Financials.Offers.Second = new ShortSaleDocumentShortSaleInfoLiensLienLienHolderContactInfoFinancialsOffersSecond();// new ShortSaleDocumentShortSaleInfoLienLienHolderContactInfoFinancialsOffersSecond();
            lien.LienHolderContactInfo.Financials.Offers.Third = new ShortSaleDocumentShortSaleInfoLiensLienLienHolderContactInfoFinancialsOffersThird();// new ShortSaleDocumentShortSaleInfoLienLienHolderContactInfoFinancialsOffersThird();
            lien.LienHolderContactInfo.Financials.Offers.Fourth = new ShortSaleDocumentShortSaleInfoLiensLienLienHolderContactInfoFinancialsOffersFourth();// new ShortSaleDocumentShortSaleInfoLienLienHolderContactInfoFinancialsOffersFourth();
            lien.LienHolderContactInfo.Financials.Offers.Fifth = new ShortSaleDocumentShortSaleInfoLiensLienLienHolderContactInfoFinancialsOffersFifth();// new ShortSaleDocumentShortSaleInfoLienLienHolderContactInfoFinancialsOffersFifth();

			return lien;
		}

		

		public string GetSiteAddress()
		{
			string siteAddress = string.Empty;
			
			if (null != DataManager.Instance.GetCurrentShortSaleDocument().ContactInfo.SiteAddress && string.Empty != DataManager.Instance.GetCurrentShortSaleDocument().ContactInfo.SiteAddress)
				siteAddress += DataManager.Instance.GetCurrentShortSaleDocument().ContactInfo.SiteAddress + " ";									

			return siteAddress;
		}

		public string GetFullSiteAddress()
		{
			string siteAddress = string.Empty;
			
			if (null != DataManager.Instance.GetCurrentShortSaleDocument().ContactInfo.SiteAddress && string.Empty != DataManager.Instance.GetCurrentShortSaleDocument().ContactInfo.SiteAddress)
				siteAddress += DataManager.Instance.GetCurrentShortSaleDocument().ContactInfo.SiteAddress + " ";
			
			if (null != DataManager.Instance.GetCurrentShortSaleDocument().ContactInfo.SiteCity && string.Empty != DataManager.Instance.GetCurrentShortSaleDocument().ContactInfo.SiteCity)
				siteAddress += DataManager.Instance.GetCurrentShortSaleDocument().ContactInfo.SiteCity + " ";

			if (null != DataManager.Instance.GetCurrentShortSaleDocument().ContactInfo.SiteState && string.Empty != DataManager.Instance.GetCurrentShortSaleDocument().ContactInfo.SiteState)
				siteAddress += DataManager.Instance.GetCurrentShortSaleDocument().ContactInfo.SiteState;

			if (null != DataManager.Instance.GetCurrentShortSaleDocument().ContactInfo.SiteZIP && string.Empty != DataManager.Instance.GetCurrentShortSaleDocument().ContactInfo.SiteZIP)
			{
				if (null != DataManager.Instance.GetCurrentShortSaleDocument().ContactInfo.SiteState && string.Empty != DataManager.Instance.GetCurrentShortSaleDocument().ContactInfo.SiteState)
					siteAddress += ", ";
				
				siteAddress += DataManager.Instance.GetCurrentShortSaleDocument().ContactInfo.SiteZIP;
			}			

			return siteAddress;
		}

		public string GetCityStateZIPFromAddress()
		{
			string siteAddress = string.Empty;			
			
			if (null != DataManager.Instance.GetCurrentShortSaleDocument().ContactInfo.SiteCity && string.Empty != DataManager.Instance.GetCurrentShortSaleDocument().ContactInfo.SiteCity)
				siteAddress += DataManager.Instance.GetCurrentShortSaleDocument().ContactInfo.SiteCity + " ";

			if (null != DataManager.Instance.GetCurrentShortSaleDocument().ContactInfo.SiteState && string.Empty != DataManager.Instance.GetCurrentShortSaleDocument().ContactInfo.SiteState)
				siteAddress += DataManager.Instance.GetCurrentShortSaleDocument().ContactInfo.SiteState;

			if (null != DataManager.Instance.GetCurrentShortSaleDocument().ContactInfo.SiteZIP && string.Empty != DataManager.Instance.GetCurrentShortSaleDocument().ContactInfo.SiteZIP)
			{
				if (null != DataManager.Instance.GetCurrentShortSaleDocument().ContactInfo.SiteState && string.Empty != DataManager.Instance.GetCurrentShortSaleDocument().ContactInfo.SiteState)
					siteAddress += ", ";
				
				siteAddress += DataManager.Instance.GetCurrentShortSaleDocument().ContactInfo.SiteZIP;
			}			

			return siteAddress;
		}

        public void OnAssignedTask()
        {
            if (null != AssignedTask)
                AssignedTask();
        }


		void OnNewDocumentCreated(DateTime dt)
		{
			if (null != DocumentCreated)
				DocumentCreated(dt);
		}

		void OnDocumentModified(DateTime dt)
		{
			if (null != DocumentModified)
				DocumentModified(dt);
		}		
	}
}
