﻿using System;
using System.Collections;
using System.Text;
using System.Windows.Forms;

namespace DealMaker.ShortSale.Utils
{
    // Implements the manual sorting of items by columns.
    public class ListViewItemComparerDesc : IComparer
    {
        private int col;
        public ListViewItemComparerDesc()
        {
            col = 0;
        }
        public ListViewItemComparerDesc(int column)
        {
            col = column;
        }
        public int Compare(object x, object y)
        {
            return String.Compare(((ListViewItem)y).SubItems[col].Text, ((ListViewItem)x).SubItems[col].Text);
        }
    }
}
