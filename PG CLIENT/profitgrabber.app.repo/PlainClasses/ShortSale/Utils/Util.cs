﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace DealMaker.ShortSale.Utils
{
    public class Util
    {
        public static decimal GetDecimalValue(string textValue)
        {
            decimal val = 0;

            try { val = Convert.ToDecimal(textValue); }
			catch { val = 0; }

            return val;
        }

        public static string GetSuffix(int nr)
        {
            if (1 == nr)
                return "st";
            else if (2 == nr)
                return "nd";
            else if (3 == nr)
                return "rd";
            else
                return "th";
        }

        public static string[] GetPartialLinesFromString(Graphics gfx, Font font, int maxWidth, string stringToBreak)
        {
            if (null == stringToBreak)
                return new string[1] {string.Empty};

            ArrayList resultLists = new ArrayList();
            string tempLine = string.Empty;
            int i = stringToBreak.IndexOf(" ");
            while (-1 != i)
            {
                if ((int)(gfx.MeasureString(tempLine + stringToBreak.Substring(0, i), font).Width) < maxWidth)
                {
                    tempLine += stringToBreak.Substring(0, i + 1);
                    stringToBreak = stringToBreak.Substring(i + 1, stringToBreak.Length - (i + 1));
                }
                else
                {
                    resultLists.Add(tempLine.TrimEnd());
                    tempLine = string.Empty;
                }


                i = stringToBreak.IndexOf(" ");
            }

            tempLine += stringToBreak;
            resultLists.Add(tempLine);
            return (string[])resultLists.ToArray(typeof(string));
        }
       

        string GetShortenedText(Graphics g, string text, Font f, int width)
        {
            string conCatStr = string.Empty;

            foreach (char c in text)
            {
                conCatStr += c;
                if (g.MeasureString(conCatStr, f).Width >= width)
                    return conCatStr + "...";
            }

            return text;
        }

        static string StartUpPath = string.Empty;

        public static void StoreStartupPath(string val)
        {
            StartUpPath = val;
        }

        public static string GetStartupPath()
        {                        
            return StartUpPath;
        }
    }
}
