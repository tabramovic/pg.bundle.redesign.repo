using System;
using System.IO;

namespace DealMaker.ShortSale.Utils
{
    /// <summary>
    /// Summary description for OffsetData.
    /// </summary>
    public sealed class OffsetData
    {
        static readonly OffsetData _instance = new OffsetData();

        public OffsetData()
        { }

        static OffsetData()
        { }

        public static OffsetData Instance
        {
            get { return _instance; }
        }

        const string OffsetsFile = @"\SSXOffsets.xml";
        int _horizontalOffset = 0;
        int _verticalOffset = 0;
        string _userAppFolder = string.Empty;

        public string UserAppFolder
        {
            get { return _userAppFolder; }
            set { _userAppFolder = value; }
        }

        /// <summary>
        /// Note: Allowed values are between -10 and 10 !
        /// </summary>
        public int HorizontalOffset
        {
            get { return _horizontalOffset; }
            set
            {
                if (value >= -30 && value <= 30)
                    _horizontalOffset = value;
                else
                    throw new Exception("Offset Settings: Setting out of range");
            }
        }

        /// <summary>
        /// Note: Allowed values are between -10 and 10 !
        /// </summary>
        public int VerticalOffset
        {
            get { return _verticalOffset; }
            set
            {
                if (value >= -30 && value <= 30)
                    _verticalOffset = value;
                else
                    throw new Exception("Offset Settings: Setting out of range");
            }
        }

        public void StoreOffsets()
        {
            System.Xml.Serialization.XmlSerializer xmlser = new System.Xml.Serialization.XmlSerializer(typeof(OffsetData));
            System.IO.FileStream fs = new System.IO.FileStream(UserAppFolder + OffsetsFile, System.IO.FileMode.Create);
            xmlser.Serialize(fs, this);
            fs.Flush();
            fs.Close();
        }

        public void LoadOffsets()
        {
            System.IO.FileStream fs = null;
            OffsetData storredOffset = null;
            System.Xml.Serialization.XmlSerializer xmlser = new System.Xml.Serialization.XmlSerializer(typeof(OffsetData));
            try
            {
                if (Directory.Exists(UserAppFolder))
                {
                    if (File.Exists(UserAppFolder + OffsetsFile))
                    {
                        fs = new System.IO.FileStream(UserAppFolder + OffsetsFile, System.IO.FileMode.Open);
                        storredOffset = (OffsetData)xmlser.Deserialize(fs);

                        HorizontalOffset = storredOffset.HorizontalOffset;
                        VerticalOffset = storredOffset.VerticalOffset;
                    }
                }
            }
            catch (System.IO.FileNotFoundException)
            {
                // File does not exist! this is not a problem! Default values (0,0) will be used 
            }
            catch (Exception x)
            {
                throw new Exception("Opening Print Offset File" + "\n" + x.Message + "\n" + x.StackTrace);
            }
            finally
            {
                try
                {
                    if (null != fs)
                        fs.Close();
                }
                catch
                {
                    // file not opened !
                }
            }
        }
    }
}
