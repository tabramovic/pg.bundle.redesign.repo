using System;

namespace DealMaker.ShortSale.Utils
{
	/// <summary>
	/// Summary description for SelectedDocuments.
	/// </summary>
	public class SelectedDocuments
	{
		public bool _standardPacketChecklist = false;
		public bool _faxCoverAuthorization = false;
		public bool _authorizationToRelease = false;
		public bool _faxCoverGeneral = false;
		public bool _lienSpecificUserDocument1 = false;
		public bool _lienSpecificUserDocument2 = false;
		public bool _lienSpecificUserDocument3 = false;
        public bool _lienSpecificUserDocument4 = false;
        public bool _lienSpecificUserDocument5 = false;
		public bool _packetCoverLetter_Him = false;
		public bool _packetCoverLetter_Her = false;
		public bool _packetCoverLetter_They = false;				
		public bool _handshipLetter = false;

		public SelectedDocuments()
		{
			
		}

		public bool IsAnyDocumentSelected()
		{
			return _standardPacketChecklist ||
					_faxCoverAuthorization ||
					_authorizationToRelease  ||
					_faxCoverGeneral ||
					_lienSpecificUserDocument1 ||
					_lienSpecificUserDocument2 ||
					_lienSpecificUserDocument3 ||
                    _lienSpecificUserDocument4 ||
                    _lienSpecificUserDocument5 ||
					_packetCoverLetter_Him ||
					_packetCoverLetter_Her ||
					_packetCoverLetter_They ||	
					_handshipLetter;
		}

		public static string GetTabNameForDocument(DocumentSet ds)
		{
			switch (ds)
			{
				case (DocumentSet.StandardPacketChecklist):
					return "Standard Packet Checklist";
				case (DocumentSet.FaxCoverAuthorization):
					return "Fax Cover Authorization";
				case (DocumentSet.AuthorizationToRelease):
					return "Authorization To Release";
				case (DocumentSet.FaxCoverGeneral):
					return "Fax Cover General";
				case (DocumentSet.PacketCoverLetterHim):
					return "Packet Cover Letter Him";
				case (DocumentSet.PacketCoverLetterHer):
					return "Packet Cover Letter Her";
				case (DocumentSet.PacketCoverLetterThey):
					return "Packet Cover Letter They";				
				case (DocumentSet.UserDocument1):
					return "User Document 1";
				case (DocumentSet.UserDocument2):
					return "User Document 2";
				case (DocumentSet.UserDocument3):
					return "User Document 3";
                case (DocumentSet.UserDocument4):
                    return "User Document 4";
                case (DocumentSet.UserDocument5):
                    return "User Document 5";
				case (DocumentSet.HardshipLetter):
					return "Hardship Letter";
				case (DocumentSet.LienSpecificUserDocument1):
					return "User Document 1";
				case (DocumentSet.LienSpecificUserDocument2):
					return "User Document 2";
				case (DocumentSet.LienSpecificUserDocument3):
					return "User Document 3";
                case (DocumentSet.LienSpecificUserDocument4):
                    return "User Document 4";
                case (DocumentSet.LienSpecificUserDocument5):
                    return "User Document 5";		
			}

			return string.Empty;
		}
	}
}
