using System;

//Added
using DealMaker.PlainClasses.ShortSale.Adapters;
using DealMaker.ShortSale.UserInfo;

namespace DealMaker.ShortSale.Utils
{
	/// <summary>
	/// Summary description for MailMergeItem.
	/// </summary>
	public sealed class MailMergeItem
	{
		static readonly MailMergeItem _instance = new MailMergeItem();

		static MailMergeItem()
		{			
		}

		MailMergeItem()
		{
            UserInfoManager.Instance.LoadData();
		}

		public static MailMergeItem Instance
		{
			get { return _instance; }
		}

		public string GetDescriptor(string mergeItemName)
		{
			switch (mergeItemName)
			{				
				case "_ai_FirstName":
					return "(Agent First Name)";
				case "_ai_LastName":
					return "(Agent Last Name)";
				case "_ai_FullName":
					return "(Agent Full Name)";
				case "_ai_Address":
					return "(Agent Address)";
				case "_ai_Phone":
					return "(Agent Phone)";
				case "_ai_Fax":
					return "(Agent Fax)";
				case "_ai_Email":
					return "(Agent Email)";
				case "_bi_FirstName":
					return "(Buyer First Name)";
				case "_bi_LastName":
					return "(Buyer Last Name)";
				case "_bi_FullName":
					return "(Buyer Full Name)";
				case "_bi_Address":
					return "(Buyer Address)";
				case "_bi_Phone":
					return "(Buyer Phone)";
				case "_bi_Fax":
					return "(Buyer Fax)";
				case "_bi_Email":
					return "(Buyer Email)";
				case "_opi_FirstName":
					return "(Owner First Name)";
				case "_opi_MiddleName":
					return "(Owner Middle Name)";
				case "_opi_LastName":
					return "(Owner Last Name)";
				case "_opi_FullName":
					return "(Owner Full Name)";
				case "_opi_PropertyAddress":
					return "(Owner Property Address)";
				case "_opi_MailingAddress":
					return "(Owner Mailing Address)";
				case "_3_CompanyName":
					return "(3rd Lien Company Name)";
				case "_3_AccountNr":
					return "(3rd Lien Account Nr.)";
				case "_2_CompanyName":
					return "(2nd Lien Company Name)";
				case "_2_AccountNr":
					return "(2nd Lien Account Nr.)";
				case "_1_CompanyName":
					return "(1st Lien Company Name)";
				case "_1_AccountNr":
					return "(1st Lien Account Nr)";
				case "_ci_firstName":
					return "(Contact First Name)";
				case "_ci_middleName":
					return "(Contact Middle Name)";
				case "_ci_LastName":
					return "(Contact Last Name)";
				case "_ci_FullName":
					return "(Contact Full Name)";
				case "_ci_SiteAddress":
					return "(Contact Site Address)";
				case "_ci_SiteCity":
					return "(Contact Site City)";
				case "_ci_SiteState":
					return "(Contact Site State)";
				case "_ci_SiteZIP":
					return "(Contact Site ZIP)";	
                case "_currentDate":
                    return "(Current Date)";
                case "_currentTime":
                    return "(Current Time)";

                //COMPANY DATA
                case "_companyName":
                    return "(Company Name)";
                case "_companyAddress":
                    return "(Address)";
                case "_companyCity":
                    return "(City)";
                case "_companyState":
                    return "(State)";
                case "_companyZIP":
                    return "(ZIP)";
                case "_companyPhone":
                    return "(Phone)";
                case "_companyFax":
                    return "(Fax)";
                case "_companyEmail":
                    return "(Email)";

                //LIEN DATA
                case "_1_SetupDept_Name":
                    return "(1st SetupDept Name)";
                case "_1_SetupDept_Phone":
                    return "(1st SetupDept Phone)";
                case "_1_SetupDept_Fax":
                    return "(1st SetupDept Fax)";
                case "_1_SetupMgr_Name":
                    return "(1st SetupMgr Name)";
                case "_1_SetupMgr_Phone":
                    return "(1st SetupMgr Phone)";
                case "_1_SetupMgr_Fax":
                    return "(1st SetupMgr Fax)";
                case "_1_Negotiator_Name":
                    return "(1st Negotiator Name)";
                case "_1_Negotiator_Phone":
                    return "(1st Negotiator Phone)";
                case "_1_Negotiator_Fax":
                    return "(1st Negotiator Fax)";
                case "_1_NegMgr_Name":
                    return "(1st NegMgr Name)";
                case "_1_NegMgr_Phone":
                    return "(1st NegMgr Phone)";
                case "_1_NegMgr_Fax":
                    return "(1st NegMgr Fax)";
                case "_2_SetupDept_Name":
                    return "(2nd SetupDept Name)";
                case "_2_SetupDept_Phone":
                    return "(2nd SetupDept Phone)";
                case "_2_SetupDept_Fax":
                    return "(2nd SetupDept Fax)";
                case "_2_SetupMgr_Name":
                    return "(2nd SetupMgr Name)";
                case "_2_SetupMgr_Phone":
                    return "(2nd SetupMgr Phone)";
                case "_2_SetupMgr_Fax":
                    return "(2nd SetupMgr Fax)";
                case "_2_Negotiator_Name":
                    return "(2nd Negotiator Name)";
                case "_2_Negotiator_Phone":
                    return "(2nd Negotiator Phone)";
                case "_2_Negotiator_Fax":
                    return "(2nd Negotiator Fax)";
                case "_2_NegMgr_Name":
                    return "(2nd NegMgr Name)";
                case "_2_NegMgr_Phone":
                    return "(2nd NegMgr Phone)";
                case "_2_NegMgr_Fax":
                    return "(1st NegMgr Fax)";
                case "_3_SetupDept_Name":
                    return "(3rd SetupDept Name)";
                case "_3_SetupDept_Phone":
                    return "(3rd SetupDept Phone)";
                case "_3_SetupDept_Fax":
                    return "(3rd SetupDept Fax)";
                case "_3_SetupMgr_Name":
                    return "(3rd SetupMgr Name)";
                case "_3_SetupMgr_Phone":
                    return "(3rd SetupMgr Phone)";
                case "_3_SetupMgr_Fax":
                    return "(3rd SetupMgr Fax)";
                case "_3_Negotiator_Name":
                    return "(3rd Negotiator Name)";
                case "_3_Negotiator_Phone":
                    return "(3rd Negotiator Phone)";
                case "_3_Negotiator_Fax":
                    return "(3rd Negotiator Fax)";
                case "_3_NegMgr_Name":
                    return "(3rd NegMgr Name)";
                case "_3_NegMgr_Phone":
                    return "(3rd NegMgr Phone)";
                case "_3_NegMgr_Fax":
                    return "(3rd NegMgr Fax)";
                
			
				default: 
					return string.Empty;
			}
		}

		public string GetValue(string mergeItemName)
		{
            User user = ApplicationHelper.LoadUser();

			switch (mergeItemName)
			{
                case "_ai_FirstName":                    
                    try { return DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.AgentInfo.FirstName; }
                    catch { return string.Empty; }
				case "_ai_LastName":
					try { return DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.AgentInfo.LastName; }
                    catch { return string.Empty; }
				case "_ai_FullName":
					try { return DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.AgentInfo.FullName; }
                    catch { return string.Empty; }
				case "_ai_Address":
					try { return DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.AgentInfo.AddressInfo.SiteAddress.Address.ToString(); }
                    catch { return string.Empty; }
				case "_ai_Phone":
					try { return DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.AgentInfo.PhoneNr; }
                    catch { return string.Empty; }
				case "_ai_Fax":
					try { return DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.AgentInfo.FaxNr; }
                    catch { return string.Empty; }
				case "_ai_Email":
					try { return DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.AgentInfo.Email; }
                    catch { return string.Empty; }
				case "_bi_FirstName":
					try { return DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.BuyerInfo.FirstName; }
                    catch { return string.Empty; }
				case "_bi_LastName":
					try { return DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.BuyerInfo.LastName; }
                    catch { return string.Empty; }
				case "_bi_FullName":
					try { return DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.BuyerInfo.FullName; }
                    catch { return string.Empty; }
				case "_bi_Address":
					try { return DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.BuyerInfo.AddressInfo.SiteAddress.Address.ToString(); }
                    catch { return string.Empty; }
				case "_bi_Phone":
					try { return DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.BuyerInfo.PhoneNr; }
                    catch { return string.Empty; }
				case "_bi_Fax":
					try { return DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.BuyerInfo.FaxNr; }
                    catch { return string.Empty; }
				case "_bi_Email":
					try { return DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.BuyerInfo.Email; }
                    catch { return string.Empty; }
				case "_opi_FirstName":
					try { return DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.OwnerInfo.FirstName; }
                    catch { return string.Empty; }
				case "_opi_MiddleName":
					try { return DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.OwnerInfo.MiddleName; }
                    catch { return string.Empty; }
				case "_opi_LastName":
					try { return DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.OwnerInfo.LastName; }
                    catch { return string.Empty; }
				case "_opi_FullName":
					try { return DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.OwnerInfo.FullName; }
                    catch { return string.Empty; }
				case "_opi_PropertyAddress":
                    string retVal = string.Empty;
                    try { retVal = DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.OwnerInfo.AddressInfo.SiteAddress.Address.SiteStreetAddress; }
                    catch { retVal = string.Empty; }
                    return retVal;
				case "_opi_MailingAddress":
                    retVal = string.Empty;
                    try { retVal = DataManager.Instance.GetCurrentShortSaleDocument().DocumentGenerationArchive.OwnerInfo.AddressInfo.MailAddress.Address.SiteStreetAddress; }
                    catch { retVal = string.Empty; }
                    return retVal;
				case "_3_CompanyName":
					retVal = string.Empty;					
					try {retVal = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[2].CompanyName;}
					catch {retVal = string.Empty;}
					return retVal;
				case "_3_AccountNr":
                    try { retVal = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[2].AccountNr; }
					catch {retVal = string.Empty;}
					return retVal;
				case "_2_CompanyName":
                    try { retVal = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[1].CompanyName; }
					catch {retVal = string.Empty;}
					return retVal;
				case "_2_AccountNr":
                    try { retVal = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[1].AccountNr; }
					catch {retVal = string.Empty;}
					return retVal;
				case "_1_CompanyName":
                    try { retVal = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[0].CompanyName; }
					catch {retVal = string.Empty;}
					return retVal;
				case "_1_AccountNr":
                    try { retVal = DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[0].AccountNr; }
					catch {retVal = string.Empty;}
					return retVal;
				case "_ci_firstName":
					try { return DataManager.Instance.GetCurrentShortSaleDocument().ContactInfo.FirstName; }
                    catch { return string.Empty; }
				case "_ci_middleName":
					try { return DataManager.Instance.GetCurrentShortSaleDocument().ContactInfo.MiddleName; }
                    catch { return string.Empty; }
				case "_ci_LastName":
					try { return DataManager.Instance.GetCurrentShortSaleDocument().ContactInfo.LastName; }
                    catch { return string.Empty; }
				case "_ci_FullName":
					try { return DataManager.Instance.GetCurrentShortSaleDocument().ContactInfo.FullName; }
                    catch { return string.Empty; }
				case "_ci_SiteAddress":
					try { return DataManager.Instance.GetCurrentShortSaleDocument().ContactInfo.SiteAddress; }
                    catch { return string.Empty; }
				case "_ci_SiteCity":
					try { return DataManager.Instance.GetCurrentShortSaleDocument().ContactInfo.SiteCity; }
                    catch { return string.Empty; }
				case "_ci_SiteState":
					try { return DataManager.Instance.GetCurrentShortSaleDocument().ContactInfo.SiteState; }
                    catch { return string.Empty; }
				case "_ci_SiteZIP":
					try { return DataManager.Instance.GetCurrentShortSaleDocument().ContactInfo.SiteZIP; }
                    catch { return string.Empty; }
                case "_currentDate":
                    try { return DateTime.Now.ToShortDateString(); }
                    catch { return string.Empty; }
                case "_currentTime":
                    try { return DateTime.Now.ToShortTimeString(); }
                    catch { return string.Empty; }

                //COMPANY DATA
                case "_companyName":
                    try { return user.CompanyName; } // UserInfoManager.Instance.P_UserInfo.CompanyName;
                    catch { return string.Empty; }
                case "_companyAddress":
                    try { return user.Address; } // UserInfoManager.Instance.P_UserInfo.Address; }
                    catch { return string.Empty; }
                case "_companyCity":
                    try { return user.City; } // UserInfoManager.Instance.P_UserInfo.City; }
                    catch { return string.Empty; }
                case "_companyState":
                    try { return user.State; } // UserInfoManager.Instance.P_UserInfo.State; }
                    catch { return string.Empty; }
                case "_companyZIP":
                    try { return user.ZIP; } // UserInfoManager.Instance.P_UserInfo.ZIP; }
                    catch { return string.Empty; }
                case "_companyPhone":
                    try { return user.PhoneNr; } // UserInfoManager.Instance.P_UserInfo.Phone; }
                    catch { return string.Empty; }
                case "_companyFax":
                    try { return user.Fax; } // UserInfoManager.Instance.P_UserInfo.Fax; }
                    catch { return string.Empty; }
                case "_companyEmail":
                    try { return user.Email; } // UserInfoManager.Instance.P_UserInfo.Email; }
                    catch { return string.Empty; }

                //LIEN DATA
                case "_1_SetupDept_Name":
                    try { return DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[0].LienHolderContactInfo.SetupDept.ContactName; }
                    catch { return string.Empty; }
                case "_1_SetupDept_Phone":
                    try { return DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[0].LienHolderContactInfo.SetupDept.Phone; }
                    catch { return string.Empty; }
                case "_1_SetupDept_Fax":
                    try { return DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[0].LienHolderContactInfo.SetupDept.Fax; }
                    catch { return string.Empty; }
                case "_1_SetupMgr_Name":
                    try { return DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[0].LienHolderContactInfo.SetupManager.ContactName; }
                    catch { return string.Empty; }
                case "_1_SetupMgr_Phone":
                    try { return DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[0].LienHolderContactInfo.SetupManager.Phone; }
                    catch { return string.Empty; }
                case "_1_SetupMgr_Fax":
                    try { return DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[0].LienHolderContactInfo.SetupManager.Fax; }
                    catch { return string.Empty; }
                case "_1_Negotiator_Name":
                    try { return DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[0].LienHolderContactInfo.Negotiator.ContactName; }
                    catch { return string.Empty; }
                case "_1_Negotiator_Phone":
                    try { return DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[0].LienHolderContactInfo.Negotiator.Phone; }
                    catch { return string.Empty; }
                case "_1_Negotiator_Fax":
                    try { return DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[0].LienHolderContactInfo.Negotiator.Fax; }
                    catch { return string.Empty; }
                case "_1_NegMgr_Name":
                    try { return DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[0].LienHolderContactInfo.NegManager.ContactName; }
                    catch { return string.Empty; }
                case "_1_NegMgr_Phone":
                    try { return DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[0].LienHolderContactInfo.NegManager.Phone; }
                    catch { return string.Empty; }
                case "_1_NegMgr_Fax":
                    try { return DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[0].LienHolderContactInfo.NegManager.Fax; }
                    catch { return string.Empty; }
                case "_2_SetupDept_Name":
                    try { return DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[1].LienHolderContactInfo.SetupDept.ContactName; }
                    catch { return string.Empty; }
                case "_2_SetupDept_Phone":
                    try { return DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[1].LienHolderContactInfo.SetupDept.Phone; }
                    catch { return string.Empty; }
                case "_2_SetupDept_Fax":
                    try { return DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[1].LienHolderContactInfo.SetupDept.Fax; }
                    catch { return string.Empty; }
                case "_2_SetupMgr_Name":
                    try { return DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[1].LienHolderContactInfo.SetupManager.ContactName; }
                    catch { return string.Empty; }
                case "_2_SetupMgr_Phone":
                    try { return DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[1].LienHolderContactInfo.SetupManager.Phone; }
                    catch { return string.Empty; }
                case "_2_SetupMgr_Fax":
                    try { return DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[1].LienHolderContactInfo.SetupManager.Fax; }
                    catch { return string.Empty; }
                case "_2_Negotiator_Name":
                    try { return DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[1].LienHolderContactInfo.Negotiator.ContactName; }
                    catch { return string.Empty; }
                case "_2_Negotiator_Phone":
                    try { return DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[1].LienHolderContactInfo.Negotiator.Phone; }
                    catch { return string.Empty; }
                case "_2_Negotiator_Fax":
                    try { return DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[1].LienHolderContactInfo.Negotiator.Fax; }
                    catch { return string.Empty; }
                case "_2_NegMgr_Name":
                    try { return DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[1].LienHolderContactInfo.NegManager.ContactName; }
                    catch { return string.Empty; }
                case "_2_NegMgr_Phone":
                    try { return DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[1].LienHolderContactInfo.NegManager.Phone; }
                    catch { return string.Empty; }
                case "_2_NegMgr_Fax":
                    try { return DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[1].LienHolderContactInfo.NegManager.Fax; }
                    catch { return string.Empty; }
                case "_3_SetupDept_Name":
                    try { return DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[2].LienHolderContactInfo.SetupDept.ContactName; }
                    catch { return string.Empty; }
                case "_3_SetupDept_Phone":
                    try { return DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[2].LienHolderContactInfo.SetupDept.Phone; }
                    catch { return string.Empty; }
                case "_3_SetupDept_Fax":
                    try { return DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[2].LienHolderContactInfo.SetupDept.Fax; }
                    catch { return string.Empty; }
                case "_3_SetupMgr_Name":
                    try { return DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[2].LienHolderContactInfo.SetupManager.ContactName; }
                    catch { return string.Empty; }
                case "_3_SetupMgr_Phone":
                    try { return DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[2].LienHolderContactInfo.SetupManager.Phone; }
                    catch { return string.Empty; }
                case "_3_SetupMgr_Fax":
                    try { return DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[2].LienHolderContactInfo.SetupManager.Fax; }
                    catch { return string.Empty; }
                case "_3_Negotiator_Name":
                    try { return DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[2].LienHolderContactInfo.Negotiator.ContactName; }
                    catch { return string.Empty; }
                case "_3_Negotiator_Phone":
                    try { return DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[2].LienHolderContactInfo.Negotiator.Phone; }
                    catch { return string.Empty; }
                case "_3_Negotiator_Fax":
                    try { return DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[2].LienHolderContactInfo.Negotiator.Fax; }
                    catch { return string.Empty; }
                case "_3_NegMgr_Name":
                    try { return DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[2].LienHolderContactInfo.NegManager.ContactName; }
                    catch { return string.Empty; }
                case "_3_NegMgr_Phone":
                    try { return DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[2].LienHolderContactInfo.NegManager.Phone; }
                    catch { return string.Empty; }
                case "_3_NegMgr_Fax":
                    try { return DataManager.Instance.GetCurrentShortSaleDocument().ShortSaleInfo.Liens.Lien[2].LienHolderContactInfo.NegManager.Fax; }
                    catch { return string.Empty; }
                
			
				default: 
					return string.Empty;
			}
		}        
	}
}
