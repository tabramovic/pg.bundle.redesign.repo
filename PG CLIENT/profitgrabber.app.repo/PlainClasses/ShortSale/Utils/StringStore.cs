using System;

namespace DealMaker.ShortSale.Utils
{
	/// <summary>
	/// Summary description for StringStore.
	/// </summary>
	public class StringStore
	{
        public const string AppName = "Short Sale Xcelerator Pro";
        public const string AutoSave = "Autosave";
        public const string SavedExtDocumentsFolder = "ExtData";
        public const string HUDSubFolder = @"\HUD\";
        public const string CreateNewFileFirst = "Create a New File First!";

        public static string[] PredefinedFolders = new string[] { "1st Mortgage", "2nd Mortgage", "3rd Mortgage", "Accounting", "BPO-Appriasal", "LegalDocs", "Pictures", "Property Information", "Re-Sale Info", "Seller Financials" };

		public StringStore()
		{
		
		}


	}
}
