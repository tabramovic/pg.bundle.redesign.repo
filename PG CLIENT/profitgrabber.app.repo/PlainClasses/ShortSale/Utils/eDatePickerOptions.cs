using System;

namespace DealMaker.ShortSale.Utils
{
	/// <summary>
	/// Summary description for eDatePickerOptions.
	/// </summary>
	public enum eDatePickerOptions
	{
		Select = 0, 
		KeepCurrent = 1,
		Clear = 2,
		NotSet = 3,
	}
}
