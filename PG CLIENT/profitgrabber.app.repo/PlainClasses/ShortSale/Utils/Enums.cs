using System;

namespace DealMaker.ShortSale.Utils
{
	/// <summary>
	/// Summary description for Enums.
	/// </summary>
	
	public enum DocumentSet
	{
		StandardPacketChecklist = 1, 
		FaxCoverAuthorization = 2,
		AuthorizationToRelease = 4,
		FaxCoverGeneral = 8,
		LienSpecificUserDocument1 = 16,
		LienSpecificUserDocument2 = 32,
		LienSpecificUserDocument3 = 64,
        LienSpecificUserDocument4 = 128,
        LienSpecificUserDocument5 = 256,
		PacketCoverLetterHim = 512,
		PacketCoverLetterHer = 1024,
		PacketCoverLetterThey = 2048,		
		UserDocument1 = 4096,
		UserDocument2 = 8192,
		UserDocument3 = 16384,
        UserDocument4 = 32768,
        UserDocument5 = 65536,				
		HardshipLetter = 131072,
	}

	public enum LienSet
	{
		FirstLienSet = 1,
		SecondLienSet = 2,
		ThirdLienSet = 3,
		Generic = 4,
		NotSelected = 5,
	}

    public enum ActivationStatus
    {
        ActiveInRefundPeriod = 1,
        ActivePastRefundPeriod = 2,
        Disabled = 4,        
        GraceTryExpired = 8,
        UserCorrupted = 16,
        NotSet = 32,
        GraceTry1 = 64,
        GraceTry2 = 128,
        GraceTry3 = 256,
        GraceTry4 = 512,
        GraceTry5 = 1024,
    }
}
