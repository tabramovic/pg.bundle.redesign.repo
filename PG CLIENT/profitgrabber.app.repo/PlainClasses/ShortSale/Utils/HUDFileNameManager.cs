﻿using System;
//using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Collections;

namespace DealMaker.ShortSale.Utils
{
    public sealed class HUDFileNameManager
    {
        static readonly HUDFileNameManager _instance = new HUDFileNameManager();

        static HUDFileNameManager()
        { 
        }

        HUDFileNameManager()
        {
        }

        public static HUDFileNameManager Instance
        {
            get { return _instance; }
        }

        public string GetPath(string startupPath)
        {
            return startupPath + @"\" + StringStore.SavedExtDocumentsFolder + StringStore.HUDSubFolder;
        }

        string GetDocumentName(string documentName)
        {
            string[] docName = documentName.Split('.');

            if (null == docName || 0 == docName.Length)
                docName = new string[1] { documentName };

            return docName[0];
        }

        public bool GetCreateDocument(string path, string documentName, int lienIdx, out string foundFileName)
        {            
            documentName = Path.GetFileName(documentName);
            path = GetPath(path);

            if (!TestForExisting(path, documentName, lienIdx, out foundFileName))
                return CreateNew(documentName, lienIdx, out foundFileName);
            else
                return true;

        }

        bool CreateNew(string documentName, int lienIdx, out string fileName)
        {
            fileName = "HUD_Lien" + lienIdx.ToString() + "_" + GetDocumentName(documentName) + "_" + DateTime.Now.Year.ToString() + "_" + DateTime.Now.Month.ToString("00") + "_" + DateTime.Now.Day.ToString() + "_" + DateTime.Now.Hour.ToString() + "hrs" + DateTime.Now.Minute.ToString() + "min" + DateTime.Now.Second.ToString() + "sec.hsd";
            return false;
        }

        bool TestForExisting(string path, string documentName, int lienIdx, out string foundFileName)
        {
            foundFileName = string.Empty;
            List<FileInfo> foundFileNames = new List<FileInfo>();

            try
            {
                DirectoryInfo di = new DirectoryInfo(path);
                FileInfo[] fileNames = di.GetFiles("*.hsd");

                if (null != fileNames && 0 != fileNames.Length)
                {
                    foreach (FileInfo fileName in fileNames)
                    {
                        string tempFileName = Path.GetFileName(fileName.FullName);
                        string[] particles = tempFileName.Split('_');
                        if (null != particles && particles.Length >= 2)
                        {
                            if (("Lien" + lienIdx == particles[1]) && (GetDocumentName(documentName) == particles[2]))
                            {
                                foundFileNames.Add(fileName);
                            }
                        }
                    }
                }
            }
            catch { }

            
            if (0 != foundFileNames.Count)
            {
                FileInfo[] ffn = foundFileNames.ToArray();
                Array.Sort(ffn, new CompareFileInfo());                
                foundFileName = ffn[ffn.Length - 1].ToString();
                return true;
            }

            foundFileName = "";
            return false;
        }
    }

    public class CompareFileInfo : IComparer
    {                
        public int  Compare(object x, object y)
        {
            FileInfo f1, f2;
 
            f1 = (FileInfo)(x);
            f2 = (FileInfo)(y);
 
            return DateTime.Compare(f1.LastWriteTime, f2.LastWriteTime);
        }        
    }
}
