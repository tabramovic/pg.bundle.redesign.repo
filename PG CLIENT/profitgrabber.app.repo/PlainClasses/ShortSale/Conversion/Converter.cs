﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml.Serialization;
using System.Windows.Forms;


using DealMaker.Conversion.ShortSale;
using DealMaker.ShortSale;

namespace DealMaker.PlainClasses.ShortSale.Conversion
{
    public class Converter
    {
        

        public Converter()
        {            
            InsertUpdateSSXDataIntoDatabaseFromXMLFile();

            bool stopHere = true;
        }

        string[] GetAvailableXMLFiles()
        {
            string[] files = Directory.GetFiles(@"c:\KIT");
            return files;
        }

        void InsertUpdateSSXDataIntoDatabaseFromXMLFile()
        {
            string[] availableXMLFiles = GetAvailableXMLFiles();

            int converted = 0;
            int failed = 0;

            foreach (string availableFile in availableXMLFiles)
            {
                //DealMaker.Conversion.ShortSale.ShortSaleDocument oldSSD = LoadDocumentFromXML(@"c:\kit\234C5D71-632E-4A59-949C-4A72689060F3");
                //DealMaker.ShortSale.ShortSaleDocument newSSD = Convert(oldSSD);

                DealMaker.Conversion.ShortSale.ShortSaleDocument oldSSD = LoadDocumentFromXML(availableFile);

                if (null == oldSSD)
                {
                    bool stopHere = true;
                }                

                DealMaker.ShortSale.ShortSaleDocument newSSD = Convert(oldSSD);

                
                if (null != newSSD)
                {
                    string error = string.Empty;

                    string pgId = availableFile.Replace(@"c:\KIT\", string.Empty);
                    DealMaker.PlainClasses.ShortSale.Adapters.DataManager.Instance.PropertyItemID = new Guid(pgId);
                    bool convertedRes = DealMaker.PlainClasses.ShortSale.Adapters.DataManager.Instance.SaveShortSaleDocument(newSSD, string.Empty, out error);
                    //MessageBox.Show("CONVERTED:" + availableFile);
                    if (convertedRes)
                        converted++;
                    else
                        failed++;

                }
                else
                {
                    failed++;
                    //MessageBox.Show("FAILED:" + availableFile);
                }

                
            }

            MessageBox.Show("Available: " + availableXMLFiles.Length.ToString() + ", Converted: " + converted.ToString() + ", Failed: " + failed.ToString());
        }

        DealMaker.Conversion.ShortSale.ShortSaleDocument LoadDocumentFromXML(string documentName)
        {
            DealMaker.Conversion.ShortSale.ShortSaleDocument oldSSD;
            string error = string.Empty;

            StreamReader reader = null;
            try	// Deserialize
            {
                XmlSerializer ser = new XmlSerializer(typeof(DealMaker.Conversion.ShortSale.ShortSaleDocument));                
                reader = new StreamReader(documentName);
                oldSSD = (DealMaker.Conversion.ShortSale.ShortSaleDocument)ser.Deserialize(reader);                                                
            }
            catch (Exception ex)
            {                
                error = ex.Message;
                oldSSD = null;
            }
            finally
            {
                if (reader != null)
                    reader.Close();
                reader = null;
            }

            return oldSSD;
        }

        DealMaker.ShortSale.ShortSaleDocument Convert(DealMaker.Conversion.ShortSale.ShortSaleDocument oldSSD)
        {
            if (null == oldSSD)
                return null;

            DealMaker.ShortSale.ShortSaleDocument newSSD = new DealMaker.ShortSale.ShortSaleDocument();

            //CONTACT INFO
            newSSD.ContactInfo = new DealMaker.ShortSale.ShortSaleDocumentContactInfo();

            //DOCUMENT TRACKER
            newSSD.DocumentTracker = new DealMaker.ShortSale.ShortSaleDocumentDocumentTracker(); 
            try { newSSD.DocumentTracker.FirstEntered = oldSSD.DocumentTracker.FirstEntered; }catch {}
            try { newSSD.DocumentTracker.LastModified = oldSSD.DocumentTracker.LastModified; }catch {}
            try { newSSD.DocumentTracker.MarkForGroupReport = oldSSD.DocumentTracker.MarkForGroupReport; }catch {}

            //DOCUMENT GENERATION ARCHIVE
            newSSD.DocumentGenerationArchive = new DealMaker.ShortSale.ShortSaleDocumentDocumentGenerationArchive();
            newSSD.DocumentGenerationArchive.AgentInfo = new DealMaker.ShortSale.ShortSaleDocumentDocumentGenerationArchiveAgentInfo();
            newSSD.DocumentGenerationArchive.AgentInfo.AddressInfo = new DealMaker.ShortSale.ShortSaleDocumentDocumentGenerationArchiveAgentInfoAddressInfo();
            newSSD.DocumentGenerationArchive.AgentInfo.AddressInfo.SiteAddress = new DealMaker.ShortSale.ShortSaleDocumentDocumentGenerationArchiveAgentInfoAddressInfoSiteAddress();
            newSSD.DocumentGenerationArchive.AgentInfo.AddressInfo.SiteAddress.Address = new DealMaker.ShortSale.ShortSaleDocumentDocumentGenerationArchiveAgentInfoAddressInfoSiteAddressAddress();
            try { newSSD.DocumentGenerationArchive.AgentInfo.AddressInfo.SiteAddress.Address.SiteStreetAddress = (string)oldSSD.DocumentGenerationArchive.AgentInfo.AddressInfo.SiteAddress.Address.SiteStreetAddress; }catch {}
            try { newSSD.DocumentGenerationArchive.AgentInfo.Email = (string)oldSSD.DocumentGenerationArchive.AgentInfo.Email; }catch {}
            try { newSSD.DocumentGenerationArchive.AgentInfo.FaxNr = (string)oldSSD.DocumentGenerationArchive.AgentInfo.FaxNr; }catch {}
            try { newSSD.DocumentGenerationArchive.AgentInfo.FirstName = (string)oldSSD.DocumentGenerationArchive.AgentInfo.FirstName; }catch {}
            try { newSSD.DocumentGenerationArchive.AgentInfo.FullName = (string)oldSSD.DocumentGenerationArchive.AgentInfo.FullName; }catch {}
            try { newSSD.DocumentGenerationArchive.AgentInfo.LastName = (string)oldSSD.DocumentGenerationArchive.AgentInfo.LastName; }catch {}
            try { newSSD.DocumentGenerationArchive.AgentInfo.PhoneNr = (string)oldSSD.DocumentGenerationArchive.AgentInfo.PhoneNr; }catch {}

            newSSD.DocumentGenerationArchive.BuyerInfo = new DealMaker.ShortSale.ShortSaleDocumentDocumentGenerationArchiveBuyerInfo();
            newSSD.DocumentGenerationArchive.BuyerInfo.AddressInfo = new DealMaker.ShortSale.ShortSaleDocumentDocumentGenerationArchiveBuyerInfoAddressInfo();
            newSSD.DocumentGenerationArchive.BuyerInfo.AddressInfo.SiteAddress = new DealMaker.ShortSale.ShortSaleDocumentDocumentGenerationArchiveBuyerInfoAddressInfoSiteAddress();
            newSSD.DocumentGenerationArchive.BuyerInfo.AddressInfo.SiteAddress.Address = new DealMaker.ShortSale.ShortSaleDocumentDocumentGenerationArchiveBuyerInfoAddressInfoSiteAddressAddress();
            try { newSSD.DocumentGenerationArchive.BuyerInfo.AddressInfo.SiteAddress.Address.SiteStreetAddress = (string)oldSSD.DocumentGenerationArchive.BuyerInfo.AddressInfo.SiteAddress.Address.SiteStreetAddress; }catch {}
            try { newSSD.DocumentGenerationArchive.BuyerInfo.Email = (string)oldSSD.DocumentGenerationArchive.BuyerInfo.Email; }catch {}
            try { newSSD.DocumentGenerationArchive.BuyerInfo.FaxNr = (string)oldSSD.DocumentGenerationArchive.BuyerInfo.FaxNr; }catch {}
            try { newSSD.DocumentGenerationArchive.BuyerInfo.FirstName = (string)oldSSD.DocumentGenerationArchive.BuyerInfo.FirstName; }catch {}
            try { newSSD.DocumentGenerationArchive.BuyerInfo.FullName = (string)oldSSD.DocumentGenerationArchive.BuyerInfo.FullName; }catch {}
            try { newSSD.DocumentGenerationArchive.BuyerInfo.LastName = (string)oldSSD.DocumentGenerationArchive.BuyerInfo.LastName; }catch {}
            try { newSSD.DocumentGenerationArchive.BuyerInfo.PhoneNr = (string)oldSSD.DocumentGenerationArchive.BuyerInfo.PhoneNr; }catch {}

            newSSD.DocumentGenerationArchive.OwnerInfo = new DealMaker.ShortSale.ShortSaleDocumentDocumentGenerationArchiveOwnerInfo();
            newSSD.DocumentGenerationArchive.OwnerInfo.AddressInfo = new DealMaker.ShortSale.ShortSaleDocumentDocumentGenerationArchiveOwnerInfoAddressInfo();
            newSSD.DocumentGenerationArchive.OwnerInfo.AddressInfo.MailAddress = new DealMaker.ShortSale.ShortSaleDocumentDocumentGenerationArchiveOwnerInfoAddressInfoMailAddress();
            newSSD.DocumentGenerationArchive.OwnerInfo.AddressInfo.MailAddress.Address = new DealMaker.ShortSale.ShortSaleDocumentDocumentGenerationArchiveOwnerInfoAddressInfoMailAddressAddress();
            try { newSSD.DocumentGenerationArchive.OwnerInfo.AddressInfo.MailAddress.Address.SiteStreetAddress = (string)oldSSD.DocumentGenerationArchive.OwnerInfo.AddressInfo.MailAddress.Address.SiteStreetAddress; }catch {}
            newSSD.DocumentGenerationArchive.OwnerInfo.AddressInfo.SiteAddress = new DealMaker.ShortSale.ShortSaleDocumentDocumentGenerationArchiveOwnerInfoAddressInfoSiteAddress();
            newSSD.DocumentGenerationArchive.OwnerInfo.AddressInfo.SiteAddress.Address = new DealMaker.ShortSale.ShortSaleDocumentDocumentGenerationArchiveOwnerInfoAddressInfoSiteAddressAddress();
            try { newSSD.DocumentGenerationArchive.OwnerInfo.AddressInfo.SiteAddress.Address.SiteStreetAddress = (string)oldSSD.DocumentGenerationArchive.OwnerInfo.AddressInfo.SiteAddress.Address.SiteStreetAddress; }catch {}
                                            


            //SHORTSALE INFO
            newSSD.ShortSaleInfo = new DealMaker.ShortSale.ShortSaleDocumentShortSaleInfo();
            try { newSSD.ShortSaleInfo.OverallMaxToPay = (string)oldSSD.ShortSaleInfo.OverallMaxToPay; }catch {}
            try { newSSD.ShortSaleInfo.OverallShortSaleStrategy = (string)oldSSD.ShortSaleInfo.OverallShortSaleStrategy; }catch {}
            try { newSSD.ShortSaleInfo.PrimaryPhone = (string)oldSSD.ShortSaleInfo.PrimaryPhone; }catch {}
            try { newSSD.ShortSaleInfo.PrimarySS = (string)oldSSD.ShortSaleInfo.PrimarySS; }catch {}
            try { newSSD.ShortSaleInfo.SecondaryPhone = (string)oldSSD.ShortSaleInfo.SecondaryPhone; }catch {}
            try { newSSD.ShortSaleInfo.SecondarySS = (string)oldSSD.ShortSaleInfo.SecondarySS; } catch {}            

            newSSD.ShortSaleInfo.Liens = new DealMaker.ShortSale.ShortSaleDocumentShortSaleInfoLiens();
            try { newSSD.ShortSaleInfo.Liens.Accept1ExpDate = oldSSD.ShortSaleInfo.Liens.Accept1ExpDate;} catch {}
            try { newSSD.ShortSaleInfo.Liens.Accept2ExpDate = oldSSD.ShortSaleInfo.Liens.Accept2ExpDate;} catch {}
            try { newSSD.ShortSaleInfo.Liens.Accept3ExpDate = oldSSD.ShortSaleInfo.Liens.Accept3ExpDate;} catch {}
            try { newSSD.ShortSaleInfo.Liens.ForeclosureDate = oldSSD.ShortSaleInfo.Liens.ForeclosureDate;} catch {}
            try { newSSD.ShortSaleInfo.Liens.TitleSeasonDate = oldSSD.ShortSaleInfo.Liens.TitleSeasonDate;} catch {}
            try { newSSD.ShortSaleInfo.Liens.Lien = new DealMaker.ShortSale.ShortSaleDocumentShortSaleInfoLiensLien[oldSSD.ShortSaleInfo.Liens.Lien.Length]; }
            catch { }

            List<DealMaker.ShortSale.ShortSaleDocumentShortSaleInfoLiensLien> newLiens = new List<DealMaker.ShortSale.ShortSaleDocumentShortSaleInfoLiensLien>();
            foreach (DealMaker.Conversion.ShortSale.ShortSaleDocumentShortSaleInfoLiensLien oldLien in oldSSD.ShortSaleInfo.Liens.Lien)
            {
                DealMaker.ShortSale.ShortSaleDocumentShortSaleInfoLiensLien newLien = new DealMaker.ShortSale.ShortSaleDocumentShortSaleInfoLiensLien();
                try { newLien.AccountNr = oldLien.AccountNr.ToString();} catch {}
                try { newLien.CompanyName = oldLien.CompanyName;} catch {}
                try { newLien.LienID = oldLien.LienID; } catch {}
                try { newLien.LogFilePath = oldLien.LogFilePath;} catch {}
                
                newLien.LienHolderContactInfo = new DealMaker.ShortSale.ShortSaleDocumentShortSaleInfoLiensLienLienHolderContactInfo();
                newLien.LienHolderContactInfo.BankruptcyDeptField = new DealMaker.ShortSale.ShortSaleDocumentShortSaleInfoLiensLienLienHolderContactInfoBankruptcyDeptField();
                try { newLien.LienHolderContactInfo.BankruptcyDeptField.ContactName = oldLien.LienHolderContactInfo.BankruptcyDeptField.ContactName;} catch {}
                try { newLien.LienHolderContactInfo.BankruptcyDeptField.Email = (string)oldLien.LienHolderContactInfo.BankruptcyDeptField.Email;} catch {}
                try { newLien.LienHolderContactInfo.BankruptcyDeptField.Fax = (string)oldLien.LienHolderContactInfo.BankruptcyDeptField.Fax;} catch {}
                try { newLien.LienHolderContactInfo.BankruptcyDeptField.Phone = oldLien.LienHolderContactInfo.BankruptcyDeptField.Phone;} catch {}
                try { newLien.LienHolderContactInfo.BankruptcyDeptField.Web = (string)oldLien.LienHolderContactInfo.BankruptcyDeptField.Web;} catch {}

                newLien.LienHolderContactInfo.BPOAppraisal1 = new DealMaker.ShortSale.ShortSaleDocumentShortSaleInfoLiensLienLienHolderContactInfoBPOAppraisal1();
                try { newLien.LienHolderContactInfo.BPOAppraisal1.ContactName = oldLien.LienHolderContactInfo.BPOAppraisal1.ContactName;} catch {}
                try { newLien.LienHolderContactInfo.BPOAppraisal1.Email = oldLien.LienHolderContactInfo.BPOAppraisal1.Email;} catch {}
                try { newLien.LienHolderContactInfo.BPOAppraisal1.Fax = (string)oldLien.LienHolderContactInfo.BPOAppraisal1.Fax;} catch {}
                try { newLien.LienHolderContactInfo.BPOAppraisal1.Phone = oldLien.LienHolderContactInfo.BPOAppraisal1.Phone;} catch {}
                try { newLien.LienHolderContactInfo.BPOAppraisal1.Web = (string)oldLien.LienHolderContactInfo.BPOAppraisal1.Web;} catch {}

                newLien.LienHolderContactInfo.BPOAppraisal2 = new DealMaker.ShortSale.ShortSaleDocumentShortSaleInfoLiensLienLienHolderContactInfoBPOAppraisal2();
                try { newLien.LienHolderContactInfo.BPOAppraisal2.ContactName = oldLien.LienHolderContactInfo.BPOAppraisal2.ContactName;} catch {}
                try { newLien.LienHolderContactInfo.BPOAppraisal2.Email = (string)oldLien.LienHolderContactInfo.BPOAppraisal2.Email;} catch {}
                try { newLien.LienHolderContactInfo.BPOAppraisal2.Fax = (string)oldLien.LienHolderContactInfo.BPOAppraisal2.Fax;} catch {}
                try { newLien.LienHolderContactInfo.BPOAppraisal2.Phone = oldLien.LienHolderContactInfo.BPOAppraisal2.Phone;} catch {}
                try { newLien.LienHolderContactInfo.BPOAppraisal2.Web = (string)oldLien.LienHolderContactInfo.BPOAppraisal2.Web;} catch {}

                newLien.LienHolderContactInfo.CustomerService = new DealMaker.ShortSale.ShortSaleDocumentShortSaleInfoLiensLienLienHolderContactInfoCustomerService();
                try { newLien.LienHolderContactInfo.CustomerService.ContactName = oldLien.LienHolderContactInfo.CustomerService.ContactName;} catch {}
                try { newLien.LienHolderContactInfo.CustomerService.Email = (string)oldLien.LienHolderContactInfo.CustomerService.Email;} catch {}
                try { newLien.LienHolderContactInfo.CustomerService.Fax = (string)oldLien.LienHolderContactInfo.CustomerService.Fax;} catch {}
                try { newLien.LienHolderContactInfo.CustomerService.Phone = (string)oldLien.LienHolderContactInfo.CustomerService.Phone;} catch {}
                try { newLien.LienHolderContactInfo.CustomerService.Web = (string)oldLien.LienHolderContactInfo.CustomerService.Web;} catch {}

                newLien.LienHolderContactInfo.ForeClosureAttorney = new DealMaker.ShortSale.ShortSaleDocumentShortSaleInfoLiensLienLienHolderContactInfoForeClosureAttorney();
                try { newLien.LienHolderContactInfo.ForeClosureAttorney.ContactName = (string)oldLien.LienHolderContactInfo.ForeClosureAttorney.ContactName;} catch {}
                try { newLien.LienHolderContactInfo.ForeClosureAttorney.Email = (string)oldLien.LienHolderContactInfo.ForeClosureAttorney.Email;} catch {}
                try { newLien.LienHolderContactInfo.ForeClosureAttorney.Fax = (string)oldLien.LienHolderContactInfo.ForeClosureAttorney.Fax;} catch {}
                try { newLien.LienHolderContactInfo.ForeClosureAttorney.Phone = (string)oldLien.LienHolderContactInfo.ForeClosureAttorney.Phone;} catch {}
                try { newLien.LienHolderContactInfo.ForeClosureAttorney.Web = (string)oldLien.LienHolderContactInfo.ForeClosureAttorney.Web;} catch {}

                newLien.LienHolderContactInfo.NegManager = new DealMaker.ShortSale.ShortSaleDocumentShortSaleInfoLiensLienLienHolderContactInfoNegManager();
                try { newLien.LienHolderContactInfo.NegManager.ContactName = oldLien.LienHolderContactInfo.NegManager.ContactName;} catch {}
                try { newLien.LienHolderContactInfo.NegManager.Email = (string)oldLien.LienHolderContactInfo.NegManager.Email;} catch {}
                try { newLien.LienHolderContactInfo.NegManager.Fax = (string)oldLien.LienHolderContactInfo.NegManager.Fax;} catch {}
                try { newLien.LienHolderContactInfo.NegManager.Phone = (string)oldLien.LienHolderContactInfo.NegManager.Phone;} catch {}
                try { newLien.LienHolderContactInfo.NegManager.Web = (string)oldLien.LienHolderContactInfo.NegManager.Web;} catch {}

                newLien.LienHolderContactInfo.Negotiator = new DealMaker.ShortSale.ShortSaleDocumentShortSaleInfoLiensLienLienHolderContactInfoNegotiator();
                try { newLien.LienHolderContactInfo.Negotiator.ContactName = oldLien.LienHolderContactInfo.Negotiator.ContactName;} catch {}
                try { newLien.LienHolderContactInfo.Negotiator.Email = (string)oldLien.LienHolderContactInfo.Negotiator.Email;} catch {}
                try { newLien.LienHolderContactInfo.Negotiator.Fax = oldLien.LienHolderContactInfo.Negotiator.Fax;} catch {}
                try { newLien.LienHolderContactInfo.Negotiator.Phone = oldLien.LienHolderContactInfo.Negotiator.Phone;} catch {}
                try { newLien.LienHolderContactInfo.Negotiator.Web = (string)oldLien.LienHolderContactInfo.Negotiator.Web;} catch {}

                newLien.LienHolderContactInfo.SetupDept = new DealMaker.ShortSale.ShortSaleDocumentShortSaleInfoLiensLienLienHolderContactInfoSetupDept();
                try { newLien.LienHolderContactInfo.SetupDept.ContactName = oldLien.LienHolderContactInfo.SetupDept.ContactName;} catch {}
                try { newLien.LienHolderContactInfo.SetupDept.Email = (string)oldLien.LienHolderContactInfo.SetupDept.Email;} catch {}
                try { newLien.LienHolderContactInfo.SetupDept.Fax = oldLien.LienHolderContactInfo.SetupDept.Fax;} catch {}
                try { newLien.LienHolderContactInfo.SetupDept.Phone = oldLien.LienHolderContactInfo.SetupDept.Phone;} catch {}
                try { newLien.LienHolderContactInfo.SetupDept.Web = (string)oldLien.LienHolderContactInfo.SetupDept.Web;} catch {}

                newLien.LienHolderContactInfo.SetupManager = new DealMaker.ShortSale.ShortSaleDocumentShortSaleInfoLiensLienLienHolderContactInfoSetupManager();
                try { newLien.LienHolderContactInfo.SetupManager.ContactName = oldLien.LienHolderContactInfo.SetupManager.ContactName;} catch {}
                try { newLien.LienHolderContactInfo.SetupManager.Email = (string)oldLien.LienHolderContactInfo.SetupManager.Email;} catch {}
                try { newLien.LienHolderContactInfo.SetupManager.Fax = (string)oldLien.LienHolderContactInfo.SetupManager.Fax;} catch {}
                try { newLien.LienHolderContactInfo.SetupManager.Phone = oldLien.LienHolderContactInfo.SetupManager.Phone;} catch {}
                try { newLien.LienHolderContactInfo.SetupManager.Web = (string)oldLien.LienHolderContactInfo.SetupManager.Web;} catch {}

                newLien.LienHolderContactInfo.Strategy = new DealMaker.ShortSale.ShortSaleDocumentShortSaleInfoLiensLienLienHolderContactInfoStrategy();
                try { newLien.LienHolderContactInfo.Strategy.MaxToPay = (string)oldLien.LienHolderContactInfo.Strategy.MaxToPay;} catch {}
                try { newLien.LienHolderContactInfo.Strategy.SelectedStrategy = oldLien.LienHolderContactInfo.Strategy.SelectedStrategy;} catch {}
                try { newLien.LienHolderContactInfo.Strategy.SelectedStrategyDesc = oldLien.LienHolderContactInfo.Strategy.SelectedStrategyDesc;} catch {}

                newLien.LienHolderContactInfo.Financials = new DealMaker.ShortSale.ShortSaleDocumentShortSaleInfoLiensLienLienHolderContactInfoFinancials();
                newLien.LienHolderContactInfo.Financials.Offers = new DealMaker.ShortSale.ShortSaleDocumentShortSaleInfoLiensLienLienHolderContactInfoFinancialsOffers();
                try { newLien.LienHolderContactInfo.Financials.Offers.Accepted = string.Empty;} catch {}
                newLien.LienHolderContactInfo.Financials.Offers.Fifth = new DealMaker.ShortSale.ShortSaleDocumentShortSaleInfoLiensLienLienHolderContactInfoFinancialsOffersFifth();
                try { newLien.LienHolderContactInfo.Financials.Offers.Fifth.Counter = (string)oldLien.LienHolderContactInfo.Financials.Offers.Fifth.Counter;} catch {}
                try { newLien.LienHolderContactInfo.Financials.Offers.Fifth.Offer = (string)oldLien.LienHolderContactInfo.Financials.Offers.Fifth.Offer;} catch {}

                newLien.LienHolderContactInfo.Financials.Offers.First = new DealMaker.ShortSale.ShortSaleDocumentShortSaleInfoLiensLienLienHolderContactInfoFinancialsOffersFirst();
                try { newLien.LienHolderContactInfo.Financials.Offers.First.Counter = oldLien.LienHolderContactInfo.Financials.Offers.First.Counter;} catch {}
                try { newLien.LienHolderContactInfo.Financials.Offers.First.Offer = oldLien.LienHolderContactInfo.Financials.Offers.First.Offer;} catch {}

                newLien.LienHolderContactInfo.Financials.Offers.Fourth = new DealMaker.ShortSale.ShortSaleDocumentShortSaleInfoLiensLienLienHolderContactInfoFinancialsOffersFourth();
                try { newLien.LienHolderContactInfo.Financials.Offers.Fourth.Counter = (string)oldLien.LienHolderContactInfo.Financials.Offers.Fourth.Counter;} catch {}
                try { newLien.LienHolderContactInfo.Financials.Offers.Fourth.Offer = (string)oldLien.LienHolderContactInfo.Financials.Offers.Fourth.Offer;} catch {}

                newLien.LienHolderContactInfo.Financials.Offers.Second = new DealMaker.ShortSale.ShortSaleDocumentShortSaleInfoLiensLienLienHolderContactInfoFinancialsOffersSecond();
                try { newLien.LienHolderContactInfo.Financials.Offers.Second.Counter = oldLien.LienHolderContactInfo.Financials.Offers.Second.Counter;} catch {}
                try { newLien.LienHolderContactInfo.Financials.Offers.Second.Offer = oldLien.LienHolderContactInfo.Financials.Offers.Second.Offer;} catch {}

                newLien.LienHolderContactInfo.Financials.Offers.Third = new DealMaker.ShortSale.ShortSaleDocumentShortSaleInfoLiensLienLienHolderContactInfoFinancialsOffersThird();
                try { newLien.LienHolderContactInfo.Financials.Offers.Third.Counter = (string)oldLien.LienHolderContactInfo.Financials.Offers.Third.Counter;} catch {}
                try { newLien.LienHolderContactInfo.Financials.Offers.Third.Offer = (string)oldLien.LienHolderContactInfo.Financials.Offers.Third.Offer;} catch {}

                newLien.LienStatus = new DealMaker.ShortSale.ShortSaleDocumentShortSaleInfoLiensLienLienStatus();
                
                newLien.LienStatus.AcceptanceLetterReceived = new DealMaker.ShortSale.ShortSaleDocumentShortSaleInfoLiensLienLienStatusAcceptanceLetterReceived();
                try { newLien.LienStatus.AcceptanceLetterReceived.Checked = oldLien.LienStatus.AcceptanceLetterReceived.Checked;} catch {}
                try { newLien.LienStatus.AcceptanceLetterReceived.Date = oldLien.LienStatus.AcceptanceLetterReceived.Date;} catch {}
                try { newLien.LienStatus.AcceptanceLetterReceived.Who = oldLien.LienStatus.AcceptanceLetterReceived.Who;} catch {}

                newLien.LienStatus.AllInfoEnteredInProfitGrabber = new DealMaker.ShortSale.ShortSaleDocumentShortSaleInfoLiensLienLienStatusAllInfoEnteredInProfitGrabber();
                try { newLien.LienStatus.AllInfoEnteredInProfitGrabber.Checked = oldLien.LienStatus.AllInfoEnteredInProfitGrabber.Checked;} catch {}
                try { newLien.LienStatus.AllInfoEnteredInProfitGrabber.Date = oldLien.LienStatus.AllInfoEnteredInProfitGrabber.Date;} catch {}
                try { newLien.LienStatus.AllInfoEnteredInProfitGrabber.Who = oldLien.LienStatus.AllInfoEnteredInProfitGrabber.Who;} catch {}

                newLien.LienStatus.AllIntakeDocsFromSeller = new DealMaker.ShortSale.ShortSaleDocumentShortSaleInfoLiensLienLienStatusAllIntakeDocsFromSeller();
                try { newLien.LienStatus.AllIntakeDocsFromSeller.Checked = oldLien.LienStatus.AllIntakeDocsFromSeller.Checked;} catch {}
                try { newLien.LienStatus.AllIntakeDocsFromSeller.Date = oldLien.LienStatus.AllIntakeDocsFromSeller.Date;} catch {}
                try { newLien.LienStatus.AllIntakeDocsFromSeller.Who = oldLien.LienStatus.AllIntakeDocsFromSeller.Who;} catch {}

                newLien.LienStatus.AnyTitleIssues = new DealMaker.ShortSale.ShortSaleDocumentShortSaleInfoLiensLienLienStatusAnyTitleIssues();
                try { newLien.LienStatus.AnyTitleIssues.Checked = oldLien.LienStatus.AnyTitleIssues.Checked;} catch {}
                try { newLien.LienStatus.AnyTitleIssues.Date = oldLien.LienStatus.AnyTitleIssues.Date;} catch {}
                try { newLien.LienStatus.AnyTitleIssues.Who = oldLien.LienStatus.AnyTitleIssues.Who;} catch {}

                newLien.LienStatus.AuthorizationFaxedIn = new DealMaker.ShortSale.ShortSaleDocumentShortSaleInfoLiensLienLienStatusAuthorizationFaxedIn();
                try { newLien.LienStatus.AuthorizationFaxedIn.Checked = oldLien.LienStatus.AuthorizationFaxedIn.Checked;} catch {}
                try { newLien.LienStatus.AuthorizationFaxedIn.Date = oldLien.LienStatus.AuthorizationFaxedIn.Date;} catch {}
                try { newLien.LienStatus.AuthorizationFaxedIn.Who = oldLien.LienStatus.AuthorizationFaxedIn.Who;} catch {}

                newLien.LienStatus.AuthorizationVerifiedReceived = new DealMaker.ShortSale.ShortSaleDocumentShortSaleInfoLiensLienLienStatusAuthorizationVerifiedReceived();
                try { newLien.LienStatus.AuthorizationVerifiedReceived.Checked = oldLien.LienStatus.AuthorizationVerifiedReceived.Checked;} catch {}
                try { newLien.LienStatus.AuthorizationVerifiedReceived.Date = oldLien.LienStatus.AuthorizationVerifiedReceived.Date;} catch {}
                try { newLien.LienStatus.AuthorizationVerifiedReceived.Who = oldLien.LienStatus.AuthorizationVerifiedReceived.Who;} catch {}

                newLien.LienStatus.BPOAppraisalCompleted = new DealMaker.ShortSale.ShortSaleDocumentShortSaleInfoLiensLienLienStatusBPOAppraisalCompleted();
                try { newLien.LienStatus.BPOAppraisalCompleted.Checked = string.Empty;} catch {}
                try { newLien.LienStatus.BPOAppraisalCompleted.Date = oldLien.LienStatus.BPOAppraisalCompleted.Date;} catch {}
                try { newLien.LienStatus.BPOAppraisalCompleted.Who = string.Empty;} catch {}

                newLien.LienStatus.BPOAppraisalOrdered = new DealMaker.ShortSale.ShortSaleDocumentShortSaleInfoLiensLienLienStatusBPOAppraisalOrdered();
                try { newLien.LienStatus.BPOAppraisalOrdered.Checked = oldLien.LienStatus.BPOAppraisalOrdered.Checked;} catch {}
                try { newLien.LienStatus.BPOAppraisalOrdered.Date = oldLien.LienStatus.BPOAppraisalOrdered.Date;} catch {}
                try { newLien.LienStatus.BPOAppraisalOrdered.Who = oldLien.LienStatus.BPOAppraisalOrdered.Who;} catch {}

                newLien.LienStatus.BPOAppraisalValue = new DealMaker.ShortSale.ShortSaleDocumentShortSaleInfoLiensLienLienStatusBPOAppraisalValue();
                try { newLien.LienStatus.BPOAppraisalValue.Checked = oldLien.LienStatus.BPOAppraisalValue.Checked;      } catch {}          
                try { newLien.LienStatus.BPOAppraisalValue.Who = oldLien.LienStatus.BPOAppraisalValue.Who;} catch {}

                newLien.LienStatus.BPOPacketComplete = new DealMaker.ShortSale.ShortSaleDocumentShortSaleInfoLiensLienLienStatusBPOPacketComplete();
                try { newLien.LienStatus.BPOPacketComplete.Checked = oldLien.LienStatus.BPOPacketComplete.Checked;} catch {}
                try { newLien.LienStatus.BPOPacketComplete.Date = oldLien.LienStatus.BPOPacketComplete.Date;} catch {}
                try { newLien.LienStatus.BPOPacketComplete.Who = oldLien.LienStatus.BPOPacketComplete.Who;} catch {}

                newLien.LienStatus.DeficiencyJudgmentWaived = new DealMaker.ShortSale.ShortSaleDocumentShortSaleInfoLiensLienLienStatusDeficiencyJudgmentWaived();
                try { newLien.LienStatus.DeficiencyJudgmentWaived.Checked = oldLien.LienStatus.DeficiencyJudgmentWaived.Checked;} catch {}
                try { newLien.LienStatus.DeficiencyJudgmentWaived.Date = oldLien.LienStatus.DeficiencyJudgmentWaived.Date;} catch {}
                try { newLien.LienStatus.DeficiencyJudgmentWaived.Who = oldLien.LienStatus.DeficiencyJudgmentWaived.Who;} catch {}

                newLien.LienStatus.MarketDataSentToNegotiator = new DealMaker.ShortSale.ShortSaleDocumentShortSaleInfoLiensLienLienStatusMarketDataSentToNegotiator();
                try { newLien.LienStatus.MarketDataSentToNegotiator.Checked = oldLien.LienStatus.MarketDataSentToNegotiator.Checked;} catch {}
                try { newLien.LienStatus.MarketDataSentToNegotiator.Date = oldLien.LienStatus.MarketDataSentToNegotiator.Date;} catch {}
                try { newLien.LienStatus.MarketDataSentToNegotiator.Who = oldLien.LienStatus.MarketDataSentToNegotiator.Who;} catch {}

                newLien.LienStatus.MinPercentOfBPOAccepted = new DealMaker.ShortSale.ShortSaleDocumentShortSaleInfoLiensLienLienStatusMinPercentOfBPOAccepted();
                try { newLien.LienStatus.MinPercentOfBPOAccepted.Checked = oldLien.LienStatus.MinPercentOfBPOAccepted.Checked;        } catch {}        
                try { newLien.LienStatus.MinPercentOfBPOAccepted.Who = oldLien.LienStatus.MinPercentOfBPOAccepted.Who;} catch {}

                newLien.LienStatus.NegotiatorAssigned = new DealMaker.ShortSale.ShortSaleDocumentShortSaleInfoLiensLienLienStatusNegotiatorAssigned();
                try { newLien.LienStatus.NegotiatorAssigned.Checked = oldLien.LienStatus.NegotiatorAssigned.Checked;} catch {}
                try { newLien.LienStatus.NegotiatorAssigned.Date = oldLien.LienStatus.NegotiatorAssigned.Date;} catch {}
                try { newLien.LienStatus.NegotiatorAssigned.Who = oldLien.LienStatus.NegotiatorAssigned.Who;} catch {}

                newLien.LienStatus.NoteHolderInvestorName = new DealMaker.ShortSale.ShortSaleDocumentShortSaleInfoLiensLienLienStatusNoteHolderInvestorName();
                try { newLien.LienStatus.NoteHolderInvestorName.Checked = oldLien.LienStatus.NoteHolderInvestorName.Checked;                } catch {}
                try { newLien.LienStatus.NoteHolderInvestorName.Who = oldLien.LienStatus.NoteHolderInvestorName.Who;} catch {}

                newLien.LienStatus.PayoffAmount = new DealMaker.ShortSale.ShortSaleDocumentShortSaleInfoLiensLienLienStatusPayoffAmount();
                try { newLien.LienStatus.PayoffAmount.Value = oldLien.LienStatus.PayoffAmount.Value;} catch {}
                

                newLien.LienStatus.PayoffOrdered = new DealMaker.ShortSale.ShortSaleDocumentShortSaleInfoLiensLienLienStatusPayoffOrdered();
                try { newLien.LienStatus.PayoffOrdered.Checked = oldLien.LienStatus.PayoffOrdered.Checked;} catch {}
                try { newLien.LienStatus.PayoffOrdered.Date = oldLien.LienStatus.PayoffOrdered.Date;} catch {}
                try { newLien.LienStatus.PayoffOrdered.Who = oldLien.LienStatus.PayoffOrdered.Who;} catch {}

                newLien.LienStatus.PreliminaryTitleSearchOrdered = new DealMaker.ShortSale.ShortSaleDocumentShortSaleInfoLiensLienLienStatusPreliminaryTitleSearchOrdered();
                try { newLien.LienStatus.PreliminaryTitleSearchOrdered.Checked = oldLien.LienStatus.PreliminaryTitleSearchOrdered.Checked;} catch {}
                try { newLien.LienStatus.PreliminaryTitleSearchOrdered.Date = oldLien.LienStatus.PreliminaryTitleSearchOrdered.Date;} catch {}
                try { newLien.LienStatus.PreliminaryTitleSearchOrdered.Who = oldLien.LienStatus.PreliminaryTitleSearchOrdered.Who;} catch {}

                newLien.LienStatus.PreliminaryTitleSearchReceived = new DealMaker.ShortSale.ShortSaleDocumentShortSaleInfoLiensLienLienStatusPreliminaryTitleSearchReceived();
                try { newLien.LienStatus.PreliminaryTitleSearchReceived.Checked = oldLien.LienStatus.PreliminaryTitleSearchReceived.Checked;} catch {}
                try { newLien.LienStatus.PreliminaryTitleSearchReceived.Date = oldLien.LienStatus.PreliminaryTitleSearchReceived.Date;} catch {}
                try { newLien.LienStatus.PreliminaryTitleSearchReceived.Who = oldLien.LienStatus.PreliminaryTitleSearchReceived.Who;} catch {}

                newLien.LienStatus.PrelimInspectionDate = new DealMaker.ShortSale.ShortSaleDocumentShortSaleInfoLiensLienLienStatusPrelimInspectionDate();
                try { newLien.LienStatus.PrelimInspectionDate.Checked = oldLien.LienStatus.PrelimInspectionDate.Checked; }
                catch { }
                try { newLien.LienStatus.PrelimInspectionDate.Date = oldLien.LienStatus.PrelimInspectionDate.Date; }
                catch { }
                try { newLien.LienStatus.PrelimInspectionDate.Who = oldLien.LienStatus.PrelimInspectionDate.Who; }
                catch { }

                newLien.LienStatus.PrelimInspectionOrdered = new DealMaker.ShortSale.ShortSaleDocumentShortSaleInfoLiensLienLienStatusPrelimInspectionOrdered();
                try { newLien.LienStatus.PrelimInspectionOrdered.Checked = oldLien.LienStatus.PrelimInspectionOrdered.Checked; }
                catch { }
                try { newLien.LienStatus.PrelimInspectionOrdered.Date = oldLien.LienStatus.PrelimInspectionOrdered.Date; }
                catch { }
                try { newLien.LienStatus.PrelimInspectionOrdered.Who = oldLien.LienStatus.PrelimInspectionOrdered.Who; }
                catch { }

                newLien.LienStatus.PropertyProfileInProfitGrabber = new DealMaker.ShortSale.ShortSaleDocumentShortSaleInfoLiensLienLienStatusPropertyProfileInProfitGrabber();
                try { newLien.LienStatus.PropertyProfileInProfitGrabber.Checked = oldLien.LienStatus.PropertyProfileInProfitGrabber.Checked; }
                catch { }
                try { newLien.LienStatus.PropertyProfileInProfitGrabber.Date = oldLien.LienStatus.PropertyProfileInProfitGrabber.Date; }
                catch { }
                try { newLien.LienStatus.PropertyProfileInProfitGrabber.Who = oldLien.LienStatus.PropertyProfileInProfitGrabber.Who; }
                catch { }

                newLien.LienStatus.ReinstatementAmount = new DealMaker.ShortSale.ShortSaleDocumentShortSaleInfoLiensLienLienStatusReinstatementAmount();
                try { newLien.LienStatus.ReinstatementAmount.Value = oldLien.LienStatus.ReinstatementAmount.Value; }
                catch { }

                newLien.LienStatus.ReinstatementOrdered = new DealMaker.ShortSale.ShortSaleDocumentShortSaleInfoLiensLienLienStatusReinstatementOrdered();
                try { newLien.LienStatus.ReinstatementOrdered.Checked = oldLien.LienStatus.ReinstatementOrdered.Checked; }
                catch { }
                try { newLien.LienStatus.ReinstatementOrdered.Date = oldLien.LienStatus.ReinstatementOrdered.Date; }
                catch { }
                try { newLien.LienStatus.ReinstatementOrdered.Who = oldLien.LienStatus.ReinstatementOrdered.Who; }
                catch { }

                newLien.LienStatus.RepairListSentToNegotiator = new DealMaker.ShortSale.ShortSaleDocumentShortSaleInfoLiensLienLienStatusRepairListSentToNegotiator();
                try { newLien.LienStatus.RepairListSentToNegotiator.Checked = oldLien.LienStatus.RepairListSentToNegotiator.Checked; }
                catch { }
                try { newLien.LienStatus.RepairListSentToNegotiator.Date = oldLien.LienStatus.RepairListSentToNegotiator.Date; }
                catch { }
                try { newLien.LienStatus.RepairListSentToNegotiator.Who = oldLien.LienStatus.RepairListSentToNegotiator.Who; }
                catch { }

                newLien.LienStatus.RequiredDocsRecorded = new DealMaker.ShortSale.ShortSaleDocumentShortSaleInfoLiensLienLienStatusRequiredDocsRecorded();
                try { newLien.LienStatus.RequiredDocsRecorded.Checked = oldLien.LienStatus.RequiredDocsRecorded.Checked; }
                catch { }
                try { newLien.LienStatus.RequiredDocsRecorded.Date = oldLien.LienStatus.RequiredDocsRecorded.Date; }
                catch { }
                try { newLien.LienStatus.RequiredDocsRecorded.Who = oldLien.LienStatus.RequiredDocsRecorded.Who; }
                catch { }

                newLien.LienStatus.RequiredDocsScanned = new DealMaker.ShortSale.ShortSaleDocumentShortSaleInfoLiensLienLienStatusRequiredDocsScanned();
                try { newLien.LienStatus.RequiredDocsScanned.Checked = oldLien.LienStatus.RequiredDocsScanned.Checked; }
                catch { }
                try { newLien.LienStatus.RequiredDocsScanned.Date = oldLien.LienStatus.RequiredDocsScanned.Date; }
                catch { }
                try { newLien.LienStatus.RequiredDocsScanned.Who = oldLien.LienStatus.RequiredDocsScanned.Who; }
                catch { }

                newLien.LienStatus.ReturnOriginalsToHomeOwner = new DealMaker.ShortSale.ShortSaleDocumentShortSaleInfoLiensLienLienStatusReturnOriginalsToHomeOwner();
                try { newLien.LienStatus.ReturnOriginalsToHomeOwner.Checked = oldLien.LienStatus.ReturnOriginalsToHomeOwner.Checked; }
                catch { }
                try { newLien.LienStatus.ReturnOriginalsToHomeOwner.Date = oldLien.LienStatus.ReturnOriginalsToHomeOwner.Date; }
                catch { }
                try { newLien.LienStatus.ReturnOriginalsToHomeOwner.Who = oldLien.LienStatus.ReturnOriginalsToHomeOwner.Who; }
                catch { }

                newLien.LienStatus.SellerAgreedToLetterTerms = new DealMaker.ShortSale.ShortSaleDocumentShortSaleInfoLiensLienLienStatusSellerAgreedToLetterTerms();
                try { newLien.LienStatus.SellerAgreedToLetterTerms.Checked = oldLien.LienStatus.SellerAgreedToLetterTerms.Checked; }
                catch { }
                try { newLien.LienStatus.SellerAgreedToLetterTerms.Date = oldLien.LienStatus.SellerAgreedToLetterTerms.Date; }
                catch { }
                try { newLien.LienStatus.SellerAgreedToLetterTerms.Who = oldLien.LienStatus.SellerAgreedToLetterTerms.Who; }
                catch { }

                newLien.LienStatus.SSPacketComplete = new DealMaker.ShortSale.ShortSaleDocumentShortSaleInfoLiensLienLienStatusSSPacketComplete();
                try { newLien.LienStatus.SSPacketComplete.Checked = oldLien.LienStatus.SSPacketComplete.Checked; }
                catch { }
                try { newLien.LienStatus.SSPacketComplete.Date = oldLien.LienStatus.SSPacketComplete.Date; }
                catch { }
                try { newLien.LienStatus.SSPacketComplete.Who = oldLien.LienStatus.SSPacketComplete.Who; }
                catch { }

                newLien.LienStatus.SSPacketReviewed = new DealMaker.ShortSale.ShortSaleDocumentShortSaleInfoLiensLienLienStatusSSPacketReviewed();
                try { newLien.LienStatus.SSPacketReviewed.Checked = oldLien.LienStatus.SSPacketReviewed.Checked; }
                catch { }
                try { newLien.LienStatus.SSPacketReviewed.Date = oldLien.LienStatus.SSPacketReviewed.Date; }
                catch { }
                try { newLien.LienStatus.SSPacketReviewed.Who = oldLien.LienStatus.SSPacketReviewed.Who; }
                catch { }

                newLien.LienStatus.SSPacketSentToLienHolder = new DealMaker.ShortSale.ShortSaleDocumentShortSaleInfoLiensLienLienStatusSSPacketSentToLienHolder();
                try { newLien.LienStatus.SSPacketSentToLienHolder.Checked = oldLien.LienStatus.SSPacketSentToLienHolder.Checked; }
                catch { }
                try { newLien.LienStatus.SSPacketSentToLienHolder.Date = oldLien.LienStatus.SSPacketSentToLienHolder.Date; }
                catch { }
                try { newLien.LienStatus.SSPacketSentToLienHolder.Who = oldLien.LienStatus.SSPacketSentToLienHolder.Who; }
                catch { }

                newLien.LienStatus.SSPacketVerifiedReceived = new DealMaker.ShortSale.ShortSaleDocumentShortSaleInfoLiensLienLienStatusSSPacketVerifiedReceived();
                try { newLien.LienStatus.SSPacketVerifiedReceived.Checked = oldLien.LienStatus.SSPacketVerifiedReceived.Checked; }
                catch { }
                try { newLien.LienStatus.SSPacketVerifiedReceived.Date = oldLien.LienStatus.SSPacketVerifiedReceived.Date; }
                catch { }
                try { newLien.LienStatus.SSPacketVerifiedReceived.Who = oldLien.LienStatus.SSPacketVerifiedReceived.Who; }
                catch { }


                newLiens.Add(newLien);
            }

            newSSD.ShortSaleInfo.Liens.Lien = newLiens.ToArray(); 
            return newSSD;
        }

    }
}
