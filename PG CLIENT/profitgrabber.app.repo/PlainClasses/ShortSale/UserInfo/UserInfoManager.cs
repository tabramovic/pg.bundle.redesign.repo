﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

//Added
using DealMaker.ShortSale.Interfaces;
using DealMaker.ShortSale.Utils;

namespace DealMaker.ShortSale.UserInfo
{
    public sealed class UserInfoManager : IExternalDocumentControl
    {
        static readonly UserInfoManager _instance = new UserInfoManager();

        static UserInfoManager()
        { }

        UserInfoManager()
        { }

        public static UserInfoManager Instance
        {
            get { return _instance; }
        }

        string _path = string.Empty;
        UserInfo _userInfo = new UserInfo();

        public UserInfo P_UserInfo
        {
            get { return _userInfo; }
            set { _userInfo = value; }
        }

        string GetFilePath()
        {
            if (!Directory.Exists(_path + @"\" + StringStore.SavedExtDocumentsFolder + @"\UserInfo\"))
                Directory.CreateDirectory(_path + @"\" + StringStore.SavedExtDocumentsFolder + @"\UserInfo\");

            return _path + @"\" + StringStore.SavedExtDocumentsFolder + @"\UserInfo\UserInfoData.xml";
        }



        #region IExternalDocumentControl Members

        public void SetDocumentName(string path, string documentId)
        {
            _path = path;
        }

        public bool LoadData()
        {
            TextReader r = null;
            try
            {
                XmlSerializer s = new XmlSerializer(typeof(UserInfo));
                r = new StreamReader(GetFilePath());
                _userInfo = s.Deserialize(r) as UserInfo;
            }
            catch (Exception ex)
            {
                return false;
            }
            finally
            {
                if (null != r)
                    r.Close();
            }

            return true;
        }

        public bool SaveData()
        {
            TextWriter w = null;
            try
            {
                XmlSerializer s = new XmlSerializer(typeof(UserInfo));
                w = new StreamWriter(GetFilePath());
                s.Serialize(w, _userInfo);
            }
            catch (Exception ex)
            {
                return false;
            }
            finally
            {
                if (null != w)
                    w.Close();
            }

            return true;
        }

        #endregion
    }
}
