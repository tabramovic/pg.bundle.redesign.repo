using System;

namespace DealMaker.ShortSale.Interfaces
{
	/// <summary>
	/// Summary description for IDisplayControlObject.
	/// </summary>
	public interface IDisplayControlObject
	{
		void RefreshGUIOnChange();
	}
}
