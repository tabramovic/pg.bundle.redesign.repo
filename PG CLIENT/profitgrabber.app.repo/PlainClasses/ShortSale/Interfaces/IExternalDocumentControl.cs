﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DealMaker.ShortSale.Interfaces
{
    public interface IExternalDocumentControl
    {
        void SetDocumentName(string path, string documentId);
        
        bool LoadData();

        bool SaveData();        
    }
}
