﻿using DataAppendService.Lib.Models.IDI;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DealMaker.PlainClasses.SkipTrace
{
	public class SkipTraceHelper
	{
		public static bool TestCreateAppendedSubGroups(int nodeId, out int outEmailFoundNodeId, out int outMobileFoundNodeId, out int outOnlyResidentalPhoneFoundNodeId, out int outContactInfoNotFoundNodeId)
		{
			const int NotInitialized = -1;
			const string EmailFound = "ST Email Found";
			const string MobileFound = "ST Mobile Found";
			const string OnlyResidentialPhoneFound = "ST Only Residential Phone Found";
			const string ContactInfoNotFound = "ST Contact Info Not Found";

			var emailFoundNodeId = NotInitialized;
			var mobileFoundNodeId = NotInitialized;
			var onlyResidentalPhoneFoundNodeId = NotInitialized;
			var contactInfoNotFoundNodeId = NotInitialized;

			var childNodes = ApplicationHelper.GetChildNodeIds(nodeId);

			childNodes.ForEach(kvp => { if (kvp.Value == EmailFound) emailFoundNodeId = kvp.Key; });
			childNodes.ForEach(kvp => { if (kvp.Value == MobileFound) mobileFoundNodeId = kvp.Key; });
			childNodes.ForEach(kvp => { if (kvp.Value == OnlyResidentialPhoneFound) onlyResidentalPhoneFoundNodeId = kvp.Key; });
			childNodes.ForEach(kvp => { if (kvp.Value == ContactInfoNotFound) contactInfoNotFoundNodeId = kvp.Key; });

			var refreshNode = (-1 == emailFoundNodeId || -1 == mobileFoundNodeId || -1 == onlyResidentalPhoneFoundNodeId || -1 == contactInfoNotFoundNodeId) ? true : false;

			if (-1 == emailFoundNodeId)
				ApplicationHelper.CreateNode(nodeId, string.Empty, EmailFound, out emailFoundNodeId, Connection.getInstance().HatchConn);

			if (-1 == mobileFoundNodeId)
				ApplicationHelper.CreateNode(nodeId, string.Empty, MobileFound, out mobileFoundNodeId, Connection.getInstance().HatchConn);

			if (-1 == onlyResidentalPhoneFoundNodeId)
				ApplicationHelper.CreateNode(nodeId, string.Empty, OnlyResidentialPhoneFound, out onlyResidentalPhoneFoundNodeId, Connection.getInstance().HatchConn);

			if (-1 == contactInfoNotFoundNodeId)
				ApplicationHelper.CreateNode(nodeId, string.Empty, ContactInfoNotFound, out contactInfoNotFoundNodeId, Connection.getInstance().HatchConn);

			outEmailFoundNodeId = emailFoundNodeId;
			outMobileFoundNodeId = mobileFoundNodeId;
			outOnlyResidentalPhoneFoundNodeId = onlyResidentalPhoneFoundNodeId;
			outContactInfoNotFoundNodeId = contactInfoNotFoundNodeId;

			return refreshNode;
		}

		public static TreeNode GetTreeNode(TreeNode tn, int groupId)
		{
			if (((Node)tn.Tag).nodeId == groupId)
				return tn;

			foreach (TreeNode child in tn.Nodes)
			{
				TreeNode tnRes = GetTreeNode(child, groupId);
				if (null != tnRes)
					return tnRes;
			}

			return null;
		}

		public static void AssignSkiptraceDataToChildNodes(int selectedNodeId, int emailFoundNodeId, int mobileFoundNodeId, int onlyResidentalPhoneFoundNodeId, int contactInfoNotFoundNodeId)
		{
			List<Guid> emailFoundGroup = new List<Guid>();
			List<Guid> mobileFoundGroup = new List<Guid>();
			List<Guid> onlyResidentalPhoneFoundGroup = new List<Guid>();
			List<Guid> contactInfoNotFoundGroup = new List<Guid>();

			var propertyItemIds = ApplicationHelper.GetPropertyItemIDsInNode(selectedNodeId);
			foreach (var piId in propertyItemIds)
			{
				SkiptraceClientResultset skipTraceData = null;
				var pi = ApplicationHelper.LoadPropertyItem(piId);

				if (pi != null)
				{
					if (!string.IsNullOrWhiteSpace(pi.Owner?.ST_P1_RESPONSE))
					{
						try
						{
							skipTraceData = JsonConvert.DeserializeObject<DataAppendService.Lib.Models.IDI.SkiptraceClientResultset>(pi.Owner?.ST_P1_RESPONSE);
						}
						catch
						{
							//log inappropriate JSON format (exception in deserializing) + stack trace into log
							skipTraceData = null;
						}
					}

					bool hasEmail = false, hasMobile = false, hasPhone = false;
					emailFoundGroup.Clear();
					mobileFoundGroup.Clear();
					onlyResidentalPhoneFoundGroup.Clear();
					contactInfoNotFoundGroup.Clear();

					if (!string.IsNullOrWhiteSpace(pi.Owner?.STEmail) || (skipTraceData?.HasAnyEmail ?? false))
					{
						emailFoundGroup.Add(piId);
						hasEmail = true;
					}

					if (!string.IsNullOrWhiteSpace(pi.Owner?.STCellphone) || (skipTraceData?.HasAnyMobilePhone ?? false))
					{
						mobileFoundGroup.Add(piId);
						hasMobile = true;
					}

					if (!hasMobile && (!string.IsNullOrWhiteSpace(pi.Owner?.STLandline) || (skipTraceData?.HasAnyResidentialPhone ?? false)))
					{
						onlyResidentalPhoneFoundGroup.Add(piId);
						hasPhone = true;
					}

					if (!hasEmail && !hasMobile && !hasPhone)
						contactInfoNotFoundGroup.Add(piId);

					foreach (var id in emailFoundGroup)
						ApplicationHelper.AssignPropertyItemToGroup(emailFoundNodeId, id);

					foreach (var id in mobileFoundGroup)
						ApplicationHelper.AssignPropertyItemToGroup(mobileFoundNodeId, piId);

					foreach (var id in onlyResidentalPhoneFoundGroup)
						ApplicationHelper.AssignPropertyItemToGroup(onlyResidentalPhoneFoundNodeId, piId);

					foreach (var id in contactInfoNotFoundGroup)
						ApplicationHelper.AssignPropertyItemToGroup(contactInfoNotFoundNodeId, piId);
				}
			}
		}
	}
}
