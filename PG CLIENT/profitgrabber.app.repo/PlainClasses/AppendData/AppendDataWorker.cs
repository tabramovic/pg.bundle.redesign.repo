﻿using System;
using System.Collections;
using System.Text;
using System.Net;
using System.IO;
using Newtonsoft.Json;
using DataAppendService.Lib.Models.IDI;

namespace DealMaker.PlainClasses.AppendData
{
    public enum AppendDataType
    {
        LL,
        CP,
        EM,
        All,
        AllExceptAlreadyMatched
    }
    public class AppendDataWorker
    {
        const string EmailFound = "ST Email Found";
        const string MobileFound = "ST Mobile Found";
        const string OnlyResidentialPhoneFound = "ST Only Residential Phone Found";
        const string ContactInfoNotFound = "ST Contact Info Not Found"; //ST Contact Info Not Found (if no Mobile or Residential or Email is found)

        int emailFoundNodeId = -1;
        int mobileFoundNodeId = -1;
        int onlyResidentalPhoneFoundNodeId = -1;
        int contactInfoNotFoundNodeId = -1;


        string AppendDataUrl = "https://www.profitgrabber.com/DataAppendService/api/append";
        //string AppendDataUrl = "https://www.profitgrabber.com/DataAppendService.Development/api/append";
        //string AppendDataUrl = "http://localhost:56420/api/append";

        private bool stopProcessing = false;

        public event SubGroupCreated OnSubGroupCreated;
        public delegate void SubGroupCreated(int nodeId);

        public event ItemIDsLoadedEventHandler OnItemIDsLoaded;
        public delegate void ItemIDsLoadedEventHandler(int numberOfItems);

        public event ItemLoadedEventHandler OnItemLoaded;
        public delegate void ItemLoadedEventHandler(string fullName, string address, string zip);

        public event AppendDataRetrievedEventHandler OnLandLineRetrieved;
        public event AppendDataRetrievedEventHandler OnCellRetrieved;
        public delegate void AppendDataRetrievedEventHandler(bool success, string fullName, string address, string zip, string phoneNr);

        public event AppendEmailRetrievedEventHandler OnEmailRetrieved;
        public delegate void AppendEmailRetrievedEventHandler(bool success, string fullName, string address, string zip, string email);

        public event ItemProcessedEventHandler OnItemProcessed;
        public delegate void ItemProcessedEventHandler();

        public event ItemSkippedEventHandler OnItemSkipped;
        public delegate void ItemSkippedEventHandler(bool name, bool address, bool zip);

        public event ProcessFinishedEventHandler OnProcessFinished;
        public delegate void ProcessFinishedEventHandler(bool success, string error, int matched, int total);

        public event SkipTraceDataRetrieved OnSkipTraceDataRetrieved;
        public delegate void SkipTraceDataRetrieved(SkiptraceClientResultset stcsr);

        public Action NotEnoughCredits;

        int nodeId;
        AppendDataType appendType;

        public Guid PIId { get; set; }

        public AppendDataWorker()
        { }

        public AppendDataWorker(int nodeId, AppendDataType appendType)
        {
            this.nodeId = nodeId;
            this.appendType = appendType;
        }

        public AppendDataWorker(int nodeId, Guid piId)
        {
            this.nodeId = nodeId;
            this.PIId = piId;
            this.appendType = AppendDataType.All;
        }

        //public void AppendDataForNodeId(int nodeId)
        //{
        //    this.nodeId = nodeId;            
        //}

        void TestCreateAppendedSubGroups(int nodeId)
        {
            var childNodes = ApplicationHelper.GetChildNodeIds(nodeId);

            childNodes.ForEach(kvp => { if (kvp.Value == EmailFound) emailFoundNodeId = kvp.Key; });
            childNodes.ForEach(kvp => { if (kvp.Value == MobileFound) mobileFoundNodeId = kvp.Key; });
            childNodes.ForEach(kvp => { if (kvp.Value == OnlyResidentialPhoneFound) onlyResidentalPhoneFoundNodeId = kvp.Key; });
            childNodes.ForEach(kvp => { if (kvp.Value == ContactInfoNotFound) contactInfoNotFoundNodeId = kvp.Key; });

            var refreshNode = (-1 == emailFoundNodeId || -1 == mobileFoundNodeId || -1 == onlyResidentalPhoneFoundNodeId || -1 == contactInfoNotFoundNodeId) ? true : false;

            if (-1 == emailFoundNodeId)
                ApplicationHelper.CreateNode(nodeId, string.Empty, EmailFound, out emailFoundNodeId, Connection.getInstance().HatchConn);

            if (-1 == mobileFoundNodeId)
                ApplicationHelper.CreateNode(nodeId, string.Empty, MobileFound, out mobileFoundNodeId, Connection.getInstance().HatchConn);

            if (-1 == onlyResidentalPhoneFoundNodeId)
                ApplicationHelper.CreateNode(nodeId, string.Empty, OnlyResidentialPhoneFound, out onlyResidentalPhoneFoundNodeId, Connection.getInstance().HatchConn);

            if (-1 == contactInfoNotFoundNodeId)
                ApplicationHelper.CreateNode(nodeId, string.Empty, ContactInfoNotFound, out contactInfoNotFoundNodeId, Connection.getInstance().HatchConn);

            if (refreshNode)
                if (null != OnSubGroupCreated)
                    OnSubGroupCreated(nodeId);
        }

        public void StopProcessing()
        {
            stopProcessing = true;
        }

        public async System.Threading.Tasks.Task StartProcessing()
        {
            await System.Threading.Tasks.Task.Delay(500);
            TestCreateAppendedSubGroups(nodeId);

            var propertyItemsInANode = ApplicationHelper.GetPropertyItemsInNode(nodeId);

            if (null != OnItemIDsLoaded)
            {
                OnItemIDsLoaded(propertyItemsInANode.Count);
                System.Diagnostics.Debug.WriteLine($"OnItemIDsLoaded ({propertyItemsInANode.Count})");
            }
            else
                System.Diagnostics.Debug.WriteLine("OnItemIDsLoaded is null");

            IDictionaryEnumerator ide = propertyItemsInANode.GetEnumerator();

            var matchedCnt = 0;
            var totalProcessCnt = 0;

            while (ide.MoveNext() && !stopProcessing)
            {
                //System.Threading.Thread.Sleep(100); //delay is here so that past retrieved record stays on screen for that period
                PropertyItem pi = ApplicationHelper.LoadPropertyItem((Guid)ide.Value);

                if (null != pi)
                {
                    var fullName = pi.Owner?.FullName;
                    var firstName = pi.Owner?.FirstName;
                    var lastName = pi.Owner?.LastName;
                    var siteAddress = pi.Owner?.SiteAddress?.FullSiteStreetAddress;
                    var city = pi.Owner?.SiteAddress?.SiteCity;
                    var state = pi.Owner?.SiteAddress?.SiteState;
                    var siteZip = pi.Owner?.SiteAddress?.SiteZIP;

                    var mailAddress = pi.Owner?.MailAddress?.FullSiteStreetAddress;
                    var mailZIP = pi.Owner?.MailAddress?.SiteZIP;

                    var address = !string.IsNullOrEmpty(mailAddress) && !string.IsNullOrEmpty(mailZIP) ? mailAddress : siteAddress;
                    var zip = !string.IsNullOrEmpty(mailAddress) && !string.IsNullOrEmpty(mailZIP) ? mailZIP : siteZip;

                    var msg = !string.IsNullOrEmpty(mailAddress) && !string.IsNullOrEmpty(mailZIP) ? $"using mail Address" : $"using site address";
                    System.Diagnostics.Debug.WriteLine($"Append Data Request: {msg} : Address:{address} ZIP:{zip} LastName:{(!string.IsNullOrEmpty(lastName) ? lastName : "N/A")}");

                    if (null != OnItemLoaded)
                        OnItemLoaded(fullName, siteAddress, siteZip);

                    if (string.IsNullOrEmpty(fullName) || string.IsNullOrEmpty(siteAddress) || string.IsNullOrEmpty(siteZip))
                    {
                        if (null != OnItemSkipped)
                            OnItemSkipped(
                                string.IsNullOrEmpty(fullName),
                                string.IsNullOrEmpty(siteAddress),
                                string.IsNullOrEmpty(siteZip)
                                );

                        continue;
                    }


                    bool hasSkipTracedData = false;

                    if (!string.IsNullOrWhiteSpace(pi.Owner?.ST_P1_RESPONSE))
                    {
                        try
                        {
                            var skipTracedData = JsonConvert.DeserializeObject<DataAppendService.Lib.Models.IDI.SkiptraceClientResultset>(pi.Owner.ST_P1_RESPONSE);
                            hasSkipTracedData = skipTracedData?.IsSkipTraced ?? false;
                        }
                        catch (Exception ex)
                        {
                            hasSkipTracedData = false;
                        }
                    }

                    if (this.appendType == AppendDataType.AllExceptAlreadyMatched
                        && (
                                null != pi.Owner.ST_P1_RESPONSE && hasSkipTracedData
                                || !string.IsNullOrWhiteSpace(pi.Owner.STEmail)
                                || !string.IsNullOrWhiteSpace(pi.Owner.STCellphone)
                                || !string.IsNullOrWhiteSpace(pi.Owner.STLandline))
                           )
                    {
                        if (null != OnItemSkipped)
                            OnItemSkipped(
                                string.IsNullOrEmpty(fullName),
                                string.IsNullOrEmpty(siteAddress),
                                string.IsNullOrEmpty(siteZip)
                                );

                        continue;
                    }

                    bool appendRes = GetAppendedData(pi, fullName, firstName, lastName, address, city, state, zip, out var matchSuccess, out var error); ;
                    if (!appendRes)
                    {
                        if (null != OnProcessFinished)
                            OnProcessFinished(false, error, matchedCnt, totalProcessCnt);

                        break;
                    }

                    totalProcessCnt++;
                    if (matchSuccess)
                        matchedCnt++;
                }
            }

            if (null != OnProcessFinished)
                OnProcessFinished(true, string.Empty, matchedCnt, totalProcessCnt);
        }

        public void ProcessSingle()
        {
            System.Threading.Tasks.Task.Delay(100);
            TestCreateAppendedSubGroups(nodeId);
            PropertyItem pi = ApplicationHelper.LoadPropertyItem(PIId);
            OnItemIDsLoaded(1);
            bool matchSuccess = false;
            if (null != pi)
            {
                var fullName = pi.Owner?.FullName;
                var firstName = pi.Owner?.FirstName;
                var city = pi.Owner?.SiteAddress?.SiteCity;

                var lastName = pi.Owner?.LastName;
                var siteAddress = pi.Owner?.SiteAddress?.FullSiteStreetAddress;
                var state = pi.Owner?.SiteAddress?.SiteState;
                var siteZip = pi.Owner?.SiteAddress?.SiteZIP;

                var mailAddress = pi.Owner?.MailAddress?.FullSiteStreetAddress;
                var mailZIP = pi.Owner?.MailAddress?.SiteZIP;

                var address = !string.IsNullOrEmpty(mailAddress) && !string.IsNullOrEmpty(mailZIP) ? mailAddress : siteAddress;
                var zip = !string.IsNullOrEmpty(mailAddress) && !string.IsNullOrEmpty(mailZIP) ? mailZIP : siteZip;

                var msg = !string.IsNullOrEmpty(mailAddress) && !string.IsNullOrEmpty(mailZIP) ? $"using mail Address" : $"using site address";
                System.Diagnostics.Debug.WriteLine($"Append Data Request: {msg} : Address:{address} ZIP:{zip} LastName:{(!string.IsNullOrEmpty(lastName) ? lastName : "N/A")}");

                OnItemLoaded(fullName, siteAddress, siteZip);

                if (string.IsNullOrEmpty(fullName) || string.IsNullOrEmpty(siteAddress) || string.IsNullOrEmpty(siteZip))
                {
                    if (null != OnItemSkipped)
                        OnItemSkipped(
                            string.IsNullOrEmpty(fullName),
                            string.IsNullOrEmpty(siteAddress),
                            string.IsNullOrEmpty(siteZip)
                            );

                    if (null != OnProcessFinished)
                        OnProcessFinished(true, string.Empty, 0, 1);

                    return;
                }


                var appendRes = GetAppendedData(pi, fullName, firstName, lastName, address, city, state, zip, out matchSuccess, out var error);

                if (!appendRes)
                {
                    if (null != OnProcessFinished)
                        OnProcessFinished(false, error, 0, 1);
                }

                if (null != OnItemProcessed)
                    OnItemProcessed();

            }

            if (null != OnProcessFinished)
                OnProcessFinished(matchSuccess, string.Empty, 1, 1);
        }

        private bool GetAppendedData(PropertyItem pi, string fullName, string firstName, string lastName, string address, string city, string state, string zip, out bool matchResult, out string error)
        {
            bool result = true;
            matchResult = false;
            error = string.Empty;

            try
            {
                System.Diagnostics.Debug.WriteLine($"Request for: {appendType}");
                var request = (HttpWebRequest)WebRequest.Create(AppendDataUrl);

                var strToEnc = $"{address}{zip}";
                var hash = Utils.Crypto.EncryptString(strToEnc);
                var escapedHash = Uri.EscapeDataString(hash);
                var esacpedAddress = Uri.EscapeDataString(address);

                var decrStr = Utils.Crypto.DecryptString(hash);
                System.Diagnostics.Debug.Assert(strToEnc.Equals(decrStr));

                var postData = $"FirstName={firstName}";
                postData += $"&LastName={lastName}";
                postData += $"&Address={esacpedAddress}";
                postData += $"&City={city}";
                postData += $"&State={state}";
                postData += $"&ZIP={zip}";
                postData += $"&Hash={escapedHash}";
                postData += $"&PGID={Globals.UserRegistration.IDUser}";
                var data = Encoding.ASCII.GetBytes(postData);

                request.Method = "POST";
                request.ContentType = "application/x-www-form-urlencoded";
                request.ContentLength = data.Length;

                using (var stream = request.GetRequestStream())
                {
                    stream.Write(data, 0, data.Length);
                }

                var response = (HttpWebResponse)request.GetResponse();

                var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                if (!string.IsNullOrEmpty(responseString))
                    pi.Owner.ST_P1_RESPONSE = responseString;

                var responseAsObj = JsonConvert.DeserializeObject<DataAppendService.Lib.Models.IDI.SkiptraceClientResultset>(responseString.ToString());
                matchResult = null != responseAsObj && responseAsObj.IsSkipTraced;

                if (!responseAsObj.HasAnyEmail)
                {
                    System.Diagnostics.Debug.WriteLine($"No Email Found");

                    if (null != OnEmailRetrieved)
                        OnEmailRetrieved(false, fullName, address, zip, string.Empty);
                }
                else
                {
                    var email = responseAsObj.Emails[0];
                    System.Diagnostics.Debug.WriteLine($"Got email:{email}");

                    pi.Owner.STLastModified = DateTime.Now;
                    pi.Owner.STEmail = email;

                    if (null != OnEmailRetrieved)
                        OnEmailRetrieved(true, fullName, address, zip, email);

                }

                if (!responseAsObj.HasAnyResidentialPhone)
                {
                    if (null != OnLandLineRetrieved)
                        OnLandLineRetrieved(false, fullName, address, zip, string.Empty);
                }

                if (!responseAsObj.HasAnyMobilePhone)
                {
                    if (null != OnCellRetrieved)
                        OnCellRetrieved(false, fullName, address, zip, string.Empty);
                }

                if (responseAsObj.HasAnyResidentialPhone || responseAsObj.HasAnyMobilePhone)
                {
                    var phoneNr = responseAsObj.HasAnyResidentialPhone ? responseAsObj?.ResidentialPhones[0] : string.Empty;
                    var cellNr = responseAsObj.HasAnyMobilePhone ? responseAsObj?.MobilePhones[0] : string.Empty;

                    System.Diagnostics.Debug.WriteLine($"Got phone#:{phoneNr}, got cell#:{cellNr}");

                    pi.Owner.STLastModified = DateTime.Now;
                    pi.Owner.STLandline = phoneNr;
                    pi.Owner.STCellphone = cellNr;

                    if (!string.IsNullOrEmpty(phoneNr) && null != OnLandLineRetrieved)
                        OnLandLineRetrieved(true, fullName, address, zip, phoneNr);

                    if (!string.IsNullOrEmpty(cellNr) && null != OnCellRetrieved)
                        OnCellRetrieved(true, fullName, address, zip, cellNr);
                }
                else
                    System.Diagnostics.Debug.WriteLine($"Got NOTHING for phone#, got NOTHING for cell#");

                if (matchResult)
                    ApplicationHelper.SavePropertyItem(pi);
                if (responseAsObj.HasAnyEmail)
                    ApplicationHelper.AssignPropertyItemToGroup(emailFoundNodeId, pi.IdPropertyItem);
                if (responseAsObj.HasAnyMobilePhone)
                    ApplicationHelper.AssignPropertyItemToGroup(mobileFoundNodeId, pi.IdPropertyItem);
                if (!responseAsObj.HasAnyMobilePhone && responseAsObj.HasAnyResidentialPhone)
                    ApplicationHelper.AssignPropertyItemToGroup(onlyResidentalPhoneFoundNodeId, pi.IdPropertyItem);
                if (!responseAsObj.HasAnyMobilePhone && !responseAsObj.HasAnyResidentialPhone && !responseAsObj.HasAnyEmail)
                    ApplicationHelper.AssignPropertyItemToGroup(contactInfoNotFoundNodeId, pi.IdPropertyItem);

                if (null != OnItemProcessed)
                    OnItemProcessed();

                if (null != OnSkipTraceDataRetrieved && null != responseAsObj)
                    OnSkipTraceDataRetrieved(responseAsObj);
            }
            catch (WebException e)
            {
                if (null != e.Response && HttpStatusCode.NotFound == ((HttpWebResponse)e.Response).StatusCode)
                {
                    error = "Not Found";

                    if (null != OnItemProcessed)
                        OnItemProcessed();

                    return true;
                }

                if (null != e.Response && HttpStatusCode.Unauthorized == ((HttpWebResponse)e.Response).StatusCode)
                {
                    if (null != NotEnoughCredits)
                    {
                        NotEnoughCredits();
                        error = "Not Enough Credits";
                        return false;
                    }
                }

                //server unavailable
                error = "Server unavailable";
                return false;
            }
            catch
            {
                //general exception
                error = "Unknown return data format.";
                return false;
            }

            return result;
        }
    }
}
