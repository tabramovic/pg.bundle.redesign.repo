﻿using System;
using System.Net.Http;
using System.Globalization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Threading.Tasks;

namespace DealMaker.PlainClasses.AppendData
{
    public class AppendDataWalletCheck
    {
        const string Url = "https://www.profitgrabber.com/DataAppendService/api/Wallet/";
        //const string Url = "https://www.profitgrabber.com/DataAppendService.Development/api/Wallet/";
        //const string Url = "http://localhost:56420/api/Wallet/";        


        const int MatchRateInPerc = 100;         //100%
        const decimal SingleMatchCost = 0.30m;   //30c

        Guid _pgId;
        int _nodeId;

        public AppendDataWalletCheck(int nodeId, Guid pgId)
        {
            _pgId = pgId;
            _nodeId = nodeId;
        }

        public async Task<Tuple<decimal, decimal>> Check()
        {            
            var nrOfRecords = ApplicationHelper.GetPropertyCountInNode(_nodeId);
            using (var client = new HttpClient())
            {
                var url = $"{Url}/{_pgId}";

                try
                {                    
                    var response = await client.GetAsync(url);

                    response.EnsureSuccessStatusCode();
                    var res = await response.Content.ReadAsStringAsync();
                    
                    var balance = WalletBalance.FromJson(res);

                    decimal availableAmount = balance.Available / 100;
                    decimal estimatedAmount = nrOfRecords * SingleMatchCost * MatchRateInPerc / 100;
                    
                    return new Tuple<decimal, decimal>(estimatedAmount, availableAmount);
                }
                catch (HttpRequestException hre)
                {
                    //log error                    
                    throw;
                }
                catch (Exception e)
                {
                    //log error
                    throw;
                }
            }
        }
    }
                
    public partial class WalletBalance
    {
        [JsonProperty("Available")]
        public decimal Available { get; set; }

        [JsonProperty("Reserved")]
        public decimal Reserved { get; set; }
    }

    public partial class WalletBalance
    {
        public static WalletBalance FromJson(string json) => JsonConvert.DeserializeObject<WalletBalance>(json);
    }

    public static class Serialize
    {
        public static string ToJson(this WalletBalance self) => JsonConvert.SerializeObject(self);
    }

    internal static class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters =
        {
            new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
        },
        };
    }    
}
