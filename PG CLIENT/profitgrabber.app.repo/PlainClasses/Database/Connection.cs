#region � Using �
using System;
using System.Data.SqlClient; 
#endregion � Using �
#region � Namespace DealMaker �
namespace DealMaker  
{
	#region � Class Connection �
	/// <summary>
	/// oot Connection.
	/// </summary>
	public class Connection 
	{
 
		#region � Data �		
		static Connection instance;
		private SqlConnection ootConnection;
		#endregion � Data �

		#region � CTR �
		private Connection()
		{
			//sasa, 06.11.2005: map all connection strings to one
			
			//this.ootConnection = new SqlConnection(@"Data Source=" + Globals.DatabaseServer + ";Initial Catalog=" + Globals.DatabaseName + ";Integrated Security=SSPI;");
			this.ootConnection = new SqlConnection(Globals.GetConnectionString());
		}
		#endregion � CTR �

		public static void Reset()
		{												
			Connection.instance = null;			
		}

		#region � getInstance �
		public static Connection getInstance()
		{	
			if (null == instance)
				instance = new Connection();						
			
			return instance;
		}
		#endregion � getInstance �

		#region � HatchConn �
		public SqlConnection HatchConn
		{
			get
			{
				if (System.Data.ConnectionState.Closed == this.ootConnection.State)
				{
					this.ootConnection.Open();
				}
				return this.ootConnection;
			}
		}
		#endregion � HatchConn �
	}
	#endregion � Class Connection �
}
#endregion � Namespace DealMaker �