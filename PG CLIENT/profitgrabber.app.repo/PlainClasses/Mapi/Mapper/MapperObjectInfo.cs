#region � Using �
using System;
using System.Collections;
#endregion � Using �
#region � Namespace DealMaker �
namespace DealMaker
{
	#region � Class MapperObjectInfo �
	/// <summary>
	/// MapperObjectInfo Class - Contains data about mapping property and propertyDescription
	/// </summary>
	public class MapperObjectInfo
	{
		#region � Data �
		private string propertyName = string.Empty;
		private string propertyDesc	= string.Empty;
		private int	   propertyPos	= 0;	
		#endregion � Data �
		#region � Properties �
		#region � PropertyName �
		public string PropertyName
		{
			get { return this.propertyName; }
			set { this.propertyName = value; }
		}
		#endregion � PropertyName �
		#region � PropertyDesc �
		public string PropertyDesc
		{
			get { return this.propertyDesc; }
			set { this.propertyDesc = value; }
		}
		#endregion � PropertyDesc �
		#region PropertyPos 
		public int PropertyPos
		{
			get { return this.propertyPos; }
			set { this.propertyPos = value; }
		}
		#endregion PropertyPos 
		#endregion � Properties �
		#region � CTR �
		public MapperObjectInfo()
		{
		}

		public MapperObjectInfo(string propertyName, string propertyDesc)
		{
			this.propertyName = propertyName;
			this.propertyDesc = propertyDesc;			
		}
		#endregion � CTR �		
		
		

		#region � Equals �
		public override bool Equals(Object obj) 
		{
			//Check for null and compare run-time types.
			if (obj == null || GetType() != obj.GetType()) 
				return false;
			MapperObjectInfo moi = (MapperObjectInfo)obj;
			if (this.PropertyName == moi.PropertyName && this.PropertyDesc == moi.PropertyDesc)
				return true;
			else
				return false;
			
		}
		#endregion � Equals �
		#region � GetHashCode �
		public override int GetHashCode() 
		{
			string tempString = this.propertyDesc + this.propertyName;
			return this.propertyName.GetHashCode() ^ this.propertyDesc.GetHashCode() ^ tempString.GetHashCode();
		}
		#endregion � GetHashCode �				
	}
	#endregion � Class MapperObjectInfo �
}
#endregion � Namespace DealMaker �