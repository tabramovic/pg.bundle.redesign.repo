#region � Using �
using System;
using System.Collections;
#endregion � Using �
#region � Namespace DealMaker �
namespace DealMaker
{
	#region � Class MapperSourceInfo �
	/// <summary>
	/// MapperSourceInfo Class - contains data about source
	/// </summary>
	public class MapperSourceInfo
	{
		#region � Data �
		private int columnIndex = -1;
		private string columnDesc = string.Empty;
		#endregion � Data �
		#region � Properties �
		#region � ColumnIndex  �
		public int ColumnIndex 
		{
			get { return this.columnIndex; }
			set { this.columnIndex = value; }
		}
		#endregion � ColumnIndex  �
		#region � ColumnDesc �
		public string ColumnDesc
		{
			get { return this.columnDesc; } 
			set { this.columnDesc = value; }
		}
		#endregion � ColumnDesc �
		#endregion � Properties �
		#region � CTR �
		public MapperSourceInfo()
		{
			this.columnIndex = -1;
			this.columnDesc = string.Empty;
		}

		public MapperSourceInfo(int columnIndex, string columnDesc)
		{
			this.columnIndex = columnIndex;
			this.columnDesc = columnDesc;
		}
		#endregion � CTR �				
		#region � Methods �
		#region � IsInitialized �
		public bool IsInitialized()
		{
			if (-1 == this.columnIndex)
				return false;
			
			return true;
		}
		#endregion � IsInitialized �
		#endregion � Methods �
	}
	#endregion � Class MapperSourceInfo �
}
#endregion � Namespace DealMaker �