using System;
using System.IO;
using System.Runtime.InteropServices;

namespace DealMaker.Mapi
{
	/// <summary>
	/// SimpleMapi class is a rudimental wraper for 
	/// Windows MAPI subsystem. 
	/// </summary>
	/// <remarks>
	/// Tested with 
	/// Outlook Express 6 (SP2)
	/// Mozilla Thunderbird version 0.7  
	/// </remarks>
	public class SimpleMapi
	{
		internal const string Delimiter = ";";

		[DllImport("MAPI32.DLL", CharSet=CharSet.Ansi,CallingConvention = CallingConvention.Winapi)]
		private static extern UInt32 MAPISendDocuments(
			UInt32 uiParam,
			string delimiter,
			string fullPaths,
			string fileNames,
			ulong reserved);

		public static void SendDocuments(string[] paths, string[] documents)
		{
			int i;
			string fullPaths = String.Empty;
			string fileNames = String.Empty;

			for (i = 0; i < paths.Length; i++)
				fullPaths += i == paths.Length - 1 ? 
							 paths[i] :
							 paths[i] + Delimiter;

			for (i = 0; i < documents.Length; i++)
				fileNames += i == documents.Length - 1 ? 
					documents[i] :
					documents[i] + Delimiter;

			MAPISendDocuments(0, Delimiter, fullPaths, fileNames, 0);
		}
	}
}