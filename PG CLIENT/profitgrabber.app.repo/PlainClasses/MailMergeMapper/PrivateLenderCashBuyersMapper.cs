using System;

namespace DealMaker
{
	/// <summary>
	/// Summary description for UserProperties.
	/// </summary>
	public class PrivateLenderCashBuyersMapper
	{
		private static string[] mappingFields = new string[] 
		{
			"Full Name",
		    "First Name",
		    "Last Name",			
			"Mail Address",
			"Mail City",			
			"Mail State",
			"Mail ZIP"			
		};
		

		public static string[] MappingFields
		{
			get {return PrivateLenderCashBuyersMapper.mappingFields;}
		}


        #region � GetStringFromPropertyItem �
        public static string GetStringFromPropertyItem(PropertyItem pi, MailMergeMapper mmm, string propertyName)
        {
            try
            {
                switch (propertyName)
                {
                    case "Full Name":
                        if (null != pi && null != pi.Owner)
                            return pi.Owner.FullName;
                        else
                            return string.Empty;

                    case "First Name":
                        if (null != pi && null != pi.Owner)
                            return pi.Owner.FirstName;
                        else
                            return string.Empty;

                    case "Last Name":
                        if (null != pi && null != pi.Owner)
                            return pi.Owner.LastName;
                        else
                            return string.Empty;                    

                    case "Mail Address":
                        if (null != pi && null != pi.Owner.MailAddress && string.Empty != pi.Owner.MailAddress.FullSiteStreetAddress.Trim())
                            return pi.Owner.MailAddress.FullSiteStreetAddress;                        
                        else
                            return string.Empty;
                        

                    case "Mail City":
                        if (null != pi && null != pi.Owner.MailAddress && string.Empty != pi.Owner.MailAddress.SiteCity.Trim())
                            return pi.Owner.MailAddress.SiteCity;
                        else                        
                            return string.Empty;
                        

                    case "Mail State":
                        if (null != pi && null != pi.Owner.MailAddress && string.Empty != pi.Owner.MailAddress.SiteState.Trim())
                            return pi.Owner.MailAddress.SiteState;                        
                        else
                            return string.Empty;
                        

                    case "Mail ZIP":
                        if (null != pi && null != pi.Owner.MailAddress && string.Empty != pi.Owner.MailAddress.SiteZIP.Trim())
                            return pi.Owner.MailAddress.SiteZIP;                        
                        else
                            return string.Empty;
                                            

                    default:
                        return string.Empty;
                }
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }
        #endregion � GetStringFromPropertyItem �


        public PrivateLenderCashBuyersMapper()
		{
			
		}
	}
}
