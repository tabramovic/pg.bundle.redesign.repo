using System;

namespace DealMaker
{
	/// <summary>
	/// Summary description for UserProperties.
	/// </summary>
	public class UserMapper
	{
		private static string[] userMappingFields = new string[] 
		{
			"First Name",
			"Last Name",
			"Full Name",
            "Company Name",
            "Company Address",
            "City",
            "State",
            "ZIP",
            "Marketing Nr.",
            "Direct Nr.",
            "Fax",            			
            "Email1",
            "Email2",
            "Buying Website",
            "Foreclosure Website",
        };
		

		public static string[] UserMappingFields
		{
			get {return UserMapper.userMappingFields;}
		}

		
		public string GetStringFromUserObject(User u, string propertyName)
		{
			switch (propertyName) 
			{
				case "First Name":
					return u.FirstName;
				case "Last Name":
					return u.LastName;
				case "Full Name":
					return u.FirstName + " " + u.LastName;

                case "Marketing Nr.":       //fall through to phoneNr
                case "Phone Nr.":
					return u.PhoneNr;

                case "Direct Nr.":
                    return u.DirectNumber;
                case "Address":             //fall through to companyAddress
                case "Company Address":
					return u.Address;
				case "City":
					return u.City;
				case "Fax":
					return u.Fax;
				case "State":
					return u.State;
				case "ZIP":
					return u.ZIP;
				case "Company Name":
					return u.CompanyName;
                case "Email1":
                    return u.Email;
                case "Email2":
                    return u.Email2;
                case "Buying Website":
                    return u.BuyingWebsite;
                case "Foreclosure Website":
                    return u.ForeclosureWebsite;
                default:
					return string.Empty;
			}
		}


		public UserMapper()
		{
			
		}
	}
}
