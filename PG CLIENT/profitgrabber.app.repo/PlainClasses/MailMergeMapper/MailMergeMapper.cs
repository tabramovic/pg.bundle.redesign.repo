#region � Using �
using System;
#endregion � Using �
#region � Namespace DealMaker �
namespace DealMaker
{
	#region � Class MailMergeMapper �
	/// <summary>
	/// Summary description for MailMergeMapper.
	/// </summary>
    /*
        Mail Merge Mapping Fields are following (as requested in specs):
		
        Full Owner Name
        First Owner Name
        Last Owner Name
        Salutation Name
        Full Site Street Address
        Site City
        Site State
        Site ZIP
        Site City/St
        Site City/St/ZIP
        Full Mail Street Address
        Mail City
        Mail State
        Mail ZIP
        Mail City/St
        Mail City/St/ZIP
        Listing Agent Full Name
        Listing Agent First Name
        Listing Agent Last Name
        Listing Agent Phone Number
        Listing Agent Fax Number
        Listing Agent Office
        Listing Agent Code
        Listing MLS Number
        Listing Price
        Offered Price (percentage * Listing Price) -- > extra added
        Alpha Offered Price (offered price spelled out) --> extra added
        Subdivision
        Legal Description
        Vesting
		
        //12 Jan 2006
        BarCode Id,
        Date
		
		
    */
    public class MailMergeMapper
	{
		#region � Data �
		private double percentage = 0;
		private static string[] mailMergeMappingFields = new string[] {
														   "Full Owner Name",
														   "First Owner Name",
														   "Last Owner Name",
														   "Salutation Name", 
														   "Site Street Address", 
														   "Site City",
														   "Site State",
														   "Site ZIP",
														   "Site City/St", 
														   "Site City/St/ZIP",
														   "Mail Street Address",
														   "Mail City",
														   "Mail State",
														   "Mail ZIP", 
														   "Mail City/St",
														   "Mail City/St/ZIP",
														   "Lst. Ag. Full Name",
														   "Lst. Ag. First Name",
														   "Lst. Ag. Last Name",
                                                           "Lst. Ag. Email",
														   "Lst. Ag. Phone Nr",
														   "Lst. Ag. Fax Nr",
														   "Lst. Ag. Office",
														   "Lst. Ag. Code",
														   "Lst. MLS Nr",
														   "Lst. Price",
														   Globals.OfferedPrice,
                                                           Globals.AlphaOfferedPrice,
                                                           "Sale Date",
														   "Subdivision",
														   "Legal Description",
														   "Vesting", 
														   "BarCode Id", 
														   "Date", 
                                                           "Date (Rich Format)"
													   };

		#endregion � Data �
		#region � CTR �
		public MailMergeMapper()
		{
			
		}
		#endregion � CTR �
		#region � Properties �
		#region � Percentage �
		public double Percentage
		{
			get {return this.percentage;}
			set {this.percentage = value;}
		}
		#endregion � Percentage �
		public static string[] MailMergeMappingFields
		{
			get { return MailMergeMapper.mailMergeMappingFields; }
		}
		#endregion � Properties �
		#region � GetStringFromPropertyItem �
		public static string GetStringFromPropertyItem (PropertyItem pi, MailMergeMapper mmm, string propertyName)
		{	
			try
			{
				switch (propertyName)
				{
					case "Full Owner Name":
						if (null != pi && null != pi.Owner)
							return pi.Owner.FullName;
						else
							return string.Empty;
							
					case "First Owner Name":
						if (null != pi && null != pi.Owner)
							return pi.Owner.FirstName;
						else
							return string.Empty;

					case "Last Owner Name":
						if (null != pi && null != pi.Owner)
							return pi.Owner.LastName;	
						else
							return string.Empty;

					case "Salutation Name":
						if (null != pi && null != pi.Owner)
							return pi.Owner.SalutationName;					
						else
							return string.Empty;

					case "Site Street Address":
						if (null != pi && null != pi.Owner.SiteAddress)
							return pi.Owner.SiteAddress.FullSiteStreetAddress;					
						else
							return string.Empty;

					case "Site City":
						if (null != pi && null != pi.Owner.SiteAddress)
							return pi.Owner.SiteAddress.SiteCity;					
						else
							return string.Empty;

					case "Site State":
						if (null != pi && null != pi.Owner.SiteAddress)
							return pi.Owner.SiteAddress.SiteState;					
						else
							return string.Empty;

					case "Site ZIP":
						if (null != pi && null != pi.Owner.SiteAddress)
							return pi.Owner.SiteAddress.SiteZIP;					
						else
							return string.Empty;

					case "Site City/St":
						if (null != pi && null != pi.Owner.SiteAddress)
							return pi.Owner.SiteAddress.SiteCitySiteState;					
						else
							return string.Empty;

					case "Site City/St/ZIP":
						if (null != pi && null != pi.Owner.SiteAddress)
							return pi.Owner.SiteAddress.SiteCitySiteStateSiteZIP;					
						else
							return string.Empty;

						//ta++, 12.10.2006:
						// Mail Address merge field maps to DB Mail Address field.
						// IF DB Mail Address field does not exist - we use SITE !!!
						//ta++, 13.10.2006.
						//There is a bug,  
							//If mail street is missing it pulls it from the site which is fine how ever it leaves the mail city and state and zip. 
							//Now what happens if the customer only has the missing mail STREET ADDRESS but 
							//	has mail city, state, zip the address will be mixed.  
						//Solution: Implement logic that defaults all mail merging fields to corresponding sites fields 
							//if and when any of the mail fields is blank.  
							//That means if the mail street is blank and mail city is present, 
							//	PG should use site street and site city. 

					case "Mail Street Address":
						if (null != pi && null != pi.Owner.MailAddress && 
							string.Empty != pi.Owner.MailAddress.FullSiteStreetAddress.Trim() &&
							string.Empty != pi.Owner.MailAddress.SiteCity.Trim() &&
							string.Empty != pi.Owner.MailAddress.SiteState.Trim() &&
							string.Empty != pi.Owner.MailAddress.SiteZIP.Trim() &&
							string.Empty != pi.Owner.MailAddress.SiteCitySiteState &&
							string.Empty != pi.Owner.MailAddress.SiteCitySiteStateSiteZIP
							)	
							return pi.Owner.MailAddress.FullSiteStreetAddress;					
						else
						{
							if (null != pi && null != pi.Owner.SiteAddress)
								return pi.Owner.SiteAddress.FullSiteStreetAddress;					
							else
								return string.Empty;							
						}

					case "Mail City":
						if (null != pi && null != pi.Owner.MailAddress &&
							string.Empty != pi.Owner.MailAddress.FullSiteStreetAddress.Trim() &&
							string.Empty != pi.Owner.MailAddress.SiteCity.Trim() &&
							string.Empty != pi.Owner.MailAddress.SiteState.Trim() &&
							string.Empty != pi.Owner.MailAddress.SiteZIP.Trim() &&
							string.Empty != pi.Owner.MailAddress.SiteCitySiteState &&
							string.Empty != pi.Owner.MailAddress.SiteCitySiteStateSiteZIP
							)	
							return pi.Owner.MailAddress.SiteCity;
						else
						{
							if (null != pi && null != pi.Owner.SiteAddress)
								return pi.Owner.SiteAddress.SiteCity;					
							else
								return string.Empty;
						}

					case "Mail State":
						if (null != pi && null != pi.Owner.MailAddress && 
							string.Empty != pi.Owner.MailAddress.FullSiteStreetAddress.Trim() &&
							string.Empty != pi.Owner.MailAddress.SiteCity.Trim() &&
							string.Empty != pi.Owner.MailAddress.SiteState.Trim() &&
							string.Empty != pi.Owner.MailAddress.SiteZIP.Trim() &&
							string.Empty != pi.Owner.MailAddress.SiteCitySiteState &&
							string.Empty != pi.Owner.MailAddress.SiteCitySiteStateSiteZIP
							)	
							return pi.Owner.MailAddress.SiteState;					
						else
						{
							if (null != pi && null != pi.Owner.SiteAddress)
								return pi.Owner.SiteAddress.SiteState;					
							else
								return string.Empty;
						}

					case "Mail ZIP":
						if (null != pi && null != pi.Owner.MailAddress && 
							string.Empty != pi.Owner.MailAddress.FullSiteStreetAddress.Trim() &&
							string.Empty != pi.Owner.MailAddress.SiteCity.Trim() &&
							string.Empty != pi.Owner.MailAddress.SiteState.Trim() &&
							string.Empty != pi.Owner.MailAddress.SiteZIP.Trim() &&
							string.Empty != pi.Owner.MailAddress.SiteCitySiteState &&
							string.Empty != pi.Owner.MailAddress.SiteCitySiteStateSiteZIP
							)	
							return pi.Owner.MailAddress.SiteZIP;
						else
						{
							if (null != pi && null != pi.Owner.SiteAddress)
								return pi.Owner.SiteAddress.SiteZIP;					
							else
								return string.Empty;
						}

					case "Mail City/St":
						if (null != pi && null != pi.Owner.MailAddress && 
							string.Empty != pi.Owner.MailAddress.FullSiteStreetAddress.Trim() &&
							string.Empty != pi.Owner.MailAddress.SiteCity.Trim() &&
							string.Empty != pi.Owner.MailAddress.SiteState.Trim() &&
							string.Empty != pi.Owner.MailAddress.SiteZIP.Trim() &&
							string.Empty != pi.Owner.MailAddress.SiteCitySiteState &&
							string.Empty != pi.Owner.MailAddress.SiteCitySiteStateSiteZIP
							)	
							return pi.Owner.MailAddress.SiteCitySiteState;
						else
						{
							if (null != pi && null != pi.Owner.SiteAddress)
								return pi.Owner.SiteAddress.SiteCitySiteState;					
							else
								return string.Empty;
						}

					case "Mail City/St/ZIP":
						if (null != pi && null != pi.Owner.MailAddress && 
							string.Empty != pi.Owner.MailAddress.FullSiteStreetAddress.Trim() &&
							string.Empty != pi.Owner.MailAddress.SiteCity.Trim() &&
							string.Empty != pi.Owner.MailAddress.SiteState.Trim() &&
							string.Empty != pi.Owner.MailAddress.SiteZIP.Trim() &&
							string.Empty != pi.Owner.MailAddress.SiteCitySiteState &&
							string.Empty != pi.Owner.MailAddress.SiteCitySiteStateSiteZIP
							)	
							return pi.Owner.MailAddress.SiteCitySiteStateSiteZIP;
						else
						{
							if (null != pi && null != pi.Owner.SiteAddress)
								return pi.Owner.SiteAddress.SiteCitySiteStateSiteZIP;					
							else
								return string.Empty;
						}

						//ta--, 12.10.2006:

					case "Lst. Ag. Full Name":
						if (null != pi && null != pi.ListingAgent)	
							return pi.ListingAgent.ListingAgentFullName;
						else
							return string.Empty;

					case "Lst. Ag. First Name":
						if (null != pi && null != pi.ListingAgent)	
							return pi.ListingAgent.ListingAgentFirstName;
						else
							return string.Empty;

					case "Lst. Ag. Last Name":
						if (null != pi && null != pi.ListingAgent)	
							return pi.ListingAgent.ListingAgentLastName;
						else
							return string.Empty;
                    
                    case "Lst. Ag. Email":
                        if (null != pi && null != pi.ListingAgent)
                            return pi.ListingAgent.ListingAgentEMail;
                        else
                            return string.Empty;

                    case "Lst. Ag. Phone Nr":
						if (null != pi && null != pi.ListingAgent)	
							return pi.ListingAgent.ListingAgentPhoneNumber;
						else
							return string.Empty;

					case "Lst. Ag. Fax Nr":
						if (null != pi && null != pi.ListingAgent)	
							return pi.ListingAgent.ListingAgentFaxNumber;
						else
							return string.Empty;

					case "Lst. Ag. Office":
						if (null != pi && null != pi.ListingAgent)	
							return pi.ListingAgent.ListingAgentOffice;
						else
							return string.Empty;

					case "Lst. Ag. Code":
						if (null != pi && null != pi.ListingAgent)	
							return pi.ListingAgent.ListingAgentCode;
						else
							return string.Empty;

					case "Lst. MLS Nr":
						if (null != pi && null != pi.ListingAgent)	
							return pi.ListingAgent.ListingAgentMLSNumber;
						else
							return string.Empty;

					case "Lst. Price":
						if (null != pi && null != pi.ListingAgent)	
							return pi.ListingAgent.ListingPrice.ToString();
						else
							return string.Empty;

					case Globals.OfferedPrice:
						if (null != pi && null != pi.ListingAgent)	
							return ((double)(pi.ListingAgent.ListingPrice * mmm.Percentage)).ToString();
						else
							return string.Empty;

                    case Globals.AlphaOfferedPrice:
                        if (null != pi && null != pi.ListingAgent)
                            return new NumberToEnglish().changeNumericToWords(((double)(pi.ListingAgent.ListingPrice * mmm.Percentage)));
                        else
                            return string.Empty;

					case "Subdivision":
						if (null != pi && null != pi.DealInfo && null != pi.DealInfo.DistrictProperty)	
							return pi.DealInfo.DistrictProperty.Subdivision;
						else
							return string.Empty;

					case "Legal Description":
						if (null != pi && null != pi.DealInfo && null != pi.DealInfo.DistrictProperty)	
							return pi.DealInfo.DistrictProperty.LegalDescription;
						else
							return string.Empty;

					case "Vesting":
						if (null != pi && null != pi.DealInfo && null != pi.DealInfo.DistrictProperty)	
							return pi.DealInfo.DistrictProperty.Vesting;
						else
							return string.Empty;

					case "BarCode Id":
						if (null != pi)
							return "*" + pi.IdPropertyItem.ToString().Substring(0, 6) + "*";
						else
							return string.Empty;

					case "Date":
						return DateTime.Now.ToShortDateString();

                    case "Date (Rich Format)":
                        DateTime dt = DateTime.Now;

                        string retVal = string.Empty;

                        retVal += dt.ToString("MMMM") + " ";
                        retVal += dt.Day.ToString();

                        string d2d = dt.ToString("dd").Substring(1);
                        string daySuffix =
                            (dt.Day == 11 || dt.Day == 12 || dt.Day == 13) ? "th"
                            : (d2d == "1") ? "st"
                            : (d2d == "2") ? "nd"
                            : (d2d == "3") ? "rd"
                            : "th";

                        retVal += daySuffix + ", ";
                        retVal += dt.ToString("yyyy");

                        return retVal;

                    case "Sale Date":

                        /* OLD
                        if (null != pi && null != pi.DealProperty && (pi.DealProperty.ForSaleDate > DateTime.MinValue) && (pi.DealProperty.ForSaleDate < DateTime.MaxValue))                        
                            return pi.DealProperty.ForSaleDate.ToShortDateString();                        
                        else                        
                            return string.Empty;
                        */

                        //NEW
                        if (null != pi && null != pi.DealInfo && null != pi.DealInfo.ForeClosureProperty && null != pi.DealInfo.ForeClosureProperty.ForeclosureSaleDate)
                            return pi.DealInfo.ForeClosureProperty.ForeclosureSaleDate;
                        else
                            return string.Empty;
                        						
					default:
						return string.Empty;
				}
			}
			catch(Exception)
			{
				return string.Empty;
			}
		}
		#endregion � GetStringFromPropertyItem �
	}
	#endregion � Class MailMergeMapper �
}
#endregion � Namespace DealMaker �