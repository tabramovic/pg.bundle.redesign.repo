﻿#region Using directives

using System;
using System.Text;

// moljac++ 2004.11.12
using System.Runtime.InteropServices;

#endregion

namespace DealMaker.Faxing
{
	public 
		class 
		WinFaxDLLWrapper
		:
		FaxInfo
		, IFax
	{
		bool bTerminate = false;
		int hCompletionPort = 0;
		int completionKey = 0;
		int bytesSent = 0;
		int hFax = 0;
		//IntPtr hFax = new IntPtr(0);
		//IntPtr hPort = new IntPtr(0);
		int hPort = 0;
		int faxJobID = 0;

		FAX_DEVICE_STATUS deviceStatus;
		FAX_EVENT faxEvent;
		FAX_JOB_PARAM jobParam;
		FAX_COVERPAGE_INFO coverPageInfo;
		IntPtr pCoverPageInfo;
		IntPtr coverPageNull; 
		IntPtr pFaxEvent;
		IntPtr pJobParam;

		public 
			WinFaxDLLWrapper
			(
			)
			: base()
		{
		}

		public
			bool
			Initialize
			(
			)
		{
			bool initialized = false;
			try
			{
				faxEvent = new FAX_EVENT();
				faxEvent.SizeOfStruct = Marshal.SizeOf(typeof(FAX_EVENT));
				pFaxEvent = Marshal.AllocHGlobal(faxEvent.SizeOfStruct);
				Marshal.StructureToPtr(faxEvent, pFaxEvent, true);

				jobParam = new FAX_JOB_PARAM();
				jobParam.SizeOfStruct = 
					Marshal.SizeOf(typeof(FAX_JOB_PARAM));
				pJobParam = 
					Marshal.AllocHGlobal(jobParam.SizeOfStruct);
				Marshal.StructureToPtr(jobParam, pJobParam, true);

				jobParam.RecipientNumber = this.FaxNumber;
				jobParam.ScheduleAction = (int)FAX_ENUM_JOB_SEND_ATTRIBUTES.JSA_NOW;
				jobParam.CallHandle = new IntPtr(0);

				coverPageInfo = new FAX_COVERPAGE_INFO();
				coverPageInfo.SizeOfStruct =
					Marshal.SizeOf(typeof(FAX_COVERPAGE_INFO));
				pCoverPageInfo =
					Marshal.AllocHGlobal(coverPageInfo.SizeOfStruct);
				Marshal.StructureToPtr(coverPageInfo, pCoverPageInfo, true);

				// connect to fax service
				if (FaxConnectFaxServer(null, out hFax) == 0)
				{
					Console.WriteLine("GetLastWin32Error: " + Marshal.GetLastWin32Error().ToString());

					//_tprintf( TEXT("FaxConnectFaxServer failed, ec = %d\n"),GetLastError() );
					return false;
				}

				// create completion port to poll against while waiting for fax completion
				hCompletionPort = Win32.CreateIoCompletionPort(Win32.INVALID_HANDLE_VALUE, 0, 0, 0);

				if (hCompletionPort == 0)
				{
					Console.WriteLine("GetLastWin32Error: " + Marshal.GetLastWin32Error().ToString());

					//_tprintf( TEXT("CreateIoCompletionPort failed, ec = %d\n"), GetLastError());
					FaxClose(hFax);
					return false;
				}

				if (!FaxInitializeEventQueue
					(
					hFax
					, hCompletionPort
					, 0
					, 0
					, 0
					)
					)
				{
					Console.WriteLine("GetLastWin32Error: " + Marshal.GetLastWin32Error().ToString());

					//_tprintf( TEXT("FaxInitializeEventQueue failed, ec = %d\n"), GetLastError() );
					FaxClose(hFax);
					return false;
				}

				FaxCompleteJobParams(ref jobParam, ref coverPageInfo);


			}
			catch (Exception )
			{
				initialized = false;
			}


			return initialized;
		}

		public
			bool
			Setup
			(
			)
		{
			return Initialize();
		}

		/// <summary>
		/// Sending a Fax to One Recipient (Win32 Environment)
		/// 
		/// A fax client application can call the FaxSendDocument function 
		/// to queue a job that will transmit an outgoing fax to one recipient, 
		/// specifying a file stored on disk. 
		/// The application must specify one file to transmit and supply a 
		/// pointer to a FAX_JOB_PARAM structure. The fax server queues the fax
		/// transmission job according to the details specified by the 
		/// FAX_JOB_PARAM structure. The structure includes the recipient's fax 
		/// number, sender and recipient data, an optional billing code, and job 
		/// scheduling information. 
		/// The application must also supply a pointer to a FAX_COVERPAGE_INFO 
		/// structure if a cover page is required. This structure contains data 
		/// to display on the cover page of a fax transmission. For more 
		/// information, see Sending a Cover Page.
		/// Call the FaxCompleteJobParams function before calling the 
		/// FaxSendDocument function. FaxCompleteJobParams is a utility function
		/// that supplies data for members in the FAX_COVERPAGE_INFO and 
		/// FAX_JOB_PARAM structures. 
		/// Call the FaxFreeBuffer function to free the memory associated with 
		/// these structures.
		/// To send a fax document efficiently to multiple recipients, an 
		/// application should call the FaxSendDocument function multiple times. 
		/// The FaxSendDocumentForBroadcast function is supported for backward 
		/// compatibility. Note that an application must first call the 
		/// FaxConnectFaxServer function to obtain a fax server handle before 
		/// calling the FaxSendDocument function and the 
		/// FaxSendDocumentForBroadcast function.
		/// </summary>
		public
			bool
			Send
			(
			)
		{
			try
			{
				coverPageNull = new IntPtr(0);
				if (!FaxSendDocument
					(
					hFax
					, FileName
					, ref jobParam
					, ref coverPageNull
					, out faxJobID
					)
					)
				{
					Console.WriteLine("GetLastWin32Error: " + Marshal.GetLastWin32Error().ToString());

					//jobParam = (FAX_JOB_PARAM) Marshal.PtrToStructure(pJobParam, typeof(FAX_JOB_PARAM));
					//coverPageInfo = (FAX_COVERPAGE_INFO) 
					Marshal.PtrToStructure(pCoverPageInfo, typeof(FAX_COVERPAGE_INFO));

					//_tprintf( TEXT("FaxSendDocument failed, ec = %d \n"),GetLastError() );

					FaxClose(hFax);
					Win32.CloseHandle(hCompletionPort);
					FaxFreeBuffer(pJobParam);
					FaxFreeBuffer(pCoverPageInfo);

					return false;
				}

				//_tprintf(TEXT("Queued document %s for transmition to %s, JobID = %d\n"), Document, Number, JobId);

				FaxFreeBuffer(pJobParam);
				FaxFreeBuffer(pCoverPageInfo);

				Marshal.StructureToPtr(faxEvent, pFaxEvent, true);

				while
					(
					! bTerminate
					&&
					(Win32.GetQueuedCompletionStatus(
					hCompletionPort
					, ref bytesSent
					, ref completionKey
					, pFaxEvent
					, Win32.INFINITE)
					!=
					0
					)
					)
				{
					faxEvent = (FAX_EVENT) Marshal.PtrToStructure
						(
						pFaxEvent
						, typeof(FAX_EVENT)
						);

					//_tprintf( TEXT("Received event 0x%x\n"),FaxEvent->EventId);

					switch ((WinFaxEvent)faxEvent.EventId)
					{
						case WinFaxEvent.FEIh_lng_idLE:
						case WinFaxEvent.FEI_COMPLETED:
						case WinFaxEvent.FEI_MODEM_POWERED_ON:
						case WinFaxEvent.FEI_MODEM_POWERED_OFF:
						case WinFaxEvent.FEI_FAXSVC_ENDED:
							bTerminate = true;
							break;
						case WinFaxEvent.FEI_JOB_QUEUED:
							//_tprintf( TEXT("JobId 0x%x queued\n"),FaxEvent->JobId );
							break;
						case WinFaxEvent.FEI_DIALING:
						case WinFaxEvent.FEI_SENDING:
						case WinFaxEvent.FEI_RECEIVING:
						case WinFaxEvent.FEI_BUSY:
						case WinFaxEvent.FEI_NO_ANSWER:
						case WinFaxEvent.FEI_BAD_ADDRESS:
						case WinFaxEvent.FEI_NO_DIAL_TONE:
						case WinFaxEvent.FEI_DISCONNECTED:
						case WinFaxEvent.FEI_FATAL_ERROR:
							if (FaxOpenPort
								(
								hFax
								, faxEvent.DeviceId
								, (int)FAX_PORT_OPEN_TYPE.PORT_OPEN_QUERY
								, hPort
								)
								)
							{
								if (FaxGetDeviceStatus
									(
									hPort
									, out deviceStatus
									)
									)
								{
									IntPtr pDeviceStatus = new IntPtr();
									deviceStatus.SizeOfStruct =
										Marshal.SizeOf(typeof(FAX_DEVICE_STATUS));
									Marshal.StructureToPtr
										(
										deviceStatus
										, pDeviceStatus
										, true
										);
									//PrintDeviceStatus(deviceStatus);
									FaxFreeBuffer(pDeviceStatus);
								}
								FaxClose(hPort);
							}
							break;
					}
				}

				FaxClose(hFax);
				Win32.CloseHandle(hCompletionPort);

				return true;
			}
			catch (System.Runtime.InteropServices.COMException ex) 
			{
				string str_err = "Faxing error! ";
				Exception fex;
				switch(ex.ErrorCode)
				{
					case -2147024883: //	0x8007000d
						str_err += "Check File Types (extension) and application mappings";
						fex = new Exception
							(
							"FaxingError: " + str_err
							, ex
							);					
						throw fex;
						//break;
					case -2147020584: //	0x800710d8
						str_err += "Check if fax is installed!";
						fex = new Exception
						(
						"FaxingError: " + str_err
						, ex
						);
					
					throw fex;
						//break;
					default:
						throw ex;
				}
			}
			catch (System.Exception ex)
			{
				//System.Windows.Forms.MessageBox.Show(ex.ToString());

				/*
				Core.Diagnostics.Trace trace;
				trace = new Core.Diagnostics.Trace("Faxing", "Communication");
				trace.Exception
							(
							  ex
							, "WinFaxDLL.Send error {0}"
							, DateTime.Now
							, Environment.UserName
							);
				*/
				throw ex;				
			}			
		}

		public
			bool
			Receive
			(
			)
		{
			return false;
		}

		internal
			class
			Win32
		{
			public static readonly int INFINITE = -1;
			public static readonly int INVALID_HANDLE_VALUE = -1;

			[DllImport("kernel32")]
			public 
				unsafe
				static 
				extern 
				int 
				CreateIoCompletionPort
				(
				int FileHandle
				, int ExistingCompletionPort
				, int CompletionKey
				, int NumberOfConcurrentThreads
				);

			[DllImport("kernel32")]
			public 
				unsafe
				static 
				extern 
				int 
				GetQueuedCompletionStatus
				(
				int CompletionPort
				, ref int lpNumberOfBytesTransferred
				, ref int lpCompletionKey
				, IntPtr lpOverlapped
				, int dwMilliseconds
				);

			[DllImport("kernel32")]
			public 
				unsafe
				static 
				extern 
				int 
				CloseHandle
				(
				int hObject
				);
		}

		#region Enumerations
		public 
			enum 
			FAX_PORT_OPEN_TYPE
		{
			PORT_OPEN_QUERY = 1
				, PORT_OPEN_MODIFY = 2
		}

		public 
			enum 
			FAX_ENUM_JOB_SEND_ATTRIBUTES
		{
			// Send now
			JSA_NOW = 0
				// Send at specific time
				, JSA_SPECIFIC_TIME
					  // Send at server configured discount period
					  , JSA_DISCOUNT_PERIOD               
		}

		[Flags]
			public 
			enum 
			WinFaxEvent
		{
			FEI_DIALING = 0x00000001
				, FEI_SENDING = 0x00000002
					  , FEI_RECEIVING = 0x00000003
							, FEI_COMPLETED = 0x00000004
								  , FEI_BUSY = 0x00000005
										, FEI_NO_ANSWER = 0x00000006
											  , FEI_BAD_ADDRESS = 0x00000007
													, FEI_NO_DIAL_TONE = 0x00000008
														  , FEI_DISCONNECTED = 0x00000009
																, FEI_FATAL_ERROR = 0x0000000a
																	  , FEI_NOT_FAX_CALL = 0x0000000b
																			, FEI_CALL_DELAYED = 0x0000000c
																				  , FEI_CALL_BLACKLISTED = 0x0000000d
																						, FEI_RINGING = 0x0000000e
																							  , FEI_ABORTING = 0x0000000f
																									, FEI_ROUTING = 0x00000010
																										  , FEI_MODEM_POWERED_ON = 0x00000011
																												, FEI_MODEM_POWERED_OFF = 0x00000012
																													  , FEIh_lng_idLE = 0x00000013
																															, FEI_FAXSVC_ENDED = 0x00000014
																																  , FEI_ANSWERED = 0x00000015
																																		, FEI_JOB_QUEUED = 0x00000016
																																			  , FEI_DELETED = 0x00000017
																																					, FEI_INITIALIZING = 0x00000018
																																						  , FEI_LINE_UNAVAILABLE = 0x00000019
																																								, FEI_HANDLED = 0x0000001a
																																									  , FEI_FAXSVC_STARTED = 0x0000001b
		}
		#endregion

		#region Structs

		[StructLayout(LayoutKind.Sequential)]
			public 
			struct 
			SYSTEMTIME
		{
			public short wYear;
			public short wMonth;
			public short wDayOfWeek;
			public short wDay;
			public short wHour;
			public short wMinute;
			public short wSecond;
			public short wMilliseconds;
		}

		[StructLayout(LayoutKind.Sequential)]
			public 
			struct 
			FAX_DEVICE_STATUS
		{
			// size of this structure
			public int SizeOfStruct;
			// caller id string
			public string CallerId;
			// station identifier
			public string Csid;
			// current page
			public int CurrentPage;
			// permanent line id
			public int DeviceId;
			// device name
			public string DeviceName;
			// document name
			public string DocumentName;
			// job type: send or receive?
			public int JobType;
			// sending phone number
			public string PhoneNumber;
			// routing information
			public string RoutingString;
			// sender name
			public string SenderName;
			// recipient name
			public string RecipientName;
			// size in bytes of the document
			public int Size;
			// starting time of the fax send/receive
			public FILETIME StartTime;
			// current status of the device, see FPS_??? masks
			public int Status;
			// status string if the Status field is zero.  this may be NULL.
			public string StatusString;
			// time the document was submitted
			public FILETIME SubmittedTime;
			// total number of pages in this job
			public int TotalPages;
			// transmitting station identifier
			public string Tsid;
			// user that submitted the active job
			public string UserName;                   
		}

		[StructLayout(LayoutKind.Sequential)]
			public 
			struct 
			FAX_COVERPAGE_INFO
		{
			// Size of this structure
			public int SizeOfStruct;               
			//
			// general
			//
			// coverpage document name
			public string CoverPageName;
			// coverpage exists on the fax server
			public bool UseServerCoverPage;         
			//
			// Recipient information
			//
			public string RecName;                    
			public string RecFaxNumber;               
			public string RecCompany;                 
			public string RecStreetAddress;           
			public string RecCity;                    
			public string RecState;                   
			public string RecZip;                     
			public string RecCountry;                 
			public string RecTitle;                   
			public string RecDepartment;              
			public string RecOfficeLocation;          
			public string RecHomePhone;               
			public string RecOfficePhone;             
			//
			// Sender information
			//
			public string SdrName;                    
			public string SdrFaxNumber;               
			public string SdrCompany;                 
			public string SdrAddress;                 
			public string SdrTitle;                   
			public string SdrDepartment;              
			public string SdrOfficeLocation;          
			public string SdrHomePhone;               
			public string SdrOfficePhone;             
			//
			// Misc information
			//
			public string Note;                       
			public string Subject;
			// Time the fax was sent
			public SYSTEMTIME TimeSent;
			// Number of pages
			public int PageCount;                  
		}

		[StructLayout(LayoutKind.Sequential)]
			public 
			struct 
			FAX_EVENT
		{
			// Size of this structure
			public int SizeOfStruct;
			// Timestamp for when the event was generated
			public FILETIME TimeStamp;
			// Permanent line id
			public int DeviceId;
			// Current event id
			public int EventId;
			// Fax Job Id,  0xffffffff indicates inactive job
			public int JobId;                      
		}

		/// <summary>
		/// The FAX_JOB_PARAM structure contains the information necessary
		/// for the fax server to send an individual fax transmission. The 
		/// structure includes the recipient's fax number, sender and recipient
		/// data, an optional billing code, and job scheduling information. 
		/// 
		/// The SizeOfStruct, RecipientNumber, and ScheduleAction members are 
		/// required; other members are optional. 
		/// </summary>
		[StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
			public 
			struct 
			FAX_JOB_PARAM
		{
			public int SizeOfStruct;               // size of this structure
			public string RecipientNumber;            // recipient fax number
			public string RecipientName;              // recipient name
			public string Tsid;                       // transmitter's id
			public string SenderName;                 // sender name
			public string SenderCompany;              // sender company
			public string SenderDept;                 // sender department
			public string BillingCode;                // billing code
			public int ScheduleAction;             // when to schedule the fax, see JSA defines
			public SYSTEMTIME ScheduleTime;               // time to send the fax when JSA_SPECIFIC_TIME is used (must be local time)
			public int DeliveryReportType;         // delivery report type, see DRT defines
			public string DeliveryReportAddress;      // email address for delivery report (ndr or dr) thru MAPI / SMTP
			public string DocumentName;               // document name (optional)
			public IntPtr CallHandle;                 // optional call handle

			//[MarshalAs(UnmanagedType., SizeConst=3)]
			public IntPtr[] Reserved;     // reserved for ms use only
		}
		#endregion

		[DllImport("winfax.dll", SetLastError = true)]
		public 
			unsafe
			static 
			extern 
			int 
			FaxConnectFaxServer
			(
			string MachineName
			, [Out] out int FaxHandle
			);

		[DllImport("winfax.dll", SetLastError = true)]
		public 
			unsafe
			static 
			extern 
			bool 
			FaxInitializeEventQueue
			(
			int FaxHandle
			, int CompletionPort
			, int CompletionKey
			, int hWnd
			, int MessageStart
			);

		[DllImport("winfax.dll", SetLastError = true)]
		public 
			unsafe
			static 
			extern 
			void 
			FaxFreeBuffer
			(
			IntPtr Buffer
			);

		[DllImport("winfax.dll", SetLastError = true)]
		public 
			static 
			extern 
			bool 
			FaxClose
			(
			int FaxHandle
			);

		[DllImport("winfax.dll", SetLastError = true)]
		public static extern bool FaxCompleteJobParams([In, Out] ref
			FAX_JOB_PARAM JobParams, [In,

			Out] ref FAX_COVERPAGE_INFO CoverpageInfo);

		[DllImport("winfax.dll", SetLastError = true)]
		public 
			static 
			extern 
			bool 
			FaxSendDocument
			(
			int FaxHandle
			, string FileName
			, [In] ref FAX_JOB_PARAM JobParams
			, [In] ref FAX_COVERPAGE_INFO CoverpageInfo
			, [Out] out int FaxJobId
			);

		[DllImport("winfax.dll", SetLastError = true)]
		public 
			static 
			extern 
			bool 
			FaxSendDocument
			(
			int FaxHandle
			, string FileName
			, [In] ref FAX_JOB_PARAM JobParams
			, [In] ref IntPtr CoverpageInfo
			, [Out] out int FaxJobId
			);

		[DllImport("winfax.dll", SetLastError = true)]
		private
			static 
			extern 
			bool 
			FaxOpenPort
			(
			int FaxHandle
			, int DeviceId
			, int Flags
			, [Out] int FaxPortHandle
			);

		[DllImport("winfax.dll", SetLastError = true)]
		private 
			static 
			extern 
			bool 
			FaxGetDeviceStatus
			(
			int FaxPortHandle
			, [Out] out FAX_DEVICE_STATUS DeviceStatus
			);

	}  
}

