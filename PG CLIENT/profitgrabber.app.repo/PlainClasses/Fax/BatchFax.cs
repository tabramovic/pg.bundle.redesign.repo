using System;
using System.Collections;
using System.Windows.Forms;
//using Core.Windows.Forms.WYSIWYG.Fax;

namespace DealMaker.Faxing
{
	/// <summary>
	/// Summary description for BatchFax.
	/// </summary>
	public class BatchFax
	{
		public BatchFax()
		{
		}

		public ArrayList Send (System.Collections.ArrayList docs)
		{
			System.Collections.ArrayList docs_failed;
			docs_failed = new System.Collections.ArrayList();

            foreach(FaxItem fi in docs)
			{
                try
                {

                    FaxCOMWrapper fw;
                    fw = new FaxCOMWrapper();

                    /*
                    Core.Windows.Forms.WYSIWYG.Fax.FaxCOMExWrapper fw;
                    fw = new Core.Windows.Forms.WYSIWYG.Fax.FaxCOMExWrapper();

                    Core.Windows.Forms.WYSIWYG.Fax.FaxCOMWrapper fw;
                    fw = new Core.Windows.Forms.WYSIWYG.Fax.FaxCOMWrapper();
				
                    Core.Windows.Forms.WYSIWYG.Fax.Print2FaxWrapper fw;
                    fw = new Core.Windows.Forms.WYSIWYG.Fax.Print2FaxWrapper();
				
                    Core.Windows.Forms.WYSIWYG.Fax.FaxCOMExWrapper fw;
                    fw = new Core.Windows.Forms.WYSIWYG.Fax.FaxCOMExWrapper();
                    */

                    fw.DocumentName = fi.DocName;
                    fw.FileName = fi.FileName;
                    fw.RecipientName = fi.RecipientName;
                    fw.FaxNumber = fi.FaxNumber;

                    Fax fax;
                    fax = new Fax(fw);

                    bool initialized = fax.FaxWrapper.Initialize();
                    //System.Windows.Forms.MessageBox.Show("faxWrapper inited: " + initialized.ToString());
                    if (!fax.Send())
                    {
                        docs_failed.Add(fi);
                    }
                }
                catch (Exception exc)
                {
                    MessageBox.Show(exc.Message + Environment.NewLine + exc.InnerException + Environment.NewLine + exc.StackTrace);
                    break;
                }
			}

			return docs_failed;
		}
	}
}

