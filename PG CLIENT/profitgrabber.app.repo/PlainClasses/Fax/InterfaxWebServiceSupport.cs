using System;
using System.Windows.Forms;
using System.Collections;
using System.IO;
using DealMaker.Faxing;
using System.Runtime.Serialization.Formatters.Binary;

using DealMaker.InterFaxWebService;

namespace DealMaker.PlainClasses.Fax
{
	/// <summary>
	/// This class wrapps calls to https://ws.interfax.net/dfs.asmx web service
	/// 
	/// Since Windows Vista comes in 6 various editions, some users can not use PG�s
	/// built-in faxing services.
	/// 
	/// This is a way around for them.
	/// </summary>
	public sealed class InterfaxWebServiceSupport
	{
		static readonly InterfaxWebServiceSupport _instance = new InterfaxWebServiceSupport();		
		
		const string UserNameConst = "UserName";
		const string PasswordConst = "Password";
		
		string _ifxUserName = string.Empty;
		string _ifxPassword = string.Empty;
		bool   _useIfxInterface = false;
		bool   _displayIfxConsole = true;

		Hashtable _ifxReturnCodes = new Hashtable();

		public delegate void IfxStatusEventHandler(string msg, bool valid);
		public event IfxStatusEventHandler IfxStatus;

		public delegate void IfxStartEventHandler(string faxNumber, string fileName);
		public event IfxStartEventHandler IfxStarted;

		public delegate void IfxEndEventHandler(string faxNumber, string fileName, bool success, string desc);
		public event IfxEndEventHandler IfxEnded;		
		
		//Interfax
		InterFax iFx = null;
				
		static InterfaxWebServiceSupport()
		{
		}

		InterfaxWebServiceSupport()
		{
			InitIfxReturnCodes();
		}

		public static InterfaxWebServiceSupport Instance
		{
			get { return _instance; }
		}

		public bool IsInitialized
		{
			get 
			{
				if (string.Empty != _ifxUserName && string.Empty != _ifxPassword)
					return true;
				else
					return false;
			}
		}

		public bool UseIfxInterface
		{
			get { return _useIfxInterface; }
			set { _useIfxInterface = value; }
		}

		public bool DisplayIfxConsole
		{
			get { return _displayIfxConsole; }
			set { _displayIfxConsole = value; }
		}
		
		public bool SaveIfxData(string userName, string password)
		{			
			Hashtable htToSave =  new Hashtable();	
			htToSave.Add(UserNameConst, userName);
			htToSave.Add(PasswordConst, password);								

			if (!Directory.Exists(Application.StartupPath))			
				Directory.CreateDirectory(Application.StartupPath);
			
			FileStream fs = null;
			try
			{
				fs = new FileStream (Application.StartupPath + @"\ifxData.dat", FileMode.OpenOrCreate, FileAccess.Write);
			}
			catch 
			{ return false; }

			try
			{
				BinaryFormatter bf=new BinaryFormatter ();
				bf.Serialize (fs, htToSave);
				_ifxUserName = userName;
				_ifxPassword = password;
			}
			catch	{ return false; }
			finally
			{
				fs.Close ();				
			}
			return true;
		}
		
		public void LoadIfxData()
		{
			string tempUserName, tempPassword;
			LoadIfxData(out tempUserName, out tempPassword);
		}

		public void LoadIfxData(out string userName, out string password)
		{
			userName = string.Empty;
			password = string.Empty;

			Hashtable htLoadedData = new Hashtable();
			FileStream fs = null;
			
			if (!Directory.Exists(Application.StartupPath))			
				return ;
			

			try { fs = new FileStream (Application.StartupPath +  @"\ifxData.dat", FileMode.Open, FileAccess.Read );	}
			catch 
			{ return; }

			if (null != fs)
			{
				try
				{					
					BinaryFormatter bf=new BinaryFormatter ();
					htLoadedData = (Hashtable)bf.Deserialize(fs);										
					if (htLoadedData.Contains(UserNameConst) && htLoadedData.Contains(PasswordConst))
					{
						_ifxUserName = userName = (string)htLoadedData[UserNameConst];
						_ifxPassword = password = (string)htLoadedData[PasswordConst];
					}

				}
				catch {	}
				finally
				{
					fs.Close ();
				}									
			}			
		}

		public void SendFaxes(ArrayList faxingItems)
		{
			if (null == faxingItems || (0 == faxingItems.Count))
			{
				if (null != IfxStatus)				
					IfxStatus(@"Outgoing queue contains no faxes. Please check fax numbers associated to contacts!"+ Environment.NewLine + "Click on Close button and verify the data!", false);
				
				return;
			}
			else
				if (null != IfxStatus)
					IfxStatus(faxingItems.Count.ToString() + " documents in outgoing queue!", true);

			if (null == iFx)
				iFx = new InterFax();

			for (int i = 0; i < faxingItems.Count; i++)
			{
				DealMaker.Faxing.FaxItem fi = faxingItems[i] as DealMaker.Faxing.FaxItem;

				if (null != fi)
				{																			
					FileStream fs = null;
					
					try { fs = File.Open(fi.FileName, FileMode.Open); }
					catch 
					{
						//todo (ta)
						continue;
					}

					if (null != fs)
					{
						
						byte[] binaryData = new byte[fs.Length];
						try {fs.Read(binaryData, 0, (int)fs.Length);}
						catch 
						{							
							continue;
						}
						

						long res = 0;
						try 
						{ 							
							if (null != IfxStarted)	
								IfxStarted(fi.FaxNumber, fi.FileName);
//#if DEBUG
//							res = iFx.Sendfax(_ifxUserName, _ifxPassword, "x1x0014808932291", binaryData, "rtf"); 							
//#else
							res = iFx.Sendfax(_ifxUserName, _ifxPassword, fi.FaxNumber, binaryData, "rtf"); 
//#endif

							
							if (null != IfxEnded)
							{
								if (res > 0)								
									IfxEnded(fi.FaxNumber, fi.FileName, true, res.ToString());															
								else
								{
									if (_ifxReturnCodes.Contains(res))
										IfxEnded(fi.FaxNumber, fi.FileName, false, res.ToString() + " - " + (string)_ifxReturnCodes[res]);
									else
										IfxEnded(fi.FaxNumber, fi.FileName, false, res.ToString() + " - Unknown Error");
								}								
							}
						}
						catch (Exception exc)
						{
							if (null != IfxEnded)															
								IfxEnded(fi.FaxNumber, fi.FileName, false, "Unknown Error (" + exc.Message + ")");
							
							try
							{
								fs.Close();
							}
							catch {}

							continue;
						}

						fs.Close();
					}
				}
			}
		}

		void InitIfxReturnCodes()
		{
			_ifxReturnCodes.Add(-112,"No valid recipients added or missing fax number");
			_ifxReturnCodes.Add(-123,"No valid documents attached");
			_ifxReturnCodes.Add(-150,"Internal System Error");
			_ifxReturnCodes.Add(-1002,"Number of types does not match number of document sizes string");
			_ifxReturnCodes.Add(-1003,"Authentication error");
			_ifxReturnCodes.Add(-1004,"Missing file type");
			_ifxReturnCodes.Add(-1005,"Transaction does not exist");
			_ifxReturnCodes.Add(-1007,"Size value is not numeric or not greater than 0");
			_ifxReturnCodes.Add(-1008,"Total sizes does not match filesdata length");
			_ifxReturnCodes.Add(-1009,"Image not available (may happen if the 'delete fax after completion' feature is active)");
			_ifxReturnCodes.Add(-1030,"Invalid Verb or VerbData");
			_ifxReturnCodes.Add(-3001,"Invalid MessageID");
			_ifxReturnCodes.Add(-3002,"From parameter is larger than image size");
			_ifxReturnCodes.Add(-3003,"Image doesn't exist");
			_ifxReturnCodes.Add(-3004,"TIFF file is empty");
			_ifxReturnCodes.Add(-3005,"Chunk size is smaller than 1");
			_ifxReturnCodes.Add(-3006,"Max item is smaller than 1");
			_ifxReturnCodes.Add(-3007,"No permission for this action (In inbound method GetList, LType is set to AccountAllMessages or AccountNewMessages, when the username is not a Primary user)");
		}

	}
}

#region SINGLETON
/*
 *	public sealed class Singleton
	{
		static readonly Singleton instance=new Singleton();

		// Explicit static constructor to tell C# compiler
		// not to mark type as beforefieldinit
		static Singleton()
		{
		}

		Singleton()
		{
		}

		public static Singleton Instance
		{
			get
			{
				return instance;
			}
		}
	}
 * */
#endregion

/*
(any value greater than 0) TransactionID. This indicates a successful connection to the Web Service.
-112 	No valid recipients added or missing fax number
-123 	No valid documents attached
-150 	Internal System Error
-1002 	Number of types does not match number of document sizes string
-1003 	Authentication error
-1004 	Missing file type
-1005 	Transaction does not exist
-1007 	Size value is not numeric or not greater than 0
-1008 	Total sizes does not match filesdata length
-1009 	Image not available (may happen if the 'delete fax after completion' feature is active)
-1030 	Invalid Verb or VerbData
-3001 	Invalid MessageID
-3002 	From parameter is larger than image size
-3003 	Image doesn't exist
-3004 	TIFF file is empty
-3005 	Chunk size is smaller than 1
-3006 	Max item is smaller than 1
-3007 	No permission for this action (In inbound method GetList, LType is set to AccountAllMessages or AccountNewMessages, when the username is not a Primary user)
 * */