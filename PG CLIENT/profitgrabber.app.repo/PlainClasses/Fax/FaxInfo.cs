﻿#region Using directives

using System;
using System.Text;

#endregion

namespace DealMaker.Faxing
{
	public 
		class 
		FaxInfo
	{
		private
			string
			h_string_filename
			= ""
			;

		public
			string
			h_string_fax_number
			= ""
			;

		public
			string
			h_string_recipienth_str_name
			= ""
			;

		public
			string
			h_string_documenth_str_name
			= ""
			;

		public
			FaxInfo
			(
			)
		{
		}

		public
			string
			FileName
		{
			get
			{
				return h_string_filename;
			}
			set
			{
				h_string_filename = value;
			}
		}

		public
			string
			FaxNumber
		{
			get
			{
				return h_string_fax_number;
			}
			set
			{
				h_string_fax_number = value;
			}
		}

		public
			string
			RecipientName
		{
			get
			{
				return h_string_recipienth_str_name;
			}
			set
			{
				h_string_recipienth_str_name = value;
			}
		}

		public
			string
			DocumentName
		{
			get
			{
				return h_string_documenth_str_name;
			}
			set
			{
				h_string_documenth_str_name = value;
			}
		}

	}
}
