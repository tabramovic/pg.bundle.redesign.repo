﻿#region Using directives

using System;
using System.Text;

// mc++ 2004.11.22
using System.Drawing;
using System.Drawing.Printing;
#endregion

namespace DealMaker.Faxing
{       
	public 
		class 
		Print2FaxWrapper
		:
		FaxInfo
		, IFax
	{
		private
			PrintDocument
			printDocument
			;

		private
			System.IO.StreamReader
			file
			;

		private
			System.Drawing.Font
			printFont
			;

		public 
			Print2FaxWrapper
			(
			)
		{

		}

		public
			bool
			Initialize
			(
			)
		{
			bool retval = false;

			retval = true;

			return retval;
		}

		public
			bool
			Setup
			(
			)
		{
			bool retval = false;

			return retval;
		}

		public
			bool
			Send
			(
			)
		{
			bool retval = false;

			try
			{
				printDocument = new PrintDocument();

				file = new System.IO.StreamReader(FileName);
				printFont = new System.Drawing.Font("Arial", 10);
				printDocument.PrinterSettings.FromPage = 1;

				foreach (string printer in PrinterSettings.InstalledPrinters)
				{
					if (printer.IndexOf("Fax") != -1 )
					{
						printDocument.PrinterSettings.PrinterName = printer;
					}
				}

				// raising PrintPage event
				printDocument.Print();
				file.Close();

				retval = true;
			}
			catch (System.Runtime.InteropServices.COMException ex) 
			{
				string str_err = "Faxing error! ";
				Exception fex;
				switch(ex.ErrorCode)
				{
					case -2147024883: //	0x8007000d
						str_err += "Check File Types (extension) and application mappings";
						fex = new Exception
							(
							"FaxingError: " + str_err
							, ex
							);					
						throw fex;
						//break;
					case -2147020584: //	0x800710d8
						str_err += "Check if fax is installed!";
						fex = new Exception
						(
						"FaxingError: " + str_err
						, ex
						);
					
					throw fex;
						//break;
					default:
						throw ex;
				}
			}
			catch (Exception ex)
			{
				//System.Windows.Forms.MessageBox.Show(ex.ToString());

				retval = false;
				throw ex;
			}

			return retval;
		}

		public
			bool
			Receive
			(
			)
		{
			bool retval = false;

			return retval;
		}

		private 
			void 
			printDocument_PrintPage
			(
			object sender
			, System.Drawing.Printing.PrintPageEventArgs e
			)
		{
			float yPos = 0f;
			int count = 0;
			float leftMargin = e.MarginBounds.Left;
			float topMargin = e.MarginBounds.Top;
			string line = null;
			float linesPerPage;
			linesPerPage = 
				e.MarginBounds.Height 
				/ 
				printFont.GetHeight(e.Graphics)
				;

			while (count < linesPerPage)
			{
				line = file.ReadLine();
				if (line == null)
				{
					break;
				}
				yPos = topMargin + count * printFont.GetHeight(e.Graphics);
				e.Graphics.DrawString
					(
					line
					, printFont
					, Brushes.Black
					, leftMargin
					, yPos
					, new StringFormat()
					);
				count++;
			}
			if (line != null)
			{
				e.HasMorePages = true;
			}
		}
	}
}

