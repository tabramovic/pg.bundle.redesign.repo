﻿#region Using directives

using System;
using System.Text;

#endregion

namespace DealMaker.Faxing
{
	/// <summary>
	/// Wrapper for Faxing with FaxCOMEx.dll extended COM server object
	/// </summary>
	/// <remarks>
	/// Properties in code are 
	/// </remarks>
	public
		class 
		FaxCOMExWrapper
		:
		FaxInfo
		, IFax
	{
		public
			FAXCOMEXLib.FAX_ACCESS_RIGHTS_ENUM
			AccessRights
			;
		public
			FAXCOMEXLib.FAX_COVERPAGE_TYPE_ENUM
			CoverPageType
			;
		public
			FAXCOMEXLib.FAX_DEVICE_RECEIVE_MODE_ENUM
			DeviceReceiveModeTypes
			;
		public
			FAXCOMEXLib.FAX_GROUP_STATUS_ENUM
			GroupStatusTypes
			;
		public
			FAXCOMEXLib.FAX_JOB_EXTENDED_STATUS_ENUM
			JobExtendedStatusTypes
			;
		public
			FAXCOMEXLib.FAX_JOB_OPERATIONS_ENUM
			JobOperationTypes
			;
		public
			FAXCOMEXLib.FAX_JOB_STATUS_ENUM
			JobStatuTypes
			;
		public
			FAXCOMEXLib.FAX_JOB_TYPE_ENUM
			JobTypes
			;
		public
			FAXCOMEXLib.FAX_LOG_LEVEL_ENUM
			LogLevelTypes
			;
		public
			FAXCOMEXLib.FAX_PRIORITY_TYPE_ENUM
			PriorityTypes
			;
		public
			FAXCOMEXLib.FAX_PROVIDER_STATUS_ENUM
			ProviderStatusTypes
			;
		public
			FAXCOMEXLib.FAX_RECEIPT_TYPE_ENUM
			ReceiptType
			;
		public
			FAXCOMEXLib.FAX_ROUTING_RULE_CODE_ENUM
			RoutingRuleCode
			;
		public
			FAXCOMEXLib.FAX_RULE_STATUS_ENUM
			RuleStatusTypes
			;
		public
			FAXCOMEXLib.FAX_SCHEDULE_TYPE_ENUM
			ScheduleTypes
			;
		public
			FAXCOMEXLib.FAX_SERVER_APIVERSION_ENUM
			ServerAPIVersionTypes
			;
		public
			FAXCOMEXLib.FAX_SERVER_EVENTS_TYPE_ENUM
			ServerEventsTypes
			;
		public
			FAXCOMEXLib.FAX_SMTP_AUTHENTICATION_TYPE_ENUM
			SMTPAuthentificationTypes
			;
		public
			FAXCOMEXLib.FaxActivity
			Activity
			;
		public
			FAXCOMEXLib.FaxActivityLogging
			ActivityLogging
			;
		public
			FAXCOMEXLib.FaxDevice
			Device
			;
		public
			FAXCOMEXLib.FaxDeviceIds
			DeviceIds
			;
		public
			FAXCOMEXLib.FaxDeviceProvider
			DeviceProvider
			;
		public
			FAXCOMEXLib.FaxDeviceProviders
			DeviceProviders
			;
		public
			FAXCOMEXLib.FaxDocument
			Document
			;
		public
			FAXCOMEXLib.FaxEventLogging
			EventLogging
			;
		public
			FAXCOMEXLib.FaxFolders
			Folders
			;
		public
			FAXCOMEXLib.FaxInboundRouting
			InboundRouting
			;
		public
			FAXCOMEXLib.FaxInboundRoutingExtension
			InboundRoutingExtension
			;
		public
			FAXCOMEXLib.FaxInboundRoutingExtensions
			InboundRoutingExtensions
			;
		public
			FAXCOMEXLib.FaxInboundRoutingMethod
			InboundRoutingMethod
			;
		public
			FAXCOMEXLib.FaxInboundRoutingMethods
			InboundRoutingMethods
			;
		public
			FAXCOMEXLib.FaxIncomingArchive
			IncommingArchive
			;
		public
			FAXCOMEXLib.FaxIncomingJob
			IncommingJob
			;
		public
			FAXCOMEXLib.FaxIncomingMessage
			IncommingMessage
			;
		public
			FAXCOMEXLib.FaxIncomingMessageIterator
			IncommingMessageIterator
			;
		public
			FAXCOMEXLib.FaxIncomingQueue
			IncommingQueue
			;
		public
			FAXCOMEXLib.FaxJobStatus
			JobStatus
			;
		public
			FAXCOMEXLib.FaxLoggingOptions
			LoggingOptions
			;
		public
			FAXCOMEXLib.FaxOutboundRouting
			OutboundRouting
			;
		public
			FAXCOMEXLib.FaxOutboundRoutingGroup
			OutboundRoutingGroup
			;
		public
			FAXCOMEXLib.FaxOutboundRoutingGroups
			OutboundRoutingGroups
			;
		public
			FAXCOMEXLib.FaxOutboundRoutingRule
			OutboundRoutingRule
			;
		public
			FAXCOMEXLib.FaxOutboundRoutingRules
			OutboundRoutingRules
			;
		public
			FAXCOMEXLib.FaxOutgoingArchive
			OutgoingArchive
			;
		public
			FAXCOMEXLib.FaxOutgoingJob
			OutgoingJob
			;
		public
			FAXCOMEXLib.FaxOutgoingJobs
			OutgoingJobs
			;
		public
			FAXCOMEXLib.FaxOutgoingMessage
			OutgoingMessage
			;
		public
			FAXCOMEXLib.FaxOutgoingMessageIterator
			OutgoingMessageIterator
			;
		public
			FAXCOMEXLib.FaxOutgoingQueue
			OutgoingQueue
			;
		public
			FAXCOMEXLib.FaxReceiptOptions
			ReceiptOptions
			;
		public
			FAXCOMEXLib.FaxRecipient
			Recipient
			;
		public
			FAXCOMEXLib.FaxRecipients
			Recipients
			;
		public
			FAXCOMEXLib.FaxSecurity
			Security
			;
		public
			FAXCOMEXLib.FaxSender
			Sender
			;
		public
			FAXCOMEXLib.FaxServer
			Server
			;

		public 
			FaxCOMExWrapper
			(
			)
		{

		}

		~FaxCOMExWrapper
			(
			)
		{
			Server.Disconnect();

			return; 
		}

		public
			bool
			Initialize
			(
			)
		{
			bool initialized = false;
			try
			{
				Server = new FAXCOMEXLib.FaxServer();

				//empty string will default to local host. 
				Server.Connect(Environment.MachineName);

				Document = new FAXCOMEXLib.FaxDocument();

				Document.Recipients.Add
					(
					this.RecipientName
					, this.FaxNumber
					);

				Document.Body = this.FileName;
				Document.DocumentName = this.DocumentName;

				initialized = true;
			}
			catch (Exception )
			{
				initialized = false;
			}

			return initialized;
		}

		public
			bool
			Setup
			(
			)
		{
			return Initialize();
		}

		public
			bool
			Send
			(
			)
		{
			bool retval = false;
			try
			{
				object response;
				response = Document.Submit(Environment.MachineName);

				retval = true;
			}
			catch (System.Runtime.InteropServices.COMException ex) 
			{
				string str_err = "Faxing error! ";
				Exception fex;
				switch(ex.ErrorCode)
				{
					case -2147024883: //	0x8007000d
						str_err += "Check File Types (extension) and application mappings";
						fex = new Exception
							(
							"FaxingError: " + str_err
							, ex
							);					
						throw fex;
						//break;
					case -2147020584: //	0x800710d8
						str_err += "Check if fax is installed!";
						fex = new Exception
						(
						"FaxingError: " + str_err
						, ex
						);
					
					throw fex;
						//break;
					default:
						throw ex;
				}
			}
			catch (System.Exception ex)
			{
				//System.Windows.Forms.MessageBox.Show(ex.ToString());
				/*
				Core.Diagnostics.Trace trace;
				trace = new Core.Diagnostics.Trace("Faxing", "Communication");
				trace.Exception
							(
							  ex
							, "FaxCOMExLib.Send error {0}"
							, DateTime.Now
							, Environment.UserName
							);
				*/
				retval = false;
				throw ex;
			}

			return retval;
		}

		public
			bool
			Receive
			(
			)
		{
			return false;
		}

	}
}

