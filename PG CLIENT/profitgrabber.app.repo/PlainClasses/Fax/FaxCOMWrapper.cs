﻿#region Using directives

using System;
using System.Text;

// moljac++ 2004.11.08
using FAXCOMLib;

#endregion

namespace DealMaker.Faxing
{
	/// <summary>
	/// Wrapper for Faxing with FaxCOM.dll COM server object
	/// </summary>
	/// <remarks>
	/// Properties in code are 
	/// </remarks>
	[System.CLSCompliant(false)]
	public
		class 
		FaxCOMWrapper
		:
		FaxInfo
		, IFax
	{
		public
			FaxServerClass
			Server
			;

		/// <summary>
		/// 
		/// </summary>
		public
			FaxDoc
			Document
			;
        
		public
			FaxJobClass     
			Job
			;

		public
			FaxJobsClass
			Jobs
			;
		public
			FaxPortClass
			Port
			;

		public
			FaxPortsClass 
			Ports
			;
		public
			FaxRoutingMethodClass 
			RoutingMethod
			;

		public
			FaxRoutingMethodsClass
			RoutingMethods
			;

		public
			FaxStatusClass 
			Status
			;

		public
			FaxTiffClass
			Tiff
			;


		public 
			FaxCOMWrapper
			(
			)
			: base()
		{
		}

		~FaxCOMWrapper
			(
			)
		{
			if (null != Server)
				Server.Disconnect();

			return; 
		}

		public
			bool
			Initialize
			(
			)
		{
			bool initialized = false;
			try
			{
				Server = new FAXCOMLib.FaxServerClass();
				Server.Connect(Environment.MachineName);

				Document = (FaxDoc) Server.CreateDocument(this.FileName);

				Document.RecipientName = this.RecipientName;
				Document.FaxNumber = this.FaxNumber;
				Document.DisplayName = this.DocumentName;

				initialized = true;
			}
			catch (Exception )
			{
				initialized = false;
			}


			return initialized;
		}

		public
			bool
			Setup
			(
			)
		{
			return Initialize();
		}

		public
			bool
			Send
			(
			)
		{
			bool retval = false;

			try
			{
				int response;
				
				response = Document.Send();

				retval = true;
			}
			catch (System.Runtime.InteropServices.COMException ex) 
			{
				string str_err = "Faxing error! ";
				Exception fex;
				switch(ex.ErrorCode)
				{
					case -2147024883: //	0x8007000d
						str_err += "Check File Types (extension) and application mappings";
						fex = new Exception
						(
						"FaxingError: " + str_err
						, ex
						);
					
					throw fex;
						//break;
					case -2147020584: //	0x800710d8
						str_err += "Check if fax is installed!";
						fex = new Exception
							(
							"FaxingError: " + str_err
							, ex
							);					
						throw fex;
						//break;
					default:
						throw ex;
				}
			}
			catch (System.Exception ex)
			{
				System.Windows.Forms.MessageBox.Show(ex.ToString());

				/*
				Core.Diagnostics.Trace trace;
				trace = new Core.Diagnostics.Trace("Faxing", "Communication");
				trace.Exception
							(
							  ex
							, "FaxCOMLib.Send error {0}"
							, DateTime.Now
							, Environment.UserName
							);
				*/

				retval = false;
				throw ex;
			}

			GC.Collect();

			return retval;
		}

		public
			bool
			Receive
			(
			)
		{
			return false;
		}
	}
}

