﻿#region Using directives

using System;
using System.Text;

#endregion

namespace DealMaker.Faxing
{
	public
		class 
		Fax
		: 
		FaxInfo
	{
		public
			IFax
			FaxWrapper
			;

		# region	Constructors
		public 
			Fax
			(
			)
		{
			FaxWrapper = new FaxCOMWrapper();
			return;
		}

		public 
			Fax
			(
			FaxCOMExWrapper	f
			)
		{
			FaxWrapper = f;
			return;
		}

		public 
			Fax
			(
			FaxCOMWrapper	f
			)
		{
			FaxWrapper = f;
			return;
		}

		public 
			Fax
			(
			Print2FaxWrapper f
			)
		{
			FaxWrapper = f;
			return;
		}

		public 
			Fax
			(
			WinFaxDLLWrapper f
			)
		{
			FaxWrapper = f;
			return;
		}

		# endregion	Constructors

		public
			bool
			Send
			(
			)
		{
			try
			{
                //System.Windows.Forms.MessageBox.Show("trying to send");
                FaxWrapper.Send();
			}
			catch (System.Runtime.InteropServices.COMException ex) 
			{
                System.Windows.Forms.MessageBox.Show(ex.Message + Environment.NewLine + ex.InnerException + Environment.NewLine + ex.ErrorCode + Environment.NewLine + ex.HelpLink);
				string str_err = "Faxing error! ";
				Exception fex;
				switch(ex.ErrorCode)
				{
					case -2147024883: //	0x8007000d
						str_err += "Check File Types (extension) and application mappings";
						fex = new Exception
							(
							"FaxingError: " + str_err
							, ex
							);					
						throw fex;
						//break;
					case -2147020584: //	0x800710d8
						str_err += "Check if fax is installed!";
						fex = new Exception
						(
						"FaxingError: " + str_err
						, ex
						);
					
					throw fex;
						//break;
					default:
						throw ex;
				}
			}
			catch (System.Exception ex) 
			{
                System.Windows.Forms.MessageBox.Show(ex.ToString());
                
                /*Core.Diagnostics.Trace trace;
                trace = new Core.Diagnostics.Trace("Faxing", "Communication");
                trace.Exception
                            (
                              ex
                            , "Fax<>.Send error {0}"
                            , DateTime.Now
                            , Environment.UserName
                            );
                */
                
				throw ex;
			}

			return false;
		}
	}
}

