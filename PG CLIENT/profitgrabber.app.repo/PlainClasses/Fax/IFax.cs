﻿#region Using directives

using System;
using System.Text;

#endregion

namespace DealMaker.Faxing
{
	public 
		interface 
		IFax
	{
		/// <summary>
		/// Method for setting up Fax properties
		/// </summary>
		/// <returns>
		/// bool. true if setup completed succesfully
		/// </returns>
		bool
			Setup
			(
			);

		/// <summary>
		/// Method for initializing Fax (powering up, querying fax/modem)
		/// </summary>
		/// <returns>
		/// bool. true if initialization completed succesfully
		/// </returns>
		bool
			Initialize
			(
			);

		/// <summary>
		/// Method for sending document over Fax
		/// </summary>
		/// <returns>
		/// bool. true if send operation completed succesfully
		/// </returns>
		bool
			Send
			(
			);

		/// <summary>
		/// Method for receiving document over Fax
		/// </summary>
		/// <returns>
		/// bool. true if receive operation completed succesfully
		/// </returns>
		bool
			Receive
			(
			);

	}
}
