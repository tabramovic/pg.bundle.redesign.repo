using System;

namespace DealMaker.Faxing
{

	public class FaxItem
	{
		private string docName = string.Empty;
		private string fileName = string.Empty;
		private string recipientName = string.Empty;
		private string faxNumber = string.Empty;
		
		public FaxItem(string docName, string fileName, string recipientName, string faxNumber)
		{
			this.docName = docName;
			this.fileName = fileName;
			this.recipientName = recipientName;			
			this.faxNumber = faxNumber;			
			
		}
		

		public string DocName
		{
			get { return this.docName; }
			set { this.docName = value; }
		}

		public string FileName
		{
			get { return this.fileName; }
			set { this.fileName = value; }
		}

		public string RecipientName 
		{
			get { return this.recipientName; }
			set { this.recipientName = value; }
		}
		
		public string FaxNumber
		{
			get { return this.faxNumber; }
			set { this.FaxNumber = value; }
		}
	}
}
