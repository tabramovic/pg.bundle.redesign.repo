#region � Using �
using System;
using System.Collections;
#endregion � Using �
#region � Namespace DealMaker �
namespace DealMaker
{
	#region � Class ImportIntermedeateObject �
	/// <summary>
	/// ImportIntermedeateObject - Holds the data which will be gathered from import files and later mapped with 
	/// objects of type PropertyItem
	/// 
	/// Why separate class?
	///		Since all the data that will be imported is not avaliable yet, I don�t want to blow away my object model
	///		each time when import interfaces change.
	/// </summary>
	public class ImportIntermedeateObject
	{
		#region � Data �
		#region � Person �
		/// <summary>First Name</summary>				
		public string		firstOwnerName						= string.Empty;
		/// <summary>Last Name</summary>
		public string		lastOwnerName						= string.Empty;
		/// <summary>Middle Name</summary>
		public string		middleOwnerName						= string.Empty;
		/// <summary>Full Name</summary>
		public string		salutationOwnerName					= string.Empty;
		/// <summary>Full Name</summary>
		public string		fullOwnerName						= string.Empty;	
		#endregion � Person �		
		#region � Site Address �
		/// <summary>Site Street Number</summary>				
		public string		siteStreetNumber				= string.Empty;
		/// <summary>Site Street PreDirectional</summary>
		public string		siteStreetPreDirectional		= string.Empty;
		/// <summary>Site Street Name</summary>
		public string		siteStreetName					= string.Empty;
		/// <summary>Site Street PostDirectional</summary>
		public string		siteStreetPostDirectional		= string.Empty;	
		/// <summary>Site Street Suffix</summary>
		public string		siteSteeetSuffix				= string.Empty;
		/// <summary>Site Street Unit</summary>
		public string		siteStreetUnitNumber			= string.Empty;		
		/// <summary>Full Site Street Address</summary>
		public string		fullSiteStreetAddress				= string.Empty;
		/// <summary>Site City (Phoenix)</summary>
		public string		siteCity						= string.Empty;
		/// <summary>Site State (AZ for Arizona)</summary>
		public string		siteState						= string.Empty;
		/// <summary>Site City Site State - (Phoen�x / AZ)</summary>
		public string		siteCitySiteState				= string.Empty;
		/// <summary>Site City Site State Site ZIP - (Phoen�x / AZ / 11111)</summary>
		public string		siteCitySiteStateSiteZIP		= string.Empty;
		/// <summary>Site ZIP</summary>
		public string		siteZIP							= string.Empty;
		/// <summary>Site Country</summary>
		public string		siteCountry						= string.Empty;
		#endregion � Site Address �
		#region � Mailing Address �
		/// <summary>Mailing Street Number</summary>				
		public string		mailingStreetNumber				= string.Empty;
		/// <summary>Mailing Street PreDirectional</summary>
		public string		mailingStreetPreDirectional		= string.Empty;
		/// <summary>Mailing Street Name</summary>
		public string		mailingStreetName				= string.Empty;
		/// <summary>Mailing Street PostDirectional</summary>
		public string		mailingStreetPostDirectional	= string.Empty;	
		/// <summary>Mailing Street Suffix</summary>
		public string		mailingSteeetSuffix				= string.Empty;
		/// <summary>Mailing Street Unit</summary>
		public string		mailingStreetUnitNumber			= string.Empty;		
		/// <summary>Full Mailing Street Name</summary>
		public string		fullMailingStreetAddress		= string.Empty;
		/// <summary>Mailing City (Phoenix)</summary>
		public string		mailingCity						= string.Empty;
		/// <summary>Mailing State (AZ for Arizona)</summary>
		public string		mailingState					= string.Empty;
		/// <summary>Mailing City Mailing State - (Phoen�x / AZ)</summary>
		public string		mailingCityMailingState			= string.Empty;
		/// <summary>Mailing City Mailing State Mailing ZIP - (Phoen�x / AZ, 11111)</summary>
		public string		mailingCityMailingStateMailingZIP	= string.Empty;
		/// <summary>Mailing ZIP</summary>
		public string		mailingZIP						= string.Empty;
		/// <summary>Mailing Country</summary>
		public string		mailingCountry					= string.Empty;
		#endregion � Mailing Address �		
		#region � Owner �
		/// <summary>Owner Phone at Home</summary>				
		public string	ownerPhoneHome				= string.Empty;
		/// <summary>Owner Phone at Work</summary>
		public string	ownerPhoneWork				= string.Empty;
		/// <summary>Owner Mobile</summary>
		public string	ownerMobile					= string.Empty;
		/// <summary>Owner E-Mail</summary>
		public string	ownerEmail					= string.Empty;
		#endregion � Owner �
		#region � Listing Agent �
		/// <summary>Listing Agent Full Name</summary>				
		public string		listingAgentFullNane			= string.Empty;
		/// <summary>Listing Agent First Name</summary>
		public string		listingAgentFirstName			= string.Empty;
		/// <summary>Listing Agent Last Name</summary>
		public string		listingAgentLastName			= string.Empty;				
		/// <summary>Listing Agent Phone Number</summary>				
		public string		listingAgentPhoneNumber			= string.Empty;
		/// <summary>Listing Agent Fax Number</summary>				
		public string		listingAgentFaxNumber			= string.Empty;
		/// <summary>Listing Agent Office</summary>				
		public string		listingAgentOffice					= string.Empty;
		/// <summary>Listing Agent Code</summary>				
		public string		listingAgentCode				= string.Empty;
		/// <summary>Listing MLS Number</summary>				
		public string		listingMLSNr					= string.Empty;
		/// <summary>Listing Price</summary>				
		public double		listingPrice					= 0;
		/// <summary>Listing Agent Email</summary>
		public string		listingAgentEmail				= string.Empty;		
		#endregion � Listing Agent �	
		#region � PropertyInfo �		
		/// <summary>Status</summary>
		public string		status							= string.Empty;
		/// <summary>Last Sale Date</summary>
		public DateTime		lastSaleDate;
		/// <summary>Last Sale Value</summary>
		public double		lastSaleValue					= 0;
		/// <summary>Sale Document Number</summary>
		public string		saleDocumentNumber				= string.Empty;
		/// <summary>Cost per Square Foot</summary>
		public double		costPerSquareFoot				= 0;
		/// <summary>First Loan</summary>
		public double		firstLoan						= 0;
		/// <summary>Second Load</summary>
		public double		secondLoan						= 0;
		/// <summary>First Loan Type</summary>
		public string		firstLoanType					= string.Empty;
		/// <summary>First Loan Interest Rate</summary>
		public double		firstLoanInterestRate			= 0;
		/// <summary>Lender</summary>
		public string		lender							= string.Empty;
		/// <summary>Loans</summary>
		public double		loans							= 0;
		
		/// <summary>Cash Down Payment</summary>
		public double		cashDownPayment					= 0;

		/// <summary>Parcel Number</summary>
		public string		parcelNumber					= string.Empty;
		/// <summary>Assessor Parcel Num Suffix</summary>
		public string		assessorParcelNumSuffix			= string.Empty;
		/// <summary>Tax Assessed Value</summary>				
		public double		taxAssessedValue				= 0;
		/// <summary>Full Cash Value</summary>
		public double		fullCashValue					= 0;
		/// <summary>Tax Status</summary>
		public string		taxStatus						= string.Empty;
		/// <summary>Tax Deliquent Year</summary>
		public int			taxDeliquentYear				= 0;
		/// <summary>Tax Amount</summary>
		public double		taxAmount						= 0;
		/// <summary>Tax Rate Area</summary>
		public string		taxRateArea						= string.Empty;
		/// <summary>Tax Exemption</summary>
		public string		taxExemption					= string.Empty;
		/// <summary>Percent Improvement</summary>
		public double		percentImprovement				= 0;
		/// <summary>Use Code</summary>
		public string		useCode							= string.Empty;
		/// <summary>Property Type</summary>
		public string		propertyType					= string.Empty;
		/// <summary>Owner State (Occupied, Absent, ...)</summary>
		public string		ownerOccupiedAbsent				= string.Empty;
		/// <summary>Land Value</summary>		
		public double		landValue						= 0;

		/// <summary>ForeClosure Status</summary>
		public string		foreClosureStatus				= string.Empty;
		/// <summary>ForeClosure File Number</summary>
		public string		foreClosureFileNumber			= string.Empty;
		/// <summary>ForeClosure File Date</summary>
		public DateTime	foreClosureFileDate;

		/// <summary>Original Mortgage Balance</summary>				
		public double		originalMortgageBalance			= 0;
		/// <summary>Unpaid Balance</summary>				
		public double		unpaidBalance					= 0;
		/// <summary>Re Instatement Amount</summary>
		public double		reInstatementAmount				= 0;
		/// <summary>Loan Recording Number</summary>
		public string		loanRecordingNumber				= string.Empty;
		/// <summary>Deed Date</summary>
		public string		deedDate						= string.Empty;
		/// <summary>Original Trustor</summary>
		public string		originalTrustor					= string.Empty;
		/// <summary>ForeClosure Sale Date</summary>
		public DateTime		foreClosureSaleDate;
		/// <summary>ForeClosure Sale Time</summary>
		public string		forecClosureSaleTime			= string.Empty;
		/// <summary>ForeClosure Sale Place</summary>
		public string		foreClosureSalePlace			= string.Empty;
		/// <summary>Trustee Sale Number</summary>
		public string		trusteeSaleNumber				= string.Empty;
		/// <summary>Trustee Name</summary>		
		public string		trusteeName						= string.Empty;
		/// <summary>Trustee Phone Number</summary>		
		public string		trusteePhoneNumber				= string.Empty;
		/// <summary>Trustee Full Street Address</summary>		
		public string		trusteeFullStreetAddress		= string.Empty;
		/// <summary>Trustee City</summary>		
		public string		trusteeCity						= string.Empty;
		/// <summary>Trustee State</summary>		
		public string		trusteeState					= string.Empty;
		/// <summary>Trustee ZIP</summary>		
		public string		trusteeZIP						= string.Empty;
		/// <summary>Mortgage Beneficiary First</summary>		
		public string		mortgageBeneficiaryFirst		= string.Empty;
		/// <summary>Mortgage Beneficiary Last</summary>		
		public string		mortgageBeneficiaryLast		= string.Empty;

		/// <summary>Estimated Home Value</summary>
		public double		estimatedHomeValue				= 0;
		/// <summary>Estimated Equity</summary>
		public double		estimatedEquity					= 0;

		/// <summary>Open Bid Amount</summary>
		public double		openBidAmount					= 0;
		/// <summary>BedRooms</summary>
		public double		bedrooms						= 0;
		/// <summary>BathRooms</summary>
		public double		bathrooms						= 0;
		/// <summary>Garage</summary>
		public string		garage							= string.Empty;
		/// <summary>Pool</summary>
		public string		pool							= string.Empty;
		/// <summary>Home Square Footage</summary>
		public double		homeSqFt						= 0;
		/// <summary>Lot Square Footage</summary>
		public double		lotSqFt							= 0;
		/// <summary>Year Built</summary>
		public int			yearBuilt						= 0;
		/// <summary>Story</summary>
		public double		story							= 0;
		/// <summary>Fire Place</summary>
		public string		firePlace						= string.Empty;

		/// <summary>SubDivision</summary>
		public string		subDivision						= string.Empty;
		/// <summary>Legal Description</summary>
		public string		legalDescription				= string.Empty;
		/// <summary>Vesting</summary>
		public string		vesting							= string.Empty;	//what is this? type?

	
		/// <summary>Divorce Status</summary>				
		public string		divorceStatus					= string.Empty;
		/// <summary>Divorce Date</summary>				
		public DateTime	divorceDate;

		/// <summary>Mail CRT</summary>
        public string		mailCRRT						= string.Empty;
		/// <summary>Mail Score</summary>
		public string		mailScore						= string.Empty;
		/// <summary>Privacy Flag</summary>
		public string		privacyFlag						= string.Empty;
		/// <summary>do Not Call Flag</summary>
		public string		doNotCallFlag					= string.Empty;


		/// <summary>User Field 1</summary>				
		public string		userField1						= string.Empty;
		/// <summary>User Field 2</summary>				
		public string		userField2						= string.Empty;
		/// <summary>User Field 3</summary>				
		public string		userField3						= string.Empty;
		/// <summary>User Field 4</summary>				
		public string		userField4						= string.Empty;
		/// <summary>User Field 5</summary>				
		public string		userField5						= string.Empty;		
		
		public string		unpayedBalance					= string.Empty;

		public string		loanRecordingDate				= string.Empty;

		public string		mapGrid							= string.Empty;

		public string		mapArea							= string.Empty;

		public string		bookNumberPartOfAPN				= string.Empty;

		public string		mapNumberPartOfAPN				= string.Empty;

		public string		parcelNumerPartOfAPN			= string.Empty;

		public string		parcelNumSuffixPartOfAPN		= string.Empty;



		#endregion � PropertyInfo �		
		#region � SkipTracedInfo �
		private string skipTracedEmail1 = string.Empty;
        private string skipTracedEmail2 = string.Empty;
        private string skipTracedEmail3 = string.Empty;
		private string skipTracedCellPhone1 = string.Empty;
        private string skipTracedCellPhone2 = string.Empty;
        private string skipTracedCellPhone3 = string.Empty;
		private string skipTracedLandLine1 = string.Empty;
        private string skipTracedLandLine2 = string.Empty;
		#endregion
		#endregion � Data �

		#region � Automatically Generated Properties and Description �
		#region · FirstOwnerName ·
		[Description("First Owner Name")]
		[ItemPosition("-7")]
		public string FirstOwnerName
		{
			get
			{
				return this.ProperCase(this.firstOwnerName);
			}
			set
			{
				this.firstOwnerName = value;
			}
		}
		#endregion · FirstOwnerName ·
		#region · LastOwnerName ·
		[Description("Last Owner Name")]
		[ItemPosition("-6")]
		public string LastOwnerName
		{
			get
			{
				return this.ProperCase(this.lastOwnerName);
			}
			set
			{
				this.lastOwnerName = value;
			}
		}
		#endregion · LastOwnerName ·
		#region · MiddleName ·
		[Description("Middle Owner Name")]		
		[ItemPosition("-5")]
		public string MiddleOwnerName
		{
			get
			{
				return this.ProperCase(this.middleOwnerName);
			}
			set
			{
				this.middleOwnerName = value;
			}
		}
		#endregion · MiddleName ·
		#region · FullOwnerName ·
		[Description("Full Owner Name")]
		[ItemPosition("-8")]
		public string FullOwnerName
		{
			get
			{
				return this.ProperCase(this.fullOwnerName);
			}
			set
			{
				this.fullOwnerName = value;
			}
		}
		#endregion · FullOwnerName ·
		#region � SalutationOwnerName �
		/*[Description("Salutation Owner Name")]		
		[ItemPosition("4")]
		public string SalutationOwnerName
		{
			get {return this.salutationOwnerName;}
			set {this.salutationOwnerName = value;}
		}*/
		#endregion � SalutationOwnerName �
		#region · SiteStreetNumber ·
		[Description("Site Street Number")]		
		[ItemPosition("7")]
		public string SiteStreetNumber
		{
			get
			{
				return this.siteStreetNumber;
			}
			set
			{
				this.siteStreetNumber = value;
			}
		}
		#endregion · SiteStreetNumber ·
		#region · SiteStreetPreDirectional ·
		[Description("Site Street Pre-directional")]		
		[ItemPosition("8")]
		public string SiteStreetPreDirectional
		{
			get
			{
				return this.siteStreetPreDirectional.ToUpper();
			}
			set
			{
				this.siteStreetPreDirectional = value;
			}
		}
		#endregion · SiteStreetPreDirectional ·
		#region · SiteStreetName ·
		[Description("Site Street Name Only")]		
		[ItemPosition("9")]
		public string SiteStreetName
		{
			get
			{
				return this.ProperCase(this.siteStreetName);
			}
			set
			{
				this.siteStreetName = value;
			}
		}
		#endregion · SiteStreetName ·
		#region · SiteStreetPostDirectional ·
		[Description("Site Street Post-directional")]		
		[ItemPosition("10")]
		public string SiteStreetPostDirectional
		{
			get
			{
				return this.siteStreetPostDirectional.ToUpper();
			}
			set
			{
				this.siteStreetPostDirectional = value;
			}
		}
		#endregion · SiteStreetPostDirectional ·
		#region · SiteSteeetSuffix ·
		[Description("Site Steeet Suffix")]		
		[ItemPosition("11")]
		public string SiteSteeetSuffix
		{
			get
			{
				return this.ProperCase(this.siteSteeetSuffix);
			}
			set
			{
				this.siteSteeetSuffix = value;
			}
		}
		#endregion · SiteSteeetSuffix ·
		#region · SiteStreetUnitNumber ·
		[Description("Site Street Unit Number")]		
		[ItemPosition("12")]
		public string SiteStreetUnitNumber
		{
			get
			{
				return this.ProperCase(this.siteStreetUnitNumber);
			}
			set
			{
				this.siteStreetUnitNumber = value;
			}
		}
		#endregion · SiteStreetUnitNumber ·
		#region · FullSiteStreetAddress ·
		[Description("Site FULL Street Address")]		
		[ItemPosition("6")]
		public string FullSiteStreetAddress
		{
			get
			{
				return this.ProperCase(this.fullSiteStreetAddress);
			}
			set
			{
				this.fullSiteStreetAddress = value;
			}
		}
		#endregion · FullSiteStreetAddress ·
		#region · SiteCity ·
		[Description("Site City")]		
		[ItemPosition("13")]
		public string SiteCity
		{
			get
			{
				return this.ProperCase(this.siteCity);
			}
			set
			{
				this.siteCity = value;
			}
		}
		#endregion · SiteCity ·
		#region · SiteState ·
		[Description("Site State")]		
		[ItemPosition("14")]
		public string SiteState
		{
			get
			{
				if (this.siteState.Length == 2)
				{
					return this.siteState.ToUpper();
				}
				else
				{
					return this.ProperCase(this.siteState);
				}
			}
			set
			{
				this.siteState = value;
			}
		}
		#endregion · SiteState ·
		#region · SiteCitySiteState ·
		[Description("Site City / State")]		
		[ItemPosition("16")]
		public string SiteCitySiteState
		{
			get
			{
				//We will manually remove quotation marks that envelope [City, State] field within .csv file
				this.siteCitySiteState = this.siteCitySiteState.Replace("\"", string.Empty);
				return this.ProperCase(this.siteCitySiteState);
			}
			set
			{
				this.siteCitySiteState = value;
			}
		}
		#endregion · SiteCitySiteState ·
		#region · SiteCitySiteStateSiteZIP ·
		/*[Description("Site City Site State Site ZIP")]		
		[ItemPosition("15")]
		public string SiteCitySiteStateSiteZIP
		{
			get
			{
				return this.siteCitySiteStateSiteZIP;
			}
			set
			{
				this.siteCitySiteStateSiteZIP = value;
			}
		}*/
		#endregion · SiteCitySiteStateSiteZIP ·
		#region · SiteZIP ·
		[Description("Site ZIP")]		
		[ItemPosition("15")]
		public string SiteZIP
		{
			get
			{
				return this.ProperCase(this.siteZIP);
			}
			set
			{
				this.siteZIP = value;
			}
		}
		#endregion · SiteZIP ·
		#region · SiteCountry ·
		[Description("County")]		
		[ItemPosition("98")]
		public string SiteCountry
		{
			get
			{
				return this.ProperCase(this.siteCountry);
			}
			set
			{
				this.siteCountry = value;
			}
		}
		#endregion · SiteCountry ·
		#region · MailingStreetNumber ·
		[Description("Mail Street Number")]		
		[ItemPosition("18")]
		public string MailingStreetNumber
		{
			get
			{
				return this.mailingStreetNumber;
			}
			set
			{
				this.mailingStreetNumber = value;
			}
		}
		#endregion · MailingStreetNumber ·
		#region · MailingStreetPreDirectional ·
		[Description("Mail Street Pre-directional")]		
		[ItemPosition("19")]
		public string MailingStreetPreDirectional
		{
			get
			{
				return this.mailingStreetPreDirectional.ToUpper();
			}
			set
			{
				this.mailingStreetPreDirectional = value;
			}
		}
		#endregion · MailingStreetPreDirectional ·
		#region · MailingStreetName ·
		[Description("Mail Street Name Only")]		
		[ItemPosition("20")]
		public string MailingStreetName
		{
			get
			{
				return this.ProperCase(this.mailingStreetName);
			}
			set
			{
				this.mailingStreetName = value;
			}
		}
		#endregion · MailingStreetName ·
		#region · MailingStreetPostDirectional ·
		[Description("Mail Street Post Directional")]		
		[ItemPosition("21")]
		public string MailingStreetPostDirectional
		{
			get
			{
				return this.mailingStreetPostDirectional.ToUpper();
			}
			set
			{
				this.mailingStreetPostDirectional = value;
			}
		}
		#endregion · MailingStreetPostDirectional ·
		#region · MailingSteeetSuffix ·
		[Description("Mail Steeet Suffix")]		
		[ItemPosition("22")]
		public string MailingSteeetSuffix
		{
			get
			{
				return this.ProperCase(this.mailingSteeetSuffix);
			}
			set
			{
				this.mailingSteeetSuffix = value;
			}
		}
		#endregion · MailingSteeetSuffix ·
		#region · MailingStreetUnitNumber ·
		[Description("Mail Street Unit Number")]		
		[ItemPosition("23")]
		public string MailingStreetUnitNumber
		{
			get
			{
				return this.ProperCase(this.mailingStreetUnitNumber);
			}
			set
			{
				this.mailingStreetUnitNumber = value;
			}
		}
		#endregion · MailingStreetUnitNumber ·
		#region · FullMailingStreetAddress ·
		[Description("Mail FULL Street Address")]		
		[ItemPosition("17")]
		public string FullMailingStreetAddress
		{
			get
			{
				return this.ProperCase(this.fullMailingStreetAddress);
			}
			set
			{
				this.fullMailingStreetAddress = value;
			}
		}
		#endregion · FullMailingStreetAddress ·
		#region · MailingCity ·
		[Description("Mail City")]		
		[ItemPosition("24")]
		public string MailingCity
		{
			get
			{
				return this.ProperCase(this.mailingCity);
			}
			set
			{
				this.mailingCity = value;
			}
		}
		#endregion · MailingCity ·
		#region · MailingState ·
		[Description("Mail State")]		
		[ItemPosition("25")]
		public string MailingState
		{
			get
			{
				if (this.mailingState.Length == 2)
				{
					return this.mailingState.ToUpper();
				}							
				else
				{
					return this.ProperCase(this.mailingState);
				}
			}
			set
			{
				this.mailingState = value;
			}
		}
		#endregion · MailingState ·
		#region · MailingCityMailingState ·
		[Description("Mail City / State")]		
		[ItemPosition("27")]
		public string MailingCityMailingState
		{
			get
			{
				//We will manually remove quotation marks that envelope [City, State] field within .csv file
				this.mailingCityMailingState = this.mailingCityMailingState.Replace("\"", string.Empty);
				return this.ProperCase(this.mailingCityMailingState);
			}
			set
			{
				this.mailingCityMailingState = value;
			}
		}
		#endregion · MailingCityMailingState ·
		#region · MailingCityMailingStateMailingZIP ·
		/*[Description("Mailing City Mailing State Mailing ZIP")]		
		[ItemPosition("28")]
		public string MailingCityMailingStateMailingZIP
		{
			get
			{
				return this.mailingCityMailingStateMailingZIP;
			}
			set
			{
				this.mailingCityMailingStateMailingZIP = value;
			}
		}*/
		#endregion · MailingCityMailingStateMailingZIP ·
		#region · MailingZIP ·
		[Description("Mail ZIP")]
		[ItemPosition("26")]
		public string MailingZIP
		{
			get
			{
				return this.ProperCase(this.mailingZIP);
			}
			set
			{
				this.mailingZIP = value;
			}
		}
		#endregion · MailingZIP ·
		#region � MailingCountry �
		/*[Description("Mailing Country")]
		[ItemPosition("30")]
		public string MailingCountry
		{
			get {return this.mailingCountry;}
			set {this.mailingCountry = value;}
		}*/
		#endregion � MailingCountry �
		#region · OwnerPhoneHome ·
		[Description("Owner Phone Number")]
		[ItemPosition("-4")]
		public string OwnerPhoneHome
		{
			get
			{
				return this.ownerPhoneHome;
			}
			set
			{
				this.ownerPhoneHome = value;
			}
		}
		#endregion · OwnerPhoneHome ·
		#region · OwnerPhoneWork ·
		[Description("Owner Phone Work")]
		[ItemPosition("-3")]
		public string OwnerPhoneWork
		{
			get
			{
				return this.ownerPhoneWork;
			}
			set
			{
				this.ownerPhoneWork = value;
			}
		}
		#endregion · OwnerPhoneWork ·
		#region · OwnerMobile ·
		[Description("Owner Mobile")]
		[ItemPosition("-2")]
		public string OwnerMobile
		{
			get
			{
				return this.ownerMobile;
			}
			set
			{
				this.ownerMobile = value;
			}
		}
		#endregion · OwnerMobile ·
		#region · OwnerEmail ·
		[Description("Owner Email")]
		[ItemPosition("-1")]
		public string OwnerEmail
		{
			get
			{
				return this.ownerEmail;
			}
			set
			{
				this.ownerEmail = value;
			}
		}
		#endregion · OwnerEmail ·
		#region · ListingAgentFullNane ·
		[Description("Listing Agent Full Name")]
		[ItemPosition("67")]
		public string ListingAgentFullNane
		{
			get
			{
				if (string.Empty == this.listingAgentFullNane.Trim())
				{
					string fn = string.Empty;
					if (string.Empty != this.listingAgentFirstName)
						fn += this.listingAgentFirstName;
					
					if (string.Empty != this.listingAgentLastName)
					{
						if (string.Empty != fn)
							fn += " ";
						fn += this.listingAgentLastName;
					}
					return fn;
				}
				else
				{
					return this.ProperCase(this.listingAgentFullNane);
				}
			}
			set
			{
				this.listingAgentFullNane = value;
			}
		}
		#endregion · ListingAgentFullNane ·
		#region · ListingAgentFirstName ·
		[Description("Listing Agent First Name")]
		[ItemPosition("68")]
		public string ListingAgentFirstName
		{
			get
			{
				return this.ProperCase(this.listingAgentFirstName);
			}
			set
			{
				this.listingAgentFirstName = value;
			}
		}
		#endregion · ListingAgentFirstName ·
		#region · ListingAgentLastName ·
		[Description("Listing Agent Last Name")]
		[ItemPosition("69")]
		public string ListingAgentLastName
		{
			get
			{
				return this.ProperCase(this.listingAgentLastName);
			}
			set
			{
				this.listingAgentLastName = value;
			}
		}
		#endregion · ListingAgentLastName ·
		#region · ListingAgentPhoneNumber ·
		[Description("Listing Agent Phone Number")]
		[ItemPosition("70")]
		public string ListingAgentPhoneNumber
		{
			get
			{
				return this.listingAgentPhoneNumber;
			}
			set
			{
				this.listingAgentPhoneNumber = value;
			}
		}
		#endregion · ListingAgentPhoneNumber ·
		#region · ListingAgentFaxNumber ·
		[Description("Listing Agent Fax Number")]
		[ItemPosition("71")]
		public string ListingAgentFaxNumber
		{
			get
			{
				return this.listingAgentFaxNumber;
			}
			set
			{
				this.listingAgentFaxNumber = value;
			}
		}
		#endregion · ListingAgentFaxNumber ·
		#region · ListingAgentOffice ·
		[Description("Listing Agent Office")]
		[ItemPosition("72")]
		public string ListingAgentOffice
		{
			get
			{
				return this.ProperCase(this.listingAgentOffice);
			}
			set
			{
				this.listingAgentOffice = value;
			}
		}
		#endregion · ListingAgentOffice ·
		#region · ListingAgentCode ·
		[Description("Listing Agent Code")]
		[ItemPosition("73")]
		public string ListingAgentCode
		{
			get
			{
				return this.listingAgentCode;
			}
			set
			{
				this.listingAgentCode = value;
			}
		}
		#endregion · ListingAgentCode ·
		#region · ListingMLSNr ·
		[Description("Listing MLS Nr")]
		[ItemPosition("74")]
		public string ListingMLSNr
		{
			get
			{
				return this.listingMLSNr;
			}
			set
			{
				this.listingMLSNr = value;
			}
		}
		#endregion · ListingMLSNr ·
		#region · ListingPrice ·
		[Description("Listing Price")]
		[ItemPosition("75")]
		public double ListingPrice
		{
			get
			{
				return this.listingPrice;
			}
			set
			{
				this.listingPrice = value;
			}
		}
		#endregion · ListingPrice ·
		#region · ListingAgentEmail ·
		[Description("Listing Agent Email")]
		[ItemPosition("76")]
		public string ListingAgentEmail
		{
			get
			{
				return this.listingAgentEmail;
			}
			set
			{
				this.listingAgentEmail = value;
			}
		}
		#endregion · ListingAgentEmail ·
		#region · Status ·
		[Description(" Status")]
		[ItemPosition("66")]
		public string Status
		{
			get
			{
				return this.status;
			}
			set
			{
				this.status = value;
			}
		}
		#endregion · Status ·
		#region · ParcelNumber ·
		[Description("Assessor Parcel Number (APN)")]
		[ItemPosition("51")]
		public string ParcelNumber
		{
			get
			{
				return this.parcelNumber;
			}
			set
			{
				this.parcelNumber = value;
			}
		}
		#endregion · ParcelNumber ·
		#region · LastSaleDate ·
		[Description("Sale Date")]
		[ItemPosition("38")]
		public DateTime LastSaleDate
		{
			get
			{
				return this.lastSaleDate;
			}
			set
			{
				this.lastSaleDate = value;
			}
		}
		#endregion · LastSaleDate ·
		#region · LastSaleValue ·
		[Description("Sale Value")]
		[ItemPosition("37")]
		public double LastSaleValue
		{
			get
			{
				return this.lastSaleValue;
			}
			set
			{
				this.lastSaleValue = value;
			}
		}
		#endregion · LastSaleValue ·
		#region · SaleDocumentNumber ·
		[Description("Sale Document Number")]
		[ItemPosition("39")]
		public string SaleDocumentNumber
		{
			get
			{
				return this.saleDocumentNumber;
			}
			set
			{
				this.saleDocumentNumber = value;
			}
		}
		#endregion · SaleDocumentNumber ·
		#region · CostPerSquareFoot ·
		[Description("Cost Per Square Foot")]
		[ItemPosition("48")]
		public double CostPerSquareFoot
		{
			get
			{
				return this.costPerSquareFoot;
			}
			set
			{
				this.costPerSquareFoot = value;
			}
		}
		#endregion · CostPerSquareFoot ·
		#region · FirstLoan ·
		[Description("First Loan Amount")]
		[ItemPosition("41")]
		public double FirstLoan
		{
			get
			{
				return this.firstLoan;
			}
			set
			{
				this.firstLoan = value;
			}
		}
		#endregion · FirstLoan ·
		#region · SecondLoan ·
		[Description("Second Loan Amount")]
		[ItemPosition("42")]
		public double SecondLoan
		{
			get
			{
				return this.secondLoan;
			}
			set
			{
				this.secondLoan = value;
			}
		}
		#endregion · SecondLoan ·
		#region · FirstLoanType ·
		[Description("First Loan Type")]
		[ItemPosition("43")]
		public string FirstLoanType
		{
			get
			{
				return this.firstLoanType;
			}
			set
			{
				this.firstLoanType = value;
			}
		}
		#endregion · FirstLoanType ·
		#region · FirstLoanInterestRate ·
		[Description("First Loan Interest Rate")]
		[ItemPosition("44")]
		public double FirstLoanInterestRate
		{
			get
			{
				return this.firstLoanInterestRate;
			}
			set
			{
				this.firstLoanInterestRate = value;
			}
		}
		#endregion · FirstLoanInterestRate ·
		#region · Lender ·
		[Description("Lender")]
		[ItemPosition("45")]
		public string Lender
		{
			get
			{
				return this.lender;
			}
			set
			{
				this.lender = value;
			}
		}
		#endregion · Lender ·
		#region · Loans ·
		[Description("Loans")]
		[ItemPosition("46")]
		public double Loans
		{
			get
			{
				return this.loans;
			}
			set
			{
				this.loans = value;
			}
		}
		#endregion · Loans ·
		#region · Equity ·
		[Description("Equity")]
		[ItemPosition("50")]
		public double Equity
		{
			get
			{
				return this.estimatedEquity;
			}
			set
			{
				this.estimatedEquity = value;
			}
		}
		#endregion · Equity ·
		#region · CashDownPayment ·
		[Description("Cash Down Payment")]
		[ItemPosition("47")]
		public double CashDownPayment
		{
			get
			{
				return this.cashDownPayment;
			}
			set
			{
				this.cashDownPayment = value;
			}
		}
		#endregion · CashDownPayment ·
		#region · TaxAssessedValue ·
		[Description("Tax Assessed Value")]
		[ItemPosition("56")]
		public double TaxAssessedValue
		{
			get
			{
				return this.taxAssessedValue;
			}
			set
			{
				this.taxAssessedValue = value;
			}
		}
		#endregion · TaxAssessedValue ·
		#region · FullCashValue ·
		[Description("Full Cash Value")]
		[ItemPosition("57")]
		public double FullCashValue
		{
			get
			{
				return this.fullCashValue;
			}
			set
			{
				this.fullCashValue = value;
			}
		}
		#endregion · FullCashValue ·
		#region · TaxStatus ·
		[Description("Tax Status")]
		[ItemPosition("58")]
		public string TaxStatus
		{
			get
			{
				return this.taxStatus;
			}
			set
			{
				this.taxStatus = value;
			}
		}
		#endregion · TaxStatus ·
		#region · TaxDeliquentYear ·
		[Description("Tax Deliquent Year")]
		[ItemPosition("59")]
		public int TaxDeliquentYear
		{
			get
			{
				return this.taxDeliquentYear;
			}
			set
			{
				this.taxDeliquentYear = value;
			}
		}
		#endregion · TaxDeliquentYear ·
		#region · TaxAmount ·
		[Description("Tax Amount")]
		[ItemPosition("60")]
		public double TaxAmount
		{
			get
			{
				return this.taxAmount;
			}
			set
			{
				this.taxAmount = value;
			}
		}
		#endregion · TaxAmount ·
		#region · TaxRateArea ·
		[Description("Tax Rate Area")]
		[ItemPosition("61")]
		public string TaxRateArea
		{
			get
			{
				return this.taxRateArea;
			}
			set
			{
				this.taxRateArea = value;
			}
		}
		#endregion · TaxRateArea ·
		#region · TaxExemption ·
		[Description("Tax Exemption")]
		[ItemPosition("62")]
		public string TaxExemption
		{
			get
			{
				return this.taxExemption;
			}
			set
			{
				this.taxExemption = value;
			}
		}
		#endregion · TaxExemption ·
		#region · PercentImprovement ·
		[Description("Percent Improvement")]
		[ItemPosition("63")]
		public double PercentImprovement
		{
			get
			{
				return this.percentImprovement;
			}
			set
			{
				this.percentImprovement = value;
			}
		}
		#endregion · PercentImprovement ·
		#region · UseCode ·
		[Description("Use Code")]
		[ItemPosition("28")]
		public string UseCode
		{
			get
			{
				return this.useCode;
			}
			set
			{
				this.useCode = value;
			}
		}
		#endregion · UseCode ·
		#region · OwnerOccupiedAbsent ·
		[Description("Owner Absent/Occupied")]
		[ItemPosition("64")]
		public string OwnerOccupiedAbsent
		{
			get
			{
				return this.ownerOccupiedAbsent;
			}
			set
			{
				this.ownerOccupiedAbsent = value;
			}
		}
		#endregion · OwnerOccupiedAbsent ·
		#region · LandValue ·
		[Description("Land Value")]
		[ItemPosition("65")]
		public double LandValue
		{
			get
			{
				return this.landValue;
			}
			set
			{
				this.landValue = value;
			}
		}
		#endregion · LandValue ·
		#region · ForeClosureStatus ·
		[Description("Foreclosure Status")]
		[ItemPosition("77")]
		public string ForeClosureStatus
		{
			get
			{
				return this.foreClosureStatus;
			}
			set
			{
				this.foreClosureStatus = value;
			}
		}
		#endregion · ForeClosureStatus ·
		#region · ForeClosureFileNumber ·
		[Description("Foreclosure File Number")]
		[ItemPosition("78")]
		public string ForeClosureFileNumber
		{
			get
			{
				return this.foreClosureFileNumber;
			}
			set
			{
				this.foreClosureFileNumber = value;
			}
		}
		#endregion · ForeClosureFileNumber ·
		#region · ForeClosureFileDate ·
		[Description("Foreclosure File Date")]
		[ItemPosition("79")]
		public DateTime ForeClosureFileDate
		{
			get
			{
				return this.foreClosureFileDate;
			}
			set
			{
				this.foreClosureFileDate = value;
			}
		}
		#endregion · ForeClosureFileDate ·
		#region · OriginalMortgageBalance ·
		[Description("Original Mortgage Balance")]
		[ItemPosition("80")]
		public double OriginalMortgageBalance
		{
			get
			{
				return this.originalMortgageBalance;
			}
			set
			{
				this.originalMortgageBalance = value;
			}
		}
		#endregion · OriginalMortgageBalance ·
		#region · ReInstatementAmount ·
		/*[Description("ReInstatement Amount")]
		[ItemPosition("74")]
		public double ReInstatementAmount
		{
			get
			{
				return this.reInstatementAmount;
			}
			set
			{
				this.reInstatementAmount = value;
			}
		}*/
		#endregion · ReInstatementAmount ·
		#region · ForeClosureSaleDate ·
		[Description("Foreclosure Sale Date")]
		[ItemPosition("85")]
		public DateTime ForeClosureSaleDate
		{
			get
			{
				return this.foreClosureSaleDate;
			}
			set
			{
				this.foreClosureSaleDate = value;
			}
		}
		#endregion · ForeClosureSaleDate ·
		#region · OpenBidAmount ·
		[Description("Open Bid Amount")]
		[ItemPosition("95")]
		public double OpenBidAmount
		{
			get
			{
				return this.openBidAmount;
			}
			set
			{
				this.openBidAmount = value;
			}
		}
		#endregion · OpenBidAmount ·
		#region · Bedrooms ·
		[Description("Bedrooms")]
		[ItemPosition("30")]
		public double Bedrooms
		{
			get
			{
				return this.bedrooms;
			}
			set
			{
				this.bedrooms = value;
			}
		}
		#endregion · Bedrooms ·
		#region · Bathrooms ·
		[Description("Bathrooms")]
		[ItemPosition("31")]
		public double Bathrooms
		{
			get
			{
				return this.bathrooms;
			}
			set
			{
				this.bathrooms = value;
			}
		}
		#endregion · Bathrooms ·
		#region · Garage ·
		[Description("Garage")]
		[ItemPosition("32")]
		public string Garage
		{
			get
			{
				return this.garage;
			}
			set
			{
				this.garage = value;
			}
		}
		#endregion · Garage ·
		#region · Pool ·
		[Description("Pool")]
		[ItemPosition("33")]
		public string Pool
		{
			get
			{
				return this.pool;
			}
			set
			{
				this.pool = value;
			}
		}
		#endregion · Pool ·
		#region · HomeSqFt ·
		[Description("Home Sq Ft")]
		[ItemPosition("34")]
		public double HomeSqFt
		{
			get
			{
				return this.homeSqFt;
			}
			set
			{
				this.homeSqFt = value;
			}
		}
		#endregion · HomeSqFt ·
		#region · LotSqFt ·
		[Description("Lot Sq Ft")]
		[ItemPosition("35")]
		public double LotSqFt
		{
			get
			{
				return this.lotSqFt;
			}
			set
			{
				this.lotSqFt = value;
			}
		}
		#endregion · LotSqFt ·
		#region · YearBuilt ·
		[Description("Year Built")]
		[ItemPosition("36")]
		public int YearBuilt
		{
			get
			{
				return this.yearBuilt;
			}
			set
			{
				this.yearBuilt = value;
			}
		}
		#endregion · YearBuilt ·
		#region · Story ·
		/*[Description("Story")]
		[ItemPosition("84")]
		public double Story
		{
			get
			{
				return this.story;
			}
			set
			{
				this.story = value;
			}
		}*/
		#endregion · Story ·
		#region · FirePlace ·
		/*[Description("Fire Place")]
		[ItemPosition("85")]
		public string FirePlace
		{
			get
			{
				return this.firePlace;
			}
			set
			{
				this.firePlace = value;
			}
		}*/
		#endregion · FirePlace ·
		#region · SubDivision ·
		[Description("SubDivision")]
		[ItemPosition("99")]
		public string SubDivision
		{
			get
			{
				return this.subDivision;
			}
			set
			{
				this.subDivision = value;
			}
		}
		#endregion · SubDivision ·
		#region · LegalDescription ·
		[Description("Legal Description")]
		[ItemPosition("100")]
		public string LegalDescription
		{
			get
			{
				return this.legalDescription;
			}
			set
			{
				this.legalDescription = value;
			}
		}
		#endregion · LegalDescription ·
		#region · Vesting ·
		[Description("Vesting")]
		[ItemPosition("101")]
		public string Vesting
		{
			get
			{
				return this.vesting;
			}
			set
			{
				this.vesting = value;
			}
		}
		#endregion · Vesting ·
		#region · DivorceStatus ·
		[Description("Divorce Status")]
		[ItemPosition("104")]
		public string DivorceStatus
		{
			get
			{
				return this.divorceStatus;
			}
			set
			{
				this.divorceStatus = value;
			}
		}
		#endregion · DivorceStatus ·
		#region · DivorceDate ·
		[Description("Divorce Date")]
		[ItemPosition("105")]
		public DateTime DivorceDate
		{
			get
			{
				return this.divorceDate;
			}
			set
			{
				this.divorceDate = value;
			}
		}
		#endregion · DivorceDate ·
		#region · MailCRRT ·
		[Description("Mail CRRT")]
		[ItemPosition("106")]
		public string MailCRRT
		{
			get
			{
				return this.mailCRRT;
			}
			set
			{
				this.mailCRRT = value;
			}
		}
		#endregion · MailCRRT ·
		#region · MailScore ·
		[Description("Mail Score")]
		[ItemPosition("107")]
		public string MailScore
		{
			get
			{
				return this.mailScore;
			}
			set
			{
				this.mailScore = value;
			}
		}
		#endregion · MailScore ·
		#region · PrivacyFlag ·
		[Description("Privacy Flag")]
		[ItemPosition("108")]
		public string PrivacyFlag
		{
			get
			{
				return this.privacyFlag;
			}
			set
			{
				this.privacyFlag = value;
			}
		}
		#endregion · PrivacyFlag ·
		#region · DoNotCallFlag ·
		[Description("Do Not Call Flag")]
		[ItemPosition("109")]
		public string DoNotCallFlag
		{
			get
			{
				return this.doNotCallFlag;
			}
			set
			{
				this.doNotCallFlag = value;
			}
		}
		#endregion · DoNotCallFlag ·
		/*#region · UserField1 ·
		[Description("User Field1")]
		public string UserField1
		{
			get
			{
				return this.userField1;
			}
			set
			{
				this.userField1 = value;
			}
		}
		#endregion · UserField1 ·
		#region · UserField2 ·
		[Description("User Field2")]
		public string UserField2
		{
			get
			{
				return this.userField2;
			}
			set
			{
				this.userField2 = value;
			}
		}
		#endregion · UserField2 ·
		#region · UserField3 ·
		[Description("User Field3")]
		public string UserField3
		{
			get
			{
				return this.userField3;
			}
			set
			{
				this.userField3 = value;
			}
		}
		#endregion · UserField3 ·
		#region · UserField4 ·
		[Description("User Field4")]
		public string UserField4
		{
			get
			{
				return this.userField4;
			}
			set
			{
				this.userField4 = value;
			}
		}
		#endregion · UserField4 ·
		#region · UserField5 ·
		[Description("User Field5")]
		public string UserField5
		{
			get
			{
				return this.userField5;
			}
			set
			{
				this.userField5 = value;
			}
		}
		#endregion · UserField5 ·	*/	

		[Description("Property Type")]
		[ItemPosition("29")]
		public string PropertyType
		{
			get {return this.propertyType;}
			set { this.propertyType = value; }
		}

		[Description("Deed Date")]
		[ItemPosition("40")]
		public string DeedDate
		{
			get {return this.deedDate;}
			set {this.deedDate = value; }
		}

		[Description("Estimated Home Value")]
		[ItemPosition("49")]
		public double EstimatedHomeValue
		{
			get {return this.estimatedHomeValue;}
			set {this.estimatedHomeValue = value;}
		}

		[Description("Book Number Part Of APN")]
		[ItemPosition("52")]
		public string BookNumberPartOfAPN
		{
			get {return this.bookNumberPartOfAPN;}
			set {this.bookNumberPartOfAPN = value; }
		}

		[Description("Map Number Part Of APN")]
		[ItemPosition("53")]
		public string MapNumberPartOfAPN
		{
			get {return this.mapNumberPartOfAPN;}
			set {this.mapNumberPartOfAPN = value; }
		}

		[Description("Parcel Number Part Of APN")]
		[ItemPosition("54")]
		public string ParcelNumerPartOfAPN
		{
			get {return this.parcelNumerPartOfAPN;}
			set {this.parcelNumerPartOfAPN = value;}
		}

		[Description("Parcel Num. Suffix Part Of APN")]
		[ItemPosition("55")]
		public string ParcelNumSuffixPartOfAPN
		{
			get {return this.parcelNumSuffixPartOfAPN;}
			set {this.parcelNumSuffixPartOfAPN = value;}
		}

		[Description("Unpaid Balance")]
		[ItemPosition("81")]
		public string UnpayedBalance
		{
			get {return this.unpayedBalance;}
			set {this.unpayedBalance = value;}
		}

		[Description("Loan Recording Number")]
		[ItemPosition("82")]
		public string LoanRecordingNumber
		{
			get {return this.loanRecordingNumber;}
			set {this.loanRecordingNumber = value; }
		}

		[Description("Loan Recording Date")]
		[ItemPosition("83")]
		public string LoanRecordingDate
		{
			get {return this.loanRecordingDate;}
			set {this.loanRecordingDate = value;}
		}

		[Description("Original Trustor")]
		[ItemPosition("84")]
		public string OriginalTrustor
		{
			get { return this.originalTrustor; }
			set { this.originalTrustor = value; }
		}

		[Description("Foreclosure Sale Time")]
		[ItemPosition("86")]
		public string ForeclosureSaleTime
		{
			get { return this.forecClosureSaleTime; }
			set { this.forecClosureSaleTime = value; }
		}

		[Description("Foreclosure Sale Place")]
		[ItemPosition("87")]
		public string ForeclosureSalePlace
		{
			get { return this.foreClosureSalePlace; }
			set { this.foreClosureSalePlace = value; }
		}

		[Description("Trustee Sale Number")]
		[ItemPosition("88")]
		public string TrusteeSaleNumber
		{
			get { return this.trusteeSaleNumber; }
			set { this.trusteeSaleNumber = value; }
		}


		[Description("Trustee Name")]
		[ItemPosition("89")]
		public string TrusteeName
		{
			get { return this.trusteeName; }
			set { this.trusteeName = value; }
		}

		[Description("Trustee Phone Number")]
		[ItemPosition("90")]
		public string TrusteePhoneNumber
		{
			get { return this.trusteePhoneNumber; }
			set { this.trusteePhoneNumber = value; }
		}

		[Description("Trustee Full Street Address")]
		[ItemPosition("91")]
		public string TrusteeFullStreetAddress
		{
			get { return this.trusteeFullStreetAddress; }
			set { this.trusteeFullStreetAddress = value; }
		}

		[Description("Trustee City")]
		[ItemPosition("92")]
		public string TrusteeCity
		{
			get {return this.trusteeCity; }
			set { this.trusteeCity = value; }
		}

		[Description("Trustee State")]
		[ItemPosition("93")]
		public string TrusteeState
		{
			get {return this.trusteeState; }
			set {this.trusteeState = value; }
		}

		[Description("Trustee ZIP")]
		[ItemPosition("94")]
		public string TrusteeZIP
		{
			get { return this.trusteeZIP; }
			set { this.trusteeZIP = value; }
		}
		
		[Description("Mortgage Beneficiary First")]
		[ItemPosition("96")]
		public string MortgageBeneficiaryFirst
		{
			get { return this.mortgageBeneficiaryFirst; }
			set { this.mortgageBeneficiaryFirst = value; }
		}

		[Description("Mortgage Beneficiary Last")]
		[ItemPosition("97")]
		public string MortgageBeneficiaryLast
		{
			get { return this.mortgageBeneficiaryLast; }
			set { this.mortgageBeneficiaryLast = value; }
		}

		[Description("Map Grid")]
		[ItemPosition("102")]
		public string MapGrid
		{
			get {return this.mapGrid;}
			set {this.mapGrid = value;}
		}

		[Description("Map Area")]
		[ItemPosition("103")]
		public string MapArea
		{
			get {return this.mapArea;}
			set {this.mapArea = value;}
		}

		[Description("Skiptraced Email Rank 0")]
		[ItemPosition("500")]
		public string SkipTracedEmail1 { get => skipTracedEmail1; set => skipTracedEmail1 = value; }

        [Description("Skiptraced Email Rank 1")]
        [ItemPosition("501")]
        public string SkipTracedEmail2 { get => skipTracedEmail2; set => skipTracedEmail2 = value; }

        [Description("Skiptraced Email Rank 2")]
        [ItemPosition("502")]
        public string SkipTracedEmail3 { get => skipTracedEmail3; set => skipTracedEmail3 = value; }

		[Description("Skiptraced Mobile Rank 0")]
		[ItemPosition("503")]
		public string SkipTracedCellPhone1 { get => skipTracedCellPhone1; set => skipTracedCellPhone1 = value; }

        [Description("Skiptraced Mobile Rank 1")]
        [ItemPosition("504")]
        public string SkipTracedCellPhone2 { get => skipTracedCellPhone2; set => skipTracedCellPhone2 = value; }

        [Description("Skiptraced Mobile Rank 2")]
        [ItemPosition("505")]
        public string SkipTracedCellPhone3 { get => skipTracedCellPhone3; set => skipTracedCellPhone3 = value; }

		[Description("Skiptraced Residential Phone Rank 0")]
		[ItemPosition("506")]
		public string SkipTracedLandLine1 { get => skipTracedLandLine1; set => skipTracedLandLine1 = value; }

        [Description("Skiptraced Residential Phone Rank 1")]
        [ItemPosition("507")]
        public string SkipTracedLandLine2 { get => skipTracedLandLine2; set => skipTracedLandLine2 = value; }

		#endregion � Automatically Generated Properties and Description �

		#region � CTR �
		public ImportIntermedeateObject()
		{			
		}
		#endregion � CTR �

		private string ProperCase(string s)
		{
			if (0 == s.Length)
				return string.Empty;

			s = s.ToLower();
			string sProper = "";

			char[] seps = new char[]{' '};
			foreach( string ss in s.Split(seps) )
			{
				if (ss.Length == 0)
					continue;

				sProper += char.ToUpper(ss[0]);
				sProper += (ss.Substring(1, ss.Length - 1) + ' ');
			}
			return sProper.TrimEnd();
		}
	}
	#endregion � Class ImportIntermedeateObject �
}
#endregion � Namespace DealMaker �