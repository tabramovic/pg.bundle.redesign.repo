using System;
using System.IO;
using System.Collections;
using DealMaker.Classes;
using DealMaker.Filters;
using DealMaker.PlainClasses;
using System.Text;
using System.Linq;
using System.Collections.Generic;
using System.Windows.Forms.VisualStyles;

namespace DealMaker.Export
{
	public class PropertyItemsExport
	{
		MemoryStream memStream = new MemoryStream();
		UTF8Encoding utf8 = new UTF8Encoding();

		string exportFileName = String.Empty;
		ArrayList filters = new ArrayList();
		ArrayList mappings = new ArrayList();
		ArrayList propertyItems = new ArrayList();
		IExportWriter exportWriter = new CSVExportWriter();

		ArrayList header = new ArrayList();

		public PropertyItemsExport(PropertyItem[] items)
		{
			foreach (PropertyItem item in items)
				propertyItems.Add(item);
		}

		protected void WriteHeader(StreamWriter sw)
		{			
			foreach(ExportAttributeMapping m in Mappings)
				header.Add(m.ColumnName);

			exportWriter.WriteHeader(sw, (string[])header.ToArray(typeof(string)));
			
			SpreadSheetData.Add(((object[])header
				.ToArray(typeof(object)))
				.ToList());
		}

		public void AddMapping(ExportAttributeMapping mapping)
		{
			mappings.Add(mapping);
		}

		public void RemoveMapping(ExportAttributeMapping mapping)
		{
			mappings.Remove(mapping);
		}

		public void AddFilter(IPropertyItemFilter filter)
		{
			filters.Add(filter);
		}

		public void RemoveFilter(IPropertyItemFilter filter)
		{
			filters.Remove(filter);
		}

		public MemoryStream MemStream
        {
			get { return memStream; }
        }
		
		public List<string> Header
        {
			get { return header.ToArray().Select(item => item.ToString()).ToList(); }
        }

		public IList<IList<object>> SpreadSheetData { get; set; }

		public int Execute()
		{
			int count = 0;			
			SpreadSheetData = new List<IList<object>>();

			try
			{
				using (StreamWriter sw = !string.IsNullOrEmpty(ExportFileName)
					? new StreamWriter(ExportFileName)
					: new StreamWriter(memStream, utf8)
					)
				{
					WriteHeader(sw);

					var sortedExportObjects = ExportObjects.OrderBy(pi => pi.IdPropertyItem).ToArray();
					foreach (PropertyItem item in sortedExportObjects)
					{
						ArrayList al = new ArrayList();

						foreach (ExportAttributeMapping m in Mappings)
						{  
							if (m.AttributeName != String.Empty &&
								m.AttributeName != ExportAttributeMapping.EmptyColumn &&
								null != item[m.AttributeName] 		//TA: ++Added
								//&& null != item[m.AttributeName].AttributeValue		//TA: ++Added, -- 02.04.2005. - COMMENTED OUT
								)							
							{	
								try
								{
                                    if (null != item && null != m && null != m.AttributeName && null != item[m.AttributeName] && null != item[m.AttributeName].AttributeValue)
                                        al.Add(item[m.AttributeName].AttributeValue.ToString());
                                    else
                                    {
                                        PropertyItemAttribute pia = (PropertyItemAttribute)item[m.AttributeName];
                                        if (null != pia)
                                        {
                                            string text = pia.AttributeName;
                                            al.Add(text);
                                        }
                                        else
                                            al.Add(string.Empty);
                                    }
								}
								catch (Exception e)
								{
									PropertyItemAttribute pia = (PropertyItemAttribute)item[m.AttributeName];
									if (null != pia)
									{
										string text = pia.AttributeName;
										al.Add(text);
									}
									else
										al.Add(string.Empty);
								}
							}
							else
								al.Add(String.Empty);
						}

						exportWriter.WriteRow(sw, (string[])al.ToArray(typeof(string)));
						SpreadSheetData.Add(((object[])al
							.ToArray(typeof(object)))
							.ToList());
						count += 1;
					}
				}
			}
			catch (
				System.Exception
#if DEBUG
				exc
#endif
				)
			{
#if DEBUG
				System.Windows.Forms.MessageBox.Show("Message: " + exc.Message + "\nInner Exception: " + exc.InnerException + "\nStack: " + exc.StackTrace);
#endif
				return 0;
			}

			return count;
		}

		#region Properties
		
		public string ExportFileName
		{
			get
			{
				return this.exportFileName;
			}
			set
			{
				this.exportFileName = value;
			}
		}

		public IExportWriter ExportWriter
		{
			get
			{
				return exportWriter;
			}
			set
			{
				if (null == value)
					throw new System.Exception("Invalid value for ExportWriter property");

				exportWriter = value;
			}
		}

		public PropertyItem[] ExportObjects
		{
			get
			{
				IEnumerator ie = propertyItems.GetEnumerator();
				ArrayList result = new ArrayList();

				while (ie.MoveNext())
				{
					bool passed = true;
					foreach (IPropertyItemFilter f in Filters)
					{
						passed &= f.EvaluateObject(ie.Current as PropertyItem);

						if (!passed)
							break;
					}

					if (!passed)
						continue;

					result.Add(ie.Current);
				}
				
				return (PropertyItem[])result.ToArray(typeof(PropertyItem));
			}
		}

		public ExportAttributeMapping[] Mappings
		{
			get 
			{
				return (ExportAttributeMapping[])mappings.ToArray(typeof(ExportAttributeMapping));
			}
		}

		public IPropertyItemFilter[] Filters
		{
			get
			{
				return (IPropertyItemFilter[])filters.ToArray(typeof(IPropertyItemFilter));
			}
		}

		#endregion
	}
}
