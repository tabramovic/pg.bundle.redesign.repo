using System;
using System.Collections;

namespace DealMaker.Export
{
	/// ihuk, 04.01.2005
	/// <summary>
	/// Represents a export mapping profile.
	/// </summary>
	public class ExportMappingProfile
	{
		private string name = String.Empty;
		private string description = String.Empty;
		private SimpleFilter[] simpleFilters = null;
		private MathematicalFilter[] mathFilters = null;
		private ExportAttributeMapping[] mappings = null;

		public ExportMappingProfile()
		{
		}

		public ExportMappingProfile(string name, string description):
			this()
		{
			this.name = name;
			this.description = description;
		}

		public ExportMappingProfile(
			string description, string name,
			SimpleFilter[] simpleFilters,
			MathematicalFilter[] mathFilters,
			ExportAttributeMapping[] mappings):
			this(description, name)
		{
			this.simpleFilters = simpleFilters;
			this.mathFilters = mathFilters;
			this.mappings = mappings;
		}

		#region Properties
		public string Name
		{
			get
			{
				return name;
			}
			set
			{
				name = value;
			}
		}

		public string Description
		{
			get
			{
				return description;
			}
			set
			{
				description = value;
			}
		}

		public SimpleFilter[] SimpleFilters
		{
			get 
			{ 
				return simpleFilters; 
			}
			set 
			{
				simpleFilters = value;
			}
		}

		public MathematicalFilter[] MathFilters
		{
			get 
			{ 
				return mathFilters;
			}
			set
			{
				mathFilters = value;
			}
		}

		public ExportAttributeMapping[] Mappings
		{
			get
			{
				return mappings;
			}
			set
			{
				mappings = value;
			}
		}
		#endregion
	}
}
