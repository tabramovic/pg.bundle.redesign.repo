using System;
using System.IO;

namespace DealMaker.Export
{
	public class CSVExportWriter: IExportWriter
	{
		public void WriteHeader(StreamWriter sw, string[] header)
		{
			string headerString = String.Empty;
			
			for (int i = 0; i < header.Length; i++)
			{
				//TA++ 14.06.2005.
				//RMW bug in excel with En-en settings
				// replaced ; with ,
				//OLD++
				//headerString += header[i] + ((i == header.Length - 1) ? String.Empty : ";");
				//OLD--
				//NEW++
				string tempHeader = string.Empty;
				if (header[i].IndexOf(",") != -1)
					tempHeader = "\"" + header[i] + "\"";
				else
					tempHeader = (string)header[i];

				headerString += tempHeader + ((i == header.Length - 1) ? String.Empty : ",");
				//NEW--
			}

			sw.WriteLine(headerString);
		}

		public void WriteRow(StreamWriter sw, string[] row)
		{
			string rowString = String.Empty;
			
			for (int i = 0; i < row.Length; i++)
			{
				//TA++ 14.06.2005.
				//RMW bug in excel with En-en settings
				// replaced ; with ,
				//OLD++
				//rowString += row[i] + ((i == row.Length - 1) ? String.Empty : ";");				
				//OLD--
				//NEW++
				string tempRow = string.Empty;
				if (row[i].IndexOf(",") != -1)
					tempRow = "\"" + row[i] + "\"";
				else
					tempRow = (string)row[i];
				rowString += tempRow + ((i == row.Length - 1) ? String.Empty : ",");
				//NEW--
			}

			sw.WriteLine(rowString);
		}

		public string GetDefaultExtension()
		{
			return ".csv";
		}

		public override string ToString()
		{
			return "CSV Text file";
		}

	}
}
