﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace DealMaker.Export
{    
    public interface IGoogleAccountsHandler
    {
        bool Save(string[] googleAccountNames, string lastUsedAccountName);
        ExportAccounts GetGoogleAccounts();
    }

    public class ExportAccounts
    {
        public string[] AccountNames { get; set; }
        public string LastUsedAccountName { get; set; } = string.Empty;
    }

    internal class GoogleAccountsHandler : IGoogleAccountsHandler       
    {        
        private const string ExportAccountsFileName = "ExportAccounts.json";

        private string GetFullExportAccountsFileNamePath => $"{OSFolderManager.Instance.GetPlainLocalUserAppDataPath()}{ExportAccountsFileName}";

        public bool Save(string[] googleAccountNames, string lastUsedAccountName)
        {
            if (!googleAccountNames?.Any() ?? false)
                return false;

            try
            {
                using (var sw = new StreamWriter(GetFullExportAccountsFileNamePath))
                {
                    var exportAccounts = new ExportAccounts() { AccountNames = googleAccountNames, LastUsedAccountName = lastUsedAccountName };
                    sw.WriteLine(JsonConvert.SerializeObject(exportAccounts));                    
                }
            }
            catch 
            {
                return false;
            }
            
            return true;
        }

        public ExportAccounts GetGoogleAccounts()
        {
            try
            {
                if (!File.Exists(GetFullExportAccountsFileNamePath))
                    return null;

                using (var sr = new StreamReader(GetFullExportAccountsFileNamePath))
                {                    
                    var content = sr.ReadToEnd();
                    if (!string.IsNullOrWhiteSpace(content))
                    {
                        return JsonConvert.DeserializeObject<ExportAccounts>(content);                        
                    }
                }
            }
            catch
            {
                return null;
            }

            return new ExportAccounts();
        }        
    }
}
