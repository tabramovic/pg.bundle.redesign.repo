using System;
using System.IO;

namespace DealMaker.Export
{
	/// <summary>
	/// Represents a object for writing export
	/// </summary>
	public interface IExportWriter
	{
		void WriteHeader(StreamWriter sw, string[] header);
		void WriteRow(StreamWriter sw, string[] row);
		string GetDefaultExtension();
	}
}
