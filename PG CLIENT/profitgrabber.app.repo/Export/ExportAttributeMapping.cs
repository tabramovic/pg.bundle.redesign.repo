using System;

namespace DealMaker.Export
{
	/// ihuk, 04.12.2004
	/// <summary>
	/// Represents mapping between PropertyItem property
	/// (attribute) and column in export
	/// </summary>
	public class ExportAttributeMapping
	{
		string attributeType = String.Empty;
		string columnName = String.Empty;
		string attributeName = String.Empty;
		public const string EmptyColumn = "Empty column";
		/// <summary>
		/// Initializes new instance of <c>ExportAttributeMapping</c> class.
		/// </summary>
		public ExportAttributeMapping()
		{
		}

		/// <summary>
		/// Initializes new instance of <c>ExportAttributeMapping</c> class.
		/// </summary>
		/// <param name="attributeName">Attribute to be maped</param>
		public ExportAttributeMapping(string attributeName)
		{
			this.attributeName = attributeName;
			this.columnName = attributeName;
		}

		/// <summary>
		/// Initializes new instance of <c>ExportAttributeMapping</c> class.
		/// </summary>
		/// <param name="attributeName">Attribute to be maped</param>
		/// <param name="columnName">Destination column name</param>
		public ExportAttributeMapping(string attributeName, string columnName)
		{
			this.attributeName = attributeName;
			this.columnName = columnName;
		}

		public ExportAttributeMapping(string attributeName, string columnName, string attributeType):
			this(attributeName, columnName)
		{
			this.attributeType = attributeType;
		}

		public override string ToString()
		{
			return attributeName;
		}

		#region Properties
		/// <summary>
		/// Gets the name of PropertyItem class attribute that will be exported.
		/// </summary>
		public string AttributeName
		{
			get
			{
				return attributeName;
			}
			set
			{
				attributeName = value;
			}
		}

		public string AttributeType
		{
			get
			{
				return attributeType;
			}
			set
			{
				attributeType = value;
			}
		}

		/// <summary>
		/// Gets or sets the column name under which attrinute
		/// is saved in export file
		/// </summary>
		public string ColumnName
		{
			get
			{
				return columnName;
			}
			set
			{
				//TODO: should we allow empty strings here?
				columnName = value;
			}
		}

		#endregion
	}
}
