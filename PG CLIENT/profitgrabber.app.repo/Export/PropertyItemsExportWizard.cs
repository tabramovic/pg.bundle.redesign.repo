using System;
using System.Resources;
using System.Windows.Forms;
using DealMaker;
using DealMaker.DALC;
using DealMaker.Classes;
using DealMaker.Forms.Wizard;
using DealMaker.Forms.Export;

namespace DealMaker.Export
{
	public class PropertyItemsExportWizard
	{
		SelectContactsPage scp;
		SelectExportProfilePage sepp;
		CreateExportProfilePage cepp;
		CreateExportFilterPage cefp;
		RenameColumnsPage rcp;
		SaveMappingProfilePage smpp;
		SelectExportLocationPage selp;
		ExportCompletedPage ecp;
		WizardForm wizardForm = new WizardForm();

		public PropertyItemsExportWizard()
		{
			System.IO.Stream  stream;
			System.Reflection.Assembly assembly;

			assembly = System.Reflection.Assembly.GetExecutingAssembly();
			stream = assembly.GetManifestResourceStream("DealMaker.res.ExportWizard.bmp");
			if (null != stream)
				wizardForm.Image = System.Drawing.Image.FromStream(stream);

			scp = new SelectContactsPage(wizardForm);
			sepp = new SelectExportProfilePage(wizardForm);
			cepp = new CreateExportProfilePage(wizardForm);
			cefp = new CreateExportFilterPage(wizardForm);
			rcp = new RenameColumnsPage(wizardForm);
			smpp = new SaveMappingProfilePage(wizardForm);
			selp = new SelectExportLocationPage(wizardForm);
			ecp = new ExportCompletedPage(wizardForm);

			wizardForm.AddPage(scp);
			wizardForm.AddPage(sepp);
			wizardForm.AddPage(cepp);
			wizardForm.AddPage(cefp);
			wizardForm.AddPage(rcp);
			wizardForm.AddPage(smpp);
			wizardForm.AddPage(selp);
			wizardForm.AddPage(ecp);

			wizardForm.Title = "Export contacts";
			wizardForm.ButtonStyles = WizardFormButtonStyles.HelpButton |
				WizardFormButtonStyles.BackButton |
				WizardFormButtonStyles.NextButton |
				WizardFormButtonStyles.CancelButton;
			wizardForm.Size = new System.Drawing.Size(584, 464);

			wizardForm.BackColor = System.Drawing.Color.FromArgb(210, 216, 240);
			wizardForm.ForeColor = System.Drawing.Color.FromArgb(47, 65, 131);
			// start by selecting contacts to export
			wizardForm.FirstPageIndex = 0;

		}

		public PropertyItemsExportWizard(PropertyItem[] items):
			this()
		{
			// start by selecting export profile
			wizardForm.FirstPageIndex = 1;
			scp.Contacts = items;
		}

		public PropertyItemsExportWizard(int groupId):
			this(GroupFactory.GetInstance().GetGroup(groupId).PropertyItems)
		{
		}

		public bool Execute()
		{
			return  DialogResult.OK == wizardForm.Execute();
		}
		#region Properties
		public WizardForm WizardForm
		{
			get
			{
				return wizardForm;
			}
		}
		#endregion
	}
}
		
