using System;
using System.IO;

namespace DealMaker.Export
{
	public class TabDelimitedExportWriter: IExportWriter
	{
		public void WriteHeader(StreamWriter sw, string[] header)
		{
			string headerString = String.Empty;
			
			for (int i = 0; i < header.Length; i++)
				headerString += header[i] + ((i == header.Length - 1) ? String.Empty : "\t");

			sw.WriteLine(headerString);
		}

		public void WriteRow(StreamWriter sw, string[] row)
		{
			string rowString = String.Empty;
			
			for (int i = 0; i < row.Length; i++)
				rowString += row[i] + ((i == row.Length - 1) ? String.Empty : "\t");

			sw.WriteLine(rowString);
		}

		public string GetDefaultExtension()
		{
			return ".txt";
		}

		public override string ToString()
		{
			return ("Tab Delimited Text file");
		}

	}
}
