using System;
using System.Drawing;
using System.Drawing.Printing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace DealMaker
{
	/// <summary>
	/// Summary description for PrinterSetupForm.
	/// </summary>
	public class PrinterSetupForm : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox tbApplicationPrinter;
		private System.Windows.Forms.Button bBrowse;
		private System.Windows.Forms.Button bClose;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public PrinterSetupForm()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.tbApplicationPrinter = new System.Windows.Forms.TextBox();
			this.bBrowse = new System.Windows.Forms.Button();
			this.bClose = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label1.Location = new System.Drawing.Point(16, 24);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(232, 23);
			this.label1.TabIndex = 0;
			this.label1.Text = "Application Printer";
			// 
			// tbApplicationPrinter
			// 
			this.tbApplicationPrinter.Location = new System.Drawing.Point(32, 64);
			this.tbApplicationPrinter.Name = "tbApplicationPrinter";
			this.tbApplicationPrinter.Size = new System.Drawing.Size(296, 20);
			this.tbApplicationPrinter.TabIndex = 1;
			this.tbApplicationPrinter.Text = "";
			// 
			// bBrowse
			// 
			this.bBrowse.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.bBrowse.Location = new System.Drawing.Point(336, 62);
			this.bBrowse.Name = "bBrowse";
			this.bBrowse.Size = new System.Drawing.Size(32, 23);
			this.bBrowse.TabIndex = 2;
			this.bBrowse.Text = "...";
			this.bBrowse.Click += new System.EventHandler(this.bBrowse_Click);
			// 
			// bClose
			// 
			this.bClose.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.bClose.Location = new System.Drawing.Point(296, 104);
			this.bClose.Name = "bClose";
			this.bClose.TabIndex = 3;
			this.bClose.Text = "Close";
			this.bClose.Click += new System.EventHandler(this.bClose_Click);
			// 
			// PrinterSetupForm
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(394, 144);
			this.Controls.Add(this.bClose);
			this.Controls.Add(this.bBrowse);
			this.Controls.Add(this.tbApplicationPrinter);
			this.Controls.Add(this.label1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
			this.Name = "PrinterSetupForm";
			this.ShowInTaskbar = false;
			this.Text = "Choose Printer Dialog";
			this.Load += new System.EventHandler(this.PrinterSetupForm_Load);
			this.ResumeLayout(false);

		}
		#endregion

		private void bClose_Click(object sender, System.EventArgs e)
		{
			this.Close();
		}

		private void bBrowse_Click(object sender, System.EventArgs e)
		{
		
		}

		private void PrinterSetupForm_Load(object sender, System.EventArgs e)
		{
			foreach (string s in PrinterSettings.InstalledPrinters)
			{
				MessageBox.Show(s);
			}
		}
	}
}
