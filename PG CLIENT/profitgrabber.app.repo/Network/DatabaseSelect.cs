using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;

namespace DealMaker.Network
{
	/// <summary>
	/// Summary description for DatabaseSelect.
	/// </summary>
	public class DatabaseSelect : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Button refreshServers;
		private System.Windows.Forms.Button save;
		private System.Windows.Forms.Button cancel;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.ListBox serverList;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox serverName;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		private PGSerializationSelectServer.DabaseQueryPacket selectedServerPacket;

		public bool restartRequired = true;
		public bool RestartRequired
		{
			set
			{
				restartRequired = value;
			}
		}

		public bool CancelEnabled
		{
			set
			{
				cancel.Enabled = value;
			}
		}


		public DatabaseSelect()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.refreshServers = new System.Windows.Forms.Button();
			this.save = new System.Windows.Forms.Button();
			this.cancel = new System.Windows.Forms.Button();
			this.serverList = new System.Windows.Forms.ListBox();
			this.label1 = new System.Windows.Forms.Label();
			this.serverName = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// refreshServers
			// 
			this.refreshServers.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.refreshServers.Location = new System.Drawing.Point(8, 160);
			this.refreshServers.Name = "refreshServers";
			this.refreshServers.Size = new System.Drawing.Size(208, 24);
			this.refreshServers.TabIndex = 0;
			this.refreshServers.Text = "Refresh Server List";
			this.refreshServers.Click += new System.EventHandler(this.refreshServers_Click);
			// 
			// save
			// 
			this.save.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.save.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.save.Location = new System.Drawing.Point(128, 248);
			this.save.Name = "save";
			this.save.Size = new System.Drawing.Size(88, 24);
			this.save.TabIndex = 2;
			this.save.Text = "Save";
			this.save.Click += new System.EventHandler(this.save_Click);
			// 
			// cancel
			// 
			this.cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.cancel.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.cancel.Location = new System.Drawing.Point(32, 248);
			this.cancel.Name = "cancel";
			this.cancel.Size = new System.Drawing.Size(88, 24);
			this.cancel.TabIndex = 3;
			this.cancel.Text = "Cancel";
			// 
			// serverList
			// 
			this.serverList.Location = new System.Drawing.Point(8, 32);
			this.serverList.Name = "serverList";
			this.serverList.Size = new System.Drawing.Size(208, 121);
			this.serverList.TabIndex = 5;
			this.serverList.SelectedIndexChanged += new System.EventHandler(this.serverList_SelectedIndexChanged);
			// 
			// label1
			// 
			this.label1.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.label1.Location = new System.Drawing.Point(8, 8);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(100, 16);
			this.label1.TabIndex = 6;
			this.label1.Text = "Available servers:";
			// 
			// serverName
			// 
			this.serverName.Location = new System.Drawing.Point(8, 216);
			this.serverName.Name = "serverName";
			this.serverName.Size = new System.Drawing.Size(208, 20);
			this.serverName.TabIndex = 7;
			this.serverName.Text = "";
			this.serverName.TextChanged += new System.EventHandler(this.serverName_TextChanged);
			// 
			// label2
			// 
			this.label2.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.label2.Location = new System.Drawing.Point(8, 192);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(192, 16);
			this.label2.TabIndex = 8;
			this.label2.Text = "Select server or enter server name:";
			// 
			// DatabaseSelect
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(210)), ((System.Byte)(216)), ((System.Byte)(240)));
			this.ClientSize = new System.Drawing.Size(224, 280);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.serverName);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.serverList);
			this.Controls.Add(this.cancel);
			this.Controls.Add(this.save);
			this.Controls.Add(this.refreshServers);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "DatabaseSelect";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Database Select";
			this.Load += new System.EventHandler(this.DatabaseSelect_Load);
			this.ResumeLayout(false);

		}
		#endregion

		private void DatabaseSelect_Load(object sender, System.EventArgs e)
		{
			//RegUtil.WriteToReg(RegUtil.REGUTIL_KEYS.HKLM, 
			//				@"Software\Turn Key\Profit Grabber Pro\Reg", 
			//				"C7", 
			//				regInfo);
			refresh_servers();

		}

		class PacketToString// : PGSerializationSelectServer.DabaseQueryPacket
		{
			public override string ToString()
			{
				return packet.Hostname.ToString()+" ["+packet.IPAddress+"]";
			}

			public PGSerializationSelectServer.DabaseQueryPacket packet;

			public PacketToString(PGSerializationSelectServer.DabaseQueryPacket packet)
			{
				this.packet = packet;
			}
		}

		void refresh_servers()
		{
			try
			{
				Cursor.Current = Cursors.WaitCursor;

				serverList.Items.Clear();
				Network.DBServerQuery.Query();
				foreach(PGSerializationSelectServer.DabaseQueryPacket packet in Network.DBServerQuery.Servers)
				{
					PacketToString pts = new PacketToString(packet);

					//				MessageBox.Show(packet.ServerName.ToString());
					serverList.Items.Add(pts);
				}

                if (serverList.Items.Count > 0)
                {
                    serverList.SelectedIndex = 0;
                }
                else
                {
                    MessageBox.Show("It looks like you are trying to find your server and it is not showing up in the server list. " + Environment.NewLine + Environment.NewLine + "Please push Cancel on the Database Select window, turn off your firewall by following your firewall software instructions, and then rerun ProfitGrabber", "ProfitGrabber Pro", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
			}
			catch(Exception ee)
			{
				MessageBox.Show("Error occured while requesting for servers!" + Environment.NewLine + ee.Message);
			}
			finally
			{
				Cursor.Current = Cursors.Default;
			}
		}

		private void refreshServers_Click(object sender, System.EventArgs e)
		{
			refresh_servers();
		}

		private void serverList_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			serverName.Text = ((PacketToString)serverList.SelectedItem).packet.Hostname;
			selectedServerPacket = ((PacketToString)serverList.SelectedItem).packet;
		}

		void show_restart_message()
		{
			if(restartRequired)
			{
				MessageBox.Show("Database settings saved, please reastart application to changes take effect!");
			}
		}

		private void save_Click(object sender, System.EventArgs e)
		{

			string serverNameString;

			Cursor.Current = Cursors.WaitCursor;

			if(selectedServerPacket != null)
			{
				serverNameString = selectedServerPacket.ServerName;
			}
			else
			{
				serverNameString = serverName.Text + @"\PG_DB2";
			}

			//TODO !!! - REMOVE THIS (REMOVES \PG2)
			//serverNameString = serverNameString.Substring(0,serverNameString.IndexOf('\\'));

			if(Util.TestConnection(serverNameString
#if DEBUG
				, null
#endif
				))
			{
				RegUtil.WriteToReg(RegUtil.REGUTIL_KEYS.HKLM, 
								Globals.RegEntry_Database, 
								"Server", 
								serverNameString);

				RegUtil.WriteToReg(RegUtil.REGUTIL_KEYS.HKLM, 
					Globals.RegEntry_Database, 
					"ServerIP", 
					selectedServerPacket != null ? selectedServerPacket.IPAddress : serverName.Text);

				Cursor.Current = Cursors.Default;
				show_restart_message();
				return;
			}
			else
			{
				if(selectedServerPacket != null)
				{
					serverNameString = serverName.Text + @"\PG_DB2";
					int where = selectedServerPacket.ServerName.IndexOf('\\');

					serverNameString = selectedServerPacket.IPAddress;

					if(where >= 0)
					{
						serverNameString = serverNameString + selectedServerPacket.ServerName.Substring(where);
					}

					//REMOVE THIS (REMOVES \PG2)
					//serverNameString = selectedServerPacket.IPAddress;

					if(Util.TestConnection(serverNameString
#if DEBUG						
						, null
#endif
						))
					{
						RegUtil.WriteToReg(RegUtil.REGUTIL_KEYS.HKLM, 
							Globals.RegEntry_Database, 
							"Server", 
							serverNameString);

						RegUtil.WriteToReg(RegUtil.REGUTIL_KEYS.HKLM, 
							Globals.RegEntry_Database, 
							"ServerIP", 
							selectedServerPacket.IPAddress);

						Cursor.Current = Cursors.Default;
						show_restart_message();
						return;
					}
				}

				Cursor.Current = Cursors.Default;
				MessageBox.Show("Server connect failed, please make sure that you entered good server name and that the server is running!");
				this.DialogResult = DialogResult.None;
			}
		}

		private void serverName_TextChanged(object sender, System.EventArgs e)
		{
			selectedServerPacket = null;
		}
	}
}
