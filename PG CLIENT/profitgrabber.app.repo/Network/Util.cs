using System;
using System.Windows.Forms;
using System.Collections.Specialized;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

//ADDED FOR DEBUG
using System.IO;

namespace DealMaker.Network
{
	/// <summary>
	/// Summary description for Util.
	/// </summary>
	public class Util
	{
		public Util()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		public static bool SetupDatabase()
		{
#if DEBUG
            
            StreamWriter sw = new StreamWriter(Application.StartupPath + @"\TestConn.txt", false);
            
#endif

			try
			{
#if DEBUG				
				sw.WriteLine("Entered Setup Database");
#endif

				//first check registry
				Globals.DatabaseServer = RegUtil.ReadFromRegistry(RegUtil.REGUTIL_KEYS.HKLM, 
					Globals.RegEntry_Database, 
					"Server", 
					"");
#if DEBUG
				sw.WriteLine($"Globals.DatabaseServer: {(string.IsNullOrEmpty(Globals.DatabaseServer) ? Globals.DatabaseServer : "no_value_found")}");
#endif

				Globals.DatabaseServerIP = RegUtil.ReadFromRegistry(RegUtil.REGUTIL_KEYS.HKLM, 
					Globals.RegEntry_Database, 
					"ServerIP", 
					"");
#if DEBUG
				sw.WriteLine($"Globals.DatabaseServerIP: {(string.IsNullOrEmpty(Globals.DatabaseServerIP) ? Globals.DatabaseServerIP : "no_value_found")}");
				sw.Flush();
#endif
				if (!string.IsNullOrEmpty(Globals.DatabaseServer))
				{
					if (TestConnection(Globals.DatabaseServer
#if DEBUG
					, sw
#endif
					))
					{
#if DEBUG
						sw.WriteLine("TestConnn successfull");
						sw.Flush();
#endif
						return true;
					}				
#if DEBUG				
				sw.WriteLine("TestConnection(Globals.DatabaseServer) returns " + TestConnection(Globals.DatabaseServer, sw).ToString());
				sw.Flush();
#endif
				}
			}
			catch 
			{
				//EMPTY CATCH 
			}


			//then app config
			try
			{
#if DEBUG
				sw.WriteLine("Reading from APP CONFIG");				
#endif

				NameValueCollection nvc = (NameValueCollection)ConfigurationSettings.GetConfig("ProfitGrabberDBSupport");
				if (null != nvc && nvc.Count != 0)
				{
					Globals.DatabaseServer = (string)nvc["DBServerInstance"];						
#if DEBUG
					sw.WriteLine("Globals.DatabaseServer: " + Globals.DatabaseServer);
					sw.Flush();
#endif

				}
				
				//29.01.06, sasa & ta: added attachDatabase... on first pg run, database needs to be attached, check attachDatabase comment
				if(TestConnection(Globals.DatabaseServer
#if DEBUG
					, sw
#endif
					))
				{
#if DEBUG
					sw.WriteLine("TestConn successful");
					sw.Flush();
#endif

					return true;
				}
				else
				{
#if DEBUG
					sw.WriteLine("TestConn NOT successful: TestConnection(" + Globals.DatabaseServer + ") returns " + TestConnection(Globals.DatabaseServer, sw));
					sw.WriteLine("Attaching database");
					sw.Flush();
#endif

					AttachDatabase();
					if(TestConnection(Globals.DatabaseServer
#if DEBUG
						, sw
#endif				
						))
						return true;
				}
			}
			catch
#if DEBUG
				(Exception ee)
#endif
			{
#if DEBUG
				MessageBox.Show(ee.ToString());
#endif
			}

			//if plain ver exit
			if(!Globals.NwVerEnabled) System.Environment.Exit(0);

			//if all fails popup database select window and call ourselfs to load data from reg :)

			DatabaseSelect databaseSelectForm = new DatabaseSelect();
			databaseSelectForm.RestartRequired = false;
			//databaseSelectForm.CancelEnabled = false;
			
			databaseSelectForm.ShowDialog();

			if(databaseSelectForm.DialogResult == DialogResult.OK)
			{
				SetupDatabase();
				return true;
			}
			else
			{
				System.Environment.Exit(0);
				return false;
			}
		}

		/// <summary>
		///	Attach Database action:
		///	.mdf && .ldf are located at the same directory as .exe itself
		///	We will manually evoke attach database action if installer didn't succeed
		/// Syntax:
		///	osql -E -S 127.0.0.1\PG_DB2 - Q "exec sp_attach_db @dbName = N'dbDealMaker', 
		///		@filename1 = N'<FULLPATH>\dbDealMaker_Data.MDF', 
		///		@filename2 = N'<FULLPATH>\dbDealMaker_Log.LDF'"
		/// </summary>
		/// <returns></returns>
		private static void AttachDatabase()
		{
			string osqlArgs = string.Empty;
			osqlArgs += @"-E -S (local)\PG_DB2 -Q ""exec sp_attach_db @dbName = N'dbDealMaker', "; 
			//osqlArgs += "\n";
			osqlArgs += @"@filename1 = N'" + Application.StartupPath + @"\dbDealMaker_Data.MDF', ";
			//osqlArgs += "\n";
			osqlArgs += @"@filename2 = N'" + Application.StartupPath + @"\dbDealMaker_Log.LDF'"" ";
			//osqlArgs += "\n";
			//osqlArgs += "\npause";
			//MessageBox.Show(osqlArgs);
			try
			{
                //MessageBox.Show(osqlArgs);
				System.Diagnostics.Process.Start("osql.exe", osqlArgs);
				//sasa 21.03.06, changed to 5 secs / we have only one try
				System.Threading.Thread.Sleep(5000);                            
			}
			catch (Exception e)
			{
				MessageBox.Show(e.Message, "\n" + e.InnerException);
			}
		}

		public static bool TestConnection(string server
#if DEBUG
			, StreamWriter sw
#endif
			)
		{

			bool connection_ok = true;
			SqlConnection connection = new SqlConnection();
			//connection.ConnectionString = Globals.GetConnectionString();
			connection.ConnectionString = Network.Util.GetConnectionStringFromServerName(server);
#if DEBUG
            //MessageBox.Show("Connection string: " + connection.ConnectionString);
            if (null != sw)
            {
                sw.WriteLine(server);
                sw.WriteLine("Connection string: " + connection.ConnectionString);
            }
#endif			

			try
			{
				connection.Open();
			}
			catch 
#if DEBUG
				(Exception exc)
#endif
			{
#if DEBUG
                MessageBox.Show(exc.Message + Environment.NewLine + exc.InnerException);
                if (null != sw)
					sw.WriteLine(exc.Message + Environment.NewLine + exc.InnerException);
#endif

				connection_ok = false;
			}
			finally
			{
#if DEBUG
				if (null != sw)
					sw.Flush();
#endif
				connection.Close();
			}

			return connection_ok;
		}

		public static string GetConnectionStringFromServerName(string server)
		{            
            if (Globals.NwVerEnabled)
			{
				//nw
				return (String.Format("user id=sa;password={2};persist security info=False;data source={0};initial catalog={1}", server, Globals.DatabaseName, Globals.DatabasePwd));
			}
			else
			{
				//plain
				return (String.Format("data source={0};initial catalog={1};Integrated Security=SSPI;", server, Globals.DatabaseName));
			}
		}
	}
}
