#define IGNORE_LOCK

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters;
using System.Runtime.Serialization.Formatters.Binary;
//using System.Runtime.Serialization.Formatters.Soap;
using PGSerializationSelectServer;
using PGNetLock;
//using System.ComponentModel;
using System.Windows.Forms;

namespace DealMaker.Network
{
	/// <summary>
	/// Summary description for NetworkLock.
	/// </summary>
	public class NetworkLock
	{
		public enum LockStatus
		{
			Pending,
			Locked,
			ReadOnly
		}

		public static LockStatus GetStatus()
		{
			if(LockedByOther) return LockStatus.ReadOnly;
			if(Locked) return LockStatus.Locked;

			return LockStatus.Pending;
		}

		public delegate void StatusChangedEventHandler();
		public static event StatusChangedEventHandler StatusChanged;

		static Mutex mutex = new Mutex();

		//if both false, query pending
		public static bool LockedByOther;
		public static bool Locked;
		
		public static Guid ContactIDToLock;

		static Thread listener;// = new Thread(new ThreadStart(listenerThread));
		static Thread query; //= new Thread(new ThreadStart(queryThread));

		static UdpClient udpClientReceive;// = new UdpClient(UDPPortReceive);
		static UdpClient udpClientSend;// = new UdpClient();

		public static void Start()
		{
			if(Globals.NwVerEnabled)
			{
				//			listener.Start();

				if(listener == null)
				{
					//				NetworkLock.Stop();
					udpClientReceive = new UdpClient(UDPPortReceive);
					udpClientSend = new UdpClient();
					listener = new Thread(new ThreadStart(listenerThread));
					listener.Start();
				}
			}
		}

		public static void Stop()
		{	
			if(Globals.NwVerEnabled)
			{
				if(listener != null)
				{
					RequestUnlock();
					udpClientReceive.Close();
					listener.Abort();
					listener = null;
				}
			}
		}

		public static void RequestLock(Guid id_contact)
		{
			if(Globals.NwVerEnabled)
			{
				//			try
				//			{
				mutex.WaitOne();

				NetworkLock.Start();
				//				if(query.ThreadState == System.Threading.ThreadState. ThreadState.
				//query.Abort();
				//wait until thread terminates to prevent sending lock request after unlock
				//try{query.Join();}
				//	catch{}

				if(query != null)
				{
					query.Abort();
					query.Join();
					query = null;
				}


				ContactIDToLock = id_contact;
				LockedByOther = false;
				Locked = false;

				mutex.ReleaseMutex();

				query = new Thread(new ThreadStart(queryThread));
				query.Start();
				//			}
				//			catch(Exception ee)
				//			{
				//				MessageBox.Show(ee.ToString());
				//			}

				update_gui();
			}
		}

		public static void RequestUnlock()
		{
			if(Globals.NwVerEnabled)
			{
				//maybe add check if guid empty?
				mutex.WaitOne();

				ContactIDToLock = Guid.Empty;
				LockedByOther = false;
				Locked = false;

				mutex.ReleaseMutex();

				if(query != null)
				{
					query.Abort();
					query.Join();
					query = null;
				}

				PGNetLock.PGNetLock packet = new PGNetLock.PGNetLock();

				packet.id_contact = ContactIDToLock;
				packet.id_app = Globals.ApplicationID;
				packet.request_lock = false;

				sendPacket(packet);

				update_gui();
			}
		}

//		const int LockValidTime = 30;

		const int UDPPortReceive = 11027;
		const int UDPPortSend = 11026;
//		const int UDPPortSend = 11006;

		const int QueryResendTime = 10;

//		static ArrayList lockedList = new ArrayList();

		static public void sendPacket(PGNetLock.PGNetLock packet)
		{
			MemoryStream memoryStream = new MemoryStream();

			IFormatter formatter = new BinaryFormatter();

			((BinaryFormatter)formatter).AssemblyFormat = FormatterAssemblyStyle.Simple;

			formatter.Serialize(memoryStream, packet);

			byte[] sendArray = memoryStream.ToArray();

			// if parse fails, try dns, if this fails, broadcast... possibile problems, but 
			// benefits are larger
			System.Net.IPAddress serverAdress = System.Net.IPAddress.Broadcast;

			//MessageBox.Show(Globals.DatabaseServerIP.ToString());
			try
			{
				serverAdress = System.Net.IPAddress.Parse(Globals.DatabaseServerIP);
			}
			catch
			{
				try
				{
					serverAdress = System.Net.Dns.Resolve(Globals.DatabaseServerIP).AddressList[0];
				}
				catch
				{
					//nothing, we already have broadcast
				}
			}

			//MessageBox.Show(serverAdress.ToString());
			///#$@#$
			//serverAdress = System.Net.IPAddress.Broadcast;

			System.Net.IPEndPoint endPoint = new IPEndPoint( serverAdress, UDPPortSend);

			try
			{			
				udpClientSend.Send(sendArray, sendArray.Length, endPoint);
			}
			catch								
#if DEBUG
				(
				Exception ee
				)
#endif				
			{
#if DEBUG
				MessageBox.Show(ee.ToString());			
#endif
			}
		}

		static public void queryThread()
		{
            try
            {

                try
                {
                    while (true)
                    {
                        //				Console.WriteLine("beat!\n");
                        if (ContactIDToLock != Guid.Empty)
                        {
                            PGNetLock.PGNetLock packet = new PGNetLock.PGNetLock();

                            packet.id_contact = ContactIDToLock;
                            packet.id_app = Globals.ApplicationID;
                            packet.request_lock = true;

                            sendPacket(packet);
                        }

                        Thread.Sleep(QueryResendTime * 1000);
                    }
                }
                catch (Exception e)
                {
                    //handle abort
                }
            }
            catch (ThreadAbortException ex)
            {
                Console.WriteLine("ResetAbort");
                Thread.ResetAbort();
            }
		}

		
		public static Network.TestNW2 f;

		static void refresh()
		{
			f.status.Text = "locked "+Locked.ToString() + " by other " + LockedByOther.ToString();
			f.requested.Text = ContactIDToLock.ToString();
		}

		private delegate void UpdateDelegate();

		static void update_gui()
		{
			//update gui, must ivnoke
			//napravi subscribeanje na evente

//			f.BeginInvoke(new UpdateDelegate(refresh));
#if IGNORE_LOCK
            return;
#endif

			if (StatusChanged != null)
				StatusChanged();

		}

		static public void listenerThread()
		{
            try
            {
                try
                {
                    IPEndPoint RemoteIpEndPoint = new IPEndPoint(IPAddress.Any, 0);

                    //if any packet recieved on port broadcast our connection string
                    while (true)
                    {
                        Byte[] received =
                            udpClientReceive.Receive(ref RemoteIpEndPoint);
                        //				udpClientReceive.re
                        //				Console.WriteLine("This message was sent from " +
                        //					RemoteIpEndPoint.Address.ToString() +
                        //					" on their port number " +
                        //					RemoteIpEndPoint.Port.ToString());

                        MemoryStream memoryStream = new MemoryStream(received);

                        IFormatter formatter = new BinaryFormatter();

                        PGNetLock.PGNetLock receivedPacket = (PGNetLock.PGNetLock)formatter.Deserialize(memoryStream);

                        memoryStream.Close();

                        mutex.WaitOne();
                        if (ContactIDToLock != Guid.Empty && receivedPacket.id_contact == ContactIDToLock)
                        {
                            if (receivedPacket.id_app_holding_lock == Globals.ApplicationID)
                            {
                                Locked = true;
                                LockedByOther = false;
                            }
                            else
                            {
                                Locked = false;
                                LockedByOther = true;
                            }

                            update_gui();
                        }
                        mutex.ReleaseMutex();
                    }
                }
                catch
                {
                    //silent for now, when controls closing sometimes leaves thread
                }
            }
            catch (ThreadAbortException ex)
            {
                Console.WriteLine("ResetAbort");
                Thread.ResetAbort();
            }
        }

		public NetworkLock()
		{
			
			//
			// TODO: Add constructor logic here
			//
		}
	}
}
