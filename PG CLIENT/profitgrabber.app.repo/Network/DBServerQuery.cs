using System;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters;
using System.Runtime.Serialization.Formatters.Binary;
using System.Net;
using System.Net.Sockets;
using System.Collections;
using System.Threading;
//using System.Runtime.Serialization.Formatters.Soap;
using PGSerializationSelectServer;

namespace DealMaker.Network
{
	/// <summary>
	/// Summary description for DBServerQuery.
	/// </summary>
	public class DBServerQuery
	{
/*		[Serializable]
		public class DabaseQueryPacket
		{
			public DateTime Time;
			public Guid Sender;
			public string Hostname;
			public string ServerName;
			public string ConnectionString;
			public Guid PacketID;
		}
*/
		public static ArrayList Servers;
		static ArrayList tmpServers;
		public const int sleepTime = 3000;

		static Guid Sender = Guid.NewGuid();

		const int UDPPortSend = 11006;
		const int UDPPortReceive = 11007;
//		const int UDPPortSend = 43206;
//		const int UDPPortReceive = 43207;

		static UdpClient udpClientReceive;

		public DBServerQuery()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		static void listenerThread()
		{
			IPEndPoint RemoteIpEndPoint = new IPEndPoint(IPAddress.Any, 0);

			udpClientReceive = new UdpClient(UDPPortReceive);

			sendQueryPacket();

			while(true)
			{
				Byte[] received = udpClientReceive.Receive(ref RemoteIpEndPoint);
			
				MemoryStream memoryStream = new MemoryStream(received);

				IFormatter formatter = new BinaryFormatter();
				//IFormatter formatter = new SoapFormatter();

				((BinaryFormatter)formatter).AssemblyFormat = FormatterAssemblyStyle.Simple;
				DabaseQueryPacket receivedPacket = (DabaseQueryPacket)formatter.Deserialize(memoryStream);

				memoryStream.Close();

				receivedPacket.IPAddress = RemoteIpEndPoint.Address.ToString();

				tmpServers.Add(receivedPacket);
			}
		}

		static void sendQueryPacket()
		{
			UdpClient udpClient = new UdpClient();
			try
			{
				byte[] sendArray = {0};

				System.Net.IPEndPoint endPoint = new IPEndPoint(IPAddress.Broadcast, UDPPortSend);

				udpClient.Send(sendArray, sendArray.Length, endPoint);
			}
			catch
			{
				System.Windows.Forms.MessageBox.Show("Unable to send broadcast server query, you must enter server name or ip adddress manually!");
			}
			finally
			{
				udpClient.Close();
			}
		}

		static public ArrayList Query()
		{
			tmpServers = new ArrayList();
			
			Thread listener = new Thread(new ThreadStart(listenerThread));

			listener.Start();

			Thread.Sleep(sleepTime);

			listener.Abort();

			try
			{
				udpClientReceive.Close();
			}
			catch
			{}

			Servers = new ArrayList();

			bool exists = false;
			foreach(DabaseQueryPacket tmppacket in tmpServers)
			{
				foreach(DabaseQueryPacket packet in Servers)
				{
					if(packet.PacketID == tmppacket.PacketID)
					{
						exists = true;
						continue;
					}
				}	
				if(!exists) Servers.Add(tmppacket);
			}

			return Servers;
		}
	}
}
