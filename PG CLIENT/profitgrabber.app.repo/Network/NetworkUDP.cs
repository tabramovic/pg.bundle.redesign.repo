using System;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters;
using System.Runtime.Serialization.Formatters.Binary;
using System.Net;
using System.Net.Sockets;
using System.Collections;

namespace DealMaker.Network
{
	//	public delegate void NetworkChangedEventHandler(object sender, EventArgs e);
	public delegate void NetworkChangedEventHandler(Network.NetworkUDP.Packet received);

	/// <summary>
	/// Summary description for NetworkUDP.
	/// </summary>
	public class NetworkUDP
	{
		const int Port = 11005;
		const int RecievedIDCache = 100;

		static Guid Sender = Guid.NewGuid();

		static Guid[] RecievedID = new Guid [RecievedIDCache];
		static int RecievedIDCnt = 0;

		public static event NetworkChangedEventHandler ChangedPropertyItem;
		public static event NetworkChangedEventHandler ChangedNode;
		public static event NetworkChangedEventHandler ChangedTask;

		//DO NOT TOUCH IF OLD VERSIONS ON NETWORK
		[Serializable]
		public class Packet
		{
			public DateTime Time;
			public Guid PropertyItemID;
			public int NodeID;
			public Guid TaskID;
			public Guid Sender;
			public Guid PacketID;
			public Guid	GRFU1;
			public Guid	GRFU2;
			public string SRFU1;
			public string SRFU2;
			public int IRFU;
		}

		public NetworkUDP()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		public static UdpClient udpClient;

		public static void ThreadListener()
		{
			if(Globals.NwVerEnabled)
			{
				udpClient = new UdpClient(Port);
				IPEndPoint RemoteIpEndPoint = new IPEndPoint(IPAddress.Any, 0);

			break_out:
				while(true)
				{
					Byte[] received = udpClient.Receive(ref RemoteIpEndPoint);

					MemoryStream memoryStream = new MemoryStream(received);

					IFormatter formatter = new BinaryFormatter();

					((BinaryFormatter)formatter).AssemblyFormat = FormatterAssemblyStyle.Simple;

					Packet receivedPacket = (Packet)formatter.Deserialize(memoryStream);

					memoryStream.Close();

					if(receivedPacket.Sender == Sender) continue;

					foreach(Guid g in RecievedID)
					{
						if(g == receivedPacket.PacketID) goto break_out;
					}

					RecievedIDCnt = (RecievedIDCnt+1)%RecievedIDCache;
					RecievedID[RecievedIDCnt] = receivedPacket.PacketID;

					if(receivedPacket.NodeID > 0)
					{
						//dobio node id

						if (ChangedNode != null)
							//						ChangedPropertyItem(null, EventArgs.Empty);
							ChangedNode(receivedPacket);

						continue;
					}

					if(receivedPacket.PropertyItemID != Guid.Empty)
					{
						//dobio property item

						if (ChangedPropertyItem != null)
							//						ChangedNode(null, EventArgs.Empty);
							ChangedPropertyItem(receivedPacket);

						continue;
					}

					if(receivedPacket.TaskID != Guid.Empty)
					{
						//dobio property item

						if (ChangedTask != null)
							ChangedTask(receivedPacket);

						continue;
					}
				}
			}
		}

		public static void Broadcast(PropertyItem pi)
		{
			if(Globals.NwVerEnabled)
			{
				Packet send = new Packet();

				send.PropertyItemID = pi.IdPropertyItem;
				send.NodeID = 0;
				send.TaskID = Guid.Empty;

				BroadcastObject(send);
			}
		}

		public static void Broadcast(Node node)
		{
			if(Globals.NwVerEnabled)
			{
				Packet send = new Packet();

				send.NodeID = node.nodeId;
				send.PropertyItemID = Guid.Empty;
				send.TaskID = Guid.Empty;

				BroadcastObject(send);
			}
		}

		public static void Broadcast(Task task)
		{
			if(Globals.NwVerEnabled)
			{
				Packet send = new Packet();

				send.TaskID = task.IdTask;
				send.PropertyItemID = Guid.Empty;
				send.NodeID = 0;

				BroadcastObject(send);
			}
		}

		public static void BroadcastObject(Packet packet)
		{
			if(Globals.NwVerEnabled)
			{
				packet.Sender = Sender;
				packet.Time = DateTime.Now;
				packet.PacketID = Guid.NewGuid();

				UdpClient udpClient = new UdpClient();
			
				MemoryStream memoryStream = new MemoryStream();

				IFormatter formatter = new BinaryFormatter();

				((BinaryFormatter)formatter).AssemblyFormat = FormatterAssemblyStyle.Simple;

				formatter.Serialize(memoryStream, packet);

				byte[] sendArray = memoryStream.ToArray();

				System.Net.IPEndPoint endPoint = new IPEndPoint(IPAddress.Broadcast, NetworkUDP.Port);

				udpClient.Send(sendArray, sendArray.Length, endPoint);

				memoryStream.Close();
			}		
		}
	}
}
