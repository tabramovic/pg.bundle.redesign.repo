using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace DealMaker.Network
{
	/// <summary>
	/// Summary description for TestNW2.
	/// </summary>
	public class TestNW2 : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.TextBox textBox1;
		private System.Windows.Forms.Button button2;
		private System.Windows.Forms.Button button3;
		private System.Windows.Forms.Button button4;
		private System.Windows.Forms.Button button5;
		public System.Windows.Forms.Label requested;
		public System.Windows.Forms.Label status;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public TestNW2()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.button1 = new System.Windows.Forms.Button();
			this.textBox1 = new System.Windows.Forms.TextBox();
			this.button2 = new System.Windows.Forms.Button();
			this.button3 = new System.Windows.Forms.Button();
			this.button4 = new System.Windows.Forms.Button();
			this.button5 = new System.Windows.Forms.Button();
			this.requested = new System.Windows.Forms.Label();
			this.status = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(16, 8);
			this.button1.Name = "button1";
			this.button1.TabIndex = 0;
			this.button1.Text = "start";
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// textBox1
			// 
			this.textBox1.Location = new System.Drawing.Point(16, 136);
			this.textBox1.Name = "textBox1";
			this.textBox1.Size = new System.Drawing.Size(416, 20);
			this.textBox1.TabIndex = 1;
			this.textBox1.Text = "";
			// 
			// button2
			// 
			this.button2.Location = new System.Drawing.Point(16, 40);
			this.button2.Name = "button2";
			this.button2.TabIndex = 2;
			this.button2.Text = "stop";
			this.button2.Click += new System.EventHandler(this.button2_Click);
			// 
			// button3
			// 
			this.button3.Location = new System.Drawing.Point(136, 8);
			this.button3.Name = "button3";
			this.button3.Size = new System.Drawing.Size(75, 24);
			this.button3.TabIndex = 3;
			this.button3.Text = "request lock";
			this.button3.Click += new System.EventHandler(this.button3_Click);
			// 
			// button4
			// 
			this.button4.Location = new System.Drawing.Point(136, 40);
			this.button4.Name = "button4";
			this.button4.Size = new System.Drawing.Size(75, 24);
			this.button4.TabIndex = 4;
			this.button4.Text = "unlock";
			this.button4.Click += new System.EventHandler(this.button4_Click);
			// 
			// button5
			// 
			this.button5.Location = new System.Drawing.Point(16, 96);
			this.button5.Name = "button5";
			this.button5.Size = new System.Drawing.Size(104, 24);
			this.button5.TabIndex = 5;
			this.button5.Text = "gen guid";
			this.button5.Click += new System.EventHandler(this.button5_Click);
			// 
			// requested
			// 
			this.requested.Location = new System.Drawing.Point(272, 16);
			this.requested.Name = "requested";
			this.requested.Size = new System.Drawing.Size(144, 23);
			this.requested.TabIndex = 6;
			// 
			// status
			// 
			this.status.Location = new System.Drawing.Point(272, 56);
			this.status.Name = "status";
			this.status.Size = new System.Drawing.Size(144, 23);
			this.status.TabIndex = 7;
			// 
			// TestNW2
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(472, 181);
			this.Controls.Add(this.status);
			this.Controls.Add(this.requested);
			this.Controls.Add(this.button5);
			this.Controls.Add(this.button4);
			this.Controls.Add(this.button3);
			this.Controls.Add(this.button2);
			this.Controls.Add(this.textBox1);
			this.Controls.Add(this.button1);
			this.Name = "TestNW2";
			this.Text = "TestNW2";
			this.Load += new System.EventHandler(this.TestNW2_Load);
			this.ResumeLayout(false);

		}
		#endregion

		void refresh()
		{
			this.status.Text = "locked "+Network.NetworkLock.Locked.ToString() + " by other " + Network.NetworkLock.LockedByOther.ToString();
			this.requested.Text = Network.NetworkLock.ContactIDToLock.ToString();
		}

		private void TestNW2_Load(object sender, System.EventArgs e)
		{
		
		}

		private void button3_Click(object sender, System.EventArgs e)
		{
			Network.NetworkLock.RequestLock( new Guid(textBox1.Text) );
		}

		private void button5_Click(object sender, System.EventArgs e)
		{
			textBox1.Text = Guid.NewGuid().ToString();
		}

		private void button4_Click(object sender, System.EventArgs e)
		{
			Network.NetworkLock.RequestUnlock();
		}

		private void button1_Click(object sender, System.EventArgs e)
		{
			Network.NetworkLock.f = this;
			Network.NetworkLock.StatusChanged +=new DealMaker.Network.NetworkLock.StatusChangedEventHandler(NetworkLock_StatusChanged);
			Network.NetworkLock.Start();
		}

		private void button2_Click(object sender, System.EventArgs e)
		{
			Network.NetworkLock.StatusChanged -=new DealMaker.Network.NetworkLock.StatusChangedEventHandler(NetworkLock_StatusChanged);
			Network.NetworkLock.Stop();
		}

		private delegate void UpdateDelegate();
		private void NetworkLock_StatusChanged()
		{
			this.BeginInvoke(new UpdateDelegate(refresh));
		}
	}
}
