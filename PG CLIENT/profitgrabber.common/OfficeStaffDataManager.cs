﻿using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace ProfitGrabber.Common
{
    public sealed class OfficeStaffDataManager
    {
        static readonly OfficeStaffDataManager _instance = new OfficeStaffDataManager();

        static OfficeStaffDataManager()
        { }

        OfficeStaffDataManager()
        { }

        public static OfficeStaffDataManager Instance
        {
            get { return _instance; }
        }

        bool _existingDBLayer = false;
        SqlConnection _connection;

        public void TestCreateDBLayer(SqlConnection conn)
        {
            if (null == _connection)
            {
                _connection = conn;
                _existingDBLayer = TestIfExistsDBLayer();

                if (!_existingDBLayer)
                    CreateDBLayer();
            }
        }

        bool TestIfExistsDBLayer()
        {
            bool retVal = false;
            SqlDataReader reader = null;
            string cmdText = "SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'OfficeStaff'";

            try
            {
                SqlCommand testForLayer = new SqlCommand(cmdText, _connection);
                reader = testForLayer.ExecuteReader();
                while (reader.Read())
                {
                    retVal = true;
                    break;
                }
            }
            catch
            {
                retVal = false;
            }
            finally
            {
                reader.Close();
            }
            return retVal;
        }

        void CreateDBLayer()
        {
            CreateTable();
            CreateInsertSPL();
            CreateGetSPL();
            CreateDeleteSPL();
        }

        bool CreateTable()
        {
            bool retVal = false;
            string cmdText = "CREATE TABLE OfficeStaff (OfficeStaffId uniqueidentifier PRIMARY KEY, OfficeStaffName nvarchar(128))";

            try
            {
                SqlCommand createLayer = new SqlCommand(cmdText, _connection);
                createLayer.ExecuteNonQuery();
                retVal = true;
            }
            catch
            {
                retVal = false;
            }
            return retVal;
        }

        void CreateInsertSPL()
        {
            try
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("CREATE PROCEDURE Insert_OfficeStaff ");
                sb.Append("( ");
                sb.Append("@officeStaffId uniqueidentifier, ");
                sb.Append("@officeStaffName nvarchar(128) ");                
                sb.Append(") ");
                sb.Append("AS ");
                sb.Append("INSERT INTO OfficeStaff( OfficeStaffId , OfficeStaffName ) VALUES ( @officeStaffId , @officeStaffName ) ");                

                string splText = sb.ToString();
                SqlCommand createSPL = new SqlCommand(splText, _connection);
                createSPL.ExecuteNonQuery();

            }
            catch (Exception exc)
            {
                System.Windows.Forms.MessageBox.Show("Can't create INSERT procedure for Office Staff list" + System.Environment.NewLine + exc.Message);
            }
        }

        void CreateGetSPL()
        {
            try
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("CREATE PROCEDURE GetOfficeStaffList ");                
                sb.Append("AS ");
                sb.Append("BEGIN ");
                sb.Append("SET NOCOUNT ON; ");
                sb.Append("SELECT OfficeStaffId, OfficeStaffName from OfficeStaff ");
                sb.Append("END ");

                string splText = sb.ToString();
                SqlCommand createSPL = new SqlCommand(splText, _connection);
                createSPL.ExecuteNonQuery();

            }
            catch (Exception exc)
            {
                System.Windows.Forms.MessageBox.Show("Can't create Get procedure for Office Staff list" + System.Environment.NewLine + exc.Message);
            }
        }




        void CreateDeleteSPL()
        {
            try
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("CREATE PROCEDURE Delete_OfficeStaff ");
                sb.Append("( ");
                sb.Append("@officeStaffId uniqueidentifier");                
                sb.Append(") ");
                sb.Append("AS ");
                sb.Append("DELETE FROM OfficeStaff WHERE OfficeStaffId = @officeStaffId ");

                string splText = sb.ToString();
                SqlCommand createSPL = new SqlCommand(splText, _connection);
                createSPL.ExecuteNonQuery();

            }
            catch (Exception exc)
            {
                System.Windows.Forms.MessageBox.Show("Can't create DELETE procedure for Office Staff list" + System.Environment.NewLine + exc.Message);
            }
        }
    }
}
