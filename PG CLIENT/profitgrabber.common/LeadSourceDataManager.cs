﻿using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace ProfitGrabber.Common
{
    public sealed class LeadSourceDataManager
    {
        static readonly LeadSourceDataManager _instance = new LeadSourceDataManager();

        static LeadSourceDataManager()
        { }

        LeadSourceDataManager()
        { }

        public static LeadSourceDataManager Instance
        {
            get { return _instance; }
        }

        bool _existingDBLayer = false;
        SqlConnection _connection;

        public void TestCreateDBLayer(SqlConnection conn)
        {
            if (null == _connection)
            {
                _connection = conn;
                _existingDBLayer = TestIfExistsDBLayer();

                if (!_existingDBLayer)
                    CreateDBLayer();
            }
        }

        bool TestIfExistsDBLayer()
        {
            bool retVal = false;
            SqlDataReader reader = null;
            string cmdText = "SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'LeadSource'";

            try
            {
                SqlCommand testForLayer = new SqlCommand(cmdText, _connection);
                reader = testForLayer.ExecuteReader();
                while (reader.Read())
                {
                    retVal = true;
                    break;
                }
            }
            catch
            {
                retVal = false;
            }
            finally
            {
                if (null != reader)
                    reader.Close();
            }
            return retVal;
        }

        void CreateDBLayer()
        {
            CreateTable();
            CreateInsertSPL();
            CreateGetSPL();
            CreateDeleteSPL();
        }

        bool CreateTable()
        {
            bool retVal = false;
            string cmdText = "CREATE TABLE LeadSource (LeadSourceId uniqueidentifier PRIMARY KEY, LeadSourceName nvarchar(128))";

            try
            {
                SqlCommand createLayer = new SqlCommand(cmdText, _connection);
                createLayer.ExecuteNonQuery();
                retVal = true;
            }
            catch
            {
                retVal = false;
            }
            return retVal;
        }

        void CreateInsertSPL()
        {
            try
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("CREATE PROCEDURE Insert_LeadSource ");
                sb.Append("( ");
                sb.Append("@leadSourceId uniqueidentifier, ");
                sb.Append("@leadSourceName nvarchar(128) ");                
                sb.Append(") ");
                sb.Append("AS ");
                sb.Append("INSERT INTO LeadSource( LeadSourceId , LeadSourceName ) VALUES ( @leadSourceId , @leadSourceName ) ");                

                string splText = sb.ToString();
                SqlCommand createSPL = new SqlCommand(splText, _connection);
                createSPL.ExecuteNonQuery();

            }
            catch (Exception exc)
            {
                System.Windows.Forms.MessageBox.Show("Can't create INSERT procedure for Lead Source list" + System.Environment.NewLine + exc.Message);
            }
        }

        void CreateGetSPL()
        {
            try
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("CREATE PROCEDURE GetLeadSourceList ");
                sb.Append("AS ");
                sb.Append("BEGIN ");
                sb.Append("SET NOCOUNT ON; ");
                sb.Append("SELECT LeadSourceId, LeadSourceName from LeadSource ");
                sb.Append("END ");

                string splText = sb.ToString();
                SqlCommand createSPL = new SqlCommand(splText, _connection);
                createSPL.ExecuteNonQuery();

            }
            catch (Exception exc)
            {
                System.Windows.Forms.MessageBox.Show("Can't create Get procedure for Office Staff list" + System.Environment.NewLine + exc.Message);
            }
        }



        void CreateDeleteSPL()
        {
            try
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("CREATE PROCEDURE Delete_LeadSource ");
                sb.Append("( ");
                sb.Append("@leadSourceId uniqueidentifier");                
                sb.Append(") ");
                sb.Append("AS ");
                sb.Append("DELETE FROM LeadSource WHERE LeadSourceId = @leadSourceId ");

                string splText = sb.ToString();
                SqlCommand createSPL = new SqlCommand(splText, _connection);
                createSPL.ExecuteNonQuery();

            }
            catch (Exception exc)
            {
                System.Windows.Forms.MessageBox.Show("Can't create DELETE procedure for Lead Staff list" + System.Environment.NewLine + exc.Message);
            }
        }
    }
}
