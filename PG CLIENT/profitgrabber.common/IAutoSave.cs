﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProfitGrabber.Common
{
    public interface IAutoSave
    {        
        void TriggerTextBoxLeave();
        void TriggerTextBoxChanged();
        void TriggerSelectedIndexChanged();
    }
}
