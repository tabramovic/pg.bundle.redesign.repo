﻿using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace ProfitGrabber.Common
{
    public sealed class UsersTableManager
    {
        protected static UsersTableManager _instance = new UsersTableManager();

        static UsersTableManager()
        { }

        UsersTableManager()
        { }

        public static UsersTableManager Instance
        {
            get { return _instance; }
        }

        bool _isTableExpanded = false;
        SqlConnection _connection;

        public void TestAlterColumn(SqlConnection conn)
        {
            if (null == _connection)
            {
                _connection = conn;
                _isTableExpanded = TestIfTableIsExpanded();

                if (!_isTableExpanded)
                {
                    ExpandTable();
                    DeletePreviousSSXData();
                }
            }
        }

        bool TestIfTableIsExpanded()
        {
            bool retVal = false;
            SqlDataReader reader = null;
            string cmdText = "SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'users' AND COLUMN_NAME = 'Email'";

            try
            {
                SqlCommand testForLayer = new SqlCommand(cmdText, _connection);
                reader = testForLayer.ExecuteReader();
                while (reader.Read())
                {
                    retVal = true;
                    break;
                }
            }
            catch
            {
                retVal = false;
            }
            finally
            {
                reader.Close();
            }

            return retVal;
        }

        bool ExpandTable()
        {
            bool retVal = false;
            string cmdText = "ALTER TABLE users ADD Email nvarchar(255);";

            try
            {
                SqlCommand createLayer = new SqlCommand(cmdText, _connection);
                createLayer.ExecuteNonQuery();
                retVal = true;
            }
            catch
            {
                retVal = false;
            }
            return retVal;
        }


        bool DeletePreviousSSXData()
        {
            bool retVal = false;
            string cmdText = "delete from shortsale";

            try
            {
                SqlCommand createLayer = new SqlCommand(cmdText, _connection);
                createLayer.ExecuteNonQuery();
                retVal = true;
            }
            catch
            {
                retVal = false;
            }
            return retVal;
        }


    }
}
