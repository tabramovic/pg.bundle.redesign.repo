using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;
using System.Globalization;

namespace WindowsApplication.AcquisitionCalculator
{
	/// <summary>
	/// Summary description for UserControl.
	/// </summary>
	public class UserControl_Sub2RetailAuction : System.Windows.Forms.UserControl
	{
		#region DATA
		private AcquisitionCalculator.Sub2 sub2;
		private int business_logic_counter;
		private decimal	dec_worth_by_comps;

		private System.Windows.Forms.GroupBox groupBoxExitStrategy;
		private System.Windows.Forms.RadioButton radioRetail;
		private System.Windows.Forms.RadioButton radioButtonAuction;
		private System.Windows.Forms.Label labelProjectedResalePrice;
		private TextBox textBoxProjectedResalePrice;
		private System.Windows.Forms.Label labelProjectedNumberOfMonths;
		private System.Windows.Forms.Label labelTransactionalCosts;
		private System.Windows.Forms.Label labelHoldingCosts;
		private System.Windows.Forms.Label labelDealCost;
		private TextBox textBoxAdvertisingCosts;
		private System.Windows.Forms.Label labelAdvertisingCosts;
		private TextBox textBoxSellingFees;
		private System.Windows.Forms.Label labelSellingFees;
		private TextBox textBoxClosingCostsAtPurchase;
		private System.Windows.Forms.Label labelClosingCostsAtPurchase;
		private TextBox textBoxClosingCostsAtResale;
		private System.Windows.Forms.Label labelClosingCostsAtResale;
		private TextBox textBoxHousePreparationCosts;
		private System.Windows.Forms.Label labelHousePreparationCosts;
		private TextBox textBoxMiscellaneousCosts;
		private System.Windows.Forms.Label labelMiscellaneousCosts;
		private System.Windows.Forms.Button buttonBreakDownAdvertisingCosts;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Label labelHoldingCostsMonthlyPayments;
		private TextBox textBoxHoldingCostsOther;
		private System.Windows.Forms.Label labelHoldingCostsOther;
		private TextBox textBoxHoldingCostsMonthlyPayments;
		private TextBox textBoxTotalHoldingCost;
		private System.Windows.Forms.Label labelTotalHoldingCost;
		private TextBox textBoxApproximateLoanPayoff;
		private System.Windows.Forms.Label labelApproximateLoanPayoff;
		private TextBox textBoxTaxesDue;
		private System.Windows.Forms.Label labelTaxesDue;
		private TextBox textBoxOtherEncumbrances;
		private System.Windows.Forms.Label labelOtherEncumbrances;
		private TextBox textBoxTotalDealCost;
		private System.Windows.Forms.Label labelTotalDealCost;
		private TextBox textBoxMaximumPurchasePrice;
		private System.Windows.Forms.Label labelMaximumPurchasePrice;
		private System.Windows.Forms.RadioButton radioButtonPaymentsResaleNo;
		private System.Windows.Forms.RadioButton radioButtonPaymentsResaleYes;
		private TextBox textBoxTotalTransactionalCost;
		private System.Windows.Forms.Label labelTotalTransactionalCost;
		private TextBox textBoxDesiredProfitInBucks;
		private TextBox textBoxlDesiredProfitInPercentage;
		private System.Windows.Forms.Label labelDesiredProfitInPercentage;
		private System.Windows.Forms.Label labelDesiredProfitInBucks;
		private TextBox textBoxProjectedNumberOfMonths;
		private System.Windows.Forms.Label labelMaxAcquisitionPrice;
		private TextBox textBoxMaxAcquisitionPrice;
		private System.Windows.Forms.Button buttonBreakDownBoxSellingFees;
		private System.Windows.Forms.Button buttonBreakDownHousePreparationCosts;
		private System.Windows.Forms.Button buttonBreakDownClosingCostsAtPurchase;
		private System.Windows.Forms.Button buttonBreakDownClosingCostsAtResale;
		private System.Windows.Forms.Button buttonBreakDownMiscellaneousCosts;
		private System.Windows.Forms.Button buttonBreakDownHoldingCostsOther;
		private TextBox textBoxHoldingMurphyFactor;
		private System.Windows.Forms.Label labelHoldingMurphyFactor;
		private TextBox textBoxTransactionalMurphyFactor;
		private System.Windows.Forms.Label labelTransactionalMurphyFactor;
		private TextBox textBoxCashToSeller;
		private System.Windows.Forms.Label labelCashToSeller;
		private System.Windows.Forms.GroupBox groupBoxPaymentsAddedToPayoff;
		private System.Windows.Forms.RadioButton radioButton1;
		private System.Windows.Forms.RadioButton radioButton2;
		private TextBox textBoxBalance1;
		private System.Windows.Forms.Label labelBalance1;
		private TextBox textBoxMonthlyPayment1;
		private System.Windows.Forms.Label labelMonthlyPayment1;
		private TextBox textBoxMonthlyPayment2;
		private System.Windows.Forms.Label labelMonthlyPayment2;
		private TextBox textBoxBalance2;
		private System.Windows.Forms.Label labelBalance2;
		private TextBox textBoxMonthlyPayment3;
		private System.Windows.Forms.Label labelMonthlyPayment3;
		private TextBox textBoxBalance3;
		private System.Windows.Forms.Label labelBalance3;
		private System.Windows.Forms.Label labelForeclosureReinstatementAmount;
		private TextBox textBoxTotalAcquisitionDealCost;
		private System.Windows.Forms.Label labelTotalAcquisitionDealCost;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.RadioButton radioButton3;
		private System.Windows.Forms.RadioButton radioButton4;
		private System.Windows.Forms.Label labelCashRequirements;
		private TextBox textBoxNeededToCoverTheCosts;
		private System.Windows.Forms.Label labelCashNeededToCoverTheCosts;
		private System.Windows.Forms.Label labelUntilGettingTheCheck;
		private WindowsApplication.AcquisitionCalculator.TextBox textBoxForeclosureReinstatementAmount;
		private System.Windows.Forms.PictureBox pictureBox1;
		private WindowsApplication.AcquisitionCalculator.TextBox textBoxRetailAuctionPercentage;
		private System.Windows.Forms.Label labelRetailAuctionPercentage;
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
		#endregion DATA

		public UserControl_Sub2RetailAuction()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();
			
			sub2 = new Sub2();
			business_logic_counter = 0;
			BussinesLogic(this, new System.EventArgs());
		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(UserControl_Sub2RetailAuction));
			this.groupBoxExitStrategy = new System.Windows.Forms.GroupBox();
			this.radioButtonAuction = new System.Windows.Forms.RadioButton();
			this.radioRetail = new System.Windows.Forms.RadioButton();
			this.labelProjectedResalePrice = new System.Windows.Forms.Label();
			this.textBoxProjectedResalePrice = new WindowsApplication.AcquisitionCalculator.TextBox();
			this.textBoxProjectedNumberOfMonths = new WindowsApplication.AcquisitionCalculator.TextBox();
			this.labelProjectedNumberOfMonths = new System.Windows.Forms.Label();
			this.labelTransactionalCosts = new System.Windows.Forms.Label();
			this.labelHoldingCosts = new System.Windows.Forms.Label();
			this.labelDealCost = new System.Windows.Forms.Label();
			this.textBoxAdvertisingCosts = new WindowsApplication.AcquisitionCalculator.TextBox();
			this.labelAdvertisingCosts = new System.Windows.Forms.Label();
			this.textBoxSellingFees = new WindowsApplication.AcquisitionCalculator.TextBox();
			this.labelSellingFees = new System.Windows.Forms.Label();
			this.textBoxClosingCostsAtPurchase = new WindowsApplication.AcquisitionCalculator.TextBox();
			this.labelClosingCostsAtPurchase = new System.Windows.Forms.Label();
			this.textBoxClosingCostsAtResale = new WindowsApplication.AcquisitionCalculator.TextBox();
			this.labelClosingCostsAtResale = new System.Windows.Forms.Label();
			this.textBoxHousePreparationCosts = new WindowsApplication.AcquisitionCalculator.TextBox();
			this.labelHousePreparationCosts = new System.Windows.Forms.Label();
			this.textBoxMiscellaneousCosts = new WindowsApplication.AcquisitionCalculator.TextBox();
			this.labelMiscellaneousCosts = new System.Windows.Forms.Label();
			this.buttonBreakDownAdvertisingCosts = new System.Windows.Forms.Button();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.radioButtonPaymentsResaleNo = new System.Windows.Forms.RadioButton();
			this.radioButtonPaymentsResaleYes = new System.Windows.Forms.RadioButton();
			this.textBoxHoldingCostsOther = new WindowsApplication.AcquisitionCalculator.TextBox();
			this.labelHoldingCostsOther = new System.Windows.Forms.Label();
			this.textBoxHoldingCostsMonthlyPayments = new WindowsApplication.AcquisitionCalculator.TextBox();
			this.labelHoldingCostsMonthlyPayments = new System.Windows.Forms.Label();
			this.textBoxTotalHoldingCost = new WindowsApplication.AcquisitionCalculator.TextBox();
			this.labelTotalHoldingCost = new System.Windows.Forms.Label();
			this.textBoxHoldingMurphyFactor = new WindowsApplication.AcquisitionCalculator.TextBox();
			this.labelHoldingMurphyFactor = new System.Windows.Forms.Label();
			this.textBoxCashToSeller = new WindowsApplication.AcquisitionCalculator.TextBox();
			this.labelCashToSeller = new System.Windows.Forms.Label();
			this.textBoxApproximateLoanPayoff = new WindowsApplication.AcquisitionCalculator.TextBox();
			this.labelApproximateLoanPayoff = new System.Windows.Forms.Label();
			this.textBoxTaxesDue = new WindowsApplication.AcquisitionCalculator.TextBox();
			this.labelTaxesDue = new System.Windows.Forms.Label();
			this.textBoxOtherEncumbrances = new WindowsApplication.AcquisitionCalculator.TextBox();
			this.labelOtherEncumbrances = new System.Windows.Forms.Label();
			this.textBoxTotalDealCost = new WindowsApplication.AcquisitionCalculator.TextBox();
			this.labelTotalDealCost = new System.Windows.Forms.Label();
			this.textBoxMaximumPurchasePrice = new WindowsApplication.AcquisitionCalculator.TextBox();
			this.labelMaximumPurchasePrice = new System.Windows.Forms.Label();
			this.textBoxTotalTransactionalCost = new WindowsApplication.AcquisitionCalculator.TextBox();
			this.labelTotalTransactionalCost = new System.Windows.Forms.Label();
			this.labelMaxAcquisitionPrice = new System.Windows.Forms.Label();
			this.textBoxMaxAcquisitionPrice = new WindowsApplication.AcquisitionCalculator.TextBox();
			this.textBoxDesiredProfitInBucks = new WindowsApplication.AcquisitionCalculator.TextBox();
			this.textBoxlDesiredProfitInPercentage = new WindowsApplication.AcquisitionCalculator.TextBox();
			this.labelDesiredProfitInPercentage = new System.Windows.Forms.Label();
			this.labelDesiredProfitInBucks = new System.Windows.Forms.Label();
			this.buttonBreakDownBoxSellingFees = new System.Windows.Forms.Button();
			this.buttonBreakDownHousePreparationCosts = new System.Windows.Forms.Button();
			this.buttonBreakDownClosingCostsAtPurchase = new System.Windows.Forms.Button();
			this.buttonBreakDownClosingCostsAtResale = new System.Windows.Forms.Button();
			this.buttonBreakDownMiscellaneousCosts = new System.Windows.Forms.Button();
			this.buttonBreakDownHoldingCostsOther = new System.Windows.Forms.Button();
			this.textBoxTransactionalMurphyFactor = new WindowsApplication.AcquisitionCalculator.TextBox();
			this.labelTransactionalMurphyFactor = new System.Windows.Forms.Label();
			this.groupBoxPaymentsAddedToPayoff = new System.Windows.Forms.GroupBox();
			this.radioButton1 = new System.Windows.Forms.RadioButton();
			this.radioButton2 = new System.Windows.Forms.RadioButton();
			this.textBoxBalance1 = new WindowsApplication.AcquisitionCalculator.TextBox();
			this.labelBalance1 = new System.Windows.Forms.Label();
			this.textBoxMonthlyPayment1 = new WindowsApplication.AcquisitionCalculator.TextBox();
			this.labelMonthlyPayment1 = new System.Windows.Forms.Label();
			this.textBoxMonthlyPayment2 = new WindowsApplication.AcquisitionCalculator.TextBox();
			this.labelMonthlyPayment2 = new System.Windows.Forms.Label();
			this.textBoxBalance2 = new WindowsApplication.AcquisitionCalculator.TextBox();
			this.labelBalance2 = new System.Windows.Forms.Label();
			this.textBoxMonthlyPayment3 = new WindowsApplication.AcquisitionCalculator.TextBox();
			this.labelMonthlyPayment3 = new System.Windows.Forms.Label();
			this.textBoxBalance3 = new WindowsApplication.AcquisitionCalculator.TextBox();
			this.labelBalance3 = new System.Windows.Forms.Label();
			this.textBoxForeclosureReinstatementAmount = new WindowsApplication.AcquisitionCalculator.TextBox();
			this.labelForeclosureReinstatementAmount = new System.Windows.Forms.Label();
			this.textBoxTotalAcquisitionDealCost = new WindowsApplication.AcquisitionCalculator.TextBox();
			this.labelTotalAcquisitionDealCost = new System.Windows.Forms.Label();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.radioButton3 = new System.Windows.Forms.RadioButton();
			this.radioButton4 = new System.Windows.Forms.RadioButton();
			this.labelCashRequirements = new System.Windows.Forms.Label();
			this.textBoxNeededToCoverTheCosts = new WindowsApplication.AcquisitionCalculator.TextBox();
			this.labelCashNeededToCoverTheCosts = new System.Windows.Forms.Label();
			this.labelUntilGettingTheCheck = new System.Windows.Forms.Label();
			this.pictureBox1 = new System.Windows.Forms.PictureBox();
			this.textBoxRetailAuctionPercentage = new WindowsApplication.AcquisitionCalculator.TextBox();
			this.labelRetailAuctionPercentage = new System.Windows.Forms.Label();
			this.groupBoxExitStrategy.SuspendLayout();
			this.groupBox1.SuspendLayout();
			this.groupBoxPaymentsAddedToPayoff.SuspendLayout();
			this.groupBox2.SuspendLayout();
			this.SuspendLayout();
			// 
			// groupBoxExitStrategy
			// 
			this.groupBoxExitStrategy.Controls.Add(this.radioButtonAuction);
			this.groupBoxExitStrategy.Controls.Add(this.radioRetail);
			this.groupBoxExitStrategy.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.groupBoxExitStrategy.Location = new System.Drawing.Point(8, 8);
			this.groupBoxExitStrategy.Name = "groupBoxExitStrategy";
			this.groupBoxExitStrategy.Size = new System.Drawing.Size(152, 40);
			this.groupBoxExitStrategy.TabIndex = 0;
			this.groupBoxExitStrategy.TabStop = false;
			this.groupBoxExitStrategy.Text = "Select Your Exit Strategy:";
			// 
			// radioButtonAuction
			// 
			this.radioButtonAuction.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.radioButtonAuction.Location = new System.Drawing.Point(80, 16);
			this.radioButtonAuction.Name = "radioButtonAuction";
			this.radioButtonAuction.Size = new System.Drawing.Size(64, 16);
			this.radioButtonAuction.TabIndex = 1;
			this.radioButtonAuction.Text = "Auction";
			// 
			// radioRetail
			// 
			this.radioRetail.Checked = true;
			this.radioRetail.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.radioRetail.Location = new System.Drawing.Point(8, 16);
			this.radioRetail.Name = "radioRetail";
			this.radioRetail.Size = new System.Drawing.Size(56, 16);
			this.radioRetail.TabIndex = 0;
			this.radioRetail.TabStop = true;
			this.radioRetail.Text = "Retail";
			// 
			// labelProjectedResalePrice
			// 
			this.labelProjectedResalePrice.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.labelProjectedResalePrice.Location = new System.Drawing.Point(160, 40);
			this.labelProjectedResalePrice.Name = "labelProjectedResalePrice";
			this.labelProjectedResalePrice.Size = new System.Drawing.Size(128, 23);
			this.labelProjectedResalePrice.TabIndex = 7;
			this.labelProjectedResalePrice.Text = "Projected Resale Price:";
			// 
			// textBoxProjectedResalePrice
			// 
			this.textBoxProjectedResalePrice.Location = new System.Drawing.Point(280, 40);
			this.textBoxProjectedResalePrice.Name = "textBoxProjectedResalePrice";
			this.textBoxProjectedResalePrice.Size = new System.Drawing.Size(60, 20);
			this.textBoxProjectedResalePrice.TabIndex = 8;
			this.textBoxProjectedResalePrice.Text = "$0.00";
			this.textBoxProjectedResalePrice.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.textBoxProjectedResalePrice.Leave += new System.EventHandler(this.BussinesLogic);
			// 
			// textBoxProjectedNumberOfMonths
			// 
			this.textBoxProjectedNumberOfMonths.Location = new System.Drawing.Point(640, 16);
			this.textBoxProjectedNumberOfMonths.Name = "textBoxProjectedNumberOfMonths";
			this.textBoxProjectedNumberOfMonths.Size = new System.Drawing.Size(32, 20);
			this.textBoxProjectedNumberOfMonths.TabIndex = 6;
			this.textBoxProjectedNumberOfMonths.Text = "0";
			this.textBoxProjectedNumberOfMonths.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.textBoxProjectedNumberOfMonths.Leave += new System.EventHandler(this.BussinesLogic);
			// 
			// labelProjectedNumberOfMonths
			// 
			this.labelProjectedNumberOfMonths.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.labelProjectedNumberOfMonths.Location = new System.Drawing.Point(520, 16);
			this.labelProjectedNumberOfMonths.Name = "labelProjectedNumberOfMonths";
			this.labelProjectedNumberOfMonths.Size = new System.Drawing.Size(128, 23);
			this.labelProjectedNumberOfMonths.TabIndex = 5;
			this.labelProjectedNumberOfMonths.Text = "Projected No. of Months:";
			// 
			// labelTransactionalCosts
			// 
			this.labelTransactionalCosts.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.labelTransactionalCosts.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.labelTransactionalCosts.Location = new System.Drawing.Point(0, 56);
			this.labelTransactionalCosts.Name = "labelTransactionalCosts";
			this.labelTransactionalCosts.Size = new System.Drawing.Size(160, 23);
			this.labelTransactionalCosts.TabIndex = 13;
			this.labelTransactionalCosts.Text = "TRANSACTIONAL COSTS:";
			// 
			// labelHoldingCosts
			// 
			this.labelHoldingCosts.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.labelHoldingCosts.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.labelHoldingCosts.Location = new System.Drawing.Point(0, 152);
			this.labelHoldingCosts.Name = "labelHoldingCosts";
			this.labelHoldingCosts.Size = new System.Drawing.Size(104, 23);
			this.labelHoldingCosts.TabIndex = 36;
			this.labelHoldingCosts.Text = "HOLDING COSTS:";
			// 
			// labelDealCost
			// 
			this.labelDealCost.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.labelDealCost.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.labelDealCost.Location = new System.Drawing.Point(0, 288);
			this.labelDealCost.Name = "labelDealCost";
			this.labelDealCost.TabIndex = 60;
			this.labelDealCost.Text = "DEAL COSTS:";
			// 
			// textBoxAdvertisingCosts
			// 
			this.textBoxAdvertisingCosts.Location = new System.Drawing.Point(104, 72);
			this.textBoxAdvertisingCosts.Name = "textBoxAdvertisingCosts";
			this.textBoxAdvertisingCosts.Size = new System.Drawing.Size(60, 20);
			this.textBoxAdvertisingCosts.TabIndex = 15;
			this.textBoxAdvertisingCosts.Text = "$0.00";
			this.textBoxAdvertisingCosts.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.textBoxAdvertisingCosts.Leave += new System.EventHandler(this.BussinesLogic);
			// 
			// labelAdvertisingCosts
			// 
			this.labelAdvertisingCosts.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.labelAdvertisingCosts.Location = new System.Drawing.Point(16, 72);
			this.labelAdvertisingCosts.Name = "labelAdvertisingCosts";
			this.labelAdvertisingCosts.Size = new System.Drawing.Size(96, 23);
			this.labelAdvertisingCosts.TabIndex = 14;
			this.labelAdvertisingCosts.Text = "Advertising Costs:";
			// 
			// textBoxSellingFees
			// 
			this.textBoxSellingFees.Location = new System.Drawing.Point(104, 96);
			this.textBoxSellingFees.Name = "textBoxSellingFees";
			this.textBoxSellingFees.Size = new System.Drawing.Size(60, 20);
			this.textBoxSellingFees.TabIndex = 24;
			this.textBoxSellingFees.Text = "$0.00";
			this.textBoxSellingFees.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.textBoxSellingFees.Leave += new System.EventHandler(this.BussinesLogic);
			// 
			// labelSellingFees
			// 
			this.labelSellingFees.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.labelSellingFees.Location = new System.Drawing.Point(16, 96);
			this.labelSellingFees.Name = "labelSellingFees";
			this.labelSellingFees.Size = new System.Drawing.Size(96, 23);
			this.labelSellingFees.TabIndex = 23;
			this.labelSellingFees.Text = "Selling Fees:";
			// 
			// textBoxClosingCostsAtPurchase
			// 
			this.textBoxClosingCostsAtPurchase.Location = new System.Drawing.Point(344, 72);
			this.textBoxClosingCostsAtPurchase.Name = "textBoxClosingCostsAtPurchase";
			this.textBoxClosingCostsAtPurchase.Size = new System.Drawing.Size(60, 20);
			this.textBoxClosingCostsAtPurchase.TabIndex = 18;
			this.textBoxClosingCostsAtPurchase.Text = "$0.00";
			this.textBoxClosingCostsAtPurchase.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.textBoxClosingCostsAtPurchase.Leave += new System.EventHandler(this.BussinesLogic);
			// 
			// labelClosingCostsAtPurchase
			// 
			this.labelClosingCostsAtPurchase.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.labelClosingCostsAtPurchase.Location = new System.Drawing.Point(208, 72);
			this.labelClosingCostsAtPurchase.Name = "labelClosingCostsAtPurchase";
			this.labelClosingCostsAtPurchase.Size = new System.Drawing.Size(144, 23);
			this.labelClosingCostsAtPurchase.TabIndex = 17;
			this.labelClosingCostsAtPurchase.Text = "Closing Costs At Purchase:";
			// 
			// textBoxClosingCostsAtResale
			// 
			this.textBoxClosingCostsAtResale.Location = new System.Drawing.Point(344, 96);
			this.textBoxClosingCostsAtResale.Name = "textBoxClosingCostsAtResale";
			this.textBoxClosingCostsAtResale.Size = new System.Drawing.Size(60, 20);
			this.textBoxClosingCostsAtResale.TabIndex = 27;
			this.textBoxClosingCostsAtResale.Text = "$0.00";
			this.textBoxClosingCostsAtResale.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.textBoxClosingCostsAtResale.Leave += new System.EventHandler(this.BussinesLogic);
			// 
			// labelClosingCostsAtResale
			// 
			this.labelClosingCostsAtResale.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.labelClosingCostsAtResale.Location = new System.Drawing.Point(208, 96);
			this.labelClosingCostsAtResale.Name = "labelClosingCostsAtResale";
			this.labelClosingCostsAtResale.Size = new System.Drawing.Size(136, 23);
			this.labelClosingCostsAtResale.TabIndex = 26;
			this.labelClosingCostsAtResale.Text = "Closing Costs At Resale:";
			// 
			// textBoxHousePreparationCosts
			// 
			this.textBoxHousePreparationCosts.Location = new System.Drawing.Point(576, 72);
			this.textBoxHousePreparationCosts.Name = "textBoxHousePreparationCosts";
			this.textBoxHousePreparationCosts.Size = new System.Drawing.Size(60, 20);
			this.textBoxHousePreparationCosts.TabIndex = 21;
			this.textBoxHousePreparationCosts.Text = "$0.00";
			this.textBoxHousePreparationCosts.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.textBoxHousePreparationCosts.Leave += new System.EventHandler(this.BussinesLogic);
			// 
			// labelHousePreparationCosts
			// 
			this.labelHousePreparationCosts.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.labelHousePreparationCosts.Location = new System.Drawing.Point(448, 72);
			this.labelHousePreparationCosts.Name = "labelHousePreparationCosts";
			this.labelHousePreparationCosts.Size = new System.Drawing.Size(136, 23);
			this.labelHousePreparationCosts.TabIndex = 20;
			this.labelHousePreparationCosts.Text = "House Preparation Costs:";
			// 
			// textBoxMiscellaneousCosts
			// 
			this.textBoxMiscellaneousCosts.Location = new System.Drawing.Point(576, 96);
			this.textBoxMiscellaneousCosts.Name = "textBoxMiscellaneousCosts";
			this.textBoxMiscellaneousCosts.Size = new System.Drawing.Size(60, 20);
			this.textBoxMiscellaneousCosts.TabIndex = 30;
			this.textBoxMiscellaneousCosts.Text = "$0.00";
			this.textBoxMiscellaneousCosts.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.textBoxMiscellaneousCosts.Leave += new System.EventHandler(this.BussinesLogic);
			// 
			// labelMiscellaneousCosts
			// 
			this.labelMiscellaneousCosts.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.labelMiscellaneousCosts.Location = new System.Drawing.Point(448, 96);
			this.labelMiscellaneousCosts.Name = "labelMiscellaneousCosts";
			this.labelMiscellaneousCosts.Size = new System.Drawing.Size(128, 23);
			this.labelMiscellaneousCosts.TabIndex = 29;
			this.labelMiscellaneousCosts.Text = "Miscellaneous Costs:";
			// 
			// buttonBreakDownAdvertisingCosts
			// 
			this.buttonBreakDownAdvertisingCosts.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.buttonBreakDownAdvertisingCosts.Image = ((System.Drawing.Image)(resources.GetObject("buttonBreakDownAdvertisingCosts.Image")));
			this.buttonBreakDownAdvertisingCosts.Location = new System.Drawing.Point(168, 72);
			this.buttonBreakDownAdvertisingCosts.Name = "buttonBreakDownAdvertisingCosts";
			this.buttonBreakDownAdvertisingCosts.Size = new System.Drawing.Size(24, 23);
			this.buttonBreakDownAdvertisingCosts.TabIndex = 16;
			this.buttonBreakDownAdvertisingCosts.Click += new System.EventHandler(this.buttonBreakDownAdvertisingCosts_Click);
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.radioButtonPaymentsResaleNo);
			this.groupBox1.Controls.Add(this.radioButtonPaymentsResaleYes);
			this.groupBox1.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.groupBox1.Location = new System.Drawing.Point(112, 152);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(224, 40);
			this.groupBox1.TabIndex = 37;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Are You making Payments until Resale?";
			// 
			// radioButtonPaymentsResaleNo
			// 
			this.radioButtonPaymentsResaleNo.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.radioButtonPaymentsResaleNo.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.radioButtonPaymentsResaleNo.Location = new System.Drawing.Point(80, 16);
			this.radioButtonPaymentsResaleNo.Name = "radioButtonPaymentsResaleNo";
			this.radioButtonPaymentsResaleNo.Size = new System.Drawing.Size(40, 16);
			this.radioButtonPaymentsResaleNo.TabIndex = 1;
			this.radioButtonPaymentsResaleNo.Text = "No";
			// 
			// radioButtonPaymentsResaleYes
			// 
			this.radioButtonPaymentsResaleYes.Checked = true;
			this.radioButtonPaymentsResaleYes.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.radioButtonPaymentsResaleYes.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.radioButtonPaymentsResaleYes.Location = new System.Drawing.Point(8, 16);
			this.radioButtonPaymentsResaleYes.Name = "radioButtonPaymentsResaleYes";
			this.radioButtonPaymentsResaleYes.Size = new System.Drawing.Size(48, 16);
			this.radioButtonPaymentsResaleYes.TabIndex = 0;
			this.radioButtonPaymentsResaleYes.TabStop = true;
			this.radioButtonPaymentsResaleYes.Text = "Yes";
			// 
			// textBoxHoldingCostsOther
			// 
			this.textBoxHoldingCostsOther.Location = new System.Drawing.Point(512, 240);
			this.textBoxHoldingCostsOther.Name = "textBoxHoldingCostsOther";
			this.textBoxHoldingCostsOther.Size = new System.Drawing.Size(60, 20);
			this.textBoxHoldingCostsOther.TabIndex = 54;
			this.textBoxHoldingCostsOther.Text = "$0.00";
			this.textBoxHoldingCostsOther.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.textBoxHoldingCostsOther.Leave += new System.EventHandler(this.BussinesLogic);
			// 
			// labelHoldingCostsOther
			// 
			this.labelHoldingCostsOther.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.labelHoldingCostsOther.Location = new System.Drawing.Point(400, 240);
			this.labelHoldingCostsOther.Name = "labelHoldingCostsOther";
			this.labelHoldingCostsOther.Size = new System.Drawing.Size(120, 23);
			this.labelHoldingCostsOther.TabIndex = 53;
			this.labelHoldingCostsOther.Text = "Holding Costs - Other:";
			// 
			// textBoxHoldingCostsMonthlyPayments
			// 
			this.textBoxHoldingCostsMonthlyPayments.Location = new System.Drawing.Point(184, 240);
			this.textBoxHoldingCostsMonthlyPayments.Name = "textBoxHoldingCostsMonthlyPayments";
			this.textBoxHoldingCostsMonthlyPayments.Size = new System.Drawing.Size(60, 20);
			this.textBoxHoldingCostsMonthlyPayments.TabIndex = 52;
			this.textBoxHoldingCostsMonthlyPayments.Text = "$0.00";
			this.textBoxHoldingCostsMonthlyPayments.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// labelHoldingCostsMonthlyPayments
			// 
			this.labelHoldingCostsMonthlyPayments.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.labelHoldingCostsMonthlyPayments.Location = new System.Drawing.Point(16, 240);
			this.labelHoldingCostsMonthlyPayments.Name = "labelHoldingCostsMonthlyPayments";
			this.labelHoldingCostsMonthlyPayments.Size = new System.Drawing.Size(184, 23);
			this.labelHoldingCostsMonthlyPayments.TabIndex = 51;
			this.labelHoldingCostsMonthlyPayments.Text = "Holding Costs - Monthly Payments:";
			// 
			// textBoxTotalHoldingCost
			// 
			this.textBoxTotalHoldingCost.Location = new System.Drawing.Point(368, 264);
			this.textBoxTotalHoldingCost.Name = "textBoxTotalHoldingCost";
			this.textBoxTotalHoldingCost.Size = new System.Drawing.Size(60, 20);
			this.textBoxTotalHoldingCost.TabIndex = 57;
			this.textBoxTotalHoldingCost.Text = "$0.00";
			this.textBoxTotalHoldingCost.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.textBoxTotalHoldingCost.Leave += new System.EventHandler(this.BussinesLogic);
			// 
			// labelTotalHoldingCost
			// 
			this.labelTotalHoldingCost.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.labelTotalHoldingCost.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.labelTotalHoldingCost.Location = new System.Drawing.Point(256, 264);
			this.labelTotalHoldingCost.Name = "labelTotalHoldingCost";
			this.labelTotalHoldingCost.Size = new System.Drawing.Size(112, 23);
			this.labelTotalHoldingCost.TabIndex = 56;
			this.labelTotalHoldingCost.Text = "Total Holding Costs:";
			// 
			// textBoxHoldingMurphyFactor
			// 
			this.textBoxHoldingMurphyFactor.Location = new System.Drawing.Point(560, 264);
			this.textBoxHoldingMurphyFactor.Name = "textBoxHoldingMurphyFactor";
			this.textBoxHoldingMurphyFactor.Size = new System.Drawing.Size(40, 20);
			this.textBoxHoldingMurphyFactor.TabIndex = 59;
			this.textBoxHoldingMurphyFactor.Text = "0%";
			this.textBoxHoldingMurphyFactor.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.textBoxHoldingMurphyFactor.Leave += new System.EventHandler(this.BussinesLogic);
			// 
			// labelHoldingMurphyFactor
			// 
			this.labelHoldingMurphyFactor.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.labelHoldingMurphyFactor.Location = new System.Drawing.Point(472, 264);
			this.labelHoldingMurphyFactor.Name = "labelHoldingMurphyFactor";
			this.labelHoldingMurphyFactor.Size = new System.Drawing.Size(80, 23);
			this.labelHoldingMurphyFactor.TabIndex = 58;
			this.labelHoldingMurphyFactor.Text = "Murphy Factor:";
			// 
			// textBoxCashToSeller
			// 
			this.textBoxCashToSeller.Location = new System.Drawing.Point(96, 312);
			this.textBoxCashToSeller.Name = "textBoxCashToSeller";
			this.textBoxCashToSeller.Size = new System.Drawing.Size(60, 20);
			this.textBoxCashToSeller.TabIndex = 62;
			this.textBoxCashToSeller.Text = "$0.00";
			this.textBoxCashToSeller.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.textBoxCashToSeller.Leave += new System.EventHandler(this.BussinesLogic);
			// 
			// labelCashToSeller
			// 
			this.labelCashToSeller.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.labelCashToSeller.Location = new System.Drawing.Point(16, 312);
			this.labelCashToSeller.Name = "labelCashToSeller";
			this.labelCashToSeller.Size = new System.Drawing.Size(88, 23);
			this.labelCashToSeller.TabIndex = 61;
			this.labelCashToSeller.Text = "Cash To Seller:";
			// 
			// textBoxApproximateLoanPayoff
			// 
			this.textBoxApproximateLoanPayoff.Location = new System.Drawing.Point(296, 312);
			this.textBoxApproximateLoanPayoff.Name = "textBoxApproximateLoanPayoff";
			this.textBoxApproximateLoanPayoff.Size = new System.Drawing.Size(60, 20);
			this.textBoxApproximateLoanPayoff.TabIndex = 64;
			this.textBoxApproximateLoanPayoff.Text = "$0.00";
			this.textBoxApproximateLoanPayoff.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.textBoxApproximateLoanPayoff.Leave += new System.EventHandler(this.BussinesLogic);
			// 
			// labelApproximateLoanPayoff
			// 
			this.labelApproximateLoanPayoff.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.labelApproximateLoanPayoff.Location = new System.Drawing.Point(168, 312);
			this.labelApproximateLoanPayoff.Name = "labelApproximateLoanPayoff";
			this.labelApproximateLoanPayoff.Size = new System.Drawing.Size(136, 23);
			this.labelApproximateLoanPayoff.TabIndex = 63;
			this.labelApproximateLoanPayoff.Text = "Approximate Loan Payoff:";
			// 
			// textBoxTaxesDue
			// 
			this.textBoxTaxesDue.Location = new System.Drawing.Point(424, 312);
			this.textBoxTaxesDue.Name = "textBoxTaxesDue";
			this.textBoxTaxesDue.Size = new System.Drawing.Size(60, 20);
			this.textBoxTaxesDue.TabIndex = 66;
			this.textBoxTaxesDue.Text = "$0.00";
			this.textBoxTaxesDue.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.textBoxTaxesDue.Leave += new System.EventHandler(this.BussinesLogic);
			// 
			// labelTaxesDue
			// 
			this.labelTaxesDue.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.labelTaxesDue.Location = new System.Drawing.Point(368, 312);
			this.labelTaxesDue.Name = "labelTaxesDue";
			this.labelTaxesDue.Size = new System.Drawing.Size(64, 23);
			this.labelTaxesDue.TabIndex = 65;
			this.labelTaxesDue.Text = "Taxes Due:";
			// 
			// textBoxOtherEncumbrances
			// 
			this.textBoxOtherEncumbrances.Location = new System.Drawing.Point(608, 312);
			this.textBoxOtherEncumbrances.Name = "textBoxOtherEncumbrances";
			this.textBoxOtherEncumbrances.Size = new System.Drawing.Size(60, 20);
			this.textBoxOtherEncumbrances.TabIndex = 68;
			this.textBoxOtherEncumbrances.Text = "$0.00";
			this.textBoxOtherEncumbrances.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.textBoxOtherEncumbrances.Leave += new System.EventHandler(this.BussinesLogic);
			// 
			// labelOtherEncumbrances
			// 
			this.labelOtherEncumbrances.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.labelOtherEncumbrances.Location = new System.Drawing.Point(496, 312);
			this.labelOtherEncumbrances.Name = "labelOtherEncumbrances";
			this.labelOtherEncumbrances.Size = new System.Drawing.Size(120, 23);
			this.labelOtherEncumbrances.TabIndex = 67;
			this.labelOtherEncumbrances.Text = "Other Encumbrances:";
			// 
			// textBoxTotalDealCost
			// 
			this.textBoxTotalDealCost.Location = new System.Drawing.Point(368, 360);
			this.textBoxTotalDealCost.Name = "textBoxTotalDealCost";
			this.textBoxTotalDealCost.Size = new System.Drawing.Size(60, 20);
			this.textBoxTotalDealCost.TabIndex = 72;
			this.textBoxTotalDealCost.Text = "$0.00";
			this.textBoxTotalDealCost.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.textBoxTotalDealCost.Leave += new System.EventHandler(this.BussinesLogic);
			// 
			// labelTotalDealCost
			// 
			this.labelTotalDealCost.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.labelTotalDealCost.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.labelTotalDealCost.Location = new System.Drawing.Point(272, 360);
			this.labelTotalDealCost.Name = "labelTotalDealCost";
			this.labelTotalDealCost.Size = new System.Drawing.Size(96, 23);
			this.labelTotalDealCost.TabIndex = 71;
			this.labelTotalDealCost.Text = "Total Deal Costs:";
			// 
			// textBoxMaximumPurchasePrice
			// 
			this.textBoxMaximumPurchasePrice.Location = new System.Drawing.Point(264, 408);
			this.textBoxMaximumPurchasePrice.Name = "textBoxMaximumPurchasePrice";
			this.textBoxMaximumPurchasePrice.Size = new System.Drawing.Size(60, 20);
			this.textBoxMaximumPurchasePrice.TabIndex = 76;
			this.textBoxMaximumPurchasePrice.Text = "$0.00";
			this.textBoxMaximumPurchasePrice.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.textBoxMaximumPurchasePrice.Leave += new System.EventHandler(this.BussinesLogic);
			// 
			// labelMaximumPurchasePrice
			// 
			this.labelMaximumPurchasePrice.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.labelMaximumPurchasePrice.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.labelMaximumPurchasePrice.Location = new System.Drawing.Point(0, 408);
			this.labelMaximumPurchasePrice.Name = "labelMaximumPurchasePrice";
			this.labelMaximumPurchasePrice.Size = new System.Drawing.Size(200, 23);
			this.labelMaximumPurchasePrice.TabIndex = 75;
			this.labelMaximumPurchasePrice.Text = "Your MAXIMUM PURCHASE PRICE:";
			// 
			// textBoxTotalTransactionalCost
			// 
			this.textBoxTotalTransactionalCost.Location = new System.Drawing.Point(368, 128);
			this.textBoxTotalTransactionalCost.Name = "textBoxTotalTransactionalCost";
			this.textBoxTotalTransactionalCost.Size = new System.Drawing.Size(60, 20);
			this.textBoxTotalTransactionalCost.TabIndex = 33;
			this.textBoxTotalTransactionalCost.Text = "$0.0";
			this.textBoxTotalTransactionalCost.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.textBoxTotalTransactionalCost.Leave += new System.EventHandler(this.BussinesLogic);
			// 
			// labelTotalTransactionalCost
			// 
			this.labelTotalTransactionalCost.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.labelTotalTransactionalCost.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.labelTotalTransactionalCost.Location = new System.Drawing.Point(224, 128);
			this.labelTotalTransactionalCost.Name = "labelTotalTransactionalCost";
			this.labelTotalTransactionalCost.Size = new System.Drawing.Size(144, 23);
			this.labelTotalTransactionalCost.TabIndex = 32;
			this.labelTotalTransactionalCost.Text = "Total Transactional Costs:";
			// 
			// labelMaxAcquisitionPrice
			// 
			this.labelMaxAcquisitionPrice.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.labelMaxAcquisitionPrice.Location = new System.Drawing.Point(520, 40);
			this.labelMaxAcquisitionPrice.Name = "labelMaxAcquisitionPrice";
			this.labelMaxAcquisitionPrice.Size = new System.Drawing.Size(120, 23);
			this.labelMaxAcquisitionPrice.TabIndex = 11;
			this.labelMaxAcquisitionPrice.Text = "Max Acquisition Price:";
			// 
			// textBoxMaxAcquisitionPrice
			// 
			this.textBoxMaxAcquisitionPrice.Location = new System.Drawing.Point(640, 40);
			this.textBoxMaxAcquisitionPrice.Name = "textBoxMaxAcquisitionPrice";
			this.textBoxMaxAcquisitionPrice.Size = new System.Drawing.Size(50, 20);
			this.textBoxMaxAcquisitionPrice.TabIndex = 12;
			this.textBoxMaxAcquisitionPrice.Text = "$0.0";
			this.textBoxMaxAcquisitionPrice.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.textBoxMaxAcquisitionPrice.Leave += new System.EventHandler(this.BussinesLogic);
			// 
			// textBoxDesiredProfitInBucks
			// 
			this.textBoxDesiredProfitInBucks.Enabled = false;
			this.textBoxDesiredProfitInBucks.Location = new System.Drawing.Point(440, 40);
			this.textBoxDesiredProfitInBucks.Name = "textBoxDesiredProfitInBucks";
			this.textBoxDesiredProfitInBucks.Size = new System.Drawing.Size(80, 20);
			this.textBoxDesiredProfitInBucks.TabIndex = 10;
			this.textBoxDesiredProfitInBucks.Text = "$0.00";
			this.textBoxDesiredProfitInBucks.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.textBoxDesiredProfitInBucks.Leave += new System.EventHandler(this.BussinesLogic);
			// 
			// textBoxlDesiredProfitInPercentage
			// 
			this.textBoxlDesiredProfitInPercentage.Location = new System.Drawing.Point(440, 16);
			this.textBoxlDesiredProfitInPercentage.Name = "textBoxlDesiredProfitInPercentage";
			this.textBoxlDesiredProfitInPercentage.Size = new System.Drawing.Size(40, 20);
			this.textBoxlDesiredProfitInPercentage.TabIndex = 4;
			this.textBoxlDesiredProfitInPercentage.Text = "0%";
			this.textBoxlDesiredProfitInPercentage.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.textBoxlDesiredProfitInPercentage.Leave += new System.EventHandler(this.BussinesLogic);
			// 
			// labelDesiredProfitInPercentage
			// 
			this.labelDesiredProfitInPercentage.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.labelDesiredProfitInPercentage.Location = new System.Drawing.Point(344, 16);
			this.labelDesiredProfitInPercentage.Name = "labelDesiredProfitInPercentage";
			this.labelDesiredProfitInPercentage.Size = new System.Drawing.Size(104, 23);
			this.labelDesiredProfitInPercentage.TabIndex = 3;
			this.labelDesiredProfitInPercentage.Text = "Desired Profit in %:";
			// 
			// labelDesiredProfitInBucks
			// 
			this.labelDesiredProfitInBucks.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.labelDesiredProfitInBucks.Location = new System.Drawing.Point(344, 40);
			this.labelDesiredProfitInBucks.Name = "labelDesiredProfitInBucks";
			this.labelDesiredProfitInBucks.Size = new System.Drawing.Size(104, 23);
			this.labelDesiredProfitInBucks.TabIndex = 9;
			this.labelDesiredProfitInBucks.Text = "Desired Profit in $:";
			// 
			// buttonBreakDownBoxSellingFees
			// 
			this.buttonBreakDownBoxSellingFees.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.buttonBreakDownBoxSellingFees.Image = ((System.Drawing.Image)(resources.GetObject("buttonBreakDownBoxSellingFees.Image")));
			this.buttonBreakDownBoxSellingFees.Location = new System.Drawing.Point(168, 96);
			this.buttonBreakDownBoxSellingFees.Name = "buttonBreakDownBoxSellingFees";
			this.buttonBreakDownBoxSellingFees.Size = new System.Drawing.Size(24, 23);
			this.buttonBreakDownBoxSellingFees.TabIndex = 25;
			this.buttonBreakDownBoxSellingFees.Click += new System.EventHandler(this.buttonBreakDownBoxSellingFees_Click);
			// 
			// buttonBreakDownHousePreparationCosts
			// 
			this.buttonBreakDownHousePreparationCosts.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.buttonBreakDownHousePreparationCosts.Image = ((System.Drawing.Image)(resources.GetObject("buttonBreakDownHousePreparationCosts.Image")));
			this.buttonBreakDownHousePreparationCosts.Location = new System.Drawing.Point(640, 72);
			this.buttonBreakDownHousePreparationCosts.Name = "buttonBreakDownHousePreparationCosts";
			this.buttonBreakDownHousePreparationCosts.Size = new System.Drawing.Size(24, 23);
			this.buttonBreakDownHousePreparationCosts.TabIndex = 22;
			this.buttonBreakDownHousePreparationCosts.Click += new System.EventHandler(this.buttonBreakDownHousePreparationCosts_Click);
			// 
			// buttonBreakDownClosingCostsAtPurchase
			// 
			this.buttonBreakDownClosingCostsAtPurchase.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.buttonBreakDownClosingCostsAtPurchase.Image = ((System.Drawing.Image)(resources.GetObject("buttonBreakDownClosingCostsAtPurchase.Image")));
			this.buttonBreakDownClosingCostsAtPurchase.Location = new System.Drawing.Point(408, 72);
			this.buttonBreakDownClosingCostsAtPurchase.Name = "buttonBreakDownClosingCostsAtPurchase";
			this.buttonBreakDownClosingCostsAtPurchase.Size = new System.Drawing.Size(24, 23);
			this.buttonBreakDownClosingCostsAtPurchase.TabIndex = 19;
			this.buttonBreakDownClosingCostsAtPurchase.Click += new System.EventHandler(this.buttonBreakDownClosingCostsAtPurchase_Click);
			// 
			// buttonBreakDownClosingCostsAtResale
			// 
			this.buttonBreakDownClosingCostsAtResale.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.buttonBreakDownClosingCostsAtResale.Image = ((System.Drawing.Image)(resources.GetObject("buttonBreakDownClosingCostsAtResale.Image")));
			this.buttonBreakDownClosingCostsAtResale.Location = new System.Drawing.Point(408, 96);
			this.buttonBreakDownClosingCostsAtResale.Name = "buttonBreakDownClosingCostsAtResale";
			this.buttonBreakDownClosingCostsAtResale.Size = new System.Drawing.Size(24, 23);
			this.buttonBreakDownClosingCostsAtResale.TabIndex = 28;
			this.buttonBreakDownClosingCostsAtResale.Click += new System.EventHandler(this.buttonBreakDownClosingCostsAtResale_Click);
			// 
			// buttonBreakDownMiscellaneousCosts
			// 
			this.buttonBreakDownMiscellaneousCosts.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.buttonBreakDownMiscellaneousCosts.Image = ((System.Drawing.Image)(resources.GetObject("buttonBreakDownMiscellaneousCosts.Image")));
			this.buttonBreakDownMiscellaneousCosts.Location = new System.Drawing.Point(640, 96);
			this.buttonBreakDownMiscellaneousCosts.Name = "buttonBreakDownMiscellaneousCosts";
			this.buttonBreakDownMiscellaneousCosts.Size = new System.Drawing.Size(24, 23);
			this.buttonBreakDownMiscellaneousCosts.TabIndex = 31;
			this.buttonBreakDownMiscellaneousCosts.Click += new System.EventHandler(this.buttonBreakDownMiscellaneousCosts_Click);
			// 
			// buttonBreakDownHoldingCostsOther
			// 
			this.buttonBreakDownHoldingCostsOther.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.buttonBreakDownHoldingCostsOther.Image = ((System.Drawing.Image)(resources.GetObject("buttonBreakDownHoldingCostsOther.Image")));
			this.buttonBreakDownHoldingCostsOther.Location = new System.Drawing.Point(576, 240);
			this.buttonBreakDownHoldingCostsOther.Name = "buttonBreakDownHoldingCostsOther";
			this.buttonBreakDownHoldingCostsOther.Size = new System.Drawing.Size(24, 23);
			this.buttonBreakDownHoldingCostsOther.TabIndex = 55;
			this.buttonBreakDownHoldingCostsOther.Click += new System.EventHandler(this.buttonBreakDownHoldingCostsOther_Click);
			// 
			// textBoxTransactionalMurphyFactor
			// 
			this.textBoxTransactionalMurphyFactor.Location = new System.Drawing.Point(552, 128);
			this.textBoxTransactionalMurphyFactor.Name = "textBoxTransactionalMurphyFactor";
			this.textBoxTransactionalMurphyFactor.Size = new System.Drawing.Size(40, 20);
			this.textBoxTransactionalMurphyFactor.TabIndex = 35;
			this.textBoxTransactionalMurphyFactor.Text = "0%";
			this.textBoxTransactionalMurphyFactor.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.textBoxTransactionalMurphyFactor.Leave += new System.EventHandler(this.BussinesLogic);
			// 
			// labelTransactionalMurphyFactor
			// 
			this.labelTransactionalMurphyFactor.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.labelTransactionalMurphyFactor.Location = new System.Drawing.Point(472, 128);
			this.labelTransactionalMurphyFactor.Name = "labelTransactionalMurphyFactor";
			this.labelTransactionalMurphyFactor.Size = new System.Drawing.Size(80, 23);
			this.labelTransactionalMurphyFactor.TabIndex = 34;
			this.labelTransactionalMurphyFactor.Text = "Murphy Factor:";
			// 
			// groupBoxPaymentsAddedToPayoff
			// 
			this.groupBoxPaymentsAddedToPayoff.Controls.Add(this.radioButton1);
			this.groupBoxPaymentsAddedToPayoff.Controls.Add(this.radioButton2);
			this.groupBoxPaymentsAddedToPayoff.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.groupBoxPaymentsAddedToPayoff.Location = new System.Drawing.Point(344, 152);
			this.groupBoxPaymentsAddedToPayoff.Name = "groupBoxPaymentsAddedToPayoff";
			this.groupBoxPaymentsAddedToPayoff.Size = new System.Drawing.Size(272, 40);
			this.groupBoxPaymentsAddedToPayoff.TabIndex = 38;
			this.groupBoxPaymentsAddedToPayoff.TabStop = false;
			this.groupBoxPaymentsAddedToPayoff.Text = "Will the Payments be added to Your total Payoff?";
			// 
			// radioButton1
			// 
			this.radioButton1.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.radioButton1.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.radioButton1.Location = new System.Drawing.Point(80, 16);
			this.radioButton1.Name = "radioButton1";
			this.radioButton1.Size = new System.Drawing.Size(168, 16);
			this.radioButton1.TabIndex = 1;
			this.radioButton1.Text = "No (seller will make Payments)";
			// 
			// radioButton2
			// 
			this.radioButton2.Checked = true;
			this.radioButton2.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.radioButton2.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.radioButton2.Location = new System.Drawing.Point(8, 16);
			this.radioButton2.Name = "radioButton2";
			this.radioButton2.Size = new System.Drawing.Size(48, 16);
			this.radioButton2.TabIndex = 0;
			this.radioButton2.TabStop = true;
			this.radioButton2.Text = "Yes";
			// 
			// textBoxBalance1
			// 
			this.textBoxBalance1.Location = new System.Drawing.Point(80, 192);
			this.textBoxBalance1.Name = "textBoxBalance1";
			this.textBoxBalance1.Size = new System.Drawing.Size(60, 20);
			this.textBoxBalance1.TabIndex = 40;
			this.textBoxBalance1.Text = "$0.00";
			this.textBoxBalance1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.textBoxBalance1.Leave += new System.EventHandler(this.BussinesLogic);
			// 
			// labelBalance1
			// 
			this.labelBalance1.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.labelBalance1.Location = new System.Drawing.Point(16, 192);
			this.labelBalance1.Name = "labelBalance1";
			this.labelBalance1.Size = new System.Drawing.Size(72, 23);
			this.labelBalance1.TabIndex = 39;
			this.labelBalance1.Text = "1st Balance:";
			// 
			// textBoxMonthlyPayment1
			// 
			this.textBoxMonthlyPayment1.Location = new System.Drawing.Point(240, 192);
			this.textBoxMonthlyPayment1.Name = "textBoxMonthlyPayment1";
			this.textBoxMonthlyPayment1.Size = new System.Drawing.Size(60, 20);
			this.textBoxMonthlyPayment1.TabIndex = 42;
			this.textBoxMonthlyPayment1.Text = "$0.00";
			this.textBoxMonthlyPayment1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.textBoxMonthlyPayment1.Leave += new System.EventHandler(this.BussinesLogic);
			// 
			// labelMonthlyPayment1
			// 
			this.labelMonthlyPayment1.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.labelMonthlyPayment1.Location = new System.Drawing.Point(152, 192);
			this.labelMonthlyPayment1.Name = "labelMonthlyPayment1";
			this.labelMonthlyPayment1.Size = new System.Drawing.Size(96, 23);
			this.labelMonthlyPayment1.TabIndex = 41;
			this.labelMonthlyPayment1.Text = "Monthly Payment:";
			// 
			// textBoxMonthlyPayment2
			// 
			this.textBoxMonthlyPayment2.Location = new System.Drawing.Point(240, 216);
			this.textBoxMonthlyPayment2.Name = "textBoxMonthlyPayment2";
			this.textBoxMonthlyPayment2.Size = new System.Drawing.Size(60, 20);
			this.textBoxMonthlyPayment2.TabIndex = 50;
			this.textBoxMonthlyPayment2.Text = "$0.00";
			this.textBoxMonthlyPayment2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.textBoxMonthlyPayment2.Leave += new System.EventHandler(this.BussinesLogic);
			// 
			// labelMonthlyPayment2
			// 
			this.labelMonthlyPayment2.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.labelMonthlyPayment2.Location = new System.Drawing.Point(152, 216);
			this.labelMonthlyPayment2.Name = "labelMonthlyPayment2";
			this.labelMonthlyPayment2.Size = new System.Drawing.Size(96, 23);
			this.labelMonthlyPayment2.TabIndex = 49;
			this.labelMonthlyPayment2.Text = "Monthly Payment:";
			// 
			// textBoxBalance2
			// 
			this.textBoxBalance2.Location = new System.Drawing.Point(80, 216);
			this.textBoxBalance2.Name = "textBoxBalance2";
			this.textBoxBalance2.Size = new System.Drawing.Size(60, 20);
			this.textBoxBalance2.TabIndex = 48;
			this.textBoxBalance2.Text = "$0.00";
			this.textBoxBalance2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.textBoxBalance2.Leave += new System.EventHandler(this.BussinesLogic);
			// 
			// labelBalance2
			// 
			this.labelBalance2.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.labelBalance2.Location = new System.Drawing.Point(16, 216);
			this.labelBalance2.Name = "labelBalance2";
			this.labelBalance2.Size = new System.Drawing.Size(72, 23);
			this.labelBalance2.TabIndex = 47;
			this.labelBalance2.Text = "2nd Balance:";
			// 
			// textBoxMonthlyPayment3
			// 
			this.textBoxMonthlyPayment3.Location = new System.Drawing.Point(568, 200);
			this.textBoxMonthlyPayment3.Name = "textBoxMonthlyPayment3";
			this.textBoxMonthlyPayment3.Size = new System.Drawing.Size(60, 20);
			this.textBoxMonthlyPayment3.TabIndex = 46;
			this.textBoxMonthlyPayment3.Text = "$0.00";
			this.textBoxMonthlyPayment3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.textBoxMonthlyPayment3.Leave += new System.EventHandler(this.BussinesLogic);
			// 
			// labelMonthlyPayment3
			// 
			this.labelMonthlyPayment3.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.labelMonthlyPayment3.Location = new System.Drawing.Point(480, 200);
			this.labelMonthlyPayment3.Name = "labelMonthlyPayment3";
			this.labelMonthlyPayment3.Size = new System.Drawing.Size(96, 23);
			this.labelMonthlyPayment3.TabIndex = 45;
			this.labelMonthlyPayment3.Text = "Monthly Payment:";
			// 
			// textBoxBalance3
			// 
			this.textBoxBalance3.Location = new System.Drawing.Point(408, 200);
			this.textBoxBalance3.Name = "textBoxBalance3";
			this.textBoxBalance3.Size = new System.Drawing.Size(60, 20);
			this.textBoxBalance3.TabIndex = 44;
			this.textBoxBalance3.Text = "$0.00";
			this.textBoxBalance3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.textBoxBalance3.Leave += new System.EventHandler(this.BussinesLogic);
			// 
			// labelBalance3
			// 
			this.labelBalance3.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.labelBalance3.Location = new System.Drawing.Point(344, 200);
			this.labelBalance3.Name = "labelBalance3";
			this.labelBalance3.Size = new System.Drawing.Size(72, 23);
			this.labelBalance3.TabIndex = 43;
			this.labelBalance3.Text = "3rd Balance:";
			// 
			// textBoxForeclosureReinstatementAmount
			// 
			this.textBoxForeclosureReinstatementAmount.Location = new System.Drawing.Point(200, 336);
			this.textBoxForeclosureReinstatementAmount.Name = "textBoxForeclosureReinstatementAmount";
			this.textBoxForeclosureReinstatementAmount.Size = new System.Drawing.Size(60, 20);
			this.textBoxForeclosureReinstatementAmount.TabIndex = 70;
			this.textBoxForeclosureReinstatementAmount.Text = "$0.00";
			this.textBoxForeclosureReinstatementAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.textBoxForeclosureReinstatementAmount.Leave += new System.EventHandler(this.BussinesLogic);
			// 
			// labelForeclosureReinstatementAmount
			// 
			this.labelForeclosureReinstatementAmount.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.labelForeclosureReinstatementAmount.Location = new System.Drawing.Point(16, 336);
			this.labelForeclosureReinstatementAmount.Name = "labelForeclosureReinstatementAmount";
			this.labelForeclosureReinstatementAmount.Size = new System.Drawing.Size(192, 23);
			this.labelForeclosureReinstatementAmount.TabIndex = 69;
			this.labelForeclosureReinstatementAmount.Text = "Foreclosure Reinstatement Amount:";
			// 
			// textBoxTotalAcquisitionDealCost
			// 
			this.textBoxTotalAcquisitionDealCost.Location = new System.Drawing.Point(264, 384);
			this.textBoxTotalAcquisitionDealCost.Name = "textBoxTotalAcquisitionDealCost";
			this.textBoxTotalAcquisitionDealCost.Size = new System.Drawing.Size(60, 20);
			this.textBoxTotalAcquisitionDealCost.TabIndex = 74;
			this.textBoxTotalAcquisitionDealCost.Text = "$0.0";
			this.textBoxTotalAcquisitionDealCost.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.textBoxTotalAcquisitionDealCost.Leave += new System.EventHandler(this.BussinesLogic);
			// 
			// labelTotalAcquisitionDealCost
			// 
			this.labelTotalAcquisitionDealCost.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.labelTotalAcquisitionDealCost.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.labelTotalAcquisitionDealCost.Location = new System.Drawing.Point(0, 384);
			this.labelTotalAcquisitionDealCost.Name = "labelTotalAcquisitionDealCost";
			this.labelTotalAcquisitionDealCost.Size = new System.Drawing.Size(272, 23);
			this.labelTotalAcquisitionDealCost.TabIndex = 73;
			this.labelTotalAcquisitionDealCost.Text = "TOTAL ACQUISITION COST OF DOING DEAL::";
			// 
			// groupBox2
			// 
			this.groupBox2.Controls.Add(this.radioButton3);
			this.groupBox2.Controls.Add(this.radioButton4);
			this.groupBox2.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.groupBox2.Location = new System.Drawing.Point(160, 432);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(296, 40);
			this.groupBox2.TabIndex = 78;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "Do You need to reinstate the Loans before the resale?";
			// 
			// radioButton3
			// 
			this.radioButton3.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.radioButton3.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.radioButton3.Location = new System.Drawing.Point(80, 16);
			this.radioButton3.Name = "radioButton3";
			this.radioButton3.Size = new System.Drawing.Size(40, 16);
			this.radioButton3.TabIndex = 1;
			this.radioButton3.Text = "No";
			// 
			// radioButton4
			// 
			this.radioButton4.Checked = true;
			this.radioButton4.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.radioButton4.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.radioButton4.Location = new System.Drawing.Point(8, 16);
			this.radioButton4.Name = "radioButton4";
			this.radioButton4.Size = new System.Drawing.Size(48, 16);
			this.radioButton4.TabIndex = 0;
			this.radioButton4.TabStop = true;
			this.radioButton4.Text = "Yes";
			// 
			// labelCashRequirements
			// 
			this.labelCashRequirements.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.labelCashRequirements.Location = new System.Drawing.Point(24, 440);
			this.labelCashRequirements.Name = "labelCashRequirements";
			this.labelCashRequirements.Size = new System.Drawing.Size(136, 23);
			this.labelCashRequirements.TabIndex = 77;
			this.labelCashRequirements.Text = "CASH REQUIREMENTS:";
			// 
			// textBoxNeededToCoverTheCosts
			// 
			this.textBoxNeededToCoverTheCosts.Location = new System.Drawing.Point(632, 440);
			this.textBoxNeededToCoverTheCosts.Name = "textBoxNeededToCoverTheCosts";
			this.textBoxNeededToCoverTheCosts.Size = new System.Drawing.Size(60, 20);
			this.textBoxNeededToCoverTheCosts.TabIndex = 81;
			this.textBoxNeededToCoverTheCosts.Text = "$0.0";
			this.textBoxNeededToCoverTheCosts.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.textBoxNeededToCoverTheCosts.Leave += new System.EventHandler(this.BussinesLogic);
			// 
			// labelCashNeededToCoverTheCosts
			// 
			this.labelCashNeededToCoverTheCosts.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.labelCashNeededToCoverTheCosts.Location = new System.Drawing.Point(472, 440);
			this.labelCashNeededToCoverTheCosts.Name = "labelCashNeededToCoverTheCosts";
			this.labelCashNeededToCoverTheCosts.Size = new System.Drawing.Size(152, 23);
			this.labelCashNeededToCoverTheCosts.TabIndex = 79;
			this.labelCashNeededToCoverTheCosts.Text = "Needed To Cover The Costs:";
			// 
			// labelUntilGettingTheCheck
			// 
			this.labelUntilGettingTheCheck.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.labelUntilGettingTheCheck.Location = new System.Drawing.Point(488, 456);
			this.labelUntilGettingTheCheck.Name = "labelUntilGettingTheCheck";
			this.labelUntilGettingTheCheck.Size = new System.Drawing.Size(160, 23);
			this.labelUntilGettingTheCheck.TabIndex = 80;
			this.labelUntilGettingTheCheck.Text = "(Until GETTING THE CHECK)";
			// 
			// pictureBox1
			// 
			this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
			this.pictureBox1.Location = new System.Drawing.Point(672, 0);
			this.pictureBox1.Name = "pictureBox1";
			this.pictureBox1.Size = new System.Drawing.Size(16, 16);
			this.pictureBox1.TabIndex = 84;
			this.pictureBox1.TabStop = false;
			// 
			// textBoxRetailAuctionPercentage
			// 
			this.textBoxRetailAuctionPercentage.Location = new System.Drawing.Point(280, 16);
			this.textBoxRetailAuctionPercentage.Name = "textBoxRetailAuctionPercentage";
			this.textBoxRetailAuctionPercentage.Size = new System.Drawing.Size(60, 20);
			this.textBoxRetailAuctionPercentage.TabIndex = 86;
			this.textBoxRetailAuctionPercentage.Text = "0%";
			this.textBoxRetailAuctionPercentage.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// labelRetailAuctionPercentage
			// 
			this.labelRetailAuctionPercentage.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.labelRetailAuctionPercentage.Location = new System.Drawing.Point(160, 16);
			this.labelRetailAuctionPercentage.Name = "labelRetailAuctionPercentage";
			this.labelRetailAuctionPercentage.Size = new System.Drawing.Size(136, 23);
			this.labelRetailAuctionPercentage.TabIndex = 85;
			this.labelRetailAuctionPercentage.Text = "Set R/A Percentage:";
			// 
			// UserControl_Sub2RetailAuction
			// 
			this.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(210)), ((System.Byte)(216)), ((System.Byte)(240)));
			this.Controls.Add(this.textBoxRetailAuctionPercentage);
			this.Controls.Add(this.labelRetailAuctionPercentage);
			this.Controls.Add(this.pictureBox1);
			this.Controls.Add(this.textBoxNeededToCoverTheCosts);
			this.Controls.Add(this.labelCashRequirements);
			this.Controls.Add(this.textBoxTotalAcquisitionDealCost);
			this.Controls.Add(this.labelTotalAcquisitionDealCost);
			this.Controls.Add(this.textBoxForeclosureReinstatementAmount);
			this.Controls.Add(this.labelForeclosureReinstatementAmount);
			this.Controls.Add(this.textBoxMonthlyPayment3);
			this.Controls.Add(this.labelMonthlyPayment3);
			this.Controls.Add(this.textBoxBalance3);
			this.Controls.Add(this.labelBalance3);
			this.Controls.Add(this.textBoxMonthlyPayment2);
			this.Controls.Add(this.labelMonthlyPayment2);
			this.Controls.Add(this.textBoxBalance2);
			this.Controls.Add(this.labelBalance2);
			this.Controls.Add(this.textBoxMonthlyPayment1);
			this.Controls.Add(this.labelMonthlyPayment1);
			this.Controls.Add(this.textBoxBalance1);
			this.Controls.Add(this.labelBalance1);
			this.Controls.Add(this.textBoxTransactionalMurphyFactor);
			this.Controls.Add(this.labelTransactionalMurphyFactor);
			this.Controls.Add(this.buttonBreakDownHoldingCostsOther);
			this.Controls.Add(this.buttonBreakDownMiscellaneousCosts);
			this.Controls.Add(this.buttonBreakDownClosingCostsAtResale);
			this.Controls.Add(this.buttonBreakDownClosingCostsAtPurchase);
			this.Controls.Add(this.buttonBreakDownHousePreparationCosts);
			this.Controls.Add(this.buttonBreakDownBoxSellingFees);
			this.Controls.Add(this.labelMaxAcquisitionPrice);
			this.Controls.Add(this.textBoxMaxAcquisitionPrice);
			this.Controls.Add(this.textBoxDesiredProfitInBucks);
			this.Controls.Add(this.textBoxlDesiredProfitInPercentage);
			this.Controls.Add(this.labelDesiredProfitInPercentage);
			this.Controls.Add(this.labelDesiredProfitInBucks);
			this.Controls.Add(this.textBoxTotalTransactionalCost);
			this.Controls.Add(this.labelTotalTransactionalCost);
			this.Controls.Add(this.textBoxMaximumPurchasePrice);
			this.Controls.Add(this.labelMaximumPurchasePrice);
			this.Controls.Add(this.textBoxTotalDealCost);
			this.Controls.Add(this.labelTotalDealCost);
			this.Controls.Add(this.textBoxOtherEncumbrances);
			this.Controls.Add(this.labelOtherEncumbrances);
			this.Controls.Add(this.textBoxTaxesDue);
			this.Controls.Add(this.labelTaxesDue);
			this.Controls.Add(this.textBoxApproximateLoanPayoff);
			this.Controls.Add(this.labelApproximateLoanPayoff);
			this.Controls.Add(this.textBoxCashToSeller);
			this.Controls.Add(this.labelCashToSeller);
			this.Controls.Add(this.textBoxHoldingMurphyFactor);
			this.Controls.Add(this.labelHoldingMurphyFactor);
			this.Controls.Add(this.textBoxTotalHoldingCost);
			this.Controls.Add(this.labelTotalHoldingCost);
			this.Controls.Add(this.textBoxHoldingCostsOther);
			this.Controls.Add(this.labelHoldingCostsOther);
			this.Controls.Add(this.textBoxHoldingCostsMonthlyPayments);
			this.Controls.Add(this.labelHoldingCostsMonthlyPayments);
			this.Controls.Add(this.buttonBreakDownAdvertisingCosts);
			this.Controls.Add(this.textBoxMiscellaneousCosts);
			this.Controls.Add(this.labelMiscellaneousCosts);
			this.Controls.Add(this.textBoxHousePreparationCosts);
			this.Controls.Add(this.labelHousePreparationCosts);
			this.Controls.Add(this.textBoxClosingCostsAtResale);
			this.Controls.Add(this.labelClosingCostsAtResale);
			this.Controls.Add(this.textBoxClosingCostsAtPurchase);
			this.Controls.Add(this.labelClosingCostsAtPurchase);
			this.Controls.Add(this.textBoxSellingFees);
			this.Controls.Add(this.labelSellingFees);
			this.Controls.Add(this.textBoxAdvertisingCosts);
			this.Controls.Add(this.labelAdvertisingCosts);
			this.Controls.Add(this.labelDealCost);
			this.Controls.Add(this.labelHoldingCosts);
			this.Controls.Add(this.labelTransactionalCosts);
			this.Controls.Add(this.textBoxProjectedNumberOfMonths);
			this.Controls.Add(this.labelProjectedNumberOfMonths);
			this.Controls.Add(this.textBoxProjectedResalePrice);
			this.Controls.Add(this.labelProjectedResalePrice);
			this.Controls.Add(this.groupBoxExitStrategy);
			this.Controls.Add(this.groupBox1);
			this.Controls.Add(this.groupBoxPaymentsAddedToPayoff);
			this.Controls.Add(this.groupBox2);
			this.Controls.Add(this.labelUntilGettingTheCheck);
			this.Controls.Add(this.labelCashNeededToCoverTheCosts);
			this.Name = "UserControl_Sub2RetailAuction";
			this.Size = new System.Drawing.Size(696, 472);
			this.groupBoxExitStrategy.ResumeLayout(false);
			this.groupBox1.ResumeLayout(false);
			this.groupBoxPaymentsAddedToPayoff.ResumeLayout(false);
			this.groupBox2.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		private void buttonBreakDownAdvertisingCosts_Click(object sender, System.EventArgs e)
		{
			FormBreakDown fbd = new FormBreakDown();
			fbd.userControl_BreakDown.DataArrayCostItems = sub2.AdvertisingCostsItems;
			fbd.ShowDialog();
			sub2.AdvertisingCostsItems = fbd.userControl_BreakDown.DataArrayCostItems;
			sub2.AdvertisingCosts = Decimal.Parse(fbd.userControl_BreakDown.textBoxTotalCost.Text, NumberStyles.Currency);
			textBoxAdvertisingCosts.Text = fbd.userControl_BreakDown.textBoxTotalCost.Text;
			BussinesLogic(buttonBreakDownAdvertisingCosts, new EventArgs());
		}

		private void buttonBreakDownBoxSellingFees_Click(object sender, System.EventArgs e)
		{
			FormBreakDown fbd = new FormBreakDown();
			fbd.userControl_BreakDown.DataArrayCostItems = sub2.SellingFeesItems;
			fbd.ShowDialog();
			sub2.SellingFeesItems = fbd.userControl_BreakDown.DataArrayCostItems;
			sub2.SellingFees = Decimal.Parse(fbd.userControl_BreakDown.textBoxTotalCost.Text, NumberStyles.Currency);
			textBoxSellingFees.Text = fbd.userControl_BreakDown.textBoxTotalCost.Text;
			BussinesLogic(buttonBreakDownBoxSellingFees, new EventArgs());
		}

		private void buttonBreakDownClosingCostsAtPurchase_Click(object sender, System.EventArgs e)
		{
			FormBreakDown fbd = new FormBreakDown();
			fbd.userControl_BreakDown.DataArrayCostItems = sub2.ClosingCostsAtPurchaseItems;
			fbd.ShowDialog();
			sub2.ClosingCostsAtPurchaseItems = fbd.userControl_BreakDown.DataArrayCostItems;
			sub2.ClosingCostsPurcahse = Decimal.Parse(fbd.userControl_BreakDown.textBoxTotalCost.Text, NumberStyles.Currency);
			textBoxClosingCostsAtPurchase.Text = fbd.userControl_BreakDown.textBoxTotalCost.Text;
			BussinesLogic(buttonBreakDownClosingCostsAtPurchase, new EventArgs());
		}

		private void buttonBreakDownClosingCostsAtResale_Click(object sender, System.EventArgs e)
		{
			FormBreakDown fbd = new FormBreakDown();
			fbd.userControl_BreakDown.DataArrayCostItems = sub2.ClosingCostsAtResaleItems;
			fbd.ShowDialog();
			sub2.ClosingCostsAtResaleItems = fbd.userControl_BreakDown.DataArrayCostItems;
			sub2.ClsoingCostsAtResale = Decimal.Parse(fbd.userControl_BreakDown.textBoxTotalCost.Text, NumberStyles.Currency);
			textBoxClosingCostsAtResale.Text = fbd.userControl_BreakDown.textBoxTotalCost.Text;
			BussinesLogic(buttonBreakDownClosingCostsAtResale, new EventArgs());
		}

		private void buttonBreakDownHousePreparationCosts_Click(object sender, System.EventArgs e)
		{
			FormBreakDown fbd = new FormBreakDown();
			fbd.userControl_BreakDown.DataArrayCostItems = sub2.HousePreparationCostsItems;
			fbd.ShowDialog();
			sub2.HousePreparationCostsItems = fbd.userControl_BreakDown.DataArrayCostItems;
			sub2.HousePreparationCosts = Decimal.Parse(fbd.userControl_BreakDown.textBoxTotalCost.Text);
			textBoxHousePreparationCosts.Text = fbd.userControl_BreakDown.textBoxTotalCost.Text;
			BussinesLogic(buttonBreakDownHousePreparationCosts, new EventArgs());
		}

		private void buttonBreakDownMiscellaneousCosts_Click(object sender, System.EventArgs e)
		{
			FormBreakDown fbd = new FormBreakDown();
			fbd.userControl_BreakDown.DataArrayCostItems = sub2.MiscellaneousCostsItems;
			fbd.ShowDialog();
			sub2.MiscellaneousCostsItems = fbd.userControl_BreakDown.DataArrayCostItems;
			sub2.MiscellaneousCosts = Decimal.Parse(fbd.userControl_BreakDown.textBoxTotalCost.Text, NumberStyles.Currency);
			textBoxMiscellaneousCosts.Text = fbd.userControl_BreakDown.textBoxTotalCost.Text;
			BussinesLogic(buttonBreakDownMiscellaneousCosts, new EventArgs());
		}

		private void buttonBreakDownHoldingCostsOther_Click(object sender, System.EventArgs e)
		{
			FormBreakDown fbd = new FormBreakDown();
			fbd.userControl_BreakDown.DataArrayCostItems = sub2.HoldingCostsOtherItems;
			fbd.ShowDialog();
			sub2.HoldingCostsOtherItems = fbd.userControl_BreakDown.DataArrayCostItems;
			sub2.HoldingCostsOther = Decimal.Parse(fbd.userControl_BreakDown.textBoxTotalCost.Text, NumberStyles.Currency);
			textBoxHoldingCostsOther.Text = fbd.userControl_BreakDown.textBoxTotalCost.Text;
			BussinesLogic(buttonBreakDownHoldingCostsOther, new EventArgs());
		}

		public void BussinesLogic(object sender, System.EventArgs e)
		{
			TextBox tb = sender as TextBox;

			if (null != tb)			
				tb.Text = tb.Text;			
			else			
				return;
			

			if (tb.Name == "textBoxlDesiredProfitInPercentage")
			{
				decimal E = Decimal.Parse(textBoxlDesiredProfitInPercentage.Text.Replace("%",""), NumberStyles.Currency);

				if (10 > E  && business_logic_counter > 0)
				{
					string str = 
						"Warning! Your goal should be to make at least 10%! "
						+
						"Anything lower than that may turn this deal into "
						+
						"a No-Profit deal if anything goes wrong!"
						;
					MessageBox.Show(str, "Warning", MessageBoxButtons.OK);
				}
			}

			decimal A = Decimal.Parse(textBoxAdvertisingCosts.Text, NumberStyles.Currency) +
						Decimal.Parse(textBoxSellingFees.Text, NumberStyles.Currency) +
						Decimal.Parse(textBoxClosingCostsAtPurchase.Text, NumberStyles.Currency) +
						Decimal.Parse(textBoxClosingCostsAtResale.Text, NumberStyles.Currency) +
						Decimal.Parse(textBoxHousePreparationCosts.Text, NumberStyles.Currency)	+
						Decimal.Parse(textBoxMiscellaneousCosts.Text, NumberStyles.Currency);

			textBoxTotalTransactionalCost.Text = A.ToString("c");

			decimal mo = Decimal.Parse(textBoxProjectedNumberOfMonths.Text, NumberStyles.Currency);
			decimal MAP = Decimal.Parse(textBoxMaxAcquisitionPrice.Text, NumberStyles.Currency);

			decimal H1A = Decimal.Parse(textBoxHoldingCostsMonthlyPayments.Text, NumberStyles.Currency);
			decimal H1B = Decimal.Parse(textBoxHoldingCostsOther.Text, NumberStyles.Currency);
			decimal B = H1A + H1B;
			textBoxTotalHoldingCost.Text = B.ToString("c");

			decimal d1 = Decimal.Parse(textBoxCashToSeller.Text, NumberStyles.Currency);
			decimal d2 = Decimal.Parse(textBoxApproximateLoanPayoff.Text, NumberStyles.Currency);
			decimal d3 = Decimal.Parse(textBoxTaxesDue.Text, NumberStyles.Currency);
			decimal d4 = Decimal.Parse(textBoxOtherEncumbrances.Text, NumberStyles.Currency);
			decimal d5 = Decimal.Parse(textBoxForeclosureReinstatementAmount.Text, NumberStyles.Currency);
			decimal C = d1 + d2 + d3 + d4 + d5; 
			textBoxTotalDealCost.Text = C.ToString("c");

			decimal MFA = Decimal.Parse(textBoxTransactionalMurphyFactor.Text.Replace("%",""), NumberStyles.Currency);
			decimal MFB = Decimal.Parse(textBoxHoldingMurphyFactor.Text.Replace("%",""), NumberStyles.Currency);

			decimal D = A * MFA / 100 + B * MFB / 100 + C;
			textBoxTotalAcquisitionDealCost.Text = D.ToString("c");

			decimal F = MAP - D;
			textBoxMaximumPurchasePrice.Text = F.ToString("c");
			business_logic_counter += 1;
						
			//SAVE RELOAD SUPPORT - BEGIN
			sub2.AuctionPercentage =  Double.Parse(textBoxRetailAuctionPercentage.Text.Replace("%",""));
			sub2.DesiredProfitPrecentual = double.Parse(textBoxlDesiredProfitInPercentage.Text.Replace("%",""), NumberStyles.Currency);			
			sub2.ProjectedNumberOfMonths = int.Parse(textBoxProjectedNumberOfMonths.Text);
			sub2.ProjectedResalePrice = decimal.Parse(textBoxProjectedResalePrice.Text, NumberStyles.Currency);
			sub2.DesiredProfit = decimal.Parse(textBoxDesiredProfitInBucks.Text, NumberStyles.Currency);
			sub2.MaxAcquisitionPrice = decimal.Parse(textBoxMaxAcquisitionPrice.Text, NumberStyles.Currency);
			sub2.AdvertisingCosts = Decimal.Parse(textBoxAdvertisingCosts.Text, NumberStyles.Currency);
			sub2.ClosingCostsPurcahse = decimal.Parse(textBoxClosingCostsAtPurchase.Text, NumberStyles.Currency);
			sub2.HousePreparationCosts = decimal.Parse(textBoxHousePreparationCosts.Text, NumberStyles.Currency);
			sub2.SellingFees = decimal.Parse(textBoxSellingFees.Text, NumberStyles.Currency);
			sub2.ClsoingCostsAtResale = decimal.Parse(textBoxClosingCostsAtResale.Text, NumberStyles.Currency);
			sub2.MiscellaneousCosts = decimal.Parse(textBoxMiscellaneousCosts.Text, NumberStyles.Currency);
			sub2.TransactionalCosts = decimal.Parse(textBoxTotalTransactionalCost.Text, NumberStyles.Currency);
			sub2.TransactoionalCostsMurphyFactor = double.Parse(textBoxTransactionalMurphyFactor.Text, NumberStyles.Currency);
			
			if (radioButtonPaymentsResaleYes.Checked)
				sub2.MakingPaymentsUntilResale = MakingPaymentsUntilResale.Yes;
			else
				sub2.MakingPaymentsUntilResale = MakingPaymentsUntilResale.No;

			if (radioButton2.Checked)
				sub2.PaymentsAddedToTotalPayoff = PaymentsAddedToTotalPayoff.Yes;
			else
				sub2.PaymentsAddedToTotalPayoff = PaymentsAddedToTotalPayoff.No;

			sub2.Balance_1st = decimal.Parse(textBoxBalance1.Text, NumberStyles.Currency);
			sub2.MonthlyPayment_1st = decimal.Parse(textBoxMonthlyPayment1.Text, NumberStyles.Currency);
			sub2.Balance_2nd = decimal.Parse(textBoxBalance2.Text, NumberStyles.Currency);
			sub2.MonthlyPayment_2nd = decimal.Parse(textBoxMonthlyPayment2.Text, NumberStyles.Currency);
			sub2.Balance_3rd = decimal.Parse(textBoxBalance3.Text, NumberStyles.Currency);						
			sub2.MonthlyPayment_3rd = decimal.Parse(textBoxMonthlyPayment3.Text, NumberStyles.Currency);

			sub2.HoldingCostsMonthlyPayments = decimal.Parse(textBoxHoldingCostsMonthlyPayments.Text, NumberStyles.Currency);
			sub2.HoldingCostsOther = decimal.Parse(textBoxHoldingCostsOther.Text, NumberStyles.Currency);
			sub2.HoldingCostsTotal = decimal.Parse(textBoxTotalHoldingCost.Text, NumberStyles.Currency); 
			sub2.HoldingCostsMurphyFactor = decimal.Parse(textBoxHoldingMurphyFactor.Text, NumberStyles.Currency); 
			sub2.CashToSeller = decimal.Parse(textBoxCashToSeller.Text, NumberStyles.Currency);
			sub2.ApproximateLoanPayoff = Decimal.Parse(textBoxApproximateLoanPayoff.Text, NumberStyles.Currency);
			sub2.TaxesDue = decimal.Parse(textBoxTaxesDue.Text, NumberStyles.Currency);
			sub2.EncumbrancesOther = decimal.Parse(textBoxOtherEncumbrances.Text, NumberStyles.Currency);
			sub2.ForeClosureReinstatementAmount = decimal.Parse(textBoxForeclosureReinstatementAmount.Text, NumberStyles.Currency);
			sub2.TotalDealCost = decimal.Parse(textBoxTotalDealCost.Text, NumberStyles.Currency);
			sub2.TotalAcquisitionDealCost = decimal.Parse(textBoxTotalAcquisitionDealCost.Text, NumberStyles.Currency);
			sub2.MaximumPurchasePrice = decimal.Parse(textBoxMaximumPurchasePrice.Text, NumberStyles.Currency);

			if (radioButton4.Checked)
				sub2.ReinstateLoansBeforeResale = ReinstateLoansBeforeResale.Yes;
			else
				sub2.ReinstateLoansBeforeResale = ReinstateLoansBeforeResale.No;

			sub2.CashNeededToCoverTheCosts = decimal.Parse(textBoxNeededToCoverTheCosts.Text, NumberStyles.Currency);

		
			/*
			    * textBoxRetailAuctionPercentage
				* textBoxlDesiredProfitInPercentage
				* textBoxProjectedNumberOfMonths
				* textBoxProjectedResalePrice
				* textBoxDesiredProfitInBucks
				* textBoxMaxAcquisitionPrice
				* textBoxAdvertisingCosts
				* textBoxClosingCostsAtPurchase
				* textBoxHousePreparationCosts
				* textBoxSellingFees
				* textBoxClosingCostsAtResale
				* textBoxMiscellaneousCosts
				* textBoxTotalTransactionalCost
				* textBoxTransactionalMurphyFactor
				* radioButtonPaymentsResaleYes
				* radioButtonPaymentsResaleNo
				* radioButton2
				* radioButton1
				* textBoxBalance1
				* textBoxMonthlyPayment1
				* textBoxBalance2
				* textBoxMonthlyPayment2
				* textBoxBalance3
				* textBoxMonthlyPayment3
				* textBoxHoldingCostsMonthlyPayments
				* textBoxHoldingCostsOther
				* textBoxTotalHoldingCost
				* textBoxHoldingMurphyFactor
				* textBoxCashToSeller
				* textBoxApproximateLoanPayoff
				* textBoxTaxesDue
				* textBoxOtherEncumbrances
				* textBoxForeclosureReinstatementAmount
				* textBoxTotalDealCost
				* textBoxTotalAcquisitionDealCost
				* textBoxMaximumPurchasePrice
				* radioButton4
				* radioButton3
				* textBoxNeededToCoverTheCosts
			 */

			//SAVE RELOAD SUPPORT - END
		}

		public Sub2 Sub2
		{
			get	{ return sub2; }
			set
			{
				sub2 = value;

				textBoxAdvertisingCosts.Text = value.AdvertisingCosts.ToString("c");
				textBoxApproximateLoanPayoff.Text = value.ApproximateLoanPayoff.ToString("c");
				textBoxRetailAuctionPercentage.Text = value.AuctionPercentage.ToString();
				textBoxBalance1.Text = value.Balance_1st.ToString("c");
				textBoxBalance2.Text = value.Balance_2nd.ToString("c");
				textBoxBalance3.Text = value.Balance_3rd.ToString("c");
				textBoxCashToSeller.Text = value.CashToSeller.ToString("c");
				textBoxClosingCostsAtPurchase.Text = value.ClosingCostsPurcahse.ToString("c");
				textBoxClosingCostsAtResale.Text = value.ClsoingCostsAtResale.ToString("c");
				textBoxDesiredProfitInBucks.Text = value.DesiredProfit.ToString("c");
				textBoxForeclosureReinstatementAmount.Text = value.ForeClosureReinstatementAmount.ToString("c");
				textBoxHoldingCostsMonthlyPayments.Text = value.HoldingCostsMonthlyPayments.ToString("c");
				textBoxHoldingCostsOther.Text = value.HoldingCostsOther.ToString("c");
				textBoxHoldingMurphyFactor.Text = value.HoldingCostsMurphyFactor.ToString();
				textBoxHousePreparationCosts.Text = value.HousePreparationCosts.ToString("c");
				textBoxlDesiredProfitInPercentage.Text = value.DesiredProfitPrecentual.ToString();
				textBoxMaxAcquisitionPrice.Text = value.MaxAcquisitionPrice.ToString("c");
				textBoxMaximumPurchasePrice.Text = value.MaximumPurchasePrice.ToString("c");
				textBoxMiscellaneousCosts.Text = value.MiscellaneousCosts.ToString("c");
				textBoxMonthlyPayment1.Text = value.MonthlyPayment_1st.ToString("c");
				textBoxMonthlyPayment2.Text = value.MonthlyPayment_2nd.ToString("c");
				textBoxMonthlyPayment3.Text = value.MonthlyPayment_3rd.ToString("c");
				textBoxNeededToCoverTheCosts.Text = value.CashNeededToCoverTheCosts.ToString("c");
				textBoxOtherEncumbrances.Text = value.EncumbrancesOther.ToString("c");
				textBoxProjectedNumberOfMonths.Text = value.ProjectedNumberOfMonths.ToString();
				textBoxProjectedResalePrice.Text = value.ProjectedResalePrice.ToString("c");
				textBoxSellingFees.Text = value.SellingFees.ToString("c");
				textBoxTaxesDue.Text = value.TaxesDue.ToString("c");
				textBoxTotalAcquisitionDealCost.Text = value.TotalAcquisitionDealCost.ToString("c");
				textBoxTotalDealCost.Text = value.TotalDealCost.ToString("c");
				textBoxTotalHoldingCost.Text = value.HoldingCostsTotal.ToString("c");
				textBoxTotalTransactionalCost.Text = value.TransactionalCosts.ToString("c");
				textBoxTransactionalMurphyFactor.Text = value.TransactoionalCostsMurphyFactor.ToString();
				
				//this.BussinesLogic(this, new EventArgs());
			}
		}

		public decimal WorthByCOMPS
		{
			set { dec_worth_by_comps = value; }
			get	{ return dec_worth_by_comps; }
		}
	}
}
