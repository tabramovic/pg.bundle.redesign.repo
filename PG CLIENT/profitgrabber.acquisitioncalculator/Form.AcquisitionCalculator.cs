using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Text;
 
namespace WindowsApplication.AcquisitionCalculator
{
	/// <summary>
	/// Summary description for Form.
	/// </summary>
	public 
		class 
		Form_AcquisitionCalculator 
		: 
		System.Windows.Forms.Form
	{
		private WindowsApplication.AcquisitionCalculator.UserControl_AcquisitionCalculator userControl_AcquisitionCalculator1;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;


		public Form_AcquisitionCalculator()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			return;
		}
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(Form_AcquisitionCalculator));
			this.userControl_AcquisitionCalculator1 = new WindowsApplication.AcquisitionCalculator.UserControl_AcquisitionCalculator();
			this.SuspendLayout();
			// 
			// userControl_AcquisitionCalculator1
			// 
			this.userControl_AcquisitionCalculator1.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(210)), ((System.Byte)(216)), ((System.Byte)(240)));
			this.userControl_AcquisitionCalculator1.Location = new System.Drawing.Point(0, 0);
			this.userControl_AcquisitionCalculator1.Name = "userControl_AcquisitionCalculator1";
			this.userControl_AcquisitionCalculator1.Size = new System.Drawing.Size(736, 585);
			this.userControl_AcquisitionCalculator1.TabIndex = 0;
			// 
			// Form_AcquisitionCalculator
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(736, 573);
			this.Controls.Add(this.userControl_AcquisitionCalculator1);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "Form_AcquisitionCalculator";
			this.Text = "Form";
			this.Closing += new System.ComponentModel.CancelEventHandler(this.Form_AcquisitionCalculator_Closing);
			this.ResumeLayout(false);

		}
		#endregion

		private void Form_AcquisitionCalculator_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
		}
	}
}
