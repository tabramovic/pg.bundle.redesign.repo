using System;
using System.Globalization;

namespace WindowsApplication.AcquisitionCalculator
{
	/// <summary>
	/// Summary description for TextBox.
	/// </summary>
	public 
		class 
		TextBox
		:
		System.Windows.Forms.TextBox
	{
		public 
			TextBox
			(
			)
			: base()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		public
			override
			string
			Text
		{
			set
			{
				switch (this.Name)
				{
					case "textBoxProjectedNumberOfMonths":

					case "textBoxAmortization":
					{
						int init_value;
						try
						{
							init_value = Int32.Parse(value.Replace("%","").Replace("$",""));
						}
						catch (FormatException)
						{
							init_value = 0;
						}
						base.Text = init_value.ToString();
						break;
					}
					case "textBoxRetailAuctionPercentage":
					case "textBoxMurphyFactor":
					case "textBoxlDesiredProfitInPercentage":
					case "textBoxOptionorSellerReceives":
					case "textBoxOptioneeBuyerReceives":
					case "textBoxDesiredGuaranteedProfitInPercentage":
					case "textBoxDesiredProfitInPercentage":

					case "textBoxLoanPoints":
					case "textBoxLTV":
					case "textBoxInterestRate":
					case "textBoxHoldingMurphyFactor":
					case "textBoxDealMurphyFactor":
					{
						double init_value;
						try
						{
							init_value = Double.Parse(value.Replace("%",""));
						}
						catch (FormatException)
						{
							init_value = 0.0;
						}
						if (init_value > 100 || init_value < 0)
						{
							init_value = 0.0;
						}
						base.Text = init_value.ToString() + "%";
						break;
					}
					case "textBoxWorthByCOMPS":
					case "textBoxTotalOwed":
					case "textBoxWorthBySeller":
					case "textBoxTotalMonthly":
					case "textBoxForeclosing":
					case "textBoxAsking":
					case "textBoxAskingAmount":

					case "textBoxProjectedResalePrice":
					case "textBoxAdvertisingCosts":
					case "textBoxClosingCostsAtPurchase":
					case "textBoxHousePreparationCosts":
					case "textBoxSellingFees":
					case "textBoxClosingCostsAtResale":
					case "textBoxMiscellaneousCosts":
					case "textBoxTotalTransactionalCosts":
					case "textBoxDesiredProfitInBucks":
					case "textBoxMaximumPurchasePrice":
					case "textBoxAgreedReservePrice":
					case "textBoxDesiredGuaranteedProfit":
					case "textBoxDesiredGuaranteedProfit2":
					case "textBoxTotalMaxPurchasePrice":
					case "textBoxTotalProfit":
					case "textBoxSellersOptionrsTotalProfit":

					case "textBoxMaxPurchaseOptionPrice_blue":
					case "textBoxMaxPurchaseOptionPrice_red":

					case "textBoxOtherCosts":
					case "textBoxMonthlyPayment":
					case "textBoxTotalFinancingCosts":
					case "textBoxHoldingCostsMonthlyPayments":
					case "textBoxTotalHoldingCost":
					case "textBoxHoldingCostsOther":
					case "textBoxOtherCashInDeal":
					case "textBoxApproximateLoanPayoff":
					case "textBoxTaxesDue":
					case "textBoxOtherEncumbrances":
					case "textBoxTotalDealCost":
					case "textBoxTotalAcquistionCostOfDeal":
					case "textBoxLoanAmount":

					case "textBoxBalance1":
					case "textBoxBalance2":
					case "textBoxBalance3":
					case "textBoxMonthlyPayment1":
					case "textBoxMonthlyPayment2":
					case "textBoxMonthlyPayment3":
					case "textBoxCashToSeller":
					case "textBoxForeclosureReinstatementAmount":
					{
						decimal init_value;
						try
						{
							init_value = Decimal.Parse(value, NumberStyles.Currency);
						}
						catch (FormatException)
						{
							init_value = 0;
						}
						base.Text = init_value.ToString("c");
						break;
					}
					case "textBoxPropertyAddress":
					default:
					{
						base.Text = value;
						break;
					}
				}
			}
			get
			{
				return base.Text.ToString();
			}
		}
	}
}
