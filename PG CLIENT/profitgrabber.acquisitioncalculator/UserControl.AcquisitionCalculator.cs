using System;
using System.Drawing;
using System.Drawing.Printing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Globalization;
using System.IO;

//Added
using ProfitGrabber.Common;

namespace WindowsApplication.AcquisitionCalculator
{
	/// <summary>
	/// Summary description for FormAcquisitionCalculator.
	/// </summary>
	public class UserControl_AcquisitionCalculator : System.Windows.Forms.UserControl
	{
		private const string DealIQDocsFolder = "DealIQDocs";
		
		private AcquisitionCalculator.AcquistionCalculator accalc;
		string filename;

		private System.Windows.Forms.TabControl tabControl;
		private System.Windows.Forms.TabPage tabPageOptions;
		private System.Windows.Forms.TabPage tabPageAllCash;
		private System.Windows.Forms.Label labelWorthByCOMPS;
		private System.Windows.Forms.Label labelTotalOwed;
		private System.Windows.Forms.Label labelWorthBySeller;
		private System.Windows.Forms.Label labelTotalMonthly;
		private System.Windows.Forms.Label labelForeclosing;
		private System.Windows.Forms.Label labelAsking;
		private System.Windows.Forms.Label labelAskingAmount;
		private System.Windows.Forms.TabPage tabPageSub2;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Button buttonSave;
		private System.Windows.Forms.Button buttonLoad;
		private System.Windows.Forms.Label labelReinstateAmount;
		private TextBox textBoxPropertyAddress;
		private TextBox textBoxWorthByCOMPS;
		private TextBox textBoxTotalOwed;
		private TextBox textBoxWorthBySeller;
		private TextBox textBoxTotalMonthly;
		private TextBox textBoxReinstateAmount;
		private System.Windows.Forms.TextBox textBoxForeclosing;
		private System.Windows.Forms.TextBox textBoxAskingCash;
		private TextBox textBoxAskingPrice;
		private System.Windows.Forms.Button _print;
		private WindowsApplication.AcquisitionCalculator.UserControl_OptionsRetailAuction userControl_OptionsRetailAuction1;
		private WindowsApplication.AcquisitionCalculator.UserControl_AllCashRetailAuction userControl_AllCashRetailAuction1;
		private WindowsApplication.AcquisitionCalculator.UserControl_Sub2 userControl_Sub21;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public UserControl_AcquisitionCalculator()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			
			textBoxPropertyAddress.Text = "Address";
			textBoxPropertyAddress.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;

			accalc = new AcquistionCalculator();
			accalc.PropertyAddress = textBoxPropertyAddress.Text;

			accalc.PropertyAddress = textBoxPropertyAddress.Text;
			accalc.Options = this.Options;
			accalc.AllCash = this.AllCash;
			accalc.Sub2 = this.Sub2;

			filename = GenerateFileNameFromPropertyAddress();

			_print.Click += new EventHandler(On_Print_Click);
		}

		~UserControl_AcquisitionCalculator()
		{
			accalc.Save(filename);
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			accalc.Save(filename);

			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.tabControl = new System.Windows.Forms.TabControl();
			this.tabPageOptions = new System.Windows.Forms.TabPage();
			this.userControl_OptionsRetailAuction1 = new WindowsApplication.AcquisitionCalculator.UserControl_OptionsRetailAuction();
			this.tabPageAllCash = new System.Windows.Forms.TabPage();
			this.userControl_AllCashRetailAuction1 = new WindowsApplication.AcquisitionCalculator.UserControl_AllCashRetailAuction();
			this.tabPageSub2 = new System.Windows.Forms.TabPage();
			this.labelWorthByCOMPS = new System.Windows.Forms.Label();
			this.labelTotalOwed = new System.Windows.Forms.Label();
			this.labelWorthBySeller = new System.Windows.Forms.Label();
			this.labelTotalMonthly = new System.Windows.Forms.Label();
			this.labelForeclosing = new System.Windows.Forms.Label();
			this.labelAsking = new System.Windows.Forms.Label();
			this.labelAskingAmount = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.buttonSave = new System.Windows.Forms.Button();
			this.buttonLoad = new System.Windows.Forms.Button();
			this.labelReinstateAmount = new System.Windows.Forms.Label();
			this.textBoxPropertyAddress = new WindowsApplication.AcquisitionCalculator.TextBox();
			this.textBoxWorthByCOMPS = new WindowsApplication.AcquisitionCalculator.TextBox();
			this.textBoxTotalOwed = new WindowsApplication.AcquisitionCalculator.TextBox();
			this.textBoxWorthBySeller = new WindowsApplication.AcquisitionCalculator.TextBox();
			this.textBoxTotalMonthly = new WindowsApplication.AcquisitionCalculator.TextBox();
			this.textBoxReinstateAmount = new WindowsApplication.AcquisitionCalculator.TextBox();
			this.textBoxForeclosing = new System.Windows.Forms.TextBox();
			this.textBoxAskingCash = new System.Windows.Forms.TextBox();
			this.textBoxAskingPrice = new WindowsApplication.AcquisitionCalculator.TextBox();
			this._print = new System.Windows.Forms.Button();
			this.userControl_Sub21 = new WindowsApplication.AcquisitionCalculator.UserControl_Sub2();
			this.tabControl.SuspendLayout();
			this.tabPageOptions.SuspendLayout();
			this.tabPageAllCash.SuspendLayout();
			this.tabPageSub2.SuspendLayout();
			this.SuspendLayout();
			// 
			// tabControl
			// 
			this.tabControl.Controls.Add(this.tabPageOptions);
			this.tabControl.Controls.Add(this.tabPageAllCash);
			this.tabControl.Controls.Add(this.tabPageSub2);
			this.tabControl.Location = new System.Drawing.Point(0, 80);
			this.tabControl.Name = "tabControl";
			this.tabControl.SelectedIndex = 0;
			this.tabControl.Size = new System.Drawing.Size(912, 616);
			this.tabControl.TabIndex = 20;
			// 
			// tabPageOptions
			// 
			this.tabPageOptions.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(210)), ((System.Byte)(216)), ((System.Byte)(240)));
			this.tabPageOptions.Controls.Add(this.userControl_OptionsRetailAuction1);
			this.tabPageOptions.Location = new System.Drawing.Point(4, 25);
			this.tabPageOptions.Name = "tabPageOptions";
			this.tabPageOptions.Size = new System.Drawing.Size(904, 587);
			this.tabPageOptions.TabIndex = 0;
			this.tabPageOptions.Text = "Options Retail/Auction";
			// 
			// userControl_OptionsRetailAuction1
			// 
			this.userControl_OptionsRetailAuction1.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(210)), ((System.Byte)(216)), ((System.Byte)(240)));
			this.userControl_OptionsRetailAuction1.Location = new System.Drawing.Point(2, 1);
			this.userControl_OptionsRetailAuction1.Name = "userControl_OptionsRetailAuction1";
			this.userControl_OptionsRetailAuction1.Size = new System.Drawing.Size(900, 580);
			this.userControl_OptionsRetailAuction1.TabIndex = 0;
			this.userControl_OptionsRetailAuction1.WorthByCOMPS = new System.Decimal(new int[] {
																								   0,
																								   0,
																								   0,
																								   0});
			// 
			// tabPageAllCash
			// 
			this.tabPageAllCash.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(210)), ((System.Byte)(216)), ((System.Byte)(240)));
			this.tabPageAllCash.Controls.Add(this.userControl_AllCashRetailAuction1);
			this.tabPageAllCash.Location = new System.Drawing.Point(4, 25);
			this.tabPageAllCash.Name = "tabPageAllCash";
			this.tabPageAllCash.Size = new System.Drawing.Size(904, 587);
			this.tabPageAllCash.TabIndex = 1;
			this.tabPageAllCash.Text = "All Cash Retail/Auction";
			// 
			// userControl_AllCashRetailAuction1
			// 
			this.userControl_AllCashRetailAuction1.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(210)), ((System.Byte)(216)), ((System.Byte)(240)));
			this.userControl_AllCashRetailAuction1.Location = new System.Drawing.Point(0, 0);
			this.userControl_AllCashRetailAuction1.Name = "userControl_AllCashRetailAuction1";
			this.userControl_AllCashRetailAuction1.Size = new System.Drawing.Size(900, 580);
			this.userControl_AllCashRetailAuction1.TabIndex = 0;
			this.userControl_AllCashRetailAuction1.WorthByCOMPS = new System.Decimal(new int[] {
																								   0,
																								   0,
																								   0,
																								   0});
			// 
			// tabPageSub2
			// 
			this.tabPageSub2.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(210)), ((System.Byte)(216)), ((System.Byte)(240)));
			this.tabPageSub2.Controls.Add(this.userControl_Sub21);
			this.tabPageSub2.Location = new System.Drawing.Point(4, 25);
			this.tabPageSub2.Name = "tabPageSub2";
			this.tabPageSub2.Size = new System.Drawing.Size(904, 587);
			this.tabPageSub2.TabIndex = 2;
			this.tabPageSub2.Text = "Sub2 Retail/Auction";
			// 
			// labelWorthByCOMPS
			// 
			this.labelWorthByCOMPS.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.labelWorthByCOMPS.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.labelWorthByCOMPS.Location = new System.Drawing.Point(0, 32);
			this.labelWorthByCOMPS.Name = "labelWorthByCOMPS";
			this.labelWorthByCOMPS.Size = new System.Drawing.Size(144, 23);
			this.labelWorthByCOMPS.TabIndex = 2;
			this.labelWorthByCOMPS.Text = "Worth by COMPS:";
			// 
			// labelTotalOwed
			// 
			this.labelTotalOwed.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.labelTotalOwed.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.labelTotalOwed.Location = new System.Drawing.Point(0, 56);
			this.labelTotalOwed.Name = "labelTotalOwed";
			this.labelTotalOwed.Size = new System.Drawing.Size(136, 23);
			this.labelTotalOwed.TabIndex = 10;
			this.labelTotalOwed.Text = "Total Owed:";
			// 
			// labelWorthBySeller
			// 
			this.labelWorthBySeller.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.labelWorthBySeller.Location = new System.Drawing.Point(256, 32);
			this.labelWorthBySeller.Name = "labelWorthBySeller";
			this.labelWorthBySeller.Size = new System.Drawing.Size(107, 23);
			this.labelWorthBySeller.TabIndex = 4;
			this.labelWorthBySeller.Text = "Worth by Seller:";
			// 
			// labelTotalMonthly
			// 
			this.labelTotalMonthly.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.labelTotalMonthly.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.labelTotalMonthly.Location = new System.Drawing.Point(256, 56);
			this.labelTotalMonthly.Name = "labelTotalMonthly";
			this.labelTotalMonthly.Size = new System.Drawing.Size(107, 23);
			this.labelTotalMonthly.TabIndex = 12;
			this.labelTotalMonthly.Text = "Total Monthly:";
			// 
			// labelForeclosing
			// 
			this.labelForeclosing.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.labelForeclosing.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.labelForeclosing.Location = new System.Drawing.Point(480, 56);
			this.labelForeclosing.Name = "labelForeclosing";
			this.labelForeclosing.Size = new System.Drawing.Size(117, 23);
			this.labelForeclosing.TabIndex = 14;
			this.labelForeclosing.Text = "Foreclosing:";
			// 
			// labelAsking
			// 
			this.labelAsking.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.labelAsking.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.labelAsking.Location = new System.Drawing.Point(720, 56);
			this.labelAsking.Name = "labelAsking";
			this.labelAsking.Size = new System.Drawing.Size(104, 23);
			this.labelAsking.TabIndex = 16;
			this.labelAsking.Text = "Asking Price:";
			// 
			// labelAskingAmount
			// 
			this.labelAskingAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.labelAskingAmount.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.labelAskingAmount.Location = new System.Drawing.Point(720, 32);
			this.labelAskingAmount.Name = "labelAskingAmount";
			this.labelAskingAmount.Size = new System.Drawing.Size(104, 23);
			this.labelAskingAmount.TabIndex = 8;
			this.labelAskingAmount.Text = "Asking Cash:";
			// 
			// label1
			// 
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label1.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.label1.Location = new System.Drawing.Point(0, 8);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(136, 23);
			this.label1.TabIndex = 1;
			this.label1.Text = "Property Address:";
			// 
			// buttonSave
			// 
			this.buttonSave.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.buttonSave.Location = new System.Drawing.Point(696, 8);
			this.buttonSave.Name = "buttonSave";
			this.buttonSave.Size = new System.Drawing.Size(40, 23);
			this.buttonSave.TabIndex = 18;
			this.buttonSave.Text = "Save";
			this.buttonSave.Visible = false;
			this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
			// 
			// buttonLoad
			// 
			this.buttonLoad.Location = new System.Drawing.Point(744, 8);
			this.buttonLoad.Name = "buttonLoad";
			this.buttonLoad.Size = new System.Drawing.Size(40, 23);
			this.buttonLoad.TabIndex = 19;
			this.buttonLoad.Text = "Load";
			this.buttonLoad.Visible = false;
			this.buttonLoad.Click += new System.EventHandler(this.buttonLoad_Click);
			// 
			// labelReinstateAmount
			// 
			this.labelReinstateAmount.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.labelReinstateAmount.Location = new System.Drawing.Point(480, 32);
			this.labelReinstateAmount.Name = "labelReinstateAmount";
			this.labelReinstateAmount.Size = new System.Drawing.Size(117, 23);
			this.labelReinstateAmount.TabIndex = 6;
			this.labelReinstateAmount.Text = "Reinstate Amount:";
			// 
			// textBoxPropertyAddress
			// 
			this.textBoxPropertyAddress.Location = new System.Drawing.Point(144, 8);
			this.textBoxPropertyAddress.Name = "textBoxPropertyAddress";
			this.textBoxPropertyAddress.ReadOnly = true;
			this.textBoxPropertyAddress.Size = new System.Drawing.Size(536, 22);
			this.textBoxPropertyAddress.TabIndex = 21;
			this.textBoxPropertyAddress.TabStop = false;
			this.textBoxPropertyAddress.Text = "Address";
			// 
			// textBoxWorthByCOMPS
			// 
			this.textBoxWorthByCOMPS.Location = new System.Drawing.Point(144, 32);
			this.textBoxWorthByCOMPS.Name = "textBoxWorthByCOMPS";
			this.textBoxWorthByCOMPS.Size = new System.Drawing.Size(80, 22);
			this.textBoxWorthByCOMPS.TabIndex = 22;
			this.textBoxWorthByCOMPS.Text = "$0.00";
			this.textBoxWorthByCOMPS.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.textBoxWorthByCOMPS.Leave += new System.EventHandler(this.ForceBussinesLogic);
			// 
			// textBoxTotalOwed
			// 
			this.textBoxTotalOwed.Location = new System.Drawing.Point(144, 56);
			this.textBoxTotalOwed.Name = "textBoxTotalOwed";
			this.textBoxTotalOwed.ReadOnly = true;
			this.textBoxTotalOwed.Size = new System.Drawing.Size(80, 22);
			this.textBoxTotalOwed.TabIndex = 23;
			this.textBoxTotalOwed.TabStop = false;
			this.textBoxTotalOwed.Text = "$0.00";
			this.textBoxTotalOwed.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// textBoxWorthBySeller
			// 
			this.textBoxWorthBySeller.Location = new System.Drawing.Point(368, 32);
			this.textBoxWorthBySeller.Name = "textBoxWorthBySeller";
			this.textBoxWorthBySeller.ReadOnly = true;
			this.textBoxWorthBySeller.Size = new System.Drawing.Size(80, 22);
			this.textBoxWorthBySeller.TabIndex = 24;
			this.textBoxWorthBySeller.TabStop = false;
			this.textBoxWorthBySeller.Text = "$0.00";
			this.textBoxWorthBySeller.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// textBoxTotalMonthly
			// 
			this.textBoxTotalMonthly.Location = new System.Drawing.Point(368, 56);
			this.textBoxTotalMonthly.Name = "textBoxTotalMonthly";
			this.textBoxTotalMonthly.ReadOnly = true;
			this.textBoxTotalMonthly.Size = new System.Drawing.Size(80, 22);
			this.textBoxTotalMonthly.TabIndex = 25;
			this.textBoxTotalMonthly.TabStop = false;
			this.textBoxTotalMonthly.Text = "$0.00";
			this.textBoxTotalMonthly.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// textBoxReinstateAmount
			// 
			this.textBoxReinstateAmount.Location = new System.Drawing.Point(600, 32);
			this.textBoxReinstateAmount.Name = "textBoxReinstateAmount";
			this.textBoxReinstateAmount.ReadOnly = true;
			this.textBoxReinstateAmount.Size = new System.Drawing.Size(80, 22);
			this.textBoxReinstateAmount.TabIndex = 26;
			this.textBoxReinstateAmount.TabStop = false;
			this.textBoxReinstateAmount.Text = "$0.00";
			this.textBoxReinstateAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// textBoxForeclosing
			// 
			this.textBoxForeclosing.Location = new System.Drawing.Point(600, 56);
			this.textBoxForeclosing.Name = "textBoxForeclosing";
			this.textBoxForeclosing.ReadOnly = true;
			this.textBoxForeclosing.Size = new System.Drawing.Size(80, 22);
			this.textBoxForeclosing.TabIndex = 27;
			this.textBoxForeclosing.TabStop = false;
			this.textBoxForeclosing.Text = "� 0,00";
			this.textBoxForeclosing.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// textBoxAskingCash
			// 
			this.textBoxAskingCash.Location = new System.Drawing.Point(824, 32);
			this.textBoxAskingCash.Name = "textBoxAskingCash";
			this.textBoxAskingCash.ReadOnly = true;
			this.textBoxAskingCash.Size = new System.Drawing.Size(80, 22);
			this.textBoxAskingCash.TabIndex = 28;
			this.textBoxAskingCash.TabStop = false;
			this.textBoxAskingCash.Text = "$0.00";
			this.textBoxAskingCash.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// textBoxAskingPrice
			// 
			this.textBoxAskingPrice.Location = new System.Drawing.Point(824, 56);
			this.textBoxAskingPrice.Name = "textBoxAskingPrice";
			this.textBoxAskingPrice.ReadOnly = true;
			this.textBoxAskingPrice.Size = new System.Drawing.Size(80, 22);
			this.textBoxAskingPrice.TabIndex = 29;
			this.textBoxAskingPrice.TabStop = false;
			this.textBoxAskingPrice.Text = "$0.00";
			this.textBoxAskingPrice.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// _print
			// 
			this._print.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this._print.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this._print.Location = new System.Drawing.Point(792, 5);
			this._print.Name = "_print";
			this._print.Size = new System.Drawing.Size(120, 23);
			this._print.TabIndex = 30;
			this._print.Text = "Print Report";
			this._print.Visible = false;
			// 
			// userControl_Sub21
			// 
			this.userControl_Sub21.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(210)), ((System.Byte)(216)), ((System.Byte)(240)));
			this.userControl_Sub21.Location = new System.Drawing.Point(2, 4);
			this.userControl_Sub21.Name = "userControl_Sub21";
			this.userControl_Sub21.Size = new System.Drawing.Size(900, 580);
			this.userControl_Sub21.TabIndex = 0;
			this.userControl_Sub21.WorthByCOMPS = new System.Decimal(new int[] {
																				   0,
																				   0,
																				   0,
																				   0});
			// 
			// UserControl_AcquisitionCalculator
			// 
			this.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(210)), ((System.Byte)(216)), ((System.Byte)(240)));
			this.Controls.Add(this.textBoxPropertyAddress);
			this.Controls.Add(this._print);
			this.Controls.Add(this.textBoxAskingPrice);
			this.Controls.Add(this.textBoxAskingCash);
			this.Controls.Add(this.textBoxForeclosing);
			this.Controls.Add(this.textBoxReinstateAmount);
			this.Controls.Add(this.textBoxTotalMonthly);
			this.Controls.Add(this.textBoxWorthBySeller);
			this.Controls.Add(this.textBoxTotalOwed);
			this.Controls.Add(this.textBoxWorthByCOMPS);
			this.Controls.Add(this.labelReinstateAmount);
			this.Controls.Add(this.buttonLoad);
			this.Controls.Add(this.buttonSave);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.labelAskingAmount);
			this.Controls.Add(this.labelAsking);
			this.Controls.Add(this.labelForeclosing);
			this.Controls.Add(this.labelTotalMonthly);
			this.Controls.Add(this.labelWorthBySeller);
			this.Controls.Add(this.labelTotalOwed);
			this.Controls.Add(this.labelWorthByCOMPS);
			this.Controls.Add(this.tabControl);
			this.Name = "UserControl_AcquisitionCalculator";
			this.Size = new System.Drawing.Size(918, 704);
			this.Leave += new System.EventHandler(this.buttonSave_Click);
			this.tabControl.ResumeLayout(false);
			this.tabPageOptions.ResumeLayout(false);
			this.tabPageAllCash.ResumeLayout(false);
			this.tabPageSub2.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		private void buttonSave_Click(object sender, System.EventArgs e)
		{
			Save();								
		}

		private void buttonLoad_Click(object sender, System.EventArgs e)
		{
			OpenFileDialog openFileDialog1 = new OpenFileDialog();
 
			openFileDialog1.InitialDirectory = Environment.CurrentDirectory;
			openFileDialog1.Filter = "XML files (*.xml)|*.xml|All files (*.*)|*.*" ;
			openFileDialog1.FilterIndex = 2 ;
			openFileDialog1.RestoreDirectory = true ;
 
			if(openFileDialog1.ShowDialog() == DialogResult.OK)			
				Load(openFileDialog1.FileNames[0]);
			

			this.textBoxPropertyAddress.Text = accalc.PropertyAddress.ToString();
			this.textBoxWorthByCOMPS.Text = accalc.WorthByCOMPS.ToString("c");
			this.textBoxWorthBySeller.Text = accalc.WorthBySeller.ToString("c");
			this.textBoxReinstateAmount.Text = accalc.ReinstateAmount.ToString("c");
			this.textBoxAskingPrice.Text = accalc.AskingPrice.ToString("c");
			this.textBoxAskingCash.Text = accalc.AskingCash;// .ToString("c");
			this.textBoxForeclosing.Text = accalc.ForeClosing; //.ToString("c");
			this.textBoxTotalMonthly.Text = accalc.TotalMonthly.ToString("c");
			this.textBoxTotalOwed.Text = accalc.TotalOwed.ToString("c");

			this.userControl_OptionsRetailAuction1.Options = accalc.Options;
			this.userControl_AllCashRetailAuction1.AllCash = accalc.AllCash;
			this.userControl_Sub21.Sub2 = accalc.Sub2;
		
			return;
		}

		public Options Options
		{
			get	{ return userControl_OptionsRetailAuction1.Options;	}
		}

		public AllCash	AllCash
		{
			get	{ return userControl_AllCashRetailAuction1.AllCash;	}
		}

		public Sub2 Sub2
		{
			get { return userControl_Sub21.Sub2; }
		}

		//FORCING RECALC PROCEDURES
		private void ForceBussinesLogic (object sender, System.EventArgs e)
		{			
			//set new WorthByComps val, and force recalc procedures
			userControl_OptionsRetailAuction1.WorthByCOMPS = Decimal.Parse(textBoxWorthByCOMPS.Text, NumberStyles.Currency);
			userControl_OptionsRetailAuction1.BussinesLogic(this, new EventArgs());		

			//set new WorthByComps val, and force recalc procedures
			userControl_AllCashRetailAuction1.WorthByCOMPS = Decimal.Parse(textBoxWorthByCOMPS.Text, NumberStyles.Currency);
			userControl_AllCashRetailAuction1.BussinesLogic(this, new EventArgs());

			//set new WorthByComps val, and force recalc procedures
			userControl_Sub21.WorthByCOMPS = Decimal.Parse(textBoxWorthByCOMPS.Text, NumberStyles.Currency);
			userControl_Sub21.BussinesLogic(this, new EventArgs());		
	
			accalc.WorthByCOMPS = Decimal.Parse(textBoxWorthByCOMPS.Text, NumberStyles.Currency);
		}			

		public string PropertyAddress
		{
			set
			{
				accalc.PropertyAddress = value;
				textBoxPropertyAddress.Text = value;
				filename = GenerateFileNameFromPropertyAddress();
			}
			get { return accalc.PropertyAddress; }
		}

		public decimal WorthByCOMPS
		{
			set
			{
				accalc.WorthByCOMPS = value;
				textBoxWorthByCOMPS.Text = value.ToString("c");
			}
			get { return accalc.WorthByCOMPS; }
		}

		public decimal WorthBySeller
		{
			set
			{
				accalc.WorthBySeller = value;
				textBoxWorthBySeller.Text = value.ToString("c");
			}
			get { return accalc.WorthBySeller;	}
		}

		public decimal AskingPrice
		{
			set
			{
				accalc.AskingPrice = value;
				textBoxAskingPrice.Text = value.ToString("c");
			}
			get { return accalc.AskingPrice; }
		}

		public string AskingCash
		{
			set
			{
				accalc.AskingCash = value;
				textBoxAskingCash.Text = value.ToString();
			}
			get { return accalc.AskingCash;	}
		}

		public decimal TotalOwed
		{
			set
			{
				accalc.TotalOwed = value;
				textBoxTotalOwed.Text = value.ToString("c");
			}
			get { return accalc.TotalOwed; }
		}

		public decimal TotalMonthly
		{
			set
			{
				accalc.TotalMonthly = value;
				textBoxTotalMonthly.Text = value.ToString("c");
			}
			get { return accalc.TotalMonthly; }
		}

		public string ForeClosing
		{
			set
			{
				accalc.ForeClosing = value;
				textBoxForeclosing.Text = value.ToString();
			}
			get { return accalc.ForeClosing; }
		}

		public decimal ReinstateAmount
		{
			set
			{
				accalc.ReinstateAmount = value;
				textBoxReinstateAmount.Text = value.ToString("c");
			}
			get { return accalc.ReinstateAmount; }
		}

		public void Save (string filename)
		{
			if (filename == string.Empty)			
				filename = GenerateFileNameFromPropertyAddress();			

			accalc.Options = userControl_OptionsRetailAuction1.Options;
			accalc.AllCash = userControl_AllCashRetailAuction1.AllCash;
			accalc.Sub2 = userControl_Sub21.Sub2;

			accalc.Save(filename);			
		}

		public void	Save()
		{
			this.Save(this.filename);
		}

		public	new	void Load (string filename)
		{
			if (filename == string.Empty)
				filename = GenerateFileNameFromPropertyAddress();
			
			if (File.Exists(filename))			
				accalc.Load(filename);
			
			if (null != accalc)
			{
				userControl_OptionsRetailAuction1.WorthByCOMPS = accalc.WorthByCOMPS;								
				userControl_AllCashRetailAuction1.WorthByCOMPS = accalc.WorthByCOMPS;
				userControl_Sub21.WorthByCOMPS = accalc.WorthByCOMPS; 									
				
				userControl_OptionsRetailAuction1.Options = accalc.Options;
				userControl_AllCashRetailAuction1.AllCash = accalc.AllCash;
				userControl_Sub21.Sub2 = accalc.Sub2;	
								
				textBoxWorthByCOMPS.Text = accalc.WorthByCOMPS.ToString("c");				
			}
		}

		public new void	Load()
		{
			this.Load(this.filename);
		}

		private string GenerateFileNameFromPropertyAddress()
		{			
			if (!Directory.Exists(Application.StartupPath + @"\" + DealIQDocsFolder))			
				Directory.CreateDirectory(Application.StartupPath + @"\" + DealIQDocsFolder);
			
			filename = Application.StartupPath + @"\" + DealIQDocsFolder + @"\";
			filename += accalc.PropertyAddress.Replace(",","_").Replace("/","_").Replace("/","_").Replace(" ","_").Replace("(","_").Replace(")","_").Replace("[","_").Replace("]","_")+ ".xml";

			return filename;
		}

		private void On_Print_Click(object sender, EventArgs e)
		{
			PrintDocument doc = new PrintDocument();
			doc.PrintPage += new PrintPageEventHandler(On_Doc_PrintPage);

			PrintPreviewDialogEx previewDialog = new PrintPreviewDialogEx();								
			previewDialog.Document = doc;

			previewDialog.ShowDialog();
		}

		private void On_Doc_PrintPage(object sender, PrintPageEventArgs e)
		{
			Save();

			Font f_16B = new Font("arial", 16, FontStyle.Bold);
			Font f_12B = new Font("arial", 12, FontStyle.Bold);
			Font f_12BI = new Font("arial", 12, FontStyle.Bold | FontStyle.Italic);
			Font f_10BI = new Font("arial", 10, FontStyle.Bold | FontStyle.Italic);
			Font f_10B = new Font("arial", 10, FontStyle.Bold);
			Font f_10 = new Font("arial", 10, FontStyle.Regular);


			e.Graphics.DrawString("Deal IQ Report", f_16B, Brushes.Black, new PointF(30, 30));

			//HEADER
			e.Graphics.DrawString("Property Address:", f_10, Brushes.Black, new PointF(30, 80));
			e.Graphics.DrawString(accalc.PropertyAddress, f_10, Brushes.Black, new Point((int)(e.Graphics.MeasureString("Property Address:", f_10B).Width) + 30 + 5, 80));

			e.Graphics.DrawString("Worth By COMPS:", f_10, Brushes.Black, new PointF(30, 110));
			e.Graphics.DrawString(accalc.WorthByCOMPS.ToString("c"), f_10, Brushes.Black, new PointF(169, 110));
			e.Graphics.DrawString("Worth By Seller:", f_10, Brushes.Black, new PointF(300, 110));
			e.Graphics.DrawString(accalc.WorthBySeller.ToString("c"), f_10, Brushes.Black, new PointF(430, 110));
			e.Graphics.DrawString("Reinstate Amount:", f_10, Brushes.Black, new PointF(550, 110));
			e.Graphics.DrawString(accalc.ReinstateAmount.ToString("c"), f_10, Brushes.Black, new PointF(695, 110));
			e.Graphics.DrawString("Asking Cash:", f_10, Brushes.Black, new PointF(30, 140));
			e.Graphics.DrawString(accalc.AskingCash, f_10, Brushes.Black, new PointF(169, 140));
			e.Graphics.DrawString("Total Owed:", f_10, Brushes.Black, new PointF(300, 140));
			e.Graphics.DrawString(accalc.TotalOwed.ToString("c"), f_10, Brushes.Black, new PointF(430, 140));
			e.Graphics.DrawString("Total Monthly:", f_10, Brushes.Black, new PointF(550, 140));
			e.Graphics.DrawString(accalc.TotalMonthly.ToString("c"), f_10, Brushes.Black, new PointF(695, 140));
			e.Graphics.DrawString("Foreclosing:", f_10, Brushes.Black, new PointF(30, 170));
			e.Graphics.DrawString(accalc.ForeClosing, f_10, Brushes.Black, new PointF(169, 170));
			e.Graphics.DrawString("Asking Price:", f_10, Brushes.Black, new PointF(300, 170));
			e.Graphics.DrawString(accalc.AskingPrice.ToString("c"), f_10, Brushes.Black, new PointF(430, 170));

			//RETAIL / AUCTION TAB
			e.Graphics.DrawString("Options Retail / Auction", f_12BI, Brushes.Black, new PointF(30, 220));

			e.Graphics.DrawString("Exit Strategy:", f_10, Brushes.Black, new Point(30, 270));			
			e.Graphics.DrawString(accalc.Options.ExitStrategy.ToString(), f_10, Brushes.Black, new Point(169, 270));
			e.Graphics.DrawString(accalc.Options.ExitStrategy.ToString() + " Percentage:", f_10, Brushes.Black, new Point(300, 270));
			e.Graphics.DrawString(accalc.Options.RetailAuctionPercentage.ToString() + "%", f_10, Brushes.Black, new Point(455, 270));
			e.Graphics.DrawString("Projected Resale Price:", f_10, Brushes.Black, new Point(550, 270));
			e.Graphics.DrawString(accalc.Options.ProjectedResalePrice.ToString("c"), f_10, Brushes.Black, new Point(695, 270));

			e.Graphics.DrawString("Transactional Costs", f_10BI, Brushes.Black, new PointF(30, 310));

			e.Graphics.DrawString("Advertising Costs:", f_10, Brushes.Black, new PointF(30, 340));
			e.Graphics.DrawString("Closing Costs at Purchase:", f_10, Brushes.Black, new PointF(300, 340));			
			e.Graphics.DrawString("House Preparation Costs:", f_10, Brushes.Black, new PointF(550, 340));
			e.Graphics.DrawString("Selling Fees:", f_10, Brushes.Black, new PointF(30, 370));
			e.Graphics.DrawString("Closing Costs at Resale:", f_10, Brushes.Black, new PointF(300, 370));			
			e.Graphics.DrawString("Miscellaneos Costs:", f_10, Brushes.Black, new PointF(550, 370));
			e.Graphics.DrawString("Murphy Factor:", f_10, Brushes.Black, new PointF(300, 420));			
			e.Graphics.DrawString("Transactional Costs:", f_10B, Brushes.Black, new PointF(550, 420));

		}
	}
}
