using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;

namespace WindowsApplication.AcquisitionCalculator
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class FormBreakDown : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Button buttonClose;
		public WindowsApplication.AcquisitionCalculator.UserControl_BreakDown userControl_BreakDown;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public FormBreakDown()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(FormBreakDown));
			this.buttonClose = new System.Windows.Forms.Button();
			this.userControl_BreakDown = new WindowsApplication.AcquisitionCalculator.UserControl_BreakDown();
			this.SuspendLayout();
			// 
			// buttonClose
			// 
			this.buttonClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.buttonClose.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.buttonClose.Location = new System.Drawing.Point(184, 255);
			this.buttonClose.Name = "buttonClose";
			this.buttonClose.TabIndex = 1;
			this.buttonClose.Text = "Close";
			this.buttonClose.Click += new System.EventHandler(this.buttonClose_Click);
			// 
			// userControl_BreakDown
			// 
			this.userControl_BreakDown.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(210)), ((System.Byte)(216)), ((System.Byte)(240)));
			this.userControl_BreakDown.DataArrayCostItems = new WindowsApplication.AcquisitionCalculator.CostItem[0];
			this.userControl_BreakDown.Location = new System.Drawing.Point(0, 0);
			this.userControl_BreakDown.Name = "userControl_BreakDown";
			this.userControl_BreakDown.Size = new System.Drawing.Size(264, 256);
			this.userControl_BreakDown.TabIndex = 2;
			// 
			// FormBreakDown
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(210)), ((System.Byte)(216)), ((System.Byte)(240)));
			this.ClientSize = new System.Drawing.Size(264, 285);
			this.Controls.Add(this.userControl_BreakDown);
			this.Controls.Add(this.buttonClose);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "FormBreakDown";
			this.Text = "BreakDown";
			this.Closing += new System.ComponentModel.CancelEventHandler(this.FormBreakDown_Closing);
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.EnableVisualStyles();
			Application.DoEvents();
			Application.Run(new Form_AcquisitionCalculator());
		}

		private void buttonClose_Click(object sender, System.EventArgs e)
		{
			this.Close();
		}

		private 
			void 
			FormBreakDown_Closing
			(
			  object sender
			, System.ComponentModel.CancelEventArgs e
			)
		{
			userControl_BreakDown.dataGridCosts_CurrentCellChanged
				(
				  this
				, new EventArgs()
				);

			return;
		}
	}
}
