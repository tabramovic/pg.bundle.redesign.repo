using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;
using System.Globalization;

namespace WindowsApplication.AcquisitionCalculator
{
	/// <summary>
	/// Summary description for UserControl.
	/// </summary>
	public class UserControl_Sub2 : System.Windows.Forms.UserControl
	{
		private AcquisitionCalculator.Sub2	sub2;
		private int business_logic_counter;
		private decimal dec_worth_by_comps;
		private	ToolTip tooltip_cash_required;

		private System.Windows.Forms.GroupBox groupBoxExitStrategy;
		private System.Windows.Forms.RadioButton radioButtonAuction;
		private System.Windows.Forms.Label labelProjectedResalePrice;
		private System.Windows.Forms.Label labelTransactionalCosts;
		private System.Windows.Forms.Label labelAdvertisingCosts;
		private System.Windows.Forms.Label labelSellingFees;
		private System.Windows.Forms.Label labelClosingCostsAtPurchase;
		private System.Windows.Forms.Label labelClosingCostsAtResale;
		private System.Windows.Forms.Label labelHousePreparationCosts;
		private System.Windows.Forms.Label labelMiscellaneousCosts;
		private System.Windows.Forms.Button buttonBreakDownAdvertisingCosts;
		private System.Windows.Forms.Label labelTotalTransactionalCost;
		private System.Windows.Forms.Label labelDesiredProfitInPercentage;
		private System.Windows.Forms.Label labelDesiredProfitInBucks;
		private System.Windows.Forms.Button buttonBreakDownBoxSellingFees;
		private System.Windows.Forms.Button buttonBreakDownClosingCostsAtPurchase;
		private System.Windows.Forms.Button buttonBreakDownClosingCostsAtResale;
		private System.Windows.Forms.Button buttonBreakDownMiscellaneousCosts;
		private System.Windows.Forms.Label labelTransactionalMurphyFactor;
		private TextBox textBoxAdvertisingCosts;
		private TextBox textBoxSellingFees;
		private TextBox textBoxClosingCostsAtPurchase;
		private TextBox textBoxClosingCostsAtResale;
		private TextBox textBoxProjectedResalePrice;
		private TextBox textBoxHousePreparationCosts;
		private TextBox textBoxMiscellaneousCosts;
		private TextBox textBoxTotalTransactionalCosts;
		private TextBox textBoxTransactionalMurphyFactor;
		private TextBox textBoxDesiredProfitInPercentage;
		private TextBox textBoxDesiredProfitInBucks;
		private TextBox textBoxProjectedNumberOfMonths;
		private System.Windows.Forms.Label labelProjectedNumberOfMonths;
		private System.Windows.Forms.Label labelRetailAuctionPercentage;
		private TextBox textBoxRetailAuctionPercentage;
		private System.Windows.Forms.RadioButton radioButtonRetail;
		private System.Windows.Forms.PictureBox pictureBox1;
		private WindowsApplication.AcquisitionCalculator.TextBox textBoxMonthlyPayment3;
		private System.Windows.Forms.Label labelMonthlyPayment3;
		private WindowsApplication.AcquisitionCalculator.TextBox textBoxBalance3;
		private System.Windows.Forms.Label labelBalance3;
		private WindowsApplication.AcquisitionCalculator.TextBox textBoxMonthlyPayment2;
		private System.Windows.Forms.Label labelMonthlyPayment2;
		private WindowsApplication.AcquisitionCalculator.TextBox textBoxBalance2;
		private System.Windows.Forms.Label labelBalance2;
		private WindowsApplication.AcquisitionCalculator.TextBox textBoxMonthlyPayment1;
		private System.Windows.Forms.Label labelMonthlyPayment1;
		private WindowsApplication.AcquisitionCalculator.TextBox textBoxBalance1;
		private System.Windows.Forms.Label labelBalance1;
		private System.Windows.Forms.Button buttonBreakDownHoldingCostsOther;
		private WindowsApplication.AcquisitionCalculator.TextBox textBoxTotalHoldingCost;
		private System.Windows.Forms.Label labelTotalHoldingCost;
		private WindowsApplication.AcquisitionCalculator.TextBox textBoxHoldingCostsOther;
		private System.Windows.Forms.Label labelHoldingCostsOther;
		private WindowsApplication.AcquisitionCalculator.TextBox textBoxHoldingCostsMonthlyPayments;
		private System.Windows.Forms.Label labelHoldingCostsMonthlyPayments;
		private System.Windows.Forms.Label labelHoldingCosts;
		private System.Windows.Forms.RadioButton radioButtonPaymentsResaleNo;
		private System.Windows.Forms.RadioButton radioButtonPaymentsResaleYes;
		private WindowsApplication.AcquisitionCalculator.TextBox textBoxForeclosureReinstatementAmount;
		private System.Windows.Forms.Label labelForeclosureReinstatementAmount;
		private WindowsApplication.AcquisitionCalculator.TextBox textBoxMaximumPurchasePrice;
		private System.Windows.Forms.Label labelMaximumPurchasePrice;
		private WindowsApplication.AcquisitionCalculator.TextBox textBoxOtherEncumbrances;
		private System.Windows.Forms.Label labelOtherEncumbrances;
		private WindowsApplication.AcquisitionCalculator.TextBox textBoxTaxesDue;
		private System.Windows.Forms.Label labelTaxesDue;
		private WindowsApplication.AcquisitionCalculator.TextBox textBoxApproximateLoanPayoff;
		private System.Windows.Forms.Label labelApproximateLoanPayoff;
		private WindowsApplication.AcquisitionCalculator.TextBox textBoxCashToSeller;
		private System.Windows.Forms.Label labelCashToSeller;
		private System.Windows.Forms.Label labelDealCost;
		private System.Windows.Forms.GroupBox groupBoxPaymentsAddedToPayoff;
		private System.Windows.Forms.RadioButton radioButton6;
		private System.Windows.Forms.RadioButton radioButtonAddPaymentsTotalPayOffNo;
		private System.Windows.Forms.RadioButton radioButtonAddPaymentsTotalPayOffYes;
		private System.Windows.Forms.RadioButton radioButtonReinstateLoansNo;
		private System.Windows.Forms.RadioButton radioButtonReinstateLoansYes;
		private System.Windows.Forms.GroupBox groupBoxPaymentsUntilResale;
		private System.Windows.Forms.GroupBox groupBoxReinstateLoans;
		private WindowsApplication.AcquisitionCalculator.TextBox textBoxCashRequired;
		private System.Windows.Forms.Label labelCashRequired;
		private System.Windows.Forms.Button buttonBreakDownHousePreparationCosts;
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public UserControl_Sub2()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

			// TODO: Add any initialization after the InitializeComponent call
			sub2 = new Sub2();
			business_logic_counter = 0;
			BussinesLogic(this, new System.EventArgs());

			string strtooltip_cash_required =
				"This is the amount of upfront funds you need to have in"
				+
				" order to do a deal. This number includes all Transactional"
				+
				"Costs EXCEPT 'Selling Fees' and 'Closing Costs at Resale' "
				+
				"� because those costs are not upfront costs, they are paid "
				+
				"out of the proceeds; all Holding Costs, Cash To Seller, and "
				+
				"any Reinstatement Amounts-if you need to prevent foreclosure "
				+
				"by reinstating the loans. The Murphy factor is applied to "
				+
				"Transactional Costs and to the Holding Costs!"
				;
			tooltip_cash_required = new ToolTip();
			tooltip_cash_required.SetToolTip(textBoxCashRequired, strtooltip_cash_required);
		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UserControl_Sub2));
            this.groupBoxExitStrategy = new System.Windows.Forms.GroupBox();
            this.radioButtonAuction = new System.Windows.Forms.RadioButton();
            this.radioButtonRetail = new System.Windows.Forms.RadioButton();
            this.labelRetailAuctionPercentage = new System.Windows.Forms.Label();
            this.labelProjectedResalePrice = new System.Windows.Forms.Label();
            this.labelTransactionalCosts = new System.Windows.Forms.Label();
            this.labelAdvertisingCosts = new System.Windows.Forms.Label();
            this.labelSellingFees = new System.Windows.Forms.Label();
            this.labelClosingCostsAtPurchase = new System.Windows.Forms.Label();
            this.labelClosingCostsAtResale = new System.Windows.Forms.Label();
            this.labelHousePreparationCosts = new System.Windows.Forms.Label();
            this.labelMiscellaneousCosts = new System.Windows.Forms.Label();
            this.buttonBreakDownAdvertisingCosts = new System.Windows.Forms.Button();
            this.labelTotalTransactionalCost = new System.Windows.Forms.Label();
            this.labelDesiredProfitInPercentage = new System.Windows.Forms.Label();
            this.labelDesiredProfitInBucks = new System.Windows.Forms.Label();
            this.buttonBreakDownBoxSellingFees = new System.Windows.Forms.Button();
            this.buttonBreakDownClosingCostsAtPurchase = new System.Windows.Forms.Button();
            this.buttonBreakDownClosingCostsAtResale = new System.Windows.Forms.Button();
            this.buttonBreakDownMiscellaneousCosts = new System.Windows.Forms.Button();
            this.labelTransactionalMurphyFactor = new System.Windows.Forms.Label();
            this.textBoxAdvertisingCosts = new WindowsApplication.AcquisitionCalculator.TextBox();
            this.textBoxSellingFees = new WindowsApplication.AcquisitionCalculator.TextBox();
            this.textBoxClosingCostsAtPurchase = new WindowsApplication.AcquisitionCalculator.TextBox();
            this.textBoxClosingCostsAtResale = new WindowsApplication.AcquisitionCalculator.TextBox();
            this.textBoxRetailAuctionPercentage = new WindowsApplication.AcquisitionCalculator.TextBox();
            this.textBoxProjectedResalePrice = new WindowsApplication.AcquisitionCalculator.TextBox();
            this.textBoxDesiredProfitInPercentage = new WindowsApplication.AcquisitionCalculator.TextBox();
            this.textBoxDesiredProfitInBucks = new WindowsApplication.AcquisitionCalculator.TextBox();
            this.textBoxHousePreparationCosts = new WindowsApplication.AcquisitionCalculator.TextBox();
            this.textBoxMiscellaneousCosts = new WindowsApplication.AcquisitionCalculator.TextBox();
            this.textBoxTotalTransactionalCosts = new WindowsApplication.AcquisitionCalculator.TextBox();
            this.textBoxTransactionalMurphyFactor = new WindowsApplication.AcquisitionCalculator.TextBox();
            this.textBoxProjectedNumberOfMonths = new WindowsApplication.AcquisitionCalculator.TextBox();
            this.labelProjectedNumberOfMonths = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.textBoxMonthlyPayment3 = new WindowsApplication.AcquisitionCalculator.TextBox();
            this.labelMonthlyPayment3 = new System.Windows.Forms.Label();
            this.textBoxBalance3 = new WindowsApplication.AcquisitionCalculator.TextBox();
            this.labelBalance3 = new System.Windows.Forms.Label();
            this.textBoxMonthlyPayment2 = new WindowsApplication.AcquisitionCalculator.TextBox();
            this.labelMonthlyPayment2 = new System.Windows.Forms.Label();
            this.textBoxBalance2 = new WindowsApplication.AcquisitionCalculator.TextBox();
            this.labelBalance2 = new System.Windows.Forms.Label();
            this.textBoxMonthlyPayment1 = new WindowsApplication.AcquisitionCalculator.TextBox();
            this.labelMonthlyPayment1 = new System.Windows.Forms.Label();
            this.textBoxBalance1 = new WindowsApplication.AcquisitionCalculator.TextBox();
            this.labelBalance1 = new System.Windows.Forms.Label();
            this.buttonBreakDownHoldingCostsOther = new System.Windows.Forms.Button();
            this.textBoxTotalHoldingCost = new WindowsApplication.AcquisitionCalculator.TextBox();
            this.labelTotalHoldingCost = new System.Windows.Forms.Label();
            this.textBoxHoldingCostsOther = new WindowsApplication.AcquisitionCalculator.TextBox();
            this.labelHoldingCostsOther = new System.Windows.Forms.Label();
            this.textBoxHoldingCostsMonthlyPayments = new WindowsApplication.AcquisitionCalculator.TextBox();
            this.labelHoldingCostsMonthlyPayments = new System.Windows.Forms.Label();
            this.labelHoldingCosts = new System.Windows.Forms.Label();
            this.groupBoxPaymentsUntilResale = new System.Windows.Forms.GroupBox();
            this.radioButtonPaymentsResaleNo = new System.Windows.Forms.RadioButton();
            this.radioButtonPaymentsResaleYes = new System.Windows.Forms.RadioButton();
            this.textBoxCashRequired = new WindowsApplication.AcquisitionCalculator.TextBox();
            this.textBoxForeclosureReinstatementAmount = new WindowsApplication.AcquisitionCalculator.TextBox();
            this.labelForeclosureReinstatementAmount = new System.Windows.Forms.Label();
            this.textBoxMaximumPurchasePrice = new WindowsApplication.AcquisitionCalculator.TextBox();
            this.labelMaximumPurchasePrice = new System.Windows.Forms.Label();
            this.textBoxOtherEncumbrances = new WindowsApplication.AcquisitionCalculator.TextBox();
            this.labelOtherEncumbrances = new System.Windows.Forms.Label();
            this.textBoxTaxesDue = new WindowsApplication.AcquisitionCalculator.TextBox();
            this.labelTaxesDue = new System.Windows.Forms.Label();
            this.textBoxApproximateLoanPayoff = new WindowsApplication.AcquisitionCalculator.TextBox();
            this.labelApproximateLoanPayoff = new System.Windows.Forms.Label();
            this.textBoxCashToSeller = new WindowsApplication.AcquisitionCalculator.TextBox();
            this.labelCashToSeller = new System.Windows.Forms.Label();
            this.labelDealCost = new System.Windows.Forms.Label();
            this.groupBoxReinstateLoans = new System.Windows.Forms.GroupBox();
            this.radioButtonReinstateLoansNo = new System.Windows.Forms.RadioButton();
            this.radioButtonReinstateLoansYes = new System.Windows.Forms.RadioButton();
            this.labelCashRequired = new System.Windows.Forms.Label();
            this.groupBoxPaymentsAddedToPayoff = new System.Windows.Forms.GroupBox();
            this.radioButtonAddPaymentsTotalPayOffYes = new System.Windows.Forms.RadioButton();
            this.radioButtonAddPaymentsTotalPayOffNo = new System.Windows.Forms.RadioButton();
            this.radioButton6 = new System.Windows.Forms.RadioButton();
            this.buttonBreakDownHousePreparationCosts = new System.Windows.Forms.Button();
            this.groupBoxExitStrategy.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBoxPaymentsUntilResale.SuspendLayout();
            this.groupBoxReinstateLoans.SuspendLayout();
            this.groupBoxPaymentsAddedToPayoff.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBoxExitStrategy
            // 
            this.groupBoxExitStrategy.Controls.Add(this.radioButtonAuction);
            this.groupBoxExitStrategy.Controls.Add(this.radioButtonRetail);
            this.groupBoxExitStrategy.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.groupBoxExitStrategy.Location = new System.Drawing.Point(8, 8);
            this.groupBoxExitStrategy.Name = "groupBoxExitStrategy";
            this.groupBoxExitStrategy.Size = new System.Drawing.Size(176, 48);
            this.groupBoxExitStrategy.TabIndex = 0;
            this.groupBoxExitStrategy.TabStop = false;
            this.groupBoxExitStrategy.Text = "Select Your Exit Strategy:";
            // 
            // radioButtonAuction
            // 
            this.radioButtonAuction.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.radioButtonAuction.Location = new System.Drawing.Point(80, 21);
            this.radioButtonAuction.Name = "radioButtonAuction";
            this.radioButtonAuction.Size = new System.Drawing.Size(72, 16);
            this.radioButtonAuction.TabIndex = 1;
            this.radioButtonAuction.Text = "Auction";
            this.radioButtonAuction.CheckedChanged += new System.EventHandler(this.BussinesLogic);
            // 
            // radioButtonRetail
            // 
            this.radioButtonRetail.Checked = true;
            this.radioButtonRetail.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.radioButtonRetail.Location = new System.Drawing.Point(8, 21);
            this.radioButtonRetail.Name = "radioButtonRetail";
            this.radioButtonRetail.Size = new System.Drawing.Size(64, 16);
            this.radioButtonRetail.TabIndex = 0;
            this.radioButtonRetail.TabStop = true;
            this.radioButtonRetail.Text = "Retail";
            this.radioButtonRetail.CheckedChanged += new System.EventHandler(this.BussinesLogic);
            // 
            // labelRetailAuctionPercentage
            // 
            this.labelRetailAuctionPercentage.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.labelRetailAuctionPercentage.Location = new System.Drawing.Point(192, 8);
            this.labelRetailAuctionPercentage.Name = "labelRetailAuctionPercentage";
            this.labelRetailAuctionPercentage.Size = new System.Drawing.Size(160, 23);
            this.labelRetailAuctionPercentage.TabIndex = 1;
            this.labelRetailAuctionPercentage.Text = "Set R/A Percentage:";
            // 
            // labelProjectedResalePrice
            // 
            this.labelProjectedResalePrice.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelProjectedResalePrice.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.labelProjectedResalePrice.Location = new System.Drawing.Point(680, 8);
            this.labelProjectedResalePrice.Name = "labelProjectedResalePrice";
            this.labelProjectedResalePrice.Size = new System.Drawing.Size(160, 24);
            this.labelProjectedResalePrice.TabIndex = 9;
            this.labelProjectedResalePrice.Text = "Projected Resale Price:";
            // 
            // labelTransactionalCosts
            // 
            this.labelTransactionalCosts.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTransactionalCosts.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.labelTransactionalCosts.Location = new System.Drawing.Point(0, 88);
            this.labelTransactionalCosts.Name = "labelTransactionalCosts";
            this.labelTransactionalCosts.Size = new System.Drawing.Size(200, 23);
            this.labelTransactionalCosts.TabIndex = 11;
            this.labelTransactionalCosts.Text = "TRANSACTIONAL COSTS:";
            // 
            // labelAdvertisingCosts
            // 
            this.labelAdvertisingCosts.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.labelAdvertisingCosts.Location = new System.Drawing.Point(16, 120);
            this.labelAdvertisingCosts.Name = "labelAdvertisingCosts";
            this.labelAdvertisingCosts.Size = new System.Drawing.Size(120, 23);
            this.labelAdvertisingCosts.TabIndex = 12;
            this.labelAdvertisingCosts.Text = "Advertising Costs:";
            // 
            // labelSellingFees
            // 
            this.labelSellingFees.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.labelSellingFees.Location = new System.Drawing.Point(16, 144);
            this.labelSellingFees.Name = "labelSellingFees";
            this.labelSellingFees.Size = new System.Drawing.Size(96, 23);
            this.labelSellingFees.TabIndex = 21;
            this.labelSellingFees.Text = "Selling Fees:";
            // 
            // labelClosingCostsAtPurchase
            // 
            this.labelClosingCostsAtPurchase.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.labelClosingCostsAtPurchase.Location = new System.Drawing.Point(272, 120);
            this.labelClosingCostsAtPurchase.Name = "labelClosingCostsAtPurchase";
            this.labelClosingCostsAtPurchase.Size = new System.Drawing.Size(168, 23);
            this.labelClosingCostsAtPurchase.TabIndex = 15;
            this.labelClosingCostsAtPurchase.Text = "Closing Costs At Purchase:";
            // 
            // labelClosingCostsAtResale
            // 
            this.labelClosingCostsAtResale.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.labelClosingCostsAtResale.Location = new System.Drawing.Point(280, 144);
            this.labelClosingCostsAtResale.Name = "labelClosingCostsAtResale";
            this.labelClosingCostsAtResale.Size = new System.Drawing.Size(160, 23);
            this.labelClosingCostsAtResale.TabIndex = 24;
            this.labelClosingCostsAtResale.Text = "Closing Costs At Resale:";
            // 
            // labelHousePreparationCosts
            // 
            this.labelHousePreparationCosts.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.labelHousePreparationCosts.Location = new System.Drawing.Point(584, 120);
            this.labelHousePreparationCosts.Name = "labelHousePreparationCosts";
            this.labelHousePreparationCosts.Size = new System.Drawing.Size(160, 23);
            this.labelHousePreparationCosts.TabIndex = 18;
            this.labelHousePreparationCosts.Text = "House Preparation Costs:";
            // 
            // labelMiscellaneousCosts
            // 
            this.labelMiscellaneousCosts.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.labelMiscellaneousCosts.Location = new System.Drawing.Point(584, 144);
            this.labelMiscellaneousCosts.Name = "labelMiscellaneousCosts";
            this.labelMiscellaneousCosts.Size = new System.Drawing.Size(160, 23);
            this.labelMiscellaneousCosts.TabIndex = 27;
            this.labelMiscellaneousCosts.Text = "Miscellaneous Costs:";
            // 
            // buttonBreakDownAdvertisingCosts
            // 
            this.buttonBreakDownAdvertisingCosts.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.buttonBreakDownAdvertisingCosts.Image = ((System.Drawing.Image)(resources.GetObject("buttonBreakDownAdvertisingCosts.Image")));
            this.buttonBreakDownAdvertisingCosts.Location = new System.Drawing.Point(216, 120);
            this.buttonBreakDownAdvertisingCosts.Name = "buttonBreakDownAdvertisingCosts";
            this.buttonBreakDownAdvertisingCosts.Size = new System.Drawing.Size(24, 23);
            this.buttonBreakDownAdvertisingCosts.TabIndex = 14;
            this.buttonBreakDownAdvertisingCosts.Click += new System.EventHandler(this.buttonBreakDownAdvertisingCosts_Click);
            // 
            // labelTotalTransactionalCost
            // 
            this.labelTotalTransactionalCost.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTotalTransactionalCost.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.labelTotalTransactionalCost.Location = new System.Drawing.Point(560, 176);
            this.labelTotalTransactionalCost.Name = "labelTotalTransactionalCost";
            this.labelTotalTransactionalCost.Size = new System.Drawing.Size(176, 23);
            this.labelTotalTransactionalCost.TabIndex = 32;
            this.labelTotalTransactionalCost.Text = "Total Transactional Costs:";
            // 
            // labelDesiredProfitInPercentage
            // 
            this.labelDesiredProfitInPercentage.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.labelDesiredProfitInPercentage.Location = new System.Drawing.Point(440, 8);
            this.labelDesiredProfitInPercentage.Name = "labelDesiredProfitInPercentage";
            this.labelDesiredProfitInPercentage.Size = new System.Drawing.Size(120, 23);
            this.labelDesiredProfitInPercentage.TabIndex = 3;
            this.labelDesiredProfitInPercentage.Text = "Desired Profit in %:";
            // 
            // labelDesiredProfitInBucks
            // 
            this.labelDesiredProfitInBucks.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.labelDesiredProfitInBucks.Location = new System.Drawing.Point(440, 32);
            this.labelDesiredProfitInBucks.Name = "labelDesiredProfitInBucks";
            this.labelDesiredProfitInBucks.Size = new System.Drawing.Size(120, 23);
            this.labelDesiredProfitInBucks.TabIndex = 7;
            this.labelDesiredProfitInBucks.Text = "Desired Profit in $:";
            // 
            // buttonBreakDownBoxSellingFees
            // 
            this.buttonBreakDownBoxSellingFees.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.buttonBreakDownBoxSellingFees.Image = ((System.Drawing.Image)(resources.GetObject("buttonBreakDownBoxSellingFees.Image")));
            this.buttonBreakDownBoxSellingFees.Location = new System.Drawing.Point(216, 144);
            this.buttonBreakDownBoxSellingFees.Name = "buttonBreakDownBoxSellingFees";
            this.buttonBreakDownBoxSellingFees.Size = new System.Drawing.Size(24, 23);
            this.buttonBreakDownBoxSellingFees.TabIndex = 23;
            this.buttonBreakDownBoxSellingFees.Click += new System.EventHandler(this.buttonBreakDownBoxSellingFees_Click);
            // 
            // buttonBreakDownClosingCostsAtPurchase
            // 
            this.buttonBreakDownClosingCostsAtPurchase.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.buttonBreakDownClosingCostsAtPurchase.Image = ((System.Drawing.Image)(resources.GetObject("buttonBreakDownClosingCostsAtPurchase.Image")));
            this.buttonBreakDownClosingCostsAtPurchase.Location = new System.Drawing.Point(520, 120);
            this.buttonBreakDownClosingCostsAtPurchase.Name = "buttonBreakDownClosingCostsAtPurchase";
            this.buttonBreakDownClosingCostsAtPurchase.Size = new System.Drawing.Size(24, 23);
            this.buttonBreakDownClosingCostsAtPurchase.TabIndex = 17;
            this.buttonBreakDownClosingCostsAtPurchase.Click += new System.EventHandler(this.buttonBreakDownClosingCostsAtPurchase_Click);
            // 
            // buttonBreakDownClosingCostsAtResale
            // 
            this.buttonBreakDownClosingCostsAtResale.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.buttonBreakDownClosingCostsAtResale.Image = ((System.Drawing.Image)(resources.GetObject("buttonBreakDownClosingCostsAtResale.Image")));
            this.buttonBreakDownClosingCostsAtResale.Location = new System.Drawing.Point(520, 144);
            this.buttonBreakDownClosingCostsAtResale.Name = "buttonBreakDownClosingCostsAtResale";
            this.buttonBreakDownClosingCostsAtResale.Size = new System.Drawing.Size(24, 23);
            this.buttonBreakDownClosingCostsAtResale.TabIndex = 26;
            this.buttonBreakDownClosingCostsAtResale.Click += new System.EventHandler(this.buttonBreakDownClosingCostsAtResale_Click);
            // 
            // buttonBreakDownMiscellaneousCosts
            // 
            this.buttonBreakDownMiscellaneousCosts.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.buttonBreakDownMiscellaneousCosts.Image = ((System.Drawing.Image)(resources.GetObject("buttonBreakDownMiscellaneousCosts.Image")));
            this.buttonBreakDownMiscellaneousCosts.Location = new System.Drawing.Point(824, 144);
            this.buttonBreakDownMiscellaneousCosts.Name = "buttonBreakDownMiscellaneousCosts";
            this.buttonBreakDownMiscellaneousCosts.Size = new System.Drawing.Size(24, 23);
            this.buttonBreakDownMiscellaneousCosts.TabIndex = 29;
            this.buttonBreakDownMiscellaneousCosts.Click += new System.EventHandler(this.buttonBreakDownMiscellaneousCosts_Click);
            // 
            // labelTransactionalMurphyFactor
            // 
            this.labelTransactionalMurphyFactor.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.labelTransactionalMurphyFactor.Location = new System.Drawing.Point(336, 176);
            this.labelTransactionalMurphyFactor.Name = "labelTransactionalMurphyFactor";
            this.labelTransactionalMurphyFactor.Size = new System.Drawing.Size(96, 23);
            this.labelTransactionalMurphyFactor.TabIndex = 30;
            this.labelTransactionalMurphyFactor.Text = "Murphy Factor:";
            this.labelTransactionalMurphyFactor.Visible = false;
            // 
            // textBoxAdvertisingCosts
            // 
            this.textBoxAdvertisingCosts.Location = new System.Drawing.Point(136, 120);
            this.textBoxAdvertisingCosts.Name = "textBoxAdvertisingCosts";
            this.textBoxAdvertisingCosts.Size = new System.Drawing.Size(80, 20);
            this.textBoxAdvertisingCosts.TabIndex = 13;
            this.textBoxAdvertisingCosts.Text = "$0.00";
            this.textBoxAdvertisingCosts.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textBoxAdvertisingCosts.Leave += new System.EventHandler(this.BussinesLogic);
            // 
            // textBoxSellingFees
            // 
            this.textBoxSellingFees.Location = new System.Drawing.Point(136, 144);
            this.textBoxSellingFees.Name = "textBoxSellingFees";
            this.textBoxSellingFees.Size = new System.Drawing.Size(80, 20);
            this.textBoxSellingFees.TabIndex = 22;
            this.textBoxSellingFees.Text = "$0.00";
            this.textBoxSellingFees.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textBoxSellingFees.Leave += new System.EventHandler(this.BussinesLogic);
            // 
            // textBoxClosingCostsAtPurchase
            // 
            this.textBoxClosingCostsAtPurchase.Location = new System.Drawing.Point(440, 120);
            this.textBoxClosingCostsAtPurchase.Name = "textBoxClosingCostsAtPurchase";
            this.textBoxClosingCostsAtPurchase.Size = new System.Drawing.Size(80, 20);
            this.textBoxClosingCostsAtPurchase.TabIndex = 16;
            this.textBoxClosingCostsAtPurchase.Text = "$0.00";
            this.textBoxClosingCostsAtPurchase.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textBoxClosingCostsAtPurchase.Leave += new System.EventHandler(this.BussinesLogic);
            // 
            // textBoxClosingCostsAtResale
            // 
            this.textBoxClosingCostsAtResale.Location = new System.Drawing.Point(440, 144);
            this.textBoxClosingCostsAtResale.Name = "textBoxClosingCostsAtResale";
            this.textBoxClosingCostsAtResale.Size = new System.Drawing.Size(80, 20);
            this.textBoxClosingCostsAtResale.TabIndex = 25;
            this.textBoxClosingCostsAtResale.Text = "$0.00";
            this.textBoxClosingCostsAtResale.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textBoxClosingCostsAtResale.Leave += new System.EventHandler(this.BussinesLogic);
            // 
            // textBoxRetailAuctionPercentage
            // 
            this.textBoxRetailAuctionPercentage.Location = new System.Drawing.Point(360, 8);
            this.textBoxRetailAuctionPercentage.Name = "textBoxRetailAuctionPercentage";
            this.textBoxRetailAuctionPercentage.Size = new System.Drawing.Size(60, 20);
            this.textBoxRetailAuctionPercentage.TabIndex = 2;
            this.textBoxRetailAuctionPercentage.Text = "0%";
            this.textBoxRetailAuctionPercentage.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textBoxRetailAuctionPercentage.Leave += new System.EventHandler(this.BussinesLogic);
            // 
            // textBoxProjectedResalePrice
            // 
            this.textBoxProjectedResalePrice.Location = new System.Drawing.Point(744, 32);
            this.textBoxProjectedResalePrice.Name = "textBoxProjectedResalePrice";
            this.textBoxProjectedResalePrice.ReadOnly = true;
            this.textBoxProjectedResalePrice.Size = new System.Drawing.Size(80, 20);
            this.textBoxProjectedResalePrice.TabIndex = 10;
            this.textBoxProjectedResalePrice.Text = "$0.00";
            this.textBoxProjectedResalePrice.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textBoxProjectedResalePrice.Leave += new System.EventHandler(this.BussinesLogic);
            // 
            // textBoxDesiredProfitInPercentage
            // 
            this.textBoxDesiredProfitInPercentage.Location = new System.Drawing.Point(560, 8);
            this.textBoxDesiredProfitInPercentage.Name = "textBoxDesiredProfitInPercentage";
            this.textBoxDesiredProfitInPercentage.Size = new System.Drawing.Size(80, 20);
            this.textBoxDesiredProfitInPercentage.TabIndex = 4;
            this.textBoxDesiredProfitInPercentage.Text = "10%";
            this.textBoxDesiredProfitInPercentage.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textBoxDesiredProfitInPercentage.Leave += new System.EventHandler(this.BussinesLogic);
            // 
            // textBoxDesiredProfitInBucks
            // 
            this.textBoxDesiredProfitInBucks.Location = new System.Drawing.Point(560, 32);
            this.textBoxDesiredProfitInBucks.Name = "textBoxDesiredProfitInBucks";
            this.textBoxDesiredProfitInBucks.ReadOnly = true;
            this.textBoxDesiredProfitInBucks.Size = new System.Drawing.Size(80, 20);
            this.textBoxDesiredProfitInBucks.TabIndex = 8;
            this.textBoxDesiredProfitInBucks.Text = "$0.00";
            this.textBoxDesiredProfitInBucks.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textBoxDesiredProfitInBucks.Leave += new System.EventHandler(this.BussinesLogic);
            // 
            // textBoxHousePreparationCosts
            // 
            this.textBoxHousePreparationCosts.Location = new System.Drawing.Point(744, 120);
            this.textBoxHousePreparationCosts.Name = "textBoxHousePreparationCosts";
            this.textBoxHousePreparationCosts.Size = new System.Drawing.Size(80, 20);
            this.textBoxHousePreparationCosts.TabIndex = 19;
            this.textBoxHousePreparationCosts.Text = "$0.00";
            this.textBoxHousePreparationCosts.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textBoxHousePreparationCosts.Leave += new System.EventHandler(this.BussinesLogic);
            // 
            // textBoxMiscellaneousCosts
            // 
            this.textBoxMiscellaneousCosts.Location = new System.Drawing.Point(744, 144);
            this.textBoxMiscellaneousCosts.Name = "textBoxMiscellaneousCosts";
            this.textBoxMiscellaneousCosts.Size = new System.Drawing.Size(80, 20);
            this.textBoxMiscellaneousCosts.TabIndex = 28;
            this.textBoxMiscellaneousCosts.Text = "$0.00";
            this.textBoxMiscellaneousCosts.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textBoxMiscellaneousCosts.Leave += new System.EventHandler(this.BussinesLogic);
            // 
            // textBoxTotalTransactionalCosts
            // 
            this.textBoxTotalTransactionalCosts.Location = new System.Drawing.Point(744, 176);
            this.textBoxTotalTransactionalCosts.Name = "textBoxTotalTransactionalCosts";
            this.textBoxTotalTransactionalCosts.ReadOnly = true;
            this.textBoxTotalTransactionalCosts.Size = new System.Drawing.Size(80, 20);
            this.textBoxTotalTransactionalCosts.TabIndex = 33;
            this.textBoxTotalTransactionalCosts.Text = "$0.00";
            this.textBoxTotalTransactionalCosts.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textBoxTotalTransactionalCosts.Leave += new System.EventHandler(this.BussinesLogic);
            // 
            // textBoxTransactionalMurphyFactor
            // 
            this.textBoxTransactionalMurphyFactor.Location = new System.Drawing.Point(440, 176);
            this.textBoxTransactionalMurphyFactor.Name = "textBoxTransactionalMurphyFactor";
            this.textBoxTransactionalMurphyFactor.Size = new System.Drawing.Size(60, 20);
            this.textBoxTransactionalMurphyFactor.TabIndex = 31;
            this.textBoxTransactionalMurphyFactor.Text = "0%";
            this.textBoxTransactionalMurphyFactor.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textBoxTransactionalMurphyFactor.Visible = false;
            this.textBoxTransactionalMurphyFactor.Leave += new System.EventHandler(this.BussinesLogic);
            // 
            // textBoxProjectedNumberOfMonths
            // 
            this.textBoxProjectedNumberOfMonths.Location = new System.Drawing.Point(360, 32);
            this.textBoxProjectedNumberOfMonths.Name = "textBoxProjectedNumberOfMonths";
            this.textBoxProjectedNumberOfMonths.Size = new System.Drawing.Size(60, 20);
            this.textBoxProjectedNumberOfMonths.TabIndex = 6;
            this.textBoxProjectedNumberOfMonths.Text = "0";
            this.textBoxProjectedNumberOfMonths.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textBoxProjectedNumberOfMonths.Leave += new System.EventHandler(this.BussinesLogic);
            // 
            // labelProjectedNumberOfMonths
            // 
            this.labelProjectedNumberOfMonths.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.labelProjectedNumberOfMonths.Location = new System.Drawing.Point(192, 32);
            this.labelProjectedNumberOfMonths.Name = "labelProjectedNumberOfMonths";
            this.labelProjectedNumberOfMonths.Size = new System.Drawing.Size(160, 23);
            this.labelProjectedNumberOfMonths.TabIndex = 5;
            this.labelProjectedNumberOfMonths.Text = "Projected No. of Months:";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(876, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(16, 16);
            this.pictureBox1.TabIndex = 83;
            this.pictureBox1.TabStop = false;
            // 
            // textBoxMonthlyPayment3
            // 
            this.textBoxMonthlyPayment3.Location = new System.Drawing.Point(312, 312);
            this.textBoxMonthlyPayment3.Name = "textBoxMonthlyPayment3";
            this.textBoxMonthlyPayment3.Size = new System.Drawing.Size(80, 20);
            this.textBoxMonthlyPayment3.TabIndex = 51;
            this.textBoxMonthlyPayment3.Text = "$0.00";
            this.textBoxMonthlyPayment3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textBoxMonthlyPayment3.Leave += new System.EventHandler(this.BussinesLogic);
            // 
            // labelMonthlyPayment3
            // 
            this.labelMonthlyPayment3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.labelMonthlyPayment3.Location = new System.Drawing.Point(200, 312);
            this.labelMonthlyPayment3.Name = "labelMonthlyPayment3";
            this.labelMonthlyPayment3.Size = new System.Drawing.Size(112, 23);
            this.labelMonthlyPayment3.TabIndex = 50;
            this.labelMonthlyPayment3.Text = "Monthly Payment:";
            // 
            // textBoxBalance3
            // 
            this.textBoxBalance3.Location = new System.Drawing.Point(104, 312);
            this.textBoxBalance3.Name = "textBoxBalance3";
            this.textBoxBalance3.Size = new System.Drawing.Size(80, 20);
            this.textBoxBalance3.TabIndex = 49;
            this.textBoxBalance3.Text = "$0.00";
            this.textBoxBalance3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textBoxBalance3.Leave += new System.EventHandler(this.BussinesLogic);
            // 
            // labelBalance3
            // 
            this.labelBalance3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.labelBalance3.Location = new System.Drawing.Point(16, 312);
            this.labelBalance3.Name = "labelBalance3";
            this.labelBalance3.Size = new System.Drawing.Size(88, 23);
            this.labelBalance3.TabIndex = 48;
            this.labelBalance3.Text = "3rd Balance:";
            // 
            // textBoxMonthlyPayment2
            // 
            this.textBoxMonthlyPayment2.Location = new System.Drawing.Point(312, 288);
            this.textBoxMonthlyPayment2.Name = "textBoxMonthlyPayment2";
            this.textBoxMonthlyPayment2.Size = new System.Drawing.Size(80, 20);
            this.textBoxMonthlyPayment2.TabIndex = 44;
            this.textBoxMonthlyPayment2.Text = "$0.00";
            this.textBoxMonthlyPayment2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textBoxMonthlyPayment2.Leave += new System.EventHandler(this.BussinesLogic);
            // 
            // labelMonthlyPayment2
            // 
            this.labelMonthlyPayment2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.labelMonthlyPayment2.Location = new System.Drawing.Point(200, 288);
            this.labelMonthlyPayment2.Name = "labelMonthlyPayment2";
            this.labelMonthlyPayment2.Size = new System.Drawing.Size(112, 23);
            this.labelMonthlyPayment2.TabIndex = 43;
            this.labelMonthlyPayment2.Text = "Monthly Payment:";
            // 
            // textBoxBalance2
            // 
            this.textBoxBalance2.Location = new System.Drawing.Point(104, 288);
            this.textBoxBalance2.Name = "textBoxBalance2";
            this.textBoxBalance2.Size = new System.Drawing.Size(80, 20);
            this.textBoxBalance2.TabIndex = 42;
            this.textBoxBalance2.Text = "$0.00";
            this.textBoxBalance2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textBoxBalance2.Leave += new System.EventHandler(this.BussinesLogic);
            // 
            // labelBalance2
            // 
            this.labelBalance2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.labelBalance2.Location = new System.Drawing.Point(16, 288);
            this.labelBalance2.Name = "labelBalance2";
            this.labelBalance2.Size = new System.Drawing.Size(88, 23);
            this.labelBalance2.TabIndex = 41;
            this.labelBalance2.Text = "2nd Balance:";
            // 
            // textBoxMonthlyPayment1
            // 
            this.textBoxMonthlyPayment1.Location = new System.Drawing.Point(312, 264);
            this.textBoxMonthlyPayment1.Name = "textBoxMonthlyPayment1";
            this.textBoxMonthlyPayment1.Size = new System.Drawing.Size(80, 20);
            this.textBoxMonthlyPayment1.TabIndex = 38;
            this.textBoxMonthlyPayment1.Text = "$0.00";
            this.textBoxMonthlyPayment1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textBoxMonthlyPayment1.Leave += new System.EventHandler(this.BussinesLogic);
            // 
            // labelMonthlyPayment1
            // 
            this.labelMonthlyPayment1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.labelMonthlyPayment1.Location = new System.Drawing.Point(200, 264);
            this.labelMonthlyPayment1.Name = "labelMonthlyPayment1";
            this.labelMonthlyPayment1.Size = new System.Drawing.Size(112, 23);
            this.labelMonthlyPayment1.TabIndex = 37;
            this.labelMonthlyPayment1.Text = "Monthly Payment:";
            // 
            // textBoxBalance1
            // 
            this.textBoxBalance1.Location = new System.Drawing.Point(104, 264);
            this.textBoxBalance1.Name = "textBoxBalance1";
            this.textBoxBalance1.Size = new System.Drawing.Size(80, 20);
            this.textBoxBalance1.TabIndex = 36;
            this.textBoxBalance1.Text = "$0.00";
            this.textBoxBalance1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textBoxBalance1.Leave += new System.EventHandler(this.BussinesLogic);
            // 
            // labelBalance1
            // 
            this.labelBalance1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.labelBalance1.Location = new System.Drawing.Point(16, 264);
            this.labelBalance1.Name = "labelBalance1";
            this.labelBalance1.Size = new System.Drawing.Size(88, 23);
            this.labelBalance1.TabIndex = 35;
            this.labelBalance1.Text = "1st Balance:";
            // 
            // buttonBreakDownHoldingCostsOther
            // 
            this.buttonBreakDownHoldingCostsOther.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.buttonBreakDownHoldingCostsOther.Image = ((System.Drawing.Image)(resources.GetObject("buttonBreakDownHoldingCostsOther.Image")));
            this.buttonBreakDownHoldingCostsOther.Location = new System.Drawing.Point(696, 288);
            this.buttonBreakDownHoldingCostsOther.Name = "buttonBreakDownHoldingCostsOther";
            this.buttonBreakDownHoldingCostsOther.Size = new System.Drawing.Size(24, 23);
            this.buttonBreakDownHoldingCostsOther.TabIndex = 47;
            this.buttonBreakDownHoldingCostsOther.Click += new System.EventHandler(this.buttonBreakDownHoldingCostsOther_Click);
            // 
            // textBoxTotalHoldingCost
            // 
            this.textBoxTotalHoldingCost.Location = new System.Drawing.Point(616, 328);
            this.textBoxTotalHoldingCost.Name = "textBoxTotalHoldingCost";
            this.textBoxTotalHoldingCost.ReadOnly = true;
            this.textBoxTotalHoldingCost.Size = new System.Drawing.Size(80, 20);
            this.textBoxTotalHoldingCost.TabIndex = 55;
            this.textBoxTotalHoldingCost.Text = "$0.00";
            this.textBoxTotalHoldingCost.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelTotalHoldingCost
            // 
            this.labelTotalHoldingCost.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTotalHoldingCost.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.labelTotalHoldingCost.Location = new System.Drawing.Point(464, 328);
            this.labelTotalHoldingCost.Name = "labelTotalHoldingCost";
            this.labelTotalHoldingCost.Size = new System.Drawing.Size(144, 23);
            this.labelTotalHoldingCost.TabIndex = 54;
            this.labelTotalHoldingCost.Text = "Total Holding Costs:";
            // 
            // textBoxHoldingCostsOther
            // 
            this.textBoxHoldingCostsOther.Location = new System.Drawing.Point(616, 288);
            this.textBoxHoldingCostsOther.Name = "textBoxHoldingCostsOther";
            this.textBoxHoldingCostsOther.Size = new System.Drawing.Size(80, 20);
            this.textBoxHoldingCostsOther.TabIndex = 46;
            this.textBoxHoldingCostsOther.Text = "$0.00";
            this.textBoxHoldingCostsOther.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textBoxHoldingCostsOther.Leave += new System.EventHandler(this.BussinesLogic);
            // 
            // labelHoldingCostsOther
            // 
            this.labelHoldingCostsOther.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.labelHoldingCostsOther.Location = new System.Drawing.Point(408, 288);
            this.labelHoldingCostsOther.Name = "labelHoldingCostsOther";
            this.labelHoldingCostsOther.Size = new System.Drawing.Size(184, 23);
            this.labelHoldingCostsOther.TabIndex = 45;
            this.labelHoldingCostsOther.Text = "Holding Costs - Other:";
            // 
            // textBoxHoldingCostsMonthlyPayments
            // 
            this.textBoxHoldingCostsMonthlyPayments.Location = new System.Drawing.Point(616, 264);
            this.textBoxHoldingCostsMonthlyPayments.Name = "textBoxHoldingCostsMonthlyPayments";
            this.textBoxHoldingCostsMonthlyPayments.ReadOnly = true;
            this.textBoxHoldingCostsMonthlyPayments.Size = new System.Drawing.Size(80, 20);
            this.textBoxHoldingCostsMonthlyPayments.TabIndex = 40;
            this.textBoxHoldingCostsMonthlyPayments.Text = "$0.00";
            this.textBoxHoldingCostsMonthlyPayments.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelHoldingCostsMonthlyPayments
            // 
            this.labelHoldingCostsMonthlyPayments.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.labelHoldingCostsMonthlyPayments.Location = new System.Drawing.Point(408, 264);
            this.labelHoldingCostsMonthlyPayments.Name = "labelHoldingCostsMonthlyPayments";
            this.labelHoldingCostsMonthlyPayments.Size = new System.Drawing.Size(216, 23);
            this.labelHoldingCostsMonthlyPayments.TabIndex = 39;
            this.labelHoldingCostsMonthlyPayments.Text = "Holding Costs - Monthly Payments:";
            // 
            // labelHoldingCosts
            // 
            this.labelHoldingCosts.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelHoldingCosts.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.labelHoldingCosts.Location = new System.Drawing.Point(0, 232);
            this.labelHoldingCosts.Name = "labelHoldingCosts";
            this.labelHoldingCosts.Size = new System.Drawing.Size(144, 23);
            this.labelHoldingCosts.TabIndex = 34;
            this.labelHoldingCosts.Text = "HOLDING COSTS:";
            // 
            // groupBoxPaymentsUntilResale
            // 
            this.groupBoxPaymentsUntilResale.Controls.Add(this.radioButtonPaymentsResaleNo);
            this.groupBoxPaymentsUntilResale.Controls.Add(this.radioButtonPaymentsResaleYes);
            this.groupBoxPaymentsUntilResale.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.groupBoxPaymentsUntilResale.Location = new System.Drawing.Point(8, 385);
            this.groupBoxPaymentsUntilResale.Name = "groupBoxPaymentsUntilResale";
            this.groupBoxPaymentsUntilResale.Size = new System.Drawing.Size(280, 71);
            this.groupBoxPaymentsUntilResale.TabIndex = 57;
            this.groupBoxPaymentsUntilResale.TabStop = false;
            this.groupBoxPaymentsUntilResale.Text = "Are You making Payments until Resale?";
            // 
            // radioButtonPaymentsResaleNo
            // 
            this.radioButtonPaymentsResaleNo.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.radioButtonPaymentsResaleNo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.radioButtonPaymentsResaleNo.Location = new System.Drawing.Point(80, 45);
            this.radioButtonPaymentsResaleNo.Name = "radioButtonPaymentsResaleNo";
            this.radioButtonPaymentsResaleNo.Size = new System.Drawing.Size(48, 16);
            this.radioButtonPaymentsResaleNo.TabIndex = 1;
            this.radioButtonPaymentsResaleNo.Text = "No";
            this.radioButtonPaymentsResaleNo.CheckedChanged += new System.EventHandler(this.BussinesLogic);
            // 
            // radioButtonPaymentsResaleYes
            // 
            this.radioButtonPaymentsResaleYes.Checked = true;
            this.radioButtonPaymentsResaleYes.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.radioButtonPaymentsResaleYes.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.radioButtonPaymentsResaleYes.Location = new System.Drawing.Point(8, 45);
            this.radioButtonPaymentsResaleYes.Name = "radioButtonPaymentsResaleYes";
            this.radioButtonPaymentsResaleYes.Size = new System.Drawing.Size(48, 16);
            this.radioButtonPaymentsResaleYes.TabIndex = 0;
            this.radioButtonPaymentsResaleYes.TabStop = true;
            this.radioButtonPaymentsResaleYes.Text = "Yes";
            this.radioButtonPaymentsResaleYes.CheckedChanged += new System.EventHandler(this.BussinesLogic);
            // 
            // textBoxCashRequired
            // 
            this.textBoxCashRequired.Location = new System.Drawing.Point(800, 528);
            this.textBoxCashRequired.Name = "textBoxCashRequired";
            this.textBoxCashRequired.ReadOnly = true;
            this.textBoxCashRequired.Size = new System.Drawing.Size(80, 20);
            this.textBoxCashRequired.TabIndex = 73;
            this.textBoxCashRequired.Text = "$0.00";
            this.textBoxCashRequired.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBoxForeclosureReinstatementAmount
            // 
            this.textBoxForeclosureReinstatementAmount.Location = new System.Drawing.Point(584, 480);
            this.textBoxForeclosureReinstatementAmount.Name = "textBoxForeclosureReinstatementAmount";
            this.textBoxForeclosureReinstatementAmount.Size = new System.Drawing.Size(80, 20);
            this.textBoxForeclosureReinstatementAmount.TabIndex = 63;
            this.textBoxForeclosureReinstatementAmount.Text = "$0.00";
            this.textBoxForeclosureReinstatementAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textBoxForeclosureReinstatementAmount.Leave += new System.EventHandler(this.BussinesLogic);
            // 
            // labelForeclosureReinstatementAmount
            // 
            this.labelForeclosureReinstatementAmount.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.labelForeclosureReinstatementAmount.Location = new System.Drawing.Point(368, 480);
            this.labelForeclosureReinstatementAmount.Name = "labelForeclosureReinstatementAmount";
            this.labelForeclosureReinstatementAmount.Size = new System.Drawing.Size(224, 23);
            this.labelForeclosureReinstatementAmount.TabIndex = 62;
            this.labelForeclosureReinstatementAmount.Text = "Foreclosure Reinstatement Amount:";
            // 
            // textBoxMaximumPurchasePrice
            // 
            this.textBoxMaximumPurchasePrice.Location = new System.Drawing.Point(800, 504);
            this.textBoxMaximumPurchasePrice.Name = "textBoxMaximumPurchasePrice";
            this.textBoxMaximumPurchasePrice.ReadOnly = true;
            this.textBoxMaximumPurchasePrice.Size = new System.Drawing.Size(80, 20);
            this.textBoxMaximumPurchasePrice.TabIndex = 69;
            this.textBoxMaximumPurchasePrice.Text = "$0.00";
            this.textBoxMaximumPurchasePrice.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelMaximumPurchasePrice
            // 
            this.labelMaximumPurchasePrice.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMaximumPurchasePrice.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.labelMaximumPurchasePrice.Location = new System.Drawing.Point(544, 504);
            this.labelMaximumPurchasePrice.Name = "labelMaximumPurchasePrice";
            this.labelMaximumPurchasePrice.Size = new System.Drawing.Size(248, 23);
            this.labelMaximumPurchasePrice.TabIndex = 68;
            this.labelMaximumPurchasePrice.Text = "Your MAXIMUM PURCHASE PRICE:";
            // 
            // textBoxOtherEncumbrances
            // 
            this.textBoxOtherEncumbrances.Location = new System.Drawing.Point(224, 480);
            this.textBoxOtherEncumbrances.Name = "textBoxOtherEncumbrances";
            this.textBoxOtherEncumbrances.Size = new System.Drawing.Size(80, 20);
            this.textBoxOtherEncumbrances.TabIndex = 61;
            this.textBoxOtherEncumbrances.Text = "$0.00";
            this.textBoxOtherEncumbrances.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textBoxOtherEncumbrances.Leave += new System.EventHandler(this.BussinesLogic);
            // 
            // labelOtherEncumbrances
            // 
            this.labelOtherEncumbrances.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.labelOtherEncumbrances.Location = new System.Drawing.Point(8, 480);
            this.labelOtherEncumbrances.Name = "labelOtherEncumbrances";
            this.labelOtherEncumbrances.Size = new System.Drawing.Size(136, 23);
            this.labelOtherEncumbrances.TabIndex = 60;
            this.labelOtherEncumbrances.Text = "Other Encumbrances:";
            // 
            // textBoxTaxesDue
            // 
            this.textBoxTaxesDue.Location = new System.Drawing.Point(800, 480);
            this.textBoxTaxesDue.Name = "textBoxTaxesDue";
            this.textBoxTaxesDue.Size = new System.Drawing.Size(80, 20);
            this.textBoxTaxesDue.TabIndex = 65;
            this.textBoxTaxesDue.Text = "$0.00";
            this.textBoxTaxesDue.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textBoxTaxesDue.Leave += new System.EventHandler(this.BussinesLogic);
            // 
            // labelTaxesDue
            // 
            this.labelTaxesDue.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.labelTaxesDue.Location = new System.Drawing.Point(720, 480);
            this.labelTaxesDue.Name = "labelTaxesDue";
            this.labelTaxesDue.Size = new System.Drawing.Size(80, 23);
            this.labelTaxesDue.TabIndex = 64;
            this.labelTaxesDue.Text = "Taxes Due:";
            // 
            // textBoxApproximateLoanPayoff
            // 
            this.textBoxApproximateLoanPayoff.Location = new System.Drawing.Point(224, 504);
            this.textBoxApproximateLoanPayoff.Name = "textBoxApproximateLoanPayoff";
            this.textBoxApproximateLoanPayoff.ReadOnly = true;
            this.textBoxApproximateLoanPayoff.Size = new System.Drawing.Size(80, 20);
            this.textBoxApproximateLoanPayoff.TabIndex = 67;
            this.textBoxApproximateLoanPayoff.Text = "$0.00";
            this.textBoxApproximateLoanPayoff.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelApproximateLoanPayoff
            // 
            this.labelApproximateLoanPayoff.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.labelApproximateLoanPayoff.Location = new System.Drawing.Point(8, 504);
            this.labelApproximateLoanPayoff.Name = "labelApproximateLoanPayoff";
            this.labelApproximateLoanPayoff.Size = new System.Drawing.Size(160, 23);
            this.labelApproximateLoanPayoff.TabIndex = 66;
            this.labelApproximateLoanPayoff.Text = "Approximate Loan Payoff:";
            // 
            // textBoxCashToSeller
            // 
            this.textBoxCashToSeller.Location = new System.Drawing.Point(224, 528);
            this.textBoxCashToSeller.Name = "textBoxCashToSeller";
            this.textBoxCashToSeller.ReadOnly = true;
            this.textBoxCashToSeller.Size = new System.Drawing.Size(80, 20);
            this.textBoxCashToSeller.TabIndex = 71;
            this.textBoxCashToSeller.Text = "$0.00";
            this.textBoxCashToSeller.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelCashToSeller
            // 
            this.labelCashToSeller.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCashToSeller.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.labelCashToSeller.Location = new System.Drawing.Point(8, 528);
            this.labelCashToSeller.Name = "labelCashToSeller";
            this.labelCashToSeller.Size = new System.Drawing.Size(216, 23);
            this.labelCashToSeller.TabIndex = 70;
            this.labelCashToSeller.Text = "MAXIMUM CASH TO SELLER:";
            // 
            // labelDealCost
            // 
            this.labelDealCost.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDealCost.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.labelDealCost.Location = new System.Drawing.Point(0, 361);
            this.labelDealCost.Name = "labelDealCost";
            this.labelDealCost.Size = new System.Drawing.Size(128, 23);
            this.labelDealCost.TabIndex = 56;
            this.labelDealCost.Text = "DEAL SUMMARY:";
            // 
            // groupBoxReinstateLoans
            // 
            this.groupBoxReinstateLoans.Controls.Add(this.radioButtonReinstateLoansNo);
            this.groupBoxReinstateLoans.Controls.Add(this.radioButtonReinstateLoansYes);
            this.groupBoxReinstateLoans.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.groupBoxReinstateLoans.Location = new System.Drawing.Point(592, 385);
            this.groupBoxReinstateLoans.Name = "groupBoxReinstateLoans";
            this.groupBoxReinstateLoans.Size = new System.Drawing.Size(298, 71);
            this.groupBoxReinstateLoans.TabIndex = 59;
            this.groupBoxReinstateLoans.TabStop = false;
            this.groupBoxReinstateLoans.Text = "Will You Reinstate the Loans Before Resale?";            
            // 
            // radioButtonReinstateLoansNo
            // 
            this.radioButtonReinstateLoansNo.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.radioButtonReinstateLoansNo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.radioButtonReinstateLoansNo.Location = new System.Drawing.Point(80, 45);
            this.radioButtonReinstateLoansNo.Name = "radioButtonReinstateLoansNo";
            this.radioButtonReinstateLoansNo.Size = new System.Drawing.Size(48, 16);
            this.radioButtonReinstateLoansNo.TabIndex = 1;
            this.radioButtonReinstateLoansNo.Text = "No";
            this.radioButtonReinstateLoansNo.CheckedChanged += new System.EventHandler(this.BussinesLogic);
            // 
            // radioButtonReinstateLoansYes
            // 
            this.radioButtonReinstateLoansYes.Checked = true;
            this.radioButtonReinstateLoansYes.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.radioButtonReinstateLoansYes.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.radioButtonReinstateLoansYes.Location = new System.Drawing.Point(8, 45);
            this.radioButtonReinstateLoansYes.Name = "radioButtonReinstateLoansYes";
            this.radioButtonReinstateLoansYes.Size = new System.Drawing.Size(48, 16);
            this.radioButtonReinstateLoansYes.TabIndex = 0;
            this.radioButtonReinstateLoansYes.TabStop = true;
            this.radioButtonReinstateLoansYes.Text = "Yes";
            this.radioButtonReinstateLoansYes.CheckedChanged += new System.EventHandler(this.BussinesLogic);
            // 
            // labelCashRequired
            // 
            this.labelCashRequired.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCashRequired.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.labelCashRequired.Location = new System.Drawing.Point(536, 528);
            this.labelCashRequired.Name = "labelCashRequired";
            this.labelCashRequired.Size = new System.Drawing.Size(272, 23);
            this.labelCashRequired.TabIndex = 72;
            this.labelCashRequired.Text = "Total CASH Required To Do The Deal:";
            // 
            // groupBoxPaymentsAddedToPayoff
            // 
            this.groupBoxPaymentsAddedToPayoff.Controls.Add(this.radioButtonAddPaymentsTotalPayOffYes);
            this.groupBoxPaymentsAddedToPayoff.Controls.Add(this.radioButtonAddPaymentsTotalPayOffNo);
            this.groupBoxPaymentsAddedToPayoff.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.groupBoxPaymentsAddedToPayoff.Location = new System.Drawing.Point(288, 385);
            this.groupBoxPaymentsAddedToPayoff.Name = "groupBoxPaymentsAddedToPayoff";
            this.groupBoxPaymentsAddedToPayoff.Size = new System.Drawing.Size(304, 71);
            this.groupBoxPaymentsAddedToPayoff.TabIndex = 58;
            this.groupBoxPaymentsAddedToPayoff.TabStop = false;
            this.groupBoxPaymentsAddedToPayoff.Text = "If No, will They Be Added To the Total Payoff?";
            // 
            // radioButtonAddPaymentsTotalPayOffYes
            // 
            this.radioButtonAddPaymentsTotalPayOffYes.Checked = true;
            this.radioButtonAddPaymentsTotalPayOffYes.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.radioButtonAddPaymentsTotalPayOffYes.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.radioButtonAddPaymentsTotalPayOffYes.Location = new System.Drawing.Point(8, 45);
            this.radioButtonAddPaymentsTotalPayOffYes.Name = "radioButtonAddPaymentsTotalPayOffYes";
            this.radioButtonAddPaymentsTotalPayOffYes.Size = new System.Drawing.Size(48, 16);
            this.radioButtonAddPaymentsTotalPayOffYes.TabIndex = 0;
            this.radioButtonAddPaymentsTotalPayOffYes.TabStop = true;
            this.radioButtonAddPaymentsTotalPayOffYes.Text = "Yes";
            this.radioButtonAddPaymentsTotalPayOffYes.CheckedChanged += new System.EventHandler(this.BussinesLogic);
            // 
            // radioButtonAddPaymentsTotalPayOffNo
            // 
            this.radioButtonAddPaymentsTotalPayOffNo.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.radioButtonAddPaymentsTotalPayOffNo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.radioButtonAddPaymentsTotalPayOffNo.Location = new System.Drawing.Point(56, 45);
            this.radioButtonAddPaymentsTotalPayOffNo.Name = "radioButtonAddPaymentsTotalPayOffNo";
            this.radioButtonAddPaymentsTotalPayOffNo.Size = new System.Drawing.Size(216, 16);
            this.radioButtonAddPaymentsTotalPayOffNo.TabIndex = 1;
            this.radioButtonAddPaymentsTotalPayOffNo.Text = "No (seller will make Payments)";
            this.radioButtonAddPaymentsTotalPayOffNo.CheckedChanged += new System.EventHandler(this.BussinesLogic);
            // 
            // radioButton6
            // 
            this.radioButton6.Location = new System.Drawing.Point(0, 0);
            this.radioButton6.Name = "radioButton6";
            this.radioButton6.Size = new System.Drawing.Size(104, 24);
            this.radioButton6.TabIndex = 0;
            // 
            // buttonBreakDownHousePreparationCosts
            // 
            this.buttonBreakDownHousePreparationCosts.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.buttonBreakDownHousePreparationCosts.Image = ((System.Drawing.Image)(resources.GetObject("buttonBreakDownHousePreparationCosts.Image")));
            this.buttonBreakDownHousePreparationCosts.Location = new System.Drawing.Point(824, 120);
            this.buttonBreakDownHousePreparationCosts.Name = "buttonBreakDownHousePreparationCosts";
            this.buttonBreakDownHousePreparationCosts.Size = new System.Drawing.Size(24, 23);
            this.buttonBreakDownHousePreparationCosts.TabIndex = 20;
            this.buttonBreakDownHousePreparationCosts.Click += new System.EventHandler(this.buttonBreakDownMisHousePreparationCosts_Click);
            // 
            // UserControl_Sub2
            // 
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.Controls.Add(this.buttonBreakDownHousePreparationCosts);
            this.Controls.Add(this.groupBoxPaymentsAddedToPayoff);
            this.Controls.Add(this.textBoxCashRequired);
            this.Controls.Add(this.textBoxForeclosureReinstatementAmount);
            this.Controls.Add(this.labelForeclosureReinstatementAmount);
            this.Controls.Add(this.textBoxMaximumPurchasePrice);
            this.Controls.Add(this.labelMaximumPurchasePrice);
            this.Controls.Add(this.textBoxOtherEncumbrances);
            this.Controls.Add(this.labelOtherEncumbrances);
            this.Controls.Add(this.textBoxTaxesDue);
            this.Controls.Add(this.labelTaxesDue);
            this.Controls.Add(this.textBoxApproximateLoanPayoff);
            this.Controls.Add(this.labelApproximateLoanPayoff);
            this.Controls.Add(this.textBoxCashToSeller);
            this.Controls.Add(this.labelCashToSeller);
            this.Controls.Add(this.groupBoxReinstateLoans);
            this.Controls.Add(this.labelCashRequired);
            this.Controls.Add(this.textBoxMonthlyPayment3);
            this.Controls.Add(this.labelMonthlyPayment3);
            this.Controls.Add(this.textBoxBalance3);
            this.Controls.Add(this.labelBalance3);
            this.Controls.Add(this.textBoxMonthlyPayment2);
            this.Controls.Add(this.labelMonthlyPayment2);
            this.Controls.Add(this.textBoxBalance2);
            this.Controls.Add(this.labelBalance2);
            this.Controls.Add(this.textBoxMonthlyPayment1);
            this.Controls.Add(this.labelMonthlyPayment1);
            this.Controls.Add(this.textBoxBalance1);
            this.Controls.Add(this.labelBalance1);
            this.Controls.Add(this.buttonBreakDownHoldingCostsOther);
            this.Controls.Add(this.textBoxTotalHoldingCost);
            this.Controls.Add(this.labelTotalHoldingCost);
            this.Controls.Add(this.textBoxHoldingCostsOther);
            this.Controls.Add(this.labelHoldingCostsOther);
            this.Controls.Add(this.textBoxHoldingCostsMonthlyPayments);
            this.Controls.Add(this.labelHoldingCostsMonthlyPayments);
            this.Controls.Add(this.labelHoldingCosts);
            this.Controls.Add(this.groupBoxPaymentsUntilResale);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.textBoxProjectedNumberOfMonths);
            this.Controls.Add(this.labelProjectedNumberOfMonths);
            this.Controls.Add(this.textBoxTransactionalMurphyFactor);
            this.Controls.Add(this.textBoxTotalTransactionalCosts);
            this.Controls.Add(this.textBoxMiscellaneousCosts);
            this.Controls.Add(this.textBoxHousePreparationCosts);
            this.Controls.Add(this.textBoxDesiredProfitInBucks);
            this.Controls.Add(this.textBoxDesiredProfitInPercentage);
            this.Controls.Add(this.textBoxProjectedResalePrice);
            this.Controls.Add(this.textBoxRetailAuctionPercentage);
            this.Controls.Add(this.textBoxClosingCostsAtResale);
            this.Controls.Add(this.textBoxClosingCostsAtPurchase);
            this.Controls.Add(this.textBoxSellingFees);
            this.Controls.Add(this.textBoxAdvertisingCosts);
            this.Controls.Add(this.labelTransactionalMurphyFactor);
            this.Controls.Add(this.buttonBreakDownMiscellaneousCosts);
            this.Controls.Add(this.buttonBreakDownClosingCostsAtResale);
            this.Controls.Add(this.buttonBreakDownClosingCostsAtPurchase);
            this.Controls.Add(this.buttonBreakDownBoxSellingFees);
            this.Controls.Add(this.labelDesiredProfitInPercentage);
            this.Controls.Add(this.labelDesiredProfitInBucks);
            this.Controls.Add(this.labelTotalTransactionalCost);
            this.Controls.Add(this.buttonBreakDownAdvertisingCosts);
            this.Controls.Add(this.labelMiscellaneousCosts);
            this.Controls.Add(this.labelHousePreparationCosts);
            this.Controls.Add(this.labelClosingCostsAtResale);
            this.Controls.Add(this.labelClosingCostsAtPurchase);
            this.Controls.Add(this.labelSellingFees);
            this.Controls.Add(this.labelAdvertisingCosts);
            this.Controls.Add(this.labelTransactionalCosts);
            this.Controls.Add(this.labelProjectedResalePrice);
            this.Controls.Add(this.labelRetailAuctionPercentage);
            this.Controls.Add(this.groupBoxExitStrategy);
            this.Controls.Add(this.labelDealCost);
            this.Name = "UserControl_Sub2";
            this.Size = new System.Drawing.Size(900, 580);
            this.groupBoxExitStrategy.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBoxPaymentsUntilResale.ResumeLayout(false);
            this.groupBoxReinstateLoans.ResumeLayout(false);
            this.groupBoxPaymentsAddedToPayoff.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		#endregion

		private void buttonBreakDownAdvertisingCosts_Click(object sender, System.EventArgs e)
		{
			FormBreakDown fbd = new FormBreakDown();
			fbd.userControl_BreakDown.DataArrayCostItems = sub2.AdvertisingCostsItems;
			fbd.ShowDialog();
			sub2.AdvertisingCostsItems = fbd.userControl_BreakDown.DataArrayCostItems;
			sub2.AdvertisingCosts = Decimal.Parse(fbd.userControl_BreakDown.textBoxTotalCost.Text, NumberStyles.Currency);
			textBoxAdvertisingCosts.Text = fbd.userControl_BreakDown.textBoxTotalCost.Text;
			BussinesLogic(buttonBreakDownAdvertisingCosts, new EventArgs());
		}

		private void buttonBreakDownBoxSellingFees_Click(object sender, System.EventArgs e)
		{
			FormBreakDown fbd = new FormBreakDown();
			fbd.userControl_BreakDown.DataArrayCostItems = sub2.SellingFeesItems;
			fbd.ShowDialog();
			sub2.SellingFeesItems = fbd.userControl_BreakDown.DataArrayCostItems;
			sub2.SellingFees = Decimal.Parse(fbd.userControl_BreakDown.textBoxTotalCost.Text, NumberStyles.Currency);
			textBoxSellingFees.Text = fbd.userControl_BreakDown.textBoxTotalCost.Text;
			BussinesLogic(buttonBreakDownBoxSellingFees, new EventArgs());
		}

		private void buttonBreakDownClosingCostsAtPurchase_Click(object sender, System.EventArgs e)
		{
			FormBreakDown fbd = new FormBreakDown();
			fbd.userControl_BreakDown.DataArrayCostItems = sub2.ClosingCostsAtPurchaseItems;
			fbd.ShowDialog();
			sub2.ClosingCostsAtPurchaseItems = fbd.userControl_BreakDown.DataArrayCostItems;
			sub2.ClosingCostsPurcahse = Decimal.Parse(fbd.userControl_BreakDown.textBoxTotalCost.Text, NumberStyles.Currency);
			textBoxClosingCostsAtPurchase.Text = fbd.userControl_BreakDown.textBoxTotalCost.Text;
			BussinesLogic(buttonBreakDownClosingCostsAtPurchase, new EventArgs());
		}

		private void buttonBreakDownClosingCostsAtResale_Click(object sender, System.EventArgs e)
		{
			FormBreakDown fbd = new FormBreakDown();
			fbd.userControl_BreakDown.DataArrayCostItems = sub2.ClosingCostsAtResaleItems;
			fbd.ShowDialog();
			sub2.ClosingCostsAtResaleItems = fbd.userControl_BreakDown.DataArrayCostItems;
			sub2.ClsoingCostsAtResale = Decimal.Parse(fbd.userControl_BreakDown.textBoxTotalCost.Text, NumberStyles.Currency);
			textBoxClosingCostsAtResale.Text = fbd.userControl_BreakDown.textBoxTotalCost.Text;
			BussinesLogic(buttonBreakDownClosingCostsAtResale, new EventArgs());
		}


		private void buttonBreakDownMiscellaneousCosts_Click(object sender, System.EventArgs e)
		{
			FormBreakDown fbd = new FormBreakDown();
			fbd.userControl_BreakDown.DataArrayCostItems = sub2.MiscellaneousCostsItems;
			fbd.ShowDialog();
			sub2.MiscellaneousCostsItems = fbd.userControl_BreakDown.DataArrayCostItems;
			sub2.MiscellaneousCosts = Decimal.Parse(fbd.userControl_BreakDown.textBoxTotalCost.Text, NumberStyles.Currency);
			textBoxMiscellaneousCosts.Text = fbd.userControl_BreakDown.textBoxTotalCost.Text;
			BussinesLogic(buttonBreakDownMiscellaneousCosts, new EventArgs());
		}

		private void buttonBreakDownMisHousePreparationCosts_Click(object sender, System.EventArgs e)
		{
			FormBreakDown fbd = new FormBreakDown();
			fbd.userControl_BreakDown.DataArrayCostItems = sub2.HousePreparationCostsItems;
			fbd.ShowDialog();
			sub2.HousePreparationCostsItems = fbd.userControl_BreakDown.DataArrayCostItems;
			sub2.HousePreparationCosts = Decimal.Parse(fbd.userControl_BreakDown.textBoxTotalCost.Text, NumberStyles.Currency);
			textBoxHousePreparationCosts.Text = fbd.userControl_BreakDown.textBoxTotalCost.Text;
			BussinesLogic(buttonBreakDownHousePreparationCosts, new EventArgs());		
		}

		private void buttonBreakDownHoldingCostsOther_Click(object sender, System.EventArgs e)
		{
			FormBreakDown fbd = new FormBreakDown();
			fbd.userControl_BreakDown.DataArrayCostItems = sub2.HoldingCostsOtherItems;
			fbd.ShowDialog();
			sub2.HoldingCostsOtherItems = fbd.userControl_BreakDown.DataArrayCostItems;
			sub2.HoldingCostsOther = Decimal.Parse(fbd.userControl_BreakDown.textBoxTotalCost.Text, NumberStyles.Currency);
			textBoxHoldingCostsOther.Text = fbd.userControl_BreakDown.textBoxTotalCost.Text;
			BussinesLogic(buttonBreakDownHoldingCostsOther, new EventArgs());		
		}

        
		public void BussinesLogic(object sender, System.EventArgs e)
		{
			sub2.AdvertisingCosts = 
				Decimal.Parse
				( 
				textBoxAdvertisingCosts.Text
				, NumberStyles.Currency
				);
			sub2.SellingFees =
				Decimal.Parse
				( 
				textBoxSellingFees.Text
				, NumberStyles.Currency
				);

			sub2.ClosingCostsPurcahse =
				Decimal.Parse
				( 
				textBoxClosingCostsAtPurchase.Text
				, NumberStyles.Currency
				);

			sub2.ClsoingCostsAtResale =
				Decimal.Parse
				( 
				textBoxClosingCostsAtResale.Text
				, NumberStyles.Currency
				);
			sub2.HousePreparationCosts =
				Decimal.Parse
				( 
				textBoxHousePreparationCosts.Text
				, NumberStyles.Currency
				);
			sub2.MiscellaneousCosts =
				Decimal.Parse
				( 
				textBoxMiscellaneousCosts.Text
				, NumberStyles.Currency
				);

			sub2.HoldingCostsOther =
				Decimal.Parse
				( 
				textBoxHoldingCostsOther.Text
				, NumberStyles.Currency
				);

			Control ctrl = sender as Control;

			if ( 
				ctrl.Name == radioButtonAuction.Name
				||
				ctrl.Name == radioButtonRetail.Name
				||
				ctrl.Name == this.Name
				)	
			{
				if (radioButtonRetail.Checked == true)
				{
					labelRetailAuctionPercentage.Text = "Set Retail Percentage:"; 
					textBoxRetailAuctionPercentage.Text = "100%";
				}
				else if 
					(
					radioButtonAuction.Checked == true
					)
				{
					labelRetailAuctionPercentage.Text = "Set Auction Percentage:"; 
					textBoxRetailAuctionPercentage.Text = "80%";
				}
			}

			if ( 
				ctrl.Name == radioButtonPaymentsResaleYes.Name
				||
				ctrl.Name == radioButtonPaymentsResaleNo.Name
				||
				ctrl.Name == this.Name
				)	
			{
				if (radioButtonPaymentsResaleYes.Checked == true)
				{
					groupBoxPaymentsAddedToPayoff.Enabled = false;
				}
				else if 
					(
					radioButtonPaymentsResaleNo.Checked == true
					)
				{
					groupBoxPaymentsAddedToPayoff.Enabled = true;
				}
			}

			TextBox tb = sender as TextBox;
			RadioButton rb = sender as RadioButton;

			if (null != tb)
			{
				tb.Text = tb.Text;
			}

			if (
				null != tb
				&&
				tb.Name == "textBoxDesiredProfitInPercentage"
				)
			{
				decimal E =
					Decimal.Parse
					(
					  textBoxDesiredProfitInPercentage.Text.Replace("%","")
					, NumberStyles.Currency
					);

				if (10 > E)
				{
					string str = 
						"Warning! Your goal should be to make at least 10%! "
						+
						"Anything lower than that may turn this deal into "
						+
						"a No-Profit deal if anything goes wrong!"
						;
					MessageBox.Show(str, "Warning", MessageBoxButtons.OK);
				}
			}

			decimal PRP =
				WorthByCOMPS
				*
				Decimal.Parse
				(
				  textBoxRetailAuctionPercentage.Text.Replace("%","")
				, NumberStyles.Currency
				)
				/
				100.00M
				;
			textBoxProjectedResalePrice.Text = PRP.ToString("c");

			decimal transactionaL_costs =
				Decimal.Parse(textBoxAdvertisingCosts.Text, NumberStyles.Currency)
				+
				Decimal.Parse(textBoxSellingFees.Text, NumberStyles.Currency)
				+
				Decimal.Parse(textBoxClosingCostsAtPurchase.Text, NumberStyles.Currency)
				+
				Decimal.Parse(textBoxClosingCostsAtResale.Text, NumberStyles.Currency)
				+
				Decimal.Parse(textBoxHousePreparationCosts.Text, NumberStyles.Currency)
				+
				Decimal.Parse(textBoxMiscellaneousCosts.Text, NumberStyles.Currency)
				;
			textBoxTotalTransactionalCosts.Text = transactionaL_costs.ToString("c");


			decimal projected_resale_price_4 =
						 WorthByCOMPS
						 *
						 Decimal.Parse
						 (
						   textBoxRetailAuctionPercentage.Text.Replace("%","")
						 , NumberStyles.Currency
						 )
						 /
						 100.00M
						 ;
			textBoxProjectedResalePrice.Text = projected_resale_price_4.ToString("c");

			decimal desired_profit_in_bucks_5 =
				projected_resale_price_4
				*
				Decimal.Parse
				(
				  textBoxDesiredProfitInPercentage.Text.Replace("%","")
				, NumberStyles.Currency
				)
				/
				100.00M
				;
			textBoxDesiredProfitInBucks.Text = desired_profit_in_bucks_5.ToString("c");



			decimal holding_costs_monthly_payments_22 =
				(
					Decimal.Parse
						(
						  textBoxMonthlyPayment1.Text
						, NumberStyles.Currency
						)
					+
					Decimal.Parse
						(
						  textBoxMonthlyPayment2.Text
						, NumberStyles.Currency
						)
					+
					Decimal.Parse
						(
						  textBoxMonthlyPayment3.Text
						, NumberStyles.Currency
						)
				)
				*
				Decimal.Parse
					(
					  textBoxProjectedNumberOfMonths.Text
					, NumberStyles.Currency
					)
				;

			/*
			if (radioButtonPaymentsResaleNo.Checked == true)
			{
				holding_costs_monthly_payments_22 = 0.0M;
			}
			*/

			textBoxHoldingCostsMonthlyPayments.Text = holding_costs_monthly_payments_22.ToString("c");


			decimal holding_costs_total_24 =
				Decimal.Parse
				(
				  textBoxHoldingCostsMonthlyPayments.Text
				, NumberStyles.Currency
				)
				+
				Decimal.Parse
				(
				  textBoxHoldingCostsOther.Text
				, NumberStyles.Currency
				)
				;
			textBoxTotalHoldingCost.Text = holding_costs_total_24.ToString("c");

			decimal approximate_loan_payoff_27 =
				Decimal.Parse
				(
				  textBoxBalance1.Text
				, NumberStyles.Currency
				)
				+
				Decimal.Parse
				(
				  textBoxBalance2.Text
				, NumberStyles.Currency
				)
				+
				Decimal.Parse
				(
				  textBoxBalance3.Text
				, NumberStyles.Currency
				)
				;
			textBoxApproximateLoanPayoff.Text = approximate_loan_payoff_27.ToString("c");			

			decimal cash_required =
				(
					transactionaL_costs
					-
					Decimal.Parse
					(
					textBoxClosingCostsAtResale.Text.Replace("%","")
					, NumberStyles.Currency
					)
					-
					Decimal.Parse
					(
					textBoxSellingFees.Text.Replace("%","")
					, NumberStyles.Currency
					)
				)
				*
				(
					1
					+
					Decimal.Parse
					(
					textBoxTransactionalMurphyFactor.Text.Replace("%","")
					, NumberStyles.Currency
					)
					/
					100M
				)
				+
				holding_costs_total_24
				*
				(
					1
					+
					/*
					Decimal.Parse
					(
					  textBoxTransactionalMurphyFactor.Text.Replace("%","")
					, NumberStyles.Currency
					)
					*/
					0
				)
				+
				Decimal.Parse
				(
				  textBoxCashToSeller.Text
				, NumberStyles.Currency
				)
				;

			if (true == radioButtonReinstateLoansYes.Checked)
			{
				cash_required += 
				Decimal.Parse
				(
				  textBoxForeclosureReinstatementAmount.Text
				, NumberStyles.Currency
				)
				;
			}
			textBoxCashRequired.Text = cash_required.ToString("c");	

			decimal maximum_purchase_price = 0.00M;		
			if (
				radioButtonPaymentsResaleYes.Checked == true
				||
				radioButtonPaymentsResaleNo.Checked == true
				&&
				radioButtonAddPaymentsTotalPayOffYes.Checked == true
				)
			{
				maximum_purchase_price = 
					Decimal.Parse
					(
					  textBoxProjectedResalePrice.Text
					, NumberStyles.Currency
					)
					-
					Decimal.Parse
					(
					textBoxDesiredProfitInBucks.Text
					, NumberStyles.Currency
					)
					- 
					Decimal.Parse
					(
					textBoxTotalTransactionalCosts.Text
					, NumberStyles.Currency
					)
					-
					Decimal.Parse
					(
					  textBoxHoldingCostsMonthlyPayments.Text
					, NumberStyles.Currency
					)
					-
					Decimal.Parse
					(
					  textBoxHoldingCostsOther.Text
					, NumberStyles.Currency
					)
					;
			}

			if (
				radioButtonPaymentsResaleNo.Checked == true
				&&
				radioButtonAddPaymentsTotalPayOffNo.Checked == true
				)
			{
				maximum_purchase_price = 
					Decimal.Parse
					(
					  textBoxProjectedResalePrice.Text
					, NumberStyles.Currency
					)
					-
					Decimal.Parse
					(
					  textBoxDesiredProfitInBucks.Text
					, NumberStyles.Currency
					)
					- 
					Decimal.Parse
					(
					  textBoxTotalTransactionalCosts.Text
					, NumberStyles.Currency
					)
					-
					Decimal.Parse
					(
					  textBoxHoldingCostsOther.Text
					, NumberStyles.Currency
					)
					;
			}

			if (maximum_purchase_price < 0.0M)
			{
				string msg =
					"Warning! Your Maximum Purchase Price is negative. "
					+ 
					"This means that the Cost of Acquiring this deal is HIGHER "
					+
					"than the Projected Resale Price?! Go back and re-evaluate the "
					+
					"components of this deal. You may need to reduce Cash To Seller"
					+
					" amount. At the end, it may not be a deal to start with, or it "
					+
					"may require a different resale Exit Strategy! (If you�re planning "
					+
					"on discounting or short selling the loans, then enter in the "
					+
					"projected loan balances after the discounting, to get a better "
					+
					"idea of the deal!)"
					;
				MessageBox.Show(msg, "Warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
			textBoxMaximumPurchasePrice.Text = maximum_purchase_price.ToString("c");

			decimal max_cash_to_seller =
				maximum_purchase_price
				-
				approximate_loan_payoff_27
				-
				Decimal.Parse
				(
				textBoxOtherEncumbrances.Text
				, NumberStyles.Currency
				)
				-
				Decimal.Parse
				(
				  textBoxTaxesDue.Text
				, NumberStyles.Currency
				)
				;
			textBoxCashToSeller.Text = max_cash_to_seller.ToString("c");

			decimal total_cash_required =
				max_cash_to_seller
				+
				Decimal.Parse
				(
				  textBoxHoldingCostsOther.Text
				, NumberStyles.Currency
				)
				;

			if (
				radioButtonPaymentsResaleYes.Checked == true	// Q1
				&&
				radioButtonReinstateLoansYes.Checked == true	// Q3
				)
			{
				total_cash_required += 
					holding_costs_monthly_payments_22
					+
					Decimal.Parse
					(
					  textBoxForeclosureReinstatementAmount.Text
					, NumberStyles.Currency
					)
					;
			}
			else if (
				radioButtonPaymentsResaleYes.Checked == true	// Q1
				&&
				radioButtonReinstateLoansNo.Checked == true		// Q3
				)
			{
				total_cash_required += 
					holding_costs_monthly_payments_22
					;
			}
			else if (
				radioButtonPaymentsResaleNo.Checked == true		// Q1
				&&
				radioButtonReinstateLoansYes.Checked == true	// Q3
				)
			{
				total_cash_required += 
					Decimal.Parse
					(
					  textBoxForeclosureReinstatementAmount.Text
					, NumberStyles.Currency
					)
					;
			}
			else if (
				radioButtonPaymentsResaleNo.Checked == true		// Q1
				&&
				radioButtonReinstateLoansNo.Checked == true		// Q3
				)
			{
			}

			textBoxCashRequired.Text = total_cash_required.ToString("c");
			
			business_logic_counter += 1;

			//SAVE RELOAD SUPPORT - BEGIN
			
			sub2.AdvertisingCosts = decimal.Parse(textBoxAdvertisingCosts.Text, NumberStyles.Currency);
			sub2.ApproximateLoanPayoff = decimal.Parse(textBoxApproximateLoanPayoff.Text, NumberStyles.Currency);
			sub2.AuctionPercentage = double.Parse(textBoxRetailAuctionPercentage.Text.Replace("%", string.Empty));

			sub2.Balance_1st = decimal.Parse(textBoxBalance1.Text, NumberStyles.Currency);
			sub2.Balance_2nd = decimal.Parse(textBoxBalance2.Text, NumberStyles.Currency);
			sub2.Balance_3rd = decimal.Parse(textBoxBalance3.Text, NumberStyles.Currency);

			sub2.CashToSeller = decimal.Parse(textBoxCashToSeller.Text, NumberStyles.Currency);

			sub2.ClosingCostsPurcahse = decimal.Parse(textBoxClosingCostsAtPurchase.Text, NumberStyles.Currency); 
			sub2.ClsoingCostsAtResale = decimal.Parse(textBoxClosingCostsAtResale.Text, NumberStyles.Currency);
			sub2.DesiredProfit = decimal.Parse(textBoxDesiredProfitInBucks.Text, NumberStyles.Currency); 

			sub2.ForeClosureReinstatementAmount = decimal.Parse(textBoxForeclosureReinstatementAmount.Text, NumberStyles.Currency);
			sub2.HoldingCostsMonthlyPayments = decimal.Parse(textBoxHoldingCostsMonthlyPayments.Text, NumberStyles.Currency);
			sub2.HoldingCostsOther = decimal.Parse(textBoxHoldingCostsOther.Text, NumberStyles.Currency);
			sub2.HousePreparationCosts = decimal.Parse(textBoxHousePreparationCosts.Text, NumberStyles.Currency);

			sub2.DesiredProfitPrecentual = double.Parse(textBoxDesiredProfitInPercentage.Text.Replace("%", string.Empty)); 

			sub2.MaximumPurchasePrice = decimal.Parse(textBoxMaximumPurchasePrice.Text, NumberStyles.Currency); 
			sub2.MiscellaneousCosts = decimal.Parse(textBoxMiscellaneousCosts.Text, NumberStyles.Currency);
			sub2.MonthlyPayment_1st = decimal.Parse(textBoxMonthlyPayment1.Text, NumberStyles.Currency);
			sub2.MonthlyPayment_2nd = decimal.Parse(textBoxMonthlyPayment2.Text, NumberStyles.Currency);
			sub2.MonthlyPayment_3rd = decimal.Parse(textBoxMonthlyPayment3.Text, NumberStyles.Currency);
			sub2.CashNeededToCoverTheCosts = decimal.Parse(textBoxCashRequired.Text, NumberStyles.Currency);
			sub2.EncumbrancesOther = decimal.Parse(textBoxOtherEncumbrances.Text, NumberStyles.Currency);

			sub2.ProjectedNumberOfMonths = int.Parse(textBoxProjectedNumberOfMonths.Text);

		 	sub2.ProjectedResalePrice = decimal.Parse(textBoxProjectedResalePrice.Text, NumberStyles.Currency);
			sub2.SellingFees = decimal.Parse(textBoxSellingFees.Text, NumberStyles.Currency);
			sub2.TaxesDue = decimal.Parse(textBoxTaxesDue.Text, NumberStyles.Currency);
			sub2.HoldingCostsTotal = decimal.Parse(textBoxTotalHoldingCost.Text, NumberStyles.Currency);
			sub2.TransactionalCosts = decimal.Parse(textBoxTotalTransactionalCosts.Text, NumberStyles.Currency);

			sub2.TransactoionalCostsMurphyFactor = double.Parse(textBoxTransactionalMurphyFactor.Text.Replace("%", string.Empty));

			sub2.ExitStrategy = radioButtonAuction.Checked ? ExitStrategy.Auction : ExitStrategy.Retail;
			sub2.MakingPaymentsUntilResale = radioButtonPaymentsResaleYes.Checked ? MakingPaymentsUntilResale.Yes : MakingPaymentsUntilResale.No;
			sub2.PaymentsAddedToTotalPayoff = radioButtonAddPaymentsTotalPayOffYes.Checked ? PaymentsAddedToTotalPayoff.Yes : PaymentsAddedToTotalPayoff.No;
			sub2.ReinstateLoansBeforeResale = radioButtonReinstateLoansYes.Checked ? ReinstateLoansBeforeResale.Yes : ReinstateLoansBeforeResale.No;

			//SAVE RELOAD SUPPORT - END

			return;
		}
		
		public Sub2	Sub2
		{
			get	{ return sub2; }
			set
			{
				sub2 = value;

				textBoxAdvertisingCosts.Text = value.AdvertisingCosts.ToString("c");
				textBoxApproximateLoanPayoff.Text = value.ApproximateLoanPayoff.ToString("c");
				textBoxRetailAuctionPercentage.Text = value.AuctionPercentage.ToString();
				textBoxBalance1.Text = value.Balance_1st.ToString("c");
				textBoxBalance2.Text = value.Balance_2nd.ToString("c");
				textBoxBalance3.Text = value.Balance_3rd.ToString("c");
				textBoxCashToSeller.Text = value.CashToSeller.ToString("c");
				textBoxClosingCostsAtPurchase.Text = value.ClosingCostsPurcahse.ToString("c");
				textBoxClosingCostsAtResale.Text = value.ClsoingCostsAtResale.ToString("c");
				textBoxDesiredProfitInBucks.Text = value.DesiredProfit.ToString("c");
				textBoxForeclosureReinstatementAmount.Text = value.ForeClosureReinstatementAmount.ToString("c");
				textBoxHoldingCostsMonthlyPayments.Text = value.HoldingCostsMonthlyPayments.ToString("c");
				textBoxHoldingCostsOther.Text = value.HoldingCostsOther.ToString("c");
				textBoxHousePreparationCosts.Text = value.HousePreparationCosts.ToString("c");
				textBoxDesiredProfitInPercentage.Text = value.DesiredProfitPrecentual.ToString();
				//textBoxMaxAcquisitionPrice.Text = value.MaxAcquisitionPrice.ToString("c");
				textBoxMaximumPurchasePrice.Text = value.MaximumPurchasePrice.ToString("c");
				textBoxMiscellaneousCosts.Text = value.MiscellaneousCosts.ToString("c");
				textBoxMonthlyPayment1.Text = value.MonthlyPayment_1st.ToString("c");
				textBoxMonthlyPayment2.Text = value.MonthlyPayment_2nd.ToString("c");
				textBoxMonthlyPayment3.Text = value.MonthlyPayment_3rd.ToString("c");
				textBoxCashRequired.Text = value.CashNeededToCoverTheCosts.ToString("c");
				textBoxOtherEncumbrances.Text = value.EncumbrancesOther.ToString("c");
				textBoxProjectedNumberOfMonths.Text = value.ProjectedNumberOfMonths.ToString();
				textBoxProjectedResalePrice.Text = value.ProjectedResalePrice.ToString("c");
				textBoxSellingFees.Text = value.SellingFees.ToString("c");
				textBoxTaxesDue.Text = value.TaxesDue.ToString("c");
				textBoxTotalHoldingCost.Text = value.HoldingCostsTotal.ToString("c");
				textBoxTotalTransactionalCosts.Text = value.TransactionalCosts.ToString("c");
				textBoxTransactionalMurphyFactor.Text = value.TransactoionalCostsMurphyFactor.ToString();

				SetExitStrategy(value.ExitStrategy);
				SetPayments(value.MakingPaymentsUntilResale);
				SetPaymentsAddedToTotalPayoff(value.PaymentsAddedToTotalPayoff);
				SetReinstateLoans(value.ReinstateLoansBeforeResale);
				
			}
		}

		void SetExitStrategy(ExitStrategy exitStrategy)
		{
			this.radioButtonAuction.CheckedChanged -= new System.EventHandler(this.BussinesLogic);
			this.radioButtonRetail.CheckedChanged -= new System.EventHandler(this.BussinesLogic);

			if (ExitStrategy.Auction == exitStrategy)
			{
				radioButtonAuction.Checked = true;
				radioButtonRetail.Checked = false;
				labelRetailAuctionPercentage.Text = "Set Auction Percentage:"; 
			}
			else
			{
				radioButtonAuction.Checked = false;
				radioButtonRetail.Checked = true;
				labelRetailAuctionPercentage.Text = "Set Retail Percentage:"; 
			}

			this.radioButtonAuction.CheckedChanged += new System.EventHandler(this.BussinesLogic);
			this.radioButtonRetail.CheckedChanged += new System.EventHandler(this.BussinesLogic);
		}

		void SetPayments(MakingPaymentsUntilResale makingPaymentsUntilResale)
		{
			this.radioButtonPaymentsResaleYes.CheckedChanged -= new System.EventHandler(this.BussinesLogic);
			this.radioButtonPaymentsResaleNo.CheckedChanged -= new System.EventHandler(this.BussinesLogic);

			if (makingPaymentsUntilResale == MakingPaymentsUntilResale.Yes)
			{
				radioButtonPaymentsResaleYes.Checked = true;
				radioButtonPaymentsResaleNo.Checked = false;
			}
			else
			{
				radioButtonPaymentsResaleYes.Checked = false;
				radioButtonPaymentsResaleNo.Checked = true;
			}

			if (radioButtonPaymentsResaleYes.Checked)			
				groupBoxPaymentsAddedToPayoff.Enabled = false;
			
			else if (radioButtonPaymentsResaleNo.Checked)			
				groupBoxPaymentsAddedToPayoff.Enabled = true;

			this.radioButtonPaymentsResaleYes.CheckedChanged += new System.EventHandler(this.BussinesLogic);
			this.radioButtonPaymentsResaleNo.CheckedChanged += new System.EventHandler(this.BussinesLogic);
			
		}

		void SetPaymentsAddedToTotalPayoff(PaymentsAddedToTotalPayoff paymentsAddedToTotalPayoff)
		{
			this.radioButtonAddPaymentsTotalPayOffYes.CheckedChanged -= new System.EventHandler(this.BussinesLogic);
			this.radioButtonAddPaymentsTotalPayOffNo.CheckedChanged -= new System.EventHandler(this.BussinesLogic);

			if (PaymentsAddedToTotalPayoff.Yes == paymentsAddedToTotalPayoff)
			{
				radioButtonAddPaymentsTotalPayOffYes.Checked = true;
				radioButtonAddPaymentsTotalPayOffNo.Checked = false;
			}
			else
			{
				radioButtonAddPaymentsTotalPayOffYes.Checked = false;
				radioButtonAddPaymentsTotalPayOffNo.Checked = true;
			}

			this.radioButtonAddPaymentsTotalPayOffYes.CheckedChanged += new System.EventHandler(this.BussinesLogic);
			this.radioButtonAddPaymentsTotalPayOffNo.CheckedChanged += new System.EventHandler(this.BussinesLogic);
		}

		void SetReinstateLoans(ReinstateLoansBeforeResale reinstateLoansBeforeResale)
		{
			this.radioButtonReinstateLoansYes.CheckedChanged -= new System.EventHandler(this.BussinesLogic);
			this.radioButtonReinstateLoansNo.CheckedChanged -= new System.EventHandler(this.BussinesLogic);

			if (ReinstateLoansBeforeResale.Yes == reinstateLoansBeforeResale)
			{
				radioButtonReinstateLoansYes.Checked = true;
				radioButtonReinstateLoansNo.Checked = false;
			}
			else
			{
				radioButtonReinstateLoansYes.Checked = false;
				radioButtonReinstateLoansNo.Checked = true;
			}
			this.radioButtonReinstateLoansYes.CheckedChanged += new System.EventHandler(this.BussinesLogic);
			this.radioButtonReinstateLoansNo.CheckedChanged += new System.EventHandler(this.BussinesLogic);
		}
		
		public decimal WorthByCOMPS
		{
			set	{ dec_worth_by_comps = value; }
			get	{ return dec_worth_by_comps; }
		}        
	}
}
