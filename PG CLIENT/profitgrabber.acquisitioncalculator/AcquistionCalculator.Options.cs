using System;
using System.Xml;
using System.Xml.Serialization;
using System.Collections;

namespace WindowsApplication.AcquisitionCalculator
{
	/// <summary>
	/// Summary description for AcquistionCalculator.
	/// </summary>
	[XmlRoot("Options")]
	public class Options
	{
		public	Options()
		{
		}

		private	ExitStrategy enum_exit_strategy;
		private	double	dbl_retail_auction_percentage;
		private	decimal	dec_projected_resale_price;
		private	int	int_projected_number_months;
		private	decimal	dec_advertsing_cost;
		private	decimal	dec_closing_cost_purchase;
		private	decimal	dec_house_preparation_costs;
		private	decimal	dec_seeling_fees;
		private decimal	dec_closing_costs_resale;
		private	decimal	dec_miscellaneous_costs;
		private decimal	dec_transactional_costs;
		private double	dbl_murphy_factor;
		private EquitySharing enum_equity_sharing;
		private double dbl_desired_profit;
		private	decimal dec_desired_profit;
		private	decimal	dec_maximum_option_price;
		private decimal	dec_agreed_reserve_price;
		private decimal	dec_desired_guaranteed_profit_in_percents;
		private	decimal	dec_desired_guaranteed_profit_in_bucks;
		private	double	dbl_optinor_seller_receives;
		private	double	dbl_optinor_buyer_receives;
		private	decimal	dec_total_profit;
		private	decimal	dec_total_profit_sellers_optinors;


		//=====================================================================
		[XmlElement]
		public ExitStrategy	ExitStrategy
		{
			set
			{
				enum_exit_strategy = value;
			}
			get
			{
				return enum_exit_strategy;
			}
		}

		[XmlElement]
		public double RetailAuctionPercentage
		{
			set
			{
				dbl_retail_auction_percentage = value;
			}
			get
			{
				return dbl_retail_auction_percentage;
			}
		}

		public decimal ProjectedResalePrice
		{
			set
			{
				dec_projected_resale_price = value;
			}
			get
			{
				return dec_projected_resale_price;
			}
		}

		public int ProjectedNumberOfMonths
		{
			set
			{
				int_projected_number_months = value;
			}
			get
			{
				return int_projected_number_months;
			}
		}

		public decimal AdvertisingCost
		{
			set
			{
				dec_advertsing_cost = value;
			}
			get
			{
				return dec_advertsing_cost;
			}
		}

		public decimal ClosingCostPurchase
		{
			set
			{
				dec_closing_cost_purchase = value;
			}
			get
			{
				return dec_closing_cost_purchase;
			}
		}
			
		public decimal HousePreparationCosts
		{
			set
			{
				dec_house_preparation_costs = value;
			}
			get
			{
				return dec_house_preparation_costs;
			}
		}
			
		public decimal SellingFees
		{
			set
			{
				dec_seeling_fees = value;
			}
			get
			{
				return dec_seeling_fees;
			}
		}

		public decimal ClosingCostsResale
		{
			set
			{
				dec_closing_costs_resale = value;
			}
			get
			{
				return dec_closing_costs_resale;
			}
		}

		public decimal MiscellaneousCosts
		{
			set
			{
				dec_miscellaneous_costs = value;
			}
			get
			{
				return dec_miscellaneous_costs;
			}
		}

		public decimal TransactionalCosts
		{
			set
			{
				dec_transactional_costs =  value;
			}
			get
			{
				return dec_transactional_costs;
			}
		}

		public double MurphyFactor
		{
			set
			{
				dbl_murphy_factor = value;
			}
			get
			{
				return dbl_murphy_factor;
			}
		}

		public EquitySharing EquitySharing
		{
			set
			{
				enum_equity_sharing = value;
			}
			get
			{
				return enum_equity_sharing;
			}
		}
			
		public double DesiredProfit
		{
			set
			{
				dbl_desired_profit = value;
			}
			get
			{
				return dbl_desired_profit;
			}
		}

		public decimal DesiredProfitPercentage
		{
			set
			{
				dec_desired_profit = value;
			}
			get
			{
				return dec_desired_profit;
			}
		}

		public decimal MaximumOptionPrice
		{
			set
			{
				dec_maximum_option_price = value;
			}
			get
			{
				return dec_maximum_option_price;
			}
		}

		public decimal AgreedReservePrice
		{
			set
			{
				dec_agreed_reserve_price = value;
			}
			get
			{
				return dec_agreed_reserve_price;
			}
		}

		public decimal DesiredGuaranteedProfitInPercents
		{
			set
			{
				dec_desired_guaranteed_profit_in_percents = value;
			}
			get
			{
				return dec_desired_guaranteed_profit_in_percents;
			}
		}

		public decimal DesiredGuaranteedProfitInBucks
		{
			set
			{
				dec_desired_guaranteed_profit_in_bucks = value;
			}
			get
			{
				return dec_desired_guaranteed_profit_in_bucks;
			}
		}
		public double OptinorSellerReceives
		{
			set
			{
				dbl_optinor_seller_receives = value;
			}
			get
			{
				return dbl_optinor_seller_receives;
			}
		}

		public double OptinorBuyerReceives
		{
			set
			{
				dbl_optinor_buyer_receives = value;
			}
			get
			{
				return dbl_optinor_buyer_receives;
			}
		}

		public decimal TotalProfit
		{
			set
			{
				dec_total_profit = value;
			}
			get
			{
				return dec_total_profit;
			}
		}

		public decimal TotalProfitSellersOptinors
		{
			set
			{
				dec_total_profit_sellers_optinors = value;
			}
			get
			{
				return dec_total_profit_sellers_optinors;
			}
		}

		//---------------------------------------------------------------------
		public CostItem[] AdvertisingCostsItems = new CostItem[]{};
		public CostItem[] SellingFeesItems = new CostItem[]{};
		public CostItem[] ClosingCostsAtPurchaseItems = new CostItem[]{};
		public CostItem[] ClosingCostsAtResaleItems = new CostItem[]{};
		public CostItem[] HousePreparationCostsItems = new CostItem[]{};
		public CostItem[] MiscellaneousCostsItems = new CostItem[]{};
		public CostItem[] OtherCostsItems = new CostItem[]{};
		public CostItem[] HoldingCostsOtherItems = new CostItem[]{};

	} // class
} // namespace
