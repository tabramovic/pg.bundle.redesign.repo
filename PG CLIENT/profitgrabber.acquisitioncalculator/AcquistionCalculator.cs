using System;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using System.Text;

namespace WindowsApplication.AcquisitionCalculator
{
	/// <summary>
	/// Summary description for AcquistionCalculator.
	/// </summary>
	[XmlRoot("AcquisitionCalculator")]
	public 
		class 
		AcquistionCalculator
	{
		public 
			Options 
			Options
			= new Options()
			;

		public 
			AllCash
			AllCash
			= new AllCash()
			;

		public 
			Sub2
			Sub2
			= new Sub2()
			;
		
		public AcquistionCalculator()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		//[XmlElement("WorthByComps")]
		private
			string
			str_property_address
			;

		//[XmlElement("WorthByComps")]
		private
			decimal
			dec_worth_by_comps
			;

		//[XmlElement("WorthBySeller")]
		private
			decimal
			dec_worth_by_seller
			;

		//[XmlElement("Asking")]
		private
			decimal
			str_asking_price
			;

		//[XmlElement("AskingCash")]
		private
			string
			dec_asking_cash
			;

		//[XmlElement("TotalOwed")]
		private
			decimal
			dec_total_owed
			;

		//[XmlElement("TotalMonthly")]
		private
			decimal
			dec_total_monthly
			;

		//[XmlElement("ForeClosing")]
		private
			string
			dec_foreclosing
			;

		//[XmlElement("AreinstateMount")]
		private
			decimal
			dec_reinstate_amount
			;

		//=====================================================================
		public
			string
			PropertyAddress
		{
			set
			{
				str_property_address = value;
			}
			get
			{
				return str_property_address;
			}
		}

		public
			decimal
			WorthByCOMPS
		{
			set
			{
				dec_worth_by_comps = Convert.ToDecimal(value);
			}
			get
			{
				return dec_worth_by_comps;
			}
		}

		public
			decimal
			WorthBySeller
		{
			set
			{
				dec_worth_by_seller = Convert.ToDecimal(value);
			}
			get
			{
				return dec_worth_by_seller;
			}
		}

		public
			decimal
			AskingPrice
		{
			set
			{
				str_asking_price = value;
			}
			get
			{
				return str_asking_price;
			}
		}

		public
			string
			AskingCash
		{
			set
			{
				//dec_asking_cash = Convert.ToDecimal(value);
				dec_asking_cash = value;
			}
			get
			{
				return dec_asking_cash;
			}
		}

		public
			decimal
			TotalOwed
		{
			set
			{
				dec_total_owed = Convert.ToDecimal(value);
			}
			get
			{
				return dec_total_owed;
			}
		}

		public
			decimal
			TotalMonthly
		{
			set
			{
				dec_total_monthly = Convert.ToDecimal(value);
			}
			get
			{
				return dec_total_monthly;
			}
		}

		public
			string
			ForeClosing
		{
			set
			{
				dec_foreclosing = value;
				//dec_foreclosing = Convert.ToDecimal(Convert.ToDecimal(value));
			}
			get
			{
				return dec_foreclosing;
			}
		}

		public
			decimal
			ReinstateAmount
		{
			set
			{
				dec_reinstate_amount = Convert.ToDecimal(value);
			}
			get
			{
				return dec_reinstate_amount;
			}
		}

		public
			void
			Save
			(
			  string filename
			)
		{
			// Serialization
			try
			{
				XmlSerializer s = new XmlSerializer
										(
										typeof(AcquistionCalculator)
										);
				TextWriter w = new StreamWriter(filename);
				s.Serialize( w, this );
				w.Close();
			}
			catch (Exception ex)
			{
				throw ex;
			}

			return;
		}

		public
			AcquistionCalculator
			Load
			(
			  string filename
			)
		{
			// DeSerialization
			try
			{
				AcquistionCalculator ac;

				XmlSerializer s = new XmlSerializer (typeof(AcquistionCalculator));
				TextReader r = new StreamReader(filename);
				ac = s.Deserialize(r) as AcquistionCalculator;
				if (null != ac)
				{
					this.AskingPrice = ac.AskingPrice;
					this.AskingCash = ac.AskingCash;
					this.ForeClosing = ac.ForeClosing;
					this.PropertyAddress = ac.PropertyAddress;
					this.ReinstateAmount = ac.ReinstateAmount;
					this.TotalMonthly = ac.TotalMonthly;
					this.TotalOwed = ac.TotalOwed;
					this.WorthByCOMPS = ac.WorthByCOMPS;
					this.WorthBySeller = ac.WorthBySeller;
					this.Options = ac.Options;
					this.AllCash = ac.AllCash;
					this.Sub2 = ac.Sub2;
				}
				else
				{
					throw new XmlException("Colud not deserialize " + filename);
				}

				r.Close();
			}
			catch (Exception ex)
			{
				throw ex;
			}

			return this;
		}

	} // class
} // namespace
