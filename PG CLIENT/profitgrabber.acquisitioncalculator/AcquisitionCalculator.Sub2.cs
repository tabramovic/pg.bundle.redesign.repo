using System;
using System.Xml;
using System.Xml.Serialization;
using System.Collections;

namespace WindowsApplication.AcquisitionCalculator
{
	/// <summary>
	/// Summary description for AcquisitionCalculator.
	/// </summary>
	[XmlRoot("Sub2")]
	public class Sub2
	{
		public	Sub2()
		{}

		ExitStrategy enum_exit_strategy;
		double dbl_auction_percentage;
		decimal dec_projected_resale_price;
		int	int_projected_number_months;
		double dbl_desired_profit;
		decimal	dec_desired_profit;
		decimal	dec_max_acquisition_price;
		decimal	dec_advertising_costs;
		decimal	dec_selling_fees;
		decimal	dec_closing_costs_purchase;
		decimal	dec_closing_costs_resale;
		decimal	dec_house_preparation_costs;
		decimal	dec_miscellaneous_costs;
		decimal	dec_transactional_costs;
		double dbl_murphy_factor;

		//.....................................................................
		MakingPaymentsUntilResale enum_making_payments_until_resale;
		PaymentsAddedToTotalPayoff enum_payments_added_to_total_payoff;		
		decimal	dec_balance_1st;
		decimal	dec_monthly_payment_1st;
		decimal dec_balance_2nd;
		decimal dec_monthly_payment_2nd;
		decimal	dec_balance_3rd;
		decimal	dec_monthly_payment_3rd;
		decimal	dec_holding_costs_monthly_payments;
		decimal	dec_holding_costs_other;
		decimal	dec_holding_costs_total;
		decimal	dec_murphy_factor;
		decimal dec_cash_to_seller;
		decimal	dec_approximate_loan_payoff;
		decimal	dec_taxes_due;
		decimal	dec_encumbrances_other;
		decimal	dec_foreclosure_reinstatement_amount;
		decimal	dec_total_deal_cost;
		decimal	dec_total_acquisition_cost;
		decimal	dec_maximum_purchase_price;
		decimal	dec_total_acquisition_deal_cost;
		ReinstateLoansBeforeResale	enum_reinstate_loans_before_resale;
		decimal	dec_cash_needed_to_cover_the_costs;

		//=====================================================================
		public
			ExitStrategy
			ExitStrategy
		{
			set
			{
				enum_exit_strategy = value;
			}
			get
			{
				return enum_exit_strategy;
			}
		}

		public
			double
			AuctionPercentage
		{
			set
			{
				dbl_auction_percentage = value;
			}
			get
			{
				return dbl_auction_percentage;
			}
		}
			
		public
			decimal
			ProjectedResalePrice
		{
			set
			{
				dec_projected_resale_price = value;
			}
			get
			{
				return dec_projected_resale_price;
			}
		}

		public
			int
			ProjectedNumberOfMonths
		{
			set
			{
				int_projected_number_months = value;
			}
			get
			{
				return int_projected_number_months;
			}
		}
			
		public
			double
			DesiredProfitPrecentual
		{
			set
			{
				dbl_desired_profit = value;
			}
			get
			{
				return dbl_desired_profit;
			}
		}

		public
			decimal
			DesiredProfit
		{
			set
			{
				dec_desired_profit = value;
			}
			get
			{
				return dec_desired_profit;
			}
		}

		public
			decimal
			MaxAcquisitionPrice
		{
			set
			{
				dec_max_acquisition_price = value;
			}
			get
			{
				return dec_max_acquisition_price;
			}
		}

		public
			decimal
			AdvertisingCosts
		{
			set
			{
				dec_advertising_costs = value;
			}
			get
			{
				return dec_advertising_costs;
			}
		}

		public
			decimal
			SellingFees
		{
			set
			{
				dec_selling_fees = value;
			}
			get
			{
				return dec_selling_fees;
			}
		}

		public
			decimal
			ClosingCostsPurcahse
		{
			set
			{
				dec_closing_costs_purchase = value;
			}
			get
			{
				return dec_closing_costs_purchase;
			}
		}

		public
			decimal
			ClsoingCostsAtResale
		{
			set
			{
				dec_closing_costs_resale = value;
			}
			get
			{
				return dec_closing_costs_resale;
			}
		}

		public
			decimal
			HousePreparationCosts
		{
			set
			{
				dec_house_preparation_costs = value;
			}
			get
			{
				return dec_house_preparation_costs;
			}
		}

		public
			decimal
			MiscellaneousCosts
		{
			set
			{
				dec_miscellaneous_costs = value;
			}
			get
			{
				return dec_miscellaneous_costs;
			}
		}

		public
			decimal
			TransactionalCosts
		{
			set
			{
				dec_transactional_costs = value;
			}
			get
			{
				return dec_transactional_costs;
			}
		}

		public
			double
			TransactoionalCostsMurphyFactor
		{
			set
			{
				dbl_murphy_factor = value;
			}
			get
			{
				return dbl_murphy_factor;
			}
		}

		//.....................................................................
		public 
			MakingPaymentsUntilResale
			MakingPaymentsUntilResale
		{
			set
			{
				enum_making_payments_until_resale = value;
			}
			get
			{
				return enum_making_payments_until_resale;
			}
		}

		public
			PaymentsAddedToTotalPayoff
			PaymentsAddedToTotalPayoff
		{
			set
			{
				enum_payments_added_to_total_payoff = value;
			}
			get
			{
				return enum_payments_added_to_total_payoff;
			}
		}
		
		public
			decimal
			Balance_1st
		{
			set
			{
				dec_balance_1st = value;
			}
			get
			{
				return dec_balance_1st;
			}
		}

		public
			decimal
			MonthlyPayment_1st
		{
			set
			{
				dec_monthly_payment_1st = value;
			}
			get
			{
				return dec_monthly_payment_1st;
			}
		}

		public
			decimal
			Balance_2nd
		{
			set
			{
				dec_balance_2nd = value;
			}
			get
			{
				return dec_balance_2nd;
			}
		}

		public
			decimal
			MonthlyPayment_2nd
		{
			set
			{
				dec_monthly_payment_2nd = value;
			}
			get
			{
				return dec_monthly_payment_2nd;
			}
		}

		public
			decimal
			Balance_3rd
		{
			set
			{
				dec_balance_3rd = value;
			}
			get
			{
				return dec_balance_3rd;
			}
		}

		public
			decimal
			MonthlyPayment_3rd
		{
			set
			{
				dec_monthly_payment_3rd = value;
			}
			get
			{
				return dec_monthly_payment_3rd;
			}
		}

		public
			decimal
			HoldingCostsMonthlyPayments
		{
			set
			{
				dec_holding_costs_monthly_payments = value;
			}
			get
			{
				return dec_holding_costs_monthly_payments;
			}
		}

		public
			decimal
			HoldingCostsOther
		{
			set
			{
				dec_holding_costs_other = value;
			}
			get
			{
				return dec_holding_costs_other;
			}
		}

		public
			decimal
			HoldingCostsTotal
		{
			set
			{
				dec_holding_costs_total = value;
			}
			get
			{
				return dec_holding_costs_total;
			}
		}

		public
			decimal
			HoldingCostsMurphyFactor
		{
			set
			{
				dec_murphy_factor = value;
			}
			get
			{
				return dec_murphy_factor;
			}
		}

		public
			decimal
			CashToSeller
		{
			set
			{
				dec_cash_to_seller = value;
			}
			get
			{
				return dec_cash_to_seller;
			}
		}

		public
			decimal
			ApproximateLoanPayoff
		{
			set
			{
				dec_approximate_loan_payoff = value;
			}
			get
			{
				return dec_approximate_loan_payoff;
			}
		}

		public
			decimal
			TaxesDue
		{
			set
			{
				dec_taxes_due = value;
			}
			get
			{
				return dec_taxes_due;
			}
		}

		public
			decimal
			EncumbrancesOther
		{
			set
			{
				dec_encumbrances_other = value;
			}
			get
			{
				return dec_encumbrances_other;
			}
		}

		public
			decimal
			ForeClosureReinstatementAmount
		{
			set
			{
				dec_foreclosure_reinstatement_amount = value;
			}
			get
			{
				return dec_foreclosure_reinstatement_amount;
			}
		}

		public
			decimal
			TotalDealCost
		{
			set
			{
				dec_total_deal_cost = value;
			}
			get
			{
				return dec_total_deal_cost;
			}
		}

		public
			decimal
			TotalAcquisitionCost
		{
			set
			{
				dec_total_acquisition_cost = value;
			}
			get
			{
				return dec_total_acquisition_cost;
			}
		}

		public
			decimal
			MaximumPurchasePrice
		{
			set
			{
				dec_maximum_purchase_price = value;
			}
			get
			{
				return dec_maximum_purchase_price;
			}
		}

		public
			decimal
			TotalAcquisitionDealCost
		{
			set
			{
				dec_total_acquisition_deal_cost = value;
			}
			get
			{
				return dec_total_acquisition_deal_cost;
			}
		}

		public
			ReinstateLoansBeforeResale
			ReinstateLoansBeforeResale
		{
			set
			{
				enum_reinstate_loans_before_resale = value;
			}
			get
			{
				return enum_reinstate_loans_before_resale;
			}
		}

		public
			decimal
			CashNeededToCoverTheCosts
		{
			set
			{
				dec_cash_needed_to_cover_the_costs = value;
			}
			get
			{
				return dec_cash_needed_to_cover_the_costs;
			}
		}

		//---------------------------------------------------------------------
		public
			CostItem[]
			AdvertisingCostsItems = new CostItem[]{}
			;

		public
			CostItem[]
			SellingFeesItems = new CostItem[]{}
			;

		public
			CostItem[]
			ClosingCostsAtPurchaseItems = new CostItem[]{}
			;
		public
			CostItem[]
			ClosingCostsAtResaleItems = new CostItem[]{}
			;

		public
			CostItem[]
			HousePreparationCostsItems = new CostItem[]{}
			;

		public
			CostItem[]
			MiscellaneousCostsItems = new CostItem[]{}
			;

		public
			CostItem[]
			HoldingCostsOtherItems = new CostItem[]{}
			;
	}
}