using System;
using System.Xml;
using System.Xml.Serialization;
using System.Collections;

namespace WindowsApplication.AcquisitionCalculator
{
	/// <summary>
	/// Summary description for AcquisitionCalculaltor.
	/// </summary>
	[XmlRoot("AllCash")]
	public
		class
		AllCash
	{
		public
			AllCash
			(
			)
		{
		}

		ExitStrategy enum_exit_strategy;
		double dbl_retail_auction_percentage;
		decimal	dec_projected_resale_price;
		int	int_projected_number_months;
		double dbl_desired_profit;
		decimal	dec_desired_profit;
		decimal	dec_max_acquisition_price;
		decimal	dec_advertising_costs;
		decimal	dec_selling_fees;
		decimal	dec_closing_costs_purchase;
		decimal	dec_closing_costs_resale;
		decimal	dec_house_preparation_costs;
		decimal	dec_miscellaneous_costs;
		decimal	dec_transactional_costs;
		double	dbl_murphy_factor;
		InterestOnlyLoan enum_interest_only_loan;
		decimal	dec_loan_amount	;
		decimal	dec_loan_points;
		double	dbl_interest_rate;
		decimal	dec_monthly_payment;
		decimal	dec_other_costs;
		decimal	dec_LTV;
		decimal	dec_total_financing_costs;
		MakingPaymentsUntilResale enum_making_payments_until_resale;
		decimal	dec_holding_costs_monthly_payments;
		decimal	dec_holding_costs_other;
		decimal	dec_holding_costs_total;
		double dbl_holding_costs_murphy_factor;
		decimal	dec_other_cash_int_the_deal;
		decimal	dec_approximate_loan_payoff;
		decimal	dec_taxes_due;
		decimal	dec_other_encumbrances;
		decimal	dec_deal_costs_total;
		decimal	dec_total_acquistion_deal_cost;
		decimal	dec_maximum_purchase_price;
		int _amortizationInMonths;

		//=====================================================================
		public int AmortizationInMonths
		{
			get { return _amortizationInMonths; }
			set { _amortizationInMonths = value; }
		}

		public
			ExitStrategy
			ExitStrategy
		{
			set
			{
				enum_exit_strategy = value;
			}
			get
			{
				return enum_exit_strategy;
			}
		}

		public
			double
			RetailAuctionPercentage
		{
			set
			{
				dbl_retail_auction_percentage = value;
			}
			get
			{
				return dbl_retail_auction_percentage;
			}
		}
			
		public
			decimal
			ProjectedResalePrice
		{
			set
			{
				dec_projected_resale_price = value;
			}
			get
			{
				return dec_projected_resale_price;
			}
		}

		public
			int
			ProjectedNumberOfMonths
		{
			set
			{
				int_projected_number_months = value;
			}
			get
			{
				return int_projected_number_months;
			}
		}
			
		public
			double
			DesiredProfitPrecentual
		{
			set
			{
				dbl_desired_profit = value;
			}
			get
			{
				return dbl_desired_profit;
			}
		}

		public
			decimal
			DesiredProfit
		{
			set
			{
				dec_desired_profit = value;
			}
			get
			{
				return dec_desired_profit;
			}
		}

		public
			decimal
			MaxAcquisitionPrice
		{
			set
			{
				dec_max_acquisition_price = value;
			}
			get
			{
				return dec_max_acquisition_price;
			}
		}

		public
			decimal
			AdvertisingCosts
		{
			set
			{
				dec_advertising_costs = value;
			}
			get
			{
				return dec_advertising_costs;
			}
		}

		public
			decimal
			SellingFees
		{
			set
			{
				dec_selling_fees = value;
			}
			get
			{
				return dec_selling_fees;
			}
		}

		public
			decimal
			ClosingCostsPurcahse
		{
			set
			{
				dec_closing_costs_purchase = value;
			}
			get
			{
				return dec_closing_costs_purchase;
			}
		}

		public
			decimal
			ClsoingCostsAtResale
		{
			set
			{
				dec_closing_costs_resale = value;
			}
			get
			{
				return dec_closing_costs_resale;
			}
		}

		public
			decimal
			PreparationCosts
		{
			set
			{
				dec_house_preparation_costs = value;
			}
			get
			{
				return dec_house_preparation_costs;
			}
		}

		public
			decimal
			MiscellaneousCosts
		{
			set
			{
				dec_miscellaneous_costs = value;
			}
			get
			{
				return dec_miscellaneous_costs;
			}
		}

		public
			decimal
			TransactionalCosts
		{
			set
			{
				dec_transactional_costs = value;
			}
			get
			{
				return dec_transactional_costs;
			}
		}

		public
			double
			TransactoionalCostsMurphyFactor
		{
			set
			{
				dbl_murphy_factor = value;
			}
			get
			{
				return dbl_murphy_factor;
			}
		}

		public
			InterestOnlyLoan
			InterestOnlyLoan
		{
			set
			{
				enum_interest_only_loan = value;
			}
			get
			{
				return enum_interest_only_loan;
			}
		}

		public
			decimal
			LoanAmount
		{
			set
			{
				dec_loan_amount = value;
			}
			get
			{
				return dec_loan_amount;
			}
		}

		public
			decimal
			LoanPoints
		{
			set
			{
				dec_loan_points = value;
			}
			get
			{
				return dec_loan_points;
			}
		}

		public
			double
			InterestRate
		{
			set
			{
				dbl_interest_rate = value;
			}
			get
			{
				return dbl_interest_rate;
			}
		}

		public
			decimal
			MonthlyPayment
		{
			set
			{
				dec_monthly_payment = value;
			}
			get
			{
				return dec_monthly_payment;
			}
		}

		public
			decimal
			OtherCosts
		{
			set
			{
				dec_other_costs = value;
			}
			get
			{
				return dec_other_costs;
			}
		}


		public
			decimal
			LTV
		{
			set
			{
				dec_LTV = value;
			}
			get
			{
				return dec_LTV;
			}
		}

		public
			decimal
			TotalFinancingCosts
		{
			set
			{
				dec_total_financing_costs = value;
			}
			get
			{
				return dec_total_financing_costs;
			}
		}

		public
			MakingPaymentsUntilResale
			MakingPaymentsUntilResale
		{
			set
			{
				enum_making_payments_until_resale = value;
			}
			get
			{
				return enum_making_payments_until_resale;
			}
		}
		
		public
			decimal
			HoldingCostsMonthlyPayments
		{
			set
			{
				dec_holding_costs_monthly_payments = value;
			}
			get
			{
				return dec_holding_costs_monthly_payments;
			}
		}

		public
			decimal
			HoldingCostsOther
		{
			set
			{
				dec_holding_costs_other = value;
			}
			get
			{
				return	dec_holding_costs_other;
			}
		}

		public
			decimal
			HoldingCostsTotal
		{
			set
			{
				dec_holding_costs_total = value;
			}
			get
			{
				return dec_holding_costs_total;
			}
		}

		public
			double
			HoldingCostsMurphyFactor
		{
			set
			{
				dbl_holding_costs_murphy_factor = value;
			}
			get
			{
				return dbl_holding_costs_murphy_factor;
			}
		}

		public
			decimal
			OtherCashInTheDeal
		{
			set
			{
				dec_other_cash_int_the_deal = value;
			}
			get
			{
				return dec_other_cash_int_the_deal;
			}
		}

		public
			decimal
			ApproximateLoanPayoff
		{
			set
			{
				dec_approximate_loan_payoff = value;
			}
			get
			{
				return dec_approximate_loan_payoff;
			}
		}

		public
			decimal
			TaxesDue
		{
			set
			{
				dec_taxes_due = value;
			}
			get
			{
				return dec_taxes_due;
			}
		}

		public
			decimal
			OtherEncumbrances
		{
			set
			{
				dec_other_encumbrances = value;
			}
			get
			{
				return dec_other_encumbrances;
			}
		}

		public
			decimal
			DealCostsTotal
		{
			set
			{
				dec_deal_costs_total = value;
			}
			get
			{
				return dec_deal_costs_total;
			}
		}

		public
			decimal
			TotalAcquisitionDealCost
		{
			set
			{
				dec_total_acquistion_deal_cost = value;
			}
			get
			{
				return dec_total_acquistion_deal_cost;
			}
		}

		public
			decimal
			MaximumPurchasePrice
		{
			set
			{
				dec_maximum_purchase_price = value;
			}
			get
			{
				return dec_maximum_purchase_price;
			}
		}

		//---------------------------------------------------------------------
		public
			CostItem[]
			AdvertisingCostsItems = new CostItem[]{}
			;

		public
			CostItem[]
			SellingFeesItems = new CostItem[]{}
			;

		public
			CostItem[]
			ClosingCostsAtPurchaseItems = new CostItem[]{}
			;
		public
			CostItem[]
			ClosingCostsAtResaleItems = new CostItem[]{}
			;

		public
			CostItem[]
			HousePreparationCostsItems = new CostItem[]{}
			;

		public
			CostItem[]
			MiscellaneousCostsItems = new CostItem[]{}
			;

		public
			CostItem[]
			OtherCostsItems = new CostItem[]{}
			;

		public
			CostItem[]
			HoldingCostsOtherItems = new CostItem[]{}
			;

		public
			CostItem[]
			FinancingCostsItems = new CostItem[]{}
			;

	} // class
} // namespace
