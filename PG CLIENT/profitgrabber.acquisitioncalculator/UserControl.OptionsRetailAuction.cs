using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;
using System.Globalization;

namespace WindowsApplication.AcquisitionCalculator
{
	/// <summary>
	/// Summary description for UserControl.
	/// </summary>
	public class UserControl_OptionsRetailAuction : System.Windows.Forms.UserControl
	{
		private AcquisitionCalculator.Options	option_retail_auction;
		private int business_logic_counter;
		private	decimal	dec_worth_by_comps;

		private System.Windows.Forms.GroupBox groupBoxExitStrategy;
		private System.Windows.Forms.RadioButton radioButtonRetail;
		private System.Windows.Forms.RadioButton radioButtonAuction;
		private System.Windows.Forms.Label labelProjectedResalePrice;
		private System.Windows.Forms.Label labelProjectedNumberOfMonths;
		private System.Windows.Forms.Label labelMurphyFactor;
		private System.Windows.Forms.Button buttonBreakDownMiscellaneousCosts;
		private System.Windows.Forms.Button buttonBreakDownClosingCostsAtResale;
		private System.Windows.Forms.Button buttonBreakDownClosingCostsAtPurchase;
		private System.Windows.Forms.Button buttonBreakDownHousePreparationCosts;
		private System.Windows.Forms.Button buttonBreakDownBoxSellingFees;
		private System.Windows.Forms.Button buttonBreakDownAdvertisingCosts;
		private System.Windows.Forms.Label labelMiscellaneousCosts;
		private System.Windows.Forms.Label labelHousePreparationCosts;
		private System.Windows.Forms.Label labelClosingCostsAtResale;
		private System.Windows.Forms.Label labelClosingCostsAtPurchase;
		private System.Windows.Forms.Label labelSellingFees;
		private System.Windows.Forms.Label labelAdvertisingCosts;
		private System.Windows.Forms.Label labelTransactionalCosts;
		private System.Windows.Forms.Label labelDesiredProfitInPercentage;
		private System.Windows.Forms.Label labelDesiredProfitInBucks;
		private System.Windows.Forms.Label labelEquitySharingRatios;
		private System.Windows.Forms.Label labelOptionorSellerReceives2;
		private System.Windows.Forms.Label labelOptionorSellerReceives;
		private System.Windows.Forms.Label labelOptioneeBuyerReceives2;
		private System.Windows.Forms.Label labelOptioneeBuyerReceives;
		private System.Windows.Forms.GroupBox groupBoxEquitySharing;
		private System.Windows.Forms.RadioButton radioButtonEquitySharingNo;
		private System.Windows.Forms.RadioButton radioButtonEquitySharingYes;
		private System.Windows.Forms.Label labelEquitySharing;
		private System.Windows.Forms.Label labelTotalProfit;
		private System.Windows.Forms.Label labelSellersOptionrsTotalProfit;
		private System.Windows.Forms.Label labelMaxPurchaseOptionPrice_blue;
		private System.Windows.Forms.Label labelPurchasePrice;
		private System.Windows.Forms.Label labelAgreedReservePrice;
		private System.Windows.Forms.Label labelMaxPurchaseOptionPrice_red;
		private TextBox textBoxProjectedResalePrice;
		private TextBox textBoxProjectedNumberOfMonths;
		private TextBox textBoxAdvertisingCosts;
		private TextBox textBoxClosingCostsAtPurchase;
		private TextBox textBoxClosingCostsAtResale;
		private TextBox textBoxHousePreparationCosts;
		private TextBox textBoxMiscellaneousCosts;
		private TextBox textBoxSellingFees;
		private TextBox textBoxMurphyFactor;
		private TextBox textBoxTotalTransactionalCosts;
		private TextBox textBoxlDesiredProfitInPercentage;
		private TextBox textBoxDesiredProfitInBucks;
		private TextBox textBoxMaxPurchaseOptionPrice_blue;
		private TextBox textBoxMaxPurchaseOptionPrice_red;
		private TextBox textBoxDesiredGuaranteedProfitInPercentage;
		private TextBox textBoxAgreedReservePrice;
		private TextBox textBoxOptionorSellerReceives;
		private TextBox textBoxOptioneeBuyerReceives;
		private TextBox textBoxSellersOptionrsTotalProfit;
		private TextBox textBoxTotalProfit;
		private TextBox textBoxDesiredGuaranteedProfitInBucks;
		private System.Windows.Forms.Label labelDesiredGuaranteedProfitInBucks;
		private System.Windows.Forms.Label labelDesiredGuaranteedProfitInPercentage;
		private System.Windows.Forms.Label labelRetailAuctionPercentage;
		private WindowsApplication.AcquisitionCalculator.TextBox textBoxRetailAuctionPercentage;
		private System.Windows.Forms.Label labelTotalTransactionalCosts;
		private System.Windows.Forms.PictureBox pictureBox1;
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public UserControl_OptionsRetailAuction()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

			// TODO: Add any initialization after the InitializeComponent call
			option_retail_auction = new Options();
			business_logic_counter = 0;
			BussinesLogic(radioButtonEquitySharingYes, new System.EventArgs());
		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UserControl_OptionsRetailAuction));
            this.groupBoxExitStrategy = new System.Windows.Forms.GroupBox();
            this.radioButtonAuction = new System.Windows.Forms.RadioButton();
            this.radioButtonRetail = new System.Windows.Forms.RadioButton();
            this.labelRetailAuctionPercentage = new System.Windows.Forms.Label();
            this.labelProjectedResalePrice = new System.Windows.Forms.Label();
            this.labelProjectedNumberOfMonths = new System.Windows.Forms.Label();
            this.labelEquitySharing = new System.Windows.Forms.Label();
            this.groupBoxEquitySharing = new System.Windows.Forms.GroupBox();
            this.radioButtonEquitySharingNo = new System.Windows.Forms.RadioButton();
            this.radioButtonEquitySharingYes = new System.Windows.Forms.RadioButton();
            this.labelMurphyFactor = new System.Windows.Forms.Label();
            this.labelMaxPurchaseOptionPrice_blue = new System.Windows.Forms.Label();
            this.buttonBreakDownMiscellaneousCosts = new System.Windows.Forms.Button();
            this.buttonBreakDownClosingCostsAtResale = new System.Windows.Forms.Button();
            this.buttonBreakDownClosingCostsAtPurchase = new System.Windows.Forms.Button();
            this.buttonBreakDownHousePreparationCosts = new System.Windows.Forms.Button();
            this.buttonBreakDownBoxSellingFees = new System.Windows.Forms.Button();
            this.buttonBreakDownAdvertisingCosts = new System.Windows.Forms.Button();
            this.labelMiscellaneousCosts = new System.Windows.Forms.Label();
            this.labelHousePreparationCosts = new System.Windows.Forms.Label();
            this.labelClosingCostsAtResale = new System.Windows.Forms.Label();
            this.labelClosingCostsAtPurchase = new System.Windows.Forms.Label();
            this.labelSellingFees = new System.Windows.Forms.Label();
            this.labelAdvertisingCosts = new System.Windows.Forms.Label();
            this.labelTransactionalCosts = new System.Windows.Forms.Label();
            this.labelDesiredProfitInPercentage = new System.Windows.Forms.Label();
            this.labelDesiredProfitInBucks = new System.Windows.Forms.Label();
            this.labelEquitySharingRatios = new System.Windows.Forms.Label();
            this.labelOptionorSellerReceives2 = new System.Windows.Forms.Label();
            this.labelOptionorSellerReceives = new System.Windows.Forms.Label();
            this.labelOptioneeBuyerReceives2 = new System.Windows.Forms.Label();
            this.labelOptioneeBuyerReceives = new System.Windows.Forms.Label();
            this.labelSellersOptionrsTotalProfit = new System.Windows.Forms.Label();
            this.labelTotalProfit = new System.Windows.Forms.Label();
            this.labelPurchasePrice = new System.Windows.Forms.Label();
            this.labelAgreedReservePrice = new System.Windows.Forms.Label();
            this.labelMaxPurchaseOptionPrice_red = new System.Windows.Forms.Label();
            this.textBoxRetailAuctionPercentage = new WindowsApplication.AcquisitionCalculator.TextBox();
            this.textBoxProjectedResalePrice = new WindowsApplication.AcquisitionCalculator.TextBox();
            this.textBoxProjectedNumberOfMonths = new WindowsApplication.AcquisitionCalculator.TextBox();
            this.textBoxAdvertisingCosts = new WindowsApplication.AcquisitionCalculator.TextBox();
            this.textBoxSellingFees = new WindowsApplication.AcquisitionCalculator.TextBox();
            this.textBoxClosingCostsAtPurchase = new WindowsApplication.AcquisitionCalculator.TextBox();
            this.textBoxClosingCostsAtResale = new WindowsApplication.AcquisitionCalculator.TextBox();
            this.textBoxHousePreparationCosts = new WindowsApplication.AcquisitionCalculator.TextBox();
            this.textBoxMiscellaneousCosts = new WindowsApplication.AcquisitionCalculator.TextBox();
            this.textBoxDesiredProfitInBucks = new WindowsApplication.AcquisitionCalculator.TextBox();
            this.textBoxlDesiredProfitInPercentage = new WindowsApplication.AcquisitionCalculator.TextBox();
            this.textBoxMaxPurchaseOptionPrice_blue = new WindowsApplication.AcquisitionCalculator.TextBox();
            this.textBoxMurphyFactor = new WindowsApplication.AcquisitionCalculator.TextBox();
            this.textBoxTotalTransactionalCosts = new WindowsApplication.AcquisitionCalculator.TextBox();
            this.textBoxDesiredGuaranteedProfitInPercentage = new WindowsApplication.AcquisitionCalculator.TextBox();
            this.textBoxAgreedReservePrice = new WindowsApplication.AcquisitionCalculator.TextBox();
            this.textBoxOptionorSellerReceives = new WindowsApplication.AcquisitionCalculator.TextBox();
            this.textBoxOptioneeBuyerReceives = new WindowsApplication.AcquisitionCalculator.TextBox();
            this.textBoxTotalProfit = new WindowsApplication.AcquisitionCalculator.TextBox();
            this.textBoxSellersOptionrsTotalProfit = new WindowsApplication.AcquisitionCalculator.TextBox();
            this.textBoxMaxPurchaseOptionPrice_red = new WindowsApplication.AcquisitionCalculator.TextBox();
            this.textBoxDesiredGuaranteedProfitInBucks = new WindowsApplication.AcquisitionCalculator.TextBox();
            this.labelDesiredGuaranteedProfitInBucks = new System.Windows.Forms.Label();
            this.labelDesiredGuaranteedProfitInPercentage = new System.Windows.Forms.Label();
            this.labelTotalTransactionalCosts = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.groupBoxExitStrategy.SuspendLayout();
            this.groupBoxEquitySharing.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBoxExitStrategy
            // 
            this.groupBoxExitStrategy.Controls.Add(this.radioButtonAuction);
            this.groupBoxExitStrategy.Controls.Add(this.radioButtonRetail);
            this.groupBoxExitStrategy.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.groupBoxExitStrategy.Location = new System.Drawing.Point(8, 8);
            this.groupBoxExitStrategy.Name = "groupBoxExitStrategy";
            this.groupBoxExitStrategy.Size = new System.Drawing.Size(192, 56);
            this.groupBoxExitStrategy.TabIndex = 0;
            this.groupBoxExitStrategy.TabStop = false;
            this.groupBoxExitStrategy.Text = "Select Your Exit Strategy:";
            // 
            // radioButtonAuction
            // 
            this.radioButtonAuction.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.radioButtonAuction.Location = new System.Drawing.Point(100, 26);
            this.radioButtonAuction.Name = "radioButtonAuction";
            this.radioButtonAuction.Size = new System.Drawing.Size(80, 16);
            this.radioButtonAuction.TabIndex = 1;
            this.radioButtonAuction.Text = "Auction";
            this.radioButtonAuction.CheckedChanged += new System.EventHandler(this.BussinesLogic);
            // 
            // radioButtonRetail
            // 
            this.radioButtonRetail.Checked = true;
            this.radioButtonRetail.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.radioButtonRetail.Location = new System.Drawing.Point(8, 26);
            this.radioButtonRetail.Name = "radioButtonRetail";
            this.radioButtonRetail.Size = new System.Drawing.Size(64, 16);
            this.radioButtonRetail.TabIndex = 0;
            this.radioButtonRetail.TabStop = true;
            this.radioButtonRetail.Text = "Retail";
            this.radioButtonRetail.CheckedChanged += new System.EventHandler(this.BussinesLogic);
            // 
            // labelRetailAuctionPercentage
            // 
            this.labelRetailAuctionPercentage.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.labelRetailAuctionPercentage.Location = new System.Drawing.Point(280, 16);
            this.labelRetailAuctionPercentage.Name = "labelRetailAuctionPercentage";
            this.labelRetailAuctionPercentage.Size = new System.Drawing.Size(184, 23);
            this.labelRetailAuctionPercentage.TabIndex = 1;
            this.labelRetailAuctionPercentage.Text = "Set Retail Percentage:";
            // 
            // labelProjectedResalePrice
            // 
            this.labelProjectedResalePrice.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelProjectedResalePrice.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.labelProjectedResalePrice.Location = new System.Drawing.Point(616, 16);
            this.labelProjectedResalePrice.Name = "labelProjectedResalePrice";
            this.labelProjectedResalePrice.Size = new System.Drawing.Size(165, 23);
            this.labelProjectedResalePrice.TabIndex = 5;
            this.labelProjectedResalePrice.Text = "Projected Resale Price:";
            // 
            // labelProjectedNumberOfMonths
            // 
            this.labelProjectedNumberOfMonths.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.labelProjectedNumberOfMonths.Location = new System.Drawing.Point(280, 40);
            this.labelProjectedNumberOfMonths.Name = "labelProjectedNumberOfMonths";
            this.labelProjectedNumberOfMonths.Size = new System.Drawing.Size(184, 23);
            this.labelProjectedNumberOfMonths.TabIndex = 3;
            this.labelProjectedNumberOfMonths.Text = "Projected Number of Months:";
            this.labelProjectedNumberOfMonths.Visible = false;
            // 
            // labelEquitySharing
            // 
            this.labelEquitySharing.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelEquitySharing.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.labelEquitySharing.Location = new System.Drawing.Point(0, 336);
            this.labelEquitySharing.Name = "labelEquitySharing";
            this.labelEquitySharing.Size = new System.Drawing.Size(152, 23);
            this.labelEquitySharing.TabIndex = 38;
            this.labelEquitySharing.Text = "EQUITY SHARING:";
            // 
            // groupBoxEquitySharing
            // 
            this.groupBoxEquitySharing.Controls.Add(this.radioButtonEquitySharingNo);
            this.groupBoxEquitySharing.Controls.Add(this.radioButtonEquitySharingYes);
            this.groupBoxEquitySharing.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.groupBoxEquitySharing.Location = new System.Drawing.Point(128, 224);
            this.groupBoxEquitySharing.Name = "groupBoxEquitySharing";
            this.groupBoxEquitySharing.Size = new System.Drawing.Size(456, 48);
            this.groupBoxEquitySharing.TabIndex = 31;
            this.groupBoxEquitySharing.TabStop = false;
            this.groupBoxEquitySharing.Text = "Are You going to share profits with the seller (\"Equity Sharing\")?";
            // 
            // radioButtonEquitySharingNo
            // 
            this.radioButtonEquitySharingNo.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.radioButtonEquitySharingNo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.radioButtonEquitySharingNo.Location = new System.Drawing.Point(80, 22);
            this.radioButtonEquitySharingNo.Name = "radioButtonEquitySharingNo";
            this.radioButtonEquitySharingNo.Size = new System.Drawing.Size(48, 16);
            this.radioButtonEquitySharingNo.TabIndex = 1;
            this.radioButtonEquitySharingNo.Text = "No";
            this.radioButtonEquitySharingNo.CheckedChanged += new System.EventHandler(this.BussinesLogic);
            // 
            // radioButtonEquitySharingYes
            // 
            this.radioButtonEquitySharingYes.Checked = true;
            this.radioButtonEquitySharingYes.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.radioButtonEquitySharingYes.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.radioButtonEquitySharingYes.Location = new System.Drawing.Point(8, 22);
            this.radioButtonEquitySharingYes.Name = "radioButtonEquitySharingYes";
            this.radioButtonEquitySharingYes.Size = new System.Drawing.Size(48, 16);
            this.radioButtonEquitySharingYes.TabIndex = 0;
            this.radioButtonEquitySharingYes.TabStop = true;
            this.radioButtonEquitySharingYes.Text = "Yes";
            this.radioButtonEquitySharingYes.CheckedChanged += new System.EventHandler(this.BussinesLogic);
            // 
            // labelMurphyFactor
            // 
            this.labelMurphyFactor.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.labelMurphyFactor.Location = new System.Drawing.Point(360, 176);
            this.labelMurphyFactor.Name = "labelMurphyFactor";
            this.labelMurphyFactor.Size = new System.Drawing.Size(96, 23);
            this.labelMurphyFactor.TabIndex = 26;
            this.labelMurphyFactor.Text = "Murphy Factor:";
            // 
            // labelMaxPurchaseOptionPrice_blue
            // 
            this.labelMaxPurchaseOptionPrice_blue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMaxPurchaseOptionPrice_blue.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.labelMaxPurchaseOptionPrice_blue.Location = new System.Drawing.Point(472, 288);
            this.labelMaxPurchaseOptionPrice_blue.Name = "labelMaxPurchaseOptionPrice_blue";
            this.labelMaxPurchaseOptionPrice_blue.Size = new System.Drawing.Size(224, 23);
            this.labelMaxPurchaseOptionPrice_blue.TabIndex = 36;
            this.labelMaxPurchaseOptionPrice_blue.Text = "Your MAXIMUM OPTION PRICE:";
            // 
            // buttonBreakDownMiscellaneousCosts
            // 
            this.buttonBreakDownMiscellaneousCosts.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.buttonBreakDownMiscellaneousCosts.Image = ((System.Drawing.Image)(resources.GetObject("buttonBreakDownMiscellaneousCosts.Image")));
            this.buttonBreakDownMiscellaneousCosts.Location = new System.Drawing.Point(864, 128);
            this.buttonBreakDownMiscellaneousCosts.Name = "buttonBreakDownMiscellaneousCosts";
            this.buttonBreakDownMiscellaneousCosts.Size = new System.Drawing.Size(24, 23);
            this.buttonBreakDownMiscellaneousCosts.TabIndex = 25;
            this.buttonBreakDownMiscellaneousCosts.Click += new System.EventHandler(this.buttonBreakDownMiscellaneousCosts_Click);
            // 
            // buttonBreakDownClosingCostsAtResale
            // 
            this.buttonBreakDownClosingCostsAtResale.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.buttonBreakDownClosingCostsAtResale.Image = ((System.Drawing.Image)(resources.GetObject("buttonBreakDownClosingCostsAtResale.Image")));
            this.buttonBreakDownClosingCostsAtResale.Location = new System.Drawing.Point(544, 136);
            this.buttonBreakDownClosingCostsAtResale.Name = "buttonBreakDownClosingCostsAtResale";
            this.buttonBreakDownClosingCostsAtResale.Size = new System.Drawing.Size(24, 23);
            this.buttonBreakDownClosingCostsAtResale.TabIndex = 22;
            this.buttonBreakDownClosingCostsAtResale.Click += new System.EventHandler(this.buttonBreakDownClosingCostsAtResale_Click);
            // 
            // buttonBreakDownClosingCostsAtPurchase
            // 
            this.buttonBreakDownClosingCostsAtPurchase.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.buttonBreakDownClosingCostsAtPurchase.Image = ((System.Drawing.Image)(resources.GetObject("buttonBreakDownClosingCostsAtPurchase.Image")));
            this.buttonBreakDownClosingCostsAtPurchase.Location = new System.Drawing.Point(544, 112);
            this.buttonBreakDownClosingCostsAtPurchase.Name = "buttonBreakDownClosingCostsAtPurchase";
            this.buttonBreakDownClosingCostsAtPurchase.Size = new System.Drawing.Size(24, 23);
            this.buttonBreakDownClosingCostsAtPurchase.TabIndex = 13;
            this.buttonBreakDownClosingCostsAtPurchase.Click += new System.EventHandler(this.buttonBreakDownClosingCostsAtPurchase_Click);
            // 
            // buttonBreakDownHousePreparationCosts
            // 
            this.buttonBreakDownHousePreparationCosts.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.buttonBreakDownHousePreparationCosts.Image = ((System.Drawing.Image)(resources.GetObject("buttonBreakDownHousePreparationCosts.Image")));
            this.buttonBreakDownHousePreparationCosts.Location = new System.Drawing.Point(864, 104);
            this.buttonBreakDownHousePreparationCosts.Name = "buttonBreakDownHousePreparationCosts";
            this.buttonBreakDownHousePreparationCosts.Size = new System.Drawing.Size(24, 23);
            this.buttonBreakDownHousePreparationCosts.TabIndex = 16;
            this.buttonBreakDownHousePreparationCosts.Click += new System.EventHandler(this.buttonBreakDownHousePreparationCosts_Click);
            // 
            // buttonBreakDownBoxSellingFees
            // 
            this.buttonBreakDownBoxSellingFees.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.buttonBreakDownBoxSellingFees.Image = ((System.Drawing.Image)(resources.GetObject("buttonBreakDownBoxSellingFees.Image")));
            this.buttonBreakDownBoxSellingFees.Location = new System.Drawing.Point(208, 136);
            this.buttonBreakDownBoxSellingFees.Name = "buttonBreakDownBoxSellingFees";
            this.buttonBreakDownBoxSellingFees.Size = new System.Drawing.Size(24, 23);
            this.buttonBreakDownBoxSellingFees.TabIndex = 19;
            this.buttonBreakDownBoxSellingFees.Click += new System.EventHandler(this.buttonBreakDownBoxSellingFees_Click);
            // 
            // buttonBreakDownAdvertisingCosts
            // 
            this.buttonBreakDownAdvertisingCosts.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.buttonBreakDownAdvertisingCosts.Image = ((System.Drawing.Image)(resources.GetObject("buttonBreakDownAdvertisingCosts.Image")));
            this.buttonBreakDownAdvertisingCosts.Location = new System.Drawing.Point(208, 112);
            this.buttonBreakDownAdvertisingCosts.Name = "buttonBreakDownAdvertisingCosts";
            this.buttonBreakDownAdvertisingCosts.Size = new System.Drawing.Size(24, 23);
            this.buttonBreakDownAdvertisingCosts.TabIndex = 10;
            this.buttonBreakDownAdvertisingCosts.Click += new System.EventHandler(this.buttonBreakDownAdvertisingCosts_Click);
            // 
            // labelMiscellaneousCosts
            // 
            this.labelMiscellaneousCosts.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.labelMiscellaneousCosts.Location = new System.Drawing.Point(624, 136);
            this.labelMiscellaneousCosts.Name = "labelMiscellaneousCosts";
            this.labelMiscellaneousCosts.Size = new System.Drawing.Size(152, 23);
            this.labelMiscellaneousCosts.TabIndex = 23;
            this.labelMiscellaneousCosts.Text = "Miscellaneous Costs:";
            // 
            // labelHousePreparationCosts
            // 
            this.labelHousePreparationCosts.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.labelHousePreparationCosts.Location = new System.Drawing.Point(624, 112);
            this.labelHousePreparationCosts.Name = "labelHousePreparationCosts";
            this.labelHousePreparationCosts.Size = new System.Drawing.Size(160, 23);
            this.labelHousePreparationCosts.TabIndex = 14;
            this.labelHousePreparationCosts.Text = "House Preparation Costs:";
            // 
            // labelClosingCostsAtResale
            // 
            this.labelClosingCostsAtResale.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.labelClosingCostsAtResale.Location = new System.Drawing.Point(296, 136);
            this.labelClosingCostsAtResale.Name = "labelClosingCostsAtResale";
            this.labelClosingCostsAtResale.Size = new System.Drawing.Size(168, 23);
            this.labelClosingCostsAtResale.TabIndex = 20;
            this.labelClosingCostsAtResale.Text = "Closing Costs At Resale:";
            // 
            // labelClosingCostsAtPurchase
            // 
            this.labelClosingCostsAtPurchase.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.labelClosingCostsAtPurchase.Location = new System.Drawing.Point(296, 112);
            this.labelClosingCostsAtPurchase.Name = "labelClosingCostsAtPurchase";
            this.labelClosingCostsAtPurchase.Size = new System.Drawing.Size(168, 23);
            this.labelClosingCostsAtPurchase.TabIndex = 11;
            this.labelClosingCostsAtPurchase.Text = "Closing Costs At Purchase:";
            // 
            // labelSellingFees
            // 
            this.labelSellingFees.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.labelSellingFees.Location = new System.Drawing.Point(16, 136);
            this.labelSellingFees.Name = "labelSellingFees";
            this.labelSellingFees.Size = new System.Drawing.Size(96, 23);
            this.labelSellingFees.TabIndex = 17;
            this.labelSellingFees.Text = "Selling Fees:";
            // 
            // labelAdvertisingCosts
            // 
            this.labelAdvertisingCosts.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.labelAdvertisingCosts.Location = new System.Drawing.Point(16, 112);
            this.labelAdvertisingCosts.Name = "labelAdvertisingCosts";
            this.labelAdvertisingCosts.Size = new System.Drawing.Size(128, 23);
            this.labelAdvertisingCosts.TabIndex = 8;
            this.labelAdvertisingCosts.Text = "Advertising Costs:";
            // 
            // labelTransactionalCosts
            // 
            this.labelTransactionalCosts.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTransactionalCosts.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.labelTransactionalCosts.Location = new System.Drawing.Point(0, 88);
            this.labelTransactionalCosts.Name = "labelTransactionalCosts";
            this.labelTransactionalCosts.Size = new System.Drawing.Size(200, 23);
            this.labelTransactionalCosts.TabIndex = 7;
            this.labelTransactionalCosts.Text = "TRANSACTIONAL COSTS:";
            // 
            // labelDesiredProfitInPercentage
            // 
            this.labelDesiredProfitInPercentage.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.labelDesiredProfitInPercentage.Location = new System.Drawing.Point(16, 288);
            this.labelDesiredProfitInPercentage.Name = "labelDesiredProfitInPercentage";
            this.labelDesiredProfitInPercentage.Size = new System.Drawing.Size(120, 23);
            this.labelDesiredProfitInPercentage.TabIndex = 32;
            this.labelDesiredProfitInPercentage.Text = "Desired Profit in %:";
            // 
            // labelDesiredProfitInBucks
            // 
            this.labelDesiredProfitInBucks.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.labelDesiredProfitInBucks.Location = new System.Drawing.Point(224, 288);
            this.labelDesiredProfitInBucks.Name = "labelDesiredProfitInBucks";
            this.labelDesiredProfitInBucks.Size = new System.Drawing.Size(120, 23);
            this.labelDesiredProfitInBucks.TabIndex = 34;
            this.labelDesiredProfitInBucks.Text = "Desired Profit in $:";
            // 
            // labelEquitySharingRatios
            // 
            this.labelEquitySharingRatios.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.labelEquitySharingRatios.Location = new System.Drawing.Point(24, 424);
            this.labelEquitySharingRatios.Name = "labelEquitySharingRatios";
            this.labelEquitySharingRatios.Size = new System.Drawing.Size(152, 16);
            this.labelEquitySharingRatios.TabIndex = 45;
            this.labelEquitySharingRatios.Text = "Equity Sharing Ratios:";
            // 
            // labelOptionorSellerReceives2
            // 
            this.labelOptionorSellerReceives2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.labelOptionorSellerReceives2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.labelOptionorSellerReceives2.Location = new System.Drawing.Point(336, 448);
            this.labelOptionorSellerReceives2.Name = "labelOptionorSellerReceives2";
            this.labelOptionorSellerReceives2.Size = new System.Drawing.Size(176, 16);
            this.labelOptionorSellerReceives2.TabIndex = 48;
            this.labelOptionorSellerReceives2.Text = "of Profit above Option Price.";
            // 
            // labelOptionorSellerReceives
            // 
            this.labelOptionorSellerReceives.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.labelOptionorSellerReceives.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.labelOptionorSellerReceives.Location = new System.Drawing.Point(80, 448);
            this.labelOptionorSellerReceives.Name = "labelOptionorSellerReceives";
            this.labelOptionorSellerReceives.Size = new System.Drawing.Size(160, 16);
            this.labelOptionorSellerReceives.TabIndex = 46;
            this.labelOptionorSellerReceives.Text = "Optionor (Seller) receives:";
            // 
            // labelOptioneeBuyerReceives2
            // 
            this.labelOptioneeBuyerReceives2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.labelOptioneeBuyerReceives2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.labelOptioneeBuyerReceives2.Location = new System.Drawing.Point(336, 472);
            this.labelOptioneeBuyerReceives2.Name = "labelOptioneeBuyerReceives2";
            this.labelOptioneeBuyerReceives2.Size = new System.Drawing.Size(176, 16);
            this.labelOptioneeBuyerReceives2.TabIndex = 51;
            this.labelOptioneeBuyerReceives2.Text = "of Profit above Option Price.";
            // 
            // labelOptioneeBuyerReceives
            // 
            this.labelOptioneeBuyerReceives.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.labelOptioneeBuyerReceives.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.labelOptioneeBuyerReceives.Location = new System.Drawing.Point(80, 472);
            this.labelOptioneeBuyerReceives.Name = "labelOptioneeBuyerReceives";
            this.labelOptioneeBuyerReceives.Size = new System.Drawing.Size(168, 16);
            this.labelOptioneeBuyerReceives.TabIndex = 49;
            this.labelOptioneeBuyerReceives.Text = "Optionee (Buyer) receives:";
            // 
            // labelSellersOptionrsTotalProfit
            // 
            this.labelSellersOptionrsTotalProfit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.labelSellersOptionrsTotalProfit.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.labelSellersOptionrsTotalProfit.Location = new System.Drawing.Point(376, 496);
            this.labelSellersOptionrsTotalProfit.Name = "labelSellersOptionrsTotalProfit";
            this.labelSellersOptionrsTotalProfit.Size = new System.Drawing.Size(120, 18);
            this.labelSellersOptionrsTotalProfit.TabIndex = 54;
            this.labelSellersOptionrsTotalProfit.Text = "Seller\'s Total Profit:";
            // 
            // labelTotalProfit
            // 
            this.labelTotalProfit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.labelTotalProfit.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.labelTotalProfit.Location = new System.Drawing.Point(128, 496);
            this.labelTotalProfit.Name = "labelTotalProfit";
            this.labelTotalProfit.Size = new System.Drawing.Size(105, 18);
            this.labelTotalProfit.TabIndex = 52;
            this.labelTotalProfit.Text = "Your Total Profit:";
            // 
            // labelPurchasePrice
            // 
            this.labelPurchasePrice.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPurchasePrice.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.labelPurchasePrice.Location = new System.Drawing.Point(0, 224);
            this.labelPurchasePrice.Name = "labelPurchasePrice";
            this.labelPurchasePrice.Size = new System.Drawing.Size(128, 23);
            this.labelPurchasePrice.TabIndex = 30;
            this.labelPurchasePrice.Text = "OPTION PRICE:";
            // 
            // labelAgreedReservePrice
            // 
            this.labelAgreedReservePrice.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.labelAgreedReservePrice.Location = new System.Drawing.Point(24, 392);
            this.labelAgreedReservePrice.Name = "labelAgreedReservePrice";
            this.labelAgreedReservePrice.Size = new System.Drawing.Size(193, 16);
            this.labelAgreedReservePrice.TabIndex = 43;
            this.labelAgreedReservePrice.Text = "Agreed Reserve Price:";
            // 
            // labelMaxPurchaseOptionPrice_red
            // 
            this.labelMaxPurchaseOptionPrice_red.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMaxPurchaseOptionPrice_red.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.labelMaxPurchaseOptionPrice_red.Location = new System.Drawing.Point(560, 538);
            this.labelMaxPurchaseOptionPrice_red.Name = "labelMaxPurchaseOptionPrice_red";
            this.labelMaxPurchaseOptionPrice_red.Size = new System.Drawing.Size(229, 22);
            this.labelMaxPurchaseOptionPrice_red.TabIndex = 56;
            this.labelMaxPurchaseOptionPrice_red.Text = "Your MAXIMUM OPTION PRICE:";
            // 
            // textBoxRetailAuctionPercentage
            // 
            this.textBoxRetailAuctionPercentage.Location = new System.Drawing.Point(464, 16);
            this.textBoxRetailAuctionPercentage.Name = "textBoxRetailAuctionPercentage";
            this.textBoxRetailAuctionPercentage.Size = new System.Drawing.Size(80, 20);
            this.textBoxRetailAuctionPercentage.TabIndex = 2;
            this.textBoxRetailAuctionPercentage.Text = "100%";
            this.textBoxRetailAuctionPercentage.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textBoxRetailAuctionPercentage.Leave += new System.EventHandler(this.BussinesLogic);
            // 
            // textBoxProjectedResalePrice
            // 
            this.textBoxProjectedResalePrice.Location = new System.Drawing.Point(784, 16);
            this.textBoxProjectedResalePrice.Name = "textBoxProjectedResalePrice";
            this.textBoxProjectedResalePrice.ReadOnly = true;
            this.textBoxProjectedResalePrice.Size = new System.Drawing.Size(80, 20);
            this.textBoxProjectedResalePrice.TabIndex = 6;
            this.textBoxProjectedResalePrice.Text = "$0.00";
            this.textBoxProjectedResalePrice.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textBoxProjectedResalePrice.Leave += new System.EventHandler(this.BussinesLogic);
            // 
            // textBoxProjectedNumberOfMonths
            // 
            this.textBoxProjectedNumberOfMonths.Location = new System.Drawing.Point(464, 40);
            this.textBoxProjectedNumberOfMonths.Name = "textBoxProjectedNumberOfMonths";
            this.textBoxProjectedNumberOfMonths.Size = new System.Drawing.Size(80, 20);
            this.textBoxProjectedNumberOfMonths.TabIndex = 4;
            this.textBoxProjectedNumberOfMonths.Text = "0";
            this.textBoxProjectedNumberOfMonths.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textBoxProjectedNumberOfMonths.Visible = false;
            // 
            // textBoxAdvertisingCosts
            // 
            this.textBoxAdvertisingCosts.Location = new System.Drawing.Point(128, 112);
            this.textBoxAdvertisingCosts.Name = "textBoxAdvertisingCosts";
            this.textBoxAdvertisingCosts.Size = new System.Drawing.Size(80, 20);
            this.textBoxAdvertisingCosts.TabIndex = 9;
            this.textBoxAdvertisingCosts.Text = "$0.00";
            this.textBoxAdvertisingCosts.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textBoxAdvertisingCosts.Leave += new System.EventHandler(this.BussinesLogic);
            // 
            // textBoxSellingFees
            // 
            this.textBoxSellingFees.Location = new System.Drawing.Point(128, 136);
            this.textBoxSellingFees.Name = "textBoxSellingFees";
            this.textBoxSellingFees.Size = new System.Drawing.Size(80, 20);
            this.textBoxSellingFees.TabIndex = 18;
            this.textBoxSellingFees.Text = "$0.00";
            this.textBoxSellingFees.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textBoxSellingFees.Leave += new System.EventHandler(this.BussinesLogic);
            // 
            // textBoxClosingCostsAtPurchase
            // 
            this.textBoxClosingCostsAtPurchase.Location = new System.Drawing.Point(464, 112);
            this.textBoxClosingCostsAtPurchase.Name = "textBoxClosingCostsAtPurchase";
            this.textBoxClosingCostsAtPurchase.Size = new System.Drawing.Size(80, 20);
            this.textBoxClosingCostsAtPurchase.TabIndex = 12;
            this.textBoxClosingCostsAtPurchase.Text = "$0.00";
            this.textBoxClosingCostsAtPurchase.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textBoxClosingCostsAtPurchase.Leave += new System.EventHandler(this.BussinesLogic);
            // 
            // textBoxClosingCostsAtResale
            // 
            this.textBoxClosingCostsAtResale.Location = new System.Drawing.Point(464, 136);
            this.textBoxClosingCostsAtResale.Name = "textBoxClosingCostsAtResale";
            this.textBoxClosingCostsAtResale.Size = new System.Drawing.Size(80, 20);
            this.textBoxClosingCostsAtResale.TabIndex = 21;
            this.textBoxClosingCostsAtResale.Text = "$0.00";
            this.textBoxClosingCostsAtResale.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textBoxClosingCostsAtResale.Leave += new System.EventHandler(this.BussinesLogic);
            // 
            // textBoxHousePreparationCosts
            // 
            this.textBoxHousePreparationCosts.Location = new System.Drawing.Point(784, 112);
            this.textBoxHousePreparationCosts.Name = "textBoxHousePreparationCosts";
            this.textBoxHousePreparationCosts.Size = new System.Drawing.Size(80, 20);
            this.textBoxHousePreparationCosts.TabIndex = 15;
            this.textBoxHousePreparationCosts.Text = "$0.00";
            this.textBoxHousePreparationCosts.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textBoxHousePreparationCosts.Leave += new System.EventHandler(this.BussinesLogic);
            // 
            // textBoxMiscellaneousCosts
            // 
            this.textBoxMiscellaneousCosts.Location = new System.Drawing.Point(784, 136);
            this.textBoxMiscellaneousCosts.Name = "textBoxMiscellaneousCosts";
            this.textBoxMiscellaneousCosts.Size = new System.Drawing.Size(80, 20);
            this.textBoxMiscellaneousCosts.TabIndex = 24;
            this.textBoxMiscellaneousCosts.Text = "$0.00";
            this.textBoxMiscellaneousCosts.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textBoxMiscellaneousCosts.Leave += new System.EventHandler(this.BussinesLogic);
            // 
            // textBoxDesiredProfitInBucks
            // 
            this.textBoxDesiredProfitInBucks.Location = new System.Drawing.Point(352, 288);
            this.textBoxDesiredProfitInBucks.Name = "textBoxDesiredProfitInBucks";
            this.textBoxDesiredProfitInBucks.ReadOnly = true;
            this.textBoxDesiredProfitInBucks.Size = new System.Drawing.Size(80, 20);
            this.textBoxDesiredProfitInBucks.TabIndex = 35;
            this.textBoxDesiredProfitInBucks.Text = "$0.00";
            this.textBoxDesiredProfitInBucks.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textBoxDesiredProfitInBucks.Leave += new System.EventHandler(this.BussinesLogic);
            // 
            // textBoxlDesiredProfitInPercentage
            // 
            this.textBoxlDesiredProfitInPercentage.Location = new System.Drawing.Point(136, 288);
            this.textBoxlDesiredProfitInPercentage.Name = "textBoxlDesiredProfitInPercentage";
            this.textBoxlDesiredProfitInPercentage.Size = new System.Drawing.Size(60, 20);
            this.textBoxlDesiredProfitInPercentage.TabIndex = 33;
            this.textBoxlDesiredProfitInPercentage.Text = "10%";
            this.textBoxlDesiredProfitInPercentage.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textBoxlDesiredProfitInPercentage.Leave += new System.EventHandler(this.BussinesLogic);
            // 
            // textBoxMaxPurchaseOptionPrice_blue
            // 
            this.textBoxMaxPurchaseOptionPrice_blue.BackColor = System.Drawing.SystemColors.Control;
            this.textBoxMaxPurchaseOptionPrice_blue.ForeColor = System.Drawing.SystemColors.WindowText;
            this.textBoxMaxPurchaseOptionPrice_blue.Location = new System.Drawing.Point(704, 288);
            this.textBoxMaxPurchaseOptionPrice_blue.Name = "textBoxMaxPurchaseOptionPrice_blue";
            this.textBoxMaxPurchaseOptionPrice_blue.ReadOnly = true;
            this.textBoxMaxPurchaseOptionPrice_blue.Size = new System.Drawing.Size(80, 20);
            this.textBoxMaxPurchaseOptionPrice_blue.TabIndex = 37;
            this.textBoxMaxPurchaseOptionPrice_blue.Text = "$0.00";
            this.textBoxMaxPurchaseOptionPrice_blue.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBoxMurphyFactor
            // 
            this.textBoxMurphyFactor.Location = new System.Drawing.Point(464, 176);
            this.textBoxMurphyFactor.Name = "textBoxMurphyFactor";
            this.textBoxMurphyFactor.Size = new System.Drawing.Size(80, 20);
            this.textBoxMurphyFactor.TabIndex = 27;
            this.textBoxMurphyFactor.Text = "0%";
            this.textBoxMurphyFactor.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textBoxMurphyFactor.Leave += new System.EventHandler(this.BussinesLogic);
            // 
            // textBoxTotalTransactionalCosts
            // 
            this.textBoxTotalTransactionalCosts.BackColor = System.Drawing.SystemColors.Control;
            this.textBoxTotalTransactionalCosts.ForeColor = System.Drawing.SystemColors.WindowText;
            this.textBoxTotalTransactionalCosts.Location = new System.Drawing.Point(784, 176);
            this.textBoxTotalTransactionalCosts.Name = "textBoxTotalTransactionalCosts";
            this.textBoxTotalTransactionalCosts.ReadOnly = true;
            this.textBoxTotalTransactionalCosts.Size = new System.Drawing.Size(80, 20);
            this.textBoxTotalTransactionalCosts.TabIndex = 29;
            this.textBoxTotalTransactionalCosts.Text = "$0.00";
            this.textBoxTotalTransactionalCosts.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBoxDesiredGuaranteedProfitInPercentage
            // 
            this.textBoxDesiredGuaranteedProfitInPercentage.Location = new System.Drawing.Point(224, 368);
            this.textBoxDesiredGuaranteedProfitInPercentage.Name = "textBoxDesiredGuaranteedProfitInPercentage";
            this.textBoxDesiredGuaranteedProfitInPercentage.Size = new System.Drawing.Size(80, 20);
            this.textBoxDesiredGuaranteedProfitInPercentage.TabIndex = 40;
            this.textBoxDesiredGuaranteedProfitInPercentage.Text = "10%";
            this.textBoxDesiredGuaranteedProfitInPercentage.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textBoxDesiredGuaranteedProfitInPercentage.Leave += new System.EventHandler(this.BussinesLogic);
            // 
            // textBoxAgreedReservePrice
            // 
            this.textBoxAgreedReservePrice.Location = new System.Drawing.Point(224, 392);
            this.textBoxAgreedReservePrice.Name = "textBoxAgreedReservePrice";
            this.textBoxAgreedReservePrice.Size = new System.Drawing.Size(80, 20);
            this.textBoxAgreedReservePrice.TabIndex = 44;
            this.textBoxAgreedReservePrice.Text = "$0.00";
            this.textBoxAgreedReservePrice.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textBoxAgreedReservePrice.Leave += new System.EventHandler(this.BussinesLogic);
            // 
            // textBoxOptionorSellerReceives
            // 
            this.textBoxOptionorSellerReceives.Location = new System.Drawing.Point(248, 448);
            this.textBoxOptionorSellerReceives.Name = "textBoxOptionorSellerReceives";
            this.textBoxOptionorSellerReceives.Size = new System.Drawing.Size(80, 20);
            this.textBoxOptionorSellerReceives.TabIndex = 47;
            this.textBoxOptionorSellerReceives.Text = "0%";
            this.textBoxOptionorSellerReceives.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textBoxOptionorSellerReceives.Leave += new System.EventHandler(this.BussinesLogic);
            // 
            // textBoxOptioneeBuyerReceives
            // 
            this.textBoxOptioneeBuyerReceives.Location = new System.Drawing.Point(248, 472);
            this.textBoxOptioneeBuyerReceives.Name = "textBoxOptioneeBuyerReceives";
            this.textBoxOptioneeBuyerReceives.Size = new System.Drawing.Size(80, 20);
            this.textBoxOptioneeBuyerReceives.TabIndex = 50;
            this.textBoxOptioneeBuyerReceives.Text = "0%";
            this.textBoxOptioneeBuyerReceives.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textBoxOptioneeBuyerReceives.Leave += new System.EventHandler(this.BussinesLogic);
            // 
            // textBoxTotalProfit
            // 
            this.textBoxTotalProfit.BackColor = System.Drawing.SystemColors.Control;
            this.textBoxTotalProfit.ForeColor = System.Drawing.SystemColors.WindowText;
            this.textBoxTotalProfit.Location = new System.Drawing.Point(248, 496);
            this.textBoxTotalProfit.Name = "textBoxTotalProfit";
            this.textBoxTotalProfit.ReadOnly = true;
            this.textBoxTotalProfit.Size = new System.Drawing.Size(80, 20);
            this.textBoxTotalProfit.TabIndex = 53;
            this.textBoxTotalProfit.Text = "$0.00";
            this.textBoxTotalProfit.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBoxSellersOptionrsTotalProfit
            // 
            this.textBoxSellersOptionrsTotalProfit.BackColor = System.Drawing.SystemColors.Control;
            this.textBoxSellersOptionrsTotalProfit.ForeColor = System.Drawing.SystemColors.WindowText;
            this.textBoxSellersOptionrsTotalProfit.Location = new System.Drawing.Point(504, 496);
            this.textBoxSellersOptionrsTotalProfit.Name = "textBoxSellersOptionrsTotalProfit";
            this.textBoxSellersOptionrsTotalProfit.ReadOnly = true;
            this.textBoxSellersOptionrsTotalProfit.Size = new System.Drawing.Size(80, 20);
            this.textBoxSellersOptionrsTotalProfit.TabIndex = 55;
            this.textBoxSellersOptionrsTotalProfit.Text = "$0.00";
            this.textBoxSellersOptionrsTotalProfit.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBoxMaxPurchaseOptionPrice_red
            // 
            this.textBoxMaxPurchaseOptionPrice_red.BackColor = System.Drawing.SystemColors.Control;
            this.textBoxMaxPurchaseOptionPrice_red.ForeColor = System.Drawing.SystemColors.WindowText;
            this.textBoxMaxPurchaseOptionPrice_red.Location = new System.Drawing.Point(800, 536);
            this.textBoxMaxPurchaseOptionPrice_red.Name = "textBoxMaxPurchaseOptionPrice_red";
            this.textBoxMaxPurchaseOptionPrice_red.ReadOnly = true;
            this.textBoxMaxPurchaseOptionPrice_red.Size = new System.Drawing.Size(80, 20);
            this.textBoxMaxPurchaseOptionPrice_red.TabIndex = 57;
            this.textBoxMaxPurchaseOptionPrice_red.Text = "$0.00";
            this.textBoxMaxPurchaseOptionPrice_red.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBoxDesiredGuaranteedProfitInBucks
            // 
            this.textBoxDesiredGuaranteedProfitInBucks.BackColor = System.Drawing.SystemColors.Control;
            this.textBoxDesiredGuaranteedProfitInBucks.ForeColor = System.Drawing.SystemColors.WindowText;
            this.textBoxDesiredGuaranteedProfitInBucks.Location = new System.Drawing.Point(592, 368);
            this.textBoxDesiredGuaranteedProfitInBucks.Name = "textBoxDesiredGuaranteedProfitInBucks";
            this.textBoxDesiredGuaranteedProfitInBucks.ReadOnly = true;
            this.textBoxDesiredGuaranteedProfitInBucks.Size = new System.Drawing.Size(80, 20);
            this.textBoxDesiredGuaranteedProfitInBucks.TabIndex = 42;
            this.textBoxDesiredGuaranteedProfitInBucks.Text = "$0.00";
            this.textBoxDesiredGuaranteedProfitInBucks.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelDesiredGuaranteedProfitInBucks
            // 
            this.labelDesiredGuaranteedProfitInBucks.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.labelDesiredGuaranteedProfitInBucks.Location = new System.Drawing.Point(400, 368);
            this.labelDesiredGuaranteedProfitInBucks.Name = "labelDesiredGuaranteedProfitInBucks";
            this.labelDesiredGuaranteedProfitInBucks.Size = new System.Drawing.Size(192, 16);
            this.labelDesiredGuaranteedProfitInBucks.TabIndex = 41;
            this.labelDesiredGuaranteedProfitInBucks.Text = "Desired Guaranteed Profit in $:";
            // 
            // labelDesiredGuaranteedProfitInPercentage
            // 
            this.labelDesiredGuaranteedProfitInPercentage.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.labelDesiredGuaranteedProfitInPercentage.Location = new System.Drawing.Point(24, 368);
            this.labelDesiredGuaranteedProfitInPercentage.Name = "labelDesiredGuaranteedProfitInPercentage";
            this.labelDesiredGuaranteedProfitInPercentage.Size = new System.Drawing.Size(193, 14);
            this.labelDesiredGuaranteedProfitInPercentage.TabIndex = 39;
            this.labelDesiredGuaranteedProfitInPercentage.Text = "Desired Guaranteed Profit in %:";
            // 
            // labelTotalTransactionalCosts
            // 
            this.labelTotalTransactionalCosts.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTotalTransactionalCosts.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.labelTotalTransactionalCosts.Location = new System.Drawing.Point(632, 176);
            this.labelTotalTransactionalCosts.Name = "labelTotalTransactionalCosts";
            this.labelTotalTransactionalCosts.Size = new System.Drawing.Size(144, 23);
            this.labelTotalTransactionalCosts.TabIndex = 28;
            this.labelTotalTransactionalCosts.Text = "Transactional Costs:";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(884, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(16, 16);
            this.pictureBox1.TabIndex = 90;
            this.pictureBox1.TabStop = false;
            // 
            // UserControl_OptionsRetailAuction
            // 
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.Controls.Add(this.textBoxProjectedNumberOfMonths);
            this.Controls.Add(this.textBoxRetailAuctionPercentage);
            this.Controls.Add(this.textBoxSellingFees);
            this.Controls.Add(this.textBoxAdvertisingCosts);
            this.Controls.Add(this.buttonBreakDownBoxSellingFees);
            this.Controls.Add(this.buttonBreakDownAdvertisingCosts);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.textBoxTotalTransactionalCosts);
            this.Controls.Add(this.labelTotalTransactionalCosts);
            this.Controls.Add(this.textBoxDesiredGuaranteedProfitInPercentage);
            this.Controls.Add(this.labelDesiredGuaranteedProfitInPercentage);
            this.Controls.Add(this.textBoxDesiredGuaranteedProfitInBucks);
            this.Controls.Add(this.labelDesiredGuaranteedProfitInBucks);
            this.Controls.Add(this.textBoxMaxPurchaseOptionPrice_red);
            this.Controls.Add(this.textBoxSellersOptionrsTotalProfit);
            this.Controls.Add(this.textBoxTotalProfit);
            this.Controls.Add(this.textBoxOptioneeBuyerReceives);
            this.Controls.Add(this.textBoxOptionorSellerReceives);
            this.Controls.Add(this.textBoxAgreedReservePrice);
            this.Controls.Add(this.textBoxMurphyFactor);
            this.Controls.Add(this.textBoxMaxPurchaseOptionPrice_blue);
            this.Controls.Add(this.textBoxlDesiredProfitInPercentage);
            this.Controls.Add(this.textBoxDesiredProfitInBucks);
            this.Controls.Add(this.textBoxMiscellaneousCosts);
            this.Controls.Add(this.textBoxHousePreparationCosts);
            this.Controls.Add(this.textBoxClosingCostsAtResale);
            this.Controls.Add(this.textBoxClosingCostsAtPurchase);
            this.Controls.Add(this.textBoxProjectedResalePrice);
            this.Controls.Add(this.labelMaxPurchaseOptionPrice_red);
            this.Controls.Add(this.labelAgreedReservePrice);
            this.Controls.Add(this.labelPurchasePrice);
            this.Controls.Add(this.labelSellersOptionrsTotalProfit);
            this.Controls.Add(this.labelTotalProfit);
            this.Controls.Add(this.labelOptioneeBuyerReceives2);
            this.Controls.Add(this.labelOptioneeBuyerReceives);
            this.Controls.Add(this.labelOptionorSellerReceives2);
            this.Controls.Add(this.labelOptionorSellerReceives);
            this.Controls.Add(this.labelEquitySharingRatios);
            this.Controls.Add(this.buttonBreakDownMiscellaneousCosts);
            this.Controls.Add(this.buttonBreakDownClosingCostsAtResale);
            this.Controls.Add(this.buttonBreakDownClosingCostsAtPurchase);
            this.Controls.Add(this.buttonBreakDownHousePreparationCosts);
            this.Controls.Add(this.labelMiscellaneousCosts);
            this.Controls.Add(this.labelHousePreparationCosts);
            this.Controls.Add(this.labelClosingCostsAtResale);
            this.Controls.Add(this.labelClosingCostsAtPurchase);
            this.Controls.Add(this.labelSellingFees);
            this.Controls.Add(this.labelAdvertisingCosts);
            this.Controls.Add(this.labelTransactionalCosts);
            this.Controls.Add(this.labelMaxPurchaseOptionPrice_blue);
            this.Controls.Add(this.labelMurphyFactor);
            this.Controls.Add(this.labelEquitySharing);
            this.Controls.Add(this.labelProjectedNumberOfMonths);
            this.Controls.Add(this.labelProjectedResalePrice);
            this.Controls.Add(this.labelRetailAuctionPercentage);
            this.Controls.Add(this.groupBoxExitStrategy);
            this.Controls.Add(this.groupBoxEquitySharing);
            this.Controls.Add(this.labelDesiredProfitInBucks);
            this.Controls.Add(this.labelDesiredProfitInPercentage);
            this.Name = "UserControl_OptionsRetailAuction";
            this.Size = new System.Drawing.Size(900, 580);
            this.groupBoxExitStrategy.ResumeLayout(false);
            this.groupBoxEquitySharing.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		#endregion

		private void buttonBreakDownAdvertisingCosts_Click(object sender, System.EventArgs e)
		{
			FormBreakDown fbd = new FormBreakDown();
			fbd.userControl_BreakDown.DataArrayCostItems = option_retail_auction.AdvertisingCostsItems;
			fbd.ShowDialog();
			option_retail_auction.AdvertisingCostsItems = fbd.userControl_BreakDown.DataArrayCostItems;
			option_retail_auction.AdvertisingCost = Decimal.Parse(fbd.userControl_BreakDown.textBoxTotalCost.Text, NumberStyles.Currency);
			textBoxAdvertisingCosts.Text = fbd.userControl_BreakDown.textBoxTotalCost.Text;
			BussinesLogic(buttonBreakDownAdvertisingCosts, new EventArgs());
		}

		private void buttonBreakDownHousePreparationCosts_Click(object sender, System.EventArgs e)
		{
			FormBreakDown fbd = new FormBreakDown();
			fbd.userControl_BreakDown.DataArrayCostItems = option_retail_auction.HousePreparationCostsItems;
			fbd.ShowDialog();		
			option_retail_auction.HousePreparationCostsItems = fbd.userControl_BreakDown.DataArrayCostItems;
			option_retail_auction.HousePreparationCosts = Decimal.Parse(fbd.userControl_BreakDown.textBoxTotalCost.Text, NumberStyles.Currency);
			textBoxHousePreparationCosts.Text = fbd.userControl_BreakDown.textBoxTotalCost.Text;
			BussinesLogic(buttonBreakDownHousePreparationCosts, new EventArgs());
		}

		private void buttonBreakDownClosingCostsAtPurchase_Click(object sender, System.EventArgs e)
		{
			FormBreakDown fbd = new FormBreakDown();
			fbd.userControl_BreakDown.DataArrayCostItems = option_retail_auction.ClosingCostsAtPurchaseItems;
			fbd.ShowDialog();
			option_retail_auction.ClosingCostsAtPurchaseItems = fbd.userControl_BreakDown.DataArrayCostItems;
			option_retail_auction.ClosingCostPurchase = Decimal.Parse(fbd.userControl_BreakDown.textBoxTotalCost.Text, NumberStyles.Currency);
			textBoxClosingCostsAtPurchase.Text = fbd.userControl_BreakDown.textBoxTotalCost.Text;
			BussinesLogic(buttonBreakDownClosingCostsAtPurchase, new EventArgs());
		}

		private void buttonBreakDownClosingCostsAtResale_Click(object sender, System.EventArgs e)
		{
			FormBreakDown fbd = new FormBreakDown();
			fbd.userControl_BreakDown.DataArrayCostItems = option_retail_auction.ClosingCostsAtResaleItems;
			fbd.ShowDialog();
			option_retail_auction.ClosingCostsAtResaleItems = fbd.userControl_BreakDown.DataArrayCostItems;
			option_retail_auction.ClosingCostsResale = Decimal.Parse(fbd.userControl_BreakDown.textBoxTotalCost.Text, NumberStyles.Currency);
			textBoxClosingCostsAtResale.Text = fbd.userControl_BreakDown.textBoxTotalCost.Text;
			BussinesLogic(buttonBreakDownClosingCostsAtResale, new EventArgs());
		}

		private void buttonBreakDownMiscellaneousCosts_Click(object sender, System.EventArgs e)
		{
			FormBreakDown fbd = new FormBreakDown();
			fbd.userControl_BreakDown.DataArrayCostItems = option_retail_auction.MiscellaneousCostsItems;
			fbd.ShowDialog();
			option_retail_auction.MiscellaneousCostsItems = fbd.userControl_BreakDown.DataArrayCostItems;
			option_retail_auction.MiscellaneousCosts = Decimal.Parse(fbd.userControl_BreakDown.textBoxTotalCost.Text, NumberStyles.Currency);
			textBoxMiscellaneousCosts.Text = fbd.userControl_BreakDown.textBoxTotalCost.Text;
			BussinesLogic(buttonBreakDownMiscellaneousCosts, new EventArgs());
		}

		private void buttonBreakDownBoxSellingFees_Click(object sender, System.EventArgs e)
		{
			FormBreakDown fbd = new FormBreakDown();
			fbd.userControl_BreakDown.DataArrayCostItems = option_retail_auction.SellingFeesItems;
			fbd.ShowDialog();
			option_retail_auction.SellingFeesItems = fbd.userControl_BreakDown.DataArrayCostItems;
			option_retail_auction.SellingFees = Decimal.Parse(fbd.userControl_BreakDown.textBoxTotalCost.Text, NumberStyles.Currency);
			textBoxSellingFees.Text = fbd.userControl_BreakDown.textBoxTotalCost.Text;
			BussinesLogic(buttonBreakDownBoxSellingFees, new EventArgs());
		}

		public void BussinesLogic(object sender, System.EventArgs e)
		{
			option_retail_auction.AdvertisingCost = Decimal.Parse(textBoxAdvertisingCosts.Text, NumberStyles.Currency);
			option_retail_auction.SellingFees =	Decimal.Parse(textBoxSellingFees.Text, NumberStyles.Currency);

			option_retail_auction.ClosingCostPurchase =	Decimal.Parse(textBoxClosingCostsAtPurchase.Text, NumberStyles.Currency);

			option_retail_auction.ClosingCostsResale = Decimal.Parse(textBoxClosingCostsAtResale.Text, NumberStyles.Currency);
			option_retail_auction.HousePreparationCosts = Decimal.Parse(textBoxHousePreparationCosts.Text, NumberStyles.Currency);
			option_retail_auction.MiscellaneousCosts = Decimal.Parse(textBoxMiscellaneousCosts.Text, NumberStyles.Currency);

			Control ctrl = sender as Control;

			if (ctrl.Name == radioButtonAuction.Name ||	ctrl.Name == radioButtonRetail.Name	|| ctrl.Name == this.Name)	
			{
				if (radioButtonRetail.Checked == true)
				{
					labelRetailAuctionPercentage.Text = "Set Retail Percentage:"; 
					textBoxRetailAuctionPercentage.Text = "100%";
				}
				else if (radioButtonAuction.Checked == true)
				{
					labelRetailAuctionPercentage.Text = "Set Auction Percentage:"; 
					textBoxRetailAuctionPercentage.Text = "80%";
				}
			}

			TextBox tb = sender as TextBox;
			RadioButton rb = sender as RadioButton;

			decimal E;
			decimal F;
			decimal PRP = 0.0M;
			decimal transactional_costs = 0.0M;

			if (null != tb)
			{
				tb.Text = tb.Text;
			}

			if (
				null != tb
				&&
				(
				tb.Name == "textBoxlDesiredProfitInPercentage"
				||
				tb.Name == "textBoxDesiredGuaranteedProfitInPercentage"
				)
				)
			{
				E = Decimal.Parse(textBoxlDesiredProfitInPercentage.Text.Replace("%",""), NumberStyles.Currency);
				F =	Decimal.Parse(textBoxDesiredGuaranteedProfitInPercentage.Text.Replace("%",""), NumberStyles.Currency);

				if ( 10 > E )
				{
					string str = 
						"Warning! Your goal should be to make at least 10%! "
						+
						"Anything lower than that may turn this deal into "
						+
						"a No-Profit deal if anything goes wrong!"
						;
					MessageBox.Show(str, "Warning", MessageBoxButtons.OK);
				}

				if ( 10 > F )
				{
					string str = 
						"Warning! Your goal should be to make at least 10%! "
						+
						"Anything lower than that may turn this deal into "
						+
						"a No-Profit deal if anything goes wrong!"
						;
					MessageBox.Show(str, "Warning", MessageBoxButtons.OK);
					textBoxDesiredGuaranteedProfitInPercentage.Text = "10%";
				}
			}

			PRP = WorthByCOMPS * Decimal.Parse(	textBoxRetailAuctionPercentage.Text.Replace("%",""), NumberStyles.Currency)	/ 100.00M;
			textBoxProjectedResalePrice.Text = PRP.ToString("c");

			transactional_costs =
				Decimal.Parse(textBoxAdvertisingCosts.Text, NumberStyles.Currency)
				+
				Decimal.Parse(textBoxSellingFees.Text, NumberStyles.Currency)
				+
				Decimal.Parse(textBoxClosingCostsAtPurchase.Text, NumberStyles.Currency)
				+
				Decimal.Parse(textBoxClosingCostsAtResale.Text, NumberStyles.Currency)
				+
				Decimal.Parse(textBoxHousePreparationCosts.Text, NumberStyles.Currency)
				+
				Decimal.Parse(textBoxMiscellaneousCosts.Text, NumberStyles.Currency)
				;
			textBoxTotalTransactionalCosts.Text = transactional_costs.ToString("c");
			

			if (radioButtonEquitySharingYes.Checked	&& null != rb)
				// red in document
			{
				labelEquitySharing.Visible = true;
				labelAgreedReservePrice.Visible = true;
				textBoxAgreedReservePrice.Visible = true;
				labelDesiredGuaranteedProfitInPercentage.Visible = true;
				textBoxDesiredGuaranteedProfitInPercentage.Visible = true;
				labelDesiredGuaranteedProfitInBucks.Visible = true;
				textBoxDesiredGuaranteedProfitInBucks.Visible = true;
				labelEquitySharingRatios.Visible = true;
				labelOptionorSellerReceives.Visible = true;
				textBoxOptionorSellerReceives.Visible = true;
				labelOptionorSellerReceives2.Visible = true;
				labelOptioneeBuyerReceives.Visible = true;
				textBoxOptioneeBuyerReceives.Visible = true;
				labelOptioneeBuyerReceives2.Visible = true;
				labelMaxPurchaseOptionPrice_red.Visible = true;
				textBoxMaxPurchaseOptionPrice_red.Visible = true;
				labelTotalProfit.Visible = true;
				textBoxTotalProfit.Visible = true;
				labelSellersOptionrsTotalProfit.Visible = true;
				textBoxSellersOptionrsTotalProfit.Visible = true;

				labelDesiredProfitInBucks.Visible = false;
				labelDesiredProfitInPercentage.Visible = false;
				textBoxDesiredProfitInBucks.Visible = false;
				textBoxlDesiredProfitInPercentage.Visible = false;
				labelMaxPurchaseOptionPrice_blue.Visible = false;
				textBoxMaxPurchaseOptionPrice_blue.Visible = false;
				
			}
			else if (radioButtonEquitySharingNo.Checked	&& null != rb)
				// blue in document
			{
				labelEquitySharing.Visible = false;
				labelAgreedReservePrice.Visible = false;
				textBoxAgreedReservePrice.Visible = false;
				labelDesiredGuaranteedProfitInPercentage.Visible = false;
				textBoxDesiredGuaranteedProfitInPercentage.Visible = false;
				labelDesiredGuaranteedProfitInBucks.Visible = false;
				textBoxDesiredGuaranteedProfitInBucks.Visible = false;
				labelEquitySharingRatios.Visible = false;
				labelOptionorSellerReceives.Visible = false;
				textBoxOptionorSellerReceives.Visible = false;
				labelOptionorSellerReceives2.Visible = false;
				labelOptioneeBuyerReceives.Visible = false;
				textBoxOptioneeBuyerReceives.Visible = false;
				labelOptioneeBuyerReceives2.Visible = false;
				labelMaxPurchaseOptionPrice_red.Visible = false;
				textBoxMaxPurchaseOptionPrice_red.Visible = false;
				labelTotalProfit.Visible = false;
				textBoxTotalProfit.Visible = false;
				labelSellersOptionrsTotalProfit.Visible = false;
				textBoxSellersOptionrsTotalProfit.Visible = false;

				labelDesiredProfitInBucks.Visible = true;
				labelDesiredProfitInPercentage.Visible = true;
				textBoxDesiredProfitInBucks.Visible = true;
				textBoxlDesiredProfitInPercentage.Visible = true;
				labelMaxPurchaseOptionPrice_blue.Visible = true;
				textBoxMaxPurchaseOptionPrice_blue.Visible = true;


			}
		
			//==========================================================
			// Yes - Red
			//
			decimal ARP = Decimal.Parse(textBoxAgreedReservePrice.Text, NumberStyles.Currency);

			if (PRP == ARP && business_logic_counter > 0 && PRP != 0.0m && ARP != 0.0m)
			{
				string str = 
					"Your Reserved Price is EQUAL to Projected Resale Price,"
					+
					" so you do NOT need Equity Sharing addendum. Change "
					+
					"Reserve Price or select 'No' for Equity Sharing!"
					;
				MessageBox.Show(str);
			}
			else if (PRP < ARP && business_logic_counter > 0)
			{
				string str = 
					"The Reserved Price MUST be lower than Projected Resale Price"
					;
				MessageBox.Show(str);
			}

				
			decimal Amurphy_factor_transactional = 0.0M;

			decimal murphy_factor_transactional = Decimal.Parse(textBoxMurphyFactor.Text.Replace("%",""), NumberStyles.Currency);
			Amurphy_factor_transactional = transactional_costs * murphy_factor_transactional / 100.00M;

			E =	Decimal.Parse(textBoxlDesiredProfitInPercentage.Text.Replace("%",""), NumberStyles.Currency);
			F = PRP - E - Amurphy_factor_transactional;

			textBoxMaxPurchaseOptionPrice_red.Text = F.ToString("c");

			if (tb != null	&&	tb.Name == "textBoxOptionorSellerReceives")
			{
				double p = Double.Parse(textBoxOptionorSellerReceives.Text.Replace("%",""));
				double p1 = 100 - p;

				textBoxOptioneeBuyerReceives.Text = p1.ToString() + "%";
			} 
			else if ( tb != null && tb.Name == "textBoxOptioneeBuyerReceives")
			{
				double p = Double.Parse(textBoxOptioneeBuyerReceives.Text.Replace("%",""));
				double p1 = 100 - p;

				textBoxOptionorSellerReceives.Text = p1.ToString() + "%";
			}

			decimal desired_profit_bucks = 
				Decimal.Parse(textBoxDesiredGuaranteedProfitInPercentage.Text.Replace("%",""), NumberStyles.Currency)
				/
				100.00m
				*
				Decimal.Parse(textBoxProjectedResalePrice.Text, NumberStyles.Currency);

			textBoxDesiredGuaranteedProfitInBucks.Text = desired_profit_bucks.ToString("c");

			decimal optionees_buyers_profit =				
				Decimal.Parse(textBoxDesiredGuaranteedProfitInBucks.Text, NumberStyles.Currency) +
				(Decimal.Parse(textBoxProjectedResalePrice.Text, NumberStyles.Currency) -
				Decimal.Parse(textBoxAgreedReservePrice.Text, NumberStyles.Currency)) *
				Decimal.Parse(textBoxOptioneeBuyerReceives.Text.Replace("%",""), NumberStyles.Currency)	/ 100.00M;
			
			textBoxTotalProfit.Text = optionees_buyers_profit.ToString("c");
					
			decimal optionors_sellers_profit =				
				Decimal.Parse(textBoxAgreedReservePrice.Text, NumberStyles.Currency) -
				Decimal.Parse(textBoxDesiredGuaranteedProfitInBucks.Text, NumberStyles.Currency) -
				Decimal.Parse(textBoxTotalTransactionalCosts.Text, NumberStyles.Currency) *
				(1.0M +	Decimal.Parse (textBoxMurphyFactor.Text.Replace("%",""), NumberStyles.Currency)	/ 100.00M) +
				(Decimal.Parse(textBoxProjectedResalePrice.Text, NumberStyles.Currency)	- 
				Decimal.Parse(textBoxAgreedReservePrice.Text, NumberStyles.Currency)) * 
				Decimal.Parse(textBoxOptionorSellerReceives.Text.Replace("%",""), NumberStyles.Currency)/ 100.0M;

			textBoxSellersOptionrsTotalProfit.Text = optionors_sellers_profit.ToString("c");

			decimal maximum_option_price = Decimal.Parse(textBoxAgreedReservePrice.Text, NumberStyles.Currency)	-
				Decimal.Parse(textBoxDesiredGuaranteedProfitInBucks.Text, NumberStyles.Currency) -
				Decimal.Parse(textBoxTotalTransactionalCosts.Text, NumberStyles.Currency) * 
				(1.0M +	Decimal.Parse(textBoxMurphyFactor.Text.Replace("%",""), NumberStyles.Currency)	/100.00M);
			textBoxMaxPurchaseOptionPrice_red.Text = maximum_option_price.ToString("c");

			//===================================
			// No - blue
			
			decimal desired_profit = Decimal.Parse(textBoxProjectedResalePrice.Text, NumberStyles.Currency) * 
				Decimal.Parse (textBoxlDesiredProfitInPercentage.Text.Replace("%",""), NumberStyles.Currency) / 100.00m;

			textBoxDesiredProfitInBucks.Text = desired_profit.ToString("c");

			// CR 2005.03.04
			decimal maximum_option_price_red = Decimal.Parse(textBoxProjectedResalePrice.Text, NumberStyles.Currency) -
				Decimal.Parse(textBoxDesiredProfitInBucks.Text, NumberStyles.Currency) - 
				Decimal.Parse(textBoxTotalTransactionalCosts.Text, NumberStyles.Currency) * 
				(1 + Decimal.Parse(textBoxMurphyFactor.Text.Replace("%",""), NumberStyles.Currency)	/ 100.00m);

			textBoxMaxPurchaseOptionPrice_blue.Text = maximum_option_price_red.ToString("c");

			//SAVE - RELOAD SUPPORT (BEGIN)
			if (radioButtonAuction.Checked)
				option_retail_auction.ExitStrategy = ExitStrategy.Auction;
			else
				option_retail_auction.ExitStrategy = ExitStrategy.Retail;

			option_retail_auction.ProjectedResalePrice = Decimal.Parse(this.textBoxProjectedResalePrice.Text, NumberStyles.Currency);
			option_retail_auction.RetailAuctionPercentage = Double.Parse(textBoxRetailAuctionPercentage.Text.Replace("%",""), NumberStyles.Currency);
			option_retail_auction.ProjectedNumberOfMonths = int.Parse(textBoxProjectedNumberOfMonths.Text, NumberStyles.Number);
			option_retail_auction.AdvertisingCost = Decimal.Parse(textBoxAdvertisingCosts.Text, NumberStyles.Currency);
			option_retail_auction.ClosingCostPurchase = Decimal.Parse(textBoxClosingCostsAtPurchase.Text, NumberStyles.Currency);
			option_retail_auction.HousePreparationCosts = Decimal.Parse(textBoxHousePreparationCosts.Text, NumberStyles.Currency);
			option_retail_auction.SellingFees = Decimal.Parse(textBoxSellingFees.Text, NumberStyles.Currency);			
			option_retail_auction.ClosingCostsResale = Decimal.Parse(textBoxClosingCostsAtResale.Text, NumberStyles.Currency);
			option_retail_auction.MiscellaneousCosts = Decimal.Parse(textBoxMiscellaneousCosts.Text, NumberStyles.Currency);
			option_retail_auction.MurphyFactor = Double.Parse(textBoxMurphyFactor.Text.Replace("%",""), NumberStyles.Currency);
			option_retail_auction.TransactionalCosts = Decimal.Parse(textBoxTotalTransactionalCosts.Text, NumberStyles.Currency);			
			option_retail_auction.DesiredProfitPercentage = Decimal.Parse (textBoxlDesiredProfitInPercentage.Text.Replace("%",""), NumberStyles.Currency);
			option_retail_auction.DesiredProfit = Double.Parse(textBoxDesiredProfitInBucks.Text, NumberStyles.Currency);

			option_retail_auction.MaximumOptionPrice = decimal.Parse(textBoxMaxPurchaseOptionPrice_blue.Text, NumberStyles.Currency);

			option_retail_auction.DesiredGuaranteedProfitInPercents = Decimal.Parse(textBoxDesiredGuaranteedProfitInPercentage.Text.Replace("%",""), NumberStyles.Currency);
			option_retail_auction.DesiredGuaranteedProfitInBucks = Decimal.Parse(textBoxDesiredGuaranteedProfitInBucks.Text, NumberStyles.Currency);
			
			option_retail_auction.AgreedReservePrice = Decimal.Parse(textBoxAgreedReservePrice.Text, NumberStyles.Currency);
			option_retail_auction.OptinorSellerReceives = Double.Parse(textBoxOptionorSellerReceives.Text.Replace("%",""), NumberStyles.Currency);
			option_retail_auction.OptinorBuyerReceives = Double.Parse(textBoxOptioneeBuyerReceives.Text.Replace("%",""), NumberStyles.Currency);

			option_retail_auction.TotalProfit = decimal.Parse(textBoxTotalProfit.Text, NumberStyles.Currency);

			option_retail_auction.TotalProfitSellersOptinors = decimal.Parse(textBoxSellersOptionrsTotalProfit.Text, NumberStyles.Currency);			

			option_retail_auction.EquitySharing = radioButtonEquitySharingYes.Checked ? EquitySharing.Yes : EquitySharing.No;			
			//SAVE - RELOAD SUPPORT (END)
			
			business_logic_counter += 1;
			return;
		}

		

		public decimal WorthByCOMPS
		{
			set	{ dec_worth_by_comps = value; }
			get	{return dec_worth_by_comps;	}
		}

		ExitStrategy SelectedExitStrategy
		{			
			set 
			{
				this.radioButtonAuction.CheckedChanged -= new System.EventHandler(this.BussinesLogic);
				this.radioButtonRetail.CheckedChanged -= new System.EventHandler(this.BussinesLogic);
				if (ExitStrategy.Auction == value)
				{					
					radioButtonAuction.Checked = true;
					radioButtonRetail.Checked = false;
				}
				else
				{
					radioButtonAuction.Checked = false;
					radioButtonRetail.Checked = true;
				}

				if (ExitStrategy.Auction == value)				
					labelRetailAuctionPercentage.Text = "Set Auction Percentage:"; 									
				else				
					labelRetailAuctionPercentage.Text = "Set Retail Percentage:"; 					

				this.radioButtonAuction.CheckedChanged += new System.EventHandler(this.BussinesLogic);
				this.radioButtonRetail.CheckedChanged += new System.EventHandler(this.BussinesLogic);
			}
		}

		EquitySharing SelectedEquitySharingOption
		{
			set 
			{
				this.radioButtonEquitySharingYes.CheckedChanged -= new System.EventHandler(this.BussinesLogic);
				this.radioButtonEquitySharingNo.CheckedChanged -= new System.EventHandler(this.BussinesLogic);

				if (EquitySharing.Yes == value)
				{					
					radioButtonEquitySharingYes.Checked = true;
					radioButtonEquitySharingNo.Checked = false;
				}
				else
				{
					radioButtonEquitySharingYes.Checked = false;
					radioButtonEquitySharingNo.Checked = true;
				}
				
				bool val = true;
				if (EquitySharing.Yes == value)
					val = true;									
				else
					val = false;
									
				labelEquitySharing.Visible = val;
				labelAgreedReservePrice.Visible = val;
				textBoxAgreedReservePrice.Visible = val;
				labelDesiredGuaranteedProfitInPercentage.Visible = val;
				textBoxDesiredGuaranteedProfitInPercentage.Visible = val;
				labelDesiredGuaranteedProfitInBucks.Visible = val;
				textBoxDesiredGuaranteedProfitInBucks.Visible = val;
				labelEquitySharingRatios.Visible = val;
				labelOptionorSellerReceives.Visible = val;
				textBoxOptionorSellerReceives.Visible = val;
				labelOptionorSellerReceives2.Visible = val;
				labelOptioneeBuyerReceives.Visible = val;
				textBoxOptioneeBuyerReceives.Visible = val;
				labelOptioneeBuyerReceives2.Visible = val;
				labelMaxPurchaseOptionPrice_red.Visible = val;
				textBoxMaxPurchaseOptionPrice_red.Visible = val;
				labelTotalProfit.Visible = val;
				textBoxTotalProfit.Visible = val;
				labelSellersOptionrsTotalProfit.Visible = val;
				textBoxSellersOptionrsTotalProfit.Visible = val;

				labelDesiredProfitInBucks.Visible = !val;
				labelDesiredProfitInPercentage.Visible = !val;
				textBoxDesiredProfitInBucks.Visible = !val;
				textBoxlDesiredProfitInPercentage.Visible = !val;
				labelMaxPurchaseOptionPrice_blue.Visible = !val;
				textBoxMaxPurchaseOptionPrice_blue.Visible = !val;

				this.radioButtonEquitySharingYes.CheckedChanged += new System.EventHandler(this.BussinesLogic);
				this.radioButtonEquitySharingNo.CheckedChanged += new System.EventHandler(this.BussinesLogic);
			}
		}

		public Options	Options
		{
			get	{ return option_retail_auction;	}
			set	{ 
				option_retail_auction = value;
				this.textBoxAdvertisingCosts.Text  = value.AdvertisingCost.ToString("c");
				this.textBoxAgreedReservePrice.Text  = value.AgreedReservePrice.ToString("c");
				this.textBoxRetailAuctionPercentage.Text  = value.RetailAuctionPercentage.ToString();
				this.textBoxClosingCostsAtPurchase.Text  = value.ClosingCostPurchase.ToString("c");
				this.textBoxClosingCostsAtResale.Text  = value.ClosingCostsResale.ToString("c");
				this.textBoxDesiredGuaranteedProfitInPercentage.Text  = value.DesiredGuaranteedProfitInPercents.ToString();
				this.textBoxDesiredGuaranteedProfitInBucks.Text  = value.DesiredGuaranteedProfitInBucks.ToString("c");
				this.textBoxDesiredProfitInBucks.Text  = value.DesiredProfit.ToString("c");
				this.textBoxHousePreparationCosts.Text  = value.HousePreparationCosts.ToString("c");
				this.textBoxlDesiredProfitInPercentage.Text  = value.DesiredProfitPercentage.ToString();
				this.textBoxMaxPurchaseOptionPrice_blue.Text  = value.MaximumOptionPrice.ToString("c");
				this.textBoxMaxPurchaseOptionPrice_red.Text  = value.MaximumOptionPrice.ToString("c");
				this.textBoxMiscellaneousCosts.Text  = value.MiscellaneousCosts.ToString("c");
				this.textBoxMurphyFactor.Text  = value.MurphyFactor.ToString();
				this.textBoxOptioneeBuyerReceives.Text  = value.OptinorBuyerReceives.ToString();
				this.textBoxOptionorSellerReceives.Text  = value.OptinorSellerReceives.ToString();
				this.textBoxProjectedNumberOfMonths.Text  = value.ProjectedNumberOfMonths.ToString();
				this.textBoxProjectedResalePrice.Text  = value.ProjectedResalePrice .ToString("c");
				this.textBoxSellersOptionrsTotalProfit.Text  = value.TotalProfitSellersOptinors.ToString("c");
				this.textBoxSellingFees.Text  = value.SellingFees.ToString("c");
				this.textBoxMaxPurchaseOptionPrice_blue.Text  = value.MaximumOptionPrice.ToString("c");
				this.textBoxMaxPurchaseOptionPrice_red.Text  = value.MaximumOptionPrice.ToString("c");
				this.textBoxTotalProfit.Text  = value.TotalProfit.ToString("c");
				this.textBoxTotalTransactionalCosts.Text  = value.TransactionalCosts.ToString("c");

				//RELOAD SUPPORT - BEGIN
				
				SelectedExitStrategy = value.ExitStrategy;
				
				
				SelectedEquitySharingOption = value.EquitySharing;
				
				//RELOAD SUPPORT - END

				//this.BussinesLogic(this, new EventArgs());
			}
		}		
	}
}
