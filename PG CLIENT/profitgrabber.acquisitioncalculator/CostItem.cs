using System;
using System.Xml;
using System.Xml.Serialization;
using System.Collections;

namespace WindowsApplication.AcquisitionCalculator
{
	/// <summary>
	/// Summary description for CostItem.
	/// </summary>
	[XmlInclude(typeof(CostItem))]
	public 
		struct
		CostItem
	{
		public
			string
			CostDescription
			;

		public
			decimal
			Cost
			;
	}
}
