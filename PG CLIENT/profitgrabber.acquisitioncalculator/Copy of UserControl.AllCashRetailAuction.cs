using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;
using System.Globalization;

namespace WindowsApplication.AcquisitionCalculator
{
	/// <summary>
	/// Summary description for UserControl.
	/// </summary>
	public class UserControl_AllCashRetailAuction : System.Windows.Forms.UserControl
	{
		private 
			AcquisitionCalculator.AllCash	
			all_cash
			;

		private 
			int 
			business_logic_counter
			;


		private
			decimal
			dec_worth_by_comps
			;

		private System.Windows.Forms.GroupBox groupBoxExitStrategy;
		private System.Windows.Forms.RadioButton radioButtonAuction;
		private System.Windows.Forms.Label labelProjectedResalePrice;
		private System.Windows.Forms.Label labelTransactionalCosts;
		private System.Windows.Forms.Label labelFinancingCosts;
		private System.Windows.Forms.Label labelHoldingCosts;
		private System.Windows.Forms.Label labelDealCost;
		private System.Windows.Forms.Label labelAdvertisingCosts;
		private System.Windows.Forms.Label labelSellingFees;
		private System.Windows.Forms.Label labelClosingCostsAtPurchase;
		private System.Windows.Forms.Label labelClosingCostsAtResale;
		private System.Windows.Forms.Label labelHousePreparationCosts;
		private System.Windows.Forms.Label labelMiscellaneousCosts;
		private System.Windows.Forms.GroupBox groupBoxInterestOnlyLoan;
		private System.Windows.Forms.Label labelMonthlyPayment;
		private System.Windows.Forms.Label labelLoanPoints;
		private System.Windows.Forms.Label labelLoanAmount;
		private System.Windows.Forms.Label labelInterestRate;
		private System.Windows.Forms.Label labelOtherCosts;
		private System.Windows.Forms.Label labelLTV;
		private System.Windows.Forms.Button buttonBreakDownAdvertisingCosts;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Label labelHoldingCostsMonthlyPayments;
		private System.Windows.Forms.Label labelHoldingCostsOther;
		private System.Windows.Forms.Label labelTotalHoldingCost;
		private System.Windows.Forms.Label labelOtherCashInDeal;
		private System.Windows.Forms.Label labelApproximateLoanPayoff;
		private System.Windows.Forms.Label labelTaxesDue;
		private System.Windows.Forms.Label labelTotalFinancingCosts;
		private System.Windows.Forms.Label labelOtherEncumbrances;
		private System.Windows.Forms.Label labelTotalDealCost;
		private System.Windows.Forms.Label labelMaximumPurchasePrice;
		private System.Windows.Forms.RadioButton radioButtonInterestLoanNo;
		private System.Windows.Forms.RadioButton radioButtonInterestLoanYes;
		private System.Windows.Forms.RadioButton radioButtonPaymentsResaleNo;
		private System.Windows.Forms.RadioButton radioButtonPaymentsResaleYes;
		private System.Windows.Forms.Label labelTotalTransactionalCost;
		private System.Windows.Forms.Label labelDesiredProfitInPercentage;
		private System.Windows.Forms.Label labelDesiredProfitInBucks;
		private System.Windows.Forms.Button buttonBreakDownBoxSellingFees;
		private System.Windows.Forms.Button buttonBreakDownHousePreparationCosts;
		private System.Windows.Forms.Button buttonBreakDownClosingCostsAtPurchase;
		private System.Windows.Forms.Button buttonBreakDownClosingCostsAtResale;
		private System.Windows.Forms.Button buttonBreakDownMiscellaneousCosts;
		private System.Windows.Forms.Button buttonBreakDownOtherCosts;
		private System.Windows.Forms.Button buttonBreakDownHoldingCostsOther;
		private System.Windows.Forms.Label labelHoldingMurphyFactor;
		private System.Windows.Forms.Label labelTransactionalMurphyFactor;
		private TextBox textBoxAdvertisingCosts;
		private TextBox textBoxSellingFees;
		private TextBox textBoxClosingCostsAtPurchase;
		private TextBox textBoxClosingCostsAtResale;
		private TextBox textBoxProjectedResalePrice;
		private TextBox textBoxHousePreparationCosts;
		private TextBox textBoxMiscellaneousCosts;
		private TextBox textBoxTotalTransactionalCosts;
		private TextBox textBoxTransactionalMurphyFactor;
		private TextBox textBoxOtherCosts;
		private TextBox textBoxLoanPoints;
		private TextBox textBoxInterestRate;
		private TextBox textBoxLoanAmount;
		private TextBox textBoxMonthlyPayment;
		private TextBox textBoxLTV;
		private TextBox textBoxTotalFinancingCosts;
		private TextBox textBoxHoldingCostsMonthlyPayments;
		private TextBox textBoxTotalHoldingCost;
		private TextBox textBoxHoldingCostsOther;
		private TextBox textBoxHoldingMurphyFactor;
		private TextBox textBoxOtherCashInDeal;
		private TextBox textBoxApproximateLoanPayoff;
		private TextBox textBoxTaxesDue;
		private TextBox textBoxOtherEncumbrances;
		private TextBox textBoxTotalDealCost;
		private TextBox textBoxMaximumPurchasePrice;
		private TextBox textBoxDesiredProfitInPercentage;
		private TextBox textBoxDesiredProfitInBucks;
		private TextBox textBoxTotalAcquistionCostOfDeal;
		private System.Windows.Forms.Label labelTotalAcquistionCostOfDeal;
		private TextBox textBoxProjectedNumberOfMonths;
		private System.Windows.Forms.Label labelProjectedNumberOfMonths;
		private System.Windows.Forms.Label labelRetailAuctionPercentage;
		private TextBox textBoxRetailAuctionPercentage;
		private System.Windows.Forms.RadioButton radioButtonRetail;
		private WindowsApplication.AcquisitionCalculator.TextBox textBox1;
		private System.Windows.Forms.Label labelAmortization;
		private System.Windows.Forms.PictureBox pictureBox1;
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public UserControl_AllCashRetailAuction()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

			// TODO: Add any initialization after the InitializeComponent call
			all_cash = new AllCash();
			business_logic_counter = 0;
			BussinesLogic(this, new System.EventArgs());
		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(UserControl_AllCashRetailAuction));
			this.groupBoxExitStrategy = new System.Windows.Forms.GroupBox();
			this.radioButtonAuction = new System.Windows.Forms.RadioButton();
			this.radioButtonRetail = new System.Windows.Forms.RadioButton();
			this.labelRetailAuctionPercentage = new System.Windows.Forms.Label();
			this.labelProjectedResalePrice = new System.Windows.Forms.Label();
			this.labelTransactionalCosts = new System.Windows.Forms.Label();
			this.labelFinancingCosts = new System.Windows.Forms.Label();
			this.labelHoldingCosts = new System.Windows.Forms.Label();
			this.labelDealCost = new System.Windows.Forms.Label();
			this.labelAdvertisingCosts = new System.Windows.Forms.Label();
			this.labelSellingFees = new System.Windows.Forms.Label();
			this.labelClosingCostsAtPurchase = new System.Windows.Forms.Label();
			this.labelClosingCostsAtResale = new System.Windows.Forms.Label();
			this.labelHousePreparationCosts = new System.Windows.Forms.Label();
			this.labelMiscellaneousCosts = new System.Windows.Forms.Label();
			this.groupBoxInterestOnlyLoan = new System.Windows.Forms.GroupBox();
			this.radioButtonInterestLoanNo = new System.Windows.Forms.RadioButton();
			this.radioButtonInterestLoanYes = new System.Windows.Forms.RadioButton();
			this.labelMonthlyPayment = new System.Windows.Forms.Label();
			this.labelLoanPoints = new System.Windows.Forms.Label();
			this.labelLoanAmount = new System.Windows.Forms.Label();
			this.labelInterestRate = new System.Windows.Forms.Label();
			this.labelOtherCosts = new System.Windows.Forms.Label();
			this.labelLTV = new System.Windows.Forms.Label();
			this.buttonBreakDownAdvertisingCosts = new System.Windows.Forms.Button();
			this.labelTotalFinancingCosts = new System.Windows.Forms.Label();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.radioButtonPaymentsResaleNo = new System.Windows.Forms.RadioButton();
			this.radioButtonPaymentsResaleYes = new System.Windows.Forms.RadioButton();
			this.labelHoldingCostsOther = new System.Windows.Forms.Label();
			this.labelHoldingCostsMonthlyPayments = new System.Windows.Forms.Label();
			this.labelTotalHoldingCost = new System.Windows.Forms.Label();
			this.labelHoldingMurphyFactor = new System.Windows.Forms.Label();
			this.labelOtherCashInDeal = new System.Windows.Forms.Label();
			this.labelApproximateLoanPayoff = new System.Windows.Forms.Label();
			this.labelTaxesDue = new System.Windows.Forms.Label();
			this.labelOtherEncumbrances = new System.Windows.Forms.Label();
			this.labelTotalDealCost = new System.Windows.Forms.Label();
			this.labelMaximumPurchasePrice = new System.Windows.Forms.Label();
			this.labelTotalTransactionalCost = new System.Windows.Forms.Label();
			this.labelDesiredProfitInPercentage = new System.Windows.Forms.Label();
			this.labelDesiredProfitInBucks = new System.Windows.Forms.Label();
			this.buttonBreakDownBoxSellingFees = new System.Windows.Forms.Button();
			this.buttonBreakDownHousePreparationCosts = new System.Windows.Forms.Button();
			this.buttonBreakDownClosingCostsAtPurchase = new System.Windows.Forms.Button();
			this.buttonBreakDownClosingCostsAtResale = new System.Windows.Forms.Button();
			this.buttonBreakDownMiscellaneousCosts = new System.Windows.Forms.Button();
			this.buttonBreakDownOtherCosts = new System.Windows.Forms.Button();
			this.buttonBreakDownHoldingCostsOther = new System.Windows.Forms.Button();
			this.labelTransactionalMurphyFactor = new System.Windows.Forms.Label();
			this.textBoxAdvertisingCosts = new WindowsApplication.AcquisitionCalculator.TextBox();
			this.textBoxSellingFees = new WindowsApplication.AcquisitionCalculator.TextBox();
			this.textBoxClosingCostsAtPurchase = new WindowsApplication.AcquisitionCalculator.TextBox();
			this.textBoxClosingCostsAtResale = new WindowsApplication.AcquisitionCalculator.TextBox();
			this.textBoxRetailAuctionPercentage = new WindowsApplication.AcquisitionCalculator.TextBox();
			this.textBoxProjectedResalePrice = new WindowsApplication.AcquisitionCalculator.TextBox();
			this.textBoxDesiredProfitInPercentage = new WindowsApplication.AcquisitionCalculator.TextBox();
			this.textBoxDesiredProfitInBucks = new WindowsApplication.AcquisitionCalculator.TextBox();
			this.textBoxHousePreparationCosts = new WindowsApplication.AcquisitionCalculator.TextBox();
			this.textBoxMiscellaneousCosts = new WindowsApplication.AcquisitionCalculator.TextBox();
			this.textBoxTotalTransactionalCosts = new WindowsApplication.AcquisitionCalculator.TextBox();
			this.textBoxTransactionalMurphyFactor = new WindowsApplication.AcquisitionCalculator.TextBox();
			this.textBoxOtherCosts = new WindowsApplication.AcquisitionCalculator.TextBox();
			this.textBoxLoanPoints = new WindowsApplication.AcquisitionCalculator.TextBox();
			this.textBoxInterestRate = new WindowsApplication.AcquisitionCalculator.TextBox();
			this.textBoxLoanAmount = new WindowsApplication.AcquisitionCalculator.TextBox();
			this.textBoxMonthlyPayment = new WindowsApplication.AcquisitionCalculator.TextBox();
			this.textBoxLTV = new WindowsApplication.AcquisitionCalculator.TextBox();
			this.textBoxTotalFinancingCosts = new WindowsApplication.AcquisitionCalculator.TextBox();
			this.textBoxHoldingCostsMonthlyPayments = new WindowsApplication.AcquisitionCalculator.TextBox();
			this.textBoxTotalHoldingCost = new WindowsApplication.AcquisitionCalculator.TextBox();
			this.textBoxHoldingCostsOther = new WindowsApplication.AcquisitionCalculator.TextBox();
			this.textBoxHoldingMurphyFactor = new WindowsApplication.AcquisitionCalculator.TextBox();
			this.textBoxOtherCashInDeal = new WindowsApplication.AcquisitionCalculator.TextBox();
			this.textBoxApproximateLoanPayoff = new WindowsApplication.AcquisitionCalculator.TextBox();
			this.textBoxTaxesDue = new WindowsApplication.AcquisitionCalculator.TextBox();
			this.textBoxOtherEncumbrances = new WindowsApplication.AcquisitionCalculator.TextBox();
			this.textBoxTotalDealCost = new WindowsApplication.AcquisitionCalculator.TextBox();
			this.textBoxMaximumPurchasePrice = new WindowsApplication.AcquisitionCalculator.TextBox();
			this.textBoxTotalAcquistionCostOfDeal = new WindowsApplication.AcquisitionCalculator.TextBox();
			this.labelTotalAcquistionCostOfDeal = new System.Windows.Forms.Label();
			this.textBoxProjectedNumberOfMonths = new WindowsApplication.AcquisitionCalculator.TextBox();
			this.labelProjectedNumberOfMonths = new System.Windows.Forms.Label();
			this.textBox1 = new WindowsApplication.AcquisitionCalculator.TextBox();
			this.labelAmortization = new System.Windows.Forms.Label();
			this.pictureBox1 = new System.Windows.Forms.PictureBox();
			this.groupBoxExitStrategy.SuspendLayout();
			this.groupBoxInterestOnlyLoan.SuspendLayout();
			this.groupBox1.SuspendLayout();
			this.SuspendLayout();
			// 
			// groupBoxExitStrategy
			// 
			this.groupBoxExitStrategy.Controls.Add(this.radioButtonAuction);
			this.groupBoxExitStrategy.Controls.Add(this.radioButtonRetail);
			this.groupBoxExitStrategy.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.groupBoxExitStrategy.Location = new System.Drawing.Point(8, 24);
			this.groupBoxExitStrategy.Name = "groupBoxExitStrategy";
			this.groupBoxExitStrategy.Size = new System.Drawing.Size(152, 40);
			this.groupBoxExitStrategy.TabIndex = 0;
			this.groupBoxExitStrategy.TabStop = false;
			this.groupBoxExitStrategy.Text = "Select Your Exit Strategy:";
			// 
			// radioButtonAuction
			// 
			this.radioButtonAuction.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.radioButtonAuction.Location = new System.Drawing.Point(80, 16);
			this.radioButtonAuction.Name = "radioButtonAuction";
			this.radioButtonAuction.Size = new System.Drawing.Size(64, 16);
			this.radioButtonAuction.TabIndex = 1;
			this.radioButtonAuction.Text = "Auction";
			this.radioButtonAuction.CheckedChanged += new System.EventHandler(this.BussinesLogic);
			// 
			// radioButtonRetail
			// 
			this.radioButtonRetail.Checked = true;
			this.radioButtonRetail.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.radioButtonRetail.Location = new System.Drawing.Point(8, 16);
			this.radioButtonRetail.Name = "radioButtonRetail";
			this.radioButtonRetail.Size = new System.Drawing.Size(56, 16);
			this.radioButtonRetail.TabIndex = 0;
			this.radioButtonRetail.TabStop = true;
			this.radioButtonRetail.Text = "Retail";
			this.radioButtonRetail.CheckedChanged += new System.EventHandler(this.BussinesLogic);
			// 
			// labelRetailAuctionPercentage
			// 
			this.labelRetailAuctionPercentage.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.labelRetailAuctionPercentage.Location = new System.Drawing.Point(168, 24);
			this.labelRetailAuctionPercentage.Name = "labelRetailAuctionPercentage";
			this.labelRetailAuctionPercentage.Size = new System.Drawing.Size(136, 23);
			this.labelRetailAuctionPercentage.TabIndex = 1;
			this.labelRetailAuctionPercentage.Text = "Set R/A Percentage:";
			// 
			// labelProjectedResalePrice
			// 
			this.labelProjectedResalePrice.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.labelProjectedResalePrice.Location = new System.Drawing.Point(512, 24);
			this.labelProjectedResalePrice.Name = "labelProjectedResalePrice";
			this.labelProjectedResalePrice.Size = new System.Drawing.Size(128, 23);
			this.labelProjectedResalePrice.TabIndex = 7;
			this.labelProjectedResalePrice.Text = "Projected Resale Price:";
			// 
			// labelTransactionalCosts
			// 
			this.labelTransactionalCosts.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.labelTransactionalCosts.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.labelTransactionalCosts.Location = new System.Drawing.Point(0, 72);
			this.labelTransactionalCosts.Name = "labelTransactionalCosts";
			this.labelTransactionalCosts.Size = new System.Drawing.Size(160, 23);
			this.labelTransactionalCosts.TabIndex = 13;
			this.labelTransactionalCosts.Text = "TRANSACTIONAL COSTS:";
			// 
			// labelFinancingCosts
			// 
			this.labelFinancingCosts.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.labelFinancingCosts.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.labelFinancingCosts.Location = new System.Drawing.Point(0, 192);
			this.labelFinancingCosts.Name = "labelFinancingCosts";
			this.labelFinancingCosts.Size = new System.Drawing.Size(120, 23);
			this.labelFinancingCosts.TabIndex = 36;
			this.labelFinancingCosts.Text = "FINANCING COSTS:";
			// 
			// labelHoldingCosts
			// 
			this.labelHoldingCosts.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.labelHoldingCosts.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.labelHoldingCosts.Location = new System.Drawing.Point(0, 272);
			this.labelHoldingCosts.Name = "labelHoldingCosts";
			this.labelHoldingCosts.Size = new System.Drawing.Size(104, 23);
			this.labelHoldingCosts.TabIndex = 54;
			this.labelHoldingCosts.Text = "HOLDING COSTS:";
			// 
			// labelDealCost
			// 
			this.labelDealCost.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.labelDealCost.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.labelDealCost.Location = new System.Drawing.Point(0, 368);
			this.labelDealCost.Name = "labelDealCost";
			this.labelDealCost.TabIndex = 65;
			this.labelDealCost.Text = "DEAL COSTS:";
			// 
			// labelAdvertisingCosts
			// 
			this.labelAdvertisingCosts.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.labelAdvertisingCosts.Location = new System.Drawing.Point(16, 96);
			this.labelAdvertisingCosts.Name = "labelAdvertisingCosts";
			this.labelAdvertisingCosts.Size = new System.Drawing.Size(96, 23);
			this.labelAdvertisingCosts.TabIndex = 14;
			this.labelAdvertisingCosts.Text = "Advertising Costs:";
			// 
			// labelSellingFees
			// 
			this.labelSellingFees.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.labelSellingFees.Location = new System.Drawing.Point(16, 120);
			this.labelSellingFees.Name = "labelSellingFees";
			this.labelSellingFees.Size = new System.Drawing.Size(96, 23);
			this.labelSellingFees.TabIndex = 23;
			this.labelSellingFees.Text = "Selling Fees:";
			// 
			// labelClosingCostsAtPurchase
			// 
			this.labelClosingCostsAtPurchase.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.labelClosingCostsAtPurchase.Location = new System.Drawing.Point(224, 96);
			this.labelClosingCostsAtPurchase.Name = "labelClosingCostsAtPurchase";
			this.labelClosingCostsAtPurchase.Size = new System.Drawing.Size(144, 23);
			this.labelClosingCostsAtPurchase.TabIndex = 17;
			this.labelClosingCostsAtPurchase.Text = "Closing Costs At Purchase:";
			// 
			// labelClosingCostsAtResale
			// 
			this.labelClosingCostsAtResale.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.labelClosingCostsAtResale.Location = new System.Drawing.Point(224, 120);
			this.labelClosingCostsAtResale.Name = "labelClosingCostsAtResale";
			this.labelClosingCostsAtResale.Size = new System.Drawing.Size(136, 23);
			this.labelClosingCostsAtResale.TabIndex = 26;
			this.labelClosingCostsAtResale.Text = "Closing Costs At Resale:";
			// 
			// labelHousePreparationCosts
			// 
			this.labelHousePreparationCosts.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.labelHousePreparationCosts.Location = new System.Drawing.Point(480, 96);
			this.labelHousePreparationCosts.Name = "labelHousePreparationCosts";
			this.labelHousePreparationCosts.Size = new System.Drawing.Size(136, 23);
			this.labelHousePreparationCosts.TabIndex = 20;
			this.labelHousePreparationCosts.Text = "House Preparation Costs:";
			// 
			// labelMiscellaneousCosts
			// 
			this.labelMiscellaneousCosts.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.labelMiscellaneousCosts.Location = new System.Drawing.Point(480, 120);
			this.labelMiscellaneousCosts.Name = "labelMiscellaneousCosts";
			this.labelMiscellaneousCosts.Size = new System.Drawing.Size(128, 23);
			this.labelMiscellaneousCosts.TabIndex = 29;
			this.labelMiscellaneousCosts.Text = "Miscellaneous Costs:";
			// 
			// groupBoxInterestOnlyLoan
			// 
			this.groupBoxInterestOnlyLoan.Controls.Add(this.radioButtonInterestLoanNo);
			this.groupBoxInterestOnlyLoan.Controls.Add(this.radioButtonInterestLoanYes);
			this.groupBoxInterestOnlyLoan.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.groupBoxInterestOnlyLoan.Location = new System.Drawing.Point(120, 192);
			this.groupBoxInterestOnlyLoan.Name = "groupBoxInterestOnlyLoan";
			this.groupBoxInterestOnlyLoan.Size = new System.Drawing.Size(152, 40);
			this.groupBoxInterestOnlyLoan.TabIndex = 37;
			this.groupBoxInterestOnlyLoan.TabStop = false;
			this.groupBoxInterestOnlyLoan.Text = "Is this interest only Loan?";
			// 
			// radioButtonInterestLoanNo
			// 
			this.radioButtonInterestLoanNo.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.radioButtonInterestLoanNo.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.radioButtonInterestLoanNo.Location = new System.Drawing.Point(80, 16);
			this.radioButtonInterestLoanNo.Name = "radioButtonInterestLoanNo";
			this.radioButtonInterestLoanNo.Size = new System.Drawing.Size(40, 16);
			this.radioButtonInterestLoanNo.TabIndex = 1;
			this.radioButtonInterestLoanNo.Text = "No";
			this.radioButtonInterestLoanNo.CheckedChanged += new System.EventHandler(this.BussinesLogic);
			// 
			// radioButtonInterestLoanYes
			// 
			this.radioButtonInterestLoanYes.Checked = true;
			this.radioButtonInterestLoanYes.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.radioButtonInterestLoanYes.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.radioButtonInterestLoanYes.Location = new System.Drawing.Point(8, 16);
			this.radioButtonInterestLoanYes.Name = "radioButtonInterestLoanYes";
			this.radioButtonInterestLoanYes.Size = new System.Drawing.Size(48, 16);
			this.radioButtonInterestLoanYes.TabIndex = 0;
			this.radioButtonInterestLoanYes.TabStop = true;
			this.radioButtonInterestLoanYes.Text = "Yes";
			this.radioButtonInterestLoanYes.CheckedChanged += new System.EventHandler(this.BussinesLogic);
			// 
			// labelMonthlyPayment
			// 
			this.labelMonthlyPayment.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.labelMonthlyPayment.Location = new System.Drawing.Point(552, 192);
			this.labelMonthlyPayment.Name = "labelMonthlyPayment";
			this.labelMonthlyPayment.Size = new System.Drawing.Size(96, 23);
			this.labelMonthlyPayment.TabIndex = 44;
			this.labelMonthlyPayment.Text = "Monthly Payment:";
			// 
			// labelLoanPoints
			// 
			this.labelLoanPoints.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.labelLoanPoints.Location = new System.Drawing.Point(280, 216);
			this.labelLoanPoints.Name = "labelLoanPoints";
			this.labelLoanPoints.Size = new System.Drawing.Size(72, 23);
			this.labelLoanPoints.TabIndex = 40;
			this.labelLoanPoints.Text = "Loan Points:";
			// 
			// labelLoanAmount
			// 
			this.labelLoanAmount.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.labelLoanAmount.Location = new System.Drawing.Point(280, 192);
			this.labelLoanAmount.Name = "labelLoanAmount";
			this.labelLoanAmount.Size = new System.Drawing.Size(80, 23);
			this.labelLoanAmount.TabIndex = 38;
			this.labelLoanAmount.Text = "Loan Amount:";
			// 
			// labelInterestRate
			// 
			this.labelInterestRate.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.labelInterestRate.Location = new System.Drawing.Point(416, 192);
			this.labelInterestRate.Name = "labelInterestRate";
			this.labelInterestRate.Size = new System.Drawing.Size(72, 23);
			this.labelInterestRate.TabIndex = 42;
			this.labelInterestRate.Text = "Interest Rate:";
			// 
			// labelOtherCosts
			// 
			this.labelOtherCosts.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.labelOtherCosts.Location = new System.Drawing.Point(240, 240);
			this.labelOtherCosts.Name = "labelOtherCosts";
			this.labelOtherCosts.Size = new System.Drawing.Size(72, 23);
			this.labelOtherCosts.TabIndex = 46;
			this.labelOtherCosts.Text = "Other Costs:";
			// 
			// labelLTV
			// 
			this.labelLTV.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.labelLTV.Location = new System.Drawing.Point(416, 216);
			this.labelLTV.Name = "labelLTV";
			this.labelLTV.Size = new System.Drawing.Size(32, 23);
			this.labelLTV.TabIndex = 49;
			this.labelLTV.Text = "LTV:";
			// 
			// buttonBreakDownAdvertisingCosts
			// 
			this.buttonBreakDownAdvertisingCosts.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.buttonBreakDownAdvertisingCosts.Image = ((System.Drawing.Image)(resources.GetObject("buttonBreakDownAdvertisingCosts.Image")));
			this.buttonBreakDownAdvertisingCosts.Location = new System.Drawing.Point(192, 96);
			this.buttonBreakDownAdvertisingCosts.Name = "buttonBreakDownAdvertisingCosts";
			this.buttonBreakDownAdvertisingCosts.Size = new System.Drawing.Size(24, 23);
			this.buttonBreakDownAdvertisingCosts.TabIndex = 16;
			this.buttonBreakDownAdvertisingCosts.Click += new System.EventHandler(this.buttonBreakDownAdvertisingCosts_Click);
			// 
			// labelTotalFinancingCosts
			// 
			this.labelTotalFinancingCosts.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.labelTotalFinancingCosts.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.labelTotalFinancingCosts.Location = new System.Drawing.Point(496, 248);
			this.labelTotalFinancingCosts.Name = "labelTotalFinancingCosts";
			this.labelTotalFinancingCosts.Size = new System.Drawing.Size(128, 23);
			this.labelTotalFinancingCosts.TabIndex = 52;
			this.labelTotalFinancingCosts.Text = "Total Financing Costs:";
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.radioButtonPaymentsResaleNo);
			this.groupBox1.Controls.Add(this.radioButtonPaymentsResaleYes);
			this.groupBox1.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.groupBox1.Location = new System.Drawing.Point(112, 272);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(224, 40);
			this.groupBox1.TabIndex = 55;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Are You making Payments until Resale?";
			// 
			// radioButtonPaymentsResaleNo
			// 
			this.radioButtonPaymentsResaleNo.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.radioButtonPaymentsResaleNo.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.radioButtonPaymentsResaleNo.Location = new System.Drawing.Point(80, 16);
			this.radioButtonPaymentsResaleNo.Name = "radioButtonPaymentsResaleNo";
			this.radioButtonPaymentsResaleNo.Size = new System.Drawing.Size(40, 16);
			this.radioButtonPaymentsResaleNo.TabIndex = 1;
			this.radioButtonPaymentsResaleNo.Text = "No";
			this.radioButtonPaymentsResaleNo.CheckedChanged += new System.EventHandler(this.BussinesLogic);
			// 
			// radioButtonPaymentsResaleYes
			// 
			this.radioButtonPaymentsResaleYes.Checked = true;
			this.radioButtonPaymentsResaleYes.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.radioButtonPaymentsResaleYes.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.radioButtonPaymentsResaleYes.Location = new System.Drawing.Point(8, 16);
			this.radioButtonPaymentsResaleYes.Name = "radioButtonPaymentsResaleYes";
			this.radioButtonPaymentsResaleYes.Size = new System.Drawing.Size(48, 16);
			this.radioButtonPaymentsResaleYes.TabIndex = 0;
			this.radioButtonPaymentsResaleYes.TabStop = true;
			this.radioButtonPaymentsResaleYes.Text = "Yes";
			this.radioButtonPaymentsResaleYes.CheckedChanged += new System.EventHandler(this.BussinesLogic);
			// 
			// labelHoldingCostsOther
			// 
			this.labelHoldingCostsOther.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.labelHoldingCostsOther.Location = new System.Drawing.Point(256, 312);
			this.labelHoldingCostsOther.Name = "labelHoldingCostsOther";
			this.labelHoldingCostsOther.Size = new System.Drawing.Size(120, 23);
			this.labelHoldingCostsOther.TabIndex = 58;
			this.labelHoldingCostsOther.Text = "Holding Costs - Other:";
			// 
			// labelHoldingCostsMonthlyPayments
			// 
			this.labelHoldingCostsMonthlyPayments.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.labelHoldingCostsMonthlyPayments.Location = new System.Drawing.Point(16, 312);
			this.labelHoldingCostsMonthlyPayments.Name = "labelHoldingCostsMonthlyPayments";
			this.labelHoldingCostsMonthlyPayments.Size = new System.Drawing.Size(184, 23);
			this.labelHoldingCostsMonthlyPayments.TabIndex = 56;
			this.labelHoldingCostsMonthlyPayments.Text = "Holding Costs - Monthly Payments:";
			// 
			// labelTotalHoldingCost
			// 
			this.labelTotalHoldingCost.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.labelTotalHoldingCost.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.labelTotalHoldingCost.Location = new System.Drawing.Point(504, 336);
			this.labelTotalHoldingCost.Name = "labelTotalHoldingCost";
			this.labelTotalHoldingCost.Size = new System.Drawing.Size(112, 23);
			this.labelTotalHoldingCost.TabIndex = 61;
			this.labelTotalHoldingCost.Text = "Total Holding Costs:";
			// 
			// labelHoldingMurphyFactor
			// 
			this.labelHoldingMurphyFactor.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.labelHoldingMurphyFactor.Location = new System.Drawing.Point(256, 336);
			this.labelHoldingMurphyFactor.Name = "labelHoldingMurphyFactor";
			this.labelHoldingMurphyFactor.Size = new System.Drawing.Size(80, 23);
			this.labelHoldingMurphyFactor.TabIndex = 63;
			this.labelHoldingMurphyFactor.Text = "Murphy Factor:";
			// 
			// labelOtherCashInDeal
			// 
			this.labelOtherCashInDeal.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.labelOtherCashInDeal.Location = new System.Drawing.Point(16, 392);
			this.labelOtherCashInDeal.Name = "labelOtherCashInDeal";
			this.labelOtherCashInDeal.Size = new System.Drawing.Size(104, 23);
			this.labelOtherCashInDeal.TabIndex = 66;
			this.labelOtherCashInDeal.Text = "Other Cash in Deal:";
			// 
			// labelApproximateLoanPayoff
			// 
			this.labelApproximateLoanPayoff.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.labelApproximateLoanPayoff.Location = new System.Drawing.Point(192, 392);
			this.labelApproximateLoanPayoff.Name = "labelApproximateLoanPayoff";
			this.labelApproximateLoanPayoff.Size = new System.Drawing.Size(136, 23);
			this.labelApproximateLoanPayoff.TabIndex = 68;
			this.labelApproximateLoanPayoff.Text = "Approximate Loan Payoff:";
			// 
			// labelTaxesDue
			// 
			this.labelTaxesDue.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.labelTaxesDue.Location = new System.Drawing.Point(400, 392);
			this.labelTaxesDue.Name = "labelTaxesDue";
			this.labelTaxesDue.Size = new System.Drawing.Size(64, 23);
			this.labelTaxesDue.TabIndex = 70;
			this.labelTaxesDue.Text = "Taxes Due:";
			// 
			// labelOtherEncumbrances
			// 
			this.labelOtherEncumbrances.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.labelOtherEncumbrances.Location = new System.Drawing.Point(544, 392);
			this.labelOtherEncumbrances.Name = "labelOtherEncumbrances";
			this.labelOtherEncumbrances.Size = new System.Drawing.Size(120, 23);
			this.labelOtherEncumbrances.TabIndex = 72;
			this.labelOtherEncumbrances.Text = "Other Encumbrances:";
			// 
			// labelTotalDealCost
			// 
			this.labelTotalDealCost.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.labelTotalDealCost.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.labelTotalDealCost.Location = new System.Drawing.Point(224, 416);
			this.labelTotalDealCost.Name = "labelTotalDealCost";
			this.labelTotalDealCost.Size = new System.Drawing.Size(96, 23);
			this.labelTotalDealCost.TabIndex = 74;
			this.labelTotalDealCost.Text = "Total Deal Costs:";
			// 
			// labelMaximumPurchasePrice
			// 
			this.labelMaximumPurchasePrice.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.labelMaximumPurchasePrice.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.labelMaximumPurchasePrice.Location = new System.Drawing.Point(416, 440);
			this.labelMaximumPurchasePrice.Name = "labelMaximumPurchasePrice";
			this.labelMaximumPurchasePrice.Size = new System.Drawing.Size(200, 23);
			this.labelMaximumPurchasePrice.TabIndex = 78;
			this.labelMaximumPurchasePrice.Text = "Your MAXIMUM PURCHASE PRICE:";
			// 
			// labelTotalTransactionalCost
			// 
			this.labelTotalTransactionalCost.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.labelTotalTransactionalCost.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.labelTotalTransactionalCost.Location = new System.Drawing.Point(464, 152);
			this.labelTotalTransactionalCost.Name = "labelTotalTransactionalCost";
			this.labelTotalTransactionalCost.Size = new System.Drawing.Size(144, 23);
			this.labelTotalTransactionalCost.TabIndex = 32;
			this.labelTotalTransactionalCost.Text = "Total Transactional Costs:";
			// 
			// labelDesiredProfitInPercentage
			// 
			this.labelDesiredProfitInPercentage.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.labelDesiredProfitInPercentage.Location = new System.Drawing.Point(352, 48);
			this.labelDesiredProfitInPercentage.Name = "labelDesiredProfitInPercentage";
			this.labelDesiredProfitInPercentage.Size = new System.Drawing.Size(104, 23);
			this.labelDesiredProfitInPercentage.TabIndex = 3;
			this.labelDesiredProfitInPercentage.Text = "Desired Profit in %:";
			// 
			// labelDesiredProfitInBucks
			// 
			this.labelDesiredProfitInBucks.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.labelDesiredProfitInBucks.Location = new System.Drawing.Point(512, 48);
			this.labelDesiredProfitInBucks.Name = "labelDesiredProfitInBucks";
			this.labelDesiredProfitInBucks.Size = new System.Drawing.Size(104, 23);
			this.labelDesiredProfitInBucks.TabIndex = 9;
			this.labelDesiredProfitInBucks.Text = "Desired Profit in $:";
			// 
			// buttonBreakDownBoxSellingFees
			// 
			this.buttonBreakDownBoxSellingFees.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.buttonBreakDownBoxSellingFees.Image = ((System.Drawing.Image)(resources.GetObject("buttonBreakDownBoxSellingFees.Image")));
			this.buttonBreakDownBoxSellingFees.Location = new System.Drawing.Point(192, 120);
			this.buttonBreakDownBoxSellingFees.Name = "buttonBreakDownBoxSellingFees";
			this.buttonBreakDownBoxSellingFees.Size = new System.Drawing.Size(24, 23);
			this.buttonBreakDownBoxSellingFees.TabIndex = 25;
			this.buttonBreakDownBoxSellingFees.Click += new System.EventHandler(this.buttonBreakDownBoxSellingFees_Click);
			// 
			// buttonBreakDownHousePreparationCosts
			// 
			this.buttonBreakDownHousePreparationCosts.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.buttonBreakDownHousePreparationCosts.Image = ((System.Drawing.Image)(resources.GetObject("buttonBreakDownHousePreparationCosts.Image")));
			this.buttonBreakDownHousePreparationCosts.Location = new System.Drawing.Point(696, 96);
			this.buttonBreakDownHousePreparationCosts.Name = "buttonBreakDownHousePreparationCosts";
			this.buttonBreakDownHousePreparationCosts.Size = new System.Drawing.Size(24, 23);
			this.buttonBreakDownHousePreparationCosts.TabIndex = 22;
			this.buttonBreakDownHousePreparationCosts.Click += new System.EventHandler(this.buttonBreakDownHousePreparationCosts_Click);
			// 
			// buttonBreakDownClosingCostsAtPurchase
			// 
			this.buttonBreakDownClosingCostsAtPurchase.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.buttonBreakDownClosingCostsAtPurchase.Image = ((System.Drawing.Image)(resources.GetObject("buttonBreakDownClosingCostsAtPurchase.Image")));
			this.buttonBreakDownClosingCostsAtPurchase.Location = new System.Drawing.Point(440, 96);
			this.buttonBreakDownClosingCostsAtPurchase.Name = "buttonBreakDownClosingCostsAtPurchase";
			this.buttonBreakDownClosingCostsAtPurchase.Size = new System.Drawing.Size(24, 23);
			this.buttonBreakDownClosingCostsAtPurchase.TabIndex = 19;
			this.buttonBreakDownClosingCostsAtPurchase.Click += new System.EventHandler(this.buttonBreakDownClosingCostsAtPurchase_Click);
			// 
			// buttonBreakDownClosingCostsAtResale
			// 
			this.buttonBreakDownClosingCostsAtResale.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.buttonBreakDownClosingCostsAtResale.Image = ((System.Drawing.Image)(resources.GetObject("buttonBreakDownClosingCostsAtResale.Image")));
			this.buttonBreakDownClosingCostsAtResale.Location = new System.Drawing.Point(440, 120);
			this.buttonBreakDownClosingCostsAtResale.Name = "buttonBreakDownClosingCostsAtResale";
			this.buttonBreakDownClosingCostsAtResale.Size = new System.Drawing.Size(24, 23);
			this.buttonBreakDownClosingCostsAtResale.TabIndex = 28;
			this.buttonBreakDownClosingCostsAtResale.Click += new System.EventHandler(this.buttonBreakDownClosingCostsAtResale_Click);
			// 
			// buttonBreakDownMiscellaneousCosts
			// 
			this.buttonBreakDownMiscellaneousCosts.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.buttonBreakDownMiscellaneousCosts.Image = ((System.Drawing.Image)(resources.GetObject("buttonBreakDownMiscellaneousCosts.Image")));
			this.buttonBreakDownMiscellaneousCosts.Location = new System.Drawing.Point(696, 120);
			this.buttonBreakDownMiscellaneousCosts.Name = "buttonBreakDownMiscellaneousCosts";
			this.buttonBreakDownMiscellaneousCosts.Size = new System.Drawing.Size(24, 23);
			this.buttonBreakDownMiscellaneousCosts.TabIndex = 31;
			this.buttonBreakDownMiscellaneousCosts.Click += new System.EventHandler(this.buttonBreakDownMiscellaneousCosts_Click);
			// 
			// buttonBreakDownOtherCosts
			// 
			this.buttonBreakDownOtherCosts.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.buttonBreakDownOtherCosts.Image = ((System.Drawing.Image)(resources.GetObject("buttonBreakDownOtherCosts.Image")));
			this.buttonBreakDownOtherCosts.Location = new System.Drawing.Point(408, 240);
			this.buttonBreakDownOtherCosts.Name = "buttonBreakDownOtherCosts";
			this.buttonBreakDownOtherCosts.Size = new System.Drawing.Size(24, 23);
			this.buttonBreakDownOtherCosts.TabIndex = 48;
			this.buttonBreakDownOtherCosts.Click += new System.EventHandler(this.buttonBreakDownOtherCosts_Click);
			// 
			// buttonBreakDownHoldingCostsOther
			// 
			this.buttonBreakDownHoldingCostsOther.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.buttonBreakDownHoldingCostsOther.Image = ((System.Drawing.Image)(resources.GetObject("buttonBreakDownHoldingCostsOther.Image")));
			this.buttonBreakDownHoldingCostsOther.Location = new System.Drawing.Point(464, 312);
			this.buttonBreakDownHoldingCostsOther.Name = "buttonBreakDownHoldingCostsOther";
			this.buttonBreakDownHoldingCostsOther.Size = new System.Drawing.Size(24, 23);
			this.buttonBreakDownHoldingCostsOther.TabIndex = 60;
			this.buttonBreakDownHoldingCostsOther.Click += new System.EventHandler(this.buttonBreakDownHoldingCostsOther_Click);
			// 
			// labelTransactionalMurphyFactor
			// 
			this.labelTransactionalMurphyFactor.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.labelTransactionalMurphyFactor.Location = new System.Drawing.Point(272, 152);
			this.labelTransactionalMurphyFactor.Name = "labelTransactionalMurphyFactor";
			this.labelTransactionalMurphyFactor.Size = new System.Drawing.Size(80, 23);
			this.labelTransactionalMurphyFactor.TabIndex = 34;
			this.labelTransactionalMurphyFactor.Text = "Murphy Factor:";
			this.labelTransactionalMurphyFactor.Visible = false;
			// 
			// textBoxAdvertisingCosts
			// 
			this.textBoxAdvertisingCosts.Location = new System.Drawing.Point(104, 96);
			this.textBoxAdvertisingCosts.Name = "textBoxAdvertisingCosts";
			this.textBoxAdvertisingCosts.Size = new System.Drawing.Size(90, 20);
			this.textBoxAdvertisingCosts.TabIndex = 15;
			this.textBoxAdvertisingCosts.Text = "$0.00";
			this.textBoxAdvertisingCosts.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.textBoxAdvertisingCosts.Leave += new System.EventHandler(this.BussinesLogic);
			// 
			// textBoxSellingFees
			// 
			this.textBoxSellingFees.Location = new System.Drawing.Point(104, 120);
			this.textBoxSellingFees.Name = "textBoxSellingFees";
			this.textBoxSellingFees.Size = new System.Drawing.Size(90, 20);
			this.textBoxSellingFees.TabIndex = 24;
			this.textBoxSellingFees.Text = "$0.00";
			this.textBoxSellingFees.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.textBoxSellingFees.Leave += new System.EventHandler(this.BussinesLogic);
			// 
			// textBoxClosingCostsAtPurchase
			// 
			this.textBoxClosingCostsAtPurchase.Location = new System.Drawing.Point(352, 96);
			this.textBoxClosingCostsAtPurchase.Name = "textBoxClosingCostsAtPurchase";
			this.textBoxClosingCostsAtPurchase.Size = new System.Drawing.Size(90, 20);
			this.textBoxClosingCostsAtPurchase.TabIndex = 18;
			this.textBoxClosingCostsAtPurchase.Text = "$0.00";
			this.textBoxClosingCostsAtPurchase.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.textBoxClosingCostsAtPurchase.Leave += new System.EventHandler(this.BussinesLogic);
			// 
			// textBoxClosingCostsAtResale
			// 
			this.textBoxClosingCostsAtResale.Location = new System.Drawing.Point(352, 120);
			this.textBoxClosingCostsAtResale.Name = "textBoxClosingCostsAtResale";
			this.textBoxClosingCostsAtResale.Size = new System.Drawing.Size(90, 20);
			this.textBoxClosingCostsAtResale.TabIndex = 27;
			this.textBoxClosingCostsAtResale.Text = "$0.00";
			this.textBoxClosingCostsAtResale.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.textBoxClosingCostsAtResale.Leave += new System.EventHandler(this.BussinesLogic);
			// 
			// textBoxRetailAuctionPercentage
			// 
			this.textBoxRetailAuctionPercentage.Location = new System.Drawing.Point(288, 24);
			this.textBoxRetailAuctionPercentage.Name = "textBoxRetailAuctionPercentage";
			this.textBoxRetailAuctionPercentage.Size = new System.Drawing.Size(60, 20);
			this.textBoxRetailAuctionPercentage.TabIndex = 2;
			this.textBoxRetailAuctionPercentage.Text = "0%";
			this.textBoxRetailAuctionPercentage.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.textBoxRetailAuctionPercentage.Leave += new System.EventHandler(this.BussinesLogic);
			// 
			// textBoxProjectedResalePrice
			// 
			this.textBoxProjectedResalePrice.Location = new System.Drawing.Point(624, 24);
			this.textBoxProjectedResalePrice.Name = "textBoxProjectedResalePrice";
			this.textBoxProjectedResalePrice.ReadOnly = true;
			this.textBoxProjectedResalePrice.TabIndex = 8;
			this.textBoxProjectedResalePrice.Text = "$0.00";
			this.textBoxProjectedResalePrice.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.textBoxProjectedResalePrice.Leave += new System.EventHandler(this.BussinesLogic);
			// 
			// textBoxDesiredProfitInPercentage
			// 
			this.textBoxDesiredProfitInPercentage.Location = new System.Drawing.Point(440, 48);
			this.textBoxDesiredProfitInPercentage.Name = "textBoxDesiredProfitInPercentage";
			this.textBoxDesiredProfitInPercentage.Size = new System.Drawing.Size(60, 20);
			this.textBoxDesiredProfitInPercentage.TabIndex = 4;
			this.textBoxDesiredProfitInPercentage.Text = "0%";
			this.textBoxDesiredProfitInPercentage.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.textBoxDesiredProfitInPercentage.Leave += new System.EventHandler(this.BussinesLogic);
			// 
			// textBoxDesiredProfitInBucks
			// 
			this.textBoxDesiredProfitInBucks.Location = new System.Drawing.Point(624, 48);
			this.textBoxDesiredProfitInBucks.Name = "textBoxDesiredProfitInBucks";
			this.textBoxDesiredProfitInBucks.ReadOnly = true;
			this.textBoxDesiredProfitInBucks.TabIndex = 10;
			this.textBoxDesiredProfitInBucks.Text = "$0.00";
			this.textBoxDesiredProfitInBucks.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.textBoxDesiredProfitInBucks.Leave += new System.EventHandler(this.BussinesLogic);
			// 
			// textBoxHousePreparationCosts
			// 
			this.textBoxHousePreparationCosts.Location = new System.Drawing.Point(608, 96);
			this.textBoxHousePreparationCosts.Name = "textBoxHousePreparationCosts";
			this.textBoxHousePreparationCosts.Size = new System.Drawing.Size(90, 20);
			this.textBoxHousePreparationCosts.TabIndex = 21;
			this.textBoxHousePreparationCosts.Text = "$0.00";
			this.textBoxHousePreparationCosts.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.textBoxHousePreparationCosts.Leave += new System.EventHandler(this.BussinesLogic);
			// 
			// textBoxMiscellaneousCosts
			// 
			this.textBoxMiscellaneousCosts.Location = new System.Drawing.Point(608, 120);
			this.textBoxMiscellaneousCosts.Name = "textBoxMiscellaneousCosts";
			this.textBoxMiscellaneousCosts.Size = new System.Drawing.Size(90, 20);
			this.textBoxMiscellaneousCosts.TabIndex = 30;
			this.textBoxMiscellaneousCosts.Text = "$0.00";
			this.textBoxMiscellaneousCosts.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.textBoxMiscellaneousCosts.Leave += new System.EventHandler(this.BussinesLogic);
			// 
			// textBoxTotalTransactionalCosts
			// 
			this.textBoxTotalTransactionalCosts.Location = new System.Drawing.Point(608, 152);
			this.textBoxTotalTransactionalCosts.Name = "textBoxTotalTransactionalCosts";
			this.textBoxTotalTransactionalCosts.ReadOnly = true;
			this.textBoxTotalTransactionalCosts.TabIndex = 33;
			this.textBoxTotalTransactionalCosts.Text = "$0.00";
			this.textBoxTotalTransactionalCosts.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.textBoxTotalTransactionalCosts.Leave += new System.EventHandler(this.BussinesLogic);
			// 
			// textBoxTransactionalMurphyFactor
			// 
			this.textBoxTransactionalMurphyFactor.Location = new System.Drawing.Point(352, 152);
			this.textBoxTransactionalMurphyFactor.Name = "textBoxTransactionalMurphyFactor";
			this.textBoxTransactionalMurphyFactor.Size = new System.Drawing.Size(60, 20);
			this.textBoxTransactionalMurphyFactor.TabIndex = 35;
			this.textBoxTransactionalMurphyFactor.Text = "0%";
			this.textBoxTransactionalMurphyFactor.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.textBoxTransactionalMurphyFactor.Visible = false;
			this.textBoxTransactionalMurphyFactor.Leave += new System.EventHandler(this.BussinesLogic);
			// 
			// textBoxOtherCosts
			// 
			this.textBoxOtherCosts.Location = new System.Drawing.Point(320, 240);
			this.textBoxOtherCosts.Name = "textBoxOtherCosts";
			this.textBoxOtherCosts.Size = new System.Drawing.Size(90, 20);
			this.textBoxOtherCosts.TabIndex = 47;
			this.textBoxOtherCosts.Text = "$0.00";
			this.textBoxOtherCosts.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.textBoxOtherCosts.Leave += new System.EventHandler(this.BussinesLogic);
			// 
			// textBoxLoanPoints
			// 
			this.textBoxLoanPoints.Location = new System.Drawing.Point(352, 216);
			this.textBoxLoanPoints.Name = "textBoxLoanPoints";
			this.textBoxLoanPoints.Size = new System.Drawing.Size(60, 20);
			this.textBoxLoanPoints.TabIndex = 41;
			this.textBoxLoanPoints.Text = "0%";
			this.textBoxLoanPoints.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.textBoxLoanPoints.Leave += new System.EventHandler(this.BussinesLogic);
			// 
			// textBoxInterestRate
			// 
			this.textBoxInterestRate.Location = new System.Drawing.Point(488, 192);
			this.textBoxInterestRate.Name = "textBoxInterestRate";
			this.textBoxInterestRate.Size = new System.Drawing.Size(60, 20);
			this.textBoxInterestRate.TabIndex = 43;
			this.textBoxInterestRate.Text = "0%";
			this.textBoxInterestRate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.textBoxInterestRate.Leave += new System.EventHandler(this.BussinesLogic);
			// 
			// textBoxLoanAmount
			// 
			this.textBoxLoanAmount.Location = new System.Drawing.Point(352, 192);
			this.textBoxLoanAmount.Name = "textBoxLoanAmount";
			this.textBoxLoanAmount.Size = new System.Drawing.Size(60, 20);
			this.textBoxLoanAmount.TabIndex = 39;
			this.textBoxLoanAmount.Text = "$0.00";
			this.textBoxLoanAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.textBoxLoanAmount.Leave += new System.EventHandler(this.BussinesLogic);
			// 
			// textBoxMonthlyPayment
			// 
			this.textBoxMonthlyPayment.Location = new System.Drawing.Point(640, 192);
			this.textBoxMonthlyPayment.Name = "textBoxMonthlyPayment";
			this.textBoxMonthlyPayment.ReadOnly = true;
			this.textBoxMonthlyPayment.Size = new System.Drawing.Size(60, 20);
			this.textBoxMonthlyPayment.TabIndex = 45;
			this.textBoxMonthlyPayment.Text = "$0.00";
			this.textBoxMonthlyPayment.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.textBoxMonthlyPayment.Leave += new System.EventHandler(this.BussinesLogic);
			// 
			// textBoxLTV
			// 
			this.textBoxLTV.Location = new System.Drawing.Point(488, 216);
			this.textBoxLTV.Name = "textBoxLTV";
			this.textBoxLTV.Size = new System.Drawing.Size(60, 20);
			this.textBoxLTV.TabIndex = 50;
			this.textBoxLTV.Text = "0%";
			this.textBoxLTV.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.textBoxLTV.Leave += new System.EventHandler(this.BussinesLogic);
			// 
			// textBoxTotalFinancingCosts
			// 
			this.textBoxTotalFinancingCosts.Location = new System.Drawing.Point(616, 248);
			this.textBoxTotalFinancingCosts.Name = "textBoxTotalFinancingCosts";
			this.textBoxTotalFinancingCosts.ReadOnly = true;
			this.textBoxTotalFinancingCosts.TabIndex = 53;
			this.textBoxTotalFinancingCosts.Text = "$0.00";
			this.textBoxTotalFinancingCosts.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.textBoxTotalFinancingCosts.Leave += new System.EventHandler(this.BussinesLogic);
			// 
			// textBoxHoldingCostsMonthlyPayments
			// 
			this.textBoxHoldingCostsMonthlyPayments.Location = new System.Drawing.Point(184, 312);
			this.textBoxHoldingCostsMonthlyPayments.Name = "textBoxHoldingCostsMonthlyPayments";
			this.textBoxHoldingCostsMonthlyPayments.Size = new System.Drawing.Size(60, 20);
			this.textBoxHoldingCostsMonthlyPayments.TabIndex = 57;
			this.textBoxHoldingCostsMonthlyPayments.Text = "$0.00";
			this.textBoxHoldingCostsMonthlyPayments.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.textBoxHoldingCostsMonthlyPayments.Leave += new System.EventHandler(this.BussinesLogic);
			// 
			// textBoxTotalHoldingCost
			// 
			this.textBoxTotalHoldingCost.Location = new System.Drawing.Point(616, 336);
			this.textBoxTotalHoldingCost.Name = "textBoxTotalHoldingCost";
			this.textBoxTotalHoldingCost.ReadOnly = true;
			this.textBoxTotalHoldingCost.TabIndex = 62;
			this.textBoxTotalHoldingCost.Text = "$0.00";
			this.textBoxTotalHoldingCost.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.textBoxTotalHoldingCost.Leave += new System.EventHandler(this.BussinesLogic);
			// 
			// textBoxHoldingCostsOther
			// 
			this.textBoxHoldingCostsOther.Location = new System.Drawing.Point(368, 312);
			this.textBoxHoldingCostsOther.Name = "textBoxHoldingCostsOther";
			this.textBoxHoldingCostsOther.TabIndex = 59;
			this.textBoxHoldingCostsOther.Text = "$0.00";
			this.textBoxHoldingCostsOther.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.textBoxHoldingCostsOther.Leave += new System.EventHandler(this.BussinesLogic);
			// 
			// textBoxHoldingMurphyFactor
			// 
			this.textBoxHoldingMurphyFactor.Location = new System.Drawing.Point(368, 336);
			this.textBoxHoldingMurphyFactor.Name = "textBoxHoldingMurphyFactor";
			this.textBoxHoldingMurphyFactor.Size = new System.Drawing.Size(60, 20);
			this.textBoxHoldingMurphyFactor.TabIndex = 64;
			this.textBoxHoldingMurphyFactor.Text = "0%";
			this.textBoxHoldingMurphyFactor.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.textBoxHoldingMurphyFactor.Leave += new System.EventHandler(this.BussinesLogic);
			// 
			// textBoxOtherCashInDeal
			// 
			this.textBoxOtherCashInDeal.Location = new System.Drawing.Point(112, 392);
			this.textBoxOtherCashInDeal.Name = "textBoxOtherCashInDeal";
			this.textBoxOtherCashInDeal.Size = new System.Drawing.Size(60, 20);
			this.textBoxOtherCashInDeal.TabIndex = 67;
			this.textBoxOtherCashInDeal.Text = "$0.00";
			this.textBoxOtherCashInDeal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.textBoxOtherCashInDeal.Leave += new System.EventHandler(this.BussinesLogic);
			// 
			// textBoxApproximateLoanPayoff
			// 
			this.textBoxApproximateLoanPayoff.Location = new System.Drawing.Point(320, 392);
			this.textBoxApproximateLoanPayoff.Name = "textBoxApproximateLoanPayoff";
			this.textBoxApproximateLoanPayoff.Size = new System.Drawing.Size(60, 20);
			this.textBoxApproximateLoanPayoff.TabIndex = 69;
			this.textBoxApproximateLoanPayoff.Text = "$0.00";
			this.textBoxApproximateLoanPayoff.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.textBoxApproximateLoanPayoff.Leave += new System.EventHandler(this.BussinesLogic);
			// 
			// textBoxTaxesDue
			// 
			this.textBoxTaxesDue.Location = new System.Drawing.Point(464, 392);
			this.textBoxTaxesDue.Name = "textBoxTaxesDue";
			this.textBoxTaxesDue.Size = new System.Drawing.Size(60, 20);
			this.textBoxTaxesDue.TabIndex = 71;
			this.textBoxTaxesDue.Text = "$0.00";
			this.textBoxTaxesDue.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.textBoxTaxesDue.Leave += new System.EventHandler(this.BussinesLogic);
			// 
			// textBoxOtherEncumbrances
			// 
			this.textBoxOtherEncumbrances.Location = new System.Drawing.Point(656, 392);
			this.textBoxOtherEncumbrances.Name = "textBoxOtherEncumbrances";
			this.textBoxOtherEncumbrances.Size = new System.Drawing.Size(60, 20);
			this.textBoxOtherEncumbrances.TabIndex = 73;
			this.textBoxOtherEncumbrances.Text = "$0.00";
			this.textBoxOtherEncumbrances.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.textBoxOtherEncumbrances.Leave += new System.EventHandler(this.BussinesLogic);
			// 
			// textBoxTotalDealCost
			// 
			this.textBoxTotalDealCost.Location = new System.Drawing.Point(320, 416);
			this.textBoxTotalDealCost.Name = "textBoxTotalDealCost";
			this.textBoxTotalDealCost.Size = new System.Drawing.Size(60, 20);
			this.textBoxTotalDealCost.TabIndex = 75;
			this.textBoxTotalDealCost.Text = "$0.00";
			this.textBoxTotalDealCost.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.textBoxTotalDealCost.Leave += new System.EventHandler(this.BussinesLogic);
			// 
			// textBoxMaximumPurchasePrice
			// 
			this.textBoxMaximumPurchasePrice.Location = new System.Drawing.Point(616, 440);
			this.textBoxMaximumPurchasePrice.Name = "textBoxMaximumPurchasePrice";
			this.textBoxMaximumPurchasePrice.ReadOnly = true;
			this.textBoxMaximumPurchasePrice.TabIndex = 0;
			this.textBoxMaximumPurchasePrice.Text = "$0.00";
			this.textBoxMaximumPurchasePrice.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.textBoxMaximumPurchasePrice.Leave += new System.EventHandler(this.BussinesLogic);
			// 
			// textBoxTotalAcquistionCostOfDeal
			// 
			this.textBoxTotalAcquistionCostOfDeal.Location = new System.Drawing.Point(616, 416);
			this.textBoxTotalAcquistionCostOfDeal.Name = "textBoxTotalAcquistionCostOfDeal";
			this.textBoxTotalAcquistionCostOfDeal.ReadOnly = true;
			this.textBoxTotalAcquistionCostOfDeal.TabIndex = 77;
			this.textBoxTotalAcquistionCostOfDeal.Text = "$0.00";
			this.textBoxTotalAcquistionCostOfDeal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// labelTotalAcquistionCostOfDeal
			// 
			this.labelTotalAcquistionCostOfDeal.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.labelTotalAcquistionCostOfDeal.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.labelTotalAcquistionCostOfDeal.Location = new System.Drawing.Point(448, 416);
			this.labelTotalAcquistionCostOfDeal.Name = "labelTotalAcquistionCostOfDeal";
			this.labelTotalAcquistionCostOfDeal.Size = new System.Drawing.Size(168, 23);
			this.labelTotalAcquistionCostOfDeal.TabIndex = 76;
			this.labelTotalAcquistionCostOfDeal.Text = "Total Acquisition Cost of Deal:";
			// 
			// textBoxProjectedNumberOfMonths
			// 
			this.textBoxProjectedNumberOfMonths.Location = new System.Drawing.Point(288, 48);
			this.textBoxProjectedNumberOfMonths.Name = "textBoxProjectedNumberOfMonths";
			this.textBoxProjectedNumberOfMonths.Size = new System.Drawing.Size(60, 20);
			this.textBoxProjectedNumberOfMonths.TabIndex = 80;
			this.textBoxProjectedNumberOfMonths.Text = "0";
			this.textBoxProjectedNumberOfMonths.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.textBoxProjectedNumberOfMonths.Leave += new System.EventHandler(this.BussinesLogic);
			// 
			// labelProjectedNumberOfMonths
			// 
			this.labelProjectedNumberOfMonths.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.labelProjectedNumberOfMonths.Location = new System.Drawing.Point(168, 48);
			this.labelProjectedNumberOfMonths.Name = "labelProjectedNumberOfMonths";
			this.labelProjectedNumberOfMonths.Size = new System.Drawing.Size(128, 23);
			this.labelProjectedNumberOfMonths.TabIndex = 79;
			this.labelProjectedNumberOfMonths.Text = "Projected No. of Months:";
			// 
			// textBox1
			// 
			this.textBox1.Location = new System.Drawing.Point(640, 216);
			this.textBox1.Name = "textBox1";
			this.textBox1.Size = new System.Drawing.Size(60, 20);
			this.textBox1.TabIndex = 82;
			this.textBox1.Text = "0%";
			this.textBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// labelAmortization
			// 
			this.labelAmortization.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.labelAmortization.Location = new System.Drawing.Point(568, 216);
			this.labelAmortization.Name = "labelAmortization";
			this.labelAmortization.Size = new System.Drawing.Size(72, 23);
			this.labelAmortization.TabIndex = 81;
			this.labelAmortization.Text = "Amortization:";
			// 
			// pictureBox1
			// 
			this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
			this.pictureBox1.Location = new System.Drawing.Point(712, 0);
			this.pictureBox1.Name = "pictureBox1";
			this.pictureBox1.Size = new System.Drawing.Size(16, 16);
			this.pictureBox1.TabIndex = 83;
			this.pictureBox1.TabStop = false;
			// 
			// UserControl_AllCashRetailAuction
			// 
			this.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(210)), ((System.Byte)(216)), ((System.Byte)(240)));
			this.Controls.Add(this.pictureBox1);
			this.Controls.Add(this.textBox1);
			this.Controls.Add(this.labelAmortization);
			this.Controls.Add(this.textBoxProjectedNumberOfMonths);
			this.Controls.Add(this.labelProjectedNumberOfMonths);
			this.Controls.Add(this.textBoxTotalAcquistionCostOfDeal);
			this.Controls.Add(this.labelTotalAcquistionCostOfDeal);
			this.Controls.Add(this.textBoxMaximumPurchasePrice);
			this.Controls.Add(this.textBoxTotalDealCost);
			this.Controls.Add(this.textBoxOtherEncumbrances);
			this.Controls.Add(this.textBoxTaxesDue);
			this.Controls.Add(this.textBoxApproximateLoanPayoff);
			this.Controls.Add(this.textBoxOtherCashInDeal);
			this.Controls.Add(this.textBoxHoldingMurphyFactor);
			this.Controls.Add(this.textBoxHoldingCostsOther);
			this.Controls.Add(this.textBoxTotalHoldingCost);
			this.Controls.Add(this.textBoxHoldingCostsMonthlyPayments);
			this.Controls.Add(this.textBoxTotalFinancingCosts);
			this.Controls.Add(this.textBoxLTV);
			this.Controls.Add(this.textBoxMonthlyPayment);
			this.Controls.Add(this.textBoxLoanAmount);
			this.Controls.Add(this.textBoxInterestRate);
			this.Controls.Add(this.textBoxLoanPoints);
			this.Controls.Add(this.textBoxTransactionalMurphyFactor);
			this.Controls.Add(this.textBoxTotalTransactionalCosts);
			this.Controls.Add(this.textBoxMiscellaneousCosts);
			this.Controls.Add(this.textBoxHousePreparationCosts);
			this.Controls.Add(this.textBoxDesiredProfitInBucks);
			this.Controls.Add(this.textBoxDesiredProfitInPercentage);
			this.Controls.Add(this.textBoxProjectedResalePrice);
			this.Controls.Add(this.textBoxRetailAuctionPercentage);
			this.Controls.Add(this.textBoxClosingCostsAtResale);
			this.Controls.Add(this.textBoxClosingCostsAtPurchase);
			this.Controls.Add(this.textBoxSellingFees);
			this.Controls.Add(this.textBoxAdvertisingCosts);
			this.Controls.Add(this.labelTransactionalMurphyFactor);
			this.Controls.Add(this.buttonBreakDownHoldingCostsOther);
			this.Controls.Add(this.buttonBreakDownOtherCosts);
			this.Controls.Add(this.buttonBreakDownMiscellaneousCosts);
			this.Controls.Add(this.buttonBreakDownClosingCostsAtResale);
			this.Controls.Add(this.buttonBreakDownClosingCostsAtPurchase);
			this.Controls.Add(this.buttonBreakDownHousePreparationCosts);
			this.Controls.Add(this.buttonBreakDownBoxSellingFees);
			this.Controls.Add(this.labelDesiredProfitInPercentage);
			this.Controls.Add(this.labelDesiredProfitInBucks);
			this.Controls.Add(this.labelTotalTransactionalCost);
			this.Controls.Add(this.labelMaximumPurchasePrice);
			this.Controls.Add(this.labelTotalDealCost);
			this.Controls.Add(this.labelOtherEncumbrances);
			this.Controls.Add(this.labelTaxesDue);
			this.Controls.Add(this.labelApproximateLoanPayoff);
			this.Controls.Add(this.labelOtherCashInDeal);
			this.Controls.Add(this.labelHoldingMurphyFactor);
			this.Controls.Add(this.labelTotalHoldingCost);
			this.Controls.Add(this.labelHoldingCostsOther);
			this.Controls.Add(this.labelHoldingCostsMonthlyPayments);
			this.Controls.Add(this.labelTotalFinancingCosts);
			this.Controls.Add(this.buttonBreakDownAdvertisingCosts);
			this.Controls.Add(this.labelLTV);
			this.Controls.Add(this.labelOtherCosts);
			this.Controls.Add(this.labelInterestRate);
			this.Controls.Add(this.labelMonthlyPayment);
			this.Controls.Add(this.labelLoanPoints);
			this.Controls.Add(this.labelLoanAmount);
			this.Controls.Add(this.labelMiscellaneousCosts);
			this.Controls.Add(this.labelHousePreparationCosts);
			this.Controls.Add(this.labelClosingCostsAtResale);
			this.Controls.Add(this.labelClosingCostsAtPurchase);
			this.Controls.Add(this.labelSellingFees);
			this.Controls.Add(this.labelAdvertisingCosts);
			this.Controls.Add(this.labelDealCost);
			this.Controls.Add(this.labelHoldingCosts);
			this.Controls.Add(this.labelFinancingCosts);
			this.Controls.Add(this.labelTransactionalCosts);
			this.Controls.Add(this.labelProjectedResalePrice);
			this.Controls.Add(this.labelRetailAuctionPercentage);
			this.Controls.Add(this.groupBoxExitStrategy);
			this.Controls.Add(this.groupBoxInterestOnlyLoan);
			this.Controls.Add(this.groupBox1);
			this.Controls.Add(this.textBoxOtherCosts);
			this.Name = "UserControl_AllCashRetailAuction";
			this.Size = new System.Drawing.Size(728, 464);
			this.groupBoxExitStrategy.ResumeLayout(false);
			this.groupBoxInterestOnlyLoan.ResumeLayout(false);
			this.groupBox1.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		private void buttonBreakDownAdvertisingCosts_Click(object sender, System.EventArgs e)
		{
			FormBreakDown fbd = new FormBreakDown();
			fbd.userControl_BreakDown.DataArrayCostItems = all_cash.AdvertisingCostsItems;
			fbd.ShowDialog();
			all_cash.AdvertisingCostsItems = fbd.userControl_BreakDown.DataArrayCostItems;
			all_cash.AdvertisingCosts = Decimal.Parse(fbd.userControl_BreakDown.textBoxTotalCost.Text, NumberStyles.Currency);
			textBoxAdvertisingCosts.Text = fbd.userControl_BreakDown.textBoxTotalCost.Text;
			BussinesLogic(buttonBreakDownAdvertisingCosts, new EventArgs());
		}

		private void buttonBreakDownBoxSellingFees_Click(object sender, System.EventArgs e)
		{
			FormBreakDown fbd = new FormBreakDown();
			fbd.userControl_BreakDown.DataArrayCostItems = all_cash.SellingFeesItems;
			fbd.ShowDialog();
			all_cash.SellingFeesItems = fbd.userControl_BreakDown.DataArrayCostItems;
			all_cash.SellingFees = Decimal.Parse(fbd.userControl_BreakDown.textBoxTotalCost.Text, NumberStyles.Currency);
			textBoxSellingFees.Text = fbd.userControl_BreakDown.textBoxTotalCost.Text;
			BussinesLogic(buttonBreakDownBoxSellingFees, new EventArgs());
		}

		private void buttonBreakDownClosingCostsAtPurchase_Click(object sender, System.EventArgs e)
		{
			FormBreakDown fbd = new FormBreakDown();
			fbd.userControl_BreakDown.DataArrayCostItems = all_cash.ClosingCostsAtPurchaseItems;
			fbd.ShowDialog();
			all_cash.ClosingCostsAtPurchaseItems = fbd.userControl_BreakDown.DataArrayCostItems;
			all_cash.ClosingCostsPurcahse = Decimal.Parse(fbd.userControl_BreakDown.textBoxTotalCost.Text, NumberStyles.Currency);
			textBoxClosingCostsAtPurchase.Text = fbd.userControl_BreakDown.textBoxTotalCost.Text;
			BussinesLogic(buttonBreakDownClosingCostsAtPurchase, new EventArgs());
		}

		private void buttonBreakDownClosingCostsAtResale_Click(object sender, System.EventArgs e)
		{
			FormBreakDown fbd = new FormBreakDown();
			fbd.userControl_BreakDown.DataArrayCostItems = all_cash.ClosingCostsAtResaleItems;
			fbd.ShowDialog();
			all_cash.ClosingCostsAtResaleItems = fbd.userControl_BreakDown.DataArrayCostItems;
			all_cash.ClsoingCostsAtResale = Decimal.Parse(fbd.userControl_BreakDown.textBoxTotalCost.Text, NumberStyles.Currency);
			textBoxClosingCostsAtResale.Text = fbd.userControl_BreakDown.textBoxTotalCost.Text;
			BussinesLogic(buttonBreakDownClosingCostsAtResale, new EventArgs());
		}

		private void buttonBreakDownHousePreparationCosts_Click(object sender, System.EventArgs e)
		{
			FormBreakDown fbd = new FormBreakDown();
			fbd.userControl_BreakDown.DataArrayCostItems = all_cash.HousePreparationCostsItems;
			fbd.ShowDialog();
			all_cash.HousePreparationCostsItems = fbd.userControl_BreakDown.DataArrayCostItems;
			all_cash.PreparationCosts = Decimal.Parse(fbd.userControl_BreakDown.textBoxTotalCost.Text);
			textBoxHousePreparationCosts.Text = fbd.userControl_BreakDown.textBoxTotalCost.Text;
			BussinesLogic(buttonBreakDownHousePreparationCosts, new EventArgs());
		}

		private void buttonBreakDownMiscellaneousCosts_Click(object sender, System.EventArgs e)
		{
			FormBreakDown fbd = new FormBreakDown();
			fbd.userControl_BreakDown.DataArrayCostItems = all_cash.MiscellaneousCostsItems;
			fbd.ShowDialog();
			all_cash.MiscellaneousCostsItems = fbd.userControl_BreakDown.DataArrayCostItems;
			all_cash.MiscellaneousCosts = Decimal.Parse(fbd.userControl_BreakDown.textBoxTotalCost.Text, NumberStyles.Currency);
			textBoxMiscellaneousCosts.Text = fbd.userControl_BreakDown.textBoxTotalCost.Text;
			BussinesLogic(buttonBreakDownMiscellaneousCosts, new EventArgs());
		}

		private void buttonBreakDownOtherCosts_Click(object sender, System.EventArgs e)
		{
			FormBreakDown fbd = new FormBreakDown();
			fbd.userControl_BreakDown.DataArrayCostItems = all_cash.OtherCostsItems;
			fbd.ShowDialog();
			all_cash.OtherCostsItems = fbd.userControl_BreakDown.DataArrayCostItems;
			all_cash.OtherCosts = Decimal.Parse(fbd.userControl_BreakDown.textBoxTotalCost.Text, NumberStyles.Currency);
			textBoxOtherCosts.Text = fbd.userControl_BreakDown.textBoxTotalCost.Text;
			BussinesLogic(buttonBreakDownOtherCosts, new EventArgs());
		}

		private void buttonBreakDownHoldingCostsOther_Click(object sender, System.EventArgs e)
		{
			FormBreakDown fbd = new FormBreakDown();
			fbd.userControl_BreakDown.DataArrayCostItems = all_cash.HoldingCostsOtherItems;
			fbd.ShowDialog();
			all_cash.HoldingCostsOtherItems = fbd.userControl_BreakDown.DataArrayCostItems;
			all_cash.HoldingCostsOther = Decimal.Parse(fbd.userControl_BreakDown.textBoxTotalCost.Text, NumberStyles.Currency);
			textBoxHoldingCostsOther.Text = fbd.userControl_BreakDown.textBoxTotalCost.Text;
			BussinesLogic(buttonBreakDownHoldingCostsOther, new EventArgs());
		}

		public
			void 
			BussinesLogic
			(
			  object sender
			, System.EventArgs e
			)
		{
			Control ctrl = sender as Control;

			if ( 
				ctrl.Name == radioButtonAuction.Name
				||
				ctrl.Name == radioButtonRetail.Name
				||
				ctrl.Name == this.Name
				)	
			{
				if (radioButtonRetail.Checked == true)
				{
					labelRetailAuctionPercentage.Text = "Set Retail Percentage:"; 
					textBoxRetailAuctionPercentage.Text = "100%";
				}
				else if 
					(
					radioButtonAuction.Checked == true
					)
				{
					labelRetailAuctionPercentage.Text = "Set Auction Percentage:"; 
					textBoxRetailAuctionPercentage.Text = "80%";
				}
			}




			TextBox tb = sender as TextBox;

			if (null != tb)
			{
				tb.Text = tb.Text;
			}

			decimal PRP =
				WorthByCOMPS
				*
				Decimal.Parse
				(
				textBoxRetailAuctionPercentage.Text.Replace("%","")
				, NumberStyles.Currency
				)
				/
				100.00M
				;
			textBoxProjectedResalePrice.Text = PRP.ToString("c");

			decimal transactionaL_costs =
				Decimal.Parse(textBoxAdvertisingCosts.Text, NumberStyles.Currency)
				+
				Decimal.Parse(textBoxSellingFees.Text, NumberStyles.Currency)
				+
				Decimal.Parse(textBoxClosingCostsAtPurchase.Text, NumberStyles.Currency)
				+
				Decimal.Parse(textBoxClosingCostsAtResale.Text, NumberStyles.Currency)
				+
				Decimal.Parse(textBoxHousePreparationCosts.Text, NumberStyles.Currency)
				+
				Decimal.Parse(textBoxMiscellaneousCosts.Text, NumberStyles.Currency)
				;
			textBoxTotalTransactionalCosts.Text = transactionaL_costs.ToString("c");


			decimal projected_resale_price_4 =
						 WorthByCOMPS
						 *
						 Decimal.Parse
						 (
						   textBoxRetailAuctionPercentage.Text.Replace("%","")
						 , NumberStyles.Currency
						 )
						 /
						 100.00M
						 ;
			textBoxProjectedResalePrice.Text = projected_resale_price_4.ToString("c");

			decimal desired_profit_in_bucks_5 =
				projected_resale_price_4
				*
				Decimal.Parse
				(
				  textBoxDesiredProfitInPercentage.Text.Replace("%","")
				, NumberStyles.Currency
				)
				/
				100.00M
				;
			textBoxProjectedResalePrice.Text = projected_resale_price_4.ToString("c");

/*
			decimal financing_mo_pymnt =
				100
				*
				Decimal.Parse
				(
				  textBoxLoanAmount.Text
				, NumberStyles.Currency
				)
				/
				WorthByCOMPS
				;
			textBoxMonthlyPayment.Text = financing_mo_pymnt.ToString("c");
*/
			decimal financing_costs =
				Decimal.Parse
				(
				  textBoxOtherCosts.Text
				, NumberStyles.Currency
				)
				+
				Decimal.Parse
				(
				  textBoxOtherCosts.Text
				, NumberStyles.Currency
				)
				;


			return;




			decimal E =
				Decimal.Parse(textBoxDesiredProfitInPercentage.Text.Replace("%","")
								, NumberStyles.Currency)
				;
			if (10 > E  && business_logic_counter > 0)
			{
				string str = 
					"Warning! Your goal should be to make at least 10%! "
					+
					"Anything lower than that may turn this deal into "
					+
					"a No-Profit deal if anything goes wrong!"
					;
				MessageBox.Show(str, "Warning", MessageBoxButtons.OK);
			}

			decimal mo =
				Decimal.Parse(textBoxProjectedNumberOfMonths.Text, NumberStyles.Currency)
				;
			decimal f1 = 
				Decimal.Parse(textBoxLoanAmount.Text, NumberStyles.Currency)
				;
			decimal f2 = 
				Decimal.Parse(textBoxLoanPoints.Text.Replace("%",""), NumberStyles.Currency)
				;
			decimal f3 =
				Decimal.Parse(textBoxInterestRate.Text.Replace("%",""), NumberStyles.Currency)
				;
			decimal f4 = 
				Decimal.Parse(textBoxMonthlyPayment.Text, NumberStyles.Currency)
				;
			decimal f5 = 
				Decimal.Parse(textBoxOtherCosts.Text, NumberStyles.Currency)
				;
			decimal F = f2 + (f3 * f1 /100) * mo + f5;

			textBoxTotalFinancingCosts.Text = F.ToString("c");

			decimal H1A = 
				Decimal.Parse(textBoxHoldingCostsMonthlyPayments.Text, NumberStyles.Currency)
				;
			decimal H1B = 
				Decimal.Parse(textBoxHoldingCostsOther.Text, NumberStyles.Currency)
				;
			decimal B = H1A + H1B;
			textBoxTotalHoldingCost.Text = B.ToString("c");

			decimal d1 =
				Decimal.Parse(textBoxOtherCashInDeal.Text, NumberStyles.Currency)
				;
			decimal d2 =
				Decimal.Parse(textBoxApproximateLoanPayoff.Text, NumberStyles.Currency)
				;
			d2 = f1 - (f1 * f2 / 100);

			decimal d3 =
				Decimal.Parse(textBoxTaxesDue.Text, NumberStyles.Currency)
				;
			decimal d4 =
				Decimal.Parse(textBoxOtherEncumbrances.Text, NumberStyles.Currency)
				;
			decimal C = d1 + d2 + d3 + d4; 
			textBoxTotalDealCost.Text = C.ToString("c");

			decimal MFA =
				Decimal.Parse(textBoxTransactionalMurphyFactor.Text.Replace("%","")
								, NumberStyles.Currency)
				;

			decimal MFB =
				Decimal.Parse(textBoxHoldingMurphyFactor.Text.Replace("%","")
								, NumberStyles.Currency)
				;

			decimal D = transactionaL_costs * MFA / 100 + F + B * MFB / 100 + C + H1A;
			textBoxTotalAcquistionCostOfDeal.Text = D.ToString("c");

			decimal F2 = D;
			textBoxMaximumPurchasePrice.Text = F2.ToString("c");

			if (0 > F && business_logic_counter > 0)
			{
				string str = 
					"Warning! Your Maximum Purchase Price is negative."
					+
					"This means that the Cost of Acquiring this deal is "
					+ 
					"HIGHER than the Projected Resale Price?! Go back "
					+
					"and re-evaluate the components of this deal. You may"
					+
					" need to reduce Cash To Seller amount. At the end, it "
					+
					"may not be a deal to start with, or it may require a "
					+
					"different resale Exit Strategy! (If you�re planning on "
					+
					"discounting or short selling the loans, then enter in "
					+
					"the projected loan balances after the discounting, to "
					+
					"get a better idea of the deal!)"
					;
				MessageBox.Show(str, "Warning", MessageBoxButtons.OK);
			}

			business_logic_counter += 1;
			return;
		}
		
		public
			AllCash
			AllCash
		{
			get
			{
				return all_cash;
			}
			set
			{
				all_cash = value;

				textBoxAdvertisingCosts.Text = value.AdvertisingCosts.ToString("c");
				textBoxApproximateLoanPayoff.Text = value.ApproximateLoanPayoff.ToString("c");
				textBoxRetailAuctionPercentage.Text = value.RetailAuctionPercentage.ToString();
				textBoxClosingCostsAtPurchase.Text = value.ClosingCostsPurcahse.ToString("c");
				textBoxClosingCostsAtResale.Text = value.ClsoingCostsAtResale.ToString("c");
				textBoxDesiredProfitInBucks.Text = value.DesiredProfit.ToString("c");
				textBoxDesiredProfitInPercentage.Text = value.DesiredProfitPrecentual.ToString();
				textBoxHoldingCostsMonthlyPayments.Text = value.HoldingCostsMonthlyPayments.ToString("c");
				textBoxHoldingCostsOther.Text = value.HoldingCostsOther.ToString("c");
				textBoxHoldingMurphyFactor.Text = value.HoldingCostsMurphyFactor.ToString();
				textBoxHousePreparationCosts.Text = value.PreparationCosts.ToString("c");
				textBoxInterestRate.Text = value.InterestRate.ToString();
				textBoxLoanAmount.Text = value.LoanAmount.ToString("c");
				textBoxLoanPoints.Text = value.LoanPoints.ToString();
				textBoxLTV.Text = value.LTV.ToString();
				textBoxMaximumPurchasePrice.Text = value.MaximumPurchasePrice.ToString("c");
				textBoxMiscellaneousCosts.Text = value.MiscellaneousCosts.ToString("c");
				textBoxMonthlyPayment.Text = value.MonthlyPayment.ToString("c");
				textBoxOtherCashInDeal.Text = value.OtherCashInTheDeal.ToString("c");
				textBoxOtherCosts.Text = value.OtherCosts.ToString("c");
				textBoxOtherEncumbrances.Text = value.OtherEncumbrances.ToString("c");
				textBoxProjectedNumberOfMonths.Text = value.ProjectedNumberOfMonths.ToString();
				textBoxProjectedResalePrice.Text = value.ProjectedResalePrice.ToString("c");
				textBoxSellingFees.Text = value.SellingFees.ToString("c");
				textBoxTaxesDue.Text = value.TaxesDue.ToString("c");
				textBoxTotalAcquistionCostOfDeal.Text = value.TotalAcquisitionDealCost.ToString("c");
				textBoxTotalDealCost.Text = value.DealCostsTotal.ToString("c");
				textBoxTotalFinancingCosts.Text = value.TotalFinancingCosts.ToString("c");
				textBoxTotalHoldingCost.Text = value.HoldingCostsTotal.ToString("c");
				textBoxTotalTransactionalCosts.Text = value.TransactionalCosts.ToString("c");
				textBoxTransactionalMurphyFactor.Text = value.TransactoionalCostsMurphyFactor.ToString("c");
				this.BussinesLogic(this, new EventArgs());
			}
		}

		public
			decimal
			WorthByCOMPS
		{
			set
			{
				dec_worth_by_comps = value;
			}
			get
			{
				return dec_worth_by_comps;
			}
		}

	}
}
