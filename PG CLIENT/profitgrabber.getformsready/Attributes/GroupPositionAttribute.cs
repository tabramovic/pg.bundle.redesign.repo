#region � Using �
using System;
#endregion � Using �
#region � Namespace GetFormsReady �
namespace GetFormsReady
{
	#region � Class GroupPositionAttribute �
	/// <summary>
	/// Class that contains comments for RT
	/// </summary>
	[AttributeUsage(AttributeTargets.All)]	
	public class GroupPositionAttribute : Attribute
	{
		#region � Data �
		private string desc;
		#endregion � Data �		
		#region � Properties �
		#region � Desc �
		public string Desc 
		{ 
			get { return this.desc; }
		}
		#endregion � Desc �
		#endregion � Properties �
		#region � CTR �
		public GroupPositionAttribute(string desc) 
		{
			this.desc = desc;
		}
		#endregion � CTR �
	}
	#endregion � Class GroupPositionAttribute �
}
#endregion � Namespace GetFormsReady �