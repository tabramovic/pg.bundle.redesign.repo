#region � Using �
using System;
#endregion � Using �
#region � Namespace GetFormsReady �
namespace GetFormsReady
{
	#region � Class GroupAttribute �
	/// <summary>
	/// Class that contains comments for RT
	/// </summary>
	[AttributeUsage(AttributeTargets.All)]	
	public class GroupAttribute : Attribute
	{
		#region � Data �
		private string desc;
		#endregion � Data �		
		#region � Properties �
		#region � Desc �
		public string Desc 
		{ 
			get { return this.desc; }
		}
		#endregion � Desc �
		#endregion � Properties �
		#region � CTR �
		public GroupAttribute(string desc) 
		{
			this.desc = desc;
		}
		#endregion � CTR �
	}
	#endregion � Class GroupAttribute �
}
#endregion � Namespace GetFormsReady �