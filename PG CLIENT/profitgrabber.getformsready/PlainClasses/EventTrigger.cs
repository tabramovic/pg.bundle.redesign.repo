using System;

namespace GetFormsReady
{
	public delegate void PrintDocEventHandler(object sender, EventArgs e);

	/// <summary>
	/// Summary description for EventTrigger.
	/// </summary>
	public class EventTrigger
	{
		// An event that clients can use to be notified whenever the
		// elements of the list change.
		public event PrintDocEventHandler ExecutedPrinting;

		// Invoke the Changed event; called whenever list changes
		protected virtual void OnPrintDoc(EventArgs e) 
		{
			if (ExecutedPrinting != null)
				ExecutedPrinting(this, e);
		}

		public void TriggerChange ()
		{
			OnPrintDoc(EventArgs.Empty);
		}

		public EventTrigger()
		{
		}				
	}
}
