using System;
using System.Reflection;
using System.Collections;
using System.Windows.Forms;

namespace GetFormsReady
{
	/// <summary>
	/// Summary description for ApplicationHelper.
	/// </summary>
	public class ApplicationHelper
	{
		public ApplicationHelper()
		{
			
		}

		public static string GetAssemblyPath()
		{
			/*			
			Assembly ass = Assembly.GetExecutingAssembly();
			string assLocation = ass.Location;
			string assName =  ass.GetName().Name + ".dll";
			assName = assName.ToLower();
			
			assLocation = assLocation.Replace(assName, string.Empty);			
			return assLocation;			*/		

			return Application.StartupPath;
		}
		
		public static short GetNumberOfCopiesToPrint(string template, eDocsType docType)
		{			
			if (docType != Globals.docType)
			{
				//trying to print wrong type of docs!
				return 0;
			}
			else
			{
				//string fileTemplate = template.Substring(template.LastIndexOf("\\") + 1);

                var chunks = template.Split('\\');
                string fileTemplate = chunks[chunks.Length - 2] + '\\' + chunks[chunks.Length - 1];

                switch (fileTemplate)
				{
					case Globals.F_PSA_Long:
						return Globals.PT_PS_PSALong;						
					case Globals.F_PSA_Short:
						return Globals.PT_PS_PSAShort;						
					case Globals.F_HOA:
						return Globals.PT_PS_HOA;
					case Globals.F_Addendum:
						return Globals.PT_PS_Addendum;
					case Globals.F_Disclosure:
						return Globals.PT_PS_Disclosure;
					case Globals.F_Fax:
						return Globals.PT_PS_Fax;
					case Globals.F_UDoc1:
						return Globals.PT_PS_UDoc1;
					case Globals.F_UDoc2:
						return Globals.PT_PS_UDoc2;
                    case Globals.F_Deed:
                        return Globals.PT_Deed;
					case Globals.F_WarrantyDeed:
						return Globals.PT_WarrantyDeed;
					case Globals.F_QuitClaimDeed:
						return Globals.PT_QuitClaimDeed;
					case Globals.F_LimitedPowerOfAttorney:
						return Globals.PT_LimitedPowerOfAttorney;
					case Globals.F_TrustAgreement:
						return Globals.PT_TrustAgreement;
					case Globals.F_AssignmentOfBeneficialInterestInTrust:
						return Globals.PT_AssignmentOfBeneficialInterestInTrust;
					case Globals.F_AuthorizationToReleaseFor1stLoan:
						return Globals.PT_AuthorizationToReleaseFor1stLoan;
					case Globals.F_AuthorizationToReleaseFor2ndLoan:
						return Globals.PT_AuthorizationToReleaseFor2ndLoan;
					case Globals.F_AuthorizationToReleaseFor3rdLoan:
						return Globals.PT_AuthorizationToReleaseFor3rdLoan;
					case Globals.F_AffidavitOfCompletedSale:
						return Globals.PT_AffidavitOfCompletedSale;
					case Globals.F_AffidavitOfNoLiens:
						return Globals.PT_AffidavitOfNoLiens;
					case Globals.F_SellerLoanDisclosureNoPromises:
						return Globals.PT_SellerLoanDisclosureNoPromises;
					case Globals.F_SellerLoanDisclosure:
						return Globals.PT_SellerLoanDisclosure;
					case Globals.F_LetterOfAgreementAndUnderstanding:
						return Globals.PT_LetterOfAgreementAndUnderstanding;
					case Globals.F_EscrowAccountLetter:
						return Globals.PT_EscrowAccountLetter;
					case Globals.F_ChangeOfAddressLetterToMortgageCompany:
						return Globals.PT_ChangeOfAddressLetterToMortgageCompany;
					case Globals.F_ChangeOfAddressLetterToHOACompany:
						return Globals.PT_ChangeOfAddressLetterToHOACompany;
					case Globals.F_CancelInsuranceLetter:
						return Globals.PT_FaxCoverPageToCancelInsurance;
					case Globals.F_FaxCoverPageBlank:
						return Globals.PT_FaxCoverPageBlank;
					case Globals.F_BillOfSale:
						return Globals.PT_BillOfSale;
					case Globals.F_PromissoryNote:
						return Globals.PT_PromissoryNote;
					case Globals.F_SellerInformationSheet:
						return Globals.PT_SellerInformationSheet;
					case Globals.F_UserSub2Doc1:
						return Globals.PT_UserSub2Doc1;
					case Globals.F_UserSub2Doc2:
						return Globals.PT_UserSub2Doc2;
					case Globals.F_OptionToPurchase:
						return Globals.PT_OptionToPurchase;
					case Globals.F_OptionToPurchaseAgreement:
						return Globals.PT_OptionToPurchaseAgreement;
					case Globals.F_AffidavitAndMemorandumOfOption:
						return Globals.PT_AffidavitAndMemorandumOfOption;
					case Globals.F_EquitySharingAddendum:
						return Globals.PT_EquitySharingAddendum;
					case Globals.F_RightToCancelAddendum:
						return Globals.PT_RightToCancelAddendum;
					case Globals.F_DeedFromSellerToBeDepositedInEscrow:
						return Globals.PT_DeedFromSellerToBeDepositedInEscrow;
					case Globals.F_QuitClaimDeedForYou:
						return Globals.PT_QuitClaimDeedForYou;
					case Globals.F_FaxCoverPageToAmendInsurance:
						return Globals.PT_FaxCoverPageToAmendInsurance;
					case Globals.F_OptionFaxCoverPageBlank:
						return Globals.PT_OptionFaxCoverPageBlank;
					case Globals.F_UserOptionDoc1:
						return Globals.PT_UserOptionDoc1;																			
					case Globals.F_UserOptionDoc2:
						return Globals.PT_UserOptionDoc2;																																																																																														
				}
			}
			return 1;
		}

		public static bool FindInStringArray(string[] array, string expression)
		{
			for (int i = 0; i < array.Length; i++)
			{				
				if ((string)array.GetValue(i) == expression)
					return true;
			}
			return false;
		}

		
		public static void ClearTabControl (TabControl tc)
		{
			TabPage selectedTabPage = (TabPage)tc.TabPages[tc.SelectedIndex];
			ApplicationHelper.ClearTabPage(selectedTabPage);
		}

		public static void ClearTabPage (TabPage tp)
		{
			IEnumerator ie = tp.Controls.GetEnumerator();
			while (ie.MoveNext())
			{
				if (ie.Current is TextBox)
				{
					TextBox tb = (TextBox)ie.Current;
					tb.Text = string.Empty;
				}
				if (ie.Current is NumericUpDown)
				{
					NumericUpDown nud = (NumericUpDown)ie.Current;
					nud.Value = 0;
				}
				if (ie.Current is CheckBox)
				{
					CheckBox cb = (CheckBox)ie.Current;
					cb.Checked = false;
				}
				if (ie.Current is DateTimePicker)
				{
					DateTimePicker dtp = (DateTimePicker)ie.Current;
					dtp.Value = DateTime.Now;
				}
				if (ie.Current is TabControl)
				{
					TabControl tc = (TabControl)ie.Current;
					ApplicationHelper.ClearTabControl(tc);
				}
			}

			//Type t = tp.GetType();
			//PropertyInfo[] pi =  t.GetProperties();

			return;
		}

	}
}
