using System;
using System.Collections;

namespace GetFormsReady
{
	/// <summary>
	/// Summary description for MenuItemObjectDescriptor.
	/// </summary>
	public class MenuItemObjectDescriptor
	{
		
		private string group = string.Empty;
		private string description = string.Empty;
		private int groupPosition = 1;		
		private int itemPosition = 1;

		public string Group
		{
			get {return this.group;}
			set {this.group = value;}
		}

		public string Description
		{
			get {return this.description;}
			set {this.description = value;}
		}

		public int GroupPosition
		{
			get {return this.groupPosition;}
			set {this.groupPosition = value;}
		}

		public int ItemPosition
		{
			get {return this.itemPosition;}
			set {this.itemPosition = value;}
		}

		public MenuItemObjectDescriptor(string group, string description, string groupPosition, string itemPosition)
		{
			this.group = group;
			this.description = description;
			try
			{
				this.groupPosition = Convert.ToInt16(GroupPosition);
			}
			catch (Exception)
			{
				this.groupPosition = 1;
			}
			try
			{
				this.itemPosition= Convert.ToInt16(itemPosition);
			}
			catch (Exception)
			{
				this.itemPosition = 1;
			}
		}
	}

	public class MenuItemObjectComparer : IComparer
	{
		#region IComparer Members

		public int Compare(object x, object y)
		{
			MenuItemObjectDescriptor leftMiod = (MenuItemObjectDescriptor)x;
			MenuItemObjectDescriptor rightMiod = (MenuItemObjectDescriptor)y;

			if (leftMiod.ItemPosition == rightMiod.ItemPosition)
				return 0;
			if (leftMiod.ItemPosition > rightMiod.ItemPosition)
				return 1;
			return -1;			
		}

		#endregion
	}


}
