using System;
using System.Collections;

namespace GetFormsReady
{
	/// <summary>
	/// Summary description for Globals.
	/// </summary>
	public class Globals
	{
		public Globals()
		{
			
		}	
		
		public static string[] _PS_SelectedTemplates							= null;
		public static string[] _Option_SelectedTemplates						= null;
		public static string[] _Sub_SelectedTemplates							= null;

		public const string UpdatedDataFound = "Updated Data Found";
		public const string UpdateDataFoundDesc = "GetFormsReady found that you have updated Contact Info in ProfitGrabber Pro!\nWould you like to import updated data into GetFormsReady?";

		public const string Message1 = "Warning! Discrepancy detected!";
		public const string Message2 = "Warning!";

		public const string Message1Desc = "Some of the information in Contact View is different than corresponding info contained in the GetFormsReady.\n\nFor your convenience, the GetFormsReady fields will be updated with the current Contact View info.";
		public const string Message2Desc = "Some of the Contact View fields contain no data, while their corresponding GetFormsReady fields do.\n\nSuggestion: update your empty Contact View fields as well!";

		//  P&S Agreement Documents
		//  Sub2 Documents
		//  Option Documents

		public const string PS = @"P && S Agreement Documents\";
		public const string Sub2 = @"Sub2 Documents\";
		public const string Option = @"Option Documents\";
		public const string UserDocuments = @"User Documents";

		
		public const string EmptyPlaceholder = "_______________";

		public static int ps_TabIndex = 0;

		public static EventTrigger et = null;
		public static MainForm mf = null;
		
		public static eDocsType    docType = eDocsType.eP_S;

		public const string UserInfo		= "User Info";
		
		public const string SellerInfo		= "P && S: Seller Info";
		public const string PropertyInfo	= "P && S: Property Info";
		public const string BuyerInfo		= "P && S: Buyer Info";
		public const string FaxToInfo		= "P && S: Fax To Info";

		public static string[]  a_eP_S			= new string[9] {Globals.UserInfo, Globals.SellerInfo, Globals.BuyerInfo, Globals.PropertyInfo, Globals.TrustInfo, Globals.Financials, Globals.Closing, Globals.OtherTerms, Globals.FaxToInfo};
		public static string[]  a_eSub2			= new string[9] {Globals.UserInfo, Globals.SUB_SellerInfo, Globals.SUB_BuyerInfo, Globals.SUB_PropertyInfo, Globals.SUB_TrustInfo, Globals.SUB_Financials, SUB_InsuranceHOA, Globals.SUB_OtherTerms, Globals.SUB_FaxToInfo};
		public static string[]  a_eOption		= new string[7] {Globals.UserInfo, Globals.Option_SellerInfo, Globals.Option_BuyerInfo, Globals.Option_PropertyInfo, Globals.Option_AcquisitionInfo, Globals.Option_TrustInfo, Globals.Option_OtherTerms,};

		public const string SUB_SellerInfo		= "Sub2: Seller Info";
		public const string SUB_PropertyInfo	= "Sub2: Property Info";
		public const string SUB_BuyerInfo		= "Sub2: Buyer Info";
		public const string SUB_FaxToInfo		= "Sub2: Fax To Info";

		public const string Option_SellerInfo	= "Option: Seller Info";
		public const string Option_PropertyInfo	= "Option: Property Info";
		public const string Option_BuyerInfo	= "Option: Buyer Info";

		public const string TrustInfo		= "P && S: Trust Info";
		public const string SUB_TrustInfo	= "Sub2: Trust Info";
		public const string Option_TrustInfo= "Option: Trust Info";

		public const string Financials		= "P && S: Financials";
		public const string SUB_Financials	= "Sub2: Financials";

		public const string Closing			= "P && S: Closing";
		public const string SUB_InsuranceHOA= "Sub2: Insurance & HOA";

		public const string OtherTerms		= "P && S: Other Terms";
		public const string SUB_OtherTerms	= "Sub2: Other Terms";
		public const string Option_OtherTerms	= "Option: Other Terms";

		public const string Option_AcquisitionInfo= "Option Info: Acquisition Info";

		public static bool CB_PS_PSALong	= false;
		public static bool CB_PS_PSAShort	= false;
		public static bool CB_PS_Trust		= false;
		public static bool CB_PS_HOA		= false;
		public static bool CB_PS_Addendum	= false;
		public static bool CB_PS_Disclosure	= false;
		public static bool CB_PS_AuthToRel1 = false;
		public static bool CB_PS_AuthToRel2 = false;
		public static bool CB_PS_Fax		= false;
		public static bool CB_PS_UDoc1		= false;
		public static bool CB_PS_UDoc2		= false;

		public const string PSA_Long		= "P & S Agreement Long";
		public const string PSA_Short		= "P & S Agreement Short";
		public const string Trust			= "Trust Agreement";
		public const string HOA				= "Holdover Occupacy Agreement";
		public const string Addendum		= "Addendum to P & S Agreement";
		public const string Disclosure		= "Seller�s Property Disclosure";
		public const string AuthToRel1		= "Authorization To Release (1st loan)";
		public const string AuthToRel2		= "Authorization To Release (2nd loan)";
		public const string Fax				= "Fax Cover Page";
		public const string UDoc1			= "User Doc 1";
		public const string UDoc2			= "User Doc 2";

		public const string F_PSA_Long		= Globals.PS + "PS Agreement Long.tx";
		public const string F_PSA_Short		= Globals.PS + "PS Agreement Short.tx";
		public const string F_Trust			= Globals.PS + "Trust Agreement.tx";
		public const string F_HOA			= Globals.PS + "HOA.tx";
		public const string F_Addendum		= Globals.PS + "Addendum.tx";
		public const string F_Disclosure	= Globals.PS + "Property Disclosure.tx";
		public const string F_AuthToRel1	= Globals.PS + "Authorization To Release 1st Loan.tx";
		public const string F_AuthToRel2	= Globals.PS + "Authorization To Release 2nd Loan.tx";
		public const string F_Fax			= Globals.PS + "Fax Cover Page.tx";
		public const string F_UDoc1			= Globals.PS + "User Doc 1.tx";
		public const string F_UDoc2			= Globals.PS + "User Doc 2.tx";



		public static bool  CB_Deed										=  false;
		public static bool  CB_WarrantyDeed								=  false;
		public static bool  CB_QuitClaimDeed							=  false;
		public static bool	CB_LimitedPowerOfAttorney					=  false;
		public static bool	CB_TrustAgreement							=  false;
		public static bool	CB_AssignmentOfBeneficialInterestInTrust	=  false;
		public static bool	CB_AuthorizationToReleaseFor1stLoan			=  false;
		public static bool	CB_AuthorizationToReleaseFor2ndLoan			=  false;
		public static bool	CB_AuthorizationToReleaseFor3rdLoan			=  false;
		public static bool	CB_AffidavitOfCompletedSale					=  false;
		public static bool	CB_AffidavitOfNoLiens						=  false;
		public static bool	CB_SellerLoanDisclosureNoPromises			=  false;
		public static bool	CB_SellerLoanDisclosure						=  false;
		public static bool	CB_LetterOfAgreementAndUnderstanding		=  false;
		public static bool	CB_EscrowAccountLetter						=  false;
		public static bool	CB_ChangeOfAddressLetterToMortgageCompany	=  false;
		public static bool	CB_ChangeOfAddressLetterToHOACompany		=  false;
		public static bool	CB_CancelInsuranceLetter					=  false;
		public static bool	CB_FaxCoverPageToCancelInsurance			=  false;
		public static bool	CB_FaxCoverPageBlank						=  false;
		public static bool	CB_BillOfSale								=  false;
		public static bool	CB_PromissoryNote							=  false;
		public static bool	CB_PromissoryNoteB							=  false;
		public static bool	CB_SellerInformationSheet					=  false;
		public static bool	CB_UserSub2Doc1								=  false;
		public static bool	CB_UserSub2Doc2								=  false;
		
				
		public const string Deed												=  "Deed (Common For Your State)";
		public const string WarrantyDeed										=  "Warranty Deed";
		public const string QuitClaimDeed										=  "Quit Claim Deed";
		public const string LimitedPowerOfAttorney								=  "Limited Power Of Attorney";
		public const string TrustAgreement										=  "Trust Agreement";
		public const string AssignmentOfBeneficialInterestInTrust				=  "Assignment of Beneficial Interest in Trust";
		public const string AuthorizationToReleaseFor1stLoan					=  "Authorization To Release For 1st Loan";
		public const string AuthorizationToReleaseFor2ndLoan					=  "Authorization To Release For 2nd Loan";
		public const string AuthorizationToReleaseFor3rdLoan					=  "Authorization To Release For 3rd Loan";
		public const string AffidavitOfCompletedSale							=  "Affidavit of Completed Sale";
		public const string AffidavitOfNoLiens									=  "Affidavit of No Liens";
		public const string SellerLoanDisclosureNoPromises						=  "Seller Loan Disclosure No Promises";
		public const string SellerLoanDisclosure								=  "Seller Loan Disclosure";
		public const string LetterOfAgreementAndUnderstanding					=  "Letter Of Agreement and Understanding";
		public const string EscrowAccountLetter									=  "Escrow Account Letter";
		public const string ChangeOfAddressLetterToMortgageCompany				=  "Change Of Address Letter to Mortgage Company";
		public const string ChangeOfAddressLetterToHOACompany					=  "Change Of Address Letter to HOA Company";
		public const string CancelInsuranceLetter								=  "Cancel Insurance Letter";
		public const string FaxCoverPageToCancelInsurance						=  "Fax Cover Page to Cancel Insurance";
		public const string FaxCoverPageBlank									=  "Fax Cover Page Blank";
		public const string BillOfSale											=  "Bill Of Sale";
		public const string PromissoryNote										=  "Promissory Note A (no interest, no payments)";
		public const string PromissoryNoteB										=  "Promissory Note B (amortized w/ interest)";
		public const string SellerInformationSheet								=  "Seller Information Sheet";
		public const string UserSub2Doc1										=  "User Sub2 Doc1";
		public const string UserSub2Doc2										=  "User Sub2 Doc2";

		public const string F_Deed										=  Globals.Sub2 + "Deed.tx";
		public const string F_WarrantyDeed								=  Globals.Sub2 + "Warranty Deed To Trustee.tx";
		public const string F_QuitClaimDeed								=  Globals.Sub2 + "Quit Claim Deed.tx";
		public const string F_LimitedPowerOfAttorney					=  Globals.Sub2 + "Limited Power Of Attorney.tx";
		public const string F_TrustAgreement							=  Globals.Sub2 + "Trust Agreement.tx";
		public const string F_AssignmentOfBeneficialInterestInTrust		=  Globals.Sub2 + "Assignment of Beneficial Interest in Trust.tx";
		public const string F_AuthorizationToReleaseFor1stLoan			=  Globals.Sub2 +  "Authorization To Release For 1st Loan.tx";
		public const string F_AuthorizationToReleaseFor2ndLoan			=  Globals.Sub2 + "Authorization To Release For 2nd Loan.tx";
		public const string F_AuthorizationToReleaseFor3rdLoan			=  Globals.Sub2 + "Authorization To Release For 3rd Loan.tx";
		public const string F_AffidavitOfCompletedSale					=  Globals.Sub2 + "Affidavit of Completed Sale.tx";
		public const string F_AffidavitOfNoLiens						=  Globals.Sub2 + "Affidavit of No Liens.tx";
		public const string F_SellerLoanDisclosureNoPromises			=  Globals.Sub2 + "Seller Loan Disclosure No Promises.tx";
		public const string F_SellerLoanDisclosure						=  Globals.Sub2 + "Seller Loan Disclosure.tx";
		public const string F_LetterOfAgreementAndUnderstanding			=  Globals.Sub2 + "Seller's Statement of Understanding.tx";
		public const string F_EscrowAccountLetter						=  Globals.Sub2 + "Escrow Account Letter.tx";
		public const string F_ChangeOfAddressLetterToMortgageCompany	=  Globals.Sub2 + "Change of Address Letter.tx";
		public const string F_ChangeOfAddressLetterToHOACompany			=  Globals.Sub2 + "HOA Change of Address Letter.tx";
		public const string F_CancelInsuranceLetter						=  Globals.Sub2 + "Cancel Insurance Letter.tx";
		public const string F_FaxCoverPageToCancelInsurance				=  Globals.Sub2 + "Fax To Cancel Insurance.tx";
		public const string F_FaxCoverPageBlank							=  Globals.Sub2 + "Fax Cover Page Blank.tx";
		public const string F_BillOfSale								=  Globals.Sub2 + "Bill Of Sale.tx";
		public const string F_PromissoryNote							=  Globals.Sub2 + "Promissory Note A.tx";
		public const string F_PromissoryNoteB							=  Globals.Sub2 + "Promissory Note B.tx";
		public const string F_SellerInformationSheet					=  Globals.Sub2 + "Seller Information Sheet.tx";
		public const string F_UserSub2Doc1								=  Globals.Sub2 + "User Sub2 Doc1.tx";
		public const string F_UserSub2Doc2								=  Globals.Sub2 + "User Sub2 Doc2.tx";

		public static bool CB_OptionToPurchase						= false;
		public static bool CB_OptionToPurchaseAgreement				= false;
		public static bool CB_AffidavitAndMemorandumOfOption		= false;
		public static bool CB_EquitySharingAddendum					= false;
		public static bool CB_RightToCancelAddendum					= false;
		public static bool CB_DeedFromSellerToBeDepositedInEscrow	= false;
		public static bool CB_QuitClaimDeedForYou					= false;
		public static bool CB_FaxCoverPageToAmendInsurance			= false;
		public static bool CB_AmendInsuranceLetter					= false;
		public static bool CB_OptionFaxCoverPageBlank				= false;
		public static bool CB_UserOptionDoc1						= false;
		public static bool CB_UserOptionDoc2						= false;

		public const string OptionToPurchase				=  "Option To Purchase";
		public const string OptionToPurchaseAgreement		=  "Option To Purchase Agreement";
		public const string AffidavitAndMemorandumOfOption	=  "Affidavit && Memorandum of Option";
		public const string EquitySharingAddendum			=  "\"Equity Sharing\" Addendum";
		public const string RightToCancelAddendum			=  "\"Right to Cancel\" Addendum";
		public const string DeedFromSellerToBeDepositedInEscrow	=  "Deed from Seller to be deposited in Escrow (prepared by Agent)";
		public const string QuitClaimDeedForYou				=  "Quit Claim Deed For You (prepared by agent)";		
		public const string FaxCoverPageToAmendInsurance	=  "Fax Cover Page to amend insurance";
		public const string AmendInsuranceLetter			= "Amend Insurance Letter.tx";
		public const string OptionFaxCoverPageBlank			=  "Fax Cover Page blank";
		public const string UserOptionDoc1					=  "User Option Doc 1";
		public const string UserOptionDoc2					=  "User Option Doc 2";

		public const string F_OptionToPurchase							=  Globals.Option + "Option To Purchase.tx";
		public const string F_OptionToPurchaseAgreement					=  Globals.Option + "Option Agreement.tx";
		public const string F_AffidavitAndMemorandumOfOption			=  Globals.Option + "Memorandum Of Option.tx";
		public const string F_EquitySharingAddendum						=  Globals.Option + "Addendum A.tx";
		public const string F_RightToCancelAddendum						=  Globals.Option + "Addendum B.tx";
		public const string F_DeedFromSellerToBeDepositedInEscrow		=  Globals.Option + "Deed.tx";
		public const string F_QuitClaimDeedForYou						=  Globals.Option + "Quit Claim.tx";		
		public const string F_FaxCoverPageToAmendInsurance				=  Globals.Option + "Fax To Amend Insurance.tx";
		public const string F_AmendInsuranceLetter						=  Globals.Option + "Amend Insurance Letter.tx";
		public const string F_OptionFaxCoverPageBlank					=  Globals.Option + "Fax Cover Page blank.tx";
		public const string F_UserOptionDoc1							=  Globals.Option + "User Option Doc 1.tx";
		public const string F_UserOptionDoc2							=  Globals.Option + "User Option Doc 2.tx";
		//Amend Insurance Letter 


		//Printing && Faxing
		public static int FX_PS_PSALong	= 0;
		public static int FX_PS_PSAShort = 0;
		public static int FX_PS_HOA = 0;	
		public static int FX_PS_Addendum 	= 0;
		public static int FX_PS_Disclosure = 0;
		public static int FX_PS_Fax = 0;		
		public static int FX_PS_UDoc1 = 0;		
		public static int FX_PS_UDoc2 = 0;		
		public static int FX_WarrantyDeed = 0;								
		public static int FX_QuitClaimDeed = 0;							
		public static int FX_LimitedPowerOfAttorney = 0;					
		public static int FX_TrustAgreement = 0;							
		public static int FX_AssignmentOfBeneficialInterestInTrust = 0;	
		public static int FX_AuthorizationToReleaseFor1stLoan = 0;		
		public static int FX_AuthorizationToReleaseFor2ndLoan = 0;			
		public static int FX_AuthorizationToReleaseFor3rdLoan = 0;			
		public static int FX_AffidavitOfCompletedSale = 0;					
		public static int FX_AffidavitOfNoLiens	 = 0;					
		public static int FX_SellerLoanDisclosureNoPromises = 0;
		public static int FX_SellerLoanDisclosure = 0;						
		public static int FX_LetterOfAgreementAndUnderstanding = 0;		
		public static int FX_EscrowAccountLetter = 0;						
		public static int FX_ChangeOfAddressLetterToMortgageCompany = 0;	
		public static int FX_ChangeOfAddressLetterToHOACompany = 0;		
		public static int FX_CancelInsuranceLetter = 0;					
		public static int FX_FaxCoverPageToCancelInsurance = 0;			
		public static int FX_FaxCoverPageBlank = 0;						
		public static int FX_BillOfSale	 = 0;							
		public static int FX_PromissoryNote = 0;							
		public static int FX_SellerInformationSheet = 0;					
		public static int FX_UserSub2Doc1 = 0;								
		public static int FX_UserSub2Doc2 = 0;								
		public static int FX_OptionToPurchase = 0;	
		public static int FX_OptionToPurchaseAgreement = 0;			
		public static int FX_AffidavitAndMemorandumOfOption = 0;		
		public static int FX_EquitySharingAddendum = 0;					
		public static int FX_RightToCancelAddendum = 0;					
		public static int FX_DeedFromSellerToBeDepositedInEscrow = 0;
		public static int FX_QuitClaimDeedForYou = 0;
		public static int FX_FaxCoverPageToAmendInsurance = 0;			
		public static int FX_OptionFaxCoverPageBlank = 0;				
		public static int FX_UserOptionDoc1 = 0;						
		public static int FX_UserOptionDoc2 = 0;

		public static short PT_PS_PSALong	 = 0;
		public static short PT_PS_PSAShort	 = 0;	
		public static short PT_PS_HOA	 = 0;		
		public static short PT_PS_Addendum	 = 0;	
		public static short PT_PS_Disclosure	 = 0;	
		public static short PT_PS_Fax	 = 0;		
		public static short PT_PS_UDoc1	 = 0;		
		public static short PT_PS_UDoc2	 = 0;
        public static short PT_Deed = 0;
        public static short PT_WarrantyDeed	 = 0;								
		public static short PT_QuitClaimDeed	 = 0;							
		public static short PT_LimitedPowerOfAttorney	 = 0;					
		public static short PT_TrustAgreement	 = 0;							
		public static short PT_AssignmentOfBeneficialInterestInTrust	 = 0;	
		public static short PT_AuthorizationToReleaseFor1stLoan	 = 0;			
		public static short PT_AuthorizationToReleaseFor2ndLoan	 = 0;			
		public static short PT_AuthorizationToReleaseFor3rdLoan	 = 0;			
		public static short PT_AffidavitOfCompletedSale	 = 0;					
		public static short PT_AffidavitOfNoLiens	 = 0;						
		public static short PT_SellerLoanDisclosureNoPromises = 0;
		public static short PT_SellerLoanDisclosure	 = 0;				
		public static short PT_LetterOfAgreementAndUnderstanding	 = 0;		
		public static short PT_EscrowAccountLetter	 = 0;						
		public static short PT_ChangeOfAddressLetterToMortgageCompany	 = 0;	
		public static short PT_ChangeOfAddressLetterToHOACompany	 = 0;		
		public static short PT_CancelInsuranceLetter	 = 0;					
		public static short PT_FaxCoverPageToCancelInsurance	 = 0;			
		public static short PT_FaxCoverPageBlank	 = 0;						
		public static short PT_BillOfSale	 = 0;								
		public static short PT_PromissoryNote	 = 0;							
		public static short PT_SellerInformationSheet	 = 0;					
		public static short PT_UserSub2Doc1	 = 0;								
		public static short PT_UserSub2Doc2	 = 0;								
		public static short PT_OptionToPurchase	 = 0;
		public static short PT_OptionToPurchaseAgreement = 0;							
		public static short PT_AffidavitAndMemorandumOfOption	 = 0;		
		public static short PT_EquitySharingAddendum	 = 0;					
		public static short PT_RightToCancelAddendum	 = 0;					
		public static short PT_DeedFromSellerToBeDepositedInEscrow	 = 0;
		public static short PT_QuitClaimDeedForYou = 0;
		public static short PT_FaxCoverPageToAmendInsurance	 = 0;			
		public static short PT_OptionFaxCoverPageBlank	 = 0;				
		public static short PT_UserOptionDoc1	 = 0;						
		public static short PT_UserOptionDoc2	 = 0;						
		
		
		//Paths
		public const string MailMergeDocumentsPath = @"\Document Templates\Get Forms Ready Templates\";
		public const string SavedDataDocumentsPath = @"\Documents\Get Forms Ready\SavedData\";
	}
}
