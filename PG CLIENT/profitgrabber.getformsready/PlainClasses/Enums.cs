using System;

namespace GetFormsReady
{
	public enum eDocsType
	{
		eP_S = 0,

		eSub2,

		eOption
	}

	public enum eUpdateResult
	{
		eUnassigned = 0,
		
		eGFRDataIsEmpty,
		
		eOwerwriteWithPGData,
		
		ePGFielsIsEmpty_GFRContainsData,

		eBoth,
	}
}
