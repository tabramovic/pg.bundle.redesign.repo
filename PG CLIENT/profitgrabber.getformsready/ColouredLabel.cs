using System;

namespace GetFormsReady
{
	/// <summary>
	/// Summary description for ColouredLabel.
	/// </summary>
	public class ColouredLabel : System.Windows.Forms.Label
	{
		public ColouredLabel()
		{
			this.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
		}
	}
}
