using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;

namespace GetFormsReady
{
	/// <summary>
	/// Summary description for DocControl.
	/// </summary>
	public class DocControl : System.Windows.Forms.UserControl
	{
		private System.Windows.Forms.TabControl tcDocs;
		private System.Windows.Forms.ContextMenu ctxMenu2;
		private System.Windows.Forms.MenuItem miAddNewDoc;
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public DocControl()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

			// TODO: Add any initialization after the InitializeComponent call

		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.tcDocs = new System.Windows.Forms.TabControl();
			this.ctxMenu2 = new System.Windows.Forms.ContextMenu();
			this.miAddNewDoc = new System.Windows.Forms.MenuItem();
			this.SuspendLayout();
			// 
			// tcDocs
			// 
			this.tcDocs.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tcDocs.Location = new System.Drawing.Point(0, 0);
			this.tcDocs.Name = "tcDocs";
			this.tcDocs.SelectedIndex = 0;
			this.tcDocs.Size = new System.Drawing.Size(776, 474);
			this.tcDocs.TabIndex = 0;
			this.tcDocs.SelectedIndexChanged += new System.EventHandler(this.tcDocs_SelectedIndexChanged);
			// 
			// ctxMenu2
			// 
			this.ctxMenu2.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					 this.miAddNewDoc});
			// 
			// miAddNewDoc
			// 
			this.miAddNewDoc.Index = 0;
			this.miAddNewDoc.Text = "Add new Document";
			this.miAddNewDoc.Click += new System.EventHandler(this.miAddNewDoc_Click);
			// 
			// DocControl
			// 
			this.ContextMenu = this.ctxMenu2;
			this.Controls.Add(this.tcDocs);
			this.Name = "DocControl";
			this.Size = new System.Drawing.Size(776, 474);
			this.ResumeLayout(false);

		}
		#endregion

		private void RemoveTabPageIfDeselected(string tabPageName)
		{
			IEnumerator ie = this.tcDocs.TabPages.GetEnumerator();
			while (ie.MoveNext())
			{
				TabPage tp = (TabPage)ie.Current;
				if (tp.Name == tabPageName)
				{								
					TabControl tc = (TabControl)tp.Parent;
					tc.TabPages.Remove(tp);
					break;
				}
			}	
		}

		private void RemoveTabPageIfDeselected(eDocsType docType)
		{			
			string[] list = null;

			switch (docType)
			{
				case eDocsType.eP_S: 
					list = Globals._PS_SelectedTemplates;
					break;

				case eDocsType.eOption:
					list = Globals._Option_SelectedTemplates;
					break;

				case eDocsType.eSub2:
					list = Globals._Sub_SelectedTemplates;
					break;
			}

			if (null == list)
				return; 

			IEnumerator ie = this.tcDocs.TabPages.GetEnumerator();
			while (ie.MoveNext())
			{
				bool found = false;
				TabPage tp = (TabPage)ie.Current;
				string tabName = (string)tp.Tag;

				for (int i = 0; i < Globals._PS_SelectedTemplates.Length; i++)
				{
					if (Globals._PS_SelectedTemplates[i] == tabName)
					{
						found = true;
						break;
					}
				}
				
				if (!found)
				{								
					TabControl tc = (TabControl)tp.Parent;
					tc.TabPages.Remove(tp);					
				}
			}	
		}
		
		public void RefreshPages ()
		{
			//Event trigger
			EventMerge em = new EventMerge();			
			int iTabIndex = Globals.ps_TabIndex;
			switch (Globals.docType)
			{
				case eDocsType.eP_S:
					#region � eDocsType.eP_S �
				
					//--------------------------------------------
					//USER DOCUMENTS
					RemoveTabPageIfDeselected(eDocsType.eP_S);
					if (null != Globals._PS_SelectedTemplates)
					{
						foreach (string selectedTemplate in Globals._PS_SelectedTemplates)
						{
							bool bFound = false;
							IEnumerator ie = this.tcDocs.TabPages.GetEnumerator();
							while (ie.MoveNext())
							{
								TabPage tp = (TabPage)ie.Current;
								if ((string)tp.Tag == selectedTemplate)
								{
									bFound = true;
									break;
								}
							}
							if (false == bFound)
							{
								string tempName = selectedTemplate;
								tempName = tempName.Replace(".tx", string.Empty);
								int lastPos = tempName.LastIndexOf(@"\");
								try
								{
									tempName = tempName.Substring(lastPos + 1, tempName.Length - (lastPos + 1));
								}
								catch {}
							
								TabPage tp = new TabPage(tempName);							
								tp.Name = tempName;
								tp.Tag = selectedTemplate;
								this.tcDocs.TabPages.Add(tp);
								EditDocumentTemplateUserControl edtuc = new EditDocumentTemplateUserControl(selectedTemplate, Globals.et, eDocsType.eP_S);
								edtuc.EventMergeDocument = em;
								edtuc.ControlTabIndex = iTabIndex;
								edtuc.Dock = DockStyle.Fill;				
								tp.Controls.Add (edtuc);
								iTabIndex++;
							}
						}
					}
					//--------------------------------------------

					if (true == Globals.CB_PS_PSALong)
					{
					bool bFound = false;
					IEnumerator ie = this.tcDocs.TabPages.GetEnumerator();
					while (ie.MoveNext())
					{
						TabPage tp = (TabPage)ie.Current;
						if (tp.Name == Globals.PSA_Long)
						{
							bFound = true;
							break;
						}
					}
					if (false == bFound)
					{
						TabPage tp = new TabPage(Globals.PSA_Long);
						tp.Name = Globals.PSA_Long;
						this.tcDocs.TabPages.Add(tp);
						EditDocumentTemplateUserControl edtuc = new EditDocumentTemplateUserControl(ApplicationHelper.GetAssemblyPath() + Globals.MailMergeDocumentsPath + Globals.F_PSA_Long, Globals.et, eDocsType.eP_S);
						edtuc.EventMergeDocument = em;
						edtuc.ControlTabIndex = iTabIndex;
						edtuc.Dock = DockStyle.Fill;				
						tp.Controls.Add (edtuc);
						iTabIndex++;
					}
				}
				else
				{						
					this.RemoveTabPageIfDeselected(Globals.PSA_Long);					
				}
					if (true == Globals.CB_PS_PSAShort)
					{	
						bool bFound = false;
						IEnumerator ie = this.tcDocs.TabPages.GetEnumerator();
						while (ie.MoveNext())
						{
							TabPage tp = (TabPage)ie.Current;
							if (tp.Name == Globals.PSA_Short)
							{
								bFound = true;
								break;
							}
						}
						if (false == bFound)
						{
							TabPage tp = new TabPage(Globals.PSA_Short);
							tp.Name = Globals.PSA_Short;
							this.tcDocs.TabPages.Add(tp);
							EditDocumentTemplateUserControl edtuc = new EditDocumentTemplateUserControl(ApplicationHelper.GetAssemblyPath() + Globals.MailMergeDocumentsPath + Globals.F_PSA_Short, Globals.et, eDocsType.eP_S);
							edtuc.EventMergeDocument = em;
							edtuc.EventMergeDocument = em;
							edtuc.ControlTabIndex = iTabIndex;
							edtuc.Dock = DockStyle.Fill;				
							tp.Controls.Add (edtuc);	
							iTabIndex++;
						}
					}
					else
					{						
						this.RemoveTabPageIfDeselected(Globals.PSA_Short);					
					}

					if (true == Globals.CB_PS_Trust)
					{	
						bool bFound = false;
						IEnumerator ie = this.tcDocs.TabPages.GetEnumerator();
						while (ie.MoveNext())
						{
							TabPage tp = (TabPage)ie.Current;
							if (tp.Name == Globals.Trust)
							{
								bFound = true;
								break;
							}
						}
						if (false == bFound)
						{
							TabPage tp = new TabPage(Globals.Trust);
							tp.Name = Globals.Trust;
							this.tcDocs.TabPages.Add(tp);
							EditDocumentTemplateUserControl edtuc = new EditDocumentTemplateUserControl(ApplicationHelper.GetAssemblyPath() + Globals.MailMergeDocumentsPath + Globals.F_Trust, Globals.et, eDocsType.eP_S);
							edtuc.EventMergeDocument = em;
							edtuc.EventMergeDocument = em;
							edtuc.ControlTabIndex = iTabIndex;
							edtuc.Dock = DockStyle.Fill;				
							tp.Controls.Add (edtuc);	
							iTabIndex++;
						}
					}
					else
					{						
						this.RemoveTabPageIfDeselected(Globals.Trust);					
					}

					if (true == Globals.CB_PS_HOA)
					{	
						bool bFound = false;
						IEnumerator ie = this.tcDocs.TabPages.GetEnumerator();
						while (ie.MoveNext())
						{
							TabPage tp = (TabPage)ie.Current;
							if (tp.Name == Globals.HOA)
							{
								bFound = true;
								break;
							}
						}
						if (false == bFound)
						{
							TabPage tp = new TabPage(Globals.HOA);
							tp.Name = Globals.HOA;
							this.tcDocs.TabPages.Add(tp);
							EditDocumentTemplateUserControl edtuc = new EditDocumentTemplateUserControl(ApplicationHelper.GetAssemblyPath() + Globals.MailMergeDocumentsPath + Globals.F_HOA, Globals.et, eDocsType.eP_S);
							edtuc.EventMergeDocument = em;
							edtuc.ControlTabIndex = iTabIndex;
							edtuc.Dock = DockStyle.Fill;				
							tp.Controls.Add (edtuc);
							iTabIndex++;
						}
					}
					else
					{						
						this.RemoveTabPageIfDeselected(Globals.HOA);					
					}

					if (true == Globals.CB_PS_Addendum)
					{	
						bool bFound = false;
						IEnumerator ie = this.tcDocs.TabPages.GetEnumerator();
						while (ie.MoveNext())
						{
							TabPage tp = (TabPage)ie.Current;
							if (tp.Name == Globals.Addendum)
							{
								bFound = true;
								break;
							}
						}
						if (false == bFound)
						{
							TabPage tp = new TabPage(Globals.Addendum);
							tp.Name = Globals.Addendum;
							this.tcDocs.TabPages.Add(tp);
							EditDocumentTemplateUserControl edtuc = new EditDocumentTemplateUserControl(ApplicationHelper.GetAssemblyPath() + Globals.MailMergeDocumentsPath + Globals.F_Addendum, Globals.et, eDocsType.eP_S);
							edtuc.EventMergeDocument = em;
							edtuc.ControlTabIndex = iTabIndex;
							edtuc.Dock = DockStyle.Fill;				
							tp.Controls.Add (edtuc);
							iTabIndex++;
						}
					}
					else
					{						
						this.RemoveTabPageIfDeselected(Globals.Addendum);					
					}

					if (true == Globals.CB_PS_Disclosure)
					{	
						bool bFound = false;
						IEnumerator ie = this.tcDocs.TabPages.GetEnumerator();
						while (ie.MoveNext())
						{
							TabPage tp = (TabPage)ie.Current;
							if (tp.Name == Globals.Disclosure)
							{
								bFound = true;
								break;
							}
						}
						if (false == bFound)
						{
							TabPage tp = new TabPage(Globals.Disclosure);
							tp.Name = Globals.Disclosure;
							this.tcDocs.TabPages.Add(tp);
							EditDocumentTemplateUserControl edtuc = new EditDocumentTemplateUserControl(ApplicationHelper.GetAssemblyPath() + Globals.MailMergeDocumentsPath + Globals.F_Disclosure, Globals.et, eDocsType.eP_S);
							edtuc.EventMergeDocument = em;
							edtuc.ControlTabIndex = iTabIndex;
							edtuc.Dock = DockStyle.Fill;				
							tp.Controls.Add (edtuc);	
							iTabIndex++;
						}
					}
					else
					{						
						this.RemoveTabPageIfDeselected(Globals.Disclosure);					
					}

					if (true == Globals.CB_PS_AuthToRel1)
					{	
						bool bFound = false;
						IEnumerator ie = this.tcDocs.TabPages.GetEnumerator();
						while (ie.MoveNext())
						{
							TabPage tp = (TabPage)ie.Current;
							if (tp.Name == Globals.AuthToRel1)
							{
								bFound = true;
								break;
							}
						}
						if (false == bFound)
						{
							TabPage tp = new TabPage(Globals.AuthToRel1);
							tp.Name = Globals.AuthToRel1;
							this.tcDocs.TabPages.Add(tp);
							EditDocumentTemplateUserControl edtuc = new EditDocumentTemplateUserControl(ApplicationHelper.GetAssemblyPath() + Globals.MailMergeDocumentsPath + Globals.F_AuthToRel1, Globals.et, eDocsType.eP_S);
							edtuc.EventMergeDocument = em;
							edtuc.ControlTabIndex = iTabIndex;
							edtuc.Dock = DockStyle.Fill;				
							tp.Controls.Add (edtuc);	
							iTabIndex++;
						}
					}
					else
					{						
						this.RemoveTabPageIfDeselected(Globals.AuthToRel1);					
					}

					if (true == Globals.CB_PS_AuthToRel2)
					{	
						bool bFound = false;
						IEnumerator ie = this.tcDocs.TabPages.GetEnumerator();
						while (ie.MoveNext())
						{
							TabPage tp = (TabPage)ie.Current;
							if (tp.Name == Globals.AuthToRel2)
							{
								bFound = true;
								break;
							}
						}
						if (false == bFound)
						{
							TabPage tp = new TabPage(Globals.AuthToRel2);
							tp.Name = Globals.AuthToRel2;
							this.tcDocs.TabPages.Add(tp);
							EditDocumentTemplateUserControl edtuc = new EditDocumentTemplateUserControl(ApplicationHelper.GetAssemblyPath() + Globals.MailMergeDocumentsPath + Globals.F_AuthToRel2, Globals.et, eDocsType.eP_S);
							edtuc.EventMergeDocument = em;
							edtuc.ControlTabIndex = iTabIndex;
							edtuc.Dock = DockStyle.Fill;				
							tp.Controls.Add (edtuc);	
							iTabIndex++;
						}
					}
					else
					{						
						this.RemoveTabPageIfDeselected(Globals.AuthToRel2);					
					}

					if (true == Globals.CB_PS_Fax)
					{	
						bool bFound = false;
						IEnumerator ie = this.tcDocs.TabPages.GetEnumerator();
						while (ie.MoveNext())
						{
							TabPage tp = (TabPage)ie.Current;
							if (tp.Name == Globals.Fax)
							{
								bFound = true;
								break;
							}
						}
						if (false == bFound)
						{
							TabPage tp = new TabPage(Globals.Fax);
							tp.Name = Globals.Fax;
							this.tcDocs.TabPages.Add(tp);
							EditDocumentTemplateUserControl edtuc = new EditDocumentTemplateUserControl(ApplicationHelper.GetAssemblyPath() + Globals.MailMergeDocumentsPath + Globals.F_Fax, Globals.et, eDocsType.eP_S);
							edtuc.EventMergeDocument = em;
							edtuc.ControlTabIndex = iTabIndex;
							edtuc.Dock = DockStyle.Fill;				
							tp.Controls.Add (edtuc);	
							iTabIndex++;
						}
					}
					else
					{						
						this.RemoveTabPageIfDeselected(Globals.Fax);					
					}

					if (true == Globals.CB_PS_UDoc1)
					{	
						bool bFound = false;
						IEnumerator ie = this.tcDocs.TabPages.GetEnumerator();
						while (ie.MoveNext())
						{
							TabPage tp = (TabPage)ie.Current;
							if (tp.Name == Globals.UDoc1)
							{
								bFound = true;
								break;
							}
						}
						if (false == bFound)
						{
							TabPage tp = new TabPage(Globals.UDoc1);
							tp.Name = Globals.UDoc1;
							this.tcDocs.TabPages.Add(tp);
							EditDocumentTemplateUserControl edtuc = new EditDocumentTemplateUserControl(ApplicationHelper.GetAssemblyPath() + Globals.MailMergeDocumentsPath + Globals.F_UDoc1, Globals.et, eDocsType.eP_S);
							edtuc.EventMergeDocument = em;
							edtuc.ControlTabIndex = iTabIndex;
							edtuc.Dock = DockStyle.Fill;				
							tp.Controls.Add (edtuc);	
							iTabIndex++;
						}
					}
					else
					{						
						this.RemoveTabPageIfDeselected(Globals.UDoc1);					
					}

					if (true == Globals.CB_PS_UDoc2)
					{	
						bool bFound = false;
						IEnumerator ie = this.tcDocs.TabPages.GetEnumerator();
						while (ie.MoveNext())
						{
							TabPage tp = (TabPage)ie.Current;
							if (tp.Name == Globals.UDoc2)
							{
								bFound = true;
								break;
							}
						}
						if (false == bFound)
						{
							TabPage tp = new TabPage(Globals.UDoc2);
							tp.Name = Globals.UDoc2;
							this.tcDocs.TabPages.Add(tp);
							EditDocumentTemplateUserControl edtuc = new EditDocumentTemplateUserControl(ApplicationHelper.GetAssemblyPath() + Globals.MailMergeDocumentsPath + Globals.F_UDoc2, Globals.et, eDocsType.eP_S);
							edtuc.EventMergeDocument = em;
							edtuc.ControlTabIndex = iTabIndex;
							edtuc.Dock = DockStyle.Fill;				
							tp.Controls.Add (edtuc);	
							iTabIndex++;
						}
					}
					else
					{						
						this.RemoveTabPageIfDeselected(Globals.UDoc2);					
					}
					Globals.ps_TabIndex = iTabIndex;
					#endregion � eDocsType.eP_S �
					break;

				case eDocsType.eSub2:
					#region � eDocsType.eSub2 �
					
					//--------------------------------------------
					//USER DOCUMENTS
					RemoveTabPageIfDeselected(eDocsType.eSub2);
					if (null != Globals._Sub_SelectedTemplates)
					{
						foreach (string selectedTemplate in Globals._Sub_SelectedTemplates)
						{
							bool bFound = false;
							IEnumerator ie = this.tcDocs.TabPages.GetEnumerator();
							while (ie.MoveNext())
							{
								TabPage tp = (TabPage)ie.Current;
								if ((string)tp.Tag == selectedTemplate)
								{
									bFound = true;
									break;
								}
							}
							if (false == bFound)
							{
								string tempName = selectedTemplate;
								tempName = tempName.Replace(".tx", string.Empty);
								int lastPos = tempName.LastIndexOf(@"\");
								try
								{
									tempName = tempName.Substring(lastPos + 1, tempName.Length - (lastPos + 1));
								}
								catch {}
							
								TabPage tp = new TabPage(tempName);							
								tp.Name = tempName;
								tp.Tag = selectedTemplate;
								this.tcDocs.TabPages.Add(tp);
								EditDocumentTemplateUserControl edtuc = new EditDocumentTemplateUserControl(selectedTemplate, Globals.et, eDocsType.eSub2);
								edtuc.EventMergeDocument = em;
								edtuc.ControlTabIndex = iTabIndex;
								edtuc.Dock = DockStyle.Fill;				
								tp.Controls.Add (edtuc);
								iTabIndex++;
							}
						}
					}
					//--------------------------------------------

					
					if (true == Globals.CB_PromissoryNoteB)
					{
						bool bFound = false;
						IEnumerator ie = this.tcDocs.TabPages.GetEnumerator();
						while (ie.MoveNext())
						{
							TabPage tp = (TabPage)ie.Current;
							if (tp.Name == Globals.PromissoryNoteB)
							{
								bFound = true;
								break;
							}
						}
						if (false == bFound)
						{
							TabPage tp = new TabPage(Globals.PromissoryNoteB);
							tp.Name = Globals.PromissoryNoteB;
							this.tcDocs.TabPages.Add(tp);
							EditDocumentTemplateUserControl edtuc = new EditDocumentTemplateUserControl(ApplicationHelper.GetAssemblyPath() + Globals.MailMergeDocumentsPath + Globals.F_PromissoryNoteB, Globals.et, eDocsType.eSub2);
							edtuc.EventMergeDocument = em;
							edtuc.ControlTabIndex = iTabIndex;
							edtuc.Dock = DockStyle.Fill;				
							tp.Controls.Add (edtuc);
							iTabIndex++;
						}
					}
					else
					{						
						this.RemoveTabPageIfDeselected(Globals.PromissoryNoteB);					
					}
					
					#region � CB_Deed �
					if (true == Globals.CB_Deed)
					{
						bool bFound = false;
						IEnumerator ie = this.tcDocs.TabPages.GetEnumerator();
						while (ie.MoveNext())
						{
							TabPage tp = (TabPage)ie.Current;
							if (tp.Name == Globals.Deed)
							{
								bFound = true;
								break;
							}
						}
						if (false == bFound)
						{
							TabPage tp = new TabPage(Globals.Deed);
							tp.Name = Globals.Deed;
							this.tcDocs.TabPages.Add(tp);
							EditDocumentTemplateUserControl edtuc = new EditDocumentTemplateUserControl(ApplicationHelper.GetAssemblyPath() + Globals.MailMergeDocumentsPath + Globals.F_Deed, Globals.et, eDocsType.eSub2);
							edtuc.EventMergeDocument = em;
							edtuc.ControlTabIndex = iTabIndex;
							edtuc.Dock = DockStyle.Fill;				
							tp.Controls.Add (edtuc);
							iTabIndex++;
						}
					}
					else
					{						
						this.RemoveTabPageIfDeselected(Globals.Deed);					
					}
					#endregion � CB_Deed �

					#region � CB_WarrantyDeed �
					if (true == Globals.CB_WarrantyDeed)
					{
						bool bFound = false;
						IEnumerator ie = this.tcDocs.TabPages.GetEnumerator();
						while (ie.MoveNext())
						{
							TabPage tp = (TabPage)ie.Current;
							if (tp.Name == Globals.WarrantyDeed)
							{
								bFound = true;
								break;
							}
						}
						if (false == bFound)
						{
							TabPage tp = new TabPage(Globals.WarrantyDeed);
							tp.Name = Globals.WarrantyDeed;
							this.tcDocs.TabPages.Add(tp);
							EditDocumentTemplateUserControl edtuc = new EditDocumentTemplateUserControl(ApplicationHelper.GetAssemblyPath() + Globals.MailMergeDocumentsPath + Globals.F_WarrantyDeed, Globals.et, eDocsType.eSub2);
							edtuc.EventMergeDocument = em;
							edtuc.ControlTabIndex = iTabIndex;
							edtuc.Dock = DockStyle.Fill;				
							tp.Controls.Add (edtuc);
							iTabIndex++;
						}
					}
					else
					{						
						this.RemoveTabPageIfDeselected(Globals.WarrantyDeed);					
					}
					#endregion � CB_WarrantyDeed �

					#region � CB_QuitClaimDeed �
					if (true == Globals.CB_QuitClaimDeed)
					{
						bool bFound = false;
						IEnumerator ie = this.tcDocs.TabPages.GetEnumerator();
						while (ie.MoveNext())
						{
							TabPage tp = (TabPage)ie.Current;
							if (tp.Name == Globals.QuitClaimDeed)
							{
								bFound = true;
								break;
							}
						}
						if (false == bFound)
						{
							TabPage tp = new TabPage(Globals.QuitClaimDeed);
							tp.Name = Globals.QuitClaimDeed;
							this.tcDocs.TabPages.Add(tp);
							EditDocumentTemplateUserControl edtuc = new EditDocumentTemplateUserControl(ApplicationHelper.GetAssemblyPath() + Globals.MailMergeDocumentsPath + Globals.F_QuitClaimDeed, Globals.et, eDocsType.eSub2);
							edtuc.EventMergeDocument = em;
							edtuc.ControlTabIndex = iTabIndex;
							edtuc.Dock = DockStyle.Fill;				
							tp.Controls.Add (edtuc);
							iTabIndex++;
						}
					}
					else
					{						
						this.RemoveTabPageIfDeselected(Globals.QuitClaimDeed);					
					}
					#endregion � CB_QuitClaimDeed �
					#region � LimitedPowerOfAttorney �
					if (true == Globals.CB_LimitedPowerOfAttorney)
					{
						bool bFound = false;
						IEnumerator ie = this.tcDocs.TabPages.GetEnumerator();
						while (ie.MoveNext())
						{
							TabPage tp = (TabPage)ie.Current;
							if (tp.Name == Globals.LimitedPowerOfAttorney)
							{
								bFound = true;
								break;
							}
						}
						if (false == bFound)
						{
							TabPage tp = new TabPage(Globals.LimitedPowerOfAttorney);
							tp.Name = Globals.LimitedPowerOfAttorney;
							this.tcDocs.TabPages.Add(tp);
							EditDocumentTemplateUserControl edtuc = new EditDocumentTemplateUserControl(ApplicationHelper.GetAssemblyPath() + Globals.MailMergeDocumentsPath + Globals.F_LimitedPowerOfAttorney, Globals.et, eDocsType.eSub2);
							edtuc.EventMergeDocument = em;
							edtuc.ControlTabIndex = iTabIndex;
							edtuc.Dock = DockStyle.Fill;				
							tp.Controls.Add (edtuc);
							iTabIndex++;
						}
					}
					else
					{						
						this.RemoveTabPageIfDeselected(Globals.LimitedPowerOfAttorney);					
					}
					#endregion � LimitedPowerOfAttorney �				
					#region � TrustAgreement �
					if (true == Globals.CB_TrustAgreement)
					{
						bool bFound = false;
						IEnumerator ie = this.tcDocs.TabPages.GetEnumerator();
						while (ie.MoveNext())
						{
							TabPage tp = (TabPage)ie.Current;
							if (tp.Name == Globals.TrustAgreement)
							{
								bFound = true;
								break;
							}
						}
						if (false == bFound)
						{
							TabPage tp = new TabPage(Globals.TrustAgreement);
							tp.Name = Globals.TrustAgreement;
							this.tcDocs.TabPages.Add(tp);
							EditDocumentTemplateUserControl edtuc = new EditDocumentTemplateUserControl(ApplicationHelper.GetAssemblyPath() + Globals.MailMergeDocumentsPath + Globals.F_TrustAgreement, Globals.et, eDocsType.eSub2);
							edtuc.EventMergeDocument = em;
							edtuc.ControlTabIndex = iTabIndex;
							edtuc.Dock = DockStyle.Fill;				
							tp.Controls.Add (edtuc);
							iTabIndex++;
						}
					}
					else
					{						
						this.RemoveTabPageIfDeselected(Globals.TrustAgreement);					
					}
					#endregion � TrustAgreement �
					#region � AssignmentOfBeneficialInterestInTrust �
					if (true == Globals.CB_AssignmentOfBeneficialInterestInTrust)
					{
						bool bFound = false;
						IEnumerator ie = this.tcDocs.TabPages.GetEnumerator();
						while (ie.MoveNext())
						{
							TabPage tp = (TabPage)ie.Current;
							if (tp.Name == Globals.AssignmentOfBeneficialInterestInTrust)
							{
								bFound = true;
								break;
							}
						}
						if (false == bFound)
						{
							TabPage tp = new TabPage(Globals.AssignmentOfBeneficialInterestInTrust);
							tp.Name = Globals.AssignmentOfBeneficialInterestInTrust;
							this.tcDocs.TabPages.Add(tp);
							EditDocumentTemplateUserControl edtuc = new EditDocumentTemplateUserControl(ApplicationHelper.GetAssemblyPath() + Globals.MailMergeDocumentsPath + Globals.F_AssignmentOfBeneficialInterestInTrust, Globals.et, eDocsType.eSub2);
							edtuc.EventMergeDocument = em;
							edtuc.ControlTabIndex = iTabIndex;
							edtuc.Dock = DockStyle.Fill;				
							tp.Controls.Add (edtuc);
							iTabIndex++;
						}
					}
					else
					{						
						this.RemoveTabPageIfDeselected(Globals.AssignmentOfBeneficialInterestInTrust);					
					}
					#endregion � AssignmentOfBeneficialInterestInTrust �
					#region � AuthorizationToReleaseFor1stLoan �
					if (true == Globals.CB_AuthorizationToReleaseFor1stLoan)
					{
						bool bFound = false;
						IEnumerator ie = this.tcDocs.TabPages.GetEnumerator();
						while (ie.MoveNext())
						{
							TabPage tp = (TabPage)ie.Current;
							if (tp.Name == Globals.AuthorizationToReleaseFor1stLoan)
							{
								bFound = true;
								break;
							}
						}
						if (false == bFound)
						{
							TabPage tp = new TabPage(Globals.AuthorizationToReleaseFor1stLoan);
							tp.Name = Globals.AuthorizationToReleaseFor1stLoan;
							this.tcDocs.TabPages.Add(tp);
							EditDocumentTemplateUserControl edtuc = new EditDocumentTemplateUserControl(ApplicationHelper.GetAssemblyPath() + Globals.MailMergeDocumentsPath + Globals.F_AuthorizationToReleaseFor1stLoan, Globals.et, eDocsType.eSub2);
							edtuc.EventMergeDocument = em;
							edtuc.ControlTabIndex = iTabIndex;
							edtuc.Dock = DockStyle.Fill;				
							tp.Controls.Add (edtuc);
							iTabIndex++;
						}
					}
					else
					{						
						this.RemoveTabPageIfDeselected(Globals.AuthorizationToReleaseFor1stLoan);					
					}
					#endregion � AuthorizationToReleaseFor1stLoan �
					#region � AuthorizationToReleaseFor2ndLoan �
					if (true == Globals.CB_AuthorizationToReleaseFor2ndLoan)
					{
						bool bFound = false;
						IEnumerator ie = this.tcDocs.TabPages.GetEnumerator();
						while (ie.MoveNext())
						{
							TabPage tp = (TabPage)ie.Current;
							if (tp.Name == Globals.AuthorizationToReleaseFor2ndLoan)
							{
								bFound = true;
								break;
							}
						}
						if (false == bFound)
						{
							TabPage tp = new TabPage(Globals.AuthorizationToReleaseFor2ndLoan);
							tp.Name = Globals.AuthorizationToReleaseFor2ndLoan;
							this.tcDocs.TabPages.Add(tp);
							EditDocumentTemplateUserControl edtuc = new EditDocumentTemplateUserControl(ApplicationHelper.GetAssemblyPath() + Globals.MailMergeDocumentsPath + Globals.F_AuthorizationToReleaseFor2ndLoan, Globals.et, eDocsType.eSub2);
							edtuc.EventMergeDocument = em;
							edtuc.ControlTabIndex = iTabIndex;
							edtuc.Dock = DockStyle.Fill;				
							tp.Controls.Add (edtuc);
							iTabIndex++;
						}
					}
					else
					{						
						this.RemoveTabPageIfDeselected(Globals.AuthorizationToReleaseFor2ndLoan);					
					}
					#endregion � AuthorizationToReleaseFor2ndLoan �
					#region � AuthorizationToReleaseFor3rdLoan �
					if (true == Globals.CB_AuthorizationToReleaseFor3rdLoan)
					{
						bool bFound = false;
						IEnumerator ie = this.tcDocs.TabPages.GetEnumerator();
						while (ie.MoveNext())
						{
							TabPage tp = (TabPage)ie.Current;
							if (tp.Name == Globals.AuthorizationToReleaseFor3rdLoan)
							{
								bFound = true;
								break;
							}
						}
						if (false == bFound)
						{
							TabPage tp = new TabPage(Globals.AuthorizationToReleaseFor3rdLoan);
							tp.Name = Globals.AuthorizationToReleaseFor3rdLoan;
							this.tcDocs.TabPages.Add(tp);
							EditDocumentTemplateUserControl edtuc = new EditDocumentTemplateUserControl(ApplicationHelper.GetAssemblyPath() + Globals.MailMergeDocumentsPath + Globals.F_AuthorizationToReleaseFor3rdLoan, Globals.et, eDocsType.eSub2);
							edtuc.EventMergeDocument = em;
							edtuc.ControlTabIndex = iTabIndex;
							edtuc.Dock = DockStyle.Fill;				
							tp.Controls.Add (edtuc);
							iTabIndex++;
						}
					}
					else
					{						
						this.RemoveTabPageIfDeselected(Globals.AuthorizationToReleaseFor3rdLoan);					
					}
					#endregion � AuthorizationToReleaseFor3rdLoan �
					#region � AffidavitOfCompletedSale �
					if (true == Globals.CB_AffidavitOfCompletedSale)
					{
						bool bFound = false;
						IEnumerator ie = this.tcDocs.TabPages.GetEnumerator();
						while (ie.MoveNext())
						{
							TabPage tp = (TabPage)ie.Current;
							if (tp.Name == Globals.AffidavitOfCompletedSale)
							{
								bFound = true;
								break;
							}
						}
						if (false == bFound)
						{
							TabPage tp = new TabPage(Globals.AffidavitOfCompletedSale);
							tp.Name = Globals.AffidavitOfCompletedSale;
							this.tcDocs.TabPages.Add(tp);
							EditDocumentTemplateUserControl edtuc = new EditDocumentTemplateUserControl(ApplicationHelper.GetAssemblyPath() + Globals.MailMergeDocumentsPath + Globals.F_AffidavitOfCompletedSale, Globals.et, eDocsType.eSub2);
							edtuc.EventMergeDocument = em;
							edtuc.ControlTabIndex = iTabIndex;
							edtuc.Dock = DockStyle.Fill;				
							tp.Controls.Add (edtuc);
							iTabIndex++;
						}
					}
					else
					{						
						this.RemoveTabPageIfDeselected(Globals.AffidavitOfCompletedSale);					
					}
					#endregion � AffidavitOfCompletedSale �
					#region � AffidavitOfNoLiens �
					if (true == Globals.CB_AffidavitOfNoLiens)
					{
						bool bFound = false;
						IEnumerator ie = this.tcDocs.TabPages.GetEnumerator();
						while (ie.MoveNext())
						{
							TabPage tp = (TabPage)ie.Current;
							if (tp.Name == Globals.AffidavitOfNoLiens)
							{
								bFound = true;
								break;
							}
						}
						if (false == bFound)
						{
							TabPage tp = new TabPage(Globals.AffidavitOfNoLiens);
							tp.Name = Globals.AffidavitOfNoLiens;
							this.tcDocs.TabPages.Add(tp);
							EditDocumentTemplateUserControl edtuc = new EditDocumentTemplateUserControl(ApplicationHelper.GetAssemblyPath() + Globals.MailMergeDocumentsPath + Globals.F_AffidavitOfNoLiens, Globals.et, eDocsType.eSub2);
							edtuc.EventMergeDocument = em;
							edtuc.ControlTabIndex = iTabIndex;
							edtuc.Dock = DockStyle.Fill;				
							tp.Controls.Add (edtuc);
							iTabIndex++;
						}
					}
					else
					{						
						this.RemoveTabPageIfDeselected(Globals.AffidavitOfNoLiens);					
					}
					#endregion � AffidavitOfNoLiens �					
					#region � SellerLoanDisclosureNoPromises �
					if (true == Globals.CB_SellerLoanDisclosureNoPromises)
					{
						bool bFound = false;
						IEnumerator ie = this.tcDocs.TabPages.GetEnumerator();
						while (ie.MoveNext())
						{
							TabPage tp = (TabPage)ie.Current;
							if (tp.Name == Globals.SellerLoanDisclosureNoPromises)
							{
								bFound = true;
								break;
							}
						}
						if (false == bFound)
						{
							TabPage tp = new TabPage(Globals.SellerLoanDisclosureNoPromises);
							tp.Name = Globals.SellerLoanDisclosureNoPromises;
							this.tcDocs.TabPages.Add(tp);
							EditDocumentTemplateUserControl edtuc = new EditDocumentTemplateUserControl(ApplicationHelper.GetAssemblyPath() + Globals.MailMergeDocumentsPath + Globals.F_SellerLoanDisclosureNoPromises, Globals.et, eDocsType.eSub2);
							edtuc.EventMergeDocument = em;
							edtuc.ControlTabIndex = iTabIndex;
							edtuc.Dock = DockStyle.Fill;				
							tp.Controls.Add (edtuc);
							iTabIndex++;
						}
					}
					else
					{						
						this.RemoveTabPageIfDeselected(Globals.SellerLoanDisclosureNoPromises);					
					}
					#endregion � SellerLoanDisclosureNoPromises �
					#region � SellerLoanDisclosure �
					if (true == Globals.CB_SellerLoanDisclosure)
					{
						bool bFound = false;
						IEnumerator ie = this.tcDocs.TabPages.GetEnumerator();
						while (ie.MoveNext())
						{
							TabPage tp = (TabPage)ie.Current;
							if (tp.Name == Globals.SellerLoanDisclosure)
							{
								bFound = true;
								break;
							}
						}
						if (false == bFound)
						{
							TabPage tp = new TabPage(Globals.SellerLoanDisclosure);
							tp.Name = Globals.SellerLoanDisclosure;
							this.tcDocs.TabPages.Add(tp);
							EditDocumentTemplateUserControl edtuc = new EditDocumentTemplateUserControl(ApplicationHelper.GetAssemblyPath() + Globals.MailMergeDocumentsPath + Globals.F_SellerLoanDisclosure, Globals.et, eDocsType.eSub2);
							edtuc.EventMergeDocument = em;
							edtuc.ControlTabIndex = iTabIndex;
							edtuc.Dock = DockStyle.Fill;				
							tp.Controls.Add (edtuc);
							iTabIndex++;
						}
					}
					else
					{						
						this.RemoveTabPageIfDeselected(Globals.SellerLoanDisclosure);					
					}
					#endregion � SellerLoanDisclosure �
					#region � LetterOfAgreementAndUnderstanding �
					if (true == Globals.CB_LetterOfAgreementAndUnderstanding)
					{
						bool bFound = false;
						IEnumerator ie = this.tcDocs.TabPages.GetEnumerator();
						while (ie.MoveNext())
						{
							TabPage tp = (TabPage)ie.Current;
							if (tp.Name == Globals.LetterOfAgreementAndUnderstanding)
							{
								bFound = true;
								break;
							}
						}
						if (false == bFound)
						{
							TabPage tp = new TabPage(Globals.LetterOfAgreementAndUnderstanding);
							tp.Name = Globals.LetterOfAgreementAndUnderstanding;
							this.tcDocs.TabPages.Add(tp);
							EditDocumentTemplateUserControl edtuc = new EditDocumentTemplateUserControl(ApplicationHelper.GetAssemblyPath() + Globals.MailMergeDocumentsPath + Globals.F_LetterOfAgreementAndUnderstanding, Globals.et, eDocsType.eSub2);
							edtuc.EventMergeDocument = em;
							edtuc.ControlTabIndex = iTabIndex;
							edtuc.Dock = DockStyle.Fill;				
							tp.Controls.Add (edtuc);
							iTabIndex++;
						}
					}
					else
					{						
						this.RemoveTabPageIfDeselected(Globals.LetterOfAgreementAndUnderstanding);					
					}
					#endregion � LetterOfAgreementAndUnderstanding �
					#region � EscrowAccountLetter �
					if (true == Globals.CB_EscrowAccountLetter)
					{
						bool bFound = false;
						IEnumerator ie = this.tcDocs.TabPages.GetEnumerator();
						while (ie.MoveNext())
						{
							TabPage tp = (TabPage)ie.Current;
							if (tp.Name == Globals.EscrowAccountLetter)
							{
								bFound = true;
								break;
							}
						}
						if (false == bFound)
						{
							TabPage tp = new TabPage(Globals.EscrowAccountLetter);
							tp.Name = Globals.EscrowAccountLetter;
							this.tcDocs.TabPages.Add(tp);
							EditDocumentTemplateUserControl edtuc = new EditDocumentTemplateUserControl(ApplicationHelper.GetAssemblyPath() + Globals.MailMergeDocumentsPath + Globals.F_EscrowAccountLetter, Globals.et, eDocsType.eSub2);
							edtuc.EventMergeDocument = em;
							edtuc.ControlTabIndex = iTabIndex;
							edtuc.Dock = DockStyle.Fill;				
							tp.Controls.Add (edtuc);
							iTabIndex++;
						}
					}
					else
					{						
						this.RemoveTabPageIfDeselected(Globals.EscrowAccountLetter);					
					}
					#endregion � EscrowAccountLetter �
					#region � ChangeOfAddressLetterToMortgageCompany �
					if (true == Globals.CB_ChangeOfAddressLetterToMortgageCompany)
					{
						bool bFound = false;
						IEnumerator ie = this.tcDocs.TabPages.GetEnumerator();
						while (ie.MoveNext())
						{
							TabPage tp = (TabPage)ie.Current;
							if (tp.Name == Globals.ChangeOfAddressLetterToMortgageCompany)
							{
								bFound = true;
								break;
							}
						}
						if (false == bFound)
						{
							TabPage tp = new TabPage(Globals.ChangeOfAddressLetterToMortgageCompany);
							tp.Name = Globals.ChangeOfAddressLetterToMortgageCompany;
							this.tcDocs.TabPages.Add(tp);
							EditDocumentTemplateUserControl edtuc = new EditDocumentTemplateUserControl(ApplicationHelper.GetAssemblyPath() + Globals.MailMergeDocumentsPath + Globals.F_ChangeOfAddressLetterToMortgageCompany, Globals.et, eDocsType.eSub2);
							edtuc.EventMergeDocument = em;
							edtuc.ControlTabIndex = iTabIndex;
							edtuc.Dock = DockStyle.Fill;				
							tp.Controls.Add (edtuc);
							iTabIndex++;
						}
					}
					else
					{						
						this.RemoveTabPageIfDeselected(Globals.ChangeOfAddressLetterToMortgageCompany);					
					}
					#endregion � ChangeOfAddressLetterToMortgageCompany �
					#region � ChangeOfAddressLetterToHOACompany �
					if (true == Globals.CB_ChangeOfAddressLetterToHOACompany)
					{
						bool bFound = false;
						IEnumerator ie = this.tcDocs.TabPages.GetEnumerator();
						while (ie.MoveNext())
						{
							TabPage tp = (TabPage)ie.Current;
							if (tp.Name == Globals.ChangeOfAddressLetterToHOACompany)
							{
								bFound = true;
								break;
							}
						}
						if (false == bFound)
						{
							TabPage tp = new TabPage(Globals.ChangeOfAddressLetterToHOACompany);
							tp.Name = Globals.ChangeOfAddressLetterToHOACompany;
							this.tcDocs.TabPages.Add(tp);
							EditDocumentTemplateUserControl edtuc = new EditDocumentTemplateUserControl(ApplicationHelper.GetAssemblyPath() + Globals.MailMergeDocumentsPath + Globals.F_ChangeOfAddressLetterToHOACompany, Globals.et, eDocsType.eSub2);
							edtuc.EventMergeDocument = em;
							edtuc.ControlTabIndex = iTabIndex;
							edtuc.Dock = DockStyle.Fill;				
							tp.Controls.Add (edtuc);
							iTabIndex++;
						}
					}
					else
					{						
						this.RemoveTabPageIfDeselected(Globals.ChangeOfAddressLetterToHOACompany);					
					}
					#endregion � ChangeOfAddressLetterToHOACompany �
					#region � CancelInsuranceLetter �
					if (true == Globals.CB_CancelInsuranceLetter)
					{
						bool bFound = false;
						IEnumerator ie = this.tcDocs.TabPages.GetEnumerator();
						while (ie.MoveNext())
						{
							TabPage tp = (TabPage)ie.Current;
							if (tp.Name == Globals.CancelInsuranceLetter)
							{
								bFound = true;
								break;
							}
						}
						if (false == bFound)
						{
							TabPage tp = new TabPage(Globals.CancelInsuranceLetter);
							tp.Name = Globals.CancelInsuranceLetter;
							this.tcDocs.TabPages.Add(tp);
							EditDocumentTemplateUserControl edtuc = new EditDocumentTemplateUserControl(ApplicationHelper.GetAssemblyPath() + Globals.MailMergeDocumentsPath + Globals.F_CancelInsuranceLetter, Globals.et, eDocsType.eSub2);
							edtuc.EventMergeDocument = em;
							edtuc.ControlTabIndex = iTabIndex;
							edtuc.Dock = DockStyle.Fill;				
							tp.Controls.Add (edtuc);
							iTabIndex++;
						}
					}
					else
					{						
						this.RemoveTabPageIfDeselected(Globals.CancelInsuranceLetter);					
					}
					#endregion � CancelInsuranceLetter �
					#region � FaxCoverPageToCancelInsurance �
					if (true == Globals.CB_FaxCoverPageToCancelInsurance)
					{
						bool bFound = false;
						IEnumerator ie = this.tcDocs.TabPages.GetEnumerator();
						while (ie.MoveNext())
						{
							TabPage tp = (TabPage)ie.Current;
							if (tp.Name == Globals.FaxCoverPageToCancelInsurance)
							{
								bFound = true;
								break;
							}
						}
						if (false == bFound)
						{
							TabPage tp = new TabPage(Globals.FaxCoverPageToCancelInsurance);
							tp.Name = Globals.FaxCoverPageToCancelInsurance;
							this.tcDocs.TabPages.Add(tp);
							EditDocumentTemplateUserControl edtuc = new EditDocumentTemplateUserControl(ApplicationHelper.GetAssemblyPath() + Globals.MailMergeDocumentsPath + Globals.F_FaxCoverPageToCancelInsurance, Globals.et, eDocsType.eSub2);
							edtuc.EventMergeDocument = em;
							edtuc.ControlTabIndex = iTabIndex;
							edtuc.Dock = DockStyle.Fill;				
							tp.Controls.Add (edtuc);
							iTabIndex++;
						}
					}
					else
					{						
						this.RemoveTabPageIfDeselected(Globals.FaxCoverPageToCancelInsurance);					
					}
					#endregion � FaxCoverPageToCancelInsurance �
					#region � FaxCoverPageBlank �
					if (true == Globals.CB_FaxCoverPageBlank)
					{
						bool bFound = false;
						IEnumerator ie = this.tcDocs.TabPages.GetEnumerator();
						while (ie.MoveNext())
						{
							TabPage tp = (TabPage)ie.Current;
							if (tp.Name == Globals.FaxCoverPageBlank)
							{
								bFound = true;
								break;
							}
						}
						if (false == bFound)
						{
							TabPage tp = new TabPage(Globals.FaxCoverPageBlank);
							tp.Name = Globals.FaxCoverPageBlank;
							this.tcDocs.TabPages.Add(tp);
							EditDocumentTemplateUserControl edtuc = new EditDocumentTemplateUserControl(ApplicationHelper.GetAssemblyPath() + Globals.MailMergeDocumentsPath + Globals.F_FaxCoverPageBlank, Globals.et, eDocsType.eSub2);
							edtuc.EventMergeDocument = em;
							edtuc.ControlTabIndex = iTabIndex;
							edtuc.Dock = DockStyle.Fill;				
							tp.Controls.Add (edtuc);
							iTabIndex++;
						}
					}
					else
					{						
						this.RemoveTabPageIfDeselected(Globals.FaxCoverPageBlank);					
					}
					#endregion � FaxCoverPageBlank �
					#region � BillOfSale �
					if (true == Globals.CB_BillOfSale)
					{
						bool bFound = false;
						IEnumerator ie = this.tcDocs.TabPages.GetEnumerator();
						while (ie.MoveNext())
						{
							TabPage tp = (TabPage)ie.Current;
							if (tp.Name == Globals.BillOfSale)
							{
								bFound = true;
								break;
							}
						}
						if (false == bFound)
						{
							TabPage tp = new TabPage(Globals.BillOfSale);
							tp.Name = Globals.BillOfSale;
							this.tcDocs.TabPages.Add(tp);
							EditDocumentTemplateUserControl edtuc = new EditDocumentTemplateUserControl(ApplicationHelper.GetAssemblyPath() + Globals.MailMergeDocumentsPath + Globals.F_BillOfSale, Globals.et, eDocsType.eSub2);
							edtuc.EventMergeDocument = em;
							edtuc.ControlTabIndex = iTabIndex;
							edtuc.Dock = DockStyle.Fill;				
							tp.Controls.Add (edtuc);
							iTabIndex++;
						}
					}
					else
					{						
						this.RemoveTabPageIfDeselected(Globals.BillOfSale);					
					}
					#endregion � BillOfSale �
					#region � PromissoryNote �
					if (true == Globals.CB_PromissoryNote)
					{
						bool bFound = false;
						IEnumerator ie = this.tcDocs.TabPages.GetEnumerator();
						while (ie.MoveNext())
						{
							TabPage tp = (TabPage)ie.Current;
							if (tp.Name == Globals.PromissoryNote)
							{
								bFound = true;
								break;
							}
						}
						if (false == bFound)
						{
							TabPage tp = new TabPage(Globals.PromissoryNote);
							tp.Name = Globals.PromissoryNote;
							this.tcDocs.TabPages.Add(tp);
							EditDocumentTemplateUserControl edtuc = new EditDocumentTemplateUserControl(ApplicationHelper.GetAssemblyPath() + Globals.MailMergeDocumentsPath + Globals.F_PromissoryNote, Globals.et, eDocsType.eSub2);
							edtuc.EventMergeDocument = em;
							edtuc.ControlTabIndex = iTabIndex;
							edtuc.Dock = DockStyle.Fill;				
							tp.Controls.Add (edtuc);
							iTabIndex++;
						}
					}
					else
					{						
						this.RemoveTabPageIfDeselected(Globals.PromissoryNote);					
					}
					#endregion � PromissoryNote �
					#region � CB_SellerInformationSheet �
					if (true == Globals.CB_SellerInformationSheet)
					{
						bool bFound = false;
						IEnumerator ie = this.tcDocs.TabPages.GetEnumerator();
						while (ie.MoveNext())
						{
							TabPage tp = (TabPage)ie.Current;
							if (tp.Name == Globals.SellerInformationSheet)
							{
								bFound = true;
								break;
							}
						}
						if (false == bFound)
						{
							TabPage tp = new TabPage(Globals.SellerInformationSheet);
							tp.Name = Globals.SellerInformationSheet;
							this.tcDocs.TabPages.Add(tp);
							EditDocumentTemplateUserControl edtuc = new EditDocumentTemplateUserControl(ApplicationHelper.GetAssemblyPath() + Globals.MailMergeDocumentsPath + Globals.F_SellerInformationSheet, Globals.et, eDocsType.eSub2);
							edtuc.EventMergeDocument = em;
							edtuc.ControlTabIndex = iTabIndex;
							edtuc.Dock = DockStyle.Fill;				
							tp.Controls.Add (edtuc);
							iTabIndex++;
						}
					}
					else
					{						
						this.RemoveTabPageIfDeselected(Globals.SellerInformationSheet);					
					}
					#endregion � CB_SellerInformationSheet �
					#region � CB_UserSub2Doc1 �
					if (true == Globals.CB_UserSub2Doc1)
					{
						bool bFound = false;
						IEnumerator ie = this.tcDocs.TabPages.GetEnumerator();
						while (ie.MoveNext())
						{
							TabPage tp = (TabPage)ie.Current;
							if (tp.Name == Globals.UserSub2Doc1)
							{
								bFound = true;
								break;
							}
						}
						if (false == bFound)
						{
							TabPage tp = new TabPage(Globals.UserSub2Doc1);
							tp.Name = Globals.UserSub2Doc1;
							this.tcDocs.TabPages.Add(tp);
							EditDocumentTemplateUserControl edtuc = new EditDocumentTemplateUserControl(ApplicationHelper.GetAssemblyPath() + Globals.MailMergeDocumentsPath + Globals.F_UserSub2Doc1, Globals.et, eDocsType.eSub2);
							edtuc.EventMergeDocument = em;
							edtuc.ControlTabIndex = iTabIndex;
							edtuc.Dock = DockStyle.Fill;				
							tp.Controls.Add (edtuc);
							iTabIndex++;
						}
					}
					else
					{						
						this.RemoveTabPageIfDeselected(Globals.UserSub2Doc1);					
					}
					#endregion � CB_UserSub2Doc1 �
					#region � UserSub2Doc2 �
					if (true == Globals.CB_UserSub2Doc2)
					{
						bool bFound = false;
						IEnumerator ie = this.tcDocs.TabPages.GetEnumerator();
						while (ie.MoveNext())
						{
							TabPage tp = (TabPage)ie.Current;
							if (tp.Name == Globals.UserSub2Doc2)
							{
								bFound = true;
								break;
							}
						}
						if (false == bFound)
						{
							TabPage tp = new TabPage(Globals.UserSub2Doc2);
							tp.Name = Globals.UserSub2Doc2;
							this.tcDocs.TabPages.Add(tp);
							EditDocumentTemplateUserControl edtuc = new EditDocumentTemplateUserControl(ApplicationHelper.GetAssemblyPath() + Globals.MailMergeDocumentsPath + Globals.F_UserSub2Doc2, Globals.et, eDocsType.eSub2);
							edtuc.EventMergeDocument = em;
							edtuc.ControlTabIndex = iTabIndex;
							edtuc.Dock = DockStyle.Fill;				
							tp.Controls.Add (edtuc);
							iTabIndex++;
						}
					}
					else
					{						
						this.RemoveTabPageIfDeselected(Globals.UserSub2Doc2);					
					}
					#endregion � UserSub2Doc2 �
					#endregion � eDocsType.eSub2 �
					break;

				case eDocsType.eOption:
					#region � eDocsType.eOption �
					
					//--------------------------------------------
					//USER DOCUMENTS
					RemoveTabPageIfDeselected(eDocsType.eOption);
					if (null != Globals._Option_SelectedTemplates)
					{
						foreach (string selectedTemplate in Globals._Option_SelectedTemplates)
						{
							bool bFound = false;
							IEnumerator ie = this.tcDocs.TabPages.GetEnumerator();
							while (ie.MoveNext())
							{
								TabPage tp = (TabPage)ie.Current;
								if ((string)tp.Tag == selectedTemplate)
								{
									bFound = true;
									break;
								}
							}
							if (false == bFound)
							{
								string tempName = selectedTemplate;
								tempName = tempName.Replace(".tx", string.Empty);
								int lastPos = tempName.LastIndexOf(@"\");
								try
								{
									tempName = tempName.Substring(lastPos + 1, tempName.Length - (lastPos + 1));
								}
								catch {}
							
								TabPage tp = new TabPage(tempName);							
								tp.Name = tempName;
								tp.Tag = selectedTemplate;
								this.tcDocs.TabPages.Add(tp);
								EditDocumentTemplateUserControl edtuc = new EditDocumentTemplateUserControl(selectedTemplate, Globals.et, eDocsType.eOption);
								edtuc.EventMergeDocument = em;
								edtuc.ControlTabIndex = iTabIndex;
								edtuc.Dock = DockStyle.Fill;				
								tp.Controls.Add (edtuc);
								iTabIndex++;
							}
						}
					}
					//--------------------------------------------
					
					if (true == Globals.CB_OptionToPurchase)
					{
						bool bFound = false;
						IEnumerator ie = this.tcDocs.TabPages.GetEnumerator();
						while (ie.MoveNext())
						{
							TabPage tp = (TabPage)ie.Current;
							if (tp.Name == Globals.OptionToPurchase)
							{
								bFound = true;
								break;
							}
						}
						if (false == bFound)
						{
							TabPage tp = new TabPage(Globals.OptionToPurchase);
							tp.Name = Globals.OptionToPurchase;
							this.tcDocs.TabPages.Add(tp);
							EditDocumentTemplateUserControl edtuc = new EditDocumentTemplateUserControl(ApplicationHelper.GetAssemblyPath() + Globals.MailMergeDocumentsPath + Globals.F_OptionToPurchase, Globals.et, eDocsType.eOption);
							edtuc.EventMergeDocument = em;
							edtuc.ControlTabIndex = iTabIndex;
							edtuc.Dock = DockStyle.Fill;				
							tp.Controls.Add (edtuc);
							iTabIndex++;
						}
					}
					else
					{						
						this.RemoveTabPageIfDeselected(Globals.OptionToPurchase);					
					}

					if (true == Globals.CB_OptionToPurchaseAgreement)
					{
						bool bFound = false;
						IEnumerator ie = this.tcDocs.TabPages.GetEnumerator();
						while (ie.MoveNext())
						{
							TabPage tp = (TabPage)ie.Current;
							if (tp.Name == Globals.OptionToPurchaseAgreement)
							{
								bFound = true;
								break;
							}
						}
						if (false == bFound)
						{
							TabPage tp = new TabPage(Globals.OptionToPurchaseAgreement);
							tp.Name = Globals.OptionToPurchaseAgreement;
							this.tcDocs.TabPages.Add(tp);
							EditDocumentTemplateUserControl edtuc = new EditDocumentTemplateUserControl(ApplicationHelper.GetAssemblyPath() + Globals.MailMergeDocumentsPath + Globals.F_OptionToPurchaseAgreement, Globals.et, eDocsType.eOption);
							edtuc.EventMergeDocument = em;
							edtuc.ControlTabIndex = iTabIndex;
							edtuc.Dock = DockStyle.Fill;				
							tp.Controls.Add (edtuc);
							iTabIndex++;
						}
					}
					else
					{						
						this.RemoveTabPageIfDeselected(Globals.OptionToPurchaseAgreement);					
					}

					if (true == Globals.CB_AffidavitAndMemorandumOfOption)
					{
						bool bFound = false;
						IEnumerator ie = this.tcDocs.TabPages.GetEnumerator();
						while (ie.MoveNext())
						{
							TabPage tp = (TabPage)ie.Current;
							if (tp.Name == Globals.AffidavitAndMemorandumOfOption)
							{
								bFound = true;
								break;
							}
						}
						if (false == bFound)
						{
							TabPage tp = new TabPage(Globals.AffidavitAndMemorandumOfOption);
							tp.Name = Globals.AffidavitAndMemorandumOfOption;
							this.tcDocs.TabPages.Add(tp);
							EditDocumentTemplateUserControl edtuc = new EditDocumentTemplateUserControl(ApplicationHelper.GetAssemblyPath() + Globals.MailMergeDocumentsPath + Globals.F_AffidavitAndMemorandumOfOption, Globals.et, eDocsType.eOption);
							edtuc.EventMergeDocument = em;
							edtuc.ControlTabIndex = iTabIndex;
							edtuc.Dock = DockStyle.Fill;				
							tp.Controls.Add (edtuc);
							iTabIndex++;
						}
					}
					else
					{						
						this.RemoveTabPageIfDeselected(Globals.AffidavitAndMemorandumOfOption);					
					}
					if (true == Globals.CB_EquitySharingAddendum)
					{
						bool bFound = false;
						IEnumerator ie = this.tcDocs.TabPages.GetEnumerator();
						while (ie.MoveNext())
						{
							TabPage tp = (TabPage)ie.Current;
							if (tp.Name == Globals.EquitySharingAddendum)
							{
								bFound = true;
								break;
							}
						}
						if (false == bFound)
						{
							TabPage tp = new TabPage(Globals.EquitySharingAddendum);
							tp.Name = Globals.EquitySharingAddendum;
							this.tcDocs.TabPages.Add(tp);
							EditDocumentTemplateUserControl edtuc = new EditDocumentTemplateUserControl(ApplicationHelper.GetAssemblyPath() + Globals.MailMergeDocumentsPath + Globals.F_EquitySharingAddendum, Globals.et, eDocsType.eOption);
							edtuc.EventMergeDocument = em;
							edtuc.ControlTabIndex = iTabIndex;
							edtuc.Dock = DockStyle.Fill;				
							tp.Controls.Add (edtuc);
							iTabIndex++;
						}
					}
					else
					{						
						this.RemoveTabPageIfDeselected(Globals.EquitySharingAddendum);					
					}
					if (true == Globals.CB_RightToCancelAddendum)
					{
						bool bFound = false;
						IEnumerator ie = this.tcDocs.TabPages.GetEnumerator();
						while (ie.MoveNext())
						{
							TabPage tp = (TabPage)ie.Current;
							if (tp.Name == Globals.RightToCancelAddendum)
							{
								bFound = true;
								break;
							}
						}
						if (false == bFound)
						{
							TabPage tp = new TabPage(Globals.RightToCancelAddendum);
							tp.Name = Globals.RightToCancelAddendum;
							this.tcDocs.TabPages.Add(tp);
							EditDocumentTemplateUserControl edtuc = new EditDocumentTemplateUserControl(ApplicationHelper.GetAssemblyPath() + Globals.MailMergeDocumentsPath + Globals.F_RightToCancelAddendum, Globals.et, eDocsType.eOption);
							edtuc.EventMergeDocument = em;
							edtuc.ControlTabIndex = iTabIndex;
							edtuc.Dock = DockStyle.Fill;				
							tp.Controls.Add (edtuc);
							iTabIndex++;
						}
					}
					else
					{						
						this.RemoveTabPageIfDeselected(Globals.RightToCancelAddendum);					
					}
					if (true == Globals.CB_DeedFromSellerToBeDepositedInEscrow)
					{
						bool bFound = false;
						IEnumerator ie = this.tcDocs.TabPages.GetEnumerator();
						while (ie.MoveNext())
						{
							TabPage tp = (TabPage)ie.Current;
							if (tp.Name == Globals.DeedFromSellerToBeDepositedInEscrow)
							{
								bFound = true;
								break;
							}
						}
						if (false == bFound)
						{
							TabPage tp = new TabPage(Globals.DeedFromSellerToBeDepositedInEscrow);
							tp.Name = Globals.DeedFromSellerToBeDepositedInEscrow;
							this.tcDocs.TabPages.Add(tp);
							EditDocumentTemplateUserControl edtuc = new EditDocumentTemplateUserControl(ApplicationHelper.GetAssemblyPath() + Globals.MailMergeDocumentsPath + Globals.F_DeedFromSellerToBeDepositedInEscrow, Globals.et, eDocsType.eOption);
							edtuc.EventMergeDocument = em;
							edtuc.ControlTabIndex = iTabIndex;
							edtuc.Dock = DockStyle.Fill;				
							tp.Controls.Add (edtuc);
							iTabIndex++;
						}
					}
					else
					{						
						this.RemoveTabPageIfDeselected(Globals.DeedFromSellerToBeDepositedInEscrow);					
					}
					if (true == Globals.CB_QuitClaimDeedForYou)
					{
						bool bFound = false;
						IEnumerator ie = this.tcDocs.TabPages.GetEnumerator();
						while (ie.MoveNext())
						{
							TabPage tp = (TabPage)ie.Current;
							if (tp.Name == Globals.QuitClaimDeedForYou)
							{
								bFound = true;
								break;
							}
						}
						if (false == bFound)
						{
							TabPage tp = new TabPage(Globals.QuitClaimDeedForYou);
							tp.Name = Globals.QuitClaimDeedForYou;
							this.tcDocs.TabPages.Add(tp);
							EditDocumentTemplateUserControl edtuc = new EditDocumentTemplateUserControl(ApplicationHelper.GetAssemblyPath() + Globals.MailMergeDocumentsPath + Globals.F_QuitClaimDeedForYou, Globals.et, eDocsType.eOption);
							edtuc.EventMergeDocument = em;
							edtuc.ControlTabIndex = iTabIndex;
							edtuc.Dock = DockStyle.Fill;				
							tp.Controls.Add (edtuc);
							iTabIndex++;
						}
					}
					else
					{						
						this.RemoveTabPageIfDeselected(Globals.QuitClaimDeedForYou);					
					}
					if (true == Globals.CB_FaxCoverPageToAmendInsurance)
					{
						bool bFound = false;
						IEnumerator ie = this.tcDocs.TabPages.GetEnumerator();
						while (ie.MoveNext())
						{
							TabPage tp = (TabPage)ie.Current;
							if (tp.Name == Globals.FaxCoverPageToAmendInsurance)
							{
								bFound = true;
								break;
							}
						}
						if (false == bFound)
						{
							TabPage tp = new TabPage(Globals.FaxCoverPageToAmendInsurance);
							tp.Name = Globals.FaxCoverPageToAmendInsurance;
							this.tcDocs.TabPages.Add(tp);
							EditDocumentTemplateUserControl edtuc = new EditDocumentTemplateUserControl(ApplicationHelper.GetAssemblyPath() + Globals.MailMergeDocumentsPath + Globals.F_FaxCoverPageToAmendInsurance, Globals.et, eDocsType.eOption);
							edtuc.EventMergeDocument = em;
							edtuc.ControlTabIndex = iTabIndex;
							edtuc.Dock = DockStyle.Fill;				
							tp.Controls.Add (edtuc);
							iTabIndex++;
						}
					}
					else
					{						
						this.RemoveTabPageIfDeselected(Globals.FaxCoverPageToAmendInsurance);					
					}
					if (true == Globals.CB_AmendInsuranceLetter)
					{
						bool bFound = false;
						IEnumerator ie = this.tcDocs.TabPages.GetEnumerator();
						while (ie.MoveNext())
						{
							TabPage tp = (TabPage)ie.Current;
							if (tp.Name == Globals.AmendInsuranceLetter)
							{
								bFound = true;
								break;
							}
						}
						if (false == bFound)
						{
							TabPage tp = new TabPage(Globals.AmendInsuranceLetter);
							tp.Name = Globals.AmendInsuranceLetter;
							this.tcDocs.TabPages.Add(tp);
							EditDocumentTemplateUserControl edtuc = new EditDocumentTemplateUserControl(ApplicationHelper.GetAssemblyPath() + Globals.MailMergeDocumentsPath + Globals.F_AmendInsuranceLetter, Globals.et, eDocsType.eOption);
							edtuc.ControlTabIndex = iTabIndex;
							edtuc.Dock = DockStyle.Fill;				
							tp.Controls.Add (edtuc);
							iTabIndex++;
						}
					}
					else
					{						
						this.RemoveTabPageIfDeselected(Globals.AmendInsuranceLetter);					
					}
					if (true == Globals.CB_OptionFaxCoverPageBlank)
					{
						bool bFound = false;
						IEnumerator ie = this.tcDocs.TabPages.GetEnumerator();
						while (ie.MoveNext())
						{
							TabPage tp = (TabPage)ie.Current;
							if (tp.Name == Globals.OptionFaxCoverPageBlank)
							{
								bFound = true;
								break;
							}
						}
						if (false == bFound)
						{
							TabPage tp = new TabPage(Globals.OptionFaxCoverPageBlank);
							tp.Name = Globals.OptionFaxCoverPageBlank;
							this.tcDocs.TabPages.Add(tp);
							EditDocumentTemplateUserControl edtuc = new EditDocumentTemplateUserControl(ApplicationHelper.GetAssemblyPath() + Globals.MailMergeDocumentsPath + Globals.F_OptionFaxCoverPageBlank, Globals.et, eDocsType.eOption);
							edtuc.ControlTabIndex = iTabIndex;
							edtuc.Dock = DockStyle.Fill;				
							tp.Controls.Add (edtuc);
							iTabIndex++;
						}
					}
					else
					{						
						this.RemoveTabPageIfDeselected(Globals.OptionFaxCoverPageBlank);					
					}
					if (true == Globals.CB_UserOptionDoc1)
					{
						bool bFound = false;
						IEnumerator ie = this.tcDocs.TabPages.GetEnumerator();
						while (ie.MoveNext())
						{
							TabPage tp = (TabPage)ie.Current;
							if (tp.Name == Globals.UserOptionDoc1)
							{
								bFound = true;
								break;
							}
						}
						if (false == bFound)
						{
							TabPage tp = new TabPage(Globals.UserOptionDoc1);
							tp.Name = Globals.UserOptionDoc1;
							this.tcDocs.TabPages.Add(tp);
							EditDocumentTemplateUserControl edtuc = new EditDocumentTemplateUserControl(ApplicationHelper.GetAssemblyPath() + Globals.MailMergeDocumentsPath + Globals.F_UserOptionDoc1, Globals.et, eDocsType.eOption);
							edtuc.EventMergeDocument = em;
							edtuc.ControlTabIndex = iTabIndex;
							edtuc.Dock = DockStyle.Fill;				
							tp.Controls.Add (edtuc);
							iTabIndex++;
						}
					}
					else
					{						
						this.RemoveTabPageIfDeselected(Globals.UserOptionDoc1);					
					}
					if (true == Globals.CB_UserOptionDoc2)
					{
						bool bFound = false;
						IEnumerator ie = this.tcDocs.TabPages.GetEnumerator();
						while (ie.MoveNext())
						{
							TabPage tp = (TabPage)ie.Current;
							if (tp.Name == Globals.UserOptionDoc2)
							{
								bFound = true;
								break;
							}
						}
						if (false == bFound)
						{
							TabPage tp = new TabPage(Globals.UserOptionDoc2);
							tp.Name = Globals.UserOptionDoc2;
							this.tcDocs.TabPages.Add(tp);
							EditDocumentTemplateUserControl edtuc = new EditDocumentTemplateUserControl(ApplicationHelper.GetAssemblyPath() + Globals.MailMergeDocumentsPath + Globals.F_UserOptionDoc2, Globals.et, eDocsType.eOption);
							edtuc.EventMergeDocument = em;
							edtuc.ControlTabIndex = iTabIndex;
							edtuc.Dock = DockStyle.Fill;				
							tp.Controls.Add (edtuc);
							iTabIndex++;
						}
					}
					else
					{						
						this.RemoveTabPageIfDeselected(Globals.UserOptionDoc2);					
					}
					#endregion � eDocsType.eOption �
					break;
			}
			this.tcDocs_SelectedIndexChanged(this.tcDocs, EventArgs.Empty);
		}

		private void miAddNewDoc_Click(object sender, System.EventArgs e)
		{
			TabPage tp = new TabPage("New Document");
			tp.Name = Globals.UDoc2;
			this.tcDocs.TabPages.Add(tp);
			EditDocumentTemplateUserControl edtuc = new EditDocumentTemplateUserControl();
			edtuc.ControlTabIndex = 0;
			edtuc.Dock = DockStyle.Fill;				
			tp.Controls.Add (edtuc);				
		}

		private void tcDocs_SelectedIndexChanged(object sender, System.EventArgs e)
		{	
			TabControl tc = (TabControl)sender;
			if (null != tc.Controls && 0 != this.tcDocs.Controls.Count)
			{
				TabPage tp = (TabPage)tc.Controls[this.tcDocs.SelectedIndex];
				EditDocumentTemplateUserControl educ  = (EditDocumentTemplateUserControl)tp.Controls[0];
			
				//++TA: Why events? txTextControld does not support merging when txTextControl when it�s not a.) loaded, b.) shown in GUI
				//fire event				
				if (null != educ && null != educ.EventMergeDocument)
					educ.EventMergeDocument.TriggerChange();							
			}
		}		
	}
}
