using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Printing;
using System.Data;
using System.Windows.Forms;
using System.Reflection;
using System.IO;

namespace GetFormsReady
{
	/// <summary>
	/// Summary description for EditDocumentTemplateUserControl.
	/// </summary>
	public class EditDocumentTemplateUserControl : System.Windows.Forms.UserControl
	{
		private EventMerge eventMerge = null;			
		private eDocsType docType = eDocsType.eP_S;
		private EventTrigger eventTrigger = null;
		private bool bAdded = false;
		private int tabIndex = 0;
		private string targetTemplate = string.Empty;
		private System.Windows.Forms.ContextMenuStrip ctxMenu;
		private System.Windows.Forms.ToolStripMenuItem menuItem1;
		private System.Windows.Forms.ToolStripMenuItem menuItem2;
		private System.Windows.Forms.ToolStripMenuItem menuItem3;
		private System.Windows.Forms.ToolStripMenuItem menuItem4;
		private System.Windows.Forms.ToolStripMenuItem menuItem5;
		private System.Windows.Forms.ToolStripMenuItem menuItem6;
		private System.Windows.Forms.ToolStripMenuItem menuItem7;
		
		private System.Windows.Forms.ToolStripMenuItem menuItem12;
		private System.Windows.Forms.ToolStripMenuItem menuItem13;
		private System.Windows.Forms.ToolStripMenuItem menuItem15;
		private System.Windows.Forms.ToolStripMenuItem menuItem16;
		private System.Windows.Forms.ToolStripMenuItem menuItem18;
		private TXTextControl.StatusBar txStatusBar1;
		private TXTextControl.ButtonBar txButtonBar1;
		private TXTextControl.RulerBar txRulerBar1;
		private TXTextControl.TextControl txTextControl1;
		private System.Windows.Forms.ToolStripMenuItem miCopy;
		private System.Windows.Forms.ToolStripMenuItem miPaste;
		private System.Windows.Forms.ToolStripMenuItem menuItem19;
		private System.Windows.Forms.ToolStripMenuItem miUserFirstName;
		private System.Windows.Forms.ToolStripMenuItem miUserLastName;
		private System.Windows.Forms.ToolStripMenuItem miUserCompanyName;
		private System.Windows.Forms.ToolStripMenuItem miUserStreetAddress;
		private System.Windows.Forms.ToolStripMenuItem miUserCity;
		private System.Windows.Forms.ToolStripMenuItem miUserState;
		private System.Windows.Forms.ToolStripMenuItem miUserZIP;
		private System.Windows.Forms.ToolStripMenuItem miUserPhone;
		private System.Windows.Forms.ToolStripMenuItem miUserFax;
        private ToolStripSeparator toolStripSeparator1;
        private ToolStripSeparator toolStripSeparator2;
        private ToolStripSeparator toolStripSeparator3;
        private ToolStripSeparator toolStripSeparator4;
        private ToolStripSeparator menuItem21;
        private ToolStripSeparator toolStripSeparator5;
        private ToolStripSeparator toolStripSeparator6;
        private IContainer components;

        public EventMerge EventMergeDocument
		{
			get { return this.eventMerge; }
			set 
			{
				this.eventMerge = value;
				this.eventMerge.MergeDocument += new TriggerMergeEventHandler(MergeFieldsTrigger);
			}
		}
		
		public EditDocumentTemplateUserControl()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();
			this.docType = Globals.docType;
			bool Landscape = (txTextControl1.PageSize.Width > txTextControl1.PageSize.Height);
			PaperSize ps = PageSetup.GetTxPaperSize(new Size((int)txTextControl1.PageSize.Width, (int)txTextControl1.PageSize.Height), Landscape);	
			
			this.SetPageMargins();					
		}
		public EditDocumentTemplateUserControl(string template, EventTrigger eventTrigger, eDocsType docType)
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

			this.targetTemplate = template;
			this.docType = docType;
			this.eventTrigger = eventTrigger;
			this.eventTrigger.ExecutedPrinting += new PrintDocEventHandler(PrintDocument);

			this.SetPageMargins();
		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		public int ControlTabIndex 
		{
			get { return this.tabIndex; }
			set { this.tabIndex = value; }
		}

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.ctxMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.menuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItem5 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItem12 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItem6 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItem7 = new System.Windows.Forms.ToolStripMenuItem();
            this.miCopy = new System.Windows.Forms.ToolStripMenuItem();
            this.miPaste = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItem15 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItem13 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItem16 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItem18 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItem19 = new System.Windows.Forms.ToolStripMenuItem();
            this.miUserFirstName = new System.Windows.Forms.ToolStripMenuItem();
            this.miUserLastName = new System.Windows.Forms.ToolStripMenuItem();
            this.miUserCompanyName = new System.Windows.Forms.ToolStripMenuItem();
            this.miUserStreetAddress = new System.Windows.Forms.ToolStripMenuItem();
            this.miUserCity = new System.Windows.Forms.ToolStripMenuItem();
            this.miUserState = new System.Windows.Forms.ToolStripMenuItem();
            this.miUserZIP = new System.Windows.Forms.ToolStripMenuItem();
            this.miUserPhone = new System.Windows.Forms.ToolStripMenuItem();
            this.miUserFax = new System.Windows.Forms.ToolStripMenuItem();
            this.txStatusBar1 = new TXTextControl.StatusBar();
            this.txButtonBar1 = new TXTextControl.ButtonBar();
            this.txRulerBar1 = new TXTextControl.RulerBar();
            this.txTextControl1 = new TXTextControl.TextControl();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.menuItem21 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.ctxMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // ctxMenu
            // 
            this.ctxMenu.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.ctxMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuItem1,
            this.toolStripSeparator1,
            this.menuItem2,
            this.menuItem3,
            this.toolStripSeparator2,
            this.menuItem4,
            this.menuItem5,
            this.toolStripSeparator3,
            this.menuItem12,
            this.menuItem6,
            this.menuItem7,
            this.toolStripSeparator4,
            this.miCopy,
            this.miPaste,
            this.menuItem21,
            this.menuItem15,
            this.menuItem13,
            this.toolStripSeparator5,
            this.menuItem16,
            this.menuItem18,
            this.toolStripSeparator6,
            this.menuItem19});
            this.ctxMenu.Name = "ctxMenu";
            this.ctxMenu.Size = new System.Drawing.Size(267, 464);
            this.ctxMenu.Opened += new System.EventHandler(this.ctxMenu_Popup);
            // 
            // menuItem1
            // 
            this.menuItem1.MergeIndex = 0;
            this.menuItem1.Name = "menuItem1";
            this.menuItem1.Size = new System.Drawing.Size(266, 26);
            this.menuItem1.Text = "New";
            this.menuItem1.Click += new System.EventHandler(this.menuItem1_Click);
            // 
            // menuItem2
            // 
            this.menuItem2.MergeIndex = 2;
            this.menuItem2.Name = "menuItem2";
            this.menuItem2.Size = new System.Drawing.Size(266, 26);
            this.menuItem2.Text = "Open File";
            this.menuItem2.Click += new System.EventHandler(this.menuItem2_Click);
            // 
            // menuItem3
            // 
            this.menuItem3.MergeIndex = 3;
            this.menuItem3.Name = "menuItem3";
            this.menuItem3.Size = new System.Drawing.Size(266, 26);
            this.menuItem3.Text = "Save File";
            this.menuItem3.Click += new System.EventHandler(this.menuItem3_Click);
            // 
            // menuItem4
            // 
            this.menuItem4.MergeIndex = 5;
            this.menuItem4.Name = "menuItem4";
            this.menuItem4.Size = new System.Drawing.Size(266, 26);
            this.menuItem4.Text = "Open Mail Merge Template";
            this.menuItem4.Click += new System.EventHandler(this.menuItem4_Click);
            // 
            // menuItem5
            // 
            this.menuItem5.MergeIndex = 6;
            this.menuItem5.Name = "menuItem5";
            this.menuItem5.Size = new System.Drawing.Size(266, 26);
            this.menuItem5.Text = "Save Mail Merge Template";
            this.menuItem5.Click += new System.EventHandler(this.menuItem5_Click);
            // 
            // menuItem12
            // 
            this.menuItem12.MergeIndex = 8;
            this.menuItem12.Name = "menuItem12";
            this.menuItem12.Size = new System.Drawing.Size(266, 26);
            this.menuItem12.Text = "Page Setup";
            this.menuItem12.Click += new System.EventHandler(this.menuItem12_Click);
            // 
            // menuItem6
            // 
            this.menuItem6.MergeIndex = 9;
            this.menuItem6.Name = "menuItem6";
            this.menuItem6.Size = new System.Drawing.Size(266, 26);
            this.menuItem6.Text = "Print";
            this.menuItem6.Click += new System.EventHandler(this.menuItem6_Click);
            // 
            // menuItem7
            // 
            this.menuItem7.MergeIndex = 10;
            this.menuItem7.Name = "menuItem7";
            this.menuItem7.Size = new System.Drawing.Size(266, 26);
            this.menuItem7.Text = "Print Preview";
            this.menuItem7.Click += new System.EventHandler(this.menuItem7_Click);
            // 
            // miCopy
            // 
            this.miCopy.MergeIndex = 12;
            this.miCopy.Name = "miCopy";
            this.miCopy.Size = new System.Drawing.Size(266, 26);
            this.miCopy.Text = "Copy";
            this.miCopy.Click += new System.EventHandler(this.miCopy_Click);
            // 
            // miPaste
            // 
            this.miPaste.MergeIndex = 13;
            this.miPaste.Name = "miPaste";
            this.miPaste.Size = new System.Drawing.Size(266, 26);
            this.miPaste.Text = "Paste";
            this.miPaste.Click += new System.EventHandler(this.miPaste_Click);
            // 
            // menuItem15
            // 
            this.menuItem15.MergeIndex = 15;
            this.menuItem15.Name = "menuItem15";
            this.menuItem15.Size = new System.Drawing.Size(266, 26);
            this.menuItem15.Text = "Redo";
            this.menuItem15.Click += new System.EventHandler(this.menuItem15_Click);
            // 
            // menuItem13
            // 
            this.menuItem13.MergeIndex = 16;
            this.menuItem13.Name = "menuItem13";
            this.menuItem13.Size = new System.Drawing.Size(266, 26);
            this.menuItem13.Text = "Undo";
            this.menuItem13.Click += new System.EventHandler(this.menuItem13_Click);
            // 
            // menuItem16
            // 
            this.menuItem16.MergeIndex = 18;
            this.menuItem16.Name = "menuItem16";
            this.menuItem16.Size = new System.Drawing.Size(266, 26);
            this.menuItem16.Text = "Merge";
            this.menuItem16.Click += new System.EventHandler(this.menuItem16_Click);
            // 
            // menuItem18
            // 
            this.menuItem18.MergeIndex = 19;
            this.menuItem18.Name = "menuItem18";
            this.menuItem18.Size = new System.Drawing.Size(266, 26);
            this.menuItem18.Text = "Switch To Template";
            this.menuItem18.Click += new System.EventHandler(this.menuItem18_Click);
            // 
            // menuItem19
            // 
            this.menuItem19.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miUserFirstName,
            this.miUserLastName,
            this.miUserCompanyName,
            this.miUserStreetAddress,
            this.miUserCity,
            this.miUserState,
            this.miUserZIP,
            this.miUserPhone,
            this.miUserFax});
            this.menuItem19.MergeIndex = 21;
            this.menuItem19.Name = "menuItem19";
            this.menuItem19.Size = new System.Drawing.Size(266, 26);
            this.menuItem19.Text = "User Info";
            // 
            // miUserFirstName
            // 
            this.miUserFirstName.MergeIndex = 0;
            this.miUserFirstName.Name = "miUserFirstName";
            this.miUserFirstName.Size = new System.Drawing.Size(224, 26);
            this.miUserFirstName.Text = "User First Name";
            this.miUserFirstName.Click += new System.EventHandler(this.Fill);
            // 
            // miUserLastName
            // 
            this.miUserLastName.MergeIndex = 1;
            this.miUserLastName.Name = "miUserLastName";
            this.miUserLastName.Size = new System.Drawing.Size(224, 26);
            this.miUserLastName.Text = "User Last Name";
            this.miUserLastName.Click += new System.EventHandler(this.Fill);
            // 
            // miUserCompanyName
            // 
            this.miUserCompanyName.MergeIndex = 2;
            this.miUserCompanyName.Name = "miUserCompanyName";
            this.miUserCompanyName.Size = new System.Drawing.Size(224, 26);
            this.miUserCompanyName.Text = "User Company Name";
            this.miUserCompanyName.Click += new System.EventHandler(this.Fill);
            // 
            // miUserStreetAddress
            // 
            this.miUserStreetAddress.MergeIndex = 3;
            this.miUserStreetAddress.Name = "miUserStreetAddress";
            this.miUserStreetAddress.Size = new System.Drawing.Size(224, 26);
            this.miUserStreetAddress.Text = "User Street Address";
            this.miUserStreetAddress.Click += new System.EventHandler(this.Fill);
            // 
            // miUserCity
            // 
            this.miUserCity.MergeIndex = 4;
            this.miUserCity.Name = "miUserCity";
            this.miUserCity.Size = new System.Drawing.Size(224, 26);
            this.miUserCity.Text = "User City";
            this.miUserCity.Click += new System.EventHandler(this.Fill);
            // 
            // miUserState
            // 
            this.miUserState.MergeIndex = 5;
            this.miUserState.Name = "miUserState";
            this.miUserState.Size = new System.Drawing.Size(224, 26);
            this.miUserState.Text = "User State";
            this.miUserState.Click += new System.EventHandler(this.Fill);
            // 
            // miUserZIP
            // 
            this.miUserZIP.MergeIndex = 6;
            this.miUserZIP.Name = "miUserZIP";
            this.miUserZIP.Size = new System.Drawing.Size(224, 26);
            this.miUserZIP.Text = "User ZIP";
            this.miUserZIP.Click += new System.EventHandler(this.Fill);
            // 
            // miUserPhone
            // 
            this.miUserPhone.MergeIndex = 7;
            this.miUserPhone.Name = "miUserPhone";
            this.miUserPhone.Size = new System.Drawing.Size(224, 26);
            this.miUserPhone.Text = "User Phone";
            this.miUserPhone.Click += new System.EventHandler(this.Fill);
            // 
            // miUserFax
            // 
            this.miUserFax.MergeIndex = 8;
            this.miUserFax.Name = "miUserFax";
            this.miUserFax.Size = new System.Drawing.Size(224, 26);
            this.miUserFax.Text = "User Fax";
            this.miUserFax.Click += new System.EventHandler(this.Fill);
            // 
            // txStatusBar1
            // 
            this.txStatusBar1.BackColor = System.Drawing.SystemColors.Control;
            this.txStatusBar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.txStatusBar1.Location = new System.Drawing.Point(0, 523);
            this.txStatusBar1.Name = "txStatusBar1";
            this.txStatusBar1.Size = new System.Drawing.Size(696, 22);
            this.txStatusBar1.TabIndex = 0;
            // 
            // txButtonBar1
            // 
            this.txButtonBar1.BackColor = System.Drawing.SystemColors.Control;
            this.txButtonBar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.txButtonBar1.Location = new System.Drawing.Point(0, 0);
            this.txButtonBar1.Name = "txButtonBar1";
            this.txButtonBar1.Size = new System.Drawing.Size(696, 28);
            this.txButtonBar1.TabIndex = 1;
            this.txButtonBar1.Text = "buttonBar1";
            // 
            // txRulerBar1
            // 
            this.txRulerBar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.txRulerBar1.Location = new System.Drawing.Point(0, 28);
            this.txRulerBar1.Name = "txRulerBar1";
            this.txRulerBar1.Size = new System.Drawing.Size(696, 25);
            this.txRulerBar1.TabIndex = 2;
            this.txRulerBar1.Text = "rulerBar1";
            // 
            // txTextControl1
            // 
            this.txTextControl1.ContextMenuStrip = this.ctxMenu;
            this.txTextControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txTextControl1.Font = new System.Drawing.Font("Arial", 10F);
            this.txTextControl1.Location = new System.Drawing.Point(0, 53);
            this.txTextControl1.Name = "txTextControl1";
            this.txTextControl1.Size = new System.Drawing.Size(696, 470);
            this.txTextControl1.TabIndex = 3;
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(263, 6);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(263, 6);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(263, 6);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(263, 6);
            // 
            // menuItem21
            // 
            this.menuItem21.MergeIndex = 14;
            this.menuItem21.Name = "menuItem21";
            this.menuItem21.Size = new System.Drawing.Size(263, 6);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(263, 6);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(263, 6);
            // 
            // EditDocumentTemplateUserControl
            // 
            this.Controls.Add(this.txTextControl1);
            this.Controls.Add(this.txRulerBar1);
            this.Controls.Add(this.txButtonBar1);
            this.Controls.Add(this.txStatusBar1);
            this.Name = "EditDocumentTemplateUserControl";
            this.Size = new System.Drawing.Size(696, 545);
            this.Load += new System.EventHandler(this.EditDocumentTemplateUserControl_Load);
            this.Leave += new System.EventHandler(this.EditDocumentTemplateUserControl_Leave);
            this.ctxMenu.ResumeLayout(false);
            this.ResumeLayout(false);

		}
		#endregion
		
		private void SetPageMargins()
		{
			this.txTextControl1.PageMargins.Left	= 50;
			this.txTextControl1.PageMargins.Right	= 50;
			this.txTextControl1.PageMargins.Top		= 60;
			this.txTextControl1.PageMargins.Bottom	= 60;
		}
	
		// This will be called whenever user clicks on Print !
		private void PrintDocument(object sender, EventArgs e) 
		{
			try
			{				
				//select control
				TabPage tp = (TabPage)this.Parent;
				TabControl tc = (TabControl)tp.Parent;                
				tc.SelectedIndex = this.ControlTabIndex;

				//merge fields
				this.MergeFields();

				//print it
				PrintDialog myPrintDialog = new PrintDialog();
				PrintDocument myPrintDocument = new PrintDocument();
				
				myPrintDocument.DocumentName = this.targetTemplate;

				myPrintDocument.PrinterSettings.FromPage	= 1;
				myPrintDocument.PrinterSettings.ToPage		= this.txTextControl1.Pages;				
				myPrintDocument.PrinterSettings.Copies		= ApplicationHelper.GetNumberOfCopiesToPrint(this.targetTemplate, this.docType);
				
				for (int i = 0; i < myPrintDocument.PrinterSettings.Copies; i++)
				{
					this.txTextControl1.Print(myPrintDocument);				
				}
			}
			catch (Exception exc)
			{
				MessageBox.Show(exc.Message);
			}
		}

		private void menuItem1_Click(object sender, System.EventArgs e)
		{
			if (false == txTextControl1.Focused)
				txTextControl1.Focus();

			txTextControl1.SelectAll();
			txTextControl1.Clear();
		}

		private void menuItem2_Click(object sender, System.EventArgs e)
		{
			try
			{
				txTextControl1.Load();				
			}
			catch (Exception exc)
			{
				MessageBox.Show(exc.Message);
				return;
			}
		}

		public void LoadDoc (string doc)
		{
			try
			{
				txTextControl1.Load(doc, TXTextControl.StreamType.InternalFormat);
				this.MergeFields();
			}
			catch (Exception exc)
			{
				MessageBox.Show(exc.Message + " " + exc.Source);
			}
		}

		private void menuItem3_Click(object sender, System.EventArgs e)
		{						
			try
			{
				System.Windows.Forms.SaveFileDialog dlgSaveFile = new SaveFileDialog();
                dlgSaveFile.Filter = "Text Control Files (*.rtf)|*.rtf|MS Word (*.doc)|*.doc|PDF files (*.pdf)|*.pdf";
                if (DialogResult.OK == dlgSaveFile.ShowDialog())
                {
                    if (dlgSaveFile.FileName != string.Empty)
                    {
                        string ext = Path.GetExtension(dlgSaveFile.FileName);

                        if (".pdf" == ext)
                            txTextControl1.Save(dlgSaveFile.FileName, TXTextControl.StreamType.AdobePDF);
                        else if (".doc" == ext)
                            txTextControl1.Save(dlgSaveFile.FileName, TXTextControl.StreamType.MSWord);
                        else
                            txTextControl1.Save(dlgSaveFile.FileName, TXTextControl.StreamType.RichTextFormat);


                        TabPage tp = (TabPage)this.Parent;
                        tp.Text = dlgSaveFile.FileName;
                    }
                }
			}
			catch (Exception exc)
			{
				MessageBox.Show(exc.ToString());
			}
		}

		private void menuItem4_Click(object sender, System.EventArgs e)
		{
			try
			{
				OpenFileDialog dlgOpenFile = new OpenFileDialog();
				dlgOpenFile.Filter = "Text Control Files (*.tx)|*.tx";			
				dlgOpenFile.ShowDialog();
				if (dlgOpenFile.FileName != "") 
					txTextControl1.Load(dlgOpenFile.FileName, TXTextControl.StreamType.InternalFormat);
			}
			catch (Exception exc)
			{
				MessageBox.Show(exc.ToString());
			}	
		}

		private void menuItem5_Click(object sender, System.EventArgs e)
		{
			string path = string.Empty;
			switch (docType)
			{
				case eDocsType.eP_S:
					path = ApplicationHelper.GetAssemblyPath() + Globals.MailMergeDocumentsPath + Globals.PS + Globals.UserDocuments;
					break;

				case eDocsType.eSub2:
					path = ApplicationHelper.GetAssemblyPath() + Globals.MailMergeDocumentsPath + Globals.Sub2 + Globals.UserDocuments;
					break;

				case eDocsType.eOption:
					path = ApplicationHelper.GetAssemblyPath() + Globals.MailMergeDocumentsPath + Globals.Option + Globals.UserDocuments;
					break;
			}

			try
			{
				SaveFileDialog dlgSaveFile = new SaveFileDialog();
				dlgSaveFile.Filter = "Text Control Files (*.tx)|*.tx";
				try
				{
					if (!Directory.Exists(path))
						Directory.CreateDirectory(path);

					dlgSaveFile.InitialDirectory = path;
				}
				catch {}

				dlgSaveFile.ShowDialog();
				if (dlgSaveFile.FileName != "") 
					txTextControl1.Save(dlgSaveFile.FileName, TXTextControl.StreamType.InternalFormat);				
			}
			catch (Exception exc)
			{
				MessageBox.Show(exc.ToString());
			}
		}

		private void menuItem6_Click(object sender, System.EventArgs e)
		{
			PrintDialog myPrintDialog = new PrintDialog();
			PrintDocument myPrintDocument = new PrintDocument();

			myPrintDialog.Document = myPrintDocument;
			myPrintDialog.AllowSomePages = false;
			myPrintDialog.AllowPrintToFile = false;
			myPrintDialog.PrinterSettings.FromPage = 1;
			myPrintDialog.PrinterSettings.ToPage = txTextControl1.Pages;

			if (myPrintDialog.ShowDialog() == DialogResult.OK )
			{
				txTextControl1.Print(myPrintDocument);
			}
		}

		private void menuItem7_Click(object sender, System.EventArgs e)
		{
			PrintDocument pd = new PrintDocument();			
			txTextControl1.PrintPreview(pd);
		}

		
		private Hashtable BuildMenuItemsFromObject ()
		{
			ArrayList alMenuItems = new ArrayList();
			Hashtable htMenuItems = new Hashtable();
			MainForm mf = Globals.mf;
			Type t = mf.GetType();			
			PropertyInfo[] pi =  t.GetProperties();

			
			for (int i = 0; i < pi.Length; i++)
			{
				object[] descAttr = pi[i].GetCustomAttributes(typeof(DescriptionAttribute), true);
				object[] groupAttr = pi[i].GetCustomAttributes(typeof(GroupAttribute), true);
				object[] groupPosAttr = pi[i].GetCustomAttributes(typeof(GroupPositionAttribute), true);
				object[] itemPosAttr = pi[i].GetCustomAttributes(typeof(ItemPositionAttribute), true);
		
				DescriptionAttribute da = null;
				GroupAttribute ga = null;
				GroupPositionAttribute gap = null;
				ItemPositionAttribute ipa = null;

				if (0 != descAttr.Length && 0 != groupAttr.Length && 0 != groupPosAttr.Length && 0 != itemPosAttr.Length)
				{   
					da = (DescriptionAttribute)descAttr[0];
					ga = (GroupAttribute)groupAttr[0];
					gap = (GroupPositionAttribute)groupPosAttr[0];
					ipa = (ItemPositionAttribute)itemPosAttr[0];

					switch (this.docType) 
					{
								
						case eDocsType.eP_S:							
							if (true == ApplicationHelper.FindInStringArray(Globals.a_eP_S, (string)ga.Desc))
							{
								//if (false == htMenuItems.Contains(da.Desc))
								//	htMenuItems.Add(da.Desc, ga.Desc);
								MenuItemObjectDescriptor miod = new MenuItemObjectDescriptor(ga.Desc, da.Desc, gap.Desc, ipa.Desc);
								alMenuItems.Add(miod);
							}
							continue;						

						case eDocsType.eSub2:
							if (true == ApplicationHelper.FindInStringArray(Globals.a_eSub2, (string)ga.Desc))
							{
								//if (false == htMenuItems.Contains(da.Desc))
									//htMenuItems.Add(da.Desc, ga.Desc);
								MenuItemObjectDescriptor miod = new MenuItemObjectDescriptor(ga.Desc, da.Desc, gap.Desc, ipa.Desc);
								alMenuItems.Add(miod);
							}
							continue;						
						case eDocsType.eOption:
							if (true == ApplicationHelper.FindInStringArray(Globals.a_eOption, (string)ga.Desc))
							{
								//if (false == htMenuItems.Contains(da.Desc))
								//htMenuItems.Add(da.Desc, ga.Desc);
								MenuItemObjectDescriptor miod = new MenuItemObjectDescriptor(ga.Desc, da.Desc, gap.Desc, ipa.Desc);
								alMenuItems.Add(miod);
							}
							continue;
					}							
				}
			}
			
			ArrayList sellerInfo	= new ArrayList();
			ArrayList propertyInfo	= new ArrayList();
			ArrayList buyerInfo		= new ArrayList();
			ArrayList faxToInfo		= new ArrayList();
			ArrayList trustInfo		= new ArrayList();
			ArrayList financials	= new ArrayList();
			ArrayList closing		= new ArrayList();
			ArrayList otherTerms	= new ArrayList();
			ArrayList insurance_HOA	= new ArrayList();
			ArrayList acquisitionInfo = new ArrayList();
			Hashtable retTable = new Hashtable();

			alMenuItems.Sort(new MenuItemObjectComparer());
			IEnumerator ie = alMenuItems.GetEnumerator();
			
			switch (Globals.docType)
			{
				case eDocsType.eP_S:
				{
					//we take only: SellerInfo, PropertyInfo, BuyerInfo, FaxToInfo, TrustInfo, Financials, Closing, OtherTerms.
					
					while (ie.MoveNext())
					{
						MenuItemObjectDescriptor miod = (MenuItemObjectDescriptor)ie.Current;

						if (miod.Group == Globals.SellerInfo)
						{						
							sellerInfo.Add(miod.Description);
							continue;
						}
						if (miod.Group == Globals.PropertyInfo)
						{						
							propertyInfo.Add(miod.Description);
							continue;
						}
						if (miod.Group == Globals.BuyerInfo)
						{						
							buyerInfo.Add(miod.Description);
							continue;
						}
						if (miod.Group == Globals.FaxToInfo)
						{						
							faxToInfo.Add(miod.Description);
							continue;
						}
						if (miod.Group == Globals.TrustInfo)
						{						
							trustInfo.Add(miod.Description);
							continue;
						}
						if (miod.Group == Globals.Financials)
						{						
							financials.Add(miod.Description);
							continue;
						}
						if (miod.Group == Globals.Closing)
						{						
							closing.Add(miod.Description);
							continue;
						}
						if (miod.Group == Globals.OtherTerms)
						{						
							otherTerms.Add(miod.Description);
							continue;
						}
					}
					
					retTable.Add(Globals.SellerInfo, sellerInfo.ToArray());
					retTable.Add(Globals.BuyerInfo, buyerInfo.ToArray());
					retTable.Add(Globals.PropertyInfo, propertyInfo.ToArray());
					retTable.Add(Globals.TrustInfo, trustInfo.ToArray());										
					retTable.Add(Globals.Financials, financials.ToArray());
					retTable.Add(Globals.Closing, closing.ToArray());
					retTable.Add(Globals.FaxToInfo, faxToInfo.ToArray());
					retTable.Add(Globals.OtherTerms, otherTerms.ToArray()); 
					return retTable;
				}
				case eDocsType.eSub2:
					//we take only: SUB_SellerInfo, SUB_PropertyInfo, SUB_BuyerInfo, SUB_TrustInfo, SUB_Financials, SUB_InsuranceHOA, SUB_OtherTerms.					
					while (ie.MoveNext())
					{
						MenuItemObjectDescriptor miod = (MenuItemObjectDescriptor)ie.Current;
						if (miod.Group == Globals.SUB_SellerInfo)
						{						
							sellerInfo.Add(miod.Description);
							continue;
						}
						if (miod.Group == Globals.SUB_PropertyInfo)
						{						
							propertyInfo.Add(miod.Description);
							continue;
						}
						if (miod.Group == Globals.SUB_BuyerInfo)
						{						
							buyerInfo.Add(miod.Description);
							continue;
						}
						if (miod.Group == Globals.SUB_TrustInfo)
						{						
							trustInfo.Add(miod.Description);
							continue;
						}
						if (miod.Group == Globals.SUB_Financials)
						{						
							financials.Add(miod.Description);
							continue;
						}
						if (miod.Group == Globals.SUB_InsuranceHOA)
						{						
							insurance_HOA.Add(miod.Description);
							continue;
						}
						if (miod.Group == Globals.SUB_OtherTerms)
						{						
							otherTerms.Add(miod.Description);
							continue;
						}
					}					
					retTable.Add(Globals.SUB_SellerInfo, sellerInfo.ToArray());
					retTable.Add(Globals.SUB_PropertyInfo, propertyInfo.ToArray());
					retTable.Add(Globals.SUB_BuyerInfo, buyerInfo.ToArray());					
					retTable.Add(Globals.SUB_TrustInfo, trustInfo.ToArray());
					retTable.Add(Globals.SUB_Financials, financials.ToArray());
					retTable.Add(Globals.SUB_InsuranceHOA, insurance_HOA.ToArray());
					retTable.Add(Globals.SUB_OtherTerms, otherTerms.ToArray()); 
					return retTable;

				case eDocsType.eOption:
					while (ie.MoveNext())
					{
						MenuItemObjectDescriptor miod = (MenuItemObjectDescriptor)ie.Current;
						if (miod.Group == Globals.Option_SellerInfo)
						{						
							sellerInfo.Add(miod.Description);
							continue;
						}
						if (miod.Group == Globals.Option_PropertyInfo)
						{						
							propertyInfo.Add(miod.Description);
							continue;
						}
						if (miod.Group == Globals.Option_BuyerInfo)
						{						
							buyerInfo.Add(miod.Description);
							continue;
						}
						if (miod.Group == Globals.Option_TrustInfo)
						{						
							trustInfo.Add(miod.Description);
							continue;
						}
						if (miod.Group == Globals.Option_AcquisitionInfo)
						{						
							acquisitionInfo.Add(miod.Description);
							continue;
						}
						if (miod.Group == Globals.Option_OtherTerms)
						{						
							otherTerms.Add(miod.Description);
							continue;
						}
					}					
					retTable.Add(Globals.Option_SellerInfo, sellerInfo.ToArray());
					retTable.Add(Globals.Option_PropertyInfo, propertyInfo.ToArray());
					retTable.Add(Globals.Option_BuyerInfo, buyerInfo.ToArray());					
					retTable.Add(Globals.Option_TrustInfo, trustInfo.ToArray());
					retTable.Add(Globals.Option_AcquisitionInfo, acquisitionInfo.ToArray());					
					retTable.Add(Globals.Option_OtherTerms, otherTerms.ToArray()); 
					return retTable;

					
			}
			return null;

		}

		private void ctxMenu_Popup(object sender, System.EventArgs e)
		{
			if (false == bAdded)
			{
				Hashtable table = this.BuildMenuItemsFromObject();
				if (null != table)
				{
					IDictionaryEnumerator ide = table.GetEnumerator();
                    ToolStripMenuItem[] mi = null;
					while (ide.MoveNext())
					{
						string miDesc = (string)ide.Key;
						Array a = (Array)ide.Value;
						mi = new ToolStripMenuItem[a.Length];
						for (int i = 0; i < a.Length; i++)
						{
							mi[i] = new ToolStripMenuItem((string)a.GetValue(i));
							mi[i].Click += new System.EventHandler(this.Fill);
						}
						//ctxMenu.Items.Add((string)ide.Key, mi);
                        ToolStripMenuItem tsmi = new ToolStripMenuItem((string)ide.Key);
                        tsmi.DropDownItems.AddRange(mi);
                        ctxMenu.Items.Add(tsmi);
                    }
					this.bAdded = true;
				}
			}								
		}

		private void Fill(object sender, System.EventArgs e)
		{

			TXTextControl.TextField textField = new TXTextControl.TextField();

			textField.Text = "(" + ((ToolStripMenuItem)sender).Text + ")";
			textField.Name = ((ToolStripMenuItem)sender).Text;
			textField.ShowActivated = true;
			textField.DoubledInputPosition = true;
			txTextControl1.TextFields.Add(textField);
		}		

		private void EditDocumentTemplateUserControl_Load(object sender, System.EventArgs e)
		{
			txTextControl1.ButtonBar = txButtonBar1;
			txTextControl1.RulerBar = txRulerBar1;
			txTextControl1.StatusBar = txStatusBar1;			
			
			bool Landscape = (txTextControl1.PageSize.Width > txTextControl1.PageSize.Height);
			PaperSize ps = PageSetup.GetTxPaperSize(new Size((int)txTextControl1.PageSize.Width, (int)txTextControl1.PageSize.Height), Landscape);

			this.LoadDoc(this.targetTemplate);
		}

		private void menuItem12_Click(object sender, System.EventArgs e)
		{
			PageSetup ps = new PageSetup();
			ps.ShowDialog(txTextControl1);
		}

		private void EditDocumentTemplateUserControl_Leave(object sender, System.EventArgs e)
		{

		}

		private void menuItem13_Click(object sender, System.EventArgs e)
		{
			this.txTextControl1.Undo();
		}

		private void menuItem15_Click(object sender, System.EventArgs e)
		{
			this.txTextControl1.Redo();
		}

		private void menuItem16_Click(object sender, System.EventArgs e)
		{			
				this.MergeFields();	
		}
		
		private void MergeFieldsTrigger(object sender, System.EventArgs e)
		{
			this.MergeFields();
		}
		
		private void MergeFields ()
		{
			try
			{
				MainForm mf = Globals.mf;
				Type t = mf.GetType();			
				PropertyInfo[] pi =  t.GetProperties();			

				foreach (TXTextControl.TextField Field in txTextControl1.TextFields) 
				{
				
					for (int i = 0; i < pi.Length; i++)
					{
						object[] descAttr = pi[i].GetCustomAttributes(typeof(DescriptionAttribute), true);
						object[] groupAttr = pi[i].GetCustomAttributes(typeof(GroupAttribute), true);
				
						DescriptionAttribute da = null;
						GroupAttribute ga = null;

						if (0 != descAttr.Length && 0 != groupAttr.Length)
						{   
							da = (DescriptionAttribute)descAttr[0];
							ga = (GroupAttribute)groupAttr[0];
						
							if (this.docType == eDocsType.eP_S)
							{		
								if (true == ApplicationHelper.FindInStringArray(Globals.a_eP_S, ga.Desc))
								{
									if (Field.Name == da.Desc)
									{
										Field.Text = (string)pi[i].GetValue(mf, null);
									}
								}
							}
							if (this.docType == eDocsType.eSub2)
							{		
								if (true == ApplicationHelper.FindInStringArray(Globals.a_eSub2, ga.Desc))
								{
									if (Field.Name == da.Desc)
									{
										Field.Text = (string)pi[i].GetValue(mf, null);
									}
								}
							}
							if (this.docType == eDocsType.eOption)
							{		
								if (true == ApplicationHelper.FindInStringArray(Globals.a_eOption, ga.Desc))
								{
									if (Field.Name == da.Desc)
									{
										Field.Text = (string)pi[i].GetValue(mf, null);
									}
								}
							}
						}
					}								
				}
			}
			catch (Exception)
			{
				//a real empty catch, because of txTextControl - 
				//when trying to merge a not fully loaded txTextControl - it crashes.
				//this catch is to prevent it.
			}
		}

		private void menuItem18_Click(object sender, System.EventArgs e)
		{
			foreach (TXTextControl.TextField Field in txTextControl1.TextFields) 
			{
				Field.Text = "(" + Field.Name + ")";				
			}		
		}

		private void miCopy_Click(object sender, System.EventArgs e)
		{
			this.txTextControl1.Copy();					
		}

		private void miPaste_Click(object sender, System.EventArgs e)
		{
			this.txTextControl1.Paste();					
		}
	}
}
