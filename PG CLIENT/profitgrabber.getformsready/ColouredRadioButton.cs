using System;

namespace GetFormsReady
{
	/// <summary>
	/// Summary description for ColouredRadioBox.
	/// </summary>
	public class ColouredRadioButton : System.Windows.Forms.RadioButton
	{
		public ColouredRadioButton()
		{
			this.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
		}
	}
}
