using System;

namespace GetFormsReady
{
	/// <summary>
	/// Summary description for ColouredCheckBox.
	/// </summary>
	public class ColouredCheckBox : System.Windows.Forms.CheckBox
	{
		public ColouredCheckBox()
		{
			this.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
		}
	}
}
