using System;
using System.Windows.Forms;
using System.Drawing;
using System.Globalization;

namespace GetFormsReady
{
	/// <summary>
	/// CurrencyTextBox Class formats text in TextBox using regional Settings (i.e. 1 --> � 1,00 for my current regional Settings)
	/// </summary>
	public class CurrencyTextBox : System.Windows.Forms.TextBox
	{						
		public CurrencyTextBox()
		{
			this.Leave += new System.EventHandler(this.textBoxLeave);
			this.Enter += new System.EventHandler(this.textBoxEnter);
		}

		private void textBoxLeave(object sender, System.EventArgs e)
		{			
			if (false == this.ReadOnly)
			{
				this.BackColor = Color.White;
			}
			try
			{
				if (this.Text == "N/A" || this.Text == "N / A" || this.Text == "N/ A" || this.Text == "N /A")
					return;
				decimal newDecValue = Decimal.Parse(this.Text, NumberStyles.Currency);
				string newValue = newDecValue.ToString("c");
				this.Text = newValue;
			}
			catch (Exception)
			{
				this.Text = string.Empty;
			}
		}

		private void textBoxEnter(object sender, System.EventArgs e)
		{
			if (false == this.ReadOnly)
			{
				this.BackColor = Color.LightBlue;
			}
		}

	}
}
