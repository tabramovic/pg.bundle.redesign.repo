using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.IO;

namespace GetFormsReady
{
	/// <summary>
	/// Summary description for LoadExtDoc.
	/// </summary>
	public class LoadExtDoc : System.Windows.Forms.Form
	{
		bool _validSelection = false;
		bool _validExit = false;
		string _path = string.Empty;

		private System.Windows.Forms.ListView _lvDocs;
		private System.Windows.Forms.Button _Cancel;
		private System.Windows.Forms.Button _OK;
		private System.Windows.Forms.ColumnHeader columnHeader1;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public LoadExtDoc(eDocsType docType, string[] selectedTemplates)
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			switch (docType)
			{
				case eDocsType.eP_S:
					_path = ApplicationHelper.GetAssemblyPath() + Globals.MailMergeDocumentsPath + Globals.PS + Globals.UserDocuments;
					break;

				case eDocsType.eSub2:
					_path = ApplicationHelper.GetAssemblyPath() + Globals.MailMergeDocumentsPath + Globals.Sub2 + Globals.UserDocuments;
					break;

				case eDocsType.eOption:
					_path = ApplicationHelper.GetAssemblyPath() + Globals.MailMergeDocumentsPath + Globals.Option + Globals.UserDocuments;
					break;
			}
			
			_Cancel.Click += new EventHandler(OnCancel_Click);
			_OK.Click += new EventHandler(OnOK_Click);

			ScanForTemplates(selectedTemplates);

		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this._lvDocs = new System.Windows.Forms.ListView();
			this.columnHeader1 = new System.Windows.Forms.ColumnHeader();
			this._Cancel = new System.Windows.Forms.Button();
			this._OK = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// _lvDocs
			// 
			this._lvDocs.CheckBoxes = true;
			this._lvDocs.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
																					  this.columnHeader1});
			this._lvDocs.FullRowSelect = true;
			this._lvDocs.GridLines = true;
			this._lvDocs.Location = new System.Drawing.Point(24, 24);
			this._lvDocs.MultiSelect = false;
			this._lvDocs.Name = "_lvDocs";
			this._lvDocs.Size = new System.Drawing.Size(680, 304);
			this._lvDocs.TabIndex = 0;
			this._lvDocs.View = System.Windows.Forms.View.Details;
			// 
			// columnHeader1
			// 
			this.columnHeader1.Text = "Document Templates";
			this.columnHeader1.Width = 665;
			// 
			// _Cancel
			// 
			this._Cancel.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this._Cancel.Location = new System.Drawing.Point(632, 352);
			this._Cancel.Name = "_Cancel";
			this._Cancel.TabIndex = 1;
			this._Cancel.Text = "Cancel";
			// 
			// _OK
			// 
			this._OK.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this._OK.Location = new System.Drawing.Point(536, 352);
			this._OK.Name = "_OK";
			this._OK.TabIndex = 2;
			this._OK.Text = "OK";
			// 
			// LoadExtDoc
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(210)), ((System.Byte)(216)), ((System.Byte)(240)));
			this.ClientSize = new System.Drawing.Size(736, 398);
			this.Controls.Add(this._OK);
			this.Controls.Add(this._Cancel);
			this.Controls.Add(this._lvDocs);
			this.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(47)), ((System.Byte)(65)), ((System.Byte)(131)));
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
			this.Name = "LoadExtDoc";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Load External Document";
			this.TopMost = true;
			this.ResumeLayout(false);

		}
		#endregion

		protected override void OnClosing(CancelEventArgs e)
		{
			if (!_validExit)
				e.Cancel = true;

			base.OnClosing (e);
		}

		
		public bool ValidSelection
		{
			get {return _validSelection;}
		}

		
		public bool ValidExit
		{
			get { return _validExit; }
		}

		
		public string[] SelectedTemplates
		{
			get 
			{
				ArrayList templates = new ArrayList();
				if (null != _lvDocs.Items)
				{
					foreach (ListViewItem lvi in _lvDocs.Items)
					{
						if (lvi.Checked)
							templates.Add((string)lvi.Tag);
					}					
				}
				return (string[])templates.ToArray(typeof(string));
			}
		}

		public string[] Templates
		{
			get 
			{
				ArrayList templates = new ArrayList();
				if (null != _lvDocs.Items)
				{
					foreach (ListViewItem lvi in _lvDocs.Items)
					{						
							templates.Add((string)lvi.Tag);
					}					
				}
				return (string[])templates.ToArray(typeof(string));
			}
		}

		
		void ScanForTemplates(string[] selectedTemplates)
		{
			try
			{
				_lvDocs.Items.Clear();
				DirectoryInfo di = new DirectoryInfo(_path);

				foreach (FileInfo fi in di.GetFiles("*.tx"))
				{
					ListViewItem lvi = new ListViewItem(fi.Name);
					lvi.Tag = (string)fi.FullName;
					if (null != selectedTemplates)
					{
						for (int i = 0; i < selectedTemplates.Length; i++)
						{
							string templ = (string)selectedTemplates[i];
							if (templ == fi.FullName)
							{
								lvi.Checked = true;
								break;
							}
						}
					}
					_lvDocs.Items.Add(lvi);
				}
			}
			catch {	}
		}


		private void OnCancel_Click(object sender, EventArgs e)
		{
			_validExit = true;
			this.Close();
		}

		private void OnOK_Click(object sender, EventArgs e)
		{
			_validExit = true;
			_validSelection = true;
			this.Close();
		}
	}
}
