using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;

namespace GetFormsReady
{
	/// <summary>
	/// Summary description for ReadFirstUserControl.
	/// </summary>
	public class ReadFirstUserControl : System.Windows.Forms.UserControl
	{		
		private System.Windows.Forms.PictureBox pbImage;
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public ReadFirstUserControl()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

			// TODO: Add any initialization after the InitializeComponent call

		}

		public string UserDefinedText
		{
			get 
			{ 				
				return (string)this.Tag;
			}
			set 
			{ 
				this.Tag = value; 
			}
		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(ReadFirstUserControl));
			this.pbImage = new System.Windows.Forms.PictureBox();
			this.SuspendLayout();
			// 
			// pbImage
			// 
			this.pbImage.Image = ((System.Drawing.Image)(resources.GetObject("pbImage.Image")));
			this.pbImage.Location = new System.Drawing.Point(0, 0);
			this.pbImage.Name = "pbImage";
			this.pbImage.Size = new System.Drawing.Size(24, 24);
			this.pbImage.TabIndex = 0;
			this.pbImage.TabStop = false;
			// 
			// ReadFirstUserControl
			// 
			this.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(210)), ((System.Byte)(216)), ((System.Byte)(240)));
			this.Controls.Add(this.pbImage);
			this.Name = "ReadFirstUserControl";
			this.Size = new System.Drawing.Size(20, 20);
			this.ResumeLayout(false);

		}
		#endregion
	}
}
