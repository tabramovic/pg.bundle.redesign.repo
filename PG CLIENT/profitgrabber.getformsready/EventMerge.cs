using System; 

namespace GetFormsReady
{
	public delegate void TriggerMergeEventHandler(object sender, EventArgs e);

	public class EventMerge
	{		
		public event TriggerMergeEventHandler MergeDocument;
		
		protected virtual void OnDataChange(EventArgs e) 
		{
			if (MergeDocument != null)
				MergeDocument(this, e);
		}
		public void TriggerChange ()
		{
			OnDataChange(EventArgs.Empty);
		}
		public EventMerge()
		{
		}				
	}
}
