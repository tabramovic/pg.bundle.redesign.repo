﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InfusionSupportServiceAPI.Models
{    
    public class UDSEntry
    {   
        public Guid IDInfusion { get; set; }
    
        public Guid IDUser { get; set; }
    
        public int? IDInfusionContact { get; set; }
    }
}