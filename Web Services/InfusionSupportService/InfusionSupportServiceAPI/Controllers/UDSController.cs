﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using InfusionSupportServiceAPI.Models;
using InfusionSupportServiceAPI.DALC;


namespace InfusionSupportServiceAPI.Controllers
{
    public class UDSController : ApiController
    {
        static readonly log4net.ILog Applog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
            
        [HttpGet]
        [Route("api/UDS/TestIfAlive")]
        public string TestIfAlive()
        {
            return $"Alive at {DateTime.Now.ToLongDateString()} {DateTime.Now.ToLongTimeString()}";
        }

        [HttpGet]
        [Route("api/UDS/TestDBConnection")]
        public string TestDBConnection()
        {
            return UDSAdapter.Instance.TestConnection().ToString();
        }

        [HttpPost]
        [Route("api/UDS/TestPost/{id}")]
        public string TestPost(int id)
        {
            return id.ToString();
        }

        [HttpPost]
        [Route("api/UDS/GetEntries_pgId/{pgId}")]
        public List<UDSEntry> GetEntries_pgId(Guid pgId)
        {
            return UDSAdapter.Instance.GetEntries_pgId(pgId);
        }

        [HttpPost]
        [Route("api/UDS/GetEntries_infusionId/{idInfusionContact}")]
        public List<UDSEntry> GetEntries_infusionId(int idInfusionContact)
        {
            return UDSAdapter.Instance.GetEntries_infusionId(idInfusionContact);
        }

        [HttpPost]
        [Route("api/UDS/DeleteFromUDS")]
        public int DeleteFromUDS(UDSEntry entry)
        {
            return UDSAdapter.Instance.DeleteFromUDS(entry.IDUser, entry.IDInfusionContact.Value);
        }

        [HttpPost]
        [Route("api/UDS/UpdateUDS")]
        public int UpdateUDS(UDSEntry entry)
        {
            return UDSAdapter.Instance.UpdateUDS(entry.IDUser, entry.IDInfusionContact.Value);
        }

        [HttpPost]
        [Route("api/UDS/InsertUDS")]
        public int InsertUDS(UDSEntry entry)
        {
            return UDSAdapter.Instance.InsertUDS(entry.IDUser, entry.IDInfusionContact.Value);
        }
    }
}
