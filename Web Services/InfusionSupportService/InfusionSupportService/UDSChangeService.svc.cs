﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.ServiceModel.Activation;

//Added
using InfusionSupportService.DALC;

namespace InfusionSupportService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class UDSChangeService : IUDSChangeService
    {
        public string TestIfAlive()
        {
            return string.Format("Alive at srvLocalTime {0} / {1} {2}", DateTime.Now.ToLocalTime(), DateTime.Now.ToShortDateString(), DateTime.Now.ToLongTimeString());
        }

        public string TestDBConnection()
        {
            return true == UDSAdapter.Instance.TestConnection() ? "Conn. OK" : "Conn. failed, see Log for details!";
        }

        public List<UDSEntry> GetEntries_pgId(Guid pgId)
        {
            return UDSAdapter.Instance.GetEntries_pgId(pgId);
        }

        public List<UDSEntry> GetEntries_infusionId(int idInfusionContact)
        {
            return UDSAdapter.Instance.GetEntries_infusionId(idInfusionContact);
        }

        public int DeleteFromUDS(Guid pgId, int idInfusionContact)
        {
            return UDSAdapter.Instance.DeleteFromUDS(pgId, idInfusionContact);
        }

        public int UpdateUDS(Guid pgId, int idInfusionContact)
        {
            return UDSAdapter.Instance.UpdateUDS(pgId, idInfusionContact);
        }

        public int InsertUDS(Guid pgId, int idInfusionContact)
        {
            return UDSAdapter.Instance.InsertUDS(pgId, idInfusionContact);
        }
    }
}
