﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace InfusionSupportService.DALC
{
    [DataContract]
    public class UDSEntry
    {
        [DataMember]
        public Guid IDInfusion { get; set; }

        [DataMember]
        public Guid IDUser { get; set; }

        [DataMember]
        public int? IDInfusionContact { get; set; }
    }
}