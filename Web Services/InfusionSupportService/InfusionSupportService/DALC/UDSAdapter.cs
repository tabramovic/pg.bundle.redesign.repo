﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace InfusionSupportService.DALC
{
    public sealed class UDSAdapter
    {        
        static readonly UDSAdapter _instance = new UDSAdapter();

        static UDSAdapter()
        { }

        UDSAdapter()
        {
            ConnString = ConfigurationManager.ConnectionStrings["UDS_DB"].ConnectionString;
        }

        string ConnString { get; set; }

        public static UDSAdapter Instance
        {
            get { return _instance; }
        }

        public log4net.ILog Applog { get; set; }
        
                
        public bool TestConnection()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnString))
                {                
                    connection.Open();
                    Applog.Info("Conn. Opened!");
                    return true;               
                }
            }
            catch (Exception exc)
            {
                Applog.Error(exc);
                return false;
            }
        }

        public List<UDSEntry> GetEntries_pgId(Guid pgId)
        {
            List<UDSEntry> resList = new List<UDSEntry>();
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnString))
                {
                    connection.Open();
                    
                    SqlCommand cmd = new SqlCommand("SELECT IDInfusion, IDUser, IDInfusionContact FROM Infusion WHERE IDUser = @IDUser", connection);
                    cmd.Parameters.AddWithValue("@IDUser", pgId);                    
                    cmd.CommandType = CommandType.Text;

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            UDSEntry entry = new UDSEntry();
                            entry.IDInfusion = !reader.IsDBNull(0) ? reader.GetGuid(0) : Guid.Empty;
                            entry.IDUser     = !reader.IsDBNull(1) ? reader.GetGuid(1) : Guid.Empty;
                            entry.IDInfusionContact = !reader.IsDBNull(2) ? reader.GetInt32(2) : 0;

                            resList.Add(entry);
                        }
                    }                    
                }
            }
            catch (Exception exc)
            {
                Applog.Error(exc);
                resList = null;
            }

            return resList;
        }

        public List<UDSEntry> GetEntries_infusionId(int idInfusionContact)
        {
            List<UDSEntry> resList = new List<UDSEntry>();
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnString))
                {
                    connection.Open();

                    SqlCommand cmd = new SqlCommand("SELECT IDInfusion, IDUser, IDInfusionContact FROM Infusion WHERE IDInfusionContact = @IDInfusionContact", connection);
                    cmd.Parameters.AddWithValue("@IDInfusionContact", idInfusionContact);
                    cmd.CommandType = CommandType.Text;

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            UDSEntry entry = new UDSEntry();
                            entry.IDInfusion = !reader.IsDBNull(0) ? reader.GetGuid(0) : Guid.Empty;
                            entry.IDUser = !reader.IsDBNull(1) ? reader.GetGuid(1) : Guid.Empty;
                            entry.IDInfusionContact = !reader.IsDBNull(2) ? reader.GetInt32(2) : 0;

                            resList.Add(entry);
                        }
                    }
                }
            }
            catch (Exception exc)
            {
                Applog.Error(exc);
                resList = null;
            }

            return resList;
        }

        public int DeleteFromUDS(Guid pgId, int idInfusionContact)
        {
            int recordsDeleted = 0;

            try
            {
                using (SqlConnection connection = new SqlConnection(ConnString))
                {
                    connection.Open();
                    string sql = "DELETE FROM Infusion WHERE IDUser = @IDUser and IDInfusionContact = @IDInfusionContact";
                    SqlCommand cmd = new SqlCommand(sql, connection);
                    cmd.Parameters.AddWithValue("@IDUser", pgId);
                    cmd.Parameters.AddWithValue("@IDInfusionContact", idInfusionContact);
                    cmd.CommandType = CommandType.Text;

                    recordsDeleted = cmd.ExecuteNonQuery();
                    Applog.Info(string.Format("{0} results for: DELETE FROM Infusion WHERE IDUser={1} and IDInfusionContact={2}", recordsDeleted , pgId, idInfusionContact));
                }
            }
            catch (Exception exc)
            {
                Applog.Error(exc);
                recordsDeleted = -1;
            }

            return recordsDeleted;
        }

        public int UpdateUDS(Guid pgId, int idInfusionContact)
        {
            int recordsUpdated = 0;

            try
            {
                using (SqlConnection connection = new SqlConnection(ConnString))
                {
                    connection.Open();
                    string sql = "UPDATE Infusion SET IDInfusionContact = @IDInfusionContact WHERE IDUser = @IDUser";
                    SqlCommand cmd = new SqlCommand(sql, connection);
                    cmd.Parameters.AddWithValue("@IDUser", pgId);
                    cmd.Parameters.AddWithValue("@IDInfusionContact", idInfusionContact);
                    cmd.CommandType = CommandType.Text;

                    recordsUpdated = cmd.ExecuteNonQuery();
                    Applog.Info(string.Format("{0} results for: UPDATE Infusion SET IDInfusionContact ={1} WHERE = IDUser={2}", recordsUpdated, idInfusionContact, pgId ));
                }
            }
            catch (Exception exc)
            {
                Applog.Error(exc);
                recordsUpdated = -1;
            }

            return recordsUpdated;
        }

        public int InsertUDS(Guid pgId, int idInfusionContact)
        {
            int recordsInserted = 0;
            Guid newId = Guid.NewGuid();

            try
            {
                using (SqlConnection connection = new SqlConnection(ConnString))
                {
                    connection.Open();
                    string sql = "INSERT INTO Infusion (IDInfusion, IDUser, IDInfusionContact) VALUES (@IDInfusion, @IDUser, @IDInfusionContact)";
                    SqlCommand cmd = new SqlCommand(sql, connection);
                    cmd.Parameters.AddWithValue("@IDInfusion", newId);
                    cmd.Parameters.AddWithValue("@IDUser", pgId);
                    cmd.Parameters.AddWithValue("@IDInfusionContact", idInfusionContact);
                    cmd.CommandType = CommandType.Text;

                    recordsInserted = cmd.ExecuteNonQuery();
                    Applog.Info(string.Format("{0} results for: INSERT INTO Infusion (IDInfusion, IDUser, IDInfusionContact) VALUES ({0}, {1}, {2})", recordsInserted, pgId, idInfusionContact));
                }
            }
            catch (Exception exc)
            {
                Applog.Error(exc);
                recordsInserted = -1;
            }

            return recordsInserted;
        }
    }
} 