using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Web;
using System.Web.Services;
using SoftwareRegistration;
using RegistrationService;

namespace WS_PG
{
	/// <summary>
	/// Summary description for Service1.
	/// </summary>
	/// 

	[WebService(Namespace="http://www.profitgrabber.com/Registration/")]
	public class Service1 : System.Web.Services.WebService
	{
		private RegistrationService.RegistrationService registrationService = new RegistrationService.RegistrationService();

		public Service1()
		{
			//CODEGEN: This call is required by the ASP.NET Web Services Designer
			InitializeComponent();
		}

		#region Component Designer generated code
		
		//Required by the Web Services Designer 
		private IContainer components = null;
				
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if(disposing && components != null)
			{
				components.Dispose();
			}
			base.Dispose(disposing);		
		}
		
		#endregion

		[WebMethod]
		public string TestIfAlive()
		{
			return "Alive at: " + DateTime.Now.ToString();
		}

		[WebMethod]
		public string TestConnection()
		{
			return registrationService.TestConnection();
		}

		[WebMethod]
		public IList RegisterUser(SoftwareRegistration.DataClasses.User user, string keycode, out string message)
		{
			return registrationService.RegisterUser(user, keycode, out message);
		}

		[WebMethod]
		public RegistrationService.RegistrationService.ShortUserInfo[] GetAllUsers(string keycode, out string message)
		{
			return registrationService.GetAllUsers(keycode, out message);
		}

		[WebMethod]
		public SoftwareRegistration.DataClasses.User GetUser(Guid registration_id, out string message)
		{
			return registrationService.GetUser(registration_id, out message);
		}

		[WebMethod]
		public decimal RequestDataPropertyProfile(SoftwareRegistration.DataClasses.Registration reg, byte[] memstream_request, out byte[] memstream_response, out string message)
		{
			return registrationService.RequestDataPropertyProfile(reg, memstream_request, out memstream_response, out message);
		}
		
		[WebMethod]
		public decimal RequestDataCOMPS(SoftwareRegistration.DataClasses.Registration reg, byte[] memstream_request, out byte[] memstream_response, out string message)
		{
			return registrationService.RequestDataCOMPS(reg, memstream_request, out memstream_response, out message);
		}

		[WebMethod]
		public bool ConfirmDataRequestSuccess(Guid id_registration, int no, int id, out string message)
		{
			return registrationService.ConfirmDataRequestSuccess(id_registration, no, id, out message);
		}

		[WebMethod]
		public SoftwareRegistration.DataClasses.ActiveModules GetActiveModules(Guid registration_key, out string message)
		{
			return registrationService.GetActiveModules(registration_key, out message);
		}

		[WebMethod]
		public bool SetActiveModules(SoftwareRegistration.DataClasses.ActiveModules module, string key, out string message)
		{
			return registrationService.SetActiveModules(module, key, out message);
		}


/*
		[WebMethod]
		public IList RegisterUser(SoftwareRegistration.DataClasses.User user, string keycode, out string message)
		{
			message = "";
			return null;
		}

		[WebMethod]
		public SoftwareRegistration.DataClasses.Registration[] GetAllRegistrations(string message)
		{
			if(message == "!mSg1")
			{
				//return nesto;
			}
			return null;
		}

		[WebMethod]
		public SoftwareRegistration.DataClasses.User GetUser(Guid idUser)
		{
			return null;
		}

		[WebMethod]
		public SoftwareRegistration.DataClasses.Address GetAddress(Guid idAddress)
		{
			return null;
		}
*/
//		[WebMethod]
//		public SoftwareRegistration.DataClasses active modules GetAddress(Guid idAddress)
//		{
//			return null;
//		}

		//update active modules (reg id, bool[])

		/*
		[WebMethod]
		public decimal RequestDataPropertyProfile(SoftwareRegistration.DataClasses.Registration reg, byte[] memstream_request, out byte[] memstream_response, out string message)
		{
			this.wsc = new WebServiceCore();
			return wsc.RequestDataPropertyProfile(reg, memstream_request, out memstream_response, out message);
		}
		
		[WebMethod]
		public decimal RequestDataCOMPS(SoftwareRegistration.DataClasses.Registration reg, byte[] memstream_request, out byte[] memstream_response, out string message)
		{
			this.wsc = new WebServiceCore();
			return wsc.RequestDataCOMPS(reg, memstream_request, out memstream_response, out message);
		}
*/	}
}
