//------------------------------------------------------------------------------
// <autogenerated>
//     This code was generated by NHibernate.
//     Runtime Version: v1.1.4322
//
//     Changes to this file may cause incorrect behavior and will be lost if 
//     the code is regenerated.
// </autogenerated>
//------------------------------------------------------------------------------

using System;

namespace SoftwareRegistration.DataClasses
{

 /// <summary>
 /// POJO for User
 /// </summary>
 /// <remark>
 /// This class is autogenerated
 /// </remark>
 [Serializable]
 public class User
 {

	#region Fields
	/// <summary>
	/// Holder for IDUser
	/// </summary>
	private Guid iDUser;
	
	/// <summary>
	/// Holder for FirstName
	/// </summary>
	private String firstName;
	
	/// <summary>
	/// Holder for MiddleName
	/// </summary>
	private String middleName;
	
	/// <summary>
	/// Holder for LastName
	/// </summary>
	private String lastName;
	
	/// <summary>
	/// Holder for Address
	/// </summary>
	private SoftwareRegistration.DataClasses.Address address;
	
	#endregion

	#region Constructors
	/// <summary>
	/// Default constructor for class User
	/// </summary>
	public User()
	{
	}
	
	/// <summary>
	/// Constructor for class User
	/// </summary>
	/// <param name="FirstName">Initial FirstName value</param>
	/// <param name="MiddleName">Initial MiddleName value</param>
	/// <param name="LastName">Initial LastName value</param>
	/// <param name="Address">Initial Address value</param>
	public User(String FirstName, String MiddleName, String LastName, SoftwareRegistration.DataClasses.Address Address)
	{
		this.firstName = FirstName;
		this.middleName = MiddleName;
		this.lastName = LastName;
		this.address = Address;
	}
	
	#endregion
	
	#region Properties
	/// <summary>
	/// Get/set for IDUser
	/// </summary>
	public Guid IDUser
	{
		get
		{
			return this.iDUser;
		}
		set
		{
			this.iDUser = value;
		}
	}
	
	/// <summary>
	/// Get/set for FirstName
	/// </summary>
	public String FirstName
	{
		get
		{
			return this.firstName;
		}
		set
		{
			this.firstName = value;
		}
	}
	
	/// <summary>
	/// Get/set for MiddleName
	/// </summary>
	public String MiddleName
	{
		get
		{
			return this.middleName;
		}
		set
		{
			this.middleName = value;
		}
	}
	
	/// <summary>
	/// Get/set for LastName
	/// </summary>
	public String LastName
	{
		get
		{
			return this.lastName;
		}
		set
		{
			this.lastName = value;
		}
	}
	
	/// <summary>
	/// Get/set for Address
	/// </summary>
	public SoftwareRegistration.DataClasses.Address Address
	{
		get
		{
			return this.address;
		}
		set
		{
			this.address = value;
		}
	}
	
	#endregion
 }
}