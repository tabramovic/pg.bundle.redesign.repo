//------------------------------------------------------------------------------
// <autogenerated>
//     This code was generated by NHibernate.
//     Runtime Version: v1.1.4322
//
//     Changes to this file may cause incorrect behavior and will be lost if 
//     the code is regenerated.
// </autogenerated>
//------------------------------------------------------------------------------

using System;

namespace SoftwareRegistration.DataClasses
{

 /// <summary>
 /// POJO for Data
 /// </summary>
 /// <remark>
 /// This class is autogenerated
 /// </remark>
 [Serializable]
 public class Data
 {

	#region Fields
	/// <summary>
	/// Holder for IDData
	/// </summary>
	private Guid iDData;
	
	/// <summary>
	/// Holder for Name
	/// </summary>
	private String name;
	
	#endregion

	#region Constructors
	/// <summary>
	/// Default constructor for class Data
	/// </summary>
	public Data()
	{
	}
	
	/// <summary>
	/// Constructor for class Data
	/// </summary>
	/// <param name="Name">Initial Name value</param>
	public Data(String Name)
	{
		this.name = Name;
	}
	
	#endregion
	
	#region Properties
	/// <summary>
	/// Get/set for IDData
	/// </summary>
	public Guid IDData
	{
		get
		{
			return this.iDData;
		}
		set
		{
			this.iDData = value;
		}
	}
	
	/// <summary>
	/// Get/set for Name
	/// </summary>
	public String Name
	{
		get
		{
			return this.name;
		}
		set
		{
			this.name = value;
		}
	}
	
	#endregion
 }
}