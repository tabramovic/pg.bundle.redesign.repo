//#define DUMP_XML_OBJECT
//#define SKIP_CREDIT_CHECK
//#define STAGING

using System;
using System.Collections;
using System.Configuration;
using System.Diagnostics;
using System.Net;
using System.Web;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;

using NHibernate;
using NHibernate.Expression;

using System.Reflection;
using System.Threading;

using SoftwareRegistration.DataClasses;
using Configuration = NHibernate.Cfg.Configuration;

namespace RegistrationService
{
	/// <summary>
	/// Summary description for Class1.
	/// </summary>
	public class RegistrationService
	{        
		private static readonly log4net.ILog Applog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

		public readonly string Version = "2.2.6"; //changed date/month charge policy, added RequestDataCOMPS3, changed comps auth / date to infinity //changed charging
		private static NHibernate.ISession          nhibernateSession;
		private static NHibernate.ISessionFactory   nhibernateSessionFactory;
		private static NHibernate.Cfg.Configuration config;

		private static String lockVar = "lock";

		private string NhibernateInit()
		{
			try 
			{
                if (nhibernateSession == null)
                {

                    System.Reflection.Assembly asm;
                    asm = Assembly.GetAssembly(typeof(RegistrationService));
                    string code_base = asm.CodeBase;

                    int position = code_base.LastIndexOf("/");
                    string location = code_base.Substring(0, position + 1);
                    location = location.Replace("file:///", "");
                    location = location.Replace('/', '\\');

                    XmlTextReader reader = null;
                    reader = new XmlTextReader(location + @"settings.xml");

                    string connection_string = "";

                    while (reader.Read())
                    {
                        if (reader.NodeType == XmlNodeType.Element)
                        {
                            if (reader.LocalName.Equals("ConnectionString"))
                            {
                                connection_string = reader.ReadString();
                            }

                            if (reader.LocalName.Equals("Balance"))
                            {
                                Console.WriteLine("{0}", reader.ReadString());
                            }
                        }
                    }

                    reader.Close();

                    config = new Configuration();
                    IDictionary props = new Hashtable();

                    props["hibernate.connection.provider"] = "NHibernate.Connection.DriverConnectionProvider";
                    props["hibernate.dialect"] = "NHibernate.Dialect.MsSql2000Dialect";
                    props["hibernate.connection.driver_class"] = "NHibernate.Driver.SqlClientDriver";
                    props["hibernate.connection.connection_string"] = connection_string;

                    foreach (DictionaryEntry de in props)
                    {
                        config.SetProperty(de.Key.ToString(), de.Value.ToString());
                    }

                    config.AddAssembly("RegistrationService");

                    //NhibernateDeInit();

                    nhibernateSessionFactory = config.BuildSessionFactory();
                    nhibernateSession = nhibernateSessionFactory.OpenSession();
                }
                else
                {
                    nhibernateSession.Clear();
                }
			}
			catch (Exception e)
			{
				System.Diagnostics.Debug.WriteLine(e.ToString());
                return e.ToString();
			}
            return "";
		}

		private void NhibernateDeInit()
		{
			try
			{
				if(nhibernateSession != null)
				{
					nhibernateSession.Close();
					nhibernateSession.Dispose();
				}
				nhibernateSession = null;

				if(nhibernateSessionFactory != null)
				{
					nhibernateSessionFactory.Close();
				}
				nhibernateSessionFactory = null;
				
				GC.Collect(3);
			}
			catch
			{
			}
		}

		public RegistrationService()
		{
			//			NhibernateInit();
		}

		~RegistrationService()
		{
			//			NhibernateDeInit();
		}

        public bool AddTopups(string regkey, decimal money, string key, out string message)
        {
            message = "";

            if (key != "29p%sp28u") return false;

            lock (lockVar)
            {
                NhibernateInit();

                ITransaction transaction = null;

                try
                {
                    transaction = nhibernateSession.BeginTransaction();

                    IList registrations = nhibernateSession.CreateCriteria(typeof(SoftwareRegistration.DataClasses.Registration)).Add(Expression.Eq("KeyCode", regkey)).List();

                    if (registrations.Count < 1)
                    {
                        transaction.Rollback();
                        message = "no such key";
                        return false;
                    }
                    Registration reg = (Registration)registrations[0];


                    IList users = nhibernateSession.CreateCriteria(typeof(User))
                        .Add(Expression.Eq("IDUser", reg.User.IDUser)).List();

                    if (users.Count < 1)
                    {
                        transaction.Rollback();
                        message = "no such user";
                        return false;
                    }

                    Topup topup = 
                        new Topup(DateTime.Now, money, 0, (User)users[0], false, null);
                    
                    nhibernateSession.Save(topup);

                    transaction.Commit();
                    //nhibernateSession.Clear();
                }
                catch (Exception e)
                {
                    message += e.ToString();
                    if (transaction != null) transaction.Rollback();
                    NhibernateDeInit();
                    return false;
                }
            }
            return true;
        }


        public bool AddInputs(string regkey, int money, int months, string key, out string message)
        {
            message = "";

            if (key != "1!3kj328a") return false;
            
            lock (lockVar)
            {
                NhibernateInit();

                ITransaction transaction = null;

                try
                {
                    transaction = nhibernateSession.BeginTransaction();

                    IList registrations = nhibernateSession.CreateCriteria(typeof(SoftwareRegistration.DataClasses.Registration)).Add(Expression.Eq("KeyCode", regkey)).List();

                    if (registrations.Count < 1)
                    {
                        transaction.Rollback();
                        message = "no such key";
                        return false;
                    }
                    Registration reg = (Registration)registrations[0];
                    

                    IList users = nhibernateSession.CreateCriteria(typeof(User))
                        .Add(Expression.Eq("IDUser", reg.User.IDUser)).List();

                    if (users.Count < 1)
                    {
                        transaction.Rollback();
                        message = "no such user";
                        return false;
                    }

                    for (int i = 0; i < months; i++)
                    {
                        DateTime dt = DateTime.Now.Date.AddMonths(i);
                        Input input = new Input(DateTime.Now, dt, money, (User)users[0]);
                        nhibernateSession.Save(input);
                    }

                    transaction.Commit();
                    //nhibernateSession.Clear();
                }
                catch (Exception e)
                {
                    message += e.ToString();
                    if (transaction != null) transaction.Rollback();
                    NhibernateDeInit();
                    return false;
                }
            }
            return true;
        }

		public IList RegisterUser(SoftwareRegistration.DataClasses.User user, SoftwareRegistration.DataClasses.Registration keycode, out string message)
		{
			lock(lockVar)
			{
				NhibernateInit();

				ITransaction transaction = null;
				ArrayList result = new ArrayList();

				try
				{
					transaction = nhibernateSession.BeginTransaction();

					IList registrations = nhibernateSession.CreateCriteria(
                        typeof(SoftwareRegistration.DataClasses.Registration))
                        .Add(Expression.Eq("KeyCode",keycode.KeyCode))
                        .Add(Expression.Or(
                            Expression.Eq("VolumeNumber", 0),
                            Expression.Eq("VolumeNumber", 1)))   //0, standard, 1 is module or reserved   
                        .List();

					if(registrations.Count == 0)
					{
						result.Add("wrong key");
						transaction.Rollback();
						return result;
					}

					SoftwareRegistration.DataClasses.Registration registration = (SoftwareRegistration.DataClasses.Registration)registrations[0];

					//break if already registred, first name is enough
                    if (registration.User != null)
                    {
                        if (registration.User.FirstName == "reserved")
                        {
                            nhibernateSession.Delete(registration.User.Address);
                            registration.User.Address = user.Address;
                            registration.User.FirstName = user.FirstName;
                            registration.User.LastName = user.LastName;
                            registration.User.MiddleName = user.MiddleName;
                        }
                        else
                        {
                            result.Add("already registred");
                            transaction.Rollback();
                            return result;
                        }
                    }
                    else
                    {
                        registration.User = user;
                    }

					registration.DateActivation = DateTime.Now.Date;

					//due to $#$@@#%* db design, we add 100 years to expiration date which anyway no one uses
					registration.DateExpiration = DateTime.Now.AddYears(100).Date;

					nhibernateSession.Save(registration);

					nhibernateSession.Flush();

					transaction.Commit();

					result.Add("Successfull!");
                    result.Add(registration.User);
				}
				catch(Exception e)
				{
					message = e.ToString();
					transaction.Rollback();
					System.Diagnostics.Debug.WriteLine(e.ToString());
					result.Clear();
					result.Add(e.ToString());
					NhibernateDeInit();
				}
				finally
				{				
					message = "";
				}
				return result;
			}
		}

        public bool QueryGuidFromKey(string keycode, out Guid id_user, out string message)
        {
            message = "";
            id_user = Guid.Empty;
            lock (lockVar)
            {
                this.NhibernateInit();
                ArrayList list = new ArrayList();
                try
                {
                    IList list2 = nhibernateSession.CreateCriteria(typeof(Registration)).Add(NHibernate.Expression.Expression.Eq("KeyCode", keycode)).List();
                    if (list2.Count == 0)
                    {
                        return false;
                    }
                    Registration registration = (Registration)list2[0];
                    id_user = registration.User.IDUser;
                }
                catch (Exception exception)
                {
                    Debug.WriteLine(exception.ToString());
                    list.Clear();
                    this.NhibernateDeInit();
                    return false;
                }
            }
            return true;
        }

		public IList GetFreeSerials(string key, out string message)
		{
			message = "";
			if(key != "fr!srl?|") return null;

			lock(lockVar)
			{
				NhibernateInit();

				ArrayList result = new ArrayList();

				try
				{
					IList registrations = nhibernateSession.CreateCriteria(
                        typeof(SoftwareRegistration.DataClasses.Registration))
                        .Add(Expression.IsNull("User.IDUser"))
                        .Add(Expression.Eq("VolumeNumber", 0))
                        .List();


					foreach(SoftwareRegistration.DataClasses.Registration reg in registrations)
					{
						result.Add(reg.KeyCode);
					}
				}
				catch(Exception e)
				{
					message = e.ToString();
					System.Diagnostics.Debug.WriteLine(e.ToString());
					result.Clear();
					NhibernateDeInit();
				}

                nhibernateSession.Clear();

				return result;
			}
		}

        public string GetReservedSerial(
            ActiveModules module,
            string key,
            out string message)
        {
            message = "";
            if (key != "e34a1!234#2k") return null;

            lock (lockVar)
            {
                NhibernateInit();

                ITransaction transaction = null;

                try
                {

                    IList registrations = nhibernateSession.CreateCriteria(
                        typeof(SoftwareRegistration.DataClasses.Registration))
                        .Add(Expression.IsNull("User.IDUser"))
                        .Add(Expression.Eq("VolumeNumber",0))
                        .SetMaxResults(1)
                        .List();

                    if(registrations == null || registrations.Count < 1)
                    {
                        message = "No free keys, please contact technical support!";
                        transaction.Rollback();
                        return "";
                    }

                    Registration registration = (Registration)registrations[0];

                    Guid regGuid = registration.IDRegistration;

                    nhibernateSession.Clear();


                    transaction = nhibernateSession.BeginTransaction();

                    //get accompanying active module
                    IList active_modules = nhibernateSession.CreateCriteria(
                        typeof(SoftwareRegistration.DataClasses.ActiveModules))
                        .Add(Expression.Eq("IDRegistration",
                            regGuid))
                        .List();

                    //todo, !!!!! ne smije postojati ni jedna registracija bez aktivnih modula
                    if (active_modules == null || active_modules.Count < 1)
                    {
                        message = "No asocciated active modules, please contact technical support!";
                        transaction.Rollback();

                        return "";
                    }

                    ActiveModules currentModule = (ActiveModules) active_modules[0];

                    //update active module with new values
                    //glupo ali ne da mi se ponovno proucavat nhibernate
                    currentModule.MOD_0 = module.MOD_0;
                    currentModule.MOD_1 = module.MOD_1;
                    currentModule.MOD_2 = module.MOD_2;
                    currentModule.MOD_3 = module.MOD_3;
                    currentModule.MOD_4 = module.MOD_4;
                    currentModule.MOD_5 = module.MOD_5;
                    currentModule.MOD_6 = module.MOD_6;
                    currentModule.MOD_7 = module.MOD_7;
                    currentModule.MOD_8 = module.MOD_8;
                    currentModule.MOD_9 = module.MOD_9;
                    currentModule.MOD_10 = module.MOD_10;
                    currentModule.MOD_11 = module.MOD_11;
                    currentModule.MOD_12 = module.MOD_12;
                    currentModule.MOD_13 = module.MOD_13;
                    currentModule.MOD_14 = module.MOD_14;
                    currentModule.MOD_15 = module.MOD_15;
                    currentModule.MOD_16 = module.MOD_16;
                    currentModule.MOD_17 = module.MOD_17;
                    currentModule.MOD_18 = module.MOD_18;
                    currentModule.MOD_19 = module.MOD_19;
                    currentModule.MOD_20 = module.MOD_20;


                    nhibernateSession.Save(currentModule);

//                    transaction.Commit();

//                    nhibernateSession.Clear();

                   // transaction = nhibernateSession.BeginTransaction();

                    IList registrations2 = nhibernateSession.CreateCriteria(typeof(SoftwareRegistration.DataClasses.Registration)).Add(Expression.Eq("IDRegistration", regGuid)).List();

                    Registration registration2 = (Registration)registrations2[0];
                    
                    registration2.VolumeNumber = 1; //!!!!! key REGISTRED

                    registration2.DateActivation = DateTime.Today.Date;
                    registration2.DateExpiration = DateTime.Today.Date.AddYears(100);

                    User user = new User("reserved", "", "", new Address());

                    nhibernateSession.SaveOrUpdate(user);

                    registration2.User = user;

                    nhibernateSession.Save(registration2);

                    transaction.Commit();

                    nhibernateSession.Clear();

                    return registration2.KeyCode;

                }
                catch (Exception e)
                {
                    message += e.ToString();
                    System.Diagnostics.Debug.WriteLine(e.ToString());
                    if(transaction != null) transaction.Rollback();
                    NhibernateDeInit();

                }
            }

            nhibernateSession.Clear();

            return "err";
        }


        public bool ActivateModule(
            string app_key,
            string module_key,
            string key,
            out string message)
        {
            message = "";
            if (key != "ew23a1#1@") return false;

            lock (lockVar)
            {
                NhibernateInit();

                ITransaction transaction = null;

                try
                {
                    transaction = nhibernateSession.BeginTransaction();

                    #region app key
                    //get "reservered" registration
                    IList app_registrations = nhibernateSession.CreateCriteria(typeof(Registration))
                        .Add(Expression.Eq("KeyCode", app_key))
                        .List();

                    if (app_registrations == null || app_registrations.Count < 1)
                    {
                        message = "wrong or used app key!";
                        transaction.Rollback();
                        return false;
                    }

                    Registration app_registration = (Registration)app_registrations[0];


                    //get accompanying active module
                    IList app_active_modules = nhibernateSession.CreateCriteria(
                        typeof(ActiveModules))
                        .Add(Expression.Eq("IDRegistration", app_registration.IDRegistration))
                        .List();

                    //todo, !!!!! ne smije postojati ni jedna registracija bez aktivnih modula
                    if (app_active_modules == null || app_active_modules.Count < 1)
                    {
                        message = "No asocciated active modules, please contact technical support!";
                        transaction.Rollback();
                        return false;
                    }

                    ActiveModules app_active_module = (ActiveModules)app_active_modules[0];

                    #endregion

                    #region module key
                    //get "reservered" registration
                    IList module_registrations = nhibernateSession.CreateCriteria(typeof(Registration))
                        .Add(Expression.Eq("KeyCode", module_key))
                        .Add(Expression.Eq("VolumeNumber", 1))   //0, standard, 1 is module or reserved   
                        .List();

                    if (module_registrations == null || module_registrations.Count < 1)
                    {
                        message = "wrong module key or key already used!";
                        transaction.Rollback();
                        return false;
                    }

                    Registration module_registration = (Registration)module_registrations[0];

                    //break if already registred, first name is enough
                    if (module_registration.User != null)
                    {
                        if (module_registration.User.FirstName != "reserved")
                        {
                            message = "already registred";
                            transaction.Rollback();
                            return false;
                        }
                        else
                        {
                            module_registration.User.FirstName = "active_module_registred";
                        }
                    }

                    //get accompanying active module
                    IList module_active_modules = nhibernateSession.CreateCriteria(
                        typeof(ActiveModules))
                        .Add(Expression.Eq("IDRegistration", module_registration.IDRegistration))
                        .List();

                    //todo, !!!!! ne smije postojati ni jedna registracija bez aktivnih modula
                    if (module_active_modules == null || module_active_modules.Count < 1)
                    {
                        message = "No asocciated active modules, please contact technical support!";
                        transaction.Rollback();
                        return false;
                    }

                    ActiveModules module_active_module = (ActiveModules)module_active_modules[0];

                    #endregion


                    //update active module with new values
                    //glupo ali ne da mi se ponovno proucavat nhibernate
                    app_active_module.MOD_0 |= module_active_module.MOD_0;
                    app_active_module.MOD_1 |= module_active_module.MOD_1;
                    app_active_module.MOD_2 |= module_active_module.MOD_2;
                    app_active_module.MOD_3 |= module_active_module.MOD_3;
                    app_active_module.MOD_4 |= module_active_module.MOD_4;
                    app_active_module.MOD_5 |= module_active_module.MOD_5;
                    app_active_module.MOD_6 |= module_active_module.MOD_6;
                    app_active_module.MOD_7 |= module_active_module.MOD_7;
                    app_active_module.MOD_8 |= module_active_module.MOD_8;
                    app_active_module.MOD_9 |= module_active_module.MOD_9;
                    app_active_module.MOD_10 |= module_active_module.MOD_10;
                    app_active_module.MOD_11 |= module_active_module.MOD_11;
                    app_active_module.MOD_12 |= module_active_module.MOD_12;
                    app_active_module.MOD_13 |= module_active_module.MOD_13;
                    app_active_module.MOD_14 |= module_active_module.MOD_14;
                    app_active_module.MOD_15 |= module_active_module.MOD_15;
                    app_active_module.MOD_16 |= module_active_module.MOD_16;
                    app_active_module.MOD_17 |= module_active_module.MOD_17;
                    app_active_module.MOD_18 |= module_active_module.MOD_18;
                    app_active_module.MOD_19 |= module_active_module.MOD_19;
                    app_active_module.MOD_20 |= module_active_module.MOD_20;

                    nhibernateSession.Evict(module_active_module);

                    module_registration.VolumeNumber = 2;

                    nhibernateSession.Save(module_registration);

                    nhibernateSession.Save(app_active_module);

                    

                    transaction.Commit();

                    nhibernateSession.Clear();

                    return true;

                }
                catch (Exception e)
                {
                    message = e.ToString();
                    System.Diagnostics.Debug.WriteLine(e.ToString());
                    transaction.Rollback();
                    NhibernateDeInit();

                }
            }
            
            nhibernateSession.Clear();

            return false;
        }

		public class ShortUserInfo
		{
			public string FirstName;
			public string MiddleName;
			public string LastName;
			public string SerialNumber;
			public DateTime ActivationDate;
			public Guid RegistrationId;
			public Guid UserId;
		}

		public ShortUserInfo[] GetAllUsers(string keycode, out string message)
		{
			lock(lockVar)
			{
				NhibernateInit();

				message = "";

				if(keycode != "!aD0420") return null;

				ArrayList result = new ArrayList();

				try
				{
					IList registrations = nhibernateSession.CreateCriteria(typeof(SoftwareRegistration.DataClasses.Registration)).List();

					foreach(SoftwareRegistration.DataClasses.Registration reg in registrations)
					{
						if(reg.User != null)
						{
							ShortUserInfo sui = new ShortUserInfo();

							sui.ActivationDate = reg.DateActivation;
							sui.RegistrationId = reg.IDRegistration;
							sui.SerialNumber = reg.KeyCode;

							sui.FirstName = reg.User.FirstName;
							sui.LastName = reg.User.LastName;
							sui.MiddleName = reg.User.MiddleName;

							sui.UserId = reg.User.IDUser;

							result.Add(sui);

							nhibernateSession.Evict(reg.User);
						}

						//odvratno, ali radi
						nhibernateSession.Evict(reg);
					}

					message = "";

                    nhibernateSession.Flush(); 
                    nhibernateSession.Clear();

					return (ShortUserInfo[])result.ToArray(typeof(ShortUserInfo));
				}
				catch(Exception e)
				{
					message = e.ToString();
					NhibernateDeInit();
					return null;
				}
			}
		}


		////TODO - user not reg
		//		public SoftwareRegistration.DataClasses.User GetUser(Guid registration_id, out string message)
		public SoftwareRegistration.DataClasses.User GetUser(Guid user_id, out string message)
		{
			lock(lockVar)
			{
				NhibernateInit();

				message = "";

				try
				{					
					IList users = nhibernateSession.CreateCriteria(typeof(SoftwareRegistration.DataClasses.User)).Add(Expression.Eq("IDUser",user_id)).List();

					if(users.Count == 0)
					{
						message = "wrong user id";
						return null;
					}

					SoftwareRegistration.DataClasses.User user = ((SoftwareRegistration.DataClasses.User)users[0]);

					if(user != null)
					{
						return user;
					}
					else
					{
						return null;
					}

					//					return ((SoftwareRegistration.DataClasses.Registration)registrations[0]).User;
				}
				catch(Exception e)
				{
					message = e.ToString();
					NhibernateDeInit();
					return null;
				}
			}
		}

		//charge count
		public int GetUserMonthNumber(Guid user_id, out string message)
		{
			lock(lockVar)
			{
				int count = 0;

				NhibernateInit();

				message = "";

				try
				{	
					IList outputs = nhibernateSession.CreateCriteria(typeof(SoftwareRegistration.DataClasses.Output)).Add(Expression.Eq("User.IDUser",user_id)).List();

					foreach(SoftwareRegistration.DataClasses.Output output in outputs)
					{
						count += output.Number;
					}

					return count;
				}
				catch(Exception e)
				{
					message = e.ToString();
					NhibernateDeInit();
					return -1;
				}
			}
		}



		////TODO - user not reg
		//public SoftwareRegistration.DataClasses.ActiveModules GetActiveModules(Guid registration_key, out string message)
		public SoftwareRegistration.DataClasses.ActiveModules GetActiveModules(Guid user_id, out string message)
		{
			lock(lockVar)
			{
				NhibernateInit();

				message = "";

				ArrayList result = new ArrayList();

				try
				{
                    IList registrations = nhibernateSession.CreateCriteria(typeof(SoftwareRegistration.DataClasses.Registration)).Add(Expression.Eq("User.IDUser", user_id)).List();

					if(registrations.Count == 0)
					{
						message = "wrong user id";
						return null;
					}

					IList modules = nhibernateSession.CreateCriteria(typeof(SoftwareRegistration.DataClasses.ActiveModules)).Add(Expression.Eq("IDRegistration",((SoftwareRegistration.DataClasses.Registration)registrations[0]).IDRegistration)).List();

					if(modules.Count == 0)
					{
						message = "no active modules found";
						return null;
					}

					message = "";
					return (SoftwareRegistration.DataClasses.ActiveModules)modules[0];
				}
				catch(Exception e)
				{
					message = e.ToString();
					NhibernateDeInit();
					return null;
				}
			}
		}

		public bool SetActiveModules(SoftwareRegistration.DataClasses.ActiveModules module, string key, out string message)
		{
			message = "";

			if(key != "1asw32*a") return false;

			lock(lockVar)
			{
				NhibernateInit();

				ITransaction transaction = null;
				ArrayList result = new ArrayList();

				try
				{
					transaction = nhibernateSession.BeginTransaction();

					IList modules = nhibernateSession.CreateCriteria(typeof(SoftwareRegistration.DataClasses.ActiveModules)).Add(Expression.Eq("IDRegistration",module.IDRegistration)).List();

					//					SoftwareRegistration.DataClasses.ActiveModules db_module = new SoftwareRegistration.DataClasses.ActiveModules();

					if(modules.Count == 0)
					{
						nhibernateSession.Save(module);
					}
					else
					{
						nhibernateSession.Evict(modules[0]);
						nhibernateSession.Update(module);
					}

					nhibernateSession.Flush();

					transaction.Commit();

					return true;
				}
				catch(Exception e)
				{
					transaction.Rollback();
					message = e.ToString();
					NhibernateDeInit();
					return false;
				}
			}
		}

		#region Data Consumption

        public decimal RequestDataPropertyProfile(SoftwareRegistration.DataClasses.Registration reg,
            byte[] memstream_request,
            out byte[] memstream_response,
            out string message)
        {
            Applog.Info($"Entered RequestDataPropertyProfile");
            return CheckForInputsAndRequest(reg, memstream_request, out memstream_response, out message);
        }

        public decimal RequestDataCOMPS(SoftwareRegistration.DataClasses.Registration reg,
            byte[] memstream_request,
            out byte[] memstream_response,
            out string message)
        {
            Applog.Info($"Entered RequestDataCOMPS");
            return CheckForInputsAndRequest(reg, memstream_request, out memstream_response, out message);
        }

        public decimal RequestDataPropertyProfile2(SoftwareRegistration.DataClasses.Registration reg,
            byte[] request,
            out byte[] memstream_response,
            out string message)
        {
            Applog.Info($"Entered RequestDataPropertyProfile2, entering buildRequestListView");
            byte[] memstream_request = buildRequestListView(request);
            return CheckForInputsAndRequest(reg, memstream_request, out memstream_response, out message);
        }

        public decimal RequestDataCOMPS2(SoftwareRegistration.DataClasses.Registration reg,
            byte[] request,
            out byte[] memstream_response,
            out string message)
        {
            Applog.Info($"Entered RequestDataCOMPS2, entering getSubjectCOMPS");
            byte[] memstream_request = getSubjectCOMPS(request, LoginID.COMPS);
            return CheckForInputsAndRequest(reg, memstream_request, out memstream_response, out message);
        }

        public decimal RequestDataCOMPS3(SoftwareRegistration.DataClasses.Registration reg,
            byte[] request,
            out byte[] memstream_response,
            out string message)
        {
            Applog.Info($"Entered RequestDataCOMPS3");
            byte[] memstream_request = getSubjectCOMPS(request, LoginID.MM);
            return CheckForInputsAndRequest(reg, memstream_request, out memstream_response, out message);
        }

        public decimal CheckForInputsAndRequest(SoftwareRegistration.DataClasses.Registration reg,
			byte []memstream_request,
			out byte []memstream_response,
			out string message)
		{
			message = "";

			//			ITransaction transaction = null;
			decimal result = 0;
            try
			{
#if STAGING
                goto ConnectLabel;
#endif
#if SKIP_CREDIT_CHECK
				goto ConnectLabel;
#endif
				lock (lockVar)
				{
					try
					{
                        
						NhibernateInit();

						//						transaction = nhibernateSession.BeginTransaction();
						IList registrations = nhibernateSession.CreateCriteria(typeof(SoftwareRegistration.DataClasses.Registration)).Add(Expression.Eq("KeyCode",reg.KeyCode)).List();

						if(registrations.Count == 0)
						{
                            message = "1u2n3f";
							memstream_response = null;
							//							transaction.Rollback();
							return 0;
						}

						foreach(SoftwareRegistration.DataClasses.Registration r in registrations)
						{
							nhibernateSession.Evict(r);
						}

						SoftwareRegistration.DataClasses.Registration registration = (SoftwareRegistration.DataClasses.Registration)registrations[0];

						if(registration.User == null)
						{
							memstream_response = null;
							message = "not registred";

							//							transaction.Rollback();
							return 0;
						}

                        int month, topup;
                        QueryAcc(registration.User.IDUser, out month, out topup);

                        if (month + topup <= 0)
                        {
                            memstream_response = null;
                            message = "no credits on account";

                            //							transaction.Rollback();
                            return 0;
                        }

					}
					catch(Exception e)
					{
						memstream_response = null;
						NhibernateDeInit();
						System.Diagnostics.Debug.WriteLine(e.ToString());
						message = e.ToString();
						//						if(transaction != null) transaction.Rollback();
						return 0;
					}

				}

#if STAGING
                ConnectLabel:
#endif
#if SKIP_CREDIT_CHECK
			ConnectLabel:
#endif
				//				if(transaction != null) transaction.Rollback();
				Connect(memstream_request, out memstream_response, reg);
			}
			catch(Exception e)
			{
				memstream_response = null;
				message = e.ToString();
				System.Diagnostics.Debug.WriteLine(e.ToString());
			}
			return result;
		}

		public bool QueryOrConfirmDataRequestSuccess(bool confirm, Guid id_user, int no, int id, out decimal total_cost, out decimal total_amount, out decimal total_month, out decimal total_topup, out string message)
		{
			total_amount = 0;
			total_cost = 0;
			total_month = 0;
			total_topup = 0;
			message = "";

			lock(lockVar)
			{
				ITransaction transaction = null;

				try
				{
					NhibernateInit();

					IList users = nhibernateSession.CreateCriteria(typeof(SoftwareRegistration.DataClasses.User)).Add(Expression.Eq("IDUser",id_user)).List();

					if(users.Count == 0)
					{
						message = "no user found";
						return false;
					}

					SoftwareRegistration.DataClasses.User user = (SoftwareRegistration.DataClasses.User)users[0];

                    DateTime fromDate = DateTime.Now.Date.AddMonths(-1);
                    DateTime toDate = DateTime.Now.Date.AddDays(1);

					//changed to date Appliance
					IList inputs = nhibernateSession.CreateCriteria(typeof(SoftwareRegistration.DataClasses.Input)).Add(Expression.Eq("User.IDUser",user.IDUser)).Add(Expression.Between("DateAppliance", fromDate, toDate)).AddOrder(new Order("DateAppliance",false)).List();

                    if (inputs.Count == 0)
                    {
                        message = "no inputs";
                        return false;
                    }

                    Input input = (Input)inputs[0];

                    DateTime outputsFromDate = input.DateAppliance.Date;

                    IList outputs = nhibernateSession.CreateCriteria(typeof(SoftwareRegistration.DataClasses.Output)).Add(Expression.Eq("User.IDUser", user.IDUser)).Add(Expression.Between("StampDateTime", outputsFromDate, toDate)).List();

					IList topups = nhibernateSession.CreateCriteria(typeof(SoftwareRegistration.DataClasses.Topup)).Add(Expression.Eq("User.IDUser",user.IDUser)).List();

                    decimal month_inputs = input.Amount;

					decimal month_outputs = 0;

					foreach(SoftwareRegistration.DataClasses.Output output in outputs)
					{
						month_outputs += output.Amount;
					}

					total_month = month_inputs - month_outputs;

					total_topup = 0;

					foreach(SoftwareRegistration.DataClasses.Topup topup in topups)
					{
						total_topup += topup.Amount;
					}

					total_amount =  total_month + total_topup;

					if(no == 0) return true;


					string service_name;

					switch(id)
					{
						case 0:
							service_name = "Ultimate Data Source COMPS";
							break;
						case 1:
							service_name = "Ultimate Data Source Property Profile";
							break;
						case 2:
							service_name = "Matching Module";
							break;
						default:
							message = "service type not supported";
							return false;
					}

					IList output_types = nhibernateSession.CreateCriteria(typeof(SoftwareRegistration.DataClasses.OutputType)).Add(Expression.Eq("Type",service_name)).List();

					if(output_types.Count == 0)
					{
						message = "service type not found in db";
						return false;
					}

					SoftwareRegistration.DataClasses.OutputType outputType = (SoftwareRegistration.DataClasses.OutputType)output_types[0];

					total_cost = outputType.Price * no;

					if(!confirm)
					{
						if(total_amount > total_cost)
						{
							return true;
						}
						else
						{
							return false;
						}
					}

					transaction = nhibernateSession.BeginTransaction(); //prije nego pocnemo stavljat u output

					decimal charge_month = total_cost;

					if(charge_month > total_month)
					{
						charge_month = total_month;
						if(charge_month < 0) charge_month = 0;
					}

					if(charge_month > 0)
					{
						SoftwareRegistration.DataClasses.Output outputEntry = new SoftwareRegistration.DataClasses.Output();

						outputEntry.User = user;
						outputEntry.OutputType = outputType;
						outputEntry.Number = no;
						outputEntry.Amount = charge_month;
						outputEntry.StampDateTime = DateTime.Now;

						nhibernateSession.Save(outputEntry);

						total_cost -= charge_month;
					}

					if(total_cost > 0)
					{
						SoftwareRegistration.DataClasses.Topup topupEntry = new SoftwareRegistration.DataClasses.Topup();

						topupEntry.User = user;
						topupEntry.OutputType = outputType;
						topupEntry.Number = no;
						topupEntry.StampDateTime = DateTime.Now;
						topupEntry.Charge = true;
						topupEntry.Amount = -total_cost;

						nhibernateSession.Save(topupEntry);
					}

					nhibernateSession.Flush();

					transaction.Commit();
				}
				catch(Exception e)
				{
					if(transaction != null) transaction.Rollback();
					NhibernateDeInit();
					System.Diagnostics.Debug.WriteLine(e.ToString());
					message = e.ToString();
					return false;
				}

			}
			return true;
		}

		//tc - total cost
		//ta - total amount
		public bool QueryDataRequestSuccess(Guid id_user, int no, int id, out int tc, out int ta, out string message)
		{
			decimal t1;
			decimal t2;
			decimal dtc;
			decimal dta;

			bool result = QueryOrConfirmDataRequestSuccess(false, id_user, no, id, out dtc, out dta, out t1, out t2, out message);

			tc = (int)dtc;
			ta = (int)dta;

			return result;
		}

		public bool QueryAcc(Guid id_user, out int month, out int topup)
		{
			decimal t1;
			decimal t2;
			string message;

			decimal dmonth;
			decimal dtopup;

			bool result = QueryOrConfirmDataRequestSuccess(false, id_user, 0, 0, out t1, out t2, out dmonth, out dtopup, out message);

			month = (int)dmonth;
			topup = (int)dtopup;

			return result;
		}

		//charge => no = amount, id = sevice id
		//id : 0 - "Ultimate Data Source COMPS"
		//id : 1 - "Ultimate Data Source Property Profile"
		//id : 2 - "Matching Module"

		////TODO - user not reg
		//		public bool ConfirmDataRequestSuccess(Guid id_registration, int no, int id, out string message)
		public bool ConfirmDataRequestSuccess(Guid id_user, int no, int id, out string message)
		{		
			decimal t1;
			decimal t2;
			decimal t3;
			decimal t4;
			return QueryOrConfirmDataRequestSuccess(true, id_user, no, id, out t1, out t2, out t3, out t4, out message);
		}

/*
		//charge => no = amount, id = sevice id
		//id : 0 - "Ultimate Data Source COMPS"
		//id : 1 - "Ultimate Data Source Property Profile"
		//id : 2 - "Matching Module"

		////TODO - user not reg
		//		public bool ConfirmDataRequestSuccess(Guid id_registration, int no, int id, out string message)
		public bool ConfirmDataRequestSuccess(Guid id_user, int no, int id, out string message)
		{
			lock(lockVar)
			{
				NhibernateInit();

				message = "";

				ITransaction transaction = null;

				try
				{
					transaction = nhibernateSession.BeginTransaction();

//										IList registrations = nhibernateSession.CreateCriteria(typeof(SoftwareRegistration.DataClasses.Registration)).Add(Expression.Eq("IDRegistration",id_registration)).List();
//
//										if(registrations.Count == 0)
//										{
//											message = "wrong id";
//											transaction.Rollback();
//											return false;
//										}
//
//										SoftwareRegistration.DataClasses.Registration registration = (SoftwareRegistration.DataClasses.Registration)registrations[0];
//
//										//if no user no one to charge
//										if(registration.User == null) return false;

					IList users = nhibernateSession.CreateCriteria(typeof(SoftwareRegistration.DataClasses.User)).Add(Expression.Eq("IDUser",id_user)).List();

					if(users.Count == 0)
					{
						message = "no user found";
						transaction.Rollback();
						return false;
					}

					SoftwareRegistration.DataClasses.Output output = new SoftwareRegistration.DataClasses.Output();

					string service_name;

					switch(id)
					{
						case 0:
							service_name = "Ultimate Data Source COMPS";
							break;
						case 1:
							service_name = "Ultimate Data Source Property Profile";
							break;
						case 2:
							service_name = "Matching Module";
							break;
						default:
							message = "service type not supported";
							transaction.Rollback();
							return false;
					}

					IList output_types = nhibernateSession.CreateCriteria(typeof(SoftwareRegistration.DataClasses.OutputType)).Add(Expression.Eq("Type",service_name)).List();

					if(output_types.Count == 0)
					{
						message = "service type not found in db";
						transaction.Rollback();
						return false;
					}

					//dodaj u output
					//					output.User = registration.User;

					output.User = (SoftwareRegistration.DataClasses.User)users[0];
					output.OutputType = (SoftwareRegistration.DataClasses.OutputType)output_types[0];
					output.Number = no;
					output.Amount = no*(((SoftwareRegistration.DataClasses.OutputType)output_types[0]).Price);
					output.StampDateTime = DateTime.Now;

					nhibernateSession.Save(output);

					nhibernateSession.Flush();

					transaction.Commit();
				}
				catch(Exception e)
				{
					message = e.ToString();
					transaction.Rollback();
					System.Diagnostics.Debug.WriteLine(e.ToString());
					NhibernateDeInit();
					return false;
				}
				return true;
			}
		}
*/
		//public
		private
			//static
			string
			Connect
			(
			byte[] memstream_request
			, out byte[] memstream_response
			, SoftwareRegistration.DataClasses.Registration reg
			)
		{
			//System.Configuration.ConfigurationManager.AppSettings[""]
			System.Reflection.Assembly   asm;
			string                       path;

			string msg = "";
			string message = "";
			memstream_response = null;

			asm = System.Reflection.Assembly.GetAssembly(typeof(RegistrationService));
			path = asm.CodeBase;
			path = path.Replace(@"file:///", "");
			path = path.Substring(0, path.LastIndexOf(@"/") + 1);

			string str_stamp = DateTime.Now.ToString("yyyy.MM.dd.HH.mm.ss.ffff");

			if(reg.KeyCode == "7E1U-02M6-R3S0-ZZ1O")
			{
				Trace.WriteLine("generating fake!");
				XmlDocument response = new XmlDocument();
				response.Load(path + @"response.xml");
				memstream_response = (Encoding.UTF8.GetBytes(response.InnerXml));
				Trace.WriteLine("generated fake!");
				message = "Fake returned!";

				return message;
			}
			msg = "Connecting " + str_stamp;
			message += msg; 

			if (null != memstream_request)
			{
				msg = "Request stream != null ";
				message += msg; 
			}
			else
			{
				msg = "Request stream == null ";
				message += msg; 

				return message;
			}

			msg = "Sending XML request";
			message += msg;

#if STAGING
            message += " http://staging.connect2data.com, ";
#endif

            // build request
            UTF8Encoding encoding = new UTF8Encoding();
			HttpWebRequest http_web_request = (HttpWebRequest)
				WebRequest.Create
				(
#if STAGING
                @"http://staging.connect2data.com"
#else
				@"https://xml.connect2data.com"
#endif
				);
			http_web_request.Credentials = new
				NetworkCredential
#if STAGING
                ("PROFGRAB","C2STAGE") // staging
#else
                //("PROFT1", "TKS1GRABR")
                    (ConfigurationManager.AppSettings["PPCOMPSLogin"], ConfigurationManager.AppSettings["PPCOMPSPwd"])
#endif
				;
			http_web_request.Method = "POST";
			http_web_request.ContentType="text/xml";

			http_web_request.ContentLength = memstream_request.Length;
			http_web_request.UserAgent = "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1)";
			http_web_request.Timeout = 300000;

			Stream request_write_stream;
			Stream response_read_stream;
			try
			{

				msg = "getting http_web_request";
				message += msg; 

				request_write_stream = http_web_request.GetRequestStream();
                Applog.Info($"Going for: {http_web_request.RequestUri}");

				msg = "got http_web_request";
				message += msg; 

				msg = "writing http_web_request write stream ";
				msg += "memstream_request.Length = " + memstream_request.Length;
				message += msg; 

				request_write_stream.Write
					(
					memstream_request
					, 0
					, memstream_request.Length
					);

				msg = " wrote http_web_request ";
				message += msg; 

				request_write_stream.Flush();
				request_write_stream.Close();
			}
			catch(WebException wex)
			{
				msg = "Exception during writting Request";
				msg += wex.Message + "@";

				msg += wex.StackTrace + "@";
				msg += wex.Status + "@";
				msg += wex.Response + "@";
				msg += wex.InnerException + "@";
				msg += wex.Source + "@";
				msg += wex.TargetSite + "@";
				message += msg; 
				//throw wex;
			}
		
			//========================================================
			HttpWebResponse http_web_response;
			memstream_response = new byte[]{};

			try
			{
				msg = " creating http_web_response ";
				message += msg; 

				http_web_response = 
					(HttpWebResponse) http_web_request.GetResponse();

				msg = " got http_web_response ";
				message += msg; 

				msg = " getting response read stream ";
				message += msg; 

				response_read_stream = http_web_response.GetResponseStream();

				msg = " got response read stream ";
				msg += "ContentLength = " + http_web_response.ContentLength;
				msg += "ContentType = " + http_web_response.ContentType;
				message += msg; 

				string str_xml;

				//=============================================================
				// version 2.
				// Pipes the stream to a higher level stream reader 
				// with the required encoding format. 
				StreamReader readStream = 
					new StreamReader (response_read_stream, Encoding.UTF8);
				str_xml = readStream.ReadToEnd();

				//=============================================================
				// convert received string into byte[]
				memstream_response = encoding.GetBytes(str_xml);
				
				//=============================================================
				msg = " got array from response read stream ";
				msg += " memstream_response.Length = " + memstream_response.Length; 
				message += msg; 

				http_web_response.Close();
				response_read_stream.Close();

			}
			catch(WebException wex)
			{
				msg = "Exception during reading Response";
				msg += wex.Message + "@";

				msg += wex.StackTrace + "@";
				msg += wex.Status + "@";
				msg += wex.Response + "@";
				msg += wex.InnerException + "@";
				msg += wex.Source + "@";
				msg += wex.TargetSite + "@";
				message += msg; 
				//throw wex;
			}
			return message;
		}

        byte[] buildRequestListView(byte[] data)
        {
            MemoryStream stream = new MemoryStream(data);
            XmlSerializer formatter = new XmlSerializer(typeof(DTD.Request.REQUESTDATA));
            stream.Position = 0;

            DTD.Request.REQUESTDATA requestData = (DTD.Request.REQUESTDATA)formatter.Deserialize(stream);

            DTD.Request.REQUEST_GROUP request = buildRequestDataListView(requestData);

#if DUMP_XML_OBJECT
            dumpObject(request, "WS_REQUEST.xml");
#endif


            XmlSerializer requestSerializer  
				= new XmlSerializer(typeof(DTD.Request.REQUEST_GROUP));

			MemoryStream requestStream = new MemoryStream();

			// IMPORTANT  -  we do not need a namespace!
			System.Xml.Serialization.XmlSerializerNamespaces nmspace = new XmlSerializerNamespaces();
			nmspace.Add("","");
			// IMPORTANT  -  we do not need a namespace!
				
			System.Xml.XmlWriter writer = new XmlTextWriter(requestStream, System.Text.UTF8Encoding.UTF8);
			//writer.Formatting = System.Xml.Formatting.Indented;
			writer.WriteStartDocument();
			//TODO: define string for request..
			writer.WriteDocType("REQUEST_GROUP", null, "C2DRequestv2.0.dtd",null);
            requestSerializer.Serialize(writer, request, nmspace);
			writer.Flush();
	
			byte[] requestArray = null;
			string resultMessage = string.Empty;

			int count =(int)requestStream.Length-3;
			requestArray = new byte[count];
			long newPos = requestStream.Seek(0, SeekOrigin.Begin);
	
			//TA++:08.04.2005
			byte[] reqStream = requestStream.ToArray();
			for (int i = 3; i < requestStream.Length; i++)
			{
				requestArray[i - 3] = (byte)reqStream[i];
			}
			//TA--:08.04.2005
			writer.Close();

            return requestArray;
        }

        private DTD.Request.REQUEST_GROUP buildRequestDataListView(DTD.Request.REQUESTDATA requestData)
        {

            DTD.Request.REQUEST_GROUP rqg
                = new DTD.Request.REQUEST_GROUP();

            rqg.REQUEST
                = new DTD.Request.REQUEST();
            rqg.REQUEST.REQUESTDATA
                = new DTD.Request.REQUESTDATA[1];

            rqg.REQUEST.REQUESTDATA[0] = requestData;

            DTD.Request._CONNECT2DATA_PRODUCT c2dp
                = new DTD.Request._CONNECT2DATA_PRODUCT();

            c2dp._DetailedComparableReport
                = DTD.Request._CONNECT2DATA_PRODUCT_DetailedComparableReport.Y;

            DTD.Request._RESPONSE_CRITERIA rescrit
                = new DTD.Request._RESPONSE_CRITERIA();

            rescrit._NumberComparablesType
                = DTD.Request._RESPONSE_CRITERIA_NumberComparablesType.Item50;
            rescrit._NumberCompFarmRecordsType
                = DTD.Request._RESPONSE_CRITERIA_NumberCompFarmRecordsType.Item100;
            rescrit._NumberSubjectPropertiesType
                = DTD.Request._RESPONSE_CRITERIA_NumberSubjectPropertiesType.Item100;

            DTD.Request._LAND_USE lu = new DTD.Request._LAND_USE();

            rqg.REQUESTING_PARTY
                = new DTD.Request.REQUESTING_PARTY[1];

            rqg.MISMOVersionID = "2.1";

            rqg.REQUEST.LoginAccountIdentifier = ConfigurationManager.AppSettings["PPCOMPSLogin"];
			//rqg.REQUEST.LoginAccountPassword="GRABR1"; 
			rqg.REQUEST.LoginAccountPassword = ConfigurationManager.AppSettings["PPCOMPSPwd"];

#if STAGING
            rqg.REQUEST.LoginAccountIdentifier = "PROFGRAB";
            rqg.REQUEST.LoginAccountPassword = "C2STAGE";
            Applog.Info($"SetStaging Login/pwd: {rqg.REQUEST.LoginAccountIdentifier} / {rqg.REQUEST.LoginAccountPassword}");
#endif


            rqg.REQUEST._JobIdentifier = "TEST MASTER V2.0";
            rqg.REQUEST._RecordIdentifier = "12345A8";
            rqg.REQUEST._HVERequestTypeSpecified = true;
            rqg.REQUEST._HVERequestType
                = DTD.Request.REQUEST_HVERequestType.Item02;
            rqg.REQUEST._HVCustomerIdentifier = "";

            return rqg;
        }

        enum LoginID
        {
            COMPS,
            MM
        }

#region . getSubject .
        private byte[] getSubjectCOMPS(byte[] data, LoginID loginID)
        {
            DTD.Request.REQUEST_GROUP request = buildRequestDataCOMPS(data, loginID);

#if DUMP_XML_OBJECT
            dumpObject(request, "WS_REQUEST.xml");
#endif

            XmlSerializer requestSerializer
                = new XmlSerializer(typeof(DTD.Request.REQUEST_GROUP));

            MemoryStream requestStream = new MemoryStream();

            // IMPORTANT  -  we do not need a namespace!
            System.Xml.Serialization.XmlSerializerNamespaces nmspace = new XmlSerializerNamespaces();
            nmspace.Add("", "");
            // IMPORTANT  -  we do not need a namespace!

            System.Xml.XmlWriter writer = new XmlTextWriter(requestStream, System.Text.UTF8Encoding.UTF8);
            //writer.Formatting = System.Xml.Formatting.Indented;
            writer.WriteStartDocument();
            //TODO: define string for request..
            writer.WriteDocType("REQUEST_GROUP", null, "C2DRequestv2.0.dtd", null);
            requestSerializer.Serialize(writer, request, nmspace);
            writer.Flush();

            //requestSerializer.Serialize(requestStream, request);
            byte[] requestArray = null;


            int count = (int)requestStream.Length - 3;
            requestArray = new byte[count];
            long newPos = requestStream.Seek(0, SeekOrigin.Begin);

            //TA++:08.04.2005
            byte[] reqStream = requestStream.ToArray();
            for (int i = 3; i < requestStream.Length; i++)
            {
                requestArray[i - 3] = (byte)reqStream[i];
            }
            //TA--:08.04.2005
            writer.Close();

            return requestArray;
        }
#endregion



#region . Build Request .
        private DTD.Request.REQUEST_GROUP buildRequestDataCOMPS(byte[] data, LoginID loginID)
        {
            MemoryStream stream = new MemoryStream(data);
            XmlSerializer formatter = new XmlSerializer(typeof(DTD.Request.REQUESTDATA));
            stream.Position = 0;

            DTD.Request.REQUESTDATA requestData = (DTD.Request.REQUESTDATA)formatter.Deserialize(stream);

            DTD.Request.REQUEST_GROUP rqg = new DTD.Request.REQUEST_GROUP();

            rqg.REQUEST = new DTD.Request.REQUEST();
            rqg.REQUEST.REQUESTDATA = new DTD.Request.REQUESTDATA[1];
            rqg.REQUEST.REQUESTDATA[0] = requestData;

            rqg.REQUESTING_PARTY = new DTD.Request.REQUESTING_PARTY[1];

            rqg.MISMOVersionID = "2.1";

            switch (loginID)
            {
                case LoginID.COMPS:
                    rqg.REQUEST.LoginAccountIdentifier = ConfigurationManager.AppSettings["PPCOMPSLogin"]; 
					//rqg.REQUEST.LoginAccountPassword="GRABR1"; 
					rqg.REQUEST.LoginAccountPassword = ConfigurationManager.AppSettings["PPCOMPSPwd"];
                    break;
                case LoginID.MM:
                    rqg.REQUEST.LoginAccountIdentifier = ConfigurationManager.AppSettings["MMLogin"];
                    rqg.REQUEST.LoginAccountPassword = ConfigurationManager.AppSettings["MMPwd"];
                    break;
            }

            rqg.REQUEST._JobIdentifier = "TEST MASTER V2.0";
            rqg.REQUEST._RecordIdentifier = "12345A8";
            rqg.REQUEST._HVERequestTypeSpecified = true;
            rqg.REQUEST._HVERequestType
                = DTD.Request.REQUEST_HVERequestType.Item02;
            rqg.REQUEST._HVCustomerIdentifier = "";

            return rqg;
        }
#endregion


#endregion Data Consumption

		public string TestConnection()
		{
			lock(lockVar)
			{
				string err = NhibernateInit();
				if(nhibernateSession != null) return "connection ok";
				return "connection failed: " + err;
			}
		}

        private void dumpObject(object theObject, string filename)
        {

            System.Xml.Serialization.XmlSerializer serializer
                = new System.Xml.Serialization.XmlSerializer(theObject.GetType());

            // IMPORTANT  -  we do not need a namespace!
            System.Xml.Serialization.XmlSerializerNamespaces nmspace = new XmlSerializerNamespaces();
            nmspace.Add("", "");
            // IMPORTANT  -  we do not need a namespace!

            System.Xml.XmlTextWriter writer = new XmlTextWriter(@"C:\Logs\" + filename, System.Text.UTF8Encoding.UTF8);
            writer.Formatting = System.Xml.Formatting.Indented;
            writer.WriteStartDocument();
            writer.WriteDocType("REQUEST_GROUP", null, "C2DRequestv2.0.PropertyProfileComps", null);
            serializer.Serialize(writer, theObject, nmspace);
            writer.Flush();
            writer.Close();

        }
	}
}
