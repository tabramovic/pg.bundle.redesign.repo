﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;

//Added
using PGWSE.Communicator;

namespace WS_Enhancement
{
    public class Global : System.Web.HttpApplication
    {        
        protected void Application_Start(object sender, EventArgs e)
        {
            string connString = ConfigurationSettings.AppSettings["ConnString"];
            string dataPassword = ConfigurationSettings.AppSettings["Password"];
            InputsManager.Instance.InitConnection(connString);
            InputsManager.Instance.DataPassword = dataPassword;
        }

        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {

        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
    }
}