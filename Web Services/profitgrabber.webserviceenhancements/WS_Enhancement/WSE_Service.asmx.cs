﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;

//Added
using PGWSE.Communicator;


namespace WS_Enhancement
{
    /// <summary>
    /// Summary description for Service1
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ToolboxItem(false)]
    public class WSE_Service : System.Web.Services.WebService
    {

        [WebMethod]
        public string TestIfAlive()
        {
            return "Alive @" + DateTime.Now.ToShortDateString() + ", " + DateTime.Now.ToShortTimeString();
        }

        [WebMethod]
        public string TestConnection()
        {
            return InputsManager.Instance.GetConnectionState();
        }

        [WebMethod]
        public ReturnObject[] GetInputs(Guid pgId)
        {
            return InputsManager.Instance.GetInputs(pgId);
        }

        [WebMethod]
        public ReturnObject[] GetTopUps(Guid pgId)
        {
            return InputsManager.Instance.GetTopUps(pgId);
        }

        [WebMethod]
        public bool InsertInputs(string password, Guid pgId, DateTime startingDate, int duration, int amount)
        {
            if (password != InputsManager.Instance.DataPassword)
                return false;

            return InputsManager.Instance.InsertInputs(pgId, startingDate, duration, amount);
        }

        [WebMethod]
        public bool InsertTopUps(string password, Guid pgId, int amount)
        {
            if (password != InputsManager.Instance.DataPassword)
                return false;

            return InputsManager.Instance.InsertTopUps(pgId, amount);
        }
    }
}
