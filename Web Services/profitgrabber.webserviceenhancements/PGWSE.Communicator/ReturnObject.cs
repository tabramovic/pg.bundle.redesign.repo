﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PGWSE.Communicator
{
    [Serializable]
    public class ReturnObject
    {
        string _timeStamp;
        string _value;

        public ReturnObject()
        { }

        public ReturnObject(string timeStamp, string value)
        {
            _timeStamp = timeStamp;
            _value = value;
        }

        public string TimeStamp
        {
            get { return _timeStamp; }
            set { _timeStamp = value; }
        }

        public string Val
        {
            get { return _value; }
            set { _value = value; }
        }
    }
}
