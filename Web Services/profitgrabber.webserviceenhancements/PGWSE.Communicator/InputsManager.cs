﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace PGWSE.Communicator
{
    [Serializable]
    public sealed class InputsManager
    {
        //SINGLETON
        static readonly InputsManager _instance = new InputsManager();

        static InputsManager()
        { }

        InputsManager()
        {            
        }

        public static InputsManager Instance
        {
            get { return _instance; }
        }


        //SQL CONN DATA
        SqlConnection _connection = null;
        string _dataPassword = string.Empty;
        
        public void InitConnection(string connString)
        {            
            _connection = new SqlConnection(connString);

            try { _connection.Open(); }
            catch { _connection = null; }
        }

        public void CloseConnection()
        {
            if (null != _connection && ConnectionState.Open == _connection.State)
            {
                try { _connection.Close(); }
                catch { }
            }
        }

        public string GetConnectionState()
        {
            return _connection.State.ToString();
        }

        public string DataPassword
        {
            get { return _dataPassword; }
            set { _dataPassword = value; }
        }

        //QUERIES

        public ReturnObject[] GetInputs(Guid pgId)
        {
            /*SQL samples for query!
             *
                SELECT StampDateTime, DateAppliance, Amount 
                FROM Inputs 
                WHERE IDUser='6562792c-20dd-4f0f-9873-587b91b97736'
                ORDER BY StampDateTime desc
             * 
             * 
             * */
            List<ReturnObject> retList = new List<ReturnObject>();

            string getInputsQuery = "SELECT StampDateTime, DateAppliance, Amount FROM Inputs WHERE IDUser='" + pgId.ToString() + "' ORDER BY StampDateTime desc";
            SqlCommand getInputsCommand = _connection.CreateCommand();
            getInputsCommand.CommandText = getInputsQuery;

            SqlDataReader getInputsReader = null;
            try 
            {
                getInputsReader = getInputsCommand.ExecuteReader();
                while (getInputsReader.Read())
				{
                    retList.Add(new ReturnObject(getInputsReader["StampDateTime"].ToString(), getInputsReader["Amount"].ToString()));
                    //Console.WriteLine("\t{0}\t{1}\t{2}", getInputsReader["StampDateTime"], getInputsReader["DateAppliance"], getInputsReader["Amount"]);
				}
                getInputsReader.Close();
			}
			catch (SqlException e)
			{
				//Console.WriteLine(e.Message);
			}

            return retList.ToArray();
        }

        public ReturnObject[] GetTopUps(Guid pgId)
        {
            /*
            * 
               select StampDateTime, Amount, Number, Charge, IDType 
               from TopUps 
               where IDType is null and 
               IDUser='6562792c-20dd-4f0f-9873-587b91b97736
            * 
            */

            List<ReturnObject> retList = new List<ReturnObject>();
            string getTopUpsQuery = "SELECT StampDateTime, Amount, Number, Charge, IDType FROM TopUps WHERE IDType is null and IDUser='" + pgId.ToString() + "' ORDER BY StampDateTime desc";
            SqlCommand getTopUpsCommand = _connection.CreateCommand();
            getTopUpsCommand.CommandText = getTopUpsQuery;

            SqlDataReader getTopUpsReader = null;
            try
            {
                getTopUpsReader = getTopUpsCommand.ExecuteReader();
                while (getTopUpsReader.Read())
                {
                    retList.Add(new ReturnObject(getTopUpsReader["StampDateTime"].ToString(), getTopUpsReader["Amount"].ToString()));
                    //Console.WriteLine("\t{0}\t{1}\t{2}\t{3}\t{4}", getTopUpsReader["StampDateTime"], getTopUpsReader["Amount"], getTopUpsReader["Number"], getTopUpsReader["Charge"], getTopUpsReader["IDType"]);
                }
                getTopUpsReader.Close();
            }
            catch (SqlException e)
            {
                //Console.WriteLine(e.Message);
            }

            return retList.ToArray();
        }


        public bool InsertInputs(Guid pgId, DateTime startingDate, int duration, int amount)
        {
            for (int i = 0; i < duration; i++)
            {
                DateTime newDate = startingDate.AddMonths(i);
                string insertInputsStatement = "INSERT INTO Inputs VALUES('" + Guid.NewGuid().ToString() + "', " + "'" + pgId.ToString() + "', " + "'" + newDate.ToShortDateString() + "'," + "'" + newDate.ToShortDateString() + "'," + amount + ")";

                SqlCommand insertInputsCommand = _connection.CreateCommand();
                insertInputsCommand.CommandText = insertInputsStatement;

                try { insertInputsCommand.ExecuteNonQuery(); }
                catch (Exception exc){ return false; }                
            }

            return true;
        }

        public bool InsertTopUps(Guid pgId, int amount)
        {
                        
            string insertTopUpStatement = "INSERT INTO TopUps VALUES('" + Guid.NewGuid().ToString() + "', " + "'" + pgId.ToString() + "', " + "'" + DateTime.Now.ToShortDateString() + "'," + amount + ", 0, 0, NULL)";

            SqlCommand insertTopUpCommand = _connection.CreateCommand();
            insertTopUpCommand.CommandText = insertTopUpStatement;

            try { insertTopUpCommand.ExecuteNonQuery(); }
            catch (Exception exc) { return false; }
            

            return true;
        }
    }
}
