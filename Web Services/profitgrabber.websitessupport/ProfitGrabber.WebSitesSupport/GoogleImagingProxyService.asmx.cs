﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Services;

//Added
using System.IO;
using System.Drawing;
using System.Configuration;
using GoogleAPI;


namespace ProfitGrabber.WebSitesSupport
{
    /// <summary>
    /// Summary description for GoogleImagingProxyService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    public class GoogleImagingProxyService : System.Web.Services.WebService
    {
        string _apiKey = ConfigurationManager.AppSettings["GoogleAPIKey"];

        [WebMethod]
        public string TestIfAlive()
        {
            DateTime timeStamp = DateTime.Now;
            return "Alive @SRV Time: " + timeStamp.ToShortDateString() + ", " + timeStamp.ToLongTimeString();
        }

        [WebMethod]
        public bool GetGeoCode(string address, out bool correctAddress, out string err, out double longtitude, out double latitude)
        {
            return new AddressToGeoCodeConverter().GetGeoCode(address, out correctAddress, out err, out longtitude, out latitude, _apiKey);
        }

        [WebMethod]
        public byte[] GetImage(MapType imgType, int zoom, int width, int height, double longtitude, double latitude, int heading)
        {
            Image img = new StreetViewImage().GetImage(imgType, zoom, width, height, longtitude, latitude, heading, _apiKey);

            MemoryStream ms = new MemoryStream();
            img.Save(ms,System.Drawing.Imaging.ImageFormat.Png);
            return  ms.ToArray();            
        }

        [WebMethod]
        public byte[] GetImageFromAddress(MapType imgType, int zoom, int width, int height, string address, int heading, out bool correctAddress)
        {            
            string err;
            double longtitude;
            double latitude;

            bool res = GetGeoCode(address, out correctAddress, out err, out longtitude, out latitude);
            
            if (correctAddress)
            {
                Image img = new StreetViewImage().GetImage(imgType, zoom, width, height, longtitude, latitude, heading, _apiKey);

                MemoryStream ms = new MemoryStream();
                img.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
                return ms.ToArray();
            }
            else
                return null;            
        }

        [WebMethod]
        public byte[] GetImageWithMarkers(MapType mapType, int zoom, int imgWidth, int imgHeight, List<string> addresses, out bool anyAddressFound)
        {            
            Image img = new StreetViewImage().GetImageWithMarkers(mapType, zoom, imgWidth, imgHeight, addresses, _apiKey, out anyAddressFound);

            if (null != img)
            {
                MemoryStream ms = new MemoryStream();
                img.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
                return ms.ToArray();
            }
            else
                return null;
        }
    }
}
