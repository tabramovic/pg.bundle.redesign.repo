﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Services;
using System.Configuration;
using System.Data;

//Added
using ProfitGrabber.WebSitesSupport.DALC;

namespace ProfitGrabber.WebSitesSupport
{
    /// <summary>
    /// Summary description for Service1
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    public class WebProxyService : System.Web.Services.WebService
    {
        static readonly log4net.ILog applog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        

        [WebMethod]
        public string TestIfAlive()
        {
            applog.Info("TestIfAlive");
            return DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString();
        }

        #region TEST CONNECTION
        [WebMethod]
        public string TestAAHConnection()
        {
            if (null != AAHConnection.Instance.Connection && ConnectionState.Open == AAHConnection.Instance.Connection.State)
                return "Connection Opened!";
            else
                return AAHConnection.Instance.ConnError;
        }

        [WebMethod]
        public string TestNPCConnection()
        {
            if (null != NPCConnection.Instance.Connection && ConnectionState.Open == NPCConnection.Instance.Connection.State)
                return "Connection Opened!";
            else
                return NPCConnection.Instance.ConnError;
        }

        [WebMethod]
        public string TestACOKConnection()
        {
            if (null != ACOKConnection.Instance.Connection && ConnectionState.Open == ACOKConnection.Instance.Connection.State)
                return "Connection Opened!";
            else
                return ACOKConnection.Instance.ConnError;
        }

        [WebMethod]
        public string TestFFConnection()
        {
            string connError = string.Empty;

            if (FFConnection.Instance.TestConnection(out connError))
                return "Connection Opened!";
            else
                return connError;
        }

        [WebMethod]
        public string TestPGDB3Connection()
        {
            if (null != PGDB3Connection.Instance.Connection && ConnectionState.Open == ACOKConnection.Instance.Connection.State)
                return "Connection Opened!";
            else
                return PGDB3Connection.Instance.ConnError;
        }
        #endregion

        #region GET AAH ENTRIES (All American Homebuyers)
        [WebMethod]
        public List<List<string>> GetAAHEntries(Guid pgId, long ticks)
        {
            //return new List<List<string>>();
            //return AAHConnection.Instance.GetEntries(pgId, ticks);
            return GetAAHEntries_V2(pgId, ticks, Guid.NewGuid());
        }

        [WebMethod]
        public List<List<string>> GetAAHEntries_V2(Guid pgId, long ticks, Guid uniqueTag)
        {            
            List<List<string>> retVal = AAHConnection.Instance.GetEntries_V2(pgId, ticks, applog);            
            return retVal;                        
        }

        [WebMethod]
        public List<List<string>> GetAAHEntries_V3(Guid pgId, long ticks, Guid uniqueTag)
        {
            applog.Info("Get AAH V3");
            DateTime dt = new DateTime(ticks, DateTimeKind.Unspecified);
            applog.Info("pgId: " + pgId.ToString() + ", ticks: " + ticks.ToString() + ", ticks2DateTime: " + dt.ToLongDateString() + " " + dt.ToLongTimeString() + ", uniqueTag: " + uniqueTag.ToString());

            string info;
            List<List<string>> retVal = AAHConnection.Instance.GetEntries_V3(pgId, ticks, uniqueTag, out info);

            applog.Info("INFO:" + info);
            applog.Info("Got " + (null != retVal ? retVal.Count.ToString() : "null!?"));

            List<string> error = new List<string>();

            if (null == retVal || 0 == retVal.Count)
                return retVal;

            List<List<string>> errVal = new List<List<string>>();
            bool validData = CheckReturnData(pgId, uniqueTag, retVal, out errVal);

            applog.Info("Valid:" + validData.ToString());            

            if (validData)
                return retVal;
            else
                return errVal;

        }
        #endregion        

        #region GET NPC ENTRIES (National Prevention Center)
        [WebMethod]
        public List<List<string>> GetNPCEntries(Guid pgId, long ticks)
        {
            //return new List<List<string>>();
            //return NPCConnection.Instance.GetEntries(pgId, ticks);
            return GetNPCEntries_V2(pgId, ticks, Guid.NewGuid());
        }

        [WebMethod]
        public List<List<string>> GetNPCEntries_V2(Guid pgId, long ticks, Guid uniqueTag)
        {            
            List<List<string>> retVal = NPCConnection.Instance.GetEntries_V2(pgId, ticks, applog);                        
            return retVal;
        }
        #endregion

        #region GET ACOK ENTRIES (Any Credit OK)
        [WebMethod]
        public List<List<string>> GetACOKEntries(Guid pgId, long ticks)
        {
            //return new List<List<string>>();
            //return ACOKConnection.Instance.GetEntries(pgId, ticks);
            return GetACOKEntries_V2(pgId, ticks, Guid.NewGuid());
        }

        [WebMethod]
        public List<List<string>> GetACOKEntries_V2(Guid pgId, long ticks, Guid uniqueTag)
        {            
            List<List<string>> retVal = ACOKConnection.Instance.GetEntries_V2(pgId, ticks, applog);            
            return retVal;
        }
        #endregion

        #region GET CB ENTRIES (Cash Buyers)
        [WebMethod]
        public List<List<string>> GetCBEntries(Guid pgId, long ticks)
        {
            //return new List<List<string>>();
            //return FFConnection.Instance.GetCBEntries(pgId, ticks);
            return GetCBEntries_V2(pgId, ticks, Guid.NewGuid());
        }

        [WebMethod]
        public List<List<string>> GetCBEntries_V2(Guid pgId, long ticks, Guid uniqueTag)
        {            
            List<List<string>> retVal = FFConnection.Instance.GetCBEntries_V2(pgId, ticks, applog);            
            return retVal;
        }
        #endregion

        #region GET PL ENTRIES (Private Lenders)
        [WebMethod]
        public List<List<string>> GetPLEntries(Guid pgId, long ticks)
        {
            //return new List<List<string>>();
            //return FFConnection.Instance.GetPLEntries(pgId, ticks);
            return GetPLEntries_V2(pgId, ticks, Guid.NewGuid());
        }

        [WebMethod]
        public List<List<string>> GetPLEntries_V2(Guid pgId, long ticks, Guid uniqueTag)
        {            
            List<List<string>> retVal = FFConnection.Instance.GetPLEntries_V2(pgId, ticks, applog);            
            return retVal;
        }
        #endregion

        #region GET VC/PL ENTRIES (Voice Connect, Pat Live)
        [WebMethod]
        public List<List<string>> GetPGDB3Entries(Guid pgId, long ticks)
        {
            //return new List<List<string>>();
            //return PGDB3Connection.Instance.GetEntries(pgId, ticks);
            return GetPGDB3Entries_V2(pgId, ticks, Guid.NewGuid());
        }

        [WebMethod]
        public List<List<string>> GetPGDB3Entries_V2(Guid pgId, long ticks, Guid uniqueTag)
        {            
            List<List<string>> retVal = PGDB3Connection.Instance.GetEntries_V2(pgId, ticks, applog);            
            return retVal;            
        }
        #endregion

        #region TESTERS
        [WebMethod]
        public List<List<string>> X_AAH_Tester()
        {
            return GetAAHEntries(new Guid("9C7653F0-EC65-4F4A-BFDD-CD892AC3256E"), new DateTime(2000, 1, 1).Ticks);
        }

        [WebMethod]
        public List<List<string>> X_AAH_Tester_V2()
        {
            return GetAAHEntries_V2(new Guid("50303295-5047-41d4-92cc-b431f0b74f33"), new DateTime(2000, 1, 1).Ticks, Guid.NewGuid());
        }

        [WebMethod]
        public List<List<string>> X_NPC_Tester()
        {
            return GetNPCEntries(new Guid("d6c1e0a8-93c6-40a6-bbdd-190b0e36dedd"), new DateTime(2000, 1, 1).Ticks);            
        }

        [WebMethod]
        public List<List<string>> X_NPC_Tester_V2()
        {
            return GetNPCEntries_V2(new Guid("d88ffc68-610c-4688-b892-2a04c1783228"), new DateTime(2000, 1, 1).Ticks, Guid.NewGuid());
        }

        [WebMethod]
        public List<List<string>> X_ACOK_Tester()
        {
            return GetACOKEntries(new Guid("4a7979d9-0b5f-4bcc-ac2b-34fb824ac5d5"), new DateTime(2019, 12, 17, 16, 4, 24).Ticks);
        }

        [WebMethod]
        public List<List<string>> X_ACOK_Tester_V2()
        {
            return GetACOKEntries_V2(new Guid("4a7979d9-0b5f-4bcc-ac2b-34fb824ac5d5"), new DateTime(2019, 12, 17, 16, 4, 24).Ticks, Guid.NewGuid());
        }

        [WebMethod]
        public List<List<string>> X_CB_Tester()
        {
            return GetCBEntries(new Guid("21db475a-3b5a-4816-b0e5-8f263e7bebf0"/*"9C7653F0-EC65-4F4A-BFDD-CD892AC3256E"*/), new DateTime(2013, 11, 11).Ticks);
        }

        [WebMethod]
        public List<List<string>> X_CB_Tester_V2()
        {
            return GetCBEntries_V2(new Guid("21db475a-3b5a-4816-b0e5-8f263e7bebf0"/*"9C7653F0-EC65-4F4A-BFDD-CD892AC3256E"*/), new DateTime(2013, 11, 11).Ticks, Guid.NewGuid());
        }

        [WebMethod]
        public List<List<string>> X_PL_Tester()
        {
            return GetPLEntries(new Guid("9C7653F0-EC65-4F4A-BFDD-CD892AC3256E"), new DateTime(2000, 1, 1).Ticks);
        }

        [WebMethod]
        public List<List<string>> X_PL_Tester_V2()
        {
            return GetPLEntries_V2(new Guid("9C7653F0-EC65-4F4A-BFDD-CD892AC3256E"), new DateTime(2000, 1, 1).Ticks, Guid.NewGuid());
        }

        [WebMethod]
        public List<List<string>> X_VCPL_Tester()
        {
            return GetPGDB3Entries(new Guid("78B56B7E-E381-481D-9229-EB957E1C0301"), new DateTime(2000, 1, 1).Ticks);
        }

        [WebMethod]
        public List<List<string>> X_CVPL_Tester_V2()
        {
            return GetPGDB3Entries_V2(new Guid("78B56B7E-E381-481D-9229-EB957E1C0301"), new DateTime(2000, 1, 1).Ticks, Guid.NewGuid());
        }
        #endregion

        #region CHECK RETURN DATA
        bool CheckReturnData(Guid pgId, Guid uniqueTag, List<List<string>> retVal, out List<List<string>> errVal)
        {
            List<string> error = new List<string>();
            errVal = new List<List<string>>();

            bool allOK = true;
            foreach (List<string> retRow in retVal)
            {
                try
                {
                    if (retRow[retRow.Count - 1].ToUpper() != uniqueTag.ToString().ToUpper() ||
                        retRow[retRow.Count - 2].ToUpper() != pgId.ToString().ToUpper())
                    {
                        allOK = false;
                    }
                }
                catch (Exception exc)
                {
                    allOK = false;
                    error.Add("ERROR");
                    error.Add(exc.Message);
                    error.Add(exc.InnerException.ToString());
                    error.Add(exc.Source);
                }
            }

            if (allOK)
                return true;
            else
            {
                retVal = new List<List<string>>();
                retVal.Add(error);

                return false;
            }
        }
        #endregion
    }
}
