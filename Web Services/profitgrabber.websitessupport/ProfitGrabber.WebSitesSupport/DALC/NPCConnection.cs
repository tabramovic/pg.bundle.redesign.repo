﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using MySql.Data.MySqlClient;

namespace ProfitGrabber.WebSitesSupport.DALC
{
    public sealed class NPCConnection
    {
        static readonly NPCConnection _instance = new NPCConnection();

        static NPCConnection()
        {}

        NPCConnection()
        {
            InitConnection(out _connError);
        }

        public static NPCConnection Instance
        {
            get { return _instance; }
        }

        MySqlConnection _conn = null;
        string _connError = string.Empty;

        public MySqlConnection Connection
        {
            get { return _conn; }
        }

        public string ConnError
        {
            get { return _connError; }
        }

        bool InitConnection(out string error)
        {
            error = string.Empty;

            _conn = new MySqlConnection();
            _conn.ConnectionString = ConfigurationManager.ConnectionStrings["NPCConnString"].ConnectionString;

            try
            {
                _conn.Open();
            }
            catch (Exception exc)
            {
                error = exc.Message + Environment.NewLine + exc.InnerException + Environment.NewLine + exc.Source;
                return false;
            }

            return true;
        }

        public List<List<string>> GetEntries(Guid pgId, long ticks)
        {
            MySqlDataReader dataReader = null;
            List<List<string>> resList = new List<List<string>>();

            try
            {
                var cmd = new MySqlCommand("GetNPCObject", _conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("@ProfitGrabberID", pgId));
                cmd.Parameters.Add(new SqlParameter("@LeadDate", new DateTime(ticks, DateTimeKind.Unspecified)));

                dataReader = cmd.ExecuteReader();

                while (dataReader.Read())
                {
                    List<string> entry = new List<string>();

                    entry.Add(dataReader["Firstname"].ToString());  //0
                    entry.Add(dataReader["LastName"].ToString());   //1
                    entry.Add(dataReader["Email"].ToString());  //2
                    entry.Add(dataReader["City"].ToString());  //3
                    entry.Add(dataReader["StateID"].ToString());  //4
                    entry.Add(dataReader["CountyID"].ToString());    //5
                    entry.Add(dataReader["MemberID"].ToString());  //6
                    entry.Add(dataReader["HomePhone"].ToString());    //7
                    entry.Add(dataReader["TimeToHome"].ToString());       //8
                    entry.Add(dataReader["WorkPhone"].ToString());    //9
                    entry.Add(dataReader["TimeToWork"].ToString());      //10
                    entry.Add(dataReader["Pager"].ToString());        //11
                    entry.Add(dataReader["Goal"].ToString());       //12
                    entry.Add(dataReader["HowHear"].ToString());       //13
                    entry.Add(dataReader["V_Created"].ToString());    //14    
                    entry.Add(dataReader["DomainId"].ToString());   //15
                    entry.Add(dataReader["P_ID"].ToString());   //16
                    entry.Add(dataReader["VisitorID"].ToString());    //17
                    entry.Add(dataReader["Bedrooms"].ToString());       //18
                    entry.Add(dataReader["Bathrooms"].ToString());  //19
                    entry.Add(dataReader["Parking"].ToString());   //20
                    entry.Add(dataReader["Pool"].ToString());      //21
                    entry.Add(dataReader["YearBuilt"].ToString());       //22
                    entry.Add(dataReader["Basement"].ToString());       //23
                    entry.Add(dataReader["Condition"].ToString());          //24
                    entry.Add(dataReader["Square"].ToString());              //25
                    entry.Add(dataReader["Occupied"].ToString());             //26
                    entry.Add(dataReader["Listed"].ToString());                //27
                    entry.Add(dataReader["ListingExpire"].ToString());          //28
                    entry.Add(dataReader["CancelListing"].ToString());              //29
                    entry.Add(dataReader["ResidentStatus"].ToString());              //30
                    entry.Add(dataReader["F_ID"].ToString());              //31
                    entry.Add(dataReader["F_VisitorID"].ToString());              //32
                    entry.Add(dataReader["ForeclosureNotice"].ToString());              //33
                    entry.Add(dataReader["ForeclosureDate"].ToString());              //34
                    entry.Add(dataReader["SaleDate"].ToString());              //35
                    entry.Add(dataReader["FirstLoan"].ToString());              //36
                    entry.Add(dataReader["SecondLoan"].ToString());              //37
                    entry.Add(dataReader["ThirdLoan"].ToString());              //38
                    entry.Add(dataReader["TotalPayments"].ToString());              //39
                    entry.Add(dataReader["Bankruptcy"].ToString());              //40
                    entry.Add(dataReader["Situation"].ToString());              //41
                    entry.Add(dataReader["ApproxMonthlyPayment1"].ToString());              //42
                    entry.Add(dataReader["ApproxMonthlyPayment2"].ToString());              //43
                    entry.Add(dataReader["ApproxMonthlyPayment3"].ToString());              //44
                    entry.Add(dataReader["Total"].ToString());              //45
                    entry.Add(dataReader["Worth"].ToString());              //46
                    entry.Add(dataReader["Net"].ToString());              //47
                    entry.Add(dataReader["M_ID"].ToString());              //48
                    entry.Add(dataReader["ProfitGrabberID"].ToString());              //49
                    entry.Add(dataReader["Title"].ToString());              //50 
                    entry.Add(DBNull.Value != dataReader["V_Created"] ? ((DateTime)dataReader["V_Created"]).Ticks.ToString() : string.Empty);   //51
                    

                    resList.Add(entry);
                }
            }
            catch (Exception exc)
            {
                bool stopHere = true;
            }
            finally 
            {
                if (null != dataReader)
                    dataReader.Close();
            }

            return resList;
        }

        public List<List<string>> GetEntries_V2(Guid pgId, long ticks, log4net.ILog appLog/*, Guid uniqueTag*/)
        {
            MySqlDataReader dataReader = null;
            List<List<string>> resList = new List<List<string>>();

            string connectionString = ConfigurationManager.ConnectionStrings["NPCConnString"].ConnectionString;

            using (var conn = new MySqlConnection(connectionString))
            {
                try
                {
                    #region COMMENTED OUT
                    /*SqlCommand cmd = new SqlCommand("GetNPCObject_V2", _conn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(new SqlParameter("@ProfitGrabberID", pgId));
                    cmd.Parameters.Add(new SqlParameter("@LeadDate", new DateTime(ticks, DateTimeKind.Unspecified)));
                    cmd.Parameters.Add(new SqlParameter("@UniqueTag", uniqueTag));

                    dataReader = cmd.ExecuteReader();*/
                    #endregion

                    DateTime dt = new DateTime(ticks, DateTimeKind.Unspecified);
                    //string strDt = dt.ToString("yyyy/MM/dd hh:mm:ss tt");
                    string strDt = dt.ToString("yyyy/MM/dd HH:mm:ss");
                    appLog.Info("=====================");
                    appLog.Info("NPC (GetEntries_V2, left join Properties): " + pgId.ToString() + ", since: " + strDt + " [" + ticks.ToString() + "]");

                    conn.Open();

                    //string sqlStatement = "SELECT V.[Firstname],V.[LastName],V.[Email],V.[City],S.Abbr,V.[CountyID],V.[MemberID],V.[HomePhone],V.[TimeToHome],V.[WorkPhone],V.[TimeToWork],V.[Pager],V.[Goal] ,V.[HowHear],V.[Created] as V_Created,V.[DomainId],P.[ID] as P_ID,P.[VisitorID],P.[Bedrooms],P.[Bathrooms],P.[Parking],P.[Pool],P.[YearBuilt],P.[Basement],P.[Condition],P.[Square],P.[Occupied],P.[Listed],P.[ListingExpire],P.[CancelListing],P.[ResidentStatus],F.[ID] as F_ID,F.[VisitorID] as F_VisitorID,F.[ForeclosureNotice],F.[ForeclosureDate],F.[SaleDate],F.[FirstLoan],F.[SecondLoan],F.[ThirdLoan],F.[TotalPayments],F.[Bankruptcy],F.[Situation],F.[ApproxMonthlyPayment1],F.[ApproxMonthlyPayment2],F.[ApproxMonthlyPayment3],F.[Total],F.[Worth],F.[Net],M.[ID] as M_ID,MPG.ProfitGrabberID ,D.Title ";
                    //sqlStatement += " FROM Visitor as V left join Property as P on P.VisitorID = V.ID and V.Created > '" + strDt + "' ";
                    //sqlStatement += " inner join Foreclosure as F on F.VisitorID = V.ID left join Member as M on M.ID = V.MemberId inner join MemberProfitGrabber as MPG on MPG.MemberId = M.ID and MPG.ProfitGrabberId = '" + pgId.ToString() + "' ";
                    //sqlStatement += " left join Domain as D on D.ID = V.DomainID left join State as S on S.ID = V.StateID";
                    //sqlStatement += " where V.Created > '" + strDt + "'";

                    string sqlStatement = "SELECT ";
                    sqlStatement += " v.`FirstName` as 'Firstname', v.`LastName`, v.`Email`, vp.`City`, ";
                    sqlStatement += " s.`Title` as 'Abbr', v.`CountyID`, m.`Id` as 'MemberID', v.`HomePhone`, ";
                    sqlStatement += " vp.`TimeToHome`, v.`WorkPhone`, vp.`TimeToWork`, vp.`Pager`, vp.`Goal`, ";
                    sqlStatement += " vp.`HowHear`, DATE_FORMAT(vp.`Created`, '%Y-%m-%d %T.000') AS 'V_Created', v.`DomainID`, vp.`ID` AS 'P_ID', vp.`VisitorID`, ";
                    sqlStatement += " vp.`Bedrooms`, vp.`Bathrooms`, vp.`Parking`, vp.`Pool`, ";
                    sqlStatement += " vp.`YearBuilt`, ";
                    sqlStatement += " '' AS 'Basement',  ";                                             // --This field was always blank and no longer exists.
                    sqlStatement += " vp.`Condition`, ";
                    sqlStatement += " vp.`Square`, vp.`Occupied`, vp.`Listed`, vp.`ListingExpire`, ";
                    sqlStatement += " vp.`CancelListing`, ";
                    sqlStatement += " '' AS 'ResidentStatus', ";                                        // --This field was always blank and no longer exists.
                    sqlStatement += " 0 AS 'F_ID', ";                                                   // --The Foreclosure table was merged into the VisitorProperty table. This primary key no longer exists.
                    sqlStatement += " vp.`VisitorID` AS 'F_VisitorID', ";
                    sqlStatement += " vp.`ForeclosureNotice`, vp.`ForeclosureDate`, vp.`SaleDate`, vp.`FirstLoan`, ";
                    sqlStatement += " vp.`SecondLoan`, vp.`ThirdLoan`, vp.`TotalPayments`, vp.`Bankruptcy`, vp.`Situation`, ";
                    sqlStatement += " vp.`ApproxMonthlyPayment1`, vp.`ApproxMonthlyPayment2`, vp.`ApproxMonthlyPayment3`, ";
                    sqlStatement += " vp.`Total`, vp.`Worth`, vp.`Net`, m.`ID` as 'M_ID', m.ProfitGrabberID, d.`url` as 'Title' ";
                    sqlStatement += " FROM dash.`Visitor` v ";
                    sqlStatement += " INNER JOIN dash.`UserMember` m ON v.`MemberID` = m.`ID` ";
                    sqlStatement += " INNER JOIN dash.`Source` src ON m.`SourceID` = src.`ID` ";
                    sqlStatement += " INNER JOIN npc.`VisitorProperty` vp ON v.`ID` = vp.`VisitorID` ";
                    sqlStatement += " LEFT JOIN dash.`State` s ON s.`ID` = v.`StateID` ";
                    sqlStatement += " LEFT JOIN dash.`Domain` d ON d.`ID` = v.`DomainID` ";
                    sqlStatement += " WHERE src.`key` = 'npc' ";
                    sqlStatement += " AND ProfitGrabberID = '" + pgId.ToString() + "' ";                //'D5E8CBF2 -8873-4861-9243-D4E283534975'-- This can be any combination of uppercase and lowercase.
                    sqlStatement += " AND v.Created > '" + strDt + "'";
                    sqlStatement += " ORDER BY m.`ID` ASC, vp.`VisitorID` ASC ";


                    appLog.Info("-");
                    appLog.Info(sqlStatement);
                    appLog.Info("-");

                    var cmd = new MySqlCommand(sqlStatement, conn);
                    cmd.CommandType = CommandType.Text;

                    dataReader = cmd.ExecuteReader();
                    
                    while (dataReader.Read())
                    {
                        List<string> entry = new List<string>();

                        entry.Add(dataReader["Firstname"].ToString());  //0
                        entry.Add(dataReader["LastName"].ToString());   //1
                        entry.Add(dataReader["Email"].ToString());  //2
                        entry.Add(dataReader["City"].ToString());  //3
                        entry.Add(dataReader["Abbr"].ToString());  //4
                        entry.Add(dataReader["CountyID"].ToString());    //5
                        entry.Add(dataReader["MemberID"].ToString());  //6
                        entry.Add(dataReader["HomePhone"].ToString());    //7
                        entry.Add(dataReader["TimeToHome"].ToString());       //8
                        entry.Add(dataReader["WorkPhone"].ToString());    //9
                        entry.Add(dataReader["TimeToWork"].ToString());      //10
                        entry.Add(dataReader["Pager"].ToString());        //11
                        entry.Add(dataReader["Goal"].ToString());       //12
                        entry.Add(dataReader["HowHear"].ToString());       //13
                        entry.Add(dataReader["V_Created"].ToString());    //14    
                        entry.Add(dataReader["DomainId"].ToString());   //15
                        entry.Add(dataReader["P_ID"].ToString());   //16
                        entry.Add(dataReader["VisitorID"].ToString());    //17
                        entry.Add(dataReader["Bedrooms"].ToString());       //18
                        entry.Add(dataReader["Bathrooms"].ToString());  //19
                        entry.Add(dataReader["Parking"].ToString());   //20
                        entry.Add(dataReader["Pool"].ToString());      //21
                        entry.Add(dataReader["YearBuilt"].ToString());       //22
                        entry.Add(dataReader["Basement"].ToString());       //23
                        entry.Add(dataReader["Condition"].ToString());          //24
                        entry.Add(dataReader["Square"].ToString());              //25
                        entry.Add(dataReader["Occupied"].ToString());             //26
                        entry.Add(dataReader["Listed"].ToString());                //27
                        entry.Add(dataReader["ListingExpire"].ToString());          //28
                        entry.Add(dataReader["CancelListing"].ToString());              //29
                        entry.Add(dataReader["ResidentStatus"].ToString());              //30
                        entry.Add(dataReader["F_ID"].ToString());              //31
                        entry.Add(dataReader["F_VisitorID"].ToString());              //32
                        entry.Add(dataReader["ForeclosureNotice"].ToString());              //33
                        entry.Add(dataReader["ForeclosureDate"].ToString());              //34
                        entry.Add(dataReader["SaleDate"].ToString());              //35
                        entry.Add(dataReader["FirstLoan"].ToString());              //36
                        entry.Add(dataReader["SecondLoan"].ToString());              //37
                        entry.Add(dataReader["ThirdLoan"].ToString());              //38
                        entry.Add(dataReader["TotalPayments"].ToString());              //39
                        entry.Add(dataReader["Bankruptcy"].ToString());              //40
                        entry.Add(dataReader["Situation"].ToString());              //41
                        entry.Add(dataReader["ApproxMonthlyPayment1"].ToString());              //42
                        entry.Add(dataReader["ApproxMonthlyPayment2"].ToString());              //43
                        entry.Add(dataReader["ApproxMonthlyPayment3"].ToString());              //44
                        entry.Add(dataReader["Total"].ToString());              //45
                        entry.Add(dataReader["Worth"].ToString());              //46
                        entry.Add(dataReader["Net"].ToString());              //47
                        entry.Add(dataReader["M_ID"].ToString());              //48
                        entry.Add(dataReader["ProfitGrabberID"].ToString());              //49
                        entry.Add(dataReader["Title"].ToString());              //50 
                        //entry.Add(DBNull.Value != dataReader["V_Created"] ? ((DateTime)dataReader["V_Created"]).Ticks.ToString() : string.Empty);   //51
                        entry.Add(DBNull.Value != dataReader["V_Created"] ? (DateTime.Parse(dataReader["V_Created"].ToString())).Ticks.ToString() : string.Empty);   //51
                        //entry.Add(dataReader["PGID"].ToString());       //52
                        //entry.Add(dataReader["UniqueTag"].ToString());       //53


                        resList.Add(entry);
                    }

                    appLog.Info("Got: " + (null != resList ? resList.Count.ToString() : "null") + " records!");
                    appLog.Info("=====================");
                }
                catch (Exception exc)
                {
                    string error = exc.Message + Environment.NewLine + exc.InnerException + Environment.NewLine + exc.Source;
                    appLog.Error("ERROR:" + error);
                    appLog.Info("=====================");
                }
                finally
                {
                    if (null != dataReader)
                        dataReader.Close();
                }
            }

            return resList;
        }
    }
}
