﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using MySql.Data.MySqlClient;

namespace ProfitGrabber.WebSitesSupport.DALC
{
    public sealed class ACOKConnection
    {
        static readonly ACOKConnection _instance = new ACOKConnection();

        static ACOKConnection()
        {}

        ACOKConnection()
        {
            InitConnection(out _connError);
        }

        public static ACOKConnection Instance
        {
            get { return _instance; }
        }

        MySqlConnection _conn = null;
        string _connError = string.Empty;

        public MySqlConnection Connection
        {
            get { return _conn; }
        }

        public string ConnError
        {
            get { return _connError; }
        }

        bool InitConnection(out string error)
        {
            error = string.Empty;

            _conn = new MySqlConnection();
            _conn.ConnectionString = ConfigurationManager.ConnectionStrings["ACOKConnString"].ConnectionString;

            try
            {
                _conn.Open();
            }
            catch (Exception exc)
            {
                error = exc.Message + Environment.NewLine + exc.InnerException + Environment.NewLine + exc.Source;
                return false;
            }

            return true;
        }

        public List<List<string>> GetEntries(Guid pgId, long ticks)
        {
            MySqlDataReader dataReader = null;
            List<List<string>> resList = new List<List<string>>();

            try
            {
                var cmd = new MySqlCommand("GetACOKObject", _conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("@ProfitGrabberID", pgId));
                cmd.Parameters.Add(new SqlParameter("@LeadDate", new DateTime(ticks, DateTimeKind.Unspecified)));

                dataReader = cmd.ExecuteReader();

                while (dataReader.Read())
                {
                    List<string> entry = new List<string>();

                    entry.Add(dataReader["ID"].ToString());  //0
                    entry.Add(dataReader["VisitorID"].ToString());   //1
                    entry.Add(dataReader["Name"].ToString());  //2
                    entry.Add(dataReader["Phone"].ToString());  //3
                    entry.Add(dataReader["HouseType"].ToString());  //4
                    entry.Add(dataReader["SqFt"].ToString());    //5
                    entry.Add(dataReader["Bedroom"].ToString());  //6
                    entry.Add(dataReader["Bath"].ToString());    //7
                    entry.Add(dataReader["YearBuilt"].ToString());       //8
                    entry.Add(dataReader["Condition"].ToString());    //9
                    entry.Add(dataReader["WhySelling"].ToString());      //10
                    entry.Add(dataReader["Address"].ToString());        //11
                    entry.Add(dataReader["City"].ToString());       //12
                    entry.Add(dataReader["State"].ToString());       //13
                    entry.Add(dataReader["Zip"].ToString());    //14    
                    entry.Add(dataReader["County"].ToString());   //15
                    entry.Add(dataReader["Created"].ToString());   //16
                    entry.Add(dataReader["Type"].ToString());    //17
                    entry.Add(dataReader["Status"].ToString());       //18                    
                    entry.Add(DBNull.Value != dataReader["Created"] ? ((DateTime)dataReader["Created"]).Ticks.ToString() : string.Empty);   //19

                    resList.Add(entry);
                }
            }
            catch (Exception exc)
            {
                bool stopHere = true;
            }
            finally 
            {
                if (null != dataReader)
                    dataReader.Close();
            }

            return resList;
        }

        public List<List<string>> GetEntries_V2(Guid pgId, long ticks, log4net.ILog appLog /*Guid uniqueTag*/)
        {
            MySqlDataReader dataReader = null;
            List<List<string>> resList = new List<List<string>>();

            string connectionString = ConfigurationManager.ConnectionStrings["ACOKConnString"].ConnectionString;

            using (var conn = new MySqlConnection(connectionString))
            {
                try
                {
                    #region SPL - COMMENTED OUT
                    /*SqlCommand cmd = new SqlCommand("GetACOKObject_V2", _conn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(new SqlParameter("@ProfitGrabberID", pgId));
                    cmd.Parameters.Add(new SqlParameter("@LeadDate", new DateTime(ticks, DateTimeKind.Unspecified)));
                    cmd.Parameters.Add(new SqlParameter("@UniqueTag", uniqueTag));

                    dataReader = cmd.ExecuteReader();*/
                    #endregion

                    DateTime dt = new DateTime(ticks, DateTimeKind.Unspecified);
                    //string strDt = dt.ToString("yyyy/MM/dd hh:mm:ss tt");
                    string strDt = dt.ToString("yyyy/MM/dd HH:mm:ss");
                    appLog.Info("=====================");
                    appLog.Info("ACOK: " + pgId.ToString() + ", since: " + strDt + " [" + ticks.ToString() + "]");

                    conn.Open();

                    //string sqlStatement = "select VS.[ID],VS.[VisitorID],VS.[Name],VS.[Phone],VS.[HouseType],VS.[SqFt],VS.[Bedroom],VS.[Bath],VS.[YearBuilt],VS.[Condition],VS.[WhySelling] ,VS.[Address],VS.[City],VS.[State],VS.[Zip],VS.[County],VS.[Created] ,VS.[Type] ,VS.[Status] ,VS.[Updated] ";
                    //sqlStatement += " from MemberProfitGrabber as MPG  inner join Member as M on M.ID = MPG.MemberID     inner join Visitor as V on V.MemberID = M.ID inner join VisitorSell as VS on VS.VisitorID = V.ID and VS.Created > '" + strDt + "'";
                    //sqlStatement += " where MPG.ProfitGrabberID = '" + pgId.ToString() + "'";

                    string sqlStatement = "SELECT ";
                    sqlStatement += " VP.ID, ";
                    sqlStatement += " VP.`VisitorID`, ";
                    sqlStatement += " CONCAT(VP.`CoBuyerFirstName`, ' ', VP.`CoBuyerLastName`) AS 'Name', ";
                    sqlStatement += " VP.CoBuyerPhone AS 'Phone', ";
                    sqlStatement += " P.PropertyType AS 'HouseType', ";
                    sqlStatement += " P.`SqFt`, ";
                    sqlStatement += " P.`Bed` AS 'Bedroom', ";
                    sqlStatement += " P.`Bath`, ";
                    sqlStatement += " P.`YearBuilt`, ";
                    sqlStatement += " '' AS 'Conditon', ";// --No field exists for this in new database
                    sqlStatement += " '' AS 'WhySelling',  "; //--No field exists for this in new database
                    sqlStatement += " P.`AddressLine1` AS 'Address', ";
                    sqlStatement += " P.`City`, ";
                    sqlStatement += " S.Title AS 'State', ";
                    sqlStatement += " P.Zip, ";
                    sqlStatement += " '' AS County, ";// --This field was always blank
                    sqlStatement += " DATE_FORMAT(VP.`Created`, '%Y-%m-%d %T.000') AS 'Created', ";
                    sqlStatement += " IF(VP.Type = 'VisitorProperty', 'VisitorBuy', VP.Type) AS 'Type', ";
                    sqlStatement += " VP.Status, ";
                    sqlStatement += " DATE_FORMAT(VP.`Updated`, '%Y-%m-%d %T.000') AS 'Updated', ";
                    sqlStatement += " M.ProfitGrabberID ";
                    sqlStatement += " FROM dash.`Visitor` V ";
                    sqlStatement += " INNER JOIN dash.`UserMember` M ON V.`MemberID` = M.`ID` ";
                    sqlStatement += " INNER JOIN dash.`Source` SRC ON M.`SourceID` = SRC.`ID` ";
                    sqlStatement += " INNER JOIN acok.`VisitorProperty` VP ON V.`ID` = VP.`VisitorID` ";
                    sqlStatement += " INNER JOIN acok.`Property` P ON P.`ID` = VP.`PropertyID` AND P.`MemberID`= M.`ID` ";
                    sqlStatement += " LEFT JOIN dash.`State` S ON S.`ID` = P.`StateID` ";
                    sqlStatement += " WHERE SRC.`key` = 'acok' ";
                    sqlStatement += " AND M.ProfitGrabberID LIKE '" + pgId.ToString() + "' ";//-- This can be any combination of uppercase and lowercase.
                    sqlStatement += " AND VP.Created >  '" + strDt + "' " ;
                    sqlStatement += " ORDER BY VP.Created ASC ";


                    appLog.Info("-");
                    appLog.Info(sqlStatement);
                    appLog.Info("-");

                    var cmd = new MySqlCommand(sqlStatement, conn);
                    cmd.CommandType = CommandType.Text;

                    dataReader = cmd.ExecuteReader();

                    while (dataReader.Read())
                    {
                        List<string> entry = new List<string>();

                        entry.Add(dataReader["ID"].ToString());  //0
                        entry.Add(dataReader["VisitorID"].ToString());   //1
                        entry.Add(dataReader["Name"].ToString());  //2
                        entry.Add(dataReader["Phone"].ToString());  //3
                        entry.Add(dataReader["HouseType"].ToString());  //4
                        entry.Add(dataReader["SqFt"].ToString());    //5
                        entry.Add(dataReader["Bedroom"].ToString());  //6
                        entry.Add(dataReader["Bath"].ToString());    //7
                        entry.Add(dataReader["YearBuilt"].ToString());       //8
                        entry.Add(string.Empty /*dataReader["Condition"].ToString()*/);    //9
                        entry.Add(string.Empty /*dataReader["WhySelling"].ToString()*/);      //10
                        entry.Add(dataReader["Address"].ToString());        //11
                        entry.Add(dataReader["City"].ToString());       //12
                        entry.Add(dataReader["State"].ToString());       //13
                        entry.Add(dataReader["Zip"].ToString());    //14    
                        entry.Add(dataReader["County"].ToString());   //15
                        entry.Add(dataReader["Created"].ToString());   //16
                        entry.Add(dataReader["Type"].ToString());    //17
                        entry.Add(dataReader["Status"].ToString());       //18                    
                        //entry.Add(DBNull.Value != dataReader["Created"] ? ((DateTime)dataReader["Created"]).Ticks.ToString() : string.Empty);   //19
                        entry.Add(DBNull.Value != dataReader["Created"] ? (DateTime.Parse(dataReader["Created"].ToString())).Ticks.ToString() : string.Empty);   //19
                        //entry.Add(dataReader["PGID"].ToString());       //20
                        //entry.Add(dataReader["UniqueTag"].ToString());       //21

                        resList.Add(entry);
                    }

                    appLog.Info("Got: " + (null != resList ? resList.Count.ToString() : "null") + " records!");
                    appLog.Info("=====================");
                }
                catch (Exception exc)
                {
                    string error = exc.Message + Environment.NewLine + exc.InnerException + Environment.NewLine + exc.Source;
                    appLog.Error("ERROR:" + error);
                    appLog.Info("=====================");
                }
                finally
                {
                    if (null != dataReader)
                        dataReader.Close();
                }
            }

            return resList;
        }
    }
}
