﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace ProfitGrabber.WebSitesSupport.DALC
{
    public sealed class PGDB3Connection
    {
        static readonly PGDB3Connection _instance = new PGDB3Connection();

        static PGDB3Connection()
        {}

        PGDB3Connection()
        {
            InitConnection(out _connError);
        }

        public static PGDB3Connection Instance
        {
            get { return _instance; }
        }

        SqlConnection _conn = null;
        string _connError = string.Empty;

        public SqlConnection Connection
        {
            get { return _conn; }
        }

        public string ConnError
        {
            get { return _connError; }
        }

        bool InitConnection(out string error)
        {
            error = string.Empty;

            _conn = new SqlConnection();
            _conn.ConnectionString = ConfigurationManager.ConnectionStrings["PGDB3ConnString"].ConnectionString;

            try
            {
                _conn.Open();
            }
            catch (Exception exc)
            {
                error = exc.Message + Environment.NewLine + exc.InnerException + Environment.NewLine + exc.Source;
                return false;
            }

            return true;
        }

        public List<List<string>> GetEntries(Guid pgId, long ticks)
        {
            SqlDataReader dataReader = null;
            List<List<string>> resList = new List<List<string>>();

            try
            {
                SqlCommand cmd = new SqlCommand("GetPG3Object", _conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("@ProfitGrabberID", pgId));
                cmd.Parameters.Add(new SqlParameter("@LeadDate", new DateTime(ticks, DateTimeKind.Unspecified)));

                dataReader = cmd.ExecuteReader();

                while (dataReader.Read())
                {
                    List<string> entry = new List<string>();

                    entry.Add(dataReader["OriginDesc"].ToString());  //0
                    entry.Add(dataReader["FirstName"].ToString());   //1
                    entry.Add(dataReader["LastName"].ToString());  //2
                    entry.Add(dataReader["FullName"].ToString());  //3
                    entry.Add(dataReader["Email"].ToString());  //4
                    entry.Add(dataReader["HomePhoneNr"].ToString());    //5
                    entry.Add(dataReader["HomeBestTimeToCall"].ToString());  //6
                    entry.Add(dataReader["WorkPhoneNr"].ToString());    //7
                    entry.Add(dataReader["WorkBestTimeToCall"].ToString());       //8
                    entry.Add(dataReader["CellPhoneNr"].ToString());    //9
                    entry.Add(dataReader["CellBestTimeToCall"].ToString());      //10
                    entry.Add(dataReader["FullSiteStreetAddress"].ToString());        //11
                    entry.Add(dataReader["SiteCity"].ToString());       //12
                    entry.Add(dataReader["SiteState"].ToString());       //13
                    entry.Add(dataReader["SiteZip"].ToString());    //14    
                    entry.Add(dataReader["Type"].ToString());   //15
                    entry.Add(dataReader["Bedrooms"].ToString());   //16
                    entry.Add(dataReader["Baths"].ToString());    //17
                    entry.Add(dataReader["Sqft"].ToString());       //18
                    entry.Add(dataReader["YearBuild"].ToString());  //19
                    entry.Add(dataReader["Garage"].ToString());   //20
                    entry.Add(dataReader["Pool"].ToString());      //21
                    entry.Add(dataReader["OccupiedBy"].ToString());       //22
                    entry.Add(dataReader["FeatureCondition"].ToString());       //23
                    entry.Add(dataReader["WhySelling"].ToString());          //24
                    entry.Add(dataReader["WorthBySeller"].ToString());              //25
                    entry.Add(dataReader["Source"].ToString());             //26
                    entry.Add(dataReader["FirstBal"].ToString());                //27
                    entry.Add(dataReader["MoPay1"].ToString());          //28
                    entry.Add(dataReader["TaxIns"].ToString());              //29
                    entry.Add(dataReader["SecondBal"].ToString());              //30
                    entry.Add(dataReader["MoPay2"].ToString());              //31
                    entry.Add(dataReader["Hoa"].ToString());              //32
                    entry.Add(dataReader["OtherLiens"].ToString());              //33
                    entry.Add(dataReader["PmntsCurrent"].ToString());              //34
                    entry.Add(dataReader["MoBehind"].ToString());              //35
                    entry.Add(dataReader["ReInsAmnt"].ToString());              //36
                    entry.Add(dataReader["InForecl"].ToString());              //37
                    entry.Add(dataReader["Listed"].ToString());              //38
                    entry.Add(dataReader["ListExpDate"].ToString());              //39
                    entry.Add(dataReader["CanCancelList"].ToString());              //40
                    entry.Add(dataReader["LowestPriceAcceptable"].ToString());              //41
                    entry.Add(dataReader["MinCashNeededWhy"].ToString());              //42
                    entry.Add(dataReader["DesiredClosingDate"].ToString());              //43
                    entry.Add(dataReader["Motivation"].ToString());              //44
                    entry.Add(dataReader["LeadSource"].ToString());              //45
                    entry.Add(dataReader["NextStep"].ToString());              //46
                    entry.Add(dataReader["EntryStamp"].ToString());              //47                    
                    entry.Add(DBNull.Value != dataReader["EntryStamp"] ? ((DateTime)dataReader["EntryStamp"]).Ticks.ToString() : string.Empty);   //48
                    

                    resList.Add(entry);
                }
            }
            catch (Exception exc)
            {
                bool stopHere = true;
            }
            finally 
            {
                if (null != dataReader)
                    dataReader.Close();
            }

            return resList;
        }

        public List<List<string>> GetEntries_V2(Guid pgId, long ticks, log4net.ILog appLog /*Guid uniqueTag*/)
        {
            SqlDataReader dataReader = null;
            List<List<string>> resList = new List<List<string>>();

            string connectionString = ConfigurationManager.ConnectionStrings["PGDB3ConnString"].ConnectionString;

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                try
                {
                    #region GET PG3 OBJECT V2 Commented Out
                    /*SqlCommand cmd = new SqlCommand("GetPG3Object_V2", _conn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(new SqlParameter("@ProfitGrabberID", pgId));
                    cmd.Parameters.Add(new SqlParameter("@LeadDate", new DateTime(ticks, DateTimeKind.Unspecified)));
                    cmd.Parameters.Add(new SqlParameter("@UniqueTag", uniqueTag));*/
                    #endregion

                    DateTime dt = new DateTime(ticks, DateTimeKind.Unspecified);
                    string strDt = dt.ToString("yyyy/MM/dd hh:mm:ss tt");
                    appLog.Info("=====================");
                    appLog.Info("VCPL: " + pgId.ToString() + ", since: " + strDt + " [" + ticks.ToString() + "]");

                    conn.Open();

                    string sqlStatement = "SELECT E.[EntryStamp],E.[ID_PG_User],O.OriginDesc as OriginDesc, C.FirstName as FirstName, C.LastName as LastName, C.FullName as FullName, C.eMail as Email, HP.PhoneNr as HomePhoneNr, HP.BestTimeToCall as HomeBestTimeToCall , WP.PhoneNr as WorkPhoneNr, WP.BestTimeToCall as WorkBestTimeToCall, CP.PhoneNr as CellPhoneNr, CP.BestTimeToCall as CellBestTimeToCall, FA.FullSiteStreetAddress as FullSiteStreetAddress, FA.SiteCity	as SiteCity, FA.SiteState as SiteState, FA.SiteZip as SiteZip, DP.[Type], DP.[Bedrooms], DP.[Baths], DP.[Sqft], DP.[YearBuild], DP.[Garage], DP.[Pool], DP.[OccupiedBy], DP.[FeatureCondition], DP.[WhySelling], DP.[WorthBySeller], DP.[Source], DP.[FirstBal], DP.[MoPay1], DP.[TaxIns], DP.[SecondBal], DP.[MoPay2], DP.[Hoa], DP.[OtherLiens], DP.[PmntsCurrent], DP.[MoBehind], DP.[ReInsAmnt], DP.[InForecl], DP.[Listed], DP.[ListExpDate], DP.[CanCancelList], DP.[LowestPriceAcceptable], DP.[MinCashNeededWhy], DP.[DesiredClosingDate], DP.[Motivation], DP.[LeadSource], DP.[NextStep]";
                    sqlStatement += " FROM [Entry] as E  inner join Origin as O on O.IdOrigin = E.IdOrigin and E.EntryStamp > '" + strDt + "'";
                    sqlStatement += " inner join Contact as C on C.IdContact = E.IdContact  inner join Phone as HP on C.HomePhone = HP.IdPhone  inner join Phone as WP on C.WorkPhone = WP.IdPhone  inner join Phone as CP on C.CellPhone = CP.IdPhone  inner join Address as FA on C.FirstAddress = FA.IdAddress  inner join DealProperty as DP on E.IdDealProperty = DP.IdDealProperty  where E.ID_PG_User = '" + pgId.ToString() + "'";

                    appLog.Info("-");
                    appLog.Info(sqlStatement);
                    appLog.Info("-");


                    SqlCommand cmd = new SqlCommand(sqlStatement, conn);
                    cmd.CommandType = CommandType.Text;

                    dataReader = cmd.ExecuteReader();

                    while (dataReader.Read())
                    {
                        List<string> entry = new List<string>();

                        entry.Add(dataReader["OriginDesc"].ToString());  //0
                        entry.Add(dataReader["FirstName"].ToString());   //1
                        entry.Add(dataReader["LastName"].ToString());  //2
                        entry.Add(dataReader["FullName"].ToString());  //3
                        entry.Add(dataReader["Email"].ToString());  //4
                        entry.Add(dataReader["HomePhoneNr"].ToString());    //5
                        entry.Add(dataReader["HomeBestTimeToCall"].ToString());  //6
                        entry.Add(dataReader["WorkPhoneNr"].ToString());    //7
                        entry.Add(dataReader["WorkBestTimeToCall"].ToString());       //8
                        entry.Add(dataReader["CellPhoneNr"].ToString());    //9
                        entry.Add(dataReader["CellBestTimeToCall"].ToString());      //10
                        entry.Add(dataReader["FullSiteStreetAddress"].ToString());        //11
                        entry.Add(dataReader["SiteCity"].ToString());       //12
                        entry.Add(dataReader["SiteState"].ToString());       //13
                        entry.Add(dataReader["SiteZip"].ToString());    //14    
                        entry.Add(dataReader["Type"].ToString());   //15
                        entry.Add(dataReader["Bedrooms"].ToString());   //16
                        entry.Add(dataReader["Baths"].ToString());    //17
                        entry.Add(dataReader["Sqft"].ToString());       //18
                        entry.Add(dataReader["YearBuild"].ToString());  //19
                        entry.Add(dataReader["Garage"].ToString());   //20
                        entry.Add(dataReader["Pool"].ToString());      //21
                        entry.Add(dataReader["OccupiedBy"].ToString());       //22
                        entry.Add(dataReader["FeatureCondition"].ToString());       //23
                        entry.Add(dataReader["WhySelling"].ToString());          //24
                        entry.Add(dataReader["WorthBySeller"].ToString());              //25
                        entry.Add(dataReader["Source"].ToString());             //26
                        entry.Add(dataReader["FirstBal"].ToString());                //27
                        entry.Add(dataReader["MoPay1"].ToString());          //28
                        entry.Add(dataReader["TaxIns"].ToString());              //29
                        entry.Add(dataReader["SecondBal"].ToString());              //30
                        entry.Add(dataReader["MoPay2"].ToString());              //31
                        entry.Add(dataReader["Hoa"].ToString());              //32
                        entry.Add(dataReader["OtherLiens"].ToString());              //33
                        entry.Add(dataReader["PmntsCurrent"].ToString());              //34
                        entry.Add(dataReader["MoBehind"].ToString());              //35
                        entry.Add(dataReader["ReInsAmnt"].ToString());              //36
                        entry.Add(dataReader["InForecl"].ToString());              //37
                        entry.Add(dataReader["Listed"].ToString());              //38
                        entry.Add(dataReader["ListExpDate"].ToString());              //39
                        entry.Add(dataReader["CanCancelList"].ToString());              //40
                        entry.Add(dataReader["LowestPriceAcceptable"].ToString());              //41
                        entry.Add(dataReader["MinCashNeededWhy"].ToString());              //42
                        entry.Add(dataReader["DesiredClosingDate"].ToString());              //43
                        entry.Add(dataReader["Motivation"].ToString());              //44
                        entry.Add(dataReader["LeadSource"].ToString());              //45
                        entry.Add(dataReader["NextStep"].ToString());              //46
                        entry.Add(dataReader["EntryStamp"].ToString());              //47                    
                        entry.Add(DBNull.Value != dataReader["EntryStamp"] ? ((DateTime)dataReader["EntryStamp"]).Ticks.ToString() : string.Empty);   //48
                        //entry.Add(dataReader["PGID"].ToString());       //49
                        //entry.Add(dataReader["UniqueTag"].ToString());       //50

                        resList.Add(entry);
                    }

                    appLog.Info("Got: " + (null != resList ? resList.Count.ToString() : "null") + " records!");
                    appLog.Info("=====================");
                }
                catch (Exception exc)
                {
                    string error = exc.Message + Environment.NewLine + exc.InnerException + Environment.NewLine + exc.Source;
                    appLog.Error("ERROR:" + error);
                    appLog.Info("=====================");
                }
                finally
                {
                    if (null != dataReader)
                        dataReader.Close();
                }
            }

            return resList;
        }
    }
}
