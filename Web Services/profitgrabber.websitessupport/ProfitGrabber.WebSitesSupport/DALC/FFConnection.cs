﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace ProfitGrabber.WebSitesSupport.DALC
{    
    public sealed class FFConnection
    {
        static readonly FFConnection _instance = new FFConnection();

        static FFConnection()
        { }

        FFConnection()
        {

        }

        public static FFConnection Instance
        {
            get { return _instance; }
        }        

        public bool TestConnection(out string error)
        {
            bool bRes = true;
            error = string.Empty;

            using (SqlConnection sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["FFConnString"].ConnectionString))
            {
                try
                {
                    sqlConn.Open();
                }
                catch (Exception exc)
                {
                    error = exc.Message + Environment.NewLine + exc.InnerException + Environment.NewLine + exc.Source;
                    bRes = false;
                }
                finally
                {
                    if (null != sqlConn && ConnectionState.Open == sqlConn.State)
                        sqlConn.Close();
                }
            }

            return bRes;
        }        

        public List<List<string>> GetCBEntries(Guid pgId, long ticks)
        {
            SqlDataReader dataReader = null;
            List<List<string>> resList = new List<List<string>>();

            using (SqlConnection sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["FFConnString"].ConnectionString))
            {
                try
                {
                    sqlConn.Open();

                    SqlCommand cmd = new SqlCommand("GetCBObject", sqlConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(new SqlParameter("@ProfitGrabberID", pgId));
                    cmd.Parameters.Add(new SqlParameter("@LeadDate", new DateTime(ticks, DateTimeKind.Unspecified)));

                    dataReader = cmd.ExecuteReader();

                    while (dataReader.Read())
                    {
                        List<string> entry = new List<string>();

                        entry.Add(DBNull.Value != dataReader["ID"] ? dataReader["ID"].ToString() : string.Empty);  //0
                        entry.Add(DBNull.Value != dataReader["buyerName1"] ? dataReader["buyerName1"].ToString() : string.Empty);   //1
                        entry.Add(DBNull.Value != dataReader["buyerName2"] ? dataReader["buyerName2"].ToString() : string.Empty);  //2
                        entry.Add(DBNull.Value != dataReader["buyerStreetAddress"] ? dataReader["buyerStreetAddress"].ToString() : string.Empty);  //3
                        entry.Add(DBNull.Value != dataReader["buyerCity"] ? dataReader["buyerCity"].ToString() : string.Empty);  //4
                        entry.Add(DBNull.Value != dataReader["buyerState"] ? dataReader["buyerState"].ToString() : string.Empty);    //5
                        entry.Add(DBNull.Value != dataReader["buyerZip"] ? dataReader["buyerZip"].ToString() : string.Empty);  //6

                        //entry.Add(DBNull.Value != dataReader["loanDate"] ? dataReader["loanDate"].ToString() : string.Empty);    //7
                        if (DBNull.Value != dataReader["loanDate"])
                        {
                            DateTime dt = dataReader.GetDateTime(7);
                            entry.Add(dt.ToShortDateString());
                        }
                        else
                            entry.Add(string.Empty);

                        
                        //entry.Add(DBNull.Value != dataReader["loanAmount"] ? dataReader["loanAmount"].ToString() : string.Empty);       //8
                        if (DBNull.Value != dataReader["loanAmount"])
                        {
                            int val = dataReader.GetInt32(8);
                            entry.Add(val.ToString("c"));
                        }
                        else
                            entry.Add(string.Empty);
                        
                        entry.Add(dataReader["PGID"].ToString());    //9
                        entry.Add(DBNull.Value != dataReader["timestamp"] ? ((DateTime)dataReader["timestamp"]).Ticks.ToString() : string.Empty);   //10

                        resList.Add(entry);
                    }
                }
                catch (Exception exc)
                {
                    bool stopHere = true;
                }
                finally
                {
                    if (null != dataReader)
                        dataReader.Close();
                }
            }

            return resList;
        }

        public List<List<string>> GetCBEntries_V2(Guid pgId, long ticks, log4net.ILog appLog /*Guid uniqueTag*/)
        {
            SqlDataReader dataReader = null;
            List<List<string>> resList = new List<List<string>>();

            using (SqlConnection sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["FFConnString"].ConnectionString))
            {
                try
                {
                    #region SPL - COMMENTED OUT 
                    /*sqlConn.Open();

                    SqlCommand cmd = new SqlCommand("GetCBObject_V2", sqlConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(new SqlParameter("@ProfitGrabberID", pgId));
                    cmd.Parameters.Add(new SqlParameter("@LeadDate", new DateTime(ticks, DateTimeKind.Unspecified)));
                    cmd.Parameters.Add(new SqlParameter("@UniqueTag", uniqueTag));

                    dataReader = cmd.ExecuteReader();*/
                    #endregion 

                    DateTime dt = new DateTime(ticks, DateTimeKind.Unspecified);
                    string strDt = dt.ToString("yyyy/MM/dd hh:mm:ss tt");
                    appLog.Info("=====================");
                    appLog.Info("CB: " + pgId.ToString() + ", since: " + strDt + " [" + ticks.ToString() + "]");

                    sqlConn.Open();
                                        
                    string sqlStatement = "SELECT [Id],[buyerName1],[buyerName2],[buyerStreetAddress],[buyerCity],[buyerState],[buyerZip],[loanDate],[loanAmount],[PGID],[timestamp] ";
                    sqlStatement += " FROM [CashBuyersDownload] ";
                    sqlStatement += " WHERE [CashBuyersDownload].[PGID] = '" + pgId.ToString() + "' ";
                    sqlStatement += " and [CashBuyersDownload].[timestamp] > '" + strDt + "'";

                    appLog.Info("-");
                    appLog.Info(sqlStatement);
                    appLog.Info("-");

                    SqlCommand cmd = new SqlCommand(sqlStatement, sqlConn);
                    cmd.CommandType = CommandType.Text;

                    dataReader = cmd.ExecuteReader();

                    while (dataReader.Read())
                    {
                        List<string> entry = new List<string>();

                        entry.Add(DBNull.Value != dataReader["ID"] ? dataReader["ID"].ToString() : string.Empty);  //0
                        entry.Add(DBNull.Value != dataReader["buyerName1"] ? dataReader["buyerName1"].ToString() : string.Empty);   //1
                        entry.Add(DBNull.Value != dataReader["buyerName2"] ? dataReader["buyerName2"].ToString() : string.Empty);  //2
                        entry.Add(DBNull.Value != dataReader["buyerStreetAddress"] ? dataReader["buyerStreetAddress"].ToString() : string.Empty);  //3
                        entry.Add(DBNull.Value != dataReader["buyerCity"] ? dataReader["buyerCity"].ToString() : string.Empty);  //4
                        entry.Add(DBNull.Value != dataReader["buyerState"] ? dataReader["buyerState"].ToString() : string.Empty);    //5
                        entry.Add(DBNull.Value != dataReader["buyerZip"] ? dataReader["buyerZip"].ToString() : string.Empty);  //6

                        //entry.Add(DBNull.Value != dataReader["loanDate"] ? dataReader["loanDate"].ToString() : string.Empty);    //7
                        if (DBNull.Value != dataReader["loanDate"])
                        {
                            DateTime tempDt = dataReader.GetDateTime(7);
                            entry.Add(tempDt.ToShortDateString());
                        }
                        else
                            entry.Add(string.Empty);


                        //entry.Add(DBNull.Value != dataReader["loanAmount"] ? dataReader["loanAmount"].ToString() : string.Empty);       //8
                        if (DBNull.Value != dataReader["loanAmount"])
                        {
                            int val = dataReader.GetInt32(8);
                            entry.Add(val.ToString("c"));
                        }
                        else
                            entry.Add(string.Empty);

                        entry.Add(dataReader["PGID"].ToString());    //9
                        entry.Add(DBNull.Value != dataReader["timestamp"] ? ((DateTime)dataReader["timestamp"]).Ticks.ToString() : string.Empty);   //10
                        entry.Add(dataReader["PGID"].ToString());       //11
                        entry.Add(string.Empty); //entry.Add(dataReader["UniqueTag"].ToString());       //12

                        resList.Add(entry);
                    }

                    appLog.Info("Got: " + (null != resList ? resList.Count.ToString() : "null") + " records!");
                    appLog.Info("=====================");
                }
                catch (Exception exc)
                {
                    string error = exc.Message + Environment.NewLine + exc.InnerException + Environment.NewLine + exc.Source;
                    appLog.Error("ERROR:" + error);
                    appLog.Info("=====================");
                }
                finally
                {
                    if (null != dataReader)
                        dataReader.Close();
                }
            }

            return resList;
        }

        public List<List<string>> GetPLEntries(Guid pgId, long ticks)
        {
            SqlDataReader dataReader = null;
            List<List<string>> resList = new List<List<string>>();

            using (SqlConnection sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["FFConnString"].ConnectionString))
            {
                try
                {
                    sqlConn.Open();

                    SqlCommand cmd = new SqlCommand("GetPLObject", sqlConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(new SqlParameter("@ProfitGrabberID", pgId));
                    cmd.Parameters.Add(new SqlParameter("@LeadDate", new DateTime(ticks, DateTimeKind.Unspecified)));

                    dataReader = cmd.ExecuteReader();

                    while (dataReader.Read())
                    {
                        List<string> entry = new List<string>();

                        entry.Add(DBNull.Value != dataReader["ID"] ? dataReader["ID"].ToString() : string.Empty);  //0
                        entry.Add(DBNull.Value != dataReader["lenderName"] ? dataReader["lenderName"].ToString() : string.Empty);   //1
                        entry.Add(DBNull.Value != dataReader["lenderStreetAddress"] ? dataReader["lenderStreetAddress"].ToString() : string.Empty);  //2
                        entry.Add(DBNull.Value != dataReader["lenderCity"] ? dataReader["lenderCity"].ToString() : string.Empty);  //3
                        entry.Add(DBNull.Value != dataReader["lenderState"] ? dataReader["lenderState"].ToString() : string.Empty);  //4
                        entry.Add(DBNull.Value != dataReader["lenderZip"] ? dataReader["lenderZip"].ToString() : string.Empty);    //5
                        
                        //entry.Add(DBNull.Value != dataReader["loanDate"] ? dataReader["loanDate"].ToString() : string.Empty);  //6
                        if (DBNull.Value != dataReader["loanDate"])
                        {
                            DateTime dt = dataReader.GetDateTime(6);
                            entry.Add(dt.ToShortDateString());
                        }
                        else
                            entry.Add(string.Empty);
                        
                        //entry.Add(DBNull.Value != dataReader["loanAmount"] ? dataReader["loanAmount"].ToString() : string.Empty);    //7
                        if (DBNull.Value != dataReader["loanAmount"])
                        {
                            int val = dataReader.GetInt32(7);
                            entry.Add(val.ToString("c"));
                        }
                        else
                            entry.Add(string.Empty);


                        entry.Add(dataReader["PGID"].ToString());    //8                        
                        entry.Add(DBNull.Value != dataReader["timestamp"] ? ((DateTime)dataReader["timestamp"]).Ticks.ToString() : string.Empty);   //9

                        resList.Add(entry);
                    }
                }
                catch (Exception exc)
                {
                    bool stopHere = true;
                }
                finally
                {
                    if (null != dataReader)
                        dataReader.Close();
                }
            }

            return resList;
        }

        public List<List<string>> GetPLEntries_V2(Guid pgId, long ticks, log4net.ILog appLog /*Guid uniqueTag*/)
        {
            SqlDataReader dataReader = null;
            List<List<string>> resList = new List<List<string>>();

            using (SqlConnection sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["FFConnString"].ConnectionString))
            {
                try
                {                    
                    #region SPL - COMMENTED OUT
                    /*SqlCommand cmd = new SqlCommand("GetPLObject_V2", sqlConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(new SqlParameter("@ProfitGrabberID", pgId));
                    cmd.Parameters.Add(new SqlParameter("@LeadDate", new DateTime(ticks, DateTimeKind.Unspecified)));
                    cmd.Parameters.Add(new SqlParameter("@UniqueTag", uniqueTag));*/
                    #endregion

                    DateTime dt = new DateTime(ticks, DateTimeKind.Unspecified);
                    string strDt = dt.ToString("yyyy/MM/dd hh:mm:ss tt");
                    appLog.Info("=====================");
                    appLog.Info("PL: " + pgId.ToString() + ", since: " + strDt + " [" + ticks.ToString() + "]");

                    sqlConn.Open();

                    string sqlStatement = "SELECT [Id],[lenderName],[lenderStreetAddress],[lenderCity],[lenderState],[lenderZip],[loanDate],[loanAmount],[PGID],[timestamp] ";
                    sqlStatement += " FROM [PrivateLendersDownload] ";	
	                sqlStatement += " WHERE	[PrivateLendersDownload].[PGID] = '" + pgId.ToString() + "' ";
                    sqlStatement += " and [PrivateLendersDownload].[timestamp] > '" + strDt + "'";

                    appLog.Info("-");
                    appLog.Info(sqlStatement);
                    appLog.Info("-");

                    SqlCommand cmd = new SqlCommand(sqlStatement, sqlConn);
                    cmd.CommandType = CommandType.Text;

                    dataReader = cmd.ExecuteReader();

                    while (dataReader.Read())
                    {
                        List<string> entry = new List<string>();

                        entry.Add(DBNull.Value != dataReader["ID"] ? dataReader["ID"].ToString() : string.Empty);  //0
                        entry.Add(DBNull.Value != dataReader["lenderName"] ? dataReader["lenderName"].ToString() : string.Empty);   //1
                        entry.Add(DBNull.Value != dataReader["lenderStreetAddress"] ? dataReader["lenderStreetAddress"].ToString() : string.Empty);  //2
                        entry.Add(DBNull.Value != dataReader["lenderCity"] ? dataReader["lenderCity"].ToString() : string.Empty);  //3
                        entry.Add(DBNull.Value != dataReader["lenderState"] ? dataReader["lenderState"].ToString() : string.Empty);  //4
                        entry.Add(DBNull.Value != dataReader["lenderZip"] ? dataReader["lenderZip"].ToString() : string.Empty);    //5

                        //entry.Add(DBNull.Value != dataReader["loanDate"] ? dataReader["loanDate"].ToString() : string.Empty);  //6
                        if (DBNull.Value != dataReader["loanDate"])
                        {
                            DateTime tempDt = dataReader.GetDateTime(6);
                            entry.Add(tempDt.ToShortDateString());
                        }
                        else
                            entry.Add(string.Empty);

                        //entry.Add(DBNull.Value != dataReader["loanAmount"] ? dataReader["loanAmount"].ToString() : string.Empty);    //7
                        if (DBNull.Value != dataReader["loanAmount"])
                        {
                            int val = dataReader.GetInt32(7);
                            entry.Add(val.ToString("c"));
                        }
                        else
                            entry.Add(string.Empty);


                        entry.Add(dataReader["PGID"].ToString());    //8                        
                        entry.Add(DBNull.Value != dataReader["timestamp"] ? ((DateTime)dataReader["timestamp"]).Ticks.ToString() : string.Empty);   //9
                        entry.Add(dataReader["PGID"].ToString());       //10
                        entry.Add(string.Empty);//entry.Add(dataReader["UniqueTag"].ToString());       //11

                        resList.Add(entry);
                    }

                    appLog.Info("Got: " + (null != resList ? resList.Count.ToString() : "null") + " records!");
                    appLog.Info("=====================");
                }
                catch (Exception exc)
                {
                    string error = exc.Message + Environment.NewLine + exc.InnerException + Environment.NewLine + exc.Source;
                    appLog.Error("ERROR:" + error);
                    appLog.Info("=====================");
                }
                finally
                {
                    if (null != dataReader)
                        dataReader.Close();
                }
            }

            return resList;
        }
    }
}
