﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Configuration;
using MySql;
using MySqlConnector;
using MySql.Data.MySqlClient;

namespace ProfitGrabber.WebSitesSupport.DALC
{
    public sealed class AAHConnection
    {
        static readonly AAHConnection _instance = new AAHConnection();

        static AAHConnection()
        {}

        AAHConnection()
        {
            InitConnection(out _connError);
        }

        public static AAHConnection Instance
        {
            get { return _instance; }
        }

        MySqlConnection _conn = null;
        string _connError = string.Empty;

        public MySqlConnection Connection
        {
            get { return _conn; }
        }

        public string ConnError
        {
            get { return _connError; }
        }

        bool InitConnection(out string error)
        {
            error = string.Empty;

            _conn = new MySqlConnection();
            _conn.ConnectionString = ConfigurationManager.ConnectionStrings["AAHConnString"].ConnectionString;

            try
            {
                _conn.Open();
            }
            catch (Exception exc)
            {
                error = exc.Message + Environment.NewLine + exc.InnerException + Environment.NewLine + exc.Source;
                return false;
            }

            return true;
        }

        public List<List<string>> GetEntries(Guid pgId, long ticks)
        {
            MySqlDataReader dataReader = null;
            List<List<string>> resList = new List<List<string>>();

            try
            {
                var cmd = new MySqlCommand("GetAAHObject", _conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("@ProfitGrabberID", pgId));
                cmd.Parameters.Add(new SqlParameter("@LeadDate", new DateTime(ticks, DateTimeKind.Unspecified)));

                dataReader = cmd.ExecuteReader();

                while (dataReader.Read())
                {
                    List<string> entry = new List<string>();

                    entry.Add(dataReader["Firstname"].ToString());  //0
                    entry.Add(dataReader["LastName"].ToString());   //1
                    entry.Add(dataReader["HomePhone"].ToString());  //2
                    entry.Add(dataReader["WorkPhone"].ToString());  //3
                    entry.Add(dataReader["V_Created"].ToString());  //4
                    entry.Add(((DateTime)dataReader["V_Created"]).Ticks.ToString());    //5
                    entry.Add(dataReader["EstimateHomeValue"].ToString());  //6
                    entry.Add(dataReader["Address"].ToString());    //7
                    entry.Add(dataReader["City"].ToString());       //8
                    entry.Add(dataReader["StateID"].ToString());    //9
                    entry.Add(dataReader["Title"].ToString());      //10
                    entry.Add(dataReader["Zip"].ToString());        //11
                    entry.Add(dataReader["SqFt"].ToString());       //12
                    entry.Add(dataReader["Pool"].ToString());       //13
                    entry.Add(dataReader["Parking"].ToString());    //14    
                    entry.Add(dataReader["Occupied"].ToString());   //15
                    entry.Add(dataReader["FirstMortgageBalance"].ToString());   //16
                    entry.Add(dataReader["FirstMonthlyPayment"].ToString());    //17
                    entry.Add(dataReader["FirstTaxIncluded"].ToString());       //18
                    entry.Add(dataReader["SecondMortgageBalance"].ToString());  //19
                    entry.Add(dataReader["SecondMonthlyPayment"].ToString());   //20
                    entry.Add(dataReader["SecondTaxIncluded"].ToString());      //21
                    entry.Add(dataReader["CurrentOnPayment"].ToString());       //22
                    entry.Add(dataReader["BackPaymentsOwed"].ToString());       //23
                    entry.Add(dataReader["ListedRealtor"].ToString());          //24
                    entry.Add(dataReader["Condition"].ToString());              //25
                    entry.Add(dataReader["WhySelling"].ToString());             //26
                    entry.Add(dataReader["HowSoon"].ToString());                //27
                    entry.Add(dataReader["MainObjective"].ToString());          //28
                    entry.Add(dataReader["P_Created"].ToString());              //29
                    entry.Add(DBNull.Value != dataReader["P_Created"] ? ((DateTime)dataReader["P_Created"]).Ticks.ToString() : string.Empty);   //30
                    entry.Add(dataReader["P_Updated"].ToString());              //31
                    entry.Add(DBNull.Value != dataReader["P_Updated"] ? ((DateTime)dataReader["P_Created"]).Ticks.ToString() : string.Empty);   //32                    

                    resList.Add(entry);
                }
            }
            catch (Exception exc)
            {
                bool stopHere = true;
            }
            finally 
            {
                if (null != dataReader)
                    dataReader.Close();
            }

            return resList;
        }

        public List<List<string>> GetEntries_V2(Guid pgId, long ticks, log4net.ILog appLog /*Guid uniqueTag*/)
        {
            MySqlDataReader dataReader = null;
            List<List<string>> resList = new List<List<string>>();

            string connectionString = ConfigurationManager.ConnectionStrings["AAHConnString"].ConnectionString;

            using (var conn = new MySqlConnection(connectionString))
            {
                try
                {
                    #region GET AAH OBJECT V2 - commented out
                    /*SqlCommand cmd = new SqlCommand("GetAAHObject_V2", _conn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(new SqlParameter("@ProfitGrabberID", pgId));
                    cmd.Parameters.Add(new SqlParameter("@LeadDate", new DateTime(ticks, DateTimeKind.Unspecified)));
                    cmd.Parameters.Add(new SqlParameter("@UniqueTag", uniqueTag));*/
                    #endregion

                    DateTime dt = new DateTime(ticks, DateTimeKind.Unspecified);
                    //string strDt = dt.ToString("yyyy/MM/dd hh:mm:ss tt");
                    string strDt = dt.ToString("yyyy/MM/dd HH:mm:ss");
                    appLog.Info("=====================");
                    appLog.Info("AAH: " + pgId.ToString() + ", since: " + strDt + " [" + ticks.ToString() + "]");

                    conn.Open();

                    #region OLD SQL STATEMENT
                    //string sqlStatement = "select V.[Firstname],V.[LastName],V.[HomePhone],V.[WorkPhone],V.[Created] as V_Created,P.[EstimateHomeValue],P.[Address],P.[City],P.[StateID],S.[Title],P.[Zip],P.[SqFt],P.[Pool],P.[Parking],P.[Occupied],P.[FirstMortgageBalance],P.[FirstMonthlyPayment],P.[FirstTaxIncluded],P.[SecondMortgageBalance],P.[SecondMonthlyPayment],P.[SecondTaxIncluded],P.[CurrentOnPayment],P.[BackPaymentsOwed],P.[ListedRealtor],P.[Condition],P.[WhySelling],P.[HowSoon],P.[MainObjective],P.[Created] as P_Created,P.[Updated] as P_Updated";
                    //sqlStatement += " from MemberProfitGrabber as MPG inner join Member as M on M.ID = MPG.MemberID inner join Visitor as V on V.MemberID = M.ID and V.Created > '" + strDt + "'";
                    //sqlStatement += " left join Property as P on P.VisitorID = V.ID left join State as S on S.ID = P.StateID where MPG.ProfitGrabberID = '" + pgId.ToString() + "'";
                    #endregion

                    //string sqlStatement = "select ";
                    //sqlStatement += " V.[Firstname],";
                    //sqlStatement += " V.[LastName],";
                    //sqlStatement += " V.[HomePhone],";
                    //sqlStatement += " V.[WorkPhone],";
                    //sqlStatement += " V.[Created] as V_Created,";
                    //sqlStatement += " P.[EstimateHomeValue],";
                    //sqlStatement += " P.[Address],";
                    //sqlStatement += " P.[City],";
                    //sqlStatement += " P.[StateID],";
                    //sqlStatement += " S.[Abbr],";
                    //sqlStatement += " P.[Zip],";
                    //sqlStatement += " P.[SqFt],";
                    //sqlStatement += " P.[Pool],";
                    //sqlStatement += " P.[Parking],";
                    //sqlStatement += " P.[Occupied],";
                    //sqlStatement += " P.[FirstMortgageBalance],";
                    //sqlStatement += " P.[FirstMonthlyPayment],";
                    //sqlStatement += " P.[FirstTaxIncluded],";
                    //sqlStatement += " P.[SecondMortgageBalance],";
                    //sqlStatement += " P.[SecondMonthlyPayment],";
                    //sqlStatement += " P.[SecondTaxIncluded],";
                    //sqlStatement += " P.[CurrentOnPayment],";
                    //sqlStatement += " P.[BackPaymentsOwed],";
                    //sqlStatement += " P.[ListedRealtor],";
                    //sqlStatement += " P.[Condition],";
                    //sqlStatement += " P.[WhySelling],";
                    //sqlStatement += " P.[HowSoon],";
                    //sqlStatement += " P.[MainObjective],";
                    //sqlStatement += " P.[Created] as P_Created,";
                    //sqlStatement += " P.[Updated] as P_Updated";
                    //sqlStatement += " from MemberProfitGrabber as MPG";
                    //sqlStatement += " inner join Member as M on M.ID = MPG.MemberID";
                    //sqlStatement += " inner join Visitor as V on V.MemberID = M.ID and V.Created > '" + strDt + "'";
                    //sqlStatement += " inner join Property as P on P.VisitorID = V.ID";
                    //sqlStatement += " left join [State] as S on S.ID = P.StateID";
                    //sqlStatement += " where MPG.ProfitGrabberID = '" + pgId.ToString() + "'";
                    //sqlStatement += " AND (P.Status = 'Completed' or (P.Status = 'Active' and DATEDIFF(minute,P.Updated, GETDATE()) > 15 ))";

                    string sqlStatement = "SELECT ";
                    sqlStatement += " V.`FirstName` as 'Firstname', V.`LastName`, V.`HomePhone`, V.`WorkPhone`, ";
                    sqlStatement += " V.`Created` as 'V_Created', P.`EstimateHomeValue`, ";
                    sqlStatement += " P.`Address`, P.`City`, P.`StateID`, S.`Title` as 'Abbr', P.`Zip`, P.`SqFt`, P.`Pool`, P.`Parking`, P.`Occupied`, ";
                    sqlStatement += " P.`FirstMortgageBalance`, P.`FirstMonthlyPayment`, P.`FirstTaxIncluded`, P.`SecondMortgageBalance`, P.`SecondMonthlyPayment`, ";
                    sqlStatement += " P.`SecondTaxIncluded`, P.`CurrentOnPayment`, P.`BackPaymentsOwed`, P.`ListedRealtor`, P.`Condition`, P.`WhySelling`, P.`HowSoon`, ";
                    sqlStatement += " P.`MainObjective`, DATE_FORMAT(P.`Created`, '%Y-%m-%d %T.000') as P_Created, DATE_FORMAT(P.`Updated`, '%Y-%m-%d %T.000') as P_Updated ";
                    sqlStatement += " FROM dash.`Visitor` V ";
                    sqlStatement += " INNER JOIN dash.`UserMember` M ON V.`MemberID` = M.`ID` ";
                    sqlStatement += " INNER JOIN dash.`Source` SRC ON M.`SourceID` = SRC.`ID` ";
                    sqlStatement += " INNER JOIN aah.`VisitorProperty` P ON V.`ID` = P.`VisitorID` ";
                    sqlStatement += " LEFT JOIN dash.`State` S ON S.`ID` = V.`StateID` ";
                    sqlStatement += " WHERE SRC.`key` = 'aah' ";
                    sqlStatement += " AND M.ProfitGrabberID = '" + pgId.ToString() + "' ";      //'D5E8CBF2-8873-4861-9243-D4E283534975'-- This can be any combination of uppercase and lowercase.
                    sqlStatement += " AND V.Created > '" + strDt + "' ";                        //'2000-01-01 12:00:00 AM'
                    sqlStatement += " AND(P.Status = 'Completed' or(P.Status = 'Active' and TIMESTAMPDIFF(MINUTE, P.`Updated`, NOW()) > 15)) ";
                    sqlStatement += " ORDER BY P_Created ASC ";



                    appLog.Info("-");
                    appLog.Info(sqlStatement);
                    appLog.Info("-");

                    var cmd = new MySqlCommand(sqlStatement, conn);
                    cmd.CommandType = CommandType.Text;

                    dataReader = cmd.ExecuteReader();

                    while (dataReader.Read())
                    {
                        List<string> entry = new List<string>();

                        entry.Add(dataReader["Firstname"].ToString());  //0
                        entry.Add(dataReader["LastName"].ToString());   //1
                        entry.Add(dataReader["HomePhone"].ToString());  //2
                        entry.Add(dataReader["WorkPhone"].ToString());  //3
                        entry.Add(dataReader["V_Created"].ToString());  //4
                        entry.Add(((DateTime)dataReader["V_Created"]).Ticks.ToString());    //5
                        entry.Add(dataReader["EstimateHomeValue"].ToString());  //6
                        entry.Add(dataReader["Address"].ToString());    //7
                        entry.Add(dataReader["City"].ToString());       //8
                        entry.Add(dataReader["StateID"].ToString());    //9
                        entry.Add(dataReader["Abbr"].ToString());      //10
                        entry.Add(dataReader["Zip"].ToString());        //11
                        entry.Add(dataReader["SqFt"].ToString());       //12
                        entry.Add(dataReader["Pool"].ToString());       //13
                        entry.Add(dataReader["Parking"].ToString());    //14    
                        entry.Add(dataReader["Occupied"].ToString());   //15
                        entry.Add(dataReader["FirstMortgageBalance"].ToString());   //16
                        entry.Add(dataReader["FirstMonthlyPayment"].ToString());    //17
                        entry.Add(dataReader["FirstTaxIncluded"].ToString());       //18
                        entry.Add(dataReader["SecondMortgageBalance"].ToString());  //19
                        entry.Add(dataReader["SecondMonthlyPayment"].ToString());   //20
                        entry.Add(dataReader["SecondTaxIncluded"].ToString());      //21
                        entry.Add(dataReader["CurrentOnPayment"].ToString());       //22
                        entry.Add(dataReader["BackPaymentsOwed"].ToString());       //23
                        entry.Add(dataReader["ListedRealtor"].ToString());          //24
                        entry.Add(dataReader["Condition"].ToString());              //25
                        entry.Add(dataReader["WhySelling"].ToString());             //26
                        entry.Add(dataReader["HowSoon"].ToString());                //27
                        entry.Add(dataReader["MainObjective"].ToString());          //28
                        entry.Add(dataReader["P_Created"].ToString());              //29
                        //entry.Add(DBNull.Value != dataReader["P_Created"] ? ((DateTime)dataReader["P_Created"]).Ticks.ToString() : string.Empty);   //30
                        entry.Add(DBNull.Value != dataReader["P_Created"] ? (DateTime.Parse(dataReader["P_Created"].ToString())).Ticks.ToString() : string.Empty);   //30                        
                        entry.Add(dataReader["P_Updated"].ToString());              //31
                        //entry.Add(DBNull.Value != dataReader["P_Updated"] ? ((DateTime)dataReader["P_Updated"]).Ticks.ToString() : string.Empty);   //32
                        entry.Add(DBNull.Value != dataReader["P_Updated"] ? (DateTime.Parse(dataReader["P_Updated"].ToString())).Ticks.ToString() : string.Empty);   //32
                        //entry.Add(DBNull.Value != dataReader["LeadDate"] ? ((DateTime)dataReader["LeadDate"]).Ticks.ToString() : string.Empty);   //33
                        //entry.Add(dataReader["PGID"].ToString());       //34
                        //entry.Add(dataReader["UniqueTag"].ToString());       //35

                        resList.Add(entry);
                    }

                    appLog.Info("Got: " + (null != resList ? resList.Count.ToString() : "null") + " records!");
                    appLog.Info("=====================");
                }
                catch (Exception exc)
                {
                    string error = exc.Message + Environment.NewLine + exc.InnerException + Environment.NewLine + exc.Source;
                    appLog.Error("ERROR:" + error);
                    appLog.Info("=====================");
                }
                finally
                {
                    if (null != dataReader)
                        dataReader.Close();
                }
            }

            return resList;
        }

        public List<List<string>> GetEntries_V3(Guid pgId, long ticks, Guid uniqueTag, out string info)
        {
            MySqlDataReader dataReader = null;
            List<List<string>> resList = new List<List<string>>();
            info = "start | ";
            string outParam = string.Empty;

            try
            {
                if (null == _conn || ConnectionState.Open != _conn.State)
                {
                    InitConnection(out outParam);
                    info += outParam + " | ";
                }

                var cmd = new MySqlCommand("GetAAHObject_V2", _conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("@ProfitGrabberID", pgId));
                cmd.Parameters.Add(new SqlParameter("@LeadDate", new DateTime(ticks, DateTimeKind.Unspecified)));
                cmd.Parameters.Add(new SqlParameter("@UniqueTag", uniqueTag));

                dataReader = cmd.ExecuteReader();

                while (dataReader.Read())
                {
                    List<string> entry = new List<string>();

                    entry.Add(dataReader["Firstname"].ToString());  //0
                    entry.Add(dataReader["LastName"].ToString());   //1
                    entry.Add(dataReader["HomePhone"].ToString());  //2
                    entry.Add(dataReader["WorkPhone"].ToString());  //3
                    entry.Add(dataReader["V_Created"].ToString());  //4
                    entry.Add(((DateTime)dataReader["V_Created"]).Ticks.ToString());    //5
                    entry.Add(dataReader["EstimateHomeValue"].ToString());  //6
                    entry.Add(dataReader["Address"].ToString());    //7
                    entry.Add(dataReader["City"].ToString());       //8
                    entry.Add(dataReader["StateID"].ToString());    //9
                    entry.Add(dataReader["Title"].ToString());      //10
                    entry.Add(dataReader["Zip"].ToString());        //11
                    entry.Add(dataReader["SqFt"].ToString());       //12
                    entry.Add(dataReader["Pool"].ToString());       //13
                    entry.Add(dataReader["Parking"].ToString());    //14    
                    entry.Add(dataReader["Occupied"].ToString());   //15
                    entry.Add(dataReader["FirstMortgageBalance"].ToString());   //16
                    entry.Add(dataReader["FirstMonthlyPayment"].ToString());    //17
                    entry.Add(dataReader["FirstTaxIncluded"].ToString());       //18
                    entry.Add(dataReader["SecondMortgageBalance"].ToString());  //19
                    entry.Add(dataReader["SecondMonthlyPayment"].ToString());   //20
                    entry.Add(dataReader["SecondTaxIncluded"].ToString());      //21
                    entry.Add(dataReader["CurrentOnPayment"].ToString());       //22
                    entry.Add(dataReader["BackPaymentsOwed"].ToString());       //23
                    entry.Add(dataReader["ListedRealtor"].ToString());          //24
                    entry.Add(dataReader["Condition"].ToString());              //25
                    entry.Add(dataReader["WhySelling"].ToString());             //26
                    entry.Add(dataReader["HowSoon"].ToString());                //27
                    entry.Add(dataReader["MainObjective"].ToString());          //28
                    entry.Add(dataReader["P_Created"].ToString());              //29
                    entry.Add(DBNull.Value != dataReader["P_Created"] ? ((DateTime)dataReader["P_Created"]).Ticks.ToString() : string.Empty);   //30
                    entry.Add(dataReader["P_Updated"].ToString());              //31
                    entry.Add(DBNull.Value != dataReader["P_Updated"] ? ((DateTime)dataReader["P_Created"]).Ticks.ToString() : string.Empty);   //32
                    entry.Add(DBNull.Value != dataReader["LeadDate"] ? ((DateTime)dataReader["LeadDate"]).Ticks.ToString() : string.Empty);   //33
                    entry.Add(dataReader["PGID"].ToString());       //34
                    entry.Add(dataReader["UniqueTag"].ToString());       //35

                    resList.Add(entry);

                    info += " added entry | ";
                }
            }
            catch (Exception exc)
            {                
                info += exc.Message + Environment.NewLine + exc.InnerException + " | ";
            }
            finally
            {
                if (null != dataReader)
                    dataReader.Close();

                info += " Closed Reader";
            }

            return resList;
        }
    }
}
