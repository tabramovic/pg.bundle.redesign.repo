﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;

//Added
//using System.Windows.Media.Imaging;
using Newtonsoft.Json;

namespace GoogleAPI
{
    public class AddressToGeoCodeConverter
    {
        static readonly log4net.ILog applog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public string Address { get; set; }

        public AddressToGeoCodeConverter()
        { }

        public AddressToGeoCodeConverter(string address)
        {
            this.Address = address;                        
        }

        public bool GetGeoCode(string address, out bool correctAddress, out string err, out double longtitude, out double latitude, string apiKey)
        {
            bool res = true;
            correctAddress = false;            
            longtitude = latitude = 0;
            err = string.Empty;

            //address = "http://maps.google.com/maps/api/geocode/json?address=1600+Amphitheatre+Parkway,+Mountain+View,+CA&sensor=false";
            //address = "http://maps.google.com/maps/api/geocode/json?address=1600 Amphitheatre Parkway, Mountain View, CA&sensor=false";
            
            GoogleGeoCodeResponse geoRes = null;

            try 
            {
                address = address.Replace(' ', '+');
                string uri = "https://maps.google.com/maps/api/geocode/json?address=" + address + "&sensor=false";
                uri += "&key=" + apiKey;

                string result = new System.Net.WebClient().DownloadString(uri);

                geoRes = JsonConvert.DeserializeObject<GoogleGeoCodeResponse>(result);
                if ("OK" == geoRes.status)
                {
                    correctAddress = true;

                    try
                    {
                        longtitude = geoRes.results[0].geometry.location.lng;
                        latitude = geoRes.results[0].geometry.location.lat;

                        applog.Info($"GetGeoCode for: {address} returned: lng/lat: {longtitude} / {latitude}");
                    }
                    catch (Exception e)
                    {
                        longtitude = latitude = 0;
                        correctAddress = false;
                        res = false;

                        applog.Error($"GetGeoCode for: {address} failed with: {e}");
                    }
                }
                else
                {
                    err = geoRes.status;
                }

            }
            catch (Exception exc)
            {                
                res = false;
                err = exc.Message;

                applog.Error($"GetGeoCode for: {address} failed with: {exc}");
            }

            return res;
        }
    }
}
