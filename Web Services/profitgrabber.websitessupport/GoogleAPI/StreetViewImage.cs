﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;
using System.Drawing;
using System.Net;

//Added
//using System.Windows.Media.Imaging;

namespace GoogleAPI
{
    public enum MapType
    {
        StreetView, 
        roadMap,
        terrain,
        satellite,
        hybrid
    }

    public class StreetViewImage
    {
        static readonly log4net.ILog applog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public StreetViewImage()
        { }

        public Image GetImage(MapType imgType, int zoom, int width, int height, double longtitude, double latitude, int heading, string apiKey)
        {
            Image img = null;

            try
            {
                string uri = string.Empty;

                if (MapType.StreetView == imgType)
                {
                    //uri = "http://maps.googleapis.com/maps/api/streetview?size=400x400&location=40.720032,%20-73.988354&sensor=false&key=AIzaSyDalNbgzT4gU9utIiJxjD_1Q5g3YryZ-oI";
                    uri = "https://maps.googleapis.com/maps/api/streetview?size=" + width + "x" + height + "&location=" + latitude + "," + longtitude + "&heading=" + heading + "&sensor=false";
                }
                else if (MapType.roadMap == imgType || MapType.terrain == imgType || MapType.satellite == imgType || MapType.hybrid == imgType)
                {
                    //uri = "http://maps.googleapis.com/maps/api/staticmap?center=40.714728,-73.998672&zoom=" + zoom.ToString() + "&size=400x400&maptype=roadmap&sensor=false";
                    //uri = "http://maps.googleapis.com/maps/api/staticmap?center=40.714728,-73.998672&zoom=" + zoom.ToString() + "&size=400x400&maptype=terrain&sensor=false";
                    //uri = "http://maps.googleapis.com/maps/api/staticmap?center=40.714728,-73.998672&zoom=" + zoom.ToString() + "&size=400x400&maptype=satellite&sensor=false";
                    //uri = "http://maps.googleapis.com/maps/api/staticmap?center=40.714728,-73.998672&zoom=" + zoom.ToString() + "&size=400x400&maptype=hybrid&sensor=false";
                    //uri = "http://maps.googleapis.com/maps/api/staticmap?center=Williamsburg,Brooklyn,NY&zoom=" + zoom.ToString() + "&size=400x400&markers=color:blue%7Clabel:S%7C11211&sensor=false";

                    uri = "https://maps.googleapis.com/maps/api/staticmap?center=" + latitude + "," + longtitude + "&zoom=" + zoom + "&size=" + width + "x" + height + "&maptype=" + imgType.ToString() + "&sensor=false";
                }

                //uri += "&key=AIzaSyDalNbgzT4gU9utIiJxjD_1Q5g3YryZ-oI";
                uri += "&key=" + apiKey;
                                                
                var wc = new WebClient();
                img = Image.FromStream(wc.OpenRead(uri));               

                
            }
            catch (Exception e)
            {
                applog.Error($"GetImage failed with: {e}");
                img = null;
            }

            return img;
        }


        public Image GetImageWithMarkers(MapType mapType, int zoom, int imgWidth, int imgHeight, List<string> addresses, string apiKey, out bool anyAddressFound)
        {
            bool correctAddress;
            string err;
            double longtitude;
            double latitude;
            List<Location> locations = new List<Location>();
            anyAddressFound = false;

            foreach (string address in addresses)
            {
                bool succConversion = new AddressToGeoCodeConverter().GetGeoCode(address, out correctAddress, out err, out longtitude, out latitude, apiKey);

                if (succConversion)
                {
                    locations.Add(new Location(latitude, longtitude));
                    anyAddressFound = true;
                }
                else
                    locations.Add(new Location(0, 0));
            }

            if (anyAddressFound)
                return GetImageWithMarkers(mapType, zoom, imgWidth, imgHeight, locations, apiKey);
            else
                return null;
        }

        public Image GetImageWithMarkers(MapType mapType, int zoom, int imgWidth, int imgHeight, List<Location> locations, string apiKey)
        {
            /*  sample URL
             * 
                http://maps.googleapis.com/maps/api/staticmap?zoom=13&size=600x300&maptype=roadmap
                &markers=color:blue|label:A|40.702147,-74.015794
                &markers=color:green|label:B|40.711614,-74.012318
                &markers=color:red|label:C|40.718217,-73.998284
                &sensor=false
             * 
            */

            Image img = null;
            Location emptyLocation = new Location(0, 0);

            try
            {
                string uri = string.Empty;

                uri = "https://maps.googleapis.com/maps/api/staticmap?"
                 + (((null != locations && locations.Count > 0) && (!emptyLocation.Equals(locations[0]))) ? "center=" + locations[0].lat.ToString() + "," + locations[0].lng.ToString() + "&" : string.Empty)
                 + "zoom=" + zoom.ToString()
                 + "&size=" + imgWidth.ToString() + "x" + imgHeight.ToString()
                 + "&maptype=" + mapType.ToString();

                int iCnt = 0;
                foreach (Location loc in locations)
                {
                    iCnt++;

                    if (emptyLocation.Equals(loc))
                        continue;

                    uri += "&markers=color:" + (1 == iCnt ? "red" : "yellow") + "|label:" + Number2String(iCnt, true) + "|" + loc.lat.ToString() + "," + loc.lng.ToString();                                        
                }

                uri += "&sensor=false";
                uri += "&key=" + apiKey;
                
                var wc = new WebClient();
                img = Image.FromStream(wc.OpenRead(uri));

            }
            catch (Exception exc)
            {
                applog.Error($"GetImageWithMarkers failed with: {exc}");
                img = null;
            }

            return img;
        }

        private String Number2String(int number, bool isCaps)
        {
            Char c = (Char)((isCaps ? 65 : 97) + (number - 1));
            return c.ToString();
        }
    }
}
