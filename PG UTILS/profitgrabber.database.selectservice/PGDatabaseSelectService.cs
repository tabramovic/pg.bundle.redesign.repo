using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.ServiceProcess;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters;
using System.Runtime.Serialization.Formatters.Binary;
//using System.Runtime.Serialization.Formatters.Soap;
using PGSerializationSelectServer;

namespace PGDatabaseSelectService
{
	public class PGDatabaseSelectService : System.ServiceProcess.ServiceBase
	{
/*		[Serializable]
		public class DabaseQueryPacket
		{
			public DateTime Time;
			public Guid Sender;
			public string Hostname;
			public string ServerName;
			public string ConnectionString;
			public Guid PacketID;
		}
*/
		static Guid Sender = Guid.NewGuid();

		const int UDPPortReceive = 11006;
		const int UDPPortSend = 11007;

		static UdpClient udpClientReceive = new UdpClient(UDPPortReceive);

		static public void listenerThread()
		{
			IPEndPoint RemoteIpEndPoint = new IPEndPoint(IPAddress.Any, 0);

			//if any packet recieved on port broadcast our connection string
			while(true)
			{
				//Byte[] received = 
				udpClientReceive.Receive(ref RemoteIpEndPoint);
//				udpClientReceive.re
//				Console.WriteLine("This message was sent from " +
//					RemoteIpEndPoint.Address.ToString() +
//					" on their port number " +
//					RemoteIpEndPoint.Port.ToString());

				/*
				MemoryStream memoryStream = new MemoryStream(received);

				IFormatter formatter = new BinaryFormatter();

				DabaseQueryPacket receivedPacket = (DabaseQueryPacket)formatter.Deserialize(memoryStream);

				memoryStream.Close();
				*/

				sendPacket();
			}
		}

		static void sendPacket()
		{
			DabaseQueryPacket packet = new DabaseQueryPacket();

			packet.PacketID = Guid.NewGuid();
			packet.Sender = Sender;
			packet.Time = DateTime.Now;
			packet.Hostname = System.Net.Dns.GetHostName();
			packet.ServerName = packet.Hostname+@"\PG_DB2";
			packet.ConnectionString = "(unused)";

			UdpClient udpClient = new UdpClient();
			
			MemoryStream memoryStream = new MemoryStream();

			IFormatter formatter = new BinaryFormatter();
//			IFormatter formatter = new SoapFormatter();

			((BinaryFormatter)formatter).AssemblyFormat = FormatterAssemblyStyle.Simple;
			formatter.Serialize(memoryStream, packet);

			byte[] sendArray = memoryStream.ToArray();
			
			System.Net.IPEndPoint endPoint = new IPEndPoint(IPAddress.Broadcast, UDPPortSend);

			udpClient.Send(sendArray, sendArray.Length, endPoint);

			//udpClient.Send(sendArray, sendArray.Length, RemoteIpEndPoint);

			memoryStream.Close();
		}

		Thread listener = new Thread(new ThreadStart(listenerThread));

		Thread netLockListener = new Thread(new ThreadStart(NetLock.listenerThread));

		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public PGDatabaseSelectService()
		{
			// This call is required by the Windows.Forms Component Designer.
			InitializeComponent();

			// TODO: Add any initialization after the InitComponent call
		}

		// The main entry point for the process
		static void Main()
		{
			System.ServiceProcess.ServiceBase[] ServicesToRun;
	
			// More than one user Service may run within the same process. To add
			// another service to this process, change the following line to
			// create a second service object. For example,
			//
			//   ServicesToRun = new System.ServiceProcess.ServiceBase[] {new Service1(), new MySecondUserService()};
			//
			ServicesToRun = new System.ServiceProcess.ServiceBase[] { new PGDatabaseSelectService() };

			System.ServiceProcess.ServiceBase.Run(ServicesToRun);
		}

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			// 
			// PGDatabaseSelectService
			// 
			this.ServiceName = "PGDatabaseSelectService";
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		/// <summary>
		/// Set things in motion so your service can do its work.
		/// </summary>
		protected override void OnStart(string[] args)
		{
			// TODO: Add code here to start your service.
			listener.Start();
			netLockListener.Start();
		}
 
		/// <summary>
		/// Stop this service.
		/// </summary>
		protected override void OnStop()
		{
			// TODO: Add code here to perform any tear-down necessary to stop your service.
			listener.Abort();

			netLockListener.Abort();

			//smarter way
			try
			{
				udpClientReceive.Close();
			}
			catch
			{}

			/*
			//unblock thread :)
			UdpClient udpClient = new UdpClient();
			byte[] sendArray = {0};

			System.Net.IPEndPoint endPoint = new IPEndPoint(IPAddress.Loopback, UDPPortSend);

			udpClient.Send(sendArray, sendArray.Length, endPoint);
			*/
		}
	}
}
