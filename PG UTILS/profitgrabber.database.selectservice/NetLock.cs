using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.ServiceProcess;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters;
using System.Runtime.Serialization.Formatters.Binary;
//using System.Runtime.Serialization.Formatters.Soap;
using PGSerializationSelectServer;
using PGNetLock;

namespace PGDatabaseSelectService
{
	public class AppContact
	{
		public Guid contact;
		public Guid app;
		public DateTime time;
	}
	/// <summary>
	/// Summary description for NetLock.
	/// </summary>
	public class NetLock
	{
		const int LockValidTime = 30;

		const int UDPPortReceive = 11026;
		const int UDPPortSend = 11027;

		static ArrayList lockedList = new ArrayList();

		static UdpClient udpClientReceive = new UdpClient(UDPPortReceive);
		static UdpClient udpClientSend = new UdpClient();

		static Mutex mutex = new Mutex();

		static public void listenerThread()
		{
			IPEndPoint RemoteIpEndPoint = new IPEndPoint(IPAddress.Any, 0);

			//if any packet recieved on port broadcast our connection string
			while(true)
			{
				Byte[] received = 
				udpClientReceive.Receive(ref RemoteIpEndPoint);
				//				udpClientReceive.re
				//				Console.WriteLine("This message was sent from " +
				//					RemoteIpEndPoint.Address.ToString() +
				//					" on their port number " +
				//					RemoteIpEndPoint.Port.ToString());

				MemoryStream memoryStream = new MemoryStream(received);

				IFormatter formatter = new BinaryFormatter();

				PGNetLock.PGNetLock receivedPacket = (PGNetLock.PGNetLock)formatter.Deserialize(memoryStream);

				memoryStream.Close();

				PGNetLock.PGNetLock packetSend = new PGNetLock.PGNetLock();
				packetSend.id_contact = receivedPacket.id_contact;
				packetSend.request_lock = false;
				packetSend.id_app = Guid.Empty;

				if(receivedPacket.request_lock)
				{
					 packetSend.id_app_holding_lock = lock_contact(receivedPacket.id_contact, receivedPacket.id_app);
				}
				else
				{
					packetSend.id_app_holding_lock = unlock_contact(receivedPacket.id_contact, receivedPacket.id_app);
				}

				MemoryStream memoryStreamSend = new MemoryStream();
				IFormatter formatterSend = new BinaryFormatter();
				((BinaryFormatter)formatterSend).AssemblyFormat = FormatterAssemblyStyle.Simple;
				formatterSend.Serialize(memoryStreamSend, packetSend);

				byte[] sendArray = memoryStreamSend.ToArray();
			
				//				System.Net.IPEndPoint endPoint = new IPEndPoint(IPAddress.Broadcast, UDPPortSend);
				System.Net.IPEndPoint endPoint = new IPEndPoint(RemoteIpEndPoint.Address, UDPPortSend);

				udpClientSend.Send(sendArray, sendArray.Length, endPoint);

				memoryStreamSend.Close();
			}
		}

		private static Guid unlock_contact(Guid contact, Guid app)
		{
			mutex.WaitOne();

			foreach(AppContact ac in lockedList)
			{
				if(ac.contact == contact)
				{
					if(ac.app == app)
					{
						lockedList.Remove(ac);
						mutex.ReleaseMutex();
						return Guid.Empty;
					}
					else
					{
						mutex.ReleaseMutex();
						return ac.app;
					}
				}
			}

			mutex.ReleaseMutex();
			return Guid.Empty;
		}


		//returns app who locked contact
		private static Guid lock_contact(Guid contact, Guid app)
		{
			mutex.WaitOne();
			again:
			foreach(AppContact ac in lockedList)
			{
				//1 app can hold only 1 lock, so we will remove all other app locks 
				if(ac.time.AddSeconds(LockValidTime) < DateTime.Now || ac.app == app)
 				{
					lockedList.Remove(ac);
					goto again;
				}

				if(ac.contact == contact)
				{
					mutex.ReleaseMutex();
					return ac.app;
				}
				//if(ac.app == app) return ac.app;

			}

			AppContact acc = new AppContact();

			acc.app = app;
			acc.contact = contact;
			acc.time = DateTime.Now;

			lockedList.Add(acc);
			mutex.ReleaseMutex();
			return app;
		}

		public NetLock()
		{
		}
	}
}
