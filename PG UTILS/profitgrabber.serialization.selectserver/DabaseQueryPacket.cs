using System;

namespace PGSerializationSelectServer
{
	/// <summary>
	/// Summary description for Class1.
	/// </summary>
	[Serializable]
	public class DabaseQueryPacket
	{
		public DateTime Time;
		public Guid Sender;
		public string Hostname;
		public string ServerName;
		public string ConnectionString;
		public string IPAddress;
		public Guid PacketID;
		public Guid GRFU1;
		public Guid GRFU2;
		public string SRFU1;
		public string SRFU2;
	}
}
