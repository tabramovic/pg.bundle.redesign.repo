﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;

using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using Microsoft.Win32;

namespace PG_Installer
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();            
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            if (IsWindows10())
                this.Text += " (Windows 10)";
        }

        private void _beforeYouInstall_Click(object sender, EventArgs e)
        {
            Process p = new Process();
            p.StartInfo.FileName = DefaultBrowserSupport.getDefaultBrowser();
            p.StartInfo.Arguments = "http://www.profitgrabber.com/training/video.php?videoURL=PG-Training-Videos/Installing-PG-Firewall-Win7.mp4&videoType=800x600";
            p.Start();
        }

        private void _trainingCourse_Click(object sender, EventArgs e)
        {
            Process p = new Process();
            p.StartInfo.FileName = DefaultBrowserSupport.getDefaultBrowser();
            p.StartInfo.Arguments = "http://www.profitgrabber.com/training";
            p.Start();
        }

        private void _helpSupport_Click(object sender, EventArgs e)
        {            
            Process p = new Process();
            p.StartInfo.FileName = DefaultBrowserSupport.getDefaultBrowser();
            p.StartInfo.Arguments = "http://www.myrealestatesupport.com/";
            p.Start();
        }

        private void _install_Click(object sender, EventArgs e)
        {            
            try
            {
                Process p = new Process();

                if (IsWindows10())
                    p.StartInfo.FileName = Application.StartupPath + @"\dotNet4\setup.exe";
                else
                    p.StartInfo.FileName = Application.StartupPath + @"\dotNet2\setup.exe";

                //MessageBox.Show(p.StartInfo.FileName);

                p.Start();
            }
            catch (Exception exc)
            {
                MessageBox.Show(string.Format("{0}", exc));
            }
        }

        bool IsWindows10()
        {
            try
            {
                var reg = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Microsoft\Windows NT\CurrentVersion");
                string productName = (string)reg.GetValue("ProductName");
                return productName.StartsWith("Windows 10");
            }
            catch(Exception exc)
            {
                return false;
            }
        }

        private void _checkFirewall_Click(object sender, EventArgs e)
        {
            //MessageBox.Show(this.Size.ToString());
            try
            {
                bool found = false;
                FirewallChecker fc = new FirewallChecker();
                List<FirewallState> extFirewalls = fc.CheckExternal();

                StringBuilder sbRes = new StringBuilder();
                if (null != extFirewalls)
                {
                    foreach (FirewallState fw in extFirewalls.ToArray())
                    {
                        found = true;
                        sbRes.Append(string.Format("Firewall name: {0}, State: {1}, Value: {2}{3}", fw.FirewallName, fw.StateName, fw.StateValue, Environment.NewLine));
                    }
                }

                if (found)
                    MessageBox.Show(sbRes.ToString(), "ProfitGrabber Pro Installer", MessageBoxButtons.OK, MessageBoxIcon.Information);
                else
                    MessageBox.Show("No external Firewalls found!", "ProfitGrabber Pro Installer", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception exc)
            {
                MessageBox.Show(string.Format("{0}", exc), "ProfitGrabber Pro Installer", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
    }
}
