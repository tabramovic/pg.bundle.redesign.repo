﻿namespace PG_Installer
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this._pbHeader = new System.Windows.Forms.PictureBox();
            this._beforeYouInstall = new System.Windows.Forms.Button();
            this._helpSupport = new System.Windows.Forms.Button();
            this._install = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this._checkFirewall = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this._pbHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // _pbHeader
            // 
            this._pbHeader.Dock = System.Windows.Forms.DockStyle.Fill;
            this._pbHeader.Location = new System.Drawing.Point(0, 0);
            this._pbHeader.Margin = new System.Windows.Forms.Padding(8);
            this._pbHeader.Name = "_pbHeader";
            this._pbHeader.Size = new System.Drawing.Size(1527, 862);
            this._pbHeader.TabIndex = 0;
            this._pbHeader.TabStop = false;
            // 
            // _beforeYouInstall
            // 
            this._beforeYouInstall.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._beforeYouInstall.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._beforeYouInstall.Location = new System.Drawing.Point(40, 379);
            this._beforeYouInstall.Margin = new System.Windows.Forms.Padding(8);
            this._beforeYouInstall.Name = "_beforeYouInstall";
            this._beforeYouInstall.Size = new System.Drawing.Size(395, 89);
            this._beforeYouInstall.TabIndex = 1;
            this._beforeYouInstall.Text = "Watch First";
            this._beforeYouInstall.UseVisualStyleBackColor = true;
            this._beforeYouInstall.Click += new System.EventHandler(this._beforeYouInstall_Click);
            // 
            // _helpSupport
            // 
            this._helpSupport.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._helpSupport.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._helpSupport.Location = new System.Drawing.Point(40, 589);
            this._helpSupport.Margin = new System.Windows.Forms.Padding(8);
            this._helpSupport.Name = "_helpSupport";
            this._helpSupport.Size = new System.Drawing.Size(395, 89);
            this._helpSupport.TabIndex = 2;
            this._helpSupport.Text = "Help && Support";
            this._helpSupport.UseVisualStyleBackColor = true;
            this._helpSupport.Click += new System.EventHandler(this._helpSupport_Click);
            // 
            // _install
            // 
            this._install.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._install.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._install.Location = new System.Drawing.Point(40, 484);
            this._install.Margin = new System.Windows.Forms.Padding(8);
            this._install.Name = "_install";
            this._install.Size = new System.Drawing.Size(395, 89);
            this._install.TabIndex = 3;
            this._install.Text = "Start Installation";
            this._install.UseVisualStyleBackColor = true;
            this._install.Click += new System.EventHandler(this._install_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.1F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(17, 288);
            this.label1.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(1235, 78);
            this.label1.TabIndex = 5;
            this.label1.Text = "Welcome to ProfitGrabber Pro Installer";
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(451, 484);
            this.label2.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(1020, 105);
            this.label2.TabIndex = 6;
            this.label2.Text = "Click here after you disabled your Firewall and start the installation.";
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(451, 589);
            this.label3.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(1030, 103);
            this.label3.TabIndex = 7;
            this.label3.Text = "If you need any help, click here to contact us.";
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(451, 379);
            this.label4.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(1065, 99);
            this.label4.TabIndex = 8;
            this.label4.Text = "Watch this short video BEFORE you start the installation process.";
            // 
            // _checkFirewall
            // 
            this._checkFirewall.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._checkFirewall.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._checkFirewall.Location = new System.Drawing.Point(40, 694);
            this._checkFirewall.Margin = new System.Windows.Forms.Padding(8);
            this._checkFirewall.Name = "_checkFirewall";
            this._checkFirewall.Size = new System.Drawing.Size(395, 89);
            this._checkFirewall.TabIndex = 9;
            this._checkFirewall.Text = "Check Firewall";
            this._checkFirewall.UseVisualStyleBackColor = true;
            this._checkFirewall.Click += new System.EventHandler(this._checkFirewall_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(1527, 251);
            this.pictureBox1.TabIndex = 10;
            this.pictureBox1.TabStop = false;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(16F, 31F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(216)))), ((int)(((byte)(240)))));
            this.ClientSize = new System.Drawing.Size(1527, 862);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this._checkFirewall);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this._install);
            this.Controls.Add(this._helpSupport);
            this.Controls.Add(this._beforeYouInstall);
            this.Controls.Add(this._pbHeader);
            this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(65)))), ((int)(((byte)(131)))));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(8);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ProfitGrabber Pro Installer";
            ((System.ComponentModel.ISupportInitialize)(this._pbHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox _pbHeader;
        private System.Windows.Forms.Button _beforeYouInstall;
        private System.Windows.Forms.Button _helpSupport;
        private System.Windows.Forms.Button _install;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button _checkFirewall;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}

