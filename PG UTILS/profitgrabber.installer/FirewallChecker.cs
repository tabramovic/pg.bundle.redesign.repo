﻿using System;
using System.Collections.Generic;

using System.Text;
using System.Management;
using System.Diagnostics;

namespace PG_Installer
{
    public class FirewallState
    {
        public string FirewallName { get; set; }
        public string StateName { get; set; }
        public string StateValue { get; set; }
    }

    public class FirewallChecker
    {
        public FirewallChecker()
        { }

        public List<FirewallState> CheckExternal()
        {
            List<FirewallState> firewallsFound = new List<FirewallState>();

            try
            {
                //select the proper wmi namespace depending of the windows version
                string WMINameSpace = System.Environment.OSVersion.Version.Major > 5 ? "SecurityCenter2" : "SecurityCenter";

                ManagementScope Scope;
                Scope = new ManagementScope(String.Format("\\\\{0}\\root\\{1}", "localhost", WMINameSpace), null);

                Scope.Connect();
                ObjectQuery Query = new ObjectQuery("SELECT * FROM FirewallProduct");
                ManagementObjectSearcher Searcher = new ManagementObjectSearcher(Scope, Query);

                foreach (ManagementObject WmiObject in Searcher.Get())
                {
                    FirewallState fws = new FirewallState();
                    fws.FirewallName = WmiObject["displayName"].ToString();

                    Console.WriteLine("{0,-35} {1,-40}", "Firewall Name", WmiObject["displayName"]);
                    if (System.Environment.OSVersion.Version.Major < 6) //is XP ?
                    {
                        Console.WriteLine("{0,-35} {1,-40}", "Enabled", WmiObject["enabled"]);
                        fws.StateName = "Enabled";
                        fws.StateValue = WmiObject["enabled"].ToString();
                    }
                    else
                    {
                        Console.WriteLine("{0,-35} {1,-40}", "State", WmiObject["productState"]);
                        fws.StateName = "State";
                        fws.StateValue = WmiObject["productState"].ToString();
                    }

                    firewallsFound.Add(fws);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(String.Format("Exception {0} Trace {1}", e.Message, e.StackTrace));
            }

            return firewallsFound;
        }
    }
}
