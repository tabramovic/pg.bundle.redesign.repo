using System;

namespace PGNetLock
{
	/// <summary>
	/// Summary description for Class1.
	/// </summary>
	[Serializable]
	public class PGNetLock
	{
		//if true, req lock, if not req unlock
		public bool request_lock;

		public Guid id_contact;

		//if app id null, packet is sent by server
		public Guid id_app;

		public Guid id_app_holding_lock;
		
		public Guid grfu1;
		public Guid grfu2;
		public bool brfu1;
		public bool brfu2;
		public string srfu1;
		public string srfu2;

		public PGNetLock()
		{
		}
	}
}
